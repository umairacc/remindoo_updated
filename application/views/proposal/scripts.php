<?php

$taxes_ids = array_column($taxes, 'id');
$taxes_rates = array_column($taxes,'tax_rate');

$tax_vals = array_combine($taxes_ids, $taxes_rates);

?>

<textarea id="template_own_content" style="display: none;"><?php if(isset($records['proposal_contents'])){ echo $records['proposal_contents']; } ?></textarea>
<textarea id="imagination" style="display: none;"><?php if(isset($records['images_section'])){ echo $records['images_section']; } ?></textarea>


<script type="text/javascript">
  
  function appendation()
  { 
    if($("#discount_amount").val()=='0')
    {
       if($("#discount_percentage").val() > '0')
       { 
    //  console.log('###########******************0');
    //console.log($("#discount_percentage").val());
          $("#discount_amount").val('0'); 
          localStorage.setItem("discount_amount", "on");
          var discount_amount=$("#discount_percentage").val();     
          $(".discount_amount").html('');
          $(".discount_amount").append(discount_amount);
          $(".discount_amount").show();
          $(".symbol").hide();
          //   $(".discount_add_option").css('display','none');
          $("#input_discount").val('');
       }
       else 
       {
          $("#discount_percentage").val('0');
          $(".discount_amount").html('');
          $(".discount_amount").append('0');
          localStorage.removeItem('discount_amount');
       }
    }
    else if($("#discount_amount").val() > '0')
    { 
     // console.log('###########******************1');
  //console.log($("#discount_percentage").val());
        $("#discount_percentage").val('0');
        localStorage.removeItem('discount_amount');
        var discount_amount=$("#discount_amount").val();     
        $(".discount_amount").html('');
        $(".discount_amount").append(discount_amount);
        $(".discount_amount").show();
        $(".symbol").show();
     //   $(".discount_add_option").css('display','none');
        $("#input_discount").val('');
    }
    else 
    {
       $("#discount_amount").val('0');
    }
    sub_total();
  }

</script>

<script type="text/javascript">
 $(function() 
 {   
   $("#Company_form").validate(
   {  
    rules: {      
      first_name: {required: true},     
      company_name: {required: true},
      last_name: {required: true},
      email: {required: true},
     },
    // Specify validation error messages
     messages: {
       proposal_name: "Please enter your proposal Name", 
       company_name: "Please Select Company Name", 
       proposal_type: "Please select your proposal Type",      
     },  
   });
   window.invoice_change_shown = "";
 });

$( document ).ready(function() {

  <?php if(!empty($get_lead_id) || !empty($records['lead_id'])){ ?>
       $(".dropdown-sin-4").trigger('click');
  <?php } ?>  

  <?php if(!empty($_SESSION['template_id'])) { ?>
       $('.dropdown-sin-1').trigger('click');
  <?php } ?>

  <?php
  if(isset($records)){ ?>
//console.log('***************###########*********************');
    //check_function();

 <?php } ?>  
});



function qty_check()
{

       //alert('product Working');
       var sum=0;
       $('.serviceprice').each(function() {  
           var qty_1=$(this).parents('.top-field-heads ').find('.service-items').find('.qty').val();
         //  console.log(qty_1);

            var test = $("input[name$='tax']:checked").val();
           if(test=='line_tax'){
           var tax=$(this).parents('.top-field-heads ').find('.tax-field').find('.tax').val();
        //   console.log(tax);
         }else{
            var tax=0;
        //    console.log(tax);
         }

            var test1 = $("input[name$='discount']:checked").val();
           if(test1=='line_discount'){

           var discount=$(this).parents('.top-field-heads ').find('.discount-field').find('.discount').val(); 
           // console.log(discount);
          }else{
           var discount=0; 
          //  console.log(discount);
          }



           var price=$(this).val();

           var tax_amount=(price * qty_1).toFixed(2);
           var dec = (tax/100).toFixed(2);        
           var discount1 = (discount/100).toFixed(2);
         // console.log(dec);
        //  console.log(discount1);

             var mult = (tax_amount* dec).toFixed(2); 
             var dis_count = (tax_amount* discount1).toFixed(2); 
          //    console.log(mult);
         // console.log(dis_count);

           var price_cal=(Number($(this).val()) * Number(qty_1)).toFixed(2); 
        //     console.log("price_cal");
          //     console.log(price_cal);



             var calculation=(Number(price_cal)+ Number(mult)).toFixed(2); 
           //    console.log("calculation");

          //   console.log(calculation);
             //  alert(mult);
             //  alert(dis_count);
           //  var calcPrice=price+mult;

             sum += Number(calculation) - Number(dis_count);
         //    console.log('****');
   //console.log(sum);
           });
    //alert(sum);
    $('.sub_total').html('');
    //$('.sub_total').html(Math.round(sum));
    $('.sub_total').html(sum.toFixed(2));
    product_sub_total();
    subscription_sub_total();
    
    return true;
}
</script>
<!-- send-to popup -->
<script type="text/javascript">
   $( document ).ready(function( $ ) 
   {
     $('.next-step').click(function()
     {          
        var ref_this = $(".addnewclient_pages1 ol.nav-tabs li a.active").attr("href");  
        // console.log(ref_this);
        <?php if(isset($records['receiver_mail_id'])){ ?>
        $("#company_name").removeClass('required');
        <?php  } ?> 
        var empty_flds = 0; 

        $('div'+ref_this+" .required:visible,#company_name").each(function() 
        {   
          if($.trim($(this).val()) == "" || $(this).val() <= '0' || $(this).attr('name') == 'qty[]' || $(this).attr('name') == 'product_qty[]')
          {  
              var input_id = $(this).attr('id'); 

              if(input_id == 'company_name')
              {
                 empty_flds++;
                 $(this).css('border', '1px solid red');
              }
              
              $('input.'+input_id).each(function()
              {
                 if($(this).is(':visible') == true && (($(this).val() == "" || $(this).val() <= '0') || ($(this).attr('name') == 'qty[]' && Number(Number($(this).val())%1) != '0') || ($(this).attr('name') == 'product_qty[]' && Number(Number($(this).val())%1) != '0')))
                 { 
                    empty_flds++;
                    $(this).css('border', '1px solid red');
                 }

                 if(ref_this == '#price_tab') 
                 {
                    if($(this).is(':hidden') == true)
                    { 
                       $(this).css('display','block');
                    }
                 }

              }); 
          } 
        }); 

        var ref_this = $(".addnewclient_pages1 ol.nav-tabs li a.active").attr("href");

        $('#price_tab div .service-table .accordion-views .ser').find('.service-newdesign1 .mandatory_er').html('');

        if(ref_this == '#price_tab' && ($('#price_tab div .service-table .accordion-views .ser').find('.service-newdesign1 .mandatory_er').html() == undefined || $('#price_tab div .service-table .accordion-views .ser').find('.service-newdesign1 .mandatory_er').html() == "") && $('.service-table .accordion-views .ser .add-service').is(':visible') == false && $('.service-table .accordion-views .ser .add-services').is(':visible') == false && $('.saved-contents .top-field-heads:visible').length == '0' && $('.extra-saved-service-content .top-field-heads:visible').length == '0')
        {         
           empty_flds = 'error';
        }        

          if(ref_this=='#start_tab'){
            if($('#receiver_mail_id_old').val()==''){
            //  console.log('empty');
              if($("#company_name").val()==''){
                $(".dropdown-display").css('border', '1px solid red');
              }
            }
            }
        
      
          if(empty_flds=='0')
          {
            $('.addnewclient_pages1  .nav-tabs  .active').closest('li').next('li').find('a').trigger('click');            
            var ref_this = $(".addnewclient_pages1 ol.nav-tabs li a.active").attr("href");
        //    console.log(ref_this);  

            
          if(ref_this=='#price_tab') 
          {                     
            
            $("#currency_symbol").addClass('required');

      
           // console.log('###');
           //   console.log($("#currency_symbol").val());
                 if($("#currency_symbol").val()==''){
               //   console.log('wrong');
                $(".currency_details").find(".dropdown-display").css('border', '1px solid red');
              }
            }

            if(ref_this=='#edit_tab') 
            {           


              $( ".choose:first" ).trigger( "click" );
                  var item_name = [];
                  var item_index = [];
                  $("input.item_name").each(function(i, sel){
                    var selectedVal = $(sel).val(); 
                    if(selectedVal.trim() != "") 
                    {                
                      item_index.push(i);
                      item_name.push(selectedVal);
                    }
                  });         

                  var price = [];                  
                  $("input.service_price").each(function(i, sel){
                    var selectedVal = $(sel).val();
                  if(selectedVal.trim() != "") 
                  {
                    price.push(selectedVal);
                  }
                  });         
                  var unit = [];
                  $("select.unit").each(function(i, sel){
                  var selectedVal = $(sel).val();
                  if(selectedVal.trim() != "" && Object.values(item_index).includes(i)) 
                  { 
                    unit.push(selectedVal);
                  }
                  });    

                //  alert(unit);


                  var qty = [];
                  $("input.qty").each(function(i, sel){
                  var selectedVal = $(sel).val();
                  if(selectedVal.trim() != "") 
                  {
                    qty.push(selectedVal);
                  }
                  });             
             
                  var tax = [];
                  $("input.tax").each(function(i, sel){
                  var selectedVal = $(sel).val();
                  if(selectedVal.trim() != "" && Object.values(item_index).includes(i)) 
                  {
                     tax.push(selectedVal);
                  }
                  });  
            //       alert(tax);

                  var discount = [];
                  $("input.discount").each(function(i, sel){
                  var selectedVal = $(sel).val();
                  if(selectedVal.trim() != "" && Object.values(item_index).includes(i)) 
                  {
                    discount.push(selectedVal);
                  }

                  });  
             //      alert(discount);

                  var description = [];
                   $("textarea.description").each(function(i, sel){
                  var selectedVal = $(sel).val();
                  if(selectedVal.trim() != "") 
                  {
                    description.push(selectedVal);
                  }
                  });  
              //    alert(description);
//
                 var service_optional =[];
                 $("input.optional").each(function(i, sel)
                 {
                    if(Object.values(item_index).includes(i))
                    {
                       if($(this).is(":checked"))
                       {
                          var selectedVal = '1';
                          service_optional.push(selectedVal);
                       }
                       else
                       {
                         var selectedVal = '0';
                         service_optional.push(selectedVal);
                       }
                    }  
                }); 
             //     alert(service_optional);

                var service_quantity = [];
               $("input.quantity").each(function(i, sel)
               {
                if(Object.values(item_index).includes(i))
                {
                   if ($(this).is(":checked"))
                  {
                    var selectedVal = '1';
                    service_quantity.push(selectedVal);
                  }
                  else
                  {
                    var selectedVal = '0';
                    service_quantity.push(selectedVal);
                  }
                }
              });
      
                  var product_name = [];
                  var product_index = [];

                   $("input.product_name").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    if(selectedVal.trim() != "")
                    {
                       product_index.push(i);
                       product_name.push(selectedVal);
                    }
                  });
            //       alert(product_name);

                  var product_price = [];
                  $("input.product_price").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    if(selectedVal.trim() != "")
                    {
                      product_price.push(selectedVal);
                    }
                  });
            //      alert(product_price);
//
                  var product_qty = [];
                  $("input.product_qty").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    if(selectedVal.trim() != "")
                    {
                      product_qty.push(selectedVal);
                    }
                  });
              //     alert(product_qty);

                  var product_tax = [];
                  $("input.product_tax").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    if(selectedVal.trim() != "" && Object.values(product_index).includes(i))
                    {
                       product_tax.push(selectedVal);
                    }
                  });
             //      alert(product_tax);

                  var product_discount = [];
                   $("input.product_discount").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    if(selectedVal.trim() != "" && Object.values(product_index).includes(i))
                    {
                      product_discount.push(selectedVal);
                    }
                  });
             //      alert(product_discount);

                  var product_description = [];
                   $("textarea.product_description").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    if(selectedVal.trim() != "")
                    {
                      product_description.push(selectedVal);
                    }
                  });
             //      alert(product_description);

                  var product_optional = [];
                   $("input.product_optional").each(function(i, sel)
                   {
                    if(Object.values(product_index).includes(i))
                    {
                        if ($(this).is(":checked"))
                        {
                          var selectedVal = '1';
                          product_optional.push(selectedVal);
                        }
                        else
                        {
                          var selectedVal = '0';
                          product_optional.push(selectedVal);
                        } 
                    }
                  });
              //     alert(product_optional);
                  var product_quantity = [];
                   $("input.product_quantity").each(function(i, sel)
                   {
                      if(Object.values(product_index).includes(i))
                      {
                        if ($(this).is(":checked"))
                        {
                          var selectedVal = '1';
                          product_quantity.push(selectedVal);
                        }
                        else
                        {
                          var selectedVal = '0';
                          product_quantity.push(selectedVal);
                        }
                      }
                  });
             //      alert(product_quantity);

                  var subscription_name = [];
                  var sub_index = [];
                   $("input.subscription_name").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    if(selectedVal.trim() != "")
                    {
                        sub_index.push(i);
                        subscription_name.push(selectedVal);
                    }
                  });
               //    alert(subscription_name);

                  var subscription_price = [];
                   $("input.subscription_price").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    if(selectedVal.trim() != "")
                    {
                      subscription_price.push(selectedVal);
                    }
                  });
              //     alert(subscription_price);

                  var subscription_unit = [];
                   $("select.subscription_unit").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    if(selectedVal.trim() != "" && Object.values(sub_index).includes(i))
                    {
                      subscription_unit.push(selectedVal);
                    }
                  });

                  // alert(subscription_unit);

                  var subscription_tax = [];  
                   $("input.subscription_tax").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    if(selectedVal.trim() != "" && Object.values(sub_index).includes(i))
                    {
                      subscription_tax.push(selectedVal);
                    }
                  });                
             //      alert(subscription_tax);
                  var subscription_discount = [];
                   $("input.subscription_discount").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    if(selectedVal.trim() != "" && Object.values(sub_index).includes(i))
                    {
                      subscription_discount.push(selectedVal);
                    }
                  });
               //    alert(subscription_discount);
                  var subscription_description = [];
                   $("textarea.subscription_description").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    if(selectedVal.trim() != "")
                    {
                      subscription_description.push(selectedVal);
                    }
                  });
              //     alert(subscription_description);
                  var subscription_optional = [];
                   $("input.subscription_optional").each(function(i, sel)
                   {
                      if(Object.values(sub_index).includes(i))
                      {
                        if ($(this).is(":checked"))
                        {
                          var selectedVal = '1';
                          subscription_optional.push(selectedVal);
                        }
                        else
                        {
                          var selectedVal = '0';
                          subscription_optional.push(selectedVal);
                        }
                      }
                  });
                //   alert(subscription_optional);
                  var subscription_quantity = [];
                   $("input.subscription_quantity").each(function(i, sel)
                   {
                      if(Object.values(sub_index).includes(i))
                      {
                        if ($(this).is(":checked"))
                        {
                          var selectedVal = '1';
                          subscription_quantity.push(selectedVal);
                        }
                        else
                        {
                          var selectedVal = '0';
                          subscription_quantity.push(selectedVal);
                        }
                      }
                  });
            var total_total=$(".total_total").text();
            var discount_amount=$('.discount_amount').text();
            var tax_total=$('.tax_total').text();
            var grand_total=$('.grand_total').text();
            var currency_symbol=$("#currency_symbol").val();


            var radioValue = $("input[name='discount']:checked").val();
            if(radioValue){

              var discount_option=radioValue;
                //alert("Your are a - " + radioValue);
            }

            var radioValue1 = $("input[name='tax']:checked").val();
            if(radioValue1){

              var tax_option=radioValue1;
                //alert("Your are a - " + radioValue);
            }

            //   alert(total_total);
            //  alert(grand_total);
            $("#services_total_total").val(total_total);
            $("#services_grand_total").val(grand_total);
            var tax_details=$("#tax_details").val();
            var discount_rate = '';

            if($('.symbol').is(':visible') == true)
            { 
               discount_rate = discount_amount;
               discount_amount = Number(Number(total_total) * Number(discount_amount/100)).toFixed(2);
            }

            //  alert(subscription_quantity);
             var formData={'item_name':item_name,'price':price,'unit':unit,'qty':qty,'tax':tax,'discount':discount,'description':description,'service_optional':service_optional,'service_quantity':service_quantity,'product_name':product_name,'product_price':product_price,'product_qty':product_qty,'product_tax':product_tax,'product_discount':product_discount,'product_description':product_description,'product_optional':product_optional,'product_quantity':product_quantity,'subscription_name':subscription_name,'subscription_price':subscription_price,'subscription_unit':subscription_unit,'subscription_tax':subscription_tax,'subscription_discount':subscription_discount,'subscription_description':subscription_description,'subscription_optional':subscription_optional,'subscription_quantity':subscription_quantity,'total_total':total_total,'discount_amount':discount_amount,'tax_total':tax_total,'grand_total':grand_total,'currency_symbol':currency_symbol,'tax_details':tax_details,'tax_option':tax_option,'discount_option':discount_option,'discount_rate':discount_rate};
           
              getMailTemplate(formData);       
            }

          if(ref_this=='#finallize_tab') {

            $(".Next_Previous").hide();
            $(".Done").show();
            var proposals=$("#proposal_contents_details").html();


            //  $("#proposal_contents_details #images_div").each(function(){
            //   $(this).html('');
            //   var image=$("#images_section").val();
            //   $(this).html('<div class="image_content"><img src='+image+' class="image"></div>');
            // });

              $("#proposal_contents_details  div.popover-lg").each(function () {
              $(this).remove();
              });  

              // $("#proposal_contents_details ").each(function () {
              // $(this).remove();
              // });  

              // $("#proposal_contents_details").each(function () {
              // $(this).data('container').remove();
              // });  

              // $("#proposal_contents_details").each(function () {
              // $(this).data('toggle').remove();
              // });  


              // $("#proposal_contents_details").each(function () {
              // $(this).data('content').remove();
              // });  


              // $("#proposal_contents_details ").each(function () {
              // $(this).data('original-title').remove();
              // });


             $("#proposal_contents_new").html($("#proposal_contents_details").html());


            $('#proposal_contents_details p').each(function() {
            var $this = $(this);
            if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
            $this.remove();
            });
            $('#proposal_contents_details [contenteditable]').removeAttr('contenteditable');

          $("#proposal_contents_details span.cke_reset").each(function () {
          $(this).remove();
          });

          $("#proposal_contents_details div.save-remove").each(function () {
          $(this).remove();
          });  

          $("#proposal_contents_details div.overly").each(function () {
          $(this).remove();
          }); 

          $("#proposal_contents_details button.copy").each(function () {
          $(this).remove();
          });    


          $("#proposal_contents_details  div.popover-lg").each(function () {
          $(this).remove();
          });          


          $("#proposal_contents_details span.cke_widget_wrapper").each(function () {
          $(this).replaceWith($(this).text());
          });

           $("#proposal_contents_details .quantity_class").each(function () {
          $(this).css('display','none');
          });

            $("#proposal_contents_details .quantity_cla").each(function () {
          $(this).css('display','block');
          });

          $("#proposal_contents_details .check_quantity").each(function () {
             $(this).remove();
          });           


            // $("#proposal_contents_details #images_div").each(function(){
            //   $(this).html('');
            //   var image=$("#images_section").val();
            //   $(this).html('<div class="image_content"><img src='+image+' class="image"></div>');
            // });


        //  console.log($("#proposal_contents_details").html());

           $("#proposal_contents").html($("#proposal_contents_details").html());

              var company_name=$("#receiver_company_name").val();
              var new_proposal='new_proposal';
              var proposal_name=$("#proposal_name").val();
              var receiver_mail_id=$("#receiver_mail_id").val();
              var sender_company=$("input[name=sender_company]").val();

              $('.sent_to').html('');
              $('.sent_to').html('<span>'+company_name+'</span><strong>'+receiver_mail_id+'</strong>');

          //    console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
          //    console.log(receiver_mail_id);
             /* $.ajax({
                url: '<?php echo base_url(); ?>/proposals/New_proposal',
                type : 'POST',
                data : {'title':new_proposal,'company_name':company_name,'proposal_name':proposal_name,'receiver_mail_id':receiver_mail_id,'sender_company':sender_company
              },                    
                  beforeSend: function() {
                    $(".LoadingImage").show();
                  },
                  success: function(data) {
              $(".LoadingImage").hide();
                   // alert(data);
              var json = JSON.parse(data);             
              subject=json['subject'];
                 body=json['body'];
              // $("#subject").html('');
              // $("#subject").html(subject);
               $("#body").html('');
              $("#body").html(body);
              }
                });*/
          }  
          }
          else if(empty_flds == 'error' && ref_this == '#price_tab')
          {
              if(($('#price_tab div .service-table .accordion-views .ser').find('.service-newdesign1 .mandatory_er').html() == undefined || $('#price_tab div .service-table .accordion-views .ser').find('.service-newdesign1 .mandatory_er').html() == ""))
              { 
                $('#price_tab div .service-table .accordion-views .ser').find('.head-services').before('<span class="mandatory_er pull-left" style="padding-left: 15px;color:red;">Mandatory Section.</span>'); 
              }              
          }                      
         });
         $('.prev-step').click(function(){

           var ref_this = $(".addnewclient_pages1 ol.nav-tabs li a.active").attr("href"); 
//console.log(ref_this);
         
           if(ref_this='#finallize_tab'){
         //   console.log('3534534534');
               $(".Next_Previous").show();
               $(".Done").hide();
           }else{
             $(".Next_Previous").hide();
           $(".Done").show();
           }


             $('.addnewclient_pages1  .nav-tabs  .active').closest('li').prev('li').find('a').trigger('click');
         });
   });

 $('#login_form').submit(function() {
    $('.LoadingImage').show();
});

</script>

<script type="text/javascript">
$(document).ready(function () {
  $(".hide_section").hide();
   $(".hide_fields_form").hide();


   $(".hide_field").click(function(){
      $(".hide_section").hide();
       $(".show_more").show();
      $(".hide_fields_form").hide();
   });
   $(".show_field").click(function(){

      $(".show_more").hide();
       $(".hide_section").show();
      $(".hide_fields_form").show();
   });
});
$("#company_added").click(function(){
   var company_name=$("#popup_company_name").val();
   var phone=$("#phone").val();
   var fax=$("#fax").val();
   var country=$("#country").val();
   var state=$("#state").val();
   var postal_code=$("#postal_code").val();
   var address=$("#address").val();
   var website=$("#website").val();
   var first_name=$("#first_name").val();
   var last_name=$("#last_name").val();
   var email=$("#email").val();
 //  console.log(email);
//   console.log(first_name);
 //  console.log(company_name);
    if (validateEmail(email)) {     
       var formData={'company_name':company_name,'phone':phone,'fax':fax,'country':country,'state':state,'postal_code':postal_code,'address':address,'website':website,'first_name':first_name,'last_name':last_name,'email':email}; 
      if(company_name!='' && first_name!='' && email!=''){      
          $.ajax({
              url: '<?php echo base_url();?>proposal_page/insert_Company/',
              type : 'POST',
              data : formData,                    
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data) {
                $(".LoadingImage").hide();
                var json = JSON.parse(data);             
                category_add=json['category_add'];
                $(".company_add").html('');
                $(".company_add").append(category_add);
              //  console.log($("#company_name").val());

                $("#company_name").trigger('click');
                  $('.dropdown-sin-4').dropdown({
                  limitCount: 5,
                  input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
                  });
                setTimeout(function() {        
                $("#send-to").modal('toggle');
                }, 2000); 


            $(".dropdown-sin-4").click(function(){ 

            $(".dropdown-display").removeAttr( 'style' ); 
            //alert('ok');
            if(($(".dropdown-sin-4.dropdown-single.active").length)=='0'){ 

            var crm_company_name=$("#company_name").val();
            ///alert(crm_company_name);
            $.ajax({
            url: '<?php echo base_url();?>proposals/company_details/',
            type : 'POST',
            data : {'crm_company_name':crm_company_name},                    
            beforeSend: function() {
            $(".LoadingImage").show();
            },
            success: function(data) { 
            $(".LoadingImage").hide();             
            var json = JSON.parse(data);             
            content=json['content'];

            console.log(content);
            image=json['image'];
            $('.client_details').html('');
            $('.client_details').html(content);
            //$('.client_details2').html('');
           // $('.client_details2').append(content);
            //   $(".linking").hide();
            $('.profile_pics').html('');
            $('.profile_pics').html(image);
            }
            });
            }
            });



              }   
          });
   
      }else{
        $(".errors").show();
        setTimeout(function() {        
          $(".errors").hide();
        }, 5000);   
      }
   }else{
     $("#email").css('border', '1px solid red');       
   }
});


function validateEmail(email) {
 var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
if (filter.test(email)) {
return true;
}
else {
return false;
}
}
$("#search_service_name").keyup(function(){
var service_name=$("#search_service_name").val();
if(service_name!=''){
  $.ajax({
      url: '<?php echo base_url();?>proposal_page/search_Service/',
      type : 'POST',
      data : {'service_name':service_name},                    
      beforeSend: function() {
         $(".LoadingImage").show();
      },
      success: function(data) {
         $(".LoadingImage").hide();
        $(".lorem-content1").removeClass("in active");
        var json = JSON.parse(data); 
        var contents=json['contents']; 
        $('.service-lists').show();
        $('.service-lists').html('');
        $('.service-lists').html(contents);
         $('.tab-used').removeClass('active');
        $('.tab-used').first().addClass('active');
        $('#all').addClass('in active');
       $('#all').hide();
        }   
  });
}
});
$("#search_product_name").keyup(function(){
var service_name=$("#search_product_name").val();
if(service_name!=''){
  $.ajax({
      url: '<?php echo base_url();?>proposal_page/search_Product/',
      type : 'POST',
      data : {'service_name':service_name},                    
      beforeSend: function() {
         $(".LoadingImage").show();
      },
      success: function(data) {
        $(".LoadingImage").hide();
        var json = JSON.parse(data); 
        var contents=json['contents']; 
        $('.product_lists').show();
        $('.product_lists').html('');
        $('.product_lists').html(contents);
        $('.tab-used1').removeClass('active');
        $('.tab-used1').first().addClass('active');
        $('#product_all').addClass('in active');
       $('#product_all').hide();
      }   
  });
}
});
function myFunction(id) 
{
  $.ajax({
      url: '<?php echo base_url();?>proposal_page/search_ServiceBYid/',
      type : 'POST',
      data : {'id':id},                    
      beforeSend: function() {
         $(".LoadingImage").show();
      },
      success: function(data) {      
        $(".LoadingImage").hide(); 
     //   $("#select-category").removeClass('show');     
        $("#select-category").modal('toggle');
         var json = JSON.parse(data); 

          $('.add-service').find(':input,:radio,:checkbox').addClass('required');
          $('.service_category_name').removeClass('required');
          $('#service_category_name').removeClass('required');
          $(".original_price").removeClass('required');


          var contents=json['contents']; 
          var length=$(".add-service").length;    
          var lengths=$(".add-services").length;    
          var sc=length + lengths; 
          //  console.log(sc);
          var count=sc + 1; 
          var i=1;

         if(length=='1' && $('#item_name').val()=='' && $('.contents .add-service:nth-child('+length+')').is(':hidden') == true )
         {
            if($('.add-service').attr('class')=='add-service input-fields hide_fields'){
            $(".add-service").hide(); 
            }else{
            $(".add-service").show();
            } 
            // $('.input-fields').show();
            // $('.remove-content').hide();

            $('#item_name').val(json['item_name']);
            $('#price').val(json['item_price']);
            $('#original_price').val(json['item_price']);
            $('#qty').val(json['item_qty']);
            $('#unit').val(json['item_unit']);
            $('#service_category_name').val(json['service_category']);
            $('textarea#description').html(json['item_description']);
            sub_total();
         }
         else
         {
              // $('.input-fields').show();
             //   $('.remove-content').hide();
            var content='   <div class="top-field-heads "><div class="proposal-fields1 service-items"> <input name="item_name[]" id="item_name" class="required item_name" value="" readonly>  <input type="hidden" name="service_category_name[]" id="service_category_name" class="service_category_name" value="testing"></div><div class="proposal-fields2 price-services service-items"><input type="hidden" placeholder="" name="original_price[]" id="original_price" class="original_price" onchange="sub_total()" value="125"><input type="text" placeholder="" name="price[]" id="price" class="decimal required service_price  serviceprice price" maxLength="6" onchange="sub_total()" value=""><span>/</span><select name="unit[]" id="unit" class="required unit"><option value="per_service">Per Service</option><option value="per_day">Per Day</option> <option value="per_month">Per Month</option><option value="per_2month">Per 2Months</option><option value="per_6month">Per 6Month</option> <option value="per_year">Per Year</option></select></div>  <div class="proposal-fields3 service-items"><input onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999" type="text" name="qty[]" id="qty"  class="decimal decimal_qty required qty" value="" onChange ="qty_check()"></div> <div class="proposal-fields5 service-items tax-field">                                 <input type="text" name="service_tax[]" id="service_tax" class="decimal tax" maxLength="4" onchange="sub_total()" value="0"> </div>   <div class="proposal-fields6 service-items discount-field">  <input type="text" name="service_discount[]" id="service_discount" class="decimal discount" maxLength="4" onchange="sub_total()" value="0"></div>  </div> <div class="bottom-field-heads"> <textarea rows="3" name="description[]" id="description" class="required description"></textarea><div class="mark-optional"> <div class="mark-left-optional pull-left border-checkbox-section"><div class="checkbox-color checkbox-primary">  <input class="border-checkbox optional" type="checkbox" id="checkbox1" name="service_optional[]" value="'+ count +'" ><label class="border-checkbox-label optional-label" for="checkbox1">mark as optional</label>  </div><div class="checkbox-color checkbox-primary">  <input class="border-checkbox quantity" type="checkbox" id="checkbox2" name="service_quantity[]" value="'+ count +'"> <label class="border-checkbox-label quantity-label" for="checkbox2">quantity  editables</label> </div> </div><div class="pull-right mark-right-optional"> <a  class="sve-action1 edit_before_save" id="service_save" onclick="saved(this)">save</a>  <a  class="sve-action1 edit_after_save" id="service_save" onclick="saved_before(this)" style="display:none">save</a> <a class="sve-action2" id="save_to_catalog" onclick="save_to_catalog(this)">save to package</a> <div class="delete_service_div buttons_hide"> <a class="sve-action2 service_close" id="service_close" onclick="deleted(this)"> Delete</a>  </div> <div class="cancel_service_div buttons_hide" style="display: none">  <a class="sve-action2 service_cancel" id="service_close" onclick="cancelled(this)"> Cancel</a> </div></div> </div> </div>';


            
              $('.extra-add-service').append('<div class="add-services input-fields">'+content+'</div>');



                  if ($('.service-table').hasClass('discount')) {
                $(".proposal-fields6").show();
                }

                if ($('.service-table').hasClass('tax')) {
                $(".proposal-fields5").show();
                }



                $('.extra-add-service input').removeAttr( "style" );
                 $('.extra-add-service textarea').removeAttr( "style" );
               var minNumber = 10; // le minimum
              var maxNumber = 100; // le maximum
              var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
            //  console.log(randomnumber);
              var newran=Math.floor(randomnumber -1 );
             // console.log(newran);
              $(".add-services .quantity").last().attr('id','checkbox'+randomnumber);
              $(".add-services .quantity-label").last().attr('for','checkbox'+randomnumber);
              $(".add-services .optional").last().attr('id','checkbox'+newran);
              $(".add-services .optional-label").last().attr('for','checkbox'+newran);
              $( ".add-services .item_name" ).last().val(json['item_name']);    
              $( ".add-services .price" ).last().val(json['item_price']);  
              $( ".add-services .original_price" ).last().val(json['item_price']);    
              $( ".add-services .qty" ).last().val(json['item_qty']);    
              $( ".add-services .unit" ).last().val(json['item_unit']);    
              $( ".add-services .description" ).last().html(json['item_description']); 
              $('.add-services .service_category_name').val(json['service_category']);
              sub_total();             
         }
        i++;
      }   
  });
}
function sub_total()
{   
  var sum=0;

  $('.serviceprice').each(function() 
  {       
    var qty=$(this).parents('.top-field-heads ').find('.service-items').find('.qty').val();

    var test = $("input[name='tax']:checked").val();
    //  console.log('****####$$$%%%^^&&***');
    //    console.log(test);
    if(test=='line_tax')
    {
      var tax=$(this).parents('.top-field-heads ').find('.tax-field').find('.tax').val();
    }
    else
    {
      var tax=0;
    }

    var test1 = $("input[name='discount']:checked").val();

    if(test1=='line_discount')
    {
        var discount=$(this).parents('.top-field-heads ').find('.discount-field').find('.discount').val();  
    }
    else
    {
        var discount=0; 
    }


    var price=$(this).val(); 
    var tax_amount=(price * qty).toFixed(2);
    var dec = (tax/100).toFixed(2); 

    var discount = (discount/100).toFixed(2);

    var mult = (tax_amount * dec).toFixed(2); 
    //  console.log(mult);

    var dis_count = (tax_amount* discount).toFixed(2);
    //  console.log(dis_count); 

    var price_cal=Number($(this).val()) * Number(qty);

    //  console.log("pricecalculation");
    //  console.log(price_cal);

    var calculation=(Number(price_cal) + Number(mult)).toFixed(2); 
    // console.log('Calculation');
    //  console.log(calculation);
    sum += (Number(calculation) - Number(dis_count));     // console.log(sum);

  });  

  $('.sub_total').html(''); 
  //$('.sub_total').html(Math.round(sum));
  $('.sub_total').html(sum.toFixed(2));
  product_sub_total();
  subscription_sub_total();
}
function myFunction2(id) 
{
      $.ajax({
      url: '<?php echo base_url();?>proposal_page/search_ProductBYid/',
      type : 'POST',
      data : {'id':id},                    
      beforeSend: function() {
         $(".LoadingImage").show();
      },
      success: function(data) 
      {      
        $(".LoadingImage").hide();      
        $("#select-products").modal('toggle');
        var json = JSON.parse(data); 
       
             
        $('.add-product').find(':input,:radio,:checkbox').addClass('required');
          $('.product_category_name').removeClass('required');
          $(".original_price").removeClass('required');
        $('#product_category_name').removeClass('required');
       if($("#line_discount").not(':checked')){
           $('.subscription_discount').removeClass('required');
           $('.product_discount').removeClass('required');           
         }
         if($("#line_tax").not(':checked')){        
           $('.subscription_tax').removeClass('required');
           $('.product_tax').removeClass('required');
         }

        var length=$(".add-product").length;
        var lengths=$(".add-products").length;
        var sc=length + lengths;
        // console.log(sc);
        var count=sc + 1; 
        var i=1;

        if(length=='1' && $('#product_name').val()=='' && $('.product-contents .add-product:nth-child('+length+')').is(':hidden') == true)
        {
          if($('.add-product').attr('class')=='add-product input-fields hide_fields'){
          $(".add-product").hide(); 
          }else{
          $(".add-product").show();
          } 
         
          $('#product_name').val(json['item_name']);
          $('#product_qty').val('1');
          $('#product_price').val(json['item_price']);       
          $('#original_product_price').val(json['item_price']);           
          $('textarea#product_description').html(json['item_description']);
          $('#product_category_name').val(json['product_category']);
          product_sub_total();
        }
        else
        {
          var content='<div class="top-field-heads"><div class="proposal-fields1 service-items">  <input type="text" name="product_name[]" id="product_name" class="product_name required" value=""><input type="hidden" name="product_category_name[]" id="product_category_name" class="product_category_name" value=""> </div><div class="proposal-fields2 price-services service-items"> <input type="hidden" placeholder="" name="original_product_price[]" id="original_product_price" class="original_product_price original_price " onchange="sub_total()" value="12"><input type="text" placeholder="" maxLength="6" name="product_price[]" id="product_price" class="decimal product_price price required" onchange="sub_total()" value=""> </div><div class="proposal-fields3 service-items"> <input onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"  type="text" name="product_qty[]" id="product_qty" class="decimal_qty  decimal product_qty required" value="1" onChange ="qty_check()"></div> <div class="proposal-fields5 service-items tax-field"><input type="text" name="product_tax[]" id="product_tax" class="decimal product_tax" maxLength="4" onchange="sub_total()" value="0"></div><div class="proposal-fields6 service-items discount-field">  <input type="text" name="product_discount[]" maxLength="4" id="product_discount" class="decimal product_discount" onchange="sub_total()" value="0"></div></div><div class="bottom-field-heads"><textarea rows="3" name="product_description[]" id="product_description" class="product_description required"></textarea><div class="mark-optional"><div class="mark-left-optional pull-left border-checkbox-section"><div class="checkbox-color checkbox-primary"><input class="border-checkbox product_optional required" type="checkbox" id="checkbox3" name="product_optional[]" value="'+ count +'"> <label class="border-checkbox-label optional-label" for="checkbox3">mark as optional</label> </div><div class="checkbox-color checkbox-primary"> <input class="border-checkbox product_quantity required" type="checkbox" id="checkbox4" name="product_quantity[]" value="'+ count +'"><label class="border-checkbox-label quantity-label" for="checkbox4">quantity  editables</label> </div> </div><div class="pull-right mark-right-optional">   <a  class="sve-action1 edit_beforeproduct_save" id="product_save" onclick="saved_product(this)">save</a><a  class="sve-action1 edit_afterproduct_save" id="product_save" onclick="savedbefore_product(this)" style="display:none;">save</a>  <a class="sve-action2" id="product_save_to_catalog" onclick="pro_save_to_catalog(this)">save to package</a> <div class="delete_product_div buttons_hide"> <a class="sve-action2 hgfghfgh product_close" id="product_close" onclick="deleted_product(this)">Delete</a></div><div class="cancel_product_div buttons_hide" style="display:none"> <a class="sve-action2 hgfghfgh product_cancel" id="product_close" onclick="cancelled_product(this)">Cancel</a></div> </div></div> </div>';
          
          $('.extra-add-products').append('<div class="add-products input-fields">'+content+'</div>');

              if ($('.service-table').hasClass('discount')) {
            $(".proposal-fields6").show();
            }

            if ($('.service-table').hasClass('tax')) {
            $(".proposal-fields5").show();
            }
          $('.extra-add-products input').removeAttr( "style" );
          $('.extra-add-products textarea').removeAttr( "style" );
             var minNumber = 500; // le minimum
          var maxNumber = 700; // le maximum
          var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
        //  console.log(randomnumber);
          var newran=Math.floor(randomnumber -1 );
         // console.log(newran);
          $(".add-products .product_quantity").last().attr('id','checkbox'+randomnumber);
          $(".add-products .quantity-label").last().attr('for','checkbox'+randomnumber);
          $(".add-products .product_optional").last().attr('id','checkbox'+newran);
          $(".add-products .optional-label").last().attr('for','checkbox'+newran);
          $( ".add-products .product_name" ).last().val(json['item_name']);    
          $( ".add-products .product_price" ).last().val(json['item_price']); 
          $( ".add-products .original_product_price" ).last().val(json['item_price']); 
          $( ".add-products .product_description" ).last().html(json['item_description']);
          $('.add-products .product_category_name').val(json['product_category']); 
          product_sub_total();
        }
        i++;
      }   
  });
}

function product_sub_total()
{
  var item_name=$("#item_name").val();
  var product_name=$("#product_name").val();
  if(item_name!='')
  {
   var sum=Number($('.sub_total').text());
    // console.log(sum);
  }
  else if(product_name!='')
  {
    var sum=Number($('.product_sub_total').text());
  }
  else
  {
    var sum=0;
  //  console.log(sum);
  }
  var sum1=0;
 // alert(sum1);
 $('.product_price').each(function() { 
    

        var qty_2=$(this).parents('.top-field-heads ').find('.service-items').find('.product_qty').val();


    var test = $("input[name$='tax']:checked").val();
        if(test=='line_tax'){
        var product_tax=$(this).parents('.top-field-heads ').find('.tax-field').find('.product_tax').val();
     //   console.log(product_tax);
      }else{
         var product_tax=0;
      //   console.log(product_tax);
      }

         var test1 = $("input[name$='discount']:checked").val();
        if(test1=='line_discount'){

        var product_discount=$(this).parents('.top-field-heads ').find('.discount-field').find('.product_discount').val(); 
     //    console.log(product_discount);
       }else{
        var product_discount=0; 
       //  console.log(product_discount);
       }

//console.log('*******************'+product_tax+'********************');
//console.log('*******************'+product_discount+'********************');1
       // alert(qty_2);
      
       
       








       // alert(product_tax);
        //alert(product_discount);
        var price=$(this).val();
      //  alert(price);
        var dec = (product_tax/100).toFixed(2);        
        var dis_count = (product_discount/100).toFixed(2);
       // alert(dec);
       //  alert(discount);
       var tax_amount=(price * qty_2).toFixed(2)

          var mult = (tax_amount* dec).toFixed(2); 
          var dis_count1 = (tax_amount* dis_count).toFixed(2); 


          var price_cal1=(Number($(this).val()) * Number(qty_2)).toFixed(2);
        //  console.log(price_cal1);

          var calculation=(Number(price_cal1) + Number(mult)).toFixed(2);

      //    console.log(calculation);
        //   alert(mult);
         // alert(dis_count);
        //  var calcPrice=price+mult;

          sum += Number(calculation) - Number(dis_count1);
         // sum += Number($(this).val());
          sum1 += Number(calculation) - Number(dis_count1);
        });

 $('.product_sub_total').html('');
 //$('.product_sub_total').html(Math.round(sum1));
 $('.product_sub_total').html(sum1.toFixed(2));
 subscription_sub_total();

 return true;
}


$(document).ready(function() {


  $(document).on('click','.linking',function()
  { 
      $('#send-to').find('input[type="text"],select').each(function()
      {
          $(this).val('');
      });
  });



    $("input[name='tax']").click(function() {
        var test = $(this).val();
        if(test=='line_tax'){
       //   $("#tax_old").val($("#tax_details").val());
          $("#tax_details").val('');
           $(".remove-content").hide();
          $(".input-fields").show();
          $(".proposal-fields5").show();


//          $(".edit_after_save").css('display','inline-block');
//           $(".edit_before_save").css('display','none');


// $(".edit_afterproduct_save").css('display','inline-block');
// $(".edit_beforeproduct_save").css('display','none');

// $(".edit_aftersubscription_save").css('display','inline-block');

// $(".edit_beforesubscription_save").css('display','none');



        $(".service-table").addClass('tax');
         $('.subscription_tax').addClass('required');
         $('.product_tax').addClass('required');
        $(".tax-field").show();
        $(".ta_x").hide();
        $(".proposal-fields5").css('display','block');
        sub_total();
      }else{

      //  console.log("*************else section*****************");
     //    console.log($("#tax_old").val());

        if($("#tax_old").val()!='0'){
          // $("#tax_old").val($("#tax_details").val());
       //   console.log($("#tax_old").val());
          // $("#tax_old").val($("#tax_details").val());
          $("#tax_details").val($("#tax_value").val());
          $(".tax_rate").html($("#tax_old").val());
       //   console.log( $(".tax_rate").html());
        $(".tax").val('0');
        $(".product_tax").val('0');
        $(".subscription_tax").val('0');
        $(".service-table").removeClass('tax');
         $('.subscription_tax').removeClass('required');
         $('.product_tax').removeClass('required');
        $(".tax-field").hide();
        $(".ta_x").show();
         $(".proposal-fields5").css('display','none');
        }

       
         sub_total();
      }
    });

     $("input[name$='discount']").click(function() {
        var test = $(this).val();
       // alert(test);
        if(test=='line_discount'){

         $(".discount_amount").val('0');

           $(".remove-content").hide();
          $(".input-fields").show();
          $(".proposal-fields6").show();
          $(".service-table").addClass('discount');
          $('.subscription_discount').addClass('required');
           $('.product_discount').addClass('required');
          $(".discount-field").show();
          $(".dis_count").hide();
          $(".proposal-fields6").css('display','block');
          sub_total();
        }else{
          $(".discount").val('0');
           $(".product_discount").val('0');
            $(".subscription_discount").val('0');
          $(".service-table").removeClass('discount');
          $('.subscription_discount').removeClass('required');
           $('.product_discount').removeClass('required');
          $(".discount-field").hide();
          $(".dis_count").show();
           $(".proposal-fields6").css('display','none');
           sub_total();
        }
    });
});






function check_function(){



 // $("input[name$='tax']").click(function() {
        var test = $("input[name='tax']:checked").val();
      //  console.log("test-test-test-test-test-test-test-test");
      //  console.log(test);
        if(test=='line_tax'){

       //   console.log('WWWWWWWWWWWWWWWWWWWWWWWWW');
//console.log(test);
           $(".proposal-fields5").removeAttr('style');
      //    $("#tax_old").val($("#tax_details").val());
          $("#tax_details").val('');
 //console.log($("#tax_details").val());
 // console.log($(".tax_rate").html());
            $(".tax_rate").html('0');
// console.log($(".tax_rate").html());
           $(".remove-content").hide();
        //  $(".input-fields").show();
          $(".proposal-fields5").show();
        $(".service-table").addClass('tax');
         $('.subscription_tax').addClass('required');
         $('.product_tax').addClass('required');
        $(".tax-field").show();
        $(".ta_x").hide();
       
        sub_total();
      }else{

      //  console.log("*************else section*****************");
       // console.log($("#tax_old").val());
        if($("#tax_old").val()!='0'){
        //   console.log($("#tax_old").val());
          // $("#tax_old").val($("#tax_details").val());
          $("#tax_details").val($("#tax_value").val());
          $(".tax_rate").html($("#tax_old").val());
       //   console.log( $(".tax_rate").html());

           $(".tax").val('0');
        $(".product_tax").val('0');
        $(".subscription_tax").val('0');



        $(".service-table").removeClass('tax');
         $('.subscription_tax').removeClass('required');
         $('.product_tax').removeClass('required');
        $(".tax-field").hide();
        $(".ta_x").show();
         $(".proposal-fields5").css('display','none');

        }

       
         sub_total();
      }
   // });

     //$("input[name$='discount']").click(function() {
        var test1 = $("input[name='discount']:checked").val();
      //   console.log("test1-test-test-test-test-test-test-test1");
       //  console.log(test1);
       // alert(test);
        if(test1=='line_discount'){
     //   console.log('WWWWWWWWWWWWWWWWWWWWWWWWW');
       //   console.log(test1);
         $(".discount_amount").val('0');

           $(".remove-content").hide();
         // $(".input-fields").show();
          $(".proposal-fields6").show();
          $(".service-table").addClass('discount');
          $('.subscription_discount').addClass('required');
           $('.product_discount').addClass('required');
          $(".discount-field").show();
          $(".dis_count").hide();
          $(".proposal-fields6").css('display','block');
          sub_total();
        }else{
        //  console.log('sample');
          $(".discount").val(0);
       //   console.log($(".discount").val())
          $('.discount').each(function() {  
            $(this).val(0);
          });


           $(".product_discount").val(0);
            $(".subscription_discount").val('0');
          $(".service-table").removeClass('discount');
          $('.subscription_discount').removeClass('required');
           $('.product_discount').removeClass('required');
          $(".discount-field").hide();
          $(".dis_count").show();
           $(".proposal-fields6").css('display','none');
           sub_total();
        }
   // });



}


function myFunction3(id) 
{
//  alert('id');
  $.ajax({
      url: '<?php echo base_url();?>proposal_page/search_subscriptionBYid/',
      type : 'POST',
      data : {'id':id},                    
      beforeSend: function() {
         $(".LoadingImage").show();
      },
      success: function(data) 
      {    
    //  alert(data);  
         $(".LoadingImage").hide();      
         $("#select-subscriptions").modal('hide');
         var json = JSON.parse(data); 
             
         $('.add-subscription').find(':input,:radio,:checkbox').addClass('required');
         $("#subscription_category_name").removeClass('required');
         $(".original_price").removeClass('required');
          if($("#line_discount").not(':checked')){
           $('.subscription_discount').removeClass('required');
           $('.product_discount').removeClass('required');
         }

         if($("#line_tax").not(':checked')){        
           $('.subscription_tax').removeClass('required');
           $('.product_tax').removeClass('required');
         }

        var length=$(".add-subscription").length;
        var lengths=$(".add-subscriptions").length;
        var sc=length + lengths;
        //  console.log(sc);
        var count=sc + 1; 
        var i=1;

        if(length == '1' && $('#subscription_name').val() == '' && $('.subscription-contents .add-subscription:nth-child('+length+')').is(':hidden') == true)
        {

        if($('.add-subscription').attr('class')=='add-subscription input-fields hide_fields'){
        $(".add-subscription").hide();
        }else{
        $(".add-subscription").show();
        }

          $('#subscription_name').val(json['item_name']);
           $('#original_subscription_price').val(json['item_price']); 
          $('#subscription_price').val(json['item_price']); 
           $('#subscription_unit').val(json['item_unit']);           
          $('textarea#subscription_description').html(json['item_description']);
           $('#subscription_category_name').val(json['subscription_category']);
          subscription_sub_total();
         }else{
      //    console.log('this section work');
           var content=' <div class="top-field-heads"> <div class="proposal-fields1 service-items"> <input type="text" name="subscription_name[]" id="subscription_name" class="subscription_name required" value=""> <input type="hidden" name="subscription_category_name[]" id="subscription_category_name" class="subscription_category_name " value="sampling-test">  </div> <div class="proposal-fields2 price-services service-items"> <input type="hidden" placeholder="" name="original_subscription_price[]" id="original_subscription_price" class="original_subscription_price price original_price" onchange="sub_total()" value="12"> <input type="text" placeholder="" name="subscription_price[]" maxLength="6" id="subscription_price" class="decimal subscription_price price required" onchange="sub_total()" value=""> </div> <div class="proposal-fields3 service-items">  <select name="subscription_unit[]" id="subscription_unit" class="subscription_unit required"> <option value="per_hour">Per Hour</option> <option value="per_day">Per Day</option> <option value="per_week">Per Week</option>  <option value="per_month">Per Month</option> <option value="per_2month">Per 2Month</option> <option value="per_5month">Per 5month</option><option value="per_year">Per Year</option></select> </div><div class="proposal-fields5 service-items tax-field"> <input type="text" name="subscription_tax[]" id="subscription_tax" class="decimal subscription_tax" onchange="sub_total()" value="0"></div> <div class="proposal-fields6 service-items discount-field"><input type="text" name="subscription_discount[]" id="subscription_discount" class="decimal subscription_discount" onchange="sub_total()" value="0"></div>  </div><div class="bottom-field-heads"> <textarea rows="3" name="subscription_description[]" id="subscription_description" class="subscription_description required">ghjghjghj</textarea> <div class="mark-optional"> <div class="mark-left-optional pull-left border-checkbox-section"> <div class="checkbox-color checkbox-primary"> <input class="border-checkbox subscription_optional  required" type="checkbox" id="checkbox5" name="subscription_optional[]" value="'+ count +'">  <label class="border-checkbox-label optional-label" for="checkbox5">mark as optional</label></div> <div class="checkbox-color checkbox-primary">     <input class="border-checkbox subscription_quantity required" type="checkbox" id="checkbox6" name="subscription_quantity[]" value="'+ count +'"><label class="border-checkbox-label quantity-label" for="checkbox6">quantity  editables</label> </div>  </div> <div class="pull-right mark-right-optional">      <a  class="sve-action1 edit_beforesubscription_save" id="subscription_save" onclick="saved_subscription(this)">save</a><a  class="sve-action1 edit_aftersubscription_save" id="subscription_save" onclick="savedbefore_subscription(this)" style="display: none;">save</a><a class="sve-action2" id="subscription_save_to_catalog" onclick="sub_save_to_catalog(this)">save to package</a> <div class="delete_subscription_div buttons_hide">  <a class="sve-action2 subscription_close" id="subscription_close" onclick="delete_subscription(this)">Delete</a></div>     <div class="cancel_subscription_div cancel-comdiv buttons_hide" style="display:none"> <a class="sve-action2 subscription_cancel" id="subscription_close" onclick="cancelled_subscription(this)">Cancel</a></div>    </div>  </div></div>';
         
           $('.extra-add-subscription').append('<div class="add-subscriptions input-fields">'+content+'</div>');


               if ($('.service-table').hasClass('discount')) {
            $(".proposal-fields6").show();
            }

            if ($('.service-table').hasClass('tax')) {
            $(".proposal-fields5").show();
            }
          $('.extra-add-subscription input').removeAttr( "style" );
          $('.extra-add-subscription textarea').removeAttr( "style" );
           var minNumber = 10001; // le minimum
           var maxNumber = 20000; // le maximum
           var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
        //   console.log(randomnumber);
           var newran=Math.floor(randomnumber -1 );
        //   console.log(newran);\
        $(".add-subscriptions .subscription_quantity").last().attr('id');
        $(".add-subscriptions .subscription_quantity").last().attr('id','checkbox'+randomnumber);
        $(".add-subscriptions .quantity-label").last().attr('for','checkbox'+randomnumber);


        $(".add-subscriptions .subscription_optional").last().attr('id','checkbox'+newran);
        $(".add-subscriptions .optional-label").last().attr('for','checkbox'+newran);
        $( ".add-subscriptions .subscription_name" ).last().val(json['item_name']);    
        $( ".add-subscriptions .subscription_price" ).last().val(json['item_price']); 
        $( ".add-subscriptions .original_subscription_price" ).last().val(json['item_price']); 
        $( ".add-subscriptions .subscription_unit" ).last().val(json['item_unit']); 
        $( ".add-subscriptions .subscription_description" ).last().html(json['item_description']); 
         $('.add-subscriptions .subscription_category_name').val(json['subscription_category']);
           subscription_sub_total();
         }
         i++;
      }   
  });
}

function subscription_sub_total()
{ 
   var product_name = [];
   $("input.product_name").each(function(i, sel){
    var selectedVal = $(sel).val();
    if(selectedVal.trim() != "")
    {
       product_name.push(selectedVal);
    }
  });

   var service_name = [];
   $("input.item_name").each(function(i, sel){
    var selectedVal = $(sel).val();
    if(selectedVal.trim() != "")
    {
       service_name.push(selectedVal);
    }
  });
  
  if(product_name!='' && service_name!='')
  {
    var product_total=Number($('.product_sub_total').text());  
    var service_total=Number($('.sub_total').text());  
    var sum=Number(product_total)+Number(service_total);
    //console.log(sum);
  }
  else if(product_name!='')
  {
    var sum=Number($('.product_sub_total').text());
  }
  else if(service_name!='')
  {
    var sum=Number($('.sub_total').text());    // console.log(sum);
  }
  else
  {
    var sum=0;  
  }
  var sum1=0;
//console.log(sum1);
//console.log('subscription_cal');
$('.subscription_price').each(function() 
{
    var test = $("input[name$='tax']:checked").val();

     if(test=='line_tax')
     {
      var tax=$(this).parents('.top-field-heads ').find('.tax-field').find('.subscription_tax').val();

    //   console.log(tax);
     }
     else
     {
        var tax=0;
       // console.log(tax);
     }

    var test1 = $("input[name$='discount']:checked").val();
    if(test1=='line_discount')
    {
       var discount=$(this).parents('.top-field-heads ').find('.discount-field').find('.subscription_discount').val(); 
         //   console.log(discount);
    }
    else
    {
       var discount=0; 
        //    console.log(discount);
    }

   //console.log('*******************'+tax+'********************');
   //console.log('*******************'+discount+'********************');

    var price=$(this).val();
    var dec = (tax/100).toFixed(2);        
    var discount = (discount/100).toFixed(2);
    // alert(dec);
    //  alert(discount);

    var mult = (price* dec).toFixed(2); 
    var dis_count = (price* discount).toFixed(2); 
    //  alert(mult);
    //  alert(dis_count);
    //  var calcPrice=price+mult;
    var price_cal=(Number($(this).val()) + Number(mult)).toFixed(2);

    sum += Number(price_cal) - Number(dis_count);     
    //sum += Number($(this).val());
    sum1 += Number(price_cal) - Number(dis_count)
         //  console.log("sum1");

});
   
   //console.log('111111111111111');
   $('.subscription_sub_total').html('');
   //$('.subscription_sub_total').html(Math.round(sum1));
   $('.subscription_sub_total').html(sum1.toFixed(2));

   $(".total_total").html('');
   //$(".total_total").html(Math.round(sum));
   $(".total_total").html(sum.toFixed(2));

     if (($("#line_discount").is(":checked")) &&  ($("#line_tax").is(":checked"))) 
     {  
       $('.grand_total').html( '' );
       $('.grand_total').html(sum.toLocaleString());
     }
     else 
     {
        var taxdetails = '<?php echo json_encode($tax_vals); ?>';
        var tax = $('#tax_details').val();
        taxdetails = JSON.parse(taxdetails);

        if ($("#line_discount").is(":checked"))
        {
        //  console.log('discount checked');
            var calcPrice = 0;

            if(tax > '0')
            {
               var calcPrice  = Number(sum*(taxdetails[tax]/100)).toFixed(2);
               $('.tax_total').html( '' );
               $('.tax_total').html( calcPrice );
            }
            
            var grandPrice  = Number(sum+Number(calcPrice)).toFixed(2);
            $('.grand_total').html( '' );
            $('.grand_total').html( grandPrice.toLocaleString() );
         }
         else
         {  
            var calcPrice = 0;

            if(tax > '0')
            {
               var calcPrice  = Number(sum*(taxdetails[tax]/100)).toFixed(2);
               $('.tax_total').html( '' );
               $('.tax_total').html( calcPrice );
            }
         
            var grandPrice  = Number(sum+Number(calcPrice)).toFixed(2);
         /*   x=x.toString();
              var lastThree = x.substring(x.length-3);
              var otherNumbers = x.substring(0,x.length-3);
              if(otherNumbers != '')
              lastThree = ',' + lastThree;
              var grandPrice = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;*/


            $('.grand_total').html( '' );
            $('.grand_total').html(grandPrice.toLocaleString());
          //  console.log(grandPrice);
            dis(grandPrice);
            
         
            function dis(sum)
            { 
              if($("#line_tax").is(":checked"))
              {
            //
               //  console.log(sum);
             // console.log('tax checked');
               if (localStorage.getItem('discount_amount') == 'on')
               {
            //alert('total');
                 var mult=Number($(".discount_amount").html());
               //  console.log('((((((((((((((((((((');
              //   console.log('sum');
              //   console.log(sum);
              //   console.log(mult);
              //  var dec = (discount_amount/100).toFixed(2);
             //   var mult = sum*dec; 
                var grandPrice=Number(sum) - Number(mult);
                  // x=x.toString();
                  // var lastThree = x.substring(x.length-3);
                  // var otherNumbers = x.substring(0,x.length-3);
                  // if(otherNumbers != '')
                  // lastThree = ',' + lastThree;
                  // var grandPrice = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;


               // console.log(grandPrice);
                $('.grand_total').html( '' );
                $('.grand_total').html( grandPrice.toLocaleString() );
              //  alert ("yes");
               // localStorage.removeItem('discount_amount');
               }
               else
               {

                var discount_amount=Number($(".discount_amount").html());
                var dec = (discount_amount/100).toFixed(2);
                var mult = sum*dec; 
                var grandPrice=sum-mult;
              //  console.log(grandPrice)
                // console.log(x);
                //   x=x.toString();
                //   var lastThree = x.substring(x.length-3);
                //   var otherNumbers = x.substring(0,x.length-3);
                //   if(otherNumbers != '')
                    
                //   lastThree = ',' + lastThree;
                //   var grandPrice = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            //console.log('^^^^^^6666^^^^^^^');
            //console.log(grandPrice);
                $('.grand_total').html( '' );
                $('.grand_total').html( grandPrice.toLocaleString() );


              }

             }
             else
             {
             //console.log('%%%%%%%%%%%%%%%%%%%%%%'); 


              if (localStorage.getItem('discount_amount') == 'on')
              {
            //alert('total');
                 var mult=Number($(".discount_amount").html());
               //  console.log('((((((((((((((((((((');
               //  console.log('sum');
               //  console.log(sum);
               //  console.log(mult);
              //  var dec = (discount_amount/100).toFixed(2);
             //   var mult = sum*dec; 
                var grandPrice=Number(sum) - Number(mult);
                  // x=x.toString();
                  // var lastThree = x.substring(x.length-3);
                  // var otherNumbers = x.substring(0,x.length-3);
                  // if(otherNumbers != '')
                  // lastThree = ',' + lastThree;
                  // var grandPrice = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;


             //   console.log(grandPrice);
                $('.grand_total').html( '' );
                $('.grand_total').html( grandPrice.toLocaleString() );
              //  alert ("yes");
               // localStorage.removeItem('discount_amount');
               }
               else
               {

             // console.log('workdfgfdgdfgdfg');
                var discount_amount=Number($(".discount_amount").html());
                var dec = (discount_amount/100).toFixed(2);
                var mult = sum*dec; 
                var grandPrice=sum-mult;
                //var x=123456524578;
                  // x=x.toString();
                  // var lastThree = x.substring(x.length-3);
                  // var otherNumbers = x.substring(0,x.length-3);
                  // if(otherNumbers != '')
                  // lastThree = ',' + lastThree;
                  // var grandPrice = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                $('.grand_total').html( '' );
                $('.grand_total').html(grandPrice.toLocaleString());
               }

               
                //dis_append();
             }

             }
                  
             }

                //console.log('22222222222222');
                $('.subscription_sub_total').html('');
                //$('.subscription_sub_total').html(Math.round(sum1));
                $('.subscription_sub_total').html(sum1.toFixed(2));

             }
  return true;            
}




$("#search_subscription_name").keyup(function(){
var service_name=$("#search_subscription_name").val();
if(service_name!=''){
  $.ajax({
      url: '<?php echo base_url();?>proposal_page/search_Subscription/',
      type : 'POST',
      data : {'service_name':service_name},                    
      beforeSend: function() {
         $(".LoadingImage").show();
      },
      success: function(data) {
         $(".LoadingImage").hide();
        var json = JSON.parse(data); 
        var contents=json['contents']; 
         $('.subscription_lists').show();
        $('.subscription_lists').html('');
        $('.subscription_lists').html(contents);
         $('.tab-used2').removeClass('active');
        $('.tab-used2').first().addClass('active');
        $('#subscription_all').addClass('in active');
       $('#subscription_all').hide();
      }   
  });
}
});
$(".closed").click(function(){
 $(".LoadingImage").hide();
});


function dis_count(rspt){

var discount=parseFloat(rspt.value);
var price=parseFloat($(rspt).parents('.top-field-heads').find('.price').val());

var dec = (discount/100).toFixed(2);
var mult = price*dec; 
var calcPrice=price-mult;
//var calcPrice  = parseFloat(price.toFixed(2)) - parseFloat(( price * discount / 100 ).toFixed(2));
$(rspt).parents('.top-field-heads').find('.price').val('');
$(rspt).parents('.top-field-heads').find('.price').val(Math.ceil(calcPrice));
if(rspt.id=='service_discount'){
 // console.log('service sub totaldiscount');
sub_total();
}else if(rspt.id=='product_discount'){
   // console.log('product sub totaldiscount');
product_sub_total();
}else{
 // console.log('Subscription sub totaldiscount');
subscription_sub_total();
}
}

function tax_amount(rspt){
var tax=parseFloat(rspt.value);
var price=parseFloat($(rspt).parents('.top-field-heads').find('.price').val());
//alert(price);

var dec = (tax/100).toFixed(2);
var mult = price*dec; 
var calcPrice=price+mult;
//var calcPrice  = parseFloat(price.toFixed(2)) + parseFloat(( price * tax / 100 ).toFixed(2));
//alert(calcPrice);
$(rspt).parents('.top-field-heads').find('.price').val('');
$(rspt).parents('.top-field-heads').find('.price').val(Math.ceil(calcPrice));  
if(rspt.id=='service_tax'){
  // console.log('service sub totatax');
sub_total();
}else if(rspt.id=='product_tax'){
  //console.log('product sub totaltax');
product_sub_total();
}else{
  //console.log('Subscription sub totatax');
subscription_sub_total();
}
//sub_total();
}


function saved(save)
{
   var lengths = $('.saved_services_contents .top-field-heads').length;
   var length = $('.saved_service_contents .top-field-heads').length;
   var nex_index;

   if($(save).parents('.input-fields').find('#item_name').val()!='' && $(save).parents('.input-fields').find('#price').val()!='' && $(save).parents('.input-fields').find('#unit').val()!='' && $(save).parents('.input-fields').find('#qty').val()!='' &&  $(save).parents('.input-fields').find('#description').val()!=''  )
   {       
       $(save).parents('.input-fields').hide();       
      
        if($(save).parents('.input-fields').hasClass('add-service input-fields'))
        { 
            $('.saved_service_contents').show();

            if(length == '1' && $('.saved_service_contents .top-field-heads:nth-child(1)').is(':hidden') == true && $('.saved_service_contents .top-field-heads:nth-child(1) .saved_item_name').html().trim() == "")
            {  
                nex_index = length;

                $('.saved_service_contents .saved_item_name').html('');
                $('.saved_service_contents .saved_item_price').html(''); 
                $('.saved_service_contents .saved_item_unit').html(''); 
                $('.saved_service_contents .saved_item_qty').html('');
                $('.saved_service_contents .saved_item_description').html('');
                $(".saved_service_contents .saved_item_tax" ).html('');
                $(".saved_service_contents .saved_item_discount" ).html('');                

               if((!$.trim($('.saved_service_contents .saved_item_name').html())))
               {

                $(save).parents('.input-fields').addClass('place'+nex_index+'');
                $('.saved_service_contents .top-field-heads:nth-child('+nex_index+') .edit_service').addClass('place'+nex_index+'');
                $('.saved_service_contents .top-field-heads:nth-child('+nex_index+')').show();

                $('.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_name').html($(save).parents('.input-fields').find('#item_name').val());
                $('.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_price').html($(save).parents('.input-fields').find('#price').val()); 
                $('.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_unit').html($(save).parents('.input-fields').find('#unit').val()); 
                $('.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_qty').html($(save).parents('.input-fields').find('#qty').val());                     
                $('.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_description').html($(save).parents('.input-fields').find('#description').val());
                $( '.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_tax' ).html($(save).parents('.input-fields').find('#service_tax').val());
                $( '.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_discount' ).html($(save).parents('.input-fields').find('#service_discount').val());
               
                if($(save).parents('.input-fields').find('[name="service_optional[]"]').prop('checked') == true)
                {
                    $('.saved_service_contents .top-field-heads:nth-child('+nex_index+') .border-saved').attr('checked','checked');
                }
                else
                {
                    $('.saved_service_contents .top-field-heads:nth-child('+nex_index+') .border-saved').removeAttr('checked');
                }
               }
            }
            else if($(save).parents('.input-fields').find('#item_name').val()!='')
            {
                 /* var index=Number($(".add-service .edit_before_save").index(save ));
                  var nex_index=Number(index);
                  
                  if(nex_index == '1' || nex_index == '0')
                  {*/
                     nex_index =  length +1;
                  /*}*/

                 $(save).parents('.input-fields').addClass('place'+nex_index+'');

                 var content='<div class="top-field-heads">  <div class="proposal-fields1 service-items"><div class="checkbox-saved"><input class="border-saved" type="checkbox"></div><p class="saved_item_name">  </p>    <p class="saved_item_description">  </p></div><div class="proposal-fields2 price-services service-items"> <span class="saved_item_price"></span><span>/</span>   <span class="saved_item_unit"></span></div> <div class="proposal-fields3 service-items"> <p class="saved_item_qty"></p> </div><div class="proposal-fields5 service-items tax-field" ><p class="saved_item_tax"> </p></div><div class="proposal-fields6 service-items discount-field"> <p class="saved_item_discount"></p> </div>  <div class="proposal-fields7 service-items discount-field1 final-edit-pro"><a class="edit_service place'+nex_index+'" id="edit_service" onclick="edit_service(this)"><i class="icofont icofont-edit" aria-hidden="true"></i></a><a class="cancel_service extra_cancel_service" id="cancel_service" onclick="cancel_service(this)">  <i class="icofont icofont-ui-delete deleteAllTask" aria-hidden="true"></i></a> </div>  </div>';
                 //console.log('false');
                //console.log($('.saved_service_contents .top-field-heads:nth-child('+nex_index+')').html());

                  if($('.saved_service_contents .top-field-heads:nth-child('+nex_index+')').html()==undefined)
                  {
                   // alert('append');
                       $('.saved_service_contents').append(content);
                       
                        if ($('.service-table').hasClass('discount')) {
                            $(".proposal-fields6").show();
                            }

                            if ($('.service-table').hasClass('tax')) {
                            $(".proposal-fields5").show();
                            }
                  }

                  // console.log('###########');
                  // console.log($(save).parents('.input-fields').find('#item_name').val());
                  // console.log(nex_index);
                  // console.log($('.saved_service_contents .top-field-heads:nth-child('+nex_index+')').find(' .saved_item_name').html());

                   $('.saved_service_contents .top-field-heads:nth-child('+nex_index+')').show();                   
                   $('.saved_service_contents .top-field-heads:nth-child('+nex_index+')').find('.saved_item_name').html($(save).parents('.input-fields').find('#item_name').val()); 

                   // console.log($( '.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_name' ).html()); 
                   $( '.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_price' ).html($(save).parents('.input-fields').find('#price').val());     
                   $( '.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_qty').html($(save).parents('.input-fields').find('#qty').val());                         
                   $( '.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_unit' ).html($(save).parents('.input-fields').find('#unit').val());    
                   $( '.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_description').html($(save).parents('.input-fields').find('#description').val());
                   $( '.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_tax' ).html($(save).parents('.input-fields').find('#service_tax').val());
                   $( '.saved_service_contents .top-field-heads:nth-child('+nex_index+') .saved_item_discount' ).html($(save).parents('.input-fields').find('#service_discount').val());
                   
                 
                   if($(save).parents('.input-fields').find('[name="service_optional[]"]').prop('checked') == true)
                   {
                       $( '.saved_service_contents .top-field-heads:nth-child('+nex_index+') .border-saved' ).attr('checked','checked');
                   }  
                   else
                   {
                       $('.saved_service_contents .top-field-heads:nth-child('+nex_index+') .border-saved').removeAttr('checked');
                   }                 
            } 
        }
        else if($(save).parents('.input-fields').hasClass('add-services input-fields'))
        { 
           //alert('ok');

               //var indexs = Number($( ".add-services #service_save" ).index( save ));  

           // alert($(save).parents('.field-add01').children('.extra-saved-service-content').children('.remove-content:nth-child('+indexs+')').attr('class'));  

           // $(save).parents('.field-add01').children('.extra-saved-service-content').children('.remove-content:nth-child('+indexs+')').remove();  
       
           //alert(index);
           //alert($(save).parents('.field-add01').children('.extra-saved-service-content').find('.remove-content:nth-child('+index+')').attr('class'));

           /*var nex_index=Number(indexs); */ 
           
           if(lengths == '0')
           {
              nex_index = Number(1); 
           }
           else
           {
              nex_index = lengths +1;
           }
       
        // alert(nex_index);
           var content='  <div class="top-field-heads">  <div class="proposal-fields1 service-items"><div class="checkbox-saved"><input class="border-saved" type="checkbox"></div><p class="saved_item_name">  </p>    <p class="saved_item_description">  </p></div><div class="proposal-fields2 price-services service-items"> <span class="saved_item_price"></span><span>/</span>   <span class="saved_item_unit"></span></div> <div class="proposal-fields3 service-items"> <p class="saved_item_qty"></p> </div><div class="proposal-fields5 service-items tax-field" ><p class="saved_item_tax"> </p></div><div class="proposal-fields6 service-items discount-field"> <p class="saved_item_discount"></p> </div>  <div class="proposal-fields7 service-items discount-field1 final-edit-pro"><a class="edit_service place'+nex_index+'" id="edit_service" onclick="edit_service(this)"><i class="icofont icofont-edit" aria-hidden="true"></i></a><a class="cancel_service extra_cancel_service" id="cancel_service" onclick="cancel_service(this)">  <i class="icofont icofont-ui-delete deleteAllTask" aria-hidden="true"></i></a> </div>  </div>'; 

          $(save).parents('.input-fields').addClass('place'+nex_index+'');
          
         //  console.log(content);
           $('.extra-saved-service-content').append('<div class="saved_services_contents remove-content">'+content+'</div>');

           if ($('.service-table').hasClass('discount')) {
                   $(".proposal-fields6").show();
                   }

                   if ($('.service-table').hasClass('tax')) {
                   $(".proposal-fields5").show();
                   }


          $('.saved_services_contents .top-field-heads:nth-child('+nex_index+')').show();          
          $( ".saved_services_contents .saved_item_name" ).last().html($(save).parents('.input-fields').find('#item_name').val());   
          $( ".saved_services_contents .saved_item_price" ).last().html($(save).parents('.input-fields').find('#price').val());     
          $( ".saved_services_contents .saved_item_qty" ).last().html($(save).parents('.input-fields').find('#qty').val());                         
          $( ".saved_services_contents .saved_item_unit" ).last().html($(save).parents('.input-fields').find('#unit').val());    
          $( ".saved_services_contents .saved_item_description" ).last().html($(save).parents('.input-fields').find('#description').val());
          $( ".saved_services_contents .saved_item_tax" ).last().html($(save).parents('.input-fields').find('#service_tax').val());
          $( ".saved_services_contents .saved_item_discount" ).last().html($(save).parents('.input-fields').find('#service_discount').val());

         
          if($(save).parents('.input-fields').find('[name="service_optional[]"]').prop('checked') == true)
          {
             $(".saved_services_contents .border-saved").last().attr('checked','checked');
          }
          else
          {
             $(".saved_services_contents .border-saved").last().removeAttr('checked');
          }
       }
             
   }
   else
   {  
       if($(save).parents('.input-fields').find('#item_name').val()==''){
          $(save).parents('.input-fields').find('#item_name').css('border', '1px solid red');
       } if($(save).parents('.input-fields').find('#price').val()==''){
          $(save).parents('.input-fields').find('#price').css('border', '1px solid red');
       } if($(save).parents('.input-fields').find('#unit').val()==''){
         $(save).parents('.input-fields').find('#unit').css('border', '1px solid red');
       } if($(save).parents('.input-fields').find('#qty').val()==''){
         $(save).parents('.input-fields').find('#qty').css('border', '1px solid red');
       }  if($(save).parents('.input-fields').find('#description').val()==''){
         $(save).parents('.input-fields').find('#description').css('border', '1px solid red');
       }
   }
}



function cancel_service(cancelled)
{

   var index=$('.top-field-heads .cancel_service').index( cancelled );
   var nex_index=Number(index) + Number(1);
  //console.log(nex_index);
  //console.log($(cancelled).parents('.remove-content').attr('class'));

  if($(cancelled).parents('.remove-content').attr('class')=='saved_service_contents remove-content'){
   $('.saved_service_contents .top-field-heads:nth-child('+nex_index+')').remove();

  // if(localStorage.getItem("price")=='service'){
  // console.log('service content');
  // console.log(nex_index);

  //    $('.contents .input-fields:nth-child('+nex_index+')').remove();
  // }


   <?php 
   if(isset($records['item_name']) && $records['item_name']!=''){ ?>
     $('.contents .input-fields:nth-child('+nex_index+')').remove();
   <?php }else{ ?>
        if(localStorage.getItem("price")=='service'){
      //  console.log('service content');
      //  console.log(nex_index);

        $('.contents .input-fields:nth-child('+nex_index+')').remove();
        }else{

    $('.contents .input-fields:nth-child('+nex_index+')').html('');
  }
   <?php } ?>





  }else{
  //    console.log('length');
  //  console.log($(".saved_services_contents").length)
  //  if($(".saved_services_contents").length=='1'){

  // var nex_index=$('.top-field-heads .extra_cancel_service').index( cancelled );
  // console.log(nex_index);

  // // console.log(nex_index);
  //  $('.extra-saved-service-content .saved_services_contents:nth-child('+nex_index+')').remove();
  //  $('.extra-add-service .input-fields:nth-child('+nex_index+')').remove();
  //   }else{
  //   console.log('else');
  var index=$('.top-field-heads .extra_cancel_service').index( cancelled );
  var nex_index=Number(index);    

  if(nex_index == 0)
  {
     nex_index = 1;
  }

  $('.extra-saved-service-content .saved_services_contents:nth-child('+nex_index+')').remove();
  $('.extra-add-service .input-fields:nth-child('+nex_index+')').remove('');
   //}
  }

     // $('.saved-productcontents .top-field-heads:nth-child('+nex_index+')').show();
     //  $(cancelled).children('.input-fields').find('.top-field-heads').remove();
     //  $(cancelled).children('.input-fields').find('.bottom-field-heads').remove();
     //  if($(cancelled).parents('.remove-content').attr('class')=='saved_service_contents remove-content'){
     //   $(cancelled).parents('.top-field-heads').hide(); 
     //  }else{
     //     $(cancelled).parents('.remove-content').find('.top-field-heads').remove();
     //  } 
      sub_total(); 
}







function saved_product(save)
{ 
   var length = $(".saved_product_contents .top-field-heads").length;
   var lengths = $(".saved_products_contents .top-field-heads").length;
   var nex_index;
    
    if($(save).parents('.input-fields').find('#product_name').val()!='' && $(save).parents('.input-fields').find('#product_price').val()!='' && $(save).parents('.input-fields').find('#product_qty').val()!='' && $(save).parents('.input-fields').find('#product_description').val()!='')
    {

       $(save).parents('.input-fields').hide();
     
       if($(save).parents('.input-fields').hasClass('add-product input-fields'))
       { 
           $('.saved_product_contents').show(); 
          
           if(length == '1' && $('.saved_product_contents .top-field-heads:nth-child(1)').is(':hidden') == true && $('.saved_product_contents .top-field-heads:nth-child(1) .saved_product_name').html().trim() == "")
           { 
              nex_index = length;

              $('.saved_product_contents .saved_product_name').html('');
              $('.saved_product_contents .saved_product_price').html('');    
              $('.saved_product_contents .saved_product_qty').html('');
              $('.saved_product_contents .saved_product_description').html('');
              $( ".saved_product_contents .saved_product_tax" ).html('');
              $(".saved_product_contents .saved_product_discount" ).html('');              

              if(!$.trim($('.saved_product_contents .saved_product_name').html()))
              {        
                $('.saved_product_contents .top-field-heads:nth-child('+nex_index+')').show();
                $(save).parents('.input-fields').addClass('place'+nex_index+'');
                $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .edit_product').addClass('place'+nex_index+'');
               
                $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .saved_product_name').html($(save).parents('.input-fields').find('#product_name').val());
                $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .saved_product_price').html($(save).parents('.input-fields').find('#product_price').val());   
                $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .saved_product_qty').html($(save).parents('.input-fields').find('#product_qty').val());                     
                $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .saved_product_description').html($(save).parents('.input-fields').find('#product_description').val());
                $( '.saved_product_contents .top-field-heads:nth-child('+nex_index+') .saved_product_tax').html($(save).parents('.input-fields').find('#product_tax').val());
                $( '.saved_product_contents .top-field-heads:nth-child('+nex_index+') .saved_product_discount' ).html($(save).parents('.input-fields').find('#product_discount').val());              

                if($(save).parents('.input-fields').find('[name="product_optional[]"]').prop('checked') == true)
                {
                   $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .border-saved').attr('checked','checked');
                }
                else
                {
                   $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .border-saved').removeAttr('checked');
                }
              }
          }          
          else if($(save).parents('.input-fields').find('#product_name').val()!='')
          {  
          //  alert('Called');
       /*   var index=Number($(".add-product .edit_beforeproduct_save").index(save));
          var nex_index=Number(index);
 
          if(nex_index == '1' || nex_index == '0')
          {
             nex_index = $('.saved_product_contents .top-field-heads').length +1;
          } */        

           nex_index = length +1;

          $(save).parents('.input-fields').addClass('place'+nex_index+'');

           var content='<div class="top-field-heads"> <div class="proposal-fields1 service-items"><div class="checkbox-saved"><input class="border-saved" type="checkbox"></div><p class="saved_product_name"> </p>  <p class="saved_product_description">  </p> </div>  <div class="proposal-fields2 price-services service-items">   <span class="saved_product_price"></span>    <!-- <span>/</span>  <span class="saved_product_unit"></span> --> </div> <div class="proposal-fields3 service-items"> <p class="saved_product_qty"></p> </div> <div class="proposal-fields5 service-items tax-field" > <p class="saved_product_tax"></p> </div><div class="proposal-fields6 service-items discount-field"> <p class="saved_product_discount"></p> </div>  <div class="proposal-fields7 service-items discount-field1 final-edit-pro"><a class="edit_product red-grcolor place'+nex_index+'" id="edit_product" onclick="edit_product(this)"><i class="icofont icofont-edit" aria-hidden="true"></i></a> <a class="cancel_product extra_cancel_product red-grcolor12" id="cancel_product" onclick="cancel_product(this)"> <i class="icofont icofont-ui-delete deleteAllTask" aria-hidden="true"></i></a>  </div>  </div>'; 
          //console.log('******');
          //console.log('false');
          //console.log($('.saved_service_contents .top-field-heads:nth-child('+nex_index+')').html());

            if($('.saved_product_contents .top-field-heads:nth-child('+nex_index+')').html()==undefined)
            {
                 $('.saved_product_contents').append(content);                 
                 
                 if ($('.service-table').hasClass('discount')) {
                      $(".proposal-fields6").show();
                      }

                      if ($('.service-table').hasClass('tax')) {
                      $(".proposal-fields5").show();
                      }
            }
              
              $('.saved_product_contents .top-field-heads:nth-child('+nex_index+')').show();            
              $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .saved_product_name').html($(save).parents('.input-fields').find('#product_name').val());
              $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .saved_product_price').html($(save).parents('.input-fields').find('#product_price').val());   
               $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .saved_product_qty').html($(save).parents('.input-fields').find('#product_qty').val());                     
              $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .saved_product_description').html($(save).parents('.input-fields').find('#product_description').val());
              $( '.saved_product_contents .top-field-heads:nth-child('+nex_index+') .saved_product_tax').html($(save).parents('.input-fields').find('#product_tax').val());
              $( '.saved_product_contents .top-field-heads:nth-child('+nex_index+') .saved_product_discount' ).html($(save).parents('.input-fields').find('#product_discount').val());

              if($(save).parents('.input-fields').find('[name="product_optional[]"]').prop('checked') == true)
              {
                 $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .border-saved').attr('checked','checked');
              }
              else
              {
                 $('.saved_product_contents .top-field-heads:nth-child('+nex_index+') .border-saved').removeAttr('checked');
              }
           }
      }
      else if($(save).parents('.input-fields').hasClass('add-products input-fields'))
      {   
        // console.log('3st'); 
         /*var index = Number($(".add-products #product_save").index(save));
         var nex_index = Number(index);   

         if(nex_index == '0')
         {
           nex_index = Number(nex_index)+Number(1);
         }*/

         if(lengths == '0')
         {
            nex_index = Number(1); 
         }
         else
         {
            nex_index = lengths +1;
         }
 
         $(save).parents('.input-fields').addClass('place'+nex_index+'');
        //alert($(save).parents('.input-fields').find('#product_name').val());
          var content=' <div class="top-field-heads"> <div class="proposal-fields1 service-items"><div class="checkbox-saved"><input class="border-saved" type="checkbox"></div><p class="saved_product_name"> </p>  <p class="saved_product_description">  </p> </div>  <div class="proposal-fields2 price-services service-items">   <span class="saved_product_price"></span>    <!-- <span>/</span>  <span class="saved_product_unit"></span> --> </div> <div class="proposal-fields3 service-items"> <p class="saved_product_qty"></p> </div> <div class="proposal-fields5 service-items tax-field" > <p class="saved_product_tax"></p> </div><div class="proposal-fields6 service-items discount-field"> <p class="saved_product_discount"></p> </div>  <div class="proposal-fields7 service-items discount-field1 final-edit-pro"><a class="edit_product red-grcolor place'+nex_index+'" id="edit_product" onclick="edit_product(this)"><i class="icofont icofont-edit" aria-hidden="true"></i></a> <a class="cancel_product extra_cancel_product red-grcolor12" id="cancel_product" onclick="cancel_product(this)"> <i class="icofont icofont-ui-delete deleteAllTask" aria-hidden="true"></i></a>  </div>  </div>';

          $('.extra-saved-product-content').append('<div class="saved_products_contents remove-content">'+content+'</div>');

          if ($('.service-table').hasClass('discount')) {
                  $(".proposal-fields6").show();
                  }

                  if ($('.service-table').hasClass('tax')) {
                  $(".proposal-fields5").show();
                  }

         $('.saved_products_contents .top-field-heads:nth-child('+nex_index+')').show();          
         $( ".saved_products_contents .saved_product_name" ).last().html($(save).parents('.input-fields').find('#product_name').val());   
         $( ".saved_products_contents .saved_product_price" ).last().html($(save).parents('.input-fields').find('#product_price').val());     
         $( ".saved_products_contents .saved_product_qty" ).last().html($(save).parents('.input-fields').find('#product_qty').val());             
         $( ".saved_products_contents .saved_product_description" ).last().html($(save).parents('.input-fields').find('#product_description').val());
         $( ".saved_products_contents .saved_product_tax" ).last().html($(save).parents('.input-fields').find('#product_tax').val());
         $( ".saved_products_contents .saved_product_discount" ).last().html($(save).parents('.input-fields').find('#product_discount').val());

         if($(save).parents('.input-fields').find('[name="product_optional[]"]').prop('checked') == true)
         {
            $('.saved_products_contents .border-saved').last().attr('checked','checked');
         }
         else
         {
            $('.saved_products_contents .border-saved').last().removeAttr('checked');
         }
      }
     
   }
   else
   {
        if($(save).parents('.input-fields').find('#product_name').val()==''){
         $(save).parents('.input-fields').find('#product_name').css('border', '1px solid red');
      } if($(save).parents('.input-fields').find('#product_price').val()==''){
         $(save).parents('.input-fields').find('#product_price').css('border', '1px solid red');
      } if($(save).parents('.input-fields').find('#product_qty').val()==''){
        $(save).parents('.input-fields').find('#product_qty').css('border', '1px solid red');
      } if($(save).parents('.input-fields').find('#product_description').val()==''){
        $(save).parents('.input-fields').find('#product_description').css('border', '1px solid red');
      }
    }
}

function cancelled_product(cancel)
{
   if($(cancel).parents('.input-fields').find('#product_name').val()!='' && $(cancel).parents('.input-fields').find('#product_price').val()!='' && $(cancel).parents('.input-fields').find('#product_qty').val()!='' && $(cancel).parents('.input-fields').find('#product_description').val()!='')
   {   
       $(cancel).parents('.input-fields').hide(); 
       var place = $(cancel).parents('.input-fields').attr('class');   

       if($(cancel).parents('.input-fields').hasClass('add-product input-fields'))
       {          
          place = place.replace('add-product','');
          place = place.replace('input-fields','');
          place = place.trim();

          var obj = $('.saved_product_contents .'+place).parents('.top-field-heads');   
          $(cancel).parents('.input-fields').find('#product_name').val($(obj).find('.saved_product_name').html().trim());
          $(cancel).parents('.input-fields').find('#product_price').val($(obj).find('.saved_product_price').html().trim());   
          $(cancel).parents('.input-fields').find('#product_qty').val($(obj).find('.saved_product_qty').html().trim());  
          $(cancel).parents('.input-fields').find('#product_description').val($(obj).find('.saved_product_description').html().trim());
          $(cancel).parents('.input-fields').find('#product_tax').val($(obj).find('.saved_product_tax').html().trim());
          $(cancel).parents('.input-fields').find('#product_discount').val($(obj).find('.saved_product_discount').html().trim());

          $('.saved_product_contents').show(); 
          $('.saved_product_contents .'+place).parents('.top-field-heads').show();
       }
       else if($(cancel).parents('.input-fields').hasClass('add-products input-fields'))
       {          
          place = place.replace('add-products','');
          place = place.replace('input-fields','');
          place = place.trim();

          var obj = $('.saved_products_contents .'+place).parents('.top-field-heads');

         $(cancel).parents('.input-fields').find('#product_name').val($(obj).find('.saved_product_name').html().trim());
         $(cancel).parents('.input-fields').find('#product_price').val($(obj).find('.saved_product_price').html().trim());   
         $(cancel).parents('.input-fields').find('#product_qty').val($(obj).find('.saved_product_qty').html().trim());  
         $(cancel).parents('.input-fields').find('#product_description').val($(obj).find('.saved_product_description').html().trim());
         $(cancel).parents('.input-fields').find('#product_tax').val($(obj).find('.saved_product_tax').html().trim());
         $(cancel).parents('.input-fields').find('#product_discount').val($(obj).find('.saved_product_discount').html().trim());

          $('.saved_products_contents').show(); 
          $('.saved_products_contents .'+place).parents('.top-field-heads').show();
       } 

       product_sub_total();
   }
   else
   {     
       if($(cancel).parents('.input-fields').find('#product_name').val()==''){
         $(cancel).parents('.input-fields').find('#product_name').css('border', '1px solid red');
      } if($(cancel).parents('.input-fields').find('#product_price').val()==''){
         $(cancel).parents('.input-fields').find('#product_price').css('border', '1px solid red');
      } if($(cancel).parents('.input-fields').find('#product_qty').val()==''){
        $(cancel).parents('.input-fields').find('#product_qty').css('border', '1px solid red');
      } if($(cancel).parents('.input-fields').find('#product_description').val()==''){
        $(cancel).parents('.input-fields').find('#product_description').css('border', '1px solid red');
      }
   }
}


function deleted_product(cancel)
{
   $(cancel).parents('.input-fields').remove();  
    
   sub_total();
}

function edit_product(edit)
{ 
    $(".delete_product_div").hide();
    $(".cancel_product_div").show();
    $(".edit_beforeproduct_save").hide();
    $(".edit_afterproduct_save").show();

  var place = $(edit).parents('.top-field-heads').find('.edit_product').attr('class'); 
  place = place.replace('edit_product',''); 
  place = place.replace('red-grcolor','');
  place = place.trim(); 

  if($(edit).parents('.remove-content').hasClass('saved_product_contents remove-content'))
  {  //alert('if'
    var index = Number($( ".saved_product_contents .edit_product" ).index( edit ));
    var nex_index=Number(index) + Number(1); 

     $('.product-contents .input-fields').each(function()
     { 
        if($(this).hasClass(place))
        {
           if(($(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == true && $(this).find('[name="product_optional[]"]').prop('checked') == false) || $(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == false && $(this).find('[name="product_optional[]"]').prop('checked') == true)
           {
              $(this).find('[name="product_optional[]"]').trigger('click');

              if($(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == true)
              {
                 $(this).find('[name="product_optional[]"]').attr('checked','checked');
              }
              else
              {
                 $(this).find('[name="product_optional[]"]').removeAttr('checked');
              }
           }
           $(this).show();
        }
     });

     $(edit).parents('.remove-content').find('.top-field-heads:nth-child('+nex_index+')').hide();
   
  }
  else if($(edit).parents('.remove-content').attr('class')=='saved_products_contents remove-content')
  { //alert('eif');
 
    /*var index = Number($( ".extra-saved-product-content .edit_product" ).index( edit ));
    var nex_index=Number(index) + Number(1);*/
 
    if(localStorage.getItem("first_item")=='yes')
    {
      localStorage.removeItem("first_item");
        $(edit).parents().next('.input-fields').show();
    }

    $('.extra-add-products .input-fields').each(function()
    {
       if($(this).hasClass(place))
       {
          if(($(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == true && $(this).find('[name="product_optional[]"]').prop('checked') == false) || $(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == false && $(this).find('[name="product_optional[]"]').prop('checked') == true)
          {
             $(this).find('[name="product_optional[]"]').trigger('click');

             if($(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == true)
             {
                $(this).find('[name="product_optional[]"]').attr('checked','checked');
             }
             else
             {
                $(this).find('[name="product_optional[]"]').removeAttr('checked');
             }
          }
          $(this).show();
       }
    });
    
    $(edit).parents('.remove-content').find('.top-field-heads').hide();
  }
}
function cancel_product(cancelled)
{ 
  var index=$('.top-field-heads .cancel_product').index(cancelled);
  var nex_index=Number(index) + Number(1); 
// alert(nex_index);
//alert($(cancelled).parents('.remove-content').attr('class'));

if($(cancelled).parents('.remove-content').attr('class')=='saved_product_contents remove-content'){ 
 $('.saved_product_contents .top-field-heads:nth-child('+nex_index+')').remove();

<?php
if(isset($records['product_name']) && $records['product_name']!=''){ ?>
 $('.product-contents .input-fields:nth-child('+nex_index+')').remove();
<?php }else{ ?>

   if(localStorage.getItem("price1")=='product'){

   $('.product-contents .input-fields:nth-child('+nex_index+')').remove();
}else{

 $('.product-contents .input-fields:nth-child('+nex_index+')').html('');
}
<?php } ?>

}else{
  var index=$('.top-field-heads .extra_cancel_product').index( cancelled );
   var nex_index=Number(index); 
   if(nex_index == 0)
   {
      nex_index = 1;
   }
$('.extra-saved-product-content .saved_products_contents:nth-child('+nex_index+')').remove();
$('.extra-add-products .input-fields:nth-child('+nex_index+')').remove();
}
    // $(cancelled).children('.input-fields').find('.top-field-heads').remove();
    // $(cancelled).children('.input-fields').find('.bottom-field-heads').remove();
    // if($(cancelled).parents('.remove-content').attr('class')=='saved_product_contents remove-content'){
    //  $(cancelled).parents('.top-field-heads').hide(); 
    // }else{
    //    $(cancelled).parents('.remove-content').find('.top-field-heads').remove();
    // } 
    sub_total(); 
}

function saved_subscription(save)
{   
   var lengths = $('.saved_subscriptions_contents .top-field-heads').length;
   var length = $('.saved_subscription_contents .top-field-heads').length;
   var nex_index;

   if($(save).parents('.input-fields').find('#subscription_name').val()!='' && $(save).parents('.input-fields').find('#subscription_price').val()!='' && $(save).parents('.input-fields').find('#subscription_unit').val()!='' && $(save).parents('.input-fields').find('#subscription_description').val()!='')
   {       
       $(save).parents('.input-fields').hide();       
      
        if($(save).parents('.input-fields').hasClass('add-subscription input-fields'))
        { 
            $('.saved_subscription_contents').show();

            if(length == '1' && $('.saved_subscription_contents .top-field-heads:nth-child(1)').is(':hidden') == true && $('.saved_subscription_contents .top-field-heads:nth-child(1) .saved_subscription_name').html().trim() == "")
            {  
                nex_index = length;

                $('.saved_subscription_contents .saved_subscription_name').html('');
                $('.saved_subscription_contents .saved_subscription_price').html(''); 
                $('.saved_subscription_contents .saved_subscription_unit').html('');      
                $('.saved_subscription_contents .saved_subscription_description').html('');
                $(".saved_subscription_contents .saved_subscription_tax" ).html('');
                $(".saved_subscription_contents .saved_subscription_discount" ).html('');              

               if((!$.trim($('.saved_subscription_contents .saved_subscription_name').html())))
               {
                  $(save).parents('.input-fields').addClass('place'+nex_index+'');
                  $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .edit_subscription').addClass('place'+nex_index+'');
                  $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+')').show();

                  $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .saved_subscription_name').html($(save).parents('.input-fields').find('#subscription_name').val());

                  $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .saved_subscription_price').html($(save).parents('.input-fields').find('#subscription_price').val());   

                   $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .saved_subscription_unit').html($(save).parents('.input-fields').find('#subscription_unit').val());   

                  $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .saved_subscription_description').html($(save).parents('.input-fields').find('#subscription_description').val());

                  $( '.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .saved_subscription_tax').html($(save).parents('.input-fields').find('#subscription_tax').val());

                  $( '.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .saved_subscription_discount').html($(save).parents('.input-fields').find('#subscription_discount').val());

                  if($(save).parents('.input-fields').find('[name="subscription_optional[]"]').prop('checked') == true)
                  {
                     $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .border-saved').attr('checked','checked');
                  }
                  else
                  {
                     $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .border-saved').removeAttr('checked');
                  }
                 }
            }
            else if($(save).parents('.input-fields').find('#subscription_name').val()!='')
            {             
                 nex_index =  length +1;              

                 $(save).parents('.input-fields').addClass('place'+nex_index+'');

                 var content='<div class="top-field-heads"><div class="proposal-fields1 service-items"><div class="checkbox-saved"><input class="border-saved" type="checkbox"></div><p class="saved_subscription_name"></p>  <p class="saved_subscription_description"></p> </div> <div class="proposal-fields2 price-services service-items"> <span class="saved_subscription_price"></span>  <span>/</span> <span class="saved_subscription_unit"></span></div><!-- <div class="proposal-fields3 service-items"><p class="saved_subscription_qty"></p>  </div> --><div class="proposal-fields5 service-items tax-field" ><p class="saved_subscription_tax"> </p></div> <div class="proposal-fields6 service-items discount-field"><p class="saved_subscription_discount"></p>  </div>  <div class="proposal-fields7 service-items discount-field1 final-edit-pro"><a class="edit_subscription red-grcolor place'+nex_index+'" id="edit_subscription" onclick="edit_sub(this)"><i class="icofont icofont-edit" aria-hidden="true"></i></a><a class="cancel_subscription extra_cancel_subscription red-grcolor12" id="cancel_subscription" onclick="cancel_sub(this)"> <i class="icofont icofont-ui-delete deleteAllTask" aria-hidden="true"></i></a></div></div>'; 
                 
                 if($('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+')').html()==undefined){
                   // alert('append');
                       $('.saved_subscription_contents').append(content);
                       
                       if ($('.service-table').hasClass('discount')) {
                            $(".proposal-fields6").show();
                            }

                            if ($('.service-table').hasClass('tax')) {
                            $(".proposal-fields5").show();
                            }

                  }
                  $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+')').show();
                  $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .saved_subscription_name').html($(save).parents('.input-fields').find('#subscription_name').val());

                  $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .saved_subscription_price').html($(save).parents('.input-fields').find('#subscription_price').val());   

                   $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .saved_subscription_unit').html($(save).parents('.input-fields').find('#subscription_unit').val());   

                  $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .saved_subscription_description').html($(save).parents('.input-fields').find('#subscription_description').val());

                  $( '.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .saved_subscription_tax').html($(save).parents('.input-fields').find('#subscription_tax').val());

                  $( '.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .saved_subscription_discount').html($(save).parents('.input-fields').find('#subscription_discount').val());

                  if($(save).parents('.input-fields').find('[name="subscription_optional[]"]').prop('checked') == true)
                  {
                     $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .border-saved').attr('checked','checked');
                  }
                  else
                  {
                     $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+') .border-saved').removeAttr('checked');
                  }
            } 
        }
        else if($(save).parents('.input-fields').hasClass('add-subscriptions input-fields'))
        {         
           if(lengths == '0')
           {
              nex_index = Number(1); 
           }
           else
           {
              nex_index = lengths +1;
           }    
       
           var content='<div class="top-field-heads"><div class="proposal-fields1 service-items"><div class="checkbox-saved"><input class="border-saved" type="checkbox"></div><p class="saved_subscription_name"></p>  <p class="saved_subscription_description"></p> </div> <div class="proposal-fields2 price-services service-items"> <span class="saved_subscription_price"></span>  <span>/</span> <span class="saved_subscription_unit"></span></div><!-- <div class="proposal-fields3 service-items"><p class="saved_subscription_qty"></p>  </div> --><div class="proposal-fields5 service-items tax-field" ><p class="saved_subscription_tax"> </p></div> <div class="proposal-fields6 service-items discount-field"><p class="saved_subscription_discount"></p>  </div>  <div class="proposal-fields7 service-items discount-field1 final-edit-pro"><a class="edit_subscription red-grcolor place'+nex_index+'" id="edit_subscription" onclick="edit_sub(this)"><i class="icofont icofont-edit" aria-hidden="true"></i></a><a class="cancel_subscription extra_cancel_subscription red-grcolor12" id="cancel_subscription" onclick="cancel_sub(this)"> <i class="icofont icofont-ui-delete deleteAllTask" aria-hidden="true"></i></a></div></div>'; 

          $(save).parents('.input-fields').addClass('place'+nex_index+'');
          
          $('.extra-saved-subscription-content').append('<div class="saved_subscriptions_contents remove-content">'+content+'</div>');
            if ($('.service-table').hasClass('discount')) {
               $(".proposal-fields6").show();
               }

               if ($('.service-table').hasClass('tax')) {
               $(".proposal-fields5").show();
               }

            $('.saved_subscriptions_contents .top-field-heads:nth-child('+nex_index+')').show();

            $( ".saved_subscriptions_contents .saved_subscription_name" ).last().html($(save).parents('.input-fields').find('#subscription_name').val());  

            $( ".saved_subscriptions_contents .saved_subscription_price" ).last().html($(save).parents('.input-fields').find('#subscription_price').val()); 

            $( ".saved_subscriptions_contents .saved_subscription_unit" ).last().html($(save).parents('.input-fields').find('#subscription_unit').val());                         
                
            $( ".saved_subscriptions_contents .saved_subscription_description" ).last().html($(save).parents('.input-fields').find('#subscription_description').val());

            $( ".saved_subscriptions_contents .saved_subscription_tax" ).last().html($(save).parents('.input-fields').find('#subscription_tax').val());

            $( ".saved_subscriptions_contents .saved_subscription_discount" ).last().html($(save).parents('.input-fields').find('#subscription_discount').val());        
           

            if($(save).parents('.input-fields').find('[name="subscription_optional[]"]').prop('checked') == true)
            {
               $('.saved_subscriptions_contents .border-saved').last().attr('checked','checked');
            }
            else
            {
               $('.saved_subscriptions_contents .border-saved').last().removeAttr('checked');
            }
       }
             
   }
   else
   {  
       if($(save).parents('.input-fields').find('#subscription_name').val()==''){
          $(save).parents('.input-fields').find('#subscription_name').css('border', '1px solid red');
       } if($(save).parents('.input-fields').find('#subscription_price').val()==''){
          $(save).parents('.input-fields').find('#subscription_price').css('border', '1px solid red');
       } if($(save).parents('.input-fields').find('#subscription_unit').val()==''){
         $(save).parents('.input-fields').find('#subscription_unit').css('border', '1px solid red');
       } if($(save).parents('.input-fields').find('#subscription_description').val()==''){
         $(save).parents('.input-fields').find('#subscription_description').css('border', '1px solid red');
       }
   }

}

function cancelled_subscription(cancel)
{
   if($(cancel).parents('.input-fields').find('#subscription_name').val()!='' && $(cancel).parents('.input-fields').find('#subscription_price').val()!='' && $(cancel).parents('.input-fields').find('#subscription_unit').val()!='' && $(cancel).parents('.input-fields').find('#subscription_description').val()!='')
   {   
       $(cancel).parents('.input-fields').hide(); 
       var place = $(cancel).parents('.input-fields').attr('class');   

       if($(cancel).parents('.input-fields').hasClass('add-subscription input-fields'))
       {          
          place = place.replace('add-subscription','');
          place = place.replace('input-fields','');
          place = place.trim();

          var obj = $('.saved_subscription_contents .'+place).parents('.top-field-heads');   
          $(cancel).parents('.input-fields').find('#subscription_name').val($(obj).find('.saved_subscription_name').html().trim());
          $(cancel).parents('.input-fields').find('#subscription_price').val($(obj).find('.saved_subscription_price').html().trim());   
          $(cancel).parents('.input-fields').find('#subscription_unit').val($(obj).find('.saved_subscription_unit').html().trim());  
          $(cancel).parents('.input-fields').find('#subscription_description').val($(obj).find('.saved_subscription_description').html().trim());
          $(cancel).parents('.input-fields').find('#subscription_tax').val($(obj).find('.saved_subscription_tax').html().trim());
          $(cancel).parents('.input-fields').find('#subscription_discount').val($(obj).find('.saved_subscription_discount').html().trim());

          $('.saved_subscription_contents').show(); 
          $('.saved_subscription_contents .'+place).parents('.top-field-heads').show();
       }
       else if($(cancel).parents('.input-fields').hasClass('add-subscriptions input-fields'))
       {          
          place = place.replace('add-subscriptions','');
          place = place.replace('input-fields','');
          place = place.trim();

          var obj = $('.saved_subscriptions_contents .'+place).parents('.top-field-heads');

          $(cancel).parents('.input-fields').find('#subscription_name').val($(obj).find('.saved_subscription_name').html().trim());
          $(cancel).parents('.input-fields').find('#subscription_price').val($(obj).find('.saved_subscription_price').html().trim());   
          $(cancel).parents('.input-fields').find('#subscription_unit').val($(obj).find('.saved_subscription_unit').html().trim());  
          $(cancel).parents('.input-fields').find('#subscription_description').val($(obj).find('.saved_subscription_description').html().trim());
          $(cancel).parents('.input-fields').find('#subscription_tax').val($(obj).find('.saved_subscription_tax').html().trim());
          $(cancel).parents('.input-fields').find('#subscription_discount').val($(obj).find('.saved_subscription_discount').html().trim());

          $('.saved_subscriptions_contents').show(); 
          $('.saved_subscriptions_contents .'+place).parents('.top-field-heads').show();
       } 

       subscription_sub_total();
   }
   else
   {     
       if($(cancel).parents('.input-fields').find('#subscription_name').val()==''){
          $(cancel).parents('.input-fields').find('#subscription_name').css('border', '1px solid red');
       } if($(cancel).parents('.input-fields').find('#subscription_price').val()==''){
          $(cancel).parents('.input-fields').find('#subscription_price').css('border', '1px solid red');
       } if($(cancel).parents('.input-fields').find('#subscription_unit').val()==''){
         $(cancel).parents('.input-fields').find('#subscription_unit').css('border', '1px solid red');
       } if($(cancel).parents('.input-fields').find('#subscription_description').val()==''){
         $(cancel).parents('.input-fields').find('#subscription_description').css('border', '1px solid red');
       }
   }
}



function delete_subscription(cancel)
{ 
   $(cancel).parents('.input-fields').remove();
   sub_total();
}


function edit_sub(edit)
{ 
   $(".delete_subscription_div").hide();
   $(".cancel_subscription_div").show();
   $(".edit_beforesubscription_save").hide();
   $(".edit_aftersubscription_save").show();

   var place = $(edit).parents('.top-field-heads').find('.edit_subscription').attr('class');
   place = place.replace('edit_subscription','');  
   place = place.replace('red-grcolor','');    
   place = place.trim();

  if($(edit).parents('.remove-content').hasClass('saved_subscription_contents remove-content'))
  {
     var index = Number($( ".saved_subscription_contents .edit_subscription" ).index( edit ));
     var nex_index=Number(index) + Number(1);
      
     $('.subscription-contents .add-subscription').each(function()
     { 
        if($(this).hasClass(place))
        {
           if(($(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == true && $(this).find('[name="subscription_optional[]"]').prop('checked') == false) || $(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == false && $(this).find('[name="subscription_optional[]"]').prop('checked') == true)
           {
              $(this).find('[name="subscription_optional[]"]').trigger('click');

              if($(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == true)
              {
                 $(this).find('[name="subscription_optional[]"]').attr('checked','checked');
              }
              else
              {
                 $(this).find('[name="subscription_optional[]"]').removeAttr('checked');
              }
           }
           $(this).show();
        }
     });

     $(edit).parents('.remove-content').find('.top-field-heads:nth-child('+nex_index+')').hide();
    
  }
  else if($(edit).parents('.remove-content').hasClass('saved_subscriptions_contents remove-content'))
  {    

    $('.extra-add-subscription .add-subscriptions').each(function()
    { 
       if($(this).hasClass(place))
       {
          if(($(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == true && $(this).find('[name="subscription_optional[]"]').prop('checked') == false) || $(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == false && $(this).find('[name="subscription_optional[]"]').prop('checked') == true)
          {
             $(this).find('[name="subscription_optional[]"]').trigger('click');

             if($(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == true)
             {
                $(this).find('[name="subscription_optional[]"]').attr('checked','checked');
             }
             else
             {
                $(this).find('[name="subscription_optional[]"]').removeAttr('checked');
             }
          }
          $(this).show();
       }
    });  
    
    $(edit).parents('.remove-content').find('.top-field-heads').hide();  
  }
}
function cancel_sub(cancelled)
{
  var index=$('.top-field-heads .cancel_subscription').index( cancelled );
  var nex_index=Number(index) + Number(1);

  if($(cancelled).parents('.remove-content').attr('class')=='saved_subscription_contents remove-content')
  {
   $('.saved_subscription_contents .top-field-heads:nth-child('+nex_index+')').remove();


   <?php 
   if(isset($records['subscription_name']) && $records['subscription_name']!=''){ ?>
   $('.subscription-contents .input-fields:nth-child('+nex_index+')').remove();
   <?php }else{ ?>
      if(localStorage.getItem("price2")=='subscription'){
       $('.subscription-contents .input-fields:nth-child('+nex_index+')').remove();
      }else{
      $('.subscription-contents .input-fields:nth-child('+nex_index+')').html('');
      }
    <?php } ?>
  }
  else
  {
    var index=$('.top-field-heads .extra_cancel_subscription').index( cancelled );
    var nex_index=Number(index);

    if(nex_index == 0)
    {
       nex_index = 1;
    }

    $('.extra-saved-subscription-content .saved_subscriptions_contents:nth-child('+nex_index+')').remove();
    $('.extra-add-subscription .input-fields:nth-child('+nex_index+')').remove();
  }
    // $(cancelled).children('.input-fields').find('.top-field-heads').remove();
    // $(cancelled).children('.input-fields').find('.bottom-field-heads').remove();
    // if($(cancelled).parents('.remove-content').attr('class')=='saved_subscription_contents remove-content'){
    //  $(cancelled).parents('.top-field-heads').hide(); 
    // }else{
    //    $(cancelled).parents('.remove-content').find('.top-field-heads').remove();
    // } 
    sub_total(); 
}
// $("#company_name").change(function(){
//  // alert('change');

// });


function save_to_catalog(service)
{  
    $('#ser_er').html('');
    var service_name = $(service).parents('.input-fields').find('#item_name').val();
    $("#service_category_popup").modal('show');

    if(service_name != '')
    {         
          var service_price=$(service).parents('.input-fields').find('#price').val();
          var service_unit=$(service).parents('.input-fields').find('#unit').val();
          var service_qty=$(service).parents('.input-fields').find('#qty').val();
          var service_description=$(service).parents('.input-fields').find('#description').val();
          var service_category='';       

          $("#service_category_save").click(function()
          {
               var service_category = $("#service_categories_name").val();

               if(service_name != '' && service_category!="")
               {
                   $('#ser_er').html('');
                   $.ajax(
                   {
                       url: '<?php echo base_url();?>proposal/service_check',
                       type : 'POST',
                       data : {'service_name':service_name,'service_category':service_category},                   
                       beforeSend: function() {
                       $(".LoadingImage").show();
                       },
                       success: function(data) 
                       {  
                          //alert(data); 
                          $(".LoadingImage").hide();  
                            var json = JSON.parse(data); 
                            status=json['status'];
                            if(status=='0')
                            { 
                                 $('#ser_er').html('');
                                  $.ajax({
                                    url: '<?php echo base_url();?>proposals/service_category_update/',
                                    type : 'POST',
                                    data : {'service_name':service_name,'service_price':service_price,'service_unit':service_unit,'service_qty':service_qty,'service_description':service_description,'service_category':service_category},                    
                                    beforeSend: function() {
                                       $(".LoadingImage").show();
                                    },
                                    success: function(data) {
                                    //  alert(data);
                                       $(".LoadingImage").hide();             
                                       var json = JSON.parse(data);             
                                       status=json['status'];

                                       if(status!='0')
                                       { 
                                          $('#ser_er').html('Service Added Successfully.').css('color','green'); 
                                          setTimeout(function(){$('#service_category_popup .close').trigger('click');},2000);
                                       }                
                                    }
                                });
                           }
                           else
                           {
                              $('#ser_er').html('Service Name Already Exists.').css('color','red');
                           }
                       }       

                  });
               }
               else
               {       
                   $('#ser_er').html('Service Name Required.').css('color','red');       
               }                
        });
    }
    else
    {       
        $('#ser_er').html('Service Name Required.').css('color','red');       
    }
 }

function pro_save_to_catalog(product)
{
    $('#product_er').html('');
    var product_name = $(product).parents('.input-fields').find('#product_name').val();
    $("#product_category_popup").modal('show');

    if(product_name != '')
    {          
          var product_price=$(product).parents('.input-fields').find('#product_price').val();
          var product_description=$(product).parents('.input-fields').find('#product_description').val();
          var product_category='';      

      $("#product_category_save").click(function()
      {     
           var product_category=$("#product_categories_name").val();
           
           if(product_name != "" && product_category!="")
           {
               $('#product_er').html('');

               $.ajax(
               {
                   url: '<?php echo base_url();?>proposal/product_check',
                   type : 'POST',
                   data : {'product_name':product_name,'product_category':product_category},                   
                   beforeSend: function() {
                    $(".LoadingImage").show();
                   },
                   success: function(data) 
                   {
                     $(".LoadingImage").hide();  
                     var json = JSON.parse(data); 
                     status=json['status'];

                     if(status=='0')
                     { 
                         $('#product_er').html('');
                        var formData={'product_name':product_name,'product_price':product_price,'product_description':product_description,'product_category':product_category};

                        $.ajax(
                        {
                            url: '<?php echo base_url();?>proposals/product_category_update/',
                            type : 'POST',
                            data : formData,                    
                            beforeSend: function() {
                               $(".LoadingImage").show();
                            },
                            success: function(data) {
                            // alert(data);
                               $(".LoadingImage").hide();             
                               var json = JSON.parse(data);             
                               status=json['status'];
                               if(status!='0')
                               {
                                  $('#product_er').html('Product Added Successfully.').css('color','green'); 
                                  setTimeout(function(){$('#product_category_popup .close').trigger('click');},2000);           
                               }                
                            }
                        });
                      }
                      else
                      {
                         $('#product_er').html('Product Name Already Exists.').css('color','red');
                      }
                    }
                 });
             }
             else
             {      
               $('#product_er').html('Product Name Required.').css('color','red');
             }          
      });

    }
    else
    {      
      $('#product_er').html('Product Name Required.').css('color','red');
    }
}

function sub_save_to_catalog(sub)
{
   $('#sub_er').html('');
   var subscription_name = $(sub).parents('.input-fields').find('#subscription_name').val();
   $("#subscription_category_popup").modal('show');

   if(subscription_name != '')
   {  
      var subscription_price=$(sub).parents('.input-fields').find('#subscription_price').val();
      var subscription_unit=$(sub).parents('.input-fields').find('#subscription_unit').val();
      var subscription_description=$(sub).parents('.input-fields').find('#subscription_description').val();
      var subscription_category='';

        $("#subscription_category_save").click(function()
        {
           var subscription_category=$("#subscription_categories_name").val();
           
           if(subscription_name != '' && subscription_category!="")
           {
                $('#sub_er').html('');
                $.ajax(
                {
                  url: '<?php echo base_url();?>proposal/subscription_check',
                  type : 'POST',
                  data : {'subscription_name':subscription_name,'subscription_category':subscription_category},                   
                  beforeSend: function() {
                   $(".LoadingImage").show();
                  },
                  success: function(data) 
                  {                   
                    $(".LoadingImage").hide();  
                    var json = JSON.parse(data); 
                    status=json['status'];
                    if(status=='0')
                    { 
                        $('#sub_er').html('');
                        var formData={'subscription_name':subscription_name,'subscription_price':subscription_price,'subscription_unit':subscription_unit,'subscription_description':subscription_description,'subscription_category':subscription_category};

                        $.ajax(
                        {
                              url: '<?php echo base_url();?>proposals/subscription_category_update/',
                              type : 'POST',
                              data : formData,                    
                              beforeSend: function() {
                                 $(".LoadingImage").show();
                              },
                              success: function(data) {
                           //    alert(data);
                                 $(".LoadingImage").hide();             
                                 var json = JSON.parse(data);             
                                 status=json['status'];
                                 if(status!='0')
                                 {
                                    $('#sub_er').html('Subscription Added Successfully.').css('color','green'); 
                                    setTimeout(function(){$('#subscription_category_popup .close').trigger('click');},2000);         
                                 }                
                              }
                        });
                     }
                     else
                     {
                        $('#sub_er').html('Subscription Name Already Exists.').css('color','red');
                     }
                   }
                });
             }
             else
             {      
               $('#sub_er').html('Subscription Name Required.').css('color','red');
             }
        });
    }
    else
    {      
      $('#sub_er').html('Subscription Name Required.').css('color','red');
    }
}

$('.tab-used').click(function(){
 if($(this).attr('href')=='#all'){
  $('#all').show();
}
$('.service-lists').hide();
$('.tab-used').show();
});


$('.tab-used1').click(function(){
 if($(this).attr('href')=='#product_all'){
  $('#product_all').show();
}
$('.product-lists').hide();
$('.tab-used1').show();
});

$('.tab-used2').click(function(){
 if($(this).attr('href')=='#subscription_all'){
  $('#subscription_all').show();
}
$('.subscription_lists').hide();
$('.tab-used2').show();
});

function content(content){
  //alert('sdsfds');
  //alert($(content).attr('class'));
$(content).find('.price').attr("contentEditable", true);
}

function header_section(){
$('.header_section').attr("contentEditable", true);
}

function footer_section(){
$('.footer_section').attr("contentEditable", true);
}


$("#other").click(function(event) 
{
  event.preventDefault();
  var proposal_mail=$("#body").html();  
  $("#proposal_mail").html(proposal_mail);
  //var proposal_subject=$("#mail_subject").next("p").html();
  //$("#subject").html(proposal_subject);
 //  console.log($("#proposal_mail").val());
  $('#body p').each(function() 
  {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
    $this.remove();
  });
  var check_content=$("#body").html();
//console.log(check_content);
  
  if(check_content=='<p><br></p>')
  {
    //  console.log($("#proposal_mail").val());
    $("#Send_proposal").modal('hide');
    $("#Send_proposal").modal('hide');
    $('.alert-warning').show();
  }
  else
  {   
    $("#login_form1").submit();
  }
  
});

<?php if($this->uri->segment(2) == 'copy_proposal'){ ?>
   
  $(document).on('click','#sent',function()
  {  
     $('.ptag_invoice_msg').html('');
     $('#Send_proposal .modal-footer').show();
     $('.first-para').show();
     var exist_total = $('.grand_total').text();

     if(qty_check() == true && invoice_change_shown == '' && Number($('.grand_total').text()).toFixed(2) != Number(exist_total).toFixed(2))
     { 
        $('.addnewclient_pages1').find('a[href="#price_tab"]').trigger('click');  
        $('#draft').hide();
        $('.Done').hide();
        $('#Send_proposal .modal-footer').hide();
        $('.first-para').hide();
        $(".Next_Previous").show();        

        if($('.ptag_invoice_msg').html() == undefined)
        {
           $('#Send_proposal .modal-body').append('<p class="ptag_invoice_msg">Grand Total Will Be : '+$('.grand_total').text()+' '+$("#currency_symbol").val()+'.</p>');
        }
        else
        {
           $('.ptag_invoice_msg').html('Grand Total Will Be : '+$('.grand_total').text()+' '+$("#currency_symbol").val()+'.');
        }
        
        invoice_change_shown = true;                
     }  
  });

<?php } ?>

$('ol.nav li:nth-child(3)').click(function(){
$( ".choose:first" ).trigger( "click" );
});


$('#email_signature').on('change', function(e){
   if(e.target.checked){    
     $('#EmailSignature').modal();
   }
});


$('#general_pdf').on('change', function(e){
if(e.target.checked){ 
 $("#pdf_download_link").show();
 // $("#pdf").slideToggle();
}else{
  $("#pdf_download_link").hide();
  // $("#pdf").slideToggle();
}
  });

$('#password_access').on('change', function(e){
if(e.target.checked){   
$("#password_section").slideDown();
}else{
 $("#password_section").slideUp();
}
});

$('#expiration_date_check').on('change', function(e){
if(e.target.checked){   
$("#expiration_date").slideToggle();
}else{
 $("#expiration_date").slideToggle();
}
});

 $( document ).ready(function() 
 {
    $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy',minDate:0 }).val();
   //$(document).on('change','.othercus',function(e){
    if($('.discount_amount').text() != '' && $('.discount_amount').text() > '0')
    { 
       localStorage.setItem('discount_amount','on');
    }
    else
    {
       localStorage.setItem('discount_amount','');
    }
      $(function() 
      {
        $( "#tabs15" ).tabs();
      });
});
</script>

<div class="modal fade" id="Preview_proposal" role="dialog">
    <div class="modal-dialog modal-lg modal-proposal-preview">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Proposal Preview</h4>
        </div>
        <div class="modal-body" id="proposal_body_content">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<div class="modal fade" id="EmailSignature" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-e-signature">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                </button>
                 <h4 class="modal-title" id="myModalLabel">E-Signature</h4>

            </div>
            <div class="modal-body">
             <?php $this->load->view('proposal/my_sign.php'); ?>
            </div>
            <div class="modal-footer">
                <button id="btnSaveSign" class="btn btn-default">Save Signature</button>
                <button  id="clearSig" class="btn btn-primary save">Clear Signature</button>
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <!-- <button type="button" class="btn btn-primary save">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<script>
      // $(document).ready(function() {
      //   $('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
      // });

      // $("#clearSig").click(function clearSig() {

      // $('#signArea').signaturePad().clearCanvas ();
      // });
      
      // $("#btnSaveSign").click(function(e){
      //   html2canvas([document.getElementById('sign-pad')], {
      //     onrendered: function (canvas) {
      //       var canvas_img_data = canvas.toDataURL('image/png');
      //       var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
      //       //ajax call to save image inside folder
      //       $.ajax({
      //         url: '<?php echo base_url(); ?>/proposals/save_sign',
      //         data: { img_data:img_data },
      //         type: 'post',
      //         dataType: 'json',
      //         success: function (response) {            
      //           $("#imgData").html('Thank you! Your signature was saved');
      //           setTimeout(function(){ $("#EmailSignature").modal('hide'); }, 3000);
      //            //window.location.reload();
      //         }
      //       });
      //     }
      //   });
      // });


$( "input" ).focus(function() {
    $( this ).removeAttr( "style" );
});

$( "textarea" ).focus(function() {
    $( this ).removeAttr( "style" );
});


 $("#preview_proposal").click(function(){
//alert('ok');

//  console.log('okkkkk');

var proposal_contents_section=$("#body").html();

//console.log('proposal_content');
//console.log(proposal_contents_section);


$("#proposal_body_content").html('');
$("#proposal_body_content").html($("#proposal_contents_details").html());

 $('#Preview_proposal').modal('show'); 
 });

 $(".dropdown-sin-4").click(function()
 { 
    $(".dropdown-display").removeAttr( 'style' ); 

    if(($(".dropdown-sin-4.dropdown-single.active").length)=='0'){

    var crm_company_name=$("#company_name").val();
    
    $.ajax({
                url: '<?php echo base_url();?>proposals/company_details/',
                type : 'POST',
                data : {'crm_company_name':crm_company_name},                    
                beforeSend: function() {
                   $(".LoadingImage").show();
                },
                success: function(data) { 
                  $(".LoadingImage").hide();             
                  var json = JSON.parse(data);             
                  content=json['content'];
                  image=json['image'];
                  $('.client_details').html('');
                  $('.client_details').append(content);
                   $('.client_details2').html('');
                  $('.client_details2').append(content);
                  //   $(".linking").hide();
                  $('.profile_pics').html('');
                  $('.profile_pics').append(image);
                }
            });
    }
});



function company_changing(company){
var crm_company_name=$(company).val();
//console.log(crm_company_name);
  $.ajax({
              url: '<?php echo base_url();?>proposals/company_details/',
              type : 'POST',
              data : {'crm_company_name':crm_company_name},                    
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data) {
                $(".LoadingImage").hide();             
                var json = JSON.parse(data);             
                content=json['content'];
                image=json['image'];
                $('.client_details').html('');
                $('.client_details').append(content);
                 $('.client_details2').html('');
                $('.client_details2').append(content);
                //   $(".linking").hide();
                $('.profile_pics').html('');
                $('.profile_pics').append(image);
              }
          });
}

$(document).ready(function(e) {
$("#sender_details").click(function(e){
  //alert('clivk');
var sender_company_name=$("#sender_company_name").val();
var sender_company_mailid=$("#sender_company_mailid").val();
if ($.trim(sender_company_mailid).length == 0) {
alert('All fields are mandatory');
e.preventDefault();
}
if (validateEmail(sender_company_mailid)) {
 $.ajax({
            url: '<?php echo base_url();?>proposal_pdf/sender_company_details/',
            type : 'POST',
            data : {'sender_company_name':sender_company_name,'sender_company_mailid':sender_company_mailid},                   
              beforeSend: function() {
                $(".LoadingImage").show();
              },
              success: function(data) { 
             //  alert(data);
                $(".LoadingImage").hide();
                var json = JSON.parse(data); 
                 content=json['content'];
                $(".sender_details_section").html('');
                 $(".sender_details_section").append(content);
                 $(".send_to_name").html('');
                 $(".send_to_name").append(sender_company_name);
               $("#Company_details").modal('hide');

              }
          });
}
else {
  $("#sender_company_mailid").css('border','1px solid red');
//alert('Invalid Email Address');
e.preventDefault();
}

});

});

function validateEmail(sEmail) {
var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
if (filter.test(sEmail)) {
return true;
}
else {
return false;
}
}


    $('.decimal_qty').keyup(function(evt){
      var text = $(this).val();
      var test_value = text.replace(/[^0-9]+/g, "");
      $(this).val(test_value);
    });

   $('.decimal').keyup(function(){
  //  console.log('#####');
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});


    $(document).on('keyup','.decimal', function(event) {
   //    console.log('#####1478');
      var val = $(this).val();
      var maxLength = 4;
   //   console.log(val.length);
      var text = $(this).val();
     
      if(val.length <= maxLength){
    //    console.log('#####***');
      var test_value = text.replace();
      $(this).val(test_value);
      } 
      

      if(isNaN(val)){
           val = val.replace(/[^0-9\.]/g,'');
           if(val.split('.').length>2) 
               val =val.replace(/\.+$/,"");
      }
      $(this).val(val); 
    });

// $("#sender_company_mailid").keyup(function(){
//  $("#sender_company_mailid").css('border','1px gray');
// });
$(".done-proposal").click(function(event){
  //alert($(this).attr('id'));
 $("#status").val('draft');   
 event.preventDefault();
  var proposal_mail=$("#body").html();  
  $("#proposal_mail").html(proposal_mail);
  //var proposal_subject=$("#mail_subject").next("p").html();
  //$("#subject").html(proposal_subject);
 //  console.log($("#proposal_mail").val());
    $('#body p').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
    $this.remove();
    });
  var check_content=$("#body").html();
  if(check_content=='<p><br></p>'){
 //   console.log($("#proposal_mail").val());
    $("#Send_proposal").modal('hide');
      $("#Send_proposal").modal('hide');
    $('.alert-warning').show();
  }else{
    $("#login_form1").submit();
  }
});

$(".blank_service").click(function()
{
    var length=$(".add-service").length;    
    var lengths=$(".add-services").length;
    var sc=length + lengths;
    var count=sc + 1; 

    var content='   <div class="top-field-heads "><div class="proposal-fields1 service-items"> <input name="item_name[]" id="item_name" class="required item_name" value="" readonly>  <input type="hidden" name="service_category_name[]" id="service_category_name" class="service_category_name" value=""></div><div class="proposal-fields2 price-services service-items"><input type="hidden" placeholder="" name="original_price[]" id="original_price" class="original_price" onchange="sub_total()" value="125"><input type="text" placeholder="" name="price[]" id="price" class="decimal required service_price  serviceprice price" maxLength="6" onchange="sub_total()" value=""><span>/</span><select name="unit[]" id="unit" class="required unit"><option value="per_service">Per Service</option><option value="per_day">Per Day</option> <option value="per_month">Per Month</option><option value="per_2month">Per 2Months</option><option value="per_6month">Per 6Month</option> <option value="per_year">Per Year</option></select></div>  <div class="proposal-fields3 service-items"><input onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"  type="text" name="qty[]" id="qty" class="decimal_qty decimal required qty" value="" onchange="qty_check()"></div> <div class="proposal-fields5 service-items tax-field"><input type="text" name="service_tax[]" id="service_tax" class="decimal tax" maxLength="4" onchange="sub_total()" value="0"> </div> <div class="proposal-fields6 service-items discount-field"> <input type="text" name="service_discount[]" id="service_discount" class="decimal discount" maxLength="4" onchange="sub_total()" value="0"></div>  </div> <div class="bottom-field-heads"> <textarea rows="3" name="description[]" id="description" class="required description"></textarea><div class="mark-optional"> <div class="mark-left-optional pull-left border-checkbox-section"><div class="checkbox-color checkbox-primary">  <input class="border-checkbox optional" type="checkbox" id="checkbox1" name="service_optional[]" value="'+ count +'" ><label class="border-checkbox-label optional-label" for="checkbox1">mark as optional</label>  </div><div class="checkbox-color checkbox-primary">  <input class="border-checkbox quantity" type="checkbox" id="checkbox2" name="service_quantity[]" value="'+ count +'"> <label class="border-checkbox-label quantity-label" for="checkbox2">quantity  editables</label> </div> </div><div class="pull-right mark-right-optional"> <a  class="sve-action1 edit_before_save" id="service_save" onclick="saved(this)">save</a><a  class="sve-action1 edit_after_save" id="service_save" onclick="saved_before(this)" style="display:none">save</a> <a class="sve-action2" id="save_to_catalog" onclick="save_to_catalog(this)">save to package</a> <div class="delete_service_div buttons_hide"> <a class="sve-action2 service_close" id="service_close" onclick="deleted(this)"> Delete</a>  </div> <div class="cancel_service_div buttons_hide" style="display: none">  <a class="sve-action2 service_cancel" id="service_close" onclick="cancelled(this)"> Cancel</a> </div></div> </div> </div>';
    var length=$(".add-service").length;    
    var lengths=$(".add-services").length; 
    if(length=='0'){
      $(".contents").append('<div class="add-service input-fields">'+content+'</div>');

      if ($('.service-table').hasClass('discount')) {
            $(".proposal-fields6").show();
            }

            if ($('.service-table').hasClass('tax')) {
            $(".proposal-fields5").show();
            }

              var minNumber = 101; // le minimum
          var maxNumber = 200; // le maximum
          var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
        //  console.log(randomnumber);
          var newran=Math.floor(randomnumber -1 );
         // console.log(newran);
          $(".add-service .quantity").last().attr('id','checkbox'+randomnumber);
          $(".add-service .quantity-label").last().attr('for','checkbox'+randomnumber);
          $(".add-service .optional").last().attr('id','checkbox'+newran);
          $(".add-service .optional-label").last().attr('for','checkbox'+newran);

    }else{

        $('.add-services').find(':input,:radio,:checkbox').addClass('required');
        $('#service_category_name').removeClass('required');
        $('.service_category_name').removeClass('required');
        $(".original_price").removeClass('required');

       $(".extra-add-service").append('<div class="add-services input-fields">'+content+'</div>');
       if ($('.service-table').hasClass('discount')) {
            $(".proposal-fields6").show();
            }

            if ($('.service-table').hasClass('tax')) {
            $(".proposal-fields5").show();
            }

              var minNumber = 1001; // le minimum
          var maxNumber = 2000; // le maximum
          var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
        //  console.log(randomnumber);
          var newran=Math.floor(randomnumber -1 );
         // console.log(newran);
          $(".add-services .quantity").last().attr('id','checkbox'+randomnumber);
          $(".add-services .quantity-label").last().attr('for','checkbox'+randomnumber);
          $(".add-services .optional").last().attr('id','checkbox'+newran);
          $(".add-services .optional-label").last().attr('for','checkbox'+newran);

    }

    if($('#select-category').hasClass('show')) 
    {
      $("#select-category").modal('hide');
    }

});

$(".blank_product").click(function(){

   var length=$(".add-product").length;    
    var lengths=$(".add-products").length; 
    var sc=length + lengths;
   // console.log(sc);
    var count=sc + 1; 
  
 var content='  <div class="top-field-heads">   <div class="proposal-fields1 service-items"> <input type="text" name="product_name[]" id="product_name" class="product_name required" value="">  <input type="hidden" name="product_category_name[]" id="product_category_name" class="product_category_name" value=""> </div>  <div class="proposal-fields2 price-services service-items"> <input type="hidden" placeholder="" name="original_product_price[]" id="original_product_price" class="decimal original_product_price original_price" onchange="sub_total()" value="10">   <input type="text" placeholder="" name="product_price[]" maxlength="6" id="product_price" class="decimal product_price price required" onchange="sub_total()" value=""> </div> <div class="proposal-fields3 service-items">  <input type="text" name="product_qty[]" maxlength="4" id="product_qty" class="decimal decimal_qty product_qty required" value="" onchange="qty_check()"> </div> <div class="proposal-fields5 service-items tax-field"> <input type="text" name="product_tax[]" id="product_tax" maxlength="4" class="decimal product_tax" value="0" onchange="sub_total()"> </div>  <div class="proposal-fields6 service-items discount-field"> <input type="text" name="product_discount[]" id="product_discount" maxlength="4" class="decimal product_discount" value="0" onchange="sub_total()">  </div>  </div> <div class="bottom-field-heads"> <textarea rows="3" name="product_description[]" id="product_description" class="product_description required"></textarea> <div class="mark-optional">  <div class="mark-left-optional pull-left border-checkbox-section"> <div class="checkbox-color checkbox-primary">  <input class="border-checkbox product_optional required" type="checkbox" id="checkbox3" name="product_optional[]" value="'+ count +'">  <label class="border-checkbox-label optional-label" for="checkbox3">mark as optional</label> </div> <div class="checkbox-color checkbox-primary">  <input class="border-checkbox product_quantity required" type="checkbox" id="checkbox4" name="product_quantity[]" value="'+ count +'">  <label class="border-checkbox-label quantity-label" for="checkbox4">quantity  editables</label>  </div> </div> <div class="pull-right mark-right-optional">   <a class="sve-action1 edit_beforeproduct_save" id="product_save" onclick="saved_product(this)">save</a><a class="sve-action1 edit_afterproduct_save" id="product_save" onclick="savedbefore_product(this)" style="display:none;">save</a> <a class="sve-action2" id="product_save_to_catalog" onclick="pro_save_to_catalog(this)">save to package</a>  <div class="delete_product_div buttons_hide"> <a class="sve-action2 hgfghfgh product_close" id="product_close" onclick="deleted_product(this)">Delete</a></div>  <div class="cancel_product_div cancel-comdiv" style="display:none"> <a class="sve-action2 hgfghfgh product_cancel" id="product_close" onclick="cancelled_product(this)">Cancel</a></div> </div> </div> </div>';
    var length=$(".add-product").length;    
    var lengths=$(".add-products").length; 
   // alert(length);
  //  alert(lengths);
    if(length=='0'){      
      $(".product-contents").append('<div class="add-product input-fields">'+content+'</div>');

      if ($('.service-table').hasClass('discount')) {
            $(".proposal-fields6").show();
            }

            if ($('.service-table').hasClass('tax')) {
            $(".proposal-fields5").show();
            }

              var minNumber = 2001; // le minimum
          var maxNumber = 3000; // le maximum
          var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
        //  console.log(randomnumber);
          var newran=Math.floor(randomnumber -1 );
         // console.log(newran);
          $(".add-product .product_quantity").last().attr('id','checkbox'+randomnumber);
          $(".add-product .quantity-label").last().attr('for','checkbox'+randomnumber);
          $(".add-product .product_optional").last().attr('id','checkbox'+newran);
          $(".add-product .optional-label").last().attr('for','checkbox'+newran);


    }else if(length=='1' && $("#product_name").val()==''){
        $(".add-product").show();
    }else{

       $('.add-products').find(':input,:radio,:checkbox').addClass('required');
        $('#product_category_name').removeClass('required');

  $('.product_category_name').removeClass('required');
       $(".extra-add-products").append('<div class="add-products input-fields">'+content+'</div>');

       if ($('.service-table').hasClass('discount')) {
            $(".proposal-fields6").show();
            }

            if ($('.service-table').hasClass('tax')) {
            $(".proposal-fields5").show();
            }

               var minNumber = 3001; // le minimum
          var maxNumber = 4000; // le maximum
          var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
        //  console.log(randomnumber);
          var newran=Math.floor(randomnumber -1 );
         // console.log(newran);
          $(".add-products .product_quantity").last().attr('id','checkbox'+randomnumber);
          $(".add-products .quantity-label").last().attr('for','checkbox'+randomnumber);
          $(".add-products .product_optional").last().attr('id','checkbox'+newran);
          $(".add-products .optional-label").last().attr('for','checkbox'+newran);

    }

     if($('#select-products').hasClass('show'))
     { 
         $("#select-products").modal('hide');
     }
 
});



$(".blank_subscription").click(function(){
   var length=$(".add-subscription").length;    
    var lengths=$(".add-subscriptions").length; 
    var sc=length + lengths;
  //  console.log(sc);
    var count=sc + 1; 
 
 var content='  <div class="top-field-heads"> <div class="proposal-fields1 service-items"> <input type="text" name="subscription_name[]" id="subscription_name" class="subscription_name required" value=""> <input type="hidden" name="subscription_category_name[]" id="subscription_category_name" class="subscription_category_name" value="">  </div> <div class="proposal-fields2 price-services service-items"> <input type="hidden" placeholder="" name="original_subscription_price[]" id="original_subscription_price" class="decimal original_subscription_price price original_price" onchange="sub_total()" value="0"> <input type="text" placeholder="" name="subscription_price[]" id="subscription_price" class="decimal subscription_price price required" maxlength="6" onchange="sub_total()" value=""> </div>  <div class="proposal-fields3 service-items">  <select name="subscription_unit[]" id="subscription_unit" class="subscription_unit required"><option value="per_hour">Per Hour</option><option value="per_day">Per Day</option> <option value="per_week">Per Week</option> <option value="per_month">Per Month</option>  <option value="per_2month">Per 2Month</option>  <option value="per_5month">Per 5month</option><option value="per_year">Per Year</option> </select></div> <div class="proposal-fields5 service-items tax-field"> <input type="text" name="subscription_tax[]" maxlength="4" id="subscription_tax" class="decimal subscription_tax" value="0" onchange="sub_total()" > </div><div class="proposal-fields6 service-items discount-field"><input type="text" name="subscription_discount[]" maxlength="4" id="subscription_discount" class="decimal subscription_discount" value="0" onchange="sub_total()">  </div> </div><div class="bottom-field-heads"><textarea rows="3" name="subscription_description[]" id="subscription_description" class="subscription_description required"></textarea><div class="mark-optional"><div class="mark-left-optional pull-left border-checkbox-section"> <div class="checkbox-color checkbox-primary"> <input class="border-checkbox subscription_optional optional required" type="checkbox" id="checkbox5" name="subscription_optional[]"  value="'+ count +'"><label class="border-checkbox-label optional-label" for="checkbox5">mark as optional</label> </div><div class="checkbox-color checkbox-primary"> <input class="border-checkbox subscription_quantity required" type="checkbox" id="checkbox6" name="subscription_quantity[]"  value="'+ count +'">  <label class="border-checkbox-label quantity-label" for="checkbox6">quantity  editables</label> </div> </div> <div class="pull-right mark-right-optional">  <a  class="sve-action1 edit_beforesubscription_save" id="subscription_save" onclick="saved_subscription(this)">save</a> <a  class="sve-action1 edit_aftersubscription_save" id="subscription_save" onclick="savedbefore_subscription(this)" style="display: none;">save</a> <a class="sve-action2" id="subscription_save_to_catalog" onclick="sub_save_to_catalog(this)">save to package</a> <div class="delete_subscription_div buttons_hide">  <a class="sve-action2 subscription_close" id="subscription_close" onclick="delete_subscription(this)">Delete</a></div> <div class="cancel_subscription_div cancel-comdiv" style="display:none">  <a class="sve-action2 subscription_cancel" id="subscription_close" onclick="cancelled_subscription(this)">Cancel</a></div> </div> </div> </div>';
    var length=$(".add-subscription").length;    
    var lengths=$(".add-subscriptions").length; 
   // alert(length);
   //  alert(lengths);
    if(length=='0'){
      $(".subscription-contents").append('<div class="add-subscription input-fields">'+content+'</div>');

      if ($('.service-table').hasClass('discount')) {
            $(".proposal-fields6").show();
            }

            if ($('.service-table').hasClass('tax')) {
            $(".proposal-fields5").show();
            }

               var minNumber = 5000; // le minimum
          var maxNumber = 7000; // le maximum
          var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
        //  console.log(randomnumber);
          var newran=Math.floor(randomnumber -1 );
         // console.log(newran);
          $(".add-subscription .subscription_quantity").last().attr('id','checkbox'+randomnumber);
          $(".add-subscription .quantity-label").last().attr('for','checkbox'+randomnumber);
          $(".add-subscription .subscription_optional").last().attr('id','checkbox'+newran);
          $(".add-subscription .optional-label").last().attr('for','checkbox'+newran);

    }else if(length=='1' && $("#subscription_name").val()==''){
        $(".add-subscription").show();
    }else{

         $('.add-subscriptions').find(':input,:radio,:checkbox').addClass('required');
        $('#subscription_category_name').removeClass('required');
$('.subscription_category_name').removeClass('required');
$(".original_price").removeClass('required');
       $(".extra-add-subscription").append('<div class="add-subscriptions input-fields">'+content+'</div>');


       if ($('.service-table').hasClass('discount')) {
            $(".proposal-fields6").show();
            }

            if ($('.service-table').hasClass('tax')) {
            $(".proposal-fields5").show();
            }

             var minNumber = 8000; // le minimum
          var maxNumber = 10000; // le maximum
          var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
        //  console.log(randomnumber);
          var newran=Math.floor(randomnumber -1 );
         // console.log(newran);
          $(".add-subscriptions .subscription_quantity").last().attr('id','checkbox'+randomnumber);
          $(".add-subscriptions .quantity-label").last().attr('for','checkbox'+randomnumber);
          $(".add-subscriptions .subscription_optional").last().attr('id','checkbox'+newran);
          $(".add-subscriptions .optional-label").last().attr('for','checkbox'+newran);

    }
     if($('#select-subscriptions').hasClass('show'))
     {
         $("#select-subscriptions").modal('hide');
     }

});


function saved_before(save)
{       
   if($(save).parents('.input-fields').find('#item_name').val()!='' && $(save).parents('.input-fields').find('#price').val()!='' && $(save).parents('.input-fields').find('#unit').val()!='' && $(save).parents('.input-fields').find('#qty').val()!='' &&  $(save).parents('.input-fields').find('#description').val()!=''  )
   {
      $(save).parents('.input-fields').hide();
      var place = $(save).parents('.input-fields').attr('class');    

      if($(save).parents('.input-fields').hasClass('add-service input-fields'))
      {         
         place = place.replace('add-service','');
         place = place.replace('input-fields','');
         place = place.trim(); 

         var obj = $('.saved_service_contents .'+place).parents('.top-field-heads');
         $(obj).find('.saved_item_name').html($(save).parents('.input-fields').find('#item_name').val());
         $(obj).find('.saved_item_description').html($(save).parents('.input-fields').find('#description').val());   
         $(obj).find('.saved_item_price').html($(save).parents('.input-fields').find('#price').val());  
         $(obj).find('.saved_item_unit').html($(save).parents('.input-fields').find('#unit').val());
         $(obj).find('.saved_item_qty').html($(save).parents('.input-fields').find('#qty').val());
         $(obj).find('.saved_item_tax').html($(save).parents('.input-fields').find('#service_tax').val());
         $(obj).find('.saved_item_discount').html($(save).parents('.input-fields').find('#service_discount').val());

         if(($(save).parents('.input-fields').find('[name="service_optional[]"]').prop('checked') == true && $(obj).find('.border-saved').prop('checked') == false) || ($(save).parents('.input-fields').find('[name="service_optional[]"]').prop('checked') == false && $(obj).find('.border-saved').prop('checked') == true))
         {
             $(obj).find('.border-saved').trigger('click');

             if($(save).parents('.input-fields').find('[name="service_optional[]"]').prop('checked') == false)
             {
                $(obj).find('.border-saved').removeAttr('checked');
             }            
         }

         $('.saved_service_contents').show(); 
         $(obj).show();
         
      }
      else if($(save).parents('.input-fields').hasClass('add-services input-fields'))
      {         
         place = place.replace('add-services','');
         place = place.replace('input-fields','');
         place = place.trim();      

         var obj = $('.saved_services_contents .'+place).parents('.top-field-heads');

         $(obj).find('.saved_item_name').html($(save).parents('.input-fields').find('#item_name').val());
         $(obj).find('.saved_item_description').html($(save).parents('.input-fields').find('#description').val());   
         $(obj).find('.saved_item_price').html($(save).parents('.input-fields').find('#price').val());  
         $(obj).find('.saved_item_unit').html($(save).parents('.input-fields').find('#unit').val());
         $(obj).find('.saved_item_qty').html($(save).parents('.input-fields').find('#qty').val());
         $(obj).find('.saved_item_tax').html($(save).parents('.input-fields').find('#service_tax').val());
         $(obj).find('.saved_item_discount').html($(save).parents('.input-fields').find('#service_discount').val());

         if(($(save).parents('.input-fields').find('[name="service_optional[]"]').prop('checked') == true && $(obj).find('.border-saved').prop('checked') == false) || ($(save).parents('.input-fields').find('[name="service_optional[]"]').prop('checked') == false && $(obj).find('.border-saved').prop('checked') == true))
         {
             $(obj).find('.border-saved').trigger('click');

             if($(save).parents('.input-fields').find('[name="service_optional[]"]').prop('checked') == false)
             {
                $(obj).find('.border-saved').removeAttr('checked');
             }            
         }

         $('.saved_services_contents').show(); 
         $(obj).show();
      }     
   }
   else
   {
       if($(save).parents('.input-fields').find('#item_name').val()==''){
          $(save).parents('.input-fields').find('#item_name').css('border', '1px solid red');
       } if($(save).parents('.input-fields').find('#price').val()==''){
          $(save).parents('.input-fields').find('#price').css('border', '1px solid red');
       } if($(save).parents('.input-fields').find('#unit').val()==''){
         $(save).parents('.input-fields').find('#unit').css('border', '1px solid red');
       } if($(save).parents('.input-fields').find('#qty').val()==''){
         $(save).parents('.input-fields').find('#qty').css('border', '1px solid red');
       }  if($(save).parents('.input-fields').find('#description').val()==''){
         $(save).parents('.input-fields').find('#description').css('border', '1px solid red');
       }
   }
}

function savedbefore_product(save)
{
  if($(save).parents('.input-fields').find('#product_name').val()!='' && $(save).parents('.input-fields').find('#product_price').val()!='' && $(save).parents('.input-fields').find('#product_qty').val()!='' && $(save).parents('.input-fields').find('#product_description').val()!='')
  {   
      $(save).parents('.input-fields').hide(); 
      var place = $(save).parents('.input-fields').attr('class');   

      if($(save).parents('.input-fields').hasClass('add-product input-fields'))
      {         
         place = place.replace('add-product','');
         place = place.replace('input-fields','');
         place = place.trim(); 

         var obj = $('.saved_product_contents .'+place).parents('.top-field-heads');
         $(obj).find('.saved_product_name').html($(save).parents('.input-fields').find('#product_name').val());
         $(obj).find('.saved_product_price').html($(save).parents('.input-fields').find('#product_price').val());   
         $(obj).find('.saved_product_qty').html($(save).parents('.input-fields').find('#product_qty').val());  
         $(obj).find('.saved_product_description').html($(save).parents('.input-fields').find('#product_description').val());
         $(obj).find('.saved_product_tax').html($(save).parents('.input-fields').find('#product_tax').val());
         $(obj).find('.saved_product_discount').html($(save).parents('.input-fields').find('#product_discount').val());
         
         if(($(save).parents('.input-fields').find('[name="product_optional[]"]').prop('checked') == true && $(obj).find('.border-saved').prop('checked') == false) || ($(save).parents('.input-fields').find('[name="product_optional[]"]').prop('checked') == false && $(obj).find('.border-saved').prop('checked') == true))
         {
             $(obj).find('.border-saved').trigger('click');

             if($(save).parents('.input-fields').find('[name="product_optional[]"]').prop('checked') == false)
             {
                $(obj).find('.border-saved').removeAttr('checked');
             }
         }
         $('.saved_product_contents').show(); 
         $(obj).show();
         
      }
      else if($(save).parents('.input-fields').hasClass('add-products input-fields'))
      {         
         place = place.replace('add-products','');
         place = place.replace('input-fields','');
         place = place.trim();      

         var obj = $('.saved_products_contents .'+place).parents('.top-field-heads');

         $(obj).find('.saved_product_name').html($(save).parents('.input-fields').find('#product_name').val());
         $(obj).find('.saved_product_price').html($(save).parents('.input-fields').find('#product_price').val());   
         $(obj).find('.saved_product_qty').html($(save).parents('.input-fields').find('#product_qty').val());  
         $(obj).find('.saved_product_description').html($(save).parents('.input-fields').find('#product_description').val());
         $(obj).find('.saved_product_tax').html($(save).parents('.input-fields').find('#product_tax').val());
         $(obj).find('.saved_product_discount').html($(save).parents('.input-fields').find('#product_discount').val());
         
         if(($(save).parents('.input-fields').find('[name="product_optional[]"]').prop('checked') == true && $(obj).find('.border-saved').prop('checked') == false) || ($(save).parents('.input-fields').find('[name="product_optional[]"]').prop('checked') == false && $(obj).find('.border-saved').prop('checked') == true))
         { 
             $(obj).find('.border-saved').trigger('click');

             if($(save).parents('.input-fields').find('[name="product_optional[]"]').prop('checked') == false)
             {
                $(obj).find('.border-saved').removeAttr('checked');
             }            
         }

         $('.saved_products_contents').show(); 
         $('.saved_products_contents .'+place).parents('.top-field-heads').show();
      }     
  }
  else
  {     
      if($(save).parents('.input-fields').find('#product_name').val()==''){
        $(save).parents('.input-fields').find('#product_name').css('border', '1px solid red');
     } if($(save).parents('.input-fields').find('#product_price').val()==''){
        $(save).parents('.input-fields').find('#product_price').css('border', '1px solid red');
     } if($(save).parents('.input-fields').find('#product_qty').val()==''){
       $(save).parents('.input-fields').find('#product_qty').css('border', '1px solid red');
     } if($(save).parents('.input-fields').find('#product_description').val()==''){
       $(save).parents('.input-fields').find('#product_description').css('border', '1px solid red');
     }
  }
}



function savedbefore_subscription(save)
{
   if($(save).parents('.input-fields').find('#subscription_name').val()!='' && $(save).parents('.input-fields').find('#subscription_price').val()!='' && $(save).parents('.input-fields').find('#subscription_unit').val()!='' && $(save).parents('.input-fields').find('#subscription_description').val()!='')
   {   
       $(save).parents('.input-fields').hide(); 
       var place = $(save).parents('.input-fields').attr('class');   
 
       if($(save).parents('.input-fields').hasClass('add-subscription input-fields'))
       {         
          place = place.replace('add-subscription','');
          place = place.replace('input-fields','');
          place = place.trim(); 

          var obj = $('.saved_subscription_contents .'+place).parents('.top-field-heads');

          $(obj).find('.saved_subscription_name').html($(save).parents('.input-fields').find('#subscription_name').val());
          $(obj).find('.saved_subscription_price').html($(save).parents('.input-fields').find('#subscription_price').val());   
          $(obj).find('.saved_subscription_unit').html($(save).parents('.input-fields').find('#subscription_unit').val());  
          $(obj).find('.saved_subscription_description').html($(save).parents('.input-fields').find('#subscription_description').val());
          $(obj).find('.saved_subscription_tax').html($(save).parents('.input-fields').find('#subscription_tax').val());
          $(obj).find('.saved_subscription_discount').html($(save).parents('.input-fields').find('#subscription_discount').val());
         
          if(($(save).parents('.input-fields').find('[name="subscription_optional[]"]').prop('checked') == true && $(obj).find('.border-saved').prop('checked') == false) || ($(save).parents('.input-fields').find('[name="subscription_optional[]"]').prop('checked') == false && $(obj).find('.border-saved').prop('checked') == true))
          {
              $(obj).find('.border-saved').trigger('click');

              if($(save).parents('.input-fields').find('[name="subscription_optional[]"]').prop('checked') == false)
              {
                 $(obj).find('.border-saved').removeAttr('checked');
              }              
          }

          $('.saved_subscription_contents').show(); 
          $(obj).show();
          
       }
       else if($(save).parents('.input-fields').hasClass('add-subscriptions input-fields'))
       {         
          place = place.replace('add-subscriptions','');
          place = place.replace('input-fields','');
          place = place.trim();      

          var obj = $('.saved_subscriptions_contents .'+place).parents('.top-field-heads');

          $(obj).find('.saved_subscription_name').html($(save).parents('.input-fields').find('#subscription_name').val());
          $(obj).find('.saved_subscription_price').html($(save).parents('.input-fields').find('#subscription_price').val());   
          $(obj).find('.saved_subscription_unit').html($(save).parents('.input-fields').find('#subscription_unit').val());  
          $(obj).find('.saved_subscription_description').html($(save).parents('.input-fields').find('#subscription_description').val());
          $(obj).find('.saved_subscription_tax').html($(save).parents('.input-fields').find('#subscription_tax').val());
          $(obj).find('.saved_subscription_discount').html($(save).parents('.input-fields').find('#subscription_discount').val());
          
          if(($(save).parents('.input-fields').find('[name="subscription_optional[]"]').prop('checked') == true && $(obj).find('.border-saved').prop('checked') == false) || ($(save).parents('.input-fields').find('[name="subscription_optional[]"]').prop('checked') == false && $(obj).find('.border-saved').prop('checked') == true))
          {
              $(obj).find('.border-saved').trigger('click');

              if($(save).parents('.input-fields').find('[name="subscription_optional[]"]').prop('checked') == false)
              {
                  $(obj).find('.border-saved').removeAttr('checked');
              }
          }

          $('.saved_subscriptions_contents').show(); 
          $('.saved_subscriptions_contents .'+place).parents('.top-field-heads').show();
       }     
   }
   else
   {     
       if($(save).parents('.input-fields').find('#subscription_name').val()==''){
          $(save).parents('.input-fields').find('#subscription_name').css('border', '1px solid red');
       } if($(save).parents('.input-fields').find('#subscription_price').val()==''){
          $(save).parents('.input-fields').find('#subscription_price').css('border', '1px solid red');
       } if($(save).parents('.input-fields').find('#subscription_unit').val()==''){
         $(save).parents('.input-fields').find('#subscription_unit').css('border', '1px solid red');
       } if($(save).parents('.input-fields').find('#subscription_description').val()==''){
         $(save).parents('.input-fields').find('#subscription_description').css('border', '1px solid red');
       }
   }
}

$(document).on('click','.border-saved',function()
{
   var element = $(this).parents('.top-field-heads').find('.proposal-fields7 a').attr('class');
   element = element.replace('edit_service','');
   element = element.replace('edit_product','');
   element = element.replace('edit_subscription','');
   element = element.replace('red-grcolor','');
   element = $.trim(element); 
   var div = "";
   var checkbox = "";
   
   if($(this).parents('.top-field-heads').parent().hasClass('saved_service_contents'))
   {         
      div = 'div .contents';
      checkbox = '.optional';
   }
   else if($(this).parents('.top-field-heads').parent().hasClass('saved_services_contents'))
   {         
      div = 'div .extra-add-service';
      checkbox = '.optional';
   }
   else if($(this).parents('.top-field-heads').parent().hasClass('saved_product_contents'))
   {         
      div = 'div .product-contents';
      checkbox = '.product_optional';
   }
   else if($(this).parents('.top-field-heads').parent().hasClass('saved_products_contents'))
   {         
      div = 'div .extra-add-products';
      checkbox = '.product_optional';
   }
   else if($(this).parents('.top-field-heads').parent().hasClass('saved_subscription_contents'))
   {         
      div = 'div .subscription-contents';
      checkbox = '.subscription_optional';
   }
   else if($(this).parents('.top-field-heads').parent().hasClass('saved_subscriptions_contents'))
   {         
      div = 'div .extra-add-subscription';
      checkbox = '.subscription_optional';
   }

   $(div+' .'+element).find('.mark-optional '+checkbox).trigger('click'); 

   if($(this).prop('checked') == false)
   {
      $(div+' .'+element).find('.mark-optional '+checkbox).removeAttr('checked');
   } 
   else
   {
      $(div+' .'+element).find('.mark-optional '+checkbox).attr('checked','checked');
   }  
});

function maxLengthCheck(object) {
if (object.value.length > object.maxLength)
  object.value = object.value.slice(0, object.maxLength)
}

function isNumeric (evt) {
var theEvent = evt || window.event;
var key = theEvent.keyCode || theEvent.which;
key = String.fromCharCode (key);
var regex = /[0-9]|\./;
if ( !regex.test(key) ) {
  theEvent.returnValue = false;
  if(theEvent.preventDefault) theEvent.preventDefault();
}
}

$("#add_mail").click(function(){
var mail_content=$("#receiver_mail_id_old").val();

var sender_company=$("#receiver_company_name_old").val();
$("#company_name").val('');

$(".company_search").find(".dropdown-selected").html('');

if (validateEmail(mail_content)) {

$(".client_details").html('');
var content='<strong>'+sender_company+'</strong><input type="hidden" name="receiver_company_name" id="receiver_company_name" value="'+sender_company+'"><strong>'+mail_content+'</strong><input type="hidden" name="receiver_mail_id" id="receiver_mail_id" value="'+mail_content+'">';
$(".client_details").append(content);
$("#send-to").modal('hide');
$("#email_add").modal('hide');
$("#company_name").removeClass('required');
}else{
  $("#email_error_section").html('Enter Valid Email Id');
  $("#email_error_section").show();
}
});


$(".documents").click(function(){


if($(this).hasClass('seleced')){
  $(this).removeClass('seleced');
}else{
//console.log($(this).attr('id'));

$(this).addClass('seleced'); 
}

var accept = [];
var content = [];
var i=0;
  $(".seleced").each(function() {
    accept.push($(this).attr('id'));

     content.push('<li class="Attch" id='+i+'>'+$(this).attr('id')+'<a class="remove_choices" id="remove_choices" onclick="remove_this(this)">X</li>');
     i++;
  });

//$(this).addClass('seleced');
//console.log(accept);

$("#document_attachments").val(accept);

<?php 
if(isset($records)){

}else{ ?>
$(".displayes").html('');
<?php }  ?>
$(".displayes").append(content);




//$("#myModal_image_upload").modal('hide');
});


function remove_this(remove){

var removeItem = $(remove).parent('.Attch').html();
//console.log(removeItem);


var value=$(remove).parent('.Attch').html().split('<a');

//console.log(value[0]);
//console.log(value[2]);

var removeItem =value[0];


$(remove).parent('.Attch').remove();


//console.log($("#document_attachments").val());

var result=$("#document_attachments").val().split(',');
//console.log(result);
var result1 = result.filter(function(elem){
   return elem != removeItem; 
});

$("#document_attachments").val(result1);

}



function removed_this(remove){

var removeItem = $(remove).parent('.Attch').html();
//console.log(removeItem);


var value=$(remove).parent('.Attch').html().split('<a');

//console.log(value[0]);
//console.log(value[2]);

var removeItem =value[0];


$(remove).parent('.Attch').remove();


//console.log($("#attch_image").val());

var result=$("#attch_image").val().split(',');
//console.log(result);
var result1 = result.filter(function(elem){
   return elem != removeItem; 
});

$("#attch_image").val(result1);

}



$("#Price_list_catalog").click(function(){
if($('.item_name').val()!='' || $(".product_name").val()!='' || $('.subscription_name').val()!=''){

$("#pricelist_name").modal('show');
}else{
  $(".pricelist-exists").modal('show');
}
});




$(".close").click(function(){
  //console.log('dgfdgfdgfd');
$(".pricelist-exists").modal('hide');
});


$("#save_price_list").click(function(event)
{
  var obj = $("#pricelistname");
  var pricelist_name = $(obj).val();

  if(pricelist_name != "")
  {
     var url = '<?php echo base_url().'Proposal_pdf/check_price_list_name'; ?>';
     check_name(obj,'',event,url);
  }
  else
  {
     $('#name_er').html('Required.');
  }

  var item_name = [];
  $("input.item_name").each(function(i, sel){
  var selectedVal = $(sel).val();
  item_name.push(selectedVal);
  });        

  var price = [];                  
  $("input.service_price").each(function(i, sel){
  var selectedVal = $(sel).val();
  price.push(selectedVal);
  });         
  var unit = [];
  $("select.unit").each(function(i, sel){
  var selectedVal = $(sel).val();
  unit.push(selectedVal);
  });  

  var qty = [];
  $("input.qty").each(function(i, sel){
  var selectedVal = $(sel).val();
  qty.push(selectedVal);
  });             

  var tax = [];
  $("input.tax").each(function(i, sel){
  var selectedVal = $(sel).val();
  tax.push(selectedVal);
  });  

  var discount = [];
  $("input.discount").each(function(i, sel){
  var selectedVal = $(sel).val();
  discount.push(selectedVal);
  });  

  var description = [];
  $("textarea.description").each(function(i, sel){
  var selectedVal = $(sel).val();
  description.push(selectedVal);
  });  

  var service_optional =[];
  $("input.optional").each(function(i, sel){
  if ($(this).is(":checked"))
  {
  var selectedVal = '1';
  service_optional.push(selectedVal);
  }else{
  var selectedVal = '0';
  service_optional.push(selectedVal);
  }

  }); 

  var service_quantity = [];
  $("input.quantity").each(function(i, sel){
  if ($(this).is(":checked"))
  {
  var selectedVal = '1';
  service_quantity.push(selectedVal);
  }else{
  var selectedVal = '0';
  service_quantity.push(selectedVal);
  }
  });

  var product_name = [];
  $("input.product_name").each(function(i, sel){
  var selectedVal = $(sel).val();
  product_name.push(selectedVal);
  });

  var product_price = [];
  $("input.product_price").each(function(i, sel){
  var selectedVal = $(sel).val();
  product_price.push(selectedVal);
  });
 
  var product_qty = [];
  $("input.product_qty").each(function(i, sel){
  var selectedVal = $(sel).val();
  product_qty.push(selectedVal);
  });
 
  var product_tax = [];
  $("input.product_tax").each(function(i, sel){
  var selectedVal = $(sel).val();
  product_tax.push(selectedVal);
  });

  var product_discount = [];
  $("input.product_discount").each(function(i, sel){
  var selectedVal = $(sel).val();
  product_discount.push(selectedVal);
  });

  var product_description = [];
  $("textarea.product_description").each(function(i, sel){
  var selectedVal = $(sel).val();
  product_description.push(selectedVal);
  }); 

  var product_optional = [];
  $("input.product_optional").each(function(i, sel){
  if ($(this).is(":checked"))
  {
  var selectedVal = '1';
  product_optional.push(selectedVal);
  }else{
  var selectedVal = '0';
  product_optional.push(selectedVal);
  }
  });
 
  var product_quantity = [];
  $("input.product_quantity").each(function(i, sel){
  if ($(this).is(":checked"))
  {
  var selectedVal = '1';
  product_quantity.push(selectedVal);
  }else{
  var selectedVal = '0';
  product_quantity.push(selectedVal);
  }
  }); 

  var subscription_name = [];
  $("input.subscription_name").each(function(i, sel){
  var selectedVal = $(sel).val();
  subscription_name.push(selectedVal);
  }); 

  var subscription_price = [];
  $("input.subscription_price").each(function(i, sel){
  var selectedVal = $(sel).val();
  subscription_price.push(selectedVal);
  });

  var subscription_unit = [];
  $("select.subscription_unit").each(function(i, sel){
  var selectedVal = $(sel).val();
  subscription_unit.push(selectedVal);
  });

  var subscription_tax = [];  
  $("input.subscription_tax").each(function(i, sel){
  var selectedVal = $(sel).val();
  subscription_tax.push(selectedVal);
  });                
 
  var subscription_discount = [];
  $("input.subscription_discount").each(function(i, sel){
  var selectedVal = $(sel).val();
  subscription_discount.push(selectedVal);
  });

  var subscription_description = [];
  $("textarea.subscription_description").each(function(i, sel){
  var selectedVal = $(sel).val();
  subscription_description.push(selectedVal);
  });

  var subscription_optional = [];
  $("input.subscription_optional").each(function(i, sel){
  if ($(this).is(":checked"))
  {
  var selectedVal = '1';
  subscription_optional.push(selectedVal);
  }else{
  var selectedVal = '0';
  subscription_optional.push(selectedVal);
  }
  });

  var subscription_quantity = [];
  $("input.subscription_quantity").each(function(i, sel){
  if ($(this).is(":checked"))
  {
  var selectedVal = '1';
  subscription_quantity.push(selectedVal);
  }else{
  var selectedVal = '0';
  subscription_quantity.push(selectedVal);
  }
  });

  var total_total=$(".total_total").text();
  var discount_amount=$('.discount_amount').text();
  var tax_total=$('.tax_total').text();
  var grand_total=$('.grand_total').text();
  var currency_symbol=$("#currency_symbol").val();

  var tax_details=$("#tax_details").val();
 
  $("#services_total_total").val(total_total);
  $("#services_grand_total").val(grand_total);

  if($('#name_er').html().trim() == "")
  {   
    var formData={'item_name':item_name,'price':price,'unit':unit,'qty':qty,'tax':tax,'discount':discount,'description':description,'service_optional':service_optional,'service_quantity':service_quantity,'product_name':product_name,'product_price':product_price,'product_qty':product_qty,'product_tax':product_tax,'product_discount':product_discount,'product_description':product_description,'product_optional':product_optional,'product_quantity':product_quantity,'subscription_name':subscription_name,'subscription_price':subscription_price,'subscription_unit':subscription_unit,'subscription_tax':subscription_tax,'subscription_discount':subscription_discount,'subscription_description':subscription_description,'subscription_optional':subscription_optional,'subscription_quantity':subscription_quantity,'pricelist_name':pricelist_name,'tax_details':tax_details};
                    
      $.ajax(
      {
          url: '<?php echo base_url();?>/proposal_pdf/pricelist_insert',
          type : 'POST',
          data : formData,                    
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data) 
            {
              $(".LoadingImage").hide();
              $("#pricelist_name").modal('hide');
              var json = JSON.parse(data);             
              content=json['content'];
                   
              $('.pricing_listing').html('');
              $('.pricing_listing').append(content);    
              $(obj).val('');
            }
      });
  }

});


function pricelist_update(price){
 //alert('ok');

  var id=$(price).val();
  var service_length=$(".add-service .input-fields:visible").length;   
  var product_length=$(".add-product .input-fields:visible").length;
  var subscription_length=$(".add-subscription .input-fields:visible").length;   

  if(id==0)
  { 
/*      $("#select-category").addClass('show');
      $("#select-products").addClass('show');
      $("#select-subscriptions").addClass('show');   */

      //alert('ok'+id);
      $(".contents").html('');
      $(".extra-add-service").html('');
      $(".saved_service_contents").css('display','none');  
      $(".blank_service").trigger('click');

      $(".product-contents").html('');
      $(".extra-add-products").html('');
      $(".saved_product_contents").css('display','none'); 
      $(".blank_product").trigger('click');


      $(".subscription-contents").html('');
      $(".extra-add-subscription").html('');
      $(".saved_subscription_contents").css('display','none'); 
      $(".blank_subscription").trigger('click');


/*      $("#select-category").modal('hide');
      $("#select-products").modal('hide');
      $("#select-subscriptions").modal('hide');*/
      sub_total();

  }
  else if(id != "")
  {


    $.ajax({
      url: '<?php echo base_url();?>/proposal_pdf/pricelist_select',
      type : 'POST',
      data : {'id':id,
      service_length:service_length,
      product_length:product_length,
      subscription_length:subscription_length
    },                    
      beforeSend: function() {
      $(".LoadingImage").show();
      },
      success: function(data) 
      {
        $(".LoadingImage").hide();
        localStorage.setItem("price", "price");
        var json = JSON.parse(data);             
        service=json['service'];
        tax_option=json['tax_option'];
        discount_option=json['discount_option'];
        //console.log(tax_option);
        //console.log(discount_option);
        //  alert(tax_option);



        if(typeof(service)=='string')
        { 
        //  console.log('service in');
           localStorage.setItem("price", "service");
          // $(".saved_service_contents").css('display','none'); 
          //$(".contents").html('');
          $(".contents").append(service);
             $('.service_category_name').removeClass('required');
             $(".original_price").removeClass('required');
        }
        product=json['product'];
        if(typeof(product)=='string')
        { 
        //    console.log('product in');
          localStorage.setItem("price1", "product"); 
         // $(".saved_product_contents").css('display','none'); 
        // $(".product-contents").html('');
         $(".product-contents").append(product);
          $('.product_category_name').removeClass('required');
          $(".original_price").removeClass('required');

        }
        subscription=json['subscription'];
        if(typeof(subscription)=='string')
        { 
        //   console.log('subscription in');
           localStorage.setItem("price2", "subscription");
          // $(".saved_subscription_contents").css('display','none'); 
        //  $(".subscription-contents").html('');
          $(".subscription-contents").append(subscription);   
        // $('.subscription-contents').find(':input,:radio,:checkbox').addClass('required');
        $('.subscription_category_name').removeClass('required');
        $(".original_price").removeClass('required');
        }
          if(tax_option==1){

        //console.log('tax on');
                var test = $("input[name='tax']:checked").val();           
                  if(test=='line_tax'){
                    $(".proposal-fields5").css('display','block');
                    $(".service-table").addClass('tax');
                    $("input[name$='tax']").attr('checked',false);
                    $('#line_tax').attr('checked',true);
                  }else{
                     $(".proposal-fields5").removeClass('tax-exits');
                  }
                check_function();
            }else{
              if(test=='line_tax'){

                  $(".proposal-fields5").css('display','block');
                  $(".service-table").addClass('tax');
                  $("input[name$='tax']").attr('checked',false);
                  $('#line_tax').attr('checked',true);
              }else{  
                 $(".proposal-fields5").addClass('tax-exits');
              }
              check_function();
            }

        if(discount_option==1){

        var test1 = $("input[name='discount']:checked").val();
        //   console.log("test-test-test-test-test-test-test-test");
        //  console.log(test1);
        if(test1=='line_discount'){
        $(".proposal-fields6").css('display','block');
        $(".service-table").addClass('discount');
        $("input[name$='discount']").attr('checked',false);
        $('#line_discount').attr('checked',true);

        }else{
        // alert('else0');
        $(".proposal-fields6").removeClass('discount-exits');
        }
        check_function();

        }else{

        if(test1=='line_discount'){
        $(".proposal-fields6").css('display','block');
        $(".service-table").addClass('discount');
        $("input[name$='discount']").attr('checked',false);
        $('#line_discount').attr('checked',true);

        }else{
        // alert('else0');
        $(".proposal-fields6").addClass('discount-exits');
        }
        check_function();

        }
        sub_total();
        }
        });
   }
}


function Quantity_checking(qtyy){
//console.log('ok');


 if ($(qtyy).is(":checked"))
                    {
$(qtyy).prev().prev('.quantity_class').css('display','inline-block');
$(qtyy).prev('.quantity_cla').css('display','none');
}else{
 $(qtyy).prev().prev('.quantity_class').css('display','none');
$(qtyy).prev('.quantity_cla').css('display','block'); 
}
}



function edit_checking(){  

   var sum=0;
   if($('.services_price').val()!=''){
    $('.services_price').each(function() { 
$(this).parents('.service_tr').find('.quantity_cla').html($(this).parents('.service_tr').find('.quantity').val());

    //  console.log('service section');
        var qty_1=$(this).parents('.service_tr').find('.quantity').val();
       // console.log(qty_1);
       var tax=$(this).parents('.service_tr').find('.edit_service_tax').val();
        // console.log(tax);
       var discount=$(this).parents('.service_tr').find('.edit_service_discount').val(); 
        //  console.log(discount);
        var price=$(this).val();


        // console.log(qty_1);
        // console.log(tax);
        // console.log(discount);
        // console.log(price);

        var tax_amount=(price * qty_1).toFixed(2);
        var dec = (tax/100).toFixed(2);        
        var discount1 = (discount/100).toFixed(2);
     //  console.log(dec);
     //  console.log(discount1);

          var mult = (tax_amount* dec).toFixed(2); 
          var dis_count = (tax_amount* discount1).toFixed(2); 
     //  console.log(mult);
//console.log(dis_count);

        var price_cal=(Number($(this).val()) * Number(qty_1)).toFixed(2); 
   //   console.log("price_cal");
   //   console.log(price_cal);

          var calculation=(Number(price_cal)+ Number(mult)).toFixed(2); 
          //  console.log("calculation");

        //  console.log(calculation);
          //  alert(mult);
          //  alert(dis_count);
        //  var calcPrice=price+mult;

          sum += Number(calculation) - Number(dis_count);
       ///   console.log('****');
//console.log(sum);
        });
}

 var sum1=0;
if($('.products_price').val()!=''){
//console.log('price');     
    $('.products_price').each(function() {  
      $(this).parents('.product_tr').find('.quantity_cla').html($(this).parents('.product_tr').find('.quantity').val());
   //   console.log('product_section');
        var qty_11=$(this).parents('.product_tr').find('.quantity').val();
       // console.log(qty_11);
       var tax1=$(this).parents('.product_tr').find('.edit_product_tax').val();
        // console.log(tax);
       var discount1=$(this).parents('.product_tr').find('.edit_product_discount').val(); 
        //  console.log(discount);
        var price1=$(this).val();


        // console.log(qty_11);
        // console.log(tax1);
        // console.log(discount1);
        // console.log(price1);

        var tax_amount1=(price1 * qty_11).toFixed(2);
        var dec1 = (tax1/100).toFixed(2);        
        var discount11 = (discount1/100).toFixed(2);
     //  console.log(dec1);
     //  console.log(discount11);

          var mult1 = (tax_amount1* dec1).toFixed(2); 
          var dis_count1 = (tax_amount1* discount11).toFixed(2); 
    //   console.log(mult1);
    //   console.log(dis_count1);

        var price_cal1=(Number($(this).val()) * Number(qty_11)).toFixed(2); 
    //  console.log("price_cal");
     // console.log(price_cal1);

          var calculation1=(Number(price_cal1)+ Number(mult1)).toFixed(2); 
          //  console.log("calculation");

       //   console.log(calculation1);
          //  alert(mult);
          //  alert(dis_count);
        //  var calcPrice=price+mult;

          sum1 += Number(calculation1) - Number(dis_count1);
        //  console.log('****');
//console.log(sum1);
        });
}


    //  var sum_sugan=0;
      if($('.subscription_price').val()!=''){
        var sum_sugan=0;
      //  console.log('sum value');
      //  console.log(sum_sugan);
    $('.subscription_prices').each(function() {  

            $(this).parents('.subscription_tr').find('.quantity_cla').html($(this).parents('.subscription_tr').find('.quantity').val());
      //  console.log('subscription_section');
        $(this).parents('.subscription_tr').attr('class');
        var qty_12=$(this).parents('.subscription_tr').find('.quantity').val();
     //   console.log(qty_12);
       var tax2=$(this).parents('.subscription_tr').find('.edit_subscription_tax').val();
        // console.log(tax);
       var discount2=$(this).parents('.subscription_tr').find('.edit_subscription_discount').val(); 
        //  console.log(discount);
        var price2=$(this).val();


        // console.log(qty_12);
        // console.log(tax2);
        // console.log(discount2);
        // console.log(price2);

        var tax_amount2=(price2 * qty_12).toFixed(2);
        var dec2 = (tax2/100).toFixed(2);        
        var discount12 = (discount2/100).toFixed(2);
      // console.log(dec2);
      // console.log(discount12);

          var mult2 = (tax_amount2* dec2).toFixed(2); 
          var dis_count2 = (tax_amount2* discount12).toFixed(2); 
      // console.log(mult2);
      // console.log(dis_count2);

        var price_cal2=(Number($(this).val()) * Number(qty_12)).toFixed(2); 
   //   console.log("price_cal");
    //  console.log(price_cal2);

          var calculation2=(Number(price_cal2)+ Number(mult2)).toFixed(2); 
          //  console.log("calculation");

       //   console.log(calculation2);

       //   console.log(dis_count2);
          //  alert(mult);
          //  alert(dis_count);
        //  var calcPrice=price+mult;
//console.log(Number(calculation2) - Number(dis_count2));
          sum_sugan +=Number(calculation2) - Number(dis_count2);
       //   console.log('****');
//console.log(parseInt(sum_sugan));
        });
}


var cal=Number(sum) + Number(sum1) + Number(sum_sugan);


var sub_total=Number(sum) + Number(sum1) + Number(sum_sugan);
//console.log(cal);
//console.log(sub_total);




if($("#tax_option").val()=='total_tax'){

var overalltax=$('.over_alltax').val();

  var taxes=(overalltax/100).toFixed(2);  
   var multes = (cal* taxes).toFixed(2); 
   var cal=Number(cal) + Number(multes);
  //  console.log('tax');
    //  console.log(cal);

}


if($("#discount_option").val()=='total_discount'){
      var overalldiscount=$('.over_alldiscount').val();
      var discounting=(overalldiscount/100).toFixed(2);  
      var multes = (cal* discounting).toFixed(2); 
      var cal=Number(cal) - Number(multes);
     // console.log('discount');
    //  console.log(cal);
}


$(".sub_total").html('');
$(".sub_total").html(sub_total);

$(".grand_total_1").html('');
$(".grand_total_1").html(Number(cal).toFixed(2));

}

$("#save_leads").click(function(){
//console.log('click');

var lead_status=$("#lead_status").val();
//console.log(lead_status);
if(lead_status==''){
  $("#lead_error").text('Leads status Field Required');
  $("#lead_error").show();
  //console.log('1');
}else{
   $("#lead_error").hide();
}
var source=$("#source").val();
if(source==''){
  $("#source_error").html('Source Field Required');
  $("#source_error").show();
  //console.log('2');
}else{
    $("#source_error").hide();
}
var assigned=$("#assigned").val();
if(assigned==''){
  $("#assigned_error").html('Assigned Field Required');
  $("#assigned_error").show();
//console.log('3');
}else{
  $("#assigned_error").hide();
}
var tags=$("#tags").val();
if(assigned==''){
  $("#tags_error").html('Tags Field Required');
//console.log('4');
$("#tags_error").show();
}else{
  $("#tags_error").hide();
}
var name=$("#name").val();
if(name==''){
  $("#name_error").html('First Name Field Required');
//console.log('5');
$("#name_error").show();
}else{
  $("#name_error").hide();
}
var last_name=$("#last_name").val();
if(last_name==''){
  $("#last_name_error").html('Last Name Field Required');
//console.log('6');
$("#last_name_error").show();
}else{
  $("#last_name_error").hide();
}
var address=$("#address").val();
if(address==''){
  $("#address_error").html('Address Field Required');
//console.log('7');
$("#address_error").show();
}else{
  $("#address_error").hide();
}
var email_address=$("#email_address").val();
if(email_address==''){
  $("#email_address_error").html('Email Address Required');

  $("#email_address_error").show(); 
}else{
 var sender_company_mailid=$("#email_address").val();
  if (validateEmail(sender_company_mailid)) {
$("#email_address_error").hide(); 
  }else{
    $("#email_address_error").html('Invalid Mail id');
     //console.log('8');
$("#email_address_error").show(); 
  }

}


var company=$("#company").val();
if(company==''){
    //console.log('9');
  $("#company_error").html('Company Required');
  $("#company_error").show();
}else{
   $("#company_error").hide();
}
var description=$("#description").val();
if(description==''){
    //console.log('10');
  $("#description_error").html('Description Required');

$("#description_error").show();
}else{
  $("#description_error").hide();
}
var phone=$("#phone").val();
if(phone==''){

  //console.log('11');
  $("#phone_error").html('Phone Number Required');

$("#phone_error").show();
}else{

$("#phone_error").hide();
}



//console.log($(".error:visible").length);

if($(".error:visible").length==0){
  $(".error").hide();

var position=$("#position").val();

var city=$("#city").val();
var state=$("#state").val();
var website=$("#website").val()
var country=$("#country").val();
var zip_code=$("#zip_code").val();
var public=$("#public").val();
var contact_today=$("#contact_today").val();

var lead_status=$("#lead_status").val();
var source=$("#source").val();
var assigned=$("#assigned").val();
var tags=$("#tags").val();

var name=$("#name").val();

var last_name=$("#last_name").val();

var address=$("#address").val();

var email_address=$("#email_address").val();

var company=$("#company").val();

var description=$("#description").val();

var country_code=$(".country_code").val();
var phone=$("#phone").val();

var number=country_code+'-'+phone;

var default_language=$("#default_language").val();
     var formData={'lead_status':lead_status,'source':source,'assigned':assigned,'tags':tags,'name':name,'last_name':last_name,'address':address,'position':position,'city':city,'email_address':email_address,'state':state,'website':website,'country':country,'phone':number,'zip_code':zip_code,'company':company,'default_language':default_language,'description':description,'public':public,'contact_today':contact_today}; 


$.ajax({
url: '<?php echo base_url();?>Leads/addLeads_ajax',
type : 'POST',
data : formData,                    
beforeSend: function() {
$(".LoadingImage").show();
},
success: function(data) {
$(".LoadingImage").hide();


}

});
  //console.log('sumbmit');
  //  $("#leads_form").submit();
}

});


$(document).ready(function($){
    $cf = $('#phone');
    $cf.blur(function(e){
        phone = $(this).val();
        phone = phone.replace(/[^0-9]/g,'');
        if (phone.length < 0 || phone.length > 20)
        {
           $("#phone").addClass('found');
           $("#phone").css('border','1px solid red');
          //  $('#phone').val('');
           // $('#phone').focus();
        }else{
           $("#phone").removeAttr('style');
        }
    });
});



$(document).ready(function($){
    $cf = $('#fax');
    $cf.blur(function(e){
        phone = $(this).val();
        phone = phone.replace(/[^0-9]/g,'');
        if (phone.length != 7)
        {
           $("#fax").addClass('found');
           $("#fax").css('border','1px solid red');
         //   $('#fax').val('');
           // $('#fax').focus();
        }else{
           $("#fax").removeAttr('style');
        }
    });
});

//$(document).ready(function(){
//check_function();
//alert($('#pricelist').val());
/*if($('#pricelist').val()!=''){
 // alert('run');
  $("#pricelist").trigger('change');
}*/


//});
        $(".dropdown-sin-1").click(function()
        {

           // $(".dropdown-display").removeAttr( 'style' ); 
            //alert('ok');
           // if(($(".dropdown-sin-1.dropdown-single.active").length)=='0'){

            var template_id = $("#proposal_type").val();
            ///alert(crm_company_name);
            $.ajax(
            {
              url: '<?php echo base_url();?>proposal_pdf/templatename_get/',
              type : 'POST',
              data : {'template_id':template_id},                    
              beforeSend: function() {
              $(".LoadingImage").show();
              },
              success: function(data) {
              $(".LoadingImage").hide();             
              var json = JSON.parse(data);             
              proposal_name=json['proposal_name'];
              $("#proposal_name").val(proposal_name);
          
              }
            });
           // }
        });

$(document).ready(function(){
$("#preview_proposal_send").click(function(){

  var template_content=$("#template_own_content").val();

//console.log('dsfsdfsdfs');
  $("#proposal_body_content").html('');
$("#proposal_body_content").html(template_content);

 $('#Preview_proposal').modal('show'); 
//alert('ok');
});
});

function check(email){
  if(/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($(email).val())){
     $(email).removeAttr('style');
} else {
   $(email).css('border','1px solid red');
}
}

// $(".add-link").blur(function(){
//   alert('check url');
//   alert($(this).val());
// });

      </script> 

<script type="text/javascript">

$(".autocomplete_client").keyup(debounce(function() 
{         
    var autoTypeNo=0;
                              
      $(this).autocomplete(
      {
          source: function( request, response ) {
              $.ajax({
                  url : '<?php echo base_url();?>/Leads/lead_SearchCompany',
                  
                   method: 'post',
                   data: {
                      name_startsWith: request.term,
                  
                   },
                   success: function(data) {
              
                     var dispn = $.parseJSON(data);   
                      response( $.map(dispn, function( item ) {
                              var code = item.split("|"); 
                                    
                              return {
                                  label: code[autoTypeNo],
                                  value: code[autoTypeNo],
                                  data : item
                              }
                          })); 
                      }
              });
          },
          autoFocus: true,            
          minLength: 0,                            

          select: function( event, ui ) 
          {
             var names = ui.item.data.split("|");                 

             $('#address').text(names[1]);            
             $('#postal_code').val(names[3]);                  
          }
      });

},500));

  function getTemplates(obj)
  {   
    console.log("We just tested.");
      var tid = obj.value;
      tid = tid.trim();
    
      $.ajax(
      {
         url:'<?php echo base_url().'User/get_template'; ?>',
         type:'POST',
         data:{'id':tid},

         beforeSend: function() {
              $(".LoadingImage").show();
         },

         success:function(template)
         { 
            var temp = JSON.parse(template);
            
            $("#subject").val('');
            $('#subject').val(temp.subject); 
            $('#body').html('');
            $('#body').html(temp.body);

            tinyMCE.get('email_proposal_subject').setContent('');
            tinyMCE.get('email_proposal_subject').setContent(temp.body);

            $(".LoadingImage").hide();
         }

      }); 

   }

   function getMailTemplate(formData)
   { 

          $('.input-fields:visible input[name="service_optional[]"]').each(function(e){
        var val=Number(e)+1;
      $(this).val(val);

      })



      $('.input-fields:visible input[name="service_quantity[]"]').each(function(e){
        var val=Number(e)+1;
      $(this).val(val);

      });


       $('.input-fields:visible input[name="product_optional[]"]').each(function(e){
        var val=Number(e)+1;
      $(this).val(val);

      });

        $('.input-fields:visible input[name="product_quantity[]"]').each(function(e){
        var val=Number(e)+1;
      $(this).val(val);

      });

         $('.input-fields:visible input[name="subscription_optional[]"]').each(function(e){
        var val=Number(e)+1;
      $(this).val(val);

      });

          $('.input-fields:visible input[name="subscription_quantity[]"]').each(function(e){
        var val=Number(e)+1;
      $(this).val(val);

      });
          
       $.ajax(
       {
           url: urls_name,
           type : 'POST',
           data : formData, 

           beforeSend: function() 
           {
             $(".LoadingImage").show();
           },
           
           success: function(data) 
           {
               $(".LoadingImage").hide();
               var json = JSON.parse(data);
               data=json['table'];
            
               var result= data;
               var finalData = result.replace(/\\/g, "");         
               
               var template_type;

               <?php if(isset($records['proposal_type']) && $records['proposal_type']!="" && $this->uri->segment(2) == 'step_proposal') { ?>
                template_type = '<?php echo $records['proposal_type']; ?>';
               <?php }else{ ?>
                template_type = $("#proposal_type").val(); 
               <?php } ?>
        
               if(template_type!='blank')
               {
                  $.ajax(
                  {
                     url: '<?php echo base_url(); ?>/proposals/template_content',
                     type : 'POST',
                     data : {'id':template_type},                    
                       beforeSend: function() {
                         $(".LoadingImage").show();
                       },
                       success: function(data) 
                       {
                          $(".LoadingImage").hide();  
                          var json = JSON.parse(data);      
                          /*var template_content=$("#template_own_content").val();

                          if(template_content!='')
                          {
                               $("#edit_tab").find('#dd-head').html($(template_content).find('#dd-head').html());
                               $("#edit_tab").find('#dd-body').html($(template_content).find('#dd-body').html());
                               $("#edit_tab").find('#dd-footer').html($(template_content).find('#dd-footer').html());
                          }
                          else
                          { */                 
                             if(typeof(json['template_head'])!="undefined")
                             { 
                              json['template_head'] = replaceAll(json['template_head'],'[Price Table]',finalData); 
                              $("#proposal_contents_details").find('#dd-head').html('');
                              $("#proposal_contents_details").find('#dd-head').html(json['template_head']);
                             } 
                             if(typeof(json['template_body'])!="undefined")
                             {  
                              json['template_body'] = replaceAll(json['template_body'],'[Price Table]',finalData);
                              $("#proposal_contents_details").find('#dd-body').html('');
                              $("#proposal_contents_details").find('#dd-body').html(json['template_body']);
                             }
                             if(typeof(json['template_footer'])!="undefined")
                             { 
                              json['template_footer'] = replaceAll(json['template_footer'],'[Price Table]',finalData);
                              $("#proposal_contents_details").find('#dd-footer').html('');
                              $("#proposal_contents_details").find('#dd-footer').html(json['template_footer']);
                             }
                          //}                                 
                       }

                  });                         
              }
              else
              {                        
                  var template_content=$("#template_own_content").val();

                  if(template_content!='')
                  {         
                      $("#edit_tab").find('#dd-head').html('');
                      $("#edit_tab").find('#dd-head').html($(template_content).find('#dd-head').html());
                      $("#edit_tab").find('#dd-footer').html('');
                      $("#edit_tab").find('#dd-footer').html($(template_content).find('#dd-footer').html());
                      $("#edit_tab").find('#dd-body').find('#gg_body_content').html('');
                      $("#edit_tab").find('#dd-body').find('#gg_body_content').html(finalData);
                  }
                  else
                  {                          
                      $("#edit_tab").find('#dd-head').html('');
                      $("#edit_tab").find('#dd-footer').html('');
                  } 
              }
             }   
         });
   }

   function replaceAll(string, term, replacement) 
   {
      return string.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
   }

   // To convert or consider the string as literal string
   function escapeRegExp(string)
   {
      return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
   }

</script>