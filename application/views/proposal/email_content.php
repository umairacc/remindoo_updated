<div class="modal-alertsuccess alert alert-tem" style="display:none;">
    <div class="newupdate_alert">
        <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close"></a> -->
        <div class="pop-realted1">
            <div class="position-alert1">
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .colorpicker.dropdown-menu.colorpicker-with-alpha.colorpicker-right.colorpicker-visible {
        z-index: 9999;
    }

    .medium-editor-element,
    .alert-tem {
        overflow-y: scroll !important;
    }

    .popover {
        position: relative !important;
        left: 0px !important;
    }

    .popover-lg.top.in {
        top: -9px !important;
    }

    .form-group .col-sm-6 {
        float: none !important;
    }

    .imagination img {
        max-width: 40% !important;
        max-height: 40% !important;
    }

    button.btn.btn-info.hidden {
        background-color: #00a2e8 !important;
        background-image: initial !important;
        padding: 6px 15px !important;
        border-radius: 50px;
    }

    <?php if ($this->uri->segment(2) == 'templates') { ?>.get-options {
        width: 600px;
    }

    #mail-template {
        width: 650px;
    }

    <?php } ?>
    /*.LoadingImage {
   display : none;
   position : fixed;
   z-index: 99999999999 !important;
   background-image : url('<?php //echo site_url()
                            ?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }*/
</style>
<div class="LoadingImage"></div>


<div class="container-fullscreen">
    <div class="container text-center">
        <div id="choose-template" class="text-center">
            <button class="choose no_sidebar" type="button" data-id="no-sidebar"><img src="<?php echo base_url(); ?>assets/img/no-sidebar.jpg" class="img-responsive" alt=""><span>No Sidebar (wide)</span></button>
            <button class="choose" type="button" data-id="left-sidebar"><img src="<?php echo base_url(); ?>assets/img/left-sidebar.jpg" class="img-responsive" alt=""><span>Left Sidebar</span></button>
            <button class="choose" type="button" data-id="right-sidebar"><img src="<?php echo base_url(); ?>assets/img/right-sidebar.jpg" class="img-responsive" alt=""><span>Right Sidebar</span></button>
            <button class="choose" type="button" data-id="both-sidebar"><img src="<?php echo base_url(); ?>assets/img/both-sidebar.jpg" class="img-responsive" alt=""><span>Both Sidebars</span></button>
        </div>
    </div>

    <div class="container-sidebar hidden" id="option-tabs">
        <div class="new-update-email1">
            <div id="get-options">
                <p style="color: red !important;">For Pasting, Use Ctrl+v For Better Results.</p>
                <!--  <h4>Drag and drop the elements below to the work area on the left</h4> -->
                <div class="get-options choose" data-id="title" id="title"><i class="fa fa-header" aria-hidden="true"></i>
                    <div>Heading</div>
                </div>
                <div class="get-options choose" data-id="content" id="content"><i class="fa fa-indent" aria-hidden="true"></i>
                    <div>Text</div>
                </div>
                <div class="get-options choose" data-id="image" id="image"><i class="fa fa-picture-o" aria-hidden="true"></i>
                    <div>Image</div>
                </div>
                <div class="get-options choose" data-id="video" id="video"><i class="fa fa-play" aria-hidden="true"></i>
                    <div>Video</div>
                </div>
                <div class="get-options choose" data-id="link" id="link"><i class="fa fa-link" aria-hidden="true"></i>
                    <div>Link</div>
                </div>
                <div class="get-options choose" data-id="divider" id="divider"><i class="fa fa-window-minimize" aria-hidden="true"></i>
                    <div>Divider</div>
                </div>
                <!--  <div class="get-options choose" data-id="quote" id="quote"><i class="fa fa-comment" aria-hidden="true"></i><div>Blockquote</div></div> -->
                <div class="get-options choose" data-id="e_signature" id="e_signature"><i class="fa fa-pencil" aria-hidden="true"></i>
                    <div>Email Signature</div>
                </div>
                <div id="editor"></div>
                <ul id="attach-data" class="list-group"></ul>
            </div>
            <?php if ($this->uri->segment(2) != 'step_proposal' && $this->uri->segment(2) != 'copy_proposal') { ?>
                <!-- <button class="btn btn-lg btn-default btn-materialize btn-left-bottom btn-left-bottom-3 hidden" type="button" id="price_table" data-toggle="tooltip" title="To add table, place the cursor where you want and click over me." data-placement="top" data-trigger="hover" style="width: 108px;padding-right:15px;background-color: #a39a98"><span class="fa fa-table" style="padding-left:5px;padding-right:5px;"></span>Table</button>  -->
                <button class="btn btn-lg btn-default btn-materialize btn-left-bottom btn-left-bottom-3 hidden" type="button" id="price_table" data-toggle="tooltip" title="To add table, drag me over the editor." data-placement="top" data-trigger="hover" style="width: 108px;padding-right:15px;background-color: #a39a98" draggable="true"><span class="fa fa-table" style="padding-left:5px;padding-right:5px;"></span>Table</button>
            <?php } ?>
            <!--  <button class="btn btn-lg btn-success btn-materialize btn-left-bottom btn-left-bottom-1 hidden" type="button" id="preview" title="Preview" data-toggle="tooltip" data-placement="top" data-trigger="hover"><i class="fa fa-search-plus" aria-hidden="true"></i> Preview </button> -->

            <button class="btn btn-lg btn-default btn-materialize btn-left-bottom btn-left-bottom-3 hidden" type="button" id="setting" title="Layout Options" data-toggle="tooltip" data-placement="top" data-trigger="hover"><span class="fa fa-cog fa-spin"></span> Setting</button>

        </div>
    </div>
    <div class="container-content hidden" id="mail-template" oncontextmenu="return false;">
        Content
    </div>
</div>
<div id="modal" class="reset-this"></div>


<div id="alerts"></div>

<div class="tools tools-left" id="settings">
    <div class="tools-header">
        <button type="button" class="close" data-dismiss="tools" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4><span class="fa fa-cog fa-spin"></span> Settings</h4>
    </div>
    <div class="tools-body">
        <h5 class="text-left option-title">Layout</h5>
        <div class="form-horizontal">


            <div class="form-group">
                <label for="body-layout-bkg-color-form" class="col-sm-6 control-label text-left">Background Color:</label>
                <div class="col-sm-6">
                    <div id="body-layout-bkg-color" class="input-group colorpicker-component">
                        <span class="input-group-addon"><i></i></span>
                        <input type="text" value="" class="form-control input-sm" id="body-layout-bkg-color-form">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="body-layout-bkg-color-form" class="col-sm-6 control-label text-left">Body Color:</label>
                <div class="col-sm-6">
                    <div id="body-layout-bkg-color-body" class="input-group colorpicker-component">
                        <span class="input-group-addon"><i></i></span>
                        <input type="text" value="" class="form-control input-sm" id="body-layout-bkg-color-body-form">
                    </div>
                </div>
            </div>

        </div>

        <h5 class="text-left option-title"></h5>
        <div class="form-horizontal">

            <div class="form-group">
                <label for="head-bkg-color-form" class="col-sm-6 control-label text-left">Background Color:</label>
                <div class="col-sm-6">
                    <div id="head-bkg-color" class="input-group colorpicker-component">
                        <span class="input-group-addon"><i></i></span>
                        <input type="text" value="" class="form-control input-sm" id="head-bkg-color-form">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="head-height" class="col-sm-4 control-label text-left">Height:</label>
                <div class="col-sm-8 text-right">
                    <input type="text" class="form-control input-sm" id="head-height" data-slider-min="0" data-slider-max="1000" data-slider-step="10" data-slider-value="0">&nbsp;&nbsp;&nbsp;<small>Height: <span id="head-height-val">auto</span></small>
                </div>
            </div>

        </div>

        <div id="dd-body-exists">
            <h5 class="text-left option-title">Content Section</h5>
            <div class="form-horizontal">

                <div class="form-group">
                    <label for="content-bkg-color-form" class="col-sm-6 control-label text-left">Background Color:</label>
                    <div class="col-sm-6">
                        <div id="content-bkg-color" class="input-group colorpicker-component">
                            <span class="input-group-addon"><i></i></span>
                            <input type="text" value="" class="form-control input-sm" id="content-bkg-color-form">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="content-height" class="col-sm-4 control-label text-left">Height:</label>
                    <div class="col-sm-8 text-right">
                        <input type="text" class="form-control input-sm" id="content-height" data-slider-min="0" data-slider-max="1000" data-slider-step="10" data-slider-value="0">&nbsp;&nbsp;&nbsp;<small>Height: <span id="content-height-val">auto</span></small>
                    </div>
                </div>

            </div>
        </div>

        <div id="dd-sidebar-left-exists">
            <h5 class="text-left option-title">Left Sidebar Section</h5>
            <div class="form-horizontal">

                <div class="form-group">
                    <label for="left-bkg-color-form" class="col-sm-6 control-label text-left">Background Color:</label>
                    <div class="col-sm-6">
                        <div id="left-bkg-color" class="input-group colorpicker-component">
                            <span class="input-group-addon"><i></i></span>
                            <input type="text" value="" class="form-control input-sm" id="left-bkg-color-form">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="left-height" class="col-sm-4 control-label text-left">Height:</label>
                    <div class="col-sm-8 text-right">
                        <input type="text" class="form-control input-sm" id="left-height" data-slider-min="0" data-slider-max="1000" data-slider-step="10" data-slider-value="0">&nbsp;&nbsp;&nbsp;<small>Height: <span id="left-height-val">auto</span></small>
                    </div>
                </div>

            </div>
        </div>

        <div id="dd-sidebar-right-exists">
            <h5 class="text-left option-title">Right Sidebar Section</h5>
            <div class="form-horizontal">

                <div class="form-group">
                    <label for="right-bkg-color-form" class="col-sm-6 control-label text-left">Background Color:</label>
                    <div class="col-sm-6">
                        <div id="right-bkg-color" class="input-group colorpicker-component">
                            <span class="input-group-addon"><i></i></span>
                            <input type="text" value="" class="form-control input-sm" id="right-bkg-color-form">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="right-height" class="col-sm-4 control-label text-left">Height:</label>
                    <div class="col-sm-8 text-right">
                        <input type="text" class="form-control input-sm" id="right-height" data-slider-min="0" data-slider-max="1000" data-slider-step="10" data-slider-value="0">&nbsp;&nbsp;&nbsp;<small>Height: <span id="right-height-val">auto</span></small>
                    </div>
                </div>

            </div>
        </div>

        <h5 class="text-left option-title">Footer Section</h5>
        <div class="form-horizontal">

            <div class="form-group">
                <label for="footer-bkg-color-form" class="col-sm-6 control-label text-left">Background Color:</label>
                <div class="col-sm-6">
                    <div id="footer-bkg-color" class="input-group colorpicker-component">
                        <span class="input-group-addon"><i></i></span>
                        <input type="text" value="" class="form-control input-sm" id="footer-bkg-color-form">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="footer-height" class="col-sm-4 control-label text-left">Height:</label>
                <div class="col-sm-8 text-right">
                    <input type="text" class="form-control input-sm" id="footer-height" data-slider-min="0" data-slider-max="1000" data-slider-step="10" data-slider-value="0">&nbsp;&nbsp;&nbsp;<small>Height: <span id="footer-height-val">auto</span></small>
                </div>
            </div>
        </div>
    </div>
    <div class="tools-footer">
        <div class="button-group text-center">
            <button class="btn btn-success btn-sm" data-dismiss="tools" type="button" id="send-message"><span class="glyphicon glyphicon-ok"></span> I'm Done</button>
            <button class="btn btn-warning btn-sm" type="button" id="test"><span class="glyphicon glyphicon-envelope"></span> Send Test</button>
            <button class="btn btn-danger btn-sm" type="button" id="delete"><i class="fa fa-delete fa-5"></i> Delete Project</button>
        </div>
    </div>
</div>