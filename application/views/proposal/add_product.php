<style type="text/css">
.annual-field2.add-input-service {
    width: 50%;
}
</style>
<div class="unit-annual floating_set">
   <div class="common-annual lead-annual">
      <div class="annual-field2 add-input-service">
         <label>Name</label>     
         <input type="text" name="product_name" required="required">
      </div>
      <div class="annual-field2 add-input-service"> 
         <label>Price </label>    
         <input type="text" name="product_price" required="required" class="decimal">
      </div>
      <div class=" spacing-annual  add-input-service">
         <label>Description</label>
         <textarea rows="3" name="product_description" id="desc-add-product" required="required"></textarea>
      </div>
      <div class="spacing-annual accotax-litd send-to">
         <label>Choose a category</label>
        <div class="product_category_add">
         <select name="product_category"  id="product_category" required="required">
         <option value="">Select </option>
         <?php
         if(count($category)>0){
         foreach($category as $cat){ 
            if($cat['category_type']=='product'){  ?>
            <option value="<?php echo $cat['id']; ?>"><?php echo $cat['category_name']; ?></option>
       <?php   }} } ?>
         </select>
         </div>
         <a href="javascript:;" data-toggle="modal" data-target="#add-new-category_product"><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add a new Category</a>
      </div>
   </div>
</div>