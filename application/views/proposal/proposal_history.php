<?php $this->load->view('includes/header');?>
<link href="<?php echo base_url(); ?>assets/css/timeline-style.css" type="text/css" rel="stylesheet" />
<div class="pcoded-content">
  <div class="pcoded-inner-content">
    <!-- Main-body start
      <div class="main-body">
        <div class="page-wrapper">
          <!-Page body start -->
    <div class="page-body">
      <div class="row">
        <div class="col-sm-12 proposal-history-wrapper">
            <div class="graybgclsnew button_visibility" style="margin:0;">
              <div class="deadline-crm1 floating_set">                   
                <?php $this->load->view('proposal/proposal_navigation_tabs'); ?>
              </div>
            </div>
          <div class="row history historynew">
            <div class="card">
              <div class="card-header">
                <h5>History</h5>
              </div>
              <div class="row mar_bottls">
                <div class="col-1">
                  <h5><i class="icofont icofont-file-text m-r-5"></i></h5>
                </div>
                <div class="col-3">
                  <input class="form-control form-control-lg" type="text" id="search-criteria" placeholder="Search notes">
                </div>
              </div>
              <div class="card-block">
                <div class="main-timeline">
                  <div class="cd-timeline cd-container">
                    <?php 
                      $i=1;
                      foreach($proposal_history as $p_h){  
                      $randomstring=generateRandomString('100');
                      if($p_h['status'] == 'opened'){$p_h['status']='viewed';}
                      ?>
                    <div class="cd-timeline-block" id="<?php echo $i; ?>">
                      <div class="cd-timeline-icon bg-primary">
                        <i class="icofont icofont-ui-file"></i>
                      </div>
                      <div class="cd-timeline-content card_main">
                        <div class="p-20">
                          <div class="timeline-details">
                            <p class="m-t-20 search_word">
                              <a href="<?php echo base_url().'proposal_page/step_proposal/'.$randomstring.'---'.$p_h['id']; ?>"><?php echo $p_h['proposal_name']; ?> </a> 
                            </p>
                          </div>
                        </div>
                        <span class="cd-date"><?php
                          $timestamp = strtotime($p_h['created_at']);
                          $newDate = date('d F Y H:i', $timestamp); 
                          echo $newDate;
                          ?></span>
                        <span class="cd-date"><?php echo 'Sent By : '.$p_h['sender_company_name']; ?></span>
                        <span class="cd-date"><?php echo 'Sent To : '.$p_h['receiver_company_name']; ?></span>
                        <span class="cd-date"><?php echo 'Status : '.ucfirst($p_h['status']); ?></span>
                      </div>
                    </div>
                    <?php $i++; } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

</div>
</div>
</div>
</div>
</div>
<?php $this->load->view('includes/footer');?>
<script type="text/javascript">
  $('#search-criteria').keyup(function(){
      //$('.box_section').hide();
      var txt = $('#search-criteria').val();
       $(".cd-timeline-block").hide();
      $('.search_word').each(function(){ 
         if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
             $(this).show();  
            $(this).parents('.cd-timeline-block').show();
  
         }});
      $('.cd-date').each(function(){ 
         if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
             $(this).show();  
            $(this).parents('.cd-timeline-block').show();  
         }
      });
  });
</script>