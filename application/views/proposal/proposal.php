<?php $this->load->view('includes/header');
error_reporting(-1);
?>
<link href="<?php echo base_url(); ?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />
<!--   Datatable header by Ram -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/email.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css?ver=2"> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
<!--   End by Ram -->
<style>
	button.btn.btn-info.btn-lg.newonoff {
		padding: 3px 10px;
		height: initial;
		font-size: 15px;
		border-radius: 5px;
	}

	.dropdown-content {
		display: none;
		position: absolute;
		background-color: #fff;
		min-width: 86px;
		overflow: auto;
		box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
		z-index: 1;
		left: -92px;
		width: 150px;
	}

	tfoot {
		display: table-header-group;
	}

	.export_option {
		margin-top: 21px !important;
	}

	.dataTables_length {
		margin-top: 45px !important
	}

	.dataTables_filter {
		margin-top: 45px !important
	}

	#viewed {
		padding: 5px;
	}

	#discussion {
		padding: 5px;
	}

	#accepted {
		padding: 5px;
	}

	#declined {
		padding: 5px;
	}

	#draft {
		padding: 5px;
	}

	.toolbar-table .dt-buttons {
		margin-top: 0px !important;
	}

	.fa-link {
		color: #F20094 !important;
		font-size: 18px;
	}

	.fa-file-zip-o {
		color: #1d8184 !important;
		font-size: 18px;
	}

	/* 18 Jan */
</style>
<!-- <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/dataTables.bootstrap4.min.css"> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<!-- Main-body start
      <div class="main-body">
        <div class="page-wrapper">
          <!-Page body start -->
		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<div class="modal-alertsuccess alert alert-success1" style="display:none;">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
						<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
						<div class="pop-realted1">
							<div class="position-alert1">
								Archieved Successfully
							</div>
						</div>
					</div>

					<div class="modal-alertsuccess alert alert-success-delete" style="display:none;">
						<div class="newupdate_alert">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<div class="pop-realted1">
								<div class="position-alert1">
									YOU HAVE SUCCESSFULLY DELETED
								</div>
							</div>
						</div>
					</div>
					<?php

					$cur_symbols = $this->Common_mdl->getallrecords('crm_currency');

					foreach ($cur_symbols as $key => $value) {
						$currency_symbols[$value['country']] = $value['currency'];
					}

					?>

					<?php
					$send_array = array();
					$send_count = 0;
					$send_amount = 0;
					$view_array = array();
					$view_count = 0;
					$view_amount = 0;
					$draft_array = array();
					$draft_count = 0;
					$draft_amount = 0;
					$indiscussion_array = array();
					$indiscussion_count = 0;
					$indiscussion_amount = 0;
					$decline_array = array();
					$decline_count = 0;
					$decline_amount = 0;
					$accepted_array = array();
					$accepted_count = 0;
					$accepted_amount = 0;
					$archive_count = 0;
					foreach ($proposal as $key => $value) {
						if ($value['status'] == 'sent') {
							$send_count += 1;
							if ($value['grand_total'] == '') {
								$values = 0;
							} else {
								$values = $value['grand_total'];
							}
							$send_amount += (int)$values;
							array_push($send_array, $value['price']);
						}
						if ($value['status'] == 'opened') {
							$view_count += 1;
							if ($value['grand_total'] == '') {
								$values = 0;
							} else {
								$values = $value['grand_total'];
							}
							$view_amount += (int)$values;
							array_push($view_array, $value['price']);
						}
						if ($value['status'] == 'draft') {
							$draft_count += 1;
							if ($value['grand_total'] == '') {
								$values = 0;
							} else {
								$values = $value['grand_total'];
							}
							$draft_amount += (int)$values;
							array_push($draft_array, $value['price']);
						}
						if ($value['status'] == 'in discussion') {
							$indiscussion_count += 1;

							if ($value['grand_total'] == '') {
								$values = 0;
							} else {
								$values = $value['grand_total'];
							}
							$indiscussion_amount += (int)$values;
							array_push($indiscussion_array, $value['price']);
						}
						if ($value['status'] == 'declined') {
							$decline_count += 1;
							if ($value['grand_total'] == '') {
								$values = 0;
							} else {
								$values = $value['grand_total'];
							}

							$decline_amount += (int)$values;
							array_push($decline_array, $value['price']);

							// $view_count+=1;
							//  if($value['grand_total']==''){ $values=0; }else{ $values=$value['grand_total']; }
							// $view_amount+= (int)$values;
							// array_push($view_array, $value['price']);                    
						}
						if ($value['status'] == 'accepted') {
							$accepted_count += 1;
							if ($value['grand_total'] == '') {
								$values = 0;
							} else {
								$values = $value['grand_total'];
							}

							$accepted_amount += (int)$values;
							array_push($accepted_array, $value['price']);

							// $view_count+=1;
							if ($value['grand_total'] == '') {
								$values = 0;
							} else {
								$values = $value['grand_total'];
							}
							//  $view_amount+= (int)$values;
							//   array_push($view_array, $value['price']);

						}
						if ($value['status'] == 'archive') {
							$archive_count += 1;
						}
					}
					?>
					<!-- Register your self card start -->
					<div class="card propose-template proposal-template-wrapper">
						<!-- admin start-->
						<div class="client_section col-xs-12 floating_set">
						</div> <!-- all-clients -->
						<div class="client_section user-dashboard-section1 floating_set">
							<div class="all_user-section floating_set proposal-commontab">

								<div class="common-reduce01 floating_set mainbgwhitecls">
									<div class="graybgclsnew button_visibility" style="margin:0;">
										<div class="deadline-crm1 floating_set">
											<?php $this->load->view('proposal/proposal_navigation_tabs'); ?>
										</div>

										<div class="all_user-section2 floating_set brtrcls">
											<div class="tab-content proposal-dashboardtt first-tab-a05 year_aligned">
												<div id="allusers" class="tab-pane fade active">
													<div class="year-status" style="margin-top: 20px !important;">
														<!--  <span class="lt-year">last year</span> -->

														<ul class="status-list statuscls">
															<li><a href="javascript:;" class="all" id="all"> All </a></li>
															<li><a href="javascript:;" class="week" id="week"> week </a></li>
															<li><a href="javascript:;" class="three_week" id="three_week">3 week</a></li>
															<li><a href="javascript:;" class="month" id="month"> month </a></li>
															<li><a href="javascript:;" class="Three_month" id="Three_month"> 3 months </a></li>
														</ul>

														<div class="date-filterop">
															<input type="text" id="datewise_filter" name="de" placeholder="From Date" class="date-picker" value="<?php echo (new DateTime())->format('d-m-Y'); ?>">
															<input type="text" id="datewise_filter1" name="de" placeholder="To Date" class="date-picker" value="">
															<button type="button" class="btn btn-primary searchclsnew" id="search">
																<i class="fa fa-search"></i>
															</button>
															<!--  <li>year</li> -->
														</div>

													</div>
													<div class="success-data innertabcls">
														<div class="inner-tab" id="tabs12">
															<div class="proposal-commontab col-md-12">
																<div class="delbuttoncls assign_delete">
																	<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
																		<button type="button" id="delete_proposal_records" class="delete_proposal_records del-tsk12 f-right" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>
																	<?php } ?>
																	<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == 1) { ?>
																		<button type="button" id="archive_proposal_records" class="del-tsk12 f-right" style="display: none;"><i class="fa fa-archive fa-6" aria-hidden="true"></i>Archive</button>
																		<button type="button" id="unarchive_proposal_records" class="del-tsk12 f-right" style="display: none;"><i class="fa fa-archive fa-6" aria-hidden="true"></i>Un Archive</button>
																	<?php } ?>
																</div>
																<div class="tab-content common_currency">
																	<ul class="nav nav-tabs md-tabs proposalulcls proposal_tab new_aug5" id="proposal_dashboard" style="margin-top: 10px;top: 80px;left: 800px; margin-left: 15px;">

																		<li class="nav-item">
																			<a class="nav-link sent_result all cor1 status_tab active" href="javascript:;" data-id="all" data-searchCol="status_TH" data-toggle="tab">
																				<input type="hidden" name="proposal_status" id="status_id" value="0">
																				<h4 class="value-text value-textnew">
																					All
																					<span class="inner-value inner-valuenew"><?php echo count($proposal); ?></span>
																				</h4>
																			</a>
																		</li>
																		<li class="nav-item">
																			<a class="nav-link sent_result sent cor1 status_tab" href="javascript:;" data-id="Sent" data-searchCol="status_TH" data-toggle="tab">
																				<input type="hidden" name="proposal_status" id="status_id" value="1">
																				<h4 class="value-text value-textnew">
																					sent
																					<span class="inner-value inner-valuenew"><?php echo $send_count; ?></span>
																				</h4>

																			</a>
																		</li>
																		<li class="nav-item">
																			<a class="nav-link view cor2 status_tab" href="javascript:;" data-toggle="tab" data-id="Viewed" data-searchCol="status_TH">
																				<input type="hidden" name="proposal_status" id="status_id" value="2">
																				<h4 class="value-text v1 value-textnew">
																					viewed
																					<span class="inner-value inner-valuenew"><?php echo $view_count; ?></span>
																				</h4>

																			</a>
																		</li>
																		<li class="nav-item">
																			<a class="nav-link indiscussion cor3 status_tab" href="javascript:;" data-toggle="tab" data-id="Discussion" data-searchCol="status_TH">
																				<input type="hidden" name="proposal_status" id="status_id" value="3">
																				<h4 class="value-text v2 value-textnew">
																					discussion
																					<span class="inner-value inner-valuenew"><?php echo $indiscussion_count; ?></span>
																				</h4>

																			</a>
																		</li>
																		<li class="nav-item">
																			<a class="nav-link accept cor4 status_tab" href="javascript:;" data-toggle="tab" data-id="Accepted" data-searchCol="status_TH">
																				<input type="hidden" name="proposal_status" id="status_id" value="4">
																				<h4 class="value-text v3 value-textnew">
																					accepted
																					<span class="inner-value inner-valuenew"><?php echo $accepted_count; ?></span>
																				</h4>

																			</a>
																		</li>
																		<li class="nav-item">

																			<a class="nav-link decline cor5 status_tab" href="javascript:;" data-toggle="tab" data-id="Declined" data-searchCol="status_TH">
																				<input type="hidden" name="proposal_status" id="status_id" value="5">
																				<h4 class="value-text v4 value-textnew">
																					declined
																					<span class="inner-value inner-valuenew status_filter" data-id="<?php if (isset($admin_settings['crm_currency'])) {
																																						echo $currency_symbols[$admin_settings['crm_currency']];
																																					} ?>"><?php echo $decline_count; ?></span>
																				</h4>
																			</a>
																		</li>
																		<li class="nav-item">
																			<a class="nav-link draft cor6 status_tab" href="javascript:;" data-toggle="tab" data-id="Draft" data-searchCol="status_TH">
																				<input type="hidden" name="proposal_status" id="status_id" value="6">
																				<h4 class="value-text v5 value-textnew">
																					draft
																					<span class="inner-value inner-valuenew"><?php echo $draft_count; ?></span>
																				</h4>
																			</a>
																		</li>
																		<li class="nav-item">
																			<a class="nav-link draft cor6 status_tab" href="javascript:;" data-toggle="tab" data-id="Archive" data-searchCol="status_TH">
																				<input type="hidden" name="proposal_status" id="status_id" value="6">
																				<h4 class="value-text v5 value-textnew">
																					Archive
																					<span class="inner-value inner-valuenew"><?php echo $archive_count; ?></span>
																				</h4>
																			</a>
																		</li>
																	</ul>
																</div>
															</div>

															<div class="tab-content <?php if ($_SESSION['permission']['Proposal_Dashboad']['view'] != 1) { ?> permission_deined <?php
																																										} ?>">
																<div id="sent" class="proposal_send tab-pane fade active">

																	<!-- For filter section -->
																	<div class="filter-data for-reportdash for_clr_reportdash">
																		<div class="filter-head">
																			<h4>FILTERED DATA</h4>
																			<button class="btn btn-danger f-right" id="clear_container">clear</button>
																			<div id="container2" class="panel-body box-container">
																			</div>
																		</div>
																	</div>
																	<!-- For filter section -->

																	<div class="proposal-info">
																		<div class="sample_section sample_sectioncls">
																			<!--   <button type="button" class="print_button btn btn-primary" id="send_table_print_report">Print</button> -->
																			<div class="export_option">
																				<div class="btn-group e-dash sve-action1 frpropos">
																					<!--  <button type="button" class="btn btn-default"><a href="javascript:;" id="sent" class="change" data-toggle="modal" data-target="#Send_proposal">Select</a></button> -->
																					<button type="button" class="btn btn-default dropdown-toggle emp-dash" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																						select
																					</button>
																					<ul class="dropdown-menu">
																						<li id="buttons"></li>
																					</ul>
																				</div>
																			</div>
																		</div>
																		<table id="send_tables" class="display nowrap" style="width:100%">
																			<thead>
																				<tr class="table-header">
																					<th class="propsal_chk_TH Exc-colvis">
																						<label class="custom_checkbox1">
																							<input type="checkbox" id="select_all_proposal">
																							<i></i>
																						</label>
																					</th>
																					<th class="sent_display1 send_propsal_TH hasFilter">Proposals
																						<img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
																						<div class="sortMask"></div>
																					</th>

																					<th class="total_cost_TH hasFilter">Total Cost
																						<img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
																						<div class="sortMask"></div>
																					</th>

																					<th class="created_by_TH hasFilter">Created By
																						<img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
																						<div class="sortMask"></div>
																					</th>

																					<th class="send_on_TH hasFilter">Sent On
																						<img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
																						<div class="sortMask"></div>
																					</th>

																					<th class="status_TH hasFilter">Status
																						<img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
																						<div class="sortMask"></div>
																					</th>
																					<th class="action_TH Exc-colvis"> Actions </th>
																				</tr>
																			</thead>

																			<tbody>
																				<?php
																				foreach ($proposal as $key => $value) { ?>
																					<tr id="<?php echo $value['id']; ?>">
																						<td>
																							<label class="custom_checkbox1">
																								<input type="checkbox" class="proposal_checkbox sent_checkbox" data-proposal-id="<?php echo $value['id']; ?>" value="<?php echo $value['id']; ?>">
																								<i></i>
																							</label>
																						</td>
																						<td class="data-search" data-search=" <?php echo $value['proposal_name'];
																																if ($value['company_id'] == 1) {
																																	echo $value['receiver_mail_id'];
																																} else {
																																	echo $value['company_name'];
																																} ?>">
																							<div class="first-info">
																								<span class="info-num"><?php echo $value['proposal_no'] ?></span>

																								<?php $randomstring = generateRandomString('100');
																								if ($_SESSION['permission']['Proposal_Dashboad']['view'] == 1) {
																									$href = base_url() . "proposal_page/step_proposal/" . $randomstring . "---" . $value['id'];
																								} else {
																									$href = "javascript:;";
																								}
																								?>
																								<span class="proposal name">
																									<p>
																										<a href="<?php echo $href; ?>">
																											<?php echo $value['proposal_name']; ?>
																										</a>
																									</p>
																								</span>
																							</div>
																							<?php if ($value['company_id'] == 1) { ?>
																								<p class="com-name"><?php echo $value['receiver_mail_id']; ?> </p>
																							<?php } else { ?>
																								<p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $value['company_id']; ?>" target="_blank"><?php echo $value['company_name']; ?> </a></p>
																							<?php   } ?>
																						</td>
																						<td data-search="<?php echo $value['grand_total']; ?>">
																							<span class="amount">
																								<i class="fa fa-gbp" aria-hidden="true"></i>
																								<?php
																								//   $ex_price=explode(',',$value['price']);
																								//   $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));
																								echo $value['grand_total'];
																								?>
																							</span>
																						</td>
																						<td data-search="<?php if ($value['sender_company_name'] == '') {
																												echo 'accountants & tax consultants';
																											} else {
																												echo $value['sender_company_name'];
																											} ?>">
																							<div class="acc-tax">
																								<?php if ($value['sender_company_name'] == '') { ?>
																									<p>accountants & tax consultants</p>
																								<?php } else { ?>
																									<p><?php echo $value['sender_company_name']; ?></p>
																								<?php } ?>
																							</div>
																						</td>
																						<?php //echo $value['created_at'];
																						$timestamp = strtotime($value['created_at']);
																						$newDate = date('d-F-Y H:i', $timestamp);

																						?>
																						<td class="send_td" data-search="" data-sort="<?php echo $newDate; ?>">
																							<span class="sent-month">
																								<?php //echo $value['created_at'];
																								echo $newDate; //outputs 02-March-2011
																								?>
																							</span>
																						</td>

																						<?php
																						$status = $value['status'];
																						if ($status == 'sent') {
																							$res = 'Sent';
																						}
																						if ($status == 'opened') {
																							$res = 'viewed';
																						} else {
																							$res = $status;
																						}
																						$res = ucfirst($res);
																						?>
																						<td data-search="<?php echo $res; ?> ">
																							<div class="sentclsbutton">
																								<i class="fa fa-check" aria-hidden="true"></i>
																								<span class="accept"><?php echo $res; ?></span>
																							</div>
																						</td>

																						<td>
																							<div class="sentclsbuttonbot">
																								<div class="dropdown">
																									<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																										<span class="dropdwn-action"></span><span class="dropdwn-action"></span><span class="dropdwn-action"></span>
																									</button>
																									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																										<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == 1) { ?>

																											<a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option"><i class="icofont icofont-edit" aria-hidden="true"></i></a>

																											<?php if ($value['status'] != 'archive') { ?>
																												<a href="javascript:void(0);" id="<?php echo $value['id']; ?>" class="edit-option archived"><i class="fa fa-file-archive-o" aria-hidden="true"></i></a>
																											<?php } else if ($value['status'] == 'archive') { ?>
																												<a href="javascript:void(0);" id="<?php echo $value['id']; ?>" class="edit-option unarchive"><i class="fa fa-file-zip-o" aria-hidden="true"></i></a>
																										<?php }
																										} ?>
																										<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
																											<a href="javascript:;" id="<?php echo $value['id']; ?>" class="edit-option deleteproposals"><i class="icofont icofont-ui-delete" aria-hidden="true"></i></a>
																										<?php } ?>
																										<?php if ($_SESSION['permission']['Proposal_Dashboad']['view'] == '1' && $res != 'draft') {
																											$link = base_url() . 'proposal/proposal/' . $randomstring . '---' . $value['id'];
																										?>
																											<a class="edit-option copy_model" data-href="<?php echo $link; ?>" data-toggle="modal" data-target="#copy_model" title="Get Proposal Link"><i class="fa fa-link" aria-hidden="true"></i></a>
																										<?php } ?>
																									</div>
																								</div>
																							</div>
																						</td>
																					</tr>
																				<?php
																				}
																				?>
																			</tbody>
																		</table>
																		<input type="hidden" class="rows_selected" id="select_proposal_count">
																	</div>
																</div>
															</div>
															<!-- end of draft section -->
														</div>
													</div>
												</div>



											</div>
										</div>
										<!-- admin close -->
									</div>

								</div>
							</div>
						</div>
					</div>
					<!-- Register your self card end -->

					<!-- Page body end -->
				</div>
			</div>

		</div>
	</div>
</div>


<div class="modal fade" id="conform_model" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="archive_ids" id="archive_ids" value="">
					<p>Do you want to archive ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" style="display:none" id="confirm_archive" onclick="archive_action()">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="delete_all_proposal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="delete_ids" id="delete_ids" value="">
				<p>Do you want to delete ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default delete_yes">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="copy_model" role="dialog">
	<div class="modal-dialog modal-copy-prop">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Copy The Link&nbsp;&nbsp;<a class="edit-option copy_clipboard"><i class="fa fa-clipboard" aria-hidden="true"></i></a></h4>
			</div>
			<div class="modal-body">
				<textarea type="text" id="copy_clipboard"></textarea>
			</div>
		</div>
	</div>
</div>

<!-- Warning Section Ends -->
<?php $this->load->view('includes/footer'); ?>

<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https:////cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/user_page/materialize.js"></script>


<!-- for drag and drop -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Sortable.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sortable-custom.js"></script>

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/custom/datatable_extension.js"></script>
<script src="<?php echo base_url() ?>assets/js/custom/buttons.colVis.js"></script>

<!-- For Date fields sorting  -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/custom/datatable_cus_sorting.js"></script>
<!-- For Date fields sorting  -->

<!-- end of drag and drop -->
<script type="text/javascript">
	$(document).ready(function() {
		window.select_wrapper = [];

		$(document).on('click', '.themicond,.export_option .frpropos button:first', function() {
			var table = $('.tab-content div .active table').attr('id');

			if (!$('#' + table).hasClass('download') && $(this).hasClass('emp-dash')) {
				$('#' + table).addClass('download');
				$('#' + table).find('thead th').each(function() {
					select_wrapper.push($(this).find('.select-wrapper').html());
					$(this).find('.select-wrapper').html('');
				});
				setTimeout(function() {}, 1000);
			} else if ($(this).hasClass('themicond') && select_wrapper != "" && select_wrapper != null) {
				var indice = 0;
				$('#' + table).find('thead th').each(function() {
					if (select_wrapper[indice] != 'undefined' && $(this).find('.select-wrapper').html() == "") {
						$(this).find('.select-wrapper').html(select_wrapper[indice]);
					}
					indice++;
				});

				if ($('#' + table).hasClass('download')) {
					$('#' + table).removeClass('download');
					$(this).trigger('click');
				}
			}
		});
	});

	var send_table;
	var pageLength = <?php echo get_firm_page_length() ?>;
	$.fn.dataTable.moment('DD-MMMM-YYYY HH:mm');

	$(document).on('click', '.Settings_Button', AddClass_SettingPopup);
	$(document).on('click', '.Button_List', AddClass_SettingPopup);

	$(document).on('click', '.dt-button.close', function() {
		$('.dt-buttons').find('.dt-button-collection').detach();
		$('.dt-buttons').find('.dt-button-background').detach();
	});

	function AddClass_SettingPopup() {
		$('button.Button_List').closest('.dt-button-collection').addClass('Settings_List');
		$('button.buttons-columnVisibility').closest('.dt-button-collection').addClass('columnVisibility_List');
	}

	function Trigger_To_Reset_Filter() {

		$('#select_all_proposal').prop('checked', false);
		$('#select_all_proposal').trigger('change');


		$('div.toolbar-table .toolbar-search li.dropdown-chose').each(function() {
			$(this).trigger('click');
		});


		MainStatus_Tab = "all_task";

		Redraw_Table(send_table);

		// Redraw_Table(TaskTable_Instance);
	}

	function toggle_action_button(i, table = '') {
		if (i) {
			$("#delete_proposal_records").show();

			if (table == 'Archive') {
				$("#unarchive_proposal_records").show();
			} else {
				$("#archive_proposal_records").show();
			}
		} else {
			$("#delete_proposal_records").hide();

			if (table == 'Archive') {
				$("#unarchive_proposal_records").hide();
			} else {
				$("#archive_proposal_records").hide();
			}
		}
	}



	function get_selected_rows() {
		var ids = [];
		send_table.column('.propsal_chk_TH', {
			search: 'applied'
		}).nodes().to$().each(function(index) {
			if ($(this).find(".proposal_checkbox").is(":checked")) {
				ids.push($(this).find(".proposal_checkbox").val());
			}
		});
		return ids;
	}

	function delete_proposal(ids) {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url() . 'proposal_page/deleteRecords'; ?>",
			cache: false,
			data: {
				'data_id': ids
			},
			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(data) {
				$(".LoadingImage").hide();

				if (data) {
					$(".alert-success-delete").show();
					setTimeout(function() {
						location.reload();
					}, 2000);
				}
			}
		});
	}

	function archive_proposal(ids) {
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: "<?php echo base_url() . 'Proposal_pdf/Archive_status'; ?>",
			data: {
				"id": ids
			},
			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(data) {
				if (data != 0) {
					$(".LoadingImage").hide();
					$('.alert-success1').show();
					$('.alert-success1 .position-alert1').text('Archived Successfully..');
					setTimeout(function() {
						$('.alert-success1').hide();
						location.reload();
					}, 1000);
				}
			}
		});
	}

	function unarchive_proposal(ids) {
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: "<?php echo base_url() . 'Proposal_pdf/UnArchive_status'; ?>",
			data: {
				"id": ids
			},
			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(data) {
				if (data != 0) {
					$(".LoadingImage").hide();
					$('.alert-success1').show();
					$('.alert-success1 .position-alert1').text('Unarchived Successfully..');
					setTimeout(function() {
						$('.alert-success1').hide();
						location.reload();
					}, 1000);
				}
			}
		});
	}

	/////////////archive functionality 
	function send_table_section(response = null) {


		<?php
		$column_setting = Firm_column_settings('proposal');
		?>

		if (response == null) {
			var column_order = <?php echo $column_setting['order'] ?>;

			var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;
		} else {
			/*   var  order=[];
           var hidden=[];
     
     JSON.parse(response.order, (key, value) => {

      order.push(value);
     
     });
       JSON.parse(response.hidden, (key, value) => {
        hidden.push(value);
     
     });
        var column_order = order.slice(0, -1);
        var hidden_coulmns =  hidden.slice(0, -1);
        */
			var column_order = JSON.parse(response.order);
			var hidden_coulmns = JSON.parse(response.hidden);


		}
		console.log(column_order);

		var column_ordering = [];

		$.each(column_order, function(i, v) {

			var index = $('#send_tables thead th').index($('.' + v));

			if (index !== -1) {
				column_ordering.push(index);
			}
		});

		//var numCols = $('#send_tables thead th').length;   
		// alert(numCols);
		// alert(column_order.length);
		send_table = $('#send_tables').DataTable({
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"pageLength": pageLength,
			"dom": '<"toolbar-table" B>lfrtip',
			buttons: [{
				extend: 'collection',
				background: false,
				text: '<i class="fa fa-cog" aria-hidden="true"></i>',
				className: 'Settings_Button',
				buttons: [{
						text: '<i class="fa fa-filter" aria-hidden="true"></i><a id="reset_filter_button" class="li_send">Reset Filter</a>',
						className: 'Button_List',
					},
					{
						extend: 'colvis',
						text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
						columns: ':not(.Exc-colvis)',
						className: 'Button_List',
						prefixButtons: [{
							text: 'X',
							className: 'close'
						}]
					}
				]
			}],
			order: [],
			columnDefs: [{
				"orderable": false,
				"targets": ['propsal_chk_TH', 'action_TH']
			}],
			colReorder: {
				realtime: false,
				order: column_ordering,
				fixedColumnsLeft: 1,
				fixedColumnsRight: 1
			},
			"iDisplayLength": 10,
			initComplete: AddColumn_Filters
		});



		ColVis_Hide(send_table, hidden_coulmns);

		Filter_IconWrap();

		Change_Sorting_Event(send_table);

		ColReorder_Backend_Update(send_table, 'proposal');

		ColVis_Backend_Update(send_table, 'proposal');

		$(document).on('click', '#sent .filter-data.for-reportdash #clear_container', function() {
			Redraw_Table(send_table);
		});

		var buttons = new $.fn.dataTable.Buttons(send_table, {
			buttons: [{
				extend: 'pdf',
				title: 'Proposal(s)',
				filename: 'Proposal',
				exportOptions: {
					columns: ':not(.Exc-colvis)',
					//columns: "thead th:not(.noExport)"
				}
			}, {
				extend: 'excel',
				title: 'Proposal(s)',
				filename: 'Proposal',
				extension: '.xls',
				exportOptions: {
					columns: ':not(.Exc-colvis)',
				}
			}, {
				extend: 'csv',
				filename: 'Proposal',
				exportOptions: {
					columns: ':not(.Exc-colvis)',
				}
			}, {
				extend: 'print',
				title: 'Proposal(s)',
				exportOptions: {
					columns: ':not(.Exc-colvis)',
				}
			}]
		}).container().appendTo($('#buttons'));


	}
	/*  $('.toolbar-table select').on('change',function(){
	  if( $(this).attr('id') != 'export_report' )
	  { 
	    var search =  $(this).val(); 

	    if(search.length) search= '^('+search.join('|') +')$'; 

	    var col = $.trim( $(this).attr('data-searchCol') );

	    //console.log(col+"value"+val);

	    table.column('.'+col).search(search,true,false).draw();

	  }
	});*/

	$(document).ready(function() {


		$(".date-picker").datepicker({
			dateFormat: 'dd-mm-yy'
		}).val();


		$(document).on('click', '.status_tab', function() {

			var col = $.trim($(this).attr('data-searchCol'));
			var val = $.trim($(this).attr('data-id'));

			Trigger_To_Reset_Filter();

			MainStatus_Tab = val;

			$('.status_tab').removeClass('active');
			$(this).addClass('active');

			if (val == "all") {
				return;
			} else {
				send_table.column('.' + col).search(val, true, false).draw();
			}

		});


		send_table_section();

		$(document).on('click', '.archived, #archive_proposal_records', function() {
			var ids = [];
			if ($(this).hasClass('archived')) {
				ids.push($(this).attr('id'));
			} else {
				ids = get_selected_rows();
			}

			var action = function() {
				archive_proposal(ids);
				$('#Confirmation_popup').modal('hide');
			};
			Conf_Confirm_Popup({
				'OkButton': {
					'Handler': action
				},
				'Info': 'Do you want to archive this proposal?'
			});
			$('#Confirmation_popup').modal('show');
		});


		$(document).on('click', '.deleteproposals, #delete_proposal_records', function() {

			var ids = [];
			if ($(this).hasClass('deleteproposals')) {
				ids.push($(this).attr('id'));
			} else {
				ids = get_selected_rows();
			}

			var action = function() {
				delete_proposal(ids);
				$('#Confirmation_popup').modal('hide');
			};
			Conf_Confirm_Popup({
				'OkButton': {
					'Handler': action
				},
				'Info': 'Do you want to delete this proposal?'
			});
			$('#Confirmation_popup').modal('show');
		});

		$(document).on('click', '.unarchive,#unarchive_proposal_records', function() {
			var ids = [];
			if ($(this).hasClass('unarchive')) {
				ids.push($(this).attr('id'));
			} else {
				ids = get_selected_rows();
			}

			var action = function() {
				unarchive_proposal(ids);
				$('#Confirmation_popup').modal('hide');
			};
			Conf_Confirm_Popup({
				'OkButton': {
					'Handler': action
				},
				'Info': 'Do you want unarchive this proposal?'
			});
			$('#Confirmation_popup').modal('show');
		});

		$(document).on("change", "#select_all_proposal", function(event) {

			var IsArchive = $('.status_tab.active').attr("data-id").trim();

			var checked = this.checked;
			$(".LoadingImage").show();
			var Selected_Num = 0;
			send_table.column('.propsal_chk_TH').nodes().to$().each(function(index) {
				if (checked) {
					Selected_Num++;
				}
				$(this).find('.proposal_checkbox').prop('checked', checked);
				toggle_action_button(checked, IsArchive);
			});
			$(".LoadingImage").hide();
		});

		$(document).on("change", ".proposal_checkbox", function(event) {
			var IsArchive = $('.status_tab.active').attr("data-id").trim();

			var tr = 0;
			var ck_tr = 0;
			send_table.column('.propsal_chk_TH').nodes().to$().each(function(index) {
				if ($(this).find(".proposal_checkbox").is(":checked")) {
					ck_tr++;
				}
				tr++;
			});

			if (tr == ck_tr) {
				$("#select_all_proposal").prop("checked", true);
			} else {
				$("#select_all_proposal").prop("checked", false);
			}

			if (ck_tr) {
				toggle_action_button(1, IsArchive);
			} else {
				toggle_action_button(0, IsArchive);
			}

		});






		var status_ID = '0';
		$("#all").click(function() {

			$(".status-list li").removeClass("highlight");
			$(this).closest('li').addClass('highlight');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() . 'Proposal/all_data'; ?>",
				data: {
					"status_ID": status_ID
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					$('.success-data').html('');
					$('.success-data').append(data);
					var table_name = 'proposal';

					var all_Codes = $.ajax({
						type: "POST",
						dataType: "json",
						data: {
							table_name: table_name
						},
						url: "<?= base_url() ?>leads/get_hidden_data",
						async: false
					}).responseJSON;

					console.log(all_Codes);

					// initialize_datatable(all_Codes);

					send_table_section(all_Codes);
					$("li a.all.status_tab").trigger('click');


				}
			});
		});
		$("#week").click(function() {

			$(".status-list li").removeClass("highlight");
			$(this).closest('li').addClass('highlight');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() . 'Proposal/checkdatetime_weeks'; ?>",
				data: {
					"status_ID": status_ID
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					$('.success-data').html('');
					$('.success-data').append(data);

					var table_name = 'proposal';

					var all_Codes = $.ajax({
						type: "POST",
						dataType: "json",
						data: {
							table_name: table_name
						},
						url: "<?= base_url() ?>leads/get_hidden_data",
						async: false
					}).responseJSON;

					console.log(all_Codes);

					send_table_section(all_Codes);
					$("li a.all.status_tab").trigger('click');


				}
			});
		});

		$("#three_week").click(function() {

			$(".status-list li").removeClass("highlight");
			$(this).closest('li').addClass('highlight');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() . 'Proposal/CheckdateTime_threeweeks'; ?>",
				data: {
					"status_ID": status_ID
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					$('.success-data').html('');
					$('.success-data').append(data);

					var table_name = 'proposal';

					var all_Codes = $.ajax({
						type: "POST",
						dataType: "json",
						data: {
							table_name: table_name
						},
						url: "<?= base_url() ?>leads/get_hidden_data",
						async: false
					}).responseJSON;

					console.log(all_Codes);

					// initialize_datatable(all_Codes);

					send_table_section(all_Codes);
					$("li a.all.status_tab").trigger('click');


				}
			});
		});

		$("#month").click(function() {

			$(".status-list li").removeClass("highlight");
			$(this).closest('li').addClass('highlight');
			$.ajax({
				type: "POST",
				// dataType: "JSON",
				url: "<?php echo base_url() . 'Proposal/CheckdateTime_Month'; ?>",
				data: {
					"status_ID": status_ID
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					$('.success-data').html('');
					$('.success-data').append(data);
					var table_name = 'proposal';

					var all_Codes = $.ajax({
						type: "POST",
						dataType: "json",
						data: {
							table_name: table_name
						},
						url: "<?= base_url() ?>leads/get_hidden_data",
						async: false
					}).responseJSON;

					console.log(all_Codes);


					send_table_section(all_Codes);
					$("li a.all.status_tab").trigger('click');


				}
			});
		});

		$("#Three_month").click(function() {

			$(".status-list li").removeClass("highlight");
			$(this).closest('li').addClass('highlight');
			$.ajax({
				type: "POST",
				// dataType: "JSON",
				url: "<?php echo base_url() . 'Proposal/CheckdateTime_threeMonth'; ?>",
				data: {
					"status_ID": status_ID
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					$('.success-data').html('');
					$('.success-data').append(data);
					var table_name = 'proposal';

					var all_Codes = $.ajax({
						type: "POST",
						dataType: "json",
						data: {
							table_name: table_name
						},
						url: "<?= base_url() ?>leads/get_hidden_data",
						async: false
					}).responseJSON;

					console.log(all_Codes);


					send_table_section(all_Codes);
					$("li a.all.status_tab").trigger('click');

				}
			});
		});

		$(document).on('click', '#close', function(e) {
			$('.alert-success').hide();
			return false;
		});

		/* Date Filter */
		$("#search").click(function() {

			var fromdate = $("#datewise_filter").val();
			var todate = $("#datewise_filter1").val();
			$.ajax({
				type: "POST",
				// dataType: "JSON",
				url: "<?php echo base_url() . 'Proposal/Date_filter'; ?>",
				data: {
					"fromdate": fromdate,
					'todate': todate
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					// console.log(data);
					$('.success-data').html('');
					$('.success-data').append(data);
					var table_name = 'proposal';

					var all_Codes = $.ajax({
						type: "POST",
						dataType: "json",
						data: {
							table_name: table_name
						},
						url: "<?= base_url() ?>leads/get_hidden_data",
						async: false
					}).responseJSON;

					console.log(all_Codes);

					// initialize_datatable(all_Codes);

					send_table_section(all_Codes);
					$("li a.all.status_tab").trigger('click');

				}
			});
		});

		<?php
		if (isset($_SESSION['firm_seen'])) {
			if ($_SESSION['firm_seen'] == 'draft') { ?>
				// alert('work');
				$("li a.draft").trigger('click');
			<?php } ?>

			<?php
			if ($_SESSION['firm_seen'] == 'sent') { ?>
				//   alert('work');
				$("li a.sent").trigger('click');
			<?php } ?>

			<?php
			if ($_SESSION['firm_seen'] == 'indiscussion') { ?>
				//   alert('work');
				$("li a.discussion").trigger('click');
			<?php } ?>

			<?php
			if ($_SESSION['firm_seen'] == 'view') { ?>
				$("li a.view").trigger('click');
			<?php } ?>
			<?php
			if ($_SESSION['firm_seen'] == 'accept') { ?>
				$("li a.accept").trigger('click');
			<?php } ?>
			<?php
			if ($_SESSION['firm_seen'] == 'decline') { ?>
				$("li.decline").trigger('click');
			<?php } ?>
		<?php } ?>


	});

	$('.copy_model').click(function() {
		$('#copy_clipboard').val('');
		$('#copy_clipboard').val($(this).data('href'));
	});

	$('.copy_clipboard').click(function() {
		/* Get the text field */
		var copyElement = document.getElementById('copy_clipboard');
		/* Select the text field */
		copyElement.select();
		copyElement.setSelectionRange(0, 99999); /*For mobile devices*/

		/* Copy the text inside the text field */

		document.execCommand('copy');

		$('#copy_model').find('.close').trigger('click');
	});


	/*
	   $(document).on('click',".nav-item",function()
	   {
	          $("#delete_ids").val('');   
	          allproposal = [];
	          $('.custom_checkbox1 input[type="checkbox"]').each(function()
	          {
	              $(this).prop('checked',false);
	          });
	          $(".delete_proposal_records").hide();
	          $("#archive_proposal_records").hide();

	   });*/
	/* Date Filter */
	/* JS by Ram  for Tag serach*/





	/* Shashmethah */

	/* function changeTabIntially(obj)
	 {
	    var div_id = $(obj).data('id');    

	    var li = $('#proposal_dashboard li'); 

	    $(li).each(function(index) 
	    {
	        var a = $(this).find('a');
	        $(a).removeClass('active');
	        $(a).attr('aria-expanded','false');
	    });    
	    
	    $('.tab-content div #sent').removeClass('active');
	    $('.tab-content div #viewed').removeClass('active');
	    $('.tab-content div #discussion').removeClass('active');
	    $('.tab-content div #accepted').removeClass('active');
	    $('.tab-content div #declined').removeClass('active');
	    $('.tab-content div #draft').removeClass('active');       

	    $(obj).addClass('active'); 
	    $(obj).attr('aria-expanded','true');  
	    $('.tab-content div #'+div_id).addClass('active');      
	 }  

	 $(document).on('click','#reset_filter_button',function()
	 {
	     var reset_li = $(this).attr('class');
	     reset_li = reset_li.replace('li_','');
	     reset_li.trim();
	     var tableid = '#'+reset_li.concat('_tables'); 
	     reset_li = reset_li.concat('_tables_filter');        
	     $('#'+reset_li).find('input[type="search"]').val(""); 
	     var table = $(tableid).DataTable();
	     Redraw_Table(table);
	 });

	  $(document).ready(function()
	  {
	    window.allproposal = []; 
	  });*/
	/*
	     function getSelectedRow(id)
	     {              
	        allproposal.push(id);   
	        return allproposal;
	     }

	     function return_indice(id)
	     { 
	        for(var i=0;i<allproposal.length;i++)
	        {
	           if(id == allproposal[i])
	           {
	              return i;
	           }          
	        }
	     }

	     function dounset(id)
	     {  
	        var indice = return_indice(id);
	         
	        if(indice > -1) 
	        {
	           allproposal.splice(indice,1);
	        }
	    
	        return allproposal;
	     }

	    $('.custom_checkbox1 input[type="checkbox"]').click(function()
	    {  
	        var wholeid;
	        var whole_id;
	        var all_prop;

	        if($(this).attr('id')) 
	        {      
	          whole_id = $(this).attr('id');
	          wholeid = whole_id.search('select_all'); 
	        }       

	        if(wholeid != 0)
	        { 
	            var id = $(this).data('proposal-id');

	            if($(this).is(":checked"))
	            {         
	               all_prop = getSelectedRow(id); 
	               $("#delete_ids").val(JSON.stringify(all_prop));     
	            }
	            else
	            {  
	               var props = dounset(id); 
	               $("#delete_ids").val(JSON.stringify(props));  
	            }   
	        }
	        else if(wholeid == 0)
	        { 
	           if($('#'+whole_id).is(':checked'))
	           { 
	              var tbl = $('#'+whole_id).parent('label').parent('th').parent('tr').parent('thead').parent('table').attr('id');

	              $('#'+tbl+' tbody tr').each(function(index)
	              {
	                  var tr_id = $(this).attr('id');
	                  all_prop = getSelectedRow(tr_id);                  
	              });

	              $("#delete_ids").val(JSON.stringify(all_prop));
	           }
	           else
	           { 
	              allproposal = [];
	              $("#delete_ids").val('');
	           }
	        }   
	    });  

	     */

	/*  $(document).on('change','#col_visibility',function()
	  {
	       var th = $(this).val();

	       if(th!="")
	       {
	           var vis_divid = $('div .tab-content').find('div .active').attr('id'); 
	           var tab_id = $('#'+vis_divid+' table').attr('id'); 

	         alert($('#'+tab_id).find('th').attr('colspan','2').attr('class'));
	       }
	  });*/

	/* Shashmethah */
</script>