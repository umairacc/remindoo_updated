<!--- Send Info  -->
<?php  if(isset($filter_sendInfo)) { ?>
<table>
 <tbody>
 
 	<?php foreach ($filter_sendInfo as $key => $value) { ?>
		<tr>
			
			<td><div class="first-info">
		          <span class="info-num"><?php echo $value['proposal_no'] ?></span>
		          <p><?php echo $value['proposal_name'];?></p>
		          <a href="javascript:;" class="edit-option">[edit]</a>
		          <a href="javascript:;" class="edit-option">[archive]</a>
		        </div>
		        <p class="com-name"><a href="javascript:;"><?php echo $value['company_name'];?> </a>(slivery khagram)</p></td>
		    <td><span class="amount"><i class="fa fa-gbp" aria-hidden="true"></i><?php 
                $ex_price=explode(',',$value['price']);
                $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));
                echo $integerIDs;?></span></td>
            <td>
              <div class="acc-tax">
                <span class="tax-inner">accotax</span>
                <p>accountants & tax consultants</p>
              </div>
            </td>  
            <td class="send_td">
              <span class="sent-month">
              <?php //echo $value['created_at'];
                $timestamp = strtotime($value['created_at']);
                $newDate = date('d F Y H:i', $timestamp); 
                echo $newDate; //outputs 02-March-2011
            ?>
              </span>                                     
            </td>  
            <td>
              <i class="fa fa-check" aria-hidden="true"></i>
              <span class="accept"><?php 
              $status=$value['status'];
              if($status=='send')
              {
                $res='sent';
              }
              if($status=='opened')
              {
                $res='viewed';
              }
              else
              {
                $res=$status;
              }
              echo ucfirst($res);
              ?></span>

          <input type="hidden" class="status_send" id="status_send" value="<?php echo $value['status']; ?>">     
            </td>       
		</tr>
 <?php }  ?>	

 </tbody>		
</table>
<?php } ?>

<!--- Opened Info  -->

<?php  if(isset($filter_openedInfo)) { ?>
<table>
 <tbody>
 	<?php foreach ($filter_openedInfo as $key => $value) { ?>
		<tr>
	        <td><div class="first-info">
	          <span class="info-num"><?php echo $value['proposal_no'] ?></span>
	          <p><?php echo $value['proposal_name'];?></p>
	          <a href="javascript:;" class="edit-option">[edit]</a>
	          <a href="javascript:;" class="edit-option">[archive]</a>
	        </div>
	        <p class="com-name"><a href="javascript:;"><?php echo $value['company_name'];?> </a>(slivery khagram)</p>
	        </td>
            <td><span class="amount"><i class="fa fa-gbp" aria-hidden="true"></i><?php 
            $ex_price=explode(',',$value['price']);
            $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));
            echo $integerIDs;?></span></td>
            <td>
              <div class="acc-tax">
                <span class="tax-inner">accotax</span>
                <p>accountants & tax consultants</p>
              </div>
            </td>
            <td>
              <span class="sent-month">
              <?php //echo $value['created_at'];
                $timestamp = strtotime($value['created_at']);
                $newDate = date('d F Y H:i', $timestamp); 
                echo $newDate; //outputs 02-March-2011
            ?>
              </span>
            </td>
            <td>
              <i class="fa fa-check" aria-hidden="true"></i>
              <span class="accept"><?php 
              $status=$value['status'];
              if($status=='send')
              {
                $res='sent';
              }
              if($status=='opened')
              {
                $res='viewed';
              }
              else
              {
                $res=$status;
              }
              echo ucfirst($res);
              ?></span>
            </td>
        </tr>
	 <?php }  ?>
</tbody>
</table>
<?php } ?>	 	

<!--- in Discussion  -->

<?php  if(isset($filter_indiscussionInfo)) { ?>
<table>
 <tbody>
 	<?php foreach ($filter_indiscussionInfo as $key => $value) { ?>
		
	    <tr>
            <td><div class="first-info">
              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
              <p><?php echo $value['proposal_name'];?></p>
              <a href="javascript:;" class="edit-option">[edit]</a>
              <a href="javascript:;" class="edit-option">[archive]</a>
            </div>
            <p class="com-name"><a href="javascript:;"><?php echo $value['company_name'];?> </a>(slivery khagram)</p>
            </td>
            <td><span class="amount"><i class="fa fa-gbp" aria-hidden="true"></i><?php 
            $ex_price=explode(',',$value['price']);
            $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));
            echo $integerIDs;?></span></td>
            <td>
              <div class="acc-tax">
                <span class="tax-inner">accotax</span>
                <p>accountants & tax consultants</p>
              </div>
            </td>
            <td>
              <span class="sent-month">
              <?php //echo $value['created_at'];
                $timestamp = strtotime($value['created_at']);
                $newDate = date('d F Y H:i', $timestamp); 
                echo $newDate; //outputs 02-March-2011
            ?>
              </span>

            </td>
            <td>
              <i class="fa fa-check" aria-hidden="true"></i>
              <span class="accept"><?php 
              $status=$value['status'];
              if($status=='send')
              {
                $res='sent';
              }
              if($status=='opened')
              {
                $res='viewed';
              }
              else
              {
                $res=$status;
              }
              echo ucfirst($res);
              ?></span>
            </td>
        </tr>


        
	 <?php }  ?>
</tbody>
</table>
<?php } ?>

<!--- accept  -->

<?php  if(isset($filter_acceptInfo)) { ?>
<table>
 <tbody>
 	<?php foreach ($filter_acceptInfo as $key => $value) { ?>
		
	    <tr>
            <td><div class="first-info">
              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
              <p><?php echo $value['proposal_name'];?></p>
              <a href="javascript:;" class="edit-option">[edit]</a>
              <a href="javascript:;" class="edit-option">[archive]</a>
            </div>
            <p class="com-name"><a href="javascript:;"><?php echo $value['company_name'];?> </a>(slivery khagram)</p>
            </td>
            <td><span class="amount"><i class="fa fa-gbp" aria-hidden="true"></i><?php 
            $ex_price=explode(',',$value['price']);
            $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));
            echo $integerIDs;?></span></td>
            <td>
              <div class="acc-tax">
                <span class="tax-inner">accotax</span>
                <p>accountants & tax consultants</p>
              </div>
            </td>
            <td>
              <span class="sent-month">
              <?php //echo $value['created_at'];
                $timestamp = strtotime($value['created_at']);
                $newDate = date('d F Y H:i', $timestamp); 
                echo $newDate; //outputs 02-March-2011
            ?>
              </span>

            </td>
            <td>
              <i class="fa fa-check" aria-hidden="true"></i>
              <span class="accept"><?php 
              $status=$value['status'];
              if($status=='send')
              {
                $res='sent';
              }
              if($status=='opened')
              {
                $res='viewed';
              }
              else
              {
                $res=$status;
              }
              echo ucfirst($res);
              ?></span>
            </td>
        </tr> 
	 <?php }  ?>
</tbody>
</table>
<?php } ?>

<!--- decline  -->

<?php  if(isset($filter_declineInfo)) { ?>
<table>
 <tbody>
 	<?php foreach ($filter_declineInfo as $key => $value) { ?>
		
	    <tr>
            <td><div class="first-info">
              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
              <p><?php echo $value['proposal_name'];?></p>
              <a href="javascript:;" class="edit-option">[edit]</a>
              <a href="javascript:;" class="edit-option">[archive]</a>
            </div>
            <p class="com-name"><a href="javascript:;"><?php echo $value['company_name'];?> </a>(slivery khagram)</p>
            </td>
            <td><span class="amount"><i class="fa fa-gbp" aria-hidden="true"></i><?php 
            $ex_price=explode(',',$value['price']);
            $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));
            echo $integerIDs;?></span></td>
            <td>
              <div class="acc-tax">
                <span class="tax-inner">accotax</span>
                <p>accountants & tax consultants</p>
              </div>
            </td>
            <td>
              <span class="sent-month">
              <?php //echo $value['created_at'];
                $timestamp = strtotime($value['created_at']);
                $newDate = date('d F Y H:i', $timestamp); 
                echo $newDate; //outputs 02-March-2011
            ?>
              </span>

            </td>
            <td>
              <i class="fa fa-check" aria-hidden="true"></i>
              <span class="accept"><?php 
              $status=$value['status'];
              if($status=='send')
              {
                $res='sent';
              }
              if($status=='opened')
              {
                $res='viewed';
              }
              else
              {
                $res=$status;
              }
              echo ucfirst($res);
              ?></span>
            </td>
        </tr>
	 <?php }  ?>
</tbody>
</table>
<?php } ?>

<!--- draft  -->

<?php  if(isset($filter_draftInfo)) { ?>
<table>
 <tbody>
 	<?php foreach ($filter_draftInfo as $key => $value) { ?>
		
	    <tr>
            <td><div class="first-info">
              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
              <p><?php echo $value['proposal_name'];?></p>
              <a href="javascript:;" class="edit-option">[edit]</a>
              <a href="javascript:;" class="edit-option">[archive]</a>
            </div>
            <p class="com-name"><a href="javascript:;"><?php echo $value['company_name'];?> </a>(slivery khagram)</p>
            </td>
            <td><span class="amount"><i class="fa fa-gbp" aria-hidden="true"></i><?php 
            $ex_price=explode(',',$value['price']);
            $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));
            echo $integerIDs;?></span></td>
            <td>
              <div class="acc-tax">
                <span class="tax-inner">accotax</span>
                <p>accountants & tax consultants</p>
              </div>
            </td>
            <td>
              <span class="sent-month">
              <?php //echo $value['created_at'];
                $timestamp = strtotime($value['created_at']);
                $newDate = date('d F Y H:i', $timestamp); 
                echo $newDate; //outputs 02-March-2011
            ?>
              </span>

            </td>
            <td>
              <i class="fa fa-check" aria-hidden="true"></i>
              <span class="accept"><?php 
              $status=$value['status'];
              if($status=='send')
              {
                $res='sent';
              }
              if($status=='opened')
              {
                $res='viewed';
              }
              else
              {
                $res=$status;
              }
              echo ucfirst($res);
              ?></span>
            </td>
        </tr>
	 <?php }  ?>
</tbody>
</table>
<?php } ?>
