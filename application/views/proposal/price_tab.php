<?php

function tot_cal($pri,$quant)
{
	return number_format($pri*$quant,'2');
}

?>
<div class="box-division03">
<div class="accordion-heading" role="tab" id="headingOne">
<h3 class="card-title accordion-title">
<a class="accordion-msg"> 
<?php if(isset($records['id'])){
$s_number = $records['id'];
			 echo "#AT";
			 printf("%04d", $s_number);

}else{                       
	 $s_number = $proposal_number;
	 echo "#AT";
	 printf("%04d", $s_number);

  }
			 ?>
<input type="hidden" name="proposal_id" id="proposal_id" value="<?php if(isset($records['id'])){ echo $records['id']; } ?>" >                                    
<input type="hidden" name="proposal_no" id="proposal_no" value="<?php  echo "#AT";
printf("%04d", $s_number); ?>" >
<input type="hidden" name="p_no" id="p_no" value="<?php  echo $proposal_number; ?>" >
<?php $this->session->set_userdata('proposals',$proposal_number); ?>
My Template</a>
</h3>
</div>
<div class="price-listpro">
<div class="price-tb-setting">
<h2>Pricing Lists</h2>
<div class="discount-types">
<div class="line-items1">
	<div class="pricing-radio">
		<label>Using Pricing List</label>
		<div class="pricing_listing">
			<select name="pricelist" id="pricelist" onchange="pricelist_update(this)">
				<option value="">Select</option>          
				<?php
				 if(count($pricelist)>0)
				 {  
					foreach($pricelist as $price){ ?>
				<option value="<?php echo $price['id'];?>" <?php if(isset($pricelist_settings)){ if($pricelist_settings['pricelist']==$price['id']){ ?> selected="selected"   <?php }} if(isset($records['pricelist']) && $records['pricelist'] == $price['id']){ echo "selected"; } ?>><?php echo $price['pricelist_name'];?></option>
				<?php } } else{?>
				  <option value="">None</option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="pricing-radio add-price-cab">
		<label>Add pricing list to package</label>
		<a href="javascript:;" id="Price_list_catalog">Add to package</a>
	</div>
</div>
</div>
</div>
</div>
<!-- 2tab -->
<div class="service-table flat-animation2 <?php if(isset($records['tax_option']) && $records['tax_option']=='line_tax'){?> tax <?php } ?> <?php if(isset($records['discount_option']) && $records['discount_option']=='line_discount'){?> discount <?php } ?>">
<!-- <div class="masonry-container floating_set">
<div class="grid-sizer"></div>  -->
<ul class="accordion-views">
<li  class="services-des show-block ser">
<a href="#" class="toggle" style="border-radius: 0px !important;">Services</a>
<div class="service-newdesign1 inner-views">
<!-- <h2>Services</h2> -->
<div class="head-services">
	<div class="proposal-fields1 div-porposal">
		<span>Name/Description</span>
	</div>
	<div class="proposal-fields2 div-porposal">
		<span>Price</span>
	</div>
	<div class="proposal-fields3 div-porposal">
		<span>Qty</span>
	</div>
	<div class="proposal-fields5 div-porposal tax-field" <?php if(isset($records['tax_option']) && $records['tax_option']=='line_tax'){?> style="display:block;" <?php } ?>>
		<span>Tax</span>
	</div>
	<div class="proposal-fields6 div-porposal discount-field" <?php if(isset($records['discount_option']) && $records['discount_option']=='line_discount'){?> style="display:block;" <?php } ?>>
		<span>Discount</span>
	</div>
	<div class="proposal-fields4 div-porposal discount-field1">
		<span>Subtotal</span>
	</div>
</div>
<div class="field-add01">
	<div class="saved-contents">
		<div class="saved_service_contents remove-content" style="display: none">
			<div class="top-field-heads" style="display: none">
				<div class="proposal-fields1 service-items">
					<div class="checkbox-saved"><input class="border-saved" type="checkbox"></div>
					<p class="saved_item_name">  </p>
					<p class="saved_item_description">  </p>
				</div>
				<div class="proposal-fields2 price-services service-items">
					<span class="saved_item_price"></span>
					<span>/</span>                                
					<span class="saved_item_unit"></span>
				</div>
				<div class="proposal-fields3 service-items">
					<p class="saved_item_qty"></p>
				</div>
				<div class="proposal-fields5 service-items tax-field" <?php if(isset($records['tax_option']) && $records['tax_option']=='line_tax'){?> style="display:block;" <?php } ?>>
					<p class="saved_item_tax"> </p>
				</div>
				<div class="proposal-fields6 service-items discount-field" <?php if(isset($records['discount_option']) && $records['discount_option']=='line_discount'){?> style="display:block;" <?php } ?>>
					<p class="saved_item_discount"></p>
				</div>
				<div class="proposal-fields7 service-items discount-field1 final-edit-pro">
					<a class="edit_service" id="edit_service" onclick="edit_service(this)"><i class="icofont icofont-edit" aria-hidden="true"></i></a>
					<a class="cancel_service" id="cancel_service" onclick="cancel_service(this)">
					<i class="icofont icofont-ui-delete deleteAllTask" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	</div>
	<div class="extra-saved-service-content"></div>
	<div class="contents common-removenew2">
		<?php
			$item_name=explode(',',$records['item_name']);
			$service_category=explode(',',$records['service_category']);
			$price=explode(',',$records['price']);
			$qty=explode(',',$records['qty']);
			$unit=explode(',',$records['unit']);
			$description=explode('(,)',$records['description']);
			 $tax=explode(',',$records['tax']);
			 $discount=explode(',',$records['discount']);
			 $optional=explode(',',$records['service_optional']);
			 $quantity=explode(',',$records['service_quantity']);

		  
			for($i=0;$i<count($item_name);$i++){ 
			
			// $j=$i+1;
			// foreach($optional as $option){
			
			// }
			
				 // echo $optional[$i];
				 // echo $i+1;
				 // echo $quantity[$i];
				 //   echo $i+1;
			//if($i=='0' || !isset($records['item_name'])){ ?>
		<?php //if(isset($records['item_name'])){ if($records['item_name']==''){ ?> <!-- style="display:none;"  --><?php //} } ?>
		<div class="add-service input-fields" <?php if(isset($records['item_name']) && $records['item_name']!=''){   }else{ ?> style="display: none;" <?php } ?>>
			
			<div class="top-field-heads ">
			<div class="proposal-fields1 service-items">
				
				<input name="item_name[]" id="item_name" class="required item_name"  value="<?php if(isset($item_name)){
					echo $item_name[$i]; } ?>">
				<input type="hidden" name="service_category_name[]" id="service_category_name" class="service_category_name" value="<?php if(isset($service_category)){ echo $service_category[$i]; } ?>">
			</div>
			<div class="proposal-fields2 price-services service-items">
				<input type="hidden" placeholder="" name="original_price[]" id="original_price" class="decimal  original_price" onChange = "sub_total()"  value="<?php if(isset($price)){  echo $price[$i]; } ?>" >
				<input type="text" placeholder="" name="price[]" id="price" maxlength="6" class="required decimal  service_price  serviceprice price" onChange = "sub_total()"  value="<?php if(isset($price)){  echo $price[$i]; } ?>" >
				<span>/</span>
				<select name="unit[]" id="unit" class="required unit">
					<option value="per_service" <?php if(isset($unit)){
						if($unit[$i]=='per_service'){ ?>
						selected="selected"
						<?php }} ?>>Per Service</option>
					<option value="per_day" <?php if(isset($unit)){
						if($unit[$i]=='per_day'){ ?>selected="selected"
						<?php }} ?>>Per Day</option>
					<option value="per_month"<?php if(isset($unit)){
						if($unit[$i]=='per_month'){ ?>selected="selected"
						<?php }} ?>>Per Month</option>
					<option value="per_2month" <?php if(isset($unit)){
						if($unit[$i]=='per_2month'){ ?>selected="selected"
						<?php }} ?>>Per 2Months</option>
					<option value="per_6month" <?php if(isset($unit)){
						if($unit[$i]=='per_6month'){ ?>selected="selected"
						<?php }} ?>>Per 6Month</option>
					<option value="per_year" <?php if(isset($unit)){
						if($unit[$i]=='per_year'){ ?>selected="selected"
						<?php }} ?>>Per Year</option>
				</select>
			</div>
			<div class="proposal-fields3 service-items">
				<input onkeypress="return isNumeric(event)"
					oninput="maxLengthCheck(this)" maxlength = "4" min="1" max="9999" step="1" type="text" name="qty[]" id="qty"   class="required decimal decimal_qty  qty" value="<?php if(isset($qty)){ echo $qty[$i]; } ?>" onChange ="qty_check()">
			</div>
			<div class="proposal-fields5 service-items tax-field" <?php if(isset($records['tax_option']) && $records['tax_option']=='line_tax'){?> style="display:block;" <?php } ?>>
				<input type="text" name="service_tax[]" maxlength="4" id="service_tax" value="<?php if($tax[$i]!=''){ echo $tax[$i]; }else{ echo 0; }?>" class="decimal tax" onChange = "sub_total(this)">
			</div>
			<div class="proposal-fields6 service-items discount-field" <?php if(isset($records['discount_option']) && $records['discount_option']=='line_discount'){?> style="display:block;" <?php } ?>>
				<input type="text" name="service_discount[]" id="service_discount" value="<?php if($discount[$i]!=''){ echo $discount[$i]; }else{ echo 0; }?>" class="decimal discount" maxlength="4" onChange = "sub_total(this)">
			</div>                                         
			</div>  
			<textarea rows="3" name="description[]" id="description" class="required description"><?php if(isset($description[$i]) && $records['description'] !=''){ echo $description[$i]; } ?></textarea>
			<div class="mark-optional"> 
			  <div class="mark-left-optional pull-left border-checkbox-section">
				 <div class="checkbox-color checkbox-primary">  

				 <input class="border-checkbox optional" type="checkbox" id="checkbox_<?php echo $i; ?>" name="service_optional[]" <?php if(isset($optional)){ if(in_array($i+1, $optional)){ ?> checked="checked" <?php } }  ?>  value="<?php echo $i+1 ?>" ><label class="border-checkbox-label optional-label" for="checkbox_<?php echo $i; ?>">mark as optional</label>  </div><div class="checkbox-color checkbox-primary">  <input class="border-checkbox quantity" type="checkbox" id="check_box_<?php echo $i; ?>" name="service_quantity[]" <?php if(isset($optional)){ if(in_array($i+1, $optional)){ ?> checked="checked" <?php } }  ?> value="<?php echo $i+1 ?>"> <label class="border-checkbox-label quantity-label" for="check_box_<?php echo $i; ?>">quantity  editables</label> </div> </div><div class="pull-right mark-right-optional"> <a  class="sve-action1 edit_before_save" id="service_save" onclick="saved(this)">save</a>  <a  class="sve-action1 edit_after_save" id="service_save" onclick="saved_before(this)" style="display:none">save</a> <a class="sve-action2" id="save_to_catalog" onclick="save_to_catalog(this)">save to package</a> 
					<div class="delete_service_div buttons_hide"> <a class="sve-action2 service_close" id="service_close" onclick="deleted(this)"> Delete</a></div>
					<div class="cancel_service_div buttons_hide" style="display: none">  <a class="sve-action2 service_cancel" id="service_close" onclick="cancelled(this)"> Cancel</a> </div></div> </div>
		</div>                                       
  
<?php //}else{ ?>
<?php // } ?>
<?php } ?>
 </div>

<div class="extra-add-service common-removenew2"></div>
<div class="bottom-field-heads">
	<div class="mark-optional">
		<div class="pull-left send-to">
			<?php 
				//  if(!isset($records)){ ?>
			<a href="javascript:;" data-toggle="modal" data-target="#select-category"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> Add a new services</a>
			<?php //} ?>
		</div>
		<div class="pull-right subtotal-list">
			<span>Subtotal:</span>
			<span class="sub_total"><?php if(isset($records['price']) && $records['price']!=""){
				
				echo  array_sum(array_map('tot_cal',$price,$qty)); }else { echo "0";  }?></span>
		</div>
	</div>
</div>
<!--  <div class="proposal-fields4 service-items">
	</div> -->
<!--    </div> -->
</div>
</div>
</li>
<li  class="services-des show-block">
<a href="#" class="toggle" style="border-radius: 0px !important;">Products</a>
<div class="service-newdesign1 inner-views">
<!--  <h2>Products</h2> -->
<div class="head-services">
	<div class="proposal-fields1 div-porposal">
		<span>Name/Description</span>
	</div>
	<div class="proposal-fields2 div-porposal">
		<span>Price</span>
	</div>
	<div class="proposal-fields3 div-porposal">
		<span>Qty</span>
	</div>
	<div class="proposal-fields5 div-porposal tax-field" <?php if(isset($records['tax_option']) && $records['tax_option']=='line_tax'){?> style="display:block;" <?php } ?>>
		<span>Tax</span>
	</div>
	<div class="proposal-fields6 div-porposal discount-field" <?php if(isset($records['discount_option']) && $records['discount_option']=='line_discount'){?> style="display:block;" <?php } ?>>
		<span>Discount</span>
	</div>
	<div class="proposal-fields4 div-porposal discount-field1">
		<span>Subtotal</span>
	</div>
</div>
<div class="field-add01">
	<div class="saved-productcontents">
		<div class="saved_product_contents remove-content" style="display: none">
			<div class="top-field-heads" style="display: none">
				<div class="proposal-fields1 service-items">
					<div class="checkbox-saved"><input class="border-saved" type="checkbox"></div>
					<p class="saved_product_name"> </p>
					<p class="saved_product_description">  </p>
				</div>
				<div class="proposal-fields2 price-services service-items">
					<span class="saved_product_price"></span>
					<!-- <span>/</span>                                
						<span class="saved_product_unit"></span> -->
				</div>
				<div class="proposal-fields3 service-items">
					<p class="saved_product_qty"></p>
				</div>
				<div class="proposal-fields5 service-items tax-field" <?php if(isset($records['tax_option']) && $records['tax_option']=='line_tax'){?> style="display:block;" <?php } ?>>
					<p class="saved_product_tax"></p>
				</div>
				<div class="proposal-fields6 service-items discount-field" <?php if(isset($records['discount_option']) && $records['discount_option']=='line_discount'){?> style="display:block;" <?php } ?>>
					<p class="saved_product_discount"></p>
				</div>
				<div class="proposal-fields7 service-items discount-field1 final-edit-pro">
					<a class="edit_product red-grcolor" id="edit_product" onclick="edit_product(this)"><i class="icofont icofont-edit" aria-hidden="true"></i></a>
					<a class="cancel_product red-grcolor12" id="cancel_product" onclick="cancel_product(this)">
					<i class="icofont icofont-ui-delete deleteAllTask" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	</div>
	<div class="extra-saved-product-content"></div>
	<div class="product-contents common-removenew2">
		<?php
			$product_name=explode(',',$records['product_name']);
			$product_category_name=explode(',',$records['product_category_name']);
			$product_price=explode(',',$records['product_price']);
			$product_qty=explode(',',$records['product_qty']);
			$product_description=explode('(,)',$records['product_description']);
			$product_tax=explode(',',$records['product_tax']);
			$product_discount=explode(',',$records['product_discount']);
			$product_optional=explode(',',$records['product_optional']);
			$product_quantity=explode(',',$records['product_quantity']);
			for($i=0;$i<count($product_name);$i++){  
			//if($i=='0' || !isset($records['product_name'])){ ?>
		<div class="add-product input-fields" <?php if(isset($records['product_name']) && $records['product_name']!=''){   }else{ ?> style="display: none;" <?php } ?>>
			<div class="top-field-heads">
				<div class="proposal-fields1 service-items">
					
					<input type="text" name="product_name[]" id="product_name" class="product_name" value="<?php if(isset($product_name)){ echo $product_name[$i]; } ?>"  >
					<input type="hidden" name="product_category_name[]" id="product_category_name" class="product_category_name" value="<?php if(isset($product_category_name)){ echo $product_category_name[$i]; } ?>"> 
				</div>
				<div class="proposal-fields2 price-services service-items">
					<input type="hidden" placeholder="" name="original_product_price[]" id="original_product_price" class="decimal  original_product_price original_price" onChange = "sub_total()"  value="<?php if(isset($price)){  echo $price[$i]; } ?>" >
					<input type="text" placeholder="" name="product_price[]" maxlength="6" id="product_price" class="decimal product_price price" onChange = "sub_total()" value="<?php if(isset($product_price)){ echo $product_price[$i]; } ?>"  >
					<span>/</span>
					<select name="unit[]" id="unit" class="required unit">
						<option value="per_service" <?php if(isset($unit)){
							if($unit[$i]=='per_service'){ ?>
							selected="selected"
							<?php }} ?>>Per Service</option>
						<option value="per_day" <?php if(isset($unit)){
							if($unit[$i]=='per_day'){ ?>selected="selected"
							<?php }} ?>>Per Day</option>
						<option value="per_month"<?php if(isset($unit)){
							if($unit[$i]=='per_month'){ ?>selected="selected"
							<?php }} ?>>Per Month</option>
						<option value="per_2month" <?php if(isset($unit)){
							if($unit[$i]=='per_2month'){ ?>selected="selected"
							<?php }} ?>>Per 2Months</option>
						<option value="per_6month" <?php if(isset($unit)){
							if($unit[$i]=='per_6month'){ ?>selected="selected"
							<?php }} ?>>Per 6Month</option>
						<option value="per_year" <?php if(isset($unit)){
							if($unit[$i]=='per_year'){ ?>selected="selected"
							<?php }} ?>>Per Year</option>
					</select>
				</div>
				<div class="proposal-fields3 service-items">
					<input type="text" name="product_qty[]" maxlength="4" min="1" max="9999" step="1" id="product_qty" class="decimal decimal_qty product_qty" value="<?php if(isset($product_qty)){ echo $product_qty[$i]; } ?>" onChange ="qty_check()">
				</div>
				<div class="proposal-fields5 service-items tax-field" <?php if(isset($records['tax_option']) && $records['tax_option']=='line_tax'){?> style="display:block;" <?php } ?>>
					<input type="text" name="product_tax[]" id="product_tax" maxlength="4" class="decimal product_tax" value="<?php if($product_tax[$i]!=''){  echo $product_tax[$i];  }else{ echo '0'; } ?>" onChange = "sub_total()">
				</div>
				<div class="proposal-fields6 service-items discount-field" <?php if(isset($records['discount_option']) && $records['discount_option']=='line_discount'){?> style="display:block;" <?php } ?>>
					<input type="text" name="product_discount[]" id="product_discount" maxlength="4" class="decimal product_discount" value="<?php if($product_discount[$i]!=''){ echo $product_discount[$i]; }else{ echo '0'; } ?>" onChange = "sub_total()">
				</div>
			</div>
			<div class="bottom-field-heads">
				<textarea rows="3" name="product_description[]" id="product_description" class="product_description"><?php if(isset($product_description)){ echo $product_description[$i]; } ?></textarea>
				<div class="mark-optional">
					<div class="mark-left-optional pull-left border-checkbox-section">
						<div class="update-qual1">
							<label class="custom_checkbox1">
							<input class="border-checkbox product_optional"  type="checkbox" id="chec_k_box<?php echo $i; ?>" name="product_optional[]" <?php if(isset($product_optional)){ if(in_array($i+1, $product_optional)){ ?> checked="checked" <?php } }  ?> value=" <?php if(isset($product_optional)){ echo $i+1; }else{?>1<?php } ?>">  <i></i></label>
							<span>mark as optional</span>
						</div>
						<div class="update-qual1">
							<label class="custom_checkbox1">
							<input class="border-checkbox product_quantity" type="checkbox" id="checkb_ox<?php echo $i; ?>" name="product_quantity[]" <?php if(isset($product_quantity)){ if(in_array($i+1, $product_quantity)){ ?> checked="checked" <?php } }  ?> value=" <?php if(isset($product_quantity)){ echo $i+1; }else{?>1<?php } ?>"> <i></i> </label>
							<span>quantity  editables </span>
						</div>
					</div>
					<div class="pull-right mark-right-optional">
						<?php //if(isset($records['product_name'])){
							// echo "Saved";
							//  }else{ ?>
						<a  class="sve-action1 edit_beforeproduct_save" id="product_save" onclick="saved_product(this)">save</a>
						<a  class="sve-action1 edit_afterproduct_save" id="product_save" onclick="savedbefore_product(this)" style="display:none;">save</a>
						<a  class="sve-action2"  id="product_save_to_catalog" onclick="pro_save_to_catalog(this)">save to package</a>
						<div class="delete_product_div buttons_hide"> <a class="sve-action2 hgfghfgh product_close" id="product_close" onclick="deleted_product(this)">Delete</a></div>
						<div class="cancel_product_div cancel-comdiv" style="display:none"> 
							<a class="sve-action2 hgfghfgh product_cancel" id="product_close" onclick="cancelled_product(this)">Cancel</a>
						</div>
						<?php //} ?>
					</div>
				</div>
			</div>
		</div>
  
	<?php //}else{ ?>                           
	<?php  } ?>
	</div>
	<div class="extra-add-products common-removenew2"></div>
	<div class="bottom-field-heads">
		<div class="mark-optional">
			<div class="pull-left send-to">
				<?php //if(!isset($records)){ ?>
				<a href="javascript:;" data-toggle="modal" data-target="#select-products"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> Add a product</a>
				<?php //}?> 
			</div>
			<div class="pull-right subtotal-list">
				<span>Subtotal:</span>
				<span class="product_sub_total"><?php if(isset($records['product_price']) && $records['product_price']!=""){
					echo  array_sum(array_map('tot_cal',$product_price,$product_qty)); }else { echo "0";  }?></span>
			</div>
		</div>
	</div>
	<!-- <div class="proposal-fields4 service-items">
		</div> -->
</div>
</div>
</li>
<li  class="services-des show-block">
<a href="#" class="toggle" style="border-radius: 0px !important;">Subscriptions</a>
<div class="service-newdesign1 inner-views">
<!-- <h2>Subscriptions</h2> -->
<div class="head-services">
	<div class="proposal-fields1 div-porposal">
		<span>Name/Description</span>
	</div>
	<div class="proposal-fields2 div-porposal">
		<span>Price</span>
	</div>
	<div class="proposal-fields3 div-porposal">
		<span>QTY</span>
	</div>
	<div class="proposal-fields5 div-porposal tax-field" <?php if(isset($records['tax_option']) && $records['tax_option']=='line_tax'){?> style="display:block;" <?php } ?>>
		<span>Tax</span>
	</div>
	<div class="proposal-fields6 div-porposal discount-field" <?php if(isset($records['discount_option']) && $records['discount_option']=='line_discount'){?> style="display:block;" <?php } ?>>
		<span>Discount</span>
	</div>
	<div class="proposal-fields4 div-porposal discount-field1">
		<span>Subtotal</span>
	</div>
</div>
<div class="field-add01">
	<div class="saved-subscriptioncontents">
		<div class="saved_subscription_contents remove-content" style="display: none;" >
			<div class="top-field-heads" style="display: none">
				<div class="proposal-fields1 service-items">
					<div class="checkbox-saved"><input class="border-saved" type="checkbox"></div>
					<p class="saved_subscription_name"></p>
					<p class="saved_subscription_description"></p>
				</div>
				<div class="proposal-fields2 price-services service-items">
					<span class="saved_subscription_price"></span>
					<span>/</span>                                
					<span class="saved_subscription_unit"></span>
				</div>
				<div class="proposal-fields5 service-items tax-field" <?php if(isset($records['tax_option']) && $records['tax_option']=='line_tax'){?> style="display:block;" <?php } ?> >
					<p class="saved_subscription_tax"> </p>
				</div>
				<div class="proposal-fields6 service-items discount-field" <?php if(isset($records['discount_option']) && $records['discount_option']=='line_discount'){?> style="display:block;" <?php } ?>>
					<p class="saved_subscription_discount"></p>
				</div>
				<div class="proposal-fields7 service-items discount-field1 final-edit-pro">
					<a class="edit_subscription red-grcolor" id="edit_subscription" onclick="edit_sub(this)"><i class="icofont icofont-edit" aria-hidden="true"></i></a>
					<a class="cancel_subscription red-grcolor12" id="cancel_subscription" onclick="cancel_sub(this)">
					<i class="icofont icofont-ui-delete deleteAllTask" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	</div>
	<div class="extra-saved-subscription-content"></div>
	<div class="subscription-contents common-removenew2">
		<?php
			$subscription_name=explode(',',$records['subscription_name']);
			$subscription_category_name=explode(',',$records['subscription_category_name']);
			$subscription_price=explode(',',$records['subscription_price']);
			$subscription_unit=explode(',',$records['subscription_unit']);
			$subscription_description=explode('(,)',$records['subscription_description']);
			$subscription_tax=explode(',',$records['subscription_tax']);
			$subscription_discount=explode(',',$records['subscription_discount']);
			$subscription_optional=explode(',',$records['subscription_optional']);
			$subscription_quantity=explode(',',$records['subscription_quantity']);
			for($i=0;$i<count($subscription_name);$i++){  
			//  if($i=='0' || !isset($records['subscription_name'])){ ?>              <!-- <div class="bottom-field-heads"> -->
		<div class="add-subscription input-fields" <?php if(isset($records['subscription_name']) && $records['subscription_name']!=''){ }else{ ?> style="display: none;" <?php } ?>>
			<div class="top-field-heads">
				<div class="proposal-fields1 service-items">
					<input type="text" name="subscription_name[]" id="subscription_name" class="subscription_name" value="<?php if(isset($subscription_name)){ echo
						$subscription_name[$i]; 
						}?>" >
					<input type="hidden" name="subscription_category_name[]" id="subscription_category_name" class="subscription_category_name" value="<?php if(isset($subscription_category_name)){ echo
						$subscription_category_name[$i]; 
						}?>">  
				</div>
				<div class="proposal-fields2 price-services service-items">
					<input type="hidden" placeholder="" name="original_subscription_price[]" id="original_subscription_price" class="decimal original_subscription_price price original_price" onChange = "sub_total()" value="<?php if(isset($subscription_price)){ echo
						$subscription_price[$i]; 
						}?>" >
					<input type="text" placeholder="" name="subscription_price[]" id="subscription_price" class="decimal subscription_price price" maxlength="6" onChange = "sub_total()" value="<?php if(isset($subscription_price)){ echo
						$subscription_price[$i]; 
						}?>" >
						<span>/</span>
						<select name="subscription_unit[]" id="subscription_unit" class="subscription_unit" >
							<option value="per_hour"  <?php if(isset($subscription_unit)){
								if($unit[$i]=='per_hour'){ ?>
								selected="selected"
								<?php }} ?>>Per Hour</option>
							<option value="per_day"  <?php if(isset($subscription_unit)){
								if($unit[$i]=='per_day'){ ?>
								selected="selected"
								<?php }} ?>>Per Day</option>
							<option value="per_week"  <?php if(isset($subscription_unit)){
								if($unit[$i]=='per_week'){ ?>
								selected="selected"
								<?php }} ?>>Per Week</option>
							<option value="per_month"  <?php if(isset($subscription_unit)){
								if($unit[$i]=='per_month'){ ?>
								selected="selected"
								<?php }} ?>>Per Month</option>
							<option value="per_2month"  <?php if(isset($subscription_unit)){
								if($unit[$i]=='per_2month'){ ?>
								selected="selected"
								<?php }} ?>>Per 2Month</option>
							<option value="per_5month"  <?php if(isset($subscription_unit)){
								if($unit[$i]=='per_5month'){ ?>
								selected="selected"
								<?php }} ?>>Per 5month</option>
							<option value="per_year"  <?php if(isset($subscription_unit)){
								if($unit[$i]=='per_year'){ ?>
								selected="selected"
								<?php }} ?>>Per Year</option>
						</select>
					</div>
				<div class="proposal-fields3 service-items">
				<input onkeypress="return isNumeric(event)"
					oninput="maxLengthCheck(this)" maxlength = "4" min="1" max="9999" step="1" type="text" name="qty[]" id="qty"   class="required decimal decimal_qty  qty" value="<?php if(isset($qty)){ echo $qty[$i]; } ?>" onChange ="qty_check()">
					<!-- <select name="subscription_unit[]" id="subscription_unit" class="subscription_unit" >
						<option value="per_hour"  <?php if(isset($subscription_unit)){
							if($unit[$i]=='per_hour'){ ?>
							selected="selected"
							<?php }} ?>>Per Hour</option>
						<option value="per_day"  <?php if(isset($subscription_unit)){
							if($unit[$i]=='per_day'){ ?>
							selected="selected"
							<?php }} ?>>Per Day</option>
						<option value="per_week"  <?php if(isset($subscription_unit)){
							if($unit[$i]=='per_week'){ ?>
							selected="selected"
							<?php }} ?>>Per Week</option>
						<option value="per_month"  <?php if(isset($subscription_unit)){
							if($unit[$i]=='per_month'){ ?>
							selected="selected"
							<?php }} ?>>Per Month</option>
						<option value="per_2month"  <?php if(isset($subscription_unit)){
							if($unit[$i]=='per_2month'){ ?>
							selected="selected"
							<?php }} ?>>Per 2Month</option>
						<option value="per_5month"  <?php if(isset($subscription_unit)){
							if($unit[$i]=='per_5month'){ ?>
							selected="selected"
							<?php }} ?>>Per 5month</option>
						<option value="per_year"  <?php if(isset($subscription_unit)){
							if($unit[$i]=='per_year'){ ?>
							selected="selected"
							<?php }} ?>>Per Year</option>
					</select> -->
				</div>
				<div class="proposal-fields5 service-items tax-field" <?php if(isset($records['tax_option']) && $records['tax_option']=='line_tax'){?> style="display:block;" <?php } ?>>
					<input type="text" name="subscription_tax[]" maxlength="4" id="subscription_tax" class="decimal subscription_tax" value="<?php if($subscription_tax[$i]!=''){ echo $subscription_tax[$i];  }else{ echo 0; } ?>" onChange = "sub_total()">
				</div>
				<div class="proposal-fields6 service-items discount-field" <?php if(isset($records['discount_option']) && $records['discount_option']=='line_discount'){?> style="display:block;" <?php } ?>>
					<input type="text" name="subscription_discount[]" maxlength="4" id="subscription_discount" class="decimal subscription_discount" value="<?php if($subscription_discount[$i]!=''){ echo $subscription_discount[$i];  }else{ echo 0; } ?>" onChange = "sub_total()">
				</div>
			</div>
			<div class="bottom-field-heads">
				<textarea rows="3" name="subscription_description[]" id="subscription_description" class="subscription_description" ><?php if(isset($subscription_description)){ echo
					$subscription_description[$i]; 
					}?></textarea>
				<div class="mark-optional">
					<div class="mark-left-optional pull-left border-checkbox-section">
						<div class="update-qual1">
							<label class="custom_checkbox1">
							<input class="border-checkbox subscription_optional optional"  type="checkbox" id="ch_eckbox<?php echo $i; ?>" name="subscription_optional[]" <?php if(isset($subscription_optional)){ if(in_array($i+1, $subscription_optional)){ ?> checked="checked" <?php } }  ?> value=" <?php if(isset($subscription_optional)){ echo $i+1; }else{?>1<?php } ?>"> </label>
							<span>mark as optional</span><i></i>
						</div>
						<div class="update-qual1">
							<label class="custom_checkbox1">
							<input class="border-checkbox subscription_quantity" type="checkbox" id="checkbo_x<?php echo $i; ?>" name="subscription_quantity[]" <?php if(isset($subscription_quantity)){ if(in_array($i+1, $subscription_quantity)){ ?> checked="checked" <?php } }  ?> value=" <?php if(isset($subscription_quantity)){ echo $i+1; }else{?>1<?php } ?>"></label>
							<span>quantity  editables</span><i></i>
						</div>
					</div>
					<div class="pull-right mark-right-optional">
						<?php //if(isset($records['subscription_name'])){
							//     echo "Saved";
							// }else{ ?>
						<a  class="sve-action1 edit_beforesubscription_save" id="subscription_save" onclick="saved_subscription(this)">save</a>
						<a  class="sve-action1 edit_aftersubscription_save" id="subscription_save" onclick="savedbefore_subscription(this)" style="display: none;">save</a>
						<a  class="sve-action2"  id="subscription_save_to_catalog" onclick="sub_save_to_catalog(this)">save to package</a>
						<div class="delete_subscription_div buttons_hide">  <a  class="sve-action2 subscription_close" id="subscription_close" onclick="delete_subscription(this)" >Delete</a></div>
						<div class="cancel_subscription_div cancel-comdiv" style="display:none"> 
							<a  class="sve-action2 subscription_cancel" id="subscription_close" onclick="cancelled_subscription(this)">Cancel</a>
						</div>
						<?php //} ?>
					</div>
				</div>
			</div>
		</div>
	
	<?php //}else{ ?>
	<?php } ?>
	</div>

<div class="extra-add-subscription common-removenew2"></div>
<div class="bottom-field-heads">
	<div class="mark-optional">
		<div class="pull-left send-to">
			<?php //if(!isset($records)){ ?>  
			<a href="javascript:;" data-toggle="modal" data-target="#select-subscriptions"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> Add a subscription</a>                                    
			<?php //} ?>
		</div>
		<div class="pull-right subtotal-list">
			<span>Subtotal:</span>
			<span class="subscription_sub_total"><?php if(isset($records['subscription_price']) && $records['subscription_price']!=""){
				  echo array_sum(explode(',', $records['subscription_price'])); }else { echo "0";  }?></span>
		</div>
	</div>
</div>

<!--  <div class="proposal-fields4 service-items">
	</div>   -->                           
<!--    </div> echo array_sum(array_map('intval', explode(',', $records['subscription_price'])));-->
</div>
</div>
</li>
<li  class="services-des grand-services1 show-block">
<div class="service-newdesign1 remove-under-padding">
<div class="pull-left grand-total">
	<!-- <p><span>Total</span><strong>:</strong></p>
	<p class="dis_count"><span>Discount</span></p>
	<p class="ta_x">
		<input type="hidden" name="tax_value" id="tax_value" value="<?php echo $tax_details['id']; ?>">
		<input type="hidden" name="tax_old" id="tax_old" value="<?php echo $tax_details['tax_rate']; ?>">
		<select id="tax_details" name="total_tax_id">
		<option value="0">Select</option>	
			<?php foreach($taxes as $tax){
             if(!isset($records['total_tax_id'])){
			?>
			<option value="<?php echo $tax['id']; ?>" <?php if($tax['tax_status']=='1'){ echo "selected"; } ?>><?php echo $tax['tax_name']; ?></option>

			<?php }else{ ?>
            
            <option value="<?php echo $tax['id']; ?>" <?php if($tax['id'] == $records['total_tax_id']){ echo "selected"; } ?>><?php echo $tax['tax_name']; ?></option>

			<?php } } ?>
		</select>
		<input type="hidden" name="tax_amount" id="tax_amount" value="<?php echo $records['tax_amount']; ?>">
		<span style="display: none;"><span><?php echo $tax_details['tax_name']; ?> (<span class="tax_rate"><?php echo $tax_details['tax_rate']; ?></span>%)</span></span>
	</p>
	<p><span>Grand Total</span></p> -->
</div>
<div class="pull-right send-to grand-discount">
	<div class="totals">
		<div class="total-prop-wrap">
			<p><span>Total</span><strong>:</strong></p>
			<p><strong class="total_total"> <?php if(isset($records['total_amount'])){ echo $records['total_amount']; } ?></strong></p>
		</div>

		<input type="hidden" name="discount_amount" id="input_discount" value="0">		
		<p class="dis_count">
			<?php
				
				$tax_amount = 0;
                $dis = '';
                $disStyle = '';

				if(!empty($records['tax_amount']) && !empty($records['total_amount']))
				{
                   $tax_amount = $records['total_amount'] * ($records['tax_amount']/100);
				}				

				if((isset($records['total_amount']) && !empty($records['total_amount'])) && (isset($records['grand_total'])) && !empty($records['grand_total']) && $records['total_amount'] > (str_replace(',', '', $records['grand_total']) - $tax_amount)) 
				{
					$dis = number_format(($records['total_amount'] + $tax_amount) - str_replace(',', '', $records['grand_total']),2);
					$disStyle = '';
				} else {
					$dis = '0';
					$disStyle = 'display: none';
				}
			?>
			<strong class="discount_amount" style="<?php echo $disStyle; ?>"><?php echo $dis; ?></strong>
			<strong class="symbol" style="display: none;">%</strong>
			<a href="javascript:;" data-toggle="modal"  class="discount_add_option" data-target="#discount_details"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> Add a discount</a>
		</p>
		<div class="tax-prop-wrap">
			<p class="ta_x">
				<input type="hidden" name="tax_value" id="tax_value" value="<?php echo $tax_details['id']; ?>">
				<input type="hidden" name="tax_old" id="tax_old" value="<?php echo $tax_details['tax_rate']; ?>">
				<select id="tax_details" name="total_tax_id">
				<option value="0">Select</option>	
					<?php foreach($taxes as $tax){
					if(!isset($records['total_tax_id'])){
					?>
					<option value="<?php echo $tax['id']; ?>" <?php if($tax['tax_status']=='1'){ echo "selected"; } ?>><?php echo $tax['tax_name']; ?></option>

					<?php }else{ ?>
					
					<option value="<?php echo $tax['id']; ?>" <?php if($tax['id'] == $records['total_tax_id']){ echo "selected"; } ?>><?php echo $tax['tax_name']; ?></option>

					<?php } } ?>
				</select>
				<input type="hidden" name="tax_amount" id="tax_amount" value="<?php echo $records['tax_amount']; ?>">
				<span style="display: none;"><span><?php echo $tax_details['tax_name']; ?> (<span class="tax_rate"><?php echo $tax_details['tax_rate']; ?></span>%)</span></span>
			</p>
			<p class="ta_x"><span class="tax_total">		
			<?php if(isset($records['total_amount']))
			{ //$total=$records['total_amount'];
				//$tax=$tax_details['tax_rate']; 
				//echo $amount=$total * ($tax/100);
				echo number_format($tax_amount,2); } ?></span></p>
		</div>	
		<div class="total-prop-wrap">
			<p><span>Grand Total:</span></p>
			<p><span class="grand_total"><?php if(isset($records['grand_total'])){ echo $records['grand_total']; } ?></span></p>
		</div>	
	</div>
</div>
</div>
</li>
</ul>
</div>
<!-- 3 product-->
</div>