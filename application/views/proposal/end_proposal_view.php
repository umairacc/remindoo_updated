<?php
error_reporting('0');
$this->load->view('includes/header'); ?>
<link href="<?php echo base_url(); ?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/timeline-style.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/timeline-style.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style1.css">
<style type="text/css">
	.LoadingImage {
		z-index: 999999999999 !important;
	}

	#mail-template table {
		/*display: block;*/
		width: 100%;
		/*overflow: auto;*/
	}

	.modal-backdrop.fade.in {
		background: rgba(0, 0, 0, 0.8);
	}

	.pcoded-content {
		float: left !important;
		width: 100%;
		z-index: 999;
	}

	.us-details .ima {
		width: 100%;
		height: auto;
		display: none;
		text-align: left;
		margin-bottom: 10px;
		resize: none;
		border: 1px solid #dbdbdb;
		padding: 8px 10px;
		text-align: center;
	}

	.ima #image_attachment {

		display: block;
		position: relative;
	}

	.times_send_to span.div1 {
		border-right: 1px solid #000;
	}

	.times_send_to .crd-by {
		border: 1px solid #000;
	}

	.message-under1 .div1 h6 {
		border-bottom: 1px solid #000;
	}

	.dropdown-main input {
		position: relative;
		opacity: 1;
	}

	.done_field {
		display: none;
	}

	.quantity_class {
		display: none;
	}

	.check_quantity {
		display: none;
	}

	.optional_check {
		display: none;
	}

	#gg_body_content .checkbox-fade {
		display: none;
	}

	#gg_body_content .check_quantity {
		display: none;
	}

	.discount-exits {
		display: block;
	}

	.tax-exits {
		display: block;
	}

	li.show-block {
		display: none;
		float: left;
		width: 100%;
	}

	.inner-views {
		display: block;
	}

	.service-table .proposal-fields1 {
		width: 35%;
	}

	.service-table .proposal-fields2 {
		width: 48%;
	}

	.price-listpro .pricing-radio:last-child label {
		display: block;
		float: left;
		margin-top: 7px;
	}

	.ScrollStyle {
		max-height: 750px;
		overflow-y: scroll;
	}

	#finallize_tab .right-side-proposal.down-op {
		margin-top: -16px;
	}

	#dd-head table {
		margin: 10px 50px 10px 50px;
	}

	#dd-body table {
		margin: 10px 50px 10px 50px;
	}

	#dd-footer table {
		margin: 10px 50px 10px 50px;
	}

	li.doc_val a {
		width: 100% !important;
	}


	/* Price table*/



	.table-scroll {
		border: 1px solid #e3e0e1;
	}

	span.service-title {
		background: #d8d8d8;
		width: 100%;
		display: block;
		padding: 10px 15px;
		font-weight: 600;
		color: #333;
	}

	.content-blog {
		margin: 20px 0;
	}

	.grand-total p {
		text-align: right;
	}

	.grand-total {
		text-align: right;
		padding: 10px 20px;
	}

	.grand-total span {
		min-width: 101px;
		display: inline-block;
		width: 110px;
		text-align: left;
	}

	.grand-total span,
	.grand-total strong {
		display: inline-block;
		vertical-align: top;
		vertical-align: middle;
		line-height: 10px;
	}

	.grand-total p {
		text-align: right;
		display: block;
	}

	.grand-total .boder-top {
		padding-top: 10px;
		border-top: 1px solid #dbdbdb;
		font-weight: 600;
		margin-top: 15px;
		font-size: 14px;
		display: block;
	}

	.sig-img,
	.sig-right {
		width: 49%;
		display: inline-block;
		vertical-align: top;
	}

	.sig-right {
		text-align: right;
	}

	.signature-blog p {
		margin-top: 35px;
		height: 34px;
		border-bottom: 1px solid #dbdbdb;
		padding-bottom: 8px;
		width: 230px;
	}

	.signature-blog table {
		width: 100%;
	}

	.signature-blog span {
		display: block;
		font-size: 14px;
		color: #000;
		margin-top: 10px;
	}

	.upper {
		text-transform: uppercase;
	}

	.signature-blog td:first-child {
		width: 60%;
	}

	.signature-blog td:last-child {
		width: 40%;
	}

	.pdf-table .table-scroll {
		margin-top: 20px;
	}

	.pdf-table .table-scroll td {
		vertical-align: top;
	}

	.col-span1,
	.col-span2,
	.col-span3,
	.col-span4,
	.row1,
	.row3 {
		display: inline-block;
		text-align: left;
		vertical-align: top;
	}

	.col-span1 {
		width: 294.8px;
	}

	.col-span2 {
		width: 129px;
	}

	.col-span3 {
		width: 76.5px;
	}

	.col-span4 {
		width: 79px;
	}

	.row3 {
		width: 795px;
		border-right: 1px solid #dbdbdb;
	}

	.row1 {
		width: 90px;
	}

	.col-span1,
	.col-span2,
	.col-span3,
	.col-span4 {
		border-right: 1px solid #dbdbdb;
		font-size: 13px;
	}

	.tab-topbar {
		font-size: 15px;
		font-weight: normal;
		display: block;
		font-weight: 600;
		vertical-align: top;
		border-bottom: 1px solid #dbdbdb;
	}

	.col-span4:last-child {
		border-right: none;
	}

	.cmn-spaces,
	.col-span1,
	.col-span2,
	.col-span3,
	.col-span4 {
		padding: 7px;
		font-size: 13px;
	}

	.full-acc {
		border-bottom: 1px solid #dbdbdb;
	}

	.col-span3:last-child {
		border-right: none;
	}

	.tab-topbar .col-span3 {
		width: 78px;
	}

	.cmn-spaces,
	.price-row,
	.full-acc {
		border-bottom: 1px solid #dbdbdb;
	}

	.cmn-spaces:last-child,
	.price-row:last-child,
	.col-span1,
	.col-span2,
	.col-span3 {
		border-bottom: none;
	}

	div * {
		line-height: 19px;
	}

	.row1 {
		color: #22b14c;
	}

	#Preview_proposal .modal-dialog.modal-lg {
		max-width: 860px;
	}

	#Preview_proposal .modal-dialog.modal-lg .row3 {
		width: 669px;
	}

	/* Price table*/

	.price-listpro,
	.input-fields .mark-optional,
	.discount_add_option,
	.mark-optional .send-to {
		display: none ! important;
	}
</style>

<script type="text/javascript">
	/* ]]> */
</script>

<!-- <form action="<?php //echo base_url(); 
					?>proposals/proposals_send" name="form1" id="login_form1" method="post"> -->
<div class="modal-alertsuccess alert alert-success followup" style="display:none;">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
	<div class="pop-realted1">
		<div class="position-alert1">
			Followup Mail Sent Successfully
		</div>
	</div>
</div>
<div class="modal-alertsuccess alert alert-success proposal" style="display:none;">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
	<div class="pop-realted1">
		<div class="position-alert1">
			Proposal Sent Successfully
		</div>
	</div>
</div>
<div class="modal-alertsuccess alert alert-success" style="display:none;">
	<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
	<div class="pop-realted1">
		<div class="position-alert1">
			Template Converted Successfully
		</div>
	</div>
</div>
<div class="pcoded-content">
	<div class="pcoded-innert-content">
		<div class="common-reduce01 new-end-porposal floating_set edit-proposal-wrapper">
			<div class="lead-post-01">
				<div class="deadline-crm1 floating_set">
					<?php $this->load->view('proposal/proposal_navigation_tabs'); ?>
					<div class="Footer common-clienttab pull-right Next_Previous">
						<div class="divleft">
							<button class="previous-step signed-change2" type="button" value="Previous Tab" text="Previous Tab">Previous
							</button>
						</div>
						<div class="divright" style="display:none;">
							<button class="next-tab signed-change3" type="button" value="Next Tab" text="Next Tab">Next
							</button>
						</div>
					</div>
				</div>
				<div class="document-center floating_set stpro">
					<div class="Companies_House floating_set">
						<div class="pull-left form_heading">
							<h2>Steps, Sending a proposal</h2>
						</div>
						<div class="Footer common-clienttab pull-right" style="display: none;">
							<div class="divleft">
								<button class="prev-step disabled" type="button" value="Previous Tab" text="Previous Tab">Previous
								</button>
							</div>
							<div class="divright">
								<button class="next-step" type="button" value="Next Tab" text="Next Tab">Next
								</button>
							</div>
						</div>
					</div>
					<div class="addnewclient_pages1 floating_set bg_new1">
						<ol class="nav nav-tabs all_user1 md-tabs floating_set">
							<li class="nav-item disabled"><a class="nav-link" data-toggle="tab" href="#start_tab">Start</a></li>
							<li class="nav-item disabled"><a class="nav-link" data-toggle="tab" href="#price_tab">Price</a></li>
							<li class="nav-item disabled"><a class="nav-link editing_tab" data-toggle="tab" href="#edit_tab">Edit</a></li>
							<li class="nav-item disabled"><a class="active nav-link" data-toggle="tab" href="#finallize_tab">Finallize</a></li>
						</ol>
					</div>
				</div>
				<div class="tab-content">
					<?php $this->load->view('proposal/step1'); ?>
					<div id="edit_tab" class="tab-pane fade current-proposal-1">
						<div class="card col-md-12" style="padding: 25px;">
							<div class="edit-proposal-tabs-wrap">
								<div class="edit-proposal-msg">
									<p><strong>
											<h6>Proposal Message:</h6>
										</strong>
									</p>
									<div class="col-md-12" disabled>
										<div id="proposal_contents_details" class="ScrollStyle col-sm-12" contenteditable="false">
											<table>
												<thead>
													<tr>
														<td id="dd-head"></td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td id="dd-body"></td>
													</tr>
												</tbody>
												<tfoot>
													<tr>
														<td id="dd-footer"></td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
								<div class="container-sidebar hidden" id="option-tabs">
									<div class="new-update-email1">
										<div id="get-options">
											<p style="color: red !important;">For Pasting, Use Ctrl+v For Better Results.</p>
											<!--  <h4>Drag and drop the elements below to the work area on the left</h4> -->
											<div class="get-options choose" data-id="title" id="title"><i class="fa fa-header" aria-hidden="true"></i>
												<div>Heading</div>
											</div>
											<div class="get-options choose" data-id="content" id="content"><i class="fa fa-indent" aria-hidden="true"></i>
												<div>Text</div>
											</div>
											<div class="get-options choose" data-id="image" id="image"><i class="fa fa-picture-o" aria-hidden="true"></i>
												<div>Image</div>
											</div>
											<div class="get-options choose" data-id="video" id="video"><i class="fa fa-play" aria-hidden="true"></i>
												<div>Video</div>
											</div>
											<div class="get-options choose" data-id="link" id="link"><i class="fa fa-link" aria-hidden="true"></i>
												<div>Link</div>
											</div>
											<div class="get-options choose" data-id="divider" id="divider"><i class="fa fa-window-minimize" aria-hidden="true"></i>
												<div>Divider</div>
											</div>
											<!--  <div class="get-options choose" data-id="quote" id="quote"><i class="fa fa-comment" aria-hidden="true"></i><div>Blockquote</div></div> -->
											<div class="get-options choose" data-id="e_signature" id="e_signature"><i class="fa fa-pencil" aria-hidden="true"></i>
												<div>Email Signature</div>
											</div>
											<div id="editor"></div>
											<ul id="attach-data" class="list-group"></ul>
										</div>
										<?php if ($this->uri->segment(2) != 'step_proposal' && $this->uri->segment(2) != 'copy_proposal') { ?>
											<!-- <button class="btn btn-lg btn-default btn-materialize btn-left-bottom btn-left-bottom-3 hidden" type="button" id="price_table" data-toggle="tooltip" title="To add table, place the cursor where you want and click over me." data-placement="top" data-trigger="hover" style="width: 108px;padding-right:15px;background-color: #a39a98"><span class="fa fa-table" style="padding-left:5px;padding-right:5px;"></span>Table</button>  -->
											<button class="btn btn-lg btn-default btn-materialize btn-left-bottom btn-left-bottom-3 hidden" type="button" id="price_table" data-toggle="tooltip" title="To add table, drag me over the editor." data-placement="top" data-trigger="hover" style="width: 108px;padding-right:15px;background-color: #a39a98" draggable="true"><span class="fa fa-table" style="padding-left:5px;padding-right:5px;"></span>Table</button>
										<?php } ?>
										<!--  <button class="btn btn-lg btn-success btn-materialize btn-left-bottom btn-left-bottom-1 hidden" type="button" id="preview" title="Preview" data-toggle="tooltip" data-placement="top" data-trigger="hover"><i class="fa fa-search-plus" aria-hidden="true"></i> Preview </button> -->

										<button class="btn btn-lg btn-default btn-materialize btn-left-bottom btn-left-bottom-3 hidden" type="button" id="setting" title="Layout Options" data-toggle="tooltip" data-placement="top" data-trigger="hover"><span class="fa fa-cog fa-spin"></span> Setting</button>

									</div>
								</div>
							</div>	
						</div>
						<?php //echo html_entity_decode($records['proposal_contents']); 
						?>
					</div>
					<div id="price_tab" class="tab-pane fade">
						<div class="left-side-proposal management_section">
							<div class="accordion-panel">
								<?php $this->load->view('proposal/price_tab.php'); ?>
							</div>
						</div>
					</div>
					<div id="finallize_tab" class="tab-pane fade in active">
						<div class="chart_end1">
							<textarea id="proposal_contents" name="proposal_contents" style="display: none;"><?php echo $records['proposal_contents_details']; ?> </textarea>
							<textarea id="proposal_mail" name="proposal_mail" style="display: none;"> </textarea>
							<div class="mark-right-optional floating_set end-prposal_01">
								<div class="draft_sect_ion" <?php
															if ($records['status'] == 'draft') { ?> style="display:inline-block;" <?php } else { ?> style="display: none;" <?php } ?>>
									<a href="javascript:;" class="sve-action2" data-toggle="modal" data-target="#Send_proposal" id="<?php echo $records['id']; ?>">Send proposal</a>
								</div>
								<?php $randomstring = generateRandomString('100'); ?>
								<a href="<?php echo base_url() . 'proposal/proposal/' . $randomstring . '---' . $records['id']; ?>" class="sve-action2" style="">Preview proposal</a> <!-- margin:25px 10px 0px 0px -->
								<!-- <input type="submit" id="other" class="sve-action1" value="send proposal" id="preview_proposal_send"> -->
							</div>
							<div class="modal fade" id="Send_proposal" role="dialog">
								<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Proposal</h4>
										</div>
										<div class="modal-body">
											<!--   <input type="hidden" name="status" value="send"> -->
											<p>This is your last change to review it before the link sent to your client.</p>
										</div>
										<div class="modal-footer">
											<input type="submit" id="<?php echo $records['id']; ?>" class="sve-action1 btn btn-card btn-primary send_propos" value="Ok i Understand">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="left-side-proposal">           
								<div class="floating_set management_section send-data">
										<div class="remove-spacing-9">
											<div class="accordion-panel">
													<div class="box-division03">
														<div class="accordion-heading" role="tab" id="headingOne">
																<h3 class="card-title accordion-title">
																	<a class="accordion-msg">Sent To</a>
																</h3>
														</div>
														<div class="accotax-litd">
																<div class="gmail-tax">
																	<div class="tax-consul">
																			<div class="img-tax">
																				<img src="http://remindoo.org/CRMTool/uploads/1521552545.png" alt="image">
																			</div>
																			<div class="tax-content">                                
																				<span><?php echo $company_details['crm_company_name']; ?></span>
																				<strong><?php echo $company_details['crm_email']; ?></strong>
																			</div>
																	</div>
																</div>
														</div>
													</div>
											</div>
											
								
											<div class="proposal-options floating_set">
									<h2>Proposal options</h2>
									<div class="enable-proposal">
											<p><input type="checkbox" class="js-small f-right fields" name="chase_for_info" id="general_pdf"> <span>General PDF version (<a href="<?php echo base_url(); ?>proposal_pdf/pdf_download/<?php echo $records['id']; ?>">Download</a>) </span></p>
												<div class="tree-enable" id="pdf" style="display: none;">
													<p><input type="checkbox" class="js-small f-right fields" name="chase_for_info"> <span>Add PDF Table of contents</span></p>
												</div>
												<p><input type="checkbox" class="js-small f-right fields" id="email_signature" name="email_signature"> <span>Enable e-signature</span></p>
								
												<p><input type="checkbox" class="js-small f-right fields" name="chase_for_info" id="password_access"> <span>Enable password protected access</span></p>
												<div class="tree-enable" id="password_section" style="display: none;">
													<p><input type="password" value=""></p>
												</div>
												<p><input type="checkbox" class="js-small f-right fields" name="chase_for_info" id="expiration_date_check"> <span>Set proposal expiration date</span></p>  
												<div class="tree-enable" id="expiration_date" style="display: none;">
													<p><input class="form-control dob_picker fields" placeholder="dd-mm-yyyy" type="text" value=""></p>
												</div>
										</div>
									</div> 
								
										</div>
								</div>       
								
							</div> -->
							<!-- left side proposal -->
							<div class="version-pdf1 floating_set">
								<div class="right-side-proposal lt-op">
									<!-- accordion-panel -->


									<div class="message-under1 floating_set times_send_to">
										<div class="accotax-litd send-to">
											Email message
										</div>

										<div class="cry_usings">
											<div class="crd-by">

												<span class="div1">
													<h6>Created by</h6>
													<?php if ($records['sender_company_name'] == '') { ?>
														<h5>ACCOTAX Accountants & Tax Consultants</h5>
													<?php } else { ?>
														<h5><?php echo $records['sender_company_name']; ?></h5>
													<?php } ?>
												</span>
												<span class="div1">
													<h6>Sent on</h6>
													<h5><?php
														$timestamp = strtotime($records['created_at']);
														$newDate = date('d F Y H:i', $timestamp);
														echo $newDate; ?>
														<!--   <span class""><?php //echo  get_time_ago( strtotime($records['created_at']) ); 
																				?></span> -->
														<span class="ex-date">Expiration Date 
														<?php
														if ($records['expiration_date'] != '') {
															$expiration_date = strtotime($records['expiration_date']);
															echo date('F d Y', $expiration_date);
														} else {
															echo 'Not Mentioned';
														} ?></span>
													</h5>
												</span>
												<span class="div1">
													<h6>Status</h6>
													<h5>
														<select id="<?php echo $records['id']; ?>" class="status_change">
															<option value="opened" <?php if ($records['status'] == 'opened') { ?>selected="selected" <?php } ?>>Viewed</option>
															<option value="sent" <?php if ($records['status'] == 'sent') { ?>selected="selected" <?php } ?>>Sent</option>
															<option value="accepted" <?php if ($records['status'] == 'accepted') { ?>selected="selected" <?php } ?>>Accept</option>
															<option value="declined" <?php if ($records['status'] == 'declined') { ?>selected="selected" <?php } ?>>Decline</option>
															<option value="in discussion" <?php if ($records['status'] == 'in discussion') { ?>selected="selected" <?php } ?>>InDiscussion</option>
															<option value="draft" <?php if ($records['status'] == 'draft') { ?>selected="selected" <?php } ?>>Draft</option>
														</select>
													</h5>
												</span>
											</div>
										</div>
									</div>


									<div id="tabs15" class="message-block01">
										<ul>
											<!--  <li><a href="#tabs-1">Analytics</a></li> -->
											<li><a href="#tabs-2">Internal Notes</a></li>
											<li><a href="#tabs-3">Discuss with Client </a></li>
											<li><a href="#tabs-4">Attachments </a></li>
										</ul>
										<div id="tabs-2">
											<div id="internal_notes_section">
												<?php
												foreach ($internal_notes as $notes) { ?>
													<div class="else-msgcont blockquote">
														<span><?php echo $notes['internal_notes']; ?></span>
														<span class="created_at">
															<?php //echo $value['created_at'];
															$timestamp = strtotime($notes['created_at']);
															$newDate = date('d F Y H:i', $timestamp);
															echo $newDate; //outputs 02-March-2011
															?>
														</span>
													</div>
												<?php }
												?>
											</div>
											<input type="hidden" name="proposal_id" id="p_id" value="<?php echo $records['id']; ?>">
											<div>
												<textarea id="internal_notes" rows="5" cols="25" style="resize:none;"></textarea>
												<span id="internal_notes_er" style="color: red;"></span>
											</div>
											<button type="button" id="internal_save" class="btn btn-primary">Save</button>
										</div>
										<div id="tabs-3">
											<span class="f-right">see all comments</span>
											<div id="message_lists" class="user-box">
												<?php
												$sender_company = $records['sender_company_name'];

												foreach ($conversation as $con) {
													// echo "jao";
													if ($con['sender_id'] != '') {   ?>
														<div class="msg-content blockquote">
															<div class="send-quote1">
																<strong><?php echo $sender_company . '  Wrote'; ?></strong>
																<span>
																	<div class="quote-msg1"><?php echo  $con['message']; ?></div>
																</span>
															</div>
														</div>
													<?php
													} else { ?>
														<div class="else-msgcont blockquote">
															<div class="send-quote1">
																<strong><?php if ($records['company_name'] == 0) {
																			echo $records['receiver_mail_id'] . '  Wrote';
																		} else {
																			echo $records['company_name'];
																		} ?></strong>
																<span><?php echo  $con['message']; ?></span>
															</div>
														</div>
												<?php   }
												} ?>
											</div>
											<div class="msg-tinybox">
												<!--  <span class="ccount">0 Comments for Letter of Engagement proposal</span> -->
												<div class="msg-tbox">
													<img class="img-fluid p-absolute" src="<?php echo base_url(); ?>/assets/images/avatar-2.jpg" alt="">
													<div class="us-details">
														<!--   <span class="usname">skelkh</span> -->
														<input type="hidden" name="sender_id" id="sender_id" value="<?php echo $records['user_id']; ?>">
														<input type="hidden" name="company_name" id="receiver_companyname" value="<?php if ($records['company_name'] == 0) {
																																		echo $records['receiver_mail_id'];
																																	} else {
																																		echo $records['company_name'];
																																	} ?>">

														<input type="hidden" id="image_type" value="">
														<input type="hidden" name="id" id="id" value="<?php echo $records['id']; ?>">
														<div class="ima"> <span id="image_attachment"></span></div>
														<input type="hidden" name="attach_img" id="attach_img" value="">
														<textarea id="message_comments" name="message"></textarea>
														<p></p>
														<textarea id="messages_comments" name="image_message" style="display: none"></textarea>

														<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
															<div style="display: none;">
																<input type="file" id="fileInput" name="fileInput" onchange="loadFile(this)">
															</div> <button type="button" class="btn  hidden" onclick="chooseFile();"> <span class="glyphicon glyphicon-picture"></span> <i class="fa fa-paperclip" aria-hidden="true"></i> </button>
														</form>
														<input type="button" class="m2send send_msg" value="post">
													</div>
												</div>
											</div>
										</div>
										<div id="tabs-4">
											<ul class="ques-quote1">

												<input type="hidden" name="documents_attach" id="documents_attach" value="<?php if (isset($records['document_attachment']) && $records['document_attachment'] != '') {
																																echo $records['document_attachment'];
																															} ?>">
												<?php
												if (isset($records['document_attachment']) && $records['document_attachment'] != '') {

													$images = explode(',', $records['document_attachment']);

													for ($i = 0; $i < count($images); $i++) {
														$imgExts = array("gif", "jpg", "jpeg", "png", "tiff", "tif");
														$urls = $images[$i];
														$urlExt = pathinfo($urls, PATHINFO_EXTENSION);
														if (in_array($urlExt, $imgExts)) {
															// echo 'Yes, '.$url.' is an Image'; 
												?>




															<li class="documents" id="<?php echo $doc['document_file']; ?>">

																<a href="<?php echo $urls; ?>" download><img src="<?php echo $urls; ?>" alt=""></a>
																<span class="tooltiptext"><?php echo  substr($urls, 38); ?></span>
																<span class="closeicon1 document_attachment_images" id="<?php echo $urls; ?>"><span class="close-ico">&times;</span></span>
															</li>
															<?php  } else {
															$imgExts1 = array("pdf", "doc", "docs", "xlsx", "xls", "csv");
															$urls = $images[$i];
															$urlExt1 = pathinfo($urls, PATHINFO_EXTENSION);
															if (in_array($urlExt1, $imgExts1)) {

																//echo 'Yes, '.$url.' is an Document'; 
															?>
																<li class="documents">
																	<a href="<?php echo base_url(); ?>assets/images/doc-icon.jpg" id="<?php echo $i; ?>" class="document_attachment_images" download><img src="<?php echo base_url(); ?>assets/images/doc-icon.jpg" alt=""></a>
																	<span class="tooltiptext"><?php echo  substr($urls, 38); ?></span>
																	<span class="closeicon1 document_attachment_images" id="<?php echo $urls; ?>"><span class="close-ico">&times;</span></span>
																</li>
												<?php }
														}
													}
												} ?>

												<input type="hidden" name="attach" id="attach" value="<?php if (isset($records['attachment']) && $records['attachment'] != '') {
																											echo $records['attachment'];
																										} ?>">

												<?php if (isset($records['attachment']) && $records['attachment'] != '') {

													$images = explode(',', $records['attachment']);

													for ($i = 0; $i < count($images); $i++) {
														$imgExts = array("gif", "jpg", "jpeg", "png", "tiff", "tif");
														$urls = $images[$i];
														$urlExt = pathinfo($urls, PATHINFO_EXTENSION);
														if (in_array($urlExt, $imgExts)) {
															// echo 'Yes, '.$url.' is an Image'; 
												?>


															<li class="documents" id="<?php echo $doc['document_file']; ?>">
																<?php
																$url1 = pathinfo($urls)['filename'];
																$strlower = preg_replace("~[\\\\/:*?'&,<>|]~", '', $url1);
																$lesg = str_replace(' ', '_', $strlower);
																//  $lesg_s = str_replace('-', '_', $lesg);
																$lesgs = str_replace('.', '_', $lesg);

																//echo pathinfo($url)['extension']; // "ext"
																//$lesgs=pathinfo($url)['filename']; // "file"
																$ext = pathinfo($urls, PATHINFO_EXTENSION);
																?>
																<a href="<?php echo base_url(); ?>attachment/<?php echo $lesgs . '.' . $ext; ?>" download><img src="<?php echo base_url(); ?>attachment/<?php echo $lesgs . '.' . $ext; ?>" alt=""></a>
																<span class="tooltiptext"><?php echo  $urls; ?></span>
																<span class="closeicon1 attachment_images" id="<?php echo  $urls; ?>"><span class="close-ico">&times;</span></span>
															</li>
															<?php  } else {
															$imgExts1 = array("pdf", "doc", "docs", "xlsx", "xls", "csv");
															$urls = $images[$i];
															$urlExt1 = pathinfo($urls, PATHINFO_EXTENSION);
															if (in_array($urlExt1, $imgExts1)) {

																//echo 'Yes, '.$url.' is an Document'; 
															?>
																<li class="documents doc_val">
																	<a href="<?php echo  base_url() . 'attachment/' . $urls; ?>" download><?php echo  $urls; ?></a>
																	<span class="tooltiptext"><?php echo  $urls; ?></span>
																	<span class="closeicon1 attachment_images" id="<?php echo  $urls; ?>"><span class="close-ico">&times;</span></span>
																</li>
												<?php }
														}
													}
												}
												?>


												<?php
												if (isset($discussion_attachments)) {
													foreach ($discussion_attachments as $discuss) {
														$result = $discuss['image'];

														//                preg_match_all('/<img[^>]+>/i',$html, $result); 

														// print_r($result);

												?>




														<li class="documents" id="<?php echo $discuss['id']; ?>">

															<a href="<?php echo $result; ?>" download> <img src="<?php echo $discuss['image']; ?>"></a>
															<!--    <span class="tooltiptext"><?php echo  $result; ?></span>  -->
															<span class="closeicon1 document_disccussion_attachment_images" id="<?php echo $discuss['id']; ?>"><span class="close-ico">&times;</span></span>
														</li>


												<?php  }
												}

												?>

											</ul>
										</div>
									</div>
									<!-- <textarea rows="1" id="subject"></textarea> -->
									<!--  <div class="hide-result">-->
									<div class="proposal_content" id="body" style="display: none"><?php echo $records['proposal_mail']; ?></div>
									<!--</div> -->
									<!--  <div class="hide-result hd">
										<textarea rows="3" cols="10" id="message"></textarea>
										<input type="button" class="send-bnn" value="send">
									</div> -->

									<div class="row history">
										<div class="card">
											<div class="card-header">
												<h5>History</h5>
											</div>
											<div class="card-block">
												<div class="main-timeline">
													<div class="cd-timeline cd-container">
														<?php
														foreach ($proposal_history as $p_h) {
															$randomstring = generateRandomString('100');
														?>
															<div class="cd-timeline-block">
																<div class="cd-timeline-icon bg-primary">
																	<?php if ($p_h['tag'] == 'insert') { ?>
																		<i class="icofont icofont-ui-file"></i>
																	<?php } else { ?>
																		<i class="fa fa-pencil-square-o fa-6"></i>
																	<?php } ?>
																</div>
																<div class="cd-timeline-content card_main">
																	<div class="p-20">
																		<div class="timeline-details">
																			<?php if ($p_h['tag'] == 'insert') { ?>
																				<p class="m-t-20"> Proposal added</p>
																			<?php } ?>
																			<?php if ($p_h['tag'] == 'Sent') { ?>
																				<p class="m-t-20">You have sent a proposal </p>
																			<?php } ?>
																			<?php if ($p_h['tag'] == 'update') { ?>
																				<p class="m-t-20"> You have update</p>
																			<?php } ?>
																			<?php if ($p_h['tag'] == 'followup') { ?>
																				<p class="m-t-20">Follow up mail send</p>
																			<?php } ?>
																			<?php if ($p_h['tag'] == 'comments') { ?>
																				<p class="m-t-20"> Proposal Comments</p>
																			<?php } ?>
																			<?php if ($p_h['tag'] == 'internal_notes') { ?>
																				<p class="m-t-20">You have add Intenal Notes </p>
																			<?php } ?>
																			<?php if ($p_h['tag'] == 'quantity_update') { ?>
																				<p class="m-t-20">Quantity updated</p>
																			<?php } ?>
																			<?php if ($p_h['tag'] == 'optional') { ?>
																				<p class="m-t-20">optional section updated</p>
																			<?php } ?>

																			<?php if ($p_h['tag'] == 'accepted') { ?>
																				<p class="m-t-20">Proposal was accepted</p>
																			<?php } ?>
																			<?php if ($p_h['tag'] == 'declined') { ?>
																				<p class="m-t-20">proposal was declined</p>
																			<?php } ?>
																			<?php if ($p_h['tag'] == 'in discussion') { ?>
																				<p class="m-t-20">proposal is in Indiscussion </p>
																			<?php } ?>

																			<?php if ($p_h['tag'] == 'draft') { ?>
																				<p class="m-t-20">proposal is in Draft</p>
																			<?php } ?>

																			<?php if ($p_h['tag'] == 'viewed') { ?>
																				<p class="m-t-20">proposal was opened</p>
																			<?php } ?>
																			<?php if ($p_h['tag'] == 'invoice') { ?>
																				<p class="m-t-20">proposal Invoice Created</p>
																			<?php } ?>


																		</div>
																	</div>
																	<span class="cd-date"><?php
																							$timestamp = strtotime($p_h['created_at']);
																							$newDate = date('d F Y H:i', $timestamp);
																							echo $newDate;
																							?></span>
																</div>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- accordion-panel -->

								<div class="right-side-proposal down-op">
									<div class="accordion-panel edit-panel1">
										<div class="downoption">
											<ul>

												<li> <a href="<?php echo base_url(); ?>uploads/proposal_pdf/<?php echo $records['pdf_file']; ?>" download class="ideal-ty">Download PDF Version</a></li>

												<li><a href="<?php echo base_url(); ?>proposal_page/Googledrive" class="ideal-ty" target="_blank">Save pdf to google drive</a></li>
												<li><a href="javascript:;" data-toggle="modal" data-target="#followup" class="ideal-ty">Send a follow up email</a></li>
												<?php $randomstring = generateRandomString('100'); ?>
												<li><a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $records['id']; ?>" class="ideal-ty">Copy as a new</a></li>
												<!--    <li><a href="javascript:;" class="ideal-ty convert-template" id="<?php echo $records['id']; ?>">Convert to template</a></li> -->
												<li><a href="javascript:;" onclick="exportpricelist()" class="ideal-ty">Export Pricing Table to Excel</a></li>
												<li>
													<!-- <div style="display: none;">
														<input type="text" name="pdf_password" id="copyTarget" value="<?php echo $records['pdf_password']; ?>">
													</div> -->
													<span id="copyTarget" style="display:none;"><?php echo $records['pdf_password']; ?></span>
													<a href="javascript:;" class="ideal-ty copy-btn" id="copyButton">Copy access key</a>
												</li>
												<?php if ($records['status'] != 'draft') {
													$randomstring = generateRandomString('100');
												?>
													<li><a href="<?php echo base_url(); ?>Proposal/proposal/<?php echo $randomstring; ?>---<?php echo $records['id']; ?>">Proposal Details</a></li>
												<?php } ?>

												<?php if ($records['status'] == 'accepted') {
													$randomstring = generateRandomString('100');
												?>
													<li><a href="<?php echo base_url(); ?>Invoice/ProposalInvoice/<?php echo $randomstring; ?>---<?php echo $records['id']; ?>">Create Invoice</a></li>
												<?php } ?>



											</ul>
										</div>
										<div class="sent-fromto">

											<div class="tax-dept-01">
												<div class="tax-head">
													<a class="accordion-msg">Sent From</a>
												</div>
												<blockquote class="blockquote">
													<div class="tax-over1">
														<img class="img-fluid p-absolute" src="<?php echo base_url(); ?>/assets/images/avatar-2.jpg" alt="">
														<div class="undetails">
															<?php if ($records['sender_company_name'] == '') { ?>
																<p class="m-b-0"> <span> ACCOTAX Accountants & Tax Consultants </span> </p>
																<h6>test@gmail.com</h6>
															<?php } else { ?>
																<p class="m-b-0"> <span> <?php echo $records['sender_company_name']; ?> </span></p>
																<h6><?php echo $records['sender_company_mailid']; ?></h6>
															<?php } ?>
														</div>
													</div>
												</blockquote>
											</div>

											<div class="tax-dept-01">
												<div class="tax-head">
													<a class="accordion-msg">Sent To</a>
												</div>
												<blockquote class="blockquote">
													<div class="tax-over1">
														<img class="img-fluid p-absolute" src="<?php echo base_url(); ?>/assets/images/avatar-2.jpg" alt="">
														<div class="undetails">
															<?php if ($records['receiver_mail_id'] == '') { ?>
																<p class="m-b-0"> <span> <?php echo $records['company_name']; ?> </span> </p>
																<h6><?php echo $company_details['crm_email']; ?></h6>
															<?php } else { ?>
																<h6> <span> <?php echo $records['receiver_company_name']; ?> </span></h6>
																<h6><?php echo $records['receiver_mail_id']; ?></h6>
															<?php } ?>
														</div>
													</div>
												</blockquote>
											</div>


										</div>
									</div>
								</div>
							</div> <!-- right proposal -->
						</div>
					</div>
				</div>
			</div>
			<!-- 4tab -->
		</div>
		</div>
		<!-- tabcontent -->
		</div>
		</form>
	</div>
</div>
<div id="select-category" class="modal fade lorem-company" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Please Select</h4>
			</div>
			<div class="modal-body">
				<div class="row seacrh-header">
					<div class="col-sm-12">
						<div class="input-group input-group-button input-group-primary">
							<input type="text" class="form-control" id="search_service_name" placeholder="Search here...">
							<button class="btn btn-primary input-group-addon" id="basic-addon1">Search</button>
						</div>
					</div>
				</div>
				<div class="category-choose1 row">
					<div class="col-sm-5 col-xs-12">
						<div class="categories-list-item">
							<h2>Categories</h2>
							<span></span>
							<ul class="nav nav-tabs">
								<li nav-item><a class="nav-link tab-used  active" data-toggle="tab" href="#all">All Categories</a></li>
								<?php $i = 1;
								foreach ($category as $cat) {
									if ($cat['category_type'] == 'service') {
										$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
										$lesg = str_replace(' ', '-', $strlower);
										$lesgs = str_replace('--', '-', $lesg);
								?>
										<li nav-item><a class="nav-link tab-used" data-toggle="tab" href="#<?php echo $lesgs; ?>"><?php echo $cat['category_name']; ?></a></li>
								<?php
										$i++;
									}
								} ?>
								<ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-7 sss tab-content">
						<div class="lorem-content1 service-lists" style="display: none;">
						</div>
						<div class="lorem-content1  tab-pane fade in active" id="all">
							<?php
							foreach ($services as $service) {
							?>
								<div class="annual-lorem " id="<?php echo $service['id']; ?>" onclick="myFunction(this.id)">
									<h3><?php echo $service['service_name']; ?></h3>
									<strong>- &euro;<?php echo $service['service_price']; ?>/<?php echo $service['service_unit']; ?></strong>
								</div>
							<?php } ?>
						</div>
						<?php
						$i = 1;
						foreach ($category as $cat) {
							if ($cat['category_type'] == 'service') {
								$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
								$lesg = str_replace(' ', '-', $strlower);
								$lesgs = str_replace('--', '-', $lesg);
						?>
								<div class="lorem-content1  tab-pane fade" id="<?php echo $lesgs; ?>">
									<?php
									foreach ($services as $service) {
										if ($service['service_category'] == $cat['category_name']) {
									?>
											<div class="annual-lorem " id="<?php echo $service['id']; ?>" onclick="myFunction(this.id)">
												<h3><?php echo $service['service_name']; ?></h3>
												<strong>- &euro;<?php echo $service['service_price']; ?>/<?php echo $service['service_unit']; ?></strong>
											</div>
									<?php }
									} ?>
								</div>
						<?php
								$i++;
							}
						} ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="#" data-dismiss="modal" class="closed">Close</a>
				<input type="submit" name="save" id="company_added" value="Save" class="btn btn-primary">
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="followup" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<p>Subject:</p>
					<p class="followup_success" style="display: none;">Followup Mail Send successfully</p>
					<input type="hidden" name="proposal_id" id="proposal_id" value="<?php echo $records['id']; ?>">
					<textarea rows="5" cols="50" id="followup_subject"> <?php echo preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($followup_subject))); ?></textarea>
				</h4>
				<span id="followup_subject_er" style="color: red;"></span>
			</div>
			<div class="modal-body">
				<p><b>Message:</b></p>
				<textarea rows="10" cols="50" id="followup_body">  <?php echo preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($followup_body))); ?></textarea>
				<span id="followup_body_er" style="color: red;"></span>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="followup_send">Send</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div id="select-products" class="modal fade lorem-company" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Please Select</h4>
			</div>
			<div class="modal-body">
				<div class="row seacrh-header">
					<div class="col-sm-12">
						<div class="input-group input-group-button input-group-primary">
							<input type="text" class="form-control" id="search_product_name" placeholder="Search here...">
							<button class="btn btn-primary input-group-addon" id="basic-addon1">Search</button>
						</div>
					</div>
				</div>
				<div class="category-choose1 row">
					<div class="col-sm-5 col-xs-12">
						<div class="categories-list-item">
							<h2>Categories</h2>
							<span></span>
							<ul class="nav nav-tabs">
								<li nav-item><a class="nav-link tab-used1  active" data-toggle="tab" href="#product_all">All Categories</a></li>
								<?php $i = 1;
								foreach ($category as $cat) {
									if ($cat['category_type'] == 'product') {
										$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
										$lesg = str_replace(' ', '-', $strlower);
										$lesgs = str_replace('--', '-', $lesg);
								?>
										<li nav-item><a class="nav-link tab-used1" data-toggle="tab" href="#<?php echo $lesgs; ?>"><?php echo $cat['category_name']; ?></a></li>
								<?php
										$i++;
									}
								} ?>
								<ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-7 tab-content">
						<div class="lorem-content1 product_lists" style="display: none;"></div>
						<div class="lorem-content1  tab-pane fade in active" id="product_all">
							<?php
							foreach ($products as $product) { ?>
								<div class="annual-lorem" id="<?php echo $product['id']; ?>" onclick="myFunction2(this.id)">
									<h3><?php echo $product['product_name']; ?></h3>
									<strong>- &euro;<?php echo $product['product_price']; ?></strong>
								</div>
							<?php } ?>
						</div>
						<?php
						$i = 1;
						foreach ($category as $cat) {
							if ($cat['category_type'] == 'product') {
								$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
								$lesg = str_replace(' ', '-', $strlower);
								$lesgs = str_replace('--', '-', $lesg);
						?>
								<div class="lorem-content1  tab-pane fade" id="<?php echo $lesgs; ?>">
									<?php
									foreach ($products as $product) {
										if ($product['product_category'] == $cat['category_name']) {
									?>
											<div class="annual-lorem" id="<?php echo $product['id']; ?>" onclick="myFunction2(this.id)">
												<h3><?php echo $product['product_name']; ?></h3>
												<strong>- &euro;<?php echo $product['product_price']; ?></strong>
											</div>
									<?php }
									} ?>
								</div>
						<?php
								$i++;
							}
						} ?>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" data-dismiss="modal" class="closed">Close</a>
			<input type="submit" name="save" id="company_added" value="Save">
		</div>
	</div>
</div>
</div>
<div id="select-subscriptions" class="modal fade lorem-company" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Please Select</h4>
			</div>
			<div class="modal-body">
				<div class="row seacrh-header">
					<div class="col-sm-12">
						<div class="input-group input-group-button input-group-primary">
							<input type="text" class="form-control" id="search_subscription_name" placeholder="Search here...">
							<button class="btn btn-primary input-group-addon" id="basic-addon1">Search</button>
						</div>
					</div>
				</div>
				<div class="category-choose1 row">
					<div class="col-sm-5 col-xs-12">
						<div class="categories-list-item">
							<h2>Categories</h2>
							<span></span>
							<ul class="nav nav-tabs">
								<li nav-item><a class="nav-link tab-used2  active" data-toggle="tab" href="#subscription_all">All Categories</a></li>
								<?php $i = 1;
								foreach ($category as $cat) {
									if ($cat['category_type'] == 'subscription') {
										$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
										$lesg = str_replace(' ', '-', $strlower);
										$lesgs = str_replace('--', '-', $lesg);
								?>
										<li nav-item><a class="nav-link tab-used2" data-toggle="tab" href="#<?php echo $lesgs; ?>"><?php echo $cat['category_name']; ?></a></li>
								<?php
										$i++;
									}
								} ?>
								<ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-7 tab-content">
						<div class="lorem-content1 subscription_lists" style="display: none;"></div>
						<div class="lorem-content1  tab-pane fade in active" id="subscription_all">
							<?php
							foreach ($subscription as $sub) { ?>
								<div class="annual-lorem" id="<?php echo $sub['id']; ?>" onclick="myFunction3(this.id)">
									<h3><?php echo $sub['subscription_name']; ?></h3>
									<strong>- &euro;<?php echo $sub['subscription_price']; ?></strong>
								</div>
							<?php } ?>
						</div>
						<?php
						$i = 1;
						foreach ($category as $cat) {
							$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
							$lesg = str_replace(' ', '-', $strlower);
							$lesgs = str_replace('--', '-', $lesg);
						?>
							<div class="lorem-content1  tab-pane fade" id="<?php echo $lesgs; ?>">
								<?php
								foreach ($subscription as $sub) {
									if ($sub['subscription_category'] == $cat['category_name']) {
								?>
										<div class="annual-lorem" id="<?php echo $sub['id']; ?>" onclick="myFunction3(this.id)">
											<h3><?php echo $sub['subscription_name']; ?></h3>
											<strong>- &euro;<?php echo $sub['subscription_price']; ?></strong>
										</div>
								<?php }
								} ?>
							</div>
						<?php
							$i++;
						} ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="#" data-dismiss="modal" class="closed">Close</a>
				<input type="submit" name="save" id="company_added" value="Save">
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="googledrive" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Share Proposal Pdf to google Drive</h4>
			</div>
			<div class="modal-body">
				Are You Want To share pdf to Drive?
				<form method="post" id="pdf_to_drive" action="<?php echo $url; ?>">
					<input type="hidden" value="Googledrive_Upload" name="googledrive">
					<input type="submit" value="Save PDF to Google Drive" id="pdf_send" name="submit">
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- common-reduce01 -->
<?php $this->load->view('includes/session_timeout'); ?>
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('proposal/scripts.php'); ?>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>
	$.validate({
		lang: 'en'
	});
</script>
<?php
if (isset($this->session->userdata['popup'])) {
	if ($this->session->userdata['popup'] == 'google_drive') { ?>
		<script type="text/javascript">
			$("#googledrive").modal('show');
			//  $("#pdf_send1").click(function(event){
			//     event.preventDefault();
			//     var id="<?php //echo $this->session->userdata['proposal_id']; 
							?>";
			//        $.ajax({
			//             url: '<?php //echo base_url(); 
									?>/proposal_pdf/session_destroying',
			//             data: { id:id },
			//             type: 'post',
			//             dataType: 'json',
			//             success: function (response) {
			//             // alert(response);
			//             //     var json = JSON.parse(response);
			//             //     status=json['status'];
			//             //     alert(status) ;  
			//             //     if(status=='1'){   
			//              $("#pdf_send").trigger('click');

			//    // window.location.href = '<?php echo base_url(); ?>/proposal_page/step_proposal/zt9t59xlxkqMgQSOv2SqW0pL71Hcm4iWjWtfVUTe35cLTM5DQuavNGCECQAmodT1ht4829pF0T4wZGc2XsxW0ODXyNp8jM8emGAX'+id;
			// //}
			//                //window.location.reload();
			//             }
			//           });

			//  });
			<?php if ($this->uri->segment(2) == 'step_proposal') { ?>

				location.reload();

			<?php } ?>
		</script>
<?php }
} ?>
<script type="text/javascript">
	function editable_content(con) {
		$(con).find('.price').attr("contentEditable", true);
	}
</script>
<script type="text/javascript">
	tinymce_config['selector'] = '#body';
	tinymce_config['inline'] = true;
	tinymce.init(tinymce_config);

	var urls_name = '<?php echo base_url(); ?>proposals/tab_details';
	$(".status_change").change(function() {
		// alert($(this).val());
		var status = $(this).val();
		var id = $(this).attr('id');
		if (status == 'sent') {
			var formData = {
				'proposal_id': id
			};
			$.ajax({
				url: '<?php echo base_url(); ?>proposals/send_proposal',
				type: 'POST',
				data: formData,
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					//alert(data);
					$(".LoadingImage").hide();
					$(".proposal").show();
					setTimeout(function() {
						$(".proposal").hide();
						window.location.href = '<?php echo base_url(); ?>/proposal';
					}, 2000);


					//alert(data);
				}
			});


		} else {


			if (status == 'draft') {
				$(".draft_sect_ion").show();
			}

			//  alert(id);
			var formData = {
				'id': id,
				'status': status
			};
			$.ajax({
				url: '<?php echo base_url(); ?>proposals/status_change',
				type: 'POST',
				data: formData,
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();

					var res = JSON.parse(data);

					if (res['status'] == '1') {
						$('div .proposal').find('div .position-alert1').html('Status Changed Successfully!');
						$('.proposal').show();
					} else {
						$('div .proposal').find('div .position-alert1').html(res['msg']);
						$('.proposal').show();
					}

					if (typeof(res['user_id']) != "undefined") {
						setTimeout(function() {
							$(".proposal").hide();
							window.location.href = '<?php echo base_url(); ?>client/client_infomation/' + res['user_id'];
						}, 2000);
					} else if (typeof(res['lead_id']) != "undefined") {
						setTimeout(function() {
							$(".proposal").hide();
							window.location.href = '<?php echo base_url(); ?>leads/leads_detailed_tab/' + res['lead_id'];
						}, 2000);
					}
				}
			});

		}
	});








	$(".send_propos").click(function() {
		//alert('ppp');
		var proposal_id = $(this).attr('id');
		$.ajax({
			url: '<?php echo base_url(); ?>proposals/send_proposal',
			type: 'POST',
			data: {
				'proposal_id': proposal_id
			},
			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(data) {
				//alert(data);
				$(".LoadingImage").hide();
				$(".proposal").show();
				// setTimeout(function(){  $(".proposal").hide();  }, 2000);
				//  window.location.href = '<?php echo base_url(); ?>/proposal';
				//alert(data);
			}
		});
	});

	$('#message_comments').keydown(function(event) {

		if (event.keyCode == 13 && !event.shiftKey) {
			event.preventDefault();
			var sender_id = $('#sender_id').val();
			var receiver_company_name = $('#receiver_companyname').val();
			var id = $('#id').val();
			var message = $("#attach_img").val() + $("#message_comments").val();

			if ($("#image_type").val() != '') {
				var types = $("#image_type").val();
			} else {
				var types = 0;
			}

			var image_message = $("#messages_comments").val();
			var formData = {
				sender_id: sender_id,
				receiver_company_name: receiver_company_name,
				proposal_id: id,
				message: message,
				types: types,
				image_message: image_message
			};

			$.ajax({
				data: formData,
				type: "post",
				url: "<?php echo base_url(); ?>/Proposal_pdf/conversation",
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					// alert(data);   
					var json = JSON.parse(data);
					msg_content = json['msg_content'];
					// $('.messagess').val("");
					$('#message_lists').val("");
					$('#message_lists').html('');
					$("#message_comments").val("");
					$('#message_lists').append(msg_content);
					// location.reload();
				}
			});


		}
	});


	$(document).on('click', '.send_msg', function(e) {
		event.preventDefault();

		var sender_id = $('#sender_id').val();
		var receiver_company_name = $('#receiver_companyname').val();
		var id = $('#id').val();
		var msg = $("#message_comments");
		var message = $("#attach_img").val() + msg.val();
		if ($("#image_type").val() != '') {
			var types = $("#image_type").val();
		} else {
			var types = 0;
		}
		$(msg).next('p').html('');
		// console.log(types);   
		var image_message = $("#messages_comments").val();
		var formData = {
			sender_id: sender_id,
			receiver_company_name: receiver_company_name,
			proposal_id: id,
			message: message,
			types: types,
			image_message: image_message
		};

		if ($.trim(msg.val()) != '') {
			msg.css('border', '');
			$.ajax({
				data: formData,
				type: "post",
				url: "<?php echo base_url(); ?>/Proposal_pdf/conversation",
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					var json = JSON.parse(data);
					msg_content = json['msg_content'];
					$('#message_lists').val("");
					$("#message_comments").val("");
					// $('.messagess').val("");
					$('#message_lists').html('');
					$('#message_lists').append(msg_content);
					//location.reload();
				}
			});
		} else {
			msg.css('border', '1px solid red');
			$(msg).next('p').css('color', 'red');
			$(msg).next('p').html('Required.');
		}

	});

	$("#followup_send").click(function() {

		$('#followup_subject p').each(function() {
			var $this = $(this);
			if ($this.html().replace(/\s|&nbsp;/g, '').length == 0)
				$this.remove();
		});

		var followup_subject = $("#followup_subject").val().trim();
		var followup_body = $("#followup_body").val().trim();
		var proposal_id = $("#proposal_id").val();


		if (followup_subject != "" && proposal_id != "" && followup_subject != null && followup_body != "" && followup_body != null) {
			$('#followup_subject_er').html('');
			$('#followup_body_er').html('');

			var formData = {
				'subject': followup_subject,
				'body': '<?php echo $followup_body; ?>',
				'proposal_id': proposal_id
			}

			$.ajax({
				data: formData,
				type: "post",
				url: "<?php echo base_url(); ?>/Proposal_pdf/followup",
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();

					$(".LoadingImage").hide();
					var json = JSON.parse(data);
					status = json['status'];
					if (status == '1') {
						$(".followup").show();
						setTimeout(function() {
							$(".followup").hide();
							$("#followup").modal('hide');
						}, 3000);
					}
				}
			});

		} else {
			if (followup_subject == "" || followup_subject == null) {
				$('#followup_subject_er').html('Required.');
			}

			if (followup_body == "" || followup_body == null) {
				$('#followup_body_er').html('Required.');
			}
		}

	});


	$(".close").click(function() {
		$(".alert").hide();
	});


	function exportpricelist() {
		var url = "<?php echo base_url(); ?>/proposal_pdf/Export/<?php echo $records['id']; ?>";
		window.location = url;
	}

	$(".convert-template").click(function() {
		var proposal_id = $(this).attr('id');
		$.ajax({
			url: '<?php echo base_url(); ?>proposal_pdf/convert_template',
			type: 'POST',
			data: {
				'id': proposal_id
			},
			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(data) {
				$(".LoadingImage").hide();
				$('.alert-success').show();
				setTimeout(function() {
					$('.alert-success').hide();
					location.reload();
				}, 3000);
			}
		});
	});

	$("#internal_save").click(function() {

		var internal_notes = $("#internal_notes").val();
		var proposal_id = $("#p_id").val();

		if (internal_notes != "" && proposal_id != "" && internal_notes != null) {
			$('#internal_notes_er').html('');

			$.ajax({
				url: '<?php echo base_url(); ?>proposal_pdf/internal_notes',
				type: 'POST',
				data: {
					'id': proposal_id,
					'internal_notes': internal_notes
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					var json = JSON.parse(data);
					$("#internal_notes").val('');
					msg_content = json['msg_content'];
					$("#internal_notes_section").html('');
					$("#internal_notes_section").append(msg_content);
				}
			});
		} else {
			$('#internal_notes_er').html('Required.');
		}
	});

	$(document).ready(function() {
		$(window).scroll(function() {
			var sticky = $('#option-tabs'),
				scroll = $(window).scrollTop();

			if (scroll >= 40) {
				sticky.addClass('fixed');
			} else {
				sticky.removeClass('fixed');

			}
		});

		$("#tax_details").trigger('change');

		$(window).scroll(function() {
			var sticky1 = $('#mail-template, .container-fullscreen'),
				scroll = $(window).scrollTop();

			if (scroll >= 40) {
				sticky1.addClass('fixed1');
			} else {
				sticky1.removeClass('fixed1');

			}
		});


		$('.accordion-views .toggle').click(function(e) {
			$(this).next('.inner-views').slideToggle(300);
			$(this).toggleClass('this-active');
		});

	});

	$(document).ready(function() {
		$('#start_tab').removeClass('active');

		var formData = {
			'item_name': <?php echo json_encode(explode(',', $records['item_name'])); ?>,
			'price': <?php echo json_encode(explode(',', $records['price'])); ?>,
			'unit': <?php echo json_encode(explode(',', $records['unit'])); ?>,
			'qty': <?php echo json_encode(explode(',', $records['qty'])); ?>,
			'tax': <?php echo json_encode(explode(',', $records['tax'])); ?>,
			'discount': <?php echo json_encode(explode(',', $records['discount'])); ?>,
			'description': <?php echo json_encode(explode(',', $records['description'])); ?>,
			'service_optional': <?php echo json_encode(explode(',', $records['service_optional'])); ?>,
			'service_quantity': <?php echo json_encode(explode(',', $records['service_quantity'])); ?>,
			'product_name': <?php echo json_encode(explode(',', $records['product_name'])); ?>,
			'product_price': <?php echo json_encode(explode(',', $records['product_price'])); ?>,
			'product_qty': <?php echo json_encode(explode(',', $records['product_quantity'])); ?>,
			'product_tax': <?php echo json_encode(explode(',', $records['product_tax'])); ?>,
			'product_discount': <?php echo json_encode(explode(',', $records['product_discount'])); ?>,
			'product_description': <?php echo json_encode(explode(',', $records['product_description'])); ?>,
			'product_optional': <?php echo json_encode(explode(',', $records['product_optional'])); ?>,
			'product_quantity': <?php echo json_encode(explode(',', $records['product_quantity'])); ?>,
			'subscription_name': <?php echo json_encode(explode(',', $records['subscription_name'])); ?>,
			'subscription_price': <?php echo json_encode(explode(',', $records['subscription_price'])); ?>,
			'subscription_unit': <?php echo json_encode(explode(',', $records['subscription_unit'])); ?>,
			'subscription_tax': <?php echo json_encode(explode(',', $records['subscription_tax'])); ?>,
			'subscription_discount': <?php echo json_encode(explode(',', $records['subscription_discount'])); ?>,
			'subscription_description': <?php echo json_encode(explode(',', $records['subscription_description'])); ?>,
			'subscription_optional': <?php echo json_encode(explode(',', $records['subscription_optional'])); ?>,
			'subscription_quantity': <?php echo json_encode(explode(',', $records['subscription_quantity'])); ?>,
			'total_total': '<?php echo $records['total_amount']; ?>',
			'discount_amount': '<?php echo $records['discount_amount']; ?>',
			'tax_total': '<?php echo $records['tax_amount']; ?>',
			'grand_total': '<?php echo $records['grand_total']; ?>',
			'currency_symbol': '<?php echo $records['currency']; ?>',
			'tax_details': '<?php echo $records['tax']; ?>',
			'tax_option': '<?php echo $records['tax_option']; ?>',
			'discount_option': '<?php echo $records['discount_option']; ?>'
		};
		getMailTemplate(formData);

		//$('input,select').attr('disabled','disabled');  
		$('.status_change').removeAttr('disabled');

		$(".f-right").click(function() {
			$('#message_lists').toggleClass('auto-height');
		});

		$("#tabs-3 span.f-right").click(function() {
			$(this).text($(this).text() == 'see all comments' ? 'see less comments' : 'see all comments');
		});

		$("#copyButton").click(function() {
			if ($("#copyTarget").html() == '') {
				$(".copy-btn").html("");
				$(".copy-btn").append('Access Key Not Inserted');
			} else {
				//alert($("#copyTarget").val());
				$(".copy-btn").html("");
				$(".copy-btn").append('Copied');
				setTimeout(function() {
					$(".copy-btn").html("");
					$(".copy-btn").append('Copy access key');
				}, 5000);
			}
		});

		$(document).on('click', '.previous-step', function() {
			var ref_this = $(".addnewclient_pages1 ol.nav-tabs li a.active");
			var prev = $('.addnewclient_pages1 .nav-tabs .active').closest('li').prev('li').find('a');

			$(ref_this).removeClass('active');
			$(prev).addClass('active');
			$(".signed-change3").parent('.divright').show();
			$($(ref_this).attr("href")).removeClass('active');
			$($(prev).attr("href")).addClass('active');
		});

		$('.next-tab').click(function() {
			var r_this = $(".addnewclient_pages1 ol.nav-tabs li a.active");
			var next = $('.addnewclient_pages1 .nav-tabs .active').closest('li').next('li').find('a');

			$(r_this).removeClass('active');
			$(next).addClass('active');
			$($(r_this).attr("href")).removeClass('active');
			$($(next).attr("href")).addClass('active');

			if ($(next).attr("href") == '#finallize_tab') {
				$(".signed-change3").parent('.divright').hide();
			} else {
				$(".signed-change3").parent('.divright').show();
			}
		});

	});

	/*Copied Link Scripr */
	document.getElementById("copyButton").addEventListener("click", function() {
		//  alert('ok');
		copyToClipboard(document.getElementById("copyTarget"));

	});

	function copyToClipboard(elem) {
		//  alert(elem);
		// create hidden text element, if it doesn't already exist
		var targetId = "_hiddenCopyText_";
		var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
		var origSelectionStart, origSelectionEnd;
		if (isInput) {
			// can just use the original source element for the selection and copy
			target = elem;
			origSelectionStart = elem.selectionStart;
			origSelectionEnd = elem.selectionEnd;
		} else {
			// must use a temporary form element for the selection and copy
			target = document.getElementById(targetId);
			if (!target) {
				var target = document.createElement("textarea");
				target.style.position = "absolute";
				target.style.left = "-9999px";
				target.style.top = "0";
				target.id = targetId;
				document.body.appendChild(target);
			}
			target.textContent = elem.textContent;
		}
		// select the content
		var currentFocus = document.activeElement;
		target.focus();
		target.setSelectionRange(0, target.value.length);

		// copy the selection
		var succeed;
		try {
			succeed = document.execCommand("copy");
		} catch (e) {
			succeed = false;
		}
		// restore original focus
		if (currentFocus && typeof currentFocus.focus === "function") {
			currentFocus.focus();
		}

		if (isInput) {
			// restore prior selection
			elem.setSelectionRange(origSelectionStart, origSelectionEnd);
		} else {
			// clear temporary content
			target.textContent = "";
		}
		return succeed;
	}


	$(".document_attachment_images").click(function() {
		console.log('document images');
		console.log($(this).attr('id'));
		console.log($("#documents_attach").val());
		var y = $("#documents_attach").val().split(',');
		console.log(y);
		var removeItem = $(this).attr('id');
		console.log(removeItem);
		y = jQuery.grep(y, function(value) {
			return value != removeItem;
		});
		console.log(y);

		var id = $("#id").val();

		if (y.length == 0) {
			var y = '';
			console.log('empty');
		}

		$.ajax({
			url: '<?php echo base_url(); ?>/proposal_pdf/document_attachments',
			type: 'POST',
			data: {
				'id': id,
				'document_attachment': y
			},
			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(data) {
				$(".LoadingImage").hide();
				if (data == '1') {
					location.reload();
				}
				// $("#pricelist_name").modal('hide');
				// var json = JSON.parse(data);             
				// content=json['content'];
				// console.log(content);              
				// $('.pricing_listing').html('');
				// $('.pricing_listing').append(content);


			}
		});


	});


	$(".document_disccussion_attachment_images").click(function() {
		console.log('document images');
		console.log($(this).attr('id'));
		// console.log($("#documents_attach").val());
		// var y=$("#documents_attach").val().split(',');
		//  console.log(y);    
		// var removeItem = $(this).attr('id');
		// console.log(removeItem);
		// y = jQuery.grep(y, function(value) {
		//   return value != removeItem;
		// });
		// console.log(y);

		var id = $(this).attr('id');
		console.log(id);



		$.ajax({
			url: '<?php echo base_url(); ?>/proposal_pdf/discuss_attachments',
			type: 'POST',
			data: {
				'id': id
			},
			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(data) {
				$(".LoadingImage").hide();
				if (data == '1') {
					location.reload();
				}
				// $("#pricelist_name").modal('hide');
				// var json = JSON.parse(data);             
				// content=json['content'];
				// console.log(content);              
				// $('.pricing_listing').html('');
				// $('.pricing_listing').append(content);


			}
		});


	});










	$(".attachment_images").click(function() {
		console.log('attachment images');
		console.log($(this).attr('id'));
		console.log('Attach');
		console.log($("#attach").val());
		var y = $("#attach").val().split(',');
		console.log(y);
		console.log('Attach1');
		var removeItem = $(this).attr('id');
		y = jQuery.grep(y, function(value) {
			return value != removeItem;
		});
		console.log(y);

		if (y.length == 0) {
			var y = '';
			console.log('empty');
		}

		var id = $("#id").val();


		$.ajax({
			url: '<?php echo base_url(); ?>/proposal_pdf/attachments',
			type: 'POST',
			data: {
				'id': id,
				'document_attachment': y
			},
			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(data) {
				$(".LoadingImage").hide();

				// if(data=='1'){
				location.reload();
				//  }
				// $("#pricelist_name").modal('hide');
				// var json = JSON.parse(data);             
				// content=json['content'];
				// console.log(content);              
				// $('.pricing_listing').html('');
				// $('.pricing_listing').append(content);


			}
		});
	});



	function chooseFile() {
		document.getElementById("fileInput").click();
	}

	function loadFile(input) {


		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				// $('.image_previewed1').attr('src', e.target.result);
				$('.image_previewed').attr('src', e.target.result);
				$(".popover-lg").css('display', 'none');
				// $("#images_section").val( e.target.result);
			}



			reader.readAsDataURL(input.files[0]);
		}



		//alert('ok');
		console.log('ajax');

		var file_data = $('#fileInput').prop('files')[0];
		var proposal_id = $("#p_id").val();
		var formData = new FormData();
		formData.append('file', file_data);
		formData.append('proposal_id', proposal_id);

		//var formData = new FormData(this);
		var url = $("#uls").val();
		console.log(url);
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url(); ?>proposal_pdf/attachments_upload',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(data) {
				$(".LoadingImage").hide();
				// console.log("success");
				// console.log(data);

				var json = JSON.parse(data);
				var image_name = json['image_name'];
				var type = json['type'];
				console.log(image_name);
				// image_attachment
				//  var image_ssss='<img src='+ image_name +' style="width:100px; height:auto;">';
				$(".ima").show();

				var static_image = '<?php echo base_url(); ?>assets/images/doc-icon.jpg';


				if (type == 'application/pdf') {
					var image_ssss_opacity = '<img src=' + static_image + ' style="width:100px; height:auto;">';
					var image_ssss = '<img src=' + static_image + ' style="width:100px; height:auto;"><span class="closeicon1 document_attachment_images" id="' + static_image + '"><span class="close-ico">×</span></span>';
					var image_original = static_image;

				}


				if (type == 'image/jpeg') {
					var image_ssss_opacity = '<img src=' + image_name + ' style="width:100px; height:auto;">';
					var image_ssss = '<img src=' + image_name + ' style="width:100px; height:auto;"><span class="closeicon1 document_attachment_images" id="' + image_name + '"><span class="close-ico">×</span></span>';
					var image_original = image_name;

				}
				if (type == 'image/png') {

					var image_ssss_opacity = '<img src=' + image_name + ' style="width:100px; height:auto;">';
					var image_ssss = '<img src=' + image_name + ' style="width:100px; height:auto;"><span class="closeicon1 document_attachment_images" id="' + image_name + '"><span class="close-ico">×</span></span>';
					var image_original = image_name;
				}

				if (type == 'application/vnd.ms-excel') {
					var image_ssss_opacity = '<img src=' + static_image + ' style="width:100px; height:auto;">';
					var image_ssss = '<img src=' + static_image + ' style="width:100px; height:auto;"><span class="closeicon1 document_attachment_images" id="' + static_image + '"><span class="close-ico">×</span></span>';
					var image_original = static_image;

				}

				if (type == 'text/csv') {
					var image_ssss_opacity = '<img src=' + static_image + ' style="width:100px; height:auto;">';
					var image_ssss = '<img src=' + static_image + ' style="width:100px; height:auto;"><span class="closeicon1 document_attachment_images" id="' + static_image + '"><span class="close-ico">×</span></span>';
					var image_original = static_image;
				}


				if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {

					var image_ssss_opacity = '<img src=' + static_image + ' style="width:100px; height:auto;">';
					var image_ssss = '<img src=' + static_image + ' style="width:100px; height:auto;"><span class="closeicon1 document_attachment_images" id="' + static_image + '"><span class="close-ico">×</span></span>';
					var image_original = static_image;
				}
				// image_attachment


				$("#image_attachment").html(image_ssss);
				$("#attach_img").val(image_ssss_opacity);
				//  $("#message_comments").val( image_ssss_opacity );
				//   $("#message_comments").css('display','none');
				$("#messages_comments").val(image_original);
				$("#image_type").val(type);
				$(".closeicon1").click(function() {
					location.reload();
				});
				// $("#image_attachment").html( image_ssss );
				// var image_ssss_opacity='<img src='+ image_name +' style="width:100px; height:auto;">';

				// $("#message_comments").val( image_ssss_opacity );
				// $("#message_comments").css('display','none');

			},
			error: function(data) {
				console.log("error");
				console.log(data);
			}
		});




		//  $("#uploadimage").submit();
	}


	function RemoveFile() {
		$('.image_previewed').attr('src', window.base + 'attachment/placeholder_image_upload.png');
	}


	// $(document).ready(function (e) {
	//  $('#uploadimage').on('submit',(function(e) {
	//  // alert('ok');
	// console.log('ajax');
	//   e.preventDefault();

	//     var file_data = $('#fileInput').prop('files')[0];
	//     var id=$("#valid").val();
	//     var form_data = new FormData();
	//     form_data.append('file', file_data);
	//     form_data.append('id', id);



	//         //var formData = new FormData(this);
	//         var url=$("#uls").val();
	//         console.log(url);
	//         $.ajax({
	//             type:'POST',
	//             url: '<?php echo base_url(); ?>proposal_pdf/attachments_upload',
	//             data:formData,
	//             cache:false,
	//             contentType: false,
	//             processData: false,
	//             success:function(data){
	//                 console.log("success");
	//                 console.log(data);
	//             },
	//             error: function(data){
	//                 console.log("error");
	//                 console.log(data);
	//             }
	//         });


	// }));

	// });
</script>