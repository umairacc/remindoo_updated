<?php $this->load->view('includes/header');
$succ = $this->session->flashdata('success');
?>
<style>	
div#cke_1_top {
display: none;
}
div#cke_2_top {
display: none;
}
.columns {
background: #fff;
padding: 20px;
}
.columns:after {
content: "";
clear: both;
display: block;
}
.columns > .editor {
float: left;
width: 65%;
position: relative;
z-index: 1;
}
.columns > .contacts {
float: right;
width: 35%;
box-sizing: border-box;
padding: 0 0 0 20px;
}
#contactList {
list-style-type: none;
margin: 0 !important;
padding: 0;
}
#contactList li {
background: #FAFAFA;
margin-bottom: 1px;
height: 36px;
line-height: 30px;
cursor: pointer;
}
#contactList li:nth-child(2n) {
background: #F3F3F3;
}
#contactList li:hover {
background: #FFFDE3;
border-left: 5px solid #DCDAC1;
margin-left: -5px;
}
.contact {
padding: 0 10px;
white-space: nowrap;
overflow: hidden;
text-overflow: ellipsis;
}
#editor2 .h-card {
background: #FFFDE3;
padding: 3px 6px;
border-bottom: 1px dashed #ccc;
}
#editor1 .h-card {
background: #FFFDE3;
padding: 3px 6px;
border-bottom: 1px dashed #ccc;
}
#editor2 {
border: 1px solid #ccc;
padding: 0 20px;
background: #fff;
position: relative;
}
#editor1 {
border: 1px solid #ccc;
padding: 0 20px;
background: #fff;
position: relative;
}
#editor1 .h-card .p-tel {
font-style: italic;
}
#editor1 .h-card .p-tel::before,
#editor1 .h-card .p-tel::after {
font-style: normal;
}
div#editor1 {
min-height: 400px;
max-height: 600px;
padding: 15px;
overflow: auto;
}
div#editor2 {
min-width: 100%;
max-width: 700px;
padding: 15px;
overflow: auto;
}
.my-error-class {
border-color:1px red;
}
</style>
<div class="pcoded-content pcodedteam-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <!--start-->
			<div class="col-sm-12">
              <!-- Register your self card start -->
              <div class="deadline-crm1 floating_set single-txt12">
                                    <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                       <li class="nav-item">
                                          Proposals
                                          <div class="slide"></div>
                                       </li>
                                    </ul>
                </div>
              <div class="col-12 card">
                <!-- admin start-->
                <div class="client_section user-dashboard-section1 floating_set">
                     <div class="modal-alertsuccess alert alert-success" style="display:none;">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 YOU HAVE SUCCESSFULLY ADDED TEMPLATE
                              </div>
                           </div>
                        </div>
                         <div class="modal-alertsuccess alert alert-success-update" style="display:none;">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 YOU HAVE SUCCESSFULLY UPDATED                              </div>
                           </div>
                        </div>
                        <div class="modal-alertsuccess alert alert-danger" style="display:none;">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                Please Fill All the required fills.
                              </div>
                           </div>
                        </div>                 
          
          <div class="columns">
          	 <div class="edit-tempsect col-xs-12 editor">
          <div class="col-md-12">          
          <label>Title</label>
          </div>
   		 <div class="col-md-12 select">          
         <select  name="title" id="title" class="form-control">
         <option value="">Select </option>
         <option value="new_proposal">New Proposal</option>
         <option value="discuss_proposal">Discuss Proposal</option>
 		 <option value="proposal_followup">Proposal Followup </option>
         </select>
          </div>         
          <div class="col-md-12">          
          <label>Subject</label>
          </div>
       
          <div cols="10" id="editor2" name="editor2" rows="10"  contenteditable="true">			
			</div>
          <div class="col-md-12">          
          <label>Body</label>
          </div>		
			<div cols="10" id="editor1" name="editor1" rows="10"  contenteditable="true">
			
			</div>
		</div>
		<div class="contacts">
			<h5>Adjustable Tokens</h5>
			<ul id="contactList">
				<li>
					<div class="contact h-card" data-contact="0" draggable="true" tabindex="0">
						::Client Name::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="1" draggable="true" tabindex="0">
						::Client Company::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="2" draggable="true" tabindex="0">
						::Client Company City::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="3" draggable="true" tabindex="0">
						::Client Company Country::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="4" draggable="true" tabindex="0">
						::Client Company Religion::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="5" draggable="true" tabindex="0">
						:: Client First Name::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="6" draggable="true" tabindex="0">
						:: Client Last Name::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="7" draggable="true" tabindex="0">
						:: Client Name::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="8" draggable="true" tabindex="0">
						:: ClientPhoneno::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="9" draggable="true" tabindex="0">
						:: ClientWebsite::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="10" draggable="true" tabindex="0">
						:: ClientProposallink::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="11" draggable="true" tabindex="0">
						:: ProposalCurrency::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="12" draggable="true" tabindex="0">
						:: ProposalExpirationDate::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="13" draggable="true" tabindex="0">
						:: ProposalName::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="14" draggable="true" tabindex="0">
						:: ProposalCodeNumber::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="15" draggable="true" tabindex="0">
						::SenderCompany::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="16" draggable="true" tabindex="0">
						::SenderCompanyAddress::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="17" draggable="true" tabindex="0">
						::SenderCompanyCity::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="18" draggable="true" tabindex="0">
						::SenderCompanyCountry::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="19" draggable="true" tabindex="0">
						::SenderCompanyReligion::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="20" draggable="true" tabindex="0">
						::SenderEmail::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="21" draggable="true" tabindex="0">
						::SenderEmailSignature::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="22" draggable="true" tabindex="0">
						::SenderName::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="23" draggable="true" tabindex="0">
						::senderPhoneno::</div>
				</li>
				<li>
					<div class="contact h-card" data-contact="24" draggable="true" tabindex="0">
						:: ProposalTitle::</div>
				</li>
			</ul>
		</div>
		<button id="submit" class="btn btn-primary">Submit</button>
	</div> 
	 </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<script src="https://cdn.ckeditor.com/4.9.0/standard-all/ckeditor.js"></script>
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 99999999999 !important;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style>
	<script>
		CKEDITOR.config.toolbar = [
		['Styles','Format','Font','FontSize'],
		'/',
		['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
		'/',
		['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Image','Table','-','Link','Flash','Smiley','TextColor','BGColor','Source']
		] ;
	
		'use strict';

		var CONTACTS = [
			{ name: 'ClientName' },
			{ name: 'ClientCompany' },
			{ name: ' ClientCompanyCity' },
			{ name: 'ClientCompanyCountry' },
			{ name: 'ClientCompanyReligion' },
			{ name: ' ClientFirstName'},
			{ name: ' ClientLastName' },
			{ name: 'ClientName' },
			{ name: 'ClientPhoneNo' },
			{ name: 'ClientWebsite' },
			{ name: 'ClientProposallink' },
			{ name: 'ProposalCurrency' },
			{ name: 'ProposalExpirationDate' },
			{ name: 'ProposalName' },
			{ name: 'ProposalCodeNumber' },
			{ name:'SenderCompany'},
			{ name:'SenderCompanyAddress'},
			{ name:'SenderCompanyCity'},
			{ name:'SenderCompanyCountry'},
			{ name:'SenderCompanyReligion'},
			{ name:'SenderEmail'},
			{ name:'SenderEmailSignature'},
			{ name:'SenderName'},
			{ name:'senderPhoneno'},
			{ name:'ProposalTitle'}
		];

		CKEDITOR.disableAutoInline = false;
		CKEDITOR.plugins.add( 'hcard', {
			requires: 'widget',
			init: function( editor ) {
				editor.widgets.add( 'hcard', {
					allowedContent: 'span(!h-card); a[href](!u-email,!p-name); span(!p-tel)',
					requiredContent: 'span(h-card)',
					pathName: 'hcard',

					upcast: function( el ) {
						return el.name == 'span' && el.hasClass( 'h-card' );
					}
				} );
				editor.addFeature( editor.widgets.registered.hcard );
				editor.on( 'paste', function( evt ) {
					var contact = evt.data.dataTransfer.getData( 'contact' );
					if ( !contact ) {
						return;
					}
					evt.data.dataValue =						
						'<span class="h-card">' +
							'::'+contact.name+'::'  +
						'</span>';
				} );
			}
		} );

		CKEDITOR.on( 'instanceReady', function() {
			
			CKEDITOR.document.getById( 'contactList' ).on( 'dragstart', function( evt ) {
				
				var target = evt.data.getTarget().getAscendant( 'div', true );

				CKEDITOR.plugins.clipboard.initDragDataTransfer( evt );

				var dataTransfer = evt.data.dataTransfer;				
				dataTransfer.setData( 'contact', CONTACTS[ target.data( 'contact' ) ] );		
				dataTransfer.setData( 'text/html', target.getText() );
				
				if ( dataTransfer.$.setDragImage ) {
					dataTransfer.$.setDragImage( target.findOne( 'img' ).$, 0, 0 );
				}
			} );
		} );

		// Initialize the editor with the hcard plugin.
		CKEDITOR.inline( 'editor1', {
			extraPlugins: 'hcard,sourcedialog,justify'
		} );

		CKEDITOR.inline( 'editor2', {
			extraPlugins: 'hcard,sourcedialog,justify'
		} );

		$("#submit").click(function(){
			var body1=$("#editor1").html();
			var  subject1=$("#editor2").html();
			var  title=$("#title").val();
			var str = "";
			$('.cke_widget_wrapper > span.h-card').each(function(){
				str += $(this).html();				
			});	

			$("span.cke_reset").each(function () {
			$(this).remove();
			});

			$("span.cke_widget_wrapper").each(function () {
			$(this).replaceWith($(this).text());
			});
			$('div[data-cke-hidden-sel="1"]').remove();

			var body=$("#editor1").html();
			var subject=$("#editor2").html();			
			var formData={'body':body,'title':title,'subject':subject};				
			 $.ajax({
	                   url: '<?php echo base_url();?>proposal_page/insert_proposal/',
	                   type : 'POST',
	                   data : formData,	                  
				       beforeSend: function() {
			              $(".LoadingImage").show();
			           },
			           success: function(msg) {
			           	$(".LoadingImage").hide();
			           		if(msg=='0'){
			           			$('.alert-danger').show();
			           		}else{
			           			$('.alert-success').show();
				              setTimeout(function() { 
				              	$('.alert-success').hide();
				              	// location.reload();  
	                              }, 3000);			              
			          		}
			           }

                  });
		});

		$("#title").change(function(){
			alert('ok');
			var  title=this.value;
			 $.ajax({
	                   url: '<?php echo base_url();?>proposal_page/select_proposal/',
	                   type : 'POST',
	                   data : {'title':title},	                  
				       beforeSend: function() {
			              $(".LoadingImage").show();
			           },
			           success: function(msg) {
			           	$(".LoadingImage").hide();			           	
						var json = JSON.parse(msg); 
						subject=json['subject'];
						body=json['body'];			
						$("#editor1").html('');
						$("#editor1").append(body);
						  $("#editor1").attr('contenteditable','true');
						$("#editor2").html('');
						$("#editor2").append(subject);
						 $("#editor2").attr('contenteditable','true');
			           }

                  });
		});

	</script>
</body>

</html>  