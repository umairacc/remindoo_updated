<?php $this->load->view('includes/header'); ?>
<!--   Datatable header by Ram -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/email.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css?ver=2"> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
<!--   End by Ram -->

<!-- tinymce editor -->
<link rel="stylesheet" type="text/css" href="https://www.tiny.cloud/css/codepen.min.css">
<!-- tinymce editor -->

<style type="text/css">
	.dropdown-content {
		display: none;
		position: absolute;
		background-color: #fff;
		min-width: 86px;
		overflow: auto;
		box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
		z-index: 1;
		left: -92px;
		width: 150px;
	}

	.help-block {
		color: red;
		padding: 5px;
	}

	.error_msgs {
		margin-left: 20px !important;
	}

	.delete_error {
		padding: 6px 15px !important;
		font-weight: 600 !important;
		line-height: 21px;
		border-radius: 50px !important;
		border: none;
		text-align: center;
		color: #fff !important;
		display: inline-block;
		min-width: 100px;
		text-transform: uppercase;
		font-size: 13px !important;
	}

	.session_category_delete_no {
		background: #4680ff !important;
	}

	.session_category_delete {
		background: #22b14c !important;
	}

	.error-border {
		border: solid 1px red;
	}

	.filter_check {
		display: none !important;
	}

	p {
		text-align: justify !important;
	}
</style>

<div class="modal-alertsuccess alert" <?php if (empty($_SESSION['success'])) {
											echo 'style="display:none;"';
										} ?>>
	<div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				<?php if (!empty($_SESSION['success'])) {
					echo $_SESSION['success'];
				} ?>
			</div>
		</div>
	</div>
</div>

<div class="modal-alertsuccess alert alert-warning categrory-add" style="display:none;">
	<div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				Category Added
			</div>
		</div>
	</div>
</div>
<div class="modal-alertsuccess alert alert-danger category-exists" style="display:none;">
	<div class="newupdate_alert"> <a href="#" class="close" onclick="$('.category-exists').hide();" data-dismiss1="alert" aria-label1="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				Category Already exists
			</div>
		</div>
	</div>
</div>

<div class="modal-alertsuccess alert alert-danger service-exists" style="display:none;">
	<div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				Service name Already exists
			</div>
		</div>
	</div>
</div>


<div class="modal-alertsuccess alert alert-danger product-exists" style="display:none;">
	<div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				Product name Already exists
			</div>
		</div>
	</div>
</div>

<div class="modal-alertsuccess alert alert-danger subscription-exists" style="display:none;">
	<div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				Subscription name Already exists
			</div>
		</div>
	</div>
</div>

<div class="modal-alertsuccess alert alert-warning category-error" style="display:none;">
	<div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				Category Required
			</div>
		</div>
	</div>
</div>

<div class="modal-alertsuccess alert alert-warning delete-error" style="display:none;">
	<div class="newupdate_alert"><span style="color:#000 !important;font-weight: 600 !important;">Delete Category</span><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
			</div>
		</div>
	</div>
</div>

<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="common-reduce01 floating_set mainbgwhitecls">
			<div class="graybgclsnew button_visibility fee-schedule-package">
				<div class="deadline-crm1 floating_set">
					<?php $this->load->view('proposal/proposal_navigation_tabs'); ?>


					<!--           <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard">
                                <li class="nav-item">
                                  <a class="nav-link active" data-toggle="tab" href="<?php echo base_url(); ?>proposal">dashboard</a>
                                  <div class="slide"></div>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" data-toggle="tab" href="#newactive">proposal</a>
                                  <div class="slide"></div>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" data-toggle="tab" href="#newactive">catalog</a>
                                  <div class="slide"></div>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" data-toggle="tab" href="#frozen">templates</a>
                                  <div class="slide"></div>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" data-toggle="tab" href="#newactive">clients</a>
                                  <div class="slide"></div>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" data-toggle="tab" href="#newactive">settings</a>
                                  <div class="slide"></div>
                                </li>
                              </ul>  -->

					<!--  <div class="create-proposal pro-dash newclscret">
                                <a href="<?php echo base_url(); ?>proposal_page/step_proposal" class="create-proposalbtn btn btn-primary">create proposal</a>
                              </div> -->
				</div>
				<div class="tab-leftside">
					<div class="right-box-05 leftsidesection">
						<h2>Catalog</h2>
						<ul class="nav nav-tabs all_user1 md-tabs floating_set prosaltempmlateulcls">
							<li>
								<a class="active-previous1 <?php
															if (isset($this->session->userdata['tab_name'])) {
															} else {
																echo "active";
															} ?>" data-toggle="tab" href="#start_tab"><span>Services</span><strong>List of all your services</strong></a>
								<div class="sub-categories1">
									<h3>Categories</h3>
									<ul class="category_list">
										<?php
										foreach ($category as $cat) {
											if ($cat['category_type'] == 'service') { ?>
												<li><a href="javascript:;" data-toggle="modal" data-target="#edit-category_service-<?php echo $cat['id']; ?>"><?php echo $cat['category_name']; ?></a>
													<!-- <?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == 1) { ?>
														<a href="javascript:;" class="edit-wed1 icon-c2" data-toggle="modal" data-target="#edit-category_service-<?php echo $cat['id']; ?>"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
													<?php } ?> -->
													<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
														<a href="javascript:;" class="edit-wed1 icon-c3" data-toggle="modal" data-target="#delete-category_service-<?php echo $cat['id']; ?>"><i class="icofont icofont-ui-delete" aria-hidden="true"></i></a>
													<?php } ?>
												</li>
										<?php }
										} ?>

									</ul>
									<div class="send-to">
										<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
											<a href="javascript:;" data-toggle="modal" data-target="#add-new-category_service"><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add new category</a>
										<?php } ?>
									</div>
								</div>
							</li>
							<li><a data-toggle="tab" href="#product_tab" class="active-previous1 <?php
																									if (isset($this->session->userdata['tab_name'])) {
																										if ($this->session->userdata['tab_name'] == 'product_tab') {
																											echo "active";
																										}
																									} ?>"><span>Products</span><strong>List of all your products</strong></a>
								<div class="sub-categories1">
									<h3>Categories</h3>
									<ul class="product_category_list">
										<?php
										foreach ($category as $cat) {
											if ($cat['category_type'] == 'product') {                      ?>
												<li><a href="javascript:;" data-toggle="modal" data-target="#edit-category_product-<?php echo $cat['id']; ?>"><?php echo $cat['category_name']; ?></a>
													<!-- <?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == 1) { ?>
														<a href="javascript:;" class="edit-wed1 icon-c2" data-toggle="modal" data-target="#edit-category_product-<?php echo $cat['id']; ?>"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
													<?php } ?> -->
													<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
														<a href="javascript:;" class="edit-wed1 icon-c3" data-toggle="modal" data-target="#delete-category_service-<?php echo $cat['id']; ?>"><i class="icofont icofont-ui-delete" aria-hidden="true"></i></a>
													<?php } ?>
												</li>

										<?php }
										} ?>

									</ul>
									<div class="send-to">
										<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
											<a href="javascript:;" data-toggle="modal" data-target="#add-new-category_product"><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add new category</a>
										<?php } ?>
									</div>
								</div>
							</li>
							<li><a data-toggle="tab" href="#subscription_tab" class="active-previous1 <?php
																										if (isset($this->session->userdata['tab_name'])) {
																											if ($this->session->userdata['tab_name'] == 'subscription_tab') {
																												echo "active";
																											}
																										} ?>"><span>Subscriptions</span><strong>List of all your Subscriptions</strong></a>
								<div class="sub-categories1">
									<h3>Categories</h3>

									<ul class="subscription_category_list">
										<?php
										foreach ($category as $cat) {
											if ($cat['category_type'] == 'subscription') { ?>
												<li><a href="javascript:;" data-toggle="modal" data-target="#edit-category_subscription-<?php echo $cat['id']; ?>"><?php echo $cat['category_name']; ?></a>
													<!-- <?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == 1) { ?>
														<a href="javascript:;" class="edit-wed1 icon-c2" data-toggle="modal" data-target="#edit-category_subscription-<?php echo $cat['id']; ?>"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
													<?php } ?> -->
													<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
														<a href="javascript:;" class="edit-wed1 icon-c3" data-toggle="modal" data-target="#delete-category_service-<?php echo $cat['id']; ?>"><i class="icofont icofont-ui-delete" aria-hidden="true"></i></a>
													<?php } ?>
												</li>
										<?php }
										} ?>

									</ul>
									<div class="send-to">
										<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
											<a href="javascript:;" data-toggle="modal" data-target="#add-new-category_subscription"><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add new category</a>
										<?php } ?>
									</div>
								</div>
							</li>
							<li><a data-toggle="tab" href="#price_tab" class="active-previous1 <?php
																								if (isset($this->session->userdata['tab_name'])) {
																									if ($this->session->userdata['tab_name'] == 'price_tab') {
																										echo "active";
																									}
																								} ?>"><span>Price Lists</span><strong>Create price lists</strong></a>

							</li>
							<li><a data-toggle="tab" href="#tax_tab" class="active-previous1 <?php
																								if (isset($this->session->userdata['tab_name'])) {
																									if ($this->session->userdata['tab_name'] == 'tax_tab') {
																										echo "active";
																									}
																								} ?>"><span>Taxes</span><strong>Set up tax rules</strong></a>
							</li>
							<!-- <li><a data-toggle="tab" href="#catalog_tab"><span>Settings</span><strong>Edit catalog settings</strong></a></li> -->
						</ul>
					</div>
				</div>
				<div class="right-sidecontent">
					<div class="right-box-05">
						<div class="tab-content fee-schedule12 feeporposalcls <?php if ($_SESSION['permission']['Proposal_Dashboad']['view'] != 1) { ?> permission_deined <?php } ?>">
							<div id="price_tab" class="tab-pane fade <?php
																		if (isset($this->session->userdata['tab_name'])) {
																			if ($this->session->userdata['tab_name'] == 'price_tab') {
																				echo "active show";
																			}
																		} ?>">
								<form class="form-horizontal" action="<?php echo base_url(); ?>proposal_pdf/delete_pricelist" method="post" id="login_form">
									<div class="annual-accounts floating_set">
										<div class="annual-head floating_set">
											<div class="annual-lefts pull-left">
												<h2>Price Lists</h2>
											</div>

											<div class="annual-rights pull-right">
												<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
													<a href="javascript:;" data-toggle="modal" data-target="#import-price_list">Import</a>
												<?php } ?>
												<a href="javascript:;" onclick="export_price_list()">Export</a>
												<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
													<a href="#" data-toggle="modal" data-target="#add_pricelist" class="tab-addnew">Add new</a>
												<?php } ?>
												<!-- <span id="seperater2slash"> / </span> -->
												<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
													<button type="button" id="submit" name="save" data-toggle="modal" data-target="#delete_pricelist" class="btn btn-primary button-loading delete-pricelist" style="display: none;">Delete</button>
												<?php } ?>
											</div>
										</div>

										<div id="delete_pricelist" class="modal fade new-adds-categories" role="dialog">
											<div class="modal-dialog modal-dialog-proposal-del">

												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">×</button>
														<h4 class="modal-title">Delete Confirmation</h4>
													</div>
													<div class="modal-body">
														Do you want to delete?
													</div>

													<div class="modal-footer">
														<button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Yes</button>
														<a href="#" data-dismiss="modal">no</a>
													</div>
												</div>

											</div>
										</div>

										<div class="annual-table floating_set ">
											<!-- <div class="annual-account-head floating_set"-->

											<!-- <div class="annual-field2"><button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Delete</button> </div> -->
											<!--  <div class="annual-field3"></div>
                                  <div class="annual-field3"></div> -->
											<!-- <div class="annual-field3"><a href="#" data-toggle="modal" data-target="#add_pricelist">Add new</a></div>
                              </div> -->
											<div class="common-annual floating_set fee-schduepage">
												<table id="display_service5" class="display nowrap" style="width:100%">
													<thead>
														<tr>
															<th class="delete-annuals1 price_chk_TH Exc-colvis">
																<!-- <div class="checkbox-color checkbox-primary"> -->
																<label class="custom_checkbox1">
																	<input class="border-checkbox" type="checkbox" id="check-box"> <i></i></label>
																<!-- <label class="border-checkbox-label" for="check-box"></label>
                                    </div> -->
															</th>
															<th class="pricename_TH hasFilter">Pricelist name
																<img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
																<div class="sortMask"></div>
																<!-- <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
															</th>
															<!-- <th style="display: none;">Description
                        <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th> -->
															<th class="cost_TH hasFilter">Total Cost
																<img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
																<div class="sortMask"></div>
																<!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
															</th>
															<th class="created_by_TH hasFilter">Created By
																<img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
																<div class="sortMask"></div>
																<!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
															</th>
															<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == '1' || $_SESSION['permission']['Proposal_Dashboad']['delete'] == '1') { ?>
																<th class="price_action_TH Exc-colvis"><strong>Action</strong></th>
															<?php } ?>
														</tr>
													</thead>
													<!--   <tfoot>
                      <tr class="table-header">
                      <th></th>
                      <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>   
                      <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                      <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                      <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>  
                      </tr>
                      </tfoot> -->
													<tbody class="lead-annual">
														<?php
														$i = 1;
														foreach ($price_list as $price) { ?>
															<tr>
																<td class="delete-annuals1">
																	<!-- <div class="checkbox-color checkbox-primary"> -->
																	<label class="custom_checkbox1"> <input class="border-checkbox price_list" type="checkbox" id="checkbox123_<?php echo $i; ?>" name="checkbox[]" value="<?php echo $price['id']; ?>"> <i></i></label>
																	<!--  <label class="border-checkbox-label" for="checkbox123_<?php echo $i; ?>"></label>
                                        </div> -->
																</td>
																<td class="delete-annuals" data-search="<?php echo $price['pricelist_name']; ?>">
																	<h2><?php echo $price['pricelist_name']; ?> </h2>
																</td>

																<!--  <td class="delete-annuals" style="display: none;">
                                        <?php echo $price['pricelist_name']; ?></td> -->

																<td class="delete-annuals3" data-search="<?php echo $price['service_price']; ?>"><strong><?php echo (is_Numeric($price['service_price']) ? $price['service_price'] : 0) + (is_Numeric($price['product_price']) ? $price['product_price'] : 0) + (is_Numeric($price['subscription_price']) ? $price['subscription_price'] : 0); ?></strong></td>
																<td data-search="<?php echo $price['created_by']; ?>">
																	<p><?php if (!empty($price['created_by'])) {
																			echo $price['created_by'];
																		} else {
																			echo $this->Common_mdl->get_price('user', 'id', $price['user_id'], 'crm_name');
																		} ?></p>
																</td>
																<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == '1' || $_SESSION['permission']['Proposal_Dashboad']['delete'] == '1') { ?>
																	<td>
																		<div class="sentclsbuttonbot">
																			<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == 1) { ?>
																				<a href="javascript:;" data-toggle="modal" data-target="#pricelist_edit_<?php echo $price['id']; ?>" class="editicon"><i class="icofont icofont-edit" aria-hidden="true"></i></a>
																			<?php } ?>
																			<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
																				<a href="javascript:;" data-toggle="modal" data-target="#pricelist_delete_<?php echo $price['id']; ?>"><i class="icofont icofont-ui-delete deleteUser" aria-hidden="true"></i></a>
																			<?php } ?>
																		</div>
																	</td>
																<?php } ?>

															</tr>

														<?php $i++;
														} ?>
													</tbody>
												</table>
												<!--  -->
											</div>
										</div>
									</div>
								</form>
								<!-- table -->
							</div> <!-- 2tab -->

							<div id="tax_tab" class="tab-pane fade <?php
																	if (isset($this->session->userdata['tab_name'])) {
																		if ($this->session->userdata['tab_name'] == 'tax_tab') {
																			echo "active show";
																		}
																	} ?>">
								<form class="form-horizontal" action="<?php echo base_url(); ?>proposal_page/delete_tax" method="post" id="login_form">
									<div class="annual-accounts floating_set">
										<div class="annual-head floating_set">
											<div class="annual-lefts pull-left">
												<h2>Taxes</h2>
											</div>
											<div class="annual-rights pull-right">
												<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
													<a href="javascript:;" data-toggle="modal" data-target="#import-tax_list">Import</a>
												<?php } ?>
												<a href="javascript:;" onclick="export_tax_list()">Export</a>
												<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
													<a href="#" data-toggle="modal" data-target="#Tax" class="tab-addnew">Add new</a>
												<?php } ?>
												<!-- <span id="seperater2slash"> / </span> -->
												<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
													<button type="button" id="submit" name="save" data-toggle="modal" data-target="#delete_tax" class="btn btn-primary button-loading delete-tax" style="display: none;">Delete</button>
												<?php } ?>
											</div>
										</div>

										<div id="delete_tax" class="modal fade new-adds-categories" role="dialog">
											<div class="modal-dialog modal-dialog-proposal-del">

												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">×</button>
														<h4 class="modal-title">Delete Confirmation</h4>
													</div>
													<div class="modal-body">
														Do you want to delete?
													</div>

													<div class="modal-footer">
														<button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Yes</button>
														<a href="#" data-dismiss="modal">no</a>
													</div>
												</div>

											</div>
										</div>

										<div class="modal-alertsuccess alert alert-warning" style="display:none;">
											<div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
												<div class="pop-realted1">
													<div class="position-alert1">
														Please Activate Tax
													</div>
												</div>
											</div>
										</div>

										<div class="annual-table floating_set ">
											<div class="common-annual">
												<!--    <div class="annual-field1"></div> -->
												<!--  <div class="annual-field2">Displayed 10 services from 39</div>
                                  <div class="annual-field3"></div>
                                  <div class="annual-field3"></div>
                                  <div class="annual-field3"><strong>Default</strong></div> -->
											</div>
											<div class="common-annual floating_set fee-schduepage">
												<table id="display_service3" class="display nowrap" style="width:100%">
													<thead>
														<tr>
															<th class="delete-annuals1 tax_chk_TH Exc-colvis">
																<!--   <div class="checkbox-color checkbox-primary"> -->
																<label class="custom_checkbox1"> <input class="border-checkbox" type="checkbox" id="checkboxes"> <i></i></label>
																<!--     <label class="border-checkbox-label" for="checkboxes"></label>
                                    </div> -->
															</th>
															<th class="taxname_TH hasFilter">Tax Name
																<img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
																<div class="sortMask"></div>
																<!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
															</th>
															<!-- <th style="display: none;">Description  
                                <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                                <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                </th> -->
															<th class="taxrate_TH hasFilter">Tax Rate <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
																<div class="sortMask"></div>
																<!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select>-->
															</th>
															<th class="status_TH">Status <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
																<div class="sortMask"></div>
																<!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
															</th>
															<th class="default_TH">Default <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
																<div class="sortMask"></div>
																<!-- <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
															</th>
															<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == '1' || $_SESSION['permission']['Proposal_Dashboad']['delete'] == '1') { ?>
																<th class="tax_action_TH Exc-colvis">Action</th>
															<?php } ?>
														</tr>
													</thead>

													<!--            <tfoot>
                              <tr class="table-header">
                              <th></th>
                              <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>  
                              <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>                  
                              
                              <th></th>                 
                              <th></th>               
                            
                              </tr>
                            </tfoot>   -->
													<tbody class="lead-annual">
														<?php
														$i = 1;
														foreach ($taxes as $tax) { ?>
															<tr>
																<td class="delete-annuals1">
																	<!-- <div class="checkbox-color checkbox-primary"> -->
																	<label class="custom_checkbox1"> <input class="border-checkbox taxes" type="checkbox" id="checkbox12_<?php echo $i; ?>" name="checkbox[]" value="<?php echo $tax['id']; ?>"> <i></i></label>
																	<!--  <label class="border-checkbox-label" for="checkbox12_<?php echo $i; ?>"></label>
                                        </div> -->
																</td>
																<td class="delete-annuals" data-search="<?php echo $tax['tax_name']; ?>">
																	<h2><?php echo $tax['tax_name']; ?> </h2>
																</td>

																<!--  <td class="delete-annuals" style="display: none;">
                                        <?php echo $tax['tax_name']; ?> 
                                    </td> -->

																<td class="delete-annuals3" data-search="<?php echo $tax['tax_rate']; ?>"><strong><?php echo $tax['tax_rate']; ?></strong></td>



																<td class="delete-annuals3 fr-checckbo">
																	<!-- <div class="checkbox-color checkbox-primary"> -->
																	<label class="custom_checkbox1"> <input class="border-checkbox optional" type="checkbox" id="checkbox_<?php echo $tax['id']; ?>" name="active_status" value="<?php echo $tax['id']; ?>" <?php if ($tax['active_status'] == '1') {  ?> checked="checked" <?php } ?>> <i></i></label>
																	<!-- <label class="border-checkbox-label optional-label" for="checkbox_<?php echo $tax['id']; ?>"></label>
                                      </div> -->
																</td>


																<td class="delete-annuals3">
																	<div class="radio radio-inline">
																		<label><input type="radio" name="taxStatus" value="<?php echo $tax['id']; ?>" <?php if ($tax['tax_status'] == '1') {  ?> checked="checked" <?php } ?>><i class="helper"></i></label>
																	</div>
																</td>
																<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == '1' || $_SESSION['permission']['Proposal_Dashboad']['delete'] == '1') { ?>
																	<td>
																		<div class="sentclsbuttonbot">
																			<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == 1) { ?>
																				<a href="javascript:;" data-toggle="modal" data-target="#tax_edit_<?php echo $tax['id']; ?>" class="editicon"><i class="icofont icofont-edit" aria-hidden="true"></i></a>
																			<?php } ?>
																			<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
																				<a href="javascript:;" data-toggle="modal" data-target="#tax_delete_<?php echo $tax['id']; ?>"><i class="icofont icofont-ui-delete deleteUser" aria-hidden="true"></i></a>
																			<?php } ?>
																		</div>
																	</td>
																<?php } ?>
															</tr>

														<?php $i++;
														} ?>
													</tbody>
												</table>




											</div>
										</div>
								</form>
							</div>
							<!-- table -->
						</div> <!-- 3tab -->
						<div id="subscription_tab" class="tab-pane sample fade in  <?php
																					if (isset($this->session->userdata['tab_name'])) {
																						if ($this->session->userdata['tab_name'] == 'subscription_tab') {
																							echo "active show";
																						}
																					} ?>">
							<form class="form-horizontal" action="<?php echo base_url(); ?>proposal_page/delete_subscription" method="post" id="login_form">
								<div class="annual-accounts floating_set">

									<div class="annual-head floating_set">
										<div class="annual-lefts pull-left">
											<h2>Subscription</h2>
										</div>
										<div class="annual-rights pull-right">
											<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
												<a href="javascript:;" data-toggle="modal" data-target="#import-subscription">Import</a>
											<?php } ?>
											<a href="javascript:;" onclick="exportsub_excel()">Export</a>
											<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
												<a href="#" data-toggle="modal" data-target="#add-new-subscriptions" class="tab-addnew">Add new</a>
											<?php } ?>
											<!-- <span id="seperater2slash"> / </span> -->
											<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
												<button type="button" data-toggle="modal" data-target="#delete_subscription" id="submit" name="save" class="btn btn-primary button-loading delete-subscription" style="display: none;">Delete</button>
											<?php } ?>

										</div>
									</div>

									<div id="delete_subscription" class="modal fade new-adds-categories" role="dialog">
										<div class="modal-dialog modal-dialog-proposal-del">

											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">×</button>
													<h4 class="modal-title">Delete Confirmation</h4>
												</div>
												<div class="modal-body">
													Do you want to delete?
												</div>

												<div class="modal-footer">
													<button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Yes</button>
													<a href="#" data-dismiss="modal">no</a>
												</div>
											</div>

										</div>
									</div>

									<div class="annual-table floating_set ">
										<div class="common-annual floating_set fee-schduepage">
											<table id="display_service4" class="display nowrap" style="width:100%">
												<thead>
													<tr>
														<th class="delete-annuals1 subscription_chk_TH Exc-colvis" width="10%">
															<!--  <div class="checkbox-color checkbox-primary"> -->
															<label class="custom_checkbox1"> <input class="border-checkbox" type="checkbox" id="checkbox_00"> <i></i></label>
															<!-- <label class="border-checkbox-label" for="checkbox_00"></label>
                                    </div> -->
														</th>
														<th class="description_TH hasFilter" width="50%">Description <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
															<div class="sortMask"></div>
															<!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
														</th>
														<!--  <th style="display: none;">Description   <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> -->
														<th class="price_TH hasFilter" width="10%">Price <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
															<div class="sortMask"></div>
															<!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
														</th>
														<th class="unit_TH hasFilter" width="10%">Unit <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
															<div class="sortMask"></div>
															<!--   <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
														</th>
														<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == '1' || $_SESSION['permission']['Proposal_Dashboad']['delete'] == '1') { ?>
															<th class="sub_action_TH Exc-colvis" width="20%"><strong>Action</strong></th>
														<?php } ?>
													</tr>
												</thead>


												<!--   <tfoot>
                              <tr class="table-header">
                              <th></th>
                              <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>      
                              <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>             
                              <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                              <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                              </tr>
                            </tfoot>  -->


												<tbody class="lead-annual">
													<?php
													$i = 1;
													foreach ($subscription as $sub) { ?>
														<tr>
															<td class="delete-annuals1">
																<!-- <div class="checkbox-color checkbox-primary"> -->
																<label class="custom_checkbox1"><input class="border-checkbox check_sub" type="checkbox" id="check_box<?php echo $i; ?>" name="checkbox[]" value="<?php echo $sub['id']; ?>"> <i></i></label>
																<!--  <label class="border-checkbox-label" for="check_box<?php echo $i; ?>"></label>
                                    </div> -->
															</td>
															<td class="delete-annuals" data-search="<?php echo $sub['subscription_name']; ?>">
																<h2><?php echo $sub['subscription_name']; ?> </h2>
																<p><?php echo $sub['subscription_description']; ?></p>
															</td>
															<!--  <td class="delete-annuals" style="display:none;">
                                    <?php echo $sub['subscription_name'] . '-' . $sub['subscription_description']; ?></td> -->
															<td class="delete-annuals3" data-search="<?php echo $sub['subscription_price']; ?>"><strong>
																	<?php echo $sub['subscription_price']; ?></strong></td>
															<td data-search="<?php $strlower = ucwords(preg_replace("~[\\\\/:*?'&,<>|]~", '', $sub['subscription_unit']));
																				$lesg = str_replace('_', ' ', $strlower);
																				echo $lesg; ?>"><?php $strlower = ucwords(preg_replace("~[\\\\/:*?'&,<>|]~", '', $sub['subscription_unit']));
																		$lesg = str_replace('_', ' ', $strlower);
																		echo $lesg; ?></td>
															<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == '1' || $_SESSION['permission']['Proposal_Dashboad']['delete'] == '1') { ?>
																<td>
																	<div class="sentclsbuttonbot">
																		<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == 1) { ?>
																			<a href="javascript:;" data-toggle="modal" data-target="#subscription_edit_<?php echo $sub['id']; ?>" class="editicon"><i class="icofont icofont-edit" aria-hidden="true"></i></a>
																		<?php } ?>
																		<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
																			<a href="javascript:;" data-toggle="modal" data-target="#subscription_delete_<?php echo $sub['id']; ?>"><i class="icofont icofont-ui-delete deleteUser" aria-hidden="true"></i>`</a>
																		<?php } ?>
																	</div>
																</td>
															<?php } ?>
														</tr>
													<?php $i++;
													} ?>
												</tbody>
											</table>
											<!--  -->
										</div>
									</div>

								</div>
							</form>
							<!-- table -->
						</div>
						<div id="start_tab" class="tab-pane fade in <?php
																	if (isset($this->session->userdata['tab_name'])) {
																		if ($this->session->userdata['tab_name'] == 'start_tab') {
																			echo "active show";
																		}
																	} else {
																		echo "active show";
																	} ?>">

							<form class="form-horizontal" action="<?php echo base_url(); ?>proposal/delete_service" method="post" id="login_form">
								<div class="annual-accounts floating_set">
									<div class="annual-head floating_set">
										<div class="annual-lefts pull-left">
											<h2>Services</h2>
										</div>
										<div class="annual-rights pull-right">
											<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
												<!-- <a href="javascript:;" data-toggle="modal" data-target="#import-service">Import</a>  -->
											<?php } ?>
											<a href="javascript:;" onclick="exportexcel()">Export</a>
											<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
												<a href="#" data-toggle="modal" data-target="#add-new-services" class="tab-addnew">Add new</a>
											<?php } ?>
											<!-- <span id="seperater2slash"> / </span> -->
											<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
												<button type="button" data-toggle="modal" data-target="#delete_service" id="submit" name="save" class="btn btn-primary button-loading delete-service" style="display: none;">Delete</button>
											<?php } ?>
										</div>
									</div>

									<div id="delete_service" class="modal fade new-adds-categories" role="dialog">
										<div class="modal-dialog modal-dialog-proposal-del">

											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">×</button>
													<h4 class="modal-title">Delete Confirmation</h4>
												</div>
												<div class="modal-body">
													Do you want to delete?
												</div>

												<div class="modal-footer">
													<button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Yes</button>
													<a href="#" data-dismiss="modal">no</a>
												</div>
											</div>

										</div>
									</div>

									<div class="annual-table floating_set ">
										<!--<div class="annual-account-head floating_set">
                                  
                                  <-- <div class="annual-field2"> <button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Delete</button>  </div> -->
										<!-- <div class="annual-field3"></div>
                                  <div class="annual-field3"></div>
                                  <div class="annual-field3"></div>
                              </div> -->
										<div class="common-annual floating_set fee-schduepage">
											<table id="display_service2" class="display nowrap" style="width:100%">
												<thead>
													<tr>
														<th class="proposal_chk_TH Exc-colvis" width="10%">

															<!-- <div class="annual-field1">
                                    <div class="checkbox-color checkbox-primary">
                                        <input class="border-checkbox" type="checkbox" id="checkbox0">
                                        <label class="border-checkbox-label" for="checkbox0"></label>
                                    </div>
                                  </div> -->

															<label class="custom_checkbox1">
																<input class="border-checkbox" type="checkbox" id="checkbox0">
																<i></i>
															</label>


														</th>
														<th class="description_TH hasFilter" width="40%">Description <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
															<div class="sortMask"></div>
															<!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
														</th>
														<!-- <th style="display: none;">Description   <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> -->
														<th class="price_TH hasFilter" width="10%">Price <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
															<div class="sortMask"></div>
															<!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
														</th>
														<th class="qty_TH hasFilter" width="10%">Qty <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
															<div class="sortMask"></div>
															<!-- <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
														</th>
														<th class="unit_TH hasFilter" width="10%">Unit <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
															<div class="sortMask"></div>
															<!-- <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
														</th>
														<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == '1' || $_SESSION['permission']['Proposal_Dashboad']['delete'] == '1') { ?>
															<th class="action_TH Exc-colvis" width="20%"><strong>Action</strong></th>
														<?php } ?>
													</tr>
												</thead>

												<!--       <tfoot>
                              <tr class="table-header">
                              <th></th>
                              <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>   
                              <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th><th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                          
                              <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>                 
                              <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>               
                            
                              </tr>
                            </tfoot>   -->


												<tbody class="lead-annual">
													<?php
													$i = 1;
													foreach ($services as $service) { ?>
														<tr>
															<td class="delete-annuals1">
																<label class="custom_checkbox1">
																	<input class="border-checkbox checking" type="checkbox" id="checkbox<?php echo $i; ?>" name="checkbox[]" value="<?php echo $service['id']; ?>">
																	<!-- <label class="border-checkbox-label" for="checkbox<?php echo $i; ?>"></label> -->
																	<i></i>
																</label>
															</td>
															<td class="delete-annuals" data-search="<?php echo $service['service_name'] ?>">
																<h2><?php echo $service['service_name']; ?> </h2>
																<p><?php echo nl2br($service['service_description']); ?></p>
															</td>
															<!--  <td class="delete-annuals" style="display: none;">
                                    <?php echo $service['service_name'] . '-' . $service['service_description']; ?></td> -->
															<td class="delete-annuals3" data-search=" <?php echo $service['service_price']; ?>"><strong>
																	<?php echo $service['service_price']; ?></strong></td>
															<td data-search="<?php echo $service['service_qty']; ?>"><?php echo $service['service_qty']; ?></td>
															<td data-search="<?php $strlower = ucwords(preg_replace("~[\\\\/:*?'&,<>|]~", '', $service['service_unit']));
																				$lesg = str_replace('_', ' ', $strlower);
																				echo $lesg; ?>"><?php $strlower = ucwords(preg_replace("~[\\\\/:*?'&,<>|]~", '', $service['service_unit']));
																		$lesg = str_replace('_', ' ', $strlower);
																		echo $lesg; ?></td>
															<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == '1' || $_SESSION['permission']['Proposal_Dashboad']['delete'] == '1') { ?>
																<td>
																	<div class="sentclsbuttonbot">
																		<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == 1) { ?>
																			<a href="javascript:;" data-toggle="modal" data-target="#service_edit_<?php echo $service['id']; ?>" class="editicon"><i class="icofont icofont-edit" aria-hidden="true"></i></a>
																		<?php } ?>
																		<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) {
																			if ($service['service_category'] != 'service') { ?>
																				<a href="javascript:;" data-toggle="modal" data-target="#service_delete_<?php echo $service['id']; ?>"><i class="icofont icofont-ui-delete deleteUser" aria-hidden="true"></i></a>
																		<?php }
																		} ?>
																	</div>
																</td>
															<?php } ?>
														</tr>
													<?php $i++;
													} ?>
												</tbody>
											</table>
											<!--  -->
										</div>
									</div>

								</div>
							</form>
							<!-- table -->
						</div>


						<div id="product_tab" class="tab-pane fade <?php
																	if (isset($this->session->userdata['tab_name'])) {
																		if ($this->session->userdata['tab_name'] == 'product_tab') {
																			echo "active show";
																		}
																	} ?>">
							<form class="form-horizontal" action="<?php echo base_url(); ?>proposal/delete_products" method="post" id="login_form">
								<div class="annual-accounts floating_set">
									<div class="annual-head floating_set">
										<div class="annual-lefts pull-left">
											<h2>Products</h2>
										</div>
										<div class="annual-rights pull-right">
											<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
												<a href="javascript:;" data-toggle="modal" data-target="#import-product">Import</a>
											<?php } ?>
											<a href="javascript:;" onclick="export_excel()">Export</a>
											<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
												<a href="#" data-toggle="modal" data-target="#add_product" class="tab-addnew">Add new</a>
											<?php } ?>
											<!-- <span id="seperater2slash"> / </span> -->
											<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
												<button type="button" id="submit" data-toggle="modal" data-target="#delete_product" name="save" class="btn btn-primary button-loading delete-product" style="display: none;">Delete</button>
											<?php } ?>
										</div>
									</div>

									<div id="delete_product" class="modal fade new-adds-categories" role="dialog">
										<div class="modal-dialog modal-dialog-proposal-del">

											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">×</button>
													<h4 class="modal-title">Delete Confirmation</h4>
												</div>
												<div class="modal-body">
													Do you want to delete?
												</div>

												<div class="modal-footer">
													<button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Yes</button>
													<a href="#" data-dismiss="modal">no</a>
												</div>
											</div>

										</div>
									</div>

									<div class="annual-table floating_set ">
										<!--<div class="annual-account-head floating_set"-->

										<!-- <div class="annual-field2"><button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Delete</button> </div> -->
										<!-- <div class="annual-field3"></div>
                                  <div class="annual-field3"></div> -->
										<!-- <div class="annual-field3"><a href="#" data-toggle="modal" data-target="#add_product">Add new</a></div> 
                              </div> -->
										<div class="common-annual floating_set fee-schduepage">
											<table id="display_service1" class="display nowrap" style="width:100%">
												<thead>
													<tr>
														<th class="delete-annuals1 proposal_chk_TH Exc-colvis" width="10%">
															<!-- <div class="checkbox-color checkbox-primary"> -->
															<label class="custom_checkbox1">
																<input class="border-checkbox" type="checkbox" id="checkbox_4"> <i></i></label>
															<!-- <label class="border-checkbox-label" for="checkbox_4"></label>
                                    </div> -->
														</th>
														<th class="description_TH hasFilter" width="60%">Description <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
															<div class="sortMask"></div>
															<!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
														</th>
														<!--  <th style="display: none;">Description   <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> -->
														<th class="price_TH hasFilter" width="10%">Price <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
															<div class="sortMask"></div>
															<!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select>-->
														</th>
														<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == '1' || $_SESSION['permission']['Proposal_Dashboad']['delete'] == '1') { ?>
															<th class="action_TH Exc-colvis" width="20%">Action</th>
														<?php } ?>
													</tr>
												</thead>

												<!--      <tfoot>
                              <tr class="table-header">
                              <th></th>
                              <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>           
                              <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th><th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>                 
                            
                              </tr>
                            </tfoot> -->

												<tbody class="lead-annual">
													<?php
													$i = 1;
													foreach ($products as $product) { ?>
														<tr>
															<td class="delete-annuals1">
																<!-- <div class="checkbox-color checkbox-primary"> -->
																<label class="custom_checkbox1"> <input class="border-checkbox products" type="checkbox" id="checkbox_<?php echo $i; ?>" name="checkbox[]" value="<?php echo $product['id']; ?>"> <i></i></label>
																<!--    <label class="border-checkbox-label" for="checkbox_<?php echo $i; ?>"></label>
                                        </div> -->
															</td>
															<td class="delete-annuals" data-search="<?php echo $product['product_name']; ?>">
																<h2><?php echo $product['product_name']; ?> </h2>
																<p><?php echo $product['product_description']; ?></p>
															</td>

															<!--  <td class="delete-annuals" style="display: none;">
                                        <?php echo $product['product_name'] . '-' . $product['product_description']; ?></p>
                                    </td> -->
															<td class="delete-annuals3" data-search="<?php echo $product['product_price']; ?>"><strong><?php echo $product['product_price']; ?></strong></td>
															<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == '1' || $_SESSION['permission']['Proposal_Dashboad']['delete'] == '1') { ?>
																<td>
																	<div class="sentclsbuttonbot">
																		<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == 1) { ?>
																			<a href="javascript:;" data-toggle="modal" data-target="#products_edit_<?php echo $product['id']; ?>" class="editicon"><i class="icofont icofont-edit" aria-hidden="true"></i></a>
																		<?php } ?>
																		<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
																			<a href="javascript:;" data-toggle="modal" data-target="#products_delete_<?php echo $product['id']; ?>"><i class="icofont icofont-ui-delete deleteUser" aria-hidden="true"></i></a>
																		<?php } ?>
																	</div>
																</td>
															<?php } ?>
														</tr>

													<?php $i++;
													} ?>
												</tbody>
											</table>
											<!--  -->
										</div>
									</div>
								</div>
							</form>
							<!-- table -->
						</div> <!-- 2tab -->
					</div>
				</div>
			</div>
			<!-- tabcontent -->
		</div>
	</div>
</div>

<!-- right -->


<div class="modal fade" id="Tax" role="dialog">
	<div class="modal-dialog modal-add-tax">

		<!-- Modal content-->
		<div class="modal-content">
			<form class="form-horizontal tax" action="<?php echo base_url(); ?>proposal_page/add_taxes" method="post" id="login_form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add New Tax</h4>
				</div>
				<div class="modal-body">
					<div class="error_msgs"></div>
					<div class="common-annual lead-annual remove-annual-space1">
						<div class="annual-field1"> </div>
						<div class="annual-field2 add-input-service">
							<label>Tax name,VAT or Sales Tax*</label>
							<input type="text" name="tax_name" data-validation="required">
							<span id="name_er" style="color:red;"></span>
						</div>
						<div class="annual-field3 add-input-service">
							<label>Tax Rate %*</label>
							<input type="text" name="tax_rate" min="1" max="100" maxlength="3" step="1" class="decimal integer" data-validation="required">
						</div>

						<div class="annual-field3"></div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="annual-field3">
						<!-- <button type="submit" name="submit" class="btn btn-success"> <i class="fa fa-plus fa-6" aria-hidden="true"></i> Add</button> -->
						<button type="submit" name="submit" class="btn btn-success">Save</button>
					</div>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
		</div>
		</form>
	</div>
</div>

<!-- modal -->
<?php include('popup_sections.php'); ?>

<?php
$i = 1;
foreach ($taxes as $tax) { ?>
	<div id="tax_edit_<?php echo $tax['id']; ?>" class="modal fade new-adds-company" role="dialog">
		<div class="modal-dialog modal-edit-tax">
			<!-- Modal content-->
			<form class="form-horizontal tax" action="<?php echo base_url(); ?>proposal_page/edit_tax" method="post" id="login_form">
				<input type="hidden" name="id" value="<?php echo $tax['id']; ?>">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Edit Tax</h4>
					</div>
					<div class="modal-body">
						<div class="error_msgs"></div>
						<div class="common-annual lead-annual remove-annual-space1">
							<div class="annual-field1"> </div>
							<div class="annual-field2 add-input-service">
								<label>Tax name,VAT or Sales Tax*</label>
								<input type="text" name="tax_name" data-validation="required" value="<?php if (isset($tax['tax_name'])) {
																											echo $tax['tax_name'];
																										} ?>">
								<span id="name_er" style="color:red;"></span>
							</div>
							<div class="annual-field3 add-input-service">
								<label>Tax Rate %*</label>
								<input type="text" name="tax_rate" min="1" max="100" maxlength="3" step="1" class="decimal" data-validation="required" value="<?php if (isset($tax['tax_rate'])) {
																																									echo $tax['tax_rate'];
																																								} ?>">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Save</button>
						<a href="#" data-dismiss="modal">CLose</a>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php } ?>

<div class="edit_delete_modals"></div>

<!-- modal -->
</div>
<!-- common-reduce -->
<?php $this->load->view('includes/session_timeout'); ?>
<?php $this->load->view('includes/footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mock.js"></script>
<script type="text/javascript">
	$('#service_scode').change(function() {
		if ($(this).val() == 'other') {
			$('.other_service').show();
		} else {
			$('.other_service').hide();
		}
	});

	var display_service1;

	var display_service2;

	var display_service3;

	var display_service4;

	var display_service5;

	$(document).on('click', '.Settings_Button', AddClass_SettingPopup);
	$(document).on('click', '.Button_List', AddClass_SettingPopup);

	$(document).on('click', '.dt-button.close', function() {
		$('.dt-buttons').find('.dt-button-collection').detach();
		$('.dt-buttons').find('.dt-button-background').detach();
	});

	function AddClass_SettingPopup() {
		$('button.Button_List').closest('.dt-button-collection').addClass('Settings_List');
		$('button.buttons-columnVisibility').closest('.dt-button-collection').addClass('columnVisibility_List');
	}
	$('.dropdown-sin-4').dropdown({
		limitCount: 5,
		input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
	});

	$('.dropdown-sin-6').dropdown({
		limitCount: 5,
		input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
	});

	$('.dropdown-sin-2').dropdown({
		limitCount: 5,
		input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
	});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.77/jquery.form-validator.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
	$.validate({
		lang: 'en'
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {

		$("#add-new-category_service, div#add-new-category_subscription, div#add-new-category_product").on("shown.bs.modal", function() {

			// alert('hi'); 

			$('div#pcoded + div + div + .modal-backdrop.fade.show').css('z-index', '9999');

		});

		$("#add-new-category_service, div#add-new-category_subscription, div#add-new-category_product").on("hidden.bs.modal", function() {

			// alert('hi'); 

			$('div#pcoded + div + div + .modal-backdrop.fade.show').css('z-index', '1040');

		})

		<?php if (isset($_SESSION['delete_error'])) { ?>

			$('.delete-error').find('.position-alert1').html('<p><?php echo $_SESSION['delete_error']; ?></p><p>Do you want to continue?</p><p class="pull-right" style="margin-bottom: -20px !important;"><a href="javascript:void(0);" class="session_category_delete_no delete_error">No</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary button-loading session_category_delete delete_error">Yes</button></p>');
			$('.delete-error').show();

			$('.session_category_delete').click(function() {
				$('#delete-category_service-<?php echo $_SESSION['deleting_categoryid']; ?> .modal-body').append('<input type="hidden" name="session_category_delete" value="1">');
				$('#delete-category_service-<?php echo $_SESSION['deleting_categoryid']; ?> #submit').trigger('click');
			});

			$('.session_category_delete_no').click(function() {
				$(".delete-error").hide();
			});

		<?php

			unset($_SESSION['delete_error']);
			unset($_SESSION['deleting_categoryid']);
		} ?>
	});

	$(function() {
		$("form[name='form']").validate({
			// Specify validation rules
			rules: {
				category_name: "required",
			},
			// Specify validation error messages
			messages: {
				category_name: "Please enter your firstname",
			},
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
	$("#category_save").click(function() {

		if ($("#category_name").val() != '') {
			var category_name = $("#category_name").val().trim();
			var category_type = $("#category_type").val();

			$.ajax({
				url: '<?php echo base_url(); ?>proposal/category_check',
				type: 'POST',
				data: {
					'category_name': category_name,
					'category_type': category_type
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {

					$(".LoadingImage").hide();
					var json = JSON.parse(data);
					status = json['status'];

					if (status == 0) {

						$("input,textarea").keyup(function() {
							$("input,textarea,select").css('border', '');
						});

						if ($("#category_name").val() != '') {

							$.ajax({
								url: '<?php echo base_url(); ?>proposal/add_category',
								type: 'POST',
								data: {
									'category_name': category_name,
									'category_type': category_type
								},
								beforeSend: function() {
									$(".LoadingImage").show();
								},
								success: function(data) {
									$(".LoadingImage").hide();
									var json = JSON.parse(data);
									category_list = json['category_list'];
									category_add = json['category_add'];
									category_pop = json['category_pop'];
									// $("#category_success").show();
									setTimeout(function() {
										$("#add-new-category_service .modal-header").find('.close').trigger('click');
									}, 2000);
									$(".category_list").html('');
									$(".category_list").append(category_list);
									$(".category_add").html('');
									$(".category_add").append(category_add);
									$(".edit_delete_modals").append(category_pop);


									$(".categrory-add").show();
									/*setTimeout(function() {
									$(".categrory-add").hide();
									}, 2000); */
								}
							});
						} else {
							$("#category_name").css('border', '1px solid red');
						}
					} else {
						$(".category-exists").show();
						setTimeout(function() {
							$(".category-exists").hide();
						}, 2000);
					}
				}

			});

		} else {
			$(".category-error").show();
			setTimeout(function() {
				$(".category-error").hide();

			}, 2000);
		}
	});

	$("#product_category_save").click(function() {
		if ($("#product_category_name").val() != '') {
			var category_name = $("#product_category_name").val().trim();
			var category_type = $("#product_category_type").val();

			$.ajax({
				url: '<?php echo base_url(); ?>proposal/category_check',
				type: 'POST',
				data: {
					'category_name': category_name,
					'category_type': category_type
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					//alert(data); 
					$(".LoadingImage").hide();
					var json = JSON.parse(data);
					status = json['status'];
					if (status == '0') {

						$("input,textarea").keyup(function() {
							$("input,textarea,select").css('border', '');
						});

						if ($("#product_category_name").val() != '') {

							$.ajax({
								url: '<?php echo base_url(); ?>proposal/add_category',
								type: 'POST',
								data: {
									'category_name': category_name,
									'category_type': category_type
								},
								beforeSend: function() {
									$(".LoadingImage").show();
								},
								success: function(data) {
									$(".LoadingImage").hide();
									var json = JSON.parse(data);
									category_list = json['category_list'];
									category_add = json['category_add'];
									category_pop = json['category_pop'];
									//   $("#categoryproduct_success").show();
									setTimeout(function() {
										$("#add-new-category_product .modal-header").find('.close').trigger('click');
									}, 2000);
									$(".product_category_list").html('');
									$(".product_category_list").append(category_list);

									$(".product_category_add").html('');
									$(".product_category_add").append(category_add);
									$(".edit_delete_modals").append(category_pop);


									$(".categrory-add").show();
									setTimeout(function() {
										$(".categrory-add").hide();
									}, 2000);
								}
							});

						} else {
							$("#product_category_name").css('border', '1px solid red');
						}
					} else {
						$(".category-exists").show();
						setTimeout(function() {
							$(".category-exists").hide();
						}, 2000);
					}
				}

			});
		} else {
			$(".category-error").show();
			setTimeout(function() {
				$(".category-error").hide();

			}, 2000);
		}
	});


	$("#subscription_category_save").click(function() {
		$('#category_check12').hide();
		$('#subscription_category_error').hide();

		if ($("#subscription_category_name").val() != '') {
			var category_name = $("#subscription_category_name").val().trim();
			var category_type = $("#subscription_category_type").val();

			$.ajax({
				url: '<?php echo base_url(); ?>proposal/category_check',
				type: 'POST',
				data: {
					'category_name': category_name,
					'category_type': category_type
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},

				success: function(data) {
					$(".LoadingImage").hide();
					var json = JSON.parse(data);
					status = json['status'];

					if (status == '0') {
						if ($("#subscription_category_name").val() != '') {
							$("input,textarea").keyup(function() {
								$("input,textarea,select").css('border', '');
							});

							$.ajax({
								url: '<?php echo base_url(); ?>proposal/add_category',
								type: 'POST',
								data: {
									'category_name': category_name,
									'category_type': category_type
								},
								beforeSend: function() {
									$(".LoadingImage").show();
								},
								success: function(data) {
									$(".LoadingImage").hide();
									var json = JSON.parse(data);
									category_list = json['category_list'];
									category_add = json['category_add'];
									category_pop = json['category_pop'];

									setTimeout(function() {
										$("#add-new-category_subscription .modal-header").find('.close').trigger('click');
									}, 2000);
									$(".subscription_category_list").html('');
									$(".subscription_category_list").append(category_list);
									$(".subscription_category_add").html('');
									$(".subscription_category_add").append(category_add);
									$(".edit_delete_modals").append(category_pop);

									$(".categrory-add").show();
									setTimeout(function() {
										$(".categrory-add").hide();
									}, 2000);
								}
							});
						} else {
							$("#subscription_category_name").css('border', '1px solid red');
						}
					} else {
						$(".category-exists").show();

						if ($(".category-exists").is(':visible') == false) {
							$('#category_check12').show();
						}

						setTimeout(function() {
							$(".category-exists").hide();
						}, 2000);
					}
				}
			});
		} else {
			$(".category-error").show();

			if ($('.category-error').is(':visible') == false) {
				$('#subscription_category_error').show();
			}

			setTimeout(function() {
				$(".category-error").hide();
			}, 2000);
		}

	});


	$(document).on('submit', '#addpricelist', function(event) {
		if ($('select[name="service_list[]"]').val() == '') {
			event.preventDefault();
			$('#price_list_service_er').html('Required.').css('color', 'red');
		} else {
			$('#price_list_service_er').html('');
			var obj = $(this).find('input[name="pricelist_name"]');
			var url = '<?php echo base_url() . 'Proposal_pdf/check_price_list_name'; ?>';
			check_name(obj, '', event, url);
		}
	});

	$(document).on("change", "#checkbox0", function() {

		var checked = this.checked;
		display_service2.column(0).nodes().to$().each(function(index) {
			if (checked) {
				$(this).find('.checking').prop('checked', 'checked');
				$(".delete-service").css('display', 'inline-block');
				// proposal.push($(this).find('.checking').data('proposal-id'));
			} else {
				$(this).find('.checking').prop('checked', false);
				$(".delete-service").css('display', 'none');
				//  $(".delete_proposal_records").hide();
				// $("#archive_proposal_records").hide();
			}
		});
		// $('.checking').not(this).prop('checked', this.checked);
	});

	$(document).on('click', '.checking', function() {
		if ($(this).is(':checked', true)) {
			$(".delete-service").css('display', 'inline-block');
		} else {
			$("#checkbox0").attr('checked', false);
			if ($("input.checking:checked").length == 0) {
				$(".delete-service").css('display', 'none');
			}
		}
	});


	$(document).on("change", "#checkbox_4", function() {

		var checked = this.checked;
		display_service1.column(0).nodes().to$().each(function(index) {
			if (checked) {
				$(this).find('.products').prop('checked', 'checked');
				$(".delete-product").css('display', 'inline-block');
				// proposal.push($(this).find('.checking').data('proposal-id'));
			} else {
				$(this).find('.products').prop('checked', false);
				$(".delete-product").css('display', 'none');
				//  $(".delete_proposal_records").hide();
				// $("#archive_proposal_records").hide();
			}
		});
		// $('.products').not(this).prop('checked', this.checked);
	});


	$(document).on('click', '.products', function() {
		if ($(this).is(':checked', true)) {
			$(".delete-product").css('display', 'inline-block');
		} else {
			$("#checkbox_4").attr('checked', false);
			if ($("input.products:checked").length == 0) {
				$(".delete-product").css('display', 'none');
			}
		}
	});


	$(document).on("change", "#checkboxes", function() {

		var checked = this.checked;
		display_service3.column(0).nodes().to$().each(function(index) {
			if (checked) {
				$(this).find('.taxes').prop('checked', 'checked');
				$(".delete-tax").css('display', 'inline-block');
				// proposal.push($(this).find('.checking').data('proposal-id'));
			} else {
				$(this).find('.taxes').prop('checked', false);
				$(".delete-tax").css('display', 'none');
				//  $(".delete_proposal_records").hide();
				// $("#archive_proposal_records").hide();
			}
		});
		//  $('.taxes').not(this).prop('checked', this.checked);
	});


	$(document).on('click', '.taxes', function() {
		if ($(this).is(':checked', true)) {
			$(".delete-tax").css('display', 'inline-block');
		} else {
			$("#checkboxes").attr('checked', false);
			if ($("input.taxes:checked").length == 0) {
				$(".delete-tax").css('display', 'none');
			}
		}
	});


	$(document).on("change", "#checkbox_00", function() {

		var checked = this.checked;
		display_service4.column(0).nodes().to$().each(function(index) {
			if (checked) {
				$(this).find('.check_sub').prop('checked', 'checked');
				$(".delete-subscription").css('display', 'inline-block');
				// proposal.push($(this).find('.checking').data('proposal-id'));
			} else {
				$(this).find('.check_sub').prop('checked', false);
				$(".delete-subscription").css('display', 'none');
				//  $(".delete_proposal_records").hide();
				// $("#archive_proposal_records").hide();
			}
		});


		// $('.check_sub').not(this).prop('checked', this.checked);
	});

	$(document).on('click', '.check_sub', function() {
		if ($(this).is(':checked', true)) {
			$(".delete-subscription").css('display', 'inline-block');
		} else {
			$("#checkbox_00").attr('checked', false);
			if ($("input.check_sub:checked").length == 0) {
				$(".delete-subscription").css('display', 'none');
			}
		}

	});





	$(document).on("change", "#check-box", function() {

		var checked = this.checked;
		display_service5.column(0).nodes().to$().each(function(index) {
			if (checked) {
				$(this).find('.price_list').prop('checked', 'checked');
				$(".delete-pricelist").css('display', 'inline-block');
				// proposal.push($(this).find('.checking').data('proposal-id'));
			} else {
				$(this).find('.price_list').prop('checked', false);
				$(".delete-pricelist").css('display', 'none');
				//  $(".delete_proposal_records").hide();
				// $("#archive_proposal_records").hide();
			}
		});


		// $('.price_list').not(this).prop('checked', this.checked);
	});



	$(document).on('click', '.price_list', function() {
		if ($(this).is(':checked', true)) {
			$(".delete-pricelist").css('display', 'inline-block');
		} else {
			$("#check-box").attr('checked', false);
			if ($("input.price_list:checked").length == 0) {
				$(".delete-pricelist").css('display', 'none');
			}
		}

	});




	function exportexcel() {
		var url = "<?php echo base_url(); ?>/proposal/Export/";
		window.location = url;
	}

	function export_excel() {
		var url = "<?php echo base_url(); ?>/proposal/Products_Export/";
		window.location = url;
	}

	function exportsub_excel() {
		var url = "<?php echo base_url(); ?>/proposal_page/Subscription_Export/";
		window.location = url;
	}

	function export_price_list() {
		var url = "<?php echo base_url(); ?>/proposal_page/Price_list_Export/";
		window.location = url;
	}

	function export_tax_list() {
		var url = "<?php echo base_url(); ?>/proposal_page/Tax_Export/";
		window.location = url;
	}

	$(' .tab-leftside .nav-tabs > li a').not('.sub-categories1 a').click(function() {
		var $this = $(this);

		var child = $this.closest('a').next('.sub-categories1'),
			rest = $('.sub-categories1:visible').not(child);

		$this.closest('a').next('.sub-categories1').slideToggle(300);
		rest.slideUp();
	});

	$(' .tab-leftside .nav-tabs li a').click(function() {
		// $('li a').removeClass("active");
		// $(this).addClass("active");
	});
	$(document).on('submit', '#login_form,#importservice-excel', function(event) {
		if ($(this).hasClass('edit_pricelist')) {
			var obj = $(this).find('input[name="pricelist_name"]');
			var id = $(this).find('input[name="id"]').val();
			var url = '<?php echo base_url() . 'Proposal_pdf/check_price_list_name'; ?>';
			$(this).find('.error_msgs').html('');
			var err = 0;
			var object = $(this);

			$(this).find('input[name="service_price[]"]').each(function() {
				var service_price = $(this).val();

				if (service_price != "" && Number(service_price) <= '0' && !$(object).find('.error_msgs .ser_err').hasClass('ser_err')) {
					$(object).find('.error_msgs').append('<p style="color:red !important;" class="ser_err">Invalid Service Price.</p>');
					err++;
				}
			});

			$(this).find('input[name="product_price[]"]').each(function() {
				var product_price = $(this).val();

				if (product_price != "" && Number(product_price) <= '0' && !$(object).find('.error_msgs .prod_err').hasClass('prod_err')) {
					$(object).find('.error_msgs').append('<p style="color:red !important;" class="prod_err">Invalid Product Price.</p>');
					err++;
				}
			});

			$(this).find('input[name="subscription_price[]"]').each(function() {
				var subscription_price = $(this).val();

				if (subscription_price != "" && Number(subscription_price) <= '0' && !$(object).find('.error_msgs .sub_err').hasClass('sub_err')) {
					$(object).find('.error_msgs').append('<p style="color:red !important;" class="sub_err">Invalid Subscription Price.</p>');
					err++;
				}
			});

			if (err > '0') {
				event.preventDefault();
			} else {
				check_name(obj, id, event, url);
			}
		} else if ($(this).hasClass('tax')) {
			var obj = $(this).find('input[name="tax_name"]');
			var id = $(this).find('input[name="id"]').val();
			var url = '<?php echo base_url() . 'Proposal_pdf/check_tax_name'; ?>';
			var tax_rate = $(this).find('input[name="tax_rate"]').val();
			$(this).find('.error_msgs').html('');
			var err = 0;

			if (tax_rate != "" && Number(tax_rate) <= '0') {
				$(this).find('.error_msgs').append('<p style="color:red !important;">Invalid Tax Rate.</p>');
				err++;
			}

			if (err > '0') {
				event.preventDefault();
			} else {
				check_name(obj, id, event, url);
			}
		} else {
			$('.LoadingImage').show();
		}
	});

	$('.decimal').keyup(function() {
		var val = $(this).val();
		if (isNaN(val)) {
			val = val.replace(/[^0-9\.]/g, '');
			if (val.split('.').length > 2)
				val = val.replace(/\.+$/, "");
		}
		$(this).val(val);
	});
</script>


<script>
	$(document).ready(function() {

		//set button id on click to hide first modal
		$("#hide_category1").on("click", function() {
			$('#service_edit_1').modal('hide');
		});
		//trigger next modal
		$("#hide_category1").on("click", function() {
			$('#add-new-category').modal('show');
		});

		$("#hide_category2").on("click", function() {
			$('#service_edit_1').modal('show');
		});

	});

	$(document).ready(function() {
		$('input:checkbox[name=active_status]').change(function() {
			var id = this.value;
			var status = '';

			status = ($(this).prop('checked')) ? 1 : 0;

			$.ajax({
				url: '<?php echo base_url(); ?>proposal_page/update_active_status',
				type: 'POST',
				data: {
					'id': id,
					'status': status
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
				}
			});

		});
	});
	$(document).ready(function() {
		$('input:radio[name=taxStatus]').change(function() {
			var id = this.value;
			//alert(id);
			$.ajax({
				url: '<?php echo base_url(); ?>proposal_page/update_tax_status',
				type: 'POST',
				data: {
					'id': id
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					var json = JSON.parse(data);
					status = json['status'];
					if (status == '0') {
						$(".alert-warning").show();
						setTimeout(function() {
							$(".alert-warning").hide();
							location.reload();
						}, 2000);
					}
				}
			});

		});
	});

	var validatePrice = function(price) {
		return /^(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(price);
	}

	$(document).on('keypress', '.decimal_qty, .integer', function(event) {
		return /\d/.test(String.fromCharCode(event.keyCode));
	});

	$.validator.addMethod('minStrict', function(value, el, param) {
		return value > param;
	});

	$('[name="form1"]').validate({
		rules: {
			service_name: {
				required: true,
			},
			other_service_name: {
				required: function(element) {
					return ($("select[name='service_name']").val() == 'other');
				}
			},
			service_price: {
				required: true,
				number: true,
				minStrict: 0,
			},
			service_qty: {
				required: true,
				digits: true,
				minStrict: 0,
			},
			service_description: {
				required: true,
			},
			service_category: {
				required: true,
			}
		},
		highlight: function(element) {
			$(element).addClass("error-border");
		},
		unhighlight: function(element) {
			$(element).removeClass("error-border");
		},
		errorPlacement: function(error, element) {
			return true;
		}
	});

	$('.editicon').click(function() {
		$($(this).data('target') + ' form').trigger('reset');
		$($(this).data('target') + ' .error_msgs').html('');
	});

	$('.tab-addnew').click(function() {
		if ($(this).data('target') == '#add-new-services') {
			$('#add_services').trigger('reset');
			$('#add-new-services .error_msgs').html('');
		} else if ($(this).data('target') == '#add_product') {
			$('#product_add').trigger('reset');
			$('#add_product .error_msgs').html('');
		} else if ($(this).data('target') == '#add-new-subscriptions') {
			$('#subscription_add').trigger('reset');
			$('#subscription_add .error_msgs').html('');
		} else if ($(this).data('target') == '#Tax') {
			$('#Tax #login_form').trigger('reset');
			$('#Tax .error_msgs').html('');
		} else if ($(this).data('target') == '#add_pricelist') {
			$('#addpricelist').trigger('reset');
			$('#addpricelist .error_msgs').html('');
		}
	});


	$("#save_service").click(function() {
		var service_name = $("select[name=service_name]").val();

		if (service_name == 'other') {
			service_name = $("input[name=other_service_name]").val();
		}

		var service_category = $("#service_category").val();

		var service_category = $("#service_category").val();

		if (service_name != "" && service_category != "") {
			$.ajax({
				url: '<?php echo base_url(); ?>proposal/service_check',
				type: 'POST',
				data: {
					'service_name': service_name,
					'service_category': service_category
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					var json = JSON.parse(data);
					status = json['status'];
					$('#add-new-services .error_msgs').html('');

					if (status == 0) {
						var price = $("input[name='service_price']").val();
						var quant = $("input[name='service_qty']").val();

						var num = parseFloat(quant);
						var cleanNum = num.toFixed(2);
						var err = 0;

						if (price != "" && Number(price) <= Number(0)) {
							$("input[name='service_price']").css('border', '1px solid red');

							if (!$('#add-new-services .error_msgs .price_er').hasClass('price_er')) {
								$('#add-new-services .error_msgs').append('<p class="price_er" style="color:red !important;">Invalid Price.</p>');
							}

							err++;
						}
						if (quant != "" && (Number(quant) <= Number(0) || parseFloat(Number(quant) % Number(1)) != Number(0))) {
							$("input[name='service_qty']").css('border', '1px solid red');

							if (!$('#add-new-services .error_msgs .quantity_er').hasClass('quantity_er')) {
								$('#add-new-services .error_msgs').append('<p class="quantity_er" style="color:red !important;">Invalid Quantity.</p>');
							}

							err++;
						}

						if ($("input[name='service_name']").val() != '' && $("input[name='service_price']").val() != '' && $("input[name='service_qty']").val() != '' && $("textarea[name='service_description']").val() != '' && $("#service_category").val() != '' && err == 0) {
							$("#add_services").submit();
						} else {
							showErrors();
						}
					} else {
						$(".service-exists").show();

						if ($(".service-exists").is(':visible') == false) {
							if (!$('#add-new-services .error_msgs .exist_er').hasClass('exist_er')) {
								$('#add-new-services .error_msgs').append('<p class="exist_er" style="color:red !important;">Service Name Already Exists.</p>');
							}
						}
						setTimeout(function() {
							$(".service-exists").hide();
						}, 2000);
					}
				}
			});
		} else {
			showErrors();
		}
	});

	function showErrors() {
		var price = $("input[name='service_price']").val();
		var quant = $("input[name='service_qty']").val();

		var num = parseFloat(quant);
		var cleanNum = num.toFixed(2);

		if ($("select[name='service_name']").val() == '') {
			$("select[name='service_name']").css('border', '1px solid red');
		} else if ($("select[name='service_name']").val() == 'other') {
			if ($("input[name=other_service_name]").val() == "") {
				$("input[name=other_service_name]").css('border', '1px solid red');
			}
		}

		if ($("input[name='service_price']").val() == '' || !validatePrice(price)) {
			$("input[name='service_price']").css('border', '1px solid red');
		}

		if ($("input[name='service_qty']").val() == '' || (quant % 1 != 0)) {
			$("input[name='service_qty']").css('border', '1px solid red');
		}

		if ($("textarea[name='service_description']").val() == '') {
			$("textarea[name='service_description']").css('border', '1px solid red');
		}

		if ($("#service_category").val() == '') {
			$("#service_category").css('border', '1px solid red');
		}

		$("input,textarea").keyup(function() {
			$("input,textarea,select").css('border', '');
		});

		$("select[name='service_category']").change(function() {
			$("select[name='service_category']").removeAttr('style');
		});
	}


	$("#save_product").click(function() {
		var service_name = $("input[name=product_name]").val();
		var service_category = $("#product_category").val();


		$.ajax({
			url: '<?php echo base_url(); ?>proposal/product_check',
			type: 'POST',
			data: {
				'product_name': service_name,
				'product_category': service_category
			},
			beforeSend: function() {
				$(".LoadingImage").show();
			},

			success: function(data) {
				//alert(data); 
				$(".LoadingImage").hide();
				var json = JSON.parse(data);
				status = json['status'];

				if (status == 0) {
					$('#add_product .error_msgs').html('');
					var err = 0;

					if ($("input[name='product_name']").val() == '') {
						$("input[name='product_name']").css('border', '1px solid red');
					}

					if ($("input[name='product_price']").val() == '') {
						$("input[name='product_price']").css('border', '1px solid red');
					} else if (Number($("input[name='product_price']").val()) <= Number(0)) {
						$("input[name='product_price']").css('border', '1px solid red');

						if (!$('#add_product .error_msgs .price_er').hasClass('price_er')) {
							$('#add_product .error_msgs').append('<p class="price_er" style="color:red !important;">Invalid Price.</p>');
						}

						err++;
					}

					if ($("textarea[name='product_description']").val() == '') {
						$("textarea[name='product_description']").css('border', '1px solid red');
					}

					if ($("#product_category").val() == '') {
						$("#product_category").css('border', '1px solid red');
					}

					$("input,textarea").keyup(function() {
						$("input,textarea,select").removeAttr('style');
					});

					$("select").change(function() {
						$("select").removeAttr('style');
					});

					if ($("input[name='product_name']").val() != '' && $("input[name='product_price']").val() != '' && $("textarea[name='product_description']").val() != '' && $("#product_category").val() != '' && err == 0) {
						$("#product_add").submit();
					}

				} else {
					$(".product-exists").show();

					if ($(".product-exists").is(':visible') == false) {
						if (!$('#add_product .error_msgs .exist_er').hasClass('exist_er')) {
							$('#add_product .error_msgs').append('<p class="exist_er" style="color:red !important;">Product Name Already Exists.</p>');
						}
					}
					setTimeout(function() {
						$(".product-exists").hide();
					}, 2000);
				}
			}
		});
	});

	$('#subscription_add').validate({
		rules: {
			subscription_name: {
				required: true,
			},
			subscription_price: {
				required: true,
				number: true,
				minStrict: 0,
			},
			subscription_unit: {
				required: true,
			},
			subscription_description: {
				required: true,
			},
			subscription_category: {
				required: true,
			},
		},
		highlight: function(element) {
			$(element).addClass("error-border");
		},
		unhighlight: function(element) {
			$(element).removeClass("error-border");
		},
		errorPlacement: function(error, element) {
			return true;
		}
	});

	$("#save_subscriptions").click(function() {
		$('#subscription_add').valid();

		var service_name = $("input[name=subscription_name]").val();
		var service_category = $("#subscription_category").val();

		$.ajax({
			url: '<?php echo base_url(); ?>proposal/subscription_check',
			type: 'POST',
			data: {
				'subscription_name': service_name,
				'subscription_category': service_category
			},
			beforeSend: function() {
				$(".LoadingImage").show();
			},

			success: function(data) {
				$(".LoadingImage").hide();
				var json = JSON.parse(data);
				status = json['status'];
				var err = 0;
				$('#add-new-subscriptions .error_msgs').html('');

				if (status == 0) {
					if ($('input[name="subscription_price"]').val() != "" && Number($('input[name="subscription_price"]').val()) <= Number(0)) {
						if (!$('#add-new-subscriptions .error_msgs .price_er').hasClass('price_er')) {
							$('#add-new-subscriptions .error_msgs').append('<p class="price_er" style="color:red !important;">Invalid Price.</p>');
						}

						err++;
					}

					if (err == 0) {
						$("#subscription_add").submit();
					}
				} else {
					$(".subscription-exists").show();

					if ($(".subscription-exists").is(':visible') == false) {
						if (!$('#add-new-subscriptions .error_msgs .exist_er').hasClass('exist_er')) {
							$('#add-new-subscriptions .error_msgs').append('<p class="exist_er" style="color:red !important;">Subscription Name Already Exists.</p>');
						}
					}

					setTimeout(function() {
						$(".subscription-exists").hide();
					}, 2000);
				}
			}

		});

	});




	$(document).on('click', ".edit_category", function() {
		var id = $(this).attr('id');
		var category_name = $(this).parents('.edit_categories').find("#edit_category_name").val().trim();

		if (category_name != '') {
			var category_type = 'service';
			$(this).parents('.edit_categories').find("#category_error").hide();
			$.ajax({
				url: '<?php echo base_url(); ?>proposal/edit_category_check',
				type: 'POST',
				data: {
					'category_name': category_name,
					'category_type': category_type,
					'id': id
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					// alert(data); 
					$(".LoadingImage").hide();
					var json = JSON.parse(data);
					status = json['status'];
					if (status == 0) {
						$("#category_edit" + id).submit();

					} else {
						$(".category-exists").show();
						setTimeout(function() {
							$(".category-exists").hide();
						}, 2000);
					}
				}

			});
		} else {
			$(this).parents('.edit_categories').find("#category_error").show();
		}


	});


	$(document).on('click', ".edit_product_category", function() {
		//  alert('ok');
		var id = $(this).attr('id');
		var category_name = $(this).parents('.edit_productcategories').find("#edit_product_categoryname").val().trim();

		if (category_name != '') {
			var category_type = 'product';
			$(this).parents('.edit_productcategories').find("#category_error").hide();
			$.ajax({
				url: '<?php echo base_url(); ?>proposal/edit_category_check',
				type: 'POST',
				data: {
					'category_name': category_name,
					'category_type': category_type,
					'id': id
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					//alert(data); 
					$(".LoadingImage").hide();
					var json = JSON.parse(data);
					status = json['status'];
					if (status == 0) {
						$("#edit_product_cate" + id).submit();
					} else {
						$(".category-exists").show();
						setTimeout(function() {
							$(".category-exists").hide();
						}, 2000);
					}
				}
			});
		} else {
			$(this).parents('.edit_productcategories').find("#category_error").show();
		}
	});

	$(document).on('click', ".edit_subscription_category", function() {
		var id = $(this).attr('id');
		var category_name = $(this).parents('.edit_subscriptioncategories').find("#edit_subscription_category_name").val().trim();
		var category_type = 'subscription';

		if (category_name != '') {
			$(this).parents('.edit_subscriptioncategories').find("#category_error").hide();
			$.ajax({
				url: '<?php echo base_url(); ?>proposal/edit_category_check',
				type: 'POST',
				data: {
					'category_name': category_name,
					'category_type': category_type,
					'id': id
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					// alert(data); 
					$(".LoadingImage").hide();
					var json = JSON.parse(data);
					status = json['status'];
					if (status == 0) {
						$("#edit_sub_cat" + id).submit();
					} else {
						$(".category-exists").show();
						setTimeout(function() {
							$(".category-exists").hide();
						}, 2000);
					}
				}
			});
		} else {
			$(this).parents('.edit_subscriptioncategories').find("#category_error").show();
		}
	});



	$(".submit_category").click(function() {
		var id = $(this).attr('id');
		var counts = "#service_edit_" + id;

		var service_name = $(counts).find('#service_name').val();
		var service_price = $(counts).find("#service_price").val();
		var service_qty = $(counts).find("#service_qty").val();
		var service_unit = $(counts).find("#service_unit").val();
		var service_description = $(counts).find("#service_description").val();
		var service_category = $(counts).find("#service_category").val();
		// console.log(service_name);

		if (service_name == 'other') {
			service_name = $(counts).find("input[name=edit_other_service_name]").val();
		}
		// console.log(service_name);

		if (service_name != "" && service_category != "") {
			$.ajax({
				url: '<?php echo base_url(); ?>proposal/edit_service_check',
				type: 'POST',
				data: {
					'service_name': service_name,
					'id': id,
					'service_category': service_category
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					var json = JSON.parse(data);
					status = json['status'];
					$(counts + ' .error_msgs').html('');

					if (status == '0') {
						var err = 0;

						if (service_price != "" && Number(service_price) <= Number(0)) {
							$(counts).find("#service_price").css('border', '1px solid red');

							if (!$(counts + ' .error_msgs .price_er').hasClass('price_er')) {
								$(counts + ' .error_msgs').append('<p class="price_er" style="color:red !important;">Invalid Price.</p>');
							}

							err++;
						}
						if (service_qty != "" && (Number(service_qty) <= Number(0) || parseFloat(Number(service_qty) % Number(1)) != Number(0))) {
							$(counts).find("#service_qty").css('border', '1px solid red');

							if (!$(counts + ' .error_msgs .quantity_er').hasClass('quantity_er')) {
								$(counts + ' .error_msgs').append('<p class="quantity_er" style="color:red !important;">Invalid Quantity.</p>');
							}

							err++;
						}

						if (service_name != '' && service_price != '' && service_qty != '' && service_unit != '' && service_description != '' && service_category != '' && err == 0) {
							$("#edit_service_section" + id).submit();
						} else {
							showErrors_edit(counts);
						}

					} else {
						$(".service-exists").show();

						if ($(".service-exists").is(':visible') == false) {
							if (!$(counts + '.error_msgs .exist_er').hasClass('exist_er')) {
								$(counts + ' .error_msgs').append('<p class="exist_er" style="color:red !important;">Service Name Already Exists.</p>');
							}
						}

						setTimeout(function() {
							$(".service-exists").hide();
						}, 2000);
					}
				}
			});
		} else {
			showErrors_edit(counts);
		}

	});

	function showErrors_edit(counts) {
		if ($(counts).find('#service_name').val() == '') {
			$(counts).find('#service_name').css('border', '1px solid red');
		}
		if ($(counts).find("#service_price").val() == '') {
			$(counts).find("#service_price").css('border', '1px solid red');
		}
		if ($(counts).find("#service_qty").val() == '') {
			$(counts).find("#service_qty").css('border', '1px solid red');
		}

		if ($(counts).find("#service_unit").val() == '') {
			$(counts).find("#service_unit").css('border', '1px solid red');
		}

		if ($(counts).find("#service_description").val() == '') {
			$(counts).find("#service_description").css('border', '1px solid red');
		}

		if ($(counts).find("#service_category").val() == '') {
			$(counts).find("#service_category").css('border', '1px solid red');
		}

		$("input,textarea").keyup(function() {
			$("input,textarea,select").css('border', '');
		});

		$("select").change(function() {
			$("select").removeAttr('style');
		});

	}

	$(".submit_product").click(function() {
		var id = $(this).attr('id');
		//alert(id);
		// var id=$("#edit_service_id").val();
		var service_name = $(this).parents('.product_edit').find("#edit_product_name").val();


		$.ajax({
			url: '<?php echo base_url(); ?>proposal/edit_product_check',
			type: 'POST',
			data: {
				'product_name': service_name,
				'id': id
			},
			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(data) {
				//alert(data); 
				$(".LoadingImage").hide();
				var json = JSON.parse(data);
				status = json['status'];

				if (status == '0') {
					var counts = "#products_edit_" + id;
					$(counts + ' .error_msgs').html('');
					var err = 0;
					//  console.log(counts);
					//  var i=0;
					var edit_product_name = $(counts).find('#edit_product_name').val();
					var product_price = $(counts).find("#product_price").val();
					// var service_qty=$(counts).find("#service_qty").val();
					// var service_unit=$(counts).find("#service_unit").val();
					var product_description = $(counts).find("#product_description").val();
					var product_category = $(counts).find("#product_category").val();

					// console.log(edit_product_name);
					// console.log(product_price);
					//  // console.log(service_qty);
					//  //  console.log(service_unit);
					//    console.log(product_description);
					//     console.log(product_category);
					//var i=0;

					if ($(counts).find('#edit_product_name').val() == '') {
						$(counts).find('#edit_product_name').css('border', '1px solid red');
					}

					if ($(counts).find("#product_price").val() == '') {
						$(counts).find("#product_price").css('border', '1px solid red');
					} else if (Number($(counts).find("#product_price").val()) <= Number(0)) {
						$(counts).find("#product_price").css('border', '1px solid red');

						if (!$(counts + ' .error_msgs .price_er').hasClass('price_er')) {
							$(counts + ' .error_msgs').append('<p class="price_er" style="color:red !important;">Invalid Price.</p>');
						}

						err++;
					}
					//  if($(counts).find("#service_qty").val()==''){
					//   $(counts).find("#service_qty").css('border','1px solid red');
					// }

					// if($(counts).find("#service_unit").val()==''){
					//   $(counts).find("#service_unit").css('border','1px solid red');
					// }


					if ($(counts).find("#product_description").val() == '') {
						$(counts).find("#product_description").css('border', '1px solid red');
					}

					if ($(counts).find("#product_category").val() == '') {
						$(counts).find("#product_category").css('border', '1px solid red');
					}

					$("input,textarea").keyup(function() {
						$("input,textarea,select").css('border', '');
					});

					$("select").change(function() {
						$("select").removeAttr('style');
					});


					if (edit_product_name != '' && product_price != '' && product_description != '' && product_category != '' && err == 0) {
						$("#edit_product_section" + id).submit();
					}
				} else {
					$(".product-exists").show();

					if ($(".product-exists").is(':visible') == false) {
						if (!$(counts + ' .error_msgs .exist_er').hasClass('exist_er')) {
							$(counts + ' .error_msgs').append('<p class="exist_er" style="color:red !important;">Product Name Already Exists.</p>');
						}
					}

					setTimeout(function() {
						$(".product-exists").hide();
					}, 2000);
				}
			}
		});
	});

	$("form[data-sub-form-validate='true']").each(function() {
		$(this).validate({
			rules: {
				subscription_name: {
					required: true,
				},
				subscription_price: {
					required: true,
					number: true,
					minStrict: 0,
				},
				subscription_unit: {
					required: true,
				},
				subscription_description: {
					required: true,
				},
				subscription_category: {
					required: true,
				},
			},
			highlight: function(element) {
				$(element).addClass("error-border");
			},
			unhighlight: function(element) {
				$(element).removeClass("error-border");
			},
			errorPlacement: function(error, element) {
				return true;
			}
		});


	});



	$(".submit_subscription").click(function() {
		var id = $(this).attr('id');
		var service_name = $(this).parents('.subscription_edit').find("#edit_subscription_name").val();

		$("#edit_subscription_section" + id).valid();

		$.ajax({
			url: '<?php echo base_url(); ?>proposal/edit_subscription_check',
			type: 'POST',
			data: {
				'subscription_name': service_name,
				'id': id
			},

			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(data) {
				$(".LoadingImage").hide();
				var json = JSON.parse(data);
				status = json['status'];
				var err = 0;
				$('#edit_subscription_section' + id + ' .error_msgs').html('');

				if (status == '0') {
					if ($('#edit_subscription_section' + id).find("#subscription_price").val() != "" && Number($('#edit_subscription_section' + id).find("#subscription_price").val()) <= Number(0)) {
						if (!$('#edit_subscription_section' + id + ' .error_msgs .price_er').hasClass('price_er')) {
							$('#edit_subscription_section' + id + ' .error_msgs').append('<p class="price_er" style="color:red !important;">Invalid Price.</p>');
						}

						err++;
					}

					if (err == 0) {
						$("#edit_subscription_section" + id).submit();
					}
				} else {
					$(".subscription-exists").show();

					if ($(".subscription-exists").is(':visible') == false) {
						if (!$('#edit_subscription_section' + id + ' .error_msgs .exist_er').hasClass('exist_er')) {
							$('#edit_subscription_section' + id + ' .error_msgs').append('<p class="exist_er" style="color:red !important;">Subscription Name Already Exists.</p>');
						}
					}

					setTimeout(function() {
						$(".subscription-exists").hide();
					}, 2000);
				}

			}
		});

	});



	function initialize_datatable5() {

		var check = 0;
		var check1 = 0;

		<?php
		$column_setting = Firm_column_settings('proposal_price');

		?>
		var column_order = <?php echo $column_setting['order'] ?>;

		var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;

		var column_ordering = [];

		$.each(column_order, function(i, v) {

			var index = $('#display_service5 thead th.' + v).index();

			if (index !== -1) {
				column_ordering.push(index);
				// console.log(column_ordering);
			}
		});



		var numCols = $('#display_service5 thead th').length;
		// console.log(numCols);
		//  console.log(column_order);
		//   console.log(column_order.length);
		display_service5 = $('#display_service5').DataTable({

			"dom": '<"toolbar-table" B>lfrtip',
			buttons: [{
				extend: 'collection',
				background: false,
				text: '<i class="fa fa-cog" aria-hidden="true"></i>',
				className: 'Settings_Button',
				buttons: [{
						text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
						className: 'Button_List',
						action: function(e, dt, node, config) {
							Trigger_To_Reset_Filter5();
						}
					},
					{
						extend: 'colvis',
						text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
						columns: ':not(.Exc-colvis)',
						className: 'Button_List',
						prefixButtons: [{
							text: 'X',
							className: 'close'
						}]
					}
				]
			}],
			order: [], //ITS FOR DISABLE SORTING
			columnDefs: [{
				"orderable": false,
				"targets": ['price_chk_TH', 'price_action_TH']
			}],


			fixedHeader: {
				header: true,
				footer: true
			},

			colReorder: {
				realtime: false,
				order: column_ordering,
				fixedColumnsLeft: 1,
				fixedColumnsRight: 1

			},
			initComplete: function() {

				var api = this.api();

				api.columns('.hasFilter').every(function() {

					var column = this;
					var TH = $(column.header());
					var Filter_Select = TH.find('.filter_check');
					///alert(Filter_Select.length);
					// alert('check');
					if (!Filter_Select.length) {
						// alert('check1');
						Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo(TH);
						var unique_data = [];
						column.nodes().each(function(d, j) {
							var dataSearch = $(d).attr('data-search');

							if (jQuery.inArray(dataSearch, unique_data) === -1) {
								//console.log(d);
								Filter_Select.append('<option  value="' + dataSearch + '">' + dataSearch + '</option>');
								unique_data.push(dataSearch);
							}

						});
					}


					Filter_Select.on('change', function() {

						var search = $(this).val();
						//console.log( search );
						if (search.length) search = '^(' + search.join('|') + ')$';

						var class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);

						var cur_column = api.column('.' + class_name + '_TH');

						cur_column.search(search, true, false).draw();
					});

					//console.log(select.attr('style'));
					Filter_Select.formSelect();


				});

			}
		});



		ColVis_Hide(display_service5, hidden_coulmns);

		//   Filter_IconWrap();

		Change_Sorting_Event(display_service5);

		ColReorder_Backend_Update(display_service5, 'proposal_price');

		ColVis_Backend_Update(display_service5, 'proposal_price');

		/*
		                        var Reset_filter = "<li><button id='reset_filter_button5' >Reset Filter</button></li>";
		      
		$("#display_service5_wrapper .toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

		$('#reset_filter_button5').click(Trigger_To_Reset_Filter5);*/


	}


	function initialize_datatable4() {

		var check = 0;
		var check1 = 0;

		<?php
		$column_setting = Firm_column_settings('proposal_subscription');
		?>
		var column_order = <?php echo $column_setting['order'] ?>;

		var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;

		var column_ordering = [];

		$.each(column_order, function(i, v) {

			var index = $('#display_service4 thead th.' + v).index();

			if (index !== -1) {
				column_ordering.push(index);
				// console.log(column_ordering);
			}
		});



		var numCols = $('#display_service4 thead th').length;
		// console.log(numCols);
		//  console.log(column_order);
		//   console.log(column_order.length);
		display_service4 = $('#display_service4').DataTable({

			"dom": '<"toolbar-table" B>lfrtip',
			buttons: [{
				extend: 'collection',
				background: false,
				text: '<i class="fa fa-cog" aria-hidden="true"></i>',
				className: 'Settings_Button',
				buttons: [{
						text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
						className: 'Button_List',
						action: function(e, dt, node, config) {
							Trigger_To_Reset_Filter4();
						}
					},
					{
						extend: 'colvis',
						text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
						columns: ':not(.Exc-colvis)',
						className: 'Button_List',
						prefixButtons: [{
							text: 'X',
							className: 'close'
						}]
					}
				]
			}],
			order: [], //ITS FOR DISABLE SORTING
			columnDefs: [{
				"orderable": false,
				"targets": ['subscription_chk_TH', 'sub_action_TH']
			}],


			fixedHeader: {
				header: true,
				footer: true
			},

			colReorder: {
				realtime: false,
				order: column_ordering,
				fixedColumnsLeft: 1,
				fixedColumnsRight: 1

			},
			initComplete: function() {

				var api = this.api();

				api.columns('.hasFilter').every(function() {

					var column = this;
					var TH = $(column.header());
					var Filter_Select = TH.find('.filter_check');
					///alert(Filter_Select.length);
					// alert('check');
					if (!Filter_Select.length) {
						// alert('check1');
						Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" searchable="true" style="display:none"></select>').appendTo(TH);
						var unique_data = [];
						column.nodes().each(function(d, j) {
							var dataSearch = $(d).attr('data-search');

							if (jQuery.inArray(dataSearch, unique_data) === -1) {
								//console.log(d);
								Filter_Select.append('<option  value="' + dataSearch + '">' + dataSearch + '</option>');
								unique_data.push(dataSearch);
							}

						});
					}



					Filter_Select.on('change', function() {

						var search = $(this).val();
						//console.log( search );
						if (search.length) search = '^(' + search.join('|') + ')$';

						var class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);

						var cur_column = api.column('.' + class_name + '_TH');

						cur_column.search(search, true, false).draw();
					});

					//console.log(select.attr('style'));
					Filter_Select.formSelect();


				});

			}
		});



		ColVis_Hide(display_service4, hidden_coulmns);

		//   Filter_IconWrap();

		Change_Sorting_Event(display_service4);

		ColReorder_Backend_Update(display_service4, 'proposal_subscription');

		ColVis_Backend_Update(display_service4, 'proposal_subscription');

		/*var Reset_filter = "<li><button id='reset_filter_button4' >Reset Filter</button></li>";
      
$("#display_service4_wrapper .toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

$('#reset_filter_button4').click(Trigger_To_Reset_Filter4);*/



	}


	function initialize_datatable3() {

		var check = 0;
		var check1 = 0;

		<?php
		$column_setting = Firm_column_settings('proposal_tax');

		?>
		var column_order = <?php echo $column_setting['order'] ?>;

		var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;

		var column_ordering = [];

		$.each(column_order, function(i, v) {

			var index = $('#display_service3 thead th.' + v).index();

			if (index !== -1) {
				column_ordering.push(index);
				// console.log(column_ordering);
			}
		});



		var numCols = $('#display_service3 thead th').length;
		// console.log(numCols);
		//  console.log(column_order);
		//   console.log(column_order.length);
		display_service3 = $('#display_service3').DataTable({

			"dom": '<"toolbar-table" B>lfrtip',
			buttons: [{
				extend: 'collection',
				background: false,
				text: '<i class="fa fa-cog" aria-hidden="true"></i>',
				className: 'Settings_Button',
				buttons: [{
						text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
						className: 'Button_List',
						action: function(e, dt, node, config) {
							Trigger_To_Reset_Filter3();
						}
					},
					{
						extend: 'colvis',
						text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
						columns: ':not(.Exc-colvis)',
						className: 'Button_List',
						prefixButtons: [{
							text: 'X',
							className: 'close'
						}]
					}
				]
			}],
			order: [], //ITS FOR DISABLE SORTING
			columnDefs: [{
				"orderable": false,
				"targets": ['tax_chk_TH', 'tax_action_TH']
			}],


			fixedHeader: {
				header: true,
				footer: true
			},

			colReorder: {
				realtime: false,
				order: column_ordering,
				fixedColumnsLeft: 1,
				fixedColumnsRight: 1

			},
			initComplete: function() {

				var api = this.api();

				api.columns('.hasFilter').every(function() {

					var column = this;
					var TH = $(column.header());
					var Filter_Select = TH.find('.filter_check');
					///alert(Filter_Select.length);
					// alert('check');
					if (!Filter_Select.length) {
						// alert('check1');
						Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo(TH);
						var unique_data = [];
						column.nodes().each(function(d, j) {
							var dataSearch = $(d).attr('data-search');

							if (jQuery.inArray(dataSearch, unique_data) === -1) {
								//console.log(d);
								Filter_Select.append('<option  value="' + dataSearch + '">' + dataSearch + '</option>');
								unique_data.push(dataSearch);
							}

						});
					}


					Filter_Select.on('change', function() {

						var search = $(this).val();
						//console.log( search );
						if (search.length) search = '^(' + search.join('|') + ')$';

						var class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);

						var cur_column = api.column('.' + class_name + '_TH');

						cur_column.search(search, true, false).draw();
					});

					//console.log(select.attr('style'));
					Filter_Select.formSelect();


				});

			}
		});


		ColVis_Hide(display_service3, hidden_coulmns);

		//   Filter_IconWrap();

		Change_Sorting_Event(display_service3);

		ColReorder_Backend_Update(display_service3, 'proposal_tax');

		ColVis_Backend_Update(display_service3, 'proposal_tax');


		/*var Reset_filter = "<li><button id='reset_filter_button3' >Reset Filter</button></li>";
      
$("#display_service3_wrapper .toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

$('#reset_filter_button3').click(Trigger_To_Reset_Filter3);*/




	}

	function initialize_datatable1() {

		var check = 0;
		var check1 = 0;

		<?php
		$column_setting = Firm_column_settings('proposal_product');

		?>
		var column_order = <?php echo $column_setting['order'] ?>;

		var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;

		var column_ordering = [];

		$.each(column_order, function(i, v) {

			var index = $('#display_service1 thead th.' + v).index();

			if (index !== -1) {
				column_ordering.push(index);
			}
		});



		var numCols = $('#display_service1 thead th').length;
		display_service1 = $('#display_service1').DataTable({

			"dom": '<"toolbar-table" B>lfrtip',
			buttons: [{
				extend: 'collection',
				background: false,
				text: '<i class="fa fa-cog" aria-hidden="true"></i>',
				className: 'Settings_Button',
				buttons: [{
						text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
						className: 'Button_List',
						action: function(e, dt, node, config) {
							Trigger_To_Reset_Filter1();
						}
					},
					{
						extend: 'colvis',
						text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
						columns: ':not(.Exc-colvis)',
						className: 'Button_List',
						prefixButtons: [{
							text: 'X',
							className: 'close'
						}]
					}
				]
			}],
			order: [], //ITS FOR DISABLE SORTING
			columnDefs: [{
				"orderable": false,
				"targets": ['proposal_TH', 'action_TH']
			}],


			fixedHeader: {
				header: true,
				footer: true
			},

			colReorder: {
				realtime: false,
				order: column_ordering,
				fixedColumnsLeft: 1,
				fixedColumnsRight: 1

			},
			initComplete: function() {

				var api = this.api();

				api.columns('.hasFilter').every(function() {

					var column = this;
					var TH = $(column.header());
					var Filter_Select = TH.find('.filter_check');
					///alert(Filter_Select.length);
					// alert('check');
					if (!Filter_Select.length) {
						// alert('check1');
						Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo(TH);
						var unique_data = [];
						column.nodes().each(function(d, j) {
							var dataSearch = $(d).attr('data-search');

							if (jQuery.inArray(dataSearch, unique_data) === -1) {
								//console.log(d);
								Filter_Select.append('<option  value="' + dataSearch + '">' + dataSearch + '</option>');
								unique_data.push(dataSearch);
							}

						});
					}


					Filter_Select.on('change', function() {

						var search = $(this).val();
						//console.log( search );
						if (search.length) search = '^(' + search.join('|') + ')$';

						var class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);

						var cur_column = api.column('.' + class_name + '_TH');

						cur_column.search(search, true, false).draw();
					});

					//console.log(select.attr('style'));
					Filter_Select.formSelect();


				});

			}
		});

		ColVis_Hide(display_service1, hidden_coulmns);

		//   Filter_IconWrap();

		Change_Sorting_Event(display_service1);

		ColReorder_Backend_Update(display_service1, 'proposal_product');

		ColVis_Backend_Update(display_service1, 'proposal_product');

		/*       var Reset_filter = "<li><button id='reset_filter_button1' >Reset Filter</button></li>";
      
$("#display_service1_wrapper .toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

$('#reset_filter_button1').click(Trigger_To_Reset_Filter1);*/




	}


	function initialize_datatable2() {
		var check = 0;
		var check1 = 0;

		<?php
		$column_setting = Firm_column_settings('proposal_service');

		?>
		var column_order = <?php echo $column_setting['order'] ?>;

		var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;

		var column_ordering = [];

		$.each(column_order, function(i, v) {

			var index = $('#display_service2 thead th.' + v).index();

			if (index !== -1) {
				column_ordering.push(index);
			}
		});



		var numCols = $('#display_service2 thead th').length;

		display_service2 = $('#display_service2').DataTable({
			"dom": '<"toolbar-table" B>lfrtip',
			buttons: [{
				extend: 'collection',
				background: false,
				text: '<i class="fa fa-cog" aria-hidden="true"></i>',
				className: 'Settings_Button',
				buttons: [{
						text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
						className: 'Button_List',
						action: function(e, dt, node, config) {
							Trigger_To_Reset_Filter2();
						}
					},
					{
						extend: 'colvis',
						text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
						columns: ':not(.Exc-colvis)',
						className: 'Button_List',
						prefixButtons: [{
							text: 'X',
							className: 'close'
						}]
					}
				]
			}],
			order: [], //ITS FOR DISABLE SORTING
			columnDefs: [{
				"orderable": false,
				"targets": ['proposal_TH', 'action_TH']
			}],


			fixedHeader: {
				header: true,
				footer: true
			},

			colReorder: {
				realtime: false,
				order: column_ordering,
				fixedColumnsLeft: 1,
				fixedColumnsRight: 1

			},
			initComplete: function() {

				var api = this.api();

				api.columns('.hasFilter').every(function() {

					var column = this;
					var TH = $(column.header());
					var Filter_Select = TH.find('.filter_check');
					///alert(Filter_Select.length);
					// alert('check');
					if (!Filter_Select.length) {
						// alert('check1');
						Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo(TH);
						var unique_data = [];
						column.nodes().each(function(d, j) {
							var dataSearch = $(d).attr('data-search');

							if (jQuery.inArray(dataSearch, unique_data) === -1) {
								//console.log(d);
								Filter_Select.append('<option  value="' + dataSearch + '">' + dataSearch + '</option>');
								unique_data.push(dataSearch);
							}

						});
					}


					Filter_Select.on('change', function() {

						var search = $(this).val();
						//console.log( search );
						if (search.length) search = '^(' + search.join('|') + ')$';

						var class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);

						var cur_column = api.column('.' + class_name + '_TH');

						cur_column.search(search, true, false).draw();
					});

					//console.log(select.attr('style'));
					Filter_Select.formSelect();


				});


			}
		});

		ColVis_Hide(display_service2, hidden_coulmns);

		Filter_IconWrap();

		Change_Sorting_Event(display_service2);

		ColReorder_Backend_Update(display_service2, 'proposal_service');

		ColVis_Backend_Update(display_service2, 'proposal_service');

		/*

		      var Reset_filter = "<li><button id='reset_filter_button' >Reset Filter</button></li>";
		      
		$("#display_service2_wrapper .toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

		$('#reset_filter_button').click(Trigger_To_Reset_Filter2);*/

	}


	function Trigger_To_Reset_Filter2() {

		$('#checkbox0').prop('checked', false);
		$('#checkbox0').trigger('change');


		$('div.toolbar-table .toolbar-search li.dropdown-chose').each(function() {
			$(this).trigger('click');
		});


		MainStatus_Tab = "all_task";

		Redraw_Table(display_service2);

		// Redraw_Table(TaskTable_Instance);

	}



	function Trigger_To_Reset_Filter1() {

		$('#checkbox_4').prop('checked', false);
		$('#checkbox_4').trigger('change');


		$('div.toolbar-table .toolbar-search li.dropdown-chose').each(function() {
			$(this).trigger('click');
		});


		MainStatus_Tab = "all_task";

		Redraw_Table(display_service1);

		// Redraw_Table(TaskTable_Instance);

	}

	function Trigger_To_Reset_Filter3() {

		$('#checkboxes').prop('checked', false);
		$('#checkboxes').trigger('change');


		$('div.toolbar-table .toolbar-search li.dropdown-chose').each(function() {
			$(this).trigger('click');
		});


		MainStatus_Tab = "all_task";

		Redraw_Table(display_service3);

		// Redraw_Table(TaskTable_Instance);

	}

	function Trigger_To_Reset_Filter4() {

		$('#checkbox_00').prop('checked', false);
		$('#checkbox_00').trigger('change');


		$('div.toolbar-table .toolbar-search li.dropdown-chose').each(function() {
			$(this).trigger('click');
		});


		MainStatus_Tab = "all_task";

		Redraw_Table(display_service4);

		// Redraw_Table(TaskTable_Instance);

	}

	function Trigger_To_Reset_Filter5() {

		$('#check-box').prop('checked', false);
		$('#check-box').trigger('change');


		$('div.toolbar-table .toolbar-search li.dropdown-chose').each(function() {
			$(this).trigger('click');
		});


		MainStatus_Tab = "all_task";

		Redraw_Table(display_service5);

		// Redraw_Table(TaskTable_Instance);

	}



	$(document).ready(function() {


		/*2*/

		initialize_datatable1();
		initialize_datatable2();
		initialize_datatable3();
		initialize_datatable4();
		initialize_datatable5();




		/*4*/


		/*5*/


	});

	$(document).on('click', '.DT', function(e) {
		if (!$(e.target).hasClass('sortMask')) {
			e.stopImmediatePropagation();
		}
	});

	// $('th .themicond').on('click', function(e) {
	//   if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
	//   $(this).parent().find('.dropdown-content').removeClass('Show_content');
	//   }else{
	//    $('.dropdown-content').removeClass('Show_content');
	//    $(this).parent().find('.dropdown-content').addClass('Show_content');
	//   }
	//   $(this).parent().find('.select-wrapper').toggleClass('special');
	//       if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
	//       }else{
	//         $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
	//         $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );

	//       }
	// });

	//  $(document).on('click', 'th .themicond', function(){
	//       if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
	//          $(this).parent().find('.dropdown-content').removeClass('Show_content');
	//       }else{
	//          $('.dropdown-content').removeClass('Show_content');
	//          $(this).parent().find('.dropdown-content').addClass('Show_content');
	//       }
	//           $(this).parent().find('.select-wrapper').toggleClass('special');
	//       if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){ 
	//       }else{
	//           $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
	//           $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
	//       }
	// });
	$("th").on("click.DT", function(e) {
		if (!$(e.target).hasClass('sortMask')) {
			e.stopImmediatePropagation();
		}
	});
</script>


<script type="text/javascript">
	$(document).ready(function() {
		$(document).on('show.bs.modal', '.modal', function(event) {
			var zIndex = 1040 + (10 * $('.modal:visible').length);
			$(this).css('z-index', zIndex);
			setTimeout(function() {
				$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
			}, 0);
		});
	})
</script>
<!--- *********************************************** -->
<!--   Datatable JS by Ram -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/user_page/materialize.js"></script>

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/custom/buttons.colVis.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/custom/datatable_extension.js"></script>

<script src="<?php echo base_url() ?>assets/js/tinymce.min.js"></script>

<script>
	var tinymce_config = {    
			plugins: ' preview  paste importcss searchreplace autolink     visualblocks visualchars fullscreen image link table  hr pagebreak nonbreaking anchor toc  advlist lists imagetools textpattern noneditable ',
			menubar: 'file edit view insert format tools table',
			toolbar: 'fontselect fontsizeselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | insertfile image  link | numlist bullist  | hr | fullscreen  preview ',
			file_picker_types: "image"
		};
	$(document).ready(function() {
		
		tinymce_config['selector'] = '#add_service_description,#edit_service_description,#desc-price-list,#desc-add-product,#desc-add-subscription';
		tinymce.init( tinymce_config );
	});
</script>