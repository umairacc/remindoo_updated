
<style>	
.cke_chrome
{
    z-index: 999 !important;
}
/*.columns {
background: #fff;
padding: 20px;
}
.columns:after {
content: "";
clear: both;
display: block;
}
.columns > .editor {
float: left;
width: 65%;
position: relative;
z-index: 1;
}
.columns > .contacts {
float: right;
width: 35%;
box-sizing: border-box;
padding: 0 0 0 20px;
}
#contactList2 {
list-style-type: none;
margin: 0 !important;
padding: 0;
}
#contactList2 li {
background: #FAFAFA;
margin-bottom: 1px;
height: 36px;
line-height: 30px;
cursor: pointer;
}
#contactList2 li:nth-child(2n) {
background: #F3F3F3;
}
#contactList2 li:hover {
background: #FFFDE3;
border-left: 5px solid #DCDAC1;
margin-left: -5px;
}
.contact2 {
padding: 0 10px;
white-space: nowrap;
overflow: hidden;
text-overflow: ellipsis;
}*/
#editor3 .h-card2 {
background: #FFFDE3;
padding: 3px 6px;
border-bottom: 1px dashed #ccc;
}
#editor4 .h-card2 {
background: #FFFDE3;
padding: 3px 6px;
border-bottom: 1px dashed #ccc;
}
#editor3 {
position: relative;
}
#editor4 {

position: relative;
}
#editor3 .h-card2 .p-tel {
font-style: italic;
}
#editor3 .h-card2 .p-tel::before,
#editor4 .h-card2 .p-tel::after {
font-style: normal;
}
div#editor4 {

padding: 15px;
overflow: auto;
}
div#editor3 {
min-width: 100%;
max-width: 700px;
overflow: auto;
}

</style>
                
          
          <div class="columns column-realign1">
          	 <div class="edit-tempsect col-xs-12 editor">
          <div class="col-md-12">          
          <label>Titles</label>
          </div>
   		 <div class="col-md-12 select">          
         <select  name="title" id="title" class="form-control" data-parsley-required="true">
         <option value="">Select </option>
			<option value="new_proposal">New Proposal</option>
			<!--  <option value="discuss_proposal">Discuss Proposal</option> -->
			<option value="proposal_followup">Proposal Followup </option>
			<option value="decline_proposal_sender">Decline Proposal Sender Notification</option>
			<option value="decline_proposal_client">Decline Proposal Client Notification</option>
			<option value="accept_proposal_sender">Accept Proposal Sender Notification </option>
			<option value="accept_proposal_client">Accept Proposal Client Notification </option>
			<option value="sender_comment_notification">Sender comment Notification</option>
			<option value="client_Comments_notification">Client Comments Notification</option>
			
         </select>
          </div>         
          <div class="col-md-12">          
          <label>Subject</label>
          </div>
       
          <div cols="10" id="editor3" name="editor3" rows="10"  contenteditable="true">		

			</div>
          <div class="col-md-12">          
          <label>Body</label>
          </div>		
			<div cols="10" id="editor4" name="editor4" rows="10"  contenteditable="true">
			
			</div>
		</div>
		<div class="contacts">
			<h5>Adjustable Tokens</h5>
			<ul id="contactList2">
				<li>
					<div class="contact2 h-card2" data-contact="0" draggable="true" tabindex="0">
						::Client Name::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="1" draggable="true" tabindex="0">
						::Client Company::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="2" draggable="true" tabindex="0">
						::Client Company City::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="3" draggable="true" tabindex="0">
						::Client Company Country::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="4" draggable="true" tabindex="0">
						::Client Company Religion::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="5" draggable="true" tabindex="0">
						:: Client First Name::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="6" draggable="true" tabindex="0">
						:: Client Last Name::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="7" draggable="true" tabindex="0">
						:: Client Name::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="8" draggable="true" tabindex="0">
						:: ClientPhoneno::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="9" draggable="true" tabindex="0">
						:: ClientWebsite::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="10" draggable="true" tabindex="0">
						:: ClientProposallink::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="11" draggable="true" tabindex="0">
						:: ProposalCurrency::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="12" draggable="true" tabindex="0">
						:: ProposalExpirationDate::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="13" draggable="true" tabindex="0">
						:: ProposalName::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="14" draggable="true" tabindex="0">
						:: ProposalCodeNumber::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="15" draggable="true" tabindex="0">
						::SenderCompany::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="16" draggable="true" tabindex="0">
						::SenderCompanyAddress::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="17" draggable="true" tabindex="0">
						::SenderCompanyCity::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="18" draggable="true" tabindex="0">
						::SenderCompanyCountry::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="19" draggable="true" tabindex="0">
						::SenderCompanyReligion::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="20" draggable="true" tabindex="0">
						::SenderEmail::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="21" draggable="true" tabindex="0">
						::SenderEmailSignature::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="22" draggable="true" tabindex="0">
						::SenderName::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="23" draggable="true" tabindex="0">
						::senderPhoneno::</div>
				</li>
				<li>
					<div class="contact2 h-card2" data-contact="24" draggable="true" tabindex="0">
						:: ProposalTitle::</div>
				</li>
			</ul>
		</div>
		<div class="modal-footer">
            
         </div>
	</div> 
	           <!-- Page body end -->
       
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
    


	
  