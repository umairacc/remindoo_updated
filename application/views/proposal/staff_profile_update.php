<?php $this->load->view('includes/header');
   $userid = $this->session->userdata('id');  
   
   if(isset($_GET['action']) && $_GET['action']=="time_card")
   {
      $css_time_active = 'in active';
      $css_bank_active = '';
      $bank_active = '';
      $time_active = 'active';
   }else{
      $css_bank_active = 'in active';
      $css_time_active = '';
      $bank_active = 'active';
      $time_active = '';
   }
   function getStartAndEndDate($week, $year) {
    $dateTime = new DateTime();
    $dateTime->setISODate($year, $week);
    $result['start_date'] = $dateTime->format('d-m-Y');
    $dateTime->modify('+6 days');
    $result['end_date'] = $dateTime->format('d-m-Y');
    return $result;
   }
   
   function getDatesFromRange($start, $end, $format = 'Y-m-d') {
      $array = array();
      $interval = new DateInterval('P1D');
   
      $realEnd = new DateTime($end);
      $realEnd->add($interval);
   
      $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
   
      foreach($period as $date) { 
          $array[] = $date->format($format); 
      }
   
      return $array;
   }
   /*function isWeekend($date) {
      $weekDay = date('w', strtotime($date));
      return ($weekDay == 0 || $weekDay == 6);
   }*/
   function isWeekend($date) {
      return (date('N', strtotime($date)) == 7);
   }
   $notstarted = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','notstarted');
          $inprogress = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','inprogress');
          $awaiting = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','awaiting');
          $testing = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','testing');
          $complete = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','complete');
          
          $role = $this->Common_mdl->getRole($_SESSION['id']);
          //print_r($task_list);
   
          $assignNotstarted = 0;
          $assignInprogress = 0;
          $assignAwaiting = 0;
          $assignTesting = 0;
          $assignComplete = 0;
   
          foreach ($task_list as $key => $value) {
              $staffs = $value['worker'].','.$value['manager'];
   
              $explode_worker=explode(',',$staffs);
   
              if($value['task_status']=='notstarted') {
                  if(in_array( $_SESSION['id'] ,$explode_worker )){
                      $assignNotstarted++;
                  }
              }
              if($value['task_status']=='inprogress') {
                  if(in_array( $_SESSION['id'] ,$explode_worker )){
                  $assignInprogress++;
                  }
              }
              if($value['task_status']=='awaiting') {
                  if(in_array( $_SESSION['id'] ,$explode_worker )){
                  $assignAwaiting++;
                  }
              }
              if($value['task_status']=='testing') {
                  if(in_array( $_SESSION['id'] ,$explode_worker )){
                  $assignTesting++;
                  }
              }
              if($value['task_status']=='complete') {
                  if(in_array( $_SESSION['id'] ,$explode_worker )){
                  $assignComplete++;
                  }
              }
   
          }
   
          function convertToHoursMins($time, $format = '%02d:%02d') {
      if ($time < 1) {
          return;
      }
      $hours = floor($time / 60);
      $minutes = ($time % 60);
      return sprintf($format, $hours, $minutes);
   }
   $exp_t =  convertToHoursMins($tot_tim);
   $ex_timer = explode(':', $exp_t);
   
    $type = CAL_GREGORIAN;
   $workdays = array();
   
   $month = date('n'); // Month ID, 1 through to 12.
      $year = date('Y'); // Year in 4 digit 2009 format.
      $day_count = cal_days_in_month($type, $month, $year); 
   
   for ($i = 1; $i <= $day_count; $i++) {
   
          $date = $year.'/'.$month.'/'.$i; //format date
          $get_name = date('l', strtotime($date)); //get week day
          $day_name = substr($get_name, 0, 3); // Trim day name to 3 chars
   
          //if not a weekend add day to array
          //if($day_name != 'Sun' && $day_name != 'Sat'){
          if($day_name != 'Sun'){
              $workdays[] = $i;
          }
   
   }
   $w_days = count($workdays);
   
   //print_r(count($workdays));
   ?>
<link href="<?php echo  base_url()?>assets/css/c3.css" rel="stylesheet" type="text/css">
<link href="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/demo/MonthPicker.min.css" rel="stylesheet" type="text/css">
<link href="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/test/test.css" rel="stylesheet" type="text/css">
<div class="pcoded-content">
<div class="staff-profile-cnt floating_set">
   <div class="title_page01 add_profile_01 floating">
      <div class="revenue-task row">
         <!-- card1 start -->
         <div class="col-md-6 col-xl-3">
            <div class="card widget-card-1">
               <div class="card-block-small">
                  <i class="icofont icofont-warning-alt bg-c-green card1-icon"></i>
                  <span class="text-c-green f-w-600">Complete Tasks</span>
                  <h4><?php echo $assignComplete;?></h4>
               </div>
            </div>
         </div>
         <!-- card1 end -->
         <!-- card1 start -->
         <div class="col-md-6 col-xl-3">
            <div class="card widget-card-1">
               <div class="card-block-small">
                  <i class="icofont icofont-ui-home bg-c-pink card1-icon"></i>
                  <span class="text-c-pink f-w-600">Testing Tasks</span>
                  <h4><?php echo $assignTesting;?></h4>
               </div>
            </div>
         </div>
         <!-- card1 end -->
         <!-- card1 start -->
         <div class="col-md-6 col-xl-3">
            <div class="card widget-card-1">
               <div class="card-block-small">
                  <i class="icofont icofont-social-twitter bg-c-yellow card1-icon"></i>
                  <span class="text-c-yellow f-w-600">Not started Tasks</span>
                  <h4><?php echo $assignNotstarted;?></h4>
               </div>
            </div>
         </div>
         <!-- card1 end -->
         <!-- card1 start -->
         <div class="col-md-6 col-xl-3">
            <div class="card widget-card-1">
               <div class="card-block-small">
                  <i class="icofont icofont-pie-chart bg-c-blue card1-icon"></i>
                  <span class="text-c-blue f-w-600">In Progress Tasks</span>
                  <h4><?php echo $assignInprogress;?></h4>
               </div>
            </div>
         </div>
         <!-- card1 end -->
      </div>
      <div class="row">
         <div class="col-md-12 col-xl-6">
            <div class="card">
               <div class="card-block user-detail-card">
                  <div class="row">
                     <div class="col-sm-4">
                        <div class="height-profile">
                           <div class="height-profile1"> <?php  $getUserProfilepic = $this->Common_mdl->getUserProfilepic($userid); ?><img src="<?php echo $getUserProfilepic;?> " alt="" class="img-fluid p-b-10"></div>
                        </div>
                        <!-- <div class="contact-icon">
                           <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="View" ><i class="icofont icofont-eye m-0"></i></button>
                           <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Print" ><i class="icofont icofont-printer m-0"></i></button>
                           <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Download" ><i class="icofont icofont-download-alt m-0"></i></button>
                           <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Share" ><i class="icofont icofont-share m-0"></i></button>
                        </div> -->
                     </div>
                     <div class="col-sm-8 user-detail">
                        <div class="row">
                           <div class="col-sm-5">
                              <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Name :</h6>
                           </div>
                           <div class="col-sm-7">
                              <h6 class="m-b-30"><?php 
                  $user_role=$this->Common_mdl->select_record('user','id',$userid);
                  $role_name=$this->Common_mdl->select_record('Role','id',$user_role['role']);
                  echo $user_role['crm_name'];?></h6>
                           </div>
                        </div>
                      
                        <div class="row">
                           <div class="col-sm-5">
                              <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-email"></i>EMP ID</h6>
                           </div>
                           <div class="col-sm-7">
                              <h6 class="m-b-30"><a href="mailto:dummy@example.com"><?php echo '#'.$userid; ?></a></h6>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-5">
                              <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-home"></i>Role ID :</h6>
                           </div>
                           <div class="col-sm-7">
                              <h6 class="m-b-30"><?php echo $role_name['role']; ?></h6>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-5">
                              <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-touch-phone"></i>Phone :</h6>
                           </div>
                           <div class="col-sm-7">
                              <h6 class="m-b-30"> </h6>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-5">
                              <h6 class="f-w-400 m-b-30"><i class="icofont icofont-web"></i>Website :</h6>
                           </div>
                           <div class="col-sm-7">
                              <h6 class="m-b-30"><a href="#!"> </a></h6>
                           </div>
                        </div> 
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
		 
		 <!-- Summery Start -->
                                            <div class="col-md-12 col-xl-6 summary-card1">
                                                <div class="card summery-card">
                                                    <div class="card-header">
                                                        <div class="card-header-left ">
                                                            <h5>Summary</h5>
                                                        </div>
                                                   
                                                    </div>
                                                    <div class="card-block">
                                                        <div class="row">
                                                            <div class="col-sm-6 b-r-default p-b-30">
                                                                <h2 class="f-w-400">13</h2>
                                                                <p class="text-muted f-w-400">Active users</p>
                                                                <div class="progress">
                                                                    <div class="progress-bar bg-c-blue" role="progressbar"
                                                                         aria-valuemin="0" aria-valuemax="100"
                                                                         style="width:50%">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 p-b-30">
                                                                <h2 class="f-w-400">28</h2>
                                                                <p class="text-muted f-w-400">Completed task</p>
                                                                <div class="progress">
                                                                    <div class="progress-bar bg-c-pink" role="progressbar"
                                                                         aria-valuemin="0" aria-valuemax="100"
                                                                         style="width:50%">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-sm-6 b-r-default p-b-30">
                                                                <h2 class="f-w-400">76</h2>
                                                                <p class="text-muted f-w-400">Active users</p>
                                                                <div class="progress">
                                                                    <div class="progress-bar bg-c-yellow" role="progressbar"
                                                                         aria-valuemin="0" aria-valuemax="100"
                                                                         style="width:50%">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 p-b-30">
                                                                <h2 class="f-w-400">76</h2>
                                                                <p class="text-muted f-w-400">Completed task</p>
                                                                <div class="progress">
                                                                    <div class="progress-bar bg-c-green" role="progressbar"
                                                                         aria-valuemin="0" aria-valuemax="100"
                                                                         style="width:50%">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- summary end -->
											
      </div>
      <!-- <div class="profile_setting1 floating">
         <!--  <div class="col-sm-4">
            <div class="profile_task1">
               <span><?php echo $assignNotstarted;?></span>
               <strong>Not started Tasks</strong>
               <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
            </div>
            <div class="profile_task1">
               <span><?php echo $assignInprogress;?></span>
               <strong>Inprogress Tasks</strong>
               <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
            </div>
            <!-- <div class="profile_task1">
               <span><?php echo $assignAwaiting;?></span>
               <strong>Awaiting Feedback Tasks</strong>
               <a href="#">More info &gt; </a>
               </div>  
            <div class="profile_task1">
               <span><?php echo $assignTesting;?></span>
               <strong>Testing Tasks</strong>
               <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
            </div>
            <div class="profile_task1">
               <span><?php echo $assignComplete;?></span>
               <strong>Complete Tasks</strong>
               <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
            </div>
            </div>  
         <div class="col-sm-4">
            <div class="profile_display1">
               <?php  $getUserProfilepic = $this->Common_mdl->getUserProfilepic($userid); ?>
               <img src="<?php echo $getUserProfilepic;?>">
               <h3> <?php 
                  $user_role=$this->Common_mdl->select_record('user','id',$userid);
                  $role_name=$this->Common_mdl->select_record('Role','id',$user_role['role']);
                  echo $user_role['crm_name'];?> </h3>
               <span>EMP ID: <?php 
                  echo '#'.$userid;
                  ?></span>
               <strong><?php 
                  echo $role_name['role'];
                  ?></strong>
            </div>
         </div>
         
         <div class="col-sm-4">
            <div class="profile_task1">
               <span><?php echo $assignNotstarted;?></span>
               <strong>Not started Tasks</strong>
               <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
            </div>
            <div class="profile_task1">
               <span><?php echo $assignInprogress;?></span>
               <strong>Inprogress Tasks</strong>
               <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
            </div>
            <!-- <div class="profile_task1">
               <span><?php echo $assignAwaiting;?></span>
               <strong>Awaiting Feedback Tasks</strong>
               <a href="#">More info &gt; </a>
               </div>  
            <div class="profile_task1">
               <span><?php echo $assignTesting;?></span>
               <strong>Testing Tasks</strong>
               <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
            </div>
            <div class="profile_task1">
               <span><?php echo $assignComplete;?></span>
               <strong>Complete Tasks</strong>
               <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
            </div>
         </div>
          
      </div>  -->
      <?php 
         //echo $user_role['role'].'priyarole';die;
         
         if($user_role['role']=='6'){?>
      <div class="overall_hours1 top-margin01 floating">
         <!-- <div class="project_hour01">
            <span>4:15:29</span>
            <h4>Projects Hours</h4>
            </div> -->
         <div class="project_hour01">
            <span><?php 
               if(isset($ex_timer[2])){ $vr = $ex_timer[2]; } else{ $vr = '00'; }
               // echo $ex_timer[0].':'.$ex_timer[1].':'.$vr;
               echo $task_cnt * 8 .' : 00 : 00';
                ?></span>
            <h4>Tasks Hours</h4>
         </div>
         <div class="project_hour01">
            <span><?php echo $w_days * 8 .' : 00 m'; ?></span>
            <h4>This month working Hours</h4>
         </div>
         <div class="project_hour01">
            <span><?php echo $w_days * 8 .' : 00 m'; ?></span>
            <h4>Working Hours</h4>
         </div>
      </div>
      <div class="additional-tab floating_set">
         <div class="deadline-crm1 floating_set">
            <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
               <li class="nav-item all-client-icon1">
                  <span><i class="fa fa-user fa-6" aria-hidden="true"></i></span>
               </li>
               <li class="nav-item">
                  <a class="nav-link <?php echo $bank_active;?>" data-toggle="tab" href="#bank_details">Bank Details</a>
                  <div class="slide"></div>
               </li>
               <li class="nav-item">
                  <a class="nav-link <?php echo $time_active;?>" data-toggle="tab" href="#time_card">Time Card - Login</a>
                  <div class="slide"></div>
               </li>
               <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#task_summary">Task summary</a>
                  <div class="slide"></div>
               </li>
               <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#all_activity">All activity log</a>
                  <div class="slide"></div>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url();?>login/logout">Logout</a>
                  <div class="slide"></div>
               </li>
            </ul>
         </div>
         <div class="all_user-section2 floating_set">
            <div class="tab-content">
               <div id="bank_details" class="tab-pane fade <?php echo $css_bank_active;?>">
                  <?php if(!empty($custom_form['labels'])){  ?>
                  <div class="new_bank1">
                     <h2>New Bank</h2>
                     <?php 
                        $succ = $this->session->flashdata('success');
                        
                         if($succ){?>
                     <div class="modal-alertsuccess alert alert-success"
                        >
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <div class="pop-realted1">
                           <div class="position-alert1"><?php echo $succ; ?></div>
                        </div>
                     </div>
                     <?php } ?>
                     <div class="bank-form">
                        <form action="<?php echo base_url();?>staff/update_custom" name="update_bank" id="update_bank" method="post">
                           <?php 
                              //  echo '<pre>';
                              //  print_r($custom_form);
                              //   echo '</pre>';
                              
                               
                              if(!empty($custom_form['labels'])){                
                              $labels=explode(',',$custom_form['labels']);
                              $field_type=explode(',',$custom_form['field_type']);
                              $field_name=explode(',',$custom_form['field_name']);
                              $values=explode(',',$custom_form['values']);
                              for($i=0;$i< count($labels);$i++){ 
                              
                                if($field_type[$i]=='textarea'){ ?>
                           <div class="form-group">
                              <label><?php echo $labels[$i]; ?></label>
                              <textarea name="<?php echo $field_name[$i]; ?>"><?php if(isset($values[$i])){
                                 echo $values[$i];
                                 } ?></textarea>
                           </div>
                           <?php  }else{ ?>
                           <div class="form-group">
                              <label><?php echo $labels[$i]; ?></label>
                              <input type="<?php echo $field_type[$i]; ?>" name="<?php echo $field_name[$i]; ?>" id="bank_name" value="<?php if(isset($values[$i])){
                                 echo $values[$i];
                                 } ?>">
                           </div>
                           <?php } }} ?>
                           <div class="text-right new-bank-update">
                              <input type="button" name="cancel" id="cancel" value="cancel">
                              <input type="submit" name="update" id="update" value="update">
                              <!-- <button type="button" name="cancel">Update</button>
                                 <button type="button" name="cancel">Cancel</button> -->
                           </div>
                        </form>
                     </div>
                  </div>
                  <?php } ?>
               </div>
               <!-- tab1 -->
               <div id="time_card" class="tab-pane fade <?php echo $css_time_active;?>">
                  <div class="new_bank1">
                     <h2>Timecard Details</h2>
                     <div class="working-hrs bank-form">
                        <div class="form-group date_birth">
                           <form name="time_cards" id="time_cards" method="post">
                              <label class="col-form-label">Month *</label>
                              <div class="working-picker">
                                 <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                 <input class="form-controlfields" type="text" name="month_pic" id="month_pic" value="<?php echo date('m').'/'.date('Y');?>">
                              </div>
                              <div class="go-bts">
                                 <button type="button" name="go" id="go">Go</button>
                                 <!--  <input type="submit" name="go" id="go" value="Go"> -->
                              </div>
                           </form>
                        </div>
                     </div>
                     <div class="work-month" id="timecard_filter">
                        <h3>Works Hours Details of <?php echo date('M').' - '.date('Y');?></h3>
                        <div class="table-responsive">
                           <table class="table">
                              <tr>
                                 <?php for ($i=$start_month; $i <=$end_month; $i++) { 
                                    $year = date('Y');
                                    $m = date('m');
                                    $dates = getStartAndEndDate($i,$year);
                                    $alldates = getDatesFromRange($dates['start_date'],$dates['end_date']);
                                    ?>
                                 <td class="filed-reject">
                                    <h4>Week:<?php echo ltrim($i,'0');?></h4>
                                    <?php 
                                       $w = 0;
                                       foreach ($alldates as $alldates_key => $alldates_value) {
                                       
                                           $weekend = isWeekend($alldates_value);
                                           //print_r($weekend);
                                           $exp_d = explode('-', $alldates_value);
                                           $d = $exp_d[1];
                                       
                                           if( $d == $m )
                                           {
                                               if($weekend==''){
                                                   $w++;
                                           ?>
                                    <p><b><?php echo $alldates_value;?></b> <span>8:00 m</span></p>
                                    <?php } else {?>
                                    <p><b><?php echo $alldates_value;?></b> <span><a href="#">Holiday</a></span></p>
                                    <?php } ?>
                                    <!-- <p><b>02-10-17</b> <span><a href="#">Holiday</a></span></p>
                                       <p><b>02-10-17</b> <span>0:0 m</span></p>
                                       <p><b>02-10-17</b> <span>0:0 m</span></p>
                                       <p><b>02-10-17 </b><span><a href="#">Holiday</a></span></p>
                                       <p><b>02-10-17</b> <span>0:0 m</span></p>
                                       <p class="removed-02"><b>02-10-17</b> <span><a href="#">Holiday</a></span></p> 
                                       
                                        -->
                                    <?php   } 
                                       }
                                          ?>
                                    <span class="total-working-hr"><strong>Total Working Hour: </strong> <?php echo $w * 8 ; ?>:0 m</span>
                                 </td>
                                 <?php }?>
                              </tr>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- tab2 -->	
               <div id="task_summary" class="tab-pane fade ">
                  <div class="new_bank1">
                     <h2>Task summary</h2>
                     <div class="task-filter1">
                        <div class="time-spent">
                           <h4>Total Task Time Spent</h4>
                           <!-- <div id="defaultCountdown"></div> -->
                           <div class="timer-section01">
                              <?php
                                 ?>
                              <span><?php echo $ex_timer[0];?> <strong>Hours</strong></span> :
                              <span><?php echo $ex_timer[1];?><strong>Minutes</strong></span> :
                              <span><?php if(isset($ex_timer[2])){ echo $ex_timer[2]; }else{ echo "00"; }?><strong>Seconds</strong></span> 
                           </div>
                        </div>
                        <div class="time-spent">
                           <h4>Task Reports</h4>
                           <div class="chart-container-01">
                              <div class="open-closed1">
                                 <!--
                                    <span> <i class="fa fa-square fa-6" aria-hidden="true"></i>Critical</span>
                                    <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> High</span>
                                    
                                    <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Medium</span>
                                    <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Low</span>
                                    
                                    <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Resolved</span>
                                    <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> closed</span> -->
                                 <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Low</span>
                                 <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> High</span>
                                 <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Medium</span>
                                 <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Super Urgent</span> 
                              </div>
                              <div id="chartContainer" style="width: 100%; height: 420px"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- tab3 -->	
               <div id="all_activity" class="tab-pane fade">
                  <div class="new_bank1">
                     <h2>All activity log</h2>
                     <div class="all_user-section2">
                        <table class="table client_table1 text-center display nowrap" id="all_activity1" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th>Activity Date</th>
                                 <th>User</th>
                                 <th>Module</th>
                                 <th>Activity</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php 
                                 //print_r($user_activity);die;
                                  foreach ($user_activity as $activity_key => $activity_value) { 
                                 
                                     $rec=$this->Common_mdl->select_record('user','id',$activity_value['user_id']);
                                     $role=$this->Common_mdl->select_record('Role','id',$rec['role']);
                                 
                                 
                                     ?>
                              <tr>
                                 <td><?php echo date("d-m-Y h:i:s A ",$activity_value['CreatedTime']);?></td>
                                 <td><?php echo $role['role'];?></td>
                                 <td><?php echo $activity_value['module'];?></td>
                                 <td><?php echo $activity_value['log'];?></td>
                              </tr>
                              <?php } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- tab4 -->					
               </div>
               <!-- tab-content --> 
            </div>
         </div>
         <!-- additional-tab -->
      </div>
   </div>
   <?php } else{ 
      ?>
   <div class="staffprofilelist01 floating">
      <div class="payroll-ac1">
	  
		<div class="payrolltax floating rev-op">
    <div class="search-vat1">
          <label>Search:</label>
          <input type="text" name="search">
    </div>  
         <div class="company-search1">
            <div class="vat-shipping">
               <label>Company Name</label> 
               <select name="companysearch" id="companysearch" class="search">
                  <option value="">All</option>
                  <?php foreach($getCompanyss as $company){ ?>
                  <option value="<?php echo $company['crm_company_number'] ?>"><?php echo $company["crm_company_name"];?></option>
                  <?php } ?>
               </select>
            </div>
            <div class="vat-shipping">
               <label>Company Type</label>
               <select name="legal_form" class="  fields search" id="legal_form">
                  <option value="" data-id="1">All</option>
                  <option value="Private Limited company" data-id="2">Private Limited company</option>
                  <option value="Public Limited company" data-id="3">Public Limited company</option>
                  <option value="Limited Liability Partnership" data-id="4">Limited Liability Partnership</option>
                  <option value="Partnership" data-id="5">Partnership</option>
                  <option value="Self Assessment" data-id="6">Self Assessment</option>
                  <option value="Trust" data-id="7">Trust</option>
                  <option value="Charity" data-id="8">Charity</option>
                  <option value="Other" data-id="9">Other</option>
               </select>
            </div>
            <div class="vat-shipping">
              <label>Deadline Type</label>
               <select name="legal_form" class="  fields search" id="legal_form">
                  <option value="" data-id="1">Due date form last 1 month</option>
                  <option value="" data-id="1">Due date form last 2 months</option>
                  <option value="" data-id="1">Due date form last 4 months</option>
                  <option value="" data-id="1">Due date form last 6 months</option>
                  <option value="" data-id="1">select custom due date</option>
               </select>
            </div>
			
				
         </div>
  		 
  		</div>
		
         <div class="deadlines-types1">
		 <div class="accident-toggle"><h2>MR ACCIDENT SOLUTIONS LIMITED</h2></div>
		 <div class="investigation-01">
            <div class="color-switches trading-swithch2">
               <button type="button" name="service_fil[]" id="vat" class="btn btn-sm btn-toggle" data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>VAT</label>
            </div>
            <div class="color-switches trading-swithch3">
               <button type="button"  name="service_fil[]" id="payroll" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Payroll</label>
            </div>
            <div class="color-switches trading-swithch4">
               <button type="button"  name="service_fil[]" id="accounts" class="btn btn-sm  btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Accounts</label>
            </div>
            <div class="color-switches trading-swithch5">
               <button type="button"  name="service_fil[]" id="conf_statement" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Confirmation Statement</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="company_tax_return"  class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Company Tax Return</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="personal_tax_return" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Personal Tax Return</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="workplace" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>WorkPlace Pension - AE</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="cis" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>CIS - Contractor</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="cissub" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>CIS - Sub Contractor</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="p11d" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>P11D</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="bookkeep" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Bookkeeping</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="management" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Management Accounts</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="investgate" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Investigation Insurance</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="registered" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Registered Address</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="taxadvice" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Tax Advice</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="taxinvest" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Tax Investigation</label>
            </div>
			</div>
         </div>
      </div>
   
   
      <div class="client_section3 table-responsive floating_set filterrec">
         <div class="service_succ"></div>
         <table class="table client_table1 text-center " id="servicedead">
            <thead>
               <tr class="text-uppercase">
                  <th>Client Name</th>
                  <th>Client Type</th>
                  <th>VAT</th>
                  <th>Payroll</th>
                  <th>Accounts</th>
                  <th>Confirmation statement</th>
                  <th>Company Tax Return</th>
                  <th>Personal Tax Return</th>
                  <th>WorkPlace Pension - AE</th>
                  <th>CIS - Contractor</th>
                  <th>CIS - Sub Contractor</th>
                  <th>P11D</th>
                  <th>Bookkeeping</th>
                  <th>Management Accounts</th>
                  <th>Investigation Insurance</th>
                  <th>Registered Address</th>
                  <th>Tax Advice</th>
                  <th>Tax Investigation</th>
               </tr>
            </thead>
            <tbody class="user-dashboard-section1">
               <?php 
                  foreach ($getCompany as $getCompanykey => $getCompanyvalue) {
                  /* echo '<pre>';
                  print_r($getCompanyvalue);*/
                      //$getusername = $this->Common_mdl->sel
                      $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                  
                  /*(isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                  ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                  
                  (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                  ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                  
                  (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                  ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                  
                  (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                  ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                  
                  (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                  ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                  
                  (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxadvice =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxadvice = '';
                  ($jsntaxadvice!='') ? $taxadvice = "checked='checked'" : $taxadvice = "";*/
                  
                  /*echo '<pre>';
                  print_r($getCompanyvalue);die;*/
                  
                  (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                  ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                  
                  (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                  ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                  
                  (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                  ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                  
                  (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                  ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                  
                  (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                  ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                  
                  (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                  ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                  
                  (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                  ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                  
                  (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                  ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                  
                  (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                  ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                  
                  (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                  ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                  
                  (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                  ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                  
                  (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                  ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                  
                  (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                  ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                  
                  (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                  ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                  
                  (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                  ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                  
                  (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                  ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                   ?>
               <tr>
                  <td><?php echo $getusername['crm_name'];?></td>
                  <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                         <input type="checkbox" name="vat" id="vat_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $vat;?> class="update_service"> <label for="vat_<?php echo $getCompanyvalue['id'];?>"> </label> 
                     </div>
					 

                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="payroll" id="payroll_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $pay;?> class="update_service">
						<label for="payroll_<?php echo $getCompanyvalue['id'];?>"> 
						</label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                         <input type="checkbox" value="" name="accounts" id="accounts_<?php echo $getCompanyvalue['id'];?>"  <?php echo $acc;?> class="update_service">
						 <label for="accounts_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                         <input type="checkbox" name="conf_statement" id="confstatement_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $cst;?> class="update_service"> 
						 <label for="confstatement_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="company_tax" id="companytax_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $tax;?> class="update_service"> 
						<label for="companytax_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" value="" name="personal_tax" id="personaltax_<?php echo $getCompanyvalue['id'];?>" <?php echo $p_tax;?> class="update_service"> 
						<label for="personaltax_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="workplace" id="workplace_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $workplace;?> class="update_service"> 
						<label for="workplace_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <label> <input type="checkbox" name="cis" id="cis_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $cis;?> class="update_service"> 
						<label for="cis_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                         <input type="checkbox" name="cissub" id="cissub_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $cissub;?> class="update_service">
						<label for="cissub_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" namne="p11d" id="p11d_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $p11d;?> class="update_service"> 
						<label for="p11d_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                         <input type="checkbox" name="bookkeep" id="bookkeep_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $bookkeep;?> class="update_service">
						<label for="bookkeep_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                         <input type="checkbox" name="management" id="management_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $management;?> class="update_service">  
						 <label for="management_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="investgate" id="investgate_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $investgate;?> class="update_service"><label for="investgate_<?php echo $getCompanyvalue['id'];?>">  </label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="registered" id="registered_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $registered;?> class="update_service"> 
						<label for="registered_<?php echo $getCompanyvalue['id'];?>"> </label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="taxadvice" id="taxadvice_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $taxadvice;?> class="update_service"> <label for="taxadvice_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="taxinvest" id="taxinvest_<?php echo $getCompanyvalue['id'];?>" value="taxinvest" <?php echo $taxinvest;?> class="update_service">  
						<label for="taxinvest_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
               </tr>
               <?php } ?>
            </tbody>
         </table>
      </div>
	</div>
   
   <div class="fade-propfile_updates floating_set">
      <div class="client_section3 table-responsive floating_set">
         <div class="status_succ"></div>
         <table class="table client_table1 text-center " id="newactives">
            <thead>
               <tr class="text-uppercase">
                  <th>Profile</th>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Active</th>
                  <!-- <th>Status</th> -->
                  <th>User Type</th>
                  <th>Actions</th>
               </tr>
            </thead>
            <tbody>
               <?php 
                  /*echo '<pre>';
                  print_r($create_clients_list);*/
                  foreach ($create_clients_list as $user => $userlist) {
                  //print_r($userlist);
                  
                          $role_name=$this->Common_mdl->select_record('Role','id',$userlist['role']);
                          $getUserProfilepic = $this->Common_mdl->getUserProfilepic($userlist['id']);
                              if($userlist['role']=='6'){
                                  $url = base_url().'user/view_staff/'.$userlist['id'];
                              }else{
                                  $url = base_url().'Client/client_info/'.$userlist['id'];
                              }
                  ?>
               <tr>
                  <td class="user_imgs sorting_1" id="img_<?php echo $userlist['id'];?>"><img src="<?php echo $getUserProfilepic;?>"></td>
                  <td><?php echo $userlist['crm_name'];?></td>
                  <td><?php echo $userlist['username'];?></td>
                  <td>
                     <select name="status<?php echo $userlist['id'];?>" id="status<?php echo $userlist['id'];?>" class="status" data-id="<?php echo $userlist['id'];?>">
                        <option value="1" <?php if($userlist['status']=='1'){?> selected="selected"<?php } ?>>Active</option>
                        <option value="2" <?php if($userlist['status']=='2'){?> selected="selected"<?php } ?>>Inactive</option>
                        <option value="3" <?php if($userlist['status']=='3'){?> selected="selected"<?php } ?>>frozen</option>
                     </select>
                  </td>
                  <!-- <td>
                     <p class=""><?php echo ucfirst($userlist['crm_firm_status']);?></p>
                     </td> -->
                  <td><?php echo $role_name['role'];?></td>
                  <td>
                     <p class="action_01">
                        <a href="<?php echo base_url();?>staff/delete/<?php echo $userlist['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a>
                        <a href="<?php echo $url;?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                        <?php if($userlist['role']=='6'){?>
                        <a href="<?php echo base_url();?>staff/staff_report/<?php echo $userlist['id'];?>"><i class="fa fa-download fa-6" aria-hidden="true"></i></a>
                        <?php } ?>
                     </p>
                  </td>
               </tr>
               <?php } ?>
            </tbody>
         </table>
      </div>
   </div>
   <?php } ?>
</div>
<!-- title_page01 -->    
<input type="hidden" id="h_f" name="h_f" value="">
<?php $this->load->view('includes/footer');?>
<script src="<?php echo  base_url()?>assets/js/jquery.lineProgressbar.js"></script>
<script src="<?php echo  base_url()?>assets/js/c3-custom-chart.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script> 
<script src="https://rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/demo/MonthPicker.min.js"></script>
<script type="text/javascript" language="javascript" src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   $('#all_activity1').DataTable();
   
   
   });
   
   $(document).ready(function(){
   //  alert('hi');
   $('#month_pic').MonthPicker({ Button: false });
   
   
   });
   
   
   
   
</script>
<!-- <script src="<?php echo base_url();?>assets/js/jquery.plugin.js"></script>
   <script src="<?php echo base_url();?>assets/js/jquery.countdown.js"></script>
   <script>
   $(function () {
   	var austDay = new Date();
   	austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 26);
   	$('#defaultCountdown').countdown({until: austDay});
   	$('#year').text(austDay.getFullYear());
   });
   </script> -->
<script type="text/javascript"> 
   window.onload = function() { 
   	$("#chartContainer").CanvasJSChart({ 
   		title: { 
   			fontSize: 24
   		}, 
   		axisY: { 
   			title: "Products in %" 
   		}, 
   		data: [ 
   		{ 
   			type: "pie", 
   			toolTipContent: "{label} <br/> {y} %", 
   			indexLabel: "{y} %", 
   			dataPoints: [ 
   				{ label: "Low",  y: <?php echo $low;?>, legendText: "Low"}, 
   				{ label: "High",    y: <?php echo $high;?>, legendText: "High"  }, 
   				{ label: "Medium",   y: <?php echo $medium;?>,  legendText: "Medium" }, 
   				{ label: "Super urgent",       y: <?php echo $super_urgent;?>,  legendText: "Super urgent"}
   			] 
   		} 
   		] 
   	}); 
   	
   	$("#chartContainer1").CanvasJSChart({ 
   		title: { 
   			fontSize: 24
   		}, 
   		axisY: { 
   			title: "Products in %" 
   		}, 
   		data: [ 
   		{ 
   			type: "pie", 
   			toolTipContent: "{label} <br/> {y} %", 
   			indexLabel: "{y} %", 
   			dataPoints: [ 
   				{ label: "Open",  y: 30.3, legendText: "Open"}, 
   				{ label: "Closed",    y: 19.1, legendText: "Closed"  }, 
   				
   			] 
   		} 
   		] 
   	}); 
   
   
   
   
   
   
   
   } 
</script> 
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   
      $( "#update_bank" ).validate({
   
        rules: {
          bank_name: "required",  
          branch_name: "required",  
          account_name: "required",  
          account_no: "required",  
        },
        messages: {
          bank_name: "Please enter your Bank Name",
          branch_name: "Please enter your Branch Name",
          account_name: "Please enter your Account Name",
          account_no: "Please enter your Account Number",
        },
        
      });
   
      $("#go").click(function(){
        var month = $("#month_pic").val();
   
         $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>staff/time_card/",
            data: {month:month},
            success: function(response) {
            $("#timecard_filter").html(response);
            },
          });
   
   
      });
   });
   
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
   $("#newactives").dataTable({
   "iDisplayLength": 10
   });
   
   $("#servicedead").dataTable({
   "iDisplayLength": 10 ,
   "scrollX": true
   
   });
   
   
   
   
   
   
    $(".status").change(function(e){
     //$('#alluser').on('change','.status',function () {
     //e.preventDefault();
     
     
      var rec_id = $(this).data('id');
      var stat = $(this).val();
     
     $.ajax({
           url: '<?php echo base_url();?>user/statusChange/',
           type: 'post',
           data: { 'rec_id':rec_id,'status':stat },
           timeout: 3000,
           success: function( data ){
               //alert('ggg');
               $(".status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('.status_succ'); });
              // setTimeout(resetAll,3000);
               //location.reload();
               setTimeout(function(){// wait for 5 secs(2)
              //location.reload(); // then reload the page.(3)
              $(".status_succ").hide();
         }, 3000);
               if(stat=='3'){
                   
                    //$this.closest('td').next('td').html('Active');
                    $('#frozen'+rec_id).html('Frozen');
     
               } else {
                    $this.closest('td').next('td').html('Inactive');
               }
               },
               error: function( errorThrown ){
                   console.log( errorThrown );
               }
           });
      });
   
   
   $(".search").change(function(){
           $(".LoadingImage").show();
   
       var data = {};
   
      data['com_num'] = $("#companysearch").val();
      data['legal_form'] = $("#legal_form").val();
      //var values = $("button[name='service_fil[]']").map(function(){return $(this).attr("aria-pressed");}).get();
      var f_h = $("#h_f").val();
     // alert(f_h);
      if(f_h)
      {
       var fields = $("button[id='"+f_h+"'").attr('id');
       var values = $("button[id='"+f_h+"'").attr("aria-pressed");
      }else{
       var fields ='';
       var values ='';
      }
     
      data['values'] = '';
      data['fields'] = '';
      $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>staff/companysearch/",
               data: data,
               success: function(response) {
                       $(".LoadingImage").hide();
   
               $(".filterrec").html(response);
               $("#servicedead").dataTable({
               "iDisplayLength": 10,
                   "dom": '<"staffprofilelist01">lfrtip'
               });
               },
       });
   });
   
   
   $(document).on('click',".update_service",function(){
       var values = this.id;
   
       if($(this). prop("checked") == true){
       var stat = "on";
       }
       else if($(this). prop("checked") == false){
       var stat = "off";
       }
      $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>staff/service_update/",
               data: {'values':values,'stat':stat},
               success: function(response) {
                   $(".service_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('.service_succ'); });
              /* $(".filterrec").html(response);
               $("#servicedead").dataTable({
               "iDisplayLength": 10,
                   "dom": '<"staffprofilelist01">lfrtip'
               });*/
               },
       });
       
       /* var notChecked = [], checked = [];
       $(":checkbox").map(function() {
           this.checked ? checked.push(this.id) : notChecked.push(this.id);
       });
       alert("checked: " + checked);
       alert("not checked: " + notChecked);*/
   });
   
   //$(".btn-sm").click(function(){
       $(document).on('click',".btn-sm",function(){
      //$(this).attr("aria-pressed")
     $(".LoadingImage").show();
        //var values = $("button[name='service_fil[]']").map(function(){return $(this).attr("aria-pressed");}).get();
        var fields = $(this).attr('id');
        var values = $(this).attr("aria-pressed");
        $("#h_f").val(fields);
      // alert(values);
    var data = {};
   
      data['com_num'] = $("#companysearch").val();
      data['legal_form'] = $("#legal_form").val();
      /*data['com_num'] = '';
      data['legal_form'] = '';*/
      data['values'] = values;
      data['fields'] = fields;
      $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>staff/companysearch/",
               data: data,
               success: function(response) {
                     $(".LoadingImage").hide();
               $(".filterrec").html(response);
               $("#servicedead").dataTable({
               "iDisplayLength": 10,
                   "dom": '<"staffprofilelist01">lfrtip'
               });
               },
       });
   
   });
   
   });
</script>