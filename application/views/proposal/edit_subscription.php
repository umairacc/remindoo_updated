<input type="hidden" name="id" id="edit_subscription_id" value="<?php if(isset($sub['id'])){ echo $sub['id']; } ?>">
<div class="unit-annual floating_set">
   <div class="common-annual lead-annual">
      <div class="annual-field2 add-input-service">
         <label>Name</label>     
         <input type="text" name="subscription_name" id="edit_subscription_name" value="<?php if(isset($sub['subscription_name'])){
            echo $sub['subscription_name'];
            } ?>">
      </div>
      <div class="annual-field3 add-input-service"> 
         <label>Price </label>    
         <input type="text" name="subscription_price" id="subscription_price" value="<?php echo $sub['subscription_price']; ?>">
      </div>
      <div class="annual-field3 add-input-service">
      <label>Unit</label>
      <select name="subscription_unit" id="subscription_unit">
         <option value="per_hour" <?php if(isset($sub['subscription_unit'])){ if($sub['subscription_unit']=='per_hour'){ ?> selected="selected" <?php }} ?>>Per Hour</option>                               
               <option value="per_day" <?php if(isset($sub['subscription_unit'])){ if($sub['subscription_unit']=='per_day'){ ?> selected="selected" <?php }} ?>>Per Day</option>
               <option value="per_month" <?php if(isset($sub['subscription_unit'])){ if($sub['subscription_unit']=='per_month'){ ?> selected="selected" <?php }} ?>>Per Month</option>
               <option value="per_2month" <?php if(isset($sub['subscription_unit'])){ if($sub['subscription_unit']=='per_2month'){ ?> selected="selected" <?php }} ?>>Per 2Months</option>
               <option value="per_6month" <?php if(isset($sub['subscription_unit'])){ if($sub['subscription_unit']=='per_6month'){ ?> selected="selected" <?php }} ?>>Per 6Month</option>
               <option value="per_year" <?php if(isset($sub['subscription_unit'])){ if($sub['subscription_unit']=='per_year'){ ?> selected="selected" <?php }} ?>>Per Year</option>
      </select>
   </div>
      <div class=" spacing-annual  add-input-service">
         <label>Description</label>
         <textarea rows="3" name="subscription_description" id="subscription_description"><?php echo $sub['subscription_description']; ?></textarea>
      </div>
      <div class="spacing-annual accotax-litd send-to">
         <label>Choose a category</label>
        <div class="subscription_category_add">
         <select name="subscription_category" id="subscription_category">
         <option value="">Select </option>
         <?php
         if(count($category)>0){
         foreach($category as $cat){ 
            if($cat['category_type']=='subscription'){?>
            <option value="<?php echo $cat['id']; ?>" <?php if(isset($sub['subscription_category'])){ if($sub['subscription_category_id']==$cat['id']){ ?> selected="selected" <?php }} ?>><?php echo $cat['category_name']; ?></option>
       <?php   }} }?>
         </select>
         </div>
         <a href="javascript:;" data-toggle="modal" data-target="#add-new-category_subscription"><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add a new Category</a>
      </div>
   </div>
</div>