


</style>

<html lang="en">
   <head>
      <title>CRM </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="description" content="#">
      <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
      <meta name="author" content="#">
      <!-- Favicon icon -->
      <link rel="icon" href="http://remindoo.org/CRMTool/assets/images/favicon.ico" type="image/x-icon">
      <!-- Google font-->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
      <!-- Required Fremwork -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/bootstrap/css/bootstrap.min.css">
      <!-- themify-icons line icon -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/icon/themify-icons/themify-icons.css">
      <!-- ico font -->
      <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/icon/icofont/css/icofont.css">
      <!-- flag icon framework css -->
      <!-- Menu-Search css -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/menu-search/css/component.css">
      <!-- jpro forms css -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/j-pro/css/demo.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/j-pro/css/font-awesome.min.css">
      <!--  <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/j-pro/css/j-pro-modern.css"> -->
      <!-- Style.css -->
      <link type="text/css" href="http://remindoo.org/CRMTool/bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
      <!-- Date-range picker css  -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/datedropper/css/datedropper.min.css" />
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <link rel="stylesheet" href="https:/resources/demos/style.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/switchery/css/switchery.min.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/jquery.mCustomScrollbar.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/owl.carousel/css/owl.carousel.css">
      <!--     <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/dataTables.bootstrap4.min.css">
         <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/buttons.dataTables.min.css?ver=2">
         <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/responsive.bootstrap4.min.css"> -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/ui_style.css?ver=3">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/common_style.css?ver=4">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/common_responsive.css?ver=2">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool//assets/css/dataTables.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool//assets/css/jquery.dropdown.css">
      <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="https://bootswatch.com/superhero/bootstrap.min.css">
      <link href="http://remindoo.org/CRMTool/css/jquery.signaturepad.css" rel="stylesheet">
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
      <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/css/bootstrap-colorpicker.min.css">
      <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/css/bootstrap-slider.min.css">
      <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/plugins/medium-editor/medium-editor.min.css">
      <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/plugins/medium-editor/template.min.css">
      <!-- <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/css/style1.css"> -->
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool//assets/icon/simple-line-icons/css/simple-line-icons.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool//assets/icon/icofont/css/icofont.css">
       <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/responsive.dataTables.css">
   <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/buttons.dataTables.min.css?ver=2">
   <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/progress-circle.css">
<style type="text/css">
   .done_field{
      display: none;
   }
   * {
    box-sizing: border-box;
}

.columns {
    float: left;
    width: 33.3%;
    padding: 8px;
}

.price {
    list-style-type: none;
    border: 1px solid #eee;
    margin: 0;
    padding: 0;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.price:hover {
    box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
}

.price .header {
    background-color: #111;
    color: white;
    font-size: 25px;
}

.price li {
    border-bottom: 1px solid #eee;
    padding: 20px;
    text-align: center;
}

.price .grey {
    background-color: #eee;
    font-size: 20px;
}

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
    font-size: 18px;
}

@media only screen and (max-width: 600px) {
    .columns {
        width: 100%;
    }
}

         .common_form_section1  .col-xs-12.col-sm-6.col-md-6 {
         float: left !important;
         }
         .common_form_section1 {
         background: #fff;
         box-shadow: 1px 2px 5px #dbdbdb;
         border-radius: 5px;
         padding: 30px;
         }
         .common_form_section1 label{
         display: block;
         margin-bottom: 7px;
         font-size: 15px;
         }
         .common_form_section1 input{
         width: 100%;
         }
         .col-sm-12{
         float: left;
         }
         .col-sm-8 {
         float: left;
         }
         .pcoded-content.card12.pre-pro {
            float: left;
            width: 100%;
        }
        .pcoded-content.card12.pre-pro table {
            display: table;
        }
        .pcoded-content.card12.pre-pro {
            background: #F6F7FB;
        }
        .modal-backdrop.show {
          opacity: 1;
      }
        
      </style>
   </head>
   <div class="LoadingImage"></div>
   <style>
      .LoadingImage {
      display : none;
      position : fixed;
      z-index: 100;
      background-image : url('http://remindoo.org/CRMTool/assets/images/ajax-loader.gif');
      background-color:#666;
      opacity : 0.4;
      background-repeat : no-repeat;
      background-position : center;
      left : 0;
      bottom : 0;
      right : 0;
      top : 0;
      }
      .pcoded-content.card12.pre-pro {
    min-height: 100vh;
}
   </style>
   <div class="theme-loader">
      <div class="ball-scale">
         <div class='contain'>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
         </div>
      </div>
   </div>

     <div class="pcoded-content card12 pre-pro">
      <div class="pcoded-inner-content">
         <input type="hidden" name="password"  id="password" value="<?php //echo $records['password']; ?>">
         <!-- Main-body start -->
         <div class="main-body">
            <div class="page-wrapper">
               <div class="deadline-crm1 floating_set">
                <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                  <li class="nav-item"><a class="nav-link">Terms and Conditions</a></li>
                </ul>
                <!--   <a href="<?php echo base_url(); ?>proposal/proposal_feedback/<?php echo $records['id']; ?>" class="qus-ans btn btn-sm btn-primary f-right">Question? Discuss Now</a> -->
               </div>
               <!-- Page body start -->
               <div class="page-body task-d">
                  <div class="row">
                     <div class="card col-xs-12">
                   
                           <div id="proposal"  style="margin-left: 36px;">

                           <?php echo $content['terms_content']; ?>

                           </div>
                       
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/timezones.full.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>  
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>  
   <!-- j-pro js -->
   <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script> -->
   <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
   <!-- jquery slimscroll js -->
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
   <!-- modernizr js -->
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
   <!--  
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script> -->
   <!-- <script src="<?php echo base_url();?>assets/js/mmc-common.js"></script>
      <script src="<?php echo base_url();?>assets/js/mmc-chat.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/chat.js"></script> -->
   <!-- Custom js -->
   <script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/masonry.pkgd.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script> 
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
   <script src="<?php echo base_url();?>assets/js/mock.js"></script>
   <!-- <script src="https://use.fontawesome.com/86c8941095.js"></script> -->
   <?php $url = $this->uri->segment('2');?>
   <?php if ($url == 'step_proposal' || $url == 'template_design' || $url=='templates') { ?>
   <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
   <?php } ?>
   <script src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/debounce.js"></script>
   <script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
   <!--   <script src="<?php //echo base_url();?>assets/js/bootstrap-slider.min.js"></script> -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
   <script src="//cdn.jsdelivr.net/medium-editor/latest/js/medium-editor.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/creative.tools.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/html2canvas.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/image-edit.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/editor.js"></script>


    <script src="<?php echo base_url(); ?>assets/signature/js/numeric-1.2.6.min.js"></script> 
    <script src="<?php echo base_url(); ?>assets/signature/js/bezier.js"></script>
    <script src="<?php echo base_url(); ?>assets/signature/js/jquery.signaturepad.js"></script>     
    <script type='text/javascript' src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
    <script src="<?php echo base_url(); ?>assets/signature/js/json2.min.js"></script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.number.min.js"></script>
    </html>