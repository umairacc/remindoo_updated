<div class="unit-annual floating_set">
   <div class="common-annual lead-annual">
      <div class="annual-field2 add-input-service">
         <label>Name</label>     
         <input type="text" name="subscription_name">
      </div>
      <div class="annual-field3 add-input-service"> 
         <label>Price </label>    
         <input type="text" name="subscription_price">
      </div>
      <div class="annual-field3 add-input-service"> 
         <label>Periods</label>    
         <select name="subscription_unit">
         <option value="per_hour">Per Hour</option>
         <option value="per_day">Per Day</option>
         <option value="per_week">Per Week</option>
         <option value="per_month">Per Month</option>
         <option value="per_2month">Per 2Month</option>
         <option value="per_5month">Per 5month</option>
         <option value="per_year">Per Year</option>
         </select>
      </div>
      <div class=" spacing-annual  add-input-service">
         <label>Description</label>
         <textarea rows="3" id="desc-add-subscription" name="subscription_description"></textarea>
      </div>
      <div class="spacing-annual accotax-litd send-to">
         <label>Choose a category</label>
        <div class="subscription_category_add">
         <select name="subscription_category" id="subscription_category">
         <option value="">Select </option>
         <?php
         if(count($category)>0){
         foreach($category as $cat){
         if($cat['category_type']=='subscription'){ ?>
            <option value="<?php echo $cat['id']; ?>"><?php echo $cat['category_name']; ?></option>
       <?php   } } }?>
         </select>
         </div>
         <a href="javascript:;" data-toggle="modal" data-target="#add-new-category_subscription"><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add a new Category</a>
      </div>
   </div>
</div>