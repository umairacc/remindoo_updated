
<script>
		CKEDITOR.config.toolbar = [
		['Styles','Format','Font','FontSize'],
		'/',
		['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
		'/',
		['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Image','Table','-','Link','Flash','Smiley','TextColor','BGColor','Source']
		] ;
	
		'use strict';

		var CONTACTS2 = [
			{ name: 'ClientName' },
			{ name: 'ClientCompany' },
			{ name: ' ClientCompanyCity' },
			{ name: 'ClientCompanyCountry' },
			{ name: 'ClientCompanyReligion' },
			{ name: ' ClientFirstName'},
			{ name: ' ClientLastName' },
			{ name: 'ClientName' },
			{ name: 'ClientPhoneNo' },
			{ name: 'ClientWebsite' },
			{ name: 'ClientProposallink' },
			{ name: 'ProposalCurrency' },
			{ name: 'ProposalExpirationDate' },
			{ name: 'ProposalName' },
			{ name: 'ProposalCodeNumber' },
			{ name:'SenderCompany'},
			{ name:'SenderCompanyAddress'},
			{ name:'SenderCompanyCity'},
			{ name:'SenderCompanyCountry'},
			{ name:'SenderCompanyReligion'},
			{ name:'SenderEmail'},
			{ name:'SenderEmailSignature'},
			{ name:'SenderName'},
			{ name:'senderPhoneno'},
			{ name:'ProposalTitle'}
		];

		CKEDITOR.disableAutoInline = false;
		CKEDITOR.plugins.add( 'hcard2', {
			requires: 'widget',
			init: function( editor ) {
				editor.widgets.add( 'hcard2', {
					allowedContent: 'span(!h-card2); a[href](!u-email,!p-name); span(!p-tel)',
					requiredContent: 'span(h-card2)',
					pathName: 'hcard2',

					upcast: function( el ) {
						return el.name == 'span' && el.hasClass( 'h-card2' );
					}
				} );
				editor.addFeature( editor.widgets.registered.hcard2 );
				editor.on( 'paste', function( evt ) {
					var contact = evt.data.dataTransfer.getData( 'contact2' );
					if ( !contact ) {
						return;
					}
					evt.data.dataValue =						
						'<span class="h-card2">' +
							'::'+contact.name+'::'  +
						'</span>';
				} );
			}
		} );

		CKEDITOR.on( 'instanceReady', function() {
			
			CKEDITOR.document.getById( 'contactList2' ).on( 'dragstart', function( evt ) {
				
				var target = evt.data.getTarget().getAscendant( 'div', true );

				CKEDITOR.plugins.clipboard.initDragDataTransfer( evt );

				var dataTransfer = evt.data.dataTransfer;				
				dataTransfer.setData( 'contact2', CONTACTS2[ target.data( 'contact2' ) ] );		
				dataTransfer.setData( 'text/html', target.getText() );
				
				if ( dataTransfer.$.setDragImage ) {
					dataTransfer.$.setDragImage( target.findOne( 'img' ).$, 0, 0 );
				}
			} );
		} );

		// Initialize the editor with the hcard plugin.
		CKEDITOR.inline( 'editor3', {
			extraPlugins: 'hcard2,sourcedialog,justify'
		} );

		CKEDITOR.inline( 'editor4', {
			extraPlugins: 'hcard2,sourcedialog,justify'
		} );



		$("#add_proposal").click(function(){
			var body1=$("#editor4").html();
			var  subject1=$("#editor3").html();
			var  title=$("#title").val();

			var str = "";
			$('.cke_widget_wrapper > span.h-card2').each(function(){
				str += $(this).html();				
			});	

			$("span.cke_reset").each(function () {
			$(this).remove();
			});

			$("span.cke_widget_wrapper").each(function () {
			$(this).replaceWith($(this).text());
			});
			$('div[data-cke-hidden-sel="1"]').remove();

				
				var body=$("#editor4").html();
				var subject=$("#editor3").html();			
				var formData={'body':body,'title':title,'subject':subject};				
			 	$.ajax({
	                   url: '<?php echo base_url();?>proposal_page/insert_proposal/',
	                   type : 'POST',
	                   data : formData,	                  
				       beforeSend: function() {
			              $(".LoadingImage").show();
			           },
			           success: function(message) {
			           	$(".LoadingImage").hide();
			           		if(message=='0'){
			           			//$('.alert-danger').show();
			           		}else{
			           			$('.alert-success').show();
				              setTimeout(function() { 
				              	$('.alert-success').hide();

				              	$("#addEmailTemplate").modal('hide');
				              	// location.reload();  
	                              }, 3000);			              
			          		}
			           }

                  });
			
		});


		$("#title").change(function(){
			var  title=this.value;
			if(title != '')
			{
				$.ajax({
	                   url: '<?php echo base_url();?>proposal_page/select_proposal/',
	                   type : 'POST',
	                   data : {'title':title},	                  
				       beforeSend: function() {
			              $(".LoadingImage").show();
			           },
			           success: function(msg) {
			           	$(".LoadingImage").hide();			           	
						var json = JSON.parse(msg); 
						subject=json['subject'];
						body=json['body'];			
						$("#editor4").html('');
						$("#editor4").append(body);
						 $("#editor4").attr('contenteditable','true');
						$("#editor3").html('');
						$("#editor3").append(subject);
						 $("#editor3").attr('contenteditable','true');
			           }

                  });
			}
			 
			 
		});     
      

</script>