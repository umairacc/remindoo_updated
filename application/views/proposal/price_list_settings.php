<?php 
error_reporting('0');
$this->load->view('includes/header');


?>
<style type="text/css">
 .dropdown-main input {

    position: relative;
    opacity: 1;
  }

</style>

<div class="pcoded-content">
  <div class="pcoded-inner-content">
    <!-- Main-body start
      <div class="main-body">
        <div class="page-wrapper">
          <!-Page body start -->
    <div class="page-body">
      <div class="row">
        <div class="col-sm-12 proposal-settings-wrapper">
            <div class="graybgclsnew button_visibility" style="margin:0;">
              <div class="deadline-crm1 floating_set">                   
              <?php $this->load->view('proposal/proposal_navigation_tabs'); ?>
              </div>
            </div>
          <div class="client-details-view hidden-user01 floating_set card-removes priceli price_setting04">
          <div class="right-side-proposal">


          <?php 

          if($records['id']==''){ ?>
          <form name="form" action="<?php echo base_url(); ?>proposals/insert_pricelist" method="post">
          <?php }else{ ?>
          <form name="form" action="<?php echo base_url(); ?>proposals/update_pricelist" method="post">
          <input type="hidden" name="id" id="id" value="<?php echo $records['id']; ?>">
          <?php } ?>

          <div class="deadline-crm1 floating_set">
               <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:;">price list settings</a>
                     <div class="slide"></div>
                  </li>
               </ul>
            </div>

            <div class="document-center client-infom-1 floating_set pricelist">

          <div class="price-tb-setting form-radio">
              <h2>Pricing table settings</h2>
              <div class="discount-types">
                  <div class="line-items1">
                    <h3>Discount Type</h3>
                        <div class="radio radio-inline">
                        <label> <input type="radio" name="discount" id="line_discount" value="line_discount" <?php if(isset($records['discount_option']) && $records['discount_option']=='line_discount'){?> checked="checked" <?php } ?>> <i class="helper"></i>Line item discount </label>
                        </div>
                        <div class="radio radio-inline">
                        <label> <input type="radio" name="discount" value="total_discount"  <?php if(isset($records['discount_option'])){   if($records['discount_option']!='line_discount'){?> checked="checked" <?php }
                        }else{ ?> checked="checked"
                          <?php }?>> <i class="helper"></i>Total discount </label>
                        </div>
                    </div> 
                    
                     <div class="line-items1">
                    <h3>Tax type</h3>
                        <div class="radio radio-inline">
                        <label> <input type="radio" name="tax" id="line_tax" value="line_tax" <?php if(isset($records['tax_option']) && $records['tax_option']=='line_tax'){?> checked="checked" <?php } ?>> <i class="helper"></i>Line item tax </label>
                        </div>
                        <div class="radio radio-inline">
                        <label> <input type="radio" name="tax" value="total_tax"  <?php if(isset($records['tax_option'])){  if($records['tax_option']!='line_tax'){?> checked="checked" <?php } }else{ ?>  checked="checked" <?php } ?>
                         <?php ?>> <i class="helper" ></i>Total tax </label>
                        </div>
                    </div> 

                     <div class="line-items1 currency_update">
                      <h3>Currency</h3>

                       <?php     
                            $cur_symbols = $this->Common_mdl->getallrecords('crm_currency');

                            foreach ($cur_symbols as $key => $value) 
                            {
                               $currency_symbols[$value['country']] = $value['currency'];
                            }
                        ?>

                       <div class="dropdown-sin-2 currency_details">

                        <select name="currency_symbol" id="currency_symbol" class="">
                        <?php if(isset($records['currency'])){?>                           
                        <?php }else if($admin_settings['crm_currency']==''){ ?>
                        <option value="">Select Currency</option>
                        <?php  } ?>
                        <?php
                         $i=1;
                         foreach($currency_symbols as $key => $value) {  ?>
                        <option value="<?php echo $key; ?>" <?php if(isset($records['currency'])){ if($records['currency']==$key){ ?> Selected="selected" <?php } }else if($admin_settings['crm_currency']==$key){ ?>Selected="selected" <?php } ?> ><?php echo $key.'-'.$value; ?></option> 
                        <?php $i++; } ?>
                        </select>
                        </div>
                    </div>
                    <!-- <a href="javascript:;"  class="send-to" data-toggle="modal" data-target="#add_currency">
                    <i class="fa fa-plus fa-6" aria-hidden="true"></i>Add Curency</a> -->
                    </div> 
                </div>  <!-- 1sect -->

                 <div class="price-tb-setting" style="display:none;">
              <h2>Pricing Lists</h2> 
                <div class="discount-types">
                  <div class="line-items1">
                        <div class="pricing-radio" style="display: none;">
                        <label>Using Pricing List</label>
                        <div class="pricing_listing">
                        <select name="pricelist" onchange="pricelist_update(this)">
                        <option value="">Select</option>
                        <?php
                        foreach($pricelist as $price){ ?>
                        <option value="<?php echo $price['id'];?>" <?php if($records['pricelist']==$price['id']){ ?> selected="selected" <?php } ?>><?php echo $price['pricelist_name'];?></option>                       
                        <?php } ?>
                        </select>
                        </div>
                        </div>
                      <div class="pricing-radio">
                         <!--  <label>Add pricing list to catalog</label>
                        <a href="javascript:;" id="Price_list_catalog">Add to catalog</a> -->
                        <!--  -->
                        <input type="submit" name="submit" class="btn btn-primary" value="update">
                        </div>
                    </div> 
                   </div>
                </div> <!-- 2tab -->
                <div class="pricing-radio">
                         <!--  <label>Add pricing list to catalog</label>
                        <a href="javascript:;" id="Price_list_catalog">Add to catalog</a> -->
                        <!--  -->
                        <input type="submit" name="submit" class="btn btn-primary" value="update">
                        </div>
                    </div> 
                </div>
          </div><!-- right-side -->

</form>
</form>
</div>
</div>
</div>
</div>
</div>
</div>

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script type="text/javascript">
  $('.dropdown-sin-4').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

    $('.dropdown-sin-6').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

    $('.dropdown-sin-2').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });
     $('.dropdown-sin-3').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

  //  dropdown-sin-2
</script>
