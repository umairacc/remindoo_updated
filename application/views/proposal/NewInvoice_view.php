<?php $this->load->view('includes/header');
  $currency_symbols = array(
                          'AED' => '&#1583;.&#1573;', // ?
                          'AFN' => '&#65;&#102;',
                          'ALL' => '&#76;&#101;&#107;',
                          'AMD' => '',
                          'ANG' => '&#402;',
                          'AOA' => '&#75;&#122;', // ?
                          'ARS' => '&#36;',
                          'AUD' => '&#36;',
                          'AWG' => '&#402;',
                          'AZN' => '&#1084;&#1072;&#1085;',
                          'BAM' => '&#75;&#77;',
                          'BBD' => '&#36;',
                          'BDT' => '&#2547;', // ?
                          'BGN' => '&#1083;&#1074;',
                          'BHD' => '.&#1583;.&#1576;', // ?
                          'BIF' => '&#70;&#66;&#117;', // ?
                          'BMD' => '&#36;',
                          'BND' => '&#36;',
                          'BOB' => '&#36;&#98;',
                          'BRL' => '&#82;&#36;',
                          'BSD' => '&#36;',
                          'BTN' => '&#78;&#117;&#46;', // ?
                          'BWP' => '&#80;',
                          'BYR' => '&#112;&#46;',
                          'BZD' => '&#66;&#90;&#36;',
                          'CAD' => '&#36;',
                          'CDF' => '&#70;&#67;',
                          'CHF' => '&#67;&#72;&#70;',
                          'CLF' => '', // ?
                          'CLP' => '&#36;',
                          'CNY' => '&#165;',
                          'COP' => '&#36;',
                          'CRC' => '&#8353;',
                          'CUP' => '&#8396;',
                          'CVE' => '&#36;', // ?
                          'CZK' => '&#75;&#269;',
                          'DJF' => '&#70;&#100;&#106;', // ?
                          'DKK' => '&#107;&#114;',
                          'DOP' => '&#82;&#68;&#36;',
                          'DZD' => '&#1583;&#1580;', // ?
                          'EGP' => '&#163;',
                          'ETB' => '&#66;&#114;',
                          'EUR' => '&#8364;',
                          'FJD' => '&#36;',
                          'FKP' => '&#163;',
                          'GBP' => '&#163;',
                          'GEL' => '&#4314;', // ?
                          'GHS' => '&#162;',
                          'GIP' => '&#163;',
                          'GMD' => '&#68;', // ?
                          'GNF' => '&#70;&#71;', // ?
                          'GTQ' => '&#81;',
                          'GYD' => '&#36;',
                          'HKD' => '&#36;',
                          'HNL' => '&#76;',
                          'HRK' => '&#107;&#110;',
                          'HTG' => '&#71;', // ?
                          'HUF' => '&#70;&#116;',
                          'IDR' => '&#82;&#112;',
                          'ILS' => '&#8362;',
                          'INR' => '&#8377;',
                          'IQD' => '&#1593;.&#1583;', // ?
                          'IRR' => '&#65020;',
                          'ISK' => '&#107;&#114;',
                          'JEP' => '&#163;',
                          'JMD' => '&#74;&#36;',
                          'JOD' => '&#74;&#68;', // ?
                          'JPY' => '&#165;',
                          'KES' => '&#75;&#83;&#104;', // ?
                          'KGS' => '&#1083;&#1074;',
                          'KHR' => '&#6107;',
                          'KMF' => '&#67;&#70;', // ?
                          'KPW' => '&#8361;',
                          'KRW' => '&#8361;',
                          'KWD' => '&#1583;.&#1603;', // ?
                          'KYD' => '&#36;',
                          'KZT' => '&#1083;&#1074;',
                          'LAK' => '&#8365;',
                          'LBP' => '&#163;',
                          'LKR' => '&#8360;',
                          'LRD' => '&#36;',
                          'LSL' => '&#76;', // ?
                          'LTL' => '&#76;&#116;',
                          'LVL' => '&#76;&#115;',
                          'LYD' => '&#1604;.&#1583;', // ?
                          'MAD' => '&#1583;.&#1605;.', //?
                          'MDL' => '&#76;',
                          'MGA' => '&#65;&#114;', // ?
                          'MKD' => '&#1076;&#1077;&#1085;',
                          'MMK' => '&#75;',
                          'MNT' => '&#8366;',
                          'MOP' => '&#77;&#79;&#80;&#36;', // ?
                          'MRO' => '&#85;&#77;', // ?
                          'MUR' => '&#8360;', // ?
                          'MVR' => '.&#1923;', // ?
                          'MWK' => '&#77;&#75;',
                          'MXN' => '&#36;',
                          'MYR' => '&#82;&#77;',
                          'MZN' => '&#77;&#84;',
                          'NAD' => '&#36;',
                          'NGN' => '&#8358;',
                          'NIO' => '&#67;&#36;',
                          'NOK' => '&#107;&#114;',
                          'NPR' => '&#8360;',
                          'NZD' => '&#36;',
                          'OMR' => '&#65020;',
                          'PAB' => '&#66;&#47;&#46;',
                          'PEN' => '&#83;&#47;&#46;',
                          'PGK' => '&#75;', // ?
                          'PHP' => '&#8369;',
                          'PKR' => '&#8360;',
                          'PLN' => '&#122;&#322;',
                          'PYG' => '&#71;&#115;',
                          'QAR' => '&#65020;',
                          'RON' => '&#108;&#101;&#105;',
                          'RSD' => '&#1044;&#1080;&#1085;&#46;',
                          'RUB' => '&#1088;&#1091;&#1073;',
                          'RWF' => '&#1585;.&#1587;',
                          'SAR' => '&#65020;',
                          'SBD' => '&#36;',
                          'SCR' => '&#8360;',
                          'SDG' => '&#163;', // ?
                          'SEK' => '&#107;&#114;',
                          'SGD' => '&#36;',
                          'SHP' => '&#163;',
                          'SLL' => '&#76;&#101;', // ?
                          'SOS' => '&#83;',
                          'SRD' => '&#36;',
                          'STD' => '&#68;&#98;', // ?
                          'SVC' => '&#36;',
                          'SYP' => '&#163;',
                          'SZL' => '&#76;', // ?
                          'THB' => '&#3647;',
                          'TJS' => '&#84;&#74;&#83;', // ? TJS (guess)
                          'TMT' => '&#109;',
                          'TND' => '&#1583;.&#1578;',
                          'TOP' => '&#84;&#36;',
                          'TRY' => '&#8356;', // New Turkey Lira (old symbol used)
                          'TTD' => '&#36;',
                          'TWD' => '&#78;&#84;&#36;',
                          'TZS' => '',
                          'UAH' => '&#8372;',
                          'UGX' => '&#85;&#83;&#104;',
                          'USD' => '&#36;',
                          'UYU' => '&#36;&#85;',
                          'UZS' => '&#1083;&#1074;',
                          'VEF' => '&#66;&#115;',
                          'VND' => '&#8363;',
                          'VUV' => '&#86;&#84;',
                          'WST' => '&#87;&#83;&#36;',
                          'XAF' => '&#70;&#67;&#70;&#65;',
                          'XCD' => '&#36;',
                          'XDR' => '',
                          'XOF' => '',
                          'XPF' => '&#70;',
                          'YER' => '&#65020;',
                          'ZAR' => '&#82;',
                          'ZMK' => '&#90;&#75;', // ?
                          'ZWL' => '&#90;&#36;',
                        ); 

?>
  <?php
    if(isset($proposal_details)){ ?>
<style type="text/css">
  
  .dis{
    display: none;
  }

    .tax{
    display: none;
  }


  .inner-addon { 
    position: relative; 
}

/* style icon */
.inner-addon .glyphicon {
  position: absolute;
  padding: 10px;
  pointer-events: none;
}

/* align icon */
.left-addon .glyphicon  { left:  0px;}
.right-addon .glyphicon { right: 0px;}

/* add padding  */
.left-addon input  { padding-left:  30px; }
.right-addon input { padding-right: 30px; }
</style>
<?php } ?>


<!-- management block -->
<div id="print_preview">
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper new-invoice">
         <div class="deadline-crm1 floating_set">
            <h4>New Invoice</h4>
          </div>
        <?php 
          echo $this->session->flashdata('invoice_success');
        ?>  

            <div class="card firm-field">
              <form id="invoice_form" name="invoice_form" action="<?php echo base_url()?>invoice/CreateInvoice" method="post" enctype="multipart/form-data">
              <?php
                if(isset($proposal_details)){ ?>
                    <input type="hidden" name="draft_id" id="draft_id" value="<?php echo $proposal_details['id']; ?>">
                    <input type="hidden" name="proposal_id" id="proposal_id" value="<?php echo $proposal_details['proposal_id']; ?>">
                <?php } ?>  
               <div class="invoice-details">
               

                     <div class="invoice-top">
                        <span class="invoice-topinner dropdown-sin-5">
                           <label>to</label>
                        <select id="clint_email" name="clint_email" placeholder="Select User">  
                          <?php 
                          if(isset($proposal_details)){ 

                            $query1=$this->db->query("SELECT * FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$_SESSION['id']." order by id DESC");
                            $results1 = $query1->result_array();
                            $res=array();
                              foreach ($results1 as $key => $value) {
                                 array_push($res, $value['id']);
                              }  
                              $im_val=implode(',',$res);      

                              $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (".$im_val.") order by id desc ");    

                              $getallUser = $query->result_array();                   

                              foreach ($getallUser as $key => $value) { 

                                 $contact_mail=$this->db->query("select main_email from client_contacts where client_id=".$value['user_id']."")->row_array();
                              ?>                        
                                     <option value=<?php echo $value['user_id']; if($proposal_details['to_id']==$value['user_id']){ ?> selected="selected"  <?php } ?>  ><?php if($contact_mail['main_email'] != '') { echo $contact_mail['main_email']; } ?></option>
                              <?php } 
                            }else{  

                             $getallUser=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' order by id DESC")->result_array(); 
                           ?>
                                <option value="">All</option> <?php 
                               foreach ($getallUser as $key => $value) { 

                                $contact_mail=$this->db->query("select main_email from client_contacts where client_id=".$value['id']."")->row_array();
                            ?>                        
                                  
                              <option value=<?php echo $value['id'];?>><?php if($contact_mail['main_email'] != '') { echo $contact_mail['main_email']; } ?></option>
                            <?php  }
                          } ?> 
                        </select> 
                        </span>
                        <span class="invoice-topinner">
                           <label>date</label>
                           <input type="text" name="invoice_date" class="datepicker1" placeholder="dd-mm-yyyy" value="<?php if(isset($proposal_details)){  echo date('d-m-Y'); } ?>">
                        </span>
                        <span class="invoice-topinner form-group row date_birth">
                           <label>due date</label>
                           <!-- <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
                           <input type="text" name="due_date" class="form-control datepicker1 fields" placeholder="dd-mm-yyyy" required="required">
                        </span>
                        <span class="invoice-topinner">
                           <label>invoice</label>
                           <input type="text" name="invoice_no" readonly value="<?php if(isset($proposal_details)){ echo $proposal_details['invoice_no']; }else{ echo(mt_rand(100000,999999)); }?>" data-parsley-required="true">
                        </span>
                       <!--  <span class="invoice-topinner">
                           <label>reference</label>
                           <select name="reference" id="refered_by" class="fields" data-parsley-required="true" data-parsley-error-message="Reference is required" required="required">
                             <option value="">select</option>
                             <?php //foreach ($referby as $referbykey => $referbyvalue) {
                                # code...
                                 ?>
                             <option value="<?php //echo $referbyvalue['id'];?>" <?php //if(isset($client[0]['crm_refered_by']) && $client[0]['crm_refered_by']==$referbyvalue['user_id']) {?> selected="selected"<?php //} ?>><?php //echo $referbyvalue['crm_first_name'];?></option>
                             <?php //} ?>
                          </select>
                           
                        </span> -->
                        <span class="preview-pdf" id="preview-pdf">
                              <!-- <i class="fa fa-eye" aria-hidden="true"></i>
                              preview -->
                              <!-- <button type="button" data-toggle="modal" data-target="#temp_invoice" class="btn btn-card btn-primary" aria-hidden="true" id="preview_btn">
                              <i class="fa fa-eye fa-5"></i>
                              preview</button> -->
                              <a href="#"  data-toggle="modal" data-target="#temp_invoice" class="btn btn-card btn-primary" aria-hidden="true" id="preview_btn"><i class="fa fa-eye fa-5"></i>preview</a>
                        </span>
                     </div>
                     <div class="amounts-detail">
                        <label>amounts are</label>
                        <select name="amounts_details" class="select_tax" id="select_tax">
                           <option value="1">Tax Exclusive</option>
                           <option value="2">Tax Inclusive</option>

                        </select>
                     </div>
                     <div class="invoice-table">
                        <div class="table-responsive">
                           <table class="invoice_table" id="invoice_table">
                              <tr>
                                 <th></th>
                                 <th>item</th>
                                 <th>description</th>
                                 <th>qty</th>
                                 <th>unit price</th>
                                 <th class="discount_tr dis">disc%</th>
                                 <th>account</th>
                                 <th class="th_tax_rate tax td_tax_rate" id="th_tax_rate">tax rate</th>
                                 <th class="th_tax_amount" id="th_tax_amount">tax amount</th>
                                 <th>amount <?php  if(isset($proposal_details['currency'])){  echo $currency_symbols[$proposal_details['currency']];  }else{  if(isset($admin_settings['crm_currency'])){ echo $currency_symbols[$admin_settings['crm_currency']]; } }?></th>
                                 <th></th>
                              </tr> 
                        <?php
                          if(isset($proposal_details)){
                                  $item_name=explode(',',$proposal_details['item_name']);
                                  $price=explode(',',$proposal_details['price']);
                                  $qty=explode(',',$proposal_details['qty']);
                                  $tax=explode(',',$proposal_details['tax']);
                                  $discount=explode(',',$proposal_details['discount']);
                                  $description=explode(',',$proposal_details['description']);
                                  $product_name=explode(',',$proposal_details['product_name']);
                                  $product_price=explode(',',$proposal_details['product_price']);
                                  $product_qty=explode(',',$proposal_details['product_qty']);
                                  $product_description=explode(',',$proposal_details['product_description']);
                                  $product_tax=explode(',',$proposal_details['product_tax']);
                                  $product_discount=explode(',',$proposal_details['product_discount']);
                                  $subscription_name=explode(',',$proposal_details['subscription_name']);
                                  $subscription_price=explode(',',$proposal_details['subscription_price']);
                                  $subscription_unit=explode(',',$proposal_details['subscription_unit']);
                                  $subscription_description=explode(',',$proposal_details['subscription_description']);
                                  $subscription_tax=explode(',',$proposal_details['subscription_tax']);
                                  $subscription_discount=explode(',',$proposal_details['subscription_discount']);
                                  $k=1;  ?>  
                                    <input type="hidden"  name="" id="tax_option" value="<?php echo $proposal_details['tax_option']; ?>">
                                    <input type="hidden"  name="" id="discount_option" value="<?php echo $proposal_details['discount_option']; ?>">
                                     <input type="hidden"  name="" id="taxes_amount" value="<?php echo $proposal_details['tax_amount']; ?>">
                                    <input type="hidden"  name="" id="discountes_amount" value="<?php echo $proposal_details['discount_amount']; ?>">                           


                                <?php  for($i=0;$i<count($item_name);$i++){ 

                                    if($item_name[0]!=''){
                                    ?>                                

                                   <tr  id="row_id_<?php echo $k; ?>" class="top-fields">

                      <td><span id="sr_no"><?php echo $k; ?></span></td>

                      <td><input type="text" name="item_name[]" id="item_name<?php echo $k; ?>" class="form-control input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required" value="<?php echo $item_name[$i]; ?>"></td>

                      <td><textarea name="description[]" id="description<?php echo $k; ?>" data-srno="1" class="form-control input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"><?php echo $description[$i]; ?></textarea> </td>

                      <td><input type="number" name="quantity[]" id="quantity<?php echo $k; ?>" data-srno="1" class="form-control input-sm  quantity decimal_qty" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php echo $qty[$i]; ?>" onchange="qty_check()"></td>

                      <td><input type="text" name="unit_price[]" id="unit_price<?php echo $k; ?>" data-srno="1" class="form-control input-sm unit_price decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php echo $price[$i]; ?>" onchange="qty_check()" ></td>

                      <td class="discount_tr dis"><input type="text" name="discount[]" id="discount<?php echo $k; ?>" data-srno="1" class="form-control input-sm discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php if($proposal_details['discount_option']!='total_discount'){if($discount[$i]!=''){ echo $discount[$i]; }else{ echo '0'; }}else{ echo '0'; } ?>" onchange="qty_check()" ></td>

                      <td><input type="text" name="account[]" id="account<?php echo $k; ?>" data-srno="1" class="form-control input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>

                      <td class="td_tax_rate tax" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate<?php echo $k; ?>" data-srno="1" maxlength="4" class="form-control input-sm tax_rate decimal" data-parsley-required="true" data-parsley-type="number" value="<?php if($proposal_details['tax_option']!='total_tax'){ if($tax[$i]!=''){ echo $tax[$i]; }else{ echo '0';} }else{ echo '0'; } ?>" onchange="qty_check()"></td>

                      <td class="td_tax_amount " id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount<?php echo $k; ?>" data-srno="1" class="form-control input-sm tax_amount" readonly /></td>

                      <td><input type="text" name="amount_gbp[]" id="amount_gbp<?php echo $k; ?>" data-srno="1" class="form-control input-sm amount_gbp" readonly /></td>
                        <td><button type="button" name="remove_row" id="<?php echo $k; ?>" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td>
                      </tr>

                      <?php $k++; }}
                                $m=$k;
                                  for($i=0;$i<count($product_name);$i++){
                                    if($product_name[0]!=''){
                                  ?>
                                   <tr  id="row_id_<?php echo $m; ?>" class="top-fields">
                                   <td><span id="sr_no"><?php echo $m; ?></span></td>

                      <td><input type="text" name="item_name[]" id="item_name<?php echo $m; ?>" class="form-control input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required" value="<?php echo $product_name[$i]; ?>"></td>

                      <td><textarea name="description[]" id="description<?php echo $m; ?>" data-srno="1" class="form-control input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"><?php echo $product_description[$i]; ?></textarea> </td>

                      <td><input type="number" name="quantity[]" id="quantity<?php echo $m; ?>" data-srno="1" class="form-control input-sm  quantity decimal_qty" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php echo $product_qty[$i]; ?>" onchange="qty_check()"></td>

                      <td><input type="text" name="unit_price[]" id="unit_price<?php echo $m; ?>" data-srno="1" class="form-control input-sm unit_price decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php echo $product_price[$i]; ?>" onchange="qty_check()"/></td>

                      <td class="discount_tr dis"><input type="text" name="discount[]" id="discount<?php echo $m; ?>" data-srno="1" class="form-control input-sm discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php if($proposal_details['discount_option']!='total_discount'){ if($product_discount[$i]!=''){ echo $product_discount[$i]; }else{ echo '0'; } }else{ echo '0'; } ?>" onchange="qty_check()"></td>

                      <td><input type="text" name="account[]" id="account<?php echo $m; ?>" data-srno="1" class="form-control input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>

                      <td class="td_tax_rate tax" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate<?php echo $m; ?>" data-srno="1" maxlength="4" class="form-control input-sm tax_rate decimal" data-parsley-required="true" data-parsley-type="number" value="<?php if($proposal_details['tax_option']!='total_tax'){ if($product_tax[$i]!=''){ echo $product_tax[$i]; }else{ echo '0'; }}else{ echo '0';} ?>" onchange="qty_check()"></td>

                      <td class="td_tax_amount" id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount<?php echo $m; ?>" data-srno="1" class="form-control input-sm tax_amount" readonly /></td>

                      <td><input type="text" name="amount_gbp[]" id="amount_gbp<?php echo $m; ?>" data-srno="1" class="form-control input-sm amount_gbp" readonly /></td>
                        <td><button type="button" name="remove_row" id="<?php echo $m; ?>" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td>
                      </tr>


                      <?php $m++; }}

                      $n=$m;

                                  for($i=0;$i<count($subscription_name);$i++){ 
                                    if($subscription_name[0]!=''){
                                    ?>

                                   <tr  id="row_id_<?php echo $n; ?>" class="top-fields">
                                   <td><span id="sr_no"><?php echo $n; ?></span></td>

                      <td><input type="text" name="item_name[]" id="item_name<?php echo $n; ?>" class="form-control input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required" value="<?php echo $subscription_name[$i]; ?>"></td>

                      <td><textarea name="description[]" id="description<?php echo $n; ?>" data-srno="1" class="form-control input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"><?php echo $subscription_description[$i]; ?></textarea> </td>

                      <td><input type="number" name="quantity[]" id="quantity<?php echo $n; ?>" data-srno="1" class="form-control input-sm  quantity decimal_qty" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="1" onchange="qty_check()"></td>

                      <td><input type="text" name="unit_price[]" id="unit_price<?php echo $n; ?>" data-srno="1" class="form-control input-sm unit_price decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php echo $subscription_price[$i]; ?>" onchange="qty_check()"/></td>

                      <td class="discount_tr dis"><input type="text" name="discount[]" id="discount<?php echo $n; ?>" data-srno="1" class="form-control input-sm discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php  if($proposal_details['discount_option']!='total_discount'){ if($subscription_discount[$i]!=''){ echo $subscription_discount[$i]; }else{ echo'0'; } }else{ echo '0'; }?>" onchange="qty_check()" ></td>

                      <td><input type="text" name="account[]" id="account<?php echo $n; ?>" data-srno="1" class="form-control input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>

                      <td class="td_tax_rate tax" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate<?php echo $n; ?>" data-srno="1" maxlength="4" class="form-control input-sm tax_rate decimal" data-parsley-required="true" data-parsley-type="number" value="<?php  if($proposal_details['tax_option']!='total_tax'){ if($subscription_tax[$i]!=''){ echo $subscription_tax[$i]; }else{ echo '0'; } }else{ echo '0'; }?>" onchange="qty_check()"></td>

                      <td class="td_tax_amount" id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount<?php echo $n; ?>" data-srno="1" class="form-control input-sm tax_amount" readonly /></td>

                      <td><input type="text" name="amount_gbp[]" id="amount_gbp<?php echo $n; ?>" data-srno="1" class="form-control input-sm amount_gbp" readonly /></td>
                      <td><button type="button" name="remove_row" id="<?php echo $n; ?>" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td>
                      </tr>
                                    
                              <?php   $n++;  }} ?>

                              <input type="hidden" name="counts_section" id="counts_section" value="<?php echo $n; ?>">

                  <?php  }else{ ?>
                     <tr class="default_row" id="default_row">
                       <td><span id="sr_no">1</span></td>
                      <td><input type="text" name="item_name[]" id="item_name1" class="form-control input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required"></td>

                      <td><textarea name="description[]" id="description1" data-srno="1" class="form-control input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"></textarea> </td>

                      <td><input type="number" name="quantity[]" id="quantity1" data-srno="1" class="form-control input-sm  quantity decimal_qty" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0"></td>

                      <td><input type="text" name="unit_price[]" id="unit_price1" data-srno="1" class="form-control input-sm unit_price decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0" /></td>

                      <td><input type="text" name="discount[]" id="discount1" data-srno="1" class="form-control input-sm discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0" ></td>

                      <td><input type="text" name="account[]" id="account1" data-srno="1" class="form-control input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>

                      <td class="td_tax_rate" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate1" data-srno="1" maxlength="4" class="form-control input-sm tax_rate decimal" data-parsley-required="true" data-parsley-type="number" value="0"></td>

                      <td class="td_tax_amount" id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount1" data-srno="1" class="form-control input-sm tax_amount" readonly /></td>

                      <td><input type="text" name="amount_gbp[]" id="amount_gbp1" data-srno="1" class="form-control input-sm amount_gbp" readonly /></td>

                      <!-- <td></td> -->
                      <!-- <td><button type="button" name="remove_row" id="1" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td></td> -->

                <!-- <td><input type="hidden" name="act_amt" id="actual_amount1" data-srno="1" class="data-srno="1" class="form-control input-sm actual_amount"> </td>   -->   
                    </tr>
                      <?php } ?>        
                    </table> 
                          
                        </div>
                         <div class="add-newline">
                              <input type="button" class="btn btn-card btn-primary addmore" id="addmore" value="add new line">
                           </div>
                           <div class="sub-tot_table" id="sub-tot_table">
                              <table id="sub_tot_table" class="sub_tot_table">
                                 <tbody>
                                    <tr>
                                       <th>sub total</th>
                                       <td> <div class="input-group">
                                              <div class="input-group-addon">
                                             <?php  if(isset($proposal_details['currency'])){  echo $currency_symbols[$proposal_details['currency']];  }else{  if(isset($admin_settings['crm_currency'])){ echo $currency_symbols[$admin_settings['crm_currency']]; } }?>
                                              </div>


                                       <input type="text" name="sub_tot" id="sub_total" class="form-control input-sm txtcal sub_total" value="0.00" readonly>
                                       </div></td>
                                    </tr>
                                    <tr id="vat_row" class="vat_row">
                                       <th>VAT</th>
                                       <td><input type="text" name="value_at_tax" id="value_at_tax" class="form-control input-sm txtcal value_at_tax" value="0.00" readonly></td>
                                    </tr>
                                    <tr>
                                       <th>includes adjustments to Tax</th>
                                       <td><input type="text" name="adjust_tax" id="adjust_tax" class="form-control input-sm adjust_tax" value="0.00"></td>
                                    </tr>
                                    <tr class="grand-total">
                                       <th>total</th>
                                       <td>
                                              <div class="input-group">
                                              <div class="input-group-addon">
                                             <?php  if(isset($proposal_details['currency'])){  echo $currency_symbols[$proposal_details['currency']];  }else{  if(isset($admin_settings['crm_currency'])){ echo $currency_symbols[$admin_settings['crm_currency']]; } }?>
                                              </div>
                                       
                                            <input type="text" name="grand_total" id="grand_total" class="form-control input-sm grand_total" value="0.00" readonly>
                                           

                                        </div></td>

                                    <input type="hidden" name="grandtotal_hidden" id="grandtotal_hidden" class="form-control input-sm grandtotal_hidden" value="0.00">   
                                       
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                     </div><!--invoice table -->


                      <div class="custom_upload upload-data06">
                      <label>Attachment</label>
                           <input type="file" id="files" name="userFiles[]" multiple="multiple">
                        <div id="image_preview"></div>
                         
                     </div>



                     <div class="save-approve" id="save-approve">
                            <select name="transaction_payment" class="select_payment" id="select_payment">
                             <option value="approve"> Approve </option> 
                             <option value="decline"> Decline </option> 
                            </select> 
                          
                        <div class="approve-btn">
                            <div class="table-savebtn">
                            <input type="hidden" name="total_item" class="total_item" id="total_item" value="1">
                            <input type="submit" class="btn btn-card btn-primary" id="invoice_save"  value="Send">
                            </div>
                           <!-- <input type="button" class="approve" value="approve"> -->
                           <a href="javascript:;" class="cancel-btn">cancel</a> 
                        </div>
                     </div>
               </div>
            </form>   
            </div>
         </div>
      </div>
   </div>
</div>
</div>
 <?php ?>
<!-- Modal -->
<!-- Modal -->
  <div class="modal fade" id="temp_invoice" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Invoice</h4>
        </div>
        <div class="modal-body">
        <form action="#" method="post">
          <div id="generate_invoice" class="invoice-details generate-invoice">
               <!-- <div class="update-logo">
                    <input type="file">
                    <div class="up-logo">
                    </div>
                  </div> -->
                  <div class="online-payments">
                    <div class="temp-edit">
                      <h3>Remindoo.org</h3>
                    </div>
                    <div class="payment-table newlayout">                        
                            <h3><?php  if(isset($proposal_details['currency'])){  echo $currency_symbols[$proposal_details['currency']];  }else{  if(isset($admin_settings['crm_currency'])){ echo $currency_symbols[$admin_settings['crm_currency']]; } } ?><span class="grand_totel"></span></h3>
                            <div class="divok">
                              <div class="uprecs">
                                <h3 class="upon-res">Upon Reciept</h3>
                                <h6>Due</h6>   
                              </div>
                              <div class="duenoin">                      
                                <span class="due-date12" id="in_number"></span>
                                <h5 class="invoice-no">invoic no</h5>
                              </div>
                            </div>
                    </div>
                  </div>
                  <div class="generate-top">
                    <span class="project-name">
                      Invoice
                     <!--  <span>project name</span> -->
                    </span>
                    <span class="date-sec">
                      <strong><div class="g-date" id="date_of_invoice"></div></strong>
                      <!-- <input type"text" class="g-date" id="date_of_invoice" value="12/2/2018">  -->
                    </span>
                  </div>
                  <div class="des-table">
                  <div class="table-responsive">
                      <table class="invoice_bill_table" id="invoice_bill_table">
                            <thead>    
                              <tr>
                                 <th>description</th>
                                 <th>qty</th>
                                 <th>price</th>
                                 <th style="width:5%;">amount</th>
                              </tr>
                            </thead>   
                            <tbody>
                              <tr class="fetch_table" id="fetch_table">
                                <td></td>

                                <td></td>

                                <td><i class="fa fa-gbp"></i> </td>

                                <td><i class="fa fa-gbp"></i> </td>

                              </tr>
                             
                            </tbody>    
                          </table>
                       
                  </div>
                    <div class="sub-totaltab">
                      <table class="fetch_sub_total" id="fetch_sub_total">
                          <tr>
                            <th>sub total</th>
                            <td class="subTot"><i class="fa fa-gbp"> </i></td>
                          </tr>
                          <tr>
                            <th>total</th>
                            <td class="gndTot"><i class="fa fa-gbp"> </i></td>
                          </tr>
                      </table>                     
                    </div>
                  </div>
                  <div class="payment-ins">
                 <!--    <span>payment instrctions</span> -->
                    </div>
                <!--   <div class="payment-ins">
                    <span class="place">united kingdom</span>
                  </div> -->
                <input type="hidden" name="total_item" class="total_item" id="total_item" value="1">
                  
               </div>
            </form>   
        </div>
       <!--  <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>


<!-- Modal -->

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<script>
   $( document ).ready(function() {
   
       var date = $('.datepicker1').datepicker({ dateFormat: 'dd-mm-yy',  minDate:0, changeMonth: true,
        changeYear: true, }).val();
   
       // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#1abc9c',
               jackColor: '#fff',
               size: 'small'
           });
       });
   
       $('#accordion_close').on('click', function(){
               $('#accordion').slideToggle(300);
               $(this).toggleClass('accordion_down');
       });
   });
</script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script type="text/javascript">
 $('.dropdown-sin-5').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });
  
</script>

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   $("#staff_form").validate({     
          ignore: false, 
          rules: {
          first_name: {required: true},
          last_name: {required: true},
          email_id:{required: true,email:true},
          telephone_number : {required:true,minlength:10,
          maxlength:10,
          number: true},
          password:{required: true},
          /*image_upload: {required: false, accept: "jpg|jpeg|png|gif"},*/
          image_upload: {
            required: function(element) {

            if ($("#image_name").val().length > 0) {
              return false;
            }
            else {
            return true;
            }
            },
            accept: "jpg|jpeg|png|gif"
          },
                           hourly_rate: {required: true,number:true},
                           roles:{required: true},
                           
                           },
                           errorElement: "span" , 
                           errorClass: "field-error",                             
                            messages: {
                             first_name: "Enter a first name",
                             last_name: "Enter a last name",
                             roles:"Select a role",
                             email_id: {
       email:"Please enter a valid email address", 
       required:"Please enter a email address",
   
   },
                            telephone_number:{  yourtel:"Enter a valid phone number.",
     required: "Enter a phone number."},
                             password: {required: "Please enter your password "},
                             image_upload: {required: 'Required!', accept: 'Not an image!'},
                             
                             hourly_rate: {required: "Please Enter Your rate",number:"Please enter numbers Only"},
                            },
                            
   
                           
                           submitHandler: function(form) {
                               var formData = new FormData($("#staff_form")[0]);
                              
   
                               $(".LoadingImage").show();
   
                               $.ajax({
                                   url: '<?php echo base_url();?>staff/add_staff/',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) {
                                       
                                       if(data == 1){
                                          
                                          // $('#new_user')[0].reset();
                                               location.reload(true);
                                           $('.alert-success').show();
                                           $('.alert-danger').hide();
   
                                       }
                                       else{
                                          // alert('failed');
                                           $('.alert-danger').show();
                                           $('.alert-success').hide();
                                       }
                                   $(".LoadingImage").hide();
                                   },
                                   error: function() { $('.alert-danger').show();
                                           $('.alert-success').hide();}
                               });
   
                               return false;
                           } ,
                            invalidHandler: function(e, validator) {
              if(validator.errorList.length)
           $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
   
           }
                            
                       });
   
   
   });
</script>

<!-- <script type="text/javascript">$(document).ready(function(){
var number = 1 + Math.floor(Math.random() * 999999999);
//$("#item1").hide();
$("input[name='refnumber']").val(number);

});
</script> -->


<script type="text/javascript">
  $(document).ready(function() {
    var sub_total_amount = $('#sub_total').val();

<?php if(isset($proposal_details)){ ?>
var count = $('#counts_section').val();

console.log('Counts');
console.log(count);
<?php }else{ ?>
var count = 1;
<?php } ?>

    

    $('#select_tax').on('change', function() {
    var select_val = $(this).val();
    if(select_val == 1)
    {
      $('table tr > th:nth-child(8), table tr > td:nth-child(8)').show();
      $('table tr > th:nth-child(9), table tr > td:nth-child(9)').show();
      $('#sub_tot_table tr.vat_row').show();
      // $('#sub-tot_table').show();
      // $('.approve-btn').show();


    } else {
      $('table tr > th:nth-child(8), table tr > td:nth-child(8)').hide();
      $('table tr > th:nth-child(9), table tr > td:nth-child(9)').hide();
      $('#sub_tot_table tr.vat_row').hide();
      // $('#sub-tot_table').hide();
      // $('.approve-btn').hide();
    }
  });

    $(document).on('click','.addmore', function() { 
      var a = $('.select_tax').val();
     
      count++;
      $('.total_item').val(count);
      var html_code = '';
      var html_code1 = '';

      if(a == 1) 
      {
      html_code += '<tr id="row_id_'+count+'">';
      html_code += '<td><span id="sr_no">'+count+'</span></td>';
      html_code += '<td><input type="text" name="item_name[]" id="item_name'+count+'" class="form-control input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required"></td>';
      html_code += '<td><textarea name="description[]" id="description'+count+'" data-srno="'+count+'" class="form-control input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"></textarea></td>';
      html_code += '<td><input type="number" name="quantity[]" id="quantity'+count+'" data-srno="'+count+'" class="form-control input-sm quantity decimal_qty" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0" <?php if(isset($proposal_details)){ ?> onchange="qty_check()" <?php } ?>></td>';


      html_code += '<td><input type="text" name="unit_price[]" id="unit_price'+count+'" data-srno="'+count+'" class="form-control input-sm unit_price decimal" maxlength="6" data-parsley-required="true" data-parsley-type="number" value="0" <?php if(isset($proposal_details)){ ?> onchange="qty_check()" <?php } ?>></td>';


      html_code += '<td class="discount_tr dis"><input type="text" name="discount[]" id="discount'+count+'" data-srno="'+count+'" class="form-control input-sm number_only discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0" <?php if(isset($proposal_details)){ ?> onchange="qty_check()" <?php } ?>></td>';
      html_code += '<td><input type="text" name="account[]" id="account'+count+'" data-srno="'+count+'" class="form-control input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>';

      html_code += '<td class="td_tax_rate tax" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate'+count+'" data-srno="'+count+'" class="form-control input-sm number_only tax_rate decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0" <?php if(isset($proposal_details)){ ?> onchange="qty_check()" <?php } ?>></td>';
      html_code += '<td class="td_tax_amount" id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount'+count+'" data-srno="'+count+'" class="form-control input-sm tax_amount" readonly></td>';
      html_code += '<td><input type="text" name="amount_gbp[]" id="amount_gbp'+count+'" data-srno="'+count+'" class="form-control input-sm amount_gbp" readonly /></td>';
      html_code += '<td><button type="button" name="remove_row" id="'+count+'" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td>';  

        <?php if(isset($proposal_details)){ ?>

        var section_count=$("#counts_section").val();

        var sec=Number(section_count) + 1;

        $("#counts_section").val(sec);
        <?php } ?>

      // html_code += '<td><input type="hidden" name="act_amt" id="actual_amount'+count+'" data-srno="'+count+'" class="data-srno="1" class="form-control input-sm actual_amount"> </td>';
      html_code += '</tr>';
       } else {
 
      html_code += '<tr id="row_id_'+count+'">';
      html_code += '<td><span id="sr_no">'+count+'</span></td>';
      html_code += '<td><input type="text" name="item_name[]" id="item_name'+count+'" class="form-control input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required"></td>';
      html_code += '<td><textarea name="description[]" id="description'+count+'" data-srno="'+count+'" class="form-control input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"></textarea></td>';
      html_code += '<td><input type="number" name="quantity[]" id="quantity'+count+'" data-srno="'+count+'" class="form-control input-sm quantity decimal_qty" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0"></td>';
      html_code += '<td><input type="text" name="unit_price[]" id="unit_price'+count+'" data-srno="'+count+'" class="form-control input-sm unit_price decimal" maxlength="6" data-parsley-required="true" data-parsley-type="number" value="0"></td>';
      html_code += '<td class="discount"><input type="text" name="discount[]" id="discount'+count+'" data-srno="'+count+'" class="form-control input-sm number_only discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0"></td>';
      html_code += '<td><input type="text" name="account[]" id="account'+count+'" data-srno="'+count+'" class="form-control input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>';
      
      html_code += '<td><input type="text" name="amount_gbp[]" id="amount_gbp'+count+'" data-srno="'+count+'" class="form-control input-sm amount_gbp" readonly /></td>';
      html_code += '<td><button type="button" name="remove_row" id="'+count+'" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td>';  

      // html_code += '<td><input type="hidden" name="act_amt" id="actual_amount'+count+'" data-srno="'+count+'" class="data-srno="1" class="form-control input-sm actual_amount"> </td>';

      html_code += '</tr>';
      }


     
      $('#invoice_table').append(html_code);


       <?php 
       if(isset($proposal_details)){

        if($proposal_details['tax_option']!='total_tax'){ ?>

        console.log('line tax');

        var counting_section="#row_id_"+count;
        console.log(counting_section);
        $(counting_section).find(".td_tax_rate").removeClass('tax');
         console.log($(counting_section).find(".td_tax_rate").attr('class'));

      <?php } ?>

      <?php   if($proposal_details['discount_option']!='total_discount'){ ?>
         console.log('line discount');
           var counting_section="#row_id_"+count;
            console.log(counting_section);
            $(counting_section).find(".discount_tr").removeClass('dis');
            console.log( $(counting_section).find(".discount_tr").attr('class'));
      <?php   } } ?>
      // $('#invoice_bill_table').append(html_code1);

      // var data=$("#description").html();

      // // $(".body-popup-content").html('');
      // $("#invoice_bill_table").append(data);

      
     
    });


  $(document).on('click', '.remove_row', function() { 
    var row_id = $(this).attr("id");
    // alert(row_id);
    var final_amount = $('#amount_gbp'+row_id).val();

    var sub_total = $('#sub_total').val();
    var result_amt = parseFloat(sub_total) - parseFloat(final_amount);
    $('#sub_total').val(result_amt);

    var tax_amt = $('#tax_amount'+row_id).val();
    if(tax_amt!=''){
    var vat_amount = $('#value_at_tax').val();
    var vat_result = parseFloat(vat_amount) - parseFloat(tax_amt);
    $('#value_at_tax').val(vat_result);
   // calculateSum();



  }
   

    // var gndtot = parseFloat(result_amt) + parseFloat(vat_result);
    // $('#grand_total').val(gndtot);


    $('#row_id_'+row_id).remove();
    count--;

 calculateSum();
    $('.total_item').val(count);  

  });


  function cal_final_total(count)
  { 
//console.log(count + 1);
var count=Number(count) + 1;
//console.log(count);
    //console.log(count);
    var a = $('.quantity').val();

    console.log(a);
    
    var final_total_amount = 0;
    for(j=1; j<=count; j++)
    { 
      var item = 0;
      var descre = 0;
      var qty = 0;
      var price = 0;
      var discount = 0;
      var account = 0;
      var taxRate = 0;
      var taxAmount = 0;
      var amount = 0;
      
      
    qty = $('#quantity'+j).val();
    if(qty > 0)
    {  
      price = $('#unit_price'+j).val();
      if(price > 0)
      {
        amount = parseFloat(qty) * parseFloat(price);
        $('#amount_gbp'+j).val(amount);

        // $('#amount_gbp'+j).each(function() {
        //     $(this).keyup(function() {

        //       calculateSum();
        //     });
        //   });
        calculateSum();
        

        discount = $('#discount'+j).val();
        console.log('Discount');

        console.log(discount);

        <?php 
       // if(isset($proposal_details)){ ?>        


         

       <?php   //}else{ ?>
        if(discount > 0)
        { 
          var dis = (discount/100).toFixed(2);
          var mult = amount*dis;
          var result_amt = amount-mult;
          $('#amount_gbp'+j).val(result_amt);
          
          calculateSum();
          
        }

        <?php //} ?>

        taxRate = $('#tax_rate'+j).val();

        <?php 
 ?>
        if(taxRate > 0)
        { 
          taxAmount = parseFloat(amount) * parseFloat(taxRate)/100;
          $('#tax_amount'+j).val(taxAmount);

          cal_tax_amount();
          
        }

        <?php //} ?>
      }        
    }     
    }  
  }

function calculateSum()
{
  var sum = 0;
  $('.amount_gbp').each(function() {
    if(!isNaN(this.value) && this.value.length!=0) {
      sum += parseFloat(this.value);
      // alert(sum);
    }
  });
  $('#sub_total').val(sum.toFixed(2));

 

  // $('#grand_total').val(sum.toFixed(2));
  update_gndtotal();
}


function cal_tax_amount()
{




      var tot = 0;
      $('.tax_amount').each(function() {
        if(!isNaN(this.value) && this.value.length!=0) {
          tot += parseFloat(this.value);
        }
      });
      $('#value_at_tax').val(tot.toFixed(2));
      update_gndtotal();

 <?php //} ?>
}



function update_gndtotal()
{

var sub = 0;
var vat = 0;
var gnd_tot = 0;
var ad = 0; 
 
  sub = parseFloat($('#sub_total').val());
  vat = parseFloat($('#value_at_tax').val());

  gnd_tot = sub + vat;

  //console.log(gnd_tot);
  $('#grand_total').val(gnd_tot.toFixed(2));
  $('#grandtotal_hidden').val(gnd_tot.toFixed(2));

}

  $(document).on('blur', '.unit_price', function(){
      <?php
    if(isset($proposal_details)){ ?>
    cal_final_total($('#counts_section').val());
    <?php }else{ ?>
    cal_final_total(count);
    <?php } ?>
  });

  $(document).on('blur', '.discount', function(){
       <?php
    if(isset($proposal_details)){ ?>
    cal_final_total($('#counts_section').val());
    <?php }else{ ?>
    cal_final_total(count);
    <?php } ?>
  });   

  $(document).on('blur', '.tax_rate', function(){
       <?php
    if(isset($proposal_details)){ ?>
    cal_final_total($('#counts_section').val());
    <?php }else{ ?>
    cal_final_total(count);
    <?php } ?>
  });

  $(document).on('blur', '.amount_gbp', function(){
       <?php
    if(isset($proposal_details)){ ?>
    cal_final_total($('#counts_section').val());
    <?php }else{ ?>
    cal_final_total(count);
    <?php } ?>
  });


$('#adjust_tax').change(function () {
 
    // initialize the sum (total price) to zero
    var sum = 0;
    // var b = $('.grandtotal_hidden').val();
    // we use jQuery each() to loop through all the textbox with 'price' class
    // and compute the sum for each loop
    // alert(sum);
    $('.adjust_tax').each(function() {
        a = parseFloat($(this).val());
        if(!isNaN(a) && a > 0) {
          sum = parseFloat($('.grandtotal_hidden').val()) + a;  
        } else {
          sum = parseFloat($('.grandtotal_hidden').val());
          // alert(sum);
        }
        
    });
    
     // console.log(sum);
    // set the computed value to 'totalPrice' textbox
    $('#grand_total').val(sum.toFixed(2));
     
});
});
</script>
<script>
$(document).ready(function(){
$('.number_only').keypress(function(e){
return isNumbers(e, this);      
});
function isNumbers(evt, element) 
{
var charCode = (evt.which) ? evt.which : event.keyCode;
if (
(charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
(charCode < 48 || charCode > 57))
return false;
return true;
}
});
</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/parsley.css') ?>">
<script src="<?php echo base_url('assets/js/parsley.min.js') ?>"></script>
<script type="text/javascript">
  // $('#invoice_form').parsley();
$( document ).ready(function() {
    $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy' }).val();
   //$(document).on('change','.othercus',function(e){
   });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#preview-pdf a').on('click', function() {
      
      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'Invoice/previewInvoice';?>",
        data: $('#invoice_form').serialize(),
        success:function(data) {
          var con = $(data).find('tbody').html();
          $('#invoice_bill_table tbody').html( con );
          var subValue = $(data).find('tfoot #sTot').html();
          $('#fetch_sub_total td.subTot').html( subValue ); 
          var gndValue = $(data).find('tfoot #gTot').html();
          $('#fetch_sub_total td.gndTot').html( gndValue );  
           $(".grand_totel").html( gndValue);  
          var in_date = $(data).find('tfoot #invoiceDate').html();
          $('#date_of_invoice').html(in_date);
          var in_no = $(data).find('tfoot #invoiceNo').html();
          $('#in_number').html(in_no);
        }

      });

    });


  });


  $("#invoice_save").click(function(event) {
  //event.preventDefault();
var client_email=$("#clint_email").val();
if ($.trim(client_email).length == 0) {
//alert('All fields are mandatory');
event.preventDefault();
}
if (validateEmail(client_email)) {

if($("input[name=due_date]").val()!=''){

 $("#invoice_form").submit();

}
}else {
  $("#clint_email").css('border','1px solid red');
//alert('Invalid Email Address');
//event.preventDefault();
}

});


  function validateEmail(sEmail) {
var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
if (filter.test(sEmail)) {
return true;
}
else {
return false;
}
}


    $('.decimal').keyup(function(){
    //  alert('gdfgfg');
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});


  $('.decimal_qty').keyup(function(evt){

     var keycode = evt.charCode || evt.keyCode;

      if (keycode  == 46) {
        return false;
      }
});


   var inputQuantity = [];
    $(function() {
      $(".decimal_qty").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".decimal_qty").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the index
//        window.console && console.log($field.is(":invalid"));
          //  $field.is(":invalid") is for Safari, it must be the last to not error in IE8
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 4);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    });
    <?php
    if(isset($proposal_details)){ ?>
      $( document ).ready(function() {
      <?php  if($proposal_details['tax_option']!='total_tax'){ ?>

          $(".td_tax_rate").removeClass('tax');

      <?php } ?>

      <?php   if($proposal_details['discount_option']!='total_discount'){ ?>
             $(".discount_tr").removeClass('dis');
      <?php   } ?>
      //  console.log('work');
           $(".discount").trigger('blur');
           $(".unit_price").trigger('blur');
           $(".amount_gbp").trigger('blur');
           $(".tax_rate").trigger('blur');
          // console.log('trigger');
          qty_check();

   
      });

    

    <?php }else{ ?>

 $(".td_tax_rate").removeClass('tax');
 $(".discount_tr").removeClass('dis');

   <?php } ?>

    function cal_taxes_count()
{
 

var tot=$('#sub_total').val();
console.log(tot);
var taxamountes=$("#taxes_amount").val();

tot = parseFloat(tot) * parseFloat(taxamountes)/100;
console.log(tot);
console.log('Working');

alert('ok');
$('#value_at_tax').val(tot);
//update_gndtotal();  
 
  sub = parseFloat($('#sub_total').val());
  vat = parseFloat($('#value_at_tax').val());

  gnd_tot = sub + vat;

  console.log(gnd_tot);
  $('#grand_total').val(gnd_tot.toFixed(2));
  $('#grandtotal_hidden').val(gnd_tot.toFixed(2));

}
    function cal_discount_count()
{
var tot=$('#grand_total').val();
console.log(tot);
var taxamountes=$("#discountes_amount").val();
console.log('Discount');
console.log(taxamountes);
tot = parseFloat(tot) * parseFloat(taxamountes)/100; 
  sub = parseFloat($('#grand_total').val());
  gnd_tot = sub - tot;

  console.log(gnd_tot);
  $('#grand_total').val(gnd_tot.toFixed(2));
  $('#grandtotal_hidden').val(gnd_tot.toFixed(2));

 // cal_discount_count();
}

function qty_check(){
    //alert('product Working');
    var sum=0;
    var vat_tot=0;

    console.log(sum);
     console.log(vat_tot);
    $('.unit_price').each(function() {  
        var qty_1=$(this).parents('.top-fields').find('.quantity').val();
      //  console.log(qty_1);
        var tax=$(this).parents('.top-fields').find('.tax_rate').val();
     //   console.log(tax);
        var discount=$(this).parents('.top-fields').find('.discount').val(); 
       //  console.log(discount);
        var price=$(this).val();
 //console.log(price);
        var tax_amount=(price * qty_1).toFixed(2);

        var dec = (tax/100).toFixed(2);  

        var discount1 = (discount/100).toFixed(2);

    //   console.log(dec);
    //   console.log(discount1);

          var mult = (tax_amount* dec).toFixed(2); 

     //      console.log(mult);  
        $(this).parents('.top-fields').find('.tax_amount').val(mult); 

          var dis_count = (tax_amount* discount1).toFixed(2); 

    //   console.log(dis_count);

        var price_cal=(Number($(this).val()) * Number(qty_1)).toFixed(2); 
       //   console.log("price_cal");
      // //     console.log(price_cal);

              var calculation=(Number(price_cal) - Number(dis_count)).toFixed(2); 
        //    console.log("calculation");

        $(this).parents('.top-fields').find('.amount_gbp').val(calculation);        


           var vat_tot=0;
           $('.tax_amount').each(function() {  
            var tax_amount  =$(this).val();

             vat_tot += Number(tax_amount);
                  console.log('****');
           });
           console.log('vat total');
console.log(vat_tot);


           $(".value_at_tax").val(Math.round(vat_tot));
           var sum=0;
             $('.amount_gbp').each(function() {
             var amount  =$(this).val();
             sum += Number(amount);
                  console.log('****');
           });
//console.log(sum);
              $(".sub_total").val(Math.round(sum));

              var sub=$(".sub_total").val();

              var vat=$(".value_at_tax").val();

              var total=Number(sub) + Number(Math.round(vat));

              $(".grand_total").val(Math.round(total));  


              if($("#tax_option").val()=="line_tax"){
                var tax_amount=0;
              }else{
                 var tax_amount=$("#taxes_amount").val();

                  var grand_totals=$(".grand_total").val();
                  console.log('###');
                  console.log(grand_totals);
                  var calcPrice  = ( grand_totals * tax_amount / 100 ).toFixed(2);
                  console.log($("#tax_option").val());
                  console.log(tax_amount);
                  //console.log()
                  console.log(calcPrice);

                  $(".value_at_tax").val(Math.round(calcPrice));
                  //  $('.tax_total').html( '' );
                  // $('.tax_total').html( calcPrice );
                  var grandPrice  = ( Number(grand_totals) +( Number(calcPrice) )).toFixed(2);
                  //$('.grand_total').html( '' );
                  $('.grand_total').val( Math.round(grandPrice));
              }

              if($("#discount_option").val()=="line_discount"){
                 var discount_amount=0;
              } else{
                var discount_amount=$("#discountes_amount").val();

                 var grand_totals=$(".grand_total").val();
                  console.log('#1#2#');
                  console.log(grand_totals);
                  var disPrice  = ( grand_totals * discount_amount / 100 ).toFixed(2);
                //  console.log(calcPrice);

                //  $(".value_at_tax").val(Math.round(calcPrice));
                  //  $('.tax_total').html( '' );
                  // $('.tax_total').html( calcPrice );
                  var grandPrice  = ( Number(grand_totals) - ( Number(disPrice) )).toFixed(2);
                  //$('.grand_total').html( '' );
                  $('.grand_total').val( Math.round(grandPrice));
              }

   //        var tax=Number($('.tax_rate').text());



        });
   }


     $(document).ready(function() {
        if (window.File && window.FileList && window.FileReader) {
          $("#files").on("change", function(e) {    
             $("#image_preview").html('');   
            var files = e.target.files,
            filesLength = files.length;
              for (var i = 0; i < filesLength; i++) {
                  var filenames = this.files[i].name;                     
                  var fileType=files[i].type;
                  var fileSize=files[i].size / 1024 / 1024;     
                  var fileName= files[i].name;  
                  console.log(fileName);
                  if(fileSize <= 2){
                  var ValidImageTypes = ["application/pdf", "image/jpg", "image/png","image/jpeg","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel","text/csv","application/doc","application/docx","application/msword"];
                  if ($.inArray(fileType, ValidImageTypes) < 0) {
                     $("#upload").attr('disabled','disabled');
                     $("#error_content").html('Document Must be Png,JPG,JPEG,PDF,xlsx,csv Format');
                   //  $("#Error_show").modal('show');
   
                   $(".Error-alert").show();
                    // alert('Document Must be Png,JPG,JPEG,PDF Format');
                  }else{ 
                     $("#upload").removeAttr("disabled");
                   if(fileType =='application/pdf'){
                      $('#image_preview').append("<div class='col-md-12 pip original' id="+files[i].name+">"+files[i].name+"<span class='removed' id='remove_"+i+"'>X</span><input type='hidden' name='image_name[]' value='" +files[i].name+ "'></div>");  
   
                       $('#image_preview1').append("<div class='col-md-12 pip' id="+files[i].name+">"+files[i].name+"<span class='removed' id='remove_"+i+"'>X</span><input type='hidden' name='image_name[]' value='" +files[i].name+ "'></div>");  
                   }else{
                       $('#image_preview').append("<div class='col-md-12 pip original' id="+files[i].name+">"+files[i].name+"<span class='removed' id='remove_"+i+"'>X</span><input type='hidden' name='image_name[]' value='" +files[i].name+ "'></div>");  
                        $('#image_preview1').append("<div class='col-md-12 pip' id="+files[i].name+">"+files[i].name+"<span class='removed' id='remove_"+i+"'>X</span><input type='hidden' name='image_name[]' value='" +files[i].name+ "'></div>");  
                   }
                    $("#remove_"+i).click(function(){
   
   
                      console.log($(this).parent(".pip").html());
                       var value=$(this).parent(".pip").html().split('<span');
                        console.log(value[0]);
                      //  console.log(value[1]);
                        var removeItem =value[0]; 
                         $(this).parent(".pip").remove();
   
                         console.log($("#attch_image").val());
                          var result=$("#attch_image").val().split(',');
                          console.log(result);
                          var result1 = result.filter(function(elem){
                          return elem != removeItem; 
                          });                       
   
                          $("#attch_image").val(result1);
                    }); 
                }
               }else{
                $("#upload").attr('disabled','disabled');
               // alert('FileSize Is Too Large');
   
               $(".File-alert").show();
               }
              } 
          });
        }else {
          alert("Your browser doesn't support to File API")
        }
      });
</script> 
</body>
</html>