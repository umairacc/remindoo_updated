<script type="text/javascript">
	$(document).ready(function() {
		$("#save_template").click(function() {
			var template_name = $("#template_name").val().trim();
			var proposals = $("#proposal_contents_details").html();

			var thead = $("#proposal_contents_details").find('table thead tr').children('td #dd-head');
			var tbody = $("#proposal_contents_details").find('table tbody tr').children('td #dd-body');
			var tfoot = $("#proposal_contents_details").find('table tfoot tr').children('td #dd-footer');

			if (template_name != '' && ($(thead).html().trim() != '' || $(tbody).html().trim() != '' || $(tfoot).html().trim() != '')) {
				$("#proposal_contents_details  div.popover-lg").each(function() {
					$(this).remove();
				});

				var template_id = $("#template_id").val();

				check_distinct('template_title', template_name, template_id, proposals);
			} else {
				if (template_name == '') {
					$("#template_name").css("border", "1px solid red");
				} else {
					$("#template_name").css("border", "");
				}

				if ($(thead).html().trim() == '' && $(tbody).html().trim() == '' && $(tfoot).html().trim() == '') {
					$("#mail-template").css("border", "1px solid red");
				} else {
					$("#mail-template").css("border", "");
				}
			}
		});

		$(".select_template").click(function() {
			var value = $(this).attr('id');
			$('#' + value).addClass('select');
			$.ajax({
				url: '<?php echo base_url(); ?>proposal_pdf/session_set/',
				type: 'POST',
				data: {
					'id': value
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					window.location.href = '<?php echo base_url(); ?>proposal_page/step_proposal';
				}
			});
		});

		$(".delete_template").click(function() {
			var value = $(this).attr('id');

			$.ajax({
				url: '<?php echo base_url(); ?>Proposal_page/delete_pro_template',
				type: 'POST',
				data: {
					'id': value
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();

					if (data == '1') {
						$('.alert-delete').show();
						setTimeout(function() {
							$('.alert-delete').hide();
							location.reload();
						}, 3000);
					}
				}
			});
		});


		$(".edit_template").click(function() {
			var value = $(this).attr('id');

			$.ajax({
				url: '<?php echo base_url(); ?>proposals/template_content',
				type: 'POST',
				data: {
					'id': value
				},
				beforeSend: function() {
					$(".LoadingImage").show();
				},
				success: function(data) {
					$(".LoadingImage").hide();
					var json = [];
					json = JSON.parse(data);
					template_name = json['template_name'];
					template_id = json['template_id'];

					$("#template_id").val(template_id);
					$("#template_name").val(template_name);
					$('div#my_templates .template-details').hide();
					$('div#all_tempaltes .template-details').hide();
					$(".all-mycnewtemp").hide();
					$('.cnew_template').show();

					/*$("#Preview_proposal_"+value).modal('hide');*/


					if (typeof(json['template_head']) != "undefined") {
						$("#proposal_contents_details").find('table thead tr').children('td #dd-head').html(json['template_head']);
					}
					if (typeof(json['template_body']) != "undefined") {
						$("#proposal_contents_details").find('table tbody tr').children('td #dd-body').html(json['template_body']);
					}
					if (typeof(json['template_footer']) != "undefined") {
						$("#proposal_contents_details").find('table tfoot tr').children('td #dd-footer').html(json['template_footer']);
					}

					/* $(".cnew_template").find('#dd-head').html('');
					$(".cnew_template").find('#dd-body').html('');
					$(".cnew_template").find('#dd-footer').html('');

					var template_content=$("#template_own_content").val();
					$(".cnew_template").find('#dd-head').append('<div class="header_section" onclick="header_section()">'+template_head+'</div>');
					$(".header_section").find("#dd-body").remove();

					$(".cnew_template").find('#dd-footer').append('<div class="footer_section" onclick="footer_section()">'+template_footer+'</div>');*/

				}

			});

		});

		$("#cancel_template").click(function() {
			location.reload();
		});
	});

	function check_distinct(field, value, template_id, proposals) {
		$.ajax({
			url: '<?php echo base_url(); ?>Proposal_page/check_distinct',
			type: 'POST',
			data: {
				'field': field,
				'value': value,
				'id': template_id
			},

			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(count) {
				$(".LoadingImage").hide();

				if (count > 0) {
					$("#template_name").css("border", "1px solid red");
					$('#template_name_er').html('Already Exists.');
				} else {
					$('#template_name_er').html('');
					if (template_id == '') {
						$.ajax({
							url: '<?php echo base_url(); ?>proposal_pdf/mytemplate',
							type: 'POST',
							data: {
								'template_title': value,
								'template_content': proposals
							},
							beforeSend: function() {
								$(".LoadingImage").show();
							},
							success: function(data) {
								$(".LoadingImage").hide();
								$('.alert-success').show();
								setTimeout(function() {
									location.reload();
								}, 3000);
							}
						});
					} else {
						$.ajax({
							url: '<?php echo base_url(); ?>proposal_pdf/mytemplate_update',
							type: 'POST',
							data: {
								'template_title': value,
								'template_content': proposals,
								'id': template_id
							},
							beforeSend: function() {
								$(".LoadingImage").show();
							},
							success: function(data) {
								$(".LoadingImage").hide();
								$('.alert-success-update').show();
								setTimeout(function() {
									location.reload();
								}, 3000);
							}
						});
					}
				}
			}

		});
	}
</script>
<div class="popup_section">

	<?php foreach ($my_template as $temp) { ?>

		<div class="modal fade" id="confirmation_<?php echo $temp['id']; ?>" role="dialog">
			<div class="modal-dialog modal-all-template">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Edit Confirmation</h4>
					</div>
					<div class="modal-body">
						<p>Are you sure you want to edit?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default edit_template" data-dismiss="modal" id="<?php echo $temp['id']; ?>">Yes</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="confirmation_select_<?php echo $temp['id']; ?>" role="dialog">
			<div class="modal-dialog modal-all-template">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Select Confirmation</h4>
					</div>
					<div class="modal-body">
						<p>Are you sure you want to select?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default select_template" data-dismiss="modal" id="<?php echo $temp['id']; ?>">Yes</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="confirmation_delete_<?php echo $temp['id']; ?>" role="dialog">
			<div class="modal-dialog modal-all-template">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Delete Confirmation</h4>
					</div>
					<div class="modal-body">
						<p>Are you sure you want to delete?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default delete_template" data-dismiss="modal" id="<?php echo $temp['id']; ?>">Yes</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
					</div>
				</div>
			</div>
		</div>



		<?php }
	if ($_SESSION['firm_id'] != 0) {
		foreach ($all as $all) { ?>

			<div class="modal fade" id="confirmation_<?php echo $all['id']; ?>" role="dialog">
				<div class="modal-dialog modal-all-template">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Edit Confirmation</h4>
						</div>
						<div class="modal-body">
							<p>Are you sure you want to edit?</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default edit_template" data-dismiss="modal" id="<?php echo $all['id']; ?>">Yes</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="confirmation_select_<?php echo $all['id']; ?>" role="dialog">
				<div class="modal-dialog modal-all-template">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Select Confirmation</h4>
						</div>
						<div class="modal-body">
							<p>Are you sure you want to select?</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default select_template" data-dismiss="modal" id="<?php echo $all['id']; ?>">Yes</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
						</div>
					</div>
				</div>
			</div>


	<?php }
	} ?>

</div>