 <?php $services_list = $this->Common_mdl->getServicelist(); ?>

 <style type="text/css">
  .choosclsnew {
    display: block;
  }
  .new-adds-company .annual-field3 {
    width: 33%
  }
 </style>

 <div class="unit-annual floating_set">
 <div class="common-annual lead-annual">
  <div class="spacing-annual accotax-litd send-to choosclsnewtop">
   <div class="choosclsnew">
     <label>Choose a service</label>
     <div class="">                  
        <select name="service_name" id="service_scode">
          <option value="">Select</option>  
          <?php foreach ($services_list as $key => $value){  ?>                                
          <option value="<?php echo $value['service_name']; ?>"><?php echo $value['service_name']; ?></option>
          <?php }  ?>
          <option value="other">Other</option>
       </select>
      </div>
    </div>
    <div class="choosclsnew other_service" style="display: none;">
      <label>Service Name</label>
      <div class=""> 
        <input type="text" name="other_service_name">
      </div>
    </div>
  </div>
  
    <div class="annual-field3 add-input-service"> 
       <label>Price </label>		
       <input type="text" name="service_price" class="decimal" >
    </div>
    <div class="annual-field3 add-input-service">
    <label>Qty</label>
    <input type="text" name="service_qty" placeholder="1.00" class="decimal"></div>
    <div class="annual-field3 add-input-service">
       <label>Unit</label>
       <select name="service_unit">
       <option value="per_service">Per Service</option>
       <option value="per_day">Per Day</option>
       <option value="per_month">Per Month</option>
       <option value="per_2month">Per 2Months</option>
       <option value="per_6month">Per 6Month</option>
       <option value="per_year">Per Year</option>
       </select>
    </div>
    <div class=" spacing-annual  add-input-service">
       <label>Description</label>
       <textarea rows="3" name="service_description" id="add_service_description" class="service-description" required="required"></textarea>
    </div>
    
    <div class="spacing-annual accotax-litd send-to choosclsnewtop">
       <div class="choosclsnew">
       <label>Choose a category</label>
       <div class="category_add">
       <select name="service_category" id="service_category" required="required">
       <option value="">Select </option>
       <?php
       if(count($category)>0){
       foreach($category as $cat){ 
          if($cat['category_type']=='service'){ ?>
          <option value="<?php echo $cat['id']; ?>"><?php echo $cat['category_name']; ?></option>
     <?php   }} } ?>
       </select>
       </div>
    </div>
       <a id="hide_category1" href="javascript:;" data-toggle="modal" data-target="#add-new-category_service"><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add a new Category</a>
    </div>
 </div>
</div>
