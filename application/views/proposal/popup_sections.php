<!--Add Category Popup -->
<div id="add-new-category_service" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog modal-add-category">
         <!-- Modal content-->
          <!-- <form action="<?php //echo base_url(); ?>proposal/add_category" method="post" name="form" id="login_form"> -->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Add new category</h4>
            </div>
            <div class="modal-body"> 
            <p id="category_success" style="display: none;color:#4CAF50;"> Category Added  </p> 

             <p id="category_check" style="display: none;color:#4CAF50;"> Category Already Exits  </p>         
               <div class="add-input-service">
               <input type="hidden" name="category_type" id="category_type" value="service"> 
                  <label>Category name</label>
                  <input type="text" name="category_name" id="category_name" required="required">
               </div>
                 <p id="category_error" style="display: none;color: red"> Category Required  </p>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <input type="submit" name="save" id="category_save" class="category_save" value="Save">              
               <a href="javascript:;" data-dismiss="modal">Close</a>
            </div>
         </div>
         <!--  </form> -->
      </div>
   </div>
<?php
if(count($category)>0){
foreach($category as $cat){ ?>
<div id="delete-category_service-<?php echo $cat['id']; ?>" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog  modal-delete-service">
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal/delete_category" method="post" id="login_form">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Delete Category</h4>
            </div>
            <div class="modal-body"> 
            <input type="hidden" name="checkbox" value="<?php echo $cat['id']; ?>">
            <input type="hidden" name="type" value="<?php echo $cat['category_type']; ?>">
               Do you want to delete?
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Yes</button>        
               <a href="#" data-dismiss="modal">no</a>
            </div>
         </div>
          </form>
      </div>
   </div>

<?php } } ?>
<?php
if(count($category)>0){
foreach($category as $cat){
if($cat['category_type']=='service'){ ?>

   <div id="edit-category_service-<?php echo $cat['id']; ?>" class="modal fade new-adds-categories edit_categories" role="dialog">
      <div class="modal-dialog modal-add-category">
         <!-- Modal content-->
           <form action="<?php echo base_url(); ?>proposal/edit_category" method="post" name="form" id="category_edit<?php echo $cat['id']; ?>"> 
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Edit category</h4>
            </div>
            <div class="modal-body"> 
            <p id="category_success" style="display: none;color: #4CAF50"> Category Added  </p>        
               <div class="add-input-service">
               <input type="hidden" name="id" id="edit_category_id" value="<?php echo $cat['id']; ?>"> 
                  <label>Category name</label>
                  <input type="text" name="category_name" id="edit_category_name" 
                  required="required" value="<?php echo $cat['category_name']; ?>">
               </div>
                 <p id="category_error" style="display: none;color: red"> Category Required  </p>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="button" name="save" id="<?php echo $cat['id']; ?>" class="edit_service_category edit_category category_save" value="Save">Save</button>         
               <a href="javascript:;" data-dismiss="modal">Close</a>
            </div>
         </div>
          </form> 
      </div>
   </div>
<?php }} } ?>


   <div id="add-new-category_subscription" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog modal-add-category">
         <!-- Modal content-->
          <!-- <form action="<?php //echo base_url(); ?>proposal/add_category" method="post" name="form" id="login_form"> -->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Add new category</h4>
            </div>
            <div class="modal-body"> 
            <p id="categorysubscription_success" style="display: none;color: #4CAF50"> Category Added  </p> 
            <p id="category_check12" style="display:none;color:red !important;"> Category Already Exits  </p> 
                <input type="hidden" name="category_type" id="subscription_category_type" value="subscription">    
               <div class="add-input-service">
              
                  <label>Category name</label>
                  <input type="text" name="category_name" id="subscription_category_name" required="required">
               </div>
                 <p id="subscription_category_error" style="display: none;color: red"> Category Required  </p>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <input type="button" name="save" id="subscription_category_save" class="category_save" value="Save">              
               <a href="javascript:;" data-dismiss="modal">Close</a>
            </div>
         </div>
         <!--  </form> -->
      </div>
   </div>
<!-- Add Service Popup -->

<?php
if(count($category)>0){
foreach($category as $cat){
if($cat['category_type']=='subscription'){ ?>

   <div id="edit-category_subscription-<?php echo $cat['id']; ?>" class="modal fade new-adds-categories edit_subscriptioncategories" role="dialog">
      <div class="modal-dialog modal-add-category">
         <!-- Modal content-->
        <form action="<?php echo base_url(); ?>proposal/edit_category" method="post" name="form" id="edit_sub_cat<?php echo $cat['id']; ?>">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Edit category</h4>
            </div>
            <div class="modal-body"> 
            <p id="category_success" style="display: none;color: #4CAF50"> Category Added  </p>        
               <div class="add-input-service">
               <input type="hidden" name="id" id="edit_subscription_id" value="<?php echo $cat['id']; ?>"> 
                  <label>Category name</label>
                  <input type="text" name="category_name" id="edit_subscription_category_name" 
                  required="required" value="<?php echo $cat['category_name']; ?>">
               </div>
                 <p id="category_error" style="display: none;color: red"> Category Required  </p>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="button" name="save" id="<?php echo $cat['id']; ?>" class="category_save edit_subscription_category" value="Save"> Save</button>             
               <a href="javascript:;" data-dismiss="modal">Close</a>
            </div>
         </div>
         </form> 
      </div>
   </div>
<?php }} } ?>

<?php
  if(count($subscription)>0) {   
  foreach($subscription as $sub){ ?> 
  <div id="subscription_delete_<?php echo $sub['id']; ?>" class="modal fade new-adds-categories" role="dialog">
     <div class="modal-dialog  modal-delete-service">
        <!-- Modal content-->
        <form class="form-horizontal" action="<?php echo base_url(); ?>proposal_page/delete_subscription" method="post" id="login_form">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Delete Subscription</h4>
           </div>
           <div class="modal-body"> 
           <input type="hidden" name="checkbox[]" value="<?php echo $sub['id']; ?>">
            Do you want to delete?
           </div>
           <!-- moadl=body -->
           <div class="modal-footer">
              <button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Yes</button>        
              <a href="#" data-dismiss="modal">no</a>
           </div>
        </div>
         </form>
     </div>
  </div>
   <?php } } ?> 

<?php
if(count($category)>0){
foreach($category as $cat){
if($cat['category_type']=='product'){ ?>

   <div id="edit-category_product-<?php echo $cat['id']; ?>" class="modal fade new-adds-categories edit_productcategories" role="dialog">
      <div class="modal-dialog modal-add-category">
         <!-- Modal content-->
          <form action="<?php echo base_url(); ?>proposal/edit_category" method="post" name="form" id="edit_product_cate<?php echo $cat['id']; ?>">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Edit category</h4>
            </div>
            <div class="modal-body"> 
            <p id="category_success" style="display: none;color: #4CAF50"> Category Added  </p>        
               <div class="add-input-service">
               <input type="hidden" name="id" id="edit_product_id" value="<?php echo $cat['id']; ?>"> 
                  <label>Category name</label>
                  <input type="text" name="category_name" id="edit_product_categoryname" 
                  required="required" value="<?php echo $cat['category_name']; ?>">
               </div>
                 <p id="category_error" style="display: none;color: red"> Category Required  </p>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="button" name="save" id="<?php echo $cat['id']; ?>" class="category_save edit_product_category" value="Save">    Save</button>          
               <a href="javascript:;" data-dismiss="modal">Close</a>
            </div>
         </div>
         </form>
      </div>
   </div>
<?php }}} ?>


   <div id="add-new-category_product" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog modal-add-category">
         <!-- Modal content-->
          <!-- <form action="<?php //echo base_url(); ?>proposal/add_category" method="post" name="form" id="login_form"> -->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Add new category</h4>
            </div>
            <div class="modal-body"> 
            <p id="categoryproduct_success" style="display: none;color: #4CAF50"> Category Added  </p>  
            <p id="category_check1" style="display: none;color:#4CAF50;"> Category Already Exits  </p> 
                 <input type="hidden" name="category_type" id="product_category_type" value="product"> 
               <div class="add-input-service">
                  <label>Category name</label>
                  <input type="text" name="category_name" id="product_category_name" required="required">
               </div>
                 <p id="product_category_error" style="display: none;color: red"> Category Required  </p>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <input type="submit" name="save" id="product_category_save" class="category_save" value="Save">              
               <a href="javascript:;" data-dismiss="modal">Close</a>
            </div>
         </div>
         <!--  </form> -->
      </div>
   </div>

   

 <div id="add-new-services" class="modal fade new-adds-company" role="dialog">
      <div class="modal-dialog modal-proposal-service">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Add new services</h4>
            </div>
            <form action="<?php echo base_url(); ?>proposal/add_service" method="post" name="form1" id="add_services">
            <div class="modal-body">       
            <div class="error_msgs"></div>       
               <div class="annual-table floating_set border-checkbox-section">
                <!--   <div class="common-annual">
                     <div class="annual-field2">Displayed 10 services from 39</div>
                     <div class="annual-field3"><strong>Price </strong></div>
                     <div class="annual-field3"><strong>Qty</strong></div>
                     <div class="annual-field3"><strong>Unit</strong></div>
                  </div> -->
                 <?php include('service_add.php'); ?>
               </div>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="button" name="save" id="save_service" value="Save">Save</button>
               <a href="#" data-dismiss="modal">Close</a>
            </div>
            </form>
         </div>
         
      </div>
   </div>
<!-- Import Service Popup -->
<div id="import-service" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog modal-import-product">     
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal/import_service" method="post" name="upload_excel" enctype="multipart/form-data" id="importservice-excel">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Import Services Excel</h4>
            </div>
            <div class="modal-body"> 
			<div class="download-sample">
             <a href="<?php echo base_url(); ?>attachment/service.xls" download>Download Sample Excel File</a>            </div>
			<p style="color:red;">*Provide Distinct Names That Is Not In The Table List</p>
               <div class="add-input-service">
                  <label>Service Excel</label>
				  <div class="custom_upload">
                 <input type="file" name="file" id="file" class="input-large" required="required">
				 </div>
				 
               </div>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="submit" id="submit" name="Import" class="btn btn-primary button-loading">Upload</button>        
               <a href="javascript:;" data-dismiss="modal">Cancel</a>
            </div>
         </div>
          </form>
      </div>
   </div>

  <!-- Edit Service Popup --> 
    <?php
    if(count($services)>0){
      foreach($services as $service){ ?>
   <div id="service_edit_<?php echo $service['id']; ?>" class="modal fade new-adds-company service-edit add_alreadys" role="dialog">
      <div class="modal-dialog modal-proposal-service">
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal/edit_service" method="post" id="edit_service_section<?php echo $service['id']; ?>">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Edit Services</h4>
            </div>
            <div class="modal-body">    
            <div class="error_msgs"></div>          
               <div class="annual-table floating_set border-checkbox-section">
                <!--   <div class="common-annual">
                     <div class="annual-field2">Displayed 10 services from 39</div>
                     <div class="annual-field3"><strong>Price </strong></div>
                     <div class="annual-field3"><strong>Qty</strong></div>
                     <div class="annual-field3"><strong>Unit</strong></div>
                  </div> -->
                 <?php include('edit_service.php'); ?>
               </div>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="button" id="<?php echo $service['id']; ?>" name="save" class="btn btn-primary button-loading submit_category">Save</button>        
               <a href="javascript:;" data-dismiss="modal">Close</a>
            </div>
         </div>
          </form>
      </div>
   </div>
   <?php } }?>
   <!--- Service Delete Popup --> 
   <?php
   if(count($services)>0){
      foreach($services as $service){ ?>
   <div id="service_delete_<?php echo $service['id']; ?>" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog modal-delete-service">
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal/delete_service" method="post" id="login_form">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Delete Services</h4>
            </div>
            <div class="modal-body"> 
            <input type="hidden" name="checkbox[]" value="<?php echo $service['id']; ?>">
               Do you want to delete?
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Yes</button>        
               <a href="#" data-dismiss="modal">no</a>
            </div>
         </div>
          </form>
      </div>
   </div>
   <?php } }?>

<!-- Add Product -->

 <div id="add_product" class="modal fade new-adds-company add_alreadys" role="dialog">
      <div class="modal-dialog modal-proposal-service">
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal/add_product" method="post" id="product_add">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Add Product</h4>
            </div>
            <div class="modal-body modal-prop-ser-body"> 
            <div class="error_msgs"></div>
            <!--    <div class="common-annual">
                        <div class="annual-field2">Displayed 10 services from 39</div>
                        <div class="annual-field3"><strong>Price </strong></div>
                     </div> -->
                     <?php include('add_product.php'); ?>
                     </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="button" id="save_product" name="save" class="btn btn-primary button-loading">Save</button>        
               <a href="javascript:;" data-dismiss="modal">Close</a>
            </div>
         </div>
          </form>
      </div>
   </div>


   <div id="add_pricelist" class="modal fade new-adds-company " role="dialog">
      <div class="modal-dialog modal-price-service">
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal_pdf/add_pricelist" method="post" id="addpricelist">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Add Pricelist</h4>
            </div>
            <div class="modal-body"> 
            <div class="error_msgs"></div>  
              <!--  <div class="common-annual">
                        <div class="annual-field2">Displayed 10 services from 39</div>
                        <div class="annual-field3"><strong>Price </strong></div>
                     </div> -->
                  <div class="unit-annual floating_set">
                  <div class="common-annual lead-annual">
                     <div class="annual-field2 add-input-service">
                        <label>Name</label>     
                        <input type="text" name="pricelist_name" data-validation="required">    
                        <span id="name_er" style="color:red;"></span>   
                     </div>


                                    <div class="common-annual lead-annual">
                     <div class="add-input-service">
                        <label>Service Name</label>   
                       <div class="dropdown-sin-4 currency_details">

                        <select name="service_list[]" id="currency_symbol" data-validation="required" multiple="multiple" placeholder="Nothing Selected">
                        <option value="">Select Service</option>
                        <?php if(count($services)>0){
                        foreach($services as $service){
                        ?>
                        <option value="<?php echo $service['id']; ?>"><?php echo $service['service_name']; ?></option>


                        <?php  } } ?>
                        </select>

                        </div>
                        <span id="price_list_service_er"></span>
                        </div>
                        </div>
                         <div class="common-annual lead-annual clerbot" style="margin-top:0px;">
                          <div class=" add-input-service">
                             <label>Products</label>   

                          <div class="dropdown-sin-6 currency_details">

                        <select name="product_list[]" id="currency_symbol" class="" multiple="multiple" placeholder="Nothing Selected">
                        <option value="">Select Product</option>
                            <?php if(count($products)>0){
                        foreach($products as $product){
                        ?>
                        <option value="<?php echo $product['id']; ?>"><?php echo $product['product_name']; ?></option>
                        <?php  } }?>


                        </select>

                        </div>

                        </div>
                        </div>

                        <div class="common-annual lead-annual">
                        <div class=" add-input-service">
                           <label>Subscription</label>   



                          <div class="dropdown-sin-2 currency_details">

                        <select name="subscription_list[]" id="currency_symbol" class="" multiple="multiple" placeholder="Nothing Selected">
                        <option value="">Select Subscription</option>
                               <?php if(count($subscription)>0){
                        foreach($subscription as $sub){
                        ?>

                        <option value="<?php echo $sub['id']; ?>"><?php echo $sub['subscription_name']; ?></option>
                        

                        <?php  } } ?>  


                        </select>

                        </div>
                        </div>
                        </div>
     <!--  <div class="annual-field3 add-input-service"> 
         <label>Total Cost </label>    
         <input type="text" name="total_cost" required="required" class="decimal">
      </div> -->
     
   </div>
</div>                     </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Save</button>        
               <a href="javascript:;" data-dismiss="modal">Close</a>
            </div>
         </div>
          </form>
      </div>
   </div>
     
        <!-- Edit Price List -->
      <?php
      if(count($price_list)>0) {
      foreach($price_list as $price){ ?> 
         <div id="pricelist_edit_<?php echo $price['id'];?>" class="modal fade new-adds-company add_alreadys1" role="dialog">
           <div class="modal-dialog modal-price-list">
              <!-- Modal content-->
              <form class="form-horizontal edit_pricelist" action="<?php echo base_url(); ?>proposal_pdf/edit_pricelist" method="post" id="login_form">
              <input type="hidden" name="id" value="<?php echo $price['id']; ?>">
              <div class="modal-content">
                 <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Edit Pricelist</h4>
                 </div>
                 <div class="modal-body"> 
                 <div class="error_msgs"></div>  
                       <div class="unit-annual floating_set">
        <div class="common-annual lead-annual">
           <div class="annual-field2 add-input-service">
              <label>Name</label>     
              <input type="text" name="pricelist_name" data-validation="required" value="<?php echo $price['pricelist_name']; ?>">
              <span id="name_er" style="color:red;"></span>
           </div>
          <!--  <div class="annual-field3 add-input-service"> 
              <label>Service Name </label>    
              <input type="text" name="total_cost" required="required" class="decimal" value="<?php echo $price['total_cost']; ?>">
           </div> -->
          
        </div>
       <!--  <label>Service</label> -->
     <?php
           $service_name=explode(',',$price['service_name']); 
           $service_price=explode(',',$price['service_price']); 
           $service_unit=explode(',',$price['service_unit']); 
           $service_description=explode(',',$price['service_description']); 
           $service_qty=explode(',',$price['service_qty']); 
              if($service_name[0]!=''){ ?>

                        <div class="annual-field2 add-input-service">         
                          <label>Service</label>
                       </div>

                       <?php
           for($i=0;$i<count($service_name);$i++){
       ?>

           <div class="unit-annual floating_set">
                          <div class="common-annual lead-annual">
                             <div class="annual-field2 add-input-service">
                                <label>Name</label>     
                                <input type="text" name="pricelist_service_name[]" data-validation="required" value="<?php echo $service_name[$i]; ?>">
                             </div>
                             <div class="annual-field3 add-input-service"> 
                                <label>Price </label>      
                                <input type="text" name="service_price[]" class="decimal" data-validation="required" data-type="number"  value="<?php echo $service_price[$i]; ?>">
                             </div>
                             <div class="annual-field3 add-input-service">
                             <label>Qty</label>
                             <input type="text" name="service_qty[]" placeholder="1.00" class="decimal" data-validation="required" data-type="number" value="<?php echo $service_qty[$i]; ?>"></div>
                             <div class="annual-field3 add-input-service">
                                <label>Unit</label>
                                <select name="service_unit[]" data-validation="required">
                                <option value="per_service" <?php if($service_unit[$i]=='per_service'){ ?> selected="selected" <?php } ?>>Per Service</option>
                                <option value="per_day" <?php if($service_unit[$i]=='per_day'){ ?> selected="selected" <?php } ?>>Per Day</option>
                                <option value="per_month" <?php if($service_unit[$i]=='per_month'){ ?> selected="selected" <?php } ?>>Per Month</option>
                                <option value="per_2month" <?php if($service_unit[$i]=='per_2month'){ ?> selected="selected" <?php } ?>>Per 2Months</option>
                                <option value="per_6month" <?php if($service_unit[$i]=='per_6month'){ ?> selected="selected" <?php } ?>>Per 6Month</option>
                                <option value="per_year" <?php if($service_unit[$i]=='per_year'){ ?> selected="selected" <?php } ?>>Per Year</option>
                                </select>
                             </div>
                             <div class=" spacing-annual  add-input-service">
                                <label>Description</label>
                                <textarea rows="3" name="service_description[]" id="desc-price-list" data-validation="required"><?php echo $service_description[$i]; ?></textarea>
                             </div>
                          </div>
                       </div>
                       <?php } } ?>

                    <!-- <label>Products</label> -->
                    <?php 
                       $product_name=explode(',',$price['product_name']); 
                       $product_price=explode(',',$price['product_price']);                 
                       $product_description=explode(',',$price['product_description']); 
                       $product_qty=explode(',',$price['product_qty']); 
                        if($product_name[0]!=''){ ?>

                        <div class="annual-field2 add-input-service">         
                          <label>Products</label>
                       </div>

                       <?php
                       for($i=0;$i<count($product_name);$i++){ 
                    ?>
                       <div class="unit-annual floating_set">
                          <div class="common-annual lead-annual">
                             <div class="annual-field2 add-input-service">
                                <label>Name</label>     
                                <input type="text" name="pricelist_product_name[]" data-validation="required" value="<?php echo $product_name[$i]; ?>">
                             </div>
                             <div class="annual-field3 add-input-service"> 
                                <label>Price </label>    
                                <input type="text" name="product_price[]" data-validation="required" data-type="number" class="decimal" value="<?php echo $product_price[$i]; ?>">
                             </div>
                             <div class=" spacing-annual  add-input-service">
                                <label>Description</label>
                                <textarea rows="3" name="product_description[]" data-validation="required"><?php echo $product_description[$i]; ?></textarea>
                             </div>  
                          </div>
                       </div>
                    <?php }} ?>

                    
                       <?php 
                       $subscription_name=explode(',',$price['subscription_name']); 
                       $subscription_price=explode(',',$price['subscription_price']);                 
                       $subscription_description=explode(',',$price['subscription_description']); 
                       $subscription_unit=explode(',',$price['subscription_unit']);

                       if($subscription_name[0]!=''){ ?>

                        <div class="annual-field2 add-input-service">         
                          <label>Subscription</label>
                       </div>
                       



                      <?php for($i=0;$i<count($subscription_name);$i++){ 
                       ?>

                    <div class="unit-annual floating_set">
                    <div class="common-annual lead-annual">
                       <div class="annual-field2 add-input-service">
                          <label>Name</label>     
                          <input type="text" name="pricelist_subscription_name[]" data-validation="required" value="<?php echo $subscription_name[$i]; ?>">
                       </div>
                       <div class="annual-field3 add-input-service"> 
                          <label>Price </label>    
                          <input type="text" name="subscription_price[]" data-validation="required" data-type="number" class="decimal" value="<?php echo $subscription_price[$i]; ?>">
                       </div>
                       <div class="annual-field3 add-input-service"> 
                          <label>Periods</label>    
                          <select name="subscription_unit[]" data-validation="required">
                          <option value="per_hour" <?php if($subscription_unit[$i]=='per_hour'){ ?> selected="selected" <?php } ?>>Per Hour</option>
                          <option value="per_day" <?php if($subscription_unit[$i]=='per_day'){ ?> selected="selected" <?php } ?>>Per Day</option>
                          <option value="per_week" <?php if($subscription_unit[$i]=='per_week'){ ?> selected="selected" <?php } ?>>Per Week</option>
                          <option value="per_month" <?php if($subscription_unit[$i]=='per_month'){ ?> selected="selected" <?php } ?>>Per Month</option>
                          <option value="per_2month" <?php if($subscription_unit[$i]=='per_2month'){ ?> selected="selected" <?php } ?>>Per 2Month</option>
                          <option value="per_5month" <?php if($subscription_unit[$i]=='per_5month'){ ?> selected="selected" <?php } ?>>Per 5month</option>
                          <option value="per_year" <?php if($subscription_unit[$i]=='per_year'){ ?> selected="selected" <?php } ?>>Per Year</option>
                          </select>
                       </div>
                       <div class=" spacing-annual  add-input-service">
                          <label>Description</label>
                          <textarea rows="3" name="subscription_description[]" data-validation="required"><?php echo $subscription_description[$i]; ?></textarea>
                       </div>
                     
                    </div>
                 </div>

                    <?php }} ?>

                    </div>     
                  </div>
                 <!-- moadl=body -->
                 <div class="modal-footer">
                    <button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Save</button>        
                    <a href="javascript:;" data-dismiss="modal">Close</a>
                 </div>
              </div>
               </form>
           </div>
        </div>
         <?php } } ?> 

   <!-- Import Products Excel -->
   <div id="import-product" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog modal-import-product">
       
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal/import_products" method="post" name="upload_excel" enctype="multipart/form-data" id="login_form">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Import Products Excel</h4>
            </div>
            <div class="modal-body"> 
              <div class="download-sample">
             <a href="<?php echo base_url(); ?>attachment/Product-list.xls" download>Download Sample Excel File</a>      
             </div>  
             <p style="color:red;">*Provide Distinct Names That Is Not In The Table List</p>  
               <div class="add-input-service">
                  <label>Products Excel</label>
                  <div class="custom_upload">
                     <input type="file" name="file" id="file" class="input-large" required="required">
                  </div>
               </div>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="submit" id="submit" name="Import" class="btn btn-primary button-loading">Upload</button>        
               <a href="javascript:;" data-dismiss="modal">Cancel</a>
            </div>
         </div>
          </form>
      </div>
   </div>

   <!-- Edit Service Popup --> 
    <?php
    if(count($products)>0){
      foreach($products as $product){ ?>
   <div id="products_edit_<?php echo $product['id']; ?>" class="modal fade new-adds-company product_edit" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal/edit_products" method="post" id="edit_product_section<?php echo $product['id']; ?>">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Products Edit</h4>
            </div>
            <div class="modal-body">      
            <div class="error_msgs"></div>        
               <div class="annual-table floating_set border-checkbox-section">
                  <!-- <div class="common-annual">
                     <div class="annual-field2">Displayed 10 services from 39</div>
                     <div class="annual-field3"><strong>Price </strong></div>
                     <div class="annual-field3"><strong>Qty</strong></div>
                     <div class="annual-field3"><strong>Unit</strong></div>
                  </div> -->
                 <?php include('edit_product.php'); ?>
               </div>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <a href="javascript:;" data-dismiss="modal">Cancel</a>
               <button type="button" id="<?php echo $product['id']; ?>" name="save" class="btn btn-primary button-loading submit_product">Save</button>        
            </div>
         </div>
          </form>
      </div>
   </div>
   <?php } }?>
   <!--- Service Delete Popup --> 
   <?php
     if(count($products)>0){
      foreach($products as $product){ ?>
   <div id="products_delete_<?php echo $product['id']; ?>" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog modal-delete-service">
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal/delete_products" method="post" id="login_form">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Delete Services</h4>
            </div>
            <div class="modal-body"> 
            <input type="hidden" name="checkbox[]" value="<?php echo $product['id']; ?>">
               Do you want to delete?
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Yes</button>        
               <a href="#" data-dismiss="modal">no</a>
            </div>
         </div>
          </form>
      </div>
   </div>
   <?php } } ?>




   <?php
if(count($taxes)>0){
$i=1;
foreach($taxes as $tax){ ?>  
 <div id="tax_delete_<?php echo $tax['id']; ?>" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog  modal-delete-service">
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal_page/delete_tax" method="post" id="login_form">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Delete Tax</h4>
            </div>
            <div class="modal-body"> 
            <input type="hidden" name="checkbox[]" value="<?php echo $tax['id']; ?>">
               Do you want to delete?
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Yes</button>        
               <a href="#" data-dismiss="modal">no</a>
            </div>
         </div>
          </form>
      </div>
   </div>
<?php } }?>

<div id="import-price_list" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog modal-import-product">     
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal_page/import_price_list" method="post" name="upload_excel" enctype="multipart/form-data" id="importservice-excel">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Import Price list Excel</h4>
            </div>
            <div class="modal-body"> 
             <div class="download-sample">
             <a href="<?php echo base_url(); ?>attachment/Pricelist.xls" download>Download Sample Excel File</a>    </div>   
             <p style="color:red;">*Provide Distinct Names That Is Not In The Table List</p>   
               <div class="add-input-service">
                  <label>Pricelist Excel</label>
                  <div class="custom_upload">
                    <input type="file" name="file" id="file" class="input-large" required="required">
                  </div>
               </div>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="submit" id="submit" name="Import" class="btn btn-primary button-loading">Upload</button>        
               <a href="javascript:;" data-dismiss="modal">Cancel</a>
            </div>
         </div>
          </form>
      </div>
   </div>
  
  <?php
   if(count($price_list)>0){
   foreach($price_list as $price){ ?> 
   <div id="pricelist_delete_<?php echo $price['id']; ?>" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog  modal-delete-service">
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal_pdf/delete_pricelist" method="post" id="login_form">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Delete Price List</h4>
            </div>
            <div class="modal-body"> 
            <input type="hidden" name="checkbox[]" value="<?php echo $price['id']; ?>">
               Do you want to delete?
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Yes</button>        
               <a href="#" data-dismiss="modal">no</a>
            </div>
         </div>
          </form>
      </div>
   </div>
  <?php } } ?> 

<div id="import-tax_list" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog modal-import-product">     
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal_page/import_tax_list" method="post" name="upload_excel" enctype="multipart/form-data" id="importservice-excel">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Import Tax list Excel</h4>
            </div>
            <div class="modal-body"> 
             <div class="download-sample"><a href="<?php echo base_url(); ?>attachment/Tax-list.xls" download>Download Sample Excel File</a>  </div>  
             <p style="color:red;">*Provide Distinct Names That Is Not In The Table List</p>      
               <div class="add-input-service">
                  <label>Tax Excel</label>
                  <div class="custom_upload">
                  <input type="file" name="file" id="file" class="input-large" required="required"></div>
               </div>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="submit" id="submit" name="Import" class="btn btn-primary button-loading">Upload</button>        
               <a href="javascript:;" data-dismiss="modal">Cancel</a>
            </div>
         </div>
          </form>
      </div>
   </div>


<div id="import-subscription" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog modal-import-product">

         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal_page/import_subscription" method="post" name="upload_excel" enctype="multipart/form-data" id="login_form">
         <div class="modal-content">

            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Import Subscription Excel</h4>
            </div>
            <div class="modal-body">  
              <div class="download-sample">
             <a href="<?php echo base_url(); ?>attachment/Subscription-list.xls" download>Download Sample Excel File</a></div>
             <p style="color:red;">*Provide Distinct Names That Is Not In The Table List</p>
               <div class="add-input-service">
                  <label>Subscription Excel</label>
                  <div class="custom_upload">
                     <input type="file" name="file" id="file" class="input-large" required="required">
                  </div>
               </div>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="submit" id="submit" name="Import" class="btn btn-primary button-loading">Upload</button>        
               <a href="javascript:;" data-dismiss="modal">Cancel</a>
            </div>
         </div>
          </form>
      </div>
   </div>


<div id="add-new-subscriptions" class="modal fade new-adds-company" role="dialog">
      <div class="modal-dialog modal-proposal-service">
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url(); ?>proposal_page/add_subscription" method="post" id="subscription_add">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Add Subscription</h4>
            </div>
            <div class="modal-body modal-subcription-body"> 
            <div class="error_msgs"></div>
               <!-- <div class="common-annual">
                        <div class="annual-field2">Displayed 10 services from 39</div>
                        <div class="annual-field3"><strong>Price </strong></div>
                     </div> -->
                      <div class="annual-table floating_set border-checkbox-section">
                     <?php include('add_subscription.php'); ?>
                      </div>
                     </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <button type="button" id="save_subscriptions" name="save" class="btn btn-primary button-loading">Save</button>        
               <a href="javascript:;" data-dismiss="modal">Close</a>
            </div>
         </div>
          </form>
      </div>
   </div>


<?php
  if(count($subscription)>0){
  foreach($subscription as $sub){ ?>
  <div id="subscription_edit_<?php echo $sub['id']; ?>" class="modal fade new-adds-company subscription_edit add_alreadys" role="dialog">
     <div class="modal-dialog">
        <!-- Modal content-->
        <form data-sub-form-validate="true" class="form-horizontal" action="<?php echo base_url(); ?>proposal_page/edit_subscription" method="post" id="edit_subscription_section<?php echo $sub['id']; ?>">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Edit Subscription</h4>
           </div>
           <div class="modal-body">      
           <div class="error_msgs"></div>        
              <div class="annual-table floating_set border-checkbox-section">
               
                <?php include('edit_subscription.php'); ?>
              </div>
           </div>
           <!-- moadl=body -->
           <div class="modal-footer">
              <a href="javascript:;" data-dismiss="modal">Cancel</a>
              <button type="button" id="<?php echo $sub['id']; ?>" name="save" class="btn btn-primary button-loading submit_subscription">Save</button>        
           </div>
        </div>
         </form>
     </div>
  </div>
   <?php } }?> 
 




  