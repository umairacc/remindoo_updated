<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <style>
 
@font-face {
    font-family: 'pop-regular';
    /*src: url('http://remindoo.co/demo/assets/pdf-fonts/Poppins-Regular.ttf') format('truetype');*/
    src: <?php echo $_SERVER['DOCUMENT_ROOT'].'/assets/pdf-fonts/Poppins-Regular.ttf'; ?> format("truetype");
    font-weight: 400; 
    font-style: normal;
}
@font-face {
    font-family: 'pop-medium';
    /*src: url('http://remindoo.co/demo/assets/pdf-fonts/Poppins-Regular.ttf') format('truetype');*/
    src: <?php echo $_SERVER['DOCUMENT_ROOT'].'/assets/pdf-fonts/Poppins-Medium.ttf'; ?> format("truetype");
    font-weight: 400; 
    font-style: normal;
}
@font-face {
    font-family: 'pop-semi';
    src: <?php echo $_SERVER['DOCUMENT_ROOT'].'/assets/pdf-fonts/Poppins-SemiBold.ttf'; ?> format("truetype");
font-weight: 400; 
font-style: normal;
}
body,.table-scroll{
  font-family: 'pop-regular';
}

  input[type=checkbox] { display: inline; }
  /*         input[type=checkbox]:before { font-family: DejaVu Sans; }*/
  table {
   width: 100%;
 }

 .header-top{
   padding-bottom: 10px;
   position: fixed;
   top: -45px;
   left: -45px;
   width: 100%;
 }
 .account-tax{
   width: 200px;
   height: 70px;
   display: table;
   margin:0 auto;
   position: relative;
 }
 .pdf-image{
   display: table-cell;
   text-align: center;
   vertical-align: middle;
 }
 .pdf-title{
   padding:220px 30px 15px;
   text-align: center;
 }
 .prepared{
   margin: 70px 0 0;
   color: #777;
 }
 .prepared {
   text-align: right;
   float: right;
   padding-right: 0;
 }
 .inline-table{
   max-width: 540px;
   text-align: left;
 }
 .pdf-title h2 {
   margin-bottom: 50px;
   font-size: 25px;
   font-weight: 600;
   color: #000;
 }
 p{
   font-size: 14px;
   /*line-height: 18px;*/
   margin: 6px 0;
 }
 .first-blog h3 {
   margin-bottom: 8px;
   color: #333;
   font-size: 16px;
 }
 .first-blog  a{
   color: #333;
 }
 .first-blog p{
   margin: 3px 0;
   padding-left: 25px;
 }
 .pdf-table h2 {
   font-weight: 600;
   font-size: 22px;
   margin: 0 0 12px;
 }
 .table-scroll {
   border: 1px solid #e3e0e1;
 }
 span.service-title {
   background: #d8d8d8;
   width: 100%;
   display: block;
   padding: 10px 15px;
   font-weight: 600;
   color: #333;
 }

 .remove-space{
   padding: 0;
 }

 p{
   font-size: 13px;
   /*line-height: 21px;*/
   text-align: justify;
 }
 .content-blog{
   margin: 20px 0;
 }
 .grand-total p {
   text-align: right;
 }
 .grand-total {
   text-align: right;
   padding: 10px 20px;
 }
 .grand-total span {
   min-width: 101px;
   display: inline-block;
   width: 110px;
   text-align: left;
 }
 .grand-total span ,.grand-total strong{
   display: inline-block;
   vertical-align: top;
   vertical-align: middle;
   line-height: 10px;
 }
 .grand-total p{
   text-align: right;
   display: block;
 }
 .grand-total .boder-top {
   padding-top: 10px;
   border-top: 1px solid #dbdbdb;
   font-weight: 600;
   margin-top: 15px;
   font-size: 14px;
   display: block;
 }
 .sig-img,.sig-right{
   width: 49%;
   display: inline-block;
   vertical-align: top;  
 }
 .sig-right{
   text-align: right;
 }
/* .signature-blog p{
   margin-top: 35px;
   height: 34px;
   padding-bottom: 8px;
 }*/
 .signature-blog table{
   width: 100%;
 }
 .signature-blog span{
   display: block;
   font-size: 14px;
   color: #000;
   margin-top: 10px;
 }
 .upper{
   text-transform: uppercase;
 }
 .signature-blog td:first-child{
   width: 60%;
 }
 .signature-blog td:last-child{
   width: 40%;
 } 
 .pdf-table .table-scroll{
   margin-top: 20px;
 }

 .pdf-table .table-scroll td{
  vertical-align: top;
}   
.col-span1,.col-span2,.col-span3,.col-span4,.row1,.row3{
  display: inline-block;
  text-align: left;
  vertical-align: top;
}

.col-span1{
  width: 294.8px;
}
.col-span2{
  width: 129px;
}
.col-span3{
  width:76.5px;
}
.col-span4{
  width: 79px;
}  
.row3 {
  width: 550px;
  border-right: 1px solid #dbdbdb;
}
.row1{
  width: 90px;
}
.col-span1,.col-span2,.col-span3,.col-span4{
 border-right: 1px solid #dbdbdb;
 font-size: 13px;
}
.tab-topbar{
  font-size: 15px;
  font-weight: normal;
  display: block;
  font-weight: 600;
  vertical-align: top;
  border-bottom: 1px solid #dbdbdb;
}   
.col-span4:last-child{
  border-right: none;
} 
.cmn-spaces,.col-span1,.col-span2,.col-span3,.col-span4{
  padding: 7px;
  font-size: 13px;
}
.full-acc{
  border-bottom: 1px solid #dbdbdb;
}
.col-span3:last-child{
  border-right: none;
}
.tab-topbar .col-span3{
  width: 78px;
}
.cmn-spaces,.price-row,.full-acc{
  border-bottom: 1px solid #dbdbdb;
}
.cmn-spaces:last-child,.price-row:last-child,.col-span1,.col-span2,.col-span3{
  border-bottom: none;
}
div *{
  line-height: 19px;
}
.row1 {
  color: #22b14c ;
} 

.tab-topbar span{
      font-family: 'pop-medium';
}
p{
  line-height: 4px;
}
.full-acc{
  display: table;
}
body{
  word-wrap: break-word;
}
.full-acc div{
   display: table-cell;
    /*width: 33.33%;  depends on the number of columns */
}
.row3 .col-span1{
  width: 282px;
}
</style>
</head>
<body>
  


  <div class="pdf-table">
   <p style="font-family: 'pop-regular';line-height: 11px;font-size: 16px;">Our Fee For the Required Services</p>

 
    <?php // echo trim(html_entity_decode($records['proposal_contents']));

   // print_r($records);exit;

    echo trim(preg_replace('/[^(\x20-\x7F)]*/','', $records['pdf_content']));

    // echo trim(html_entity_decode($records['pdf_content']));

    // echo trim(preg_replace("/[\r\n]+/", " ", $records['pdf_content']));   

     ?> 

   </div> 
      

<?php  if(isset($records['accept_signature']) && !empty($records['accept_signature'])){ ?>

    <div class="signature-blog">

  <table width="100%" style="margin-top: 15px;">
    <tr>
    <td style="width: 50%;">
      <p>Authorized Signature</p>
    <span class="upper"><img src="<?php echo base_url().$records['accept_signature']; ?>" style="width:150px !important;height:60px !important;"></span>
  
    </td>

      <?php if($records['accept_date']!=0 && $records['accept_date']!='' ) { ?>
    <td style="width: 50%;">
<div style="padding-left: 80px;">      <p style="line-height: 7px;margin-bottom: 0;">Signed Date</p>
    <span class="upper"><?php echo date("Y-m-d h:i:s" ,  $records['accept_date']) ?></span></div>
    </td>
  
   <?php } ?>

  </tr>
  </table>
 </div>
 <?php }else if(isset($records['email_signature']) && $records['email_signature'] == "on"){ ?>

         <div class="signature-blog">
           <table width="100%" style="margin-top: 15px;">
             <tr>
             <td style="width: 50%;">
               <p>Signature</p>
             <span class="upper"><img src="<?php echo base_url().$signature['signature_path']; ?>" style="width:150px !important;height:60px !important;"></span>  
             </td>
             </tr>
           </table>
         </div>
  <?php }   ?>
   

  <!-- <main>
    <p>page1</p>
    <p>page2></p>
  </main>
 --></body>
</html>