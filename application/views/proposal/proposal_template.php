<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   ?>
<style> 
   .columns > .editor {
   float: left;
   width: 66%;
   position: relative;
   z-index: 1;
   }
   .columns > .contacts {
   float: right;
   width: 34%;
   box-sizing: border-box;
   } 
   .my-error-class {
   border-color:1px red;
   }
   #editor1.price_table{
    display: none;
   }
  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
 }
</style>
<div class="pcoded-content pcodedteam-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
             <?php echo $this->session->flashdata('alert-currency'); ?>    
                  <!--start-->
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card propose-template">
                        <!-- admin start-->
                           <div class="modal-alertsuccess alert alert-success" style="display:none;">
                               <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    YOU HAVE SUCCESSFULLY ADDED TEMPLATE
                                 </div>
                              </div>
                              </div>
                           </div>
                           <div class="modal-alertsuccess alert alert-success-update" style="display:none;">   
                            <div class="newupdate_alert">                    
                              <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    YOU HAVE SUCCESSFULLY UPDATED                              
                                 </div>
                              </div>
                           </div>
                           </div>
                           <div class="modal-alertsuccess alert alert-danger required" style="display:none;">
                               <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Please Fill All the required fills.
                                 </div>
                              </div>
                           </div>
                           </div>
                            <div class="modal-alertsuccess alert alert-danger alert-danger-exists" style="display:none;">
                                <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Template Name Already Exists
                                 </div>
                              </div>
                              </div>
                           </div>
                           <div class="modal-alertsuccess alert alert-danger alert-danger-check" style="display:none;">
                               <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Please select records.
                                 </div>
                              </div>
                              </div>
                           </div>
                           <div class="modal-alertsuccess alert alert-danger alert-success-delete" style="display:none;">
                               <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    YOU HAVE SUCCESSFULLY DELETED                              
                                 </div>
                              </div>
                              </div>
                           </div>
                           <!-- List -->

                           <div class="deadline-crm1 floating_set">
                    <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard">
                      <li class="nav-item">
                        <a class="nav-link "  href="<?php echo base_url(); ?>proposal">dashboard</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link "  href="<?php echo base_url(); ?>proposal_page/proposal_template">proposal</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link"  href="<?php echo base_url(); ?>Proposal/fee_schedule">Package</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link"  href="<?php echo base_url(); ?>proposal_page/templates">templates</a>
                        <div class="slide"></div>
                      </li>
                     <!--  <li class="nav-item">
                        <a class="nav-link"  href="<?php echo base_url(); ?>proposal_page/step_proposal">Create Proposal</a>
                        <div class="slide"></div>
                      </li> -->
                      <li class="nav-item">
                        <a class="nav-link active"  href="<?php echo base_url(); ?>proposal/template">settings</a>
                        <div class="slide"></div>
                      </li>
                    </ul> 

                    <div class="setting-realign1">
                    <div class="count-value1 cv pull-right">
                                       <a class="btn btn-primary" id="add_template" href="javascript:;" data-toggle="modal" data-target="#myTemplate">Add New Template</a>  
                                   
                                       <button type="button" id="delete_template_records" name="submit" class="btn btn-danger f-right template_delete" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>
                                      
                                       <!-- <a type="button"  class="btn btn-danger f-right">Delete</a> -->
                                       <div class="create-proposal pro-dash">
                                       
                                      <!-- <a href="<?php echo base_url();?>proposal_page/new_proposal" class="create-proposalbtn btn btn-success">Add Email Template</a> -->

                                      <a class="btn btn-success create-proposalbtn" href="<?php echo base_url().'Email_template/add_template'; ?>" data-toggle="" data-target="" >Add Email Template</a> 


                                        <!--  <div class="create-proposal pro-dash"> -->
                      <a href="<?php echo base_url();?>proposal_page/step_proposal" class="create-proposalbtn btn btn-primary">create proposal</a>
                 <!--    </div> -->

                                     <!--  <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_currency">Add Curency</a> -->
                                              </div>  

                    </div>
                   

                     </div>
                   </div>
                    
              <form action="<?php echo base_url(); ?>proposal/template_delete" class="require-validation" method="post">
                    
                    
                                   
                              <div class="all_user-section floating_set pros-editemp">
                                 <!-- <div class="deadline-crm1 floating_set">

                                    <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                       <li class="nav-item">
                                          Email Template
                                          <div class="slide"></div>
                                       </li>
                                    </ul>
                                    
                                 </div> -->
                                 <div class="all_user-section6 floating_set">

                                    <div class="tab-content">
                                       <div id="allusers" class="tab-pane fade in active">
                                          <div id="task"></div>
                                          <div class="client_section3 table-responsive">
                                             <div class="status_succ"></div>
                                             <div class="all-usera1 data-padding1 ">
                                             
                                                <div class="">
                                                   <table class="table client_table1 text-center display nowrap printableArea" id="display_service1" cellspacing="0" width="100%">
                                                      <thead>
                                                         <tr class="text-uppercase">
                                                            <th>

                                                              <label class="custom_checkbox1">
                                                                  <input type="checkbox" id="select_all_template">
                                                                  <i></i>   
                                                                  </label>
                                                              
                                                             </th>
                                                            <th>S.no
                                                             <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                                        <select multiple="true" searchable="true" class="filter_check" id="" style="display: none;"> </select>
                                        </th>
                                                            <th>Template Title
                                                             <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                                        <select multiple="true" searchable="true" class="filter_check" id="" style="display: none;"> </select>
                                        </th>
                                                            <th>Template Type
                                                             <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                                        <select multiple="true" searchable="true" class="filter_check" id="" style="display: none;"> </select>
                                        </th>
                                                            <th>Status
                                                             <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                                        <select multiple="true" searchable="true" class="filter_check" id="" style="display: none;"> </select>
                                        </th>
                                                            <th>Created At
                                                             <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                                        <select multiple="true" searchable="true" class="filter_check" id="" style="display: none;"> </select>
                                        </th>
                                                            <th>Status Action
                                                             <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                                        <select multiple="true" class="filter_check" searchable="true" id="" style="display: none;"> </select></th>
                                                            <th>Action</th>
                                                         </tr>
                                                      </thead>


                                                        <tfoot>
                    <tr class="table-header">
                    <th></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th ><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th ><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th ></th>
                    <th></th>
                   
                    </tr>
                  </tfoot>
                                                      <tbody>
                                                         <?php
                                                            $i=1;
                                                            foreach($result as $value){ ?>
                                                         <tr>
                                                            <td>
                                                               <label class="custom_checkbox1">
                                                                
                                                                  <input type="checkbox" class="template_checkbox" data-template-id="<?php echo $value['id'];?>">
                                                                   <i></i>   
                                                                  </label>
                                                              
                                                            </td>
                                                            <td><?php echo $i; ?></td>
                                                            <td><?php echo $value['template_title']; ?></td>
                                                            <td>
                                                               <?php $strlower = ucwords(preg_replace("~[\\\\/:*?'&,<>|]~", '', $value['template_type']));
                                                                  $lesg = str_replace('_', ' ', $strlower);
                                                                  echo $lesg; 

                                                              $email_actions = $this->db->query("SELECT * FROM email_templates_actions WHERE module = 'proposal'")->result_array();
                                                              
                                                              $template_type = "";
                                                              
                                                              foreach($email_actions as $key1 => $value1) 
                                                              {
                                                                  if(ucwords(str_replace('_', ' ', $value1['action'])) == $value['template_type'])
                                                                  {
                                                                      $template_type = $value1['action_id'];
                                                                  }
                                                              }
                                                                  ?>                                    
                                                            </td>
                                                            <td><?php echo $value['status']; ?></td>
                                                            <td><?php echo $value['created_at']; ?></td>
                                                            <td>
                                                               <select name="status" class="test btn btn-xs green dropdown-toggle" id="<?php echo $value['id']; ?>" > 
                                                          <?php $status_action=$value['status'];?>    
                                                                  <option value="none">Select</option>
                                                                  <option value="active" <?php if($status_action == 'active'){ echo "selected='selected'"; } ?>>Active</option>
                                                                  <option value="inactive" <?php if($status_action == 'inactive'){ echo "selected='selected'"; } ?>>In active</option>
                                                               </select>
                                                            </td>
                                                            <td>
                                                            <!-- <a href="<?php echo base_url().'proposal/update_contents/'.$value['id'];?>"><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Edit</a> -->
                                                            <!-- <a href="#" class="edit_Template" data-toggle="modal" data-target="#editTemplate"><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Edit</a>  -->
                                                            <a href="#" class="edit_Template" data-toggle="modal" data-target="#myTemplate"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> </a> 

                          <input type="hidden" class="edit_templateID" value="<?php echo $value['id'];?>">                                                           
                          <input type="hidden" class="edit_templateTitle" value="<?php echo $value['template_title'];?>">
                          <input type="hidden" class="edit_templateType" value="<?php echo $template_type;?>">
                          <div class="edit_templateContent" id="edit_templateContent" style="display: none;">
                            <?php echo $value['template_content'];?>
                          </div>                                
                                                            </td> 

              <div class="modal-alertsuccess alert alert-success1" id="delete_user<?php echo $value['id'];?>" style="display:none;">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_yes"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
                  </div></div>                                                 
                                                         </tr>
                                                         <?php $i++; }  ?>   
                                                      </tbody>
                                                   </table>
                                              <input type="hidden" class="rows_selected" id="select_template_count" >     
                                                </div>
                                             </div>
                                             <!-- List -->           
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        <!-- Page body end -->
                     </div>
                  </div>
                  <!-- Main-body end -->
                  <div id="styleSelector">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Email Template Popup -->
<div class="modal fade new-adds-company" id="myTemplate" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Template</h4>
         </div>
         <div class="modal-body">
            <form id="proposal_template" class="require-validation" method="post">           
               <?php include('proposal_mail_content.php'); ?>
            </form>
         </div>
         <div class="modal-footer">
            <button id="new_template" >Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

<!-- <div class="modal fade new-adds-company" id="addEmailTemplate" role="dialog">
   <div class="modal-dialog"> -->
      <!-- Modal content-->
      <!-- <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Email Template</h4>
         </div>
         <div class="modal-body">
            <form id="new_proposal" name="new_proposal" class="require-validation" method="post">

                <?php //include('new_proposal1.php'); ?>

            </form>
         </div>
         <div class="modal-footer">
            <button id="add_proposal">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div> -->


<form action="<?php echo base_url()?>proposal/addCurrency" name="currency_form" id="currency_form" method="post">
<div class="modal fade" id="add_currency">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Currency</h4>
         </div>
         <div class="col-xs-12 inside-popup">
            <div class="col-xs-12 col-md-5">
              <label>Currency :</label>
            </div>
             <div class="col-xs-12 col-md-7">
                <input type="text" name="currency" id="currency" data-parsley-required="true">
            </div>
         </div>
         <div class="modal-footer">
            <button id="submit">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
</form>

<!-- ajax loader -->
<!-- <div class="LoadingImage" ></div> -->
<!-- <style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style> -->

<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<!-- <script src="https://cdn.ckeditor.com/4.9.0/standard-all/ckeditor.js"></script> -->
<!-- <script src="https://cdn.ckeditor.com/4.9.0/standard-all/ckeditor.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script src="<?php echo base_url()?>assets/js/user_page/materialize.js"></script> 
 


 <?php //include('new_proposal_script.php'); ?>  

<script>
   // $(document).ready(function() {
   //     $('#display_service1').DataTable({
   //       "scrollX": true
   //   });
   //   });


   $(document).ready(function(){    

    var check=0;
    var check1=0;
    var numCols = $('#display_service1 thead th').length;
        var table10 = $('#display_service1').DataTable({
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=1;
                     $('#display_service1 thead th').find('.filter_check').each( function(){
                     // alert('ok');
                       $(this).attr('id',"all_"+q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){                 
                        var select = $("#all_"+i); 
                      
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                      //    console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 

                   
           
                     $("#all_"+i).formSelect();  
                  }
        }
    });
      for(j=1;j<numCols;j++){  
          $('#all_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#all_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');    
             

              table10.column(c).search(search, true, false).draw();  
          });
       }

    });
</script>

<script type="text/javascript">
   $(document).ready(function(){
      $( "#proposal_template" ).validate({
          errorClass: "my-error-class",
        rules: {
          template_title: "required",  
         // template_content: "required",  
          template_type: "required"
        },
        messages: {
          template_title: "Please enter your Title",
          //template_content: "Please enter your Content",
          template_type: "Please Select Template Type"
        },        
      });
   });
    CKEDITOR.replace( 'editor1', {
        extraPlugins: 'divarea,hcard,sourcedialog,justify'
    });
  //  CKEDITOR.replace( 'editor1', {
  // // extraPlugins: 'divarea,hcard'
  //  });
   'use strict';
   var CONTACTS = [
   { name: 'Username' },
   { name: 'Client Name' },
   { name: 'Accountant Name' },
   { name: 'Task Number' },
   { name: 'Task Name' },
   { name: 'Task Due Date'},
   { name: 'Task Client' },
   { name: 'Firm Name' },
   { name: 'accountant number' },
   { name: 'invoice amount' },
   { name: 'price table' }, 
   { name: 'invoice due date' },      
   ];
   CKEDITOR.disableAutoInline = true;   
   CKEDITOR.plugins.add( 'hcard', {
   requires: 'widget',
   init: function( editor ) {
    editor.widgets.add( 'hcard', {
      allowedContent: 'span(!h-card); a[href](!u-email,!p-name); span(!p-tel)',
      requiredContent: 'span(h-card)',
      pathName: 'hcard',
      upcast: function( el ) {
        return el.name == 'span' && el.hasClass( 'h-card' );
      }
    } );        
    editor.addFeature( editor.widgets.registered.hcard );       
    editor.on( 'paste', function( evt ) {
      var contact = evt.data.dataTransfer.getData( 'contact' ); 
      if ( !contact ) {
        return;
      }
      evt.data.dataValue =            
        '<span class="content h-card">' +
          '['+contact.name+']'  +
        '</span>';
    } );
   }
   });
   CKEDITOR.on( 'instanceReady', function() {     
   CKEDITOR.document.getById( 'contactList' ).on( 'dragstart', function( evt ) {      
    var target = evt.data.getTarget().getAscendant( 'div', true );
    
    CKEDITOR.plugins.clipboard.initDragDataTransfer( evt );
    var dataTransfer = evt.data.dataTransfer;       
    dataTransfer.setData( 'contact', CONTACTS[ target.data( 'contact' ) ] );  
    dataTransfer.setData( 'text/html', target.getText() );

    // if (dataTransfer.$.setDragImage) {
    //       dataTransfer.$.setDragImage(target.findOne('img').$, 0, 0);
    //     }        
   } );
   } );

     // Initialize the editor with the hcard plugin.
    // CKEDITOR.inline('editor1', {
    //   extraPlugins: 'hcard,sourcedialog,justify,divarea,toolbar'
    // });



//$("#editor1").attr('contenteditable',true);

   $("#template_title").keyup(function(){
   $("#template_title").removeAttr("style");
   });
   $("#template_type").change(function(){
   $("#template_type").removeAttr("style");
   });



  
    $(document).on('click', '.edit_Template', function() {
      var tem_id = $(this).closest('td').find('.edit_templateID').val(); 
      var title = $(this).closest('td').find('.edit_templateTitle').val();
      var type = $(this).closest('td').find('.edit_templateType').val();
      var tem_content = $(this).closest('td').find('.edit_templateContent').html();

      $('#id').val(tem_id);
      $('#template_title').val(title);
      $('#template_type').val(type);
      
      CKEDITOR.instances.editor1.setData(tem_content);
    });
  
 
   
   $("#new_template").click(function(){     
    var templateID = $('#id').val();
    // var template_content=$(".cke_wysiwyg_div > p").text();
    var template_content1=$(".cke_wysiwyg_div").html();
    $('div[data-cke-hidden-sel="1"]').remove();
    var template_content=$(".cke_wysiwyg_div").html();
    $('.cke_wysiwyg_div > span.h-card').each(function(){
    str += $(this).html();        
    });

    $("span.cke_reset").each(function () {
    $(this).remove();
    });

    $("span.cke_widget_wrapper").each(function () {
    $(this).replaceWith($(this).text());
    });

    var template_content=$(".cke_wysiwyg_div").html();    
    var  template_type=$("#template_type").val();
    var  template_title=$("#template_title").val();
    if(template_content==''){
      $("#error").show();   
      setTimeout(function() {
        $("#error").hide(); 
      }, 2000);     
    }
    if(template_type==''){;
      $("#template_type").css({"border-color": "red", 
      "border-width":"1px", 
      "border-style":"solid"});
    }
    if(template_title==''){
      $("#template_title").css({"border-color": "red", 
      "border-width":"1px", 
      "border-style":"solid"});
    }
   if(template_content!='' && template_type!='' && template_title!=''){ 


    if(templateID==''){
     // alert('insert');
     $.ajax({
                    url: '<?php echo base_url();?>proposal/templatename_check/',
                    type : 'POST',
                    data : {'template_title':template_title},                    
           beforeSend: function() {
                 $(".loading-image").show();
              },
              success: function(msg) {
              var json = JSON.parse(msg); 
              msg=json['msg'];
                $(".loading-image").hide();
                  if(msg==1){
                      $('.alert-danger-exists').show();
                       setTimeout(function() {
                     $('.alert-danger-exists').hide(); 
                  }, 2000); 
                  }else{  

    var formData={'templateID':templateID,'template_content':template_content,'template_title':template_title,'template_type':template_type};       
     $.ajax({
                    url: '<?php echo base_url();?>proposal/insert_template/',
                    type : 'POST',
                    data : formData,                    
           beforeSend: function() {
                 $(".loading-image").show();
              },
              success: function(msg) { 
                //alert(msg);
                $(".loading-image").hide();
                 var json = JSON.parse(msg); 
                  msg=json['value'];
                  message=json['message'];

                  if(msg==0){
                      $('.required').show();
                  }else{
                    if(message==0){
                        setTimeout(function() { 
                          $('.alert-success').show();
                           location.reload();  
                                     }, 1000);     
                    }else{
                        setTimeout(function() { 
                          $('.alert-success-update').show();
                           location.reload();  
                                     }, 1000); 
                    }               
                }
              }
   
                  });

      }
              }
   
                  });

   }else{


     $.ajax({
                    url: '<?php echo base_url();?>proposal/edit_templatename_check/',
                    type : 'POST',
                    data : {'template_title':template_title,'templateID':templateID,},                    
           beforeSend: function() {
                 $(".loading-image").show();
              },
              success: function(msg) {
              var json = JSON.parse(msg); 
              msg=json['msg'];
                $(".loading-image").hide();
                  if(msg==1){
                      $('.alert-danger-exists').show();
                       setTimeout(function() {
                     $('.alert-danger-exists').hide(); 
                  }, 2000); 
                  }else{

    // alert('update');
      var formData={'templateID':templateID,'template_content':template_content,'template_title':template_title,'template_type':template_type};       
     $.ajax({
                    url: '<?php echo base_url();?>proposal/insert_template/',
                    type : 'POST',
                    data : formData,                    
           beforeSend: function() {
                 $(".loading-image").show();
              },
              success: function(msg) {
                //alert(msg);
                $(".loading-image").hide();
                
                  if(msg==0){
                      $('.required').show();
                  }else{
                  setTimeout(function() { 
                    $('.alert-success-update').show();
                     location.reload();  
                               }, 1000);                    
                }
              }
   
                  });
    }
  }
});

   }
   }else{
    $('.required').show();
     setTimeout(function() { 
      $('.required').hide();
    }, 1000); 
   }
   });
   $('.test').change(function() {  
    //alert('ok');
   var task_status = $(this).val();
   var id=this.id;
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>proposal/update_status",
      data:  'id='+id+"&task_status="+task_status, 
        beforeSend: function() {
          $(".loading-image").show();
        },
        success: function(data){     
           $(".loading-image").hide();        
          $(".alert-success-update").show();
          setTimeout(function() { $(".alert-success-update").hide(); 
          location.reload(); }, 5000);
        }
    });  
   });
</script> 
<script type="text/javascript">
   $('#timepicker1').timepicker();   
</script> 

<script type="text/javascript">
$(document).ready(function() {


$(document).on('click','.template_checkbox',function(){

 // $('').on('click', function() {
    if($(this).is(':checked', true)) {
       $(".template_delete").show();
    }else{
       $(".template_delete").hide(); 
    }
  });

$(document).on('change','#select_all_template',function(){

 // alert('ok');
  // $('').on('click', function() {
    if($(this).is(':checked', true)) {
      $(".template_delete").show();
      $(".template_checkbox").prop('checked', true);
      } else {
         $(".template_delete").hide();
        $(".template_checkbox").prop('checked', false);
      }  
        $("#select_template_count").val($("input.template_checkbox:checked").length+" Selected");
  });




  
    $('.template_checkbox').on('click', function() {
      $("#select_template_count").val($("input.template_checkbox:checked").length+" Selected");
    }); 

 $('#delete_template_records').on('click', function() {
  var template = [];
  $(".template_checkbox:checked").each(function() {
    template.push($(this).data('template-id'));
  });
  if(template.length <=0) {
     $('.alert-danger-check').show();
  } else {
    // TEMPLATE = "Are you sure you want to delete"+(template.length>1?" these":" this")+" row?";
    // var checked_template = confirm(TEMPLATE);
    $('#delete_user'+template).show();
    $('.delete_yes').click(function() { 
       var selected_template_values = template.join(",");

      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'proposal/deletetemplate';?>",  
        cache: false,
        data: {'data_id':selected_template_values},
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) 
        { 
          $(".LoadingImage").hide();
         // var template_ids = data.split(",");
         // for (var i=0; i < template_ids.length; i++ ) { 
         // $("#"+template_ids[i]).remove(); }           
          if(data == 1)          
          { 
             $('#delete_user'+template).hide();
             $(".alert-success-delete").show();
          }   
          setTimeout(function(){ location.reload(); }, 2000);   
        }
      });
    });
  }
});  

});  

</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/parsley.css') ?>">
<script src="<?php echo base_url('assets/js/parsley.min.js') ?>"></script>
<script type="text/javascript">
$('#currency_form').parsley();
</script>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/parsley.css') ?>">
<script src="<?php echo base_url('assets/js/parsley.min.js') ?>"></script>
 --><script type="text/javascript">
$( document ).ready(function() {
$("#editor3").attr('contenteditable','true');
$("#editor1").attr('contenteditable','true');
});


  $('#new_proposal').parsley();

  $("#add_template").click(function(){
   // alert('ok');
     $("#editor1").attr('contenteditable',true);
 $("div#myTemplate .cke_wysiwyg_div").html('');
 $("div#myTemplate #template_type").val('');
   $("div#myTemplate #template_title").val('');
  });

  $(document).on('click','.DT', function (e) {
        if (!$(e.target).hasClass('sortMask')) {      
            e.stopImmediatePropagation();
        }
    });

$('th .themicond').on('click', function(e) {
  if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
  $(this).parent().find('.dropdown-content').removeClass('Show_content');
  }else{
   $('.dropdown-content').removeClass('Show_content');
   $(this).parent().find('.dropdown-content').addClass('Show_content');
  }
  $(this).parent().find('.select-wrapper').toggleClass('special');
      if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
      }else{
        $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
        $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );

      }
});

 $(document).on('click', 'th .themicond', function(){
      if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
         $(this).parent().find('.dropdown-content').removeClass('Show_content');
      }else{
         $('.dropdown-content').removeClass('Show_content');
         $(this).parent().find('.dropdown-content').addClass('Show_content');
      }
          $(this).parent().find('.select-wrapper').toggleClass('special');
      if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){ 
      }else{
          $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
          $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
      }
});


   $("th").on("click.DT", function (e) {      
        if (!$(e.target).hasClass('sortMask')) {       
            e.stopImmediatePropagation();
        }
    });


   // $("#add_template").click(function(){
   //  //alert('ok');

  
   // });

   $(document).on('change','#template',function()
   {        
      var tid = $(this).val();

      $.ajax(
      {
         url:'<?php echo base_url().'Proposal/get_template'; ?>',
         type:'POST',
         data:{'id':tid},

         success:function(template)
         { 
            var temp = JSON.parse(template);               
            $('.cke_wysiwyg_div').html(temp.body);
         }

      }); 

   });

   $(document).on('change','#template_type',function()
   {
       var action_id = $(this).val(); 

       $.ajax(
       {
          url:'<?php echo base_url().'Proposal/getemail_titles'; ?>',
          type:'POST',
          data:{'action_id':action_id},

          success:function(titles)
          {             
             $('#template_select').html(titles);
          }

       });

   });

</script>



</body>
</html>