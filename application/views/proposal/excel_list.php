<?php
// The function header by sending raw excel
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Service-list.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<table>
<tr>
<th>S.no</th>
<th>Service Name</th>
<th>Service Price</th>
<th>Service Unit</th>
<th>Service Description</th>
<th>Service Qty</th>
<th>Service Category</th>
</tr>
<?php $i=1;
foreach($records as $data)
	{ ?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $data['service_name']; ?></td>
<td><?php echo $data['service_price']; ?></td>
<td><?php echo $data['service_unit']; ?></td>
<td><?php echo $data['service_description']; ?></td>
<td><?php echo $data['service_qty']; ?></td>
<td><?php echo $data['service_category']; ?></td>
</tr>
<?php  $i++;  } ?>


</table>