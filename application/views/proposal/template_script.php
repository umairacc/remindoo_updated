

<script type="text/javascript">
 $(document).ready(function(){
      $( "#proposal_template1" ).validate({
          errorClass: "my-error-class",
        rules: {
          templatetitle_edit: "required",  
          template_content: "required",  
          templatetype_edit: "required"
        },
        messages: {
          templatetitle_edit: "Please enter your Title",
          template_content: "Please enter your Title",
          templatetype_edit: "Please Select Template Type"
        },        
      });
   });	

CKEDITOR.replace( 'editor2', {
 extraPlugins: 'divarea,hcard1,sourcedialog,justify'
 });

'use strict';
   var CONTACTS1 = [
   { name: 'Username' },
   { name: 'Client Name' },
   { name: 'Accountant Name' },
   { name: 'Task Number' },
   { name: 'Task Name' },
   { name: ' Task Due Date'},
   { name: ' Task Client' },
   { name: 'Firm Name' },
   { name: 'accountant number' },
   { name: 'invoice amount' },
   { name: 'firm name' },
   { name: 'price table' }, 
   { name: 'invoice' },      
   ];

CKEDITOR.disableAutoInline = true;		
   CKEDITOR.plugins.add( 'hcard1', {
   requires: 'widget',
   init: function( editor ) {
   	editor.widgets.add( 'hcard1', {
   		allowedContent: 'span(!h-card1); a[href](!u-email,!p-name); span(!p-tel)',
   		requiredContent: 'span(h-card1)',
   		pathName: 'hcard1',
   		upcast: function( el ) {
   			return el.name == 'span' && el.hasClass( 'h-card1' );
   		}
   	} );				
   	editor.addFeature( editor.widgets.registered.hcard1 );				
   	editor.on( 'paste', function( evt ) {
   		var contact = evt.data.dataTransfer.getData( 'contact1' );
   		if ( !contact ) {
   			return;
   		}
   		evt.data.dataValue =						
   			'<span class="content h-card1">' +
   				'['+contact.name+']'  +
   			'</span>';
   	} );
   }
   });

CKEDITOR.on( 'instanceReady', function() {			
   CKEDITOR.document.getById( 'contactList1' ).on( 'dragstart', function( evt ) {			
   	var target = evt.data.getTarget().getAscendant( 'div', true );
   	
   	CKEDITOR.plugins.clipboard.initDragDataTransfer( evt );
   	var dataTransfer = evt.data.dataTransfer;				
   	dataTransfer.setData( 'contact1', CONTACTS1[ target.data( 'contact1' ) ] );	
   	dataTransfer.setData( 'text/html', target.getText() );				
   } );
   } );   

$("#templatetitle_edit").keyup(function(){
   $("#templatetitle_edit").removeAttr("style");
   });
   $("#templatetype_edit").change(function(){
   $("#templatetype_edit").removeAttr("style");
   });


</script>


