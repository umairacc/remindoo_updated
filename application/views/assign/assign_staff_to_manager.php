<?php $this->load->view('includes/header');
$succ = $this->session->flashdata('success');
?>
<div class="pcoded-content pcodedteam-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <!--start-->
                  
				  <div class="update-dept01">
				  
                     <?php if($succ){?>
                     <div class="modal-alertsuccess alert alert-success"
                        ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<div class="pop-realted1">
    <div class="position-alert1">
	<?php echo $succ; ?></div> </div> </div>
	<?php } ?>
                    
                      <div class=" new-task-teams">
                      <h2>Assign Staff To Manager</h2>
                     <form id="assign_staff_manager" method="post" action="<?php echo base_url()?>assign/assign_stafftomanager" enctype="multipart/form-data">
                     <div class="row">
                        <div class="form-group col-sm-6">
                           <label>Select Manager</label>
                            <div class="dropdown-sin-1">
                           <select name="manager" id="manager" Placeholder="Select Manager">
                               <option value="">--Select--</option>
                              <?php foreach ($manager as $key => $value) { ?>
                              <option value="<?php echo $value['id'];?>"><?php echo $value['username'];?></option>
                              <?php } ?>
                           </select>
                           </div>
                        </div>

                     
                          <div class="form-group col-sm-6 select_staff">
                                    <label>Select Staff</label>
                                    <div class="dropdown-sin-2">
                                       <select name="staff[]" id="staff" multiple placeholder="Select">
                                       <!-- <option value="">Select</option> -->
                                          <?php foreach($staff as $staffs) {
                                             ?>
                                          <option value="<?php echo $staffs['id']?>"><?php echo $staffs['username'];?></option>
                                          <?php } ?>
                                       </select>
                                    </div>
                            </div>
                                         
                    
                        <div class="form-group create-btn col-sm-12">
                           <input type="submit" name="add_task" class="btn-primary" value="create"/>
                        </div>
						</div>
                     </form>
                     </div>
                  </div>
                  <!-- close -->
               </div>
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<!-- ajax loader -->
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style>
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>


<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- j-pro js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
 
<script type="text/javascript">
   $('#timepicker1').timepicker();
</script> 

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script type="text/javascript">
      $.validator.setDefaults({
         ignore: []
     });
 //  $(document).ready(function(){

      $( "#assign_staff_manager" ).validate({

           errorPlacement: function(error, element) {
               if (element.attr("name") == "staff[]" )
                     error.insertAfter(".dropdown-sin-2");
              else if (element.attr("name") == "manager" )
                     error.insertAfter(".dropdown-sin-1");
                                  
                 else
                     error.insertAfter(element);
             },
        rules: {
          manager: "required",  
          "staff[]": "required",  
        },
        messages: {
          manager: "This Field is required",
          "staff[]": "This Field is required",
        },
        
      });
  $('.dropdown-sin-1').dropdown({
  input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   $('.dropdown-sin-2').dropdown({
 // limitCount: 5,
  input: '<input type="text" maxLength="20" placeholder="Search">'
   });
 //  });

 
  /** for succesmsg hide **/
  $(document).ready(function(){
     setTimeout(function(){ 
      $('.alert-success').hide(); }, 2000);
  });
  /** endof successmsg hide **/
</script>
<?php $this->load->view('includes/footer_second');?>
</body>
</html>