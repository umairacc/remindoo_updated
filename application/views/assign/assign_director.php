<?php $this->load->view('includes/header');
   $role = $this->Common_mdl->getRole($_SESSION['id']);
   $succ = $this->session->flashdata('success');
   
   ?>
<style>
   /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   img.user_imgs {
   width: 38px;
   height: 38px;
   border-radius: 50%;
   display: inline-block;
   }
   .text12345 + .modal-backdrop ~ .modal-backdrop {display:none}

    tfoot {
    display: table-header-group;
}
  
   .dropdown-content {
   display: none;
   position: absolute;
   background-color: #fff;
   min-width: 86px;
   overflow: auto;
   box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   z-index: 1;
   left: -92px;
   width: 150px;
   }
</style>
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card dept-card-perm">
                        <!-- admin start-->
                        <div class="client_section col-xs-12 floating_set">
                          
                           <div class="all_user-section floating_set">
                              <div class="import-container-1">
                                 <div class="page-header card">
                                    <div class="align-items-end">
                                       <div class="page-header-title pull-left">
                                          <div class="d-inline">
                                             <h4>Manager Assigne
                                             </h4>
                                          </div>
                                       </div>
                                       <div class="pull-right csv-sample01 ">
                                          <button type="button" id="deleteTriger" class="deleteTri del-tsk12 f-right" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>
                                            <button type="button" id="archiveTriger" data-toggle="modal" data-target="#Bulkarchiveconfirmation" class="archiveTri del-tsk12 f-right" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Archive</button>

                                          <a href="<?php echo base_url() ?>assign/assign_manager_to_director"><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Add New</a>
                                         <!--  <a href="<?php echo base_url()?>team/assigned_team"><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Assign Staff</a> -->
                                       </div>
                                    </div>
                                 </div>
                                 <?php if($succ){?>
                                 <div class="modal-alertsuccess alert alert-success"
                                    >
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <div class="pop-realted1">
                                       <div class="position-alert1">
                                          <?php echo $succ; ?>
                                       </div>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <div class=" all_user-section-team floating_set">
                                    <div class="tab-content floating_set">
                                       <div id="alltasks" class="tab-pane fade in active">
                                          <div class="count-value1 floating_set">
                                          </div>
                                          <div class="client_section3 table-responsive floating_set">
                                             <?php //$this->load->view('users/task_summary');?>
                                             <div id="status_succ"></div>
                                             <div class="all-usera1 user-dashboard-section1">
                                                <table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
                                                   <thead>
                                                      <tr class="text-uppercase">
                                                         <th>
                                                            <div class="checkbox-color checkbox-primary">
                                                               <input type="checkbox"  id="bulkDelete"  /><label for="bulkDelete"></label>
                                                            </div>
                                                         </th>
                                                         <th>SNO</th>
                                                         <th>Director</th>
                                                         <th>Manager</th>
                                                         <th>Actions</th>
                                                      </tr>
                                                   </thead>

                                                   <tfoot>
                                                      <tr class="text-uppercase">
                                                         <th>
                                                          
                                                         </th>
                                                         <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <th></th>
                                                      </tr>
                                                   </tfoot>
                                                   <tbody>
                                                      <?php $i =1 ; foreach ($assign_director as $key => $value) {
                                                         ?>
                                                      <tr>
                                                         <td>
                                                            <div class="checkbox-color checkbox-primary">
                                                               <input type='checkbox' id="checkbox<?php echo $value['id'];?>"  class='deleteRow' value="<?php echo $value['id'];?>"  />
                                                               <label for="checkbox<?php echo $value['id'];?>">
                                                               </label>
                                                            </div>
                                                         </td>
                                                         <td><?php echo $i;?></td>
                                                         <td><?php
                                              echo $this->Common_mdl->get_field_value('user','username','id',$value['director']);
                                                          //echo $value['manager'];?></td>
                                                          <td>
                                                            <?php
                                                            
                                                            if($value['manager']!=''){
                                                            $ex_manager=explode(',', $value['manager']); 
                                                            $names=array();
                                                            foreach ($ex_manager as $man_key => $man_value) {
                                                              $res=$this->Common_mdl->get_field_value('user','username','id',$man_value);
                                                               array_push($names, $res);
                                                             } 
                                                             echo implode(',', $names);
                                                              } ?>
                                                          </td>                            
                                                         <td>
                                                            <p class="action_01">
                                                               <!-- <a href="<?php echo base_url().'team/delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> -->
                                                               <a href="#" onclick="return confirm_delete('<?php echo $value['id'];?>');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                                             <!--   <a href="<?php echo base_url().'team/update_team/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a> -->
                                                              <!-- <a href="<?php base_url();?>update_assign_staff_to_manager/<?php echo $value['id'];?>"> -->
                                                              <a href="<?php echo base_url() ?>assign/update_assign_manager_to_director/<?php echo $value['id'];?>">
                                                              <i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                                              <a href="javascript:void(0)"  data-toggle="modal" data-target="#archiveconfirmation" onclick="showconfirmation(this)" data-id="<?php echo $value['id'];?>">
                                                              <i class="fa fa-archive archieve_click" aria-hidden="true"></i>

                                                              </a>
                                                            </p>
                                                         </td>
                                                         <div class="modal-alertsuccess alert alert-success" id="delete_team<?php echo $value['id'];?>" style="display:none;">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                                                            <div class="pop-realted1">
                                                               <div class="position-alert1">
                                                                  Are you sure want to delete <b><a href="#" class="delete_yes"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </tr>
                                                      <div class="modal-alertsuccess alert alert-success" id="delete_user<?php echo $value['id'];?>" style="display:none;">
                                                         <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                                                         <div class="pop-realted1">
                                                            <div class="position-alert1">
                                                               Are you sure want to delete <b><a href="<?php echo base_url();?>assign/director_assign_delete/<?php echo $value['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <?php $i++; } ?>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- home-->
                                 </div>
                                 <!-- admin close -->
                              </div>
                              <!-- Register your self card end -->
                           </div>
                        </div>
                     </div>
                     <!-- Page body end -->
                  </div>
               </div>
               <!-- Main-body end -->
               <div id="styleSelector">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="archiveconfirmation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <inpu type="hidden" name="archive_assign_id" id="archive_assign_id" value="">
          <p>Do you want to archive this?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="confirm_archive">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>


  <div class="modal fade" id="Bulkarchiveconfirmation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <inpu type="hidden" name="archive_assign_id" id="Bulkarchive_director_id" value="">
          <p>Do you want to archive this?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="Bulkconfirm_archive">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
 
<script type="text/javascript">
   $('#timepicker1').timepicker();
</script> 

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script type="text/javascript">

  function showconfirmation(value)
   {
   // alert($(value).data('id'));
    var id=$(value).data('id');
//alert(id);
 //$("#archive_assign_id").addClass('gdfgdfgdfg');
    $("#archive_assign_id").attr('value',id);
  }

$("#confirm_archive").click(function(){
var id=$("#archive_assign_id").val();
 $.ajax({
  url: 'http://remindoo.uk/Assign/archive_assign_director/'+id,
  type : 'get',
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data) {
      //alert(data);
     
       $(".LoadingImage").hide();
       location.reload();
     
    }

  });
});




   $(document).ready(function(){

      $( "#add_new_team" ).validate({

        rules: {
          new_team: "required",  
        },
        messages: {
          new_team: "Please enter your Team"
        },
        
      });
   });
</script>
<script>
   $(document).ready(function() {
   
   
   //   $("#alltask").dataTable({
   //      "iDisplayLength": 10,
   //   "scrollX": true,
   //  /* "processing":true,
   //  "serverSide": true,
   //    "ajax": {
   //           "url": "<?php echo site_url('user/ajax_list')?>",
   //           "type": "POST",
   //           "data": function ( data ) {
   //               data.today = $('#dropdown1').val();
   
   //           }
   //      },*/
   
   // /*  initComplete: function () {
   //           this.api().columns(5).every( function () {
   //               var column = this;
   
   //                // Get the header name for this column
   //   /*var headerName = 'Status';
   //   // generate the id name for the element to append to.
   //   var appendHere = '#' + headerName + '-select-filter';*/
   
   //            /*   var select = $('<select><option value="">All</option></select>')
   //                   .appendTo( $('#select-filter').empty() )
   //                   .on( 'change', function () {
   //                       var val = $.fn.dataTable.util.escapeRegex(
   //                           $(this).val()
   //                       );
   
   //                       column
   //                           .search( val ? '^'+val+'$' : '', true, false )
   //                           .draw();
   //                   } );
   
   //               column.data().unique().sort().each( function ( d, j ) {
   //                   select.append( '<option value="'+d+'">'+d+'</option>' )
   //               } );
   //           } );
   //       }*/
   //   });
   $("#inprogresses").dataTable({
        "iDisplayLength": 10,
     });
   $("#notstarted").dataTable({
        "iDisplayLength": 10,
     });
   $("#started").dataTable({
        "iDisplayLength": 10,
     });
   
   
   
   //$(".status").change(function(e){
   $('#alluser').on('change','.status',function () {
   //e.preventDefault();
   
   
    var rec_id = $(this).data('id');
    var stat = $(this).val();
   
   $.ajax({
         url: '<?php echo base_url();?>user/statusChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         timeout: 3000,
         success: function( data ){
          //alert('ggg');
             $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });
             setTimeout(resetAll,3000);
             if(stat=='3'){
              
               //$this.closest('td').next('td').html('Active');
               $('#frozen'+rec_id).html('Frozen');
   
             } else {
               $this.closest('td').next('td').html('Inactive');
             }
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
    });
   
   // payment/non payment status
   $('.suspent').click(function() {
     if($(this).is(':checked'))
         var stat = '1';
     else
         var stat = '0';
     var rec_id = $(this).val();
     var $this = $(this);
      $.ajax({
         url: '<?php echo base_url();?>user/suspentChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         success: function( data ){
          //alert('ggg');
             $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
             if(stat=='1'){
              
               $this.closest('td').next('td').html('Payment');
   
             } else {
               $this.closest('td').next('td').html('Non payment');
             }
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
   });
   });
   
</script>
<script>
   $(document).ready(function() {
    // var table =  $('#alltask').DataTable();
     
      $('#dropdown2').on('change', function () {
                    // table.columns(5).search( this.value ).draw();
                    var filterstatus = $(this).val();
                    $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>tasksummary/taskFilter",
            data: {filterstatus:filterstatus},
            success: function(response) {
   
              $(".all-usera1").html(response);
    
            },
            //async:false,
          });
                 });
   
      /*$('#dropdown1').on('change', function () {
                    // table.columns(2).search( this.value ).draw();
                    
                 } );*/
   
                
   });
   
</script>
<script>
   $(document).ready(function() {
   $("#bulkDelete").on('click',function() { // bulk checked
        var status = this.checked;
   
        $(".deleteRow").each( function() {
          $(this).prop("checked",status);
   
        });
        if(status==true)
        {
          $('#deleteTriger').show();
           $('#archiveTriger').show();
        } else {
          $('#deleteTriger').hide();
           $('#archiveTriger').hide();
        }
      });
   
   $(".deleteRow").on('click',function() { // bulk checked
   
        var status = this.checked;
   
        if(status==true)
        {
          $('#deleteTriger').show();
           $('#archiveTriger').show();
        } else {
          $('#deleteTriger').hide();
           $('#archiveTriger').hide();
        }
      });
      
      $('#deleteTriger').on("click", function(event){ // triggering delete one by one
       
        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
          var ids = [];
          $('.deleteRow').each(function(){
            if($(this).is(':checked')) { 
              ids.push($(this).val());
            }
          });
   
        $('#delete_team'+ids).show();
        $('.delete_yes').click(function() {  
          var ids_string = ids.toString();  // array to string conversion 
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>assign/directordelete/",
            data: {data_ids:ids_string},
            success: function(response) {
              //dataTable.draw(); // redrawing datatable
              var emp_ids = response.split(",");
             for (var i=0; i < emp_ids.length; i++ ) {
            
              $("#"+emp_ids[i]).remove(); 
             }
             $(".alert-success-delete").show();
              setTimeout(function() { 
              location.reload(); }, 500);
                },
            //async:false,
          });
   
        });  
   
          
        }
      });


       $('#archiveTriger').on("click", function(event){ // triggering delete one by one
       
        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
          var ids = [];
          $('.deleteRow').each(function(){
            if($(this).is(':checked')) { 
              ids.push($(this).val());
            }
          });        
          var id=ids.toString();
          $("#Bulkarchive_director_id").val(id);       
        $('#Bulkconfirm_archive').click(function() {  
          var ids_string = $("#Bulkarchive_director_id").val();  // array to string conversion 
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>Assign/bulkArchive_director/",
            data: {data_ids:ids_string},
            success: function(response) {              
             if(response==1)
             {
              location.reload();
              }
             },
           
          });
   
        });  
   
          
        }
      });
   
   });
</script>
<script type="text/javascript">
   function confirm_delete(id)
   {
   $('#delete_user'+id).show();
   return false;
   }
   
   $(document).on('click','#close',function(e)
    {
    $('.alert-success').hide();
    return false;
    });
   $(document).ready(function(){
              setTimeout(function() { 
                $('.alert-success').hide();
             }, 2000);
   });


    $(document).ready(function() {
     
      var check=0;
      var check1=0;
      var numCols = $('#alltask thead th').length;   
      var table = $('#alltask').DataTable({
         "dom": '<"toolbar-table">lfrtip',
       initComplete: function () { 
                  var q=1;
                     $('tfoot th').find('.filter_check').each( function(){
                       $(this).attr('id',q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){      
                      // if(i==1){
                      //     check=1;          
                      //     i=Number(i) + 1;
                      //     var select = $("#1"); 
                      //   }
                      // }else if(i==7){
                      //     check=1;            
                      //     i=Number(i) + 1;
                      //     var select = $("#7"); 
                      // }else{
                          var select = $("#"+i); 
                    //  }          
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      });                   
                    if(check=='1'){                 
                       i=Number(i) - 1;
                       check=0;
                    } 
                     $("#"+i).formSelect();  
                  }
        }
    });
      for(j=2;j<numCols;j++){  
          $('#'+j).on('change', function(){  
            var c=$(this).attr('id');         
              var search = [];              
              $.each($('#'+c+ ' option:selected'), function(){                
                  search.push($(this).val());
              });      
              search = search.join('|');             
              if(c==8){               
                c=Number(c) + 1;
              }             
              table.column(c).search(search, true, false).draw();  
          });
       }

         $('#bulkDelete').click(function(event) {  //on click                         
            $('[name=alltask_length]').val( 100 ).trigger('change');
            var checked = this.checked;
            table.column(0).nodes().to$().each(function(index) {    
            if (checked) {
            $(this).find('.deleteRow').prop('checked', 'checked'); 
            $(".LoadingImage").show();
            setTimeout(function(){ $('[name=alltask_length]').val( 10 ).trigger('change'); $(".LoadingImage").hide();  }, 0);
            $("#deleteTriger").show();
             $("#archiveTriger").show();
            } else {
            $(this).find('.deleteRow').removeProp('checked'); 
            $(".LoadingImage").show();   
            setTimeout(function(){ $('[name=alltask_length]').val( 10 ).trigger('change'); $(".LoadingImage").hide(); }, 0);                 
            $("#deleteTriger").hide();   
              $("#archiveTriger").hide();   
            }
            });    
            table.draw();
            });
});

</script>

<div class="text12345"></div>