<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');
    
   ?>

   <div class="leads-section floating_set">
    <div class="deadline-crm1 floating_set">
      <div class="pull-left">
        <h4>Document Management</h4>
      </div>
    </div>
    <div class="proposal-sent-sample floating_set">
      <div class="management_section floating_set ">
       <div id="new_lead" class="lead-summary2 floating_set new-leadss">
               <form id="edit_doc_form" name="edit_leads_form" action="<?php echo base_url()?>Document/update_document_data/<?php echo $documents[0]['id']; ?>" method="post" class="edit_leads_form" enctype="multipart/form-data">
               
                  <div class="row">
                   <div class="form-group col-sm-12">
                     <label class="label-column1 col-form-label">Name</label>
                     <div class="label-column2">
                     <input type="text" class="form-control" name="document_name" id="document_name" placeholder="" value="<?php echo $documents[0]['document_name'];?>">
                     </div>
                    </div>
                     <div class="form-group col-sm-12">
                     <label class="label-column1 col-form-label">Document Category</label>
                     <div class="label-column2">
                     <input type="text" class="form-control" name="document_category" id="document_category" placeholder="" value="<?php echo $documents[0]['document_category'];?>">
                     </div>
                    </div>
                     <div class="form-group col-sm-12">
                     <label class="label-column1 col-form-label">Add File</label>
                     <div class="label-column2">
                     <input type="file" class="form-control" name="document_file" id="document_file">
                     <input type="hidden" id="old_doc_file" name="old_doc_file" value="<?php echo $documents[0]['document_file']; ?>">
                       <div class="extra_view">
                       <a href="<?php echo $documents[0]['document_file']; ?>" target="_blank">View</a>
                       <a href="javascript:void(0)" class="remove_file">Remove</a>
                       </div>
                     </div>
                    </div>
                    <div class="form-group create-btn new_task-bts1">
                        <input type="submit" name="add_task" class="btn-primary" value="Update">
                    </div>
                  </div>
               </form>  
              </div> <!-- new lead-->
      </div>
    </div>
   </div>

   <style type="text/css">
   .ui-datepicker
   {
   z-index: 9999999999 !important;
   }
</style>
<!-- Modal -->
<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>

<script type="text/javascript">
  $('.remove_file').on('click',function(){
    $('.extra_view').remove();
    $('#old_doc_file').val('');

  });
  $(document).ready(function(){
 $( "#edit_doc_form" ).validate({

        rules: {
        
            document_name: {
            required:true,
            },
            document_category: {
            required:true,
            },
           document_file: {
            //required:true,
             required: function(element) {
         
             if ($("#old_doc_file").val()=='') {
                return true;
             }
             else {
             return false;
             }
            },

   },
          //company:"required",
        },
        messages: {

          document_name: "Please enter your Document Name",
          document_category: "Please enter your Document Category",
          document_file: "Please Upload File",
        
        },
        
      });
  });
</script>