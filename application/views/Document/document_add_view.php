<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');
    
   ?>

   <div class="leads-section floating_set">
    <div class="deadline-crm1 floating_set">
      <div class="pull-left">
        <h4>Document Management</h4>
      </div>
    </div>
    <div class="proposal-sent-sample floating_set">
      <div class="management_section floating_set ">
       <div id="new_lead" class="lead-summary2 floating_set new-leadss">
               <form id="add_doc_form" name="edit_leads_form" action="<?php echo base_url()?>Document/add_document_data" method="post" class="edit_leads_form" enctype="multipart/form-data">
               
                  <div class="row">
                   <div class="form-group col-sm-12">
                     <label class="label-column1 col-form-label">Name</label>
                     <div class="label-column2">
                     <input type="text" class="form-control" name="document_name" id="document_name" placeholder="" value="">
                     </div>
                    </div>
                     <div class="form-group col-sm-12">
                     <label class="label-column1 col-form-label">Document Category</label>
                     <div class="label-column2">
                     <input type="text" class="form-control" name="document_category" id="document_category" placeholder="" value="">
                     </div>
                    </div>
                     <div class="form-group col-sm-12">
                     <label class="label-column1 col-form-label">Add File</label>
                     <div class="label-column2">
                     <input type="file" class="form-control" name="document_file" id="document_file">
                     </div>
                    </div>
                    <div class="form-group create-btn new_task-bts1">
                        <input type="submit" name="add_task" class="btn-primary" value="Add">
                         <input type="button" name="reset" class="btn-primary" onclick="clear_form_elements('#add_doc_form')" value="Reset">
                       
                    </div>
                  </div>
               </form>  
              </div> <!-- new lead-->
      </div>
    </div>
   </div>

   <style type="text/css">
   .ui-datepicker
   {
   z-index: 9999999999 !important;
   }
</style>
<!-- Modal -->
<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
 $( "#add_doc_form" ).validate({

        rules: {
        
            document_name: {
            required:true,
            },
            document_category: {
            required:true,
            },
           document_file: {
           required:true,
            },
          //company:"required",
        },
        messages: {

          document_name: "Please enter your Document Name",
          document_category: "Please enter your Document Category",
          document_file: "Please Upload File",
        
        },
        
      });
  });

  function clear_form_elements(ele) {
   
   $(ele).find(':input').each(function() {
   
    switch(this.type) {
        case 'password':
        case 'select-multiple':
        case 'select-one':
        case 'text':
        case 'file':
        case 'textarea':
            $(this).val('');
            break;
        case 'checkbox':
        case 'radio':
            this.checked = false;
    }
   
   });
   
 
   }
</script>