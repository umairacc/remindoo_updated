                   <table class="table client_table1 text-center display nowrap" id="document_list" cellspacing="0" width="100%">
                                             <thead>
                                                <tr class="text-uppercase">
                                                   <th><div class="checkbox-fade fade-in-primary">
                                                      <label>
                                                      <input type="checkbox" id="select_all_leads">
                                                      <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                                      </label>
                                                    </div></th>
                                                   <th>SNO</th>
                                                   <th>File Name</th>
                                                   <th>Category</th>
                                                   <th>Link</th>
                                                   <th>Actions</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                                <?php $i =1 ; 
                                                foreach ($documents as $key => $value) {
                                                   ?>
                                                <tr>
                                                <td>
                                                   <div class="checkbox-fade fade-in-primary">
                                                      <label>
                                                      <input type="checkbox" class="leads_checkbox" data-leads-id="<?php echo $value['id'];?>">
                                                      <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                                       </label>
                                                  </div>
                                                </td>
                                                   <td><?php echo $i;?></td>
                                                   <td><?php echo $value['document_name'];?></td>
                                                   <td><?php echo $value['document_category'];?></td>
                                                   <td><?php //echo $value['document_file'];?><a class="dwnspl" href="<?php echo $value['document_file'];?>" download >Download</a></td>
                                                   <td>
                                                      <p class="action_01">
                                                       <!--  <a href="javascript:void(0)" data-id="<?php echo $value['id'];?>" class="document_list_delete"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>  -->
                                                       <a href="#" onclick="return confirm('<?php echo $value['id'];?>');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                                        <!--  <a href="<?php echo base_url(); ?>document/edit_document/<?php echo $value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a> -->
                                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#popup_edit_doc_<?php echo $value['id'];?>" ><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                                      </p>
                                                   </td>
                  <div class="modal-alertsuccess alert alert-success" id="delete_all_leads<?php echo $value['id'];?>" style="display:none;">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_yes"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
                  </div></div> 
                  <div class="modal-alertsuccess alert alert-success" id="delete_user<?php echo $value['id'];?>" style="display:none;">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="javascript:void(0);" class="document_list_delete" data-id="<?php echo $value['id'];?>" > Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
                  </div></div>  
                                                </tr>
                                                <?php $i++; } ?>
                                             </tbody>
                                          </table>