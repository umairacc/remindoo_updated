 <?php   foreach ($documents as $key => $value) { ?>
  <div class="modal fade recurring-msg common-schedule-msg1" id="popup_edit_doc_<?php echo $value['id'];?>" role="dialog">
     <div class="modal-dialog">
        <!-- Modal content-->
            <form id="edit_doc_form_<?php echo $value['id'];?>" data-id="<?php echo $value['id']; ?>" name="edit_leads_form" action="<?php echo base_url()?>Document/update_document_data/<?php echo $value['id']; ?>" method="post" class="edit_doc_form" enctype="multipart/form-data">
             
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Document Update</h4>
           </div>
           <div class="modal-body">
                 <div class="row">
                   <div class="form-group col-sm-12">
                     <label class="label-column1 col-form-label">Name</label>
                     <div class="label-column2">
                     <input type="text" class="form-control" name="document_name" id="document_name" placeholder="" value="<?php echo $value['document_name'];?>">
                     </div>
                    </div>
                     <div class="form-group col-sm-12">
                     <label class="label-column1 col-form-label">Document Category</label>
                     <div class="label-column2">
                     <input type="text" class="form-control" name="document_category" id="document_category" placeholder="" value="<?php echo $value['document_category'];?>">
                     </div>
                    </div>
                     <div class="form-group col-sm-12">
                     <label class="label-column1 col-form-label">Add File</label>
                     <div class="label-column2">
                     <input type="file" class="form-control" name="document_file" id="document_file">
                     <input type="hidden" id="old_doc_file" class="old_doc_file_<?php echo $value['id'];?>" name="old_doc_file" value="<?php echo $value['document_file']; ?>">
                       <div class="extra_view_<?php echo $value['id'];?>">
                       <a href="<?php echo $value['document_file']; ?>" target="_blank">View</a>
                       <a href="javascript:void(0)" class="remove_file" data-id="<?php echo $value['id'];?>">Remove</a>
                       </div>
                     </div>
                    </div>
                   <!--  <div class="form-group create-btn new_task-bts1">
                        <input type="submit" name="add_task" class="btn-primary" value="Update">
                    </div> -->
                  </div>
             </div>
             <div class="modal-footer">
                <button type="submit" name="add_task"  class="btn btn-default submitBtn" id="submitBtn">Update</button>
                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
               
             </div>
          </div>
          </form>  
       </div>
    </div>
    <?php } ?>
    <script type="text/javascript">
      $(document).ready(function(){

 $('.edit_doc_form').each(function () {
    var form_id=$(this).attr('data-id');
        $(this).validate({
              rules: {
        
            document_name: {
            required:true,
            },
            document_category: {
            required:true,
            },
           document_file: {
            //required:true,
             required: function(element) {
         
             if ($("#old_doc_file").val()=='') {
                return true;
             }
             else {
             return false;
             }
            },

   },
          //company:"required",
        },
        messages: {

          document_name: "Please enter your Document Name",
          document_category: "Please enter your Document Category",
          document_file: "Please Upload File",
        
        },
        submitHandler: function(form) {
        //  var form = $('#'+form_id)[0];
        var formData = new FormData(form);
            $('.LoadingImage').show();
              $.ajax({
                  url: '<?php echo base_url();?>Document/update_document_data/'+form_id,
                  type: "POST",
                   data: formData,
                    contentType: false,       
                    cache: false,             
                    processData:false,
                  success: function(response) {
                      //$('#answers').html(response);
                      console.log(response);
                       $('.LoadingImage').hide();
                     $('.document_list').html(response);
                      $("#document_list").dataTable({
                          //  "iDisplayLength": 10,
                          // "scrollX": true,
                       });
                    $('#popup_edit_doc_'+form_id).modal('hide');
                    $('.modal-backdrop.show').hide();
                    $('.edit_popup').load("<?php echo base_url(); ?>Document/document_edit_poup");

                  }            
              });
              return false;
          }

        });
    });

    $('.remove_file').on('click',function(){
      var id=$(this).attr('data-id');
    $('.extra_view_'+id).remove();
    $('.old_doc_file_'+id).val('');

  });

      });
    </script>