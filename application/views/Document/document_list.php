<?php
$this->load->view('includes/header');
$role = $this->Common_mdl->getRole($_SESSION['id']);
$succ = $this->session->flashdata('success');

?>
<style>
	/*span.newonoff {
	background: #4680ff;
	padding: 6px 15px;
	display: inline-block;
	color: #fff;
	border-radius: 5px;
	}*/
	button.btn.btn-info.btn-lg.newonoff {
		padding: 3px 10px;
		height: initial;
		font-size: 15px;
		border-radius: 5px;
	}

	img.user_imgs {
		width: 38px;
		height: 38px;
		border-radius: 50%;
		display: inline-block;
	}

	.new-task-teams {
		box-shadow: none;
	}

	button.btn.btn-info.btn-lg.newonoff {
		padding: 3px 10px;
		height: initial;
		font-size: 15px;
		border-radius: 5px;
	}


	/* #datepicker{
	height: 38px;
	width: 110px;
	background-color: #E8E8E7;
	border: none;
	}*/
	/*.but{
	height: 38px;
	width: 83px;
	padding-top: 6px;
	}*/
	.dropdown-content {
		display: none;
		position: absolute;
		background-color: #fff;
		min-width: 86px;
		overflow: auto;
		box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
		z-index: 1;
		left: -92px;
		width: 150px;
	}

	.dropdown {
		position: relative;
		display: inline-block;
	}

	/*
	.dropdown-content {
	display: none;
	position: absolute;
	background-color: #f1f1f1;
	min-width: 86px;
	overflow: auto;
	box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
	z-index: 1;
	}
	*/
	.dropdown-content a {
		color: black;
		padding: 9px 22px;
		text-decoration: none;
		display: block;
	}

	/*
	.dropdown a:hover {
	background-color: #4c7ffe;
	color: #fff;
	}*/
	.show {
		display: block;
	}

	.hide {
		display: none;
	}

	.popup-inner.archivecls .body {
		text-align: right;
		padding: 20px;
	}

	.popup-inner.archivecls label {
		padding-left: 20px;
	}
</style>
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<!-- Main-body start -->
		<div class="main-body">
			<div class="page-wrapper">
				<!-- Page body start -->
				<div class="page-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="deadline-crm1 floating_set doc-manage">
								<ul class="nav nav-tabs all_user1 md-tabs pull-left">
									<li class="nav-item">
										<a class="nav-link">Documents Management</a>
									</li>
								</ul>
								<div class="count-value1 pull-right ">
									<button id="deleteTriger" class="deleteTri" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete</button>

								</div>
								<div class="pull-right csv-sample01 ">
									<!--   <a class="btn-card btn btn-primary" href="<?php echo base_url(); ?>document/document_add"><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Add New</a> -->
									<a class="btn-card btn btn-primary" href="javascript:void(0)" data-toggle="modal" data-target="#popup_add_doc"><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Add New</a>

									<!--       <a class="btn-card btn btn-primary" href="javascript:void(0)" data-toggle="modal" data-target="#popup_add_doc" ><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Add New</a> -->
									&nbsp;
									<button type="button" id="delete_leads" class="btn-card del-tsk12 f-right delete_leads" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>

								</div>
							</div>
							<!-- Register your self card start -->
							<div class="col-xs-12 card">
								<!-- admin start-->
								<div class="client_section lead-sets col-xs-12 floating_set">
									<div class="all_user-section floating_set">
										<div class="import-container-1">
											<div class="tab-content">
												<div id="newactive" class="tab-pane fade in active">


													<?php if ($succ) { ?>
														<div class="modal-alertsuccess alert alert-success">
															<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
															<div class="pop-realted1">
																<div class="position-alert1">
																	<?php echo $succ; ?>
																</div>
															</div>
														</div>
													<?php } ?>
													<div class="modal-alertsuccess alert alert-success" id="deleted_success_message" style="display:none;">
														<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
														<div class="pop-realted1">
															<div class="position-alert1">
																Document was Deleted Successfully
															</div>
														</div>
													</div>

													<div class="all-usera1 user-dashboard-section1 document_list">
														<table class="table client_table1 text-center display nowrap" id="document_list" cellspacing="0" width="100%">
															<thead>
																<tr class="text-uppercase">
																	<th>
																		<div class="checkbox-fade fade-in-primary">
																			<label>
																				<input type="checkbox" id="select_all_leads">
																				<span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>
																			</label>
																		</div>
																	</th>
																	<th>SNO</th>
																	<th>File Name</th>
																	<th>Category</th>
																	<th>Link</th>
																	<th>Actions</th>
																</tr>
															</thead>
															<tbody>
																<?php $i = 1;
																if (is_array($documents)) {
																	foreach ($documents as $key => $value) {
																	?>
																		<tr>
																			<td>
																				<div class="checkbox-fade fade-in-primary">
																					<label>
																						<input type="checkbox" class="leads_checkbox" data-leads-id="<?php echo $value['id']; ?>">
																						<span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>
																					</label>
																				</div>
																			</td>
																			<td><?php echo $i; ?></td>
																			<td><?php echo $value['document_name']; ?></td>
																			<td><?php echo $value['document_category']; ?></td>
																			<td><?php //echo $value['document_file'];
																				?><a class="dwnspl" href="<?php echo $value['document_file']; ?>" download>Download</a></td>
																			<td>
																				<p class="action_01">
																					<!--  <a href="javascript:void(0)" data-id="<?php echo $value['id']; ?>" class="document_list_delete"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>  -->
																					<a href="#" onclick="return confirm('<?php echo $value['id']; ?>');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a>
																					<!--  <a href="<?php echo base_url(); ?>document/edit_document/<?php echo $value['id']; ?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a> -->
																					<a href="javascript:void(0)" data-toggle="modal" data-target="#popup_edit_doc_<?php echo $value['id']; ?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
																					<a href="javascript:void(0)" onclick="return showconfirmation('<?php echo $value['id']; ?>')">
																						<i class="fa fa-archive archieve_click" aria-hidden="true"></i>
	
																					</a>
																				</p>
																			</td>
																			<div class="modal-alertsuccess alert alert-success" id="delete_all_leads<?php echo $value['id']; ?>" style="display:none;">
																				<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
																				<div class="pop-realted1">
																					<div class="position-alert1">
																						Are you sure want to delete <b><a href="#" class="delete_yes"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
																				</div>
																			</div>
																			<div class="modal-alertsuccess alert alert-success" id="delete_user<?php echo $value['id']; ?>" style="display:none;">
																				<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
																				<div class="pop-realted1">
																					<div class="position-alert1">
																						Are you sure want to delete <b><a href="javascript:void(0);" class="document_list_delete" data-id="<?php echo $value['id']; ?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
																				</div>
																			</div>
																		</tr>
																	<?php $i++;
																	} 
																}
																?>
															</tbody>
														</table>
													</div>
												</div>

											</div>
											<!-- end of status table -->
											<!-- admin close -->
										</div>
										<!-- Register your self card end -->
									</div>
								</div>
							</div>
							<!-- Page body end -->
						</div>
					</div>
					<!-- Main-body end -->
					<div id="styleSelector">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="popup" id="arc_con" data-popup="popup-2">
	<div class="popup-inner archivecls">
		<div class="head">
			<h3>Confirmation</h3>
			<a class="popup-close" data-popup-close="popup-2" href="#">x</a>
		</div>
		<label>Do You Want Archive this?</label>
		<div class="body">
			<input type="hidden" id="archive_id" />
			<button type="button" class="btn btn-primary" onClick="archive_update()">yes</button>
			<button type="button" class="btn btn-danger" data-popup-close="popup-2">Close</button>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade recurring-msg common-schedule-msg1" id="popup_add_doc" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<form id="add_doc_form" name="edit_leads_form" action="<?php echo base_url() ?>Document/add_document_data" method="post" class="edit_leads_form" enctype="multipart/form-data">

			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Document Add</h4>
				</div>
				<div class="modal-body">
					<div class="row">

						<div id="new_lead" class="lead-summary2 floating_set new-leadss">

							<div class="row">
								<div class="form-group col-sm-12">
									<label class="label-column1 col-form-label">Name</label>
									<div class="label-column2">
										<input type="text" class="form-control" name="document_name" id="document_name" placeholder="" value="">
									</div>
								</div>
								<div class="form-group col-sm-12">
									<label class="label-column1 col-form-label">Document Category</label>
									<div class="label-column2">
										<input type="text" class="form-control" name="document_category" id="document_category" placeholder="" value="">
									</div>
								</div>
								<div class="form-group col-sm-12">
									<label class="label-column1 col-form-label">Add File</label>
									<div class="label-column2">
										<input type="file" class="form-control" name="document_file" id="document_file">
									</div>
								</div>
								<!--    <div class="form-group create-btn new_task-bts1">
								<input type="submit" name="add_task" class="btn-primary" value="Add">
								 <input type="button" name="reset" class="btn-primary" onclick="clear_form_elements('#add_doc_form')" value="Reset">
							  
						  </div> -->
							</div>

						</div> <!-- new lead-->

					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" name="add_task" class="btn btn-default submitBtn" id="submitBtn">Add</button>
					<button type="button" name="reset" class="btn btn-default" onclick="clear_form_elements('#add_doc_form')">Reset</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- modal close-->

<!-- Modal -->
<div class="edit_popup rs_edit_popup_common">
	<?php foreach ($documents as $key => $value) { ?>
		<div class="modal fade recurring-msg common-schedule-msg1 rs_edit_popup" id="popup_edit_doc_<?php echo $value['id']; ?>" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<form id="edit_doc_form_<?php echo $value['id']; ?>" data-id="<?php echo $value['id']; ?>" name="edit_leads_form" action="<?php echo base_url() ?>Document/update_document_data/<?php echo $value['id']; ?>" method="post" class="edit_doc_form" enctype="multipart/form-data">

					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Document Update</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="form-group col-sm-12">
									<label class="label-column1 col-form-label">Name</label>
									<div class="label-column2">
										<input type="text" class="form-control" name="document_name" id="document_name" placeholder="" value="<?php echo $value['document_name']; ?>">
									</div>
								</div>
								<div class="form-group col-sm-12">
									<label class="label-column1 col-form-label">Document Category</label>
									<div class="label-column2">
										<input type="text" class="form-control" name="document_category" id="document_category" placeholder="" value="<?php echo $value['document_category']; ?>">
									</div>
								</div>
								<div class="form-group col-sm-12">
									<label class="label-column1 col-form-label">Add File</label>
									<div class="label-column2">
										<input type="file" class="form-control" name="document_file" id="document_file">
										<input type="hidden" id="old_doc_file" class="old_doc_file_<?php echo $value['id']; ?>" name="old_doc_file" value="<?php echo $value['document_file']; ?>">
										<div class="extra_view_<?php echo $value['id']; ?>">
											<a href="<?php echo $value['document_file']; ?>" target="_blank">View</a>
											<a href="javascript:void(0)" class="remove_file" data-id="<?php echo $value['id']; ?>">Remove</a>
										</div>
									</div>
								</div>
								<!--  <div class="form-group create-btn new_task-bts1">
								<input type="submit" name="add_task" class="btn-primary" value="Update">
						  </div> -->
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" name="add_task" class="btn btn-default submitBtn" id="submitBtn">Update</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

						</div>
					</div>
				</form>
			</div>
		</div>
	<?php } ?>
	<!-- modal close-->
</div>




<?php $this->load->view('includes/session_timeout'); ?>
<?php $this->load->view('includes/footer'); ?>

<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/jquery.tagsinput.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/email.css">

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
<script type="text/javascript" language="javascript" src="<?php echo base_url() ?>assets/js/jquery.tagsinput.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dropdown.js"></script>
<script>
	function showconfirmation(id) {
		// alert(id);
		//$('#archive_id').val(id);
		bootbox.confirm({
			title: "Notification Section",
			message: "Do You want Archive This?",
			buttons: {
				confirm: {
					label: 'Confirm',
					className: 'btn-success'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-danger'
				}
			},
			callback: function(result) {
				if (result) {
					$.ajax({
						url: 'http://remindoo.uk/Document/archive_document/' + id,
						type: 'get',
						beforeSend: function() {
							$(".LoadingImage").show();
						},
						success: function(data) {
							//alert(data);

							$(".LoadingImage").hide();
							location.reload();

						}

					});
				}
			}
		});
	}



	$(document).ready(function() {


		$("#document_list").dataTable({


		});

	});



	//$('.leads_source_delete').click(function(){
	$(document).on('click', '.document_list_delete', function() {
		var id = $(this).attr('data-id');
		//alert(id);

		//if (confirm('Are you sure want to delete')) {
		var data = {};
		data['del'] = 'delete';
		$('.LoadingImage').show();
		$.ajax({
			url: '<?php echo base_url(); ?>Document/document_list_delete/' + id,
			type: 'post',
			data: data,
			success: function(data, status) {
				$('.LoadingImage').hide();
				$('.document_list').html(data);
				var table = $("#document_list").dataTable({
					//  "iDisplayLength": 10,
					// "scrollX": true,
				});
				$('#deleted_success_message').show();
				setTimeout(function() {
					$('#deleted_success_message').hide();
				}, 2000);

				access_permission_function(); // from footer function

				/** for datatable chckbox **/
				$('#select_all_leads').click(function(event) { //on click                         
					$('[name=all_leads_length]').val(100).trigger('change');
					var checked = this.checked;
					table.column(0).nodes().to$().each(function(index) {
						if (checked) {
							$(this).find('.leads_checkbox').prop('checked', 'checked');
							$(".LoadingImage").show();
							setTimeout(function() {
								$('[name=all_leads_length]').val(10).trigger('change');
								$(".LoadingImage").hide();
							}, 0);

							$(".delete_leads").show();
						} else {
							$(this).find('.leads_checkbox').removeProp('checked');
							$(".LoadingImage").show();
							setTimeout(function() {
								$('[name=all_leads_length]').val(10).trigger('change');
								$(".LoadingImage").hide();
							}, 0);
							$(".delete_leads").hide();
						}
					});
					table.draw();
				});
				/** datatable checkbox **/
			}
		});

		//}

	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#add_doc_form").validate({

			rules: {

				document_name: {
					required: true,
				},
				document_category: {
					required: true,
				},
				document_file: {
					required: true,
				},
				//company:"required",
			},
			messages: {

				document_name: "Please enter your Document Name",
				document_category: "Please enter your Document Category",
				document_file: "Please Upload File",

			},
			submitHandler: function(form) {
				var form = $('#add_doc_form')[0];
				var formData = new FormData(form);
				$('.LoadingImage').show();
				$.ajax({
					url: '<?php echo base_url(); ?>Document/add_document_data',
					// type: form.method,
					// data: $(form).serialize(),
					type: "POST",
					data: formData,
					contentType: false,
					cache: false,
					processData: false,
					success: function(response) {
						//$('#answers').html(response);
						console.log(response);
						$('.LoadingImage').hide();
						$('.document_list').html(response);
						var table = $("#document_list").dataTable({
							//  "iDisplayLength": 10,
							// "scrollX": true,
						});
						access_permission_function(); // from footer function
						$('#select_all_leads').click(function(event) { //on click                         
							$('[name=all_leads_length]').val(100).trigger('change');
							var checked = this.checked;
							table.column(0).nodes().to$().each(function(index) {
								if (checked) {
									$(this).find('.leads_checkbox').prop('checked', 'checked');
									$(".LoadingImage").show();
									setTimeout(function() {
										$('[name=all_leads_length]').val(10).trigger('change');
										$(".LoadingImage").hide();
									}, 0);

									$(".delete_leads").show();
								} else {
									$(this).find('.leads_checkbox').removeProp('checked');
									$(".LoadingImage").show();
									setTimeout(function() {
										$('[name=all_leads_length]').val(10).trigger('change');
										$(".LoadingImage").hide();
									}, 0);
									$(".delete_leads").hide();
								}
							});
							table.draw();
						});
						$('#popup_add_doc').modal('hide');
						$('.modal-backdrop.show').hide();
						$('.edit_popup').load("<?php echo base_url(); ?>Document/document_edit_poup");
					}
				});
				return false;
			}

		});


		$('.edit_doc_form').each(function() {
			var form_id = $(this).attr('data-id');
			$(this).validate({
				rules: {

					document_name: {
						required: true,
					},
					document_category: {
						required: true,
					},
					document_file: {
						//required:true,
						required: function(element) {

							if ($("#old_doc_file").val() == '') {
								return true;
							} else {
								return false;
							}
						},

					},
					//company:"required",
				},
				messages: {

					document_name: "Please enter your Document Name",
					document_category: "Please enter your Document Category",
					document_file: "Please Upload File",

				},
				submitHandler: function(form) {
					//  var form = $('#'+form_id)[0];
					var formData = new FormData(form);
					$('.LoadingImage').show();
					$.ajax({
						url: '<?php echo base_url(); ?>Document/update_document_data/' + form_id,
						type: "POST",
						data: formData,
						contentType: false,
						cache: false,
						processData: false,
						success: function(response) {
							//$('#answers').html(response);
							console.log(response);
							$('.LoadingImage').hide();
							$('.document_list').html(response);
							var table = $("#document_list").dataTable({
								//  "iDisplayLength": 10,
								// "scrollX": true,
							});
							$('#select_all_leads').click(function(event) { //on click                         
								$('[name=all_leads_length]').val(100).trigger('change');
								var checked = this.checked;
								table.column(0).nodes().to$().each(function(index) {
									if (checked) {
										$(this).find('.leads_checkbox').prop('checked', 'checked');
										$(".LoadingImage").show();
										setTimeout(function() {
											$('[name=all_leads_length]').val(10).trigger('change');
											$(".LoadingImage").hide();
										}, 0);

										$(".delete_leads").show();
									} else {
										$(this).find('.leads_checkbox').removeProp('checked');
										$(".LoadingImage").show();
										setTimeout(function() {
											$('[name=all_leads_length]').val(10).trigger('change');
											$(".LoadingImage").hide();
										}, 0);
										$(".delete_leads").hide();
									}
								});
								table.draw();
							});
							access_permission_function(); // from footer function
							$('#popup_edit_doc_' + form_id).modal('hide');
							$('.modal-backdrop.show').hide();
							$('.edit_popup').load("<?php echo base_url(); ?>Document/document_edit_poup");

						}
					});
					return false;
				}

			});
		});

		$('.remove_file').on('click', function() {
			var id = $(this).attr('data-id');
			$('.extra_view_' + id).remove();
			$('.old_doc_file_' + id).val('');

		});

	});

	function clear_form_elements(ele) {

		$(ele).find(':input').each(function() {

			switch (this.type) {
				case 'password':
				case 'select-multiple':
				case 'select-one':
				case 'text':
				case 'file':
				case 'textarea':
					$(this).val('');
					break;
				case 'checkbox':
				case 'radio':
					this.checked = false;
			}

		});


	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		//$('.leads_checkbox').on('click', function() {
		$(document).on('click', '.leads_checkbox', function() {
			// if($(this).is(':checked', true)) {
			//    $(".delete_leads").show();
			// }else{
			//    $(".delete_leads").hide(); 
			// }
			/** 14-06-2018 **/
			var len = $("input.leads_checkbox:checked").length;
			if (len > 0) {
				$(".delete_leads").show();
			} else {
				$(".delete_leads").hide();
			}
			/** end of 14-06-2018 **/
		});

		$('#select_all_leads').on('click', function() {
			//alert('zzz');
			if ($(this).is(':checked', true)) {
				$(".delete_leads").show();
				$(".leads_checkbox").prop('checked', true);
			} else {
				$(".delete_leads").hide();
				$(".leads_checkbox").prop('checked', false);
			}
			$("#select_leads_count").val($("input.leads_checkbox:checked").length + " Selected");
		});

		$('.leads_checkbox').on('click', function() {
			$("#select_leads_count").val($("input.leads_checkbox:checked").length + " Selected");
		});

		$('#delete_leads').on('click', function() {
			var leads = [];
			$(".leads_checkbox:checked").each(function() {
				leads.push($(this).data('leads-id'));
			});
			if (leads.length <= 0) {
				$('.alert-danger-check').show();
			} else {
				$('#delete_all_leads' + leads).show();
				$('.delete_yes').click(function() {
					var selected_leads_values = leads.join(",");

					$.ajax({
						type: "POST",
						url: "<?php echo base_url() . 'Document/delete_bulk_document'; ?>",
						cache: false,
						data: 'data_id=' + selected_leads_values,
						beforeSend: function() {
							$(".LoadingImage").show();
						},
						success: function(data) {
							$(".LoadingImage").hide();
							var leads_ids = data.split(",");
							for (var i = 0; i < leads_ids.length; i++) {
								$("#" + leads_ids[i]).remove();
							}
							$(".alert-success-delete").show();
							setTimeout(function() {
								location.reload();
							}, 2000);
						}
					});
				});
			}
		});
	});
	$(document).on('click', '#close', function(e) {
		$('.alert-success').hide();
		return false;
	});

	function confirm(id) {
		$('#delete_user' + id).show();
		return false;
	}








	//alert(id);
	/* try{
	$('#arc_con').show();
	
 }catch(a){alert(a);}
	return false;*/
	//}
</script>