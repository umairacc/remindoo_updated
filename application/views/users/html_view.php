<!DOCTYPE html>
<html>
<head>
	<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}


</style>
</head>
<body>
   
   <table  width="90%">
         
   	  <thead>
   	  	  <tr>
   	  	  	 <th>S.NO</th>
   	  	  	 <th>NAME</th>
   	  	  	 <th>ACTIVE</th>
   	  	  	 <th>USERTYPE</th>
   	  	  	 <th>COMPANY NAME</th>
   	  	  	 <th>COMPANYSTATUS</th>
   	  	  	 <th>STATUS</th>
   	  	  	 
   	  	  </tr>
   	  </thead>
       
      

 <tbody>
    <?php $i=0; foreach ($refer_data as $n) : ?>
      <?php $i++;
            if($n['status']==1){ $n['status']="Active";}
            if($n['status']==2){ $n['status']="In-active";}
            if($n['status']==3){ $n['status']="Frozen";}
                        
            if($n['role']==1){ $n['role']="Super Admin";}
            if($n['role']==2){ $n['role']="Sub Admin";}
            if($n['role']==3){ $n['role']="Director";}
            if($n['role']==4){ $n['role']="Client";}
            if($n['role']==5){ $n['role']="Manager";}
            if($n['role']==6){ $n['role']="Staff";}

            if($n['client_status']==0){ $n['client_status']="Client";}
            if($n['client_status']==1){ $n['client_status']="Company House";}
            if($n['client_status']==2){ $n['client_status']="Import";}  ?>
    <tr>
    	<td><?= $i; ?></td>
    	<td><?= $n['crm_name']; ?></td>
        <td><?= $n['status']; ?></td>
        <td><?= $n['role']; ?></td>
        <td><?= $n['crm_company_name']; ?></td>
        <td><?= $n['crm_company_status']; ?></td>
        <td><?= $n['client_status']; ?></td>
    </tr>
    </tbody>
    <?php endforeach ?>


   </table>
</body>
</html>