<?php $this->load->view('includes/header');?>
<style>
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   img.user_imgs {
   width: 38px;
   height: 38px;
   border-radius: 50%;
   display: inline-block;
   }
</style>
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section last-commit1 col-xs-12 floating_set">
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left">
                                    <li class="nav-item all-client-icon1">
                                       <span><i class="fa fa-calendar fa-6" aria-hidden="true"></i></span>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" href="#alltasks">All Tasks</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#inprogress">Create new task</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#inprogress">Import Tasks</a>
                                       <div class="slide"></div>
                                    </li>
                                  </ul>
                              </div>
                              <div class="all_user-section2 floating_set">
                                 <div class="tab-content">
                                    <div id="alltasks" class="tab-pane fade in active">
                                       <div class="client_section-task table-responsive floating_set">
                                          <div id="status_succ"></div>
                                          <div class="all-usera1 user-dashboard-section1">
                                             <table class="tbl-accordion table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
                                                <thead>
                                                   <tr class="text-uppercase">
                                                      <th>#</th>
                                                      <th>Task list</th>
                                                      <th>Last Commit</th>
                                                      <th>Status</th>
                                                      <th>Assigned User</th>
                                                   </tr>
                                                </thead>
                                                <tbody>

                                                   <!-- first-row -->
                                                	<tr>
                                                      <td>
                                                      <table cellpadding="0" cellspacing="0" class="tbl-accordion-nested">   
                                                         <thead>
                                                         <tr>
                                                		<td><span>#12</span></td>
                                                		<td>Add Proper Cursor In Sortable Page</td>
                                                		<td><input class="form-control datepicker" type="text" name="data" id="date"></td>
                                                		<td>
                                                				<div class="fake-select single-select toggle-select">
																<span class="title" data-orig-text="Designer">Open</span>
																<ul class="fake-select-list">
																<li><a href="#">Close</a></li>
																<li><a href="#">Open</a></li>
																</ul>
																</div>
														</td>		
                                                		<td class="assigned-user1">
                                                			<img src="<?php echo base_url()?>assets/images/by-date4.png" alt="data">
                                                			<a href="#"><i class="fa fa-plus fa-6" aria-hidden="true"></i></a>
                                                		</td>
                                                	</tr>
                                                 </thead>  

                                                 <tbody>
                                                      
                                                      <tr class="edit-draft">
                                                         <td><span>Action</span></td>
                                                         <td>
                                                <div class="fake-select single-select toggle-select">
                                                <span class="title" data-orig-text="Designer"><i class="fa fa-cog fa-6" aria-hidden="true"></i></span>
                                                <ul class="fake-select-list">
                                                <li><a href="#">Choose Designer</a></li>
                                                <li><a href="#">Alexander McQueen</a></li>
                                                <li><a href="#">Christian Louboutin</a></li>
                                                <li class="selected"><a href="#">Jimmy Choo</a></li>
                                                <li class="selected"><a href="#">Manolo Blahnik</a></li>
                                                </ul>
                                                </div>
                                             
                                                <div class="due-hours1">
                                                   <span>Due:</span>
                                                   <strong>23 Hours</strong>
                                                   
                                                   <div class="alexander fake-select single-select toggle-select">
                                                <span class="title" data-orig-text="Designer">Normal</span>
                                                <ul class="fake-select-list">
                                                <li><a href="#">Choose Designer</a></li>
                                                <li><a href="#">Alexander McQueen</a></li>
                                                <li><a href="#">Christian Louboutin</a></li>
                                                <li class="selected"><a href="#">Jimmy Choo</a></li>
                                                <li class="selected"><a href="#">Manolo Blahnik</a></li>
                                                </ul>
                                                </div>

                                                 <div class="alexander1 fake-select single-select toggle-select">
                                                <span class="title" data-orig-text="Designer">Open</span>
                                                <ul class="fake-select-list">
                                                <li><a href="#">Choose Designer</a></li>
                                                <li><a href="#">Alexander McQueen</a></li>
                                                <li><a href="#">Christian Louboutin</a></li>
                                                <li class="selected"><a href="#">Jimmy Choo</a></li>
                                                <li class="selected"><a href="#">Manolo Blahnik</a></li>
                                                </ul>
                                                </div>

                                                 <div class="alexander1 fake-select single-select toggle-select">
                                                <span class="title" data-orig-text="Designer"></span>
                                                <ul class="fake-select-list">
                                                <li><a href="#">Choose Designer</a></li>
                                                <li><a href="#">Alexander McQueen</a></li>
                                                <li><a href="#">Christian Louboutin</a></li>
                                                <li class="selected"><a href="#">Jimmy Choo</a></li>
                                                <li class="selected"><a href="#">Manolo Blahnik</a></li>
                                                </ul>
                                                </div>

                                             </div>   
                                          </td> 
                                       </tr> 

                                                 </tbody>
                                             </table>  
                                          </td>
                                       </tr>  <!-- first-row close-->       



                                                	



                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- home-->
                                    <!-- frozen -->
                                 </div>
                              </div>
                              <!-- admin close -->
                           </div>
                           <!-- Register your self card end -->
                        </div>
                     </div>
                  </div>
                  <!-- Page body end -->
               </div>
            </div>
            <!-- Main-body end -->
         </div>
      </div>
   </div>
</div>
</div>
</div>
<?php $this->load->view('includes/footer');?>

<script type="text/javascript" src="<?php echo base_url()?>assets/js/rekaf.js"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
   $(document).ready(function() {
   
   
     var tabletask1 =$("#alltask").dataTable({
        "iDisplayLength": 10,
     "scrollX": true,
     "dom": '<"toolbar-table">lfrtip'
     });
   
   
     $("div.toolbar-table").html('<div class="filter-task1"><h2>Filter:</h2><ul><li><img src="<?php echo  base_url()?>assets/images/by-date1.png" alt="data"><select><option>By Date</option><option>date1</option></select></li><li><img src="<?php echo  base_url()?>assets/images/by-date2.png" alt="data"><select><option>By Status</option><option>date1</option></select></li><li><img src="<?php echo  base_url()?>assets/images/by-date3.png" alt="data"><select><option>By Priority</option><option>date1</option></select></li><li><select><option>Export</option><option>date1</option></select></li></ul></div>');
   	

   	$('.toggle-select').rekaf({
					clickRemoveSelected: true
				});
   
   
   });
   
</script>

<script type="text/javascript">
    $(document).ready(function(){
    
    $('.tbl-accordion-nested').each(function(){
  var thead = $(this).find('thead');
  var tbody = $(this).find('tbody');
  
  tbody.hide();
  thead.click(function(){
    tbody. slideToggle();
  })
})
});
</script>