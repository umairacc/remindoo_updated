<?php
  $column_helper = [
        'crm_company_name'    =>  'ClientColumn_Client' ,
        'crm_legal_form'      =>  'ClientColumn_Type',
        'crm_company_type'    =>  'ClientColumn_Type',
        'crm_company_status'  =>  'ClientColumn_Status',
        'contact_landline'    =>  'ClientColumn_landlines',
        'contact_work_email'  =>  'ClientColumn_work_emails',
        'proof_attach_file'   =>  'ClientColumn_addBaseUrl',
        'client_assignees'    =>  'ClientColumn_assignees',
        'CreatedTime'         =>  'ClientColumn_created_date',
        'crm_confirmation_statement_due_date' => 'ClientColumn_check_CS_ch_overdue',
        'crm_ch_accounts_next_due'            => 'ClientColumn_check_Acc_ch_overdue'

        /*'staff_manager'=>'ClientColumn_staff_manager',
        'staff_managed' => 'ClientColumn_staff_managed',
        'team' => 'ClientColumn_team',
        'team_allocation' => 'ClientColumn_team_allocation',
        'department'=>'ClientColumn_department',
        'department_allocation'=>'ClientColumn_department_allocation',
        'member_manager'=> 'ClientColumn_member_manager',
        'member_managed' => 'ClientColumn_member_managed',
        'crm_company_status' => 'ClientColumn_client_AddtedFrom',*/
          ];

        $FirmId = $_SESSION['firmId'];
        $status =[ 'Inactive','Active','Inactive','Frozen','Draft','Archive'];

?>


<div class="filter-data for-reportdash">
  <div class="filter-head">
    <h4>FILTERED DATA</h4>
    <button class="btn btn-danger f-right" id="clear_container">clear</button>
    <div id="container2" class="panel-body box-container">

    </div>
  </div>                                   
</div>

 <table class="table client_table1 text-center display nowrap printableArea" id="client_table" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th class="select_row_TH">
            <label class="custom_checkbox1">
              <input type="checkbox" id="select_all_client"><i></i>
            </label>
        </th>
        

        <?php
            foreach ($column_setting as $key => $colval) 
            {
            
        ?>
              <th class="<?php echo $colval['field_propriety'].'_TH';?> hasFilter" data-orderNo="<?php echo $colval['list_page_order'];?>">
                <?php echo $colval['label']?>
                <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                <div class="sortMask"></div>
                <?php if( trim( $colval['field_propriety'] ) == "crm_company_status" )
                {

                  echo ClientColumn_Status_SelectBox();
                }
                else if( trim( $colval['field_propriety'] ) == "client_assignees" )
                {
                  echo ClientColumn_FirmStaffs_SelectBox();
                } 
                ?>
              </th>
        <?php
            }
        ?> 
        <th class="action_TH">
          Action
        </th>
        <th class="crm_legal_form__primary_search_TH" style="display: none;">
        </th>
          <th class="Status_primary_search_TH" style="display: none;">
        <th class="Created_date_primary_search_TH" style="display: none;">
        </th>
        
        </th>
      </tr>
    </thead>
    
    <tbody>
                                                      <?php  
foreach ($getallUser as $getallUserkey => $getallUservalue) 
           {
            if( $getallUservalue['status'] == '' ) $getallUservalue['status']=0;
            
            $client=$this->Common_mdl->select_record('client','user_id',$getallUservalue['id']);                    
            $contact=$this->db->query('select * from client_contacts where client_id='.$getallUservalue['id'].' and make_primary=1')->row_array();
            ?>
                            <tr class="table_row" id="<?php echo $getallUservalue['id'];?>">
                               <td>
                                 <label class="custom_checkbox1">
                                      <input type="checkbox" class="select_client" value="<?php echo $getallUservalue['id'];?>"><i></i>
                                  </label>
                                </td>
                                <?php
                                       foreach ($column_setting as $key => $colval) 
                                       {

                                          if( $colval['field_type']==1)
                                          {
                                            $data = $this->db->query("SELECT value FROM  tblcustomfieldsvalues WHERE relid=".$getallUservalue['id']." AND fieldid=".$colval['field_propriety'])->row_array();

                                            echo Change_DataFormat( (!empty($data['value'])?$data['value']:'') );
                                          }
                                          else if( isset( $column_helper[ $colval['field_propriety'] ] ) )
                                          {
                                            echo $column_helper[ $colval['field_propriety'] ]($getallUservalue,$client,$contact);
                                          }
                                          else if(preg_match("/^contact_/i", $colval['field_propriety']))
                                          {
                                            $column_name = trim( preg_replace("/^contact_/", "", $colval['field_propriety'] ) );
                                            
                                            echo Change_DataFormat(  $contact[$column_name]  );
                                          }                                        
                                          else if(preg_match("/^usertable_/i", $colval['field_propriety']))
                                          {
                                            $column_name = trim( preg_replace("/^usertable_/", "", $colval['field_propriety'] ) );
                                            
                                            echo Change_DataFormat(  $getallUservalue[$column_name]  );
                                          }
                                          else 
                                          {
                                            echo Change_DataFormat( $client[ $colval['field_propriety'] ] );
                                          }
                                         }
                                      ?>
                                    <td class="action_TD">
                                        <p class="action_01 sss">
                                          <?php 
                                          if( $_SESSION['permission']['Client_Section']['edit']!=0 )
                                          {
                                            $Is_Disabled = ($getallUservalue['status'] == 5?'disabled':'');
                                            ?>

                                          <a href="javascript:void(0)" data-id="<?php echo $getallUservalue['id'];?>" class="ArchiveClient <?php echo $Is_Disabled?>">
                                            <i class="fa fa-archive" aria-hidden="true"></i>
                                          </a>
                                          <a href="<?php echo base_url().'Client/addnewclient/'.$getallUservalue['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>

                                          <?php 

                                          }                                          
                                          if( $_SESSION['permission']['Client_Section']['delete']!=0 )
                                          {

                                            ?>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#deleteconfirmation" data-id="<?php echo $getallUservalue['id'];?>" class='DeleteClient'><i class="fa fa-trash fa-6" aria-hidden="true" ></i></a>
                                          <?php 

                                          }

                                          ?>
                                        </p>
                                    </td>
                                    <td style="display: none;" data-search="<?php echo $client['crm_legal_form']; ?>"><?php echo $client['crm_legal_form']; ?> </td>
                                    <td style="display: none;" class="Status_primary_search_TD" data-search="<?php echo $status[ $getallUservalue['status'] ];?>"><?php echo $status[ $getallUservalue['status'] ];?></td>
                                    <!-- D'nt move this its should stay last , for from-to search-->
                                    <td style="display: none;"><?php echo date('Ymd',trim($getallUservalue['CreatedTime']) );?></td>
                                   <!-- D'nt move this its should stay last , for from-to search-->
                         </tr>
            <?php
              }
            ?>
                                        </tbody>

</table>
<input type="hidden" class="rows_selected" id="select_client_count" >          