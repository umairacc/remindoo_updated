<?php
if (isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout'] == 'remindoo_superadmin') {
   $key_val = 1;
   $this->load->view('super_admin/superAdmin_header');
} else {
   $key_val = 0;
   $this->load->view('includes/new_header');
}
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

<div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
   <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
      <div class="pop-realted1">
         <div class="position-alert1">
            Please! Select Record...
         </div>
      </div>
   </div>
</div>

<style type="text/css">
   .dropdown-content {
      display: none;
      position: absolute;
      background-color: #fff;
      min-width: 86px;
      overflow: auto;
      box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
      z-index: 1;
      left: -92px;
      width: 150px;
   }

   .modal-alertsuccess a.close {
      margin-top: 5px;
      margin-right: 15px;
   }

   .pop-realted1 {
      padding: 10px;
      background-color: #fff;
   }
</style>

<div class="pcoded-content pcodedteam-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body task-progress-view rem-tasks">
               <div class="row">
                  <?php echo $this->session->flashdata('alert-currency'); ?>
                  <!--start-->
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="propose-template">
                        <!-- admin start-->


                        <div class="modal-alertsuccess alert alert-danger" <?php if (empty($_SESSION['fail_msg'])) {
                                                                              echo 'style="display:none;"';
                                                                           } ?>>
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 Process Failed.
                              </div>
                           </div>
                        </div>


                        <div class="modal-alert_insert alert alert-sucess" style="display: none">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted2">
                              <div class="position-alert1">
                                 Created Successfully.
                              </div>
                           </div>
                        </div>

                        <?php if ($_SESSION['user_type'] != 'SA') { ?>
                           <div class="deadline-crm1 floating_set">
                              <div class="page-heading">Task Progress View</div>
                              <ul id="task_active_wrap" class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard page-tabs client-top-nav">

                              <li class="nav-item">
                                 <a class="nav-link" href="<?php echo base_url(); ?>user/task_list">All Tasks</a>
                                 <div class="slide"></div>
                              </li>


                              <?php if ($_SESSION['permission']['Task']['create'] == 1) { ?>
                                 <li class="nav-item">
                                    <a class="nav-link " href="<?php echo base_url(); ?>user/new_task">Create task</a>
                                    <div class="slide"></div>
                                 </li>

                                 <!-- <li class="nav-item">
                                    <a class="nav-link " href="<?php echo base_url(); ?>task">Task Timeline</a>
                                    <div class="slide"></div>
                                 </li> -->

                              <?php }
                              if ($_SESSION['user_type'] == 'FA') { ?>
                                 <li class="nav-item">
                                    <a class="nav-link active" href="<?php echo base_url(); ?>Task_Status/task_progress_view">Task Progress Status</a>
                                    <div class="slide"></div>
                                 </li>
                              <?php }

                              if ($_SESSION['permission']['Task']['edit'] == 1) { ?>
                                 
                                 <li class="nav-item">
                                    <a class="nav-link " href="<?php echo base_url(); ?>user/task_list_kanban">Switch To Kanban</a>
                                    <div class="slide"></div>
                                 </li>

                                 <li class="nav-item">
                                    <a class="nav-link " href="<?php echo base_url(); ?>user/task_list/archive_task">Archive Task</a>
                                    <div class="slide"></div>
                                 </li>

                                 <li class="nav-item">
                                    <a class="nav-link " href="<?php echo base_url(); ?>user/task_list/complete_task">Completed Task</a>
                                    <div class="slide"></div>
                                 </li>

                                 <li class="nav-item">
                                    <a class="nav-link" data-toggle="modal" data-target="#import-task">Import tasks</a>
                                    <div class="slide"></div>
                                 </li>
                              </ul>

                           </div>
                        <?php } }?>
                        <!-- List -->

                     </div>

                     <!-- <div class="f-right">
                        <button type="button" data-toggle="modal" data-target="#deleteconfirmation" id="delete_task" class="btn btn-danger" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete </button>
                     </div> -->


                     <div class="all_user-section floating_set pros-editemp pros-edit-task">
                        <div class="page-data-block">
                           <div class="rem-card">
                              <div class="deadline-crm1 floating_set">
                                 <button type="button" data-toggle="modal" data-target="#add_status" class="rem-btn add_button"> Add Progress </button>
                              </div>
                              <div class="all_user-section6 floating_set">
                                 <div class="tab-content">
                                    <div id="allusers" class="tab-pane fade in active">
                                       <div id="task"></div>
                                       <div class="client_section3 table-responsive">
                                          <div class="status_succ"></div>
                                          <div class="all-usera1 data-padding1 ">
                                             <div class="">
                                                <table class="table client_table1 text-center display nowrap printableArea" id="display_service1" cellspacing="0" width="100%">
                                                   <thead>
                                                      <tr class="text-uppercase">

                                                         <th>OrderBY</th>

                                                         <th class="">
                                                            <label class="custom_checkbox1" style="margin-left: 5px;">
                                                               <input type="checkbox" id="select_all_templates">
                                                               <i></i>
                                                            </label>
                                                         </th>

                                                         <th style="display:none">S.no
                                                            <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                                            <div class="sortMask"></div>

                                                         </th>

                                                         <th class="progress_TH hasFilter">Progress
                                                            <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                                            <div class="sortMask"></div>

                                                         </th>

                                                         <th style="display:none">ID</th>
                                                         <th>Action</th>

                                                      </tr>
                                                   </thead>

                                                   <tbody>
                                                      <?php

                                                      // print_r($progress_task_status);

                                                      foreach ($progress_task_status as $key => $task_value) { ?>

                                                         <tr>
                                                            <td><?php echo $task_value['order_by']; ?></td>
                                                            <td>

                                                               <label class="custom_checkbox1">

                                                                  <input type="checkbox" class="template_checkbox" data-id="<?php echo $task_value['id']; ?>"><i></i>

                                                               </label>

                                                            </td>

                                                            <td style="display:none"><?php echo $key + 1; ?></td>

                                                            <td class="progress_TD" data-search="<?php echo $task_value['status_name'];  ?>"><?php echo $task_value['status_name']; ?></td>

                                                            <td style="display:none"><?php echo $task_value['id']; ?></td>

                                                            <td>

                                                               <a href="javascript:;" data-toggle="modal" data-target="#add_status" data-id='<?php echo $task_value['id']; ?>' data-status_name="<?php echo $task_value['status_name']; ?>" class="edit_btn"><i class="icofont icofont-edit" aria-hidden="true"></i></a>


                                                               <a href="javascript:;" data-toggle="modal" data-target="#add_status" data-status_name="<?php echo $task_value['status_name']; ?>" data-id='<?php echo $task_value['id']; ?>' class="del_btn"><i class="icofont icofont-ui-delete" aria-hidden="true"></i></a>

                                                            </td>

                                                         </tr>
                                                      <?php  }  ?>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                          <!-- List -->
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="modal fade" id="add_status" role="dialog">
                        <div class="modal-dialog add-dialog-progress">
                           <!-- Modal content-->
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 <h4>
                                    <div class="cont"></div>
                                 </h4>
                              </div>
                              <div class="modal-body">

                                 <input type="text" class="clr-check-client" name="status_name" id="status_name" value="">
                                 <input type="hidden" name="status_id" id="status_id" value="">
                                 <input type="hidden" name="del_status_id" id="del_status_id" value="">
                                 <input type="hidden" name="user_type" id="user_type" value="<?php echo $key_val ?>">
                                 <div id="error_html" style="color:red"></div>
                              </div>
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-default" id="btn_submit">Ok</button>
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                              </div>
                           </div>
                        </div>
                     </div>


                     <!-- Page body end -->
                  </div>
               </div>
               <!-- Main-body end -->
               <div id="styleSelector">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>


<div class="modal fade" id="deleteconfirmation" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Confirmation</h4>
         </div>
         <div class="modal-body">
            <input type="hidden" name="delete_process_id" id="delete_process_id" value="">
            <input type="hidden" name="user_type" id="del_user_type" value="<?php echo $key_val ?>">
            <p> Are you sure want to delete ?</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" id="confirm_delete" data-dismiss="modal" onclick="delete_action()">Yes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="import-task" role="dialog">
   <div class="modal-dialog modal-import-dialog modal-import-dialog-task-progress">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header modal-import-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Import Task</h4>
         </div>
         <div class="modal-body <?php if ($_SESSION['permission']['Task']['create'] != '1') { ?> permission_deined <?php } ?>">
            <!-- content update after page load source from tasks/import viewfile -->
         </div>

      </div>
   </div>
</div>

<?php

$this->load->view('includes/session_timeout');

if ($_SESSION['firm_id'] == 0) {
   $this->load->view('super_admin/superAdmin_footer');
} else {
   $this->load->view('includes/footer');
}

?>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/user_page/materialize.js"></script>
<script src="<?php echo base_url() ?>assets/js/custom/datatable_extension.js"></script>


<script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>

<script type="text/javascript">
   var table10;

   $(document).ready(function() {
      var check = 0;
      var check1 = 0;
      var numCols = $('#display_service1 thead th').length;

      var key_val = '<?php echo $key_val ?>';
      // if(key_val==1)
      // {
      progress_datatable();
      // }
      // else
      // {
      //   Firm_progress_datatable();
      // }
      //   function Firm_progress_datatable(){
      //     table10 = $('#display_service1').DataTable({
      //     "aaSorting": [],
      //       columnDefs: [
      //         { targets: 0, visible: false },
      //         ],
      //     });

      //   }


      function progress_datatable() {

         table10 = $('#display_service1').DataTable({

            rowReorder: {
               selector: '.Reorder',
               realtime: false,
            },
            columnDefs: [{
                  targets: 0,
                  visible: false
               },
               {
                  "targets": [1, 5], //first & last column / numbering column
                  "orderable": false, //set not orderable
               },
               {
                  "targets": [2, 3, 4],
                  "className": 'Reorder',
               },
            ],
            initComplete: function() {




               var api = this.api();

               api.columns('.hasFilter').every(function() {

                  var column = this;
                  var TH = $(column.header());
                  var Filter_Select = TH.find('.filter_check');

                  if (!Filter_Select.length) {

                     Filter_Select = $('<select multiple="true" class="filter_check" style="display:none"></select>').appendTo(TH);
                     var unique_data = [];
                     column.nodes().each(function(d, j) {
                        var dataSearch = $(d).attr('data-search');

                        if (jQuery.inArray(dataSearch, unique_data) === -1) {
                           //console.log(d);
                           Filter_Select.append('<option  value="' + dataSearch + '">' + dataSearch + '</option>');
                           unique_data.push(dataSearch);
                        }

                     });
                  }


                  Filter_Select.on('change', function() {

                     var search = $(this).val();
                     console.log(search.length);

                     var class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);

                     if (search.length) search = '^(' + search.join('|') + ')$';




                     var cur_column = api.column('.' + class_name + '_TH');

                     cur_column.search(search, true, false).draw();
                  });

                  //console.log(select.attr('style'));
                  Filter_Select.formSelect();

               });


            }

         });

         Filter_IconWrap();
         table10.on('row-reordered', function(e, diff, edit) {
            var res_arr = [];
            table10.one('draw', function() {
               console.log('Redraw occurred at: ' + new Date().getTime());

               table10.rows().every(function(rowIdx, tableLoop, rowLoop) {
                  res_arr.push(this.data()[4]);
                  // console.log(rowIdx+1, this.data()[4]);
               });
               var user_val = '<?php echo $key_val ?>';
               if (res_arr.length > 0) {
                  $.ajax({
                     type: 'POST',
                     url: '<?php echo base_url(); ?>Task_Status/order_update/',
                     data: {
                        idsInOrder: res_arr,
                        user_val: user_val
                     },
                     dataType: "json",
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(response) {
                        $(".LoadingImage").hide();
                     }
                  });
               }
            });
            console.log(res_arr);
         });


      }

   });
</script>

<script>
	$(document).ready(function(){
      function clientinputstyle(){
         $(".clr-check-client").keyup(function(){
            var clr = $(this).val();
            if(clr.length >= 3){
               $(this).addClass("clr-check-client-outline");
            }else{
               $(this).removeClass("clr-check-client-outline");
            }
         });
         $(".clr-check-client").each(function(i){
            if($(this).val() !== ""){
            $(this).addClass("clr-check-client-outline");
            }  
         })
      }

      clientinputstyle();

      $(".edit_btn").click(function(){
         clientinputstyle();
      })
   })

</script>

<script type="text/javascript">
   // $(document).on('click','.DT', function (e) 
   // {
   //     if(!$(e.target).hasClass('sortMask')) 
   //     {      
   //         e.stopImmediatePropagation();
   //     }
   // });

   // $(document).on('click', 'th .themicond', function()
   // { 
   //      if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
   //         $(this).parent().find('.dropdown-content').removeClass('Show_content');
   //      }else{
   //         $('.dropdown-content').removeClass('Show_content');
   //         $(this).parent().find('.dropdown-content').addClass('Show_content');
   //      }
   //          $(this).parent().find('.select-wrapper').toggleClass('special');
   //      if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
   //      }else{
   //          $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
   //          $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
   //      }       
   // });   

   $(document).ready(function() {
      // window.alltemplate = [];
   });

   // function getSelectedRow(id)
   // {       
   //    var  alltemplate = [];       
   //    alltemplate.push(id);   
   //    return $.unique(alltemplate);
   // }


   function getSelectedRow() {
      var alltask = [];

      table10.column(1).nodes().to$().each(function(index) {

         if ($(this).find(".template_checkbox").is(":checked")) {
            alltask.push($(this).find(".template_checkbox").attr('data-id'));
         }

      });

      return alltask;

   }
   // function return_indice(id)
   // { 
   //    for(var i=0;i<alltemplate.length;i++)
   //    {
   //       if(id == alltemplate[i])
   //       {
   //          return i;
   //       }          
   //    }
   // }

   // function dounset(id)
   // {
   //    alltemplate = $.unique(alltemplate);
   //    var index = return_indice(id);

   //    if(index > -1) 
   //    {
   //       alltemplate.splice(index, 1);
   //    }

   //    return alltemplate;
   // }

   $('.template_checkbox').change(function(e) {
      $('#delete_process_id').val('');
      var id = $(this).data('id');
      var all_temp = [];
      all_temp = getSelectedRow();
      console.log(all_temp);
      if (all_temp.length > 0) {
         $('#delete_process_id').val(all_temp);
         $("#delete_task").show();
      } else {

         $('#select_all_templates').prop('checked', false);
         //all_temp = dounset(id);  
         $("#delete_task").hide();
      }

      //showdelete(all_temp); 
   });

   $('#btn_submit').click(function() {
      $('#error_html').html('');


      var status_name = $("#status_name").val();
      var status_id = $("#status_id").val();
      var del_status_id = $("#del_status_id").val();
      var status_url = '<?php echo base_url(); ?>Task_Status/insert_task_status';
      var user_type = $("#user_type").val();
      if (status_id != '') {
         status_url = '<?php echo base_url(); ?>Task_Status/updated_task_status';
      }
      if (status_name != '') {
         $('#add_status').hide();
         $.ajax({
            url: status_url,
            type: 'POST',
            data: {
               'status_name': status_name,
               'status_id': status_id,
               'del_status_id': del_status_id,
               'user_val': user_type
            },

            beforeSend: function() {
               $(".LoadingImage").show();
            },
            success: function(status) {

               $(".LoadingImage").hide();

               $('.popup_info_msg').show();

               if (del_status_id == 1) {
                  $(".popup_info_msg .position-alert1").html('').html("Delete the Progress Successfully");
               } else if (status_id != '') {
                  $(".popup_info_msg .position-alert1").html('').html("Update the Progress Successfully");
               } else {
                  $(".popup_info_msg .position-alert1").html('').html("Insert the Progress Successfully");

               }

               if (status == 1) {
                  location.reload();
               }
               $('#modal-alert_insert').show();
            }

         });
      } else {
         $('#error_html').html('Enter Progress');
      }
   });

   $('.add_button').click(function(e) {
      $('#error_html').html('');
      $('#status_name').val('');
      $('#status_id').val('');
      $('#del_status_id').val('');
      $('#add_status').find('.cont').html('').append('Enter the Progress');
      $('#add_status').find('#status_name').attr('type', 'text');
   });


   $('.edit_btn').click(function(e) {
      $('#error_html').html('');
      $('#status_name').val('');
      $('#status_id').val('');
      $('#del_status_id').val('');

      $('#status_name').val($(this).data('status_name'));
      $('#status_id').val($(this).data('id'));
      $('#add_status').find('.cont').html('').append('Enter the Progress');
      $('#add_status').find('#status_name').attr('type', 'text');
   });


   $('.del_btn').click(function(e) {
      $('#error_html').html('');
      $('#status_name').val('');
      $('#status_id').val('');
      $('#del_status_id').val('');
      $('#status_name').val($(this).data('status_name'));
      $('#status_id').val($(this).data('id'));
      $('#del_status_id').val(1);
      $('#add_status').find('#status_name').attr('type', 'hidden');

      $('#add_status').find('.cont').html('').append(' Are you sure want to delete?');
   });

   $(document).on('change', '#select_all_templates', function() {
      var all_temp = [];
      $('#delete_process_id').val('');

      if ($(this).is(':checked', true)) {

         table10.column(1).nodes().to$().each(function(index) {
            $(this).find(".template_checkbox").prop('checked', true);
            // var id = $(this).find(".template_checkbox").data('id');  

         });
         all_temp = getSelectedRow();
         $("#delete_task").show();

         $('#delete_process_id').val(all_temp);
      } else {
         table10.column(1).nodes().to$().each(function(index) {
            $(this).find(".template_checkbox").prop('checked', false);
         });
         //   all_temp = [];
         //alltemplate = [];
         $("#delete_task").hide();

      }
      //  console.log(all_temp);

      //showdelete(all_temp);        
   });



   /*function getframe(heading,$url)
   {
      $('#template_iframe').find('.modal-title').html('');
      $('#template_iframe').find('.modal-title').html(heading);

      $.ajax(
      {
         url:$url,
         async:'true',
         type:'POST',     

         success:function(response)
         {
            $('#template_iframe').find('.modal-body').html('');
            $('#template_iframe').find('.modal-body').html(response);
            $('#temp_butt').trigger('click');
         }

      });
   }   */


   function delete_action() {

      $.ajax({
         type: "POST",
         url: "<?php echo base_url() . 'Task_Status/alltasks_delete'; ?>",
         data: {
            'id': $("#delete_process_id").val(),
            'user_val': $("#del_user_type").val()
         },
         beforeSend: function() {

            $(".LoadingImage").show();
         },
         success: function(data) {

            $(".LoadingImage").hide();

            $(".popup_info_msg").show();
            $('.popup_info_msg .position-alert1').html('Success !!! Progress Successfully Deleted..');
            setTimeout(function() {
               $(".popup_info_msg").hide();
               location.reload();
            }, 1500);

            /*var task_ids = data.split(",");
            for (var i=0; i < task_ids.length; i++ ) { 
            $("#"+task_ids[i]).remove(); } 
            $(".alert-success-delete").show();
            setTimeout(function() { 
            }, 500);  */
         }
      });
   }
</script>
<?php $this->load->view('tasks/import_task'); ?>