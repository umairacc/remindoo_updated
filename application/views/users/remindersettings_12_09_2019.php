<?php 

if($_SESSION['firm_id'] == 0)
{
   $this->load->view('super_admin/superAdmin_header');
}
else
{
   $this->load->view('includes/header');
}

?>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
<style type="text/css">
  .sp-replacer {
  width: 275px;
  border-radius: 50px;
  background: #fff;
 }
 .sp-preview{
  width: 230px;
  height: 23px;    
 }
 
 .sp-preview, .sp-alpha, .sp-thumb-el {
  background-image: none;
  margin: 2px 0px 2px 10px;  
  }

  .sp-dd{
    margin-left: 5px;
  }

</style>

<?php 

      $service = array();

      $service_list = $this->Common_mdl->getServicelist();       

      if(!empty($this->uri->segment(3)))
      {
         foreach ($service_list as $key => $value) 
         {
            if($value['id'] == $this->uri->segment(3))
            {
              $service[$value['id']] = $value['service_name'];
            }
         }
      }
      else
      {
         foreach ($service_list as $key => $value) 
         {
            $service[$value['id']] = $value['service_name'];
         }
      }

      $reminders = array();

      foreach($service as $key => $value) 
      { 
         $reminders[$key][$value] = array();                                
      } 
    
      if(count($reminder_setting)>0)
      { 
         foreach ($reminder_setting as $key1 => $value1) 
         {    
             foreach($service as $key => $value)
             {
                 if($value1['service_id'] == $key)
                 {
                    $reminders[$key][$value][] = $value1;
                 }
             }  
         }
      }   
?>

<div class="modal-alertsuccess alert succ popup_info_msg" style="<?php if($_SESSION['msg']!=""){ echo "display: block"; }else { echo "display: none"; } ?>">
   <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
   <div class="pop-realted1">
   <div class="position-alert1"> 
   <?php if($_SESSION['msg']!=""){ echo $_SESSION['msg']; } ?>      
   </div>
   </div>
   </div>
</div>


<div class="pcoded-content tasktimetop serviceremindercls">
  <div class="pcoded-inner-content">

            <div class="card propose-template" style="display: block;">
            <!-- admin start-->
            <div class="client_section col-xs-12 floating_set">
             
              </div> <!-- all-clients -->
              <div class="client_section user-dashboard-section1 floating_set">
                <div class="all_user-section floating_set proposal-commontab tsk-timeframe">
				
               <div class="deadline-crm1 floating_set">  
                  <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard">
                    <li class="nav-item borbottomcls">
                      <a class="nav-link"  href="javascript:void(0)">Reminder Settings</a>
                      <div class="slide"></div>
                    </li>
                  </ul>
               </div>  
              </div>

              <?php foreach($reminders as $key => $value) {

                       foreach($value as $key1 => $values) {
               ?>
                 
                <div class="reminder_sett">
                  <div class="card-header col-md-12" style="margin-top: 0px;">
                    <div class="reminder_content remind-cis">                      
                      <div class="deadline-crm1 floating_set">
                         <div class="toggle-adds1">
                             <h3><?php echo $key1; ?></h3>
                             <?php if($_SESSION['permission']['Reminder_Settings']['create'] == '1') { ?>
                             <span class="due-date f-right"><a href="javascript:;" data-toggle="modal" data-target="#add-reminder" class="btn-add-task add_reminder" data-service="<?php echo $key; ?>">
                              <i class="fa fa-plus"></i> add reminder</a></span>
                              <?php } ?>
                         </div>
                      </div>
                       
                      <?php if(count($values)>0) { ?>

                      <div class="reminder-setting floating_set">  
                        <div class="email-sec floating_set">
                          <?php 

                          foreach($values as $value1) {

                            $placeholder = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id',$value1['id']); 
                            $exp_contect=explode(',', $placeholder[0]['placeholder']);                                                                                 
                            $content ='';
                            foreach ($exp_contect as $val)
                            {               
                              $content.='<span target="message_'.$value1['id'].'" class="add_merge">'.$val.'</span><br>';
                            } 
                            
                          if($value1['due']=='due_by'){
                          $due='Due in';
                          }elseif($value1['due']=='overdue_by' ){
                          $due='Overdue';
                          }

                          ?>
                          <div class="email-innersec"> 
                            <div class="email-sec1">                  
                              <span class="due-date"><?php echo $due.' '.$value1['days'].' '?>day(s)</span>
                                <?php if($_SESSION['permission']['Reminder_Settings']['edit'] == '1' || $_SESSION['permission']['Reminder_Settings']['delete'] == '1') { ?>
                                  <a href="javascript:;" class="date-editop reminder_popup" data-toggle="modal" data-target="<?php echo "#edit-reminder_".$value1['id']; ?>" data-due="<?php if($value1['due']=='due_by'){  echo 'due in'; }elseif($value1['due']=='overdue_by' ){   echo 'overdue'; } ?>" data-service="<?php echo $value1['service_id'];?>" data-id="<?php echo $value1['id']; ?>" data-duevalue="<?php echo $value1['due']; ?>" data-duedays="<?php echo $value1['days'];?>" data-subject="<?php echo $value1['subject'];?>"  onclick="getColor('<?php echo $value1['id']; ?>','<?php echo $value1['headline_color'];?>');"><i class="fa fa-edit"></i> edit</a>
                                  <?php } ?> <!-- data-message="<?php echo html_entity_decode($value1['message']);?>" -->
                              <div class="position-es">
                                  <i class="fa fa-envelope"></i>
                                </div>
                            </div>
                          </div>

                            <div class="modal fade data-target-reminder edit-reminders addcustom-reminder" id="<?php echo "edit-reminder_".$value1['id']; ?>" role="dialog">
                            <div class="modal-dialog modal-md">    
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close popup_close" id="<?php echo "close_".$value1['id']; ?>" data-dismiss="modal">x</button>
                                  <h4 class="modal-title">edit reminder</h4>
                                </div>
                                <?php if($_SESSION['permission']['Reminder_Settings']['edit'] == '1') { ?>
                                <form action="<?php echo base_url().'User/edit_reminder'; ?>" method="post" accept-charset="utf-8" id="edit_form" novalidate="novalidate">
                                <input type="hidden" name="id" id="edit_id" value="<?php echo $value1['id']; ?>">
                                <div class="modal-body">
                                 <div class="modal-topsec">
                                    <div class="send-invoice">
                                      <div class="send-label">
                                      <span class="invoice-notify">Send</span>
                                          <select name="due" id="edit_due">
                                          <?php 

                                          $due = "";
                                          $due1 = "";

                                          if($value1['due']=='due_by'){ $due = "selected"; }
                                          else{ $due1 = "selected"; }

                                          ?>
                                          <option value="due_by" <?php echo $due; ?>>due by</option>
                                          <option value="overdue_by" <?php echo $due1; ?>>overdue by</option>
                                          </select>
                                    </div>
                                    <div class="send-label">
                                      <span class="days1">Days</span>
                                      <input type="text" class="due-days decimal decimal_days" data-validation="required,number" name="days" id="edit_days" value="<?php echo $value1['days'];?>">
                                    </div>
                                    <div class="send-label">
                                    <span class="invoice-notify">Template</span>
                                    <select name="<?php echo "template_".$value1['id'];?>" id="<?php echo "template_".$value1['id'];?>" onchange="getTemplates(this);">
                                      <option value="">Select</option>
                                      <?php foreach($reminder_templates as $key4 => $value4){ ?>              
                                      <option value="<?php echo $value4['id']; ?>"><?php echo ucwords($value4['title']); ?></option>            
                                      <?php } ?>
                                    </select>
                                    </div>
                                    <div class="send-label">
                                      <span class="days1">Choose Headline Color:</span>
                                      <input type="text" class="" id="<?php echo "headlinecolor_".$value1['id'];?>" name="headline_color" value="<?php echo $value1['headline_color'];?>">
                                    </div>
                                    </div>
                                    <div class="insert-placeholder">                          
                                      <?php echo $content; ?>
                                    </div>
                                 </div><!--modal-topsec-->
                                 <div class="modal-bottomsec">
                                   <input type="text" class="invoice-msg" name="subject" id="<?php echo "edit_subject_".$value1['id'];?>" placeholder="Subject" value="<?php echo $value1['subject'];?>" style="width: 100%;">
                                    <button type="button" class="btn-success reminder-save pull-right eclear" style="margin-top: 5px;" id="<?php echo "clear_".$value1['id']; ?>">Clear</button>
                                    <textarea name="<?php echo "message_".$value1['id']; ?>" data-validation="required" id="<?php echo "editor1_".$value1['id']; ?>" ><?php echo $value1['message'];?></textarea>
                                    <input type="hidden" name="service_id" id="service_id" value="<?php echo $value1['service_id'];?>">
                                 </div>
                                </div>
                                 <?php } ?>
                                <div class="modal-footer text-right">
                                  <?php if($_SESSION['permission']['Reminder_Settings']['delete'] == '1') { ?>
                                  <?php if($value1['firm_id'] == $_SESSION['firm_id']){ ?> 
                                  <button type="button" data-toggle="modal" data-target="<?php echo "#delete_reminder_".$value1['id']; ?>" class="del-tsk12 for_user_permission_delete">Delete</button>
                                  <?php } } ?>
                                  <?php if($_SESSION['permission']['Reminder_Settings']['edit'] == '1') { ?>
                                  <button type="submit" class="reminder-save">save</button>
                                  <?php } ?>
                                </div>
                                <?php if($_SESSION['permission']['Reminder_Settings']['edit'] == '1') { ?>
                                </form>
                                <?php } ?>
                              </div>      
                            </div>
                          </div>

                           <div class="modal fade" id="<?php echo "delete_reminder_".$value1['id']; ?>" role="dialog">
                             <div class="modal-dialog">    
                               <!-- Modal content-->
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h4 class="modal-title">Confirmation</h4>
                                 </div>
                                 <div class="modal-body">
                                 <input  type="hidden" name="delete_task_id" id="delete_task_id" value="">
                                   <p> Are you sure want to delete ?</p>
                                 </div>
                                 <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal" onclick="delete_reminder('<?php echo $value1['id']; ?>');">Yes</button>
                                   <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                 </div>
                               </div>
                             </div>
                          </div>  
                          <?php } ?>                        
                        </div>
                      </div> 
                      <?php } ?>                  
                    </div>
                  </div>
                </div>                
              <?php } } ?>
                </div>
              </div>
         </div>
    </div>
    


   <div class="modal fade addcustom-reminder" id="add-reminder" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">add reminder</h4>
        </div>
        <form action="" method="post" id="addreminder_section" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
              <div class="send-label">
              <span class="invoice-notify">Send</span>
              <select name="due">
                <option value="due_by">due by</option>
                <option value="overdue_by">overdue by</option>
              </select>
              </div>
              <div class="send-label">
                <span class="days1">Days</span>
                <input type="text" class="due-days decimal_days decimal" data-validation="required,number" name="days">
              </div>
              <div class="send-label">
              <span class="invoice-notify">Template</span>
              <select name="template" id="template">
                <option value="">Select</option>
                <?php foreach($reminder_templates as $key => $value){ ?>              
                <option value="<?php echo $value['id']; ?>"><?php echo ucwords($value['title']); ?></option>            
                <?php } ?>
              </select>
              </div>             
              <div class="send-label">
                <span class="days1">Choose Headline Color:</span>
                <input type="text" class="" name="headline_color" id="headline_color1" value="">
              </div>
          </div>
            <div class="insert-placeholder1">
           
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
            <input type="text" class="invoice-msg" id="subjects" name="subject" placeholder="Subject" value="">
            <button type="button" class="btn-success reminder-save pull-right clear" style="margin-top: 5px;" id="clear">Clear</button>
            <textarea name="message" id="editor2" data-validation="required"></textarea>
         </div>
        </div>
        <div class="modal-footer">        
          <button type="submit" class="reminder-save addreminder">save</button>
        </div>
      </form>
      </div>      
    </div>
  </div>

  <?php 

  if($_SESSION['firm_id'] == 0)
  {
     $this->load->view('super_admin/superAdmin_footer');
  }
  else
  {
     $this->load->view('includes/footer');
  }

  ?>  
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
  <script type="text/javascript" src="https://cdn.ckeditor.com/4.9.0/standard-all/ckeditor.js"></script>  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.79/jquery.form-validator.js"></script>
  <script>
    $.validate({
      lang: 'en'
    });
  </script>

<script type="text/javascript">

	$(document).ready(function()
  {
			
      $("#headline_color1").spectrum({
          preferredFormat: "hex",
          showInput: true,
          color: '#eb34de'
      });

      $('.sp-dd').html('');
      $('.sp-dd').html('<img src="https://remindoo.uk/assets/images/new_update1.png">');
     
      /*$(document).on('change','#firm_settings',function()
      { 
  				var id=$(this).val(); 

          if(id!='')
          {
  				  get_reminders(id);
          }
			});*/
	});

  /*$(document).ready(function()
  {
      var service_id = '<?php echo $this->uri->segment(3); ?>';
      
      if(service_id!='')
      { 
        get_reminders(service_id);
      }
    
  });*/

 /* function get_reminders(id)
  {
     $.ajax(
     {
          url: '<?php echo base_url(); ?>/User/reminder_settings/'+id,
          type: 'post',
          data: {'id':id},
          beforeSend: function() 
          {
               $(".LoadingImage").show();
          },
          success: function(response)
          { 
              $(".reminder_content").html('');
              $(".reminder_content").html(response);
              $(".LoadingImage").hide();
          }
     });
  }
*/
  $(document).on('click','.add_reminder',function(){ 
    var value=$(this).data('service');
      $.ajax({
                  url: '<?php echo base_url(); ?>/User/email_content_get/'+value,
                  type: 'post',
                  data: {'id':value},
                  beforeSend: function() {
                       $(".LoadingImage").show();
                  },
                  success: function(response){                   
                      var json = JSON.parse(response);
                      subject=json['subject'];
                      content=json['content'];
                      $("#subjects").val(subject);
                    $(".insert-placeholder1").html(content);
                      $("#addreminder_section").attr('action','<?php echo base_url(); ?>User/add_reminder/'+value);
                       $(".LoadingImage").hide();
                  }
            });
  });


    $(document).on('click','.add_merge',function() 
    {
        var target = $(this).attr('target');  
        var name=$(this).html();    
        var message = $('[name='+target+']').val();
        name = message.concat(name); 
        
        $('[name='+target+']').val(name);       
    }); 

    $(document).on('click','.clear',function() 
    {            
        var clear_id = $(this).attr('id'); 
        var id = $('#'+clear_id).next('[name=message]').attr('id'); 
        $('#'+id).val('');       
    });

	  $(document).on('click','#close_info_msg',function()
    { 
       $('.popup_info_msg').hide();  
    });

    $(document).on('change','#template',function()
    {        
       var tid = $(this).val();

       $.ajax(
       {
          url:'<?php echo base_url().'User/get_template'; ?>',
          type:'POST',
          data:{'id':tid},

          beforeSend: function() {
               $(".LoadingImage").show();
          },

          success:function(template)
          { 
             var temp = JSON.parse(template);
             
             $('#subjects').val(temp.subject);
             $('#editor2').val(temp.body);
             $(".LoadingImage").hide();
          }

       }); 

    });    

</script>

<script type="text/javascript">
  
  $(document).on('click','.eclear',function() 
  {            
      var clear_id = $(this).attr('id'); 
      var len = clear_id.length; 
      var num = clear_id.substr(6,len); 

      var id = $('#'+clear_id).next('[name=message_'+num+']').attr('id'); 
      $('#'+id).val('');       
  });

 function delete_reminder(record_id)
 {   

    $.ajax(
    {
       url: '<?php echo base_url(); ?>User/delete_reminder',
       type: 'POST',
       data: {'record_id':record_id},

       beforeSend: function() 
       {
          $('#close_'+record_id).trigger('click'); 
          $(".LoadingImage").show();
       },
       success:function(status)
       { 
          $(".LoadingImage").hide();
          
          if(status == 1)
          {           
             $('.popup_info_msg').show();
             $('.position-alert1').html("Reminder Deleted Successfully!");   
             window.location.href = '<?php echo base_url().'user/Service_reminder_settings/'; ?>';          
          }
       }

    });
 }

 function getTemplates(obj)
 {        
     var rid = obj.id;
     rid = rid.replace('template_',''); 
     rid = rid.trim();

     var tid = obj.value;
     tid = tid.trim();
   
     $.ajax(
     {
        url:'<?php echo base_url().'User/get_template'; ?>',
        type:'POST',
        data:{'id':tid},

        beforeSend: function() {
             $(".LoadingImage").show();
        },

        success:function(template)
        { 
           var temp = JSON.parse(template);
           
           $('#edit_subject_'+rid).val(temp.subject);
           $('#clear_'+rid).next('[name=message_'+rid+']').val(temp.body);
           $(".LoadingImage").hide();
        }

     }); 

  }

  function getColor(id,colorhsv)
  {
     $("#headlinecolor_"+id).spectrum({
         preferredFormat: "hex",
         showInput: true,
         color: colorhsv
     });
     $('.sp-dd').html('');
     $('.sp-dd').html('<img src="https://remindoo.uk/assets/images/new_update1.png">');
  }

</script>