<?php $this->load->view('includes/header');?>


<?php 



 ?>
<section class="client-details-view various-section01 floating_set sec-settings">
	<div class="client-inform-data1 floating_set">


		<div class="deadline-crm1 floating_set">
		<ul class="class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
				<h4><i class="fa fa-user fa-6" aria-hidden="true"></i> Admin Panel</h4>
			</ul>
	</div>
		
		<div class="information-tab floating_set">		
			<!-- <div class="home-inform1">
				<a href="#">Home /</a>
				<a href="#">Admin Panel /</a>
				<span>Section Settings</span>
			</div> -->
		<h3>Here you can turn on/off various sections of the site.</h3>
		<div class="calendar-section1 calendar-sectioncls">
			<form action="<?php echo base_url(); ?>sample/section_settings" method="post">
				<div class="dash-off floating_set">
				<div class="form-group">
					<label>Dashboard Section</label>
					<p>
						<label class="custom_checkbox1">
					<input type="checkbox" class="user_checkbox" name="MyDesk_Section" <?php if($section_settings['MyDesk_Section']=='0'){ ?> checked="checked" <?php } ?> ></label><span>Enable</span></p>
				</div>
				<div class="form-group">
					<label>Task Section</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Task_Section" <?php if($section_settings['Task_Section']=='0'){ ?> checked="checked" <?php } ?> ></label><span>Enable</span></p>
				</div>
				<div class="form-group">
					<label>CRM Section</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="CRM_Section" <?php if($section_settings['CRM_Section']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>	
				<div class="form-group">
					<label>Proposal Section</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Proposal_Section" <?php if($section_settings['Proposal_Section']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>
				<div class="form-group">
					<label>Clients Section</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Clients_Section" <?php if($section_settings['Clients_Section']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>
				<div class="form-group">
					<label>Deadline Manager Section</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Deadline_Manager_Section" <?php if($section_settings['Deadline_Manager_Section']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>
				<div class="form-group">
					<label>Reports Section</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Reports_Section" <?php if($section_settings['Reports_Section']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>

				<!-- ********* -->

				<div class="form-group">
					<label>Admin Settings</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Admin_Settings" <?php if($section_settings['Admin_Settings']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>
				<div class="form-group">
					<label>Firm Settings</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Firm_Settings" <?php if($section_settings['Firm_Settings']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>	
				<div class="form-group">
					<label>Column Settings</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Column_Settings" <?php if($section_settings['Column_Settings']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>
				
				<div class="form-group">
					<label>Team and Management Settings</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Team_and_Management_Settings" <?php if($section_settings['Team_and_Management_Settings']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>
				<div class="form-group">
					<label>Tickets</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Tickets_Section" <?php if($section_settings['Tickets_Section']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>

				<div class="form-group">
					<label>Chat</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Chat_Section" <?php if($section_settings['Chat_Section']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>
					<div class="form-group">
					<label>Documents</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Documents_Section" <?php if($section_settings['Documents_Section']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>
					<div class="form-group">
					<label>Invoice</label>
					<p><label class="custom_checkbox1"><input type="checkbox" class="user_checkbox" name="Invoice_Section" <?php if($section_settings['Invoice_Section']=='0'){ ?> checked="checked" <?php } ?>></label><span>Enable</span></p>
				</div>
			</div>
				<div class="form-group button-submit01">
						<button type="submit" name="submit">Update</button>
				</div>		

			</form>
			
		</div> <!-- calendar-section -->
	</div>
</div>
</section>		


	
<?php $this->load->view('includes/footer');?>