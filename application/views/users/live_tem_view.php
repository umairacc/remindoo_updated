<?php $this->load->view('includes/header'); ?>
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
  .add_class{
    display: none;
  }
  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}

  tfoot {
    display: table-header-group;
}
</style>
<!-- <div class="container">
  <h2>Modal Example</h2>
  <!-- Trigger the modal with a button -->
 <!--  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

  <!-- Modal -->
  <!-- <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
    <!--   <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div> --> 



  <!-- <div class="modal fade" id="Bulkmy_Modal" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input type="hidden" name="archive_task_id" id="Bulkarchive_task_id" value="">
          <p>Do You Want Archive this Task?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default Bulkarchive_task" >yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div> -->

<style>

/** 28-06-2018 **/
.time{
	    padding: 8px;
    font-weight: bold;
}
.new_play_stop{
	    width: 20px;
    height: 20px;
    background: #000;
    color: #fff;
    line-height: initial;
    text-align: center;
    font-size: 15px;
    vertical-align: top;
    border-radius: 50%;
    margin: 7px 0 0 0;
}
/** for timer style 28-06-2018 **/
   /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   /** 04-06-2018 **/
   select.close-test-04{
    display: none;
   }
   /** 04-06-2018 **/
   span.demo {
   padding: 0 10px;
   }
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   img.user_imgs {
   width: 38px;
   height: 38px;
   border-radius: 50%;
   display: inline-block;
   }


a.adduser1 {
    background: #38b87c;
    color: #fff;
    padding: 3px 7px 3px;
    border-radius: 6px;
    vertical-align: middle;
    display: inline-block;
}
   #adduser label {
   text-transform: capitalize;
   font-size: 14px;
   }
  span.hours-left {
    background: #f76480;
    color: #fff;
    padding: 3px 10px;
    border-radius: 3px;
    margin-right: 15px;
}
   .dropdown12 button.btn.btn-primary {
   background: #ccc;
   color: #555;
   font-size: 13px;
   padding: 4px 10px 4px;
   margin-top: -2px;
   border-color:transparent;
   }
   select + .dropdown:hover .dropdown-menu
   {
   display: none;
   }
   select + .dropdown12 .dropdown-menu
   {
   padding: 0 !important;
   }
   select + .dropdown12 .dropdown-menu a {
   color: #000 !important;
   padding: 7px 10px !important;
   border: none !important;
   font-size: 14px;
   }
   select + .dropdown12 .dropdown-menu li {
   border: none !important;
   padding: 0px !important;
   }
   select + .dropdown12 .dropdown-menu a:hover {
   background:#4c7ffe ;
   color: #fff !important;
   }
   span.created-date {
   color: gray;
   padding-left: 10px;
   font-size: 13px;
   font-weight: 600;
   }
   span.created-date i.fa.fa-clock-o {
   padding: 0 5px;
   }
   .dropdown12 span.caret {
   right: -2px;
   z-index: 99999;
   border-top: 4px solid #555;
   border-left: 4px solid transparent;
   border-right: 4px solid transparent;
   border-bottom: 4px solid transparent;
   top: 12px;
   position: relative;
   }
   span.timer {
   padding: 0 10px;
   }
   body {
   font-family:"Arial", Helvetica, sans-serif;
   text-align: center;
   }
   #controls{
   font-size: 12px;
   }
   #time {
   font-size: 150%;
   }
</style>


<link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/responsive.dataTables.css">
<link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/progress-circle.css">

<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section col-xs-12 floating_set">
                           
                           <div class="all_user-section floating_set">
                              
<!-- success message -->
            
<!-- end of success message -->


                              <div class="all_user-section2 floating_set">
                                 <div class="tab-content">
                                    <div id="alltasks" class="tab-pane fade in active">
                                    




<table  class="table client_table1 all_task_table text-center display nowrap">
<thead><tr><th>User Name</th><th>Task Name</th><th>status</th></tr></thead><tbody>
<?php 
foreach ($live_worker as $v) 
{
?>

<tr><td><?=$v['crm_name']?></td><td><?=$v['subject']?></td><td>In Prograss</td></tr>
<?php
}
?> 
</tbody>
</table>
                                      
                                       <div class="client_section3  floating_set">
                                          <div id="status_succ"></div>
                                          <div class="all-usera1 table-responsive user-dashboard-section1 for_task_status">
                                 
                                 
                              </div>
                           </div>
                           <!-- admin close -->
                        </div>
                        <!-- Register your self card end -->
                     </div>
                  </div>
               </div>
               <!-- Page body end -->
            </div>
         </div>
         <!-- Main-body end -->
         <div id="styleSelector">
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>



<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
   <h1>Warning!!</h1>
   <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
   <div class="iew-container">
      <ul class="iew-download">
         <li>
            <a href="http://www.google.com/chrome/">
               <img src="assets/images/browser/chrome.png" alt="Chrome">
               <div>Chrome</div>
            </a>
         </li>
         <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
               <img src="assets/images/browser/firefox.png" alt="Firefox">
               <div>Firefox</div>
            </a>
         </li>
         <li>
            <a href="http://www.opera.com">
               <img src="assets/images/browser/opera.png" alt="Opera">
               <div>Opera</div>
            </a>
         </li>
         <li>
            <a href="https://www.apple.com/safari/">
               <img src="assets/images/browser/safari.png" alt="Safari">
               <div>Safari</div>
            </a>
         </li>
         <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
               <img src="assets/images/browser/ie.png" alt="">
               <div>IE (9 & above)</div>
            </a>
         </li>
      </ul>
   </div>
   <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->


<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<!-- <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>  -->
<!-- <script src="<?php echo base_url();?>js/jquery.workout-timer.js"></script> -->
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.tagsinput.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
 <script type="text/javascript" language="javascript" src="http://remindoo.org/CRMTool/assets/js/dataTables.responsive.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/js/jquery.tagsinput.js"></script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script> 
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>




    
<!-- end of 29-06-2018 -->
<!-- 30-07-2018 -->

<!-- end of 30-07-2018 -->