<div class="card cardfull for_user_assign">
    <div class="card-block user-box assign-user"><?php
        if (count($explode_worker) > 0){
            $userwiseTimers = $this->Task_creating_model->getSubtaskTimers($task_form[0]['sub_task_id']);
            
            foreach($userwiseTimers as $uk=>$ut){
                $getUserProfilepic = $this->Common_mdl->getUserProfilepic($uk);
                $get_staff_data    = $this->Common_mdl->GetAllWithWhere('user', 'id', $uk); ?>

                <div class="media">
                    <div class="media-left media-middle photo-table">
                        <input type="hidden" class="staff_id" id="staff_id" value="<?php echo $uk; ?>">
                        <a href="#">
                            <img src="<?php echo $getUserProfilepic; ?>" alt="Not Found" onerror="this.src='<?php echo base_url() ?>assets/images/avatar-3.jpg';" >                   
                        </a>
                    </div>
                    <div class="media-body">
                        <h6><?php echo isset($get_staff_data[0]['crm_name']) ? $get_staff_data[0]['crm_name'] : ''; ?></h6><?php 
                        $get_user_status1 = $this->Common_mdl->get_field_value('staff_form', 'roles', 'user_id', $uk);
                        $get_user_status  = $this->Common_mdl->get_field_value('roles_section', 'role', 'id', $get_user_status1); ?>
                        <p> <?php echo $get_user_status; ?> </p>                                    
                    </div>
                    <div>
                        <div class="time" style="width: 100px !important;"><?php 
                            echo $this->Task_creating_model->sec_to_time($ut['total_time_spent']); ?>
                        </div>
                    </div>
                </div><?php

                if (count($get_staff_data) > 0){
                    $sub_task_id = array_filter(explode(",",$task_form[0]['sub_task_id']));
                    
                    if(count($sub_task_id)>0){
                        foreach($ut as $u){ 
                            if(gmdate("H:i:s", $u['time_spent']) != '00:00:00'){ ?>              
                                <div class="media">
                                    <div class="media-left media-middle photo-table"></div>
                                    <div class="media-body"><?php echo $u['subject']; ?></div>
                                    <div><div class="time" style="width: 100px !important;"><?php echo $this->Task_creating_model->sec_to_time($u['time_spent']); ?></div></div>
                                </div><?php
                            }
                        }
                    }
                }
            }            
        } ?>
    </div>
</div>