  <?php   

  if( !empty( $_SESSION['is_superAdmin_login'] ) )
  {
    $this->load->view('super_admin/superAdmin_header');
  }
  else
  {
    $this->load->view('includes/header');
  }


  $succ = $this->session->flashdata('success');
  $error = $this->session->flashdata('error');
  $userid = $this->session->userdata('id'); 
  ?>
<style type="text/css">
   .to-do-list.sortable-moves
   {
    padding-left: 0;
   }
   .pcoded-content
   {
     float: left !important;
     width:100%;
   }
   .card-block.sub_contents
   {
    margin: 0;
    padding: 15px !important;
    float: left;
    width: 100%;
    height: auto;
    overflow: auto;
   }
   .CONTENT_NAME .card-header h5
   {
    color: #22b14c; 
   }
</style>
<!-- show info  -->
  <div class="modal-alertsuccess alert info_popup" style="display:none;">
    <div class="newupdate_alert">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
     <div class="pop-realted1">
        <div class="position-alert1 info-text">
         </div>
        </div>
     </div>
  </div>
  <!-- end show info  -->

 <div class="modal fade" id="custom_add_edit_popup" role="dialog">
    <div class="modal-dialog modal-firm-field modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Field</h4>
        </div>
        <form id="custom_add_edit_form" class="validation" method="post" action="" enctype="multipart/form-data"> 
          <div class="modal-body">
            <div class="field-name1 form-group">
              <label class="firm-label">field name</label>
              <input type="text" id="name" name="name">
            </div>
            <div class="field-type1 coustom_field_content">
                <label class="firm-label">field type</label>
              <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                <option value="input">Input</option>                
                <option value="number">Number</option>
                <option value="url">URL</option>
                <option value="email">Email</option>
                <option value="textarea">Textarea</option>
                <option value="select">Select</option>                            
                <option value="date_picker">Date Picker</option>
                <option value="checkbox">Checkbox</option>
              </select>            
              <div id="options_wrapper" style="display:none;">
                <div class="form-group" app-field-wrapper="options">
                  <label for="options" class="control-label"> 
                  <small class="req text-danger">*</small>Options</label>
                  <textarea id="options" name="options" class="form-control" rows="3"></textarea>
                </div>  
              </div>
              <input type="hidden" name="custom_id">
            </div>
            
            <div class="field-name1 form-group">            
              <label class="custom_checkbox1">
                <input type="checkbox" id="required" name="required" value="1">
                <i></i>
              </label>
              <span>Is Required</span>
            </div>

            <input type="hidden" name="id">
            <input type="hidden" name="section_name"  value="">
            <input type="hidden" name="sub_content" value="">            
          </div>
          <div class="modal-footer">
            <div class="modal-save">
                <input type="submit" class="add_new_field" value="Save"/>
              </div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div> 
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="defult_field_edit_popup" role="dialog">
    <div class="modal-dialog modal-md modal-edit-firm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Field</h4>
        </div>
        <form id="defult_field_edit_form" class="validation" method="post" action="" enctype="multipart/form-data"> 
          <div class="modal-body">
             <div class="field-name1 form-group">
              <label>field name</label>
              <input type="text" id="field_name" name="name">
              <input type="hidden" id="field_id" name="id">
            </div>
          </div>
          <div class="modal-footer">
            <div class="modal-save">
                <input type="submit" class="add_new_field" value="Save"/>
              </div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div> 
        </form>
      </div>
    </div>
  </div>
  <div class="pcoded-content">
    <div class="pcoded-inner-content">
      <div class="page-body">
        
  <div class="firmmytodoclstop firm-field-wrapper">
    <div class="firmmytodoclsbottom">
      <?php
      foreach ($section_data as $key => $section_value)
      {
       ?>
        <div class="col-xl-6 col-sm-6 lf-mytodo">
        <!-- To Do Card List card start -->
        <div class="card fr-hgt SECTION_NAME" data-id="<?php echo $key;?>">
           <div class="card-header">
              <h5>
                 <?php

                  $section_name =  ( !empty( $dynamic_service[$key] ) ? $dynamic_service[$key] : $key );
                  echo ucwords( preg_replace( ['/_/','/-/'] , [' ','/'] , $section_name ) ); 
                  
                  ?>
                 <span class="f-right">
                 </span>
              </h5>
           </div>
           <div class="card-block widnewtodo">
              <div class="tab-content tabs card-block">
                 <div class="tab-pane active" id="home1" role="tabpanel">
                    <div class="new-task" id="draggablePanelList">
                       <div class="no-todo">                                            
                        <?php
                        foreach ($section_value as $content_key => $content_value)
                        {
                          ?>
                        <div class="card fr-hgt CONTENT_NAME" data-id="<?php echo $content_key;?>">
                         
                          <div class="card-header">
                            <h5>
                            <?php if( $content_key != '' )
                            {
                             echo ucwords ( preg_replace( ['/_/','/-/'] , [' ','/'] , $content_key ) );
                            } 
                            if( $_SESSION['permission']['Firm_Fields_Settings']['create'] == 1 )
                            {
                            ?>
                               <span class="f-right"> 
                               <div class="add-extrafield">
                                 <span class="ex-field" data-section="<?php echo $key;?>" data-content="<?php echo $content_key;?>"><i class="fa fa-plus"></i></span>
                                 </div>  
                               </span>
                            <?php
                            } 
                            ?>   
                            </h5>
                          </div>

                          <div class="card-block sub_contents ">
                            <div class="sortable">
                              <?php 
                                foreach ($content_value as $element_key => $element)
                                {
                                  $is_costom = ($element['custom_id']!=0)?'coustom_field':'';
                              ?>
                                <div class="to-do-list card-sub <?php echo $is_costom;?>" data-id="<?php echo $element['id']; ?>">
                                  <span class="field_lable"><?php echo $element['field_name']; ?></span>
                                  <div class="f-right">
                                    <?php 
                                    if( $_SESSION['permission']['Firm_Fields_Settings']['edit'] == 1 )
                                    {
                                    ?>
                                    <a href="javascript:;" class="edit_position edit_fields <?php echo $is_costom;?>" data-id="<?php echo $element['id'];?>">
                                      <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                    </a>
                                    <?php
                                    }
                                    if( $_SESSION['permission']['Firm_Fields_Settings']['delete'] == 1 )
                                    {
                                      ?>
                                    <a href="javascript:;" class="delete_position delete_fields <?php echo $is_costom;?>" data-id="<?php echo $element['id'];?>">
                                      <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                    </a>
                                    <?php } ?>


                                  </div>                 
                                </div>
                              <?php     
                                }
                              ?>
                            </div>
                          </div>
                        </div>  
                        <?php    
                        }
                        ?>
                        </div>
                       </div> 
                    </div>
                 </div>
              </div>
           </div>
        </div>  
      <?php
      }
      ?> 
    </div>
  </div>
      </div>
    </div>
  </div>
<?php

if( !empty( $_SESSION['is_superAdmin_login'] ) )
  {
    $this->load->view('super_admin/superAdmin_footer');
  }
  else
  {      
    $this->load->view('includes/footer');
  }


?>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/Sortable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/sortable-custom.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    //last 3 for other required fields
    var duedate_fields = "<?php echo $mandatory_fields['ids'];?>";
        duedate_fields = duedate_fields.split(',');
    duedate_fields.forEach(function(v,i){
      $('.delete_fields[data-id="'+v+'"]').addClass('disabled');
    });
    
    $( ".sub_contents div.sortable" ).sortable({
      axis:'Y',
      scroll : true,
      update: function(event, ui) {
        console.log(ui.item.length+" UI ITEM length");
        console.log(ui.item.parent().attr('class')+"class"+ui.item.parent().children('.ui-sortable-handle').length);
        var Reorder_updates = [];
        ui.item.parent().children('.ui-sortable-handle').each(function()
          { 
            /* console.log( "elem id"+$(this).attr('data-id'));
            //2nd index refer to custom field
            var data = [ $(this).attr('data-id') , 0 ];
            if( $(this).hasClass('coustom_field') )
            {
              data[1] = 1;
            }*/
            Reorder_updates.push( $(this).attr('data-id') );
          });
        $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field_order",
              data:  { 'datas' : Reorder_updates }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){

                 location.reload();
              }
            });
      }  
    });

    $(".edit_fields").click(function()
    { 
      var data = {};
        data['id'] = $(this).attr('data-id');
      if( ! $(this).hasClass('coustom_field') )
      {
        var id  = data['id'];
        var label = $(this).closest(".ui-sortable-handle").find('.field_lable').text().trim();
        //console.log('id'+id+'label'+label);
        $('#defult_field_edit_popup').find('#field_name').val( label );
        $('#defult_field_edit_popup').find('#field_id').val( id );
        $('#defult_field_edit_popup').modal('show');
      }
      else
      {
        $.ajax({
                url: '<?php echo base_url();?>user/custom_firmField_info',
                dataType : 'json',
                type : 'POST',
                data : data,
                beforeSend : function()
                {
                  $(".LoadingImage").show();
                },
                success: function(data)
                { 
                  $('#custom_add_edit_popup').find('modal-title').text("Edit Field");
                  $('#custom_add_edit_popup').find('input[name="id"]').val( data.id );
                  $('#custom_add_edit_popup').find('input[name="custom_id"]').val( data.field_propriety );
                  $('#custom_add_edit_popup').find('input[name="name"]').val( data.label );
                  $('#custom_add_edit_popup').find('textarea[name="options"]').val( data.options );
                  if(data.required==1)
                  {
                    $('#custom_add_edit_popup').find('input[name="required"]').prop( "checked" ,true );
                  }
                  $('#custom_add_edit_popup').find('select.selectpicker').val( data.type ).trigger('change');
                  $('#custom_add_edit_popup').modal('show');
                  $(".LoadingImage").hide();               
                }
              }); 
      }
      
    });

    $("#defult_field_edit_form").validate({        
      ignore: false,     
      rules: {
        name: {required: true},
      },
      submitHandler: function(form)
      {
        var formdata = new FormData( $("#defult_field_edit_form")[0] );
        $.ajax({
                url: '<?php echo base_url();?>user/UpdateDefult_FirmField',
                dataType : 'json',
                type : 'POST',
                data : formdata,
                contentType : false,
                processData : false,
                beforeSend:function(){$('.LoadingImage').show();},
                success: function(data)
                { 
                  $('.LoadingImage').hide();
                  $('#defult_field_edit_popup').modal('hide');
                  if(data.result)
                    {
                      $('.info_popup .info-text').html('Field Save Success Fully!..');
                      $('.info_popup').show();
                      setTimeout(function(){location.reload();},1000); 
                    }
                }
              }); 
      }
    });


$(".ex-field").on('click',function() { 
        var section = $(this).data('section');
        var content = $(this).data('content');
        $("#custom_add_edit_popup").find('input , textarea').not("input[type='submit']").val('');
        $("#custom_add_edit_popup").find('select').val('').trigger('change');
        $('#custom_add_edit_popup').find('modal-title').text("Add New Field");
        $("#custom_add_edit_popup").find('input[name="section_name"]').val( section );
        $("#custom_add_edit_popup").find('input[name="sub_content"]').val( content );
        $("#custom_add_edit_popup").modal('show');
      });


$(".selectpicker").change(function(){
  var rec = $(this).val();
  if(rec=='select' || rec=='checkbox' )
  {
     $(this).next().show();    
  }
  else
  {
      $(this).next().hide();        
  }
}); 

$("#custom_add_edit_form").validate({

      ignore: false,     
      rules: {
      name: {required: true},
      type: {required: true},
      },
      errorElement: "span" , 
      errorClass: "field-error",                             
       messages: {
        name: "Enter a name",
       },
      beforeSend:function(){$('.LoadingImage').show();},
      submitHandler: function(form) {
          $(".LoadingImage").show();

          var formData = new FormData($("#custom_add_edit_form")[0]);


          $.ajax({
              url: '<?php echo base_url();?>user/firm_custom_fields_action',                                
              type : 'POST',
              data : formData,
              contentType : false,
              processData : false,
              success: function(data) {
                  console.log(data);
              $(".LoadingImage").hide();
              $('#custom_add_edit_popup').modal('hide');
              if(data)
                  {
                     $('.info_popup .info-text').html('Field Save Success Fully!..');
                  }
                  else
                  {
                     $('.info_popup .info-text').html('Some thing oops try again!..');
                  }
                $('.info_popup').show();
                setTimeout(function(){location.reload();},1000); 
              },
              error: function() { $('.alert-danger').show();
                      $('.alert-success').hide();}
          });

          return false;
      } 
  });

  $(".delete_fields").click(function(){
      var data = {};
      data['id'] = $(this).attr('data-id');
      /*if( $(this).hasClass('coustom_field') )
      {
        data['is_custom'] = 1;
      }*/
      var action = function(){ 
      $('#Confirmation_popup').modal('hide'); 
              $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>User/delete_firm_field",
                    data:  data, 
                    beforeSend: function() {
                       $(".LoadingImage").show();
                    },
                    success: function(data){
                      $(".LoadingImage").hide();  
                      if(data)
                      {
                        $('.info_popup .info-text').html('Deleted Success Fully..');
                        $('.info_popup').show();
                        setTimeout(function(){location.reload();},1000);
                      }
                                    
                    }
                  });
            };
        Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to delete this field?'});  
        $('#Confirmation_popup').modal('show');
    });
$(document).on("click",".cancel-btns", function(){
  $(this).parents('.firm-fields-div').hide();
});
$(document).on("click","a.close",function(){
  $(this).closest('div.alert').hide();
});
   
});
   </script>