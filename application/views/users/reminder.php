<?php 
if($_SESSION['firm_id'] == '0')
{
   $this->load->view('super_admin/superAdmin_header');
}
else
{
   $this->load->view('includes/header');
}
?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
<link rel="stylesheet" type="text/css" href="https://www.tiny.cloud/css/codepen.min.css">

<style type="text/css">
  .sp-replacer {
  width: 95% !important;
  border-radius: 50px;
  background: #fff;
 }
 .sp-preview{
  width: 90% !important;
  height: 23px;    
 }
 
 .sp-preview, .sp-alpha, .sp-thumb-el {
  background-image: none;
  margin: 2px 0px 2px 10px !important;  
  }

  .sp-dd{
    margin-left: 3%;
  }

  #editor1 {
  border: 1px solid #ccc;
  padding: 0 20px;
  background: #fff !important;
  position: relative; 
  }
  
  div#editor1 {
  min-width: 100%;
  max-width: 700px;
  padding: 15px;
  overflow: auto;
  height: 250px;
  margin-top: 100px;
  }

  .form-error
  {
  	color: red;
  }

</style>

<div class="pcoded-content pcodedteam-content allemailclsnew" style="margin-top: -60px !important;">
<div class="pcoded-inner-content">
<!-- Main-body start -->
<div class="main-body">
<div class="page-wrapper">
<!-- Page body start -->
<div class="page-body">
<div class="row">              
<!--start-->
<div class="col-sm-12">
   <!-- Register your self card start -->
   <div class="card propose-template reminder-propose-wrapper">     


    <div class="deadline-crm1 floating_set">                 
      <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard">
        <li class="nav-item borbottomcls">
          <a class="nav-link"  href="javascript:void(0)">Reminder</a>
          <div class="slide"></div>
        </li>
      </ul>      
    </div>                     

                      
<div class="all_user-section floating_set pros-editemp">
 
   <div class="all_user-section2 floating_set">

      <div class="tab-content">
         <div id="allusers" class="tab-pane fade in active">
            <div id="task"></div>
            <div class="client_section3 table-responsive">
               <div class="status_succ"></div>
               <div class="all-usera1 data-padding1 ">

           	   <?php if($_SESSION['permission']['Reminder_Settings']['create'] == '1' && !isset($record)){ ?>

           	   <form action="<?php echo base_url().'User/add_reminder/'.$service_id.''; ?>" method="post" id="addreminder_section" accept-charset="utf-8" novalidate="novalidate">
	       	       <div class="modal-body col-md-12">
	       	        <div class="modal-topsec modal-topsec-reminder">
	       	           <div class="send-invoice">
	       	             
	       	             <div class="send-label col-md-6">
	       	             <span class="invoice-notify">Send</span>
	       	             <select name="due" class="clr-check-select">
	       	               <option value="due_by">Before</option>
	       	               <option value="overdue_by">After</option>
	       	             </select>
	       	             </div>
	       	             <div class="send-label col-md-6">
	       	               <span class="days1">Days</span>
	       	               <input type="text" class="due-days decimal_days decimal clr-check-client" data-validation="required,number" name="days" style="width: 95%;">
	       	             </div>
	       	             
	       	            
	       	             <div class="send-label col-md-6">
	       	             <span class="invoice-notify">Template</span>
	       	             <select name="template" id="template" class="clr-check-select">
	       	               <option value="">Select</option>
	       	               <?php foreach($reminder_templates as $key => $value){ ?>              
	       	               <option value="<?php echo $value['id']; ?>"><?php echo ucwords($value['reminder_heading']); ?></option>            
	       	               <?php } ?>
	       	             </select>
	       	             </div>             
	       	             <div class="send-label col-md-6" style="display: none;">
	       	               <span class="invoice-notify">Choose Headline Color:</span><br>
	       	               <input type="text" class="due-days decimal_days decimal" name="headline_color" id="headline_color1" value="">
	       	             </div>
	       	            
	       	         </div>
	       	           <div class="insert-placeholder1" style="margin-left: 17px !important;">
	       	            <?php echo $content; ?>
	       	           </div>
	       	        </div><!--modal-topsec-->
	       	        <div class="modal-bottomsec modal-bottomsec-reminder" style="margin-left: 17px !important;width:95% !important;">
                   <div class="email-btn-wrapper">
                     <div class="clear-reminder-btn">
                       <input type="text" class="invoice-msg clr-check-client" id="subjects" name="subject" placeholder="Subject" value="<?php echo $subject; ?>" data-validation="required">
                       <button type="button" class="btn-success reminder-save pull-right clear" style="margin-top: 5px;" id="clear">Clear</button>
                     </div>
                   </div>
	       	           <textarea id="editor1" name="message">
                       
                     </textarea>	       	          
                     <span id="editor1_er"></span>	       	           
	       	        </div>
	       	       </div>
	       	       <div class="modal-footer modal-footer-save-reminder">        
	       	         <button type="submit" class="reminder-save addreminder">save</button>
	       	       </div>
           	    </form>
                
                <?php }else if($_SESSION['permission']['Reminder_Settings']['edit'] == '1' && isset($record) && count($record)>0) { ?>			

				<form action="<?php echo base_url().'User/edit_reminder'; ?>" method="post" accept-charset="utf-8" id="edit_form" novalidate="novalidate">
				<input type="hidden" name="id" id="edit_id" value="<?php echo $record['id']; ?>">
				<div class="modal-body col-md-12">
				<div class="modal-topsec">
				<div class="send-invoice">
				  <div class="send-label col-md-6">
				  <span class="invoice-notify">Send</span>
				      <select name="due" id="edit_due" class="clr-check-select">
				      <?php 

				      $due = "";
				      $due1 = "";

				      if($record['due']=='due_by'){ $due = "selected"; }
				      else{ $due1 = "selected"; }

				      ?>
				      <option value="due_by" <?php echo $due; ?>>Before</option>
				      <option value="overdue_by" <?php echo $due1; ?>>After</option>
				      </select>
				</div>
				<div class="send-label col-md-6">
				  <span class="days1">Days</span>
				  <input type="text" class="due-days decimal decimal_days clr-check-client" data-validation="required,number" name="days" id="edit_days" value="<?php echo $record['days'];?>" style="width: 95%;">
				</div>

				<div class="send-label col-md-6" >
				<span class="invoice-notify">Template</span>
				<select class="clr-check-select" name="<?php echo "template_".$record['id'];?>" id="<?php echo "template_".$record['id'];?>" onchange="getTemplates(this);">
				  <option value="">Select</option>
				  <?php foreach($reminder_templates as $key4 => $value4){ ?>              
				  <option value="<?php echo $value4['id']; ?>"><?php echo ucwords($value4['reminder_heading']); ?></option>            
				  <?php } ?>
				</select>
				</div>

				<div class="send-label col-md-6" style="display: none;">
				  <span class="invoice-notify">Choose Headline Color:</span><br>
				  <input type="text" class="due-days decimal_days decimal" id="<?php echo "headlinecolor_".$record['id'];?>" name="headline_color" value="<?php echo $record['headline_color'];?>">
				</div>
				</div>
				<div class="insert-placeholder" style="margin-left: 17px !important;">                          
				  <?php echo $content; ?>
				</div>
				</div><!--modal-topsec-->
				<div class="modal-bottomsec" style="margin-left: 17px !important;width:95% !important;">
          <div class="email-btn-wrapper">
            <div class="clear-reminder-btn">
              <input type="text" class="invoice-msg clr-check-client" name="subject" id="<?php echo "edit_subject_".$record['id'];?>" placeholder="Subject" value="<?php echo $record['subject'];?>" data-validation="required">
              <button type="button" class="btn-success reminder-save pull-right eclear" style="margin-top: 5px;" id="<?php echo "clear_".$record['id']; ?>">Clear</button>
            </div>
          </div>
				<textarea id="editor1" name="message">
					<?php echo $record['message'];?>
        </textarea> 
				
        <span id="editor1_er"></span>
		
				<input type="hidden" name="service_id" id="service_id" value="<?php echo $record['service_id'];?>">
				</div>
				</div>
				<?php } ?>
				<div class="modal-footer text-right">
				<?php if($_SESSION['permission']['Reminder_Settings']['delete'] == '1' && isset($record) && count($record)>0) { ?>
				<?php if($record['firm_id'] == $_SESSION['firm_id'] && $record['super_admin_owned']==0){ ?> 
				<button type="button" data-toggle="modal" data-target="<?php echo "#delete_reminder_".$record['id']; ?>" class="del-tsk12 for_user_permission_delete">Delete</button>
				<?php } } ?>
				<?php if($_SESSION['permission']['Reminder_Settings']['edit'] == '1' && isset($record) && count($record)>0) { ?>
				<button type="submit" class="reminder-save" style="margin-right: 3.8% !important;">save</button>
				<?php } ?>
				</div>
				<?php if($_SESSION['permission']['Reminder_Settings']['edit'] == '1' && isset($record) && count($record)>0) { ?>
				</form>
				<?php } ?>

                      </div>
                      <!-- List -->           
                   </div>
                </div>
             </div>
          </div>
       </div>
           
         <!-- Page body end -->
   </div>
   </div>
   <!-- Main-body end -->
   <div id="styleSelector">
   </div>
</div> 
</div>
</div>
</div>
</div>
</div>      

 <div class="modal fade" id="<?php echo "delete_reminder_".$record['id']; ?>" role="dialog">
   <div class="modal-dialog">    
     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Confirmation</h4>
       </div>
       <div class="modal-body">
       <input  type="hidden" name="delete_task_id" id="delete_task_id" value="">
         <p> Are you sure want to delete ?</p>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="delete_reminder('<?php echo $record['id']; ?>');">Yes</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
       </div>
     </div>
   </div>
</div>  

<?php 

if($_SESSION['firm_id'] == '0')
{
   $this->load->view('super_admin/superAdmin_footer');
}
else
{
   $this->load->view('includes/footer');
}

?> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
<!-- <script type="text/javascript">

	CKEDITOR.config.toolbar = [
	['Styles','Format','Font','FontSize'],
	'/',
	['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
	'/',
	['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	['Image','Table','-','Link','Flash','Smiley','TextColor','BGColor','Source']
	] ;

	'use strict';

	CKEDITOR.config.extraPlugins = 'colorbutton,colordialog';

</script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.9.0/standard-all/ckeditor.js"></script>  --> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.79/jquery.form-validator.js"></script>


<script>
  $.validate({
    lang: 'en'
  });
</script>

<script>
	$(document).ready(function(){
		$(".clr-check-client").keyup(function(){
			var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-client-outline");
			}else{
				$(this).removeClass("clr-check-client-outline");
			}
		});
    $(".clr-check-client").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-client-outline");
      }  
    })

		$(".clr-check-select").change(function(){
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		});
    $(".clr-check-select").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-select-client-outline");
      }  
    })
	});

</script>

<script type="text/javascript">

$(document).ready(function()
{ 
  /* tinymce editor*/
  tinymce_config['images_upload_url'] = "<?php echo base_url();?>Firm/uplode_mail_header_image";
  tinymce_config['selector']          = '#editor1';

  tinymce.init( tinymce_config );
  /* tinymce editor*/

   $('.header-navbar').css('display','none');

   <?php if(isset($record) && count($record)>0) { ?>
   getColor('<?php echo $record['id']; ?>','<?php echo $record['headline_color']; ?>');
   <?php } ?>   
       
});
 
 $('#addreminder_section,#edit_form').submit(function(event)
 { 	 
 	   $(".LoadingImage").show();

     var message = tinyMCE.get('editor1').getContent();

     if(message == "<p><br></p>" || message == "")
     {
        event.preventDefault();
        $('#editor1_er').html('Required.').css('color','red');
        $(".LoadingImage").hide();
     }
     else
     {
        $('#editor1_er').html('');        
     }
 })

 function delete_reminder(record_id)
 {   

    $.ajax(
    {
       url: '<?php echo base_url(); ?>User/delete_reminder',
       type: 'POST',
       data: {'record_id':record_id},

       beforeSend: function() 
       {
          $('#close_'+record_id).trigger('click'); 
          $(".LoadingImage").show();
       },
       success:function(status)
       { 
          $(".LoadingImage").hide();
          
          if(status == 1)
          {           
             window.close();         
          }
       }

    });
 }

 function getTemplates(obj)
 {        
     var rid = obj.id;
     rid = rid.replace('template_',''); 
     rid = rid.trim();

     var tid = obj.value;
     tid = tid.trim();
   
     $.ajax(
     {
        url:'<?php echo base_url().'User/get_template'; ?>',
        type:'POST',
        data:{'id':tid},

        beforeSend: function() {
             $(".LoadingImage").show();
        },

        success:function(template)
        { 
           var temp = JSON.parse(template);
           
           $('#edit_subject_'+rid).val(temp.subject);
           tinyMCE.get('editor1').setContent( temp.body );
           $(".LoadingImage").hide();
        }

     }); 

  }

  $(document).on('change','#template',function()
  {        
     var tid = $(this).val();

     $.ajax(
     {
        url:'<?php echo base_url().'User/get_template'; ?>',
        type:'POST',
        data:{'id':tid},
        beforeSend: function() {
             $(".LoadingImage").show();
        },
        success:function(template)
        { 
           var temp = JSON.parse(template);
           
           $('#subjects').val(temp.subject);           
           tinyMCE.get('editor1').setContent( temp.body );
           $(".LoadingImage").hide();
        }
     }); 
   });    
 
 function getColor(id,colorhsv)
 {
    $("#headlinecolor_"+id).spectrum({
        preferredFormat: "hex",
        showInput: true,
        color: colorhsv
    });
    $('.sp-dd').html('');
    $('.sp-dd').html('<img src="<?php echo base_url(); ?>assets/images/new_update1.png">');
 }

 $(document).on('click','.add_merge',function() 
 { 
     var name=$(this).html();  
     var text = tinyMCE.get('editor1').getContent()+'<p>'+name+'</p>';         
     tinyMCE.get('editor1').setContent( text );
     //$('#editor1').append();

 }); 

 $(document).on('click','.clear,.eclear',function() 
 {            
     //$('#editor1').html('');      
     tinyMCE.get('editor1').setContent(''); 
 });


  $(document).ready(function()
  {
			
      $("#headline_color1").spectrum({
          preferredFormat: "hex",
          showInput: true,
          color: '#eb34de'
      });

      $("#headline_color1").val('#eb34de');

      $('.sp-dd').html('');
      $('.sp-dd').html('<img src="<?php echo base_url(); ?>assets/images/new_update1.png">');
     
      /*$(document).on('change','#firm_settings',function()
      { 
  				var id=$(this).val(); 

          if(id!='')
          {
  				  get_reminders(id);
          }
			});*/
  });


</script> 