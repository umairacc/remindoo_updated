<?php $this->load->view('includes/header');

 $userid = $this->session->userdata('id');  


if(isset($_GET['action']) && $_GET['action']=="time_card")
{
    $css_time_active = 'in active';
    $css_bank_active = '';
    $bank_active = '';
    $time_active = 'active';
}else{
    $css_bank_active = 'in active';
    $css_time_active = '';
    $bank_active = 'active';
    $time_active = '';
}
function getStartAndEndDate($week, $year) {
  $dateTime = new DateTime();
  $dateTime->setISODate($year, $week);
  $result['start_date'] = $dateTime->format('d-m-Y');
  $dateTime->modify('+6 days');
  $result['end_date'] = $dateTime->format('d-m-Y');
  return $result;
}

function getDatesFromRange($start, $end, $format = 'Y-m-d') {
    $array = array();
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

    foreach($period as $date) { 
        $array[] = $date->format($format); 
    }

    return $array;
}
/*function isWeekend($date) {
    $weekDay = date('w', strtotime($date));
    return ($weekDay == 0 || $weekDay == 6);
}*/
function isWeekend($date) {
    return (date('N', strtotime($date)) == 7);
}
$notstarted = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','notstarted');
        $inprogress = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','inprogress');

        $awaiting = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','awaiting');
        $testing = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','testing');
        $complete = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','complete');
        
        $role = $this->Common_mdl->getRole($_SESSION['id']);
        //print_r($task_list);

        $assignNotstarted = 0;
        $assignInprogress = 0;
        $assignAwaiting = 0;
        $assignTesting = 0;
        $assignComplete = 0;

        foreach ($task_list as $key => $value) {
            $staffs = $value['worker'].','.$value['manager'];

            $explode_worker=explode(',',$staffs);

            if($value['task_status']=='notstarted') {
                if(in_array( $_SESSION['id'] ,$explode_worker )){
                    $assignNotstarted++;
                }
            }
            if($value['task_status']=='inprogress') {
                if(in_array( $_SESSION['id'] ,$explode_worker )){
                $assignInprogress++;
                }
            }
            if($value['task_status']=='awaiting') {
                if(in_array( $_SESSION['id'] ,$explode_worker )){
                $assignAwaiting++;
                }
            }
            if($value['task_status']=='testing') {
                if(in_array( $_SESSION['id'] ,$explode_worker )){
                $assignTesting++;
                }
            }
            if($value['task_status']=='complete') {
                if(in_array( $_SESSION['id'] ,$explode_worker )){
                $assignComplete++;
                }
            }

        }
?>

<link href="<?php echo  base_url()?>assets/css/c3.css" rel="stylesheet" type="text/css">
<link href="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/demo/MonthPicker.min.css" rel="stylesheet" type="text/css">
<link href="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/test/test.css" rel="stylesheet" type="text/css">
<link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<div class="pcoded-inner-content">

<div class="title_page01 add_profile_01 floating">

<div class="profile_setting1 floating">
            <div class="col-sm-4">
              <!-- <div class="profile_task1">
                        <span><?php echo $assignNotstarted;?></span>
                        <strong>Not started Tasks</strong>
                        <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
                </div> -->

                 <div class="profile_task1">
                        <span><?php //echo $assignInprogress;
                        echo count($inprogress);
                        ?></span>
                        <strong>Open Tasks</strong>
                        <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
                </div>

                 <!-- <div class="profile_task1">
                        <span><?php echo $assignAwaiting;?></span>
                        <strong>Awaiting Feedback Tasks</strong>
                        <a href="#">More info &gt; </a>
                </div> -->
<!-- 
                 <div class="profile_task1">
                        <span><?php echo $assignTesting;?></span>
                        <strong>Testing Tasks</strong>
                        <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
                </div> -->

                 <div class="profile_task1">
                        <span><?php //echo $assignComplete;
                          echo count($complete);
                        ?></span>
                        <strong>Complete Tasks</strong>
                        <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
                </div>
            </div> <!-- col-sm-8 -->

            <div class="col-sm-4">
                <div class="profile_display1">
                    <?php  $getUserProfilepic = $this->Common_mdl->getUserProfilepic($userid); ?>
                       <img src="<?php echo $getUserProfilepic;?>">
                        <h3> <?php 
                        $user_role=$this->Common_mdl->select_record('user','id',$userid);
                        $role_name=$this->Common_mdl->select_record('Role','id',$user_role['role']);
                        echo $user_role['crm_name'];?> </h3>
                        <span>EMP ID: <?php 
                        echo '#'.$userid;
                        ?></span>
                        <strong><?php 
                                            echo $role_name['role'];
                                            ?></strong>
               </div>  
               
               </div> <!-- col-sm-4 -->   
    	 <div class="col-sm-4">
                <div class="profile_task1">
                        <span><?php echo $assignNotstarted;?></span>
                        <strong>Monthly Attendance</strong>
                        <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
                </div>

                 <div class="profile_task1">
                        <span><?php echo $assignInprogress;?></span>
                        <strong>Monthly Absent</strong>
                        <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
                </div>

                 <!-- <div class="profile_task1">
                        <span><?php echo $assignAwaiting;?></span>
                        <strong>Awaiting Feedback Tasks</strong>
                        <a href="#">More info &gt; </a>
                </div> -->

                 <div class="profile_task1">
                        <span><?php echo $assignTesting;?></span>
                        <strong>Monthly Leave</strong>
                        <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
                </div>

                 <div class="profile_task1">
                        <span><?php echo $assignComplete;?></span>
                        <strong>Total Award</strong>
                        <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
                </div>
            </div> <!-- col-sm-4 -->
    </div>

 <!-- status board -->

          <div class="status-board">
    <form id="insert_dashboard" class="validation" method="post" action="<?php echo base_url().'User/firm_setting_insert'?>" enctype="multipart/form-data">
          <h1>Status Board</h1>
          <?php if($this->session->flashdata('success')): ?>
    <p><?php echo $this->session->flashdata('success'); ?></p>
<?php endif; ?>
          <label>Company Name</label>
            <div class="form-group search-sec">
              <!-- <i class="fa fa-search" aria-hidden="true"></i> -->
              <input type="text" class="form-control" name="company_name" id="search_company" placeholder="Client Name" value="" required>
              <input type="hidden" value="" name="user_id" id="user_id">
            <div id="searchresult"></div>
            </div>
           <!--  <div class="form-group">
                           <label>Company Type</label>
                           <select name="priority" class="form-control valid" id="priority">
                              <option value="active">Active</option>
                              <option value="inactive">Inactive</option>
                           </select>
            </div> -->
            <div id="service" class="tab-pane fade active in">
  <div class="basic-available-data">
    <div class="basic-available-data1">

               <table class="client-detail-table01 service-table-client">
                     <thead>
                        <tr>
                        <th>Services</th>
                        <th>( Services )Enable/Disable</th>
                        <th>( Reminders )Enable/Disable</th>
                        <th>( Text )Enable/Disable</th>
                         <th>( Invoice Notifications )Enable/Disable</th></tr>
                     </thead>
                     <tbody>
                        <tr id="service_activation_tab">
                              <td>Confirmation Statement</td>
                              <td><input type="checkbox" class="js-small f-right tab-one" name="confirm[tab]" data-id="cons" ></td>
                              <td><input type="checkbox" class="js-small f-right tab-second" name="confirm[reminder]" data-id="enableconf"></td>
                              <td><input type="checkbox" class="js-small f-right" name="confirm[text]" data-id="onet"></td>
                              <td><input type="checkbox" class="js-small f-right" name="confirm[invoice]" data-id="incs"></td>
                           </tr>
                           <tr>
                             
                              <td>Accounts</td>
                              <td><input type="checkbox" class="js-small f-right tab-one" name="accounts[tab]" data-id="accounts"></td>
                              <td><input type="checkbox" class="js-small f-right tab-second" name="accounts[reminder]" data-id="enableacc"></td>
                              <td><input type="checkbox" class="js-small f-right" name="accounts[text]" data-id="twot"></td>
                              <td><input type="checkbox" class="js-small f-right" name="accounts[invoice]" data-id="inas"></td>
                           </tr>

                           <tr>
                              <td>Company Tax Return</td>
                              <td><input type="checkbox" class="js-small f-right" name="companytax[tab]" data-id="companytax"></td>
                              <td><input type="checkbox" class="js-small f-right" name="companytax[reminder]" data-id="enabletax"></td>
                              <td><input type="checkbox" class="js-small f-right" name="companytax[text]" data-id="threet"></td>
                              <td><input type="checkbox" class="js-small f-right" name="companytax[invoice]" data-id="inct"></td>
                           </tr>
                           <tr>
                              <td>Personal Tax Return</td>
                              <td><input type="checkbox" class="js-small f-right" name="personaltax[tab]" data-id="personaltax"></td>
                              <td><input type="checkbox" class="js-small f-right" name="personaltax[reminder]" data-id="enablepertax"></td>
                              <td><input type="checkbox" class="js-small f-right" name="personaltax[text]" data-id="fourt"></td>
                              <td><input type="checkbox" class="js-small f-right" name="personaltax[invoice]" data-id="inpt"></td>
                           </tr>

                           <tr>
                              <td>Payroll</td>
                              <td><input type="checkbox" class="js-small f-right" name="payroll[tab]" data-id="payroll"></td>
                              <td><input type="checkbox" class="js-small f-right" name="payroll[reminder]" data-id="enablepay"></td>
                              <td><input type="checkbox" class="js-small f-right" name="payroll[text]" data-id="fivet"></td>
                              <td><input type="checkbox" class="js-small f-right" name="payroll[invoice]" data-id="inpr"></td>
                           </tr>
                           <tr>
                            
                              <td>WorkPlace Pension - AE</td>
                              <td><input type="checkbox" class="js-small f-right" name="workplace[tab]" data-id="workplace"></td>
                              <td><input type="checkbox" class="js-small f-right" name="workplace[reminder]" data-id="enablework"></td>
                              <td><input type="checkbox" class="js-small f-right" name="workplace[text]" data-id="sixt"></td>
                              <td><input type="checkbox" class="js-small f-right" name="workplace[invoice]" data-id="inpn"></td>
                           </tr>

                           <tr>
                           
                              <td>VAT Returns</td>
                              <td><input type="checkbox" class="js-small f-right" name="vat[tab]" data-id="vat"></td>
                              <td><input type="checkbox" class="js-small f-right" name="vat[reminder]" data-id="enablevat"></td>
                              <td><input type="checkbox" class="js-small f-right" name="vat[text]" data-id="sevent"></td>
                              <td><input type="checkbox" class="js-small f-right" name="vat[invoice]" data-id="invt"></td>
                           </tr>
                           <tr>
                              <td>CIS - Contractor</td>
                              <td><input type="checkbox" class="js-small f-right" name="cis[tab]" data-id="cis"></td>
                              <td><input type="checkbox" class="js-small f-right" name="cis[reminder]"  data-id="enablecis"></td>
                              <td><input type="checkbox" class="js-small f-right" name="cis[text]" data-id="eightt"></td>
                              <td><input type="checkbox" class="js-small f-right" name="cis[invoice]" data-id="incis"></td>
                           </tr>

                           <tr>
                              
                              <td>CIS - Sub Contractor</td>
                              <td><input type="checkbox" class="js-small f-right" name="cissub[tab]" data-id="cissub"></td>
                              <td><input type="checkbox" class="js-small f-right" name="cissub[reminder]"  data-id="enablecissub"></td>
                              <td><input type="checkbox" class="js-small f-right" name="cissub[text]" data-id="ninet"></td>
                              <td><input type="checkbox" class="js-small f-right" name="cissub[invoice]" data-id="incis_sub"></td>
                           </tr>
                           <tr>
                              <td>P11D</td>
                              <td><input type="checkbox" class="js-small f-right" name="p11d[tab]" data-id="p11d" ></td>
                              <td><input type="checkbox" class="js-small f-right" name="p11d[reminder]" data-id="enableplld"></td>
                              <td><input type="checkbox" class="js-small f-right" name="p11d[text]" data-id="tent"></td>
                              <td><input type="checkbox" class="js-small f-right" name="p11d[invoice]" data-id="inp11d"></td>
                           </tr>

                           <tr>
                     
                              <td>Bookkeeping</td>
                              <td><input type="checkbox" class="js-small f-right" name="bookkeep[tab]" data-id="bookkeep"></td>
                              <td><input type="checkbox" class="js-small f-right" name="bookkeep[reminder]" data-id="enablebook"></td></td>
                              <td><input type="checkbox" class="js-small f-right" name="bookkeep[text]" data-id="elevent"></td>
                              <td><input type="checkbox" class="js-small f-right" name="bookkeep[invoice]" data-id="inbook"></td>
                           </tr>
                           <tr>
                             
                              <td>Management Accounts</td>
                              <td><input type="checkbox" class="js-small f-right" name="management[tab]" data-id="management"></td>
                              <td><input type="checkbox" class="js-small f-right" name="management[reminder]" data-id="enablemanagement"></td>
                              <td><input type="checkbox" class="js-small f-right" name="management[text]" data-id="twelvet"></td>
                              <td><input type="checkbox" class="js-small f-right" name="management[invoice]" data-id="inma"></td>
                           </tr>

                        <!-- ********** -->

                        <tr>
                       
                              <td>Investigation Insurance</td>
                              <td><input type="checkbox" class="js-small f-right" name="investgate[tab]" data-id="investgate"></td>
                              <td><input type="checkbox" class="js-small f-right" name="investgate[reminder]" data-id="enableinvest"></td>
                              <td><input type="checkbox" class="js-small f-right" name="investgate[text]" data-id="thirteent"></td>
                              <td><input type="checkbox" class="js-small f-right" name="investgate[invoice]"></td>
                           </tr>
                           <tr>
                  
                              <td>Registered Address</td>
                              <td><input type="checkbox" class="js-small f-right" name="registered[tab]" data-id="registered"></td>
                              <td><input type="checkbox" class="js-small f-right" name="registered[reminder]" data-id="enablereg"></td>
                              <td><input type="checkbox" class="js-small f-right" name="registered[text]" data-id="fourteent"></td>
                              <td><input type="checkbox" class="js-small f-right" name="registered[invoice]"></td>
                           </tr>

                           <tr>
                             
                              <td>Tax Advice</td>
                              <td><input type="checkbox" class="js-small f-right" name="taxadvice[tab]" data-id="taxadvice"></td>
                              <td><input type="checkbox" class="js-small f-right" name="taxadvice[reminder]" data-id="enabletaxad"></td>
                              <td><input type="checkbox" class="js-small f-right" name="taxadvice[text]" data-id="fifteent"></td>
                              <td><input type="checkbox" class="js-small f-right" name="taxadvice[invoice]"></td>
                           </tr>
                           <tr>
                            
                              <td>Tax Investigation</td>
                              <td><input type="checkbox" class="js-small f-right" name="taxinvest[tab]" data-id="taxinvest"></td>
                              <td><input type="checkbox" class="js-small f-right" name="taxinvest[reminder]" data-id="enabletaxinves"></td>
                              <td><input type="checkbox" class="js-small f-right" name="taxinvest[text]" data-id="sixteent"></td>
                              <td><input type="checkbox" class="js-small f-right" name="taxinvest[invoice]"></td>
                           </tr>
                     </tbody>
                  </table>
                </div>
              </div>


         </div> <!-- service -->
<!-- service tab close -->
          

            <div class="floation_set text-right accordion_ups">
            <input type="submit" class="add_acc_client" value="Save Setting" id="submit"/>
            </div>
        </form>
          </div>

          <!-- status board -->


          <!-- status board -->

          <div class="status-board">
          <h1>Status Board</h1>
          <label>Company Name</label>
            <div class="form-group search-sec">
              <i class="fa fa-search" aria-hidden="true"></i>
              <input type="text" class="form-control search_company1" name="company_name" id="search_company1" placeholder="Client Name" value="" required>
              <input type="hidden" value="" name="user_id" id="user_id1">
            <div id="searchresult1"></div>
            </div>
            <div class="form-group">
                           <label>Company Type</label>
                           <select name="type" class="form-control valid search_company1" id="priority">
                              <option value="active">Active</option>
                              <option value="inactive">Inactive</option>
                           </select>
            </div> 
            <label>Service Types</label>
            <div class="options-div">

              <div class="options-inner">
                <input type="checkbox" class="js-small f-right tab-one" name="confirm[tab]" data-id="cons" >
                <span class="label-name">confirmation</span>
              </div>
              <div class="options-inner">
                <input type="checkbox" class="js-small f-right tab-one" name="accounts[tab]" data-id="accounts">
                <span class="label-name">Accounts</span>
              </div>
              <div class="options-inner">
               <input type="checkbox" class="js-small f-right" name="companytax[tab]" data-id="companytax">
                <span class="label-name">Company Tax Return</span>
              </div>
              <div class="options-inner">
                <input type="checkbox" class="js-small f-right" name="personaltax[tab]" data-id="personaltax">
                <span class="label-name">Personal Tax Return</span>
              </div>
              <div class="options-inner">
                <input type="checkbox" class="js-small f-right" name="payroll[tab]" data-id="payroll">
                <span class="label-name">Payroll</span>
              </div>
              <div class="options-inner">
                <input type="checkbox" class="js-small f-right" name="vat[tab]" data-id="vat">
                <span class="label-name">VAT</span>
              </div>
            </div>

            <div class="tax-details">
            
            <table class="table client_table1 text-center dataTable no-footer" id="" role="grid" aria-describedby="newinactives_info">
               <thead>
                  <tr class="text-uppercase" role="row">
                     <th class="sorting_asc" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Profile: activate to sort column descending" style="width: 0px;">client name</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 0px;">client type</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending" style="width: 0px;">confirmation</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 0px;">Accounts</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="User Type: activate to sort column ascending" style="width: 0px;">Company Tax Return</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 0px;">Personal Tax Return</th>
                      <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 0px;">Payroll</th>
                       <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 0px;">VAT</th>
                  </tr>
               </thead>
               <tbody id="searchresult">
                  <tr role="row" class="odd">
                     <td class="user_imgs sorting_1">Accotac London Limited</td>
                     <td>Limited</td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                  </tr>
               </tbody>
            </table>
            </div>
          </div>

          <!-- status board -->
          

<?php 
//echo $user_role['role'].'priyarole';die;

if($user_role['role']=='6'){?>
    <div class="overall_hours1 top-margin01 floating">   
        <!-- <div class="project_hour01">
            <span>4:15:29</span>
            <h4>Projects Hours</h4>
        </div> -->
        <div class="project_hour01">
            <span>8:23:29</span>
            <h4>Tasks Hours</h4>
        </div>
        <div class="project_hour01">
            <span>0:0m</span>
            <h4>This month working Hours</h4>
        </div>
        <div class="project_hour01">
            <span>22:20m</span>
            <h4>Working Hours</h4>
        </div>    
    </div>

</div> <!-- title_page01 -->	

    <div class="additional-tab floating_set">
    <div class="deadline-crm1 floating_set">
					<ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                      <li class="nav-item all-client-icon1">
						<span><i class="fa fa-user fa-6" aria-hidden="true"></i></span>
					 </li>	

					  <li class="nav-item">
                        <a class="nav-link <?php echo $bank_active;?>" data-toggle="tab" href="#bank_details">Bank Details</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link <?php echo $time_active;?>" data-toggle="tab" href="#time_card">Time Card - Login</a>
                        <div class="slide"></div>
                      </li>
                        <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#task_summary">Task summary</a>
                        <div class="slide"></div>
                      </li>
                        <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#all_activity">All activity log</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url();?>login/logout">Logout</a>
                        <div class="slide"></div>
                      </li>
                    </ul>
        </div>  

        <div class="all_user-section2 floating_set">
                      <div class="tab-content">
                        <div id="bank_details" class="tab-pane fade <?php echo $css_bank_active;?>">
                        	<div class="new_bank1">	
                        		<h2>New Bank</h2>
                                <?php 
                                $succ = $this->session->flashdata('success');

                                 if($succ){?>
                     <div class="alert alert-success"
                        ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $succ; ?></div><?php } ?>
                        		<div class="bank-form">	
                        			<form action="<?php echo base_url();?>staff/update_bank" name="update_bank" id="update_bank" method="post">

                        				<div class="form-group">
                        					<label>Bank Name</label>
                        					<input type="name" name="bank_name" id="bank_name" value="<?php echo $user_details['bank_name'];?>">
                        				</div>
                        				<div class="form-group">
                        					<label>Branch Name</label>
                        					<input type="name" name="branch_name" id="branch_name" value="<?php echo $user_details['branch_name'];?>">
                        				</div>
                        				<div class="form-group">
                        					<label>Account Name</label>
                        					<input type="name" name="account_name" id="account_name" value="<?php echo $user_details['account_name'];?>">
                        				</div>
                        				<div class="form-group">
                        					<label>A/C number</label>
                        					<input type="name" name="account_no" id="account_no" value="<?php echo $user_details['account_no'];?>">
                        				</div>
                        				<div class="text-right new-bank-update">
                                            <input type="button" name="cancel" id="cancel" value="cancel">
                                            <input type="submit" name="update" id="update" value="update">
                                            <!-- <button type="button" name="cancel">Update</button>
                        					<button type="button" name="cancel">Cancel</button> -->
                        				</div>	

                        			</form>
                        		</div>	
                        	</div>	
                        </div>	<!-- tab1 -->

                        <div id="time_card" class="tab-pane fade <?php echo $css_time_active;?>">
                        	<div class="new_bank1">	
                        		<h2>Timecard Details</h2>
                        			<div class="working-hrs bank-form">
                        				<div class="form-group date_birth">
                        					<form name="time_cards" id="time_cards" method="post">
                                             <label class="col-form-label">Month *</label>
                                             <div class="working-picker">
                                                <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                <input class="form-controlfields" type="text" name="month_pic" id="month_pic" value="<?php echo date('m').'/'.date('Y');?>">
                                             </div>
                                             <div class="go-bts">
                                             		<button type="button" name="go" id="go">Go</button>
                                                   <!--  <input type="submit" name="go" id="go" value="Go"> -->
                                             </div>		
                                         </form>
                                          </div>
                                    </div>

                                <div class="work-month" id="timecard_filter">    
                                <h3>Works Hours Details of <?php echo date('M').' - '.date('Y');?></h3>
								<div class="table-responsive">
									<table class="table">
											 <tr>
                                                <?php for ($i=$start_month; $i <=$end_month; $i++) { 
                                                      $year = date('Y');
                                                      $m = date('m');
                                                      $dates = getStartAndEndDate($i,$year);
                                                      $alldates = getDatesFromRange($dates['start_date'],$dates['end_date']);
                                                   ?>
                                                  <td class="filed-reject">
                                                     <h4>Week:<?php echo ltrim($i,'0');?></h4>
                                                    <?php 
                                                    $w = 0;
                                                    foreach ($alldates as $alldates_key => $alldates_value) {

                                                        $weekend = isWeekend($alldates_value);
                                                        //print_r($weekend);
                                                        $exp_d = explode('-', $alldates_value);
                                                        $d = $exp_d[1];

                                                        if( $d == $m )
                                                        {
                                                            if($weekend==''){
                                                                $w++;
                                                        ?>
                                                    <p><b><?php echo $alldates_value;?></b> <span>8:00 m</span></p><?php } else {?><p><b><?php echo $alldates_value;?></b> <span><a href="#">Holiday</a></span></p> <?php } ?>
                                                    <!-- <p><b>02-10-17</b> <span><a href="#">Holiday</a></span></p>
                                                    <p><b>02-10-17</b> <span>0:0 m</span></p>
                                                    <p><b>02-10-17</b> <span>0:0 m</span></p>
                                                    <p><b>02-10-17 </b><span><a href="#">Holiday</a></span></p>
                                                    <p><b>02-10-17</b> <span>0:0 m</span></p>
                                                    <p class="removed-02"><b>02-10-17</b> <span><a href="#">Holiday</a></span></p> 
                                                   
                                                     -->
                                                     <?php   } 
                                                 }
                                                    ?>
                                                     <span class="total-working-hr"><strong>Total Working Hour: </strong> <?php echo $w * 8 ; ?>:0 m</span>
                                               </td>
                                                <?php }?>
                                                 
                                                </tr>

										</table>
								</div>
							</div>					
						</div>	
					</div> <!-- tab2 -->	

					<div id="task_summary" class="tab-pane fade ">
						<div class="new_bank1">	
                        		<h2>Task summary</h2>	
                        			<div class="task-filter1">
                        				<div class="time-spent">
                        					<h4>Total Task Time Spent</h4>
                        						<!-- <div id="defaultCountdown"></div> -->
                        							<div class="timer-section01">
                        								<span>0 <strong>Hours</strong></span> :
                        								<span>00 <strong>Minutes</strong></span> :
                        								<span>00<strong>Seconds</strong></span> 
                        							</div>	
                        				</div>

                        				<div class="time-spent">
                        					<h4>Task Reports</h4>
                        				
											
											<div class="chart-container-01"> <div class="open-closed1">
												<!--
											<span> <i class="fa fa-square fa-6" aria-hidden="true"></i>Critical</span>
											<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> High</span>

											<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Medium</span>
											<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Low</span>

											<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Resolved</span>
											<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> closed</span> -->

                                          <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Low</span>
                                            <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> High</span>
                                            <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Medium</span>
                                            <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Super Urgent</span> 

											</div>

                        					<div id="chartContainer" style="width: 100%; height: 420px"></div> 
                        				</div>
                        			</div>
                        					
                        			</div>			
                        		</div>
                        </div>		<!-- tab3 -->	

                        <div id="all_activity" class="tab-pane fade">
                        	<div class="new_bank1">	
                        		<h2>All activity log</h2>	
                        	<div class="all_user-section2">
                        		<table class="table client_table1 text-center display nowrap" id="all_activity1" cellspacing="0" width="100%">
                        			<thead>
                        				<tr>
                        						<th>Activity Date</th>
                        						<th>User</th>
                        						<th>Module</th>
                        						<th>Activity</th>
                        				</tr>
                        			</thead>

                        			<tbody>
                                        <?php 
                                        //print_r($user_activity);die;
                                         foreach ($user_activity as $activity_key => $activity_value) { 

                                            $rec=$this->Common_mdl->select_record('user','id',$activity_value['user_id']);
                                            $role=$this->Common_mdl->select_record('Role','id',$rec['role']);

                                            ?>
                        				<tr>
                        					<td><?php echo date("d-m-Y h:i:s A ",$activity_value['CreatedTime']); ?></td>
                        					<td><?php echo $role['role'];?></td>
                                            <td><?php echo $activity_value['module'];?></td>
                        					<td><?php echo $activity_value['log'];?></td>
                        				</tr>
                        				<?php } ?>
                        				
                        			</tbody>
                        		</table>
                        	</div>
                        </div>	<!-- tab4 -->					




                                          


                    
                    </div>  <!-- tab-content --> 
                     
    </div>    


</div> <!-- additional-tab -->

</div>
<?php } else{ ?>

<?php } ?>

<?php $this->load->view('includes/footer');?>

<script src="<?php echo  base_url()?>assets/js/jquery.lineProgressbar.js"></script>
<script src="<?php echo  base_url()?>assets/js/c3-custom-chart.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script> 


<script src="https://rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/demo/MonthPicker.min.js"></script>


<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">

        $(document).ready(function(){
    $('#all_activity1').DataTable();
    

});
        
    $(document).ready(function(){
      //  alert('hi');
    $('#month_pic').MonthPicker({ Button: false });

    
});

    


</script>

<!-- <script src="<?php echo base_url();?>assets/js/jquery.plugin.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.countdown.js"></script>
<script>
$(function () {
	var austDay = new Date();
	austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 26);
	$('#defaultCountdown').countdown({until: austDay});
	$('#year').text(austDay.getFullYear());
});
</script> -->


<script type="text/javascript"> 
window.onload = function() { 
	$("#chartContainer").CanvasJSChart({ 
		title: { 
			fontSize: 24
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		data: [ 
		{ 
			type: "pie", 
			toolTipContent: "{label} <br/> {y} %", 
			indexLabel: "{y} %", 
			dataPoints: [ 
				{ label: "Low",  y: <?php echo $low;?>, legendText: "Low"}, 
				{ label: "High",    y: <?php echo $high;?>, legendText: "High"  }, 
				{ label: "Medium",   y: <?php echo $medium;?>,  legendText: "Medium" }, 
				{ label: "Super urgent",       y: <?php echo $super_urgent;?>,  legendText: "Super urgent"}
			] 
		} 
		] 
	}); 
	
	$("#chartContainer1").CanvasJSChart({ 
		title: { 
			fontSize: 24
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		data: [ 
		{ 
			type: "pie", 
			toolTipContent: "{label} <br/> {y} %", 
			indexLabel: "{y} %", 
			dataPoints: [ 
				{ label: "Open",  y: 30.3, legendText: "Open"}, 
				{ label: "Closed",    y: 19.1, legendText: "Closed"  }, 
				
			] 
		} 
		] 
	}); 







} 
</script> 

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script type="text/javascript">
   $(document).ready(function(){

      $( "#update_bank" ).validate({

        rules: {
          bank_name: "required",  
          branch_name: "required",  
          account_name: "required",  
          account_no: "required",  
        },
        messages: {
          bank_name: "Please enter your Bank Name",
          branch_name: "Please enter your Branch Name",
          account_name: "Please enter your Account Name",
          account_no: "Please enter your Account Number",
        },
        
      });

      $("#go").click(function(){
        var month = $("#month_pic").val();

         $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>staff/time_card/",
            data: {month:month},
            success: function(response) {
            $("#timecard_filter").html(response);
            },
          });


      });
   });


</script>
<script>  
   $(document).ready(function(){  
        $('#search_company').keyup(function(){  
         // alert('text');
             var query = $(this).val();  
             if(query != '')  
             {  
                  $.ajax({  
                       url:"<?php echo base_url();?>user/search_companies/",  
                       method:"POST",  
                       data:{query:query},  
                       success:function(data)  
                       {  
                            $('#searchresult').fadeIn();  
                            $('#searchresult').html(data);  
                       }
                  });  
             }
             if(query=='')
             {
               $('#searchresult').fadeOut();
             }  
        });  
        $(document).on('click', 'li', function(){  
             $('#search_company').val($(this).text()); 
             $('#user_id').val($(this).data('id')); 
             $('#searchresult').fadeOut();  
        });  
   }); 

$(document).ready(function(){  
        $('#search_company1').keyup(function(){  
         // alert('text');
             var query = $(this).val();  
             if(query != '')  
             {  
                  $.ajax({  
                       url:"<?php echo base_url();?>user/search_companies/",  
                       method:"POST",  
                       data:{query:query},  
                       success:function(data)  
                       {  
                            $('#searchresult1').fadeIn();  
                            $('#searchresult1').html(data);  
                       }
                  });  
             }
             if(query=='')
             {
               $('#searchresult1').fadeOut();
             }  
        });  
        $(document).on('click', 'li', function(){  
             $('#search_company1').val($(this).text()); 
             $('#user_id1').val($(this).data('id')); 
             $('#searchresult1').fadeOut();  
        });  
   }); 


$("#related_to").change(function () {
        var val = this.value;

        if(val=='projects'){

             $.ajax( {

        type:"post",
        dataType : "json",
        url:"<?php echo base_url();?>user/get_projects",
        processData: false,
        contentType: false,
         data:{ 
          'get':'get'
        }
        ,
        success:function(response)
        {


        var ret='<div id="frame_projects"><label>Projects</label><select name="r_project" class="form-control" id="r_projects" placeholder="select">';

            $.each(response, function (index, value){
            ret+='<option value='+value.id+'>'+value.project_name+'</option>';
            });

          ret+='</select><div>';

         $("#base_projects").before(ret);
        }
    });


        }else{
        
        $('#frame_projects').remove();
        }
        
    });


   $(".search_company1").keyup(function(){
       var currentRequest = null;    
       var name = $(this).val();
       var type=$('#priority').val();
       alert(name);
       alert(type);
       return false;
       //var term = $(this).val().replace(/ +?/g, '');
       //var term = $(this).val($(this).val().replace(/ +?/g, ''));
       $("#searchresult").show();
       $('#selectcompany').hide();
        $(".LoadingImage").show();
       currentRequest = $.ajax({
          url: '<?php echo base_url();?>user/Searchservices/',
          type: 'post',
          data: { 'term':term },
          beforeSend : function()    {           
          if(currentRequest != null) {
              currentRequest.abort();
          }
      },
          success: function( data ){
              $("#searchresult").html(data);
               $(".LoadingImage").hide();
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });
     });

 
</script> 