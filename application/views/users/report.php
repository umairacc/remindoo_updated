<?php $this->load->view('includes/header');?>
<style>
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   } 
   .dropdown-content {
   display: none;
   position: absolute;
   background-color: #fff;
   min-width: 86px;
   overflow: auto;
   box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   z-index: 1;
   left: -92px;
   width: 150px;
   }
   .dropdown {
   position: relative;
   display: inline-block;
   }
   .dropdown-content a {
   color: black;
   padding: 9px 22px;
   text-decoration: none;
   display: block;
   }
   .show {display:block;}
   .hide{
   display: none;
   }
</style>
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set time-sheet">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set">
                              <ul class="md-tabs tabs112 all_user1 floating_set" id="proposal_dashboard">
                                <li class="nav-item">
                                   <a class="nav-link" href="javascript:;">Tasks
                                   </a>
                                </li>
                              </ul>
                                 
                                 <div class="count-value1 pull-right ">                         
                                    <button id="deleteTriger" class="deleteTri" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete</button>
                                 </div>
                              </div>
                              <div class="all_user-section2 floating_set">
                                 <div class="tab-content">
                                    <div id="allusers" class="tab-pane fade in active">
                                       <div id="task"></div>
                                       <div class="client_section3 table-responsive ">
                                          <div class="status_succ"></div>
                                          <div class="all-usera1 data-padding1 ">
                                             <div class="">
                                                <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%">
                                                   <thead>
                                                      <tr class="text-uppercase">
                                                        <!--  <th>
                                                            <div class="checkbox-color checkbox-primary">
                                                               <input type="checkbox"  id="bulkDelete"  /><label for="bulkDelete"></label>
                                                            </div>
                                                         </th> -->
                                                         <th>Task Name</th>
                                                         <th>Start Date</th>
                                                         <th>End Date</th>
                                                         <th>Reports to</th>
                                                         <th>Visible To</th>
                                                        
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      <tr id="2346" role="row" class="odd">
                                                           <?php foreach ($task_list as $key => $value) { ?>

                                                         <td> <?php echo ucfirst($value['subject']);?></a></td>
                                                         <td>                                      
                                                           <?php echo $value['start_date'];?>
                                                         </td>
                                                         <td>
                                                           <?php echo $value['end_date'];?>
                                                         </td>
                                                         <td id="innac"><select name="company_name" id="company_name" > <?php 
                                                $query1=$this->db->query("SELECT * FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$_SESSION['id']." order by id DESC");
                                $results1 = $query1->result_array();
                                $res=array();
                                foreach ($results1 as $key => $values) {
                                     array_push($res, $values['id']);
                                  }
                                  if(!empty($res)){
                                  $im_val=implode(',',$res);
                                 // echo $im_val;
                              $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (".$im_val.") order by id desc ");    
                               $results = $query->result_array();  
                               }
                               else{
                                $results=0;
                               
                                    $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id=".$_SESSION['id']." order by id desc ");    
                                     $results = $query->result_array();  
                               }  
                                                if(count($results) > 0)  
                                                {  
                                                   foreach($results as $key => $val)
                                                   {  ?>
                                             <option value="<?php echo $val['id'];?>"  <?php if(isset($value['company_name'])){     
                                                if($value['company_name']==$val['id']){ ?>
                                                selected="selected"
                                                <?php } } ?>
                                                ><?php echo $val["crm_company_name"];?></option>
                                             <?php
                                                }  
                                                }  ?>
                                          </select></td>
                                                         <td> <?php  //print_r($task_form[0]['worker']);
                                 if(isset($value['worker'])){
                                   echo $explode_worker=explode(',',$value['worker']);
                                 }
                                 if(isset($value['team'])){
                                  echo $explode_team=explode(',',$value['team']);
                                 }
                                 if(isset($value['department'])){
                                  echo $explode_department=explode(',',$value['department']);
                                 }
                                 ?></td>
                                                       <!--   <td>Manual</td> -->
                                                      </tr>
                                                  <?php } ?>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- home-->
                                    <!-- frozen -->
                                 </div>
                              </div>
                              <!-- admin close -->
                           </div>
                           <!-- Register your self card end -->
                        </div>
                     </div>
                  </div>
                  <!-- Page body end -->
               </div>
            </div>
            <!-- Main-body end -->
            <div id="styleSelector">
            </div>
<div class="col-xs-12 time_stats">
<h3>time stats</h3>
<div class="col-md-12">
<div class="col-md-2">
<button type="button" id="last_7" class="btn btn-default change">last 7 days</button>
</div>
<div class="col-md-2">
<button type="button" id="last_30" class="btn btn-default change">last 30 days</button>
</div>
<div class="col-md-2">
<button type="button" id="last_90" class="btn btn-default change">last 90 days</button>
</div>
<div class="col-md-2">
 <div class="btn-group e-dash sve-action1">
                   <button type="button" class="btn btn-default"><a href="javascript:;" id="sent" class="change" data-toggle="modal" data-target="#Send_proposal">view by Month</a></button>
                   <button type="button" class="btn btn-default dropdown-toggle emp-dash" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   <span class="caret"></span>
                   <span class="sr-only">Toggle Dropdown</span>
                   </button>
                   <ul class="dropdown-menu">                                           
                     <li><a href="javascript:;" id="jan" class="change" >Jan</a></li>
                     <li><a href="javascript:;" id="feb" class="change" >Feb</a></li>
                     <li><a href="javascript:;" id="mar" class="change">Mar</a></li>
                     <li><a href="javascript:;" id="apr" class="change" >Apr</a></li>  
                     <li><a href="javascript:;" id="mat" class="change" >May</a></li>
                     <li><a href="javascript:;" id="june" class="change" >June</a></li>
                     <li><a href="javascript:;" id="july" class="change">July</a></li>
                     <li><a href="javascript:;" id="sep" class="change" >Sep</a></li>  
                     <li><a href="javascript:;" id="oct" class="change">Oct</a></li>
                     <li><a href="javascript:;" id="nov" class="change">Nov</a></li>
                     <li><a href="javascript:;" id="dec" class="change">Dec</a></li>                               
                   </ul>
                 </div>
</div>
</div>
<div class="col-xs-12 line_charts">

 <div id="linechart_material" style="width: 100%; height: 500px"></div>
 </div>
 </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<!-- email all -->
<input type="hidden" name="user_id" id="user_id" value="">
<!-- filter add -->
<div clas="filter-bar" style="display:none;">
   <nav class="navbar navbar-light" >
      <ul class="nav navbar-nav">
         <li class="nav-item active">
            <a class="nav-link" href="#!">Filter: <span class="sr-only">(current)</span></a>
         </li>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#!" id="bydate1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icofont icofont-clock-time"></i> By Date</a>
            <input type="text" id="datepicker" name="de" placeholder="By Date">
         </li>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" name="stati" href="#!" id="statuswise_filter1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icofont icofont-chart-histogram-alt"></i> By Service</a>
            <div class="dropdown-menu" aria-labelledby="statuswise_filter1">
               <a class="dropdown-item" href="#!">All</a>
               <div class="dropdown-divider"></div>
               <a class="dropdown-item" href="#!">Active</a>
               <a class="dropdown-item" href="#!">In-Active</a>
               <a class="dropdown-item" href="#!">Fozen</a>
            </div>
         </li>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#!" id="priority"1 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icofont icofont-sub-listing"></i> By Clients</a>
            <div class="dropdown-menu" aria-labelledby="priority1">
               <a class="dropdown-item" href="#!">All</a>
               <div class="dropdown-divider"></div>
               <a class="dropdown-item" href="#!">Private Limited company</a>
               <a class="dropdown-item" href="#!">Public Limited company</a>
               <a class="dropdown-item" href="#!">Limited Liability Partnership</a>
               <a class="dropdown-item" href="#!">Partnership</a>
               <a class="dropdown-item" href="#!">Self Assessment</a>
               <a class="dropdown-item" href="#!">Trust</a>
               <a class="dropdown-item" href="#!">Charity</a>
               <a class="dropdown-item" href="#!">Charity</a>
            </div>
         </li>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#!" id="priority"1 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icofont icofont-sub-listing"></i> By User</a>
            <div class="dropdown-menu" aria-labelledby="priority1">
               <a class="dropdown-item" href="#!">All</a>
               <div class="dropdown-divider"></div>
               <a class="dropdown-item" href="#!">Private Limited company</a>
               <a class="dropdown-item" href="#!">Public Limited company</a>
               <a class="dropdown-item" href="#!">Limited Liability Partnership</a>
               <a class="dropdown-item" href="#!">Partnership</a>
               <a class="dropdown-item" href="#!">Self Assessment</a>
               <a class="dropdown-item" href="#!">Trust</a>
               <a class="dropdown-item" href="#!">Charity</a>
               <a class="dropdown-item" href="#!">Charity</a>
            </div>
         </li>
         <li class="nav-item">
            <div class="dropdown-primary dropdown open">
               <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button>
               <div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url();?>User/user_exportCSV?stati=0&de=0&pe=0" class="dropdown-item waves-light waves-effect but">Csv</a><a href="<?php echo base_url();?>User/pdf?stati=0&de=0&pe=0" class="dropdown-item waves-light waves-effect but">Pdf</a><a href="<?php echo base_url();?>User/html?stati=0&de=0&pe=0" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>
            </div>
         </li>
      </ul>
   </nav>
</div>


<!-- filter end-->
<?php // $this->load->view('users/column_setting_view');?>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.tagsinput.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="
https://cdn.datatables.net/select/1.2.6/js/dataTables.select.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/js/jquery.tagsinput.js"></script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
google.charts.load('current', {'packages':['line']});
      google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('number', 'Day');
      data.addColumn('number', 'Guardians of the Galaxy');
      data.addColumn('number', 'The Avengers');
      data.addColumn('number', 'Transformers: Age of Extinction');

      data.addRows([
        [1,  37.8, 80.8, 41.8],
        [2,  30.9, 69.5, 32.4],
        [3,  25.4,   57, 25.7],
        [4,  11.7, 18.8, 10.5],
        [5,  11.9, 17.6, 10.4],
        [6,   8.8, 13.6,  7.7],
        [7,   7.6, 12.3,  9.6],
        [8,  12.3, 29.2, 10.6],
        [9,  16.9, 42.9, 14.8],
        [10, 12.8, 30.9, 11.6],
        [11,  5.3,  7.9,  4.7],
        [12,  6.6,  8.4,  5.2],
        [13,  4.8,  6.3,  3.6],
        [14,  4.2,  6.2,  3.4]
      ]);

      var options = {
       
        width: 1000,
        height: 500
      };

      var chart = new google.charts.Line(document.getElementById('linechart_material'));

      chart.draw(data, google.charts.Line.convertOptions(options));
    }
     </script> 
<script type="text/javascript">



   CKEDITOR.replace('editor4');
</script>
<script>
   $(document).ready(function() {
var editor;


   
   
   var dataTable =  $("#alluser").DataTable({
       "iDisplayLength": 10,
    // "scrollX": true,
    "dom": '<"toolbar-table">lfrtip',     
        select: true,
        
     });
   
   $("div.toolbar-table").html('<div class="filter-task1"><li><select id="statuswise_filter" name="stati" class="form-control"><option value="0">All Client</option><option value="0">All</option><option value="1">Active</option><option value="2">In-Active</option><option value="3">Fozen</option></select></li>\
    <li> <div class="dropdown-sin-4"><select id="service" class="form-control"> <option value="0">All Service</option><?php 
  $query1=$this->db->query("SELECT * FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$_SESSION['id']." order by id DESC");
  $results1 = $query1->result_array();
  $res=array();
  foreach ($results1 as $key => $value) {
  array_push($res, $value['id']);
  }
  if(!empty($res)){
  $im_val=implode(',',$res);
  // echo $im_val;
  $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (".$im_val.") order by id desc ");    
  $results = $query->result_array();  
  }
  else{
  $results=0;

  $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id=".$_SESSION['id']." order by id desc ");    
  $results = $query->result_array();  
  }  
  if(count($results) > 0)  
  {  
  foreach($results as $key => $val)
  {  ?><option value="<?php echo $val['id'];?>"  <?php if(isset($records['company_id'])){     
  if($records['company_id']==$val['id']){ ?> selected="selected" <?php } } ?>  ><?php echo $val["crm_company_name"];?></option><?php
  }  
  }  ?> </div></li>\
  <li></li></ul></div>'); 

   // <select id="priority" class="form-control"><option value="0">All User</option> <option value="0">All</option> <?php //foreach ($roles as $roles_key => $roles_value) { ?><option value="<?php //echo $roles_value['id'];?>"><?php //echo $roles_value['role']; ?></option><?php //} ?></select> 
   
     $('.search-input-text').tagsInput({   // initialization of tags input 
           'height':'100%',
           'width':'100%',
           'interactive':true,
           'defaultText':'Add a tag',
           'hide':true,
           'delimiter':',',
           'unique':true,
           'onAddTag':tagDraw,
           'onRemoveTag':tagDraw,
           'removeWithBackspace' : true,
           'minChars' : 0,
           'maxChars' : 0, //if not provided there is no limit,
           'placeholderColor' : '#AAA'
         });
         function tagDraw(){              //draw a request on add or remove tag
           var v= $(".search-input-text").val();
           dataTable.column(4).search(v).draw();
         }
   
   
   
  
   
   
   
   });
   
</script>

<script type="text/javascript">
   $('.dropdown-sin-4').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
    });
</script>
<script type="text/javascript">
   $(document).ready(function(){
     
   
     $( function() {
    $( "#datepicker" ).datepicker();
     $( "#datepicker1" ).datepicker();
   } );
   
   
   $("#sel").html('<form action="<?php echo base_url();?>User/user_exportCSV" method="POST"><div class="filter-task1"><h2>Filter:</h2><ul><li><img src="http://remindoo.org/CRMTool/assets/images/by-date1.png" alt="data"><input type="text" id="datepicker" name="de" placeholder="By Date"></li><li><img src="http://remindoo.org/CRMTool/assets/images/by-date2.png" alt="data"><select id="statuswise_filter" name="stati"><option value="0">By Status</option><option value="1">Active</option><option value="2">In-Active</option><option value="3">Frozen</option></select></li></ul></div>');
   });


   $(".change").click(function(){
//console.log()
  });
   
   
   $(document).on('click','#close',function(e)
    {
    $('.alert-success').hide();
    return false;
    });
</script>