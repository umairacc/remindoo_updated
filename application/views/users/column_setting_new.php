<?php $this->load->view('includes/header');?>
   <style type="text/css">
     .signed-change4
     {
      background-image: url(../assets/images/new_update10.png);
      background-color: #99d9ea;
     }
     .common-clienttab  .signed-change5{
        background-image: url(../assets/images/new_update11.png);
        background-position: right 15px center;
        padding-left: 12px;
        padding-right: 30px;
        background-color: #00a2e8;
     }
   </style>
<section class="client-details-view hidden-user01 floating_set columsettingcls colsettingclsnew">
   <?php if($this->session->flashdata('success')): ?>
   <p><div class="modal-alertsuccess alert alert-success" >
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <div class="pop-realted1">
              <div class="position-alert1">
                 Your Setting was Updated successfully.
              </div>
           </div>
      </div></p>
   <?php endif; ?>
      <div class="information-tab floating_set admin_floating1">
      <div class="deadline-crm1 floating_set">
                   <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                 <li class="nav-item form_info">
                        <a class="nav-link form_info active" data-toggle="tab" href="#allusers">client column settings</a>
                 </li>                    
               </ul>
             </div>
   <div class="tab-content">                     
    <div id="allusers" class="tab-pane active">
      <form class="client-firm-info1 validation fr-colsetting" method="post" action="<?php echo base_url().'user/update_column_setting_new'?>" enctype="multipart/form-data">
    <div class="information-tab floating_set">
      <div class="deadline-crm1 edit_right02 ">
               <div class="Footer common-clienttab pull-right">
               <?php   if($_SESSION['permission']['Client_Column_Settings']['edit']=='1'){   ?>
               <div class="change-client-bts1">
                  <input type="submit" class="signed-change1" id="save_exit" value ="Save & Exit">
               </div>
               <?php } ?>
               <div class="divleft">
                  <button  class="signed-change4 click_function" id="previous" type="button"  text="Previous Tab">Previous 
                  </button>
               </div>
               <div class="divright">
                  <button  class="signed-change5 click_function"  id="next" type="button"   text="Next Tab">Next
                  </button>
               </div>
            </div>
            </div>

     <div class="space-required">
      <div class="document-center client-infom-1 floating_set">
         <div class="Companies_House floating_set">
            <div class="pull-left">
               <h2>Column Setting</h2>
            </div>
            
         </div>
         <div class="addnewclient_pages1 floating_set bg_new1">
            <ol class="nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
               <li class="nav-item it0" value="0"><a class="nav-link active" data-toggle="tab" href="#required_information">
                  <strong>1</strong>Required information</a>
               </li>
               <li class="nav-item it1 bbs" value="0"><a class="nav-link" data-toggle="tab" href="#details"><strong>2.</strong>Details</a></li>
               <!-- <li class="nav-item it1" value="1"><a class="nav-link" data-toggle="tab" href="#contacts">Contacts</a></li> --> 
               <li class="nav-item it2" value="1"><a class="nav-link" data-toggle="tab" href="#contacts1"><strong>3.</strong>Contacts</a></li>
               <!--  <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#service"><strong>2.</strong>Service</a></li> -->
               <li class="nav-item it3 hide" value="2"><a class="nav-link" data-toggle="tab" href="#assignto"><strong>4.</strong>Assign to</a></li>
               <li class="nav-item it4 hide" value="2"><a class="nav-link" data-toggle="tab" href="#other"><strong>5.</strong>Other</a></li>
               <!-- <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#task"><strong>5.</strong>Task</a></li> -->
               <!-- <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#invoices">Invoices</a></li>
                  <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#timeline">Timeline</a></li> -->
            <li class="nav-item it5 hide" value="2"><a class="nav-link" data-toggle="tab" href="#noted"><strong>6.</strong>Referral</a></li> 
               <!-- <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#documents"><strong>8.</strong>Documents</a></li> -->
               <!-- <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#deadlines">Deadlines</a></li>
                  <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#referrals">Referrals</a></li> -->
               <li class="nav-item it7 hide" value="2" style=""><a class="nav-link" data-toggle="tab" href="#confirmation-statement"><strong>6.</strong>Confirmation Statement</a></li>
               <li class="nav-item it6 hide" value="2" style=""><a class="nav-link" data-toggle="tab" href="#accounts"><strong>7.</strong>Accounts</a></li>
               <li class="nav-item it12 hide" value="2" style=""><a class="nav-link" data-toggle="tab" href="#personal-tax-returns"><strong>8.</strong> Personal Tax Return</a></li>
               <li class="nav-item it11 hide" value="2" style=""><a class="nav-link" data-toggle="tab" href="#payroll"><strong>9.</strong> Payroll</a></li>
               <li class="nav-item it8 hide" value="2" style=""><a class="nav-link" data-toggle="tab" href="#vat-Returns" ><strong>10.</strong> VAT Returns</a></li>
               <li class="nav-item it13 hide" value="2" style=""><a class="nav-link" data-toggle="tab" href="#management-account"><strong>11.</strong> Management Accounts</a></li>
               <li class="nav-item it14 hide" value="2" style=""><a class="nav-link" data-toggle="tab" href="#investigation-insurance"><strong>12.</strong> Investigation Insurance</a></li>
            </ol>
         </div>
         <!-- tab close --> 
      </div>
     </div>
      <?php
         $rel_id=( isset($client) ? $client[0]['user_id'] : false);       
         ?>
	
			<div class="newupdate_design client-information-view1 column-groups <?php if($_SESSION['permission']['Client_Column_Settings']['view']!='1'){   ?> permission_deined <?php } ?> ">
      <div class="management_section client-details-tab01 data-mang-06 column_st-new floating_set">
      
      <div class="card-block accordion-block">
         <div class="tab-content">
            <div id="required_information" class="accord-proposal1 tab-pane fade in active">
            <div class="space-required">
               <div class="management_form1 box-division03">
                  <!--  <div class="form-titles"><a href="#" class="waves-effect" data-toggle="modal" data-target="#default-Modal"><h2>View on Companies House</h2></a></div> -->
                  <!--  <form> -->
                  <div class="form-group row name_fields">
                     <label class="col-sm-4 col-form-label">Name</label>
                     <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right" name="company_name1" id="company_name1" value="1" <?php if(isset($client[0]['crm_company_name1']) && ($client[0]['crm_company_name1']=='1') ){ ?> checked="checked"<?php } ?>>
                     </div>
                  </div>
                  <div class="form-group row">
                     <label class="col-sm-4 col-form-label">Legal Form</label>
                     <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right" name="legal_form" id="legal_form" value="1" <?php if(isset($client[0]['crm_legal_form']) && ($client[0]['crm_legal_form']=='1') ){ ?> checked="checked"<?php } ?>>
                     </div>
                  </div>
                 <!--  <div class="form-group row">
                     <label class="col-sm-4 col-form-label">Allocation Holder</label>
                     <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right" name="allocation_holder" id="allocation_holder" value="1" <?php if(isset($client[0]['crm_allocation_holder']) && ($client[0]['crm_allocation_holder']=='1') ){ ?> checked="checked"<?php } ?>>
                     </div>
                  </div> -->
               </div>
               <!--  <div class="button-group">
                  <div class="floatleft">
                     <button type="button" class="btn btn-primary btnNext">Next</button>
                  </div>
                  
                  </div> -->
            </div>
         </div>
            <!-- 1tab -->
            <div id="details" class="accord-proposal1 tab-pane fade">
               <div class="accordion-panel">
                  <div class="box-division03">
                     <div class="accordion-heading" role="tab" id="headingOne">
                        <h3 class="card-title accordion-title">
                           <a class="accordion-msg">Important Information</a>
                        </h3>
                     </div>
                     <div id="collapse" class="panel-collapse">
                        <div class="basic-info-client1">
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Company Name</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" name="company_name" id="company_name" placeholder="Accotax Limited" value="1" <?php if(isset($client[0]['crm_company_name']) && ($client[0]['crm_company_name']=='1') ){?> checked="checked"<?php } ?>>       
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Company Number</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="company_number" id="company_number" <?php if(isset($client[0]['crm_company_number']) && ($client[0]['crm_company_number']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Incorporation Date</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="date_of_creation" id="date_of_creation" <?php if(isset($client[0]['crm_incorporation_date']) && ($client[0]['crm_incorporation_date']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Registered In</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="registered_in" id="registered_in" <?php if(isset($client[0]['crm_registered_in']) && ($client[0]['crm_registered_in']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Address Line 1</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="address_line_one" id="address_line_one" <?php if(isset($client[0]['crm_address_line_one']) && ($client[0]['crm_address_line_one']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Address Line 2</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" name="address_line_two" id="address_line_two" value="1" <?php if(isset($client[0]['crm_address_line_two']) && ($client[0]['crm_address_line_two']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Address Line 3</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="address_line_three" id="address_line_three" <?php if(isset($client[0]['crm_address_line_three']) && ($client[0]['crm_address_line_two']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Town/City</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="crm_town_city" id="crm_town_city" <?php if(isset($client[0]['crm_town_city']) && ($client[0]['crm_town_city']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Post Code</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1"  name="crm_post_code" id="crm_post_code" <?php if(isset($client[0]['crm_post_code']) && ($client[0]['crm_post_code']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Company Status</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1"  name="company_status" id="company_status" <?php if(isset($client[0]['crm_company_status']) && ($client[0]['crm_company_status']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Company Type</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="company_type" id="company_type" <?php if(isset($client[0]['crm_company_type']) && ($client[0]['crm_company_type']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Company S.I.C</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="company_sic" id="company_sic" <?php if(isset($client[0]['crm_company_sic']) && ($client[0]['crm_company_sic']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Letter of Enagagement Sign Date</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="letter_sign" id="letter_sign" <?php if(isset($client[0]['crm_letter_sign']) && ($client[0]['crm_letter_sign']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Business Website</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="business_website" id="business_website" <?php if(isset($client[0]['crm_business_website']) && ($client[0]['crm_business_website']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>


                            <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Company URL</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="business_website" id="business_website" <?php if(isset($client[0]['crm_company_url']) && ($client[0]['crm_company_url']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>

                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Officers URL</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="business_website" id="business_website" <?php if(isset($client[0]['crm_officers_url']) && ($client[0]['crm_officers_url']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>



                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Accounting System In Use</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="accounting_system" id="accounting_system" <?php if(isset($client[0]['crm_accounting_system']) && ($client[0]['crm_accounting_system']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- accordion-panel -->
            <!--    <div class="accordion-panel">
                  <div class="box-division03">
                     <div class="accordion-heading" role="tab" id="headingOne">
                        <h3 class="card-title accordion-title">
                           <a class="accordion-msg">Companies House & HMRC</a>
                        </h3>
                     </div>
                     <div id="collapse" class="panel-collapse">
                        <div class="basic-info-client1">
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Authentication Code</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="auth_code" id="auth_code" <?php if(isset($client[0]['crm_companies_house_authorisation_code']) && ($client[0]['crm_companies_house_authorisation_code']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">UTR Number</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="crm_company_utr" id="crm_company_utr" <?php if(isset($client[0]['crm_company_utr']) && ($client[0]['crm_company_utr']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div> -->
               <!-- accordion-panel -->
             <!--   <div class="accordion-panel">
                  <div class="box-division03">
                     <div class="accordion-heading" role="tab" id="headingOne">
                        <h3 class="card-title accordion-title">
                           <a class="accordion-msg">Payroll</a>
                        </h3>
                     </div>
                     <div id="collapse" class="panel-collapse">
                        <div class="basic-info-client1">
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Accounts Office Reference Number</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="acc_ref_number" id="acc_ref_number" <?php if(isset($client[0]['crm_accounts_office_reference']) && ($client[0]['crm_accounts_office_reference']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">PAYE Reference Number</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="paye_ref_number" id="paye_ref_number" <?php if(isset($client[0]['crm_paye_ref_number']) && ($client[0]['crm_paye_ref_number']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div> -->
            </div>
            <!-- VAT Returns -->
            <div id="contacts1" class="tab-pane fade">
               <div class="space-required">
            <div class="make_a_primary">
                  <div class="client-info-circle1 floating_set">
                     <div class="accordion-panel contactcc ">
                        <div class="accordion-heading" role="tab" id="headingOne">
                           <h3 class="card-title accordion-title">
                              <a class="accordion-msg">Contact</a>
                           </h3>
                        </div>
                        <div class="primary-info03 column_setting_new_9 floating_set">
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1 only-title-add12">
                                 
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">title</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_title" id="contact_title" <?php if(isset($client[0]['contact_title']) && ($client[0]['contact_title']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>

                                <!--     <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">Contact Type</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_title" id="contact_title" <?php if(isset($client[0]['contact_type']) && ($client[0]['contact_type']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span> -->

                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">first name</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_first_name" id="contact_first_name" <?php if(isset($client[0]['contact_first_name']) && ($client[0]['contact_first_name']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">middle name</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_middle_name" id="contact_middle_name" <?php if(isset($client[0]['contact_middle_name']) && ($client[0]['contact_middle_name']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>

                                     <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">Last name</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_middle_name" id="contact_middle_name" <?php if(isset($client[0]['contact_last_name']) && ($client[0]['contact_last_name']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>


                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">surname</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_surname" id="contact_surname" <?php if(isset($client[0]['contact_surname']) && ($client[0]['contact_surname']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">prefered name</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_preferred_name" id="contact_preferred_name" <?php if(isset($client[0]['contact_preferred_name']) && ($client[0]['contact_preferred_name']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">mobile</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_mobile" id="contact_mobile" <?php if(isset($client[0]['contact_mobile']) && ($client[0]['contact_mobile']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">main E-Mail address</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_main_email" id="contact_main_email" <?php if(isset($client[0]['contact_main_email']) && ($client[0]['contact_main_email']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">Nationality</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_nationality" id="contact_nationality" <?php if(isset($client[0]['contact_nationality']) && ($client[0]['contact_nationality']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">PSC</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_psc" id="contact_psc" <?php if(isset($client[0]['contact_psc']) && ($client[0]['contact_psc']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">shareholder</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_shareholder" id="contact_shareholder" <?php if(isset($client[0]['contact_shareholder']) && ($client[0]['contact_shareholder']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">national insurance number</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_ni_number" id="contact_ni_number" <?php if(isset($client[0]['contact_ni_number']) && ($client[0]['contact_ni_number']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">Country Of Residence</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_country_of_residence" id="contact_country_of_residence" <?php if(isset($client[0]['contact_country_of_residence']) && ($client[0]['contact_country_of_residence']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                   
                                    <span class="primary-inner contact_type">
                                    <label class="col-sm-4 col-form-label">contact type</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_type" id="contact_type" <?php if(isset($client[0]['contact_type']) && ($client[0]['contact_type']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner spnMulti" id="others_customs" style="display:none">
                                    <label class="col-sm-4 col-form-label">Other(Custom)</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_other_custom" id="contact_other_custom" <?php if(isset($client[0]['contact_other_custom']) && ($client[0]['contact_other_custom']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">address line1</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_address_line1" id="contact_address_line1" <?php if(isset($client[0]['contact_address_line1']) && ($client[0]['contact_address_line1']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">address line2</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_address_line2" id="contact_address_line2" <?php if(isset($client[0]['contact_address_line2']) && ($client[0]['contact_address_line2']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">town/city</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_town_city" id="contact_town_city" <?php if(isset($client[0]['contact_town_city']) && ($client[0]['contact_town_city']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label"l>post code</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_post_code" id="contact_post_code" <?php if(isset($client[0]['contact_post_code']) && ($client[0]['contact_post_code']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">landline</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_landline" id="contact_landline" <?php if(isset($client[0]['contact_landline']) && ($client[0]['contact_landline']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">Work email</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_work_email" id="contact_work_email" <?php if(isset($client[0]['contact_work_email']) && ($client[0]['contact_work_email']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">date of birth</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_date_of_birth" id="contact_date_of_birth" <?php if(isset($client[0]['contact_date_of_birth']) && ($client[0]['contact_date_of_birth']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">nature of control</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_nature_of_control" id="contact_nature_of_control" <?php if(isset($client[0]['contact_nature_of_control']) && ($client[0]['contact_nature_of_control']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">marital status</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_marital_status" id="contact_marital_status" <?php if(isset($client[0]['contact_marital_status']) && ($client[0]['contact_marital_status']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">utr number</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_utr_number" id="contact_utr_number" <?php if(isset($client[0]['contact_utr_number']) && ($client[0]['contact_utr_number']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">Occupation</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_occupation" id="contact_occupation" <?php if(isset($client[0]['contact_occupation']) && ($client[0]['contact_occupation']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                                    <span class="primary-inner">
                                    <label class="col-sm-4 col-form-label">Appointed On</label>
                                    <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right" value="1" name="contact_appointed_on" id="contact_appointed_on" <?php if(isset($client[0]['contact_appointed_on']) && ($client[0]['contact_appointed_on']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                    </span>
                               
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            </div>
            <!-- contact1 -->
            <div id="assignto" class="tab-pane fade">
         <div class="masonry-container floating_set">
<div class="grid-sizer"></div>
               <div class="accordion-panel">
                  <div class="box-division03">
                     <div class="accordion-heading" role="tab" id="headingOne">
                        <h3 class="card-title accordion-title">
                           <a class="accordion-msg">Staff</a>
                        </h3>
                     </div>
                     <div id="collapse" class="panel-collapse">
                        <div class="basic-info-client1">
                           <div class="form-group row radio_bts ">
                              <label class="col-sm-4 col-form-label">Manager/Reviewer</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right fields" name="staff_manager" id="staff_manager" value="1" <?php if(isset($client[0]['staff_manager']) && ($client[0]['staff_manager']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row assign_cus_type">
                              <label class="col-sm-4 col-form-label">Managed By</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right" value="1" name="staff_managed" id="staff_managed" <?php if(isset($client[0]['staff_managed']) && ($client[0]['staff_managed']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- accordion-panel --> 
               <!--  <div class="accordion-panel">
                  <div class="box-division03">
                          <div class="accordion-heading" role="tab" id="headingOne">
                             <h3 class="card-title accordion-title">
                                <a class="accordion-msg">Responsible Team</a>
                             </h3>
                          </div>
                          <div id="collapse" class="panel-collapse">
                             <div class="basic-info-client1">
                                <div class="form-group row radio_bts ">
                                   <label class="col-sm-4 col-form-label">Team</label>
                                   <div class="col-sm-8">
                                      <input type="checkbox" class="js-small f-right fields" name="team" id="team" value="1" <?php if(isset($client[0]['team']) && ($client[0]['team']=='1') ){?> checked="checked"<?php } ?>>
                                   </div>
                                </div>
                                <div class="form-group row assign_cus_type">
                                   <label class="col-sm-4 col-form-label">Allocation office</label>
                                   <div class="col-sm-8">
                                      
                                      <input type="checkbox" class="js-small f-right" value="1" name="team_allocation" id="team_allocation" <?php if(isset($client[0]['team_allocation']) && ($client[0]['team_allocation']=='1') ){?> checked="checked"<?php } ?>>
                                   </div>
                                </div>
                                
                             </div>
                          </div>
                       </div>
                  </div> -->
               <!-- accordion-panel --> 
               <!-- <div class="accordion-panel">
                  <div class="box-division03">
                          <div class="accordion-heading" role="tab" id="headingOne">
                             <h3 class="card-title accordion-title">
                                <a class="accordion-msg">Responsible Department</a>
                             </h3>
                          </div>
                          <div id="collapse" class="panel-collapse">
                             <div class="basic-info-client1">
                                <div class="form-group row radio_bts ">
                                   <label class="col-sm-4 col-form-label">Department</label>
                                   <div class="col-sm-8">
                                      <input type="checkbox" class="js-small f-right fields" name="department" id="department" value="1" <?php if(isset($client[0]['department']) && ($client[0]['department']=='1') ){?> checked="checked"<?php } ?>>
                                   </div>
                                </div>
                                <div class="form-group row assign_cus_type">
                                   <label class="col-sm-4 col-form-label">Allocation office</label>
                                   <div class="col-sm-8">
                                      <input type="checkbox" class="js-small f-right" value="1" name="department_allocation" id="department_allocation" <?php if(isset($client[0]['department_allocation']) && ($client[0]['department_allocation']=='1') ){?> checked="checked"<?php } ?>>
                                   </div>
                                </div>
                                
                             </div>
                          </div>
                       </div>
                  </div> -->
               <!-- accordion-panel --> 
               <!--  <div class="accordion-panel">
                  <div class="box-division03">
                          <div class="accordion-heading" role="tab" id="headingOne">
                             <h3 class="card-title accordion-title">
                                <a class="accordion-msg">Responsible Members</a>
                             </h3>
                          </div>
                          <div id="collapse" class="panel-collapse">
                             <div class="basic-info-client1">
                                <div class="form-group row radio_bts ">
                                   <label class="col-sm-4 col-form-label">Manager/Reviewer</label>
                                   <div class="col-sm-8">
                                      <input type="checkbox" class="js-small f-right fields" name="member_manager" id="member_manager" value="1" <?php if(isset($client[0]['member_manager']) && ($client[0]['member_manager']=='1') ){?> checked="checked"<?php } ?>>
                                   </div>
                                </div>
                                <div class="form-group row assign_cus_type">
                                   <label class="col-sm-4 col-form-label">Managed By</label>
                                   <div class="col-sm-8">
                                      <input type="checkbox" class="js-small f-right" value="1" name="member_managed" id="member_managed" <?php if(isset($client[0]['member_managed']) && ($client[0]['member_managed']=='1') ){?> checked="checked"<?php } ?>>
                                   </div>
                                </div>
                                
                             </div>
                          </div>
                       </div>
                  </div> -->
               <!-- accordion-panel --> 
               <div class="accordion-panel">
                  <div class="box-division03">
                     <div class="accordion-heading" role="tab" id="headingOne">
                        <h3 class="card-title accordion-title">
                           <a class="accordion-msg">Anti Money Laundering Checks</a>
                        </h3>
                     </div>
                     <div id="collapse" class="panel-collapse">
                        <div class="basic-info-client1">
                           <div class="form-group row radio_bts ">
                              <label class="col-sm-4 col-form-label">Client ID Verified</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right fields" name="client_id_verified" id="client_id_verified" value="1" <?php if(isset($client[0]['crm_assign_client_id_verified']) && ($client[0]['crm_assign_client_id_verified']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row assign_cus_type">
                              <label class="col-sm-4 col-form-label">Type of ID Provided</label>
                              <div class="col-sm-8">
                                 <!-- <select name="type_of_id" id="person" class="form-control fields assign_cus">
                                    <option value="" selected="selected">--Select--</option>
                                    <option value="Passport" <?php if(isset($client[0]['crm_assign_type_of_id']) && $client[0]['crm_assign_type_of_id']=='Passport') {?> selected="selected"<?php } ?>>Passport</option>
                                    <option value="Driving_License" <?php if(isset($client[0]['crm_assign_type_of_id']) && $client[0]['crm_assign_type_of_id']=='Driving_License') {?> selected="selected"<?php } ?>>Driving License</option>
                                    <option value="Other_Custom" <?php if(isset($client[0]['crm_assign_type_of_id']) && $client[0]['crm_assign_type_of_id']=='Other_Custom') {?> selected="selected"<?php } ?>>Other Custom</option>
                                    </select> -->
                                 <input type="checkbox" class="js-small f-right" value="1" name="type_of_id" id="type_of_id" <?php if(isset($client[0]['crm_assign_type_of_id']) && ($client[0]['crm_assign_type_of_id']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <!-- <div class="form-group row name_fields spanassign" style="display:none">
                              <label class="col-sm-4 col-form-label">Other Custom</label>
                              <div class="col-sm-8">
                                 <input type="text" name="assign_other_custom" id="assign_other_custom" placeholder="" value="<?php if(isset($client[0]['crm_assign_other_custom']) && ($client[0]['crm_assign_other_custom']!='') ){ echo $client[0]['crm_assign_other_custom'];}?>" class="fields">
                              </div>
                              </div> -->
                           <div class="form-group row radio_bts ">
                              <label class="col-sm-4 col-form-label">Proof of Address</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right fields" name="proof_of_address" id="proof_of_address" value="1" <?php if(isset($client[0]['crm_assign_proof_of_address']) && ($client[0]['crm_assign_proof_of_address']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row radio_bts ">
                              <label class="col-sm-4 col-form-label">Meeting with the Client</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right fields" name="meeting_client" id="meeting_client" value="1" <?php if(isset($client[0]['crm_assign_meeting_client']) && ($client[0]['crm_assign_meeting_client']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- accordion-panel -->   
               <div class="accordion-panel">
                  <div class="box-division03">
                     <div class="accordion-heading" role="tab" id="headingOne">
                        <h3 class="card-title accordion-title">
                           <a class="accordion-msg">Client Source</a>
                        </h3>
                     </div>
                     <div id="collapse" class="panel-collapse">
                        <div class="basic-info-client1">
                           <div class="form-group row">
                              <label class="col-sm-4 col-form-label">Source</label>
                              <div class="col-sm-8">
                                 <!--  <select name="source" id="source" class="form-control fields">
                                    <option value="" selected="selected">--Select--</option>
                                    <option value="Google" <?php if(isset($client[0]['crm_assign_source']) && $client[0]['crm_assign_source']=='Google') {?> selected="selected"<?php } ?>>Google</option>
                                    <option value="FB" <?php if(isset($client[0]['crm_assign_source']) && $client[0]['crm_assign_source']=='FB') {?> selected="selected"<?php } ?>>FB</option>
                                    <option value="Website" <?php if(isset($client[0]['crm_assign_source']) && $client[0]['crm_assign_source']=='Website') {?> selected="selected"<?php } ?>>Website</option>
                                    <option value="Existing Client" <?php if(isset($client[0]['crm_assign_source']) && $client[0]['crm_assign_source']=='Existing Client') {?> selected="selected"<?php } ?>>Existing Client</option>
                                    </select> -->
                                 <input type="checkbox" class="js-small f-right" value="1" name="crm_assign_source" id="crm_assign_source" <?php if(isset($client[0]['crm_assign_source']) && ($client[0]['crm_assign_source']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Refered by Existing Client</label>
                              <div class="col-sm-8">
                                 <!--  <input type="text" name="refer_exist_client" id="refer_exist_client" placeholder="" value="" class="fields"> -->
                                 <!--    <select name="refer_exist_client" id="refer_exist_client" class="fields">
                                    <option value=''>--Select one--</option>
                                    <?php foreach ($referby as $referbykey => $referbyvalue) {
                                       # code...
                                       ?>
                                    <option value="<?php echo $referbyvalue['crm_first_name'];?>" <?php if(isset($client[0]['crm_refered_by']) && $client[0]['crm_refered_by']==$referbyvalue['crm_first_name']) {?> selected="selected"<?php } ?>><?php echo $referbyvalue['crm_first_name'];?></option>
                                    <?php } ?>
                                    </select> -->
                                 <input type="checkbox" class="js-small f-right" value="1" name="refer_exist_client" id="refer_exist_client" <?php if(isset($client[0]['crm_refered_by']) && ($client[0]['crm_refered_by']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Relationship to the Client</label>
                              <div class="col-sm-8">
                                 <!--  <input type="text" name="relationship_client" id="relationship_client" placeholder="" value="<?php if(isset($client[0]['crm_assign_relationship_client']) && ($client[0]['crm_assign_relationship_client']!='') ){ echo $client[0]['crm_assign_relationship_client'];}?>" class="fields"> -->
                                 <input type="checkbox" class="js-small f-right" value="1" name="relationship_client" id="relationship_client" <?php if(isset($client[0]['crm_assign_relationship_client']) && ($client[0]['crm_assign_relationship_client']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- accordion-panel -->
             
               <!-- accordion-panel -->   
            </div>
         </div>
            <!-- assignto --> 
                        <div id="other" class="tab-pane fade">
                        <div class="masonry-container floating_set">
                        <div class="grid-sizer"></div>
                        <div class="accordion-panel">
                        <div class="box-division03">
                        <div class="accordion-heading" role="tab" id="headingOne">
                           <h3 class="card-title accordion-title">
                              <a class="accordion-msg">Other</a>
                           </h3>
                        </div>
                        <div id="collapse" class="panel-collapse">
                           <div class="basic-info-client1">
                              <div class="form-group row radio_bts ">
                                 <label class="col-sm-4 col-form-label">Previous Accounts</label>
                                 <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" <?php if(isset($client[0]['crm_previous_accountant']) && ($client[0]['crm_previous_accountant']=='1') ){?> checked="checked"<?php } ?> data-id="preacc">
                                 </div>
                              </div>
                              <?php 
                                 (isset($client[0]['crm_previous_accountant']) && $client[0]['crm_previous_accountant'] == 'on') ? $pre =  "block" : $pre = 'none';
                                 
                                 
                                 ?>
                              <div id="enable_preacc" style="display:<?php echo $pre;?>;">
                                 <div class="form-group row name_fields">
                                    <label class="col-sm-4 col-form-label">Name of the Firm</label>
                                    <div class="col-sm-8">
                                  
                                       <input type="checkbox" class="js-small f-right" value="1" name="name_of_firm" id="name_of_firm" <?php if(isset($client[0]['crm_other_name_of_firm']) && ($client[0]['crm_other_name_of_firm']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row ">
                                    <label class="col-sm-4 col-form-label">Address</label>
                                    <div class="col-sm-8">
                                      
                                       <input type="checkbox" class="js-small f-right" value="1" name="other_address" id="other_address" <?php if(isset($client[0]['crm_other_address']) && ($client[0]['crm_other_address']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                              </div>
                              <!-- preacc close-->
                              <div class="form-group row name_fields">
                                 <label class="col-sm-4 col-form-label">Contact Tel</label>
                                 <div class="col-sm-8">
                                  
                                    <input type="checkbox" class="js-small f-right" value="1" name="other_contact_no" id="other_contact_no" <?php if(isset($client[0]['crm_other_contact_no']) && ($client[0]['crm_other_contact_no']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                              </div>
                              <div class="form-group row name_fields">
                                 <label class="col-sm-4 col-form-label">Email Address</label>
                                 <div class="col-sm-8">
                                    <!--   <input type="email" name="emailid" id="emailid" placeholder="" value="<?php if(isset($client[0]['crm_email']) && ($client[0]['crm_email']!='') ){ echo $client[0]['crm_email'];}?>" class="fields"> -->
                                    <input type="checkbox" class="js-small f-right" value="1" name="emailid" id="emailid" <?php if(isset($client[0]['crm_email']) && ($client[0]['crm_email']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                              </div>
                              <div class="form-group row radio_bts ">
                                 <label class="col-sm-4 col-form-label">Chase for Information</label>
                                 <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right fields" name="chase_for_info" id="chase_for_info" <?php if(isset($client[0]['crm_other_chase_for_info']) && ($client[0]['crm_other_chase_for_info']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                              </div>
                              <div class="form-group row ">
                                 <label class="col-sm-4 col-form-label">Notes</label>
                                 <div class="col-sm-8">
                                   
                                    <input type="checkbox" class="js-small f-right" value="1" name="other_notes" id="other_notes" <?php if(isset($client[0]['crm_other_notes']) && ($client[0]['crm_other_notes']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                              </div>
                           </div>
                        </div>
                        </div>
                        </div>

                        <!-- accordion-panel -->
                        <div class="accordion-panel">
                        <div class="box-division03">
                        <div class="accordion-heading" role="tab" id="headingOne">
                           <h3 class="card-title accordion-title">
                              <a class="accordion-msg">Client Login</a>
                           </h3>
                        </div>
                        <div id="collapse" class="panel-collapse">
                           <div class="basic-info-client1">
                              <div class="form-group row name_fields">
                                 <label class="col-sm-4 col-form-label">Username</label>
                                 <div class="col-sm-8">
                                   
                                    <input type="checkbox" class="js-small f-right" value="1" name="user_name" id="user_name" <?php if(isset($client[0]['crm_other_username']) && ($client[0]['crm_other_username']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                              </div>
                           </div>
                        </div>
                        </div>
                        </div>
                        <!-- accordion-panel -->

                        </div>
                        </div>
            <!-- other -->
               <!-- noted -->
                     <div id="noted" class="tab-pane fade">
                     <div class="accordion-panel">
                     <div class="box-division03">
                      
                         <div class="accordion-panel accordion-panelnew">
                     <div class="box-division03">
                     <div class="accordion-heading" role="tab" id="headingOne">
                        <h3 class="card-title accordion-title">
                           <a class="accordion-msg">Invite Client</a>
                        </h3>
                     </div>
                     <div id="collapse" class="panel-collapse">
                        <div class="basic-info-client1">
                           <div class="form-group row radio_bts ">
                              <label class="col-sm-4 col-form-label">Invite to use our system</label>
                              <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right fields" name="invite_use" id="invite_use" <?php if(isset($client[0]['crm_other_invite_use']) && ($client[0]['crm_other_invite_use']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                        
                       
                        
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Send invitation link</label>
                              <div class="col-sm-8">
                                 <!--  <input type="text" name="send_invit_link" id="send_invit_link" placeholder="" value="<?php if(isset($client[0]['crm_other_send_invit_link']) && ($client[0]['crm_other_send_invit_link']!='') ){ echo $client[0]['crm_other_send_invit_link'];}?>" class="fields"> -->
                                 <input type="checkbox" class="js-small f-right" value="1" name="send_invit_link" id="send_invit_link" <?php if(isset($client[0]['crm_other_send_invit_link']) && ($client[0]['crm_other_send_invit_link']=='1') ){?> checked="checked"<?php } ?>>
                              </div>
                           </div>
                         
                        </div>
                     </div>
                     </div>
                     </div>
                     </div>
                     </div>
                     <!-- accordion-panel -->
                     </div> <!-- noted -->
                        <div id="confirmation-statement" class="tab-pane fade">
                        <div class="masonry-container floating_set">
                        <div class="grid-sizer"></div>
                           <div class="accordion-panel">
                              <div class="box-division03">
                                 <div class="accordion-heading" role="tab" id="headingOne">
                                    <h3 class="card-title accordion-title">
                                       <a class="accordion-msg">Important Information</a>
                                    </h3>
                                 </div>
                                 <div id="collapse" class="panel-collapse">
                                    <div class="basic-info-client1">
                                       <div class="form-group row name_fields">
                                          <label class="col-sm-4 col-form-label">Companies Authentication Code</label>
                                          <div class="col-sm-8">
                                             <!--     <input type="text" name="confirmation_auth_code" id="confirmation_auth_code" placeholder="" value="<?php if(isset($client[0]['crm_confirmation_auth_code']) && ($client[0]['crm_confirmation_auth_code']!='') ){ echo $client[0]['crm_confirmation_auth_code'];}?>" class="fields"> -->
                                             <input type="checkbox" class="js-small f-right" value="1" name="confirmation_auth_code" id="confirmation_auth_code" <?php if(isset($client[0]['crm_confirmation_auth_code']) && ($client[0]['crm_confirmation_auth_code']=='1') ){?> checked="checked"<?php } ?>>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- accordion-panel -->
                           <div class="accordion-panel">
                              <div class="box-division03">
                                 <div class="accordion-heading" role="tab" id="headingOne">
                                    <h3 class="card-title accordion-title">
                                       <a class="accordion-msg">Confirmation Statement</a>
                                    </h3>
                                 </div>
                                 <div id="collapse" class="panel-collapse">
                                    <div class="basic-info-client1">
                                       <?php 
                                          (isset(json_decode($client[0]['conf_statement'])->reminder) && $client[0]['conf_statement'] != '') ? $one =  json_decode($client[0]['conf_statement'])->reminder : $one = '';
                                          ($one!='') ? $one_cs = "" : $one_cs = "display:none;";
                                          
                                          ?> 
                                       <div class="form-group row name_fields">
                                          <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Next Statement Date</label>
                                          <div class="col-sm-8">
                                             <!--   <input type="text" name="confirmation_next_made_up_to" id="confirmation_next_made_up_to" placeholder="" value="<?php if(isset($company['confirmation_statement']['next_made_up_to'])){ echo $company['confirmation_statement']['next_made_up_to']; }  ?>" class="fields datepicker"> -->
                                             <input type="checkbox" class="js-small f-right" value="1" name="confirmation_next_made_up_to" id="confirmation_next_made_up_to" <?php if(isset($client[0]['crm_confirmation_statement_date']) && ($client[0]['crm_confirmation_statement_date']=='1') ){?> checked="checked"<?php } ?>>
                                          </div>
                                       </div>
                                       <div class="form-group row name_fields">
                                          <label class="col-sm-4 col-form-label">Due By</label>
                                          <div class="col-sm-8">
                                             <!--  <input type="text" name="confirmation_next_due" id="confirmation_next_due" placeholder="" value="<?php if(isset($company['confirmation_statement']['next_due'])){ echo $company['confirmation_statement']['next_due']; }  ?>" class="fields datepicker"> -->
                                             <input type="checkbox" class="js-small f-right" value="1" name="confirmation_next_due" id="confirmation_next_due" <?php if(isset($client[0]['crm_confirmation_statement_due_date']) && ($client[0]['crm_confirmation_statement_due_date']=='1') ){?> checked="checked"<?php } ?>>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- accordion-panel -->
                           <!-- accordion-panel -->
                           <div class="accordion-panel">
                              <div class="box-division03">
                                 <div class="accordion-heading" role="tab" id="headingOne">
                                    <h3 class="card-title accordion-title">
                                       <a class="accordion-msg"></a>
                                    </h3>
                                 </div>
                                 <div id="collapse" class="panel-collapse">
                                    <div class="basic-info-client1">
                                       <div class="form-group row">
                                          <label class="col-sm-4 col-form-label">Officers</label>
                                          <div class="col-sm-8">
                                          
                                             <input type="checkbox" class="js-small f-right" value="1" name="confirmation_officers" id="confirmation_officers" <?php if(isset($client[0]['crm_confirmation_officers']) && ($client[0]['crm_confirmation_officers']=='1') ){?> checked="checked"<?php } ?>>
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <label class="col-sm-4 col-form-label">Share Capital</label>
                                          <div class="col-sm-8">
                                         
                                             <input type="checkbox" class="js-small f-right" value="1" name="share_capital" id="share_capital" <?php if(isset($client[0]['crm_share_capital']) && ($client[0]['crm_share_capital']=='1') ){?> checked="checked"<?php } ?>>
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <label class="col-sm-4 col-form-label">Shareholders</label>
                                          <div class="col-sm-8">
                                            
                                             <input type="checkbox" class="js-small f-right" value="1" name="shareholders" id="shareholders" <?php if(isset($client[0]['crm_shareholders']) && ($client[0]['crm_shareholders']=='1') ){?> checked="checked"<?php } ?>> 
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <label class="col-sm-4 col-form-label">People with Significant Control</label>
                                          <div class="col-sm-8">
                                            
                                             <input type="checkbox" class="js-small f-right" value="1" name="people_with_significant_control" id="people_with_significant_control" <?php if(isset($client[0]['crm_people_with_significant_control']) && ($client[0]['crm_people_with_significant_control']=='1') ){?> checked="checked"<?php } ?>> 
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- accordion-panel -->
                         
                              </div>
                           <!-- accordion-panel -->
                        </div>
            <!-- confirmation-statement -->
                        <div id="accounts" class="tab-pane fade">
                        <div class="masonry-container floating_set">
                        <div class="grid-sizer"></div>
                        <div class="accordion-panel">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Important Information</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                    <div class="form-group row name_fields">
                                       <label class="col-sm-4 col-form-label">Companies Authentication Code</label>
                                       <div class="col-sm-8">
                                          <!-- <input type="text" name="accounts_auth_code" id="accounts_auth_code" placeholder="" value="<?php if(isset($client[0]['crm_accounts_auth_code']) && ($client[0]['crm_accounts_auth_code']!='') ){ echo $client[0]['crm_accounts_auth_code'];}?>" class="fields"> -->
                                          <input type="checkbox" class="js-small f-right" value="1" name="accounts_auth_code" id="accounts_auth_code" <?php if(isset($client[0]['crm_accounts_auth_code']) && ($client[0]['crm_accounts_auth_code']=='1') ){?> checked="checked"<?php } ?>> 
                                       </div>
                                    </div>
                                    <div class="form-group row name_fields">
                                       <label class="col-sm-4 col-form-label">Company's UTR Number</label>
                                       <div class="col-sm-8">
                                          <!--   <input type="number" name="accounts_utr_number" id="accounts_utr_number" placeholder="" value="<?php if(isset($client[0]['crm_accounts_utr_number']) && ($client[0]['crm_accounts_utr_number']!='') ){ echo $client[0]['crm_accounts_utr_number'];}?>" class="fields"> -->
                                          <input type="checkbox" class="js-small f-right" value="1" name="accounts_utr_number" id="accounts_utr_number" <?php if(isset($client[0]['crm_accounts_utr_number']) && ($client[0]['crm_accounts_utr_number']=='1') ){?> checked="checked"<?php } ?>> 
                                       </div>
                                    </div>
                                    <!-- <div class="form-group row radio_bts ">
                                       <label class="col-sm-4 col-form-label">Companies House Reminders</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="company_house_reminder" id="company_house_reminder" <?php if(isset($client[0]['crm_companies_house_email_remainder']) && ($client[0]['crm_companies_house_email_remainder']=='1') ){?> checked="checked"<?php } ?>>
                                       </div>
                                       </div> -->
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                        <div class="accordion-panel">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Accounts</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                    <?php 
                                       (isset(json_decode($client[0]['accounts'])->reminder) && $client[0]['accounts'] != '') ? $two =  json_decode($client[0]['accounts'])->reminder : $two = '';
                                       
                                       ($two !='') ? $two_acc = "" : $two_acc = "display:none;";
                                       
                                       ?>
                                    <div class="form-group row name_fields" >
                                       <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Next accounts made up to</label>
                                       <div class="col-sm-8">
                                          <!-- <input type="text" name="accounts_next_made_up_to" id="accounts_next_made_up_to" placeholder="" value="<?php if(isset($company['accounts']['next_made_up_to'])){ echo $company['accounts']['next_made_up_to']; }  ?>" class="fields datepicker"> -->
                                          <input type="checkbox" class="js-small f-right fields" name="accounts_next_made_up_to" id="accounts_next_made_up_to" <?php if(isset($client[0]['crm_ch_yearend']) && ($client[0]['crm_ch_yearend']=='1') ){?> checked="checked"<?php } ?>>
                                       </div>
                                    </div>
                                    <div class="form-group row name_fields">
                                       <label class="col-sm-4 col-form-label">Accounts Due Date - Companies House</label>
                                       <div class="col-sm-8">
                                          <!--  <input type="text" name="accounts_next_due" id="accounts_next_due" placeholder="" value="<?php if(isset($company['accounts']['next_due'])){ echo $company['accounts']['next_due']; }  ?>" class="fields datepicker"> -->
                                          <input type="checkbox" class="js-small f-right fields" name="accounts_next_due" id="accounts_next_due" <?php if(isset($client[0]['crm_ch_accounts_next_due']) && ($client[0]['crm_ch_accounts_next_due']=='1') ){?> checked="checked"<?php } ?> >
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                        <!-- accordion-panel -->
                        <div class="accordion-panel">
                           <div class="box-division03">
                             
                                 <div class="accordion-heading" role="tab" id="headingOne">
                                    <h3 class="card-title accordion-title">
                                       <a class="accordion-msg">Tax Return </a>
                                    </h3>
                                 </div>
                                 <div id="collapse" class="panel-collapse">
                                    <div class="basic-info-client1">
                                       <div class="form-group row name_fields">
                                          <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Accounts Due Date - HMRC</label>
                                          <div class="col-sm-8">
                                             <!--   <input type="text" name="accounts_due_date_hmrc" id="accounts_due_date_hmrc" placeholder="" value="<?php echo date("F j, Y", strtotime("+1 years", strtotime($company['accounts']['next_made_up_to'])));
                                                ?>" class="fields datepicker"> -->
                                             <input type="checkbox" class="js-small f-right" value="1" name="accounts_due_date_hmrc" id="accounts_due_date_hmrc" <?php if(isset($client[0]['crm_accounts_due_date_hmrc']) && ($client[0]['crm_accounts_due_date_hmrc']=='1') ){?> checked="checked"<?php } ?> >
                                          </div>
                                       </div>
                                       <div class="form-group row name_fields">
                                          <label class="col-sm-4 col-form-label">Tax Payment Date to HMRC</label>
                                          <div class="col-sm-8">
                                             <!--   <input type="text" name="accounts_tax_date_hmrc" id="accounts_tax_date_hmrc" placeholder="" value="<?php echo date("F j, Y", strtotime("+1 days", strtotime($company['accounts']['next_due'])));?>" class="fields datepicker"> -->
                                             <input type="checkbox" class="js-small f-right" value="1" name="accounts_tax_date_hmrc" id="accounts_tax_date_hmrc" <?php if(isset($client[0]['crm_accounts_tax_date_hmrc']) && ($client[0]['crm_accounts_tax_date_hmrc']=='1') ){?> checked="checked"<?php } ?>>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              
                           </div>
                           <!-- accordion-panel -->
                           <!-- accordion-panel -->
                        </div>

                           </div>
                        </div>
            <!-- accounts -->
                           <div id="personal-tax-returns" class="tab-pane fade">
                           <div class="masonry-container floating_set">
                           <div class="grid-sizer"></div>
                              <div class="accordion-panel">
                                 <div class="box-division03">
                                    <div class="accordion-heading" role="tab" id="headingOne">
                                       <h3 class="card-title accordion-title">
                                          <a class="accordion-msg">Important Information</a>
                                       </h3>
                                    </div>
                                    <div id="collapse" class="panel-collapse">
                                       <div class="basic-info-client1">
                                          <div class="form-group row ">
                                             <label class="col-sm-4 col-form-label">Personal UTR Number</label>
                                             <div class="col-sm-8">
                                                <!-- <input type="number" name="personal_utr_number" id="personal_utr_number" class="form-control fields" value="<?php if(isset($client[0]['crm_personal_utr_number']) && ($client[0]['crm_personal_utr_number']!='') ){ echo $client[0]['crm_personal_utr_number'];}?>"> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="personal_utr_number" id="personal_utr_number" <?php if(isset($client[0]['crm_personal_utr_number']) && ($client[0]['crm_personal_utr_number']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row ">
                                             <label class="col-sm-4 col-form-label">National Insurance Number</label>
                                             <div class="col-sm-8">
                                                <!--  <input type="number" name="ni_number" id="ni_number" class="form-control fields" value="<?php if(isset($client[0]['crm_ni_number']) && ($client[0]['crm_ni_number']!='') ){ echo $client[0]['crm_ni_number'];}?>"> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="ni_number" id="ni_number" <?php if(isset($client[0]['crm_ni_number']) && ($client[0]['crm_ni_number']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row radio_bts ">
                                             <label class="col-sm-4 col-form-label">Property Income</label>
                                             <div class="col-sm-8">
                                                <input type="checkbox" class="js-small f-right fields" name="property_income" id="property_income" <?php if(isset($client[0]['crm_property_income']) && ($client[0]['crm_property_income']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row radio_bts ">
                                             <label class="col-sm-4 col-form-label">Additional Income</label>
                                             <div class="col-sm-8">
                                                <input type="checkbox" class="js-small f-right fields" name="additional_income" id="additional_income" <?php if(isset($client[0]['crm_additional_income']) && ($client[0]['crm_additional_income']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- accordion-panel -->
                              <div class="accordion-panel">
                                 <div class="box-division03">
                                    <div class="accordion-heading" role="tab" id="headingOne">
                                       <h3 class="card-title accordion-title">
                                          <a class="accordion-msg">Personal Tax Return          </a>
                                       </h3>
                                    </div>
                                    <div id="collapse" class="panel-collapse">
                                       <div class="basic-info-client1">
                                          <div class="form-group row name_fields">
                                             <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Tax Return Date</label>
                                             <div class="col-sm-8">
                                                <!--  <input type="text" name="personal_tax_return_date" id="personal_tax_return_date" placeholder="" value="<?php if(isset($client[0]['crm_personal_tax_return_date']) && ($client[0]['crm_personal_tax_return_date']!='') ){ echo $client[0]['crm_personal_tax_return_date'];}?>" class="datepicker fields"> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="personal_tax_return_date" id="personal_tax_return_date" <?php if(isset($client[0]['crm_personal_tax_return_date']) && ($client[0]['crm_personal_tax_return_date']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Due Date on Paper Return</label>
                                             <div class="col-sm-8">
                                                <!--  <input type="text" name="personal_due_date_return" id="personal_due_date_return" placeholder="" value="<?php if(isset($client[0]['crm_personal_due_date_return']) && ($client[0]['crm_personal_due_date_return']!='') ){ echo $client[0]['crm_personal_due_date_return'];}?>" class="fields datepicker"> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="personal_due_date_return" id="personal_due_date_return" >
                                             </div>
                                          </div>
                                          <div class="form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Due Date online filing</label>
                                             <div class="col-sm-8">
                                                <!-- <input type="text" name="personal_due_date_online" id="personal_due_date_online" placeholder="" value="<?php if(isset($client[0]['crm_personal_due_date_online']) && ($client[0]['crm_personal_due_date_online']!='') ){ echo $client[0]['crm_personal_due_date_online'];}?>" class="fields datepicker"> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="personal_due_date_online" id="personal_due_date_online" <?php if(isset($client[0]['crm_personal_due_date_online']) && ($client[0]['crm_personal_due_date_online']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- accordion-panel -->
                              <!-- accordion-panel -->
                        
                              <!-- accordion-panel -->
                           </div>
                           </div>
            <!-- Personal Tax Return -->
                                 <div id="payroll" class="tab-pane fade">
                                 <div class="masonry-container floating_set">
                                 <div class="grid-sizer"></div>
                                 <div class="accordion-panel">
                                 <div class="box-division03">
                                 <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Important Information</a>
                                 </h3>
                                 </div>
                                 <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                 <div class="form-group row name_fields">
                                 <label class="col-sm-4 col-form-label">Accounts Office Reference Number</label>
                                 <div class="col-sm-8">
                                 <!--  <input type="number" name="payroll_acco_off_ref_no" id="payroll_acco_off_ref_no" placeholder="" value="<?php if(isset($client[0]['crm_payroll_acco_off_ref_no']) && ($client[0]['crm_payroll_acco_off_ref_no']!='') ){ echo $client[0]['crm_payroll_acco_off_ref_no'];}?>" class="fields"> -->
                                 <input type="checkbox" class="js-small f-right" value="1" name="payroll_acco_off_ref_no" id="payroll_acco_off_ref_no" <?php if(isset($client[0]['crm_payroll_acco_off_ref_no']) && ($client[0]['crm_payroll_acco_off_ref_no']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row name_fields">
                                 <label class="col-sm-4 col-form-label">PAYE Office Ref Number</label>
                                 <div class="col-sm-8">
                                 <!--   <input type="number" name="paye_off_ref_no" id="paye_off_ref_no" placeholder="" value="<?php if(isset($client[0]['crm_paye_off_ref_no']) && ($client[0]['crm_paye_off_ref_no']!='') ){ echo $client[0]['crm_paye_off_ref_no'];}?>" class="fields"> -->
                                 <input type="checkbox" class="js-small f-right" value="1" name="paye_off_ref_no" id="paye_off_ref_no" <?php if(isset($client[0]['crm_paye_off_ref_no']) && ($client[0]['crm_paye_off_ref_no']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 </div>
                                 </div>
                                 </div>
                                 </div>
                                 <!-- accordion-panel -->
                                 <div class="accordion-panel">
                                 <div class="box-division03">
                                 <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Payroll</a>
                                 </h3>
                                 </div>
                                 <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                 <div class="form-group row name_fields">
                                 <label class="col-sm-4 col-form-label">Payroll Registration Date</label>
                                 <div class="col-sm-8">
                                   
                                    <input type="checkbox" class="js-small f-right" value="1" name="payroll_reg_date" id="payroll_reg_date" <?php if(isset($client[0]['crm_payroll_reg_date']) && ($client[0]['crm_payroll_reg_date']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row">
                                 <label class="col-sm-4 col-form-label">Number of Employees</label>
                                 <div class="col-sm-8">
                                  
                                    <input type="checkbox" class="js-small f-right" value="1" name="no_of_employees" id="no_of_employees" <?php if(isset($client[0]['crm_no_of_employees']) && ($client[0]['crm_no_of_employees']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                 <label class="col-sm-4 col-form-label">First Pay Date</label>
                                 <div class="col-sm-8">
                                   
                                    <input type="checkbox" class="js-small f-right" value="1" name="first_pay_date" id="first_pay_date" <?php if(isset($client[0]['crm_first_pay_date']) && ($client[0]['crm_first_pay_date']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row">
                                 <label class="col-sm-4 col-form-label">Payroll Run</label>
                                 <div class="col-sm-8">

                                    <input type="checkbox" class="js-small f-right" value="1" name="payroll_run" id="payroll_run" <?php if(isset($client[0]['crm_payroll_run']) && ($client[0]['crm_payroll_run']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                 <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Payroll Run Date</label>
                                 <div class="col-sm-8">
                                  
                                    <input type="checkbox" class="js-small f-right" value="1" name="payroll_run_date" id="payroll_run_date" <?php if(isset($client[0]['crm_payroll_run_date']) && ($client[0]['crm_payroll_run_date']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                 <label class="col-sm-4 col-form-label">RTI Due/Deadline Date</label>
                                 <div class="col-sm-8">
                                  
                                    <input type="checkbox" class="js-small f-right" value="1" name="rti_deadline" id="rti_deadline" <?php if(isset($client[0]['crm_rti_deadline']) && ($client[0]['crm_rti_deadline']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row radio_bts ">
                                 <label class="col-sm-4 col-form-label">Previous Year Requires</label>
                                 <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right fields" name="previous_year_require" id="previous_year_require" <?php if(isset($client[0]['crm_previous_year_require']) && ($client[0]['crm_previous_year_require']=='1') ){?> checked="checked"<?php } ?> data-id="preyear">
                                 </div>
                                 </div>
                                 <?php 
                                 (isset($client[0]['crm_previous_year_require']) && $client[0]['crm_previous_year_require'] == 'on') ? $preyear =  "block" : $preyear = 'none';


                                 ?>
                                 <div id="enable_preyear" style="display:<?php echo $preyear;?>;">
                                 <div class="form-group row name_fields">
                                    <label class="col-sm-4 col-form-label">If Yes</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right" value="1" name="payroll_if_yes" id="payroll_if_yes" <?php if(isset($client[0]['crm_payroll_if_yes']) && ($client[0]['crm_payroll_if_yes']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                 <label class="col-sm-4 col-form-label">PAYE Scheme Ceased</label>
                                 <div class="col-sm-8">
                                  
                                    <input type="checkbox" class="js-small f-right" value="1" name="confirmation_next_due" id="confirmation_next_due" <?php if(isset($client[0]['crm_paye_scheme_ceased']) && ($client[0]['crm_paye_scheme_ceased']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 </div>
                                 </div>
                                 </div>
                                 </div>
                                 <!-- accordion-panel -->
                                 <!-- accordion-panel -->
                                 <div class='worktab' style="<?php echo $worktab;?>">
                                 <div class="accordion-panel">
                                 <div class="box-division03">
                                 <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">WorkPlace Pension - AE          </a>
                                 </h3>
                                 </div>
                                 <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label">Staging Date</label>
                                    <div class="col-sm-8">
                                     
                                       <input type="checkbox" class="js-small f-right" value="1" name="staging_date" id="staging_date" <?php if(isset($client[0]['crm_staging_date']) && ($client[0]['crm_staging_date']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Pension ID </label>
                                    <div class="col-sm-8">
                                     
                                       <input type="checkbox" class="js-small f-right" value="1" name="staging_date" id="staging_date" <?php if(isset($client[0]['crm_pension_id']) && ($client[0]['crm_pension_id']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Pension Submission Due date</label>
                                    <div class="col-sm-8">
                                      
                                       <input type="checkbox" class="js-small f-right" value="1" name="pension_subm_due_date" id="pension_subm_due_date" <?php if(isset($client[0]['crm_pension_subm_due_date']) && ($client[0]['crm_pension_subm_due_date']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label">Defer/Postpone Upto</label>
                                    <div class="col-sm-8">
                                       
                                       <input type="checkbox" class="js-small f-right" value="1" name="defer_post_upto" id="defer_post_upto" <?php if(isset($client[0]['crm_postponement_date']) && ($client[0]['crm_postponement_date']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label">The Pensions Regulator Opt Out Date</label>
                                    <div class="col-sm-8">
                                    
                                       <input type="checkbox" class="js-small f-right" value="1" name="pension_regulator_date" id="pension_regulator_date" <?php if(isset($client[0]['crm_the_pensions_regulator_opt_out_date']) && ($client[0]['crm_the_pensions_regulator_opt_out_date']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label">Re-Enrolment Date</label>
                                    <div class="col-sm-8">
                                      
                                       <input type="checkbox" class="js-small f-right" value="1" name="paye_re_enrolment_date" id="paye_re_enrolment_date" <?php if(isset($client[0]['crm_paye_re_enrolment_date']) && ($client[0]['crm_paye_re_enrolment_date']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Declaration of Compliance Due</label>
                                    <div class="col-sm-8">
                                     
                                       <input type="checkbox" class="js-small f-right" value="1" name="datepicker11" id="datepicker11" <?php if(isset($client[0]['crm_declaration_of_compliance_due_date']) && ($client[0]['crm_declaration_of_compliance_due_date']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label">Declaration of Compliance last filed</label>
                                    <div class="col-sm-8">
                                     
                                       <input type="checkbox" class="js-small f-right" value="1" name="declaration_of_compliance_last_filed" id="declaration_of_compliance_last_filed" <?php if(isset($client[0]['crm_declaration_of_compliance_submission']) && ($client[0]['crm_declaration_of_compliance_submission']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Pension Provider Name</label>
                                    <div class="col-sm-8">
                                    
                                       <input type="checkbox" class="js-small f-right" value="1" name="paye_pension_provider" id="paye_pension_provider" <?php if(isset($client[0]['crm_paye_pension_provider']) && ($client[0]['crm_paye_pension_provider']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Pension Provider User ID</label>
                                    <div class="col-sm-8">
                                       
                                       <input type="checkbox" class="js-small f-right" value="1" name="paye_pension_provider_userid" id="paye_pension_provider_userid" <?php if(isset($client[0]['crm_pension_id']) && ($client[0]['crm_pension_id']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Employer Contribution Percentage</label>
                                    <div class="col-sm-8">
                                      
                                       <input type="checkbox" class="js-small f-right" value="1" name="employer_contri_percentage" id="employer_contri_percentage" <?php if(isset($client[0]['crm_employer_contri_percentage']) && ($client[0]['crm_employer_contri_percentage']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Employee Contribution Percentage</label>
                                    <div class="col-sm-8">
                                     
                                       <input type="checkbox" class="js-small f-right" value="1" name="employee_contri_percentage" id="employee_contri_percentage" <?php if(isset($client[0]['crm_employee_contri_percentage']) && ($client[0]['crm_employee_contri_percentage']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Other Notes</label>
                                    <div class="col-sm-8">
                                      
                                       <input type="checkbox" class="js-small f-right" value="1" name="pension_notes" id="pension_notes" <?php if(isset($client[0]['crm_pension_notes']) && ($client[0]['crm_pension_notes']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 </div>
                                 </div>
                                 </div>
                                 </div>
                                 <!-- accordion-panel -->
                                 <!-- accordion-panel -->
                                 </div>
                                 <!--worktab close-->
                                 <div class="contratab" style="<?php echo $contratab;?>">
                                 <div class="accordion-panel">
                                 <div class="box-division03">
                                 <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">CIS Contractor/Sub Contractor</a>
                                 </h3>
                                 </div>
                                 <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                 <div class="form-group row name_fields">
                                    <label class="col-sm-4 col-form-label">C.I.S Contractor</label>
                                    <div class="col-sm-8">
                                       <!--  <input type="checkbox" class="js-small f-right fields" name="cis_contractor" id="cis_contractor" <?php if(isset($client[0]['crm_cis_contractor']) && ($client[0]['crm_cis_contractor']=='on') ){?> checked="checked"<?php } ?>> -->
                                       <input type="checkbox" class="js-small f-right" value="1" name="cis_contractor" id="cis_contractor" <?php if(isset($client[0]['crm_cis_contractor']) && ($client[0]['crm_cis_contractor']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Start Date</label>
                                    <div class="col-sm-8">
                                       <!--  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
                                       <!--   <input class="form-control datepicker fields" type="text" name="cis_contractor_start_date" id="datepicker14" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_cis_contractor_start_date']) && ($client[0]['crm_cis_contractor_start_date']!='') ){ echo $client[0]['crm_cis_contractor_start_date'];}?>"/> -->
                                       <input type="checkbox" class="js-small f-right" value="1" name="cis_contractor_start_date" id="cis_contractor_start_date" <?php if(isset($client[0]['crm_cis_contractor_start_date']) && ($client[0]['crm_cis_contractor_start_date']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">C.I.S Scheme</label>
                                    <div class="col-sm-8">
                                       <!--  <textarea rows="3" placeholder="" name="cis_scheme_notes" id="cis_scheme_notes" class="fields"><?php if(isset($client[0]['crm_cis_scheme_notes']) && ($client[0]['crm_cis_scheme_notes']!='') ){ echo $client[0]['crm_cis_scheme_notes'];}?></textarea> -->
                                       <input type="checkbox" class="js-small f-right" value="1" name="cis_scheme_notes" id="cis_scheme_notes" <?php if(isset($client[0]['crm_cis_scheme_notes']) && ($client[0]['crm_cis_scheme_notes']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row name_fields">
                                    <label class="col-sm-4 col-form-label">CIS Subcontractor</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" name="cis_subcontractor" id="cis_subcontractor" class="js-small f-right fields" <?php if(isset($client[0]['crm_cis_subcontractor']) && ($client[0]['crm_cis_subcontractor']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Start Date</label>
                                    <div class="col-sm-8">
                                       <!--  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker fields" type="text" name="cis_subcontractor_start_date" id="datepicker15" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_cis_subcontractor_start_date']) && ($client[0]['crm_cis_subcontractor_start_date']!='') ){ echo $client[0]['crm_cis_subcontractor_start_date'];}?>"/> -->
                                       <input type="checkbox" class="js-small f-right" value="1" name="datepicker15" id="datepicker15" <?php if(isset($client[0]['crm_cis_subcontractor_start_date']) && ($client[0]['crm_cis_subcontractor_start_date']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">C.I.S Scheme</label>
                                    <div class="col-sm-8">
                                       <!--  <textarea rows="3" placeholder="" name="cis_subcontractor_scheme_notes" id="cis_subcontractor_scheme_notes" class="fields"><?php if(isset($client[0]['crm_cis_subcontractor_scheme_notes']) && ($client[0]['crm_cis_subcontractor_scheme_notes']!='') ){ echo $client[0]['crm_cis_subcontractor_scheme_notes'];}?></textarea>
                                          -->                                   
                                       <input type="checkbox" class="js-small f-right" value="1" name="cis_subcontractor_scheme_notes" id="cis_subcontractor_scheme_notes" <?php if(isset($client[0]['crm_cis_subcontractor_scheme_notes']) && ($client[0]['crm_cis_subcontractor_scheme_notes']=='1') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 </div>
                                 </div>
                                 </div>
                                 </div>
                                 <!-- accordion-panel -->
                                 <!-- accordion-panel -->
                                 </div>
                                 <!-- contratab close-->
                                 <div class="p11dtab" style="<?php echo $p11dtab?>">
                                 <div class="accordion-panel">
                                 <div class="box-division03">
                                 <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">P11D</a>
                                 </h3>
                                 </div>
                                 <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                 <div class="form-group row  date_birth">
                                 <label class="col-sm-4 col-form-label">P11D Start Date</label>
                                 <div class="col-sm-8">

                                 <input type="checkbox" class="js-small f-right" value="1" name="p11d_start_date" id="p11d_start_date" <?php if(isset($client[0]['crm_p11d_latest_action_date']) && ($client[0]['crm_p11d_latest_action_date']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                 <label class="col-sm-4 col-form-label">Number of P11D's to do</label>
                                 <div class="col-sm-8">

                                 <input type="checkbox" class="js-small f-right" value="1" name="p11d_todo" id="p11d_todo" <?php if(isset($client[0]['crm_p11d_latest_action']) && ($client[0]['crm_p11d_latest_action']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                 <label class="col-sm-4 col-form-label">First Benefit Pay Date</label>
                                 <div class="col-sm-8">

                                 <input type="checkbox" class="js-small f-right" value="1" name="p11d_first_benefit_date" id="p11d_first_benefit_date" <?php if(isset($client[0]['crm_latest_p11d_submitted']) && ($client[0]['crm_latest_p11d_submitted']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                 <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>P11D Due date</label>
                                 <div class="col-sm-8">

                                 <input type="checkbox" class="js-small f-right" value="1" name="p11d_due_date" id="p11d_due_date" <?php if(isset($client[0]['crm_next_p11d_return_due']) && ($client[0]['crm_next_p11d_return_due']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row radio_bts ">
                                 <label class="col-sm-4 col-form-label">Previous Year Requires</label>
                                 <div class="col-sm-8">
                                 <input type="checkbox" class="js-small f-right fields" name="p11d_previous_year_require" id="p11d_previous_year_require" <?php if(isset($client[0]['crm_p11d_previous_year_require']) && ($client[0]['crm_p11d_previous_year_require']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row name_fields">
                                 <label class="col-sm-4 col-form-label">If Yes</label>
                                 <div class="col-sm-8">

                                 <input type="checkbox" class="js-small f-right" value="1" name="p11d_payroll_if_yes" id="p11d_payroll_if_yes" <?php if(isset($client[0]['crm_p11d_payroll_if_yes']) && ($client[0]['crm_p11d_payroll_if_yes']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>
                                 <div class="form-group row  date_birth">
                                 <label class="col-sm-4 col-form-label">PAYE Scheme Ceased Date</label>
                                 <div class="col-sm-8">

                                 <input type="checkbox" class="js-small f-right" value="1" name="p11d_paye_scheme_ceased" id="p11d_paye_scheme_ceased" <?php if(isset($client[0]['crm_p11d_records_received']) && ($client[0]['crm_p11d_records_received']=='1') ){?> checked="checked"<?php } ?>>
                                 </div>
                                 </div>

                                 </div>
                                 </div>
                                 </div>
                                 <!-- accordion-panel -->
                                 <!-- accordion-panel -->
                                 </div>
                                 <!-- p11dtab close -->

                                 </div>
                                 </div>
                                 </div>
               <!-- Payroll -->
                                 <div id="vat-Returns" class="tab-pane fade">
                                 <div class="masonry-container floating_set">
                                 <div class="grid-sizer"></div>
                                 <div class="accordion-panel">
                                 <div class="box-division03">
                                    <div class="accordion-heading" role="tab" id="headingOne">
                                       <h3 class="card-title accordion-title">
                                          <a class="accordion-msg">Important Information</a>
                                       </h3>
                                    </div>
                                    <div id="collapse" class="panel-collapse">
                                       <div class="basic-info-client1">
                                          <div class="form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">VAT Number</label>
                                             <div class="col-sm-8">                                              
                                                   <input type="checkbox" class="js-small f-right" value="1" name="vat_number_one" id="vat_number_one" <?php if(isset($client[0]['crm_vat_number']) && ($client[0]['crm_vat_number']=='1') ){?> checked="checked"<?php } ?> >                            
                                             </div>
                                             
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </div>
                                 <!-- accordion-panel -->
                                 <div class="accordion-panel">
                                 <div class="box-division03">
                                    <div class="accordion-heading" role="tab" id="headingOne">
                                       <h3 class="card-title accordion-title">
                                          <a class="accordion-msg">VAT</a>
                                       </h3>
                                    </div>
                                    <div id="collapse" class="panel-collapse">
                                       <div class="basic-info-client1">
                                          <div class="form-group row  date_birth">
                                             <label class="col-sm-4 col-form-label">VAT Registration Date</label>
                                             <div class="col-sm-8">
                                              
                                                <input type="checkbox" class="js-small f-right" value="1" name="vat_registration_date" id="vat_registration_date" <?php if(isset($client[0]['crm_vat_date_of_registration']) && ($client[0]['crm_vat_date_of_registration']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <label class="col-sm-4 col-form-label">VAT Frequency</label>
                                             <div class="col-sm-8">
                                              
                                                <input type="checkbox" class="js-small f-right" value="1" name="vat_frequency" id="vat_frequency" <?php if(isset($client[0]['crm_vat_frequency']) && ($client[0]['crm_vat_frequency']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row help_icon date_birth">
                                             <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>VAT Quarter End Date</label>
                                             <div class="col-sm-8">
                                               
                                                <input type="checkbox" class="js-small f-right" value="1" name="vat_quater_end_date" id="vat_quater_end_date" <?php if(isset($client[0]['crm_vat_quater_end_date']) && ($client[0]['crm_vat_quater_end_date']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>VAT Quarters</label>
                                             <div class="col-sm-8">
                                               
                                                <input type="checkbox" class="js-small f-right" value="1" name="vat_quarters" id="vat_quarters" <?php if(isset($client[0]['crm_vat_quarters']) && ($client[0]['crm_vat_quarters']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row help_icon date_birth">
                                             <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>VAT Due Date</label>
                                             <div class="col-sm-8">
                                                <!--  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
                                                <!-- <input class="form-control datepicker fields" name="vat_due_date" id="datepicker24" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_vat_due_date']) && ($client[0]['crm_vat_due_date']!='') ){ echo $client[0]['crm_vat_due_date'];}?>"/> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="vat_due_date" id="vat_due_date" <?php if(isset($client[0]['crm_vat_due_date']) && ($client[0]['crm_vat_due_date']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <label class="col-sm-4 col-form-label">VAT Scheme</label>
                                             <div class="col-sm-8">
                                                <!--  <select name="vat_scheme" id="vat_scheme" class="form-control fields">
                                                   <option value="">--Select--</option>
                                                   <option value="Standard Rate" <?php if(isset($client[0]['crm_vat_scheme']) && $client[0]['crm_vat_scheme']=='Standard Rate') {?> selected="selected"<?php } ?>>Standard Rate</option>
                                                   <option value="Flat Rate Scheme" <?php if(isset($client[0]['crm_vat_scheme']) && $client[0]['crm_vat_scheme']=='Flat Rate Scheme') {?> selected="selected"<?php } ?>>Flat Rate Scheme</option>
                                                   <option value="Marginal Scheme" <?php if(isset($client[0]['crm_vat_scheme']) && $client[0]['crm_vat_scheme']=='Marginal Scheme') {?> selected="selected"<?php } ?>>Marginal Scheme</option>
                                                   </select> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="vat_scheme" id="vat_scheme" <?php if(isset($client[0]['crm_vat_scheme']) && ($client[0]['crm_vat_scheme']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <label class="col-sm-4 col-form-label">Flat Rate Category</label>
                                             <div class="col-sm-8">
                                                <!--  <input type="text" name="flat_rate_category" id="flat_rate_category" placeholder="" value="<?php if(isset($client[0]['crm_flat_rate_category']) && ($client[0]['crm_flat_rate_category']!='') ){ echo $client[0]['crm_flat_rate_category'];}?>" class="fields"> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="flat_rate_category" id="flat_rate_category" <?php if(isset($client[0]['crm_flat_rate_category']) && ($client[0]['crm_flat_rate_category']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <label class="col-sm-4 col-form-label">Flat Rate Percentage</label>
                                             <div class="col-sm-8">
                                                <!--  <input type="number" name="flat_rate_percentage" id="flat_rate_percentage" placeholder="" value="<?php if(isset($client[0]['crm_flat_rate_percentage']) && ($client[0]['crm_flat_rate_percentage']!='') ){ echo $client[0]['crm_flat_rate_percentage'];}?>" class="fields"> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="flat_rate_percentage" id="flat_rate_percentage" <?php if(isset($client[0]['crm_flat_rate_percentage']) && ($client[0]['crm_flat_rate_percentage']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Direct Debit with HMRC</label>
                                             <div class="col-sm-8">
                                                <input type="checkbox" class="js-small f-right fields" name="direct_debit" id="direct_debit" <?php if(isset($client[0]['crm_direct_debit']) && ($client[0]['crm_direct_debit']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Annual Accounting Scheme</label>
                                             <div class="col-sm-8">
                                                <input type="checkbox" class="js-small f-right fields" name="annual_accounting_scheme" id="annual_accounting_scheme" <?php if(isset($client[0]['crm_annual_accounting_scheme']) && ($client[0]['crm_annual_accounting_scheme']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <label class="col-sm-4 col-form-label">Box 5 Figure of Last Quarter</label>
                                             <div class="col-sm-8">                                 
                                                <input type="checkbox" class="js-small f-right" value="1" name="box5_of_last_quarter_submitted" id="box5_of_last_quarter_submitted" <?php if(isset($client[0]['crm_box5_of_last_quarter_submitted']) && ($client[0]['crm_box5_of_last_quarter_submitted']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <label class="col-sm-4 col-form-label">VAT Address</label>
                                             <div class="col-sm-8">
                                                  <input type="checkbox" class="js-small f-right" value="1" name="vat_address" id="vat_address" <?php if(isset($client[0]['crm_vat_address']) && ($client[0]['crm_vat_address']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </div>
                                 <!-- accordion-panel -->
                                 <!-- accordion-panel -->
                                 <div class="accordion-panel">
                                 <div class="box-division03">
                                    <div class="accordion-heading" role="tab" id="headingOne">
                                       <h3 class="card-title accordion-title">
                                          <a class="accordion-msg">Notes if any other</a>
                                       </h3>
                                    </div>
                                    <div id="collapse" class="panel-collapse">
                                       <div class="basic-info-client1">
                                          <div class="form-group row">
                                             <label class="col-sm-4 col-form-label">Notes</label>
                                             <div class="col-sm-8">
                                               
                                                <input type="checkbox" class="js-small f-right" value="1" name="vat_notes" id="vat_notes" <?php if(isset($client[0]['crm_vat_notes']) && ($client[0]['crm_vat_notes']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <?php
                                             //echo render_custom_fields_one( 'vat',$rel_id);  ?>
                                       </div>
                                    </div>
                                 </div>
                                 </div>
                                 <!-- accordion-panel -->
                                 </div>
                                 </div>
               <!-- VAT Returns -->
                              <div id="management-account" class="tab-pane fade">
                              <div class="masonry-container floating_set">
                              <div class="grid-sizer"></div>
                                 <div class="bookkeeptab" style="">
                                    <div class="accordion-panel">
                                       <div class="box-division03">
                                          <div class="accordion-heading" role="tab" id="headingOne">
                                             <h3 class="card-title accordion-title">
                                                <a class="accordion-msg">Bookkeeping</a>
                                             </h3>
                                          </div>
                                          <div id="collapse" class="panel-collapse">
                                             <div class="basic-info-client1">
                                                <div class="form-group row">
                                                   <label class="col-sm-4 col-form-label">Bookkeepeing to Done</label>
                                                   <div class="col-sm-8">                                    
                                                      <input type="checkbox" class="js-small f-right" value="1" name="bookkeeping" id="bookkeeping" <?php if(isset($client[0]['crm_bookkeeping']) && ($client[0]['crm_bookkeeping']=='1') ){?> checked="checked"<?php } ?>>
                                                   </div>
                                                </div>
                                                <div class="form-group row help_icon date_birth">
                                                   <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Next Bookkeeping Date</label>
                                                   <div class="col-sm-8">
                                                      <!--      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
                                                      <!--  <input class="form-control datepicker fields" name="next_booking_date" id="datepicker26" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_next_booking_date']) && ($client[0]['crm_next_booking_date']!='') ){ echo $client[0]['crm_next_booking_date'];}?>"/> -->
                                                      <input type="checkbox" class="js-small f-right" value="1" name="next_booking_date" id="next_booking_date" <?php if(isset($client[0]['crm_next_booking_date']) && ($client[0]['crm_next_booking_date']=='1') ){?> checked="checked"<?php } ?>>
                                                   </div>
                                                </div>
                                                <div class="form-group row">
                                                   <label class="col-sm-4 col-form-label">Method of Bookkeeping</label>
                                                   <div class="col-sm-8">
                                                      <!--  <select name="method_bookkeeping" id="method_bookkeeping" class="form-control fields">
                                                         <option value="">--Select--</option>
                                                         <option value="Excel" <?php if(isset($client[0]['crm_method_bookkeeping']) && $client[0]['crm_method_bookkeeping']=='Excel') {?> selected="selected"<?php } ?>>Excel</option>
                                                         <option value="Software" <?php if(isset($client[0]['crm_method_bookkeeping']) && $client[0]['crm_method_bookkeeping']=='Software') {?> selected="selected"<?php } ?>>Software</option>
                                                         </select> -->
                                                      <input type="checkbox" class="js-small f-right" value="1" name="method_bookkeeping" id="method_bookkeeping" <?php if(isset($client[0]['crm_method_bookkeeping']) && ($client[0]['crm_method_bookkeeping']=='1') ){?> checked="checked"<?php } ?>>
                                                   </div>
                                                </div>
                                                <div class="form-group row">
                                                   <label class="col-sm-4 col-form-label">How Client will provide records</label>
                                                   <div class="col-sm-8">
                                                      <!-- <select name="client_provide_record" id="client_provide_record" class="form-control fields">
                                                         <option value="">--Select--</option>
                                                         <option value="Email/DropBox" <?php if(isset($client[0]['crm_client_provide_record']) && $client[0]['crm_client_provide_record']=='Email/DropBox') {?> selected="selected"<?php } ?>>Email/DropBox</option>
                                                         <option value="Google/Online Portal" <?php if(isset($client[0]['crm_client_provide_record']) && $client[0]['crm_client_provide_record']=='Google/Online Portal') {?> selected="selected"<?php } ?>>Google/Online Portal</option>
                                                         </select> -->
                                                      <input type="checkbox" class="js-small f-right" value="1" name="client_provide_record" id="client_provide_record" <?php if(isset($client[0]['crm_client_provide_record']) && ($client[0]['crm_client_provide_record']=='1') ){?> checked="checked"<?php } ?>>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- accordion-panel -->
                                    <!-- accordion-panel -->
                                 </div>
                                 <!-- bookkeep tab close-->
                                 <div class="accordion-panel">
                                    <div class="box-division03">
                                       <div class="accordion-heading" role="tab" id="headingOne">
                                          <h3 class="card-title accordion-title">
                                             <a class="accordion-msg">Management Accounts</a>
                                          </h3>
                                       </div>
                                       <div id="collapse" class="panel-collapse">
                                          <div class="basic-info-client1">
                                             <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Management Accounts Frequency</label>
                                                <div class="col-sm-8">
                                                   <!--   <select name="manage_acc_fre" id="manage_acc_fre" class="form-control fields">
                                                      <option value="">--Select--</option>
                                                      <option value="Weekly" <?php if(isset($client[0]['crm_manage_acc_fre']) && $client[0]['crm_manage_acc_fre']=='Weekly') {?> selected="selected"<?php } ?>>Weekly</option>
                                                      <option value="Monthly" <?php if(isset($client[0]['crm_manage_acc_fre']) && $client[0]['crm_manage_acc_fre']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                                                      <option value="Quarterly" <?php if(isset($client[0]['crm_manage_acc_fre']) && $client[0]['crm_manage_acc_fre']=='Quarterly') {?> selected="selected"<?php } ?>>Quarterly</option>
                                                      <option value="Annually" <?php if(isset($client[0]['crm_manage_acc_fre']) && $client[0]['crm_manage_acc_fre']=='Annually') {?> selected="selected"<?php } ?>>Annually</option>
                                                      </select> -->
                                                   <input type="checkbox" class="js-small f-right" value="1" name="manage_acc_fre" id="manage_acc_fre" <?php if(isset($client[0]['crm_manage_acc_fre']) && ($client[0]['crm_manage_acc_fre']=='1') ){?> checked="checked"<?php } ?>>
                                                </div>
                                             </div>
                                             <div class="form-group row help_icon date_birth">
                                                <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Next Management Accounts Due date</label>
                                                <div class="col-sm-8">
                                                   <!--  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input class="form-control datepicker fields" name="next_manage_acc_date" id="datepicker28" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_next_manage_acc_date']) && ($client[0]['crm_next_manage_acc_date']!='') ){ echo $client[0]['crm_next_manage_acc_date'];}?>"/> -->
                                                   <input type="checkbox" class="js-small f-right" value="1" name="next_manage_acc_date" id="next_manage_acc_date" <?php if(isset($client[0]['crm_next_manage_acc_date']) && ($client[0]['crm_next_manage_acc_date']=='1') ){?> checked="checked"<?php } ?>>
                                                </div>
                                             </div>
                                             <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Method of Bookkeeping</label>
                                                <div class="col-sm-8">
                                                   <!--  <select name="manage_method_bookkeeping" id="manage_method_bookkeeping" class="form-control fields">
                                                      <option value="">--Select--</option>
                                                      <option value="Excel" <?php if(isset($client[0]['crm_manage_method_bookkeeping']) && $client[0]['crm_manage_method_bookkeeping']=='Excel') {?> selected="selected"<?php } ?>>Excel</option>
                                                      <option value="Software" <?php if(isset($client[0]['crm_manage_method_bookkeeping']) && $client[0]['crm_manage_method_bookkeeping']=='Software') {?> selected="selected"<?php } ?>>Software</option>
                                                      </select> -->
                                                   <input type="checkbox" class="js-small f-right" value="1" name="manage_method_bookkeeping" id="manage_method_bookkeeping" <?php if(isset($client[0]['crm_manage_method_bookkeeping']) && ($client[0]['crm_manage_method_bookkeeping']=='1') ){?> checked="checked"<?php } ?>>
                                                </div>
                                             </div>
                                             <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">How Client will provide records</label>
                                                <div class="col-sm-8">
                                                   <!--  <select name="manage_client_provide_record" id="manage_client_provide_record" class="form-control fields">
                                                      <option value="">--Select--</option>
                                                      <option value="Email/DropBox" <?php if(isset($client[0]['crm_manage_client_provide_record']) && $client[0]['crm_manage_client_provide_record']=='Email/DropBox') {?> selected="selected"<?php } ?>>Email/DropBox</option>
                                                      <option value="Google/Online Portal" <?php if(isset($client[0]['crm_manage_client_provide_record']) && $client[0]['crm_manage_client_provide_record']=='Google/Online Portal') {?> selected="selected"<?php } ?>>Google/Online Portal</option>
                                                      </select> -->
                                                   <input type="checkbox" class="js-small f-right" value="1" name="manage_client_provide_record" id="manage_client_provide_record" <?php if(isset($client[0]['crm_manage_client_provide_record']) && ($client[0]['crm_manage_client_provide_record']=='1') ){?> checked="checked"<?php } ?>>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- accordion-panel -->
                             
                                 <!-- accordion-panel -->
                              </div>
                           </div>
               <!-- Management Accounts --> 
            
                           <div id="investigation-insurance" class="tab-pane fade">
                        <div class="masonry-container floating_set">
                           <div class="grid-sizer"></div>
                              <div class="accordion-panel">
                                 <div class="box-division03">
                                    <div class="accordion-heading" role="tab" id="headingOne">
                                       <h3 class="card-title accordion-title">
                                          <a class="accordion-msg">Investigation Insurance</a>
                                       </h3>
                                    </div>
                                    <div id="collapse" class="panel-collapse">
                                       <div class="basic-info-client1">
                                          <div class="form-group row help_icon date_birth">
                                             <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Insurance Start Date</label>
                                             <div class="col-sm-8">
                                                <!--   <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
                                                <!--     <input class="form-control datepicker fields" name="insurance_start_date" id="datepicker30" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_invesitgation_insurance']) && ($client[0]['crm_invesitgation_insurance']!='') ){ echo $client[0]['crm_invesitgation_insurance'];}?>"/> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="insurance_start_date" id="insurance_start_date" <?php if(isset($client[0]['crm_invesitgation_insurance']) && ($client[0]['crm_invesitgation_insurance']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row help_icon date_birth">
                                             <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Insurance Renew Date</label>
                                             <div class="col-sm-8">
                                                <!--  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
                                                <!-- <input class="form-control datepicker fields" name="insurance_renew_date" id="datepicker31" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_insurance_renew_date']) && ($client[0]['crm_insurance_renew_date']!='') ){ echo $client[0]['crm_insurance_renew_date'];}?>"/> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="insurance_renew_date" id="insurance_renew_date" <?php if(isset($client[0]['crm_insurance_renew_date']) && ($client[0]['crm_insurance_renew_date']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <label class="col-sm-4 col-form-label">Insurance Provider</label>
                                             <div class="col-sm-8">
                                                <!--  <select name="insurance_provider" id="insurance_provider" class="form-control fields">
                                                   <option value="">--Select--</option>
                                                   <option value="Excel" <?php if(isset($client[0]['crm_insurance_provider']) && $client[0]['crm_insurance_provider']=='Excel') {?> selected="selected"<?php } ?>>Excel</option>
                                                   <option value="Software" <?php if(isset($client[0]['crm_insurance_provider']) && $client[0]['crm_insurance_provider']=='Software') {?> selected="selected"<?php } ?>>Software</option>
                                                   </select> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="insurance_provider" id="insurance_provider" <?php if(isset($client[0]['crm_insurance_provider']) && ($client[0]['crm_insurance_provider']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <label class="col-sm-4 col-form-label">History of Previous Claims</label>
                                             <div class="col-sm-8">
                                                <!--   <textarea rows="3" placeholder="" name="claims_note" id="claims_note" class="fields"><?php if(isset($client[0]['crm_claims_note']) && ($client[0]['crm_claims_note']!='') ){ echo $client[0]['crm_claims_note'];}?></textarea> -->
                                                <input type="checkbox" class="js-small f-right" value="1" name="claims_note" id="claims_note" <?php if(isset($client[0]['crm_claims_note']) && ($client[0]['crm_claims_note']=='1') ){?> checked="checked"<?php } ?>>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- accordion-panel -->
                              <!-- accordion-panel -->
                              <div class="registeredtab" style="<?php echo $registeredtab;?>">
                                 <div class="accordion-panel">
                                    <div class="box-division03">
                                       <div class="accordion-heading" role="tab" id="headingOne">
                                          <h3 class="card-title accordion-title">
                                             <a class="accordion-msg">Registered Office</a>
                                          </h3>
                                       </div>
                                       <div id="collapse" class="panel-collapse">
                                          <div class="basic-info-client1">
                                             <div class="form-group row help_icon date_birth">
                                                <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Registered Office Start Date</label>
                                                <div class="col-sm-8">
                                                   <!--  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input class="form-control datepicker fields" name="registered_start_date" id="datepicker33" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_registered_start_date']) && ($client[0]['crm_registered_start_date']!='') ){ echo $client[0]['crm_registered_start_date'];}?>"/> -->
                                                   <input type="checkbox" class="js-small f-right" value="1" name="registered_start_date" id="registered_start_date" <?php if(isset($client[0]['crm_registered_start_date']) && ($client[0]['crm_registered_start_date']=='1') ){?> checked="checked"<?php } ?>>
                                                </div>
                                             </div>
                                             <div class="form-group row help_icon date_birth">
                                                <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Registered Office Renew Date</label>
                                                <div class="col-sm-8">
                                                   <!--  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input class="form-control datepicker fields" name="registered_renew_date" id="datepicker34" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_registered_renew_date']) && ($client[0]['crm_registered_renew_date']!='') ){ echo $client[0]['crm_registered_renew_date'];}?>"/> -->
                                                   <input type="checkbox" class="js-small f-right" value="1" name="registered_renew_date" id="registered_renew_date" <?php if(isset($client[0]['crm_registered_renew_date']) && ($client[0]['crm_registered_renew_date']=='1') ){?> checked="checked"<?php } ?>>
                                                </div>
                                             </div>
                                             <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Registered office in Use</label>
                                                <div class="col-sm-8">
                                                   <!--  <input type="text" name="registered_office_inuse" id="registered_office_inuse" placeholder="" value="<?php if(isset($client[0]['crm_registered_office_inuse']) && ($client[0]['crm_registered_office_inuse']!='') ){ echo $client[0]['crm_registered_office_inuse'];}?>" class="fields"> -->
                                                   <input type="checkbox" class="js-small f-right" value="1" name="registered_office_inuse" id="registered_office_inuse" <?php if(isset($client[0]['crm_registered_office_inuse']) && ($client[0]['crm_registered_office_inuse']=='1') ){?> checked="checked"<?php } ?>>
                                                </div>
                                             </div>
                                             <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">History of Previous Claims</label>
                                                <div class="col-sm-8">
                                                   <!--  <textarea rows="3" placeholder="" name="registered_claims_note" id="registered_claims_note" class="fields"><?php if(isset($client[0]['crm_registered_claims_note']) && ($client[0]['crm_registered_claims_note']!='') ){ echo $client[0]['crm_registered_claims_note'];}?></textarea> -->
                                                   <input type="checkbox" class="js-small f-right" value="1" name="registered_claims_note" id="registered_claims_note" <?php if(isset($client[0]['crm_registered_claims_note']) && ($client[0]['crm_registered_claims_note']=='1') ){?> checked="checked"<?php } ?>>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- accordion-panel -->
                                 <!-- accordion-panel -->
                              </div>
                              <!-- registeredtab close-->
                              <div class="taxadvicetab" style="<?php echo $taxadvicetab;?>">
                                 <div class="accordion-panel">
                                    <div class="box-division03">
                                       <div class="accordion-heading" role="tab" id="headingOne">
                                          <h3 class="card-title accordion-title">
                                             <a class="accordion-msg">Tax Advice/Investigation</a>
                                          </h3>
                                       </div>
                                       <div id="collapse" class="panel-collapse">
                                          <div class="basic-info-client1">
                                             <div class="form-group row help_icon date_birth">
                                                <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Start Date</label>
                                                <div class="col-sm-8">
                                                   <!--   <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input class="form-control datepicker fields" name="investigation_start_date" id="datepicker36" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_investigation_start_date']) && ($client[0]['crm_investigation_start_date']!='') ){ echo $client[0]['crm_investigation_start_date'];}?>"/> -->
                                                   <input type="checkbox" class="js-small f-right" value="1" name="investigation_start_date" id="confirmation_next_due" <?php if(isset($client[0]['crm_investigation_start_date']) && ($client[0]['crm_investigation_start_date']=='1') ){?> checked="checked"<?php } ?>>
                                                </div>
                                             </div>
                                             <div class="form-group row help_icon date_birth">
                                                <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>End Date</label>
                                                <div class="col-sm-8">
                                                   <!--  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input class="form-control datepicker fields" name="investigation_end_date" id="datepicker37" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_investigation_end_date']) && ($client[0]['crm_investigation_end_date']!='') ){ echo $client[0]['crm_investigation_end_date'];}?>"/> -->
                                                   <input type="checkbox" class="js-small f-right" value="1" name="investigation_end_date" id="investigation_end_date" <?php if(isset($client[0]['crm_investigation_end_date']) && ($client[0]['crm_investigation_end_date']=='1') ){?> checked="checked"<?php } ?>>
                                                </div>
                                             </div>
                                             <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Notes</label>
                                                <div class="col-sm-8">
                                                   <!-- textarea rows="3" placeholder="" name="investigation_note" id="investigation_note" class="fields"><?php if(isset($client[0]['crm_investigation_note']) && ($client[0]['crm_investigation_note']!='') ){ echo $client[0]['crm_investigation_note'];}?></textarea> -->
                                                   <input type="checkbox" class="js-small f-right" value="1" name="investigation_note" id="investigation_note" <?php if(isset($client[0]['crm_investigation_note']) && ($client[0]['crm_investigation_note']=='1') ){?> checked="checked"<?php } ?>>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- accordion-panel -->
                                 <!-- accordion-panel -->
                              </div>
                            
                           </div>
                        </div>
               <!-- Investigation Insurance --> 

               <!-- Notes tab close -->
                     <div id="documents" class="tab-pane fade">
                        <div class="accordion-panel">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Documents</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                    <div class="form-group row">
                                       <label class="col-sm-4 col-form-label">upload Document</label>
                                       <div class="col-sm-8">
                                          <input type="file" name="document" id="document" class="fields">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                     </div>
               <!-- documents tab close -->
                        <div id="task" class="tab-pane fade">
                           <div class="all_user-section2 floating_set">
                              <div class="all-usera1 user-dashboard-section1">
                                 <div class="client_section3 table-responsive ">
                                    <table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
                                       <thead>
                                          <tr class="text-uppercase">
                                             <th>
                                                <div class="checkbox-fade fade-in-primary">
                                                   <label>
                                                   <input type="checkbox"  id="bulkDelete"  />
                                                   <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                                   </label>
                                                </div>
                                             </th>
                                             <th>Timer</th>
                                             <th>Task Name</th>
                                             <!-- <th>CRM-Username</th> -->
                                             <th>Start Date</th>
                                             <th>Due Date</th>
                                             <th>Status</th>
                                             <th>Priority</th>
                                             <th>Tag</th>
                                             <th>Assignto</th>
                                             <th>Actions</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php foreach ($task_list as $key => $value) {
                                             if($value['worker']=='')
                                             {
                                             $value['worker'] = 0;
                                             }
                                             $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
                                             if($value['task_status']=='notstarted')
                                             {
                                             $stat = 'Not Started';
                                             }else if($value['task_status']=='inprogress')
                                             {
                                             $stat = 'In Progress';
                                             }else if($value['task_status']=='awaiting')
                                             {
                                             $stat = 'Awaiting for a feedback';
                                             }else if($value['task_status']=='testing')
                                             {
                                             $stat = 'Testing';
                                             }else if($value['task_status']='complete')
                                             {
                                             $stat = 'Complete';
                                             }
                                             $exp_tag = explode(',', $value['tag']);
                                             
                                             ?>
                                          <tr id="<?php echo $value['id']; ?>">
                                             <td>
                                                <div class="checkbox-fade fade-in-primary">
                                                   <label>
                                                   <input type='checkbox'  class='deleteRow' value="<?php echo $value['id'];?>"  />
                                                   <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                                   </label>
                                                </div>
                                             </td>
                                             <td><?php echo date('Y-m-d H:i:s', $value['created_date']);?></td>
                                             <td><?php echo ucfirst($value['subject']);?></td>
                                             <td><?php echo $value['start_date'];?></td>
                                             <td><?php echo $value['end_date'];?></td>
                                             <td><?php echo $stat;?></td>
                                             <td>
                                                <?php echo $value['priority'];?>
                                             </td>
                                             <td>
                                                <?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                                                   echo $tagName = $this->Common_mdl->getTag($exp_tag_value).' ';
                                                   }?>
                                             </td>
                                             <td class="user_imgs">
                                                <?php
                                                   foreach($staff as $key => $val){     
                                                   ?>
                                                <img src="<?php echo base_url().'uploads/'.$val['profile'];?>" alt="img">
                                                <?php } ?>
                                             </td>
                                             <td>
                                                <p class="action_01">
                                                   <?php if($role!='Staff'){?>   
                                                   <a href="<?php echo base_url().'user/delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                                   <?php } ?>
                                                   <a href="<?php echo base_url().'user/update_task/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                                   <!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                                </p>
                                             </td>
                                          </tr>
                                          <?php } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
               <!-- task tab tab close -->
            </div>
            <!-- tabcontent -->
            </div>
			</div>
         </div>
                  <div class="floation_set text-right accordion_ups">
                     <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields">
                     <input type="hidden" name="client_id" id="client_id" value="<?php if(isset($user[0]['client_id']) && ($user[0]['client_id']!='') ){ echo $user[0]['client_id'];}?>" class="fields">
                     <input type="hidden" id="company_house" name="company_house" value="<?php if(isset($user[0]['company_roles']) && ($user[0]['company_roles']!='') ){ echo $user[0]['company_roles'];}?>" class="fields">
                     <!-- <input type="submit" class="add_acc_client" value="Save & Exit" id="submit"/> -->
                  </div>
      </div>
      <!-- information-tab -->
      </div>
     

   </form>
  

      
    </div>

   </div>
   </div>            
         
</section>
<!-- client-details-view -->
<?php  
   foreach ($contactRec as $contactRec_key => $contactRec_value) { 
   $id   = $contactRec_value['id']; 
      ?>
<div id="modalcontact<?php echo $id;?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align-last: center">contact Delete</h4>
         </div>
         <div class="modal-body">
            <p>Are you sure want to delete?</p>
         </div>
         <div class="modal-footer">
            <a href="#" class="delcontact" data-value="<?php echo $id;?>">Delete</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<?php  } ?>
<!--modal 2-->
<div class="modal fade all_layout_modals" id="exist_person" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Select Existing Person</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="main_contacts" id="main_contacts" style="display:block">
               <?php 
                  echo '<h3>'.$company['company_name'].'</h3>';
                  
                  $array = array('First','Second','Third','Fourth','Fifth','Sixth','Seventh','Eight','Nineth','Tenth');
                  $i=0;
                  foreach ($officers as $key => $value) {
                  
                  
                  
                  if($value['officer_role'] == 'director' && !isset($value['resigned_on'])) {
                  $dateofbirth=(isset($value['date_of_birth']['year'])&&$value['date_of_birth']['year']!='') ? $value['date_of_birth']['year'] : '';
                  $month=(isset($value['date_of_birth']['month'])&&$value['date_of_birth']['month']!='') ? date("F", mktime(0, 0, 0, $value['date_of_birth']['month'], 10)) : '';
                  $concat=$dateofbirth.' '.$month;
                  ?>
               <div class="trading_tables">
                  <div class="trading_rate">
                     <p>
                        <span><strong><?php echo $value['name'];?></strong></span>
                        <span><?php echo 'Born'.$dateofbirth;?></span>
                        <span class="fields" id="usecontact<?php echo $array[$i];?>"><a href="#" onclick="return getmaincontact(<?php echo "'".$company['company_number']."'".','."'".$value['links']['officer']['appointments']."'".','."'".$array[$i]."'".','."'".$concat."'"?>);">Use as Contact</a></span>
                     </p>
                  </div>
               </div>
               <?php 
                  $i++;
                  }
                  
                  }   
                  ?>
            </div>
         </div>
         <!-- modalbody -->
      </div>
   </div>
</div>
<!-- modal-close -->
<!-- modal 2-->
<!-- new person add contact delete modal popup-->
<div id="modalnewperson" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align-last: center">contact Delete</h4>
         </div>
         <div class="modal-body">
            <p>Are you sure want to delete?</p>
         </div>
         <div class="modal-footer">
            <a href="#" class="delnewperson" data-value="">Delete</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!--modal close-->
<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <link rel="stylesheet" href="https:/resources/demos/style.css"> -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/client_page.js"></script>
<script>
   $( function() {
   /*  $( "#datepicker1,#datepicker2,#datepicker3,#datepicker4,#datepicker5,#datepicker6,#datepicker7,#datepicker8,#datepicker9,#datepicker10,#datepicker11,#datepicker12,#datepicker13,#datepicker14,#datepicker15,#datepicker16,#datepicker17,#datepicker18,#datepicker19,#datepicker20,#datepicker21,#datepicker22,#datepicker23,#datepicker24,#datepicker25,#datepicker26,#datepicker27,#datepicker28,#datepicker29,#datepicker30,#datepicker31,#datepicker32,#datepicker33,#datepicker34,#datepicker35,#datepicker36,#datepicker37,#datepicker38,#datepicker39,#letter_sign,#confirmation_next_reminder,#accounts_next_reminder_date,#personal_next_reminder_date,#datepicker,.datepicker,#personal_tax_return_date,#p11d_due_date").datepicker({ dateFormat: "yyyy-mm-dd" });*/
   $(".datepicker").datepicker({dateFormat: 'd MM, y'});
   } );
</script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
        function arr(name){
       var values = $("input[name='"+name+"[]']").map(function(){return $(this).val();}).get();
       return values;
      }
   
   
     function contact_add(id){
    var incre=$('#incre').val();
    //alert(incre);
    
          //var fields = $( "#contact_info" ).serializeArray();
   
   //alert(getFormData('contact_info'));
   
      //$(".contact_add").click(function(){
       //alert('contact_add');
   
         var data={};
   
         var title=[];
   $("select[name=title]").each(function(){
       title.push($(this).val());
   }); 
   
         //data['title'] = arr('title'); 
         data['title'] = title; 
   
         //$("input[name='title[]']").map(function(){return $(this).val();}).get();
         data['first_name'] = arr('first_name');
         data['middle_name'] = arr('middle_name');
         data['last_name'] = arr('last_name');
         data['surname'] = arr('surname');
         data['preferred_name'] = arr('preferred_name');
         data['mobile'] = arr('mobile');
         data['main_email'] = arr('main_email');
         //data['work_email'] = arr('work_email');
         data['nationality'] = arr('nationality');
         data['psc'] = arr('psc');
         //data['shareholder'] = $("input[name='shareholder[]']:selec"). val();
         var usertype=[];
   $("select[name=shareholder]").each(function(){
       usertype.push($(this).val());
   }); 
   
       var marital=[];
   $("select[name=marital_status]").each(function(){
       marital.push($(this).val());
   }); 
   
      var contacttype=[];
   $("select[name=contact_type]").each(function(){
       contacttype.push($(this).val());
   }); 
         data['shareholder'] = usertype;
         data['ni_number'] = arr('ni_number');
         data['content_type'] = arr('content_type');
         data['address_line1'] = arr('address_line1');
         data['address_line2'] = arr('address_line2');
         data['town_city'] = arr('town_city');
         data['post_code'] = arr('post_code');
         for (var i = 1; i <=incre; i++) {
      
         data['landline'+i] = arr('landline'+i);
         data['work_email'+i] = arr('work_email'+i);
     }
         data['date_of_birth'] = arr('date_of_birth');
         data['nature_of_control'] = arr('nature_of_control');
         data['marital_status'] = marital;
         data['contact_type'] = contacttype;
         data['utr_number'] = arr('utr_number');
         data['occupation'] = arr('occupation');
         data['appointed_on'] = arr('appointed_on');
         data['country_of_residence'] = arr('country_of_residence');
         data['make_primary'] = arr('make_primary');
         data['other_custom'] = arr('other_custom');
         data['client_id'] = id;
   
        var cnt = $("#append_cnt").val();
   
           if(cnt==''){
               cnt = 1;
           }else{
               cnt = parseInt(cnt)+1;
           }
           
           $("#append_cnt").val(cnt);
   
   
   
         data['cnt'] =cnt;
         $.ajax({
         url: '<?php echo base_url();?>client/update_client_contacts/',
         type : 'POST',
         data : data,
         success: function(data) {
   //alert('ggg');
            //$(".LoadingImage").hide();
            // location.reload();
                                             add_assignto(id);
   
   
         },
         });
      //});
   }
   
   function add_assignto(clientId)
   {
      var data = {};
   
   
             var team=[];
   $("select[name=team]").each(function(){
       team.push($(this).val());
   }); 
      data['team'] = team;
   
       data['allocation_holder'] = arr('allocation_holder');
       data['clientId'] = clientId;
       $.ajax({
         url: '<?php echo base_url();?>client/add_assignto/',
       type: "POST",
       data: data,
       success: function(data)  
       {
            add_responsibleuser(clientId);
       }
     });
   }
   
   function add_responsibleuser(clientId)
   {
      var data = {};
      /*data['assign_managed'] = arr('assign_managed');
       data['manager_reviewer'] = arr('manager_reviewer');
   */
           var assign_managed=[];
   $("select[name=assign_managed]").each(function(){
       assign_managed.push($(this).val());
   }); 
   
       var manager_reviewer=[];
   $("select[name=manager_reviewer]").each(function(){
       manager_reviewer.push($(this).val());
   }); 
   
      data['assign_managed'] = assign_managed;
       data['manager_reviewer'] = manager_reviewer;
   
       data['clientId'] = clientId;
       $.ajax({
         url: '<?php echo base_url();?>client/add_responsibleuser/',
       type: "POST",
       data: data,
       success: function(data)  
       {
         add_assigntodepart(clientId);
          //location.reload();  
         // window.location.href="<?php echo base_url().'user'?>"
       }
     });
   }
   
   function add_assigntodepart(clientId)
   {
      var data = {};
   
   
       var depart=[];
   $("select[name=depart]").each(function(){
       depart.push($(this).val());
   }); 
      data['depart'] = depart;
       data['allocation_holder'] = arr('allocation_holder_dept');
       data['clientId'] = clientId;
       $.ajax({
         url: '<?php echo base_url();?>client/add_assigntodepart/',
       type: "POST",
       data: data,
       success: function(data)  
       {
            add_responsiblemember(clientId);
       }
     });
   }
   
   function add_responsiblemember(clientId)
   {
      var data = {};
      /*data['assign_managed'] = arr('assign_managed');
       data['manager_reviewer'] = arr('manager_reviewer');
   */
           /*var assign_managed_member=[];
   $("select[name=assign_managed_member]").each(function(){
       assign_managed_member.push($(this).val());
   });*/ 
   
       var manager_reviewer_member=[];
   $("select[name=manager_reviewer_member]").each(function(){
       manager_reviewer_member.push($(this).val());
   }); 
   
      //data['assign_managed_member'] = assign_managed_member;
      data['assign_managed_member'] = arr('assign_managed_member');
       data['manager_reviewer_member'] = manager_reviewer_member;
   
       data['clientId'] = clientId;
       $.ajax({
         url: '<?php echo base_url();?>client/add_responsiblemember/',
       type: "POST",
       data: data,
       success: function(data)  
       {
         
          //location.reload();  
         // window.location.href="<?php echo base_url().'user'?>"
       }
     });
   }
   
   $("#insert_form").validate({
   
          ignore: false,
      // errorClass: "error text-warning",
       //validClass: "success text-success",
       /*highlight: function (element, errorClass) {
           //alert('em');
          // $(element).fadeOut(function () {
              // $(element).fadeIn();
           //});
       },*/
                           rules: {
                           company_name: {required: true},
                          // company_number: {required: true},
                   
                           },
                           errorElement: "span", 
                           errorClass: "field-error",                             
                            messages: {
                             company_name: "Give company name",
                             //company_number: "Give company number",
                             
                            },
   
                           
                           submitHandler: function(form) {
                               var formData = new FormData($("#insert_form")[0]);
   
                               $(".LoadingImage").show();
                               $.ajax({
                                   url: '<?php echo base_url();?>client/updates_client/',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) {
                                    var Contact_id = $('#user_id').val();
   
                                    contact_add(Contact_id);
                                    //add_assignto(Contact_id);
                                       if(data == '1'){
                                          // $('#insert_form')[0].reset();
                                           $('.alert-success').show();
                                           $('.alert-danger').hide();
                                       }
                                       else if(data == '0'){
                                          // alert('failed');
                                           $('.alert-danger').show();
                                           $('.alert-success').hide();
                                       }
                                   $(".LoadingImage").hide();
                                   },
                                   error: function() {
                                        $(".LoadingImage").hide();
                                       alert('faileddd');}
                               });
   
                               return false;
                           } ,
                            /* invalidHandler: function(e,validator) {
           for (var i=0;i<validator.errorList.length;i++){   
               $(validator.errorList[i].element).parents('.panel-collapse.collapse').collapse('show');
           }
       }*/
        invalidHandler: function(e, validator) {
              if(validator.errorList.length)
           $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
   
           }
   
                       });
   
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
   
   /* $( "#datepicker" ).datepicker({
       dateFormat: 'yy-mm-dd'
   });*/
   
    $('.js-small').change(function() {
       
     //$(this).closest('div').find('.field').prop('disabled', !$(this).is(':checked'));
   
     var check= $(this).data('id');
     //alert(check);
    //alert($(this).is(':checked'));
    //alert($(this).val());
    if($(this).is(':checked')&&(check=='accounts')){
       $(".it6").attr("style", "display:block");
       $(".it6").removeClass("hide");
    } else if(check=='accounts') {
       $(".it6").attr("style", "display:none");
       $(".it6").addClass("hide");
    }
   
   if($(this).is(':checked')&&(check=='payroll')){
       $(".it11").attr("style", "display:block");
       $(".it11").removeClass("hide");
    } else if(check=='paye') {
       $(".it11").attr("style", "display:none");
       $(".it11").addClass("hide");
    }
   
   if($(this).is(':checked')&&(check=='vat')){
       $(".it8").attr("style", "display:block");
       $(".it8").removeClass("hide");
    } else if(check=='vat') {
       $(".it8").attr("style", "display:none");
       $(".it8").addClass("hide");
    }
   
   
    if($(this).is(':checked')&&(check=='cons')){
       $(".it7").attr("style", "display:block");
       $(".it7").removeClass("hide");
    } else if(check=='cons') {
       $(".it7").attr("style", "display:none");
       $(".it7").addClass("hide");
    }
    
    if($(this).is(':checked')&&(check=='personaltax')){
       $(".it12").attr("style", "display:block");
       $(".it12").removeClass("hide");
    } else if(check=='personaltax') {
       $(".it12").attr("style", "display:none");
       $(".it12").addClass("hide");
    }
   
     if($(this).is(':checked')&&(check=='management')){
       $(".it13").attr("style", "display:block");
       $(".it13").removeClass("hide");
    } else if(check=='management') {
       $(".it13").attr("style", "display:none");
       $(".it13").addClass("hide");
    }
   
     if($(this).is(':checked')&&(check=='investgate')){
       $(".it14").attr("style", "display:block");
       $(".it14").removeClass("hide");
    } else if(check=='investgate') {
       $(".it14").attr("style", "display:none");
       $(".it14").addClass("hide");
    }
   
   if($(this).is(':checked')&&(check=='preacc')){
       $("#enable_preacc").attr("style", "display:block");
    } else if(check=='preacc') {
       $("#enable_preacc").attr("style", "display:none");
      
    }
    //company tax section enable/disable
    if($(this).is(':checked')&&(check=='companytax')){
      
       $(".companytax").attr("style", "display:block");
    } else if(check=='companytax') {
         
       $(".companytax").attr("style", "display:none");
      
    }
   
    //workplace pension section enable/disable
    if($(this).is(':checked')&&(check=='workplace')){
      
       $(".worktab").attr("style", "display:block");
    } else if(check=='workplace') {
         
       $(".worktab").attr("style", "display:none");
      
    }
   
    //cis/subcis  section enable/disable
    if($(this).is(':checked')&&(check=='cis')){
      
       $(".contratab").attr("style", "display:block");
    } else if(check=='cis') {
         
       $(".contratab").attr("style", "display:none");
      
    }
   
    //p11d  section enable/disable
    if($(this).is(':checked')&&(check=='p11d')){
      
       $(".p11dtab").attr("style", "display:block");
    } else if(check=='p11d') {
         
       $(".p11dtab").attr("style", "display:none");
      
    }
   
    //bookkeep section enable/disable
    if($(this).is(':checked')&&(check=='bookkeep')){
      
       $(".bookkeeptab").attr("style", "display:block");
    } else if(check=='bookkeep') {
         
       $(".bookkeeptab").attr("style", "display:none");
      
    }
   
    //registered section enable/disable
    if($(this).is(':checked')&&(check=='registered')){
      
       $(".registeredtab").attr("style", "display:block");
    } else if(check=='registered') {
         
       $(".registeredtab").attr("style", "display:none");
      
    }
   
    //Tax Advice/Investigation section enable/disable
    if($(this).is(':checked')&&(check=='taxadvice')){
      
       $(".taxadvicetab").attr("style", "display:block");
    } else if(check=='taxadvice') {
         
       $(".taxadvicetab").attr("style", "display:none");
      
    }
   
   //Previous Year Requires
    if($(this).is(':checked')&&(check=='preyear')){
      
       $("#enable_preyear").attr("style", "display:block");
    } else if(check=='preyear') {
         
       $("#enable_preyear").attr("style", "display:none");
      
    }
   
    // confirmation remainder
    if($(this).is(':checked')&&(check=='enableconf')){
      
       $(".enable_conf").attr("style", "");
    } else if(check=='enableconf') {
         
       $(".enable_conf").attr("style", "display:none");
      
    }
   
    // Accounts remainder
    if($(this).is(':checked')&&(check=='enableacc')){
      
       $(".enable_acc").attr("style", "");
       $(".enable_accdue").attr("style", "");
    } else if(check=='enableacc') {
         
       $(".enable_acc").attr("style", "display:none");
       $(".enable_accdue").attr("style", "display:none");  
    }
    // company tax remainder
    if($(this).is(':checked')&&(check=='enabletax')){
      
       $(".enable_tax").attr("style", "");
      
    } else if(check=='enabletax') {
         
       $(".enable_tax").attr("style", "display:none");
      
    }
   
    // Personal Tax Return remainder
    if($(this).is(':checked')&&(check=='enablepertax')){
      
       $(".enable_pertax").attr("style", "");
      
    } else if(check=='enablepertax') {
         
       $(".enable_pertax").attr("style", "display:none");
       
    }
    // payroll remainder
    if($(this).is(':checked')&&(check=='enablepay')){
      
       $(".enable_pay").attr("style", "");
      
    } else if(check=='enablepay') {
         
       $(".enable_pay").attr("style", "display:none");
       
    }
    // WorkPlace Pension - AE remainder
    if($(this).is(':checked')&&(check=='enablework')){
      
       $(".enable_work").attr("style", "");
       $(".enable_workdue").attr("style", "");
      
    } else if(check=='enablework') {
         
       $(".enable_work").attr("style", "display:none");
       $(".enable_workdue").attr("style", "display:none");
       
    }
    // C.I.S Contractor remainder
    if($(this).is(':checked')&&(check=='enablecis')){
      
       $(".enable_cis").attr("style", "");
      
    } else if(check=='enablecis') {
         
       $(".enable_cis").attr("style", "display:none");
       
    }
   // C.I.S Sub Contractor remainder
    if($(this).is(':checked')&&(check=='enablecissub')){
      
       $(".enable_cissub").attr("style", "");
      
    } else if(check=='enablecissub') {
         
       $(".enable_cissub").attr("style", "display:none");
       
    }
    // P11D remainder
    if($(this).is(':checked')&&(check=='enableplld')){
      
       $(".enable_plld").attr("style", "");
      
    } else if(check=='enableplld') {
         
       $(".enable_plld").attr("style", "display:none");
       
    }
   // VAT Quarters remainder
    if($(this).is(':checked')&&(check=='enablevat')){
      
       $(".enable_vat").attr("style", "");
      
    } else if(check=='enablevat') {
         
       $(".enable_vat").attr("style", "display:none");
       
    }
    // Bookkeeping remainder
    if($(this).is(':checked')&&(check=='enablebook')){
      
       $(".enable_book").attr("style", "");
      
    } else if(check=='enablebook') {
         
       $(".enable_book").attr("style", "display:none");
       
    }
    // Management Accounts remainder
    if($(this).is(':checked')&&(check=='enablemanagement')){
      
       $(".enable_management").attr("style", "");
      
    } else if(check=='enablemanagement') {
         
       $(".enable_management").attr("style", "display:none");
       
    }
   
    // Investigation insurance remainder
    if($(this).is(':checked')&&(check=='enableinvest')){
      
       $(".enable_invest").attr("style", "");
      
    } else if(check=='enableinvest') {
         
       $(".enable_invest").attr("style", "display:none");
       
    }
   
    // registration add remainder
    if($(this).is(':checked')&&(check=='enablereg')){
      
       $(".enable_reg").attr("style", "");
      
    } else if(check=='enablereg') {
         
       $(".enable_reg").attr("style", "display:none");
       
    }
    // tax advice add remainder
    if($(this).is(':checked')&&(check=='enabletaxad')){
      
       $(".enable_taxad").attr("style", "");
      
    } else if(check=='enabletaxad') {
         
       $(".enable_taxad").attr("style", "display:none");
       
    }
   
   //onet remainder
    if($(this).is(':checked')&&(check=='onet')){
      
       $(".onet").attr("style", "");
      
    } else if(check=='onet') {
         
       $(".onet").attr("style", "display:none");
       
    }
    //twot remainder
    if($(this).is(':checked')&&(check=='twot')){
      
       $(".twot").attr("style", "");
      
    } else if(check=='twot') {
         
       $(".twot").attr("style", "display:none");
       
    }
   
   //threet remainder
    if($(this).is(':checked')&&(check=='threet')){
      
       $(".threet").attr("style", "");
      
    } else if(check=='threet') {
         
       $(".threet").attr("style", "display:none");
       
    }
   
    //fourt remainder
    if($(this).is(':checked')&&(check=='fourt')){
      
       $(".fourt").attr("style", "");
      
    } else if(check=='fourt') {
         
       $(".fourt").attr("style", "display:none");
       
    }
    //fivet remainder
    if($(this).is(':checked')&&(check=='fivet')){
      
       $(".fivet").attr("style", "");
      
    } else if(check=='fivet') {
         
       $(".fivet").attr("style", "display:none");  
    }
   
     //sixt remainder
    if($(this).is(':checked')&&(check=='sixt')){
      
       $(".sixt").attr("style", "");
      
    } else if(check=='sixt') {
         
       $(".sixt").attr("style", "display:none");  
    }
   
    //sevent remainder
    if($(this).is(':checked')&&(check=='sevent')){
      
       $(".sevent").attr("style", "");
      
    } else if(check=='sevent') {
         
       $(".sevent").attr("style", "display:none");  
    }
   
   //eightt remainder
    if($(this).is(':checked')&&(check=='eightt')){
      
       $(".eightt").attr("style", "");
      
    } else if(check=='eightt') {
         
       $(".eightt").attr("style", "display:none");  
    }
    //ninet remainder
    if($(this).is(':checked')&&(check=='ninet')){
      
       $(".ninet").attr("style", "");
      
    } else if(check=='ninet') {
         
       $(".ninet").attr("style", "display:none");  
    }
   
    //tent remainder
    if($(this).is(':checked')&&(check=='tent')){
      
       $(".tent").attr("style", "");
      
    } else if(check=='tent') {
         
       $(".tent").attr("style", "display:none");  
    }
   
    //elevent remainder
    if($(this).is(':checked')&&(check=='elevent')){
      
       $(".elevent").attr("style", "");
      
    } else if(check=='elevent') {
         
       $(".elevent").attr("style", "display:none");  
    }
   
    //twelvet remainder
    if($(this).is(':checked')&&(check=='twelvet')){
      
       $(".twelvet").attr("style", "");
      
    } else if(check=='twelvet') {
         
       $(".twelvet").attr("style", "display:none");  
    }
   
    //thirteent remainder
    if($(this).is(':checked')&&(check=='thirteent')){
      
       $(".thirteent").attr("style", "");
      
    } else if(check=='thirteent') {
         
       $(".thirteent").attr("style", "display:none");  
    }
   
    //fourteent remainder
    if($(this).is(':checked')&&(check=='fourteent')){
      
       $(".fourteent").attr("style", "");
      
    } else if(check=='fourteent') {
         
       $(".fourteent").attr("style", "display:none");  
    }
   
    //fifteent remainder
    if($(this).is(':checked')&&(check=='fifteent')){
      
       $(".fifteent").attr("style", "");
      
    } else if(check=='fifteent') {
         
       $(".fifteent").attr("style", "display:none");  
    }
   
    //sixteent remainder
    if($(this).is(':checked')&&(check=='sixteent')){
      
       $(".sixteent").attr("style", "");
      
    } else if(check=='sixteent') {
         
       $(".sixteent").attr("style", "display:none");  
    }
   
   });
   
   
   
   $('.contactupdate').click(function(){
            $(".LoadingImage").show();
   
   var Contact_id = $(this).data("id");
   /*var formData = new FormData($("#contact_info"+Contact_id));
   alert("#contact_info"+Contact_id);*/
   /*var datas = $("#contact_info"+Contact_id).serialize();
   alert(datas);
   */
   /*  var formElement = document.getElementById("#contact_info"+Contact_id);
           var formData = new FormData(formElement);*/
         // alert(formData);
   
         /* var rec = getFormData('contact_info'+Contact_id);
          alert(JSON.stringify(rec))*/
   //alert(Contact_id);
   var data = {};
   
   data['title'] = $("#title"+Contact_id).val();
   data['first_name'] = $("#first_name"+Contact_id).val();
   data['middle_name'] = $("#middle_name"+Contact_id).val();
   data['surname'] = $("#surname"+Contact_id).val();
   data['preferred_name'] = $("#preferred_name"+Contact_id).val();
   data['mobile'] = $("#mobile"+Contact_id).val();
   data['main_email'] = $("#main_email"+Contact_id).val();
   data['nationality'] = $("#nationality"+Contact_id).val();
   data['psc'] = $("#psc"+Contact_id).val();
   data['ni_number'] = $("#ni_number"+Contact_id).val();
   data['address_line1'] = $("#address_line1"+Contact_id).val();
   data['address_line2'] = $("#address_line2"+Contact_id).val();
   data['town_city'] = $("#town_city"+Contact_id).val();
   data['post_code'] = $("#post_code"+Contact_id).val();
   data['landline'] = $("#landline"+Contact_id).val();
   data['work_email'] = $("#work_email"+Contact_id).val();
   data['date_of_birth'] = $("#date_of_birth"+Contact_id).val();
   data['nature_of_control'] = $("#nature_of_control"+Contact_id).val();
   data['marital_status'] = $("#marital_status"+Contact_id).val();
   data['utr_number'] = $("#utr_number"+Contact_id).val();
   data['shareholder'] = $("#shareholder"+Contact_id).val();
   data['contact_type'] = $("#contact_type"+Contact_id).val();
   
   data['Contact_id'] = Contact_id;
     $.ajax({
      url: '<?php echo base_url();?>client/updates_clientcontact/',
       type: "POST",
       data: data,
       success: function(data)  
       {
         $('.succ').hide();
         $("#succ"+Contact_id).show();
         $(".LoadingImage").hide();
   
       }
     });
   //alert(JSON.stringify($("#title"+Contact_id)));
   /*$.ajax({
                                   url: '<?php echo base_url();?>client/updates_clientcontact/',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : data,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) {
                                   alert('ffff');
                                   },
                                   error: function() {
                                        $(".LoadingImage").hide();
                                       alert('failed');}
                               });*/
   });
   
   
   function getFormData(formId) {
       return $('#' + formId).serializeArray().reduce(function (obj, item) {
           var name = item.name,
               value = item.value;
   
           if (obj.hasOwnProperty(name)) {
               if (typeof obj[name] == "string") {
                   obj[name] = [obj[name]];
                   obj[name].push(value);
               } else {
                   obj[name].push(value);
               }
           } else {
               obj[name] = value;
           }
           return obj;
       }, {});
   }
   
   
   $(".delcontact").on('click',function(e){
       e.preventDefault();
    id = $(this).data('value');
   //alert(id);
     $.ajax({
         type:"POST",
         url:"<?php echo base_url().'client/delete_contact/'?>",
         data:"id="+id,
         cache: false,  
         success:
         function(data)  
         {
   
         $(".remove"+id).remove();
         $('#modalcontact'+id).modal('hide');
         return false;
         
         }
       });
   return false;
   });
   // delete new person
   $(".delnewperson").on('click',function(e){
       e.preventDefault();
    //id = $(this).data('value');
    $(".removenew").remove();
    $('#modalnewperson').modal('hide');
    return false;
   });
   $("#select_responsible_type").change(function(){
   //alert($(this).val());
   var rec = $(this).val();
   if(rec=='staff')
   {
      $('.responsible_user_tbl').show();
      $('.responsible_team_tbl').hide();
      $('.responsible_department_tbl').hide();
      $('.responsible_member_tbl').hide();
   
   }else if(rec=='team')
   {
      $('.responsible_team_tbl').show();
      $('.responsible_user_tbl').hide();
      $('.responsible_department_tbl').hide();
      $('.responsible_member_tbl').hide();
   }else if(rec=='departments')
   {
      $('.responsible_department_tbl').show();
      $('.responsible_user_tbl').hide();
      $('.responsible_team_tbl').hide();
      $('.responsible_member_tbl').hide();
   }else if(rec=='members')
   {
      $('.responsible_member_tbl').show();
      $('.responsible_user_tbl').hide();
      $('.responsible_team_tbl').hide();
      $('.responsible_department_tbl').hide();
   
   }
   else{
         $('.responsible_user_tbl').hide();
         $('.responsible_team_tbl').hide();
         $('.responsible_department_tbl').hide();
         $('.responsible_member_tbl').hide();
   }
   });
   $("#person").change(function(){
      var person = $(':selected',this).val();
      var client_id = $('#user_id').val();
      // alert(legal_form);
      if(person=='opt1'){
      // $('#exist_person').modal('show');
      $('.all_layout_modals').modal('show');
      }else if(person=='opt2')
      {
         var cnt = $("#append_cnt").val();
   
               if(cnt==''){
               cnt = 1;
              }else{
               cnt = parseInt(cnt)+1;
              }
           $("#append_cnt").val(cnt);
   
           var incre=$("#incre").val();
           if(incre==''){
               incre = 1;
              }else{
               incre = parseInt(incre)+1;
              }
           $("#incre").val(incre);
           
           //alert(incre);
          $.ajax({
             url: '<?php echo base_url();?>client/new_contact_clientinfo/',
             type: 'post',
             data:{ 'cnt':cnt, 'incre':incre},
             //data: "person="+person+"&client_id="+client_id,
             success: function( data ){
                 $('.contact_form').append(data);
                 },
                 error: function( errorThrown ){
                     console.log( errorThrown );
                 }
             });
      }
      
      }); 
   // Other custom label
   $(document).on('change','.othercus',function(e){
   //$(".othercus").change(function(){
      var custom = $(':selected',this).val();
      var client_id = $('#user_id').val();
       //alert(custom);
       
    if(custom=='Other')
      {
     // $('.contact_type').next('span').show();
     $(this).closest('.contact_type').next('.spnMulti').show();
    } else {
   $('.contact_type').next('span').hide();
    }
   
      
      }); 
   
   //Assign Other custom label
   $(document).on('change','.assign_cus',function(e){
   //$(".othercus").change(function(){
      var custom = $(':selected',this).val();
      var client_id = $('#user_id').val();
       //alert(custom);
       
    if(custom=='Other_Custom')
      {
     // $('.contact_type').next('span').show();
     $(this).closest('.assign_cus_type').next('.spanassign').show();
    } else {
   $(this).closest('.assign_cus_type').next('.spanassign').hide();
    }
   
      
      }); 
   
   });
   
</script>
<script>
   /* $( "#searchCompany" ).autocomplete({
          source: function(request, response) {
              //console.info(request, 'request');
              //console.info(response, 'response');
   
              $.ajax({
                  //q: request.term,
                  url: "<?=site_url('client/SearchCompany')?>",
                  data: { term: $("#searchCompany").val()},
                  dataType: "json",
                  type: "POST",
                  success: function(data) {
                      //alert(JSON.stringify(data.searchrec));
                      //add(data.searchrec);
                      //console.log(data);
                      $(".ui-autocomplete").css('display','block');
                      response(data.searchrec);
                  }
              
              });
          },
          minLength: 2
      });*/
   $("#searchCompany").keyup(function(){
       var currentRequest = null;    
       var term = $(this).val();
       //var term = $(this).val().replace(/ +?/g, '');
       //var term = $(this).val($(this).val().replace(/ +?/g, ''));
       $("#searchresult").show();
       $('#selectcompany').hide();
        $(".LoadingImage").show();
       currentRequest = $.ajax({
          url: '<?php echo base_url();?>client/SearchCompany/',
          type: 'post',
          data: { 'term':term },
          beforeSend : function()    {           
          if(currentRequest != null) {
              currentRequest.abort();
          }
      },
          success: function( data ){
              $("#searchresult").html(data);
               $(".LoadingImage").hide();
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });
     });
   var xhr = null;
   function getCompanyRec(companyNo)
   {
      $(".LoadingImage").show();
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
       
      xhr = $.ajax({
          url: '<?php echo base_url();?>client/CompanyDetails/',
          type: 'post',
          data: { 'companyNo':companyNo },
          success: function( data ){
              //alert(data);
              $("#searchresult").hide();
              $('#companyprofile').show();
              $("#companyprofile").html(data);
              $(".LoadingImage").hide();
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });}
   
   
      function getCompanyView(companyNo)
   {
      //$('.modal-search').hide();
      $(".LoadingImage").show();
     var user_id= $("#user_id").val();
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
   
       
      xhr = $.ajax({
          url: '<?php echo base_url();?>client/selectcompany/',
          type: 'post',
          dataType: 'JSON',
          data: { 'companyNo':companyNo,'user_id': user_id},
          success: function( data ){
              //alert(data);
              $("#searchresult").hide();
              $('#companyprofile').hide();
              $('#selectcompany').show();
              $("#selectcompany").html(data.html);
              $('.main_contacts').html(data.html);
   // append form field value
   $("#company_house").val('1');
   $("#company_name").val(data.company_name);
   $("#company_name1").val(data.company_name);
   $("#company_number").val(data.company_number);
   $("#companynumber").val(data.company_number);
   $("#company_url").val(data.company_url);
   $("#company_url_anchor").attr("href",data.company_url);
   $("#officers_url").val(data.officers_url);
   $("#officers_url_anchor").attr("href",data.officers_url);
   $("#company_status").val(data.company_status);
   $("#company_type").val(data.company_type);
   //$("#company_sic").append(data.company_sic);
   $("#company_sic").val(data.company_sic);
   $("#sic_codes").val(data.sic_codes);
   
   $("#register_address").val(data.address1+"\n"+data.address2+"\n"+data.locality+"\n"+data.postal);
   $("#allocation_holder").val(data.allocation_holder);
   $("#date_of_creation").val(data.date_of_creation);
   $("#period_end_on").val(data.period_end_on);
   $("#next_made_up_to").val(data.next_made_up_to);
   $("#next_due").val(data.next_due);
   $("#confirm_next_made_up_to").val(data.confirm_next_made_up_to);
   $("#confirm_next_due").val(data.confirm_next_due);
   
   $("#tradingas").val(data.company_name);
   $("#business_details_nature_of_business").val(data.nature_business);
   $("#user_id").val(data.user_id);
   
   $(".LoadingImage").hide();
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });}
   
      function getmaincontact(companyNo,appointments,i,birth)
   {
      $(".LoadingImage").show();
      var user_id= $("#user_id").val();
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
              var cnt = $("#append_cnt").val();
   
            if(cnt==''){
            cnt = 1;
           }else{
            cnt = parseInt(cnt)+1;
           }
        $("#append_cnt").val(cnt);
   
        var incre=$("#incre").val();
        if(incre==''){
            incre = 1;
           }else{
            incre = parseInt(incre)+1;
           }
        $("#incre").val(incre);
       
      xhr = $.ajax({
          url: '<?php echo base_url();?>client/maincontact_info/',
          type: 'post',
          
          data: { 'companyNo':companyNo,'appointments': appointments,'user_id': user_id,'cnt':cnt,'birth':birth,'incre':incre},
          success: function( data ){
            $("#usecontact"+i).html('<span class="succ_contact"><a href="#">Added</a></span>');
              //$("#default-Modal,.modal-backdrop.show").hide();
                // $('#default-Modal').modal('hide');
                 // $('#exist_person').modal('hide');
                 $('.all_layout_modal').modal('hide');
   $('.contact_form').append(data);
   $(".LoadingImage").hide();
   
   //$('.contactexist').click(function(){
   
   
   
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
   
          });
   }
   
   
   
   function formSubmit ( formID ) {
   
   $(".LoadingImage").show();
   
   
   //var formId = $(this).closest("form").attr('id');
   
   var formData = $("#contact_exist"+formID).serialize();
   
   var Contact_id = $('#user_id').val();
   
   var data = {};
   data['Contact_id'] = Contact_id;
   data['formdata'] = formData;
   $.ajax({
   url: '<?php echo base_url();?>client/insert_clientcontact/'+Contact_id,
    type: "POST",
    data: data,
    success: function(data)  
    {
      $('.succ').hide();
      $("#succ").show();
   $(".LoadingImage").hide();
   
    }
   });
   
   //});
   }
   
   function make_a_primary(id,clientId)
   {
   $(".LoadingImage").show();
   
   //alert(id);
   var data = {};
   data['contact_id'] = id;
   data['clientId'] = clientId;
   
   $.ajax({
   url: '<?php echo base_url();?>client/update_primary_contact/',
    type: "POST",
    data: data,
    success: function(data)  
    {
        $("#contactss").html(data);
            $(".LoadingImage").hide();
            $(".contactcc").hide();
   
         // location.reload();
   
      //alert('success');
    }
   });
   
   }
   
   /**************************************************
   PACK ADD ROW 
   ***************************************************/  
   
   $('.yk_pack_addrow').click(function(e)
   {
   e.preventDefault();
   
   
   /*data = '<tr class="success ykpackrow"><td><select name="team[]" id="team"  placeholder="Select"><?php foreach($teamlist as $teamkey => $teamval) {?><option value="<?php echo $teamval["id"];?>"><?php echo $teamval["team"];?></option><?php } ?></select></td><td><input type="text" name="allocation_holder[]" id="allocation_holder" ></td><td><button type="button" class="btn btn-danger yk_pack_delrow">Delete</button></td><tr>';
   $(this).parents('.pack_add_row_wrpr_member').before(data);*/
   
   
   
   $(this).parents('.pack_add_row_wrpr').before('<tr class="success ykpackrow"><td><select name="team" id="team"  placeholder="Select"><option value="">--select--</option><?php foreach($teamlist as $teamkey => $teamval) {?><option value="<?php echo $teamval["id"];?>"><?php echo $teamval["team"];?></option><?php } ?></select></td><td><input type="text" name="allocation_holder[]" id="allocation_holder" ></td><td><button type="button" class="btn btn-danger yk_pack_delrow">Delete</button></td><tr>');
   
   
   });
   
   /**************************************************
   PACK REMOVE ROW 
   ***************************************************/  
   
   $(document).on('click','.yk_pack_delrow',function(e)
   {
   $(this).parents('.ykpackrow').remove();
   
   });
   
   /**************************************************
   depart ADD ROW 
   ***************************************************/  
   
   $('.yk_pack_addrow_depart').click(function(e)
   {
   e.preventDefault();
   
   $(this).parents('.pack_add_row_wrpr_depart').before('<tr class="success ykpackrow_depart"><td><select name="depart" id="depart" class="fields"><option value="">--select--</option><?php foreach ($deptlist as $deptlistkey => $deptlistvalue) { ?><option value="<?php echo $deptlistvalue["id"];?>" ><?php echo $deptlistvalue["department"];?></option><?php } ?></select></td><td><input type="text" name="allocation_holder_dept[]" id="allocation_holder_dept" ></td><td><button type="button" class="btn btn-danger yk_pack_delrow_depart">Delete</button></td></tr>');
   
   
   });
   
   /**************************************************
   depart REMOVE ROW 
   ***************************************************/  
   
   $(document).on('click','.yk_pack_delrow_depart',function(e)
   {
   $(this).parents('.ykpackrow_depart').remove();
   
   });
   
   
   /**************************************************
   PACK ADD ROW 
   ***************************************************/  
   
   $('.yk_pack_addrow_user').click(function(e)
   {
   e.preventDefault();
   /*var rec = '<div class="dropdown-sin-2">
                              <select style="display:none" name="manager_reviewer[]" id="manager_reviewer" multiple placeholder="Select">'
   <?php foreach($staff_form as $managers) {
      ?>
                                 '<option value="<?php echo $managers['id']?>"><?php echo $managers['crm_name'];?></option>'
                                 <?php } ?>
                                 
                             '</select>
                           </div> ';*/
                           //var rec = '';
                           // data = $('.append_responsible_staff').html();
                           data='<tr class="success ykpackrow_user append_responsible_staff "><td><div class="dropdown-sin-2"><select style="display:block" name="manager_reviewer" id="manager_reviewer"  placeholder="Select"><?php foreach($staff_form as $managers) {?><option value="<?php echo $managers["id"]?>"><?php echo $managers["crm_name"];?></option><?php } ?></select></div></td><td><div class="dropdown-sin-2"><select style="display:block" name="assign_managed" id="assign_managed"  placeholder="Select"><?php foreach($managed_by as $managedby) {?><option value="<?php echo $managedby["id"]?>"><?php echo $managedby["crm_name"];?></option><?php } ?></select></div></td><td><button type="button" class="btn btn-danger yk_pack_delrow_user">Delete</button></td></tr>';
   $(this).parents('.pack_add_row_wrpr_user').before(data);
   
   
   });
   
   /**************************************************
   PACK REMOVE ROW 
   ***************************************************/  
   
   $(document).on('click','.yk_pack_delrow_user',function(e)
   {
   $(this).parents('.ykpackrow_user').remove();
   
   });
   
   /**************************************************
   member ADD ROW 
   ***************************************************/  
   
   $('.yk_pack_addrow_member').click(function(e)
   {
   e.preventDefault();
   
                              data = '<tr class="success ykpackrowfirsts ykpackrow_member"><td><div class="dropdown-sin-2"><select style="display:block" name="manager_reviewer_member" id="manager_reviewer_member"  placeholder="Select"><?php foreach($referby as $referbykey => $referbyvalue) {?><option value="<?php echo $referbyvalue["crm_first_name"];?>"><?php echo $referbyvalue["crm_first_name"];?></option><?php } ?></select></div></td><td><div class="dropdown-sin-2"><input type="text" name="assign_managed_member[]" id="assign_managed_member" placeholder="" value="" class="fields"></div></td><td><button type="button" class="btn btn-danger yk_pack_delrow_member">Delete</button></td><tr>';
   $(this).parents('.pack_add_row_wrpr_member').before(data);
   
   
   });
   
   /**************************************************
   member REMOVE ROW 
   ***************************************************/  
   
   $(document).on('click','.yk_pack_delrow_member',function(e)
   {
   $(this).parents('.ykpackrow_member').remove();
   
   });
   
   
   /**************************************************
   landline ADD ROW 
   ***************************************************/  
   
   //$('.yk_pack_addrow_landline').click(function(e)
   $(document).on('click','.yk_pack_addrow_landline',function(e)
   {
   e.preventDefault();
   var id=$(this).data('id');
   $(this).parents('.pack_add_row_wrpr_landline').before('<span class="primary-inner success ykpackrow_landline"><label>landline</label><input type="text" class="text-info" name="landline'+id+'[]" id="landline" value=""><a href="javascript:;" class="btn btn-danger yk_pack_delrow_landline"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></span>');
   
   
   });
   
   /**************************************************
   landline REMOVE ROW 
   ***************************************************/  
   
   $(document).on('click','.yk_pack_delrow_landline',function(e)
   {
   $(this).parents('.ykpackrow_landline').remove();
   
   });
   
   /**************************************************
   email ADD ROW 
   ***************************************************/  
   
   //$('.yk_pack_addrow_email').click(function(e)
   $(document).on('click','.yk_pack_addrow_email',function(e)
   {
   e.preventDefault();
   var id=$(this).data('id');
   $(this).parents('.pack_add_row_wrpr_email').before('<span class="primary-inner success ykpackrow_email"><label>Work email</label><input type="text" class="text-info" name="work_email'+id+'[]" id="work_email"  value=""><a href="javascript:;" class="btn btn-danger yk_pack_delrow_email"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></span>');
   
   
   });
   
   /**************************************************
   email REMOVE ROW 
   ***************************************************/  
   
   $(document).on('click','.yk_pack_delrow_email',function(e)
   {
   $(this).parents('.ykpackrow_email').remove();
   
   });
   $('.dropdown-sin-21').dropdown({
      limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
</script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
   $("#alltask").dataTable({
         "iDisplayLength": 10,
      "scrollX": true,
      });
   });
</script>
<script type="text/javascript">
   function check_checkbox()
          {
   
                  if($('[name="confirmation_next_reminder_date"]:checked').length < 1)
                  {
                    $("#add_custom_reminder").fadeIn(2000);
                  $("#add_custom_reminder_label").fadeIn(2000);              
                  $("#add_custom_reminder_link").hide();
                  }else
                  {
                     $("#add_custom_reminder").hide();
                     $("#add_custom_reminder_label").hide();
                     $("#add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
          function check_checkbox1()
          {
   
                  if($('[name="accounts_next_reminder_date"]:checked').length < 1)
                  {
                    $("#accounts_custom_reminder").fadeIn(2000);
                  $("#accounts_custom_reminder_label").fadeIn(2000);
                  $("#accounts_custom_reminder_link").hide();             
   
                  }else
                  {
                     $("#accounts_custom_reminder").hide();
                     $("#accounts_custom_reminder_label").hide();
                     $("#accounts_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
          function check_checkbox2()
          {
   
                  if($('[name="company_next_reminder_date"]:checked').length < 1)
                  {
                     //alert("fdg");
                    $("#company_custom_reminder").fadeIn(2000);
                  $("#company_custom_reminder_label").fadeIn(2000);
                  $("#company_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#company_custom_reminder").hide();
                     $("#company_custom_reminder_label").hide();
                     $("#company_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox3()
          {
   
                  if($('[name="Personal_next_reminder_date"]:checked').length < 1)
                  {
                    $("#personal_custom_reminder").fadeIn(2000);
                  $("#personal_custom_reminder_label").fadeIn(2000);
                  $("#personal_custom_reminder_link").hide();             
   
                  }else
                  {
                     $("#personal_custom_reminder").hide();
                     $("#personal_custom_reminder_label").hide();
                     $("#personal_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox4()
          {
   
                  if($('[name="Payroll_next_reminder_date"]:checked').length < 1)
                  {
                    $("#payroll_add_custom_reminder").fadeIn(2000);
                  $("#payroll_add_custom_reminder_label").fadeIn(2000);
                  $("#payroll_add_custom_reminder_link").hide();             
   
                  }else
                  {
                     $("#payroll_add_custom_reminder").hide();
                     $("#payroll_add_custom_reminder_label").hide();
                     $("#payroll_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox5()
          {
   
                  if($('[name="Pension_next_reminder_date"]:checked').length < 1)
                  {
                    $("#pension_add_custom_reminder").fadeIn(2000);
                  $("#pension_create_task_reminder_label").fadeIn(2000);
                  $("#pension_create_task_reminder_link").hide();            
   
                  }else
                  {
                     $("#pension_add_custom_reminder").hide();
                     $("#pension_create_task_reminder_label").hide();
                     $("#pension_create_task_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox6()
          {
   
                  if($('[name="Cis_next_reminder_date"]:checked').length < 1)
                  {
                    $("#cis_add_custom_reminder").fadeIn(2000);
                  $("#cis_add_custom_reminder_label").fadeIn(2000);
                  $("#cis_add_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#cis_add_custom_reminder").hide();
                     $("#cis_add_custom_reminder_label").hide();
                     $("#cis_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox7()
          {
   
                  if($('[name="P11d_next_reminder_date"]:checked').length < 1)
                  {
                    $("#p11d_add_custom_reminder").fadeIn(2000);
                  $("#p11d_add_custom_reminder_label").fadeIn(2000);
                  $("#p11d_add_custom_reminder_link").hide();             
   
                  }else
                  {
                     $("#p11d_add_custom_reminder").hide();
                     $("#p11d_add_custom_reminder_label").hide();
                     $("#p11d_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox8()
          {
   
                  if($('[name="bookkeep_next_reminder_date"]:checked').length < 1)
                  {
                    $("#bookkeep_add_custom_reminder").fadeIn(2000);
                  $("#bookkeep_add_custom_reminder_label").fadeIn(2000);
                  $("#bookkeep_add_custom_reminder_link").hide();            
   
                  }else
                  {
                     $("#bookkeep_add_custom_reminder").hide();
                     $("#bookkeep_add_custom_reminder_label").hide();
                     $("#bookkeep_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox9()
          {
   
                  if($('[name="manage_next_reminder_date"]:checked').length < 1)
                  {
                    $("#manage_add_custom_reminder").fadeIn(2000);
                  $("#manage_add_custom_reminder_label").fadeIn(2000);
                  $("#manage_add_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#manage_add_custom_reminder").hide();
                     $("#manage_add_custom_reminder_label").hide();
                     $("#manage_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox10()
          {
   
                  if($('[name="Vat_next_reminder_date"]:checked').length < 1)
                  {
                    $("#vat_add_custom_reminder").fadeIn(2000);
                  $("#vat_add_custom_reminder_label").fadeIn(2000);
                  $("#vat_add_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#vat_add_custom_reminder").hide();
                     $("#vat_add_custom_reminder_label").hide();
                     $("#vat_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox11()
          {
   
                  if($('[name="Declatrion_next_reminder_date"]:checked').length < 1)
                  {
                    $("#Declration_add_custom_reminder").fadeIn(2000);
                  $("#Declration_add_custom_reminder_label").fadeIn(2000);
                  $("#Declration_add_custom_reminder_link").hide();             
   
                  }else
                  {
                     $("#Declration_add_custom_reminder").hide();
                     $("#Declration_add_custom_reminder_label").hide();
                     $("#Declration_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox12()
          {
   
                  if($('[name="cissub_next_reminder_date"]:checked').length < 1)
                  {
                    $("#cissub_add_custom_reminder").fadeIn(2000);
                  $("#cissub_add_custom_reminder_label").fadeIn(2000);
                  $("#cissub_add_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#cissub_add_custom_reminder").hide();
                     $("#cissub_add_custom_reminder_label").hide();
                     $("#cissub_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
      
      
</script>
<script type="text/javascript">
   $( document ).ready(function() {
   
       $("#add_custom_reminder").hide();
       $("#add_custom_reminder_label").hide();
   
       $("#accounts_custom_reminder").hide();
       $("#accounts_custom_reminder_label").hide();
   
       $("#company_custom_reminder").hide();
       $("#company_custom_reminder_label").hide();
   
       $("#personal_custom_reminder").hide();
       $("#personal_custom_reminder_label").hide();
   
       $("#payroll_add_custom_reminder").hide();
       $("#payroll_add_custom_reminder_label").hide();
   
       $("#pension_add_custom_reminder").hide();
       $("#pension_create_task_reminder_label").hide();
   
       $("#cis_add_custom_reminder").hide();
       $("#cis_add_custom_reminder_label").hide();
   
       $("#p11d_add_custom_reminder").hide();
       $("#p11d_add_custom_reminder_label").hide();
   
       $("#bookkeep_add_custom_reminder").hide();
       $("#bookkeep_add_custom_reminder_label").hide();
   
       $("#manage_add_custom_reminder").hide();
       $("#manage_add_custom_reminder_label").hide();
   
       $("#vat_add_custom_reminder").hide();
       $("#vat_add_custom_reminder_label").hide();
   
        $("#Declration_add_custom_reminder").hide();
       $("#Declration_add_custom_reminder_label").hide();
   
        $("#cissub_add_custom_reminder").hide();
       $("#cissub_add_custom_reminder_label").hide();
   
      });
</script>

<script type="text/javascript">
   $(document).ready(function(){

setTimeout(function(){ $('.alert-success').hide() }, 2000);
   });
</script>


<script>
  

  $(".click_function").click(function(e){
    e.preventDefault();   
    if($(this).attr('id')=='next'){     
       $('#allusers').find('ol.nav-tabs .active').closest('li').next('li').find('a').trigger('click').parent().show();
    }else{   
        $('#allusers').find('ol.nav-tabs .active').closest('li').prev('li').find('a').trigger('click');
    }
  });
</script>