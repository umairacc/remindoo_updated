<?php echo $_SESSION['id']; ?>

<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');
   $userid = $this->session->userdata('id'); 
   ?>
<style type="text/css">
   .to-do-list.sortable-moves {
   padding-left: 0;
   }
   .pcoded-content
   {
   float: left !important;
   width:100%;
   }
   .hide_classes{
      display: none;
   }
</style>
<!-- management block -->
<div class="pcoded-content pcode-mydisk">
<div class="pcoded-inner-content">
<!-- Main-body start -->
<div class="main-body myDesk">
   <div class="page-wrapper">
      <div class="card desk-dashboard">
         <div class="inner-tab123" >
            <div class="deadline-crm1" >
               <ul class="nav" id="proposal_dashboard">
                  <li class="active <?php $query=$this->Common_mdl->get_price('settings','user_id','1','dashboard_basic_details'); if($query==0){ ?> hide_check  <?php } ?>" >
                     <a data-toggle="tab" href="#Basics">Basics
                     </a>
                  </li>
                  <li class="<?php $query=$this->Common_mdl->get_price('settings','user_id','1','dashboard_task_details'); if($query==0){ ?> hide_check  <?php } ?>">
                     <a data-toggle="tab" href="#Tasks">Tasks
                     </a>
                  </li>
                  <li class="<?php $query=$this->Common_mdl->get_price('settings','user_id','1','dashboard_basic_details'); if($query==0){ ?> hide_check  <?php } ?>">
                     <a data-toggle="tab" href="#Clients">Clients
                     </a>
                  </li>
                  <li class="<?php $query=$this->Common_mdl->get_price('settings','user_id','1','dashboard_proposal'); if($query==0){ ?> hide_check  <?php } ?>">
                     <a data-toggle="tab"  href="#Proposals">Proposals
                     </a>
                  </li>
                  <li class="<?php $query=$this->Common_mdl->get_price('settings','user_id','1','dashboard_deadline_details'); if($query==0){ ?> hide_check  <?php } ?>">
                     <a data-toggle="tab"  href="#Deadline-Manager">Deadline Manager
                     </a>
                  </li>
               </ul>
            </div>
          <div class="tab-content">
               <!-- tab1 -->
               <div id="Basics" class="proposal_send tab-pane fade in active <?php $query=$this->Common_mdl->get_price('settings','user_id','1','dashboard_basic_details'); 
               if($query==0){ ?> hide_classes <?php } ?>"  >
                  <!-- pro summary -->
				  
				<div class="invoice-crm1 disk-redesign1">  
				<div class="top-leads fr-task cfssc redesign-lead-wrapper">
				<div class="lead-data1 pull-right leadsG leads_status_count">
				
				<div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
				<div class="lead-point1">
				<a href="<?php echo base_url(); ?>Firm_dashboard/Active_client" target="_blank">
				<strong><?php print_r($active_client['client_count']);?></strong>
				<span class="status_filter">archive</span>
				</a>
				</div>
				</div>
				
				 <div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
				<div class="lead-point1">
				<a href="<?php echo base_url(); ?>Firm_dashboard/Complete" target="_blank">
				<strong><?php print_r($completed_tasks['task_count']);?></strong>
				<span class="status_filter">Completed task</span>
				</a>
				</div>
				</div>
				
				 <div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
				<div class="lead-point1">
				<a href="<?php echo base_url(); ?>Firm_dashboard/Inactive_client" target="_blank">
				<strong><?php print_r($inactive_client['client_count']);?></strong>
				<span class="status_filter">Inactive users</span>
				</a>
				</div>
				</div>
				
				 <div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
				<div class="lead-point1">
				<a href="<?php echo base_url(); ?>leads" target="_blank">
				<strong><?php print_r(count($total_leads));?></strong>
				<span class="status_filter">Over All Leads</span>
				</a>
				</div>
				</div>
				

				</div>
				</div>
				</div>
				  
                  <div class="floating_set space-disk1">
                     <div class="col-md-12 col-xl-6">
                        <div class="card">
                           <div class="card-block user-detail-card">
                              <div class="row">
                                 <div class="col-sm-4">
                                    <div class="height-profile">
                                       <div class="height-profile1"> <?php  $getUserProfilepic = $this->Common_mdl->getUserProfilepic($userid); ?><img src="<?php echo $getUserProfilepic;?> " alt="" class="img-fluid p-b-10"></div>
                                    </div>
                                    <!-- <div class="contact-icon">
                                       <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="View" ><i class="icofont icofont-eye m-0"></i></button>
                                       <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Print" ><i class="icofont icofont-printer m-0"></i></button>
                                       <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Download" ><i class="icofont icofont-download-alt m-0"></i></button>
                                       <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Share" ><i class="icofont icofont-share m-0"></i></button>
                                       </div> -->
                                 </div>
                                 <div class="col-sm-8 user-detail">
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Name :</h6>
                                       </div>
                                       <div class="col-sm-7">
                                          <h6 class="m-b-30"><?php 
                                             $user_role=$this->Common_mdl->select_record('user','id',$userid);
                                             $role_name=$this->Common_mdl->select_record('Role','id',$user_role['role']);
                                             echo $user_role['crm_name'];?></h6>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-email"></i>EMP ID</h6>
                                       </div>
                                       <div class="col-sm-7">
                                          <h6 class="m-b-30"><a href="mailto:dummy@example.com"><?php echo '#'.$userid; ?></a></h6>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-home"></i>Role ID :</h6>
                                       </div>
                                       <div class="col-sm-7">
                                          <h6 class="m-b-30"><?php echo $role_name['role']; ?></h6>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-touch-phone"></i>Phone :</h6>
                                       </div>
                                       <div class="col-sm-7">
                                          <h6 class="m-b-30"> </h6>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-web"></i>Website :</h6>
                                       </div>
                                       <div class="col-sm-7">
                                          <h6 class="m-b-30"><a href="#!"> </a></h6>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Summery Start -->
                    <!-- <div class="col-md-12 col-xl-6 summary-card1">
                        <div class="card summery-card">
                           <div class="card-header">
                              <div class="card-header-left ">
                                 <h5>Summary</h5>
                              </div>
                           </div>
                           <div class="card-block">
                              <div class="row">
                                 <div class="col-sm-6 b-r-default p-b-30">
                                    <h2 class="f-w-400"><?php print_r($active_client['client_count']);?></h2>
                                    <p class="text-muted f-w-400"><a href="<?php echo base_url(); ?>Firm_dashboard/Active_client" target="_blank">Active users</a></p>
                                    <div class="progress">
                                       <div class="progress-bar bg-c-blue" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 p-b-30">
                                    <h2 class="f-w-400"><?php print_r($completed_tasks['task_count']);?></h2>
                                    <p class="text-muted f-w-400"><a href="<?php echo base_url(); ?>Firm_dashboard/Complete" target="_blank">Completed task</a></p>
                                    <div class="progress">
                                       <div class="progress-bar bg-c-pink" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="row">
                                 <div class="col-sm-6 b-r-default p-b-30">
                                    <h2 class="f-w-400"><?php print_r($inactive_client['client_count']);?></h2>
                                    <p class="text-muted f-w-400"><a href="<?php echo base_url(); ?>Firm_dashboard/Inactive_client" target="_blank">Inactive users</a></p>
                                    <div class="progress">
                                       <div class="progress-bar bg-c-yellow" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 p-b-30">
                                    <!--    <h2 class="f-w-400"><?php print_r($closed_tasks['task_count']);?></h2>
                                       <p class="text-muted f-w-400">Completed task</p>  
                                    <h2 class="f-w-400"><?php print_r(count($total_leads));?></h2>
                                    <p class="text-muted f-w-400"><a href="<?php echo base_url(); ?>leads" target="_blank">Over All Leads</a></p>
                                    <div class="progress">
                                       <div class="progress-bar bg-c-green" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>  --> 
                     <!-- summary end -->
                  </div>
                  <!-- pro summary -->
               </div>
               <!-- tab2-->
               <div id="Tasks" class="tab-pane fade">
			   
				
				<div class="invoice-crm1 disk-redesign1">  
				<div class="top-leads fr-task cfssc redesign-lead-wrapper">
				<div class="lead-data1 pull-right leadsG leads_status_count">

				<div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
				<div class="lead-point1">
				<a href="<?php echo base_url(); ?>user/task_list" target="_blank">
				<strong><?php echo $tasks['task_count']; ?></strong>
				<span class="status_filter">Tasks</span>
				</a>
				</div>
				</div>
				
				<div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
				<div class="lead-point1">
				<a href="<?php echo base_url(); ?>Firm_dashboard/Complete" target="_blank">
				<strong><?php echo $closed_tasks['task_count']; ?> </strong>
				<span class="status_filter">Completed Tasks</span>
				</a>
				</div>
				</div>
				
				<div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
				<div class="lead-point1">
				<a href="<?php echo base_url(); ?>Firm_dashboard/inprogress" target="_blank">
				<strong><?php echo $pending_tasks['task_count']; ?> </strong>
				<span class="status_filter">Inprogress Tasks</span>
				</a>
				</div>
				</div>
				
				<div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
				<div class="lead-point1">
				<a href="<?php echo base_url(); ?>Firm_dashboard/notstarted" target="_blank">
				<strong><?php echo $cancelled_tasks['task_count']; ?> </strong>
				<span class="status_filter">Not Started Tasks</span>
				</a>
				</div>
				</div>
				
				</div>
				</div>
				</div>

                   
                 <!--  <div class="revenue-task row">
                      
                     <div class="col-md-6 col-xl-3">
                        <div class="card widget-card-1">
                           <div class="card-block-small">
                              <i class="icofont icofont-tasks-alt bg-c-green card1-icon"></i>
                              <span class="text-c-green f-w-600"><a href="<?php echo base_url(); ?>user/task_list" target="_blank">Tasks</a></span>
                               
                              <h4>
                                 <?php echo $tasks['task_count']; ?>  
                              </h4>
                           </div>
                        </div>
                     </div>
                   
                     <div class="col-md-6 col-xl-3">
                        <div class="card widget-card-1">
                           <div class="card-block-small">
                              <i class="icofont icofont-tasks-alt bg-c-pink card1-icon"></i>
                              <span class="text-c-pink f-w-600"><a href="<?php echo base_url(); ?>Firm_dashboard/Complete" target="_blank">Completed Tasks</a></span>
                               
                              <h4>
                                 <?php echo $closed_tasks['task_count']; ?> 
                              </h4>
                           </div>
                        </div>
                     </div>
                    
                     <div class="col-md-6 col-xl-3">
                        <div class="card widget-card-1">
                           <div class="card-block-small">
                              <i class="icofont icofont-tasks-alt bg-c-yellow card1-icon"></i>
                              <span class="text-c-yellow f-w-600"><a href="<?php echo base_url(); ?>Firm_dashboard/inprogress" target="_blank">Inprogress Tasks</a></span>
                              
                              <h4>
                                 <?php echo $pending_tasks['task_count']; ?> 
                              </h4>
                           </div>
                        </div>
                     </div>
                  
                     <div class="col-md-6 col-xl-3">
                        <div class="card widget-card-1">
                           <div class="card-block-small">
                              <i class="icofont icofont-tasks-alt bg-c-blue card1-icon"></i>
                              <span class="text-c-blue f-w-600"><a href="<?php echo base_url(); ?>Firm_dashboard/notstarted" target="_blank">Not Started Tasks</a></span>
                               
                              <h4>
                                 <?php echo $cancelled_tasks['task_count']; ?> 
                              </h4>
                           </div>
                        </div>
                     </div>
                   
                  </div>  -->
				  
                  <div class="cards12 space-disk1 col-xs-12 mydisk-realign1">
                     <div class="col-xl-6 col-sm-6 lf-mytsk">
                        <!-- To Do List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>My Tasks</h5>
                              <!-- <div class="card-header-right"> <i class="icofont icofont-spinner-alt-5"></i> </div> -->
                           </div>
                           <div class="card-block">
                              <!-- <h3> Status</h3> -->
                              <h3 <?php
                              if(!empty($task_list)){ ?>   <?php } ?> >Task Name <span class="f-right">Start Date / End Date</span></h3>
                             <?php  
                             if(empty($task_list)){ ?>                                

                             <div class="new-task12">
                               No Tasks Available 
                             </div>
                             <?php  }else{ ?>
                              <div class="new-task">
                                
                                 <?php 
                                    foreach($task_list as $task){                                  

                                    $explode_worker=explode(',',$task['worker']);


                                    if(strtotime($task['end_date']) < time()){
                                    $date = date('y-m-j&\nb\sp;g:i:s');
                                    
                                    $start_date=date('Y-m-d',strtotime($task['start_date']));
                                    $end_date=$task['end_date'];
                                                                      
                                    $date1 = strtotime(''.$start_date.' 00:00:00');

                                    $date2 = strtotime(''.$end_date.' 00:00:00');

                                    $today = time();
                                    
                                    $dateDiff = $date2 - $date1;
                                  
                                    $dateDiffForToday = $today - $date1;
                                   
                                    $percentage = $dateDiffForToday / $dateDiff * 100;

                                    $percentageRounded = round($percentage);

									         ?>
                                    
                                   <span class="percentage-code1">overdue</span>
								   <?php 
                                    }else{
                                    
                                    $percentageRounded='0';
									?>
                                    <span class="percentage-code1"> <?php echo $percentageRounded . '%';  ?> </span>
									<?php 
                                    }
									?>
                                  
								
                                 <div class="to-do-list">
                                    <div class="checkbox-fade fade-in-primary">
                                       <label class="check-task">
                                          <span><a href="<?php echo base_url();?>user/task_details/<?php echo $task['id']; ?>"><?php echo $task['subject']; ?></a></span> 
                                                <span class="bg-c-pink">
                                                <?php 
                                                $username=array();
                                                foreach($explode_worker as $key => $val){     
                                                     $getUserProfilename = $this->Common_mdl->getUserProfileName($val);
                                                     array_push($username,$getUserProfilename);
                                                } 
                                                echo implode(',', $username); ?>                                                   
                                                </span>
                                          <div class="progress">
                                             <div class="progress-bar bg-c-blue" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentageRounded.'%'; ?>">
                                             </div>
                                          </div>
                                       </label>
                                    </div>
                                    <span class="f-right end-dateop"></span>
                                    <span class="f-right end-dateop"><?php echo $task['start_date']; ?>/<?php echo date('d-m-Y',strtotime($task['end_date'])); ?></span>
                                    <!-- <div class="f-right">
                                       <a href="#" class="delete_todolist"><i class="icofont icofont-ui-delete"></i></a>
                                       </div> -->
                                 </div>
                                 <?php }  ?>            
                              </div>
                              <?php } ?>
                           </div>
                        </div>
                        <!-- To Do List card end -->
                     </div>
                     <div class="col-xl-6 col-sm-6 lf-mytodo  mydisk-view">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 My To Do items 
                                 <span class="f-right">
                                    <ul class="nav nav-tabs  tabs" role="tablist">
                                       <li class="nav-item">
                                          <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">New Td Do</a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">View All</a>
                                       </li>
                                    </ul>
                                 </span>
                              </h5>
                           </div>
                           <div class="widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task align-todos" id="draggablePanelList">
                                       <div class="no-todo">
                                          <h5 class="bg-c-green12"> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Latest to do's <button type="button" class="btn btn-primary btn-sm f-right" data-toggle="modal" data-target="#myTodo">New Todo's</button></h5>
                                          <div class="sortable edit_after" id="5">
                                             <?php
                                                foreach($todo_list as $todo){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $todo['id']; ?>">
                                                <div class="top-disk01">
                                                   <label class="check-task custom_checkbox1">
                                                   <input type="checkbox" value="" id="<?php echo $todo['id']; ?>" onclick="todo_status(this)"> <i></i> </label>
                                                  <!--  <span class="cr">
                                                   <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                   </span> -->
                                                   <span><?php echo $todo['todo_name']; ?></span> 
                                                </div>
                                                <div class="f-right">
                                                   <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
                                                   <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
                                                </div>
                                             </div>
                                             <?php } ?>
                                          </div>
                                       </div>
                                       <h5 class="bg-c-green12"> <i class="fa fa-check"></i> Latest finished to do's</h5>
                                       <div class="completed_todos">
                                          <?php
                                             foreach($todo_completedlist as $todo){ ?>
                                          <div class="to-do-list card-sub" id="1">
                                             <div class="top-disk01">
                                                <label class="check-task done-task custom_checkbox1">
                                                
												<input type="checkbox" value="" checked="checked" id="<?php echo $todo['id']; ?>" onclick="todo_status(this)" > <i></i>  </label>
                                               <!--  <span class="cr">
                                                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                </span> -->
                                                <span><?php echo $todo['todo_name']; ?></span> 
                                               
                                             </div>
                                             <div class="f-right">
                                                <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
                                                <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
                                             </div>
                                          </div>
                                          <?php } ?>
                                          <!--  <p>No todos found</p> -->
                                       </div>
                                    </div>
                                 </div>
                                 <div class="tab-pane" id="profile1" role="tabpanel">
                                    <div class="new-task">
                                       <?php
                                          foreach($todolist as $todo){ ?>
                                       <div class="to-do-list sortable-moves">
                                          <div class="to-do-list card-sub" id="1">
                                             <div class="top-disk01">
                                                <label class="custom_checkbox1 check-task <?php if($todo['status']=='completed'){ ?> done-task <?php } ?>">
                                                <input type="checkbox" value="" <?php if($todo['status']=='completed'){ ?> checked="checked" <?php } ?> id="<?php echo $todo['id']; ?>" onclick="todo_status(this)" > <i></i></label>
                                                <!-- <span class="cr">
                                                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                </span>  --> 
                                                <span><?php echo $todo['todo_name']; ?></span> 
                                                 
                                             </div>
                                             <div class="f-right">
                                                <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
                                                <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                       <?php } ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- To Do Card List card end -->
                     </div>
                  </div>
               </div>
               <!-- tab 3-->
               <div id="Clients" class="tab-pane fade">
                
				  
					<div class="invoice-crm1 disk-redesign1">  
				<div class="top-leads fr-task cfssc redesign-lead-wrapper">
				<div class="lead-data1 pull-right leadsG leads_status_count">

				<div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
				<div class="lead-point1">
				<a href="<?php echo base_url(); ?>invoice" target="_blank">
				<strong><?php echo $invoice['invoice_count']; ?></strong>
				<span class="status_filter">Invoices</span>
				</a>
				</div>
				</div>
				
				<div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
				<div class="lead-point1">
				<a href="<?php echo base_url(); ?>invoice" target="_blank">
				<strong><?php echo $approved_invoice['invoice_count']; ?></strong>
				<span class="status_filter">Paid Invoices</span>
				</a>
				</div>
				</div>
				
				<div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
				<div class="lead-point1">
				<a href="<?php echo base_url(); ?>invoice" target="_blank">
				<strong><?php echo $cancel_invoice['invoice_count']; ?></strong>
				<span class="status_filter">Unpaid Invoices</span>
				</a>
				</div>
				</div>
				
				<div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
				<div class="lead-point1">
				<a href="<?php echo base_url(); ?>invoice" target="_blank">
				<strong><?php echo $expired_invoice['invoice_count']; ?></strong>
				<span class="status_filter">Cancelled Invoices</span>
				</a>
				</div>
				</div>

				</div>
				</div>
				</div>
				
						  <div class="card desk-dashboard mydisk-realign1 floating_set">
                    <!--  <div class="revenue-task row">
                         
                        <div class="col-md-6 col-xl-3">
                           <div class="card widget-card-1">
                              <div class="card-block-small">
                                 <i class="icofont icofont-list bg-c-green card1-icon"></i>
                                 <span class="text-c-green f-w-600"><a href="<?php echo base_url(); ?>invoice" target="_blank">Invoices</a></span>
                                  
                                 <h4>
                                    <a href="<?php echo base_url(); ?>invoice" target="_blank"><?php echo $invoice['invoice_count']; ?></a> 
                                 </h4>
                              </div>
                           </div>
                        </div>
                        
                        <div class="col-md-6 col-xl-3">
                           <div class="card widget-card-1">
                              <div class="card-block-small">
                                 <i class="icofont icofont-list bg-c-pink card1-icon"></i>
                                 <span class="text-c-pink f-w-600"><a href="<?php echo base_url(); ?>invoice" target="_blank">Paid Invoices</a></span>
                                 
                                 <h4>
                                    <a href="<?php echo base_url(); ?>invoice" target="_blank">
                                       <?php echo $approved_invoice['invoice_count']; ?> 
                                 </h4>
                              </div>
                           </div>
                        </div>
                        
                        <div class="col-md-6 col-xl-3">
                        <div class="card widget-card-1">
                        <div class="card-block-small yellowclr">
                        <i class="icofont icofont-list bg-c-yellow card1-icon"></i>
                        <span class="text-c-yellow f-w-600"><a href="<?php echo base_url(); ?>invoice" target="_blank">Unpaid Invoices</a></span>
                        
                        <h4>
                        <a href="<?php echo base_url(); ?>invoice" target="_blank"><?php echo $cancel_invoice['invoice_count']; ?></a> 
                        </h4>
                        </div>
                        </div>
                        </div>
                         
                        <div class="col-md-6 col-xl-3">
                           <div class="card widget-card-1">
                              <div class="card-block-small">
                                 <i class="icofont icofont-list bg-c-blue card1-icon"></i>
                                 <span class="text-c-blue f-w-600"><a href="<?php echo base_url(); ?>invoice" target="_blank">Cancelled Invoices</a></span>
                                                        
                                 <h4>
                                    <a href="<?php echo base_url(); ?>invoice" target="_blank"><?php echo $expired_invoice['invoice_count']; ?></a> 
                                 </h4>
                              </div>
                           </div>
                        </div>
                     </div> -->
					 
				
					 
					 
                      
                     <div class="row impo">
						
					<div class="number-mydisk floating_set">	
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="<?php echo base_url(); ?>Firm_dashboard/All_client" target="_blank">
								<span>Number of Client</span>
								<div class="count-pos-01">
									<strong><?php echo $client['client_count']; ?></strong>
									</div>
								</a>
							</div>							
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="<?php echo base_url(); ?>Firm_dashboard/Active_client" target="_blank">
								<span>Number of Active Client</span>
								<div class="count-pos-01">
									<strong><?php echo $active_client['client_count']; ?></strong>
									</div>
								</a>
							</div>							
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="<?php echo base_url(); ?>Firm_dashboard/Inactive_client" target="_blank">
								<span>Non Active Client</span>
								<div class="count-pos-01">
									<strong><?php echo $nonactive_client['client_count']; ?></strong>
									</div>
								</a>
							</div>							
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="<?php echo base_url(); ?>Firm_dashboard/Frozen_client" target="_blank">
								<span>Frozen Clients</span>
								<div class="count-pos-01">
									<strong><?php echo $archive_client['client_count']; ?></strong>
									</div>
								</a>
							</div>							
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="<?php echo base_url(); ?>user" target="_blank">
								<span>New added This month</span>
								<div class="count-pos-01">
									<strong><?php echo $new_client['client_count']; ?></strong>
									</div>
								</a>
							</div>							
						</div>
						
						
					</div>
                      <!--   <div class="job-card count-details lefsec col-xs-12 col-md-6">
                           <div class="card12">
                              <div class="card">
                                 <div class="card-header">
                                    <div class="media">
                                       <div class="media-body media-middle">
                                          <div class="company-name">
                                          <a href="<?php echo base_url(); ?>Firm_dashboard/All_client" target="_blank">
                                             <p>Number of Client</p>
                                             </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="card-block">

                                    <p class="text-muted"> <a href="<?php echo base_url(); ?>Firm_dashboard/All_client" target="_blank"><?php echo $client['client_count']; ?></a></p>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header">
                                    <div class="media">
                                       <div class="media-body media-middle">
                                          <div class="company-name">
                                             <p> <a href="<?php echo base_url(); ?>Firm_dashboard/Active_client" target="_blank">Number of Active Client</a></p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="card-block">
                                    <p class="text-muted"> <a href="<?php echo base_url(); ?>Firm_dashboard/Active_client" target="_blank"><?php echo $active_client['client_count']; ?></a></p>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header">
                                    <div class="media">
                                       <div class="media-body media-middle">
                                          <div class="company-name">
                                             <p> <a href="<?php echo base_url(); ?>Firm_dashboard/Inactive_client" target="_blank">Non Active Client</a></p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="card-block">
                                    <p class="text-muted"> <a href="<?php echo base_url(); ?>Firm_dashboard/Inactive_client" target="_blank"><?php echo $nonactive_client['client_count']; ?></a></p>
                                 </div>
                              </div>
                           </div>
                           <div class="card12 secon">
                              <div class="card">
                                 <div class="card-header">
                                    <div class="media">
                                       <div class="media-body media-middle">
                                          <div class="company-name">
                                             <p> <a href="<?php echo base_url(); ?>Firm_dashboard/Frozen_client" target="_blank">Frozen Clients</a></p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="card-block">
                                    <p class="text-muted"> <a href="<?php echo base_url(); ?>Firm_dashboard/Frozen_client" target="_blank"><?php echo $archive_client['client_count']; ?></a></p>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header">
                                    <div class="media">
                                       <div class="media-body media-middle">
                                          <div class="company-name">
                                             <p><a href="<?php echo base_url(); ?>user" target="_blank">New added This month</a></p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="card-block">
                                    <p class="text-muted"><a href="<?php echo base_url(); ?>user"><?php echo $new_client['client_count']; ?></a></p>
                                 </div>
                              </div>
                           </div>
                        </div> -->
                        <div class="card desk-dashboard fr-bg12 widmar col-xs-12 col-md-6 mydisk-realign1">
                           <div class="card">
                              <div class="card-header">
                                 <h5>Clients</h5>
                              </div>
                              <span class="time-frame"><label>Time Frame:</label>
                              <select id="filter_client">
                                 <option value="year">This Year</option>
                                 <option value="month">This Month</option>
                              </select>
                              <div class="card-block fileters_client">
                                 <div id="chart_Donut11" style="width: 100%; height: 309px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--tab 4-->
               <div id="Proposals" class="tab-pane fade">
                  <!--chart sec -->
                  <div class="row col-12">
                     <div class="card desk-dashboard teo fr-bg12 col-xs-12 col-md-6 mydisk-realign1">
                        <div class="card">
                           <div class="card-header">
                              <h5>Proposals</h5>
                           </div>
                           <span class="time-frame"><label>Time Frame:</label>
                           <select id="proposal">
                              <option value="month">This Month</option>
                              <option value="year">This Year</option>
                           </select>
                           <div class="card-block proposal">
                              <div id="piechart" style="width: 100%; height: 309px;"></div>
                           </div>
                        </div>
                     </div>
                     <!--     <div class="card desk-dashboard teo fr-bg12 widmar col-xs-12 col-md-6">
                        <div class="card">
                         <div class="accident-toggle">
                           <h2>Timesheets</h2>
                         </div>
                         <span class="time-frame"><label>Time Frame:</label>
                         <select>
                           <option>This Month</option>
                           <option>This Year</option>
                         </select>
                         <div class="card-block">
                           <div id="chart_Donut" style="width: 100%; height: 309px;"></div>
                         </div>
                         </div>
                        </div> -->
                  </div>
               </div>
               <!--tab 5-->
               <div id="Deadline-Manager" class="tab-pane fade">
                 
				<!-- <div class="number-mydisk deadline-disk1 floating_set">	
						<div class="count-mydisk1">
							<div class="target-disk">
								<span><a href="<?php echo base_url(); ?>/Firm_dashboard/vat" target="_blank">
								VAT</a></span>
								<ul class="text-muted">
                                 <?php
                                    foreach($oneyear as $date){ 
                                      // echo $date['crm_vat_due_date']."-fgf-";
                                      if($date['crm_vat_due_date']!=''){ ?>
                                 <li><?php echo $date['crm_company_name']; ?>-<?php echo (strtotime($date['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($date['crm_vat_due_date'])): '0'); ?></li>
                                 <?php } } ?>                    
                              </ul>
								<div class="count-pos-01">
									<strong><i class="icofont icofont-presentation-alt st-icon bg-c-yellow"></i></strong>
									</div>
							</div>
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
								<span><a href="<?php echo base_url(); ?>/Firm_dashboard/accounts" target="_blank">
								Accounts</a></span>
								<ul class="text-muted">
                                  <?php
                                    foreach($oneyear as $date){ 
                                      if($date['crm_ch_accounts_next_due']!=''){?>
                                 <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_ch_accounts_next_due']; ?></li>
                                 <?php }} ?>                    
                              </ul>
								<div class="count-pos-01">
									<strong><i class="icofont icofont-presentation-alt st-icon bg-c-yellow"></i></strong>
									</div>
							</div>
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
								<span><a href="<?php echo base_url(); ?>/Firm_dashboard/company_tax" target="_blank">
								Company Tax Returns</a></span>
								 <ul class="text-muted">
                                 <?php
                                    foreach($oneyear as $date){ 
                                      if($date['crm_accounts_tax_date_hmrc']!=''){?>
                                 <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_accounts_tax_date_hmrc']; ?></li>
                                 <?php }} ?> 
                              </ul>
								<div class="count-pos-01">
									<strong><i class="icofont icofont-presentation-alt st-icon bg-c-yellow"></i></strong>
									</div>
							</div>
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
								<span><a href="<?php echo base_url(); ?>/Firm_dashboard/personal_tax" target="_blank">
								Personal Tax Returns</a></span>
								 <ul class="text-muted">
                                   <?php
                                    foreach($oneyear as $date){ 
                                      if($date['crm_personal_due_date_return']!=''){?>
                                 <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_personal_due_date_return']; ?></li>
                                 <?php }} ?> 
                              </ul>
								<div class="count-pos-01">
									<strong><i class="icofont icofont-presentation-alt st-icon bg-c-yellow"></i></strong>
									</div>
							</div>
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
								<span><a href="<?php echo base_url(); ?>/Firm_dashboard/payroll" target="_blank">
								Payroll</a></span>
								 <ul class="text-muted">
                                   <?php
                                    foreach($oneyear as $date){ 
                                      if($date['crm_rti_deadline']!=''){?>
                                 <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_rti_deadline']; ?></li>
                                 <?php }} ?> 
                              </ul>
								<div class="count-pos-01">
									<strong><i class="icofont icofont-presentation-alt st-icon bg-c-yellow"></i></strong>
									</div>
							</div>
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
								<span><a href="<?php echo base_url(); ?>/Firm_dashboard/confirm_statement" target="_blank">
								Confimation Statements</a></span>
								 <ul class="text-muted">
                                  <?php
                                    foreach($oneyear as $date){ 

                                      if($date['crm_confirmation_statement_due_date']!=''){
                                       ?>
                                 <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_confirmation_statement_due_date']; ?></li>
                                 <?php }} ?>  
                              </ul>
								<div class="count-pos-01">
									<strong><i class="icofont icofont-presentation-alt st-icon bg-c-yellow"></i></strong>
									</div>
							</div>
						</div>
						
						
						
					</div>  -->	
				   
                    <div class=" job-card">
                     <div class="col-md-12 col-xl-4">
                        <div class="card widget-statstic-card">
                           <div class="card-header">
                              <div class="media">
                                 <div class="media-body media-middle">
                                    <div class="company-name">
                                       <p><a href="<?php echo base_url(); ?>/Firm_dashboard/vat" target="_blank">VAT</a></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card-block">
                              <i class="icofont icofont-presentation-alt st-icon bg-c-yellow"></i>
                              <ul class="text-muted">
                                 <?php
                                    foreach($oneyear as $date){ 
                                      // echo $date['crm_vat_due_date']."-fgf-";
                                      if($date['crm_vat_due_date']!=''){ ?>
                                 <li><?php echo $date['crm_company_name']; ?>-<?php echo (strtotime($date['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($date['crm_vat_due_date'])): '0'); ?></li>
                                 <?php } } ?>                    
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 col-xl-4">
                        <div class="card widget-statstic-card">
                           <div class="card-header">
                              <div class="media">
                                 <div class="media-body media-middle">
                                    <div class="company-name">
                                       <p><a href="<?php echo base_url(); ?>/Firm_dashboard/accounts" target="_blank">Accounts</a></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card-block">
                              <i class="icofont icofont-files st-icon bg-c-blue"></i>
                              <ul class="text-muted">
                                 <?php
                                    foreach($oneyear as $date){ 
                                      if($date['crm_ch_accounts_next_due']!=''){?>
                                 <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_ch_accounts_next_due']; ?></li>
                                 <?php }} ?> 
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 col-xl-4">
                        <div class="card widget-statstic-card">
                           <div class="card-header">
                              <div class="media">
                                 <div class="media-body media-middle">
                                    <div class="company-name">
                                       <p><a href="<?php echo base_url(); ?>/Firm_dashboard/company_tax" target="_blank">Company Tax Returns</a></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card-block">
                              <i class="icofont icofont-paper st-icon bg-c-green"></i>
                              <ul class="text-muted">
                                 <?php
                                    foreach($oneyear as $date){ 
                                      if($date['crm_accounts_tax_date_hmrc']!=''){?>
                                 <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_accounts_tax_date_hmrc']; ?></li>
                                 <?php }} ?> 
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 col-xl-4">
                        <div class="card widget-statstic-card">
                           <div class="card-header">
                              <div class="media">
                                 <div class="media-body media-middle">
                                    <div class="company-name">
                                       <p><a href="<?php echo base_url(); ?>/Firm_dashboard/personal_tax" target="_blank">Personal Tax Returns</a></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card-block">
                              <i class="icofont icofont-file-document st-icon bg-c-pink"></i>
                              <ul class="text-muted">
                                 <?php
                                    foreach($oneyear as $date){ 
                                      if($date['crm_personal_due_date_return']!=''){?>
                                 <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_personal_due_date_return']; ?></li>
                                 <?php }} ?> 
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 col-xl-4">
                        <div class="card widget-statstic-card">
                           <div class="card-header">
                              <div class="media">
                                 <div class="media-body media-middle">
                                    <div class="company-name">
                                       <p><a href="<?php echo base_url(); ?>/Firm_dashboard/payroll" target="_blank">Payroll</a></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card-block">
                              <i class="icofont icofont-file-text st-icon bg-c-yellow"></i>
                              <ul class="text-muted">
                                 <?php
                                    foreach($oneyear as $date){ 
                                      if($date['crm_rti_deadline']!=''){?>
                                 <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_rti_deadline']; ?></li>
                                 <?php }} ?> 
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 col-xl-4">
                        <div class="card widget-statstic-card">
                           <div class="card-header">
                              <div class="media">
                                 <div class="media-body media-middle">
                                    <div class="company-name">
                                       <p><a href="<?php echo base_url(); ?>/Firm_dashboard/confirm_statement" target="_blank">Confimation Statements</a></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card-block">
                              <i class="icofont icofont-file-document st-icon bg-c-blue"></i>
                              <ul class="text-muted">
                                 <?php
                                    foreach($oneyear as $date){ 

                                      if($date['crm_confirmation_statement_due_date']!=''){
                                       ?>
                                 <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_confirmation_statement_due_date']; ?></li>
                                 <?php }} ?> 
                              </ul>
                           </div>
                        </div>
                     </div>
                      
                  </div> 
                  <!-- fullwidth map -->
                  <div class="floating_set set-timenone">
				  <div class="desk-dashboard widmar floating_set mydisk-realign1">
                     <div class="card-header">
                        <h5>Deadlines</h5>
                     </div>
                     <span class="time-frame"><label>Time Frame:</label>
                     <select id="filter_deadline">
                        <option value="month">This Month</option>
                        <option value="year">This Year</option>
                     </select>
                     <div class="card-block deadline">
                        <div id="columnchart_values" ></div>
                     </div>
					 
                  </div>
                  <!-- fullwidth map-->
               </div>
               <!--tab end-->
            </div>
         </div>
      </div>
      <!-- chart sec -->
   </div>
</div>
<!-- management block -->
<div class="modal fade" id="myTodo" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New Todo's</h4>
         </div>
         <div class="modal-body">
            <label>Todo's Name</label>
            <input type="text" name="todo_name" id="todo_name" value="">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" id="todo_save">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<?php
   foreach($todolist as $todo){
   ?>
<div class="modal fade edittodo_view" id="EditTodo_<?php echo $todo['id']; ?>" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Todos</h4>
         </div>
         <div class="modal-body">
            <input type="hidden" name="id" id="todo_id" value="<?php echo $todo['id']; ?>">
            <input type="text" name="todo_name" class="todo_name" value="<?php echo $todo['todo_name']; ?>">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default save" id="<?php echo $todo['id']; ?>">Save</button>
            <button type="button" class="btn btn-default close" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="DeleteTodo_<?php echo $todo['id']; ?>" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete todos</h4>
         </div>
         <div class="modal-body">
            <p>Do you want to Delete Todo's ?</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default delete" id="<?php echo $todo['id']; ?>">Yes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/todo.js"></script>
<!-- jquery sortable js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Sortable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/sortable-custom.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>

<script src="<?php echo base_url();?>assets/js/RGraph.common.core.js"></script>
<script src="<?php echo base_url();?>assets/js/RGraph.bar.js"></script>
<script src="<?php echo base_url();?>assets/js/RGraph.common.dynamic.js"></script>
<script type="text/javascript">


$( document ).ready(function() {

  

   $(".hide_check").each(function(){
      $(this).last().next('li').trigger('click');
       console.log($(this).last().next('li').attr('class'));
   });

  

   // console.log( "ready!" );
   
   // var sec_id=$(".hide_classes").next().attr('id');
   
   // console.log(sec_id);
   
   // console.log($('#proposal_dashboard a[href="#'+sec_id+'"]').attr('class'));

   // $('#proposal_dashboard a[href="#'+sec_id+'"]').trigger('click');

});


   $(".nav-link").click(function(){
      alert('ok123');
   });   



   $(document).ready(function() {
   
         //   var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
   /*$( ".datepicker" ).datepicker({
          dateFormat: 'yy-mm-dd'
      });*/
   
   $('.tags').tagsinput({
       allowDuplicates: true
      });
      
      $('.tags').on('itemAdded', function(item, tag) {
          $('.items').html('');
   var tags = $('.tags').tagsinput('items');
        
      });
   });
   
   $("#contact_today").click(function(){
     if($("#contact_today").is(':checked')){
       $(".selectDate").css('display','none');
     }else{
       $(".selectDate").css('display','block');
     }
   });
   
   $(".contact_update_today").click(function(){
     var id = $(this).attr("data-id");
     if($(this).is(':checked')){
   
       $("#selectDate_update_"+id).css('display','none');
     }else{
       $("#selectDate_update_"+id).css('display','block');
     }
   });
   
    $("#all_leads").dataTable({
          "iDisplayLength": 10,
       });
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
   
      $( "#leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });
   
   
      $( ".edit_leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });
   });
   
   
   $(".sortby_filter").click(function(){
         $(".LoadingImage").show();
   
      var sortby_val = $(this).attr("id");
      var data={};
      data['sortby_val'] = sortby_val;
      $.ajax({
      url: '<?php echo base_url();?>leads/sort_by/',
      type : 'POST',
      data : data,
      success: function(data) {
        $(".LoadingImage").hide();
   
      $(".sortrec").html(data);   
      $("#all_leads").dataTable({
      "iDisplayLength": 10,
      });
      },
      });
   
   });
   $("#import_leads").validate({
     rules: {
        file: {required: true, accept: "csv"},
   
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},
   
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
   
   });
                            
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
       $('.edit-toggle1').click(function(){
         $('.proposal-down').slideToggle(300);
       });
   
        $("#legal_form12").change(function() {
          var val = $(this).val();
          if(val === "from2") {
            // alert('hi');
              $(".from2option").show();
          }
          else {
              $(".from2option").hide();
          }
          });
   })
</script>
<script>
   $( function() {
   
    $( ".sortable" ).sortable(
   {
         update: function(event, ui) {       
       var arr = [];
     $("div.sortable ").each(function() { 
         var idsInOrder = [];
       // idsInOrder.push($(this).attr('id')) 
        var ul_id = $(this).attr('id');
      //  alert(ul_id);
   
        $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
        idsInOrder.push($(this).attr('id')) 
    });
   // alert(idsInOrder);
   // arr.push({[ul_id]:idsInOrder});   
     arr[ul_id] = idsInOrder;   
    });
   
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/update_type_todolist",
    data:  { 'datas' : arr }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
      $(".LoadingImage").hide();               
    }
    });
   }
      });
    //$( ".sortable" ).disableSelection();
   
   } );
   
   $("#todo_save").click(function(){
   var todo_name=$("#todo_name").val();
   console.log(todo_name);
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/todolist_add",
    data:  { 'todo_name' : todo_name }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
       $("#myTodo").modal('hide');
       $(".LoadingImage").hide();
      var json = JSON.parse(data);
      data=json['content'];      
       $(".sortable").html('');
       $(".sortable").append(data);        
    }
    });
   
   });
   
   function todo_status(todo){
   var id=$(todo).attr('id');
   //alert(id);
    $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/todos_status",
    data:  { 'id' : id}, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){       
       $(".LoadingImage").hide();
       var json = JSON.parse(data);
       data=json['content'];  
       complete_data=json['completed_content'];      
       $(".sortable").html('');
       $(".sortable").append(data); 
       $(".completed_todos").html('');
       $(".completed_todos").append(complete_data);        
     }
   });
   }
   
   $('.save').click(function(){
   
   var id=$(this).attr('id');
   // console.log(id);
   var todo_name= $("div#EditTodo_"+$(this).attr('id')+" .todo_name").val();
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/todolist_edit",
    data:  { 'todo_name' : todo_name,'id':id }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
            $(".LoadingImage").hide();
     // console.log(data);
     $(".edittodo_view").hide();
       $("#EditTodo_"+$(this).attr('id')+"").modal('hide');
       console.log(id);
       $("#EditTodo_"+id).find(".close").trigger('click');
       var json = JSON.parse(data);
       data=json['content'];  
       complete_data=json['completed_content'];      
       $(".sortable").html('');
       $(".sortable").append(data); 
       $(".completed_todos").html('');
       $(".completed_todos").append(complete_data); 
     // location.reload();
    }
    });
   
   //console.log(todo_name);
   
   });
   
   $('.delete').click(function(){
   var id=$(this).attr('id');
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/todolist_delete",
    data:  { 'id':id }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
       $("#DeleteTodo_"+$(this).attr('id')+"").modal('hide');
       $(".LoadingImage").hide();
      location.reload();
    }
    });
   });
   
   $("#filter_client").change(function(){
   if($(this).val()=='month'){
   id=$(this).val();
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/client_filter",
    data:  { 'id':id }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
     // console.log(data);   
      $(".fileters_client").html('');
       $(".fileters_client").append(data);   
       $(".LoadingImage").hide();
     
    }
    });
   
   }else{
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/client_filter_year",
    data:  { 'id':id }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
    //  console.log(data);   
      $(".fileters_client").html('');
       $(".fileters_client").append(data);   
       $(".LoadingImage").hide();
     
    }
    });
   
   }
   });
   $("#filter_deadline").change(function(){
   var id=$(this).val();
   if($(this).val()=='year'){    
      $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/Firm_dashboard/deadline_filter_year",
      data:  { 'id':id }, 
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data){
       // console.log(data);   
        $(".deadline").html('');
         $(".deadline").append(data);   
         $(".LoadingImage").hide();     
      }
      });
   }else{
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/Firm_dashboard/deadline_filter_month",
        data:  { 'id':id }, 
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data){
         // console.log(data);   
          $(".deadline").html('');
           $(".deadline").append(data);   
           $(".LoadingImage").hide();     
        }
        });
   }
   });
   
   $("#proposal").change(function(){
   var id=$(this).val();
   if($(this).val()=='year'){    
      $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/Firm_dashboard/filter_proposal_year",
      data:  { 'id':id }, 
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data){
       // console.log(data);   
        $(".proposal").html('');
         $(".proposal").append(data);   
         $(".LoadingImage").hide();     
      }
      });
   }else{
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/Firm_dashboard/filter_proposal_month",
        data:  { 'id':id }, 
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data){
         // console.log(data);   
          $(".proposal").html('');
           $(".proposal").append(data);   
           $(".LoadingImage").hide();     
        }
        });
   }
   });
   
   
     google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart2);
   
      function drawChart2() {
   
        var data = google.visualization.arrayToDataTable([
          ['Task', 'link','Hours per Day'],
          ['Accepted',  '<?php echo base_url(); ?>Firm_dashboard/accept',  <?php echo $accept_proposal['proposal_count']; ?>],
          ['In Discussion',  '<?php echo base_url(); ?>Firm_dashboard/indiscussion',  <?php echo $indiscussion_proposal['proposal_count']; ?>],
          ['Sent', '<?php echo base_url(); ?>Firm_dashboard/sent', <?php echo $sent_proposal['proposal_count']; ?>],
          ['Viewed', '<?php echo base_url(); ?>Firm_dashboard/view', <?php echo $viewed_proposal['proposal_count']; ?>],
          ['Declined',  '<?php echo base_url(); ?>Firm_dashboard/decline',  <?php echo $declined_proposal['proposal_count']; ?>],
           ['Archived',   '<?php echo base_url(); ?>Firm_dashboard/archieve', <?php echo $archive_proposal['proposal_count']; ?>],
           ['Draft',   '<?php echo base_url(); ?>Firm_dashboard/draft', <?php echo $draft_proposal['proposal_count']; ?>]
        ]);
   
         var view = new google.visualization.DataView(data);
         view.setColumns([0, 2]);
   
        var options = {
          title: 'My Proposal Activities'
        };
   
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
   
         chart.draw(view, options);
   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
   
   
</script>
<script type="text/javascript">
         google.charts.load("current", {packages:["corechart"]});
         google.charts.setOnLoadCallback(drawChart1);
         function drawChart1() {
         var data = google.visualization.arrayToDataTable([
         ['Status','link','Count'],
         ['Active Client','<?php echo base_url(); ?>Firm_dashboard/Active_client',     <?php echo $active['client_count']; ?>],
         ['Inactive Client', '<?php echo base_url(); ?>Firm_dashboard/Inactive_client',     <?php echo $inactive['client_count']; ?>],
         ['Frozen Client', '<?php echo base_url(); ?>Firm_dashboard/Frozen_client', <?php echo $frozen['client_count']; ?>],
         ]);
         var view = new google.visualization.DataView(data);
         view.setColumns([0, 2]);
         var options = {
         title: 'Client Lists',
        //SS pieHole: 0.4,
         };
         var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
         chart.draw(view, options);
         var selectHandler = function(e) {
         //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
         window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
         }
</script>
<p id="myValueHolder"></p>
<script type="text/javascript">
 function drawChart () {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', 'Value');
    
    data.addRows([
      ["Confirmation", <?php echo $confirmation['deadline_count']; ?>],
      ["Accounts", <?php echo $account['deadline_count']; ?>],
      ["Personal Tax", <?php echo $personal_due['deadline_count']; ?>],
      ["VAT ", <?php echo $vat['deadline_count']; ?>],
      ["Payroll", <?php echo $payroll['deadline_count']; ?>],
      ["WorkPlace Pension - AE", <?php echo $pension['deadline_count']; ?>],
      ["CIS", <?php echo $registeration['deadline_count']; ?>],
      ["CIS-Sub", 0],
      ["P11D", <?php echo $plld['deadline_count']; ?>],
      ["Management Accounts", <?php echo $management['deadline_count']; ?>],
      ["Bookkeeping", <?php echo $booking['deadline_count']; ?>],
      ["Investigation Insurance", <?php echo $insurance['deadline_count']; ?>],
      ["Registeration", <?php echo $registeration['deadline_count']; ?>],
      ["Tax Advice", <?php echo $investigation['deadline_count']; ?>],
      ["Tax Investigation", <?php echo $investigation['deadline_count']; ?>]
    ]);

    // Set chart options
   

    
    var chart = new google.visualization.ColumnChart(document.querySelector('#columnchart_values'));
    
    google.visualization.events.addListener(chart, 'select', function () {
        var selection = chart.getSelection();
        if (selection.length) { 
      
            var row = selection[0].row;
            document.querySelector('#myValueHolder').innerHTML = data.getValue(row, 1);
            
            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1, {
                type: 'string',
                role: 'style',
                calc: function (dt, i) {
                 // alert(row);
                 if(i == row){
                  load(row);
               }
                    return (i == row) ? '' : null;
                }
            }]);
            
            chart.draw(view, {
                height: 400,
                width: 600
            });
        }
    });
    
    chart.draw(data, {
        height: 400,
        width: 1200
    });
}
google.load('visualization', '1', {packages:['corechart'], callback: drawChart});

function load(row){
//alert(row);
var index = row;
 switch (index) {
            case 0: window.open('<?php echo base_url(); ?>Firm_dashboard/confirm_statement', '_blank'); break;
            case 1: window.open('<?php echo base_url(); ?>Firm_dashboard/accounts', '_blank'); break;
            case 2: window.open('<?php echo base_url(); ?>Firm_dashboard/personal_tax', '_blank'); break;
            case 3: window.open('<?php echo base_url(); ?>Firm_dashboard/vat', '_blank'); break;
            case 4: window.open('<?php echo base_url(); ?>Firm_dashboard/payroll', '_blank'); break;  
            case 5: window.open('<?php echo base_url(); ?>Firm_dashboard/workplace', '_blank'); break;
            case 6: window.open('<?php echo base_url(); ?>Firm_dashboard/cis', '_blank'); break;
            case 7: window.open('<?php echo base_url(); ?>Firm_dashboard/cis_sub', '_blank'); break;
            case 8: window.open('<?php echo base_url(); ?>Firm_dashboard/p11d', '_blank'); break;
            case 9: window.open('<?php echo base_url(); ?>Firm_dashboard/management', '_blank'); break;
            case 10: window.open('<?php echo base_url(); ?>Firm_dashboard/bookkeeping', '_blank'); break;
            case 11: window.open('<?php echo base_url(); ?>Firm_dashboard/investication', '_blank'); break;  
            case 12: window.open('<?php echo base_url(); ?>Firm_dashboard/register', '_blank'); break;
            case 13: window.open('<?php echo base_url(); ?>Firm_dashboard/tax', '_blank'); break;
            case 14: window.open('<?php echo base_url(); ?>Firm_dashboard/tax_inves', '_blank'); break;  
        }
}
</script>
<script type="text/javascript">
   $(document).ready(function () {
     
     // $( "#tabs112" ).tabs({
   
     // });
   
     // $("#tabs112 ul.md-tabs.tabs112 li").click(function(){
     //  drawChart();
     //   drawChart1();
     //    drawChart2();
       
     // });
   
   
   });
</script>