<?php 
  $this->load->view('includes/new_header');
   $role = $this->Common_mdl->getRole($_SESSION['id']);
   function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
   }
      
      
   ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree_select/style.css?ver=2">
<style>
   /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   /** 04-06-2018 **/


.loadMore {
    padding-bottom: 30px;
    padding-top: 30px;
    text-align: center;
    width: 100%;
}
.loadMore a {
    background: #22b14c;
  border: none;
    font-size: 13px;
    color: #fff;
    min-width: 90px;
    font-weight: 600;
    padding: 5px 10px;
    text-align: center;
    line-height: 19px;
    border-radius: 50px;
}
.loadMore a:hover {
    background-color: #021737;
}


   select.close-test-04{
    display: none;
   }
   /** 04-06-2018 **/
   span.demo {
   padding: 0 10px;
   }
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   img.user_imgs {
   width: 38px;
   height: 38px;
   border-radius: 50%;
   display: inline-block;
   }


a.adduser1 {
    background: #38b87c;
    color: #fff;
    padding: 3px 7px 3px;
    border-radius: 6px;
    vertical-align: middle;
    display: inline-block;
}
   #adduser label {
   text-transform: capitalize;
   font-size: 14px;
   }
  span.hours-left {
    background: #f76480;
    color: #fff;
    padding: 3px 10px;
    border-radius: 3px;
    margin-right: 15px;
}
   .dropdown12 button.btn.btn-primary {
   background: #ccc;
   color: #555;
   font-size: 13px;
   padding: 4px 10px 4px;
   margin-top: -2px;
   border-color:transparent;
   }
   select + .dropdown:hover .dropdown-menu
   {
   display: none;
   }
   select + .dropdown12 .dropdown-menu
   {
   padding: 0 !important;
   }
   select + .dropdown12 .dropdown-menu a {
   color: #000 !important;
   padding: 7px 10px !important;
   border: none !important;
   font-size: 14px;
   }
   select + .dropdown12 .dropdown-menu li {
   border: none !important;
   padding: 0px !important;
   }
   select + .dropdown12 .dropdown-menu a:hover {
   background:#4c7ffe ;
   color: #fff !important;
   }
   span.created-date {
   color: gray;
   padding-left: 10px;
   font-size: 13px;
   font-weight: 600;
   }
   span.created-date i.fa.fa-clock-o {
   padding: 0 5px;
   }
   .dropdown12 span.caret {
   right: -2px;
   z-index: 99999;
   border-top: 4px solid #555;
   border-left: 4px solid transparent;
   border-right: 4px solid transparent;
   border-bottom: 4px solid transparent;
   top: 12px;
   position: relative;
   }
   span.timer {
   padding: 0 10px;
   }
   
   #controls{
   font-size: 12px;
   }
   #time {
   font-size: 150%;
   }
</style>
<div class="modal-alertsuccess alert alert-danger-check succ dashboard_success_message" style="display:none;">
        <div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              Success !!! Status have been changed successfully...
         </div></div>
         </div>
   </div>


<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body task-kan-ban rem-tasks task-kanban-wrapper">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section col-xs-12 floating_set">
                          
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set">
                              <div class="page-heading">Task Kan Ban View</div>
                                 <ul id="task_active_wrap" class="nav nav-tabs all_user1 md-tabs pull-left page-tabs client-top-nav">
                                 <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url(); ?>user/task_list">All Tasks</a>
                                    <div class="slide"></div>
                                 </li>


                                 <?php if ($_SESSION['permission']['Task']['create'] == 1) { ?>
                                    <li class="nav-item">
                                       <a class="nav-link " href="<?php echo base_url(); ?>user/new_task">Create task</a>
                                       <div class="slide"></div>
                                    </li>

                                    <!-- <li class="nav-item">
                                       <a class="nav-link " href="<?php echo base_url(); ?>task">Task Timeline</a>
                                       <div class="slide"></div>
                                    </li> -->

                                 <?php }
                                 if ($_SESSION['user_type'] == 'FA') { ?>
                                    <li class="nav-item">
                                       <a class="nav-link " href="<?php echo base_url(); ?>Task_Status/task_progress_view">Task Progress Status</a>
                                       <div class="slide"></div>
                                    </li>
                                 <?php }

                                 if ($_SESSION['permission']['Task']['edit'] == 1) { ?>
                                    
                                    <li class="nav-item">
                                       <a class="nav-link active" href="<?php echo base_url(); ?>user/task_list_kanban">Switch To Kanban</a>
                                       <div class="slide"></div>
                                    </li>

                                    <li class="nav-item">
                                       <a class="nav-link " href="<?php echo base_url(); ?>user/task_list/archive_task">Archive Task</a>
                                       <div class="slide"></div>
                                    </li>

                                    <li class="nav-item">
                                       <a class="nav-link " href="<?php echo base_url(); ?>user/task_list/complete_task">Completed Task</a>
                                       <div class="slide"></div>
                                    </li>

                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="modal" data-target="#import-task">Import tasks</a>
                                       <div class="slide"></div>
                                    </li>
                                    <?php } ?>
                                 </ul>
								 <div class="row for_task_serach">
                          <input type="text" name="for_kan_search" id="for_kan_search" class="for_kanban_search form-control" placeholder="Search Task">

                            <div class="dropdown-sin-25">
                            <select class="for_kanban_select" > 
                                   <?php foreach ($user_list as $key => $value) { ?>
                                     <option value="<?php echo $value['crm_name']; ?>" ><?php echo $value['crm_name']; ?></option>
                                    
                                 <?php   } ?>
                                                         
                                          </select>
                                    </div> 
                           </div>
                    </div>
<!-- success message -->
            <div class="modal-alertsuccess alert alert-success" style="display:none;"><div class="newupdate_alert">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                          <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                Archieved Successfully
                              </div></div>
                           </div>
            </div>
<!-- end of success message -->

          <!-- switch kanban -->
            <div id="kanban_tab" class="tskkanban">

               <div class="lead-summary2 floating_set">
             
                  <!--success message -->
                  <div class="modal-alertsuccess  alert status_succ status_msg" style="display: none;">
                    <div class="newupdate_alert  borderless-card" id="timeout">
                     
                    <div class="pop-realted1   success-breadcrumb"><div class="position-alert1 breadcrumb-header"><span>Success !!! Status have been changed successfully...</span></div></div></div></div>
                  <!-- success message -->
                  
                  <div class="lead-summary3 floating_set  sortby_kanban new-change-summary">
                  <div class="page-data-block kan-ban-view">
			
                     <?php

                     // $task_status=array("notstarted"=>"Not Started","inprogress"=>"In progress","awaiting"=>"Awaiting Feedback","complete"=>"Complete");
                     //print_r($task_list);
                     $status_icons = array("fa fa-ban","fa fa-clock-o","fa fa-comment","fa fa-file-archive-o","fa fa-check");
                      foreach($task_status as $ts_key => $ts_value) 
                      {  
                      $current_value= $ts_value['id'];
                      $leads_rec=array();
                        $leads_rec= array_filter($task_list, function ($var) use ($current_value) {
                        // echo $current_value;
                        return ($var['task_status'] == $current_value);
                          });
                       // print_r($leads_rec);
                            ?>

                     <div class="new-row-data1 width-junk col-sm-3">
					             <div class="new-quote-lead kan-test-task search_class">
                              <div class="junk-box"> 
                              
                                 <!-- <div class="trial-stated-title">
                                 <input type="hidden" name="" id="<?php echo $current_value;?>_status" value="<?php echo count($leads_rec); ?>">
                                    <h4> <?php echo $ts_value['status_name'];?><div class="<?php echo $current_value;?>_count_div">  <?php echo count($leads_rec); ?>  </div> </h4>
                                 </div> -->
                                 <div class="kanban-task-test">
                                    <div>
                                       <i class="<?php echo $status_icons[$ts_key] ?>" aria-hidden="true"></i> <span><?php echo $ts_value['status_name'];?></span>
                                    </div>
                                    <span class="<?php echo $current_value;?>_count_div kanban-status-name"><?php echo count($leads_rec); ?></span>
                                 </div>
                                 <!-- <input class="form-control form-control-lg search-criteria" type="text" id="search-criteria" placeholder="Search notes">  -->
                                                    
                                 <div class="back-set02 draglist" id="sit-drag_<?php echo $current_value;?>">                                       
                                    <?php 
                                       $ts_status = $ts_key;

                                    // print_r($leads_rec);

                                       if(!empty($leads_rec)){

                                       foreach (array_values($leads_rec) as $leads_rec_key => $value) {

                                       if($leads_rec_key < 50) {
                                          $getUserProfilepic = $this->Common_mdl->getUserProfilepic($value['worker']);                                  
                                          ?>
                                    <div class="user-leads1 kanban_block <?php echo $value['subject'];?>" id="<?php echo $value['id'] ?>" >
                                 
                                       <h3> 
                                          <a href="<?php echo base_url().'user/task_details/'.$value['id'];?>">
                                             <span class="search_word"><?php echo $value['subject'];?> </span>
                                          </a>
                                          <div class="kanban-icon">
                                             <!-- <img src="<?php echo $getUserProfilepic;?>" alt="img"> -->
                                             <span></span>
                                             <span></span>
                                             <span></span>
                                          </div>
                                       </h3>
                                       <div class="kanban-detail-data">
                                          <div class="kanban-title-month">
                                             <h3>
                                                <span class="search_word"><?php 
                                                   $ex_val= $this->Common_mdl->get_module_user_data('TASK',$value['id']);
                                                   //$ex_val=Get_Module_Assigees( 'TASK' ,$value['id'] );
                                                   $var_array=array();
                                                      if(count($ex_val)>0)
                                                         {
                                                            foreach ($ex_val as $key => $value1) {
                                                         
                                                            $user_name = $this->Common_mdl->select_record('user','id',$value1);
                                                            array_push($var_array, $user_name['crm_name']);
                                                            }
                                                            
                                                         }
                                                         $Assignee= implode(',',$var_array);
                                                         echo $Assignee
                                                ;?> </span>
                                             </h3> 

                                             <div class="source-set1 source-set3 ">
                                                <input type="hidden" id="status_<?php echo $value['id'] ?>" name="" value="<?php echo $current_value;?>" >
                                                <span class="search_word">Created <?php  $date=date('Y-m-d H:i:s',$value['created_date']);
                                                   echo time_elapsed_string($date);?>
                                                </span>
                                             </div>                        
                                          </div>

                                          <div class="source-google">
                                             <div class="source-set1">                                          
                                             </div>                                          
                                          </div>
                                          <!-- <div class="src-comment">
                                             <?php 
                                             $tsk_res=$this->db->query("select * from task_comments where task_id=".$value['id']."")->result_array();
                                             ?>
                                             <span><i class="fa fa-comments" aria-hidden="true"></i><p class="countclsnew"><?php echo count($tsk_res);?></p></span>
                                             
                                             
                           
                                             <div class="tinytiny">
                                                <strong class="search_word">
                                                <?php  $tag =explode(',', $value['tag']);
                                                   foreach ($tag as $t_key => $t_value) { ?>
                                                <a href="javascript:;"><?php if($t_value==2){ echo "To DO"; }if($t_value==1){ echo "Bug";} ?></a><?php } ?>
                                                </strong> 
                                             </div> -->
                                             <!-- end 18-04-2018 -->
                                          <!-- </div> -->
                                       </div>
                                       <!-- <div class="kanban-img">
                                          <img class="kanban-img-bot" src="<?php echo $getUserProfilepic;?>" alt="img">
                                       </div> -->
                                    </div>
                                    <?php }
                                    }
                                 }else{ ?> 
                              
                                    <?php } ?>

                                    <div class="view_more_packages_<?php echo $ts_value['id'] ?>">
                  
                                    </div>

                                    <input type="hidden" class="pac_limit" id="pac_limit_<?php echo $ts_value['id'] ?>" value="50">
                                    <input type="hidden" class="pac_start" id="pac_start_<?php echo $ts_value['id'] ?>" value="0">
                                    <?php if(count($leads_rec) > 50){ ?>
                                    <div class="loadMore" style="">
                                    <a href="javascript:;">Load More</a>
                                 </div>
                                 <?php } ?>
                                 </div>
                              </div>
                           </div>
                     </div>
                     <?php } ?>
                  </div>
               </div>
				  </div>
           
            </div>
        <!-- end of switch kanban -->            
                        </div>
                        <!-- Register your self card end -->
                     </div>
                  </div>
               </div>
               <!-- Page body end -->
            </div>
         </div>
         <!-- Main-body end -->
         <div id="styleSelector">
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>



<div class="modal fade" id="import-task" role="dialog">
<div class="modal-dialog modal-import-dialog">

<!-- Modal content-->
   <div class="modal-content">
      <div class="modal-header modal-import-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Import Task</h4>
      </div>
      <div class="modal-body">
      </div>
      <!-- <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
      </div>
   </div>
</div>

<?php
   $data = $this->db->query("SELECT * FROM update_client")->result_array();
   foreach($data as $row) { ?>
<!-- Modal -->
<div id="myModal<?php echo $row['user_id'];?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">CRM-Updated fields</h4>
         </div>
         <div class="modal-body">
            <?php if(isset($row['company_name'])&&($row['company_name']!='0')){?>
            <p>Company Name: <span><?php echo $row['company_name'];?></span></p>
            <?php } ?> 
            <?php if(isset($row['company_url'])&&($row['company_url']!='0')){?>
            <p>Company URL: <span><?php echo $row['company_url'];?></span></p>
            <?php } ?>
            <?php if(isset($row['officers_url'])&&($row['officers_url']!='0')){?>
            <p>Officers URL: <span><?php echo $row['officers_url'];?></span></p>
            <?php } ?>
            <?php if(isset($row['incorporation_date'])&&($row['incorporation_date']!='0')){?>
            <p>Incorporation Date: <span><?php echo $row['incorporation_date'];?></span></p>
            <?php } ?>
            <?php if(isset($row['register_address'])&&($row['register_address']!='0')){?>
            <p>Registered Address: <span><?php echo $row['register_address'];?></span></p>
            <?php }  ?>
            <?php if(isset($row['company_status'])&&($row['company_status']!='0')){?>
            <p>Company Status: <span><?php echo $row['company_status'];?></span></p>
            <?php }  ?>
            <?php if(isset($row['company_type'])&&($row['company_type']!='0')){?>
            <p>Company Type: <span><?php echo $row['company_type'];?></span></p>
            <?php } ?>
            <?php if(isset($row['accounts_periodend'])&&($row['accounts_periodend']!='0')){?>
            <p>Accounts Period End: <span><?php echo $row['accounts_periodend'];?></span></p>
            <?php } ?>
            <?php if(isset($row['hmrc_yearend'])&&($row['hmrc_yearend']!='0')){?>
            <p>HMRC Year End: <span><?php echo $row['hmrc_yearend'];?></span></p>
            <?php } ?>
            <?php if(isset($row['ch_accounts_next_due'])&&($row['ch_accounts_next_due']!='0')){?>
            <p>CH Accounts Next Due: <span><?php echo $row['ch_accounts_next_due'];?></span></p>
            <?php } ?>
            <?php if(isset($row['confirmation_statement_date'])&&($row['confirmation_statement_date']!='0')){?>
            <p>Confirmation Statement Date: <span><?php echo $row['confirmation_statement_date'];?></span></p>
            <?php } ?>
            <?php if(isset($row['confirmation_statement_due_date'])&&($row['confirmation_statement_due_date']!='0')){?>
            <p>Confirmation Statement Due: <span><?php echo $row['confirmation_statement_due_date'];?></span></p>
            <?php } ?>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- /.modal -->
<?php } ?>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<!-- <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>  -->
<script src="<?php echo base_url();?>js/jquery.workout-timer.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<!-- <script type="text/javascript" language="javascript" src="http://remindoo.org/CRMTool/assets/js/dataTables.responsive.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script>
   
   
         function humanise (diff) {
   
   // The string we're working with to create the representation 
   var str = '';
   // Map lengths of `diff` to different time periods 
   var values = [[' year', 365], [' month', 30], [' day', 1]];
   // Iterate over the values... 
   for (var i=0;i<values.length;i++) {
   var amount = Math.floor(diff / values[i][1]);
   // ... and find the largest time value that fits into the diff 
   if (amount >= 1) { 
   // If we match, add to the string ('s' is for pluralization) 
   str += amount + values[i][0] + (amount > 1 ? 's' : '') + ' '; 
   // and subtract from the diff 
   diff -= amount * values[i][1];
   }
   else
   {
   str += amount + values[i][0]+' ';
   }
   
   } return str; } 
     
</script>
<?php
   /*  foreach ($task_list as $key => $value) {
      # code...
   
     $s_date = $value['start_date'];
     $d = date("F j, Y G:i:s",strtotime($s_date));
   //echo $d;die;
   }*/
   ?>

<script>
   

   function file_download(data)
   {
   var type = data.d_type;
   var status = data.status;
   var priority = data.priority;
   if(type=='excel')
   {
   window.location.href="<?php echo base_url().'user/task_excel?status="+status+"&priority="+priority+"'?>";
   }else if(type=='pdf')
   {
   window.location.href="<?php echo base_url().'user/task_pdf?status="+status+"&priority="+priority+"'?>";
   }else if(type=='html')
   {
   window.open(
   "<?php echo base_url().'user/task_html?status="+status+"&priority="+priority+"'?>",
   '_blank' // <- This is what makes it open in a new window.
   );
   // window.location.href="<?php echo base_url().'user/task_html?status="+status+"&priority="+priority+"'?>";
   }
   }
   
   $(function() {
    
    var hours = minutes = seconds = milliseconds = 0;
    var prev_hours = prev_minutes = prev_seconds = prev_milliseconds = undefined;
    var timeUpdate;
   
    // Start/Pause/Resume button onClick
    $("#start_pause_resume").button().click(function(){
        // Start button
        if($(this).text() == "Start"){  // check button label
            $(this).html("<span class='ui-button-text'>Pause</span>");
            updateTime(0,0,0,0);
        }
    // Pause button
        else if($(this).text() == "Pause"){
            clearInterval(timeUpdate);
            $(this).html("<span class='ui-button-text'>Resume</span>");
        }
    // Resume button    
        else if($(this).text() == "Resume"){
            prev_hours = parseInt($("#hours").html());
            prev_minutes = parseInt($("#minutes").html());
            prev_seconds = parseInt($("#seconds").html());
            prev_milliseconds = parseInt($("#milliseconds").html());
            
            updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds);
            
            $(this).html("<span class='ui-button-text'>Pause</span>");
        }
    });
    
    // Reset button onClick
    $("#reset").button().click(function(){
        if(timeUpdate) clearInterval(timeUpdate);
        setStopwatch(0,0,0,0);
        $("#start_pause_resume").html("<span class='ui-button-text'>Start</span>");      
    });
    
    // Update time in stopwatch periodically - every 25ms
    function updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds){
        var startTime = new Date();    // fetch current time
        
        timeUpdate = setInterval(function () {
            var timeElapsed = new Date().getTime() - startTime.getTime();    // calculate the time elapsed in milliseconds
            
            // calculate hours                
            hours = parseInt(timeElapsed / 1000 / 60 / 60) + prev_hours;
            
            // calculate minutes
            minutes = parseInt(timeElapsed / 1000 / 60) + prev_minutes;
            if (minutes > 60) minutes %= 60;
            
            // calculate seconds
            seconds = parseInt(timeElapsed / 1000) + prev_seconds;
            if (seconds > 60) seconds %= 60;
            
            // calculate milliseconds 
            milliseconds = timeElapsed + prev_milliseconds;
            if (milliseconds > 1000) milliseconds %= 1000;
            
            // set the stopwatch
            setStopwatch(hours, minutes, seconds, milliseconds);
            
        }, 25); // update time in stopwatch after every 25ms
        
    }
    
    // Set the time in stopwatch
    function setStopwatch(hours, minutes, seconds, milliseconds){
        $("#hours").html(prependZero(hours, 2));
        $("#minutes").html(prependZero(minutes, 2));
        $("#seconds").html(prependZero(seconds, 2));
        $("#milliseconds").html(prependZero(milliseconds, 3));
    }
    
    // Prepend zeros to the digits in stopwatch
    function prependZero(time, length) {
        time = new String(time);    // stringify time
        return new Array(Math.max(length - time.length + 1, 0)).join("0") + time;
    }
   });
   
   
   function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
   }
   

   // Set the date we're counting down to
   var countDownDate = new Date("Sep 4, 2017 15:37:25").getTime();
   
   // Update the count down every 1 second
   var x = setInterval(function() {
   
    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    //alert(distance);
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    //document.getElementById("demo").innerHTML = days + "d " + hours + "h "
   var result = humanise(days) + hours + "h "
    + minutes + "m " + seconds + "s ";
    // console.log(result);
    $(".demos").html(result);
   
   
    
    // If the count down is over, write some text 
    if (distance < 0) {
     // alert('fff');
      //  clearInterval(x);
        $(".demos").html('EXPIRED');
        //document.getElementById("demo").innerHTML = "EXPIRED";
    }
   }, 1000);
</script>
<!--  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/> -->
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.workout-timer.css" />
<!-- Example-page styles -->
<style>
   body {
   margin-bottom: 40px;
   background-color:#ECF0F1;
   }
   .event-log {
   width: 100%;
   margin: 20px 0;
   }
   .code-example {
   margin: 20px 0;
   }
</style>
<!-- Syntax highlighting -->
<!--   <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.8.0/styles/default.min.css"> -->
<!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.8.0/highlight.min.js"></script>-->

<script>
   // Syntax highlighting
   $('pre code').each(function(i, block) {
     hljs.highlightBlock(block);
   });
   
   
</script><script type="text/javascript">
   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-36251023-1']);
   _gaq.push(['_setDomainName', 'jqueryscript.net']);
   _gaq.push(['_trackPageview']);
   
   (function() {
     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
   })();
   
</script>
<script type="text/javascript">



  $( document ).ready(function() {
  
   //  $.ajax( {
  
   //     type:"post",
   //     dataType : "json",
   //     url:"<?php echo base_url();?>user/get_projects",
   //     processData: false,
   //     contentType: false,
   //      data:{ 
   //       'get':'get'
   //     }
   //     ,
   //     success:function(response)
   //     {
  
  
   //     var ret='<div id="frame_projects"><label>Projects</label><select name="r_project" class="form-control" id="r_projects" placeholder="select">';
  
   //         $.each(response, function (index, value){
   //         ret+='<option value='+value.id+'>'+value.project_name+'</option>';
   //         });
  
   //       ret+='</select><div>';
  
   //      $("#base_projects").before(ret);
   //     }
   // });
  
  
      var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  
      // Multiple swithces
      var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
  
      elem.forEach(function(html) {
          var switchery = new Switchery(html, {
              color: '#1abc9c',
              jackColor: '#fff',
              size: 'small'
          });
      });
  
      $('#accordion_close').on('click', function(){
              $('#accordion').slideToggle(300);
              $(this).toggleClass('accordion_down');
      });
  
      $('.dropdown-sin-25').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
      });
       $('.dropdown-sin-27').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
      });
        $('.dropdown-sin-29').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
      });
      
  });
</script>

  <script type="text/javascript">
  $(document).ready(function () {
   
    $(function () {
     // alert('zz');
        $(".draglist").sortable({
            connectWith: ".draglist",
            cursor: "move"
        }).disableSelection();
        
        $(".draglist").sortable(
        {
           update: function( event, ui ) 
           {              
               if (this === ui.item.parent()[0]) 
               {                  
              //   alert(ui.item[0].id);
                  task_id=ui.item[0].id;
                var id = $(this).attr("id");
                var array = id.split('_');
                var task_status_name=array[1];
              
               // var task_arr = <?php echo json_encode($task_status); ?>;
                //console.log(ui.item);
              //  alert(lead_status_id);
                var data = {};
                var source_status=$('#status_'+task_id).val();
                var val1=$('#'+task_status_name+'_status').val();
                var val2=$('#'+source_status+'_status').val();
               
                
              //  var lead_id =leads_id;
              
                   data['task_id'] = task_id;
               data['status'] = task_status_name;
              $(".LoadingImage").show();
                   $.ajax({
                      type: "POST",
                      url: "<?php echo base_url();?>User/update_task_status_kanban/",
                      data: data,
                      success: function(response) {

                       var result_desval=parseInt(val1)+parseInt(1);
                        var result_sorceval=parseInt(val2)-parseInt(1);

                        // console.log(val1);
                        // console.log(val2);
                        //   console.log(result_desval);
                        //     console.log(result_sorceval);

                        $('#status_'+task_id).val(task_status_name);
                       $('#'+task_status_name+'_status').val(result_desval);
                       $('#'+source_status+'_status').val(result_sorceval);
                       $('.'+task_status_name+'_count_div').html('').html(result_desval);
                       $('.'+source_status+'_count_div').html('').html(result_sorceval);
                       $(".LoadingImage").hide();
                      
                         $('.dashboard_success_message').show();
                setTimeout(function(){ $('.dashboard_success_message').hide(); }, 2000);
                                        
                      },
                   });
                                               
               }            
           }
        });

    });


});
</script>

<script type="text/javascript">
   $(".kanban-icon").click(function(){
      $(this).parent().parent().find(".kanban-detail-data").slideToggle();
});
</script>

    <script type="text/javascript">
        $('.for_kanban_search').on('keyup',function(){
            //alert('121');
            var txt=$(this).val();
            $('.kanban_block').each(function(){
                   if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
                        $(this).show();
                 
                       //$(this).unwrap();

                   }
                   else
                   {
                     $(this).hide();
                
                   }
                });

        });


          $('.for_kanban_select').on('change',function(){
            //alert('121');
            var txt=$(this).val();
            $('.kanban_block').each(function(){
                   if($(this).text().indexOf(txt) != -1){
                        $(this).show();
                 
                       //$(this).unwrap();

                   }
                   else
                   {
                     $(this).hide();
                
                   }
                });

        });


  $('.search-criteria').keyup(function(){
      //$('.box_section').hide();
      var txt = $(this).val();
      $(this).parents('.new-row-data1').find('.kanban_block').hide();
      $(this).parents('.new-row-data1').find('.search_word').each(function(){
         if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
             $(this).parents('.kanban_block').show();  
         }
      });
  });


    $(document).on('click','.loadMore',function(e){

      var parent=$(this).parent('.draglist');

        var id=parent.find('.pac_start').attr('id');
        id=id.split("_");

      // alert(id[2]);
        var ext_lmt=parent.find('.pac_limit').val();
        var ext_start=parent.find('.pac_start').val();

    // var ext_lmt = $('#pac_limit').val();
    // var ext_start = $('#pac_start').val();
       var new_start = parseInt(ext_start)+parseInt(ext_lmt);
       var res="<?php echo $ass_res ?>";
    // // alert(ext_lmt);
    // // alert(new_start);
    // $('body').addClass('openloader');

    $.ajax({
      url:"<?php echo base_url();?>Task/get_list_kanban",
      data:{'limit':ext_lmt,'start':new_start,'status':id[2],res:res},
      type:"POST",
       dataType: 'json',
      success:function(data){
        //console.log(data)
       // setTimeout(function(){ $('body').removeClass('openloader'); }, 2000);
        if(data!=0){
          //$('#pac_limit').val(ext_lmt);
          parent.find('.pac_start').val(new_start);
         parent.find('.view_more_packages_'+id[2]).append(data.holdproduct);
        }else{
           parent.find('.loadMore').hide();
        }
      }
    });

  });

    </script>
<?php $this->load->view('tasks/import_task'); ?>