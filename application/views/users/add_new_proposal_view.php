<?php $this->load->view('includes/header');?>



<section class="client-details-view various-section01 floating_set">
	<div class="client-inform-data1 floating_set">
		<h2><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add New Proposal</h2>
		
		<div class="proposal-data1 floating_set">
		<form>	
		<div class="floating_set data-float01">	
			<div class="col-sm-6 proposal-form-left">
				<div class="form-group">
					<label>* Subject</label>
					<input type="text" name="subject">
				</div>
				<div class="form-group">
					<label>* Related</label>
					<select><option>Customer</option><option>Lead</option></select>
				</div>
				
				<div class="full-calendar-time1">
				<div class="form-group  date_birth">
				<label>* Date</label>
				<div class="form-birth05">
				<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
				<input class="form-control datepicker fields" type="text" name="date_of_creation" id="date_of_creation1" placeholder="Incorporation date" value=""/>
				</div>
				</div> 
					<div class="form-group  date_birth">
				<label>Open Till</label>
				<div class="form-birth05">
				<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
				<input class="form-control datepicker fields" type="text" name="date_of_creation" id="date_of_creation2" placeholder="Incorporation date" value=""/>
				</div>
				</div> 

				<div class="form-group">
					<label>* Currency</label>
					<select class="selectpicker" data-live-search="true" >
					<option>USD</option>
					<option>EUR </option>
					</select>
				</div>	
				<div class="form-group">
					<label>Discount Type</label>
					<select><option>No Discount</option><option>Lead</option></select>
				</div>
			</div>
				<div class="tags-allow1">
					<h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
					<input type="text" value="" class="tags" />
				</div>	
				<div class="form-group">
					<label>Allow Comments</label>
					 <input type="checkbox" class="js-small f-right"  data-id="cons">
				</div>
			</div>		 

			<div class="col-sm-6 proposal-form-right">
					<div class="full-calendar-time1">
						<div class="form-group">
					<label>Status</label>
					<select><option>Draft</option><option>Lead</option></select>
				</div>
						<div class="form-group">
					<label>Assigned</label>
					<select class="selectpicker" data-live-search="true" >
					<option>Alabama</option>
					<option>Alaska </option>
					</select>
				</div>
			</div>	
				<div class="form-group">
					<label>To</label>
					<input type="text" name="to">
				</div>
				<div class="form-group">
					<label>Address</label>
					<textarea rows="3"></textarea>
				</div>

				<div class="full-calendar-time1">
						<div class="form-group">
					<label>City</label>
					<input type="text" name="to">
				</div>
						<div class="form-group">
					<label>State</label>
					<input type="text" name="to">
				</div>
						<div class="form-group">
					<label>Country</label>
					<select class="selectpicker" data-live-search="true" >
					<option>India</option>
					<option>Australia </option>
					</select>
				</div>
						<div class="form-group">
					<label>Zip Code</label>
					<input type="text" name="to">
				</div>
				<div class="form-group">
					<label>* Email</label>
					<input type="text" name="to">
				</div>
				<div class="form-group">
					<label>Phone</label>
					<input type="text" name="to">
				</div>
			</div>	
		</div>
		</div>	


		<div class="form-submit-bt01 text-right">
				<button type="button" name="button">Save & Send</button>
				<button type="submit" name="button">Save</button>
		</div>	






				</form>
				</div>
					
					
			
	
</div>
</section>			


<?php $this->load->view('includes/footer');?>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
 <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>


<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
	$('.tags').tagsinput({
    	allowDuplicates: true
    });
    
    $('.tags').on('itemAdded', function(item, tag) {
        $('.items').html('');
		var tags = $('.tags').tagsinput('items');
      
    });


    $( function() {
    $('#date_of_creation1,#date_of_creation2').datepicker();
  } );

});
</script>