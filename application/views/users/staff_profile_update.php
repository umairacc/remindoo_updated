<?php $this->load->view('includes/header');
   $userid = $this->session->userdata('id');
   
   if(isset($_GET['action']) && $_GET['action']=="time_card")
   {
      $css_time_active = 'in active';
      $css_bank_active = '';
      $bank_active = '';
      $time_active = 'active';
   }else{
      $css_bank_active = 'in active';
      $css_time_active = '';
      $bank_active = 'active';
      $time_active = '';
   }
  /** for us 09-08-2018 **/
function time_to_sec($time) {
list($h, $m, $s) = explode (":", $time);
$seconds = 0;
$seconds += (intval($h) * 3600);
$seconds += (intval($m) * 60);
$seconds += (intval($s));
return $seconds;
}
function sec_to_time($sec) {
return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}
function calculate_test($a,$b){
$difference = $b-$a;

$second = 1;
$minute = 60*$second;
$hour   = 60*$minute;

$ans["hour"]   = floor(($difference)/$hour);
$ans["minute"] = floor((($difference)%$hour)/$minute);
$ans["second"] = floor(((($difference)%$hour)%$minute)/$second);
//echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

$test=$ans["hour"].":".$ans["minute"].":".$ans["second"];
return $test;
}
/** end of 09-08-2018 **/
   function getStartAndEndDate($week, $year) {
    $dateTime = new DateTime();
    $dateTime->setISODate($year, $week);
    $result['start_date'] = $dateTime->format('d-m-Y');
    $dateTime->modify('+6 days');
    $result['end_date'] = $dateTime->format('d-m-Y');
    return $result;
   }
   
   function getDatesFromRange($start, $end, $format = 'Y-m-d') {
      $array = array();
      $interval = new DateInterval('P1D');
   
      $realEnd = new DateTime($end);
      $realEnd->add($interval);
   
      $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
   
      foreach($period as $date) { 
          $array[] = $date->format($format); 
      }
   
      return $array;
   }
   /*function isWeekend($date) {
      $weekDay = date('w', strtotime($date));
      return ($weekDay == 0 || $weekDay == 6);
   }*/
   function isWeekend($date) {
      return (date('N', strtotime($date)) == 7);
   }
   $notstarted = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','notstarted');
          $inprogress = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','inprogress');
          $awaiting = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','awaiting');
          $testing = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','testing');
          $complete = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','complete');
          
          $role = $this->Common_mdl->getRole($_SESSION['id']);
          //print_r($task_list);
   
          $assignNotstarted = 0;
          $assignInprogress = 0;
          $assignAwaiting = 0;
          $assignTesting = 0;
          $assignComplete = 0;
   
          foreach ($task_list as $key => $value) {
              $staffs = $value['worker'].','.$value['manager'];
   
              $explode_worker=explode(',',$staffs);
   
              if($value['task_status']=='notstarted') {
                  if(in_array( $_SESSION['id'] ,$explode_worker )){
                      $assignNotstarted++;
                  }
              }
              if($value['task_status']=='inprogress') {
                  if(in_array( $_SESSION['id'] ,$explode_worker )){
                  $assignInprogress++;
                  }
              }
              if($value['task_status']=='awaiting') {
                  if(in_array( $_SESSION['id'] ,$explode_worker )){
                  $assignAwaiting++;
                  }
              }
              if($value['task_status']=='testing') {
                  if(in_array( $_SESSION['id'] ,$explode_worker )){
                  $assignTesting++;
                  }
              }
              if($value['task_status']=='complete') {
                  if(in_array( $_SESSION['id'] ,$explode_worker )){
                  $assignComplete++;
                  }
              }
   
          }
   
          function convertToHoursMins($time, $format = '%02d:%02d') {
      if ($time < 1) {
          return;
      }
      $hours = floor($time / 60);
      $minutes = ($time % 60);
      return sprintf($format, $hours, $minutes);
   }
   $exp_t =  convertToHoursMins($tot_tim);
   $ex_timer = explode(':', $exp_t);
   
    $type = CAL_GREGORIAN;
   $workdays = array();
   
   $month = date('n'); // Month ID, 1 through to 12.
      $year = date('Y'); // Year in 4 digit 2009 format.
      $day_count = cal_days_in_month($type, $month, $year); 
   
   for ($i = 1; $i <= $day_count; $i++) {
   
          $date = $year.'/'.$month.'/'.$i; //format date
          $get_name = date('l', strtotime($date)); //get week day
          $day_name = substr($get_name, 0, 3); // Trim day name to 3 chars
   
          //if not a weekend add day to array
          //if($day_name != 'Sun' && $day_name != 'Sat'){
          if($day_name != 'Sun'){
              $workdays[] = $i;
          }
   
   }
   $w_days = count($workdays);
   
   //print_r(count($workdays));
   ?>
<link href="<?php echo  base_url()?>assets/css/c3.css" rel="stylesheet" type="text/css">
<link href="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/demo/MonthPicker.min.css" rel="stylesheet" type="text/css">
<link href="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/test/test.css" rel="stylesheet" type="text/css">
<div class="pcoded-content card desk-dashboard">
   <div class="inner-tab123" id="tabs112">
      <div class="staff-profile-cnt floating_set">
         <div class="deadline-crm1 floating_set" >
            <ul class="md-tabs tabs112 floating_set" id="proposal_dashboard">
               <li class="nav-item">
                  <a class="nav-link" href="#Basics">Basics
                  </a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#Tasks">Tasks
                  </a>
               </li>
               <li class="nav-item">
                  <a class="nav-link"  href="#Proposals">Proposals
                  </a>
               </li>
               <li class="nav-item">
                  <a class="nav-link"  href="#Deadline-Manager">Deadline Manager
                  </a>
               </li>
               <li class="nav-item">
                  <a class="nav-link"  href="#Services">Services
                  </a>
               </li>
            </ul>
         </div>
         <div class="for-sep12">
            <!-- tab1 -->
            <div id="Basics" class="proposal_send">
               <!-- basics -->
               <div class="row">
                  <div class="col-md-12 col-xl-6">
                     <div class="card">
                        <div class="card-block user-detail-card">
                           <div class="row">
                              <div class="col-sm-4">
                                 <div class="height-profile">
                                    <div class="height-profile1"> <?php  $getUserProfilepic = $this->Common_mdl->getUserProfilepic($userid); ?><img src="<?php echo $getUserProfilepic;?> " alt="" class="img-fluid p-b-10"></div>
                                 </div>
                                 <!-- <div class="contact-icon">
                                    <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="View" ><i class="icofont icofont-eye m-0"></i></button>
                                    <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Print" ><i class="icofont icofont-printer m-0"></i></button>
                                    <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Download" ><i class="icofont icofont-download-alt m-0"></i></button>
                                    <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Share" ><i class="icofont icofont-share m-0"></i></button>
                                    </div> -->
                              </div>
                              <div class="col-sm-8 user-detail">
                                 <div class="row">
                                    <div class="col-sm-5">
                                       <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Name :</h6>
                                    </div>
                                    <div class="col-sm-7">
                                       <h6 class="m-b-30"><?php 
                                          $user_role=$this->Common_mdl->select_record('user','id',$userid);
                                          $role_name=$this->Common_mdl->select_record('Role','id',$user_role['role']);
                                          echo $user_role['crm_name'];?></h6>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-sm-5">
                                       <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-email"></i>EMP ID</h6>
                                    </div>
                                    <div class="col-sm-7">
                                       <h6 class="m-b-30"><a href="mailto:dummy@example.com"><?php echo '#'.$userid; ?></a></h6>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-sm-5">
                                       <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-home"></i>Role ID :</h6>
                                    </div>
                                    <div class="col-sm-7">
                                       <h6 class="m-b-30"><?php echo $role_name['role']; ?></h6>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-sm-5">
                                       <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-touch-phone"></i>Phone :</h6>
                                    </div>
                                    <div class="col-sm-7">
                                       <h6 class="m-b-30"> </h6>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-sm-5">
                                       <h6 class="f-w-400 m-b-30"><i class="icofont icofont-web"></i>Website :</h6>
                                    </div>
                                    <div class="col-sm-7">
                                       <h6 class="m-b-30"><a href="#!"> </a></h6>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Summery Start -->
                  <div class="col-md-12 col-xl-6 summary-card1">
                     <div class="card summery-card">
                        <div class="card-header">
                           <div class="card-header-left ">
                              <h5>Summary</h5>
                           </div>
                        </div>
                        <div class="card-block">
                           <div class="row">
                              <div class="col-sm-6 b-r-default p-b-30">
                                 <h2 class="f-w-400"><?php echo $active_client['client_count']; ?></h2>
                                 <p class="text-muted f-w-400">Active users</p>
                                 <?php
                                    $percentage = $active_client['client_count'];
                                    $totalWidth = 100;
                                    
                                    $new_width = ($percentage / 100) * $totalWidth;
                                    ?>
                                 <div class="progress">
                                    <div class="progress-bar bg-c-blue" role="progressbar"
                                       aria-valuemin="0" aria-valuemax="100"
                                       style="width:<?php echo $new_width.'%'; ?>">
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-6 p-b-30">
                                 <h2 class="f-w-400"><?php echo $completed_task['task_count']; ?></h2>
                                 <p class="text-muted f-w-400">Completed task</p>
                                 <?php
                                    $percentage = $completed_task['task_count'];
                                    $totalWidth = 100;
                                    
                                    $new_width = ($percentage / 100) * $totalWidth;
                                    ?>
                                 <div class="progress">
                                    <div class="progress-bar bg-c-pink" role="progressbar"
                                       aria-valuemin="0" aria-valuemax="100"
                                       style="width:<?php echo $new_width.'%'; ?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6 b-r-default p-b-30">
                                 <h2 class="f-w-400"><?php echo $inactive_client['client_count']; ?></h2>
                                 <p class="text-muted f-w-400">Inactive users</p>
                                 <?php
                                    $percentage = $inactive_client['client_count'];
                                    $totalWidth = 100;
                                    
                                    $new_width = ($percentage / 100) * $totalWidth;
                                    ?>
                                 <div class="progress">
                                    <div class="progress-bar bg-c-yellow" role="progressbar"
                                       aria-valuemin="0" aria-valuemax="100"
                                       style="width:<?php echo $new_width.'%'; ?>">
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-6 p-b-30">
                                 <h2 class="f-w-400"><?php echo $inprogress_task['task_count']; ?></h2>
                                 <p class="text-muted f-w-400">Inprogress task</p>
                                 <?php
                                    $percentage = $inprogress_task['task_count'];
                                    $totalWidth = 100;
                                    $new_width = ($percentage / 100) * $totalWidth;
                                    ?>
                                 <div class="progress">
                                    <div class="progress-bar bg-c-green" role="progressbar"
                                       aria-valuemin="0" aria-valuemax="100"
                                       style="width:<?php echo $new_width.'%'; ?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- summary end -->
               </div>
               <!-- basics -->
            </div>
            <!-- end tab 1-->
            <!-- tab2-->
            <div id="Tasks">
               <div class="revenue-task row">
                  <!-- card1 start -->
                  <div class="col-md-6 col-xl-3">
                     <div class="card widget-card-1">
                        <div class="card-block-small">
                           <i class="icofont icofont-tasks-alt bg-c-green card1-icon"></i>
                           <span class="text-c-green f-w-600">Tasks</span>
                           <h4><?php echo $assignComplete;?></h4>
                        </div>
                     </div>
                  </div>
                  <!-- card1 end -->
                  <!-- card1 start -->
                  <div class="col-md-6 col-xl-3">
                     <div class="card widget-card-1">
                        <div class="card-block-small">
                           <i class="icofont icofont-tasks-alt bg-c-pink card1-icon"></i>
                           <span class="text-c-pink f-w-600">Closed Tasks</span>
                           <h4><?php echo $assignTesting;?></h4>
                        </div>
                     </div>
                  </div>
                  <!-- card1 end -->
                  <!-- card1 start -->
                  <div class="col-md-6 col-xl-3">
                     <div class="card widget-card-1">
                        <div class="card-block-small">
                           <i class="icofont icofont-tasks-alt bg-c-yellow card1-icon"></i>
                           <span class="text-c-yellow f-w-600">Pending Tasks</span>
                           <h4><?php echo $assignNotstarted;?></h4>
                        </div>
                     </div>
                  </div>
                  <!-- card1 end -->
                  <!-- card1 start -->
                  <div class="col-md-6 col-xl-3">
                     <div class="card widget-card-1">
                        <div class="card-block-small">
                           <i class="icofont icofont-tasks-alt bg-c-blue card1-icon"></i>
                           <span class="text-c-blue f-w-600">Cancelled Tasks</span>
                           <h4><?php echo $assignInprogress;?></h4>
                        </div>
                     </div>
                  </div>
                  <!-- card1 end -->
               </div>
               <div class="cards12 col-xs-12">
                  <div class="col-xl-6">
                     <!-- To Do List card start -->
                     <div class="card fr-hgt">
                        <div class="card-header">
                           <h5>My Tasks</h5>
                           <div class="card-header-right"> <i class="icofont icofont-spinner-alt-5"></i> </div>
                        </div>
                        <div class="card-block">
                           <!-- <h3> Status</h3> -->
                           <h3>Task Name <span class="f-right">Start Date / End Date</span></h3>
                           <div class="new-task notask">
                              <?php 
                                 if(empty($task_list)){ ?>
                              No Tasks Available
                              <?php  }else{
                                 foreach($task_list as $task){
                                 
                                   if(strtotime($task['start_date']) < time()){
                                 $date = date('y-m-j&\nb\sp;g:i:s');
                                 
                                 $start_date=date('Y-m-d',strtotime($task['start_date']));
                                 $end_date=$task['end_date'];
                                 //echo $start_date;
                                 //echo $end_date;
                                 
                                 $date1 = strtotime(''.$start_date.' 00:00:00');
                                 $date2 = strtotime(''.$end_date.' 00:00:00');
                                 $today = time();
                                 
                                 $dateDiff = $date2 - $date1;
                                 // echo $dateDiff;
                                 // echo $today;
                                 // echo $date1;
                                 $dateDiffForToday = $today - $date1;
                                 //echo $dateDiffForToday;
                                 
                                 $percentage = $dateDiffForToday / $dateDiff * 100;
                                 $percentageRounded = round($percentage);
                                 
                                 echo $percentageRounded . '%';
                                 }else{
                                 
                                 $percentageRounded='0';
                                 echo $percentageRounded . '%';
                                 }
                                 ?>
                              <div class="to-do-list">
                                 <div class="checkbox-fade fade-in-primary">
                                    <label class="check-task">
                                       <!--  <input type="checkbox" value="">
                                          <span class="cr">
                                          <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                          </span> -->
                                       <span><?php echo $task['subject']; ?></span> 
                                       <span class="bg-c-pink">Over Due</span>
                                       <div class="progress">
                                          <div class="progress-bar bg-c-blue" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentageRounded.'%'; ?>">
                                          </div>
                                       </div>
                                    </label>
                                 </div>
                                 <span class="f-right end-dateop"></span>
                                 <span class="f-right end-dateop"><?php echo $task['start_date']; ?>/<?php echo date('d-m-Y',strtotime($task['end_date'])); ?></span>
                                 <!-- <div class="f-right">
                                    <a href="#" class="delete_todolist"><i class="icofont icofont-ui-delete"></i></a>
                                    </div> -->
                              </div>
                              <?php }} ?>            
                           </div>
                        </div>
                     </div>
                     <!-- To Do List card end -->
                  </div>
                  <div class="col-xl-6">
                     <!-- To Do Card List card start -->
                     <div class="card fr-hgt">
                        <div class="card-header">
                           <h5>
                              My To Do items 
                              <span class="f-right">
                                 <ul class="nav nav-tabs  tabs" role="tablist">
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">New Td Do</a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">View All</a>
                                    </li>
                                 </ul>
                              </span>
                           </h5>
                        </div>
                        <div class="card-block widnewtodo">
                           <div class="tab-content tabs card-block">
                              <div class="tab-pane active" id="home1" role="tabpanel">
                                 <div class="new-task" id="draggablePanelList">
                                    <div class="no-todo">
                                       <h5 class="bg-c-green12"> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Latest to do's <button type="button" class="btn btn-primary btn-sm f-right" data-toggle="modal" data-target="#myTodo">New Todo's</button></h5>
                                       <div class="sortable" id="5">
                                          <?php
                                             foreach($todo_list as $todo){ ?>
                                          <div class="to-do-list card-sub" id="<?php echo $todo['id']; ?>">
                                             <div class="checkbox-fade fade-in-primary">
                                                <label class="check-task">
                                                <input type="checkbox" value="" id="<?php echo $todo['id']; ?>" onclick="todo_status(this)">
                                                <span class="cr">
                                                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                </span>
                                                <span><?php echo $todo['todo_name']; ?></span> 
                                                </label>
                                             </div>
                                             <div class="f-right">
                                                <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
                                                <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
                                             </div>
                                          </div>
                                          <?php } ?>
                                       </div>
                                    </div>
                                    <h5 class="bg-c-green12"> <i class="fa fa-check"></i> Latest finished to do's</h5>
                                    <div class="completed_todos">
                                       <?php
                                          foreach($todo_completedlist as $todo){ ?>
                                       <div class="to-do-list card-sub" id="1">
                                          <div class="checkbox-fade fade-in-primary">
                                             <label class="check-task done-task">
                                             <input type="checkbox" value="" checked="checked" id="<?php echo $todo['id']; ?>" onclick="todo_status(this)" >
                                             <span class="cr">
                                             <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                             </span>
                                             <span><?php echo $todo['todo_name']; ?></span> 
                                             </label>
                                          </div>
                                          <div class="f-right">
                                             <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
                                             <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
                                          </div>
                                       </div>
                                       <?php } ?>
                                       <!--  <p>No todos found</p> -->
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane" id="profile1" role="tabpanel">
                                 <div class="new-task">
                                    <?php
                                       foreach($todolist as $todo){ ?>
                                    <div class="to-do-list sortable-moves">
                                       <div class="to-do-list card-sub" id="1">
                                          <div class="checkbox-fade fade-in-primary">
                                             <label class="check-task <?php if($todo['status']=='completed'){ ?> done-task <?php } ?>">
                                             <input type="checkbox" value="" <?php if($todo['status']=='completed'){ ?> checked="checked" <?php } ?> id="<?php echo $todo['id']; ?>" onclick="todo_status(this)" >
                                             <span class="cr">
                                             <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                             </span>
                                             <span><?php echo $todo['todo_name']; ?></span> 
                                             </label>
                                          </div>
                                          <div class="f-right">
                                             <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
                                             <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- To Do Card List card end -->
                  </div>
               </div>
            </div>
            <!-- tab 2 end -->
            <!--tab 3-->
            <div id="Proposals">
               <div class="row col-12">
                  <div class="card desk-dashboard teo fr-bg12 col-xs-12 col-md-6">
                     <div class="card">
                        <div class="accident-toggle">
                           <h2>Proposals</h2>
                        </div>
                        <span class="time-frame"><label>Time Frame:</label>
                        <select id="proposal">
                           <option value="month">This Month</option>
                           <option value="year">This Year</option>
                        </select>
                        <div class="card-block proposal">
                           <div id="piechart" style="width: 100%; height: 309px;"></div>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="card desk-dashboard fr-bg12 widmar col-xs-12 col-md-6">
                     <div class="card">
                     <div class="accident-toggle">
                       <h2>Clients</h2>
                     </div>
                     <span class="time-frame"><label>Time Frame:</label>
                     <select id="filter_client">
                       <option value="year">This Year</option>
                       <option value="month">This Month</option>
                     </select>
                     <div class="card-block fileters_client">
                       <div id="chart_Donut11" style="width: 100%; height: 309px;"></div>
                     </div>
                     </div>
                     </div> -->
               </div>
            </div>
            <!-- tab 3 end -->

             <div id="Services">
  <div class="row job-card">
   <div class="card fr-hgt">
   <!--   <div class="col-md-12 col-xl-4"> -->
     <div class="card-block user-detail-card">
    
            <?php                  
               $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);                
              
                  
                  (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                  ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                  
                  (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                  ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                  
                  (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                  ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                  
                  (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                  ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                  
                  (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                  ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                  
                  (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                  ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                  
                  (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                  ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                  
                  (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                  ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                  
                  (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                  ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                  
                  (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                  ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                  
                  (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                  ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                  
                  (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                  ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                  
                  (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                  ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                  
                  (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                  ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                  
                  (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                  ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                  
                  (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                  ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                   ?>
                    <table border="2">
                     
               <tr class="for_table">
               <td> Vat Return </td>               
                  <td>
                     <div class="checkbox-color checkbox-primary">
                         <input type="checkbox"  name="vat" id="vat" value="" <?php echo $vat;?> class="update_service" > <label for="vat"> </label> 
                     </div>
                  </td>
                  </tr>
                  <tr>
                  <td> Payroll</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="payroll" id="payroll" value="" <?php echo $pay;?> class="update_service"  >
                  <label for="payroll"> 
                  </label> 
                     </div>
                  </td>
                  </tr>
                  <tr>
                  <td>Accounts</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                         <input type="checkbox" value="" name="accounts" id="accounts"  <?php echo $acc;?> class="update_service" >
                   <label for="accounts"></label> 
                     </div>
                  </td></tr>
                  <tr>
                  <td>Confirmation Statement</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                         <input type="checkbox" name="conf_statement" id="conf_statement" value="" <?php echo $cst;?> class="update_service" > 
                   <label for="conf_statement"></label> 
                     </div>
                  </td></tr>
                  <tr>
                  <td>Company Tax Return</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="company_tax" id="company_tax_return" value="" <?php echo $tax;?> class="update_service"  > 
                  <label for="company_tax_return"></label> 
                     </div>
                  </td></tr>
                  <tr>
                  <td>Personal Tax</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" value="" name="personal_tax" id="personal_tax_return" <?php echo $p_tax;?> class="update_service"  > 
                  <label for="personal_tax_return"></label> 
                     </div>
                  </td></tr>
                  <tr>
                  <td>Workplace -pension</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="workplace" id="workplace" value="" <?php echo $workplace;?> class="update_service" > 
                  <label for="workplace"></label> 
                     </div>
                  </td></tr>
                  <tr>
                  <td>Cis </td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="cis" id="cis" value="" <?php echo $cis;?> class="update_service"  > 
                  <label for="cis"></label> 
                     </div>
                  </td></tr>
                  <tr>
                  <td> Cis -Sub Contractor</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                         <input type="checkbox" name="cissub" id="cissub" value="" <?php echo $cissub;?> class="update_service" >
                  <label for="cissub"></label> 
                     </div>
                  </td></tr>
                  <tr>
                  <td> P11d</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" namne="p11d" id="p11d" value="" <?php echo $p11d;?> class="update_service" > 
                  <label for="p11d"></label> 
                     </div>
                  </td></tr>
                  <tr>
                  <td> Book keeping</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                         <input type="checkbox" name="bookkeep" id="bookkeep" value="" <?php echo $bookkeep;?> class="update_service"  >
                  <label for="bookkeep"></label> 
                     </div>
                  </td>
                  </tr>
                  <tr>
                  <td> Management</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                         <input type="checkbox" name="management" id="management" value="" <?php echo $management;?> class="update_service"  >  
                   <label for="management"></label> 
                     </div>
                  </td></tr>
                  <tr>
                  <td> Investigation Insurance</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="investgate" id="investgate" value="" <?php echo $investgate;?> class="update_service"  ><label for="investgate">  </label> 
                     </div>
                  </td></tr>
                  <tr>
                  <td> Registered</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="registered" id="registered" value="" <?php echo $registered;?> class="update_service" " > 
                  <label for="registered"> </label> 
                     </div>
                  </td></tr>
                  <tr>
                  <td> Tax Advice</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="taxadvice" id="taxadvice" value="" <?php echo $taxadvice;?> class="update_service"  > <label for="taxadvice"></label> 
                     </div>
                  </td></tr>
                  <tr>
                  <td> Tax Investigate</td>
                  <td>
                     <div class="checkbox-color checkbox-primary">
                        <input type="checkbox" name="taxinvest" id="taxinvest" value="" <?php echo $taxinvest;?> class="update_service">
                  <label for="taxinvest"></label> 
                     </div>
                  </td>
               </tr>
             </table>
            </div>
            </div>
            </div>
            </div>
           <!--  </div> -->


            <!--tab 4-->
            <div id="Deadline-Manager">
               <?php 
                  //echo $user_role['role'].'priyarole';die;
                  
                  if($user_role['role']=='6'){?>
               <div class="overall_hours1 top-margin01 floating">
                  <!-- <div class="project_hour01">
                     <span>4:15:29</span>
                     <h4>Projects Hours</h4>
                     </div> -->
                  <div class="project_hour01">
                     <span><?php 
                        if(isset($ex_timer[2])){ $vr = $ex_timer[2]; } else{ $vr = '00'; }
                        // echo $ex_timer[0].':'.$ex_timer[1].':'.$vr;
                        echo $task_cnt * 8 .' : 00 : 00';
                         ?></span>
                     <h4>Tasks Hours</h4>
                  </div>
                  <div class="project_hour01">
                     <span><?php echo $w_days * 8 .' : 00 m'; ?></span>
                     <h4>This month working Hours</h4>
                  </div>
                  <div class="project_hour01">
                     <span><?php echo $w_days * 8 .' : 00 m'; ?></span>
                     <h4>Working Hours</h4>
                  </div>
               </div>
               <div class="additional-tab floating_set">
                  <div class="deadline-crm1 floating_set">
                     <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                        <li class="nav-item all-client-icon1">
                           <span><i class="fa fa-user fa-6" aria-hidden="true"></i></span>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link <?php echo $bank_active;?>" data-toggle="tab" href="#bank_details">Bank Details</a>
                           <div class="slide"></div>
                        </li>
                        <!-- <li class="nav-item">
                           <a class="nav-link <?php echo $time_active;?>" data-toggle="tab" href="#time_card">Time Card - Login</a>
                           <div class="slide"></div>
                        </li> -->
                        <li class="nav-item">
                           <a class="nav-link <?php echo $time_active;?>" data-toggle="tab" href="#task_summary">Task summary</a>
                           <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#all_activity">All activity log</a>
                           <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="<?php echo base_url();?>login/logout">Logout</a>
                           <div class="slide"></div>
                        </li>
                     </ul>
                  </div>
                  <div class="all_user-section2 floating_set">
                     <div class="tab-content">
                        <div id="bank_details" class="tab-pane fade <?php echo $css_bank_active;?>">
                           <?php if(!empty($custom_form['labels'])){  ?>
                           <div class="new_bank1">
                              <h2>New Bank</h2>
                              <?php 
                                 $succ = $this->session->flashdata('success');
                                 
                                  if($succ){?>
                              <div class="modal-alertsuccess alert alert-success"
                                 >
                                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                 <div class="pop-realted1">
                                    <div class="position-alert1"><?php echo $succ; ?></div>
                                 </div>
                              </div>
                              <?php } ?>
                              <div class="bank-form">
                                 <form action="<?php echo base_url();?>staff/update_custom" name="update_bank" id="update_bank" method="post">
                                    <?php 
                                       //  echo '<pre>';
                                       //  print_r($custom_form);
                                       //   echo '</pre>';
                                       
                                        
                                       if(!empty($custom_form['labels'])){                
                                       $labels=explode(',',$custom_form['labels']);
                                       $field_type=explode(',',$custom_form['field_type']);
                                       $field_name=explode(',',$custom_form['field_name']);
                                       $values=explode(',',$custom_form['values']);
                                       for($i=0;$i< count($labels);$i++){ 
                                       
                                         if($field_type[$i]=='textarea'){ ?>
                                    <div class="form-group">
                                       <label><?php echo $labels[$i]; ?></label>
                                       <textarea name="<?php echo $field_name[$i]; ?>"><?php if(isset($values[$i])){
                                          echo $values[$i];
                                          } ?></textarea>
                                    </div>
                                    <?php  }else{ ?>
                                    <div class="form-group">
                                       <label><?php echo $labels[$i]; ?></label>
                                       <input type="<?php echo $field_type[$i]; ?>" name="<?php echo $field_name[$i]; ?>" id="bank_name" value="<?php if(isset($values[$i])){
                                          echo $values[$i];
                                          } ?>">
                                    </div>
                                    <?php } }} ?>
                                    <div class="text-right new-bank-update">
                                       <input type="button" name="cancel" id="cancel" value="cancel">
                                       <input type="submit" name="update" id="update" value="update">
                                       <!-- <button type="button" name="cancel">Update</button>
                                          <button type="button" name="cancel">Cancel</button> -->
                                    </div>
                                 </form>
                              </div>
                           </div>
                           <?php } ?>
                        </div>
                        <!-- tab1 -->
                        <div id="time_card" class="tab-pane fade <?php echo $css_time_active;?>" style="display: none;" >
                           <div class="new_bank1">
                              <h2>Timecard Details</h2>
                              <div class="working-hrs bank-form">
                                 <div class="form-group date_birth">
                                    <form name="time_cards" id="time_cards" method="post">
                                       <label class="col-form-label">Month *</label>
                                       <div class="working-picker">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-controlfields" type="text" name="month_pic" id="month_pic" value="<?php echo date('m').'/'.date('Y');?>">
                                       </div>
                                       <div class="go-bts">
                                          <button type="button" name="go" id="go">Go</button>
                                          <!--  <input type="submit" name="go" id="go" value="Go"> -->
                                       </div>
                                    </form>
                                 </div>
                              </div>
                              <div class="work-month" id="timecard_filter">
                                 <h3>Works Hours Details of <?php echo date('M').' - '.date('Y');?></h3>
                                 <div class="table-responsive">
                                    <table class="table">
                                       <tr>
                                          <?php for ($i=$start_month; $i <=$end_month; $i++) { 
                                             $year = date('Y');
                                             $m = date('m');
                                             $dates = getStartAndEndDate($i,$year);
                                             $alldates = getDatesFromRange($dates['start_date'],$dates['end_date']);
                                             ?>
                                          <td class="filed-reject">
                                             <h4>Week:<?php echo ltrim($i,'0');?></h4>
                                             <?php 
                                                $w = 0;
                                                foreach ($alldates as $alldates_key => $alldates_value) {
                                                
                                                    $weekend = isWeekend($alldates_value);
                                                    //print_r($weekend);
                                                    $exp_d = explode('-', $alldates_value);
                                                    $d = $exp_d[1];
                                                
                                                    if( $d == $m )
                                                    {
                                                        if($weekend==''){
                                                            $w++;
                                                    ?>
                                             <p><b><?php echo $alldates_value;?></b> <span>8:00 m</span></p>
                                             <?php } else {?>
                                             <p><b><?php echo $alldates_value;?></b> <span><a href="#">Holiday</a></span></p>
                                             <?php } ?>
                                             <!-- <p><b>02-10-17</b> <span><a href="#">Holiday</a></span></p>
                                                <p><b>02-10-17</b> <span>0:0 m</span></p>
                                                <p><b>02-10-17</b> <span>0:0 m</span></p>
                                                <p><b>02-10-17 </b><span><a href="#">Holiday</a></span></p>
                                                <p><b>02-10-17</b> <span>0:0 m</span></p>
                                                <p class="removed-02"><b>02-10-17</b> <span><a href="#">Holiday</a></span></p> 
                                                
                                                 -->
                                             <?php   } 
                                                }
                                                   ?>
                                             <span class="total-working-hr"><strong>Total Working Hour: </strong> <?php echo $w * 8 ; ?>:0 m</span>
                                          </td>
                                          <?php }?>
                                       </tr>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- tab2 -->  
                        <div id="task_summary" class="tab-pane fade <?php echo $css_time_active;?> ">
                           <div class="new_bank1">
                              <h2>Task summary</h2>
                              <div class="task-filter1">
                                 <div class="time-spent">
                                    <h4>Total Task Time Spent</h4>
                                    <!-- <div id="defaultCountdown"></div> -->
                                    <div class="timer-section01">
                                       <?php
                                          ?>
                                       <span><?php if(isset($ex_timer[0]) && $ex_timer[0]!=''){ echo $ex_timer[0]; }else{ echo "00"; } ?> <strong>Hours</strong></span> :
                                       <span><?php if(isset($ex_timer[1])){ echo $ex_timer[1]; }else{ echo "00"; }?><strong>Minutes</strong></span> :
                                       <span><?php if(isset($ex_timer[2])){ echo $ex_timer[2]; }else{ echo "00"; }?><strong>Seconds</strong></span> 
                                    </div>
                                 </div>
                                 <!-- 09-08-2018 for individual task timing -->
                                <div class="time-spent">
                                    <h4>Individual Task Time Spent</h4>
                                    <!-- <div id="defaultCountdown"></div> -->
                                    <div class="table-responsive">
                                      <table class="table">
                                      <thead>
                                     
                                          <th>Sno</th>
                                          <th>Task Name</th>
                                          <th>Timer</th>
                                      </thead>
                                      <tbody>
<?php  $individual_timer=$this->db->query("select * from individual_task_timer where user_id=".$_SESSION['id']." ")->result_array();
$pause_val='on';
$pause='on';

if(count($individual_timer)>0){
  $sno=1;
foreach ($individual_timer as $intime_key => $intime_value) {
   $task_id=$intime_value['task_id'];
  $task_data=$this->db->query("select * from add_new_task where id=".$task_id." ")->row_array();
  if(!empty($task_data)){
  $for_total_times=0;
  $its_time=$intime_value['time_start_pause'];
  $res=explode(',', $its_time);
    $res1=array_chunk($res,2);
  $result_value=array();
//  $pause='on';
  foreach($res1 as $rre_key => $rre_value)
  {
     $abc=$rre_value;
     if(count($abc)>1){
     if($abc[1]!='')
     {
        $ret_val=calculate_test($abc[0],$abc[1]);
        array_push($result_value, $ret_val) ;
     }
     else
     {
      $pause='';
      $pause_val='';
        $ret_val=calculate_test($abc[0],time());
         array_push($result_value, $ret_val) ;
     }
    }
    else
    {
      $pause='';
      $pause_val='';
        $ret_val=calculate_test($abc[0],time());
         array_push($result_value, $ret_val) ;
    }
  }
  // $time_tot=0;
   foreach ($result_value as $re_key => $re_value) {
      //$time_tot+=time_to_sec($re_value) ;
      $for_total_times+=time_to_sec($re_value) ;
   }
  $exp_ts =  convertToHoursMins($for_total_times);
   $ex_timers = explode(':', $exp_ts);
?>
                                          <tr>
                                            <td><?php echo $sno; ?></td>
                                            <td><?php echo $this->Common_mdl->get_field_value('add_new_task','subject','id',$intime_value['task_id']);
                                            //$intime_value['task_id']; ?></td>
                                            <td> <div class="timer-section01 for_inner_timer">
                                       <?php
                                          ?>
                                       <span><?php echo $ex_timers[0];?> <strong>Hours</strong></span> :
                                       <span><?php echo $ex_timers[1];?><strong>Minutes</strong></span> :
                                       <span><?php if(isset($ex_timers[2])){ echo $ex_timers[2]; }else{ echo "00"; }?><strong>Seconds</strong></span> 
                                    </div></td>
                                          </tr>
<?php 
$sno++;
} // if
} // for
} // if
?>                                    </tbody>
                                      </table>
                                    </div>
                                 </div>
                                 <!-- end of individual task timing -->
                                 <div class="time-spent">
                                    <h4>Task Reports</h4>
                                    <div class="chart-container-01">
                                       <div class="open-closed1">
                                          <!--
                                             <span> <i class="fa fa-square fa-6" aria-hidden="true"></i>Critical</span>
                                             <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> High</span>
                                             
                                             <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Medium</span>
                                             <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Low</span>
                                             
                                             <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Resolved</span>
                                             <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> closed</span> -->
                                          <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Low</span>
                                          <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> High</span>
                                          <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Medium</span>
                                          <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Super Urgent</span> 
                                       </div>
                                       <div id="chartContainer" style="width: 100%; height: 420px"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- tab3 -->  
                        <div id="all_activity" class="tab-pane fade">
                           <div class="new_bank1">
                              <h2>All activity log</h2>
                              <div class="all_user-section2">
                                 <table class="table client_table1 text-center display nowrap" id="all_activity1" cellspacing="0" width="100%">
                                    <thead>
                                       <tr>
                                          <th>Activity Date</th>
                                          <th>User</th>
                                          <th>Module</th>
                                          <th>Activity</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <?php 
                                          //print_r($user_activity);die;
                                           foreach ($user_activity as $activity_key => $activity_value) { 
                                          
                                              $rec=$this->Common_mdl->select_record('user','id',$activity_value['user_id']);
                                              $role=$this->Common_mdl->select_record('Role','id',$rec['role']);
                                          
                                          
                                              ?>
                                       <tr>
                                          <td><?php echo date("d-m-Y h:i:s A ",$activity_value['CreatedTime']);?></td>
                                          <td><?php echo $role['role'];?></td>
                                          <td><?php echo $activity_value['module'];?></td>
                                          <td><?php echo $activity_value['log'];?></td>
                                       </tr>
                                       <?php } ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                           <!-- tab4 -->         
                        </div>
                        <!-- tab-content --> 
                     </div>
                  </div>
                  <!-- additional-tab -->
               </div>
                <div class="row card desk-dashboard widmar col-xs-12 dvn-chart">
                   <div class="accident-toggle">
                      <h2>Deadlines</h2>
                   </div>
                   <span class="time-frame"><label>Time Frame:</label>
                   <select id="filter_deadline">
                      <option value="month">This Month</option>
                      <option value="year">This Year</option>
                   </select>
                   <div class="card-block deadline">
                      <div id="columnchart_values" style="width: 100%; height: 350px;"></div>
                   </div>
                </div>
            </div>
            <?php }else{  ?>
            <div class="row job-card">
               <div class="col-md-12 col-xl-4">
                  <div class="card widget-statstic-card">
                     <div class="card-header">
                        <div class="media">
                           <div class="media-body media-middle">
                              <div class="company-name">
                                 <p>VAT</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="card-block">
                        <i class="icofont icofont-presentation-alt st-icon bg-c-yellow"></i>
                        <ul class="text-muted">
                           <?php
                              foreach($oneyear as $date){ 
                                if($date['crm_vat_due_date']!='')?>
                           <li><?php echo $date['crm_company_name']; ?>-<?php echo (strtotime($date['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($date['crm_vat_due_date'])): ''); ?></li>
                           <?php } ?>                    
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-md-12 col-xl-4">
                  <div class="card widget-statstic-card">
                     <div class="card-header">
                        <div class="media">
                           <div class="media-body media-middle">
                              <div class="company-name">
                                 <p>Accounts</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="card-block">
                        <i class="icofont icofont-files st-icon bg-c-blue"></i>
                        <ul class="text-muted">
                           <?php
                              foreach($oneyear as $date){ 
                                if($date['crm_ch_accounts_next_due']!='')?>
                           <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_ch_accounts_next_due']; ?></li>
                           <?php } ?> 
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-md-12 col-xl-4">
                  <div class="card widget-statstic-card">
                     <div class="card-header">
                        <div class="media">
                           <div class="media-body media-middle">
                              <div class="company-name">
                                 <p>Company Tax Returns</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="card-block">
                        <i class="icofont icofont-paper st-icon bg-c-green"></i>
                        <ul class="text-muted">
                           <?php
                              foreach($oneyear as $date){ 
                                if($date['crm_accounts_tax_date_hmrc']!='')?>
                           <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_accounts_tax_date_hmrc']; ?></li>
                           <?php } ?> 
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-md-12 col-xl-4">
                  <div class="card widget-statstic-card">
                     <div class="card-header">
                        <div class="media">
                           <div class="media-body media-middle">
                              <div class="company-name">
                                 <p>Personal Tax Returns</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="card-block">
                        <i class="icofont icofont-file-document st-icon bg-c-pink"></i>
                        <ul class="text-muted">
                           <?php
                              foreach($oneyear as $date){ 
                                if($date['crm_personal_due_date_return']!='')?>
                           <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_personal_due_date_return']; ?></li>
                           <?php } ?> 
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-md-12 col-xl-4">
                  <div class="card widget-statstic-card">
                     <div class="card-header">
                        <div class="media">
                           <div class="media-body media-middle">
                              <div class="company-name">
                                 <p>Payroll</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="card-block">
                        <i class="icofont icofont-file-text st-icon bg-c-yellow"></i>
                        <ul class="text-muted">
                           <?php
                              foreach($oneyear as $date){ 
                                if($date['crm_rti_deadline']!='')?>
                           <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_rti_deadline']; ?></li>
                           <?php } ?> 
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-md-12 col-xl-4">
                  <div class="card widget-statstic-card">
                     <div class="card-header">
                        <div class="media">
                           <div class="media-body media-middle">
                              <div class="company-name">
                                 <p>Confimation Statements</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="card-block">
                        <i class="icofont icofont-file-document st-icon bg-c-blue"></i>
                        <ul class="text-muted">
                           <?php
                              foreach($oneyear as $date){ 
                                if($date['crm_confirmation_statement_due_date']!='')?>
                           <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_confirmation_statement_due_date']; ?></li>
                           <?php } ?> 
                        </ul>
                     </div>
                  </div>
               </div>
               <!-- due details section -->
              

            </div>
            <?php } ?>



            
         </div>
      </div>
      <div class="title_page01 add_profile_01 floating">
         <!-- <div class="profile_setting1 floating">
            <!--  <div class="col-sm-4">
               <div class="profile_task1">
                  <span><?php echo $assignNotstarted;?></span>
                  <strong>Not started Tasks</strong>
                  <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
               </div>
               <div class="profile_task1">
                  <span><?php echo $assignInprogress;?></span>
                  <strong>Inprogress Tasks</strong>
                  <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
               </div>
               <!-- <div class="profile_task1">
                  <span><?php echo $assignAwaiting;?></span>
                  <strong>Awaiting Feedback Tasks</strong>
                  <a href="#">More info &gt; </a>
                  </div>  
               <div class="profile_task1">
                  <span><?php echo $assignTesting;?></span>
                  <strong>Testing Tasks</strong>
                  <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
               </div>
               <div class="profile_task1">
                  <span><?php echo $assignComplete;?></span>
                  <strong>Complete Tasks</strong>
                  <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
               </div>
               </div>  
            <div class="col-sm-4">
               <div class="profile_display1">
                  <?php  $getUserProfilepic = $this->Common_mdl->getUserProfilepic($userid); ?>
                  <img src="<?php echo $getUserProfilepic;?>">
                  <h3> <?php 
               $user_role=$this->Common_mdl->select_record('user','id',$userid);
               $role_name=$this->Common_mdl->select_record('Role','id',$user_role['role']);
               echo $user_role['crm_name'];?> </h3>
                  <span>EMP ID: <?php 
               echo '#'.$userid;
               ?></span>
                  <strong><?php 
               echo $role_name['role'];
               ?></strong>
               </div>
            </div>
            
            <div class="col-sm-4">
               <div class="profile_task1">
                  <span><?php echo $assignNotstarted;?></span>
                  <strong>Not started Tasks</strong>
                  <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
               </div>
               <div class="profile_task1">
                  <span><?php echo $assignInprogress;?></span>
                  <strong>Inprogress Tasks</strong>
                  <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
               </div>
               <!-- <div class="profile_task1">
                  <span><?php echo $assignAwaiting;?></span>
                  <strong>Awaiting Feedback Tasks</strong>
                  <a href="#">More info &gt; </a>
                  </div>  
               <div class="profile_task1">
                  <span><?php echo $assignTesting;?></span>
                  <strong>Testing Tasks</strong>
                  <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
               </div>
               <div class="profile_task1">
                  <span><?php echo $assignComplete;?></span>
                  <strong>Complete Tasks</strong>
                  <a href="<?php echo base_url()?>user/task_list">More info &gt; </a>
               </div>
            </div>
             
            </div>  -->
      </div>
   </div>
</div>
<div class="modal fade" id="myTodo" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New Todo's</h4>
         </div>
         <div class="modal-body">
            <label>Todo's Name</label>
            <input type="text" name="todo_name" id="todo_name" value="">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" id="todo_save">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<?php
   foreach($todolist as $todo){
   ?>
<div class="modal fade" id="EditTodo_<?php echo $todo['id']; ?>" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Todos</h4>
         </div>
         <div class="modal-body">
            <input type="hidden" name="id" id="todo_id" value="<?php echo $todo['id']; ?>">
            <input type="text" name="todo_name" class="todo_name" value="<?php echo $todo['todo_name']; ?>">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default save" id="<?php echo $todo['id']; ?>">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="DeleteTodo_<?php echo $todo['id']; ?>" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete todos</h4>
         </div>
         <div class="modal-body">
            <p>Do you want to Delete Todo's ?</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default delete" id="<?php echo $todo['id']; ?>">Yes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<!-- title_page01 -->    
<input type="hidden" id="h_f" name="h_f" value="">
<?php $this->load->view('includes/footer');?>
<script src="<?php echo  base_url()?>assets/js/jquery.lineProgressbar.js"></script>
<script src="<?php echo  base_url()?>assets/js/c3-custom-chart.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script> 
<script src="https://rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/demo/MonthPicker.min.js"></script>
<script type="text/javascript" language="javascript" src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/todo.js"></script>
<!-- jquery sortable js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Sortable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/sortable-custom.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   
   $('#all_activity1').DataTable();
      
   });
   
   $(document).ready(function(){
   //  alert('hi');
   $('#month_pic').MonthPicker({ Button: false });
   
    $("#legal_form12").change(function() {
    var val = $(this).val();
    if(val === "from2") {
      // alert('hi');
        $(".from2option").show();
    }
    else {
        $(".from2option").hide();
    }
    });
   
   });
   
   
   
   
</script>
<!-- <script src="<?php echo base_url();?>assets/js/jquery.plugin.js"></script>
   <script src="<?php echo base_url();?>assets/js/jquery.countdown.js"></script>
   <script>
   $(function () {
    var austDay = new Date();
    austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 26);
    $('#defaultCountdown').countdown({until: austDay});
    $('#year').text(austDay.getFullYear());
   });
   </script> -->
<script type="text/javascript"> 
   window.onload = function() { 
    $("#chartContainer").CanvasJSChart({ 
      title: { 
        fontSize: 24
      }, 
      axisY: { 
        title: "Products in %" 
      }, 
      data: [ 
      { 
        type: "pie", 
        toolTipContent: "{label} <br/> {y} %", 
        indexLabel: "{y} %", 
        dataPoints: [ 
          { label: "Low",  y: <?php echo $low;?>, legendText: "Low"}, 
          { label: "High",    y: <?php echo $high;?>, legendText: "High"  }, 
          { label: "Medium",   y: <?php echo $medium;?>,  legendText: "Medium" }, 
          { label: "Super urgent",       y: <?php echo $super_urgent;?>,  legendText: "Super urgent"}
        ] 
      } 
      ] 
    }); 
    
    $("#chartContainer1").CanvasJSChart({ 
      title: { 
        fontSize: 24
      }, 
      axisY: { 
        title: "Products in %" 
      }, 
      data: [ 
      { 
        type: "pie", 
        toolTipContent: "{label} <br/> {y} %", 
        indexLabel: "{y} %", 
        dataPoints: [ 
          { label: "Open",  y: 30.3, legendText: "Open"}, 
          { label: "Closed",    y: 19.1, legendText: "Closed"  }, 
          
        ] 
      } 
      ] 
    }); 
   } 
</script> 
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   
    $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
      $( "#update_bank" ).validate({
   
        rules: {
          bank_name: "required",  
          branch_name: "required",  
          account_name: "required",  
          account_no: "required",  
        },
        messages: {
          bank_name: "Please enter your Bank Name",
          branch_name: "Please enter your Branch Name",
          account_name: "Please enter your Account Name",
          account_no: "Please enter your Account Number",
        },
        
      });
   
      $("#go").click(function(){
        var month = $("#month_pic").val();
   
         $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>staff/time_card/",
            data: {month:month},
            success: function(response) {
            $("#timecard_filter").html(response);
            },
          });
   
   
      });
   });
   
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
   
   
   $("#newactives").dataTable({
   "iDisplayLength": 10
   });
   
   $("#servicedead").dataTable({
   "iDisplayLength": 10 ,
   "scrollX": true
   
   });   
   
    $(".status").change(function(e){
     //$('#alluser').on('change','.status',function () {
     //e.preventDefault();
     
     
      var rec_id = $(this).data('id');
      var stat = $(this).val();
     
     $.ajax({
           url: '<?php echo base_url();?>user/statusChange/',
           type: 'post',
           data: { 'rec_id':rec_id,'status':stat },
           timeout: 3000,
           success: function( data ){
               //alert('ggg');
               $(".status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('.status_succ'); });
              // setTimeout(resetAll,3000);
               //location.reload();
               setTimeout(function(){// wait for 5 secs(2)
              //location.reload(); // then reload the page.(3)
              $(".status_succ").hide();
         }, 3000);
               if(stat=='3'){
                   
                    //$this.closest('td').next('td').html('Active');
                    $('#frozen'+rec_id).html('Frozen');
     
               } else {
                    $this.closest('td').next('td').html('Inactive');
               }
               },
               error: function( errorThrown ){
                   console.log( errorThrown );
               }
           });
      });
   
   
   $(".search").change(function(){
           $(".LoadingImage").show();
   
       var data = {};
   
      data['com_num'] = $("#companysearch").val();
      data['legal_form'] = $("#legal_form").val();
      //var values = $("button[name='service_fil[]']").map(function(){return $(this).attr("aria-pressed");}).get();
      var f_h = $("#h_f").val();
     // alert(f_h);
      if(f_h)
      {
       var fields = $("button[id='"+f_h+"'").attr('id');
       var values = $("button[id='"+f_h+"'").attr("aria-pressed");
      }else{
       var fields ='';
       var values ='';
      }
     
      data['values'] = '';
      data['fields'] = '';
      $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>staff/companysearch/",
               data: data,
               success: function(response) {
                       $(".LoadingImage").hide();
   
               $(".filterrec").html(response);
               $("#servicedead").dataTable({
               "iDisplayLength": 10,
                   "dom": '<"staffprofilelist01">lfrtip'
               });
               },
       });
   });
   
   
   $(document).on('click',".update_service",function(){
       var values = this.id;
   
       if($(this). prop("checked") == true){
       var stat = "on";
       }
       else if($(this). prop("checked") == false){
       var stat = "off";
       }
      $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>staff/service_update/",
               data: {'values':values,'stat':stat},
               success: function(response) {
                   $(".service_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('.service_succ'); });
              /* $(".filterrec").html(response);
               $("#servicedead").dataTable({
               "iDisplayLength": 10,
                   "dom": '<"staffprofilelist01">lfrtip'
               });*/
               },
       });
       
       /* var notChecked = [], checked = [];
       $(":checkbox").map(function() {
           this.checked ? checked.push(this.id) : notChecked.push(this.id);
       });
       alert("checked: " + checked);
       alert("not checked: " + notChecked);*/
   });
   
   //$(".btn-sm").click(function(){
       $(document).on('click',".btn-sm",function(){
      //$(this).attr("aria-pressed")
     $(".LoadingImage").show();
        //var values = $("button[name='service_fil[]']").map(function(){return $(this).attr("aria-pressed");}).get();
        var fields = $(this).attr('id');
        var values = $(this).attr("aria-pressed");
        $("#h_f").val(fields);
      // alert(values);
    var data = {};
   
      data['com_num'] = $("#companysearch").val();
      data['legal_form'] = $("#legal_form").val();
      /*data['com_num'] = '';
      data['legal_form'] = '';*/
      data['values'] = values;
      data['fields'] = fields;
      $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>staff/companysearch/",
               data: data,
               success: function(response) {
                     $(".LoadingImage").hide();
               $(".filterrec").html(response);
               $("#servicedead").dataTable({
               "iDisplayLength": 10,
                   "dom": '<"staffprofilelist01">lfrtip'
               });
               },
       });
   
   });
   
   });
   
</script>
<script>
   $( function() {
   
    $( ".sortable" ).sortable(
   {
         update: function(event, ui) {       
       var arr = [];
     $("div.sortable ").each(function() { 
         var idsInOrder = [];
       // idsInOrder.push($(this).attr('id')) 
        var ul_id = $(this).attr('id');
      //  alert(ul_id);
   
        $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
        idsInOrder.push($(this).attr('id')) 
    });
   // alert(idsInOrder);
   // arr.push({[ul_id]:idsInOrder});   
     arr[ul_id] = idsInOrder;   
    });
   
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/update_type_todolist",
    data:  { 'datas' : arr }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
      $(".LoadingImage").hide();               
    }
    });
   }
      });
    //$( ".sortable" ).disableSelection();
   
   } );
   
   $("#todo_save").click(function(){
   var todo_name=$("#todo_name").val();
   console.log(todo_name);
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/todolist_add",
    data:  { 'todo_name' : todo_name }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
       $("#myTodo").modal('hide');
       $(".LoadingImage").hide();
      var json = JSON.parse(data);
      data=json['content'];      
       $(".sortable").html('');
       $(".sortable").append(data);        
    }
    });
   
   });
   
   function todo_status(todo){
   var id=$(todo).attr('id');
   //alert(id);
    $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/todos_status",
    data:  { 'id' : id}, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){       
       $(".LoadingImage").hide();
       var json = JSON.parse(data);
       data=json['content'];  
       complete_data=json['completed_content'];      
       $(".sortable").html('');
       $(".sortable").append(data); 
       $(".completed_todos").html('');
       $(".completed_todos").append(complete_data);        
     }
   });
   }
   
   $('.save').click(function(){
   
   var id=$(this).attr('id');
   // console.log(id);
   var todo_name= $("div#EditTodo_"+$(this).attr('id')+" .todo_name").val();
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/todolist_edit",
    data:  { 'todo_name' : todo_name,'id':id }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
       $("#EditTodo_"+$(this).attr('id')+"").modal('hide');
       $(".LoadingImage").hide();
      location.reload();
    }
    });
   
   //console.log(todo_name);
   
   });
   
   $('.delete').click(function(){
   var id=$(this).attr('id');
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/todolist_delete",
    data:  { 'id':id }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
       $("#DeleteTodo_"+$(this).attr('id')+"").modal('hide');
       $(".LoadingImage").hide();
      location.reload();
    }
    });
   });
   
   $("#filter_client").change(function(){
   if($(this).val()=='month'){
   id=$(this).val();
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Staff/client_filter",
    data:  { 'id':id }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
      console.log(data);   
      $(".fileters_client").html('');
       $(".fileters_client").append(data);   
       $(".LoadingImage").hide();
     
    }
    });
   
   }else{
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Staff/client_filter_year",
    data:  { 'id':id }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
      console.log(data);   
      $(".fileters_client").html('');
       $(".fileters_client").append(data);   
       $(".LoadingImage").hide();
     
    }
    });
   
   }
   });
   $("#filter_deadline").change(function(){
   var id=$(this).val();
   if($(this).val()=='year'){    
      $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/Staff/deadline_filter_year",
      data:  { 'id':id }, 
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data){
       // console.log(data);   
        $(".deadline").html('');
         $(".deadline").append(data);   
         $(".LoadingImage").hide();     
      }
      });
   }else{
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/Staff/deadline_filter_month",
        data:  { 'id':id }, 
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data){
         // console.log(data);   
          $(".deadline").html('');
           $(".deadline").append(data);   
           $(".LoadingImage").hide();     
        }
        });
   }
   });
   
   $("#proposal").change(function(){
   var id=$(this).val();
   if($(this).val()=='year'){    
      $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/Staff/filter_proposal_year",
      data:  { 'id':id }, 
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data){
       // console.log(data);   
        $(".proposal").html('');
         $(".proposal").append(data);   
         $(".LoadingImage").hide();     
      }
      });
   }else{
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/Staff/filter_proposal_month",
        data:  { 'id':id }, 
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data){
         // console.log(data);   
          $(".proposal").html('');
           $(".proposal").append(data);   
           $(".LoadingImage").hide();     
        }
        });
   }
   });
   google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
   
      function drawChart2() {
   
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Accepted',     <?php echo $accept_proposal['proposal_count']; ?>],
          ['In Discussion',      <?php echo $indiscussion_proposal['proposal_count']; ?>],
          ['Sent',  <?php echo $sent_proposal['proposal_count']; ?>],
          ['Viewed', <?php echo $viewed_proposal['proposal_count']; ?>],
          ['Declined',    <?php echo $declined_proposal['proposal_count']; ?>],
           ['Archived',    <?php echo $archive_proposal['proposal_count']; ?>]
        ]);
   
        var options = {
          title: 'My Daily Activities'
        };
   
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
   
        chart.draw(data, options);
      }
   
   
</script>
<script type="text/javascript">
   google.charts.load("current", {packages:["corechart"]});
   google.charts.setOnLoadCallback(drawChart);
   function drawChart1() {
     var data = google.visualization.arrayToDataTable([
       ['Legal Form', 'Count'],
       ['Private Limited company',     <?php echo $Private['limit_count']; ?>],
       ['Public Limited company',      <?php echo $Public['limit_count']; ?>],
       ['Limited Liability Partnership',  <?php echo $limited['limit_count']; ?>],
       ['Partnership', <?php echo $Partnership['limit_count']; ?>],
       ['Self Assessment',    <?php echo $self['limit_count']; ?>],
       ['Trust',<?php echo $Trust['limit_count']; ?>],
       ['Charity',<?php echo $Charity['limit_count']; ?>],
        ['Other',<?php echo $Other['limit_count']; ?>],
     ]);
   
     var options = {
       title: 'Client Lists',
       pieHole: 0.4,
     };
   
     var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
     chart.draw(data, options);
   }
   
</script>
<p id="myValueHolder"></p>
<script type="text/javascript">
 function drawChart () {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', 'Value');
    
    data.addRows([
      ["Confirmation", <?php if(isset($confirmation['deadline_count']) && $confirmation['deadline_count']!=''){ echo $confirmation['deadline_count']; }else{ echo '0'; } ?>],
      ["Accounts", <?php if(isset($account['deadline_count']) &&  $account['deadline_count']!=''){ echo $account['deadline_count']; }else{ echo '0'; } ?>],
      ["Personal Tax", <?php if(isset($personal_due['deadline_count']) && $personal_due['deadline_count']!=''){ echo $personal_due['deadline_count']; }else{ echo '0'; } ?>],
      ["VAT ", <?php if(isset($vat['deadline_count']) && $vat['deadline_count']!=''){ echo $vat['deadline_count'];  }else{ echo '0'; } ?>],
      ["Payroll", <?php if(isset($payroll['deadline_count']) && $payroll['deadline_count']!=''){ echo $payroll['deadline_count']; }else{ echo '0'; } ?>],
      ["WorkPlace Pension - AE", <?php if(isset($pension['deadline_count']) && $pension['deadline_count']!=''){ echo $pension['deadline_count']; }else{ echo '0'; } ?>],
      ["CIS", <?php if(isset($registeration['deadline_count']) && $registeration['deadline_count']!=''){ echo $registeration['deadline_count']; }else{ echo '0'; } ?>],
      ["CIS-Sub", 0],
      ["P11D", <?php if(isset($plld['deadline_count']) && $plld['deadline_count']!=''){ echo $plld['deadline_count']; }else{ echo '0'; } ?>],
      ["Management Accounts", <?php if(isset($management['deadline_count']) && $management['deadline_count']!=''){ echo $management['deadline_count']; }else{ echo '0'; }  ?>],
      ["Bookkeeping", <?php if(isset($booking['deadline_count']) && $booking['deadline_count']!=''){ echo $booking['deadline_count']; }else{ echo '0'; } ?>],
      ["Investigation Insurance", <?php if(isset($insurance['deadline_count']) && $insurance['deadline_count']!=''){ echo $insurance['deadline_count']; }else{ echo '0'; } ?>],
      ["Registeration", <?php if(isset($registeration['deadline_count']) && $registeration['deadline_count']!=''){ echo $registeration['deadline_count']; }else{ echo '0'; }  ?>],
      ["Tax Advice", <?php if(isset($investigation['deadline_count']) && $investigation['deadline_count']!=''){ echo $investigation['deadline_count']; }else{ echo '0'; } ?>],
      ["Tax Investigation", <?php if(isset($investigation['deadline_count']) && $investigation['deadline_count']!=''){ echo $investigation['deadline_count']; }else{ echo '0'; } ?>]
    ]);

    // Set chart options
   

    
    var chart = new google.visualization.ColumnChart(document.querySelector('#columnchart_values'));
    
    google.visualization.events.addListener(chart, 'select', function () {
        var selection = chart.getSelection();
        if (selection.length) { 
      
            var row = selection[0].row;
            document.querySelector('#myValueHolder').innerHTML = data.getValue(row, 1);
            
            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1, {
                type: 'string',
                role: 'style',
                calc: function (dt, i) {
                 // alert(row);
                 if(i == row){
                  load(row);
               }
                    return (i == row) ? '' : null;
                }
            }]);
            
            chart.draw(view, {
                height: 400,
                width: 600
            });
        }
    });
    
    chart.draw(data, {
        height: 400,
        width: 1200
    });
}
google.load('visualization', '1', {packages:['corechart'], callback: drawChart});

function load(row){
//alert(row);
var index = row;
 switch (index) {
            case 0: window.open('<?php echo base_url(); ?>Firm_dashboard/confirm_statement', '_blank'); break;
            case 1: window.open('<?php echo base_url(); ?>Firm_dashboard/accounts', '_blank'); break;
            case 2: window.open('<?php echo base_url(); ?>Firm_dashboard/personal_tax', '_blank'); break;
            case 3: window.open('<?php echo base_url(); ?>Firm_dashboard/vat', '_blank'); break;
            case 4: window.open('<?php echo base_url(); ?>Firm_dashboard/payroll', '_blank'); break;  
            case 5: window.open('<?php echo base_url(); ?>Firm_dashboard/workplace', '_blank'); break;
            case 6: window.open('<?php echo base_url(); ?>Firm_dashboard/cis', '_blank'); break;
            case 7: window.open('<?php echo base_url(); ?>Firm_dashboard/cis_sub', '_blank'); break;
            case 8: window.open('<?php echo base_url(); ?>Firm_dashboard/p11d', '_blank'); break;
            case 9: window.open('<?php echo base_url(); ?>Firm_dashboard/management', '_blank'); break;
            case 10: window.open('<?php echo base_url(); ?>Firm_dashboard/bookkeeping', '_blank'); break;
            case 11: window.open('<?php echo base_url(); ?>Firm_dashboard/investication', '_blank'); break;  
            case 12: window.open('<?php echo base_url(); ?>Firm_dashboard/register', '_blank'); break;
            case 13: window.open('<?php echo base_url(); ?>Firm_dashboard/tax', '_blank'); break;
            case 14: window.open('<?php echo base_url(); ?>Firm_dashboard/tax_inves', '_blank'); break;  
        }
}
</script>
<script type="text/javascript">
   $(document).ready(function () {
     
     $( "#tabs112" ).tabs({
   
     });
   
     $("#tabs112 ul.md-tabs.tabs112 li").click(function(){
       drawChart1();
        drawChart2();
       drawChart();
     });  
   
   });


   $(document).ready(function(){
    $(".update_service").click(function(){
      //alert($(this).attr('id'));
         if($(this).is(':checked')) { 
            alert($(this).attr('id'));
         }  
    });
   });

</script>