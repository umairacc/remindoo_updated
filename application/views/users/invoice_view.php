<?php $this->load->view('includes/header');?>
<style>
/*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   li.ui-state-default.ui-sortable-handle.ui-sortable-helper
   {
      /*top: 0px !important;*/
   }
   .card.firm-field span.pro-head {
    font-weight: 600;
    font-size: 15px;
    text-transform: uppercase;
    }
   .card.firm-field {
    padding: 30px;
    background: #fff;
    box-shadow: 0 1px 11px 0 rgba(0, 0, 0, 0.12);
    }  
   body
   {
      overflow-x:hidden; 
   }
   .card.firm-field {
       padding: 30px;
   }
   .invoice-details {
       margin-top: 30px;
   }
   span.invoice-topinner {
    float: left;
    width: 15%;
   }
   span.invoice-topinner label {
    display: block;
    text-transform: capitalize;
    font-size: 14px;
   }
   span.invoice-topinner input {
    padding-left: 10px;
    height: 30px;
   }
   span.invoice-topinner {
    float: left;
    width: 17%;
   }
   i.fa.fa-eye {
    color: #303549;
    font-size: 14px;
   }
   span.preview-pdf {
    float: right;
    display: inline-block;
    height: 60px;
    vertical-align: middle;
    line-height: 60px;
    color: #303549;
    font-size: 14px;
   }
   .invoice-top {
    float: left;
    width: 100%;
   }
   .amounts-detail {
    float: right;
    width: auto;
    padding-top: 30px;
   }
   .amounts-detail select {
    background: transparent;
    border: 1px solid #dbdbdb;
    height: 30px;
    padding: 0 10px;
    text-transform: capitalize;
   }
   .amounts-detail label {
    text-transform: capitalize;
    font-size: 14px;
    padding-right: 5px;
   }
   .invoice-table table {
    width: 100%;
    margin-top: 30px;
   }
   .invoice-table table th {
    text-transform: capitalize;
    font-size: 14px;
    padding: 5px 10px;

   }
   .invoice-table table tr:first-child {
    border: 1px solid #ccc;
   }
   .invoice-table table td {
    border: 1px solid #ccc;
    padding: 10px;
    height: 30px;
   }
   .invoice-table table td:last-child {
    text-align: center;
   }
   td i.fa.fa-times:hover {
    color: red;
    transition: color 0.5s ease;
    cursor: pointer;
   }
   .add-newline input {
    background: #0956ff !important;
    color: #fff;
    border: none;
    text-transform: capitalize;
    font-size: 14px;
    margin-top: 20px;
    padding: 5px 10px;
   }
   .add-newline {
    display: inline-block;
   }
   .sub-tot_table {
    float: right;
    display: inline-block;
    width:35%;
   }
   .sub-tot_table th, .sub-tot_table td, .sub-tot_table tr {
    border: none !important;
   }
   .sub-tot_table td {
    font-size: 14px;
   }
   tr.grand-total {
    border-top: 2px solid #ccc !important;
    border-bottom: 2px solid #ccc !important;
   }
   .table-savebtn input {
    background: #0956ff;
    color: #fff;
    border: none;
    font-size: 15px;
    padding: 3px 10px;
    text-transform: capitalize;
   }
   .approve-btn input.approve {
    background: #0956ff;
    border: none;
    color: #fff;
    text-transform: capitalize;
    font-size: 15px;
    padding: 2px 10px 4px;
    margin-right: 10px;
   }
   .approve-btn a.cancel-btn {
    background: gray;
    color: #fff;
    padding: 5px 10px;
    font-size: 15px;
    text-transform: capitalize;
   }
   .table-savebtn {
    display: inline-block;
    padding-top: 20px;
   }
   .approve-btn {
    display: inline-block;
    float: right;
    padding-top: 20px;
   }
   .invoice-table
   {
      padding-left: 0;
   }
   .save-approve {
    float: left;
    width: 100%;
   }
   @media(max-width: 1024px) {

      span.invoice-topinner {
       float: left;
       width: auto;
       padding-right: 15px;
       padding-top: 10px;
      }

   }
   @media(max-width: 991px) {

      span.preview-pdf
      {
         float: right;
         width: 100%;
         height: auto;
         line-height: normal;
         text-align: right;
         padding: 15px 0 0 15px;
      }
      .sub-tot_table
      {
         width:auto;
         padding-top: 10px;

      }
   }
   @media(min-width: 640px) and (max-width: 767px) {

      span.invoice-topinner input
      {
         width: 100%;
      }
      span.invoice-topinner
      {
         width:50%;
      }
      .sub-tot_table
      {
         width: auto;
      }

   }
   @media(max-width: 639px) {

      span.invoice-topinner, span.invoice-topinner input
      {
         width:100%;
      }
      .sub-tot_table
      {
         width: 100%;
      }

   }
   @media(max-width: 424px) {
      .approve-btn
      {
         width:100%;
         text-align: right;
      }
      .sub-tot_table
      {
         overflow-x:auto;
      }
   }


</style>
<!-- management block -->
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <div class="card firm-field">
               <span class="pro-head">new invoice</span>
               <div class="selectnew-invoice">
               <div class="newinvoice-btn">
                 <span class="add-newinvoice dropdown-toggle" data-toggle="dropdown">+ New</span>
                 <span class="caret"></span></button>
                  <ul class="dropdown-menu">
                    <li><a href="#"><i class="fa fa-sticky-note-o" aria-hidden="true"></i>Invoice</a></li>
                    <li><a href="#"><i class="fa fa-sticky-note-o" aria-hidden="true"></i>Quote</a></li>
                    <li><a href="#"><i class="fa fa-repeat" aria-hidden="true"></i>Repeating invoice</a></li>
                    <li><a href="#"><i class="fa fa-repeat" aria-hidden="true"></i>Credit note</a></li>
                    <li><a href="#"><i class="fa fa-repeat" aria-hidden="true"></i>Add contact group</a></li>
                  </ul>
                </div>
                <a href="javascript:;" class="statements">Send Statements</a>
                <a href="javascript:;" class="import">Import</a>
               </div>
               <!-- <div class="j-info"><i class="fa fa-circle" aria-hidden="true"></i> please not that only selected ( <i class="fa fa-check" aria-hidden="true"></i> ) fields are available in important template.</div> -->
               <div class="invoice-details">
                     <div class="invoice-top">
                        <span class="invoice-topinner">
                           <label>to</label>
                           <input type="text" name="name">
                        </span>
                        <span class="invoice-topinner">
                           <label>date</label>
                           <input type="text" name="name" class="datepicker">
                        </span>
                        <span class="invoice-topinner">
                           <label>due date</label>
                           <input type="text" name="name" class="datepicker">
                        </span>
                        <span class="invoice-topinner">
                           <label>invoice</label>
                           <input type="text" name="name">
                        </span>
                        <span class="invoice-topinner">
                           <label>reference</label>
                           <input type="text" name="name">
                        </span>
                        <span class="preview-pdf">
                              <i class="fa fa-eye" aria-hidden="true"></i>
                              preview
                        </span>
                     </div>
                     <div class="amounts-detail">
                        <label>amounts are</label>
                        <select>
                           <option>tax exclusive</option>
                           <option>tax</option>

                        </select>
                     </div>
                     <div class="invoice-table">
                        <div class="table-responsive">
                           <table>
                              <tr>
                                 <th></th>
                                 <th>item</th>
                                 <th>description</th>
                                 <th>qty</th>
                                 <th>unit price</th>
                                 <th>disc%</th>
                                 <th>account</th>
                                 <th>tax rate</th>
                                 <th>tax amount</th>
                                 <th>amount GBP</th>
                                 <th></th>
                              </tr>
                              <tr>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td><i class="fa fa-times" aria-hidden="true"></i></td>
                              </tr>
                               <tr>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td><i class="fa fa-times" aria-hidden="true"></i></td>
                              </tr>
                               <tr>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                                 <td><i class="fa fa-times" aria-hidden="true"></i></td>
                              </tr>
                           </table> 
                          
                        </div>
                         <div class="add-newline">
                              <input type="button" value="add new line">
                           </div>
                           <div class="sub-tot_table">
                              <table>
                                 <tbody>
                                    <tr>
                                       <th>sub total</th>
                                       <td>0.00</td>
                                    </tr>
                                    <tr>
                                       <th>VAT</th>
                                       <td>0.00</td>
                                    </tr>
                                    <tr>
                                       <th>includes adjustments to Tax</th>
                                       <td>0.00</td>
                                    </tr>
                                    <tr class="grand-total">
                                       <th>total</th>
                                       <td>0.00</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                     </div><!--invoice table -->
                     <div class="save-approve">
                        <div class="table-savebtn">
                           <input type="button" value="save">
                        </div>
                        <div class="approve-btn">
                           <input type="button" class="approve" value="approve">
                           <a href="javascript:;" class="cancel-btn">cancel</a> 
                        </div>
                     </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ajax loader -->
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style>
<!-- ajax loader end-->
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
   <h1>Warning!!</h1>
   <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
   <div class="iew-container">
      <ul class="iew-download">
         <li>
            <a href="http://www.google.com/chrome/">
               <img src="assets/images/browser/chrome.png" alt="Chrome">
               <div>Chrome</div>
            </a>
         </li>
         <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
               <img src="assets/images/browser/firefox.png" alt="Firefox">
               <div>Firefox</div>
            </a>
         </li>
         <li>
            <a href="http://www.opera.com">
               <img src="assets/images/browser/opera.png" alt="Opera">
               <div>Opera</div>
            </a>
         </li>
         <li>
            <a href="https://www.apple.com/safari/">
               <img src="assets/images/browser/safari.png" alt="Safari">
               <div>Safari</div>
            </a>
         </li>
         <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
               <img src="assets/images/browser/ie.png" alt="">
               <div>IE (9 & above)</div>
            </a>
         </li>
      </ul>
   </div>
   <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- j-pro js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Bootstrap date-time-picker js -->
<!--     <script type="text/javascript" src="assets/pages/advance-elements/moment-with-locales.min.js"></script>
   <script type="text/javascript" src="bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
   <script type="text/javascript" src="assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
   <script type="text/javascript" src="bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
   <script type="text/javascript" src="bower_components/datedropper/js/datedropper.min.js"></script> -->
<!-- Custom js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<script>
   $( document ).ready(function() {
   
       var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
       // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#1abc9c',
               jackColor: '#fff',
               size: 'small'
           });
       });
   
       $('#accordion_close').on('click', function(){
               $('#accordion').slideToggle(300);
               $(this).toggleClass('accordion_down');
       });
   });
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   $("#staff_form").validate({
     
          ignore: false,
   
          /*onfocusout: function(element) {
           if ( !this.checkable(element)) {
               this.element(element);
           }
       },*/
      // errorClass: "error text-warning",
       //validClass: "success text-success",
       /*highlight: function (element, errorClass) {
           //alert('em');
          // $(element).fadeOut(function () {
              // $(element).fadeIn();
           //});
       },*/
                           rules: {
                           first_name: {required: true},
                           last_name: {required: true},
                           email_id:{required: true,email:true},
                           telephone_number : {required:true,minlength:10,
     maxlength:10,
     number: true},
     
                           password:{required: true},
                           /*image_upload: {required: false, accept: "jpg|jpeg|png|gif"},*/
                          image_upload: {
       required: function(element) {
   
       if ($("#image_name").val().length > 0) {
          return false;
       }
       else {
       return true;
       }
     },
     accept: "jpg|jpeg|png|gif"
   },
                           hourly_rate: {required: true,number:true},
                           roles:{required: true},
                           
                           },
                           errorElement: "span" , 
                           errorClass: "field-error",                             
                            messages: {
                             first_name: "Enter a first name",
                             last_name: "Enter a last name",
                             roles:"Select a role",
                             email_id: {
       email:"Please enter a valid email address", 
       required:"Please enter a email address",
   
   },
                            telephone_number:{  yourtel:"Enter a valid phone number.",
     required: "Enter a phone number."},
                             password: {required: "Please enter your password "},
                             image_upload: {required: 'Required!', accept: 'Not an image!'},
                             
                             hourly_rate: {required: "Please Enter Your rate",number:"Please enter numbers Only"},
                            },
                            
   
                           
                           submitHandler: function(form) {
                               var formData = new FormData($("#staff_form")[0]);
                              
   
                               $(".LoadingImage").show();
   
                               $.ajax({
                                   url: '<?php echo base_url();?>staff/add_staff/',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) {
                                       
                                       if(data == 1){
                                          
                                          // $('#new_user')[0].reset();
                                               location.reload(true);
                                           $('.alert-success').show();
                                           $('.alert-danger').hide();
   
                                       }
                                       else{
                                          // alert('failed');
                                           $('.alert-danger').show();
                                           $('.alert-success').hide();
                                       }
                                   $(".LoadingImage").hide();
                                   },
                                   error: function() { $('.alert-danger').show();
                                           $('.alert-success').hide();}
                               });
   
                               return false;
                           } ,
                            invalidHandler: function(e, validator) {
              if(validator.errorList.length)
           $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
   
           }
                            
                       });
   
   
   });
</script>
</body>
</html>