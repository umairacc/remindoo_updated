<?php $this->load->view('includes/header');?>

    <div class="modal-alertsuccess alert succ header_popup_info_msg" <?php if(empty($_SESSION['success_msg'])){ echo 'style="display:none;"'; } ?>>
       <div class="header_newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
       <div class="header_pop-realted1">
       <div class="header_position-alert1">
             <?php if(!empty($_SESSION['success_msg'])){ echo $_SESSION['success_msg']; } ?>
       </div>
       </div>
       </div>
    </div> 

<section class="client-details-view various-section01 floating_set all-notifications-wrapper">
	<div class="pcoded-content">
		<div class="pcoded-inner-content">
			<div class="page-body">
				
	<div class="client-inform-data1 floating_set">

<div class="deadline-crm1 floating_set">
						 <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
							 <h4> Notifications</h4>
						 </ul>
					 </div>

	<form action="<?php echo base_url(); ?>sample/notification_settings" method="post">
<div class="information-tab floating_set">
	<div class="width-inform1">
	<div class="notification-section1">
			<h3>Notifications</h3>
			<div class="notification-bing">
					<h4>Notifications</h4>
					<div class="form-on-off">
							<label>Nofications</label>
							<input type="checkbox" class="js-small f-right main_notify" name="notification" <?php if($section_settings['notification']=='0'){ ?> Checked="checked" <?php  } ?> >
					</div>
					<!-- <div class="form-on-off">
							<label>Nofications sound #bing</label>
							<input type="checkbox" class="js-small f-right">
					</div> -->
			</div>
	</div>		

	<div class="notification-section1 sub_notifiy">
			<h3>Notifications</h3>
			<div class="notification-bing">
				<!-- 	<h4>Alerts</h4>
					<div class="form-on-off">
							<label>Co email</label>
							<input type="checkbox" class="js-small f-right">
					</div> -->
					<h4>Reminders</h4>
					<div class="form-on-off">
							<label>Invoice</label>
							<input type="checkbox" class="js-small f-right" name="invoice_notification" <?php if($section_settings['invoice_notification']=='0'){ ?> Checked="checked" <?php  } ?>>
					</div>
					<div class="form-on-off">
							<label>Open tasks</label>
							<input type="checkbox" class="js-small f-right" name="task_notification" <?php if($section_settings['task_notification']=='0'){ ?> Checked="checked" <?php  } ?>>
					</div>
					<div class="form-on-off">
							<label>Proposal</label>
							<input type="checkbox" class="js-small f-right" name="proposal_notification" <?php if($section_settings['proposal_notification']=='0'){ ?> Checked="checked" <?php  } ?>>
					</div>
					<div class="form-on-off">
							<label>Deadline Notifications</label>
							<input type="checkbox" class="js-small f-right" name="deadline_notification" <?php if($section_settings['deadline_notification']=='0'){ ?> Checked="checked" <?php  } ?>>
					</div>
					<div class="form-on-off">
							<label>Client MemberShip Alerts</label>
							<input type="checkbox" class="js-small f-right" name="client_notification" <?php if($section_settings['client_notification']=='0'){ ?> Checked="checked" <?php  } ?>>
					</div>
					<h4>Updates</h4>
					<div class="form-on-off">
							<label>Document Notification</label>
							<input type="checkbox" class="js-small f-right" name="document_notification" <?php if($section_settings['document_notification']=='0'){ ?> Checked="checked" <?php  } ?>>
					</div>

					<div class="form-on-off">
							<label>Companyhouse Notification</label>
							<input type="checkbox" class="js-small f-right" name="companyhouse_notification" <?php if($section_settings['companyhouse_notification']=='0'){ ?> Checked="checked" <?php  } ?>>
					</div>

					<div class="form-on-off">
							<label>Leads Notification</label>
							<input type="checkbox" class="js-small f-right" name="Leads_notification" <?php if($section_settings['Leads_notification']=='0'){ ?> Checked="checked" <?php  } ?>>
					</div>


					
					<!-- <div class="form-on-off">
							<label>Hustle&co Weekly Digest</label>
							<input type="checkbox" class="js-small f-right">
					</div> -->
					<!-- <div class="upcoming-reports">
					<h4><strong>Weekly Reports</strong> <span>Coming</span></h4>
					
					<div class="form-on-off">
							<label>Monday (Prep email)</label>
							<input type="checkbox" class="js-small f-right">
					</div>
					<div class="form-on-off">
							<label>Friday (Wrap-up-email)</label>
							<input type="checkbox" class="js-small f-right">
					</div>
				</div> -->
			</div>


	</div>	

	
	
	</div>

	<div class="form-group button-submit01">
				<button type="submit" name="submit">Update</button>
		</div>

</div>
</form>

</div>
			</div>
		</div>
	</div>
</section>			


<?php $this->load->view('includes/footer');?>

<script type="text/javascript">

	$(document).on('click','#close_info_msg',function()
	{
        $('.header_popup_info_msg').hide();
	});

	$(document).on('change','.main_notify',function()
	{
		if($(this). prop("checked"))
		{
			
		}
		else
		{
			
		}

	});

</script>