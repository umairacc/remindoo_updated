<?php $this->load->view('includes/header');?>
<!-- Modal -->
<!-- <div id="myModalcolumn" class="modal fade" role="dialog">
  <div class="modal-dialog modal-column-setting">
  
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Choose new column</h4>
      </div> -->
      <form id="column_setting" class="validation" method="post" action=""> 
      <div class="modal-body">
         
    <div class="card">
    <!-- admin start-->
    <?php 
    /*echo "<pre>";
    print_r($client);echo "</pre>";*/ ?>
    <div class="modal-alertsuccess alert alert-success" style="display:none;"
            ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<div class="pop-realted1">
    <div class="position-alert1">Your column setting was successfully Updated.
		</div></div></div>
			
            <div class="modal-alertsuccess alert alert-danger" style="display:none;">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<div class="pop-realted1">
    <div class="position-alert1">Please fill the required fields.
		</div></div></div>
   
    <div class="addnewclient_pages1 floating_set">
    
    <ul class="nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#required_information">Required information</a></li>
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#main_Contact">Main Contact</a></li> 
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#business">Business</a></li> 
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#company">Company</a></li> 
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#income">Income</a></li> 
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#services">Services</a></li> 
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#account">Account & Returns</a></li> 
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#confirmation">Confirmation Statement</a></li> 
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#vat">VAT</a></li> 
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#registration">Registration</a></li> 
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#other">Other Details</a></li> 
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#pay">PAYE Details</a></li> 
</ul>   
<!-- tab close -->
</div>

    <div class="management_section floating_set">

    
    <div class="card-block accordion-block">
    
    <!-- tab start -->
        <div class="tab-content">
        <div id="required_information" class="tab-pane fade in active">
            
        <div class="management_form1">  
            <!-- <div class="form-titles"><a href="#" class="waves-effect" data-toggle="modal" data-target="#default-Modal"><h2>View on Companies House</h2></a></div> -->
      <!--  <form> -->

            <div class="form-group row help_icon radio_bts">
            <label class="col-sm-4 col-form-label">Name</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="company_name" id="company_name" placeholder="Accotax Limited" value="1" <?php if(isset($client['company_name']) && ($client['company_name']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>   
            

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Legal Form</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="legal_form" id="legal_form" value="1" <?php if(isset($client['legal_form']) && ($client['legal_form']==1) ){ ?> checked="checked"<?php } ?>>
           
            </div>
            </div>

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Allocation Holder</label>
            <div class="col-sm-8">
          
            <input type="checkbox" class="js-small f-right" name="allocation_holder" id="allocation_holder" value="1" <?php if(isset($client['allocation_holder']) && ($client['allocation_holder']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

    </div>
    </div>  <!-- 1tab close-->
      
    <div id="business" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!-- <form>-->
                <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Trading As</label>
            <div class="col-sm-8">
                
                <input type="checkbox" class="js-small f-right" name="tradingas" id="tradingas" value="1" <?php if(isset($client['tradingas']) && ($client['tradingas']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>   

             <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Commenced Trading</label>
            <div class="col-sm-8">
            
                <input type="checkbox" class="js-small f-right" name="commenced_trading" id="commenced_trading" value="1" <?php if(isset($client['commenced_trading']) && ($client['commenced_trading']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>  

             <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Registered for SA</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="registered_for_sa" id="registered_for_sa" value="1" <?php if(isset($client['registered_for_sa']) && ($client['registered_for_sa']==1) ){ ?> checked="checked"<?php } ?>>
                
             </div>
             </div>  

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Turnover</label>
            <div class="col-sm-8">
          
            <input type="checkbox" class="js-small f-right" name="business_details_turnover" id="business_details_turnover" value="1" <?php if(isset($client['business_details_turnover']) && ($client['business_details_turnover']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Nature of Business</label>
            <div class="col-sm-8">
    
                 <input type="checkbox" class="js-small f-right" name="business_details_nature_of_business" id="business_details_nature_of_business" value="1" <?php if(isset($client['business_details_nature_of_business']) && ($client['business_details_nature_of_business']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 
        <!-- </form>-->
      </div>
        </div> <!-- 2tab close -->



    <!-- 3tab start -->
   
    <div id="main_Contact" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!-- <form>-->
             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Person</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="person" id="person" value="1" <?php if(isset($client['person']) && ($client['person']==1) ){ ?> checked="checked"<?php } ?>>

            </div>
            </div>

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Title</label>
            <div class="col-sm-8">
            
             <input type="checkbox" class="js-small f-right" name="title" id="title" value="1" <?php if(isset($client['title']) && ($client['title']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">First Name</label>
            <div class="col-sm-8">
           

                 <input type="checkbox" class="js-small f-right" name="first_name" id="first_name" value="1" <?php if(isset($client['first_name']) && ($client['first_name']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>  

             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Middle Name</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="middle_name" id="middle_name" value="1" <?php if(isset($client['middle_name']) && ($client['middle_name']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>   

             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Last Name</label>
            <div class="col-sm-8">
               
                <input type="checkbox" class="js-small f-right" name="last_name" id="last_name" value="1" <?php if(isset($client['last_name']) && ($client['last_name']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>  

             <div class="form-group row radio_bts ">
            <label class="col-sm-4 col-form-label">Create Self Assessment Client</label>
            <div class="col-sm-8">
               
            <input type="checkbox" class="js-small f-right" name="create_self_assessment_client" id="create_self_assessment_client" value="1" <?php if(isset($client['create_self_assessment_client']) && ($client['create_self_assessment_client']==1) ){ ?> checked="checked"<?php } ?>>
        
             </div>
             </div>  

             <div class="form-group row ">
            <label class="col-sm-4 col-form-label">Client Does Self Assessment</label>
            <div class="col-sm-8">
          
                <input type="checkbox" class="js-small f-right" name="client_does_self_assessment" id="client_does_self_assessment" value="1" <?php if(isset($client['client_does_self_assessment']) && ($client['client_does_self_assessment']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>  

              <div class="form-group row help_icon">
            <label class="col-sm-4 col-form-label">Perferred Name</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="name" id="name" value="1" <?php if(isset($client['preferred_name']) && ($client['preferred_name']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>  

             <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Date of Birth</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="dob" id="dob" value="1" <?php if(isset($client['date_of_birth']) && ($client['date_of_birth']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>  

             <div class="form-group row ">
            <label class="col-sm-4 col-form-label">Email</label>
            <div class="col-sm-8">
            
                <input type="checkbox" class="js-small f-right" name="email_id" id="email_id" value="1" <?php if(isset($client['email']) && ($client['email']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 

             <div class="form-group row ">
            <label class="col-sm-4 col-form-label">Postal Address</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="postal_address" id="postal_address" value="1" <?php if(isset($client['postal_address']) && ($client['postal_address']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>  

              <div class="form-group row ">
            <label class="col-sm-4 col-form-label">Telephone Number</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="telephone_number" id="telephone_number" value="1" <?php if(isset($client['telephone_number']) && ($client['telephone_number']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 

             <div class="form-group row ">
            <label class="col-sm-4 col-form-label">Mobile Number</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="mobile_number" id="mobile_number" value="1" <?php if(isset($client['mobile_number']) && ($client['mobile_number']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 

             <div class="form-group row ">
            <label class="col-sm-4 col-form-label">NI Number</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="ni_number" id="ni_number" value="1" <?php if(isset($client['ni_number']) && ($client['ni_number']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 

             <div class="form-group row ">
            <label class="col-sm-4 col-form-label">Personal UTR Number</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="personal_utr_number" id="personal_utr_number" value="1" <?php if(isset($client['personal_utr_number']) && ($client['personal_utr_number']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 

              <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Terms Signed</label>
            <div class="col-sm-8">
               
                <input type="checkbox" class="js-small f-right" name="terms_signed" id="terms_signed" value="1" <?php if(isset($client['terms_signed']) && ($client['terms_signed']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>  

             <div class="form-group row radio_bts ">
            <label class="col-sm-4 col-form-label">Phone ID Verified</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="photo_id_verified" id="photo_id_verified" value="1" <?php if(isset($client['photo_id_verified']) && ($client['photo_id_verified']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>  

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Address Verified</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="address_verified" id="address_verified" value="1" <?php if(isset($client['address_verified']) && ($client['address_verified']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 

       <!-- </form>-->
      </div>
    </div> <!-- accordion-panel -->  
<!-- 3tab close -->
    
        <div id="income" class="tab-pane fade">
            <div class="management_form1 management_accordion">  
        <!--<form>-->

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Previous</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="previous" id="previous" value="1" <?php if(isset($client['previous']) && ($client['previous']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Current</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="current" id="current" value="1" <?php if(isset($client['current']) && ($client['current']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">IR 35 Notes</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="ir_35_notes" id="ir_35_notes" value="1" <?php if(isset($client['ir_35_notes']) && ($client['ir_35_notes']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>  



       <!-- </form>-->
      </div> 
    </div> <!-- accordion-panel -->  
<!-- 4tab close -->


<!-- 5tab start -->
    <div id="other" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!-- <form>-->

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Previous Accountant</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="previous_accountant" id="previous_accountant" value="1" <?php if(isset($client['previous_accountant']) && ($client['previous_accountant']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Referred By</label>
            <div class="col-sm-8">
                
        <input type="checkbox" class="js-small f-right" name="refered_by" id="refered_by" value="1" <?php if(isset($client['refered_by']) && ($client['refered_by']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Allocated Office</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="allocated_office" id="allocated_office" value="1" <?php if(isset($client['allocated_office']) && ($client['allocated_office']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>

             <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Initial Contact</label>
            <div class="col-sm-8">
            
                <input type="checkbox" class="js-small f-right" name="inital_contact" id="inital_contact" value="1" <?php if(isset($client['inital_contact']) && ($client['inital_contact']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 

              <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Quote Email Sent</label>
            <div class="col-sm-8">
          <input type="checkbox" class="js-small f-right" name="quote_email_sent" id="quote_email_sent" value="1" <?php if(isset($client['quote_email_sent']) && ($client['quote_email_sent']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 

              <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Welcome Email</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="welcome_email_sent" id="welcome_email_sent" value="1" <?php if(isset($client['welcome_email_sent']) && ($client['welcome_email_sent']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 

               <div class="form-group row">
            <label class="col-sm-4 col-form-label">Internal Reference</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="internal_reference" id="internal_reference" value="1" <?php if(isset($client['internal_reference']) && ($client['internal_reference']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>

               <div class="form-group row">
            <label class="col-sm-4 col-form-label">Profession</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="profession" id="profession" value="1" <?php if(isset($client['profession']) && ($client['profession']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>

               <div class="form-group row">
            <label class="col-sm-4 col-form-label">Website</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="website" id="website" value="1" <?php if(isset($client['website']) && ($client['website']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>

               <div class="form-group row">
            <label class="col-sm-4 col-form-label">Accounting System</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="accounting_system" id="accounting_system" value="1" <?php if(isset($client['accounting_system']) && ($client['accounting_system']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>

              <div class="form-group row">
            <label class="col-sm-4 col-form-label">Notes</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="notes" id="notes" value="1" <?php if(isset($client['notes']) && ($client['notes']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>  
             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Urgent</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="other_urgent" id="other_urgent" value="1" <?php if(isset($client['other_urgent']) && ($client['other_urgent']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 
             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Profile Image</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="profile_image" id="profile_image" value="1" <?php if(isset($client['profile_image']) && ($client['profile_image']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 
             <div class="form-group row name_fields radio_button01">
            <label class="col-sm-4 col-form-label">Gender</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="gender" id="gender" value="1" <?php if(isset($client['gender']) && ($client['gender']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>
             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Packages</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="packages" id="packages" value="1" <?php if(isset($client['packages']) && ($client['packages']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>
              
       <!--</form>-->
      </div>  
    </div> <!-- accordion-panel -->  
<!-- 5tab close -->

   
    <div id="services" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!-- <form> -->
             <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Accounts</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="accounts" id="accounts" value="1" <?php if(isset($client['accounts']) && ($client['accounts']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Bookkeeping</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="bookkeeping" id="bookkeeping" value="1" <?php if(isset($client['bookkeeping']) && ($client['bookkeeping']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>

            

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">CT600 Return</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="ct600_return" id="ct600_return" value="1" <?php if(isset($client['ct600_return']) && ($client['ct600_return']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Payroll</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="payroll" id="payroll" value="1" <?php if(isset($client['payroll']) && ($client['payroll']==1) ){ ?> checked="checked"<?php } ?>>

             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Auto-Enrollement</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="auto_enrolment" id="auto_enrolment" value="1" <?php if(isset($client['auto_enrolment']) && ($client['auto_enrolment']==1) ){ ?> checked="checked"<?php } ?>>

             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">VAT Returns</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="vat_returns" id="vat_returns" value="1" <?php if(isset($client['vat_returns']) && ($client['vat_returns']==1) ){ ?> checked="checked"<?php } ?>>

             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Confirmation Statement</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="confirmation_statement" id="confirmation_statement" value="1" <?php if(isset($client['confirmation_statement']) && ($client['confirmation_statement']==1) ){ ?> checked="checked"<?php } ?>>

             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">CIS</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="cis" id="cis" value="1" <?php if(isset($client['cis']) && ($client['cis']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">P11D</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="p11d" id="p11d" value="1" <?php if(isset($client['p11d']) && ($client['p11d']==1) ){ ?> checked="checked"<?php } ?>>

             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Investigation Insurance</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="invesitgation_insurance" id="invesitgation_insurance" value="1" <?php if(isset($client['invesitgation_insurance']) && ($client['invesitgation_insurance']==1) ){ ?> checked="checked"<?php } ?>>

             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Registered Address</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="services_registered_address" id="services_registered_address" value="1" <?php if(isset($client['services_registered_address']) && ($client['services_registered_address']==1) ){ ?> checked="checked"<?php } ?>>

             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Bill Payment</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="bill_payment" id="bill_payment" value="1" <?php if(isset($client['bill_payment']) && ($client['bill_payment']==1) ){ ?> checked="checked"<?php } ?>>

             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Advice</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="advice" id="advice" value="1" <?php if(isset($client['advice']) && ($client['advice']==1) ){ ?> checked="checked"<?php } ?>>

             </div>
             </div>

             <div class="combing_pricing floation_set">
                <h3>Combing pricing</h3>

                 <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Monthly Charge</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="monthly_charge" id="monthly_charge" value="1" <?php if(isset($client['monthly_charge']) && ($client['monthly_charge']==1) ){ ?> checked="checked"<?php } ?>>

             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Annual Charge</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="annual_charge" id="annual_charge" value="1" <?php if(isset($client['annual_charge']) && ($client['annual_charge']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div>
         </div>


       <!-- </form>-->
      </div>
  </div>
<!-- 6tab close -->


        <!-- 7tab start -->
    <div id="account" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
        <!--<form>-->

            <div class="form-group row help_icon date_birth">
            <label class="col-sm-4 col-form-label">Accounts Period End</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="accounts_periodend" id="accounts_periodend" value="1" <?php if(isset($client['accounts_periodend']) && ($client['accounts_periodend']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row help_icon date_birth">
            <label class="col-sm-4 col-form-label">CH Year End(Companies House)</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="period_end_on" id="period_end_on" value="1" <?php if(isset($client['ch_yearend']) && ($client['ch_yearend']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row help_icon date_birth">
            <label class="col-sm-4 col-form-label">HMRC Year End</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="next_made_up_to" id="next_made_up_to" value="1" <?php if(isset($client['hmrc_yearend']) && ($client['hmrc_yearend']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row help_icon date_birth">
            <label class="col-sm-4 col-form-label">CH Accounts Next Due</label>
            <div class="col-sm-8">
                  <input type="checkbox" class="js-small f-right" name="next_due" id="next_due" value="1" <?php if(isset($client['ch_accounts_next_due']) && ($client['ch_accounts_next_due']==1) ){ ?> checked="checked"<?php } ?>>

            </div>
            </div>

             <div class="form-group row help_icon date_birth">
            <label class="col-sm-4 col-form-label">CT600 Due</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="ct600_due" id="ct600_due" value="1" <?php if(isset($client['ct600_due']) && ($client['ct600_due']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row help_icon">
            <label class="col-sm-4 col-form-label">Companies House Email Reminder</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="companies_house_email_remainder" id="companies_house_email_remainder" value="1" <?php if(isset($client['companies_house_email_remainder']) && ($client['companies_house_email_remainder']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Latest Action</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="latest_action" id="latest_action" value="1" <?php if(isset($client['latest_action']) && ($client['latest_action']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Latest Action Date</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="latest_action_date" id="latest_action_date" value="1" <?php if(isset($client['latest_action_date']) && ($client['latest_action_date']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Records Received</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="records_received_date" id="records_received_date" value="1" <?php if(isset($client['records_received_date']) && ($client['records_received_date']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

        <!--</form>-->
      </div>
  </div>  
<!-- 7tab close -->
    
        <!-- 8tab start -->
    
    <div id="confirmation" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!-- <form>-->

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Confirmation Statement Date</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="confirm_next_made_up_to" id="confirm_next_made_up_to" value="1" <?php if(isset($client['confirmation_statement_date']) && ($client['confirmation_statement_date']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Confirmation Statement Due</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="confirm_next_due" id="confirm_next_due" value="1" <?php if(isset($client['confirmation_statement_due_date']) && ($client['confirmation_statement_due_date']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Latest Action</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="confirmation_latest_action" id="confirmation_latest_action" value="1" <?php if(isset($client['confirmation_latest_action']) && ($client['confirmation_latest_action']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Latest Action Date</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="confirmation_latest_action_date" id="confirmation_latest_action_date" value="1" <?php if(isset($client['confirmation_latest_action_date']) && ($client['confirmation_latest_action_date']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Records Received</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="confirmation_records_received_date" id="confirmation_records_received_date" value="1" <?php if(isset($client['confirmation_records_received_date']) && ($client['confirmation_records_received_date']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Officers</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="confirmation_officers" id="confirmation_officers" value="1" <?php if(isset($client['confirmation_officers']) && ($client['confirmation_officers']==1) ){ ?> checked="checked"<?php } ?>>

            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Share Capital</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="share_capital" id="share_capital" value="1" <?php if(isset($client['share_capital']) && ($client['share_capital']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Shareholders</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="shareholders" id="shareholders" value="1" <?php if(isset($client['shareholders']) && ($client['shareholders']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">People with Significant Control</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="people_with_significant_control" id="people_with_significant_control" value="1" <?php if(isset($client['people_with_significant_control']) && ($client['people_with_significant_control']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

       <!-- </form>-->
      </div>
  </div>
<!-- 8tab close -->
    
    <div id="vat" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!-- <form>-->

                <div class="form-group row">
<label class="col-sm-4 col-form-label">VAT Quarters</label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="vat_quarters" id="vat_quarters" value="1" <?php if(isset($client['vat_quarters']) && ($client['vat_quarters']==1) ){ ?> checked="checked"<?php } ?>>

</div>
</div>
        
        <div class="form-group row help_icon date_birth">
            <label class="col-sm-4 col-form-label">VAT Quarter End</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="vat_quater_end_date" id="vat_quater_end_date" value="1" <?php if(isset($client['vat_quater_end_date']) && ($client['vat_quater_end_date']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Next Return Date</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="next_return_due_date" id="next_return_due_date" value="1" <?php if(isset($client['next_return_due_date']) && ($client['next_return_due_date']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row">
<label class="col-sm-4 col-form-label">Latest Action</label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="vat_latest_action" id="vat_latest_action" value="1" <?php if(isset($client['vat_latest_action']) && ($client['vat_latest_action']==1) ){ ?> checked="checked"<?php } ?>>

</div>
</div>  

        <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Latest Action Date</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="vat_latest_action_date" id="vat_latest_action_date" value="1" <?php if(isset($client['vat_latest_action_date']) && ($client['vat_latest_action_date']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>


            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Records Received</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="vat_records_received_date" id="vat_records_received_date" value="1" <?php if(isset($client['vat_records_received_date']) && ($client['vat_records_received_date']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>


             <div class="form-group row">
<label class="col-sm-4 col-form-label">VAT Member State</label>
<div class="col-sm-8">
     <input type="checkbox" class="js-small f-right" name="vat_member_state" id="vat_member_state" value="1" <?php if(isset($client['vat_member_state']) && ($client['vat_member_state']==1) ){ ?> checked="checked"<?php } ?>>
</div>
</div>  

    <div class="form-group row name_fields">
<label class="col-sm-4 col-form-label">VAT Number</label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="vat_number" id="vat_number" value="1" <?php if(isset($client['vat_number']) && ($client['vat_number']==1) ){ ?> checked="checked"<?php } ?>>
</div>
</div>   

<div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Date of Registration</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="vat_date_of_registration" id="vat_date_of_registration" value="1" <?php if(isset($client['vat_date_of_registration']) && ($client['vat_date_of_registration']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Effective Date</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="vat_effective_date" id="vat_effective_date" value="1" <?php if(isset($client['vat_effective_date']) && ($client['vat_effective_date']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Estimated Turnover</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="vat_estimated_turnover" id="vat_estimated_turnover" value="1" <?php if(isset($client['vat_estimated_turnover']) && ($client['vat_estimated_turnover']==1) ){ ?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Transfer of Going Concern</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="transfer_of_going_concern" id="transfer_of_going_concern" value="1" <?php if(isset($client['transfer_of_going_concern']) && ($client['transfer_of_going_concern']==1) ){ ?> checked="checked"<?php } ?>>
             </div>
             </div> 

               <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Involved in Any Other Business</label>
            <div class="col-sm-8">

   <input type="checkbox" class="js-small f-right" name="involved_in_any_other_business" id="involved_in_any_other_business" <?php if(isset($client['involved_in_any_other_business']) && ($client['involved_in_any_other_business']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div> 

               <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Flat Rate</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="flat_rate" id="flat_rate" <?php if(isset($client['flat_rate']) && ($client['flat_rate']==1) ){?> checked="checked"<?php } ?> >
             </div>
             </div> 


               <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Direct Debit</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="direct_debit" id="direct_debit" <?php if(isset($client['direct_debit']) && ($client['direct_debit']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div> 
        
              <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Annual Accounting Scheme</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="annual_accounting_scheme" id="annual_accounting_scheme" <?php if(isset($client['annual_accounting_scheme']) && ($client['annual_accounting_scheme']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div> 

             <div class="form-group row">
<label class="col-sm-4 col-form-label">Flat Rate Category</label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="flat_rate_category" id="flat_rate_category" <?php if(isset($client['flat_rate_category']) && ($client['flat_rate_category']==1) ){?> checked="checked"<?php } ?>>
</div>
</div>

    <div class="form-group row">
<label class="col-sm-4 col-form-label">Month of Last Quarter Submitted</label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="month_of_last_quarter_submitted" id="month_of_last_quarter_submitted" <?php if(isset($client['month_of_last_quarter_submitted']) && ($client['month_of_last_quarter_submitted']==1) ){?> checked="checked"<?php } ?>>
</div>
</div>
    
    <div class="form-group row">
<label class="col-sm-4 col-form-label">Box 5 of Last Quarter Submitted </label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="box5_of_last_quarter_submitted" id="box5_of_last_quarter_submitted" <?php if(isset($client['box5_of_last_quarter_submitted']) && ($client['box5_of_last_quarter_submitted']==1) ){?> checked="checked"<?php } ?>>
</div>
</div>

      <div class="form-group row">
            <label class="col-sm-4 col-form-label">VAT Address</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="vat_address" id="vat_address" <?php if(isset($client['vat_address']) && ($client['vat_address']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>


              <div class="form-group row">
            <label class="col-sm-4 col-form-label">Notes</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="vat_notes" id="vat_notes" <?php if(isset($client['vat_notes']) && ($client['vat_notes']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>
    


        <!--</form>-->
      </div>
  </div>
 
<!-- 9tab close -->
    
        <!-- 10tab start -->
    <!-- paye details -->
    <div id="pay" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
        <!--<form>-->

             <div class="form-group row">
<label class="col-sm-4 col-form-label">Employers Reference</label>
<div class="col-sm-8">
     <input type="checkbox" class="js-small f-right" name="employers_reference" id="employers_reference" <?php if(isset($client['employers_reference']) && ($client['employers_reference']==1) ){?> checked="checked"<?php } ?>>

</div>
</div>
        
         <div class="form-group row">
<label class="col-sm-4 col-form-label">Accounts Office Reference </label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="accounts_office_reference" id="accounts_office_reference" <?php if(isset($client['accounts_office_reference']) && ($client['accounts_office_reference']==1) ){?> checked="checked"<?php } ?>>
</div>
</div>

    
 <div class="form-group row">
<label class="col-sm-4 col-form-label">Years Required</label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="years_required" id="years_required" <?php if(isset($client['years_required']) && ($client['years_required']==1) ){?> checked="checked"<?php } ?>>
</div>
</div>


 <div class="form-group row">
<label class="col-sm-4 col-form-label">Annual/Monthly Submissions</label>
<div class="col-sm-8">
     <input type="checkbox" class="js-small f-right" name="annual_or_monthly_submissions" id="annual_or_monthly_submissions" <?php if(isset($client['annual_or_monthly_submissions']) && ($client['annual_or_monthly_submissions']==1) ){?> checked="checked"<?php } ?>>
</div>
</div>


 <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Irregular Monthly Pay</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="irregular_montly_pay" id="irregular_montly_pay" <?php if(isset($client['irregular_montly_pay']) && ($client['irregular_montly_pay']==1) ){?> checked="checked"<?php } ?>>

             </div>
             </div> 


 <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Nil EPS</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="nil_eps" id="nil_eps" <?php if(isset($client[0]['nil_eps']) && ($client[0]['nil_eps']==1) ){?> checked="checked"<?php } ?>> 
             </div>
             </div> 

              <div class="form-group row">
<label class="col-sm-4 col-form-label">Number of Employees</label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="no_of_employees" id="no_of_employees" <?php if(isset($client['no_of_employees']) && ($client['no_of_employees']==1) ){?> checked="checked"<?php } ?>>
</div>
</div>
    
     <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">First Pay Date</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="first_pay_date" id="first_pay_date" <?php if(isset($client['first_pay_date']) && ($client['first_pay_date']==1) ){?> checked="checked"<?php } ?>>

            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">RTI Deadline</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="rti_deadline" id="rti_deadline" <?php if(isset($client['rti_deadline']) && ($client['rti_deadline']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">PAYE Scheme Ceased</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="paye_scheme_ceased" id="paye_scheme_ceased" <?php if(isset($client['paye_scheme_ceased']) && ($client['paye_scheme_ceased']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row">
<label class="col-sm-4 col-form-label">PAYE Latest Action</label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="paye_latest_action" id="paye_latest_action" <?php if(isset($client['paye_latest_action']) && ($client['paye_latest_action']==1) ){?> checked="checked"<?php } ?>>
</div>
</div>

 <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">PAYE Latest Action Date</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="paye_latest_action_date" id="paye_latest_action_date" <?php if(isset($client['paye_latest_action_date']) && ($client['paye_latest_action_date']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">PAYE Records Received</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="paye_records_received" id="paye_records_received" <?php if(isset($client['paye_records_received']) && ($client['paye_records_received']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Next P11D Return Due</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="next_p11d_return_due" id="next_p11d_return_due" <?php if(isset($client['next_p11d_return_due']) && ($client['next_p11d_return_due']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Latest P11D Submitted</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="latest_p11d_submitted" id="latest_p11d_submitted" <?php if(isset($client['latest_p11d_submitted']) && ($client['latest_p11d_submitted']==1) ){?> checked="checked"<?php } ?>>

            </div>
            </div>

             <div class="form-group row">
<label class="col-sm-4 col-form-label">P11D Latest Action</label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="p11d_latest_action" id="p11d_latest_action" <?php if(isset($client['p11d_latest_action']) && ($client['p11d_latest_action']==1) ){?> checked="checked"<?php } ?>>
</div>
</div>
    

    <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">P11D Latest Action Date</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="p11d_latest_action_date" id="p11d_latest_action_date" <?php if(isset($client['p11d_latest_action_date']) && ($client['p11d_latest_action_date']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">P11D Records Received</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="p11d_records_received" id="p11d_records_received" <?php if(isset($client['p11d_records_received']) && ($client['p11d_records_received']==1) ){?> checked="checked"<?php } ?>>

            </div>
            </div>

             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">CIS Contractor</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="cis_contractor" id="cis_contractor" <?php if(isset($client['cis_contractor']) && ($client['cis_contractor']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div>

             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">CIS Subcontractor</label>
            <div class="col-sm-8">
                <input type="checkbox" name="cis_subcontractor" id="cis_subcontractor" class="js-small f-right" <?php if(isset($client['cis_subcontractor']) && ($client['cis_subcontractor']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div> 

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">CIS Deadline</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="cis_deadline" id="cis_deadline" <?php if(isset($client['cis_deadline']) && ($client['cis_deadline']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Auto-Enrollement Latest Action</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="auto_enrolment_latest_action" id="auto_enrolment_latest_action" <?php if(isset($client['auto_enrolment_latest_action']) && ($client['auto_enrolment_latest_action']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>


            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Auto-Enrollement Latest Action Date</label>
            <div class="col-sm-8">
        <input type="checkbox" class="js-small f-right" name="auto_enrolment_latest_action_date" id="auto_enrolment_latest_action_date" <?php if(isset($client['auto_enrolment_latest_action_date']) && ($client['auto_enrolment_latest_action_date']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Auto-Enrollement Record Received</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="auto_enrolment_records_received" id="auto_enrolment_records_received" <?php if(isset($client['auto_enrolment_records_received']) && ($client['auto_enrolment_records_received']==1) ){?> checked="checked"<?php } ?>>

            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Auto-Enrollement Staging</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="auto_enrolment_staging" id="auto_enrolment_staging" <?php if(isset($client['auto_enrolment_staging']) && ($client['auto_enrolment_staging']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Postponement Date</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="postponement_date" id="postponement_date" <?php if(isset($client['postponement_date']) && ($client['postponement_date']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">The Pensions Regulator Opt Out Date</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="the_pensions_regulator_opt_out_date" id="the_pensions_regulator_opt_out_date" <?php if(isset($client['the_pensions_regulator_opt_out_date']) && ($client['the_pensions_regulator_opt_out_date']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

           <!--  <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Re-Enrolment Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker" type="text" name="re_enrolment_date" id="re_enrolment_date" placeholder="dd/mm/yyy" value="<?php if(isset($client[0]['re_enrolment_date']) && ($client[0]['re_enrolment_date']!='') ){ echo $client[0]['re_enrolment_date'];}?>" />
            </div>
            </div>

             <div class="form-group row">
<label class="col-sm-4 col-form-label">Pension Provider </label>
<div class="col-sm-8">
<input type="text" name="pension_provider" id="pension_provider" value="<?php if(isset($client[0]['pension_provider']) && ($client[0]['pension_provider']!='') ){ echo $client[0]['pension_provider'];}?>"> 
</div>
</div>   -->

<div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Re-Enrolment Date</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="paye_re_enrolment_date" id="paye_re_enrolment_date" <?php if(isset($client['paye_re_enrolment_date']) && ($client['paye_re_enrolment_date']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

              <div class="form-group row">
<label class="col-sm-4 col-form-label">Pension Provider</label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="paye_pension_provider" id="paye_pension_provider" <?php if(isset($client['paye_pension_provider']) && ($client['paye_pension_provider']==1) ){?> checked="checked"<?php } ?>>
</div>
</div>  

              <div class="form-group row">
<label class="col-sm-4 col-form-label">Pension ID </label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="pension_id" id="pension_id" <?php if(isset($client['pension_id']) && ($client['pension_id']==1) ){?> checked="checked"<?php } ?>>
</div>
</div>  
    
        <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Declaration of Compliance Due</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="declaration_of_compliance_due_date" id="declaration_of_compliance_due_date" <?php if(isset($client['declaration_of_compliance_due_date']) && ($client['declaration_of_compliance_due_date']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

              <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Declaration of Compliance Submission</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="declaration_of_compliance_submission" id="declaration_of_compliance_submission" <?php if(isset($client['declaration_of_compliance_submission']) && ($client['declaration_of_compliance_submission']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

              <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Pension Deadline</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="pension_deadline" id="pension_deadline" <?php if(isset($client['pension_deadline']) && ($client['pension_deadline']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

            <div class="form-group row">
<label class="col-sm-4 col-form-label">Notes</label>
<div class="col-sm-8">
    <input type="checkbox" class="js-small f-right" name="note" id="note" <?php if(isset($client['note']) && ($client['note']==1) ){?> checked="checked"<?php } ?>>
</div>
</div>

        <!--</form>-->
      </div>
  </div> 
<!-- 10tab close -->


    <!-- 11tab start -->
    <div id="registration" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
        <!--<form>-->

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Registration Fee Paid</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="registration_fee_paid" id="registration_fee_paid" <?php if(isset($client['registration_fee_paid']) && ($client['registration_fee_paid']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">64-8 Registration</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="64_8_registration" id="64_8_registration" <?php if(isset($client['64_8_registration']) && ($client['64_8_registration']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>

        <!--</form>-->
      </div>
  </div>
<!-- 11tab close -->

<!-- 12tab start -->
    <div id="company" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!--<form>-->

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Company Number</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="company_number" id="company_number" <?php if(isset($client['company_number']) && ($client['company_number']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div>  

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Company URL</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="company_url" id="company_url" <?php if(isset($client['company_url']) && ($client['company_url']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div> 

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Officers URL</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="officers_url" id="officers_url" <?php if(isset($client['officers_url']) && ($client['officers_url']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div> 

            <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Company Name</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="company_name1" id="company_name1" <?php if(isset($client['company_name1']) && ($client['company_name1']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div>   

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Registered Address</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="register_address" id="register_address" <?php if(isset($client['register_address']) && ($client['register_address']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div>
            
             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Company Status</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="company_status" id="company_status" <?php if(isset($client['company_status']) && ($client['company_status']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div>

             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Business Type</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="company_type" id="company_type" <?php if(isset($client['company_type']) && ($client['company_type']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div>

             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Company SIC</label>
            <div class="col-sm-8">
                 <input type="checkbox" class="js-small f-right" name="company_sic" id="company_sic" <?php if(isset($client['company_sic']) && ($client['company_sic']==1) ){?> checked="checked"<?php } ?>>

            <input type="hidden" name="sic_codes" id="sic_codes" value="">
             </div>
             </div>
        
             
              <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label"> Authentication Code</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="companies_house_authorisation_code" id="companies_house_authorisation_code" <?php if(isset($client['companies_house_authorisation_code']) && ($client['companies_house_authorisation_code']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div>
              <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Company UTR</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="company_utr" id="company_utr" <?php if(isset($client['company_utr']) && ($client['company_utr']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div> 
             

             <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Incorporation Date</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" name="date_of_creation" id="date_of_creation" <?php if(isset($client['incorporation_date']) && ($client['incorporation_date']==1) ){?> checked="checked"<?php } ?>>
             </div>
             </div>   


            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Managed By</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right" name="managed" id="managed" <?php if(isset($client['managed']) && ($client['managed']==1) ){?> checked="checked"<?php } ?>>
            </div>
            </div> 
 
       
      </div>
  </div>
<!-- 12tab close -->

    </div>  <!-- tab-content -->
    
    
        <input type="hidden" name="user_id" id="user_idss" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>">
         <input type="hidden" name="client_id" id="client_id" value="<?php if(isset($user[0]['client_id']) && ($user[0]['client_id']!='') ){ echo $user[0]['client_id'];}?>">
         <input type="hidden" id="company_house" name="company_house" value="">
     
  
</div>

</div> <!-- managementclose -->

<!-- admin close-->
</div>
 </div>
      <div class="modal-footer">
        <div class="floation_set text-right accordion_ups">
        <input type="submit" class="add_acc_client" value="Update Column Setting" id="submit"/>
    </div> 
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
       </form>  
    <!-- </div>
  </div>
</div> -->
<!-- /.modal -->
<?php $this->load->view('includes/footer');?>

<script type="text/javascript">

$(document).ready(function(){
  $('#column_setting').on('submit', function (e) {

          e.preventDefault();
 $(".LoadingImage").show();
          $.ajax({
            type: 'post',
            url: '<?php echo base_url()."User/column_update";?>',
            data: $('form').serialize(),
            success: function (data) {
                if(data == '1'){
                // $('#new_user')[0].reset();
                $('.alert-success').show();
                $('.alert-danger').hide();
                location.reload();
               // $('.all-usera1').load('<?php echo base_url()?>user .all-usera1');
                
              
                }
                else if(data == '0'){
                // alert('failed');
                $('.alert-danger').show();
                $('.alert-success').hide();
                }
                $(".LoadingImage").hide();
            }
          });

        });




});

</script>