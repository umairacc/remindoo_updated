<?php

function prepare_options($arr, $key){
    $html = '';
    if(isset($arr[$key])){
        foreach($arr[$key] as $k=>$v){
            $html .= '<option value="'.$v.'">'.$v.'</option>';
            $html .= prepare_options($arr, $k);
        }
    }    
    return $html;
}

$title_array = array(
    'subject_TH' => array(
        'title'  => 'Subject',
        'column' => 'subject'
    ),
    'timer_TH' => array(
        'title'  => 'Timer',
        'column' => 'timer'
    ),
    'startdate_TH' => array(
        'title'  => 'Start Date',
        'column' => 'start_date'
    ),
    /*'duedate_TH' => array(
        'title'  => 'Due Date',
        'column' => 'end_date'
    ),*/
    'int_duedate_TH' => array(
        'title'  => 'Due Date Internal',
        'column' => 'start_d'
    ),
    'duedate_TH' => array(
        'title'  => 'Due Date External',
        'column' => 'end_date'
    ),
    // 'duedate_TH' => array(
    //     'title'  => 'Due Dates',
    //     'column' => 'end_date'
    // ),
    'status_TH' => array(
        'title' => 'Status'
    ),
    'progress_TH' => array(
        'title' => 'Progress Status'
    ),
    'priority_TH' => array(
        'title' => 'Priority'
    ),
    'tag_TH' => array(
        'title'  => 'Tag',
        'column' => 'tag'
    ),
    'assignto_TH' => array(
        'title' => 'Assignto'
    ),
    'services_TH' => array(
        'title'  => 'Related Services',
        'column' => 'related_service'
    ),
    'company_TH' => array(
        'title'  => 'Company Name',
        'column' => 'company'
    ),
    'action_TH' => array(
        'title'  => 'action',
        'column' => 'Actions'
    ),
    'billable_TH' => array(
        'title'  => 'Billable',
        'column' => 'billable'
    )
);

$select_array               = array();
$column_setting             = Firm_column_settings('task_list');
$th_order                   = $column_setting['order'];
$th_order                   = explode(",", str_replace('"', '', substr($th_order, 1, -1)));
// echo '<pre>';
// print_r($th_order);
// exit;
// if (in_array('duedate_TH', $th_order)) {
//     $new = array();
//     foreach ($th_order as $k => $value) {
//       $new[] = $value;
//       if ($value == 'duedate_TH') {
//         $new[] = 'intduedate_TH';
//       }
//     }
//       $th_order = $new;
//     }
    
$task_status_array          = '';
$progress_status_array      = '';
$priority_array             = '';
$user_list_array            = '';
$billable_array             = '';
$select_array['status_TH']  = $task_status_array;

ob_start(); ?>

<select class="filter_check" multiple="" searchable="true" style="display: none;"><?php
    foreach ($progress_task_status as $key => $status_val) { ?>
        <option value="<?php
            echo $status_val['status_name']; ?>"><?php
            echo $status_val['status_name']; ?>
        </option><?php
    } ?>      
</select> <?php
$progress_status_array       = ob_get_clean();
$select_array['progress_TH'] = $progress_status_array;

ob_start(); ?>

<select class="filter_check" multiple="" searchable="true" style="display: none;"> 
    <option value="low" >Low</option>
    <option value="medium">Medium</option>
    <option value="high">High</option>
    <option value="super_urgent" >Super Urgent</option>
</select><?php

$priority_array              = ob_get_clean();
$select_array['priority_TH'] = $priority_array;

ob_start(); ?>

<select class="filter_check" multiple="" searchable="true" style="display: none;"><?php
    $html = '';
    foreach ($nested_user_list as $key => $value) { 
        if($key == 0){
            foreach($value as $k1=>$v1){
                $html .= '<option value="'.$v1.'">'.$v1.'</option>';
                $html .= prepare_options($nested_user_list, $k1);
            }
        }         
    }
    echo $html; ?>
</select><?php

$user_list_array             = ob_get_clean();
$select_array['assignto_TH'] = $user_list_array;

ob_start(); ?>

<select class="filter_check" multiple="" style="display: none;" searchable="true"> 
    <option value="Billable">Billable</option>
    <option value="Non Billable" >Non Billable</option>
</select><?php

$billable_array              = ob_get_clean();
$select_array['billable_TH'] = $billable_array; ?>

<tr class="text-uppercase">
    <th <?php if ($_SESSION['permission']['Task']['view'] != '1') { echo "style='display:none'"; } ?> class="subtask_toggle_TH Exc-colvis">
        <div style="opacity:0">#</div>
    </th>
    <th class="select_row_TH Exc-colvis">
        <div class="checkbox-fade fade-in-primary">
            <label class="custom_checkbox1">
                <input type="checkbox" id="select_alltask">
                <i></i>
            </label>
        </div>
    </th><?php
    foreach ($th_order as $key => $column_value){       
        $select_control = "";
        $data_column    = isset($title_array[$column_value]['column']) ? 'data-column="' . $title_array[$column_value]["column"] . '"' : '';
        
        if(isset($select_array[$column_value])){           // echo $column_value;
            $select_control = $select_array[$column_value];           
        }

        if($key > 1){ ?>           
            <th <?php  echo $data_column; ?>  class="<?php echo $column_value; ?> "><?php echo $title_array[$column_value]['title']; ?><img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                <div class="sortMask"></div><?php
                echo $select_control; ?>
            </th><?php
        }
    } ?>
    <!--  <th class="action_TH Exc-colvis">Actions </th> -->
</tr>