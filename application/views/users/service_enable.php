<?php $this->load->view('includes/header');?>
<link href="<?php echo base_url();?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <!--start-->
               <div class="title_page01 floating">
                  <div class="modal-alertsuccess alert alert-success" style="display:none;"
                     >
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                     <div class="pop-realted1">
                        <div class="position-alert1">
                           Your record was successfully updated.
                        </div>
                     </div>
                  </div>
                  <div class="modal-alertsuccess alert alert-danger" style="display:none;">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                     <div class="pop-realted1">
                        <div class="position-alert1">
                           Please fill the required fields.
                        </div>
                     </div>
                  </div>
                  <div class="service_view01 upload-file05 ">


                        <div id="Services">
  <div class="job-card">
   <div class="fr-hgt">
   <!--   <div class="col-md-12 col-xl-4"> -->
     <div class=" user-detail-card">
      <input type="hidden" name="service_user_id" id="service_user_id" value=" <?php echo $getCompanyvalue['user_id']; ?>">
            <?php                  
               $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);                
              
                  
                  (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                  ($jsnCst!='') ? $cst = "checked='checked'   disabled='disabled'" : $cst = "";
                  
                  (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                  ($jsnAcc!='') ? $acc = "checked='checked' disabled='disabled'" : $acc = "";
                  
                  (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                  ($jsntax!='') ? $tax = "checked='checked' disabled='disabled'" : $tax = "";
                  
                  (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                  ($jsnp_tax!='') ? $p_tax = "checked='checked' disabled='disabled'" : $p_tax = "";
                  
                  (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                  ($jsnpay!='') ? $pay = "checked='checked' disabled='disabled'" : $pay = "";
                  
                  (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                  ($jsnworkplace!='') ? $workplace = "checked='checked' disabled='disabled'" : $workplace = "";
                  
                  (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                  ($jsnvat!='') ? $vat = "checked='checked' disabled='disabled'" : $vat = "";
                  
                  (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                  ($jsncis!='') ? $cis = "checked='checked' disabled='disabled'" : $cis = "";
                  
                  (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                  ($jsncis_sub!='') ? $cissub  = "checked='checked' disabled='disabled'" : $cissub     = "";
                  
                  (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                  ($jsnp11d!='') ? $p11d   = "checked='checked' disabled='disabled'" : $p11d   = "";
                  
                  (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                  ($jsnbk!='') ? $bookkeep     = "checked='checked' disabled='disabled'" : $bookkeep   = "";
                  
                  (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                  ($jsnmgnt!='') ? $management     = "checked='checked' disabled='disabled'" : $management     = "";
                  
                  (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                  ($jsninvest!='') ? $investgate   = "checked='checked' disabled='disabled'" : $investgate     = "";
                  
                  (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                  ($jsnreg!='') ? $registered  = "checked='checked' disabled='disabled'" : $registered     = "";
                  
                  (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                  ($jsntaxad!='') ? $taxadvice     = "checked='checked' disabled='disabled'" : $taxadvice  = "";
                  
                  (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                  ($jsntaxinvest!='') ? $taxinvest     = "checked='checked' disabled='disabled'" : $taxinvest  = "";
                   ?>
                    <table border="2">
                     
               <tr class="for_table">
               <td> Vat Return </td>               
                  <td>
                     <label class="custom_checkbox1">
                         <input type="checkbox"  name="vat" id="vat" value="" <?php echo $vat;?> class="update_service" > 
                         <i></i> </label> <!-- <label for="vat"> </label>  -->
                     
                  </td>
                  </tr>
                  <tr>
                  <td> Payroll</td>
                  <td>
                     <label class="custom_checkbox1">
                        <input type="checkbox" name="payroll" id="payroll" value="" <?php echo $pay;?> class="update_service"  >
                        <i></i></label>
                  <!-- <label for="payroll"> 
                  </label>  --> 
                  </td>
                  </tr>
                  <tr>
                  <td>Accounts</td>
                  <td>
                     <label class="custom_checkbox1">
                         <input type="checkbox" value="" name="accounts" id="accounts"  <?php echo $acc;?> class="update_service" >
                         <i></i></label>
                   <!-- <label for="accounts"></label> --> 
                     
                  </td></tr>
                  <tr>
                  <td>Confirmation Statement</td>
                  <td>
                     <label class="custom_checkbox1">
                         <input type="checkbox" name="conf_statement" id="conf_statement" value="" <?php echo $cst;?> class="update_service" > <i></i> </label>
                   <!-- <label for="conf_statement"></label> --> 
                 
                  </td></tr>
                  <tr>
                  <td>Company Tax Return</td>
                  <td>
                    <label class="custom_checkbox1">
                    <input type="checkbox" name="company_tax" id="company_tax_return" value="" <?php echo $tax;?> class="update_service"  >
                    <i></i> </label>
                  <!-- <label for="company_tax_return"></label> 
                     </div> -->
                  </td></tr>
                  <tr>
                  <td>Personal Tax</td>
                  <td>
                     <label class="custom_checkbox1">
                        <input type="checkbox" value="" name="personal_tax" id="personal_tax_return" <?php echo $p_tax;?> class="update_service"  >  <i></i></label>
                  <!-- <label for="personal_tax_return"></label> 
                     </div> -->
                  </td></tr>
                  <tr>
                  <td>Workplace -pension</td>
                  <td>
                    <label class="custom_checkbox1">
                        <input type="checkbox" name="workplace" id="workplace" value="" <?php echo $workplace;?> class="update_service" >
                        <i></i> </label>
                 <!--  <label for="workplace"></label> 
                     </div> -->
                  </td></tr>
                  <tr>
                  <td>Cis </td>
                  <td>
                    <label class="custom_checkbox1">
                        <input type="checkbox" name="cis" id="cis" value="" <?php echo $cis;?> class="update_service"  > 
                        <i></i></label>
                 <!--  <label for="cis"></label> 
                     </div> -->
                  </td></tr>
                  <tr>
                  <td> Cis -Sub Contractor</td>
                  <td>
                     <label class="custom_checkbox1">
                         <input type="checkbox" name="cissub" id="cissub" value="" <?php echo $cissub;?> class="update_service" >
                         <i></i></label>
                 <!--  <label for="cissub"></label> 
                     </div> -->
                  </td></tr>
                  <tr>
                  <td> P11d</td>
                  <td>
                    <label class="custom_checkbox1">
                        <input type="checkbox" namne="p11d" id="p11d" value="" <?php echo $p11d;?> class="update_service" > 
                        <i></i></label>
            <!--       <label for="p11d"></label> 
                     </div> -->
                  </td></tr>
                  <tr>
                  <td> Book keeping</td>
                  <td>
                    <label class="custom_checkbox1">
                         <input type="checkbox" name="bookkeep" id="bookkeep" value="" <?php echo $bookkeep;?> class="update_service"  >
                         <i></i></label>
               <!--    <label for="bookkeep"></label> 
                     </div> -->
                  </td>
                  </tr>
                  <tr>
                  <td> Management</td>
                  <td>
                     <label class="custom_checkbox1">
                         <input type="checkbox" name="management" id="management" value="" <?php echo $management;?> class="update_service"  >   <i></i></label>
                <!--    <label for="management"></label> 
                     </div> -->
                  </td></tr>
                  <tr>
                  <td> Investigation Insurance</td>
                  <td>
                     <label class="custom_checkbox1">
                        <input type="checkbox" name="investgate" id="investgate" value="" <?php echo $investgate;?> class="update_service"  >  <i></i></label>
                        <!-- <label for="investgate">  </label> 
                     </div> -->
                  </td></tr>
                  <tr>
                  <td> Registered</td>
                  <td>
                     <label class="custom_checkbox1">
                        <input type="checkbox" name="registered" id="registered" value="" <?php echo $registered;?> class="update_service" " >   <i></i></label>
                <!--   <label for="registered"> </label> 
                     </div> -->
                  </td></tr>
                  <tr>
                  <td> Tax Advice</td>
                  <td>
                   <label class="custom_checkbox1">
                        <input type="checkbox" name="taxadvice" id="taxadvice" value="" <?php echo $taxadvice;?> class="update_service"  > 
                    <i></i> </label>    <!-- <label for="taxadvice"></label> 
                     </div> -->
                  </td></tr>
                  <tr>
                  <td> Tax Investigate</td>
                  <td>
                   <label class="custom_checkbox1">
                        <input type="checkbox" name="taxinvest" id="taxinvest" value="" <?php echo $taxinvest;?> class="update_service">
                 <i></i></label>
                 <!--  <label for="taxinvest"></label> 
                     </div> -->
                  </td>
               </tr>
             </table>
            </div>
            </div>
            </div>
            </div>


                  </div>
               </div>
            </div>
          </div>
      </div>
 </div>

 <div class="modal fade" id="managernotification" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Send Request To Manager</h4>
        </div>
        <div class="modal-body">

        <input type="hidden" name="users_id" id="users_id" value="">
        <input type="hidden" name="service_name" id="request_service_name" value="">
              <div class="dropdown-sin-3">
              <select style="display:none" name="manager[]" id="manager" multiple placeholder="Select">
              <option value="">None</option>
              <?php foreach($manager as $managers) {
              ?>
              <option value="<?php echo $managers['id']?>"><?php echo $managers['crm_name'];?></option>
              <?php } ?>
              </select>
              </div>

              <div class="col-xs-12">
              <textarea id="editor4"></textarea></div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-primary send_notification" id="send_notify" >Send</button>
          <button type="button" class="btn btn-default notification_close" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>

<?php $this->load->view('includes/footer');?>
<script type="text/javascript">

   CKEDITOR.replace('editor4');
</script>
<script type="text/javascript">
   $('.dropdown-sin-3').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
    });
     $('.dropdown-sin-5').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
    });

    $(document).ready(function(){
    $(".update_service").click(function(){
         if($(this).is(':checked')) { 
          $("#request_service_name").val($(this).attr('id'));
           $("#users_id").val($("#service_user_id").val());
            $("#managernotification").modal('show');
         }  
    });
   });


   $("#send_notify").click(function(){
        var service_name=$("#request_service_name").val();
        var user_id=$("#users_id").val();
        var message = CKEDITOR.instances['editor4'].getData(); 
        var manager_id=$("#manager").val();  
        var data={'service_name':service_name,'user_id':user_id,'message':message,'manager_id':manager_id}
         $.ajax({
            url: '<?php echo base_url();?>Sample/manager_notification/',
            type: "POST",
            data: data,
            success: function(data)  
            {
                if(data=='1'){
                    $(".alert-success").show();
                    setTimeout(function(){ 
                     $(".alert-success").hide();
                    $("#managernotification").modal('hide');
                    }, 2000);
                }
            }

  });
   });

    </script>