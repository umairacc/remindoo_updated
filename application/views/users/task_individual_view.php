<?php

 $this->load->view('includes/header'); ?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css"> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree_select/style.css?ver=2">


<style type="text/css">
  .add_class{
    display: none;
  }
  .dropdown-content {
    display: none;
    position: absolute; 
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}

tfoot {
    display: table-header-group;
}

</style>
<div class="dummy_content" style="display: none;"></div>
<div class="modal fade" id="my_Modal" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input type="hidden" name="archive_task_id" id="archive_task_id" value="">
        <input type="hidden" name="status_value" id="status_value" value="">
          <p>Do You Want Archive this Task?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal" onclick="archive_action()">yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>      
    </div>
  </div>
<div class="modal fade" id="deleteconfirmation" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input  type="hidden" name="delete_task_id" id="delete_task_id" value="">
          <p> Are you sure want to delete ?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="confirm_delete" data-dismiss="modal" onclick="delete_action()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="review_send" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input  type="hidden" name="delete_task_id" id="delete_task_id" value="">
          <p> Are you sure want send review ?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="confirm_delete" data-dismiss="modal" onclick="review_action()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
</div>

<?php
    $service_display='';
   $role = $this->Common_mdl->getRole($_SESSION['id']);
   function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
   }

   function calculate_test($a,$b){
$difference = $b-$a;

$second = 1;
$minute = 60*$second;
$hour   = 60*$minute;

$ans["hour"]   = floor(($difference)/$hour);
$ans["minute"] = floor((($difference)%$hour)/$minute);
$ans["second"] = floor(((($difference)%$hour)%$minute)/$second);
//echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

$test=$ans["hour"].":".$ans["minute"].":".$ans["second"];
return $test;
}
function time_to_sec($time) {
list($h, $m, $s) = explode (":", $time);
$seconds = 0;
$seconds += (intval($h) * 3600);
$seconds += (intval($m) * 60);
$seconds += (intval($s));
return $seconds;
}
function sec_to_time($sec) {
return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}    

?>

<style>
/** 28-06-2018 **/
.time{
      padding: 8px;
    font-weight: bold;
}


   select.close-test-04{
    display: none;
   }
   /** 04-06-2018 **/
   span.demo {
   padding: 0 10px;
   }
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   img.user_imgs {
   width: 38px;
   height: 38px;
   border-radius: 50%;
   display: inline-block;
   }


   #adduser label {
   text-transform: capitalize;
   font-size: 14px;
   }
  span.hours-left {
    background: #f76480;
    color: #fff;
    padding: 3px 10px;
    border-radius: 3px;
    margin-right: 15px;
}
   .dropdown12 button.btn.btn-primary {
   background: #ccc;
   color: #555;
   font-size: 13px;
   padding: 4px 10px 4px;
   margin-top: -2px;
   border-color:transparent;
   }
   select + .dropdown:hover .dropdown-menu
   {
   display: none;
   }
   select + .dropdown12 .dropdown-menu
   {
   padding: 0 !important;
   }
   select + .dropdown12 .dropdown-menu a {
   color: #000 !important;
   padding: 7px 10px !important;
   border: none !important;
   font-size: 14px;
   }
   select + .dropdown12 .dropdown-menu li {
   border: none !important;
   padding: 0px !important;
   }
   select + .dropdown12 .dropdown-menu a:hover {
   background:#4c7ffe ;
   color: #fff !important;
   }
   span.created-date {
   color: gray;
   padding-left: 10px;
   font-size: 13px;
   font-weight: 600;
   }
   span.created-date i.fa.fa-clock-o {
   padding: 0 5px;
   }
   .dropdown12 span.caret {
   right: -2px;
   z-index: 99999;
   border-top: 4px solid #555;
   border-left: 4px solid transparent;
   border-right: 4px solid transparent;
   border-bottom: 4px solid transparent;
   top: 12px;
   position: relative;
   }
   span.timer {
   padding: 0 10px;
   color: red;
   }
   body {
   font-family:"Arial", Helvetica, sans-serif;
   text-align: center;
   }
   #controls{
   font-size: 12px;
   }
   #time {
   font-size: 150%;
   }
  
</style>
<?php 
 
     $for_main_task_timer=array();
     $for_user_edit_per=array();     
     if(count($for_user_edit_permission)>0){
     foreach ($for_user_edit_permission as $edit_per_key => $edit_per_value) {
      array_push($for_user_edit_per, $edit_per_value['id']);
     }
     }
?>
<!-- end of 29-06-2018 -->
<div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
         <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
               Please! Select Record...
         </div></div>
         </div>
</div> 
<div id="action_result" class="modal-alertsuccess alert succ dashboard_success_message" style="display:none;">
       <div class="newupdate_alert">  <a href="#" class="close" id="close_action_result">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              Success !!! Staff Assign have been changed successfully...
         </div></div>
         </div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/progress-circle.css">

<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section col-xs-12 floating_set newtaslcls">
                        <input type="hidden" name="assigneed_value" id="assigneed_value" value="">
                         
                              </div> <!-- all-clients -->
                           <div class="all_user-section floating_set clientredesign">
                             
                              </div>
                              <div class="all_user-section2 floating_set <?php if($_SESSION['permission']['Task']['view']!='1'){ ?> permission_deined<?php } ?>" >
                                 <div class="tab-content">
                                    <div id="alltasks" class="data_active_task tab-pane fade in active">
                                    <div class="all_task_counts">
                                    </div>
                                    <input type="hidden" name="for_status_us" id="for_status_us">                                     
                                       <div class="client_section3  floating_set">
                                          <div id="status_succ"></div>
                                          <div class="all-usera1  user-dashboard-section1 for_task_status button_visibility">
                                            <?php
                                            foreach ($task_list as $tre_key => $tre_value) {
                                            $time=json_decode($tre_value['counttimer']);
                                            $time_start_date =$tre_value['time_start_date'];
                                            $time_end_date=$tre_value['time_end_date'];
                                            $hours=0;
                                            $mins=0;
                                            $sec=0;
                                            $pause='';
                                            
                                            // $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$tre_value['id']." and user_id=".$_SESSION['id']." ")->result_array();
                                              // if($_SESSION['user_type']=='FU')
                                              // {
                                              //     $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$tre_value['id']." and  user_id=".$_SESSION['id']." ")->result_array();
                                              // }
                                              // else
                                              // {
                                                  $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$tre_value['id']."")->result_array();
                                            //  }
                                            $pause_val='on';
                                            $pause='on';
                                            $for_total_time=0;
                                            if(count($individual_timer)>0){
                                            foreach ($individual_timer as $intime_key => $intime_value) {
                                            $its_time=$intime_value['time_start_pause'];
                                            $res=explode(',', $its_time);
                                              $res1=array_chunk($res,2);
                                            $result_value=array();
                                            //  $pause='on';
                                            foreach($res1 as $rre_key => $rre_value)
                                            {
                                               $abc=$rre_value;
                                               if(count($abc)>1){
                                               if($abc[1]!='')
                                               {
                                                  $ret_val=calculate_test($abc[0],$abc[1]);
                                                  array_push($result_value, $ret_val) ;
                                               }
                                               else
                                               {
                                                $pause='';
                                                $pause_val='';
                                                  $ret_val=calculate_test($abc[0],time());
                                                   array_push($result_value, $ret_val) ;
                                               }
                                              }
                                              else
                                              {
                                                $pause='';
                                                $pause_val='';
                                                  $ret_val=calculate_test($abc[0],time());
                                                   array_push($result_value, $ret_val) ;
                                              }
                                            }
                                           
                                             foreach ($result_value as $re_key => $re_value) {
                                               
                                                $for_total_time+=time_to_sec($re_value);
                                             }

                                            }
                                           
                                            $for_main_task_timer[$tre_value['id']]=$for_total_time;

                                            $hr_min_sec=sec_to_time($for_total_time);
                                             $hr_explode=explode(':',$hr_min_sec);
                                              $hours=(int)$hr_explode[0];
                                              $min=(int)$hr_explode[1];
                                             $sec=(int)$hr_explode[2];

                                            }
                                            else
                                            {
                                              $hours=0;
                                              $min=0;
                                              $sec=0;
                                              $pause='on';
                                            }
                                        
                                    ?>
                                  <input type="hidden" name="trhours_<?php echo $tre_value['id'];?>" id="trhours_<?php echo $tre_value['id'];?>" value='<?php echo $hours; ?>' >
                                  <input type="hidden" name="trmin_<?php echo $tre_value['id'];?>" id="trmin_<?php echo $tre_value['id'];?>" value='<?php echo $min;?>' >
                                  <input type="hidden" name="trsec_<?php echo $tre_value['id'];?>" id="trsec_<?php echo $tre_value['id'];?>" value='<?php echo $sec; ?>' >
                                  <input type="hidden" name="trmili_<?php echo $tre_value['id'];?>" id="trmili_<?php echo $tre_value['id'];?>" value='0' >
                                  <input type="hidden" name="trpause_<?php echo $tre_value['id'];?>" id="trpause_<?php echo $tre_value['id'];?>" value="<?php echo $pause; ?>" >
                                  <?php                             }                                  ?>


                                  <table class="table client_table1 all_task_table text-center display nowrap tasklistseccls" id="alltask" cellspacing="0" width="100%" data-status="notstarted">
                                      <thead>
                                      <tr class="text-uppercase">
                        
                             

                                       <th class="assignto_TH hasFilter">Assignees 
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                       <select class="filter_check"  multiple="" style="display: none;"> 
                                       <?php foreach ($user_list as $key => $value) { ?>
                                         <option value="<?php echo $value['crm_name']; ?>" ><?php echo $value['crm_name']; ?></option>
                                        
                                     <?php   } ?>
                                         
                                        </select>
                                      </th>

                                      <th class="subject_TH hasFilter" >Subject
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      
                                      </th>

                                        <th class="due_TH">Time </th>

                            
                                      <th class="company_TH hasFilter">Company Name 
                                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                        <div class="sortMask"></div>                                       
                                      </th> 

                                    
                                      <th class="billable_TH hasFilter">Billable 

                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                      <div class="sortMask"></div> 
                                       <select class="filter_check"  multiple="" style="display: none;"> 
                                          <option value="Billable">Billable</option>
                                          <option value="Non Billable" >Non Billable</option>
                                        </select>
                                      </th>

                                       <th class="timer_TH hasFilter" style="display: none">Timer
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div>                                     
                                      </th>

                                   
                                      </tr>
                                      </thead>                
                                  
                                                                                                                            
                                      <tbody>
                                      <?php 
                                      foreach ($task_list as $key => $value) 
                                      {
                                         foreach ($user_list as $user_key => $user_value) 
                                      {
                                      $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$value['id']." and  user_id=".$user_value['id']." ")->result_array();
                                      //print_r($individual_timer);


                                      if(!empty($individual_timer)){

                                          error_reporting(0);                                   
                                          if($value['start_date']!='' && $value['end_date']!=''){    
                                            $start  = date_create(implode('-', array_reverse(explode('-', $value['start_date']))));
                                            $end  = date_create(implode('-', array_reverse(explode('-', $value['end_date']))));
                                            $diff    = date_diff ( $start, $end );
                                            $y =  $diff->y;
                                            $m =  $diff->m;
                                            $d =  $diff->d;
                                            $h =  $diff->h;
                                            $min =  $diff->i;
                                            $sec =  $diff->s;
                                          }else{
                                            $y =  0;
                                            $m =  0;
                                            $d =  0;
                                            $h =  0;
                                            $min = 0;
                                            $sec =  0;
                                          }

                                
                                      $d_rec = date('Y-m-d',strtotime($value['end_date']));

                                      $Ddays = dateDiffrents( date('Y-m-d') , $d_rec );

                                      if ( $Ddays < 0 )
                                      {                                     
                                        $d_val = abs( $Ddays ). " Days Delay";
                                      }
                                      else if ( $Ddays === 0 )
                                      {
                                        $d_val = "Due Today";                                        
                                      }
                                      else 
                                      {
                                        $d_val = $Ddays." Days";
                                      }  

                                      if($value['worker']=='')
                                      {
                                       $value['worker'] = 0;
                                      }

                                      $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
                                      if($value['task_status']=='notstarted')
                                      {
                                        $percent = 0;
                                        $stat = 'Not Started';
                                      } if($value['task_status']=='inprogress')
                                      {
                                        $percent = 25;
                                        $stat = 'In Progress';
                                      } if($value['task_status']=='awaiting')
                                      {
                                        $percent = 50;
                                        $stat = 'Awaiting for a feedback';
                                      } if($value['task_status']=='testing')
                                      {
                                        $percent = 75;
                                        $stat = 'Testing';
                                      } if($value['task_status']=='complete')
                                      {
                                        $percent = 100;
                                        $stat = 'Complete';
                                      }
                                      $exp_tag = explode(',', $value['tag']);
                                      $explode_worker=explode(',',$value['worker']);
                                      /** new 12-06-2018 **/
                                      $explode_team=explode(',',$value['team']);
                                      $explode_department=explode(',',$value['department']);
                                      /** end of 12-06-2018 **/

                                      ?>
                                 
              <tr  id="<?php echo $value['id']; ?>" class="count_section" data-id="<?php echo $value['id']; ?>">
         


          <!--   <td  class="subtask_toggle_TD <?php echo (!empty($value['sub_task_id'])?'details-control':' ');?>" data-id="<?php echo $value['id']; ?>" <?php if($_SESSION['permission']['Task']['view']!='1'){ echo "style='display:none'";} ?>></td>                          
 -->

         

    <!--         <td class="select_row_TD"> <div style="" class="checkbox-fade fade-in-primary">
            <label class="custom_checkbox1">
            <input type="checkbox" class="alltask_checkbox" data-alltask-id="<?php echo $value['id'];?>">
           <i></i>
            </label>
            </div> 
            </td> -->


               <td class="assignto_TD user_imgs" id="task_<?php echo $value['id'];?>"  class="assignto_class" data-search="<?php echo $Assignee;?>">

            <span style="" class="task_<?php echo $value['id'];?>">
  
            <strong><?php echo $user_value['crm_name']; ?></strong>

            </span>
            </td>

                

              <td class="subject_TD" data-search="<?php echo ucfirst($value['subject']);?>">

              <?php if($_SESSION['permission']['Task']['edit']=='1' ){ ?>

              <a href="<?php echo base_url().'user/task_details/'.$value['id'];?>" target="_blank"><?php }else{ ?><a href="javascript:;"><?php } ?>
              <?php echo ucfirst($value['subject']);?>
                
              </a>
              </td>

           <td  class="due_TD due_class">
             <div style="">
            <span class="hours-left">
          

           <?php
          if($value['timer_status']!=''){ echo convertToHoursMins($value['timer_status']); }else{ echo '0'; }?> hours<?php //echo $value['task_status'];?></span>
       
           </div>
           </td>


            
        
           

           <?php  $companyName = $this->Common_mdl->get_price('client','id',$value['company_name'],'crm_company_name'); ?>
           <td class="s company_class" data-search="<?php echo $companyName; ?>">
           <div style="" ?>
         	  <?php echo $companyName; ?>
            </div>
           </td> 
           <?php
           $isBillable = ($value['billable']=='Billable'?'Billable':'Non Billable'); 

           ?>
            <td class="billable_TD" data-search="<?php echo $isBillable; ?>">
            <div style="" ?>
              <?php echo $isBillable; ?>
              </div>
            </td>
         


              <?php
                  $time=json_decode($value['counttimer']);
                  $time_start_date =$value['time_start_date'];
                  $time_end_date=$value['time_end_date'];
                  $hours=0;
                  $mins=0;
                  $sec=0;
                  $pause='';

                 
                    $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$value['id']." and  user_id=".$user_value['id']." ")->result_array();
                
                  $pause_val='on';
                  $pause='on';
                  $for_total_time=0;
                  if(count($individual_timer)>0)
                  {

                  foreach ($individual_timer as $intime_key => $intime_value) 
                  {
                  $its_time=$intime_value['time_start_pause'];
                  $res=explode(',', $its_time);
                  $res1=array_chunk($res,2);
                  $result_value=array();
                  //  $pause='on';
                  foreach($res1 as $rre_key => $rre_value)
                  {
                     $abc=$rre_value;
                     if(count($abc)>1){
                     if($abc[1]!='')
                     {
                        $ret_val=calculate_test($abc[0],$abc[1]);
                        array_push($result_value, $ret_val) ;
                     }
                     else
                     {
                      $pause='';
                      $pause_val='';
                        $ret_val=calculate_test($abc[0],time());
                         array_push($result_value, $ret_val) ;
                     }
                    }
                    else
                    {
                      $pause='';
                      $pause_val='';
                        $ret_val=calculate_test($abc[0],time());
                         array_push($result_value, $ret_val) ;
                    }
                  }
                
                   foreach ($result_value as $re_key => $re_value) {
                      
                      $for_total_time+=time_to_sec($re_value) ;
                   }
                  }
             
                    $hr_min_sec=sec_to_time($for_total_time);
                    $hr_explode=explode(':',$hr_min_sec);
                    $hours=(int)$hr_explode[0];
                    $min=(int)$hr_explode[1];
                    $sec=(int)$hr_explode[2];

                  }
                  else
                  {
                    $hours=0;
                    $min=0;
                    $sec=0;
                    $pause='on';
                  }


                  
                  $sub_task_id=array_filter(explode(",",$value['sub_task_id']));
                  if(count($sub_task_id)>0)
                  {
                    $main_task_timer=0;
                    foreach ($sub_task_id as $sk => $sv)
                    {
                      if(array_key_exists($sv,$for_main_task_timer))
                      {
                        $main_task_timer+=$for_main_task_timer[$sv];
                      }
                    }

                    $hr_min_sec=sec_to_time($main_task_timer);
                    $hr_explode=explode(':',$hr_min_sec);
                    $hours=(int)$hr_explode[0];
                    $min=(int)$hr_explode[1];
                    $sec=(int)$hr_explode[2];

                  }
                  $isPasuse = ($pause==''?'time_pause1':'');
                  ?>
                  <td  class="timer_TD timer_class" data-search="<?php echo $hours.':'.$min.':'.$sec;?>" style="display: none"> 
                <div style="" class="stopwatch" data-autostart="false" data-date="<?php echo '2018/06/23 15:37:25'; ?>" data-id="<?php echo $value['id']; ?>"  data-subid="<?php echo ($value['sub_task_id'])?$value['sub_task_id']:"0"; ?>" data-hour="<?php echo $hours;?>" data-min="<?php echo $min; ?>" data-sec="<?php echo $sec;?>" data-mili="0" data-start="<?php if($time_start_date!=''){ echo date('Y/m/d H:i:s',strtotime($time_start_date)); } else { echo ""; } ?>" data-end="<?php if($time_end_date!=''){ echo date('Y/m/d H:i:s',strtotime($time_end_date)); } else { echo ""; } ?>" data-current="Start" data-pauseon="<?php echo $pause;?>" >
                <div class="time timer_<?php echo $value['id'];?>">
                <span class="hours">00</span>: 
                <span class="minutes">00</span>: 
                <span class="seconds">00</span><!-- :: --> 
              <!--   <span class="milliseconds">000</span> -->
                </div>
               
                <div class="controls per_timerplay_<?php echo $value['id']; ?>" id="<?php echo $value['id'];?>">
                <!-- Some configurability -->

                <?php //09-08-2018 
                if($_SESSION['permission']['Task']['edit']==1 && $value['task_status']!="complete"&& empty($value['sub_task_id']) ){
                ?>
                <button class="toggle for_timer_start_pause new_play_stop per_timerplay_<?php echo $value['id']; ?> <?php echo $isPasuse;?>" id="<?php echo $value['id']; ?>" data-pausetext="||" data-resumetext=">"> > </button>
                <?php } ?>
                <!--  <button class="reset">Reset</button> -->
                </div>
                </div>
                </td>

            
             </tr>
                       
                                    
                                      <?php   }
                                    }
                                      }  ?>

                                     
                                                </tbody>
                                             </table>
                         <input type="hidden" class="rows_selected" id="select_alltask_count" >                     
                                          </div>
                                       </div>
                                    </div>                                
                                 <!-- home-->                             
                                 <!-- new_user --> 
                              </div>
                           </div>
                           <!-- admin close -->
                        </div>
                        <!-- Register your self card end -->
                     </div>
                  </div>
               </div>
               <!-- Page body end -->
            </div>
         </div>
         <!-- Main-body end -->
         <div id="styleSelector">
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>



<div class="modal fade" id="import-task" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Import Task</h4>
          </div>
          <div class="modal-body">
                   <!-- content update after page load source from tasks/import viewfile -->
          </div>
      </div>
  </div>
</div>

<!-- Warning Section Ends -->
<?php
   $data = $this->db->query("SELECT * FROM update_client")->result_array();
   foreach($data as $row) { ?>
<!-- Modal -->
<div id="myModal<?php echo $row['user_id'];?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">CRM-Updated fields</h4>
         </div>
         <div class="modal-body">
            <?php if(isset($row['company_name'])&&($row['company_name']!='0')){?>
            <p>Company Name: <span><?php echo $row['company_name'];?></span></p>
            <?php } ?> 
            <?php if(isset($row['company_url'])&&($row['company_url']!='0')){?>
            <p>Company URL: <span><?php echo $row['company_url'];?></span></p>
            <?php } ?>
            <?php if(isset($row['officers_url'])&&($row['officers_url']!='0')){?>
            <p>Officers URL: <span><?php echo $row['officers_url'];?></span></p>
            <?php } ?>
            <?php if(isset($row['incorporation_date'])&&($row['incorporation_date']!='0')){?>
            <p>Incorporation Date: <span><?php echo $row['incorporation_date'];?></span></p>
            <?php } ?>
            <?php if(isset($row['register_address'])&&($row['register_address']!='0')){?>
            <p>Registered Address: <span><?php echo $row['register_address'];?></span></p>
            <?php }  ?>
            <?php if(isset($row['company_status'])&&($row['company_status']!='0')){?>
            <p>Company Status: <span><?php echo $row['company_status'];?></span></p>
            <?php }  ?>
            <?php if(isset($row['company_type'])&&($row['company_type']!='0')){?>
            <p>Company Type: <span><?php echo $row['company_type'];?></span></p>
            <?php } ?>
            <?php if(isset($row['accounts_periodend'])&&($row['accounts_periodend']!='0')){?>
            <p>Accounts Period End: <span><?php echo $row['accounts_periodend'];?></span></p>
            <?php } ?>
            <?php if(isset($row['hmrc_yearend'])&&($row['hmrc_yearend']!='0')){?>
            <p>HMRC Year End: <span><?php echo $row['hmrc_yearend'];?></span></p>
            <?php } ?>
            <?php if(isset($row['ch_accounts_next_due'])&&($row['ch_accounts_next_due']!='0')){?>
            <p>CH Accounts Next Due: <span><?php echo $row['ch_accounts_next_due'];?></span></p>
            <?php } ?>
            <?php if(isset($row['confirmation_statement_date'])&&($row['confirmation_statement_date']!='0')){?>
            <p>Confirmation Statement Date: <span><?php echo $row['confirmation_statement_date'];?></span></p>
            <?php } ?>
            <?php if(isset($row['confirmation_statement_due_date'])&&($row['confirmation_statement_due_date']!='0')){?>
            <p>Confirmation Statement Due: <span><?php echo $row['confirmation_statement_due_date'];?></span></p>
            <?php } ?>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- /.modal -->



<?php } ?>





  <div id="addSubTask-Popup" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title" style="text-align-last: center">Add Sub Task</h4>
          </div>
          <div class="modal-body">
             <input id="subTaskName" placeholder="Task Name" class="addSubtask">
          </div>
          <div class="modal-footer profileEdit">
             <a href="javascript:;" id="saveSubtaskbutton">save</a>
              <a href="javascript:;" data-dismiss="modal">Close</a>
          </div>
        </div>
    </div>
  </div>

             <div id="assignTask-Popup" class="modal fade" role="dialog">
                <div class="modal-dialog">
                   <!-- Modal content-->
                   <div class="modal-content">
                      <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
                      </div>
                      <div class="modal-body">
                    
                       <input type="text" class="tree_select" name="assignees[]" 
                        placeholder="Select">
               
                      </div>
                      <div class="modal-footer profileEdit">
                         <input type="hidden" name="hidden">
                         <a href="javascript;" id="assign_tsk_btn" data-dismiss="modal"  class="assign_tsk_btn">save</a>
                      </div>
                   </div>
                </div>
             </div>



<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url()?>assets/js/user_page/materialize.js"></script> 

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>

<script src="<?php echo base_url()?>assets/js/custom/buttons.colVis.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>

<script src="<?php echo base_url()?>assets/js/custom/datatable_extension.js"></script>


<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script> 

<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>

<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script type="text/javascript">


$(document).ready(function(){

    $('.tags').tagsinput({
        allowDuplicates: true
      });

   


      $('.dropdown-sin-2').dropdown({
         limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });

      
    $('.dropdown-sin-7').dropdown({
      
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
    
   


 $('.dropdown-sin-25').dropdown({
     
     input: '<input type="text" maxLength="20" placeholder="Search">'
      });
       $('.dropdown-sin-27').dropdown({
     
     input: '<input type="text" maxLength="20" placeholder="Search">'
      });
        $('.dropdown-sin-29').dropdown({
     
     input: '<input type="text" maxLength="20" placeholder="Search">'
      });
        
});









  $(document).ready(function() {

  $("#confirm-submit.confirm12").on("shown.bs.modal", function () {

   // alert('hi'); 

    $('div#pcoded + div + div + .modal-backdrop.fade.show').css('z-index', '9999');
    
});

    $("#confirm-submit.confirm12").on("hidden.bs.modal", function () {

   // alert('hi'); 

    $('div#pcoded + div + div + .modal-backdrop.fade.show').css('z-index', '1040');
    
})
});
</script>

<script  type="text/javascript">
   var TaskTable_Instance;
   var MainStatus_Tab ='all_task';
/*single and multiple row task archive*/
$('.archive_task_button').on('click', function() {
  
  var alltask = getSelectedRow();

  if(alltask.length <=0) {
    $('.popup_info_msg').show();
    $(".popup_info_msg .position-alert1").html("Please Selecte Any Records..");    
  } else {
 // console.log(alltask);
  $("#archive_task_id").val(JSON.stringify(alltask));
  $("#status_value").val($(this).data('status'));
  //unarchive does not had conform box
  if($(this).data('status')=="unarchive")archive_action();
  }
});

function archive_action() 
{ //model yes button click
var id=$("#archive_task_id").val();
var status=$("#status_value").val();
 $.ajax({
                    url: '<?php echo base_url(); ?>user/archive_update',
                    type : 'POST',
                    data : { 'id':id,'status':status},                    
                      beforeSend: function() {
                        $(".LoadingImage").show();
                      },
                      success: function(data) {
                        $(".LoadingImage").hide();
                        $(".popup_info_msg").show();
                        $(".popup_info_msg .position-alert1").html('Success !!! Task Archive successfully...');
                   //    console.log(data);
                       setTimeout(function () {$(".popup_info_msg").hide(); location.reload();}, 1500);                                        
                      }

                    });

}

function archieve_click(val){ //single button click
//  alert('ok');
//  alert($(val).attr('id'));
  $("#archive_task_id").val(JSON.stringify([$(val).attr('id')]));
  $("#status_value").val($(val).data('status'));
  //alert($("#archive_task_id").val());
  //alert($("#my_Modal").attr('class'));
}
/*single and multiple row task archive*/  
   
         function humanise (diff) {
   
   // The string we're working with to create the representation 
   var str = '';
   // Map lengths of `diff` to different time periods 
   var values = [[' year', 365], [' month', 30], [' day', 1]];
   // Iterate over the values... 
   for (var i=0;i<values.length;i++) {
   var amount = Math.floor(diff / values[i][1]);
   // ... and find the largest time value that fits into the diff 
   if (amount >= 1) { 
   // If we match, add to the string ('s' is for pluralization) 
   str += amount + values[i][0] + (amount > 1 ? 's' : '') + ' '; 
   // and subtract from the diff 
   diff -= amount * values[i][1];
   }
   else
   {
   str += amount + values[i][0]+' ';
   }
   
   } return str; } 
     
</script>
<?php

   ?><script></script>
<script>
   
$(document).on('click','.sendtoreview',function(){
  var id=$(this).data("id");
  
  var timer=0;
  timer+=parseInt($(".timer_"+id+" .hours").text());
  timer+=parseInt($(".timer_"+id+" .minutes").text());
  timer+=parseInt($(".timer_"+id+" .seconds").text());
//alert(timer);
if(timer<=0){$(".popup_info_msg").show();$(".popup_info_msg .position-alert1").html("You Don't Have Worked Time..");return;}
  $.ajax({
            url: '<?php echo base_url();?>user/task_statusChange/',
            type: 'post',
            data: { 'rec_id':id,'status':'complete'},
            timeout: 3000,
            success: function( data ){
               $(".popup_info_msg").show();
               $('.popup_info_msg .position-alert1').html('Success !!! Request Send successfully...');
               setTimeout(function () {
                $(".popup_info_msg").hide();
                }, 1500);
             
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
});
function AddClass_SettingPopup()
      {
        
      $('button.Button_List').closest('.dt-button-collection').addClass('Settings_List');
      $('button.buttons-columnVisibility').closest('.dt-button-collection').addClass('columnVisibility_List');
      }
   
      $(document).ready(function() {






/*for COLUMN oredering*/
        <?php 
          $column_setting = Firm_column_settings('task_list');
        ?>
        var column_order = <?php echo $column_setting['order'] ?>;
        
        var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;


        var column_ordering = [];

        $.each(column_order,function(i,v){

          var index = $('#alltask thead th').index($('.'+v));

          if(index!==-1)
          {
             // alert(index);
            column_ordering.push(index);
          }
        });
/*for COLUMN oredering*/
     // table_sorting_trigger();
   //          "dom": '<"toolbar-table" <"#table-buttons.dropdown.table-buttons"<"#table-buttonsList.dropdown-menu.table-buttonsList"B>> >lfrtip',

       humanise();  
   

         TaskTable_Instance = $('#alltask').DataTable({

          "dom": '<"toolbar-table" B>lfrtip',
           buttons: [
            {
                extend: 'collection',
                background:false,
                text: '<i class="fa fa-cog" aria-hidden="true"></i>',
                className:'Settings_Button',
                buttons:[
                        { 
                          text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
                          className:'Button_List',
                          action: function ( e, dt, node, config )
                          {
                              Trigger_To_Reset_Filter();
                          }
                        },
                      
                ]
            }
          ],
          order: [],//ITS FOR DISABLE SORTING
          columnDefs: 
          [
            {"orderable": false,"targets": ['subtask_toggle_TH','select_row_TH','timer_TH','due_TH','action_TH']}
          ],
        
        //
    //"search": {"regex": true,"smart": false},

       // responsive: true,
          "iDisplayLength": 10,
            /*  scrollX:        true,*/
       //  scrollCollapse: true,
       // // paging:         false,
       //  fixedColumns:   {
       //      leftColumns: 1,
       //      rightColumns: 1
       //  },
       initComplete: function () {
        var api = this.api();

              api.columns('.hasFilter').every( function () {

                        var column = this;
                        var TH = $(column.header());
                        var Filter_Select = TH.find('.filter_check');

                    if( !Filter_Select.length )
                    {

				              Filter_Select = $('<select multiple="true" class="filter_check" style="display:none"></select>').appendTo( TH );
                      var unique_data = [];
                        column.nodes().each( function ( d, j ) { 
                          var dataSearch = $(d).attr('data-search');
                          
                            if( jQuery.inArray(dataSearch, unique_data) === -1 )
                            {
                              //console.log(d);
                              Filter_Select.append( '<option  value="'+dataSearch+'">'+dataSearch+'</option>' );
                              unique_data.push( dataSearch );
                            }

                        });
                    }
                    

                    Filter_Select.on( 'change', function () {

                        var search =  $(this).val(); 
                        console.log( search.length );
                       
                            var  class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/); 

                             if(search.length) search= '^('+search.join('|') +')$'; 
                           

                    

                       var cur_column = api.column( '.'+class_name+'_TH' );
                       
                        cur_column.search( search, true, false ).draw();
                    } );
                    	
                        //console.log(select.attr('style'));
                        Filter_Select.formSelect(); 

                      }); 
              
         
        }
    });
    
      ColVis_Hide( TaskTable_Instance , hidden_coulmns );

      Filter_IconWrap();

      Change_Sorting_Event( TaskTable_Instance ); 

      

      ColVis_Backend_Update ( TaskTable_Instance , 'task_list' );

      $(document).on('click', '.Settings_Button', AddClass_SettingPopup);
      $(document).on('click', '.Button_List', AddClass_SettingPopup );

      $(document).on('click','.dt-button.close',function()
      {
        $('.dt-buttons').find('.dt-button-collection').detach();
        $('.dt-buttons').find('.dt-button-background').detach();
      });

          $(document).on('click', '.details-control', function () {
      
          var tr = $(this).closest('tr');
          var row = TaskTable_Instance.row(tr);

          if ( row.child.isShown() )
          {
              // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
           }
           else
           {
            var id=$(this).data('id');     

           // alert(id);
              $.ajax({
                    url: '<?php echo base_url(); ?>Sub_Task/sub_task_get',
                    type : 'POST',
                    data : { 'id':id },                    
                      beforeSend: function() {
                        $(".LoadingImage").show();
                      },
                      success: function(data) {

                                  var parentTableThW = [];
                                    $('#alltask thead th').each(function(){
                                        parentTableThW.push($(this).outerWidth()); 
                                    }); 
                                  $(".dummy_content").html(data);
                                  $('.dummy_content').find('tr').each(function(){
                                  var i=0;

                                   var sub_val=$(this).closest('.test_check').find('.subTaskcheckbox').data('alltask-id');

                                    vars['hours_'+sub_val ]=$('#trhours_'+sub_val).val();
                                    vars['minutes_'+sub_val]=$('#trmin_'+sub_val).val();
                                     vars['seconds_'+sub_val]=$('#trsec_'+sub_val).val();
                                      vars['milliseconds_'+sub_val]=0;
                                       vars['data_pause_'+sub_val]=$('#trpause_'+sub_val).val();

                                    $(this).find('td').each(function(){
                                    
                                      console.log(parentTableThW[i] );

                                      $(this).css({ width : parentTableThW[i] });
                                      i++;

                                    });
                                  });

                                   $(".LoadingImage").hide();
                                   row.child(  $(".dummy_content").html()  ).show();
                                   
                                   tr.addClass('shown'); 
                                   working_hours_timer(id); 
                      } 

                    });

            // Open this row
           
         }

      });


  
  /*this button for toggle settings*/
 /* $("div#table-buttons").prepend('<button class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cogs" aria-hidden="true"></i></button>');*/
  /*this button for toggle settings*/

    $('.toolbar-search').dropdown({      
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
      
      $(document).on('click', '.task_status_val', function(){
          
          var col = $.trim( $(this).attr('data-searchCol') );
          var val = $.trim( $(this).attr('data-id') );
          
          Trigger_To_Reset_Filter();
         

          MainStatus_Tab = val;

          TaskTable_Instance.column('.'+col).search(val,true,false).draw();
        });
      
      $('.toolbar-table select').on('change',function(){
        if( $(this).attr('id') != 'export_report' )
        {
          var search =  $(this).val(); 

          if(search.length) search= '^('+search.join('|') +')$'; 

          var col = $.trim( $(this).attr('data-searchCol') );

          //console.log(col+"value"+val);

          TaskTable_Instance.column('.'+col).search(search,true,false).draw();

        }
      });

        function  Trigger_To_Reset_Filter()
              {

                  $('#select_alltask').prop('checked',false);
                  $('#select_alltask').trigger('change');
                  

                  $('div.toolbar-table .toolbar-search li.dropdown-chose').each(function(){
                    $(this).trigger('click');
                  });


                  MainStatus_Tab ="all_task";

                  Redraw_Table(TaskTable_Instance);

                 // Redraw_Table(TaskTable_Instance);
                   
              }



      
      });
      
</script>
<script>
   $(document).ready(function() {
    //  var check=0;
    //   var check1=0;
    //   var numCols = $('#alltask thead th').length;  

    //     var table = $('#alltask').DataTable({
    //      "dom": '<"toolbar-table">lfrtip',
    //     responsive: true,
    //       "iDisplayLength": 10,
    //    initComplete: function () { 
    //                  var q=2;
    //                  $('#alltask tfoot th').find('.filter_check').each( function(){
    //                    $(this).attr('id',q);
    //                    q++;
    //                  });
    //               $('#alltask tfoot th').find(".multiple-select-dropdown").css('display','none');
    //               for(i=2;i<numCols;i++){ 
    //               //console.log('*****************'+i+'***************');
    //                  if(i==3){
    //                       check=1;          
    //                       i=Number(i) + 1;
    //                       var select = $("#3"); 
    //                   }else{
    //                       var select = $("#"+i); 
    //                   } 
    //                   console.log(select);
    //                   console.log('*****************'+i+'***************');
    //                   // var select = $("#frozen_"+i); 
                              
    //                   this.api().columns([i]).every( function () {
    //                     var column = this;
    //                     column.data().unique().sort().each( function ( d, j ) {    
    //                     //  console.log(d);         
    //                       select.append( '<option value="'+d+'">'+d+'</option>' )
    //                     });
    //                   });     
    //                     if(check=='1'){                 
    //                      i=Number(i) - 1;
    //                      check=0;
    //                   }              
    //                 // console.log('*****************'+i+'***************');
    //                  $("#"+i).formSelect();  
    //               }
    //     }
    // });
    //   for(j=2;j<numCols;j++){  
    //       $('#'+j).on('change', function(){  
    //         var c=$(this).attr('id');  
    //       //  alert(c);
    //           var search = [];              
    //           $.each($('#'+c+ ' option:selected'), function(){                
    //               search.push($(this).val());
    //           });      
    //           search = search.join('|');             
    //           // if(c==8){               
    //           //   c=Number(c) + 1;
    //           // }             
    //           table.column(c).search(search, true, false).draw();  
    //       });
    //    }
      
      
       
      $('#dropdown2').on('change', function () {
                    // table.columns(5).search( this.value ).draw();
                    var filterstatus = $(this).val();
                    $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>tasksummary/taskFilter",
                  data: {filterstatus:filterstatus},
                  success: function(response) {
   
                     $(".all-usera1").html(response);
    
                  },
                  //async:false,
               });
                 });
   
      /*$('#dropdown1').on('change', function () {
                    // table.columns(2).search( this.value ).draw();
                    
                 } );*/
   
                
   });
   
</script>
<script>
/*may be old required ment code*/

  $(document).on('change','.test_status',function () {
            //$(".task_status").change(function(){
       var rec_id = $(this).data('id');
       var stat = $(this).val();
          $.ajax({
            url: '<?php echo base_url();?>user/test_statuschange/',
            type: 'post',
            data: { 'rec_id':rec_id,'status':stat },
            timeout: 3000,

            success: function( data ){
               //alert('ggg');
                $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });

             //   setTimeout(resetAll,3000);
               /* if(stat=='3'){
                  
                   //$this.closest('td').next('td').html('Active');
                   $('#frozen'+rec_id).html('Frozen');
      
                } else {
                   $this.closest('td').next('td').html('Inactive');
                }*/
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
   });



   $(document).ready(function() {




   
   $(  document ).on('change', ".export_report",function(){
   //$("#prioritywise_filter").change(function(){
      var data = {};
      data['priority'] = $('.prioritywise_filter').val();
      data['status'] = $('.statuswise_filter').val();
      //data['status'] ='';
     // alert( data['status'] );
      data['d_type'] = $(this).val();
      file_download(data);
      return false;
    });



   
   function file_download(data)
   {
   var type = data.d_type;
   var status = data.status;
   var priority = data.priority;
   if(type=='excel')
   {
   window.location.href="<?php echo base_url().'user/task_excel?status="+status+"&priority="+priority+"'?>";
   }else if(type=='pdf')
   {
   window.location.href="<?php echo base_url().'user/task_pdf?status="+status+"&priority="+priority+"'?>";
   }else if(type=='html')
   {
   window.open(
   "<?php echo base_url().'user/task_html?status="+status+"&priority="+priority+"'?>",
   '_blank' // <- This is what makes it open in a new window.
   );
   // window.location.href="<?php echo base_url().'user/task_html?status="+status+"&priority="+priority+"'?>";
   }
   }
   
   
   
   });
   

    $(document).on("click",".assignuser_staff",function(){

      var id = $(this).attr("data-id");
      var data = {};
     $('.assign_staff_div').html('');
     
      data['task_id'] = id;


      data['assignuser_staff'] = $('#assignuser_staff_'+id).val();
       data['assignuser_staffid'] = $('#assignuser_staffid_'+id).val();
       data['type']=  $('#update_staffid_'+id).val();
   
      if($('#assignuser_staff_'+id).val()!='' &&  data['type'] != '3'){
      
 
    

      $(".LoadingImage").show();
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/assign_status_change/",
               data: data,
               success: function(response) {
                $('#assignuser_'+id+' .close').trigger('click');

                 $('#adduser_'+id+' #assign_task_status_'+id).html('').html(response);
                $(".LoadingImage").hide();
               },
            });
      }
      else if(data['type'] == '3')
      {
        $(".LoadingImage").show();
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/assign_status_change/",
               data: data,
               success: function(response) {
                $('#assignuser_'+id+' .close').trigger('click');

                 $('#adduser_'+id+' #assign_task_status_'+id).html('').html(response);
                $(".LoadingImage").hide();
               },
            });
      }
      else
      {
        $('.assign_staff_div').html('Enter Status');
      }
   });

   $(".assignuser_change").click(function(){
     $('.assign_staff_div').html('');

        var  id = $(this).attr("data-id");

        $('#assignuser_staff_'+id).val('');
        $('#assignuser_staffid_'+id).val('');
        $('#update_staffid_'+id).val('');

      $('#assignuser_'+id+' h4').text('Reason Status');
      $('.delete_st_reason').css('display','block');
       $('.delete_div_reason').css('display','none');
        $('#update_staffid_'+id).val('1');

   });

    $(document).on("click",".edit_user_change",function(){
       $('.assign_staff_div').html('');

      var id = $(this).attr("data-id");
      var data = {};

       $('#assignuser_staff_'+id).val('');
        $('#assignuser_staffid_'+id).val('');
        $('#update_staffid_'+id).val('');

      var data_status = $('#assign_task_status_'+id).val();
      $('#assignuser_'+id+' h4').text('Reason Status');
      $('.delete_st_reason').css('display','block');
       $('.delete_div_reason').css('display','none');
      var data_text= $('#assign_task_status_'+id+' option:selected').text();

     

       if(data_status!='')
       {
        $('#assignuser_staff_'+id).val(data_text);
        $('#assignuser_staffid_'+id).val(data_status);
         $('#update_staffid_'+id).val('2');
       }
      
       

    });
    $(document).on("change",".assign_task_status",function(){
      var id=$(this).attr('id');
      id=id.split('_');
      var val=$(this).val();
      if(val!="")
      {
        $('#edit_user_change_'+id[3]).css('display','block');
        $('#delete_user_change_'+id[3]).css('display','block');
      }
      else
      {
         $('#edit_user_change_'+id[3]).css('display','none');
        $('#delete_user_change_'+id[3]).css('display','none');

      }


    });


     $(document).on("click",".delete_user_change",function(){
       $('.assign_staff_div').html('');

      var id = $(this).attr("data-id");

      
        $('#assignuser_staffid_'+id).val('');
        $('#update_staffid_'+id).val('');

      var data = {};
      var data_status = $('#assign_task_status_'+id).val();
     // alert(data_status);
     $('.delete_st_reason').css('display','none');
       $('.delete_div_reason').css('display','block');
       $('#assignuser_'+id+' h4').text('Do you Want Delete the status');
      var data_text= $('#assign_task_status_'+id+' option:selected').text();
     

       if(data_status!='')
       {
        $('#assignuser_staffid_'+id).val(data_status);
        $('#update_staffid_'+id).val('3');
       }
    


    });

      $(document).on("click",".save_assign_staff",function(){
      var id = $(this).attr("data-id");
      var data = {};
      var Assignees=[];
      // var countries =$("#workers"+id ).val();

    $('#adduser_'+id+' .comboTreeItemTitle input[type="checkbox"]:checked').each(function(){
    var id1 = $(this).closest('.comboTreeItemTitle').attr('data-id');
      var id2 = id1.split('_');
   Assignees.push( id2[0] );
     });
                         //  data['assignee'] = Assignees

    var assign_role = Assignees.join(',');

   // alert(assign_role);
      data['task_id'] = id;
      data['assign_role'] = assign_role;
      data['assign_task_status'] = $('#assign_task_status_'+id).val();

      data['description'] = $('#description_'+id).val();

      $(".LoadingImage").show();
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/update_assignees/",
               data: data,
               success: function(response) {
                  // alert(response); die();
                  $(".LoadingImage").hide();
               //$('#task_'+id).html(response);
               $('.task_'+id).html(response);
                  $('.popup_info_msg').show();
                  $(".popup_info_msg .position-alert1").html('Success !!! Staff Assign have been changed successfully...');
                 setTimeout(function(){ $('.popup_info_msg').hide(); }, 1500);
               },
            });
   });

  
   
   
   $(function() {
    
    var hours = minutes = seconds = milliseconds = 0;
    var prev_hours = prev_minutes = prev_seconds = prev_milliseconds = undefined;
    var timeUpdate;
   
    // Start/Pause/Resume button onClick
    $("#start_pause_resume").button().click(function(){ 
        // Start button
        if($(this).text() == "Start"){  // check button label
            $(this).html("<span class='ui-button-text'>Pause</span>");
            updateTime(0,0,0,0);
        }
    // Pause button
        else if($(this).text() == "Pause"){
            clearInterval(timeUpdate);
            $(this).html("<span class='ui-button-text'>Resume</span>");
        }
    // Resume button    
        else if($(this).text() == "Resume"){
            prev_hours = parseInt($("#hours").html());
            prev_minutes = parseInt($("#minutes").html());
            prev_seconds = parseInt($("#seconds").html());
            prev_milliseconds = parseInt($("#milliseconds").html());
            
            updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds);
            
            $(this).html("<span class='ui-button-text'>Pause</span>");
        }
    });
    
    // Reset button onClick subject
    $("#reset").button().click(function(){
        if(timeUpdate) clearInterval(timeUpdate);
        setStopwatch(0,0,0,0);
        $("#start_pause_resume").html("<span class='ui-button-text'>Start</span>");      
    });
    
    // Update time in stopwatch periodically - every 25ms
    function updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds){
        var startTime = new Date();    // fetch current time
        
        timeUpdate = setInterval(function () {
            var timeElapsed = new Date().getTime() - startTime.getTime();    // calculate the time elapsed in milliseconds
            
            // calculate hours                
            hours = parseInt(timeElapsed / 1000 / 60 / 60) + prev_hours;
            
            // calculate minutes
            minutes = parseInt(timeElapsed / 1000 / 60) + prev_minutes;
            if (minutes > 60) minutes %= 60;
            
            // calculate seconds
            seconds = parseInt(timeElapsed / 1000) + prev_seconds;
            if (seconds > 60) seconds %= 60;
            
            // calculate milliseconds 
            milliseconds = timeElapsed + prev_milliseconds;
            if (milliseconds > 1000) milliseconds %= 1000;
            
            // set the stopwatch
            setStopwatch(hours, minutes, seconds, milliseconds);
            
        }, 25); // update time in stopwatch after every 25ms
        
    }
    
    // Set the time in stopwatch
    function setStopwatch(hours, minutes, seconds, milliseconds){
        $("#hours").html(prependZero(hours, 2));
        $("#minutes").html(prependZero(minutes, 2));
        $("#seconds").html(prependZero(seconds, 2));
        $("#milliseconds").html(prependZero(milliseconds, 3));
    }
    
    // Prepend zeros to the digits in stopwatch
    function prependZero(time, length) {
        time = new String(time);    // stringify time
        return new Array(Math.max(length - time.length + 1, 0)).join("0") + time;
    }
   });
   
   
 /*  function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
   }
   */
   

 

   // Set the date we're counting down to
   var countDownDate = new Date("Sep 4, 2017 15:37:25").getTime();
   
   // Update the count down every 1 second
   var x = setInterval(function() {
   
    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    //alert(distance);
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    //document.getElementById("demo").innerHTML = days + "d " + hours + "h "
   var result = humanise(days) + hours + "h "
    + minutes + "m " + seconds + "s ";
    // console.log(result);
    $(".demos").html(result);
   
   
    
    // If the count down is over, write some text 
    if (distance < 0) {
     // alert('fff');
      //  clearInterval(x);
        $(".demos").html('EXPIRED');
        //document.getElementById("demo").innerHTML = "EXPIRED";
    }
   }, 1000);
</script>
<!--  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/> -->
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.workout-timer.css" />
<!-- Example-page styles -->
<style>
   body {
   margin-bottom: 40px;
   background-color:#ECF0F1;
   }
   .event-log {
   width: 100%;
   margin: 20px 0;
   }
   .code-example {
   margin: 20px 0;
   }
</style>

<script>
  var vars = {};
  //var time_count=0;
  function working_hours_timer(sub_task=null)
  {

        
    test();
     $('.stopwatch').each(function () {
//        $(document).on('each','.stopwatch',function(){

        // Cache very important elements, especially the ones used always
      //  var element = $(this);

         if(sub_task==null){
        // console.log('s'); 
       var element = $(this);
         } else { 
         // console.log('no'); 
        //  var element = $(this).closest('tr .test_check').find('.stopwatch');
            var subtask_id=$(this).closest('tr .test_check').find('.subTaskcheckbox').data('alltask-id');

            var element = $(this).closest('tr .subtask_check_'+sub_task+'_'+subtask_id).find('.stopwatch');
          }


        var running = element.data('autostart');
        var for_date = element.data('date');
       
        var task_id=element.data('id');

        var data_pause=element.data('pauseon');
          var data_pause=vars['data_pause_'+task_id];
       
        var hours=element.data('hour');
        var minutes=element.data('min');
        var seconds=element.data('sec');
        var milliseconds=element.data('mili');

        if(hours=='0')
        {
          var hours=vars['hours_'+task_id];
        }
        if(minutes=='0')
        {
          var minutes=vars['minutes_'+task_id];
        }
        if(seconds=='0')
        {
          var seconds=vars['seconds_'+task_id];

        }
        if(milliseconds=='0')
        { //alert("assig"+vars['milliseconds_'+task_id]);

          var milliseconds=vars['milliseconds_'+task_id];
        }

         element.closest('tr').find('td .hours-left').html('').html(parseInt(hours)+':'+parseInt(minutes)+' hours');

       // alert(milliseconds);
        var start=element.data('start');
        var end=element.data('end');

        var hoursElement = element.find('.hours');
        var minutesElement = element.find('.minutes');
        var secondsElement = element.find('.seconds');
        var millisecondsElement = element.find('.milliseconds');
        var toggleElement = element.find('.toggle');
        var resetElement = element.find('.reset');
        var pauseText = toggleElement.data('pausetext');
        var resumeText = toggleElement.data('resumetext');
        var startText = toggleElement.text();

        // And it's better to keep the state of time in variables 
        // than parsing them from the html.
        //var hours, minutes, seconds, milliseconds, timer;
   
     


        
    
            
       

         var timer;

        function prependZero(time, length) {

          //  alert('zzz');
            // Quick way to turn number to string is to prepend it with a string
            // Also, a quick way to turn floats to integers is to complement with 0
            time = '' + (time | 0);     

            // And strings have length too. Prepend 0 until right.
            while (time.length < length) time = '0' + time;
            return time;
            
        }

        function setStopwatch(hours, minutes, seconds, milliseconds) {
            // Using text(). html() will construct HTML when it finds one, overhead.
            hoursElement.text(prependZero(hours, 2));
            minutesElement.text(prependZero(minutes, 2));
            secondsElement.text(prependZero(seconds, 2));            
            millisecondsElement.text(prependZero(milliseconds, 3));
        }

        // Update time in stopwatch periodically - every 25ms
        function runTimer() {
            // Using ES5 Date.now() to get current timestamp            
            var startTime = Date.now();
         
        //  var startTime=1530000373832;

        //  //var startTime=1529926802;
        //  var startTime=new Date("2016/06/25 00:00:00");  
        // var startTime = new Date("June 23, 2018 15:37:25").getTime();
        if(hours!=0 && minutes!=0 && seconds!=0 && milliseconds!=0)
        {
             var startTime = Date.now();
        }
        else
        {
           // hours=$('#trhours_'+task_id).val();
           //  $minutes=$('#trmin_'+task_id).val();
           //  seconds=$('#trsec_'+task_id).val();
           //  milliseconds=$('#trmili_'+task_id).val();
            //var startTime = new Date("June 23, 2018 15:37:25").getTime();
//            var startTime = new Date("2018/06/23 15:37:25").getTime();
           //var startTime = new Date(for_date).getTime();
           var startTime = Date.now();
        }
        




         // alert("before float"+milliseconds);

            var prevHours = parseFloat(hours);
            var prevMinutes = parseFloat(minutes);
            var prevSeconds = parseFloat(seconds);
            var prevMilliseconds = parseFloat(milliseconds);

            //alert("insidetimer"+prevMilliseconds);
           
            timer = setInterval(function () {
            
                var timeElapsed = Date.now() - startTime;
          
                hours = (timeElapsed / 3600000) + prevHours;
                minutes = ((timeElapsed / 60000) + prevMinutes) % 60;
                seconds = ((timeElapsed / 1000) + prevSeconds) % 60;
                milliseconds = (timeElapsed + prevMilliseconds) % 1000;

            element.attr('data-hour',hours);
            element.attr('data-min',minutes);
            element.attr('data-sec',seconds);
            element.attr('data-mili',milliseconds);
            element.attr('data-pauseon','');

            vars['hours_'+task_id]=hours;
            vars['minutes_'+task_id]=minutes;
            vars['seconds_'+task_id]=seconds;
            vars['milliseconds_'+task_id]=milliseconds;
            vars['data_pause_'+task_id]='';

             element.closest('tr').find('td .hours-left').html('').html(parseInt(hours)+':'+parseInt(minutes)+' hours');

           // $('#trpause_'+task_id).val('');

            // $('#trhours_'+task_id).val(hours);
            // $('#trmin_'+task_id).val(minutes);
            // $('#trsec_'+task_id).val(seconds);
            // $('#trmili_'+task_id).val(milliseconds);
          //  alert(hours+"h"+minutes+"m"+seconds+"s");
                setStopwatch(hours, minutes, seconds, milliseconds);
          

            }, 25);
        }

        // Split out timer functions into functions.
        // Easier to read and write down responsibilities
        function run() {
            $(this).attr('pause','');
            running = true;
            runTimer();
            if(start=='')
            {
                // $.ajax({
                //   url: '<?php echo base_url();?>user/task_countdown_update_start/',
                //   type: 'post',
                //   data: { 'task_id':task_id,'start':start },
                //   timeout: 3000,
                //   success: function( data ){
                //   //  alert('updated');
                //    toggleElement.attr('data-start','changed');
                //   }
                // });
            }
            toggleElement.text(pauseText);
            toggleElement.attr('data-current',pauseText);
        }

        function pause() {
            running = false;
            clearTimeout(timer);
         //   alert(toggleElement.data('pausetext'));
            toggleElement.text(resumeText);
            toggleElement.attr('data-current',resumeText);
              element.attr('data-pauseon','on');
               //  $('#trpause_'+task_id).val('on');
                      vars['data_pause_'+task_id]='on';
          //  alert(parseInt(hours)+"--"+parseInt(minutes)+"--"+parseInt(seconds)+"--"+parseInt(milliseconds));
            $.ajax({
            url: '<?php echo base_url();?>user/task_countdown_update/',
            type: 'post',
            data: { 'task_id':task_id,'hours':hours,'minutes':minutes,'seconds':seconds,'milliseconds':milliseconds,'pause':'on' },
            timeout: 3000,
            success: function( data ){
            //  alert('updated');
            }
          });
            
        }

        function reset() {
            running = false;
            pause();
            hours = minutes = seconds = milliseconds = 0;
            setStopwatch(hours, minutes, seconds, milliseconds);
            toggleElement.text(startText);
            toggleElement.attr('data-current',startText);
        }
      //  And button handlers merely call out the responsibilities
        toggleElement.on('click', function () {
            if(running) {
            //  alert("true");
              pause();
            } 
            else{ 
            // alert("false");
              run();
              }
        });

        resetElement.on('click', function () {
            reset();
        });

        demo();
        function demo(){
              running = false;
            
            //  hours =74; minutes=37;seconds =53; milliseconds = 764;
           
            element.attr('data-hour',hours);
            element.attr('data-min',minutes);
            element.attr('data-sec',seconds);
            element.attr('data-mili',milliseconds);

            vars['hours_'+task_id]=hours;
            vars['minutes_'+task_id]=minutes;
            vars['seconds_'+task_id]=seconds;
            vars['milliseconds_'+task_id]=milliseconds;
       

            //  $('#trhours_'+task_id).val(hours);
            // $('#trmin_'+task_id).val(minutes);
            // $('#trsec_'+task_id).val(seconds);
            // $('#trmili_'+task_id).val(milliseconds);

  // hours=$('#trhours_'+task_id).val();
  //           minutes=$('#trmin_'+task_id).val();
  //           seconds=$('#trsec_'+task_id).val();
  //           milliseconds=$('#trmili_'+task_id).val();
             if(hours!=0 || minutes!=0 || seconds!=0)
             {
              if(data_pause=='')
              { //alert("inside");

                run();
              }
             }
              //alert("after"+milliseconds);
              setStopwatch(hours, minutes, seconds, milliseconds);
              
        }




    });


  }
       // Init timers
       $(document).ready(function(){

 
        
        <?php 
        foreach ($task_list as $tk_key => $tk_value) {
         ?>       
          vars['hours_'+<?php echo $tk_value['id'];?> ]=$('#trhours_<?php echo $tk_value['id']?>').val();
          vars['minutes_'+<?php echo $tk_value['id'];?>]=$('#trmin_<?php echo $tk_value['id']?>').val();
           vars['seconds_'+<?php echo $tk_value['id'];?>]=$('#trsec_<?php echo $tk_value['id']?>').val();
            vars['milliseconds_'+<?php echo $tk_value['id'];?>]=0;
             vars['data_pause_'+<?php echo $tk_value['id'];?>]=$('#trpause_<?php echo $tk_value['id']?>').val();
         <?php
        }
        ?> 

        working_hours_timer();
   $(document).on('click','.paginate_button',function(){
           working_hours_timer();
   });
  
 });
   /***************************************************/

    function test(){
  <?php foreach ($custom_permission as $style_key => $style_value) 
  {
 
  if(!in_array($style_value['id'],$for_user_edit_per))
    { ?>

     $('td a.per_assigne_<?php echo $style_value['id']; ?>').css('display','none');
    $(".per_chkbox_<?php echo $style_value['id']; ?>").html('<div class="checkbox-fade fade-in-primary"><label>--</label></div>');
    $(".per_chkbox_<?php echo $style_value['id']; ?>").html('<div class="checkbox-fade fade-in-primary"><label>--</label></div>');
    //$('.per_timerplay_<?php echo $style_value['id']; ?>').css('display','none');
    $('td a.per_assigne_<?php echo $style_value['id']; ?>').css('display','none');
    $('.per_action_<?php echo $style_value['id']; ?>').css('display','none');
    $('.per_taskstatus_<?php echo $style_value['id']; ?>,.per_taskpriority_<?php echo $style_value['id']; ?>').css('display','none');

 <?php } 
      } ?>
    }
test();
   /*****************************************************/
       $(document).on('click', '.workout-timer__play-pause', function(){
         //$('.workout-timer').workoutTimer();
         var id = $(this).attr("data-id");
         var txt = $("#counter_"+id).html();
         var data = {};
         data['id'] = id;
         data['time'] = txt;
             $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>user/update_timer/",
                  data: data,
                  success: function(response) {
                 // $('#task_'+id).html(response);
                  },
               });
        /* alert(id);
         alert(txt);*/
       });
   
   
   
      /* var $eventLog = $('#event-log');
       var logEvent = function(eventType, eventTarget, timer) {
         var newLog = '"' + eventType + '" event triggered on #' + eventTarget.attr('id') + '\r\n';
         $eventLog.val( newLog.concat( $eventLog.val() ) );
       };
   
       $('.workout-timer-events').workoutTimer({
         onStart: logEvent,
         onRestart: logEvent,
         onPause: logEvent,
         onRoundComplete: logEvent,
         onComplete: logEvent
       });*/
     
</script>
<script>
   // Syntax highlighting
   $('pre code').each(function(i, block) {
     hljs.highlightBlock(block);
   });
   
   
</script><script type="text/javascript">
   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-36251023-1']);
   _gaq.push(['_setDomainName', 'jqueryscript.net']);
   _gaq.push(['_trackPageview']);
   
   (function() {
     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
   })();
   
</script>
<script type="text/javascript">

  

$('#alltask thead').on('click', '#overall_tasks', function(e){
    if ($(this).is(':checked')) {
     $(".assign_delete").show(); 
     }else{
      $(".assign_delete").hide(); 
     }  
     $('#alltask .overall_tasks').not(this).prop('checked', this.checked);

 });
</script>
      <script type="text/javascript">

function delete_task(del)
{
  $("#delete_task_id").val(JSON.stringify([$(del).data('id')]));
}
function delete_action()
{
 
  $.ajax({
        type: "POST",
        url: "<?php echo base_url().'user/alltasks_delete';?>",        
        data: {'id':$("#delete_task_id").val()},
        beforeSend: function() {

          $(".LoadingImage").show();
        },
        success: function(data) {

          $(".LoadingImage").hide();

          $(".popup_info_msg").show();
          $('.popup_info_msg .position-alert1').html('Success !!! Task Successfully Deleted..');
         setTimeout(function() {$(".popup_info_msg").hide();location.reload(); },1500);
           
          /*var task_ids = data.split(",");
          for (var i=0; i < task_ids.length; i++ ) { 
          $("#"+task_ids[i]).remove(); } 
          $(".alert-success-delete").show();
          setTimeout(function() { 
          }, 500);  */   
        }
      });
}

function review_action()
{
  var task_id=[];
   //alert(task_id.length);
   TaskTable_Instance.column(1).nodes().to$().each(function(index) {
        
      if($(this).find(".alltask_checkbox").is(":checked")){
      task_id.push($(this).closest('tr').attr('data-id'));
      }
    });
   if(task_id.length > 0)
      {

  $.ajax({
        type: "POST",
        url: "<?php echo base_url().'user/manager_notify';?>",        
        data: {task_id:task_id,},
        beforeSend: function() {

         // $(".LoadingImage").show();
        },
        success: function(data) {

         //  $(".LoadingImage").hide();

         //  $(".popup_info_msg").show();
         //  $('.popup_info_msg .position-alert1').html('Success !!! Task Successfully Deleted..');
         // setTimeout(function() {
         //  $(".popup_info_msg").hide();
         //  location.reload();
         //   },1500);
           
          /*var task_ids = data.split(",");
          for (var i=0; i < task_ids.length; i++ ) { 
          $("#"+task_ids[i]).remove(); } 
          $(".alert-success-delete").show();
          setTimeout(function() { 
          }, 500);  */   
        }
      });
  }
  else
  {
    $(".popup_info_msg").show();
          $('.popup_info_msg .position-alert1').html('Please Select the Task');
         setTimeout(function() {
          $(".popup_info_msg").hide();
          location.reload();
           },1500);

  }
}

     

        



      $(document).ready(function () {
    //var table = $('#alltask').DataTable();           
         var tmp = []; 
       $('#alltask tbody').on('click', '.overall_tasks', function(e){
             $(".assign_delete").show();    
           $(".overall_tasks").each(function() {
               if ($(this).is(':checked')) {    

               var result = $(this).attr('id').split('_');
               var checked = result[2];
               tmp.push(checked);
               }
            });         
     // alert(tmp);    

          });     


        $(document).on('click' , ".assign_tsk_btn" , function(){
      //   alert('assign');
            var alltask = getSelectedRow();
          if(alltask.length <=0) {
             $('.popup_info_msg').show();
             $(".popup_info_msg .position-alert1").html("Please Selecte Any Records..");
          } else {
   
           var selected_alltask_values = alltask.join(",");

            $('.assignTask-Popup .comboTreeItemTitle input[type="checkbox"]:checked').each(function(){
              var id1 = $(this).closest('.comboTreeItemTitle').attr('data-id');
              var id2 = id1.split('_');
              Assignees.push( id2[0] );
               });
                         //  data['assignee'] = Assignees

            var assign_role = Assignees.join(',');
 
            var formData={'task_id':selected_alltask_values,'assign_role':assign_role};
          // alert(assign_to);
           $.ajax({
            type: "POST",
            url: "<?php echo base_url().'user/update_assignees';?>",
            cache: false,
            data: formData,
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data) {
              $(".LoadingImage").hide();
              var json = JSON.parse(data); 
              status=json['status'];
              $(".popup_info_msg").show();
              $(".popup_info_msg .position-alert1").html(" Staff Assigned Successfully...");
              if(status=='1'){
               setTimeout(function(){ $(".popup_info_msg").hide(); location.reload(); }, 3000); 
                  }           
                 }
              });

           
          }
        });

             
        $(document).on('click' , "#assign_member" , function(){
      //   alert('assign');
            var alltask = getSelectedRow();
          if(alltask.length <=0) {
             $('.popup_info_msg').show();
             $(".popup_info_msg .position-alert1").html("Please Selecte Any Records..");
          } else {
   
           var selected_alltask_values = alltask.join(",");

   
       $(".assigned_staff").click(function(){
       var assign_to= $(".workers_assign").val();

       var formData={'task_id':selected_alltask_values,'staff_id':assign_to};
      // alert(assign_to);
       $.ajax({
        type: "POST",
        url: "<?php echo base_url().'user/alltasks_assign';?>",
        cache: false,
        data: formData,
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          $(".LoadingImage").hide();
          var json = JSON.parse(data); 
          status=json['status'];
          $(".popup_info_msg").show();
          $(".popup_info_msg .position-alert1").html(" Staff Assigned Successfully...");
          if(status=='1'){
           setTimeout(function(){ $(".popup_info_msg").hide(); location.reload(); }, 3000); 
              }           
             }
          });

       });

      }
    });

        $("#saveSubtaskbutton").click(function()
        {
          
          var name = $("#addSubTask-Popup").find("#subTaskName").val();

          if(name=='')return;

          var selected = getSelectedRow();

          var ids = JSON.stringify( selected );

          $.ajax({url:"<?php echo base_url()?>Task/addSubTask",
            type:"POST",
            data:{'name':name,'ids':ids},
            beforeSend : function ()
            {
              $(".LoadingImage").show();
            },
            success:function(res)
            {
              $(".LoadingImage").hide();
                
                console.log(  selected );

                $.each( selected ,function(i,v){
                  var data1={};

                  var tr = $("#"+v);
                  var row = TaskTable_Instance.row( tr );

                var toggleElement = tr.find('.stopwatch .toggle');
                data1['status']='inprogress';
                data1['rec_id']=v;
                tr.find(".task_status").val('inprogress');  

                 var all_qCodes = $.ajax({type: "POST",data: data1 ,dataType: "json", url: "<?=base_url()?>user/task_statusChange", async: false}).responseJSON;
                  
                  if(toggleElement.hasClass('time_pause1') )
                 {
                    toggleElement.trigger('click');
                   // data1['change_id']='1';
                 }



                 if ( row.child.isShown() ) 
                 {                 
                  row.child.hide();
                  tr.removeClass('shown');
                 }
                 else
                 {
                   tr.find("td:nth-child(1)").addClass("details-control");
                 }

              });
                $("#addSubTask-Popup").modal('hide');
            }
        });







        });

         $(document).on('change',".subTaskcheckbox",function(){
          var con = $(this).is(":checked");
          var sel = $(this).closest('tr').find('select.task_status');
          var strikeout = $(this).closest('tr').find('td.subject_td');

          if( con )
          {
            sel.val('complete');
            sel.trigger('change');
            strikeout.addClass('strikeout');
          }
          else
          {
            
            sel.val('inprogress');
           
            sel.trigger('change');
            strikeout.removeClass('strikeout');

          }
        });

         //  $("#delete_task").click(function(){
         //    $('#delete_user'+tmp).show();
         //       $('.delete_yes').click(function() {
         //          var formData={'id':tmp};
         //       $.ajax({
            
         //         url: '<?php echo base_url();?>user/tasks_delete',
         //         type : 'POST',        
         //         data : formData,
         //         beforeSend: function() {
         //           $(".LoadingImage").show();
         //         },
         //         success: function(data) {
         //         // alert(data);
         //            $(".LoadingImage").hide();
         //           location.reload();
         //         }
         //    });
         //         }); 
            
         // }); 
       
        
      });

$(".overall_tasks").each(function() {
               if ($(this).is(':checked')) {    

               var result = $(this).attr('id').split('_');
               var checked = result[2];
               tmp.push(checked);
               }
            }); 
</script>

<script type="text/javascript">
 $(document).ready(function() {
   

 $(document).on('click','#delete_task', function() {

    var alltask = getSelectedRow();
    //alert(JSON.stringify(alltask));
    //$("#delete_task_id").attr("value",JSON.stringify(alltask));
    $("#delete_task_id").val(JSON.stringify(alltask));

    });

 });  






  $( document ).ready(function() {
  
    $.ajax( {
  
       type:"post",
       dataType : "json",
       url:"<?php echo base_url();?>user/get_projects",
       processData: false,
       contentType: false,
        data:{ 
         'get':'get'
       }
       ,
       success:function(response)
       {
  
  
       var ret='<div id="frame_projects"><label>Projects</label><select name="r_project" class="form-control" id="r_projects" placeholder="select">';
  
           $.each(response, function (index, value){
           ret+='<option value='+value.id+'>'+value.project_name+'</option>';
           });
  
         ret+='</select><div>';
  
        $("#base_projects").before(ret);
       }
   });
  
  
      var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  
      // Multiple swithces
      var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
  
      elem.forEach(function(html) {
          var switchery = new Switchery(html, {
              color: '#1abc9c',
              jackColor: '#fff',
              size: 'small'
          });
      });
  
      $('#accordion_close').on('click', function(){
              $('#accordion').slideToggle(300);
              $(this).toggleClass('accordion_down');
      });
  
     
      
  });
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->

<script type="text/javascript">
  $(document).ready(function(){
    $("#country").change(function(){
    var country_id = $(this).val();
    //alert(country_id);
  
      $.ajax({
  
        url:"<?php echo base_url().'Client/state';?>",
        data:{"country_id":country_id},
        type:"POST",
        success:function(data){
          //alert('hi');
          $("#state").append(data);
          
        }
  
      });
    });
     $("#state").change(function(){
    var state_id = $(this).val();
    //alert(country_id);
  
      $.ajax({
  
        url:"<?php echo base_url().'Client/city';?>",
        data:{"state_id":state_id},
        type:"POST",
        success:function(data){
          //alert('hi');
          $("#city").append(data);
          
        }
  
      });
    });
  
  
  
  $("#related_to").change(function () {
       var val = this.value;
  
       if(val=='projects'){
  
            $.ajax( {
  
       type:"post",
       dataType : "json",
       url:"<?php echo base_url();?>user/get_projects",
       processData: false,
       contentType: false,
        data:{ 
         'get':'get'
       }
       ,
       success:function(response)
       {
  
  
       var ret='<div id="frame_projects"><label>Projects</label><select name="r_project" class="form-control" id="r_projects" placeholder="select">';
  
           $.each(response, function (index, value){
           ret+='<option value='+value.id+'>'+value.project_name+'</option>';
           });
  
         ret+='</select><div>';
  
        $("#base_projects").before(ret);
       }
   });
  
  
       }else{
       
       $('#frame_projects').remove();
       }
       
   });
  
  
  
  });

$(document).ready(function(){      




 
        <?php
        if(isset($_SESSION['firm_seen'])){
        if($_SESSION['firm_seen']=='complete'){ ?>
          $("div.comple_task").trigger('click');
       <?php } ?>

        <?php
        if($_SESSION['firm_seen']=='notstarted'){ ?>
          $("div.nots_task").trigger('click');
       <?php } ?>


        <?php
        if($_SESSION['firm_seen']=='inprogress'){ ?>
          $("div.inpro_task").trigger('click');
       <?php } ?>

         <?php
        if($_SESSION['firm_seen']=='low'){ ?>
          $("#prioritywise_filter").val('low').trigger('change');
       <?php } ?>

         <?php
        if($_SESSION['firm_seen']=='medium'){ ?>
          $("#prioritywise_filter").val('medium').trigger('change');
       <?php } ?>

         <?php
        if($_SESSION['firm_seen']=='high'){ ?>
          $("#prioritywise_filter").val('high').trigger('change');
       <?php } ?>

        <?php
        if($_SESSION['firm_seen']=='super_urgent'){ ?>
          $("#prioritywise_filter").val('super_urgent').trigger('change');
       <?php } ?>


       <?php } ?>
   });
  </script>
<!-- for new timer -->
<script type="text/javascript">
$(document).on('click','.for_timer_start_pause',function(){

   var toggleElement = $(this).closest('tr').find('.task_status');

var task_id=$(this).attr('id');
//var task_id=$(this).parent().attr("id");
$.ajax({
url: '<?php echo base_url();?>user/task_timer_start_pause/',
type: 'post',
data: { 'task_id':task_id },
timeout: 3000,
success: function( data ){
//  alert('updated');
}
});
var data1={};
if(toggleElement.val()=='complete')
{
data1['change_id']=1;
data1['status']=toggleElement.val();
}
data1['task_id']=task_id;

$.ajax({
url: '<?php echo base_url();?>user/task_timer_start_pause_individual/',
type: 'post',
data: data1,
timeout: 3000,
success: function( data ){
//  alert('updated');
}
});
});

</script>
<!-- end of new timer -->
<!-- for customize permission style -->
<script type="text/javascript">
$(document).ready(function(){
 <?php foreach ($custom_permission as $style_key => $style_value) { 
  ?>

  <?php
if(!in_array($style_value['id'],$for_user_edit_per)){
  ?>
$(".per_chkbox_<?php echo $style_value['id']; ?>").html('<div class="checkbox-fade fade-in-primary"><label>--</label></div>');
//$('button.per_timerplay_<?php echo $style_value['id']; ?>').css('display','none');
//$('td a.per_assigne_<?php echo $style_value['id']; ?>').css('display','none');
$('td a.adduser1.per_assigne_<?php echo $style_value['id']; ?>').css('display','none');  
$('.per_action_<?php echo $style_value['id']; ?>').css('display','none');
$('.per_taskstatus_<?php echo $style_value['id']; ?>,.per_taskpriority_<?php echo $style_value['id']; ?>').css('display','none');
 <?php }
  } ?>
});


$(document).on('change','.task_status',function(e){

    var toggleElement1 = $(this).closest('tr').find('.stopwatch');
    
    var rec_id = $(this).data('id');
    var data1={};
    var stat = $.trim( $(this).val() );

      if(stat=='complete')
       {
        var sub_check=toggleElement1.attr('data-subid');
         // if( $(this).closest('tr').hasClass('shown')){
         //       $(this).closest('tr').find('.details-control').trigger('click');
         //    }
         if(sub_check!=0)
          {
             var res = sub_check.split(",");
              $.each(res, function (key, val) {

               var toggleElement = $('tr .subtask_check_'+rec_id+'_'+val).find('.stopwatch .toggle');

              if(toggleElement.hasClass('time_pause1') )
               {
                var url1="<?php echo base_url();?>user/task_timer_start_pause_individual";
               toggleElement.trigger('click');

                 }else{
                  var url1="";
                 }
                 //else{
                  data1['task_id']=val;
                  data1['status']='complete';
                  data1['change_id']=1;
               var all_qCodes = $.ajax({type: "POST",   data: data1, dataType: "json", url:url1 , async: false}).responseJSON;
            // }
                    
                });

             }
           }
           
           var toggleElement = $(this).closest('tr').find('.stopwatch .toggle');
         if(stat=='complete' && toggleElement.hasClass('time_pause1') )
         {

            toggleElement.trigger('click');

         }
         else
         {         
       data1['rec_id']=rec_id;
       data1['status']=stat;

       var that = $(this);
       if(stat=='')return;
          $.ajax({
            url: '<?php echo base_url();?>user/task_statusChange/',
            type: 'post',
            data: data1,
            
            beforeSend:function(){$(".LoadingImage").show();},
            success: function( data ){
               $(".LoadingImage").hide();

               $(".popup_info_msg").show();
               $('.popup_info_msg .position-alert1').html('Success !!! Task status have been changed successfully...');
                $('.taskpagedashboard').load("<?php echo base_url();?>user/task_summary_data");
                
                var cell = that.closest('td');
                cell.attr('data-search',stat);               
               setTimeout(function () {
                $(".popup_info_msg").hide();
                }, 1500);
             
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });          
        }
   });




 $(document).on('change','.task_priority',function () {                                
      
       var rec_id = $(this).data('id');       
       var priority = $.trim( $(this).val() );
       var  that = $(this);
       if(priority=='')return;
          $.ajax({
            url: '<?php echo base_url();?>user/task_priorityChange/',
            type: 'post',
            data: { 'rec_id':rec_id,'priority':priority },
            timeout: 3000,
            beforeSend:function(){$(".LoadingImage").show();},
            success: function( data ){
              
              $(".LoadingImage").hide();
              $(".popup_info_msg").show();
               $('.popup_info_msg .position-alert1').html('Success !!! Task Priority have been changed successfully...');
               setTimeout(function () {
                $(".popup_info_msg").hide(); 

                }, 1500);

                var cell = that.closest('td');
                cell.attr('data-search',priority);
                TaskTable_Instance.cell( cell ).invalidate().draw();

                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });           
        
   });



$('.suspent').click(function() {
        if($(this).is(':checked'))
            var stat = '1';
        else
            var stat = '0';
        var rec_id = $(this).val();
        var $this = $(this);
         $.ajax({
            url: '<?php echo base_url();?>user/suspentChange/',
            type: 'post',
            data: { 'rec_id':rec_id,'status':stat },
            success: function( data ){
               //alert('ggg');
                $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
                if(stat=='1'){
                  
                   $this.closest('td').next('td').html('Payment');
      
                } else {
                   $this.closest('td').next('td').html('Non payment');
                }
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
      });



 $('#alluser').on('change','.status',function () {
      //e.preventDefault();  
       var rec_id = $(this).data('id');
       var stat = $(this).val();      
      $.ajax({
            url: '<?php echo base_url();?>user/statusChange/',
            type: 'post',
            data: { 'rec_id':rec_id,'status':stat },
            timeout: 3000,
            success: function( data ){
               //alert('ggg');
                $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });
               // setTimeout(resetAll,3000);
                if(stat=='3'){
                  
                   //$this.closest('td').next('td').html('Active');
                   $('#frozen'+rec_id).html('Frozen');
      
                } else {
                   $this.closest('td').next('td').html('Inactive');
                }
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
       });





$(document).on("change", "#select_alltask", function(event) {

   var table=$(this).attr("id").replace("select_","");
   var checked = this.checked;
   var count=0;
   var task_id=0;
    var user_type="<?php echo $_SESSION['user_type'] ?>";

     $(".LoadingImage").show();   

    TaskTable_Instance.column(1,{ search: 'applied' })
    .nodes()
    .to$().each( function () {

   
      $(this).find('.alltask_checkbox').prop('checked',checked);

      console.log(MainStatus_Tab);

        if (checked)
         {         
            $(this).find('.alltask_checkbox').prop('checked',true);


            if(MainStatus_Tab == "archive")$("#unarchive_task").show();
            else $('#archive_task').show(); 
            $("#assign_member").show();
           // $("#btn_review_send").show();
            
            $("#delete_task").show();  
            $("#addSubtask-Btn").show(); 
            $('.task_assign_btn').show();

         
        } 
        else
         {

            $(this).find('.alltask_checkbox').prop('checked',false);
            
            if(MainStatus_Tab == "archive")$('#unarchive_task').hide();
            else $("#archive_task").hide();
            $("#addSubtask-Btn").hide();  
            $('.task_assign_btn').hide();
            $("#assign_member").hide();
            $("#delete_task").hide(); 
             // $("#btn_review_send").hide();       
        }



    });
    $(".LoadingImage").hide();    
    //$("#select_"+table+"_count").val($("input."+table+"_checkbox:checked").length + " Selected");
  });

$(document).on("change",".alltask_checkbox",function(event)
    { 
      var count=0;
      var tr=0;
      var ck_tr=0;
      var task_id=0;
       var user_type="<?php echo $_SESSION['user_type'] ?>";
      TaskTable_Instance.column(1).nodes().to$().each(function(index) {
        
      if($(this).find(".alltask_checkbox").is(":checked")){
          ck_tr++;

         var status_val= '';  
         var table_id=$(this).closest('tr');
         status_val= table_id.find(".task_status").val();  

      }

      
      tr++;

      });
       //  alert("re"+$("#alltask").attr("data-status")+ck_tr);
        if(tr==ck_tr)
          {
            $("#select_alltask").prop("checked",true);
           } 
        else
        {
         $("#select_alltask").prop("checked",false);
        } 
      if(ck_tr)
      {
           // alert(user_type);
            if(MainStatus_Tab=="archive")$("#unarchive_task").show();
            else $('#archive_task').show(); 
            $("#assign_member").show();
            $("#addSubtask-Btn").show();  
            $('.task_assign_btn').show();
            $("#delete_task").show();  
            // if(count==0 && user_type!='FA'){
            //   // alert('hai');
            //  $("#btn_review_send").show();
            // }
            // else
            // {
            //   $("#btn_review_send").hide();
            // }
      }
      else  
      {
            if(MainStatus_Tab=="archive")$('#unarchive_task').hide();
            else $("#archive_task").hide();
            $("#assign_member").hide();
            $("#addSubtask-Btn").hide(); 
            $('.task_assign_btn').hide(); 
            $("#delete_task").hide();  
             //$("#btn_review_send").hide();
      } 
     
          $("#select_alltask_count").val($("input.alltask_checkbox:checked").length+" Selected");
       
    });
  
$("#close_action_result").click(function(){$(".popup_info_msg").hide();});//for close mssage box
$("#close_info_msg").click(function(){$(".popup_info_msg").hide(); });

 </script>
    
<script type="text/javascript">
    $(document).ready(function(){
  
 var fruits = [];

$('.data_active_task th').each(function(){
fruits.push($(this).outerWidth());
  //console.log(fruits);
}); 


 $(document).on("click",".controls .for_timer_start_pause",function() {
    $(this).toggleClass('time_pause1')
    });

});

  function getSelectedRow()
  {
    var alltask = [];

    TaskTable_Instance.column(1).nodes().to$().each(function(index) {
      
      if( $(this).find(".alltask_checkbox").is(":checked") )
      {
        alltask.push( $(this).find(".alltask_checkbox").attr('data-alltask-id') );      
      }

    });

    return alltask;

  }
</script>

<script src="<?php echo base_url();?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/icontains.js"></script>
<script type="text/javascript">

      var tree_select = <?php echo json_encode( GetAssignees_SelectBox() ); ?>;
      <?php  if(!empty($asssign_group)) { ?>
      var arr=<?php echo json_encode($asssign_group); ?>;
      console.log(tree_select);       
      var tree_id=$('.tree_select').comboTree({
        source : tree_select,
        isMultiple: true,   
      });
      <?php } ?>
           
        
             
      $(".user_change").click(function(){


       var id = $(this).data('target');
       id=id.split('_');
       id=id[1];
       var main_id=id;
       var task_name='TASK';
       var Assignees=[];



      if(id!='')
      {

        $.ajax({
          url : "<?php echo base_url()?>leads/get_assigness",
          type : "POST",
          data : {"id":id,"task_name":task_name},
          dataType : "json",        
          beforeSend : function(){
            $(".LoadingImage").show();
          },
          success: function(data){
            $(".LoadingImage").hide();  

          $('#adduser_'+main_id+' .comboTreeItemTitle').click(function(){ 

          Assignees=[];

            var id = $(this).attr('data-id');
            var id = id.split('_');

            $('#adduser_'+main_id+' .comboTreeItemTitle').each(function(){
            var id1 = $(this).attr('data-id');
            var id1 = id1.split('_');
            //console.log(id[1]+"=="+id1[1]);
            if(id[1]==id1[1])
            {
               $(this).toggleClass('disabled');
            }


            });
            $(this).removeClass('disabled');

              $('#adduser_'+main_id+' .comboTreeItemTitle input[type="checkbox"]:checked').each(function(){
            var id = $(this).closest('#adduser_'+main_id+' .comboTreeItemTitle').attr('data-id');
            var id = id.split('_');
            Assignees.push( id[0] );
             });

          var assign_role = Assignees.join(',');
       $('#adduser_txt_'+main_id).val(assign_role);

         });
                      


                 var arr1 = data.asssign_group;
                 var assign_check = data.assign_check; 
                        //alert(assign_check);
                 var unique = arr1.filter(function(itm, i, arr1) {
              return i == arr1.indexOf(itm);
               });
                 
       $('#adduser_'+main_id+' .comboTreeItemTitle').each(function(){

        $(this).find("input:checked").trigger('click');

               var id1 = $(this).attr('data-id');
                var id = id1.split('_');
             if(jQuery.inArray(id[0], unique) !== -1)
               {
                  $(this).find("input").trigger('click');
                 
                  Assignees.push( id[0] );
              }
                    
               });

 


   

         
         var assign_role = Assignees.join(',');
       $('#adduser_txt_'+main_id).val(assign_role);
         //alert(Assignees);
  
          }
      });
      
      }   
});
    


    function prependZero(time, length) {
        time = new String(time);    // stringify time
        return new Array(Math.max(length - time.length + 1, 0)).join("0") + time;
    }
      setInterval(function () {
          $.ajax({
            url: '<?php echo base_url();?>user/main_task_timer/',
            type: 'post',
            dataType: "json",
            success: function( data ){
            //  var result= $.parseJSON(data);
            //  alert('updated');
             let time_val='';
             let element='';
             let hoursElement='';
             let secondsElement='';
              let millisecondsElement='';
            $.each( data ,function(i,v){

               time_val=v.split(',');
            //  console.log(time_val[3]);

                element = $('#'+time_val[3]).find('.stopwatch');
             //  console.log(element.data('subid'));

        if(element.data('subid')!=0)
        {

            element.attr('data-hour',time_val[0]);
            element.attr('data-min',time_val[1]);
            element.attr('data-sec',time_val[2]);
         
            element.attr('data-pauseon','');

            hoursElement = element.find('.hours');
            minutesElement = element.find('.minutes');
            secondsElement = element.find('.seconds');
            millisecondsElement = element.find('.milliseconds');


          hoursElement.text(prependZero(time_val[0], 2));
            minutesElement.text(prependZero(time_val[1], 2));
            secondsElement.text(prependZero(time_val[2], 2));            
            millisecondsElement.text(prependZero(milliseconds, 3));

             }
            });
            //console.log(result);
            }
          });
            //}, 10000);
        }, 60000);

         </script>



<?php $this->load->view('tasks/import_task'); ?>
  