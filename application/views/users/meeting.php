<?php $this->load->view('includes/header');?>  

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<div class="pcoded-content">
<div class="meeting_slider">
<div class="owl-carousel">
                  <div class="item selectable-days" data-hash="one">
                      <span class="day">
                        Tue
                        <p>oct 24</p>
                      </span>
                      <p>unavailable</p>
                  </div>
                  <div class="item selectable-days" data-hash="two">
                      <span class="day">
                        Tue
                        <p>oct 24</p>
                      </span>
                      <p>unavailable</p>
                  </div>
                  <div class="item selectable-days enable" data-hash="three">
                      <span class="day">
                        Tue
                        <p>oct 24</p>
                      </span>
                      
                  </div>
                  <div class="item selectable-days enable" data-hash="four">
                      <span class="day">
                        Tue
                        <p>oct 24</p>
                      </span>
                      
                  </div>
                  <div class="item selectable-days enable" data-hash="five">
                      <span class="day">
                        Tue
                        <p>oct 24</p>
                      </span>
                      
                  </div>
                  <div class="item selectable-days enable" data-hash="six">
                      <span class="day">
                        Tue
                        <p>oct 24</p>
                      </span>
                      
                  </div>
                  <div class="item selectable-days enable" data-hash="seven">
                      <span class="day">
                        Tue
                        <p>oct 24</p>
                      </span>
                      
                  </div>
                  <div class="item selectable-days enable" data-hash="eight">
                      <span class="day">
                        Tue
                        <p>oct 24</p>
                      </span>
                      
                  </div>
              </div> 
            </div>  
</div>

<script type="text/javascript">
	$('.owl-carousel').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots:false,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:5,
        },
        1000:{
            items:7,
        }
    }
})
</script>