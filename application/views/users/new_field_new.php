  
<form action="<?php echo base_url().'User/field/'.$custom_fields[0]['id']?>" method="post" accept-charset="utf-8" novalidate="novalidate">
    <div class="modal-body">
		 <div class="form-group">
			<label class="col-sm-5 col-form-label">Field Name</label>
			<div class="col-sm-7">
				<input type="text" id="name" name="name" class="form-control" value="<?php if(isset($custom_fields[0]['name'])){ echo $custom_fields[0]['name']; } ?>">
			</div>
		</div>	
		
		<div class="form-group">
			<label class="col-sm-5 col-form-label">Field Type</label>
			<div class="col-sm-7">
				<select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input" <?php if(isset($custom_fields[0]['type']) && $custom_fields[0]['type']=='input') {?> selected="selected"<?php } ?>>Input</option>
                                <option value="number" <?php if(isset($custom_fields[0]['type']) && $custom_fields[0]['type']=='number') {?> selected="selected"<?php } ?>>Number</option>
                                <option value="textarea" <?php if(isset($custom_fields[0]['type']) && $custom_fields[0]['type']=='textarea') {?> selected="selected"<?php } ?>>Textarea</option>
                                <option value="select" <?php if(isset($custom_fields[0]['type']) && $custom_fields[0]['type']=='select') {?> selected="selected"<?php } ?>>Select</option>
                                <!-- <option value="checkbox" <?php if(isset($custom_fields[0]['type']) && $custom_fields[0]['type']=='checkbox') {?> selected="selected"<?php } ?>>Checkbox</option> -->
                                <option value="date_picker" <?php if(isset($custom_fields[0]['type']) && $custom_fields[0]['type']=='date_picker') {?> selected="selected"<?php } ?>>Date Picker</option>
                            </select>
			</div>
		</div>

        <div id="options_wrapper" style="display:none;">
       
        <div class="form-group" app-field-wrapper="options">
            <label for="options" class="col-sm-5  control-label"> 
                <small class="req text-danger">* </small>Options</label>
                <div class="col-sm-7"><textarea id="options" name="options" class="form-control" rows="3"><?php if(isset($custom_fields[0]['options'])){ echo $custom_fields[0]['options']; } ?></textarea></div></div>  
            </div>
			
		 <div class="col-md-12">
               <?php //$rel_id=( isset($client) ? $client->userid : false); ?>
               <?php // echo render_custom_fields_one( 'customers',2); 

               ?>
               <?php 
        
             
               ?>
            </div>
		

         <!-- <div class="form-group row title_submitbt">
           <button type="submit" class="btn btn-info pull-right">Save</button>
          </div> -->
           <div class="modal-save">
              <input type="submit" class="add_new_field" value="Save" id="submit"/>
            </div>
              </div> 

                    </form>
      

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>

    <!-- j-pro js -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>

    <!-- jquery slimscroll js -->

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

    <!-- modernizr js -->

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>

    

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   
    

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>


    <script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/demo-12.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>

    

  <script>

    $( document ).ready(function() {
    
        var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();

        // Multiple swithces
        var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));

        elem.forEach(function(html) {
            var switchery = new Switchery(html, {
                color: '#1abc9c',
                jackColor: '#fff',
                size: 'small'
            });
        });

        $('#accordion_close').on('click', function(){
                $('#accordion').slideToggle(300);
                $(this).toggleClass('accordion_down');
        });

        $("#type").change(function(){
//alert($(this).val());
var rec = $(this).val();
if(rec=='select' || rec=='checkbox' )
{
    $('#options_wrapper').show();
}
else{
        $('#options_wrapper').hide();
}
});
});
    </script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$("#admin_setting").validate({
  
       ignore: false,

       /*onfocusout: function(element) {
        if ( !this.checkable(element)) {
            this.element(element);
        }
    },*/
   // errorClass: "error text-warning",
    //validClass: "success text-success",
    /*highlight: function (element, errorClass) {
        //alert('em');
       // $(element).fadeOut(function () {
           // $(element).fadeIn();
        //});
    },*/
                        rules: {
                        name: {required: true},
                        
                        },
                        errorElement: "span" , 
                        errorClass: "field-error",                             
                         messages: {
                          name: "Enter a name",
                         },
                         

                        
                        submitHandler: function(form) {
                            var formData = new FormData($("#admin_setting")[0]);
                            

                            $(".LoadingImage").show();

                            $.ajax({
                                url: '<?php echo base_url();?>user/add_admin/',
                                dataType : 'json',
                                type : 'POST',
                                data : formData,
                                contentType : false,
                                processData : false,
                                success: function(data) {
                                    
                                    if(data == 1){
                                       
                                       // $('#new_user')[0].reset();
                                        $('.alert-success').show();
                                        $('.alert-danger').hide();
                                        window.location = "<?php echo base_url();?>user/admin_setting";
                                    }
                                    else{
                                       // alert('failed');
                                        $('.alert-danger').show();
                                        $('.alert-success').hide();
                                    }
                                $(".LoadingImage").hide();
                                },
                                error: function() { $('.alert-danger').show();
                                        $('.alert-success').hide();}
                            });

                            return false;
                        } ,
                         invalidHandler: function(e, validator) {
           if(validator.errorList.length)
        $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')

        }
                         
                    });


});
</script>
<script type="text/javascript">
 $(document).ready(function(){
   $("#country").change(function(){
   var country_id = $(this).val();
   //alert(country_id);

     $.ajax({

       url:"<?php echo base_url().'Client/state';?>",
       data:{"country_id":country_id},
       type:"POST",
       success:function(data){
         //alert('hi');
         $("#state").append(data);
         
       }

     });
   });
    $("#state").change(function(){
   var state_id = $(this).val();
   //alert(country_id);

     $.ajax({

       url:"<?php echo base_url().'Client/city';?>",
       data:{"state_id":state_id},
       type:"POST",
       success:function(data){
         //alert('hi');
         $("#city").append(data);
         
       }

     });
   });
 });
 </script>

</body>



</html>