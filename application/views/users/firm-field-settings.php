<?php $this->load->view('includes/header');?>

<!-- management block -->
<div class="pcoded-content firm-pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <div class="card firm-field">
			
               <div class="import-container-1 margin-les">
                   
                  <div class="page-header card">
                                        <div class="align-items-end">
                                                <div class="page-header-title pull-left">
                                                    
                                                    <div class="d-inline">
                                                        <h4> client fields setup 
														</h4>
                                                    </div>
                                                </div>
												<div class="pull-right csv-sample01 ">
												<a href="<?php echo base_url().'user/custom_fields';?>">
												<i class="fa fa-pencil fa-6" aria-hidden="true"></i> Custom fields Edit & Delete</a>
												</div>
                                        </div>
                                    </div>
                                    
                                    </div>
									
			   
               
               <!-- <div class="j-info"><i class="fa fa-circle" aria-hidden="true"></i> please not that only selected ( <i class="fa fa-check" aria-hidden="true"></i> ) fields are available in important template.</div> -->
               <div class="info-field">
                  <div class="panel-group indy-masonry-container masonry-container" id="indy-masonry-container">
					
					<div class="grid-sizer"></div>
                     <div class="panel panel-default msrItem accordion-panel">
					 <div class="border-firm-setting">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse1">
                           <h4 class="panel-title">
                              <a href="">Details</a>
                           </h4>
                           <!-- <i class="fa fa-angle-double-down" aria-hidden="true"></i> -->
                        </div>
                        <div id="collapse1" class="panel-collapse collapse show">
                           <div class="panel-body">
                            <div class="sub-headparent"><span class="sub-head">important details</span></div>
                              <div class="left-half">
                              <ul class="sortable">
                              <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check"> -->
                                       <label for="p-check">Company Name</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check1"> -->
                                       <label for="p-check1">Company Number</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check2"> -->
                                       <label for="p-check2">Incorporation Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check3"> -->
                                    <label for="p-check3">Registered In</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check38"> -->
                                       <label for="p-check38">Address Line 1</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check39"> -->
                                       <label for="p-check39">Address Line 2</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check40"> -->
                                       <label for="p-check40">Address Line 3</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check41"> -->
                                       <label for="p-check41">Town/City</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>
                              <!-- left half-->

                              <div class="left-half right-half">
                              <ul class="sortable">
                                 
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check42"> -->
                                       <label for="p-check42">Post Code</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check43"> -->
                                       <label for="p-check43">Company Status</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                  <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check44"> -->
                                       <label for="p-check44">Company Type</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check4"> -->
                                       <label for="p-check4">Company S.I.C</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <?php //print_r($edit_fields); ?>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check6"> -->
                                       <label for="p-check6"> <?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','business_website');
                                       echo (isset($business)&&($business[0]['name']!=''))? $business[0]['name']:'Business Websites';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="business_website">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check32"> -->
                                       <label for="p-check32"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','accounting_system_inuse');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Accounting System In Use';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data">
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="accounting_system_inuse">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div>
                              <div class="sub-headparent"><span class="sub-head">companies house & HMRC</span></div>

                              <div class="left-half">
                                 <ul class="sortable">  
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check33"> -->
                                       <label for="p-check33">Authentication Code</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>

                               <div class="left-half right-half">
                                 <ul class="sortable">
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check34"> -->
                                       <label for="p-check34">UTR Number</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>

                               <div class="sub-headparent"><span class="sub-head">payroll</span></div>

                               <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check35"> -->
                                       <label for="p-check35">Accounts Office Reference Number</label>
                                    </div>
                                   <!--  <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                               </div>

                              <div class="left-half right-half">
                              <ul class="sortable">
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check36"> -->
                                       <label for="p-check36">PAYE Reference Number</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 
                                 </ul>
                              </div>
                              <!-- right half-->

                              <div class="sub-headparent"><span class="sub-head">VAT</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check37"> -->
                                       <label for="p-check37">VAT Number</label>
                                    </div>
                                   <!--  <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>
                              <!-- custom -->
                              <div class="left-half">
                                 <ul class="sortable">
                                    <?php 
                                        $variable=$this->db->query('SELECT * FROM tblcustomfields WHERE fieldto="customers" AND active=1')->result_array();
                                        foreach ($variable as $key => $value) {
                                          
                                       ?>
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check37"> -->
                                       <label for="p-check37"><?php echo $value['name'];?></label>
                                    </div>
                                   <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <form class="validation edit_custom_field_form" method="post" action="" enctype="multipart/form-data">
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="<?php echo $value['fieldto'];?>">
            <input type="hidden" name="id" id="idto" value="<?php echo $value['id'];?>">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <?php } ?>
                                 </ul>
                              </div>
                              <!-- custom end-->

                           </div>

                           <!--add field-->

                           <div class="add-extrafield">
                              <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="customers"><i class="fa fa-plus"></i></span>
                           </div>

                           <!--add field-->


                        </div>
                     </div>
					 </div>

                     <div class="panel panel-default msrItem accordion-panel">
					 <div class="border-firm-setting">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse2">
                           <h4 class="panel-title">
                              <a  href="#collapse2">assign to</a>
                           </h4>
                           <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse show">
                           <div class="panel-body">
                              <div class="sub-headparent"><span class="sub-head">Anti Money Laundering Checks</span></div>
                              <div class="left-half">
                              <ul class="sortable">
                              <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check7"> -->
                                       <label for="p-check7">Client ID Verified</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check8"> -->
                                       <label for="p-check8">Type of ID Provided</label>
                                    </div>
                                   <!--  <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 
                                 </ul>
                              </div>
                              <!-- left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check9"> -->
                                       <label for="p-check9">Proof of Address</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check45"> -->
                                       <label for="p-check45">Meeting with the Client</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>

                              <div class="sub-headparent"><span class="sub-head">client source</span></div>

                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check10"> -->
                                       <label for="p-check10">Source</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check11"> -->
                                       <label for="p-check11">Refered by Existing Client</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>
                              <div class="left-half right-half">
                                 <ul class="sortable"> 
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check12"> -->
                                       <label for="p-check12"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','relationship_client');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Relationship to the Client';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="relationship_client">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div>

                               <div class="sub-headparent"><span class="sub-head">Notes if any other</span></div>

                              <div class="left-half">
                              <ul class="sortable"> 
                                 
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check46"> -->
                                       <label for="p-check46"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','assign_notes');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Notes';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="assign_notes">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>

                              </div>
                              <!-- right half-->
                              <!-- custom -->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <?php 
                                        $variable=$this->db->query('SELECT * FROM tblcustomfields WHERE fieldto="assign_to" AND active=1')->result_array();
                                        foreach ($variable as $key => $value) {
                                          
                                       ?>
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check37"> -->
                                       <label for="p-check37"><?php echo $value['name'];?></label>
                                    </div>
                                   <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <form class="validation edit_custom_field_form" method="post" action="" enctype="multipart/form-data">
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="<?php echo $value['fieldto'];?>">
            <input type="hidden" name="id" id="idto" value="<?php echo $value['id'];?>">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <?php } ?>
                                 </ul>
                              </div>
                              <!-- custom end-->
                           </div>
                           <!--add field-->

                           <div class="add-extrafield">
                              <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="assign_to"><i class="fa fa-plus"></i></span>
                           </div>

                           <!--add field-->
                        </div>
						</div>
                     </div>
                     <div class="panel panel-default msrItem accordion-panel">
					 <div class="border-firm-setting">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse3">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">other</a>
                           </h4>
                           <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse show">
                           <div class="panel-body">
                              <div class="sub-headparent"><span class="sub-head">other</span></div>
                              <div class="left-half">
                              <ul class="sortable">
                              <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check13"> -->
                                       <label for="p-check13">Previous Accounts</label>
                                    </div>
                                   <!--  <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check14"> -->
                                       <label for="p-check14"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','other_contact_no');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Contact Tel';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                        <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="other_contact_no">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check47">
                                       <label for="p-check47">Email Address</label>
                                    </div>
                                     <!--  <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>

                              <!-- left half-->
                               <div class="left-half right-half">
                                 <ul class="sortable">
                                     <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check48"> -->
                                       <label for="p-check48"> Chase for Information</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check49"> -->
                                       <label for="p-check49"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','other_notes');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Notes';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                         <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="other_notes">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                                 </div>

                                 <div class="sub-headparent"><span class="sub-head">invite client</span></div>
                                 <div class="left-half">
                                    <ul class="sortable">
                                    <li class="ui-state-default">
                                       <div class="sep-sec">
                                          <div class="left-field cus-checkbox1">
                                             <!-- <input type="checkbox" name="public" value="Public" id="p-check51"> -->
                                             <label for="p-check51">Invite to use our system</label>
                                          </div>
                                          <!-- <div class="right-field">
                                             <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                             <div class="edit-field hide">
                                                <div class="panel-heading">edit information</div>
                                                <div class="panel-body">
                                                   <label>field name</label>
                                                   <input type="text" name="cname">
                                                   <label>field name</label>
                                                   <select>
                                                      <option>text</option>
                                                      <option>number</option>
                                                   </select>
                                                   <div class="save-can">
                                                      <a href="javascript:;">save</a>
                                                      <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div> -->
                                       </div>
                                       </li>
                                       <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check15"> -->
                                       <label for="p-check15">crm</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                              </li>
                               <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check52"> -->
                                       <label for="p-check52">Proposal</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                              </li>
                               <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check53"> -->
                                       <label for="p-check53">Tasks</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                              </li>
                                    </ul>
                                 </div>
                              <div class="left-half right-half">
                              <ul class="sortable">
                              
                               <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check54"> -->
                                       <label for="p-check54"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','send_invit_link');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Send invitation link';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                         <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="send_invit_link">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                              </li>
                               <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check55"> -->
                                       <label for="p-check55">Username</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                              </li>
                               <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check56"> -->
                                       <label for="p-check56">Password</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                              </li>
                               <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check57"> -->
                                       <label for="p-check57"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','other_any_notes');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Notes';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                           <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="other_any_notes">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                              </li>
                              </ul>
                              </div>
                              <!-- right half-->
                              <!-- custom -->
                              <div class="left-half">
                                 <ul class="sortable">
                                    <?php 
                                        $variable=$this->db->query('SELECT * FROM tblcustomfields WHERE fieldto="other" AND active=1')->result_array();
                                        if(isset($variable)){
                                        foreach ($variable as $key => $value) {
                                          
                                       ?>
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check37"> -->
                                       <label for="p-check37"><?php echo $value['name'];?></label>
                                    </div>
                                   <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <form class="validation edit_custom_field_form" method="post" action="" enctype="multipart/form-data">
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="<?php echo $value['fieldto'];?>">
            <input type="hidden" name="id" id="idto" value="<?php echo $value['id'];?>">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <?php } } ?>
                                 </ul>
                              </div>
                              <!-- custom end-->
                           </div>
                           <!--add field-->

                           <div class="add-extrafield">
                              <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="other"><i class="fa fa-plus"></i></span>
                           </div>

                           <!--add field-->
                        </div>
						</div>
                     </div>
                     <div class="panel panel-default msrItem accordion-panel">
					 <div class="border-firm-setting">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse4">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">confirmation statement</a>
                           </h4>
                           <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse show">
                           <div class="panel-body">
                           <div class="sub-headparent"><span class="sub-head">Important Information</span></div>
                              <div class="left-half">
                              <ul class="sortable">
                              <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check16"> -->
                                       <label for="p-check16"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','confirmation_auth_code');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Companies Authentication Code';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="confirmation_auth_code">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check17"> -->
                                       <label for="p-check17"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','confirmation_officers');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Officers';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="confirmation_officers">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check18"> -->
                                       <label for="p-check18"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','share_capital');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Share Capital';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="share_capital">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check18"> -->
                                       <label for="p-check18"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','shareholders');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Share Holders';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="shareholders">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check18"> -->
                                       <label for="p-check18"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','people_with_significant_control');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'People with significant control';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="people_with_significant_control">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div>
                              <!-- left half-->

                              <div class="sub-headparent"><span class="sub-head">Confirmation Statement</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check19"> -->
                                       <label for="p-check19">Next Statement Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>
                              <div class="left-half right-half">
                                 <ul class="sortable"> 
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check20"> -->
                                       <label for="p-check20">Due By</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>
                                <div class="sub-headparent"><span class="sub-head">Reminders</span></div>
                                <div class="left-half">
                                    <ul class="sortable">
                                       <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check21"> -->
                                       <label for="p-check21">Next Reminder Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                              </li>
                              <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check22"> -->
                                       <label for="p-check22">Create Task For Reminders</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                    </ul>
                                 </div>
                              <div class="left-half right-half">
                              <ul class="sortable"> 
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check23"> -->
                                       <label for="p-check23">Add Custom Reminder</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>
                              <!-- right half-->

                              <div class="sub-headparent"><span class="sub-head">Reminders</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check24"> -->
                                       <label for="p-check24"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','confirmation_notes');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Notes';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="confirmation_notes">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div>

                              <!-- custom -->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <?php 
                                        $variable=$this->db->query('SELECT * FROM tblcustomfields WHERE fieldto="confirmation" AND active=1')->result_array();
                                        if(isset($variable)){
                                        foreach ($variable as $key => $value) {
                                          
                                       ?>
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check37"> -->
                                       <label for="p-check37"><?php echo $value['name'];?></label>
                                    </div>
                                   <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <form class="validation edit_custom_field_form" method="post" action="" enctype="multipart/form-data">
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="<?php echo $value['fieldto'];?>">
            <input type="hidden" name="id" id="idto" value="<?php echo $value['id'];?>">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <?php } } ?>
                                 </ul>
                              </div>
                              <!-- custom end-->

                              <!--add field-->

                           <div class="add-extrafield">
                              <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="confirmation"><i class="fa fa-plus"></i></span>
                           </div>

                           <!--add field-->
                           </div>
                        </div>
						</div>
                     </div>
                     <div class="panel panel-default msrItem accordion-panel">
					 <div class="border-firm-setting">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse5">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">accounts</a>
                           </h4>
                           <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse show">
                           <div class="panel-body">
                           <div class="sub-headparent"><span class="sub-head">Important Information</span></div>
                              <div class="left-half">
                              <ul class="sortable">
                              <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check25"> -->
                                       <label for="p-check25"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','accounts_auth_code');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:'Companies Authentication Code';?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="accounts_auth_code">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check26"> -->
                                       <label for="p-check26"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','accounts_utr_number');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Company's UTR Number";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                           <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="accounts_utr_number">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div>
                              <!-- left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check27"> -->
                                       <label for="p-check27">Companies House Reminders</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>

                              <div class="sub-headparent"><span class="sub-head">Accounts</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check28"> -->
                                       <label for="p-check28">Next accounts made up to</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check29"> -->
                                       <label for="p-check29">Accounts Due Date - Companies House</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>
                              <div class="sub-headparent"><span class="sub-head">Reminders</span></div>

                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check59"> -->
                                       <label for="p-check59">Next Reminder Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check60"> -->
                                       <label for="p-check60">Create Task For Reminders</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>

                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check61"> -->
                                       <label for="p-check61">Add Custom Reminder</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>

                              <div class="sub-headparent"><span class="sub-head">Tax Return</span></div>
                              <div class="left-half">
                                 <ul class="sortable"> 
                                   <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check62"> -->
                                       <label for="p-check62">account office reference</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>    
                                 </ul>
                              </div>
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                     <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check63"> -->
                                       <label for="p-check63">Tax Payment Date to HMRC</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>  
                                 </ul>
                              </div>
                              <div class="sub-headparent"><span class="sub-head">Reminders</span></div>

                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check64"> -->
                                       <label for="p-check64">Next Reminder Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check65"> -->
                                       <label for="p-check65">Create Task For Reminders</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>

                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check66"> -->
                                       <label for="p-check66">Add Custom Reminder</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div>

                              <div class="sub-headparent"><span class="sub-head">Notes if any other</span></div>

                              <div class="left-half">
                              <ul class="sortable">                             
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check67"> -->
                                       <label for="p-check67"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','accounts_notes');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Notes";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="accounts_notes">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div>
                              <!-- right half-->
                              <!-- custom -->
                              <div class="left-half">
                                 <ul class="sortable">
                                    <?php 
                                        $variable=$this->db->query('SELECT * FROM tblcustomfields WHERE fieldto="accounts" AND active=1')->result_array();
                                        if(isset($variable)){
                                        foreach ($variable as $key => $value) {
                                          
                                       ?>
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check37"> -->
                                       <label for="p-check37"><?php echo $value['name'];?></label>
                                    </div>
                                   <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <form class="validation edit_custom_field_form" method="post" action="" enctype="multipart/form-data">
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="<?php echo $value['fieldto'];?>">
            <input type="hidden" name="id" id="idto" value="<?php echo $value['id'];?>">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <?php } } ?>
                                 </ul>
                              </div>
                              <!-- custom end-->
                           </div>
                           <!--add field-->

                           <div class="add-extrafield">
                              <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="accounts"><i class="fa fa-plus"></i></span>
                           </div>

                           <!--add field-->
                        </div>
						</div>
                     </div> <!-- panel end -->

                     <div class="panel panel-default msrItem accordion-panel">
					 <div class="border-firm-setting">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse6">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">personal tax return</a>
                           </h4>
                           <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse show">
                           <div class="panel-body">
                           <div class="sub-headparent"><span class="sub-head">Important Information</span></div>
                              <div class="left-half">
                              <ul class="sortable">
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check68"> -->
                                       <label for="p-check68"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','personal_utr_number');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Personal UTR Number";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="personal_utr_number">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check69"> -->
                                       <label for="p-check69"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','ni_number');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"National Insurance Number";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="ni_number">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>   
                                 </ul>
                              </div><!-- left half--> 

                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check70"> -->
                                       <label for="p-check70">Property Income</label>
                                    </div>
                                   <!--  <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>  
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check71"> -->
                                       <label for="p-check71">Additional Income</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>  
                                 </ul>
                              </div><!--right half -->

                              <div class="sub-headparent"><span class="sub-head">other</span></div>
                              <div class="left-half">
                                 <ul class="sortable">                                    
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check31"> -->
                                       <label for="p-check31">Tax Return Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check72"> -->
                                       <label for="p-check72"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','personal_due_date_return');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Due Date on Paper Return";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="personal_due_date_return">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->

                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check73"> -->
                                       <label for="p-check73"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','personal_due_date_online');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Due Date online filing";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="personal_due_date_online">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> <!--right half-->
                              <div class="sub-headparent"><span class="sub-head">Reminders</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check74"> -->
                                       <label for="p-check74">Next Reminder Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check75"> -->
                                       <label for="p-check75">Create Task For Reminders</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                     <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check76"> -->
                                       <label for="p-check76">Add Custom Reminder</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->

                              <div class="sub-headparent"><span class="sub-head">Notes if any other</span></div>
                               <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check76"> -->
                                       <label for="p-check76"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','personal_notes');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Notes";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="personal_notes">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->

                              <!-- custom -->
                              <div class="left-half">
                                 <ul class="sortable">
                                    <?php 
                                        $variable=$this->db->query('SELECT * FROM tblcustomfields WHERE fieldto="personal_tax" AND active=1')->result_array();
                                        if(isset($variable)){
                                        foreach ($variable as $key => $value) {
                                          
                                       ?>
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check37"> -->
                                       <label for="p-check37"><?php echo $value['name'];?></label>
                                    </div>
                                   <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <form class="validation edit_custom_field_form" method="post" action="" enctype="multipart/form-data">
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="<?php echo $value['fieldto'];?>">
            <input type="hidden" name="id" id="idto" value="<?php echo $value['id'];?>">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <?php } } ?>
                                 </ul>
                              </div>
                              <!-- custom end-->

                           </div>
                           <!--add field-->

                           <div class="add-extrafield">
                              <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="personal_tax"><i class="fa fa-plus"></i></span>
                           </div>

                           <!--add field-->
                        </div>
						</div>
                     </div> <!-- panel end -->

                     <div class="panel panel-default msrItem accordion-panel">
					 <div class="border-firm-setting">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse7">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">payroll</a>
                           </h4>
                           <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse show">
                           <div class="panel-body">
                           <div class="sub-headparent"><span class="sub-head">Important Information</span></div>
                           <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check77"> -->
                                       <label for="p-check77"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','payroll_acco_off_ref_no');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Accounts Office Reference Number";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="payroll_acco_off_ref_no">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                           </div><!--left half-->
                           <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check78"> -->
                                       <label for="p-check78"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','paye_off_ref_no');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"PAYE Office Ref Number";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="paye_off_ref_no">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->
                              <div class="sub-headparent"><span class="sub-head">Payroll</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check79"> -->
                                       <label for="p-check79">Payroll Registration Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check80"> -->
                                       <label for="p-check80"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','no_of_employees');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Number of Employees";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="no_of_employees">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check81"> -->
                                       <label for="p-check81">First Pay Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check82"> -->
                                       <label for="p-check82">Payroll Run</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check83"> -->
                                       <label for="p-check83">Payroll Run Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check84"> -->
                                       <label for="p-check84">RTI Due/Deadline Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check85"> -->
                                       <label for="p-check85">Previous Year Requires</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check86"> -->
                                       <label for="p-check86">PAYE Scheme Ceased</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->
                              <div class="sub-headparent"><span class="sub-head">Reminders</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check87"> -->
                                       <label for="p-check87">Next Reminder Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check88"> -->
                                       <label for="p-check88">Create Task For Reminders</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                     <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check89"> -->
                                       <label for="p-check89">Add Custom Reminder</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->
                              <div class="sub-headparent"><span class="sub-head">WorkPlace Pension - AE</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                     <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check90"> -->
                                       <label for="p-check90">Staging Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                  <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check91"> -->
                                       <label for="p-check91"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','pension_id');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Pension ID";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="pension_id">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                  <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check92"> -->
                                       <label for="p-check92">Pension Submission Due date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                  <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check93"> -->
                                       <label for="p-check93">Defer/Postpone Upto</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                  <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check94"> -->
                                       <label for="p-check94">The Pensions Regulator Opt Out Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                  <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check95"> -->
                                       <label for="p-check95">Re-Enrolment Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                  <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check96"> -->
                                       <label for="p-check96">Declaration of Compliance Due</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check97"> -->
                                       <label for="p-check97">Declaration of Compliance last filed</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check98"> -->
                                       <label for="p-check98"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','paye_pension_provider');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Pension Provider Name";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="paye_pension_provider">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                <!--<li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check99">
                                       <label for="p-check99">Pension Provider User ID</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                 <option value="checkbox">Checkbox</option>
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="paye_pension_provider_userid">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li> -->
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check100">
                                       <label for="p-check100">Pension Provider Password</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check101"> -->
                                       <label for="p-check101"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','employer_contri_percentage');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Employer Contribution Percentage";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="employer_contri_percentage">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check102"> -->
                                       <label for="p-check102"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','employee_contri_percentage');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Employee Contribution Percentage";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="employee_contri_percentage">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check103">
                                       <label for="p-check103"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','pension_notes');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Other Notes";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="pension_notes">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->
                              <div class="sub-headparent"><span class="sub-head">Reminders</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check104"> -->
                                       <label for="p-check104">Next Reminder Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check105"> -->
                                       <label for="p-check105">Create Task For Reminders</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                     <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check106"> -->
                                       <label for="p-check106">Add Custom Reminder</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->
                              <div class="sub-headparent"><span class="sub-head">P11D</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check107"> -->
                                       <label for="p-check107">P11D Start Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check108"> -->
                                       <label for="p-check108"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','p11d_todo');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Number of P11D's to do";?></label>
                                    </div>
                                  <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="p11d_todo">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div> 
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check109"> -->
                                       <label for="p-check109">First Benefit Pay Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check110"> -->
                                       <label for="p-check110">P11D Due date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check111"> -->
                                       <label for="p-check111">Previous Year Requires</label>
                                    </div>
                                   <!--  <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check112"> -->
                                       <label for="p-check112"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','p11d_payroll_if_yes');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"If Yes";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                         <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="p11d_payroll_if_yes">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check113"> -->
                                       <label for="p-check113">PAYE Scheme Ceased Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->
                              <!-- <div class="sub-headparent"><span class="sub-head">Reminders</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check104">
                                       <label for="p-check104">Next Reminder Date</label>
                                    </div>
                                     <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> 
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check105">
                                       <label for="p-check105">Create Task For Reminders</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> --><!--left half-->
                              <!-- <div class="left-half right-half">
                                 <ul class="sortable">
                                     <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check106">
                                       <label for="p-check106">Add Custom Reminder</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> --><!--right half-->
                              <div class="sub-headparent"><span class="sub-head">Notes if any other</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check106"> -->
                                       <label for="p-check106"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','p11d_notes');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Notes";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="p11d_notes">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <!-- custom -->
                              <div class="left-half">
                                 <ul class="sortable">
                                    <?php 
                                        $variable=$this->db->query('SELECT * FROM tblcustomfields WHERE fieldto="payroll" AND active=1')->result_array();
                                        if(isset($variable)){
                                        foreach ($variable as $key => $value) {
                                          
                                       ?>
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check37"> -->
                                       <label for="p-check37"><?php echo $value['name'];?></label>
                                    </div>
                                   <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <form class="validation edit_custom_field_form" method="post" action="" enctype="multipart/form-data">
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="<?php echo $value['fieldto'];?>">
            <input type="hidden" name="id" id="idto" value="<?php echo $value['id'];?>">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <?php } } ?>
                                 </ul>
                              </div>
                              <!-- custom end-->
                           </div>
                           <div class="add-extrafield">
                              <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="payroll"><i class="fa fa-plus"></i></span>
                           </div>
						   </div>
                        </div>
                     </div> <!-- panel end -->

                     <div class="panel panel-default msrItem accordion-panel">
					 <div class="border-firm-setting">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse7">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">VAT returns</a>
                           </h4>
                           <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                        </div>
                        <div id="collapse7" class="panel-collapse collapse show">
                           <div class="panel-body">
                           <div class="sub-headparent"><span class="sub-head">Important Information</span></div>
                           <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check107"> -->
                                       <label for="p-check107"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','vat_number_one');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"VAT Number";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="vat_number_one">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <div class="sub-headparent"><span class="sub-head">VAT</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check108"> -->
                                       <label for="p-check108">VAT Registration Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check109"> -->
                                       <label for="p-check109">VAT Frequency</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <!-- <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check110">
                                       <label for="p-check110">VAT Quarter End Date</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check111">
                                       <label for="p-check111">VAT Quarters</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check112">
                                       <label for="p-check112">VAT Due Date</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check113">
                                       <label for="p-check113">VAT Scheme</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li> -->
                                 </ul>
                              </div><!--left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check114"> -->
                                       <label for="p-check114"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','flat_rate_category');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Flat Rate Category";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                         <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="flat_rate_category">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check115"> -->
                                       <label for="p-check115"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','flat_rate_percentage');
                                       echo (isset($business[0]['name'])&&($business[0]['name']!=''))? $business[0]['name']:"Flat Rate Percentage";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="flat_rate_percentage">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check116"> -->
                                       <label for="p-check116">Direct Debit with HMRC</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check117"> -->
                                       <label for="p-check117">Annual Accounting Scheme</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check118"> -->
                                       <label for="p-check118">Box 5 Figure of Last Quarter</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check119"> -->
                                       <label for="p-check119"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','vat_address');
                                       echo (isset($business)&&($business[0]['name']!=''))? $business[0]['name']:"VAT Address";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="vat_address">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->
                              <!-- custom -->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <?php 
                                        $variable=$this->db->query('SELECT * FROM tblcustomfields WHERE fieldto="vat" AND active=1')->result_array();
                                        if(isset($variable)){
                                        foreach ($variable as $key => $value) {
                                          
                                       ?>
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check37"> -->
                                       <label for="p-check37"><?php echo $value['name'];?></label>
                                    </div>
                                   <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <form class="validation edit_custom_field_form" method="post" action="" enctype="multipart/form-data">
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="<?php echo $value['fieldto'];?>">
            <input type="hidden" name="id" id="idto" value="<?php echo $value['id'];?>">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <?php } } ?>
                                 </ul>
                              </div>
                              <!-- custom end-->
                           </div>
                           <div class="add-extrafield">
                              <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="vat"><i class="fa fa-plus"></i></span>
                           </div>
                        </div>
						</div>
                     </div> <!-- panel end -->

                      <div class="panel panel-default msrItem accordion-panel">
					  <div class="border-firm-setting">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse8">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">management account</a>
                           </h4>
                           <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                        </div>
                        <div id="collapse8" class="panel-collapse collapse show">
                           <div class="panel-body">
                           <div class="sub-headparent"><span class="sub-head">Bookkeeping</span></div>
                           <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check120"> -->
                                       <label for="p-check120">Bookkeepeing to Done</label>
                                    </div>
                                   <!--  <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check121"> -->
                                       <label for="p-check121">Next Bookkeeping Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                           </div><!--left half-->
                           <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check122"> -->
                                       <label for="p-check122">Method of Bookkeeping</label>
                                    </div>
                                    <div class="right-field">
                                    <!--    <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check123"> -->
                                       <label for="p-check123">How Client will provide records</label>
                                    </div>
                                   <!--  <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->
                              <!-- <div class="sub-headparent"><span class="sub-head">Reminders</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check124">
                                       <label for="p-check124">Next Reminder Date</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check125">
                                       <label for="p-check125">Create Task For Reminders</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> --><!--left half-->
                             <!--  <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check126">
                                       <label for="p-check126">Add Custom Reminder</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> --><!--right half-->
                              <div class="sub-headparent"><span class="sub-head">Management Accounts</span>
                              </div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check127"> -->
                                       <label for="p-check127">Management Accounts Frequency</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check128"> -->
                                       <label for="p-check128">Next Management Accounts Due date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check129"> -->
                                       <label for="p-check129">Method of Bookkeeping</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check130"> -->
                                       <label for="p-check130">How Client will provide records</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->
                              <!-- <div class="sub-headparent"><span class="sub-head">Reminders</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check131">
                                       <label for="p-check131">Next Reminder Date</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check132">
                                       <label for="p-check132">Create Task For Reminders</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> --><!--left half-->
                              <!-- <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check133">
                                       <label for="p-check133">Add Custom Reminder</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> --><!--right half-->
                              <div class="sub-headparent"><span class="sub-head">Notes if any other</span>
                              </div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check134"> -->
                                       <label for="p-check134"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','manage_notes');
                                       echo (isset($business)&&($business[0]['name']!=''))? $business[0]['name']:"Notes";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="manage_notes">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <!-- custom -->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <?php 
                                        $variable=$this->db->query('SELECT * FROM tblcustomfields WHERE fieldto="management" AND active=1')->result_array();
                                        if(isset($variable)){
                                        foreach ($variable as $key => $value) {
                                          
                                       ?>
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check37"> -->
                                       <label for="p-check37"><?php echo $value['name'];?></label>
                                    </div>
                                   <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <form class="validation edit_custom_field_form" method="post" action="" enctype="multipart/form-data">
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="<?php echo $value['fieldto'];?>">
            <input type="hidden" name="id" id="idto" value="<?php echo $value['id'];?>">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <?php } } ?>
                                 </ul>
                              </div>
                              <!-- custom end-->
                           </div>
                           <div class="add-extrafield">
                              <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="management"><i class="fa fa-plus"></i></span>
                           </div>
						   </div>
                        </div>
                     </div> <!-- panel end -->

                     <div class="panel panel-default msrItem accordion-panel">
					 <div class="border-firm-setting">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse9">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">Investigation Insurance</a>
                           </h4>
                           <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                        </div>
                        <div id="collapse9" class="panel-collapse collapse show">
                           <div class="panel-body">
                           <div class="sub-headparent"><span class="sub-head">Investigation Insurance</span></div>
                           <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check135"> -->
                                       <label for="p-check135">Insurance Start Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check136"> -->
                                       <label for="p-check136">Insurance Renew Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check137"> -->
                                       <label for="p-check137">Insurance Provider</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check138"> -->
                                       <label for="p-check138"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','claims_note');
                                       echo (isset($business)&&($business[0]['name']!=''))? $business[0]['name']:"History of Previous Claims";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="claims_note">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->
                              <!-- <div class="sub-headparent"><span class="sub-head">Reminders</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check139">
                                       <label for="p-check139">Next Reminder Date</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check140">
                                       <label for="p-check140">Create Task For Reminders</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> --><!--left half-->
                              <!-- <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check141">
                                       <label for="p-check141">Add Custom Reminder</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> --><!--right half-->
                              <div class="sub-headparent"><span class="sub-head">Registered Office</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                     <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check142"> -->
                                       <label for="p-check142">Registered Office Start Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                  <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check143"> -->
                                       <label for="p-check143">Registered Office Renew Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check144"> -->
                                       <label for="p-check144"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','registered_office_inuse');
                                       echo (isset($business)&&($business[0]['name']!=''))? $business[0]['name']:"Registered office in Use";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="registered_office_inuse">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check145"> -->
                                       <label for="p-check145"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','registered_claims_note');
                                       echo (isset($business)&&($business[0]['name']!=''))? $business[0]['name']:"History of Previous Claims";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="registered_claims_note">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->
                              <!-- <div class="sub-headparent"><span class="sub-head">Reminders</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check146">
                                       <label for="p-check146">Next Reminder Date</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check147">
                                       <label for="p-check147">Create Task For Reminders</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> --><!--left half-->
                              <!-- <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check148">
                                       <label for="p-check148">Add Custom Reminder</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> --><!--right half-->
                              <div class="sub-headparent"><span class="sub-head">Tax Advice/Investigation</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check149"> -->
                                       <label for="p-check149">Start Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check150"> -->
                                       <label for="p-check150">End Date</label>
                                    </div>
                                    <!-- <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check151"> -->
                                       <label for="p-check151"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','investigation_note');
                                       echo (isset($business)&&($business[0]['name']!=''))? $business[0]['name']:"Notes";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="investigation_note">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--right half-->
                               <!-- <div class="sub-headparent"><span class="sub-head">Reminders</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check152">
                                       <label for="p-check152">Next Reminder Date</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check153">
                                       <label for="p-check153">Create Task For Reminders</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> --><!--left half-->
                              <!-- <div class="left-half right-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <input type="checkbox" name="public" value="Public" id="p-check154">
                                       <label for="p-check154">Add Custom Reminder</label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="cname">
                                             <label>field name</label>
                                             <select>
                                                <option>text</option>
                                                <option>number</option>
                                             </select>
                                             <div class="save-can">
                                                <a href="javascript:;">save</a>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div> --><!--right half-->
                              <div class="sub-headparent"><span class="sub-head">Notes if any other</span></div>
                              <div class="left-half">
                                 <ul class="sortable">
                                    <li class="ui-state-default">
                                    <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                       <!-- <input type="checkbox" name="public" value="Public" id="p-check155"> -->
                                       <label for="p-check155"><?php 
                                        $business=$this->Common_mdl->GetAllWithWhere('tblcustomfields','fieldto','tax_investigation_note');
                                       echo (isset($business)&&($business[0]['name']!=''))? $business[0]['name']:"Notes";?></label>
                                    </div>
                                    <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <div class="alert alert-success" style="display:none;"
                                          ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Added.</div>
                                          <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>New Field Not Added.</div>
                                          <form class="validation edit_field_form" method="post" action="" enctype="multipart/form-data"> 
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field Type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="tax_investigation_note">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 </ul>
                              </div><!--left half-->
                               <!-- custom -->
                              <div class="left-half right-half">
                                 <ul class="sortable">
                                    <?php 
                                        $variable=$this->db->query('SELECT * FROM tblcustomfields WHERE fieldto="investigation" AND active=1')->result_array();
                                        if(isset($variable)){
                                        foreach ($variable as $key => $value) {
                                          
                                       ?>
                                    <li class="ui-state-default">
                                 <div class="sep-sec">
                                    <div class="left-field cus-checkbox1">
                                      <!--  <input type="checkbox" name="public" value="Public" id="p-check37"> -->
                                       <label for="p-check37"><?php echo $value['name'];?></label>
                                    </div>
                                   <div class="right-field">
                                       <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                       <div class="edit-field hide">
                                          <div class="panel-heading">edit information</div>
                                          <form class="validation edit_custom_field_form" method="post" action="" enctype="multipart/form-data">
                                          <div class="panel-body">
                                             <label>field name</label>
                                             <input type="text" name="name">
                                             <span class="error" style="display:none;">Enter a name</span>
                                             <label>field type</label>
                                             <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
                            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            <input type="hidden" name="fieldto" id="editfieldto" value="<?php echo $value['fieldto'];?>">
            <input type="hidden" name="id" id="idto" value="<?php echo $value['id'];?>">
                                             <div class="save-can">
                                               <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                <a href="javascript:;" class="cancel-btns">cancel</a>
                                             </div>
                                          </div>
                                       </form>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <?php } } ?>
                                 </ul>
                              </div>
                              <!-- custom end-->

                           </div>
                           <div class="add-extrafield">
                              <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="investigation"><i class="fa fa-plus"></i></span>
                           </div>
                        </div>
						</div>
                     </div> <!-- panel end -->

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- management block -->
<?php //$this->load->view('users/column_setting_view');?>
<?php //$this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>


  <div class="modal-alertsuccess alert alert-success" style="display:none;"
            ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			
			 <div class="pop-realted1">
    <div class="position-alert1">
	New Field Added.</div></div></div>
			
            <div class="modal-alertsuccess alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>  <div class="pop-realted1">
    <div class="position-alert1"> New Field Not Added. </div> </div> </div>
	
 <div class="modal fade" id="new-field" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Field</h4>
        </div>

      
			
        <form id="field_form" class="validation" method="post" action="" enctype="multipart/form-data"> 
           <div class="modal-body">
               <div class="field-name1 form-group">
               <label>field name</label>
               <input type="text" id="name" name="name">
            </div>
            <div class="field-type1 ">
               <label>field type</label>
              <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>
                                <!-- <option value="checkbox">Checkbox</option> -->
                                <option value="date_picker">Date Picker</option>
                            </select>
            
            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            </div>
            <input type="hidden" name="fieldto" id="fieldto" value="">

            <div class="modal-save">
              <input type="submit" class="add_new_field" value="Save" id="submit"/>
            </div>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
     </form>
      </div>
    </div>
  </div>

<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script>
   $(document).ready(function() {
   
     $("i.fa.fa-pencil-square").on('click',function() {

       $('.edit-field').removeClass('foo');

       $(this).next('.edit-field').addClass('foo')
       
       $('.edit-field:not(.foo)').removeClass('show');

       $(this).next('.edit-field').toggleClass('show');
       //$(this).next('.edit-field').toggleClass('show');
     });

     $('.cancel-btns').on('click',function(){

      $('.edit-field').removeClass('show');

     });

     // $('.sep-sec').draggable();

     $( ".sortable" ).sortable(
     {
         tolerance: 'pointer',
        cursor: 'move',
        forcePlaceholderSize: true,
        dropOnEmpty: true,
        connectWith: '.sortable',
        create:function(){
          $(this).height($(this).height());
         },

         stop: function(event, ui) {
        alert("New position: " + ui.item.index());
    }


         }).disableSelection();

     // $('.sortable').draggable();
    // $( ".sortable" ).disableSelection();
   
   
   
   $(".ex-field").on('click',function() { // bulk checked
        var fields = $(this).data('field');

       $('#fieldto').val(fields);

      });

$(".selectpicker").change(function(){
//alert($(this).val());
var rec = $(this).val();
if(rec=='select' || rec=='checkbox' )
{
  
   $(this).next().show();
    //$('#options_wrapper').show();
}
else{
    $(this).next().hide();
        //$('#options_wrapper').hide();
}
}); 

  
   
   //$(".status").change(function(e){
   $('#alluser').on('change','.status',function () {
   //e.preventDefault();
   
   
    var rec_id = $(this).data('id');
    var stat = $(this).val();
   
   $.ajax({
         url: '<?php echo base_url();?>user/statusChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         timeout: 3000,
         success: function( data ){
          //alert('ggg');
             $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });
             setTimeout(resetAll,3000);
             if(stat=='3'){
              
               //$this.closest('td').next('td').html('Active');
               $('#frozen'+rec_id).html('Frozen');
   
             } else {
               $this.closest('td').next('td').html('Inactive');
             }
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
    });
   
   // payment/non payment status
   $('.suspent').click(function() {
     if($(this).is(':checked'))
         var stat = '1';
     else
         var stat = '0';
     var rec_id = $(this).val();
     var $this = $(this);
      $.ajax({
         url: '<?php echo base_url();?>user/suspentChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         success: function( data ){
          //alert('ggg');
             $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
             if(stat=='1'){
              
               $this.closest('td').next('td').html('Payment');
   
             } else {
               $this.closest('td').next('td').html('Non payment');
             }
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
   });
   });
   
</script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$("#field_form").validate({
  
       ignore: false,

      
                        rules: {
                        name: {required: true},
                        
                        },
                        errorElement: "span" , 
                        errorClass: "field-error",                             
                         messages: {
                          name: "Enter a name",
                         },
                      
                        submitHandler: function(form) {
                            var formData = new FormData($("#field_form")[0]);
                            
                            $(".LoadingImage").show();

                            $.ajax({
                                url: '<?php echo base_url();?>user/field_insert/',
                                dataType : 'json',
                                type : 'POST',
                                data : formData,
                                contentType : false,
                                processData : false,
                                success: function(data) {
                                    console.log(data);
                                    if(data == 1){
                                       
                                       // $('#new_user')[0].reset();
                                        $('.alert-success').show();
                                        $('.alert-danger').hide();
                                      // location.reload(); 
                                    }
                                    else{
                                       // alert('failed');
                                        $('.alert-danger').show();
                                        $('.alert-success').hide();
                                    }
                                $(".LoadingImage").hide();
                                },
                                error: function() { $('.alert-danger').show();
                                        $('.alert-success').hide();}
                            });

                            return false;
                        } ,
                         invalidHandler: function(e, validator) {
           if(validator.errorList.length)
        $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')

        }
                         
                    });


});
</script>
<script type="text/javascript">
$(document).ready(function(){

$(".edit_field_form").submit(function () {

      var clikedForm = $(this); // Select Form

      if (clikedForm.find("[name='name']").val() == '') {
         //$('.error').show();
         $(this).find('.error').show();
        //alert('Enter Valid name');
        return false;
      } else {
            $('.error').hide();
         $.ajax({
         type: "POST",
         url: '<?php echo base_url();?>user/edit_field_insert/',
         data: $(this).serialize(),
         success: function (data) { 
            // Inserting html into the result div
            //console.log(data);
                                    if(data == 1){
                                       
                                       // $('#new_user')[0].reset();
                                        $('.alert-success').show();
                                        $('.alert-danger').hide();
                                       location.reload(); 
                                    }
                                    else{
                                       // alert('failed');
                                        $('.alert-danger').show();
                                        $('.alert-success').hide();
                                    }
         },
         error: function(jqXHR, text, error){
            // Displaying if there are any errors
               $('#result').html(error);           
        }
    });
      return false;
      }
     
     

    });
   
$(".edit_custom_field_form").submit(function () {

      var clikedForm = $(this); // Select Form

      if (clikedForm.find("[name='name']").val() == '') {
         //$('.error').show();
         $(this).find('.error').show();
        //alert('Enter Valid name');
        return false;
      } else {
            $('.error').hide();
         $.ajax({
         type: "POST",
         url: '<?php echo base_url();?>user/edit_custom_field_insert/',
         data: $(this).serialize(),
         success: function (data) { 
            // Inserting html into the result div
            //console.log(data);
                                    if(data == 1){
                                       
                                       // $('#new_user')[0].reset();
                                        $('.alert-success').show();
                                        $('.alert-danger').hide();
                                       location.reload(); 
                                    }
                                    else{
                                       // alert('failed');
                                        $('.alert-danger').show();
                                        $('.alert-success').hide();
                                    }
         },
         error: function(jqXHR, text, error){
            // Displaying if there are any errors
               $('#result').html(error);           
        }
    });
      return false;
      }
     
    

    });

/*$(".edit_field_form").validate({
  
       ignore: false,

      
                        rules: {
                        name: {required: true},
                        
                        },
                        errorElement: "span" , 
                        errorClass: "field-error",                             
                         messages: {
                          name: "Enter a name",
                         },
                      
                        submitHandler: function(form) {
                            var formData = new FormData($(".edit_field_form")[0]);
                            
                            $(".LoadingImage").show();

                            $.ajax({
                                url: '<?php echo base_url();?>user/edit_field_insert/',
                                dataType : 'json',
                                type : 'POST',
                                data : formData,
                                contentType : false,
                                processData : false,
                                success: function(data) {
                                    console.log(data);
                                    if(data == 1){
                                       
                                       // $('#new_user')[0].reset();
                                        $('.alert-success').show();
                                        $('.alert-danger').hide();
                                        
                                    }
                                    else{
                                       // alert('failed');
                                        $('.alert-danger').show();
                                        $('.alert-success').hide();
                                    }
                                $(".LoadingImage").hide();
                                },
                                error: function() { $('.alert-danger').show();
                                        $('.alert-success').hide();}
                            });

                            return false;
                        } ,
                         invalidHandler: function(e, validator) {
           if(validator.errorList.length)
        $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')

        }
                         
                    });*/


});
</script>
<script type="text/javascript">
   $(document).ready(function(){
     $('#column_setting').on('submit', function (e) {
   
             e.preventDefault();
    $(".LoadingImage").show();
             $.ajax({
               type: 'post',
               url: '<?php echo base_url()."User/column_update";?>',
               data: $('form').serialize(),
               success: function (data) {
                   if(data == '1'){
                   // $('#new_user')[0].reset();
                   $('.alert-success').show();
                   $('.alert-danger').hide();
                   location.reload();
                  // $('.all-usera1').load('<?php echo base_url()?>user .all-usera1');
                   
                 
                   }
                   else if(data == '0'){
                   // alert('failed');
                   $('.alert-danger').show();
                   $('.alert-success').hide();
                   }
                   $(".LoadingImage").hide();
               }
             });
   
           });
   
   
   
   
   });
</script>
<!-- 
<script src="<?php echo base_url()?>assets/js/jquery.indyMasonry.js"  type="text/javascript"></script>	

<script>
		var time = undefined;
		$(function(){
			//init
			

			//update columns size on window resize
			$( window ).on('resize', function(e) {
				clearTimeout(time);
				time = setTimeout(function(){
					$('.indy-masonry-container').msrItems('refresh');
				}, 200);
			})
			
			if ($(window).width() < 1599) {
				$('.indy-masonry-container').msrItems({
				'colums': 2, //columns number
				'margin': 15 //right and bottom margin
			});
			}			
			
			else {
				$('.indy-masonry-container').msrItems({
				'colums': 3, //columns number
				'margin': 15 //right and bottom margin
			});
			}
			
			
			
			 
			 
		});
	</script>
	
	-->