<!-- management block -->        

           <div class="deadline-crm1 floating_set">
           <!--  <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard"><h4>reminders setting</h4></ul> -->

           <div class="f-right toggle-adds1">
                 <span class="due-date"><a href="javascript:;" data-toggle="modal" data-target="#add-reminder" class="btn-add-task add_reminder" data-service="<?php echo $service_id;?>">
                  <i class="fa fa-plus"></i> add reminder</a></span>
           </div>
           </div>

          <div class="reminder-setting floating_set" style="margin-top: 5%;">         

            <div class="email-sec floating_set">
              <?php 

              $exp_contect=explode(',', $email_temp[0]['placeholder']);                            

              if(isset($reminder_setting)){
              foreach ($reminder_setting as $key => $value) {
                $content ='';
                foreach ($exp_contect as $key=>$val)
                {               
                  $content.='<span target="message_'.$value["id"].'" class="add_merge">'.$val.'</span><br>';
                } 

                if($value['due']=='due_by'){
                $due='due in';
                }elseif($value['due']=='overdue_by' ){
                $due='overdue';
                }
                ?>
              <div class="email-innersec"> 
                <div class="email-sec1">                  
                  <span class="due-date"><?php echo $due.' '.$value['days'].' '?>day</span>
                      <a href="javascript:;" class="date-editop reminder_popup" data-toggle="modal" data-target="<?php echo "#edit-reminder_".$value['id']; ?>" data-due="<?php if($value['due']=='due_by'){  echo 'due in'; }elseif($value['due']=='overdue_by' ){   echo 'overdue'; } ?>" data-service="<?php echo $value['service_id'];?>" data-id="<?php echo $value['id']; ?>" data-duevalue="<?php echo $value['due']; ?>" data-duedays="<?php echo $value['days'];?>" data-subject="<?php echo $value['subject'];?>" data-message="<?php echo $value['message'];?>"><i class="fa fa-edit"></i> edit</a>
                  <div class="position-es">
                      <i class="fa fa-envelope"></i>
                    </div>
                </div>
              </div>

                <div class="modal fade data-target-reminder edit-reminders addcustom-reminder" id="<?php echo "edit-reminder_".$value['id']; ?>" role="dialog">
                <div class="modal-dialog modal-md">    
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close popup_close" id="<?php echo "close_".$value['id']; ?>" data-dismiss="modal">x</button>
                      <h4 class="modal-title">edit reminder</h4>
                    </div>
                    <form action="<?php echo base_url().'User/edit_reminder'; ?>" method="post" accept-charset="utf-8" id="edit_form" novalidate="novalidate">
                    <input type="hidden" name="id" id="edit_id" value="<?php echo $value['id']; ?>">
                    <div class="modal-body">
                     <div class="modal-topsec">
                        <div class="send-invoice">
                          <div class="send-label">
                          <span class="invoice-notify">send</span>
                              <select name="due" id="edit_due">
                              <?php 

                              $due = "";
                              $due1 = "";

                              if($value['due']=='due_by'){ $due = "selected"; }
                              else{ $due1 = "selected"; }

                              ?>
                              <option value="due_by" <?php echo $due; ?>>due by</option>
                              <option value="overdue_by" <?php echo $due1; ?>>overdue by</option>
                              </select>
                        </div>
                        <div class="send-label">
                          <span class="days1">days</span>
                          <input type="text" class="due-days decimal decimal_days" data-validation="required,number" name="days" id="edit_days" value="<?php echo $value['days'];?>">
                        </div>
                        <div class="send-label">
                        <span class="invoice-notify">Template</span>
                        <select name="<?php echo "template_".$value['id'];?>" id="<?php echo "template_".$value['id'];?>" onchange="getTemplates(this);">
                          <option value="">Select</option>
                          <?php foreach($reminder_templates as $key => $value1){ ?>              
                          <option value="<?php echo $value1['id']; ?>"><?php echo ucwords($value1['title']); ?></option>            
                          <?php } ?>
                        </select>
                        </div>
                        </div>
                        <div class="insert-placeholder">                          
                          <?php echo $content; ?>
                        </div>
                     </div><!--modal-topsec-->
                     <div class="modal-bottomsec">
                       <input type="text" class="invoice-msg" name="subject" id="<?php echo "edit_subject_".$value['id'];?>" placeholder="Subject" value="<?php echo $value['subject'];?>" style="width: 100%;">
                        <button type="button" class="btn-success reminder-save pull-right eclear" style="margin-top: 5px;" id="<?php echo "clear_".$value['id']; ?>">Clear</button>
                        <textarea name="<?php echo "message_".$value['id']; ?>" data-validation="required" id="<?php echo "editor1_".$value['id']; ?>" ><?php echo $value['message'];?></textarea>
                        <input type="hidden" name="service_id" id="service_id" value="<?php echo $value['service_id'];?>">
                     </div>
                    </div>
                    <div class="modal-footer text-right">
                      <?php if($value['firm_id'] == $_SESSION['firm_id']){ ?> 
                      <button type="button" data-toggle="modal" data-target="<?php echo "#delete_reminder_".$value['id']; ?>" class="del-tsk12 for_user_permission_delete">Delete</button>
                      <?php } ?>
                      <button type="submit" class="reminder-save">save</button>
                    </div>
                    </form>
                  </div>      
                </div>
              </div>

               <div class="modal fade" id="<?php echo "delete_reminder_".$value['id']; ?>" role="dialog">
                 <div class="modal-dialog">    
                   <!-- Modal content-->
                   <div class="modal-content">
                     <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                       <h4 class="modal-title">Confirmation</h4>
                     </div>
                     <div class="modal-body">
                     <input  type="hidden" name="delete_task_id" id="delete_task_id" value="">
                       <p> Are you sure want to delete ?</p>
                     </div>
                     <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal" onclick="delete_reminder('<?php echo $value['id']; ?>');">Yes</button>
                       <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                     </div>
                   </div>
                 </div>
              </div>

              <?php } } ?>
            </div>
          </div>         
     
<!-- management block -->
<script src="https://cdn.ckeditor.com/4.9.0/standard-all/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
  $.validate({
    lang: 'en'
  });
</script>

<script type="text/javascript">
  
  $(document).on('click','.eclear',function() 
  {            
      var clear_id = $(this).attr('id'); 
      var len = clear_id.length; 
      var num = clear_id.substr(6,len); 

      var id = $('#'+clear_id).next('[name=message_'+num+']').attr('id'); 
      $('#'+id).val('');       
  });

 function delete_reminder(record_id)
 {
    var service_id = $('#firm_settings').val(); 

    $.ajax(
    {
       url: '<?php echo base_url(); ?>User/delete_reminder',
       type: 'POST',
       data: {'record_id':record_id},

       beforeSend: function() 
       {
          $('#close_'+record_id).trigger('click'); 
          $(".LoadingImage").show();
       },
       success:function(status)
       { 
          $(".LoadingImage").hide();
          
          if(status == 1)
          {           
             $('.popup_info_msg').show();
             $('.position-alert1').html("Reminder Deleted Successfully!");   
             window.location.href = '<?php echo base_url().'user/Service_reminder_settings/'; ?>'+service_id;         
          }
       }

    });
 }

 function getTemplates(obj)
 {        
    var rid = obj.id;
    rid = rid.replace('template_',''); 
    rid = rid.trim();

    var tid = obj.value;
    tid = tid.trim();
  
    $.ajax(
    {
       url:'<?php echo base_url().'User/get_template'; ?>',
       type:'POST',
       data:{'id':tid},

       success:function(template)
       { 
          var temp = JSON.parse(template);
          
          $('#edit_subject_'+rid).val(temp.subject);
          $('#clear_'+rid).next('[name=message_'+rid+']').val(temp.body);
       }

    }); 

 }

</script>