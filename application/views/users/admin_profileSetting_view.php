<?php $this->load->view('includes/header');?>
<style type="text/css">
   .firm_contact_country_code_select_div
   {
      width: 114px;
      display: inline-block;
   }
  input#company_contact_no
   {
      width: calc(100% - 123px);
      margin-left: 5px;
   }
   .firm_contact_group span.field-error
   {
      display: inline-block !important;
   }
   @media(min-width:1600px){
    .service_view01 .row.form-group.new-append1{
      padding:0;
      margin-left:0;
      margin-right:0;
      width:100%;
    }
    #admin_setting .form-group.row.name_fields{
      width: 23%;
    }
  }

  /*for color picker*/
  .sp-dd
  {
    background: url('<?php echo base_url()?>assets/images/new_update1.png');
    background-repeat: no-repeat;
    width: 10px;
    margin: 10px 0px 0px 5px;
  } 
  /*for color picker*/
    
    /*for temporary*/
    /*.cke_reset_all,.cke_reset_all *, .cke_reset_all a,.cke_reset_all textarea {
    display: inherit;
    vertical-align: top;
    }*/
    .show_hide_password
    {
          position: absolute;
          padding: 8px;
          min-width: 40px;
          right: 172px;
    }
</style>
<!-- <link href="<?php echo base_url();?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" /> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/spectrum.css">
<link href="<?php echo base_url();?>assets/css/timepicki.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://www.tiny.cloud/css/codepen.min.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<div class="pcoded-content adminsettingcls">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <!--start-->
               <div class="title_page01 floating">
                  <div class="modal-alertsuccess alert alert-success info_box" style="display:none;">
                     <a href="#" class="close" aria-label="close">&times;</a>
                     <div class="pop-realted1">
                        <div class="position-alert1 info_text">
                        </div>
                     </div>
                  </div>
                  <div class="deadline-crm1 floating_set" style="display: none;">
                      <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                          <li class="nav-item form_info">
                            <a class="nav-link form_info active" data-toggle="tab" href="#adminprofile">Admin Profile</a>
                          </li>
                          <li class="nav-item ">
                            <a class="nav-link " data-toggle="tab" href="#adminsettings">Admin settings</a>
                          </li>
                      </ul>
                  </div>
                  <div class="service_view01 upload-file05 admin-profile-wrapper">
                      <div class="tab-content">
                     <div id="adminprofile" class="tab-pane active">
                     <div class="admin-boxshadow floating adminsettingsection">
                        <h2> Admin Setting </h2>
                        <form id="admin_setting" class="validation dragfilesclstop" method="post" action="">
                           <div class="row">

                               <div class="col-sm-4 form-group">
                                 <div class="col-sm-12">
                                  <label class="col-form-label">Company Name <span class="Hilight_Required_Feilds">*</span> </label>
                                  <input type="text" class="clr-check-client" name="company_name" id="company_name" value="<?php if(isset($admin_set['company_name']) && $admin_set['company_name']!='') { echo $admin_set['company_name']; } ?>">
                                 </div>
                              </div>

                         

                              <div class="col-sm-4 form-group">
                                  <label class="col-form-label">Company Contact No <span class="Hilight_Required_Feilds">*</span></label>
                                 <div class="col-sm-12 firm_contact_group">

                                    <div class="firm_contact_country_code_select_div">
                                     <select class="firm-con-code" name="country_code" placeholder="Country Code">
                                        <option value="">Select Country Code</option>
                                      <?php 
                                        foreach ( $countries as $value )
                                        {
                                          $select = "";
                                          if( $admin_set['country_code'] == $value['id'] )
                                          {
                                            $select = "selected='selected'"; 
                                          }
                                          echo "<option value='".$value['id']."' ".$select.">".$value['sortname']."</option>";
                                        }
                                      ?>
                                      </select>
                                    </div>
                                    <input type="text" class="clr-check-client" name="company_contact_no" id="company_contact_no" value="<?php if(isset($admin_set['company_contact_no']) && $admin_set['company_contact_no']!='') { echo $admin_set['company_contact_no']; } ?>">
                                 </div>
                              </div>
                             
                              <div class="col-sm-4 form-group">
                                 <div class="col-sm-12">
                                  <label class="col-form-label">Currency <span class="Hilight_Required_Feilds">*</span></label>
                          <div class="dropdown-sin-2 currency_details">
                                    <select name="currency" id="currency">
                                  <option value="">Select Currency</option>
                                   <?php
                         foreach($currency as $key => $value) {  ?>

                        <option value="<?php echo $value['country']; ?>" <?php if($value['country']==$admin_set['crm_currency']){ ?> Selected="selected" <?php } ?> ><?php echo $value['country'].'-'.$value['currency']; ?></option> 
                        <?php  } ?>
                                      
                                      <!--  -->
                                    </select>
                                    </div>
                                 </div>
                              </div>
                        <div class="col-sm-4 form-group">
                           <div class="col-sm-12">
                            <label class="col-form-label">Email Reminder Cron Time <span class="Hilight_Required_Feilds">*</span></label>
                              <input type="text" class="clr-check-client" name="client_reminder_time" id="client_reminder_time" value="<?php if(isset($admin_set['client_reminder_time']) && $admin_set['client_reminder_time']!='') { echo $admin_set['client_reminder_time']; } ?>">
                           </div>
                        </div>

                        <!-- Comment out Logo label -->
                        <!-- <div class="col-sm-4 form-group fileupload_01 attach-files">
                          <div class="col-sm-12">
                            <label class="col-form-label">Logo</label>
                              <div class="upload_input09 custom_upload">
                                 <input type="file" name="logo" id="logo_image">
                              </div>
                           </div>
                        </div> -->
                          <!-- <div class="row form-group full-width"></div> -->
                          <div class="col-sm-4 form-group">
                            <div class="col-sm-12">
                                <label class="col-form-label">Reminder Send Status <span class="Hilight_Required_Feilds">*</span> </label>
                                <div class="source_select_div dropdown-multiple ">
                                    <select name="reminder_send_status[]" id="reminder_send_status" class="sel_status" multiple="multiple" placeholder="Nothing Selected" style="display: none;">
                                        <?php
                                              $rss = json_decode($admin_set['reminder_send_status'], true);
                                            if( empty( $rss ) )
                                            {
                                              $rss = array();
                                            }
                                            
                                        ?>
                                        <option value="1" <?php if(in_array(1,$rss)) echo "selected"; ?>>Active</option>
                                        <option value="0" <?php if(in_array(0,$rss)) echo "selected"; ?>>Inactive</option>
                                        <option value="3" <?php if(in_array(3,$rss)) echo "selected"; ?>>Frozen</option>
                                        <option value="5" <?php if(in_array(5,$rss)) echo "selected"; ?>>Archive</option>
                                    </select>
                                </div>
                              </div>
                              </div>

                              <div class="col-sm-4 form-group">
                                 <div class="col-sm-12">
                                  <label class="col-form-label">Service Reminder CC</label>
                                    <input type="text" class="clr-check-client" name="company_email" id="company_email" value="<?php if(isset($admin_set['company_email']) && $admin_set['company_email']!='') { echo $admin_set['company_email']; } ?>">
                                 </div>
                              </div>

                            <div class="col-sm-4 form-group">
                              <div class="col-sm-12">
                                <label class="col-form-label">Auto Create Task Status <span class="Hilight_Required_Feilds">*</span> </label>
                                <div class="source_select_div dropdown-multiple">
                                    <select name="auto_create_task_status[]" id="auto_create_task_status" class="sel_status" multiple="multiple" placeholder="Nothing Selected" style="display: none;">
                                        <?php
                                            $acts = json_decode($admin_set['auto_create_task_status'], true);
                                            if(empty($acts))
                                            {
                                                $acts = array();                                             
                                            }
                                            
                                        ?>
                                        <option value="1" <?php if(in_array(1,$acts)) echo "selected"; ?>>Active</option>
                                        <option value="0" <?php if(in_array(0,$acts)) echo "selected"; ?>>Inactive</option>
                                        <option value="3" <?php if(in_array(3,$acts)) echo "selected"; ?>>Frozen</option>
                                        <option value="5" <?php if(in_array(5,$acts)) echo "selected"; ?>>Archive</option>
                                    </select>
                                </div>
                                </div>
                          </div>

                          <div class="col-sm-4 form-group">
                            <div class="col-sm-12">
                              <label class="col-form-label">Service Reminder Stop Status
                                  <span class="Hilight_Required_Feilds">*</span>
                              </label>
                              <div class="reminder_stop_status_select_div">
                                <select name="reminder_stop_status[]" class="sel_status" multiple="multiple" placeholder="Nothing Selected">
                                    <?php
                                      $RST = array();                                             
                                      if( !empty( $admin_set['reminder_stop_status'] ) )
                                      {
                                        $RST = json_decode($admin_set['reminder_stop_status'], true);
                                      }
                                      foreach ($task_status as $key => $value)
                                      {
                                        $selected = "";
                                        if( in_array( $value['id'] , $RST ) )
                                          $selected ="selected='selected'";
                                        ?>

                                        <option value="<?php echo $value['id']?>" <?php echo $selected;?> ><?php echo $value['status_name']?></option>

                                        <?php
                                      }
                                    ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-4 form-group">
                            <div class="col-sm-12">
                              <label class="col-form-label">Client Non Responding Reminder No.
                                  <span class="Hilight_Required_Feilds">*</span>
                              </label>

                              <?php
                                $client_non_responding_reminder_no_arr = [];
                                if(isset($admin_set['client_non_responding_reminder_no'])){
                                  $client_non_responding_reminder_no_arr = explode(",",$admin_set['client_non_responding_reminder_no']);
                                }
                              ?>
                              <select name="client_non_responding_reminder_no[]" id="client_non_responding_reminder_no" multiple>
                                  <option value="1" <?php if(in_array(1, $client_non_responding_reminder_no_arr)){ echo 'selected';}?>>1st</option>
                                  <option value="2" <?php if(in_array(2, $client_non_responding_reminder_no_arr)){ echo 'selected';}?>>2nd</option>
                                  <option value="3" <?php if(in_array(3, $client_non_responding_reminder_no_arr)){ echo 'selected';}?>>3rd</option>
                                  <option value="4" <?php if(in_array(4, $client_non_responding_reminder_no_arr)){ echo 'selected';}?>>4th</option>
                                  <option value="5" <?php if(in_array(5, $client_non_responding_reminder_no_arr)){ echo 'selected';}?>>5th</option>
                                  <option value="6" <?php if(in_array(6, $client_non_responding_reminder_no_arr)){ echo 'selected';}?>>6th</option>
                                  <option value="7" <?php if(in_array(7, $client_non_responding_reminder_no_arr)){ echo 'selected';}?>>7th</option>
                                  <option value="8" <?php if(in_array(8, $client_non_responding_reminder_no_arr)){ echo 'selected';}?>>8th</option>
                                  <option value="9" <?php if(in_array(9, $client_non_responding_reminder_no_arr)){ echo 'selected';}?>>9th</option>
                                  <option value="10" <?php if(in_array(10, $client_non_responding_reminder_no_arr)){ echo 'selected';}?>>10th</option>
                                  <option value="11" <?php if(in_array(11, $client_non_responding_reminder_no_arr)){ echo 'selected';}?>>11th</option>
                                  <option value="12" <?php if(in_array(12, $client_non_responding_reminder_no_arr)){ echo 'selected';}?>>12th</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4 form-group">
                            <div class="col-sm-12">
                              <label class="col-form-label">Page Length
                                  <span class="Hilight_Required_Feilds">*</span>
                              </label>
                                <?php 
                                $sel = array_fill_keys(['10','25','50','100','-1'], " ");
                                
                                if( !empty( $admin_set['page_length'] ) )
                                {
                                  $sel[ trim( $admin_set['page_length'] ) ] = "selected='selected'";
                                }
                                ?>
                                <select name="page_length" class="clr-check-select">
                                  <option value="10" <?php echo $sel['10']; ?> >10</option>
                                  <option value="25" <?php echo $sel['25']; ?> >25</option>
                                  <option value="50" <?php echo $sel['50']; ?> >50</option>
                                  <option value="100" <?php echo $sel['100']; ?> >100</option>
                                  <option value="-1" <?php echo $sel['-1']; ?> >All</option>
                                </select>
                            </div>
                          </div>
                          <div class="col-sm-4 form-group">
                            <div class="col-sm-12">
                              <label class="col-form-label">Mail Background color
                              </label>
                              <input type='text' class="color_picker" name="background_color" value="<?php echo $admin_set['header_footer_background_color'];?>" />
                            </div>
                          </div>
                       

                        <div class="rownamefiledcls">
                          <div class="form-group row name_fields">
                             <label class="col-sm-10 col-form-label">Service Reminders</label>
                             <div class="col-sm-2 dislaynpnecls">
                                <input type="checkbox" class="js-small f-right" name="service_reminder" id="" value="1" <?php if(isset($admin_set['service_reminder']) && ($admin_set['service_reminder']=='1') ){ ?> checked="checked"<?php } ?>>
                             </div>
                          </div>

                          <div class="form-group row name_fields">
                             <label class="col-sm-10 col-form-label">Missing Details send option in userlist</label>
                             <div class="col-sm-2 dislaynpnecls">
                                <input type="checkbox" class="js-small f-right" name="missing_details" id="" value="1" <?php if(isset($admin_set['missing_details']) && ($admin_set['missing_details']=='1') ){ ?> checked="checked"<?php } ?>>
                             </div>
                          </div>


                          <div class="form-group row name_fields">
                             <label class="col-sm-10 col-form-label">Automatically Create Missing Details Task</label>
                             <div class="col-sm-2 dislaynpnecls">
                                <input type="checkbox" class="js-small f-right" name="missing_task" id="" value="1" <?php if(isset($admin_set['missing_task']) && ($admin_set['missing_task']=='1') ){ ?> checked="checked"<?php } ?>>
                             </div>
                          </div>

                          <div class="form-group row name_fields">
                             <label class="col-sm-10 col-form-label">Sms meeting Reminders</label>
                             <div class="col-sm-2 dislaynpnecls">
                                <input type="checkbox" class="js-small f-right" name="meeting_reminder" id="" value="1" <?php if(isset($admin_set['meeting_reminder']) && ($admin_set['meeting_reminder']=='1') ){ ?> checked="checked"<?php } ?>>
                             </div>
                          </div>


                         <div class="form-group row name_fields" style="display: none;">
                           <label class="col-sm-10 col-form-label">Deadline Day Sms To manager</label>
                           <div class="col-sm-2 dislaynpnecls">
                              <input type="checkbox" class="js-small f-right" name="deadline_reminder" id="" value="1" <?php if(isset($admin_set['deadline_reminder']) && ($admin_set['deadline_reminder']=='1') ){ ?> checked="checked"<?php } ?>>
                           </div>
                        </div>
                      </div>
                </div>

                

                <div class="row form-group new-append1">
                      <div class="full-dats1 col-sm-12 col-xs-12">                   
                        <label>Email Header <span class="Hilight_Required_Feilds">*</span> </label>
                        <textarea type="text" name="header1" id="header1"  placeholder="Write something"><?php echo $admin_set['header1']; ?></textarea>
                      </div>
                    
                      <div class="full-dats1 col-sm-12 col-xs-12">                   
                        <label>Email Footer <span class="Hilight_Required_Feilds">*</span></label>
                        <textarea type="text" name="footer1" id="footer1" placeholder="Write something"><?php echo $admin_set['footer1']; ?></textarea>
                      </div>
                </div>
                <div class="row form-group new-append1">
                      <div class="full-dats1 col-sm-6 col-xs-6">                   
                        <label>SMTP Server <span class="Hilight_Required_Feilds">*</span> </label>
                        <input type="text" name="smtp_server" placeholder="ssl://smtp.gmail.com" class="form-control smtp_details clr-check-client"
                        value="<?php echo ( !empty( $smtp_settings['smtp_server'] ) ? $smtp_settings['smtp_server'] : '' );?>" 
                        ><a href="#" class="test_mail btn btn-primary">Test Mail</a>
                        <p style="color: #f75a5a;margin-top: 10px;"> * If you are using GMAIL SMTP You need to enable less secure apps access & disable two-factor auth.</p>
                      </div>
                      
                    
                      <div class="full-dats1 col-sm-6 col-xs-6">                   
                        <label>SMTP Email <span class="Hilight_Required_Feilds">*</span></label>
                        <input type="text" name="smtp_email" placeholder="name@mydomain.com" class="form-control smtp_details clr-check-client"
                        value="<?php echo ( !empty( $smtp_settings['smtp_email'] ) ? $smtp_settings['smtp_email'] : $admin_set['company_email'] );?>" 
                        >
                      </div>

                      <div class="full-dats1 col-sm-6 col-xs-6">                   
                        <label>From Name <span class="Hilight_Required_Feilds">*</span> </label>
                        <input type="text" name="smtp_from_name" placeholder="Accotax" class="form-control smtp_details clr-check-client"
                        value="<?php echo ( !empty( $smtp_settings['smtp_from_name'] ) ? $smtp_settings['smtp_from_name'] : '' );?>" 
                        >
                      </div>
                    
                      <div class="full-dats1 col-sm-6 col-xs-6">                   
                        <label>SMTP Security</label>
                          <?php 
                            $sel = array_fill_keys(['SSL','TLS'], " ");
                            
                            if( !empty( $smtp_settings['smtp_security'] ) )
                            {
                              $sel[ trim( $smtp_settings['smtp_security'] ) ] = "selected='selected'";
                            }
                          ?>
                         <select name="smtp_security" class="smtp_security smtp_details clr-check-select"> 
                            <option value=""></option>
                            <option value="SSL" <?php echo $sel['SSL'];?> >SSL</option>
                            <option value="TLS" <?php echo $sel['TLS'];?> >TLS</option>
                        </select> 
                      </div>

                      <div class="full-dats1 col-sm-6 col-xs-6">                   
                        <label>SMTP Port <span class="Hilight_Required_Feilds">*</span></label>
                         <input type="text" name="smtp_port" class="form-control smtp_port smtp_details clr-check-client"
                         value="<?php echo ( !empty( $smtp_settings['smtp_port'] ) ? $smtp_settings['smtp_port'] : '' );?>" 
                         >
                      </div>

                      <div class="full-dats1 col-sm-6 col-xs-6">                   
                        <label>SMTP Username <span class="Hilight_Required_Feilds">*</span></label>
                         <input type="text" name="smtp_username" class="form-control smtp_details clr-check-client" placeholder="name@mydomain.com"
                         value="<?php echo ( !empty( $smtp_settings['smtp_username'] ) ? $smtp_settings['smtp_username'] : '' );?>" 
                         >
                      </div>
                      <div class="full-dats1 col-sm-6 col-xs-6">                   
                        <label>SMTP Password <span class="Hilight_Required_Feilds">*</span></label>
                         <input type="password" name="smtp_password" class="form-control smtp_details clr-check-select"
                         value="<?php echo ( !empty( $smtp_settings['smtp_password'] ) ? $smtp_settings['smtp_password'] : '' );?>"
                         >
                         <i class="fa fa-eye show_hide_password"></i>
                      </div>
                </div>

                <?php 
                if( $_SESSION['permission']['Admin_Settings']['edit'] == 1 ) 
                {
                ?>
                <div class="form-group row title_submitbt submitborder">
                    <input type="submit" value="submit" name="submit" id="submit_button">
                </div>
                <?php
                }
                ?>
            </form>
            </div>
<!--end -->
</div>


                  </div>
               </div>
               <!-- close -->
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>

<div class="modal fade" id="test_smtp_details" role="dialog" style="display: none;">
  <div class="modal-dialog modal-test-mail">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Test Mail</h4>
      </div>
      <div class="modal-body">      
        <div class="form-group">
          <div class="alert return_message" style="display: none;">
          </div>
        </div>
        <div class="form-group">
          <input type="email" id="test_to_mail_address">
          <button class="btn btn-info send_test_smtp_details">Send</button>          
        </div>
      </div>
    </div>  
  </div>
</div>

<?php // $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<!-- Warning Section Ends -->
<!-- Required Jquery -->
<!-- j-pro js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<!-- jquery slimscroll js -->
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<!-- <script src="<?php echo base_url();?>assets/js/jquery.fileuploads.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/jquery.filer.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/timepicki.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/tinymce.min.js">
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
	$(document).ready(function(){
		$(".clr-check-client").keyup(function(){
			var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-client-outline");
			}else{
				$(this).removeClass("clr-check-client-outline");
			}
		});
    $(".clr-check-client").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-client-outline");
      }  
    })

		$(".clr-check-select").change(function(){
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		});
    $(".clr-check-select").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-select-client-outline");
      }  
    })
	});

</script>


<script>
$( document ).ready(function() {

  tinymce_config['images_upload_url'] = "<?php echo base_url();?>Firm/uplode_mail_header_image";
  tinymce_config['selector']          = '#footer1,#header1';
  

  tinymce.init( tinymce_config );

  $('#client_non_responding_reminder_no').select2();

    /*var editorConfig = {
      removePlugins : 'image',    
      extraPlugins : 'colorbutton,justify,image2',
      filebrowserUploadUrl: "<?php echo base_url();?>Firm/uplode_mail_header_image",
    
      toolbarGroups: [
    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
    { name: 'document', groups: [ 'mode', 'doctools', 'document' ] },
    { name: 'forms', groups: [ 'forms' ] },
    '/',
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    { name: 'paragraph', groups: [ 'align', 'list', 'indent', 'blocks', 'bidi', 'paragraph' ] },
    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
    { name: 'links', groups: [ 'links' ] },
    { name: 'insert', groups: [ 'insert' ] },
    '/',
    { name: 'styles', groups: [ 'styles' ] },
    { name: 'colors', groups: [ 'colors' ] },
    { name: 'tools', groups: [ 'tools' ] },
    { name: 'others', groups: [ 'others' ] },
    { name: 'about', groups: [ 'about' ,'Widgets'] }
  ],
  removeButtons : 'Source,Templates,Save,NewPage,Preview,Print,Find,Replace,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,CreateDiv,Language,Flash,Smiley,Iframe,ShowBlocks,About'
       };

    CKEDITOR.replace( 'header1', editorConfig );*/
   // CKEDITOR.replace( 'footer1', editorConfig );

    var header_footer_color = "<?php echo $admin_set['header_footer_background_color'];?>";
    $(".color_picker").spectrum({
          preferredFormat: "hex"          
      });
    $('.sp-dd').html('');

    $('.dropdown-sin-2').dropdown({
        limitCount: 5,
        input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
    });


    $('.source_select_div').dropdown({
        limitCount: 5,
        input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
    });
    
    $('.reminder_stop_status_select_div').dropdown();

    $(".firm_contact_country_code_select_div").dropdown({
      choice:function()
      { 
        $("select.firm-con-code").valid(); 
      }
    });
   

    /*$("#logo_image").filer({files:[{url:"https://remindoo.uk/uploads/825898.png",type: "image/png"}]});*/
   
       var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
       // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#1abc9c',
               jackColor: '#fff',
               size: 'small'
           });
       });
   
       $('#accordion_close').on('click', function(){
               $('#accordion').slideToggle(300);
               $(this).toggleClass('accordion_down');
       });

         $('#client_reminder_time').timepicki({
            start_time: ["10", "00", "AM"],
            step_size_minutes:5
        });

     /*CKEDITOR.editorConfig = function (config) {

      config.language = 'es';
      config.uiColor = '#fff';
      config.height = 50;
      config.toolbarCanCollapse = true;
      config.toolbarLocation = 'bottom';        

    };*/

   /* CKEDITOR.replace( 'header1', {
    filebrowserUploadUrl: "<?php echo base_url();?>upload/" 
    } );
    CKEDITOR.replace( 'footer1' );*/


    jQuery.validator.addMethod("contact_no",function(inputtxt ,element)
    {
       var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
      // console.log(this.optional(element)+"inside contact_no");
      return this.optional(element) || phoneno.test(inputtxt);
           
    }, "Enter Valid Contact Number.");
    jQuery.validator.addMethod("Check_FileType",function(inputtxt ,element)
    {
      
      if(inputtxt=='')
      {
        return true;
      }
      else
      {
        var accept_ext = ['gif','jpg','png','jpeg','jPG','JPG'];
        var file_ext = inputtxt.split('.').pop();
        if(accept_ext.indexOf(file_ext)!==-1)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
           
    }, "allowed file type gif|jpg|png|jpeg|jPG|JPG.");

   /* $('#admin_setting').submit(function() {
        $(this).find('.sel_status').each(function() {
            $(this).rules("add", {
                required : true
            });
        });
    });*/
     /*var validateEditor = function(textarea)
     {
          //CKEDITOR.instances[textarea.id].updateElement(); // update textarea
          tinymce.get(textarea.id).triggerSave();
          var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
          return editorcontent.length === 0;
     }
*/

   $("#admin_setting").validate({
          ignore: false,
              /*onfocusout: function(element) {
               if ( !this.checkable(element)) {
                   this.element(element);
               }
           },*/
          // errorClass: "error text-warning",
           //validClass: "success text-success",
           /*highlight: function (element, errorClass) {
               //alert('em');
              // $(element).fadeOut(function () {
                  // $(element).fadeIn();
               //});
           },*/     
           rules: {
            company_name: {required: true},
            company_email: {
              email:true
            },
            page_length: {required: true},
            currency: {required: true},
            country_code : {required: true},
            company_contact_no: {required: true,contact_no:true},                          
            client_reminder_time: {required: true},
            Logo:{Check_FileType:true},
            "reminder_send_status[]" : { required : true },
            "auto_create_task_status[]" : { required : true },
            "reminder_stop_status[]"    : { required : true },
            header1 : { required : true },
            footer1 : { required : true },
         
           },
           messages :
           {
             user_name : {remote : "This Name already exist."}
           },
           errorElement: "span" , 
           errorClass: "field-error", 
           errorPlacement:function(error , element)
           {
            
            if( $(element).hasClass("sel_status") )
            {
              error.insertAfter( $(element).parent() );
            }
            else if( element.attr("name") == "country_code")
            {
              $(".firm_contact_group").append(error);     
            }
            else if( $(element).attr('name') == "currency")
            {
              error.insertAfter( $(element).parent() ); 
            }else if( $(element).attr('name') == "header1" || $(element).attr('name') == "footer1")
            {
              error.insertAfter( $(element).parent('div').find('div.tox') ); 
            }
            else
            {
              error.insertAfter( $(element) );  
            }
           },                           
           submitHandler: function(form) {
               var formData = new FormData($("#admin_setting")[0]);
               
               

               $.ajax({
                  url: '<?php echo base_url();?>user/update_firm_admin_settings/',
                  dataType : 'json',
                  type : 'POST',
                  data : formData,
                  contentType : false,
                  processData : false,
                  beforSend: function()
                  {
                    $(".LoadingImage").show();
                  },
                  success: function(data)
                  {
                    if(data.response == 1)
                    {
                      $('.info_box .info_text').html(' Settings Update Successfully.. ');
                      $('.info_box').show();
                    }
                    setTimeout(() => {
                      $('.info_box').hide();
                    },2000)
                      $(".LoadingImage").hide();
                    
                  },
                  error: function()
                  {
                    $('.info_box .info_text').html(' Some Thing Wrong.. ');
                    $('.info_box').show();
                  }
               });
           }                           
     });
   
   $('div.alert-success .close, .alert-danger .close').click(function(){
      $(this).closest('div.alert').hide();      
   });

   $('.smtp_security').change(function()
   {
    console.log("inside the function");

    var sec = $(this).val();
    console.log( sec );
    var port = 25;
    
    if( sec == 'TLS') port = 587;
    else if ( sec == 'SSL')   port = 465;

    $('.smtp_port').val( port );

   });

   $('.test_mail').click(function(){

    var is_empty = 0;
    $('.smtp_details').each(function(){
      
      if( $(this).val() == '' )
      {
        $(this).css('border','1px solid red');
        is_empty = 1;
      }
      else
      {        
        $(this).css('border','1px solid #bfbebe');
      }
    });

    if( !is_empty )
    {
      $('.return_message').hide();
      $('#test_to_mail_address').val('');
      $('#test_smtp_details').modal('show');
    }

   });
   
   $('.send_test_smtp_details').click(function(){

    var source       = {};
    source['to'] = $('#test_to_mail_address').val();

    if( source['to'] == '' )
    {
      $('#test_to_mail_address').css('border','1px solid red');
      return;
    }
    else
    {
      $('#test_to_mail_address').css('border','1px solid #bfbebe');
    }

    $('.smtp_details').each(function(){
      source[ $(this).attr('name') ] = $(this).val();
    });
    $.ajax({
            url: '<?php echo base_url();?>Firm/test_smtp_details',
            dataType  : 'json',
            type      : 'POST',
            data      : source,            
            beforSend : function()
            {
              $(".LoadingImage").show();
            },
            success: function(data)
            {
              if(data.result == 1)
              { 
                $('#test_smtp_details .return_message').removeClass('alert-danger').addClass('alert-success').html('<b>Mail Sent Successfully..!</b>').show();
                setTimeout(function(){ $('#test_smtp_details').modal('hide');} ,1000);                
              }
              else
              {
                $('#test_smtp_details .return_message').removeClass('alert-success').addClass('alert-danger').html('<b>Mail Not Sent</b><p>'+data.message+'</p>').show();
              }
              $(".LoadingImage").hide();
            }
          });

   });

   $('.show_hide_password').click(function(){

    var input = $('input[name="smtp_password"]');
    
    if( input.attr('type') == 'text' )
        input.attr('type' , 'password' );
    else
        input.attr('type' , 'text' );
   });

   });
</script>
<!-- <script type="text/javascript">
   $(document).ready(function(){
     $("#country").change(function(){
     var country_id = $(this).val();
     //alert(country_id);
   
       $.ajax({
   
         url:"<?php echo base_url().'Client/state';?>",
         data:{"country_id":country_id},
         type:"POST",
         success:function(data){
           //alert('hi');
           $("#state").append(data);
           
         }
   
       });
     });
      $("#state").change(function(){
     var state_id = $(this).val();
     //alert(country_id);
   
       $.ajax({
   
         url:"<?php echo base_url().'Client/city';?>",
         data:{"state_id":state_id},
         type:"POST",
         success:function(data){
           //alert('hi');
           $("#city").append(data);
           
         }
   
       });
     });
   });
</script> -->
</body>
</html>