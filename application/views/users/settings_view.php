<?php $this->load->view('includes/header');?>


<section class="client-details-view settings-various1 floating_set">
	<div class="client-inform-data1 floating_set">
		<h2><i class="fa fa-cog fa-6" aria-hidden="true"></i> Settings</h2>
	<div class="bg-set-settingss floating_set">	
		<div class="default-services">
			<div class="form-radio">
				<div class="radio radiofill radio-primary radio-inline">
					<label>
					<input type="radio" name="member" value="free" data-bv-field="member">
					<i class="helper"></i>Select here
				</label>
				</div>
				<div class="radio radiofill radio-primary radio-inline">
					<label>
					<input type="radio" name="member" value="personal" data-bv-field="member">
					<i class="helper"></i>Another select
					</label>
				</div>
			</div>

			<div class="custom-button01">
				<a href="#">New Service</a>
			</div>	
		</div>	

		<div class="accordion-setting">

			<div id="accordion" role="tablist" aria-multiselectable="true">
					<div class="accordion-panel">
					<div class="accordion-heading" role="tab" id="headingOne">
					<h3 class="card-title accordion-title">
					<a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					Company Tax Return
					</a> </h3>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
					<div class="accordion-content accordion-desc">

						<div class="new-step-link1 text-right">
							<a href="#">New Step</a>
						</div>	

						<div class="settings-step-section">
								<h4>Step 1</h4>
								<div class="step-form-set01">
									<div class="column-setting-row">
										<div class="completion1"> <i class="fa fa-arrow-circle-o-up fa-6" aria-hidden="true"></i> Completion</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
									</div>		

									<div class="column-delete">
										<div class="column-setting-row">
										<div class=" completion1"> <i class="fa fa-user fa-6" aria-hidden="true"></i> obtain payment from client</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
										</div>

										<div class="column-setting-row ">
										<div class=" completion1"> <i class="fa fa-user fa-6" aria-hidden="true"></i> Send client HMRC Tax Payment slip</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
										</div>

										<div class="column-setting-row">
										<div class="completion1"> <i class="fa fa-user fa-6" aria-hidden="true"></i> Add notes forward to next job</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
										</div>
							</div>
								</div>	<!-- step-form-set01 -->	
					</div>

					<div class="settings-step-section">
								<h4>Step 2</h4>
								<div class="step-form-set01">
									<div class="column-setting-row">
										<div class="completion1"> <i class="fa fa-arrow-circle-o-up fa-6" aria-hidden="true"></i> Client Approval</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
									</div>		

									<div class="column-delete">
										<div class="column-setting-row">
										<div class=" completion1"> <i class="fa fa-user fa-6" aria-hidden="true"></i> Send final return to client</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
										</div>

										<div class="column-setting-row ">
										<div class=" completion1"> <i class="fa fa-user fa-6" aria-hidden="true"></i> return approved by client</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
										</div>

							</div>
					</div>
								</div>	<!-- step-form-set01 -->

								<div class="settings-step-section">
								<h4>Step 3</h4>
								<div class="step-form-set01">
									<div class="column-setting-row">
										<div class="completion1"> <i class="fa fa-arrow-circle-o-up fa-6" aria-hidden="true"></i> Prepare Tax Return</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
									</div>		

									<div class="column-delete">
										<div class="column-setting-row">
										<div class=" completion1"> <i class="fa fa-user fa-6" aria-hidden="true"></i> Enter Accounts on CT600 </div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
										</div>

										<div class="column-setting-row ">
										<div class=" completion1"> <i class="fa fa-user fa-6" aria-hidden="true"></i>Adjusted for non allowable add backs</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
										</div>

										<div class="column-setting-row ">
										<div class=" completion1"> <i class="fa fa-user fa-6" aria-hidden="true"></i>calculate and enter Capital Allowances</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
										</div>

										<div class="column-setting-row ">
										<div class=" completion1"> <i class="fa fa-user fa-6" aria-hidden="true"></i>Review for loo relif</div>
										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
										</div>

										<div class="column-setting-row ">
										<div class=" completion1"> <i class="fa fa-user fa-6" aria-hidden="true"></i>Prepare final return</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
										</div>

										<div class="column-setting-row ">
										<div class=" completion1"> <i class="fa fa-user fa-6" aria-hidden="true"></i>Calculate liability</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
										</div>



							</div>
					</div>
								</div>	<!-- step-form-set01 -->


					</div>
				</div>	
			</div>	<!-- accordion-panel -->

			<div class="accordion-panel">
					<div class="accordion-heading" role="tab" id="headingtwo">
					<h3 class="card-title accordion-title">
					<a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="true" aria-controls="collapsetwp">
					Confirmation Statement
					</a> </h3>
					</div>
					<div id="collapsetwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingtwo">
					<div class="accordion-content accordion-desc">
						<div class="settings-step-section">
								<h4>Step 2</h4>
								<div class="step-form-set01">
									<div class="column-setting-row">
										<div class="completion1"> <i class="fa fa-arrow-circle-o-up fa-6" aria-hidden="true"></i> Client Approval</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
									</div>		

									<div class="column-delete">
										<div class="column-setting-row">
										<div class=" completion1"> <i class="fa fa-user fa-6" aria-hidden="true"></i> Send final return to client</div>

										<div class="completion2">
											<a href="#"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
										</div>
										</div>

										

							</div>
					</div>
								</div>	<!-- step-form-set01 -->
					</div>
				</div>
			</div>	<!-- accordion-panel -->


		</div> <!-- accordion -->
		</div>	<!-- accordion-setting -->



</div>
		</div> <!-- client -->
</section>

<?php $this->load->view('includes/footer');?>