<style type="text/css">
    .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}
</style>

<?php
 $this->load->view('includes/header');  ?>

 <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
 <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!--   Datatable header by Ram -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css?ver=2"> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">

<!-- Tree Select -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree_select/style.css?ver=2">
<!-- / -->
<!--   End by Ram -->
 <?php
 $succ = $this->session->flashdata('success');
 $error = $this->session->flashdata('error'); 
 $stafff = $this->db->query("select group_concat(`crm_refered_by` separator ',') as refer_ids from client where crm_refered_by !='' ")->row_array();
 //print_r($staffs);
 $dept =  explode(',', $stafff['refer_ids']);   
 $staff_form = $this->db->query("select * from user where id in ( '" . implode( "','", $dept ) . "' )")->result_array();
/** 16-07-2018 **/
$it_session_id='';
$get_this_qry=$this->db->query("select * from user where id=".$_SESSION['id'])->row_array();
$created_id=$get_this_qry['firm_admin_id'];
$its_firm_admin_id=$get_this_qry['firm_admin_id'];
// if($_SESSION['role']==6) // staff
// {
// $it_session_id=$_SESSION['id'];
// }
// else if($_SESSION['role']==5) //manager
// {
// $it_session_id=$created_id;
// }
// else if($_SESSION['role']==4)//client
// {
// $it_session_id=$created_id;
// }
// else if($_SESSION['role']==3) //director
// {
// $it_session_id=$created_id;
// }
// else if($_SESSION['role']==2) // sub admin
// {
//   $it_session_id=$created_id;
// }
// else
// {
  $it_session_id=$_SESSION['id'];
//}
$result=array();
   array_push($result, $it_session_id);
   $reassign_firm_id=$its_firm_admin_id;
 $tot_val=1;
    for($z=0;$z<$tot_val;$z++)
    {
      if($reassign_firm_id==0)
      {
        $tot_val=0;
      }
      else
      {
          $get_this_qry=$this->db->query("select * from user where id=".$reassign_firm_id)->row_array();
          if(!empty($get_this_qry)){
             array_push($result, $get_this_qry['id']);
          $new_created_id=$get_this_qry['firm_admin_id'];
          $reassign_firm_id=$get_this_qry['firm_admin_id'];
          }
          else
          {
            $tot_val=0;
          }
         
      }

    }
     $res=(!empty($result))?implode(',',array_filter(array_unique($result))):'';
      /**end of 16-07-2018 **/
     /** for newly added **/
    $staff_form = $this->db->query("select * from user where role=6 and firm_admin_id=".$_SESSION['id']." ")->result_array();
    $team = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();
    $department=$this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();
     /** 16-07-2018 **/
    $team_num=array();
    $for_cus_team=$this->db->query("select * from team_assign_staff where FIND_IN_SET('".$_SESSION['id']."',staff_id)")->result_array();
    foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
           array_push($team_num,$cu_team_value['team_id']);
    }
        if(!empty($team_num) && count($team_num)>0){
              $res_team_num=implode('|', $team_num);
        }else{
            $res_team_num=0;
        }
      $department_num=array();
      $for_cus_department=$this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
        foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
            array_push($department_num,$cu_dept_value['depart_id']);
        }
          if(!empty($department_num) && count($department_num)>0){
              $res_dept_num=implode('|', $department_num);
          }else
          {
               $res_dept_num=0;
          }
          /** end of 16-07-2018 **/   
           ?>
<div class="pcoded-content leads-section floating_set" >
<div class="bgnew-lead1">
<!-- for success message -->
<!-- <div class="floating_set dashboard_success_message" style="display: none;">
    <div class="status_succ"><div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Status have been changed successfully...</span></div></div></div></div>
</div> -->
<div class="modal-alertsuccess alert  succ popup_info_box" style="display:none;">
  <div class="newupdate_alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              </div>
         </div>
         </div>
   </div>

<div class="modal-alertsuccess alert alert-danger-check succ dashboard_success_message" style="display:none;">
      <div class="newupdate_alert">   <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              Success !!! Status have been changed successfully...
         </div>
         </div>
   </div></div>

<div class="modal-alertsuccess alert alert-danger-check succ dashboard_success_message_source" style="display:none;">
       <div class="newupdate_alert">  <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
               Updated Source Successfully...
         </div></div>
         </div>
   </div>
<div class="modal-alertsuccess alert alert-danger-check succ staff-added dashboard_success_message_staff" style="display:none;">
      <div class="newupdate_alert">   <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
               Staff Assigned Successfully...
         </div></div>
         </div>
   </div>
<!-- end of success msg -->
   <div class="deadline-crm1 floating_set">
      <div class="pull-left" id="lead_dashboard">
         <!-- <ul class="news-leads">
            <li><a href="#" data-toggle="modal" data-target="#new-lead">NEW LEAD</a></li>
            <li><a href="#" data-toggle="modal" data-target="#import-lead">IMPORT LEADS</a></li>
            <li class="edit-op"><a href="<?php echo base_url()?>leads/kanban">SWITCH TO KAN BAN</a></li>
            </ul> -->
         <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
            <li class="nav-item">
               <a class="nav-link tap-toggle  status_filter_new_all" data-tap="#dashboard">Dashboard</a>
              <!--  <a class="nav-link active" href="<?php echo base_url()?>leads">Dashboard</a> -->
               <div class="slide"></div>
            </li>
       
            <li class="nav-item">
               <a class="nav-link tap-toggle" data-tap="#new_lead">NEW LEAD</a>
               <div class="slide"></div>
            </li>
          <?php if($_SESSION['permission']['Import_leads'][0]['create']=='1'){ ?>  
            <li class="nav-item">
               <a class="nav-link" data-toggle="modal"  href="#import-lead">IMPORT Leads</a>
               <div class="slide"></div>
            </li>
         <?php } ?>
            <li class="nav-item">
               <a class="nav-link tap-toggle" data-tap="#leads_source" >Lead sources</a>
               <div class="slide"></div>
            </li>
       
            <li class="nav-item">
               <a class="nav-link tap-toggle"  data-tap="#leads_status" >lead status</a>
               <div class="slide"></div>
            </li>
       
            <li class="nav-item">
               <a class="nav-link tap-toggle"  data-tap="#kanban_tab" >SWITCH TO KAN BAN</a>
               <div class="slide"></div>
            </li>
         </ul>       
         
      </div>
      <div class="count-value1 cv pull-right">  
      </div>
   </div>
   <div class="proposal-sent-sample floating_set">
      <div class="management_section floating_set quote-add-01 ">
         <!-- tab start -->
         <div class="tab-content sort-leads1">
            <div id="new_lead" style="display: none;" class="tap-content lead-summary2 floating_set new-leadss lead-bgset01 <?php if($_SESSION['permission']['Leads']['create']!=1){ ?> permission_deined <?php } ?>" >
               <form id="leads_form" name="leads_form" method="post">
               <div class="modal-footer leadss">
                        <input type="submit" name="save" id="save" value="Create">
                        <a href="#"  onclick="clear_form_elements('#leads_form')">Reset</a>
                </div>
                  <input type="hidden" name="kanpan" id="kanpan" value="0">
                  <div class="space-equal-data col-sm-6 ">
                     <div class="lead-popupsection1 floating_set avoid-break01">
                        <div class="col-xs-12 col-sm-3 lead-form1 ">
                           <label>Lead status</label>
                           <div class="dropdown-sin-1">
                              <select name="lead_status" id="lead_status" placeholder="Nothing Selected">
                                 <option value="">Nothing selected</option>
                                 <?php foreach($leads_status as $ls_value) { ?>
                                 <option value='<?php echo $ls_value['id'];?>'><?php echo $ls_value['status_name'];?></option>                          
                                 <?php } ?>
                              </select>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-3  lead-form1 ">
                           <label>Source</label>
                           <div class="source_select_div">
                              <select name="source[]" id="source"  multiple="multiple" placeholder="Nothing Selected">
                                 <?php 
                                  $i=0;
                                 foreach($source as $lsource_value) { ?>
                                 <option value='<?php echo $lsource_value['id'];?>'  ><?php echo $lsource_value['source_name'];?></option>
                                 <?php 
                                 $i++; } ?>
                                 <!-- <option value='1'>Google</option>
                                    <option value-'2'>Facebook</option> -->
                              </select>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-3  lead-form1 add_tree">
                           <label>Assigned</label>
                           <input type="text" class="tree_select" name="assignees[]"  placeholder="Select">
                            <input type="hidden" id="assign_role" name="assign_role" >
              <!--              <div class="dropdown-sin-3 lead-form-st">
                              <select name="assigned[]" id="assigned" placeholder="Nothing Selected" multiple="multiple" >
                                 <?php
                                    if(isset($staff_form) && $staff_form!='') 
                                    { ?>
                                      <option disabled>Staff</option> 
                                      <?php 
                                       $i=0;
                                     foreach($staff_form as $value) 
                                     {
                                    ?>
                                 <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
                                 <?php
                                 $i++;
                                    }
                                    }
                               if(isset($team) && $team!='') 
                                    { ?>
                                      <option disabled>Team</option> 
                                      <?php 
                                       $i=0;
                                     foreach($team as $value) 
                                     {
                                    ?>
                                 <option value="tm_<?php echo $value['id']; ?>" ><?php echo $value["team"]; ?></option>
                                 <?php
                                 $i++;
                                    }
                                    }
                                  if(isset($department) && $department!='') 
                                    { ?>
                                      <option disabled>Department</option> 
                                      <?php 
                                       $i=0;
                                     foreach($department as $value) 
                                     {
                                    ?>
                                 <option value="de_<?php echo $value['id']; ?>"><?php echo $value["new_dept"]; ?></option>
                                 <?php
                                 $i++;
                                    }
                                    }
                                    ?>
                              </select>
                           </div> -->
                        </div>
   <!--                      <div class="col-xs-12 col-sm-3  lead-form1">
                        <label>Notify to(Manager)</label>
                        <div class="review_manager_select_div">
                        <select name="review_manager[]" id="review_manager" placeholder="Nothing Selected" multiple="multiple">
                           <?php
                              if(isset($manager) && $manager!='') 
                              {
                               foreach($manager as $value) 
                               {
                              ?>
                           <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                        </div>
                     </div> -->
                     </div>
                     <div class="tags-allow1 floating_set">
                        <h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
                        <input type="text" value="" name="tags" id="tags" class="tags" />
                     </div>
                     <div class="lead-popupsection2 floating_set avoid-break01">
                        <div class="formgroup col-xs-12 col-sm-6">
                           <label class="">Name</label>
                           <div class="">
                              <input type="text" name="name" id="name">
                           </div>
                        </div>                    
                        <div class="formgroup col-xs-12 col-sm-6">
                           <label class=" col-form-label">Address</label>
                           <div class="edit-field-popup1">
                              <textarea rows="1" name="address" id="address"></textarea>
                           </div>
                        </div>
                        <div class="formgroup col-xs-12 col-sm-6">
                           <label class=" col-form-label">Position</label>
                           <div class=" edit-field-popup1">
                              <input type="text" name="position" id="position">
                           </div>
                        </div>
                        <div class="formgroup col-xs-12 col-sm-6">
                           <label class=" col-form-label">City</label>
                           <div class=" edit-field-popup1">
                              <input type="text" name="city" id="city">
                           </div>
                        </div>
                        <div class="formgroup col-xs-12 col-sm-6">
                           <label class=" col-form-label">Email Address</label>
                           <div class=" edit-field-popup1">
                              <input type="text" name="email_address" id="email_address">
                           </div>
                        </div>
                               <div class="formgroup col-xs-12 col-sm-6">
                           <label class=" col-form-label">State</label>
                           <div class=" edit-field-popup1">
                              <input type="text" name="state" id="state">
                           </div>
                        </div>                       
                     </div>
                  </div>
                  <div class="space-equal-data col-sm-6 secondone ">
                     <div class="lead-popupsection2 floating_set avoid-break01">              
                        <div class="formgroup col-xs-12 col-sm-6">
                           <label class=" col-form-label">Website</label>
                           <div class=" edit-field-popup1">
                              <input type="text" name="website" id="website">
                           </div>
                        </div>
                        <div class="formgroup col-xs-12 col-sm-6">
                           <label class=" col-form-label">Country</label>
                           <div class="dropdown-sin-22 edit-field-popup1">
                              <select name="country" id="country">
                                 <option value=''>Nothing Selected</option>
                                 <?php foreach ($countries as $key => $value) { ?>
                                 <option value="<?php echo $value['id']?>"><?php echo $value['name'];?></option>
                                 <?php }?>
                              </select>
                           </div>
                        </div>
                        <div class="formgroup col-xs-12 col-sm-6 con-pho">
                           <label class="col-sm-12 col-form-label">Phone</label>
                     <div class="col-sm-12 hol">
                     <input type="text" name="country_code" class="country_code" value="" placeholder="Code">
                     <input type="text" name="phone" id="phone" class="telephone_number" value="<?php echo $value['phone'];?>">
                     </div>
                        </div>
                        <div class="formgroup col-xs-12 col-sm-6 zip-code-alert1">
                           <label class=" col-form-label">Zip Code</label>
                           <div class=" edit-field-popup1">
                              <input type="number" name="zip_code" id="zip_code">
                           </div>
                        </div>

                        <div class="formgroup col-xs-12 col-sm-6">
                           <label class="col-form-label">Company</label>
                             <div class=" edit-field-popup1">
                              <input type="text" name="company" id="company" placeholder="Company Name">
                           </div>                       
                        </div>                      
                        <div class="formgroup col-sm-6">
                           <label class=" col-form-label">Description</label>
                           <div class=" edit-field-popup1">
                              <textarea rows="3" name="description" id="description"></textarea>
                           </div>
                        </div>
                        <div class="formgroup checkbox-select1 col-sm-12 border-checkbox-section">
                           <div class="data_rights1">
                             <label class="custom_checkbox1"> <input type="checkbox" name="public" id="public" value="on">
                            </label>  <span>Public</span>
                           </div>
                           <div class="data_rights1">
                              <label class="custom_checkbox1"><input type="checkbox" name="contact_today" checked id="contact_today" value="on"></label>
                              <span>Contacted Today</span>
                           </div>
                        </div>
                        <div class="dedicated-sup col-sm-12 selectDate" style="display:none;">
                           <label><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Choose contact Date</label>
                           <input type="text" name="contact_date" id="contact_date" class="dob_picker" placeholder="Select your date" />
                        </div>
                     </div>
                     
                  </div>
               </form>
            </div>



            <div id="dashboard" class="in active tap-content <?php if($_SESSION['permission']['Leads']['view']!=1){ ?> permission_deined <?php } ?>"   style="display: block;">            
               <div class="switch-list01 floating_set">
                  <?php if($succ){?>             
                     <div class="modal-alertsuccess alert alert-success succ popup_success_msg">
                      <div class="newupdate_alert">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                           <div class="pop-realted1">
                           <div class="position-alert1">
                           <?php echo $succ; ?>
                           </div></div>
                           </div>
                     </div>
                  <?php }if($error){?>
                  <div class="alert alert-danger popup_success_msg">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $error; ?>
                  </div>
                  <?php } ?>                 
               </div>
               <!-- switch-list01 -->
               <!-- lead-summary1 -->
               <div class="lead-summary2 wrapper-lead-float floating_set top-leads">
               <div class="delete_leads btn btn-card btn-primary leadsG f-right" >
                  <button type="button" id="assign_member_button" class="btn btn-primary" style="display: none;">Assign </button>
                  <button type="button" id="archive_leads_button" class="del-tsk12 f-right" style="display: none;" data-toggle="modal" data-target="#confirm_archive"><i class="fa fa-archive fa-6" aria-hidden="true"></i>Archive</button>
                  <button type="button" id="unarchive_leads_button" class="del-tsk12 f-right" style="display: none;"><i class="fa fa-archive fa-6" aria-hidden="true"></i>UnArchive</button>
                  <button type="button" id="delete_leads_button" data-toggle="modal" data-target="#leads_deleteconfirmation" class="del-tsk12 f-right" style="display: none;"><i 
                  class="fa fa-trash fa-6" aria-hidden="true"> </i>Delete</button>
                </div>
        
        
          <div class="top-leads fr-task cfssc redesign-l
          ead-wrapper">
                  <div class="lead-data1 pull-right leadsG leads_status_count">
                     
                     <?php 
                      $cl=1; //css color prop
                      foreach(array_reverse($leads_status) as $ls_value) {
                        $s_id = $ls_value['id'];  
/** for permission 16-07-2018 **/
$it_session_id='';
$get_this_qry=$this->db->query("select * from user where id=".$_SESSION['id'])->row_array();
$created_id=$get_this_qry['firm_admin_id'];
$its_firm_admin_id=$get_this_qry['firm_admin_id'];
if($_SESSION['role']==6) // staff
{
$it_session_id=$_SESSION['id'];
}
else if($_SESSION['role']==5) //manager
{
$it_session_id=$created_id;
}
else if($_SESSION['role']==4)//client
{
$it_session_id=$created_id;
}
else if($_SESSION['role']==3) //director
{
$it_session_id=$created_id;
}
else if($_SESSION['role']==2) // sub admin
{
  $it_session_id=$created_id;
}
else
{
  $it_session_id=$_SESSION['id'];
}
/** end of permission 16-07-2018 **/
   $result=array();
   array_push($result, $it_session_id);
   $reassign_firm_id=$its_firm_admin_id;
 $tot_val=1;
    for($z=0;$z<$tot_val;$z++)
    {
      if($reassign_firm_id==0)
      {
        $tot_val=0;
      }
      else
      {
          $get_this_qry=$this->db->query("select * from user where id=".$reassign_firm_id)->row_array();
          if(!empty($get_this_qry)){
             array_push($result, $get_this_qry['id']);
          $new_created_id=$get_this_qry['firm_admin_id'];
          $reassign_firm_id=$get_this_qry['firm_admin_id'];
          }
          else
          {
            $tot_val=0;
          }
         
      }

    }
     $res=(!empty($result) && count($result) > 1)?implode(',',array_filter(array_unique($result))):'0';
/** end of permission 16-07-2018 **/
if($_SESSION['role']==6)
{
  $sql=$this->db->query("select count(*) as i from (SELECT * FROM leads li where li.user_id=".$it_session_id." and  li.lead_status='".$s_id."'
UNION
SELECT * FROM leads lis where lis.user_id!=".$it_session_id." and public='on' and  lis.lead_status='".$s_id."'
UNION
SELECT * FROM leads tls where tls.user_id!=".$it_session_id." and (FIND_IN_SET('".$it_session_id."',assigned) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `dept`, ',') REGEXP ',($res_dept_num),') )  and tls.lead_status='".$s_id."' ) x
")->row_array();
}
else if($_SESSION['role']==4)
{
   
  $sql=$this->db->query("select * from client where user_id=".$_SESSION['id']." ")->row_array();
  $its_id=$sql['id'];
  $sql=$this->db->query("select count(*) as i from (SELECT * FROM leads li where li.company=".$its_id." and  li.lead_status='".$s_id."' UNION
SELECT * FROM leads lis where lis.user_id in($res) and public='on' and lis.lead_status='".$s_id."') x")->row_array();

}
else if($_SESSION['role']==5 || $_SESSION['role']==3 || $_SESSION['role']==2)
{

 // $res=implode(',', $over_all_res);
$sql=$this->db->query("select count(*) as i from (SELECT * FROM leads li where li.user_id in ($res) and li.lead_status='".$s_id."'
UNION
SELECT * FROM leads lis where lis.user_id in($res) and public='on' and lis.lead_status='".$s_id."') x ")->row_array();
}
else
{
 // $res=implode(',', $over_all_res);
$sql=$this->db->query("select count(*) as i from (SELECT * FROM leads li where li.user_id in ($res) and li.firm_id='".$_SESSION['firm_id']."' and li.lead_status='".$s_id."'
UNION
SELECT * FROM leads lis where lis.user_id in($res) and public='on' and lis.lead_status='".$s_id."') x ")->row_array();
   // $data['leads']=$this->db->query("SELECT * FROM leads li where li.user_id=".$it_session_id." ")->result_array();
}

                        ?>

                             <div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors<?php echo $cl ?>  <?php $strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $ls_value['status_name']));
        $lesg = str_replace(' ', '-', $strlower);
        $lesgs = str_replace('--', '-', $lesg);echo $lesgs;?>" id="<?php echo $ls_value['id'];?>" data-status="<?php echo $ls_value['status_name'];?>" data-id="<?php echo $ls_value['status_name'];?>" data-searchCol="status_TH" >
                        <div class="lead-point1">
                           <strong id="<?php echo $ls_value['status_name'];?>"><?php echo $sql['i'];?></strong>
                           <span id="<?php echo $ls_value['id'];?>" class="status_filter"><?php echo $ls_value['status_name'];?></span>
                        </div>
                     </div>
                   
                     <?php 
                     $cl++;//for css color
                      } ?>
                   </div>
                  </div>
                  <input type="hidden" name="leads_status_us" id="leads_status_us" value="">
                  
                  <div class="lead-summary3 client_section3 floating_set sortrec table-responsive">
                  <div class="sort-by floating_set">
                     <span>Sort By:</span> <strong> <span id="date_created" class="sortby_filter"> Data Createds </span> <span id="kan_ban_order" class="sortby_filter"> Kan Ban Order </span> <span id="last_contact" class="sortby_filter"> Last Contact </span></strong>
                  </div>
                     <table class="table client_table1 text-center display dataTable nowrap" id="all_leads" cellspacing="0" width="100%">
                        <thead>
                           <tr class="text-uppercase">
                              <th class="lead_chk_TH Exc-colvis">
                
                
                <label class="custom_checkbox1">
                                <input type="checkbox" id="select_all_leads">
                                <i></i>
                                </label>
                               
                </th>
                              <th class="s_no_TH hasFilter">SNO<div class="sortMask"></div> 
                              </th>
                              <th  class="name_TH hasFilter">Name
                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                <div class="sortMask"></div> 
                               
                              </th>
                              <!--   <th style="display: none;">Name
                                  <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                </th> -->
                              <th class="company_TH hasFilter">Company
                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                <div class="sortMask"></div> 
                               
                              </th>
                              <th class="email_TH hasFilter">Email
                                <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                <div class="sortMask"></div> 
                                
                              </th>
                              <th class="phone_TH hasFilter">Phone
                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                <div class="sortMask"></div> 
                               
                                </th>
                              <th class="tags_TH hasFilter">Tags
                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                <div class="sortMask"></div> 
                               
                                </th>
                              <th class="assigned_TH hasFilter">Assigned
                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                <div class="sortMask"></div> 
                               

                              <!-- <th style="display: none;">Assigned
                                <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
                              </th>
                              <th class="status_TH hasFilter">status
                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                

                               
                                     <select class="filter_check" multiple="" style="display: none;">
                                         <?php foreach($leads_status as $ls_value) { ?>
                                  <option value='<?php echo $ls_value['status_name'];?>' ><?php echo $ls_value['status_name'];?></option>
                                  <?php } ?>
                                        </select>
                               <div class="sortMask"></div> 
                        
                               </th>
                              <th class="source_TH hasFilter">source
                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                <div class="sortMask"></div> 


                                
                                </th>
                              <!--  <th style="display: none;">status
                                <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                               </th> -->
                              <th class="contact_date_TH hasFilter">contact Date
                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                <div class="sortMask"></div> 
                               
                                </th>
                              <th class="action_TH Exc-colvis">Action
                              
                                </th>
                           </tr>
                        </thead>
                       <!--  <tfoot class="ex_data1">
                        <tr>
                        <th>
                        </th>
                        </tr>
                        </tfoot>  -->                      
                        <tbody>
                           <?php $i =1 ; foreach ($leads as $key => $value) {
                            $getUserName=array();
                              // $getUserProfilepic = $this->Common_mdl->getUserProfilepic($value['assigned']); 
                              $getUserProfilepic=array();
                              $ex_assign=explode(',',$value['assigned']);
                              foreach ($ex_assign as $ex_key => $ex_value) {
                                 $getUserProfilepic[] = $this->Common_mdl->getUserProfilepic($ex_value);
                                  $getUserName[] = $this->Common_mdl->getUserProfileName($ex_value);                                 
                              }
                              $ex_team=explode(',',$value['team']);
                              $ex_dept=explode(',', $value['dept']);
                              /*if($value['lead_status']==1)
                              {
                                $value['lead_status'] = 'Customer';
                              }else{
                                $value['lead_status'] = '';
                              }*/
                               $status_name = $this->Common_mdl->GetAllWithWhere('leads_status','id',$value['lead_status']);
                               if(!empty($status_name))
                               {
                                $statusname = $status_name[0]['status_name'];
                               }else{
                                $statusname = '-';
                               }
                            

                                    $leads_source=array();
                                    $res_source='';
                                    if($value['source']!=''){
                                        foreach (explode(',',$value['source']) as $source_key => $source_value) {
                                           $source_name = $this->Common_mdl->GetAllWithWhere('source','id',$source_value);
                                             if(!empty($source_name)){
                                           $leads_res = $source_name[0]['source_name'];
                                            if($leads_res!='')
                                            {
                                              $leads_source[] = $leads_res;
                                            }
                                           }
                                          }
                                      }
                                      if(count($leads_source)>0)
                                      {
                                        $res_source= implode(',',$leads_source);
                                      }
                                      else
                                      {
                                        $res_source= "-";
                                      }
                              ?>
                           <tr>
                              <td>
                                 <label class="custom_checkbox1">
                                     <?php
                              /** 16-06-2018 **/
                              ?>
                                    <input type="checkbox" class="leads_checkbox" data-leads-id="<?php echo $value['id'];?>">
                                    <i></i>
                                <?php  ?>
                                    </label>
                                 </div>
                               
                              </td>

                              <td data-search="<?php echo $i; ?>"><?php echo $i;?></td>
                              <td data-search="<?php echo $value['name']; ?>">
                                 <!-- <a href="#" data-toggle="modal" data-target="#profile_lead_<?php echo $value['id'];?>"><?php echo $value['name'];?></a> -->
                                <?php  if($_SESSION['permission']['Leads']['view']=='1'){  ?>
                                 <a href="<?php echo base_url().'leads/leads_detailed_tab/'.$value['id'];?>" > <?php }else{ ?>
                                 <a href="javascript:;"><?php } ?>
                                 <?php echo $value['name'];?></a>
                              </td>
                                <!-- <td style="display: none;">                                
                                 <?php echo $value['name'];?>
                              </td> -->
                            
                              <?php 
                              //echo $value['company'];
                              if(is_numeric($value['company']))
                              {
                                 $client_query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and id='".$value['company']."' order by id desc ")->result_array();
                                 if(count($client_query)>0)
                                 {
                                  $companyName=$client_query[0]['crm_company_name'];
                                 }
                                 else
                                 {
                                  $companyName='-';
                                 }
                              }
                              else
                              {
                                 $companyName=$value['company'];
                              }                                
                              ?>
                                <td  data-search="<?php echo $companyName; ?>" >
                                <?php echo $companyName; ?>
                              </td>

                              <td data-search="<?php echo $value['email_address']; ?>"><?php echo $value['email_address'];?></td>

                              <td data-search="<?php echo $value['phone']; ?>"><?php echo $value['phone'];?></td>

                              <td data-search="<?php echo $value['tags']; ?>"><?php echo $value['tags'];?></td>

                              <td id="task_<?php echo $value['id'];?>" class="user_imgs" data-search=" <?php echo implode(',',$getUserName); ?>" >
                                         
                                              <?php
                                               /** 16-06-2018 **/
                             if($_SESSION['permission']['Leads']['edit']=='1'){ ?>
                                   <a href="javascript:;" data-toggle="modal" data-target="#adduser_<?php echo $value['id'];?>" class="adduser1 user_change"><i class="fa fa-plus"></i></a>
                                   <?php } ?>

                              <?php foreach ($getUserProfilepic as $pro_img_key => $pro_img_value) {
                                 ?>
                                 <!--  <img src="<?php echo $pro_img_value;?>" alt="img" height="50" width="50"> -->
                                 <?php
                              } ?>
                          
                  
                                <?php  echo implode(',',$getUserName);

                                $ex_val=Get_Module_Assigees( 'LEADS' ,$value['id'] );
                                $var_array=array();
                                 if(count($ex_val)>0)
                                         {
                                          foreach ($ex_val as $key => $value1) {
                                        
                                           $user_name = $this->Common_mdl->select_record('user','id',$value1);
                                            array_push($var_array, $user_name['crm_name']);
                                           }
                                          
                                         }


                                echo implode(',',$var_array);



                                  //} ?>
                            <!--   <img src="<?php //echo $getUserProfilepic;?>" alt="img" height="50" width="50"> -->
                              </td>

                              <!-- <td style="display: none;">
                              <?php 

                               // echo implode(',',$getUserName);    
                            //} ?>                           
                              </td> -->
                              <td class="lead_status_<?php echo $value['id'];?>" data-search="<?php 
                                    foreach($leads_status as $ls_value) { 
                                      if($value['lead_status']==$ls_value['id']){
                              echo $ls_value['status_name'];
                               } } ?>">
                              <?php //echo $statusname;?>
                                 <select name="lead_status_new" id="lead_status_new<?php echo $value['id'];?>" class="change_lead_status" data-id="<?php echo $value['id'];?>" placeholder="Nothing Selected" <?php if($_SESSION['id']!=$value['user_id']){ echo "disabled"; } ?>  <?php  if($_SESSION['permission']['Leads']['edit']!='1'){ ?> disabled="disabled" <?php  } ?>>
                           <!-- <option value="">Nothing selected</option> -->
                                    <?php foreach($leads_status as $ls_value) { ?>
                                  <option data-statusname="<?php echo $ls_value['status_name'];?>" value='<?php echo $ls_value['id'];?>'
                                  <?php if($value['lead_status']==$ls_value['id']){ echo 'selected="selected"'; }?>  ><?php echo $ls_value['status_name'];?></option>
                                  <?php } ?>

                                  </select>
                              </td>

                              <!-- <td style="display: none;"> 
                                    <?php foreach($leads_status as $ls_value) { ?>
                                 <?php  if($value['lead_status']==$ls_value['id']){  echo $ls_value['status_name']; }?>
                                  <?php } ?>                               
                              </td> -->
                              
                              <td id="source_<?php echo $value['id'];?>" class="source-plus" data-search="<?php echo $res_source;?>">
                                 <?php
                              /** 16-06-2018 **/
                              if($_SESSION['permission']['Leads']['edit']=='1'){ ?>
                              <a href="javascript:;" data-toggle="modal" data-target="#adduser_source_<?php echo $value['id'];?>" class="adduser1"><i class="fa fa-plus"></i></a>
                              <?php } ?>

                              <?php echo $res_source;?>
                               
                              </td>

                               <!-- <td style="display: none;"><?php echo $res_source;?>                              
                              </td> -->
                              <td data-search="<?php echo $value['contact_date']; ?>"><?php echo $value['contact_date'];?></td>
                              <td>
                                <?php
                              /** 16-06-2018 **/
                             //  if($_SESSION['id']==$value['user_id']){ ?>
                                 <p class="action_01">
                                  <?php  if($_SESSION['permission']['Leads']['delete']=='1'){ ?>
                                  <a href="javascript:void(0)" data-toggle="modal" data-target="#leads_deleteconfirmation" onclick="delete_leads(this)" data-id="<?php echo $value['id'];?>"><i class="fa fa-trash fa-6" aria-hidden="true" ></i></a>
                            
                                  <?php  } ?> 
                                   <?php  if($_SESSION['permission']['Leads']['edit']=='1'){ ?>
                                    <a href="<?php echo base_url().'leads/edit_lead_view/'.$value['id'].'';?>" ><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                    <?php } ?>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#confirm_archive" onclick="archive_leads(this)" data-id="<?php echo $value['id'];?>"><i class="fa fa-archive archieve_click" aria-hidden="true" ></i></a>
                                    <!-- end page edit -->
                                 </p>
                               
                                 <?php //} ?>
                              </td>                            
                           </tr>                           

                            <div id="adduser_<?php echo $value['id'];?>" class="add-user-assign1 modal fade" role="dialog">
                            <div class="modal-dialog">
                               <!-- Modal content-->
                               <div class="modal-content">
                                  <div class="modal-header">
                                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                                     <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
                                  </div>
                                  <div class="modal-body dash_tree">
                                       <input type="text" class='tree_select' id="tree_select" name="assignees[]"  placeholder="Select">     
                                       <input type="hidden" id="adduser_txt_<?php echo $value['id'];?>" name="assign_role">
                                       

                                  </div>
                                  <div class="modal-footer profileEdit">
                                     <input type="hidden" name="hidden">
                                     <a href="javascript:void();" id="acompany_name" data-dismiss="modal" data-id="<?php echo $value['id'];?>" class="save_assign_staff">save</a>
                                  </div>
                               </div>
                            </div>
                         </div>



                                                      <div id="task_assign" class="modal fade" role="dialog">
                                                      <div class="modal-dialog">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                               <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                               <!--  <label>assign to </label> -->
                                                               <div class="dropdown-sin-2 lead-form-st">

                                                               <input type="hidden" name="task_id" id="task_id" value=""> 

                                        <select multiple placeholder="select" name="workers[]" id="workers<?php echo $value['id'];?>" class="workers_assign">
                                       <?php  if(count($staff_form)){ ?>
                                                                  <option disabled>Staff</option>
                                                                     <?php foreach($staff_form as $s_key => $s_val){ ?>
                                                                      <option value="<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$ex_assign )) {?> selected="selected"<?php } ?> ><?php echo $s_val['crm_name'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                                              <?php if(count($team)){ ?>
                                                                  <option disabled>Team</option>
                                                                     <?php foreach($team as $s_key => $s_val){ ?>
                                                                      <option value="tm_<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$ex_team )) {?> selected="selected"<?php } ?> ><?php echo $s_val['team'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                                                  <?php if(count($department)){ ?>
                                                                  <option disabled>Department</option>
                                                                     <?php foreach($department as $s_key => $s_val){ ?>
                                                                      <option value="de_<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$ex_dept )) { ?> selected="selected"<?php } ?> ><?php echo $s_val['new_dept'];?></option>
                                                                     <?php } 
                                                                     } ?> ?>
                                                                 </select>
                                                               </div>
                                                            </div>
                                                            <div class="modal-footer profileEdit">
                                                               <input type="hidden" name="hidden">
                                                               <a href="javascript:void();" id="acompany_name"  data-id="<?php echo $value['id'];?>" class="assigned_staff">save</a>
                                                                <a href="javascript:;" data-dismiss="modal">Close</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>



                                                  <div id="adduser_source_<?php echo $value['id'];?>" class="add-user-assign1 modal fade" role="dialog">
                                                      <div class="modal-dialog">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                               <h4 class="modal-title" style="text-align-last: center">Source</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                               <!--  <label>assign to </label> -->
                                                               <div class="dropdown-sin-33">
                                                                  <select multiple placeholder="select" name="source_new[]" id="source_new<?php echo $value['id'];?>" class="workers">
                                                                     <?php  foreach($source as $lsource_value) { ?>
                                 <option value='<?php echo $lsource_value['id'];?>' <?php  if(in_array($lsource_value['id'], explode(',',$value['source']))){ echo "selected='selected'"; }?> ><?php echo $lsource_value['source_name'];?></option>
                                 <?php 
                                  } ?>
                                                                  </select>
                                                               </div>
                                                            </div>
                                                            <div class="modal-footer profileEdit">
                                                               <input type="hidden" name="hidden">
                                                               <a href="javascript:void();" id="acompany_name" data-dismiss="modal" data-id="<?php echo $value['id'];?>" class="save_assign_source">save</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                           <?php $i++; } ?>
                        </tbody>
                     </table>
        <input type="hidden" class="rows_selected" id="select_leads_count" >              
                  </div>
               </div>
               <!-- lead-summary2 -->
            </div>
            <!-- dashboard -->
            <!-- new lead -->
         
            <!-- new lead-->
            <!-- for leads source section -->
            <div id="leads_source" style="display:none;" class="lead-source-tsk tap-content lead-summary2 floating_set new-leadss cmn-table-height01 <?php if($_SESSION['permission']['Leads_source'][0]['view']!=1){ ?>permission_deined <?php } ?>">
               <div class="pull-right csv-sample01 spl12">
               <?php if($_SESSION['permission']['Leads_source'][0]['create']==1){ ?> 
                  <a class="btn btn-card btn-primary leadsG" data-toggle="modal" data-target="#leads_source_add"><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Add New</a><?php } ?>
                  <button type="button" data-toggle="modal" data-target="#source_deleteconfirmation" id="delete_source_button" class="delete_source btn btn-card btn-primary leadsG del-tsk12 f-right" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>
               </div>
               <div class="all-usera1 user-dashboard-section1 leads_source">
                  <table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
                     <thead>
                        <tr class="text-uppercase">
                           <th class="source_chk_TH Exc-colvis">
                            <label class="custom_checkbox1">
                                <input type="checkbox" id="select_all_source">
                            <i></i>
                                </label>
                               </th>
                           <th class="s_no_TH">SNO</th>
                           <th class="source_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                <div class="sortMask"></div> Source</th>
                           <th class="action_TH Exc-colvis">Actions</th>
                        </tr>
                     </thead>
                 <!--       <tfoot class="ex_data1">
                          <tr>
                          <th>
                          </th>
                          </tr>
                          </tfoot> -->
                     <tbody>
                        <?php $i =1 ; foreach ($source as $key => $value) {
                           ?>
                        <tr>
                           <td>
                            <label class="custom_checkbox1">
                                <input type="checkbox" class="source_checkbox" data-source-id="<?php echo $value['id'];?>">
                                <i></i>
                                </label>
                              
                            </td> 
                           <td><?php echo $i;?></td>
                           <td data-search="<?php echo $value['source_name'];?>"><?php echo $value['source_name'];?></td>
                           <td>
                              <p class="action_01">
                                 <!-- <a href="<?php echo base_url().'leads/source_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> -->
                                 <!--     <a href="<?php echo base_url().'leads/update_source/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a> -->
                                 <!-- <a href="javascript:void(0)" data-id="<?php echo $value['id'];?>" class="leads_source_delete"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> --> 

                                 <a href="javascript:;" data-toggle="modal" data-target="#source_deleteconfirmation" onclick="delete_source(this);" data-id='<?php echo $value['id'];?>'><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>

                                 <a data-toggle="modal" data-target="#leads_source_edit_<?php echo $value['id']?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>

                                 <!-- <a data-toggle="modal" data-target="#my_Modal_<?php echo $value['id']?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a> -->
                             </p>

                              
                           </td>
                              
                        </tr>
                        <?php $i++; } ?>
                     </tbody>
                  </table>
        <input type="hidden" class="rows_selected" id="select_source_count" >                        
               </div>
               <div id="leads_source_add" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title">Add New Source</h4>
                        </div>
                        <div class="modal-body">
                           <div class=" new-task-teams">
                              <!-- <form id="add_new_team" method="post" action="<?php echo base_url()?>/leads/add_new_source" enctype="multipart/form-data"> -->
                              <div class="form-group">
                                 <label>New Source</label>
                                 <input type="text" class="form-control" name="new_team" id="new_lead_source" placeholder="Enter Source name" value="">
                                 <label for="new_team" generated="true" class="source_error add_source_error" style="color: red;"></label>
                              </div>
                              <!-- <div class="form-group create-btn">
                                 <input type="submit" name="add_task" id="leads_source_save" class="btn-primary" value="create"/>
                              </div> -->
                              <!--  </form> -->
                           </div>
                        </div>
                        <div class="modal-footer">
                                 <a href="#" data-dismiss="modal">Close</a>
                               <input type="submit"  name="add_task" id="leads_source_save" class="btn-primary" value="create"/>
                           </div>
                     </div>
                  </div>
               </div>
               <!-- source edit -->
               <div class="edit_source">
                  <?php foreach ($source as $key => $value) { ?>
                  <div id="leads_source_edit_<?php echo $value['id'];?>" class="modal fade" role="dialog">
                     <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                           <div class="modal-header">
                              <button type="button" class="close close_<?php echo $value['id'];?>" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Edit Source</h4>
                           </div>
                           <div class="modal-body">
                              <div class=" new-task-teams">
                                 <!-- <form id="add_new_team" method="post" action="<?php echo base_url()?>/leads/add_new_source" enctype="multipart/form-data"> -->
                                 <div class="form-group">
                                    <label>New Source</label>
                                    <input type="text" class="form-control" name="new_team" id="new_lead_source_<?php echo $value['id'];?>" placeholder="Enter Source name" value="<?php echo $value['source_name'];?>">
                                    <label for="new_team" generated="true" class="edit_source_error_<?php echo $value['id'];?>" style="color: red;"></label>
                                 </div>
                                 <!-- <div class="form-group create-btn">
                                    <input type="submit" name="add_task" data-id="<?php echo $value['id']?>" class="btn-primary edit_source_save" value="Update"/>
                                 </div> -->
                                 <!--  </form> -->
                              </div>
                           </div>
                           <div class="modal-footer">
                                 <a href="#" data-dismiss="modal">Close</a>
                                <input type="submit" name="add_task" data-id="<?php echo $value['id']?>" class="btn-primary edit_source_save" value="Update"/>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php } ?>
               </div>
               <!-- end source -->
            </div>
            <!-- end of leads source -->
            <!-- for leads status section -->
            <div id="leads_status" style="display: none;" class="lead-source-tsk tap-content lead-summary2 floating_set new-leadss cmn-table-height01 <?php if($_SESSION['permission']['Lead_Status'][0]['view']!=1){ ?> permission_deined <?php } ?>">
               <div class="pull-right csv-sample01 spl12 ">
               <?php
               if($_SESSION['permission']['Lead_Status'][0]['create']!=1){ ?>
                  <a class="btn btn-card btn-primary leadsG" data-toggle="modal" data-target="#leads_status_add"><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Add New</a><?php } ?>
                  <button type="button" data-toggle="modal" data-target="#status_deleteconfirmation"  id="delete_status_button" class="delete_status btn btn-card btn-primary leadsG del-tsk12 f-right" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>
               </div>
               <div class="all-usera1 user-dashboard-section1 leads_status">
                  <table class="table client_table1 text-center display nowrap" id="alltask_status" cellspacing="0" width="100%">
                     <thead>
                        <tr class="text-uppercase">
                           <th class="status_chk_TH Exc-colvis">
                            <label class="custom_checkbox1">
                                <input type="checkbox" id="select_all_status">
                                <i></i>
                                </label>
                              
                            </th>
                           <th class="s_no_TH">SNO</th>
                           <th class="status_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  Status</th>
                           <th class="action_TH Exc-colvis">Actions</th>
                        </tr>
                     </thead>
             <!--           <tfoot class="ex_data1">
                        <tr>
                        <th>
                        </th>
                        </tr>
                        </tfoot> -->
                     <tbody>
                        <?php $i =1 ; foreach ($leads_status as $key => $value) {
                           ?>
                        <tr>
                           <td>
                             <label class="custom_checkbox1">
                                <input type="checkbox" class="status_checkbox" data-status-id="<?php echo $value['id'];?>">
                                <i></i>
                                </label>
                             
                            </td>
                           <td><?php echo $i;?></td>
                           <td data-search="<?php echo $value['status_name'];?>"><?php echo $value['status_name'];?></td>
                           <td>
                              <p class="action_01">
                                 <!--  <a href="<?php echo base_url().'leads/s_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                    <a href="<?php echo base_url().'leads/update_status/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a> -->
                                <!--  <a href="javascript:void(0)" data-id="<?php echo $value['id'];?>" class="leads_status_delete"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>  -->
                                  <a href="javascript:;" data-toggle="modal" data-target="#status_deleteconfirmation" onclick="delete_status(this);" data-id='<?php echo $value['id'];?>'><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                 <a data-toggle="modal" data-target="#leads_status_edit_<?php echo $value['id']?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                              </p>
                           </td>
                              
                        </tr>
                        <?php $i++; } ?>
                     </tbody>
                  </table>
          <input type="hidden" class="rows_selected" id="select_status_count" >         
               </div>
               <div id="leads_status_add" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title">Add New Status</h4>
                        </div>
                        <div class="modal-body">
                           <div class=" new-task-teams">
                              <!-- <form id="add_new_team" method="post" action="<?php echo base_url()?>/leads/add_new_source" enctype="multipart/form-data"> -->
                              <div class="form-group">
                                 <label>New Status</label>
                                 <input type="text" class="form-control" name="new_team" id="new_lead_status" placeholder="Enter Status name" value="">
                                 <label for="new_team" generated="true" class="source_error add_status_error" style="color: red;"></label>
                              </div>
                              <!-- <div class="form-group create-btn">
                                 <input type="submit" name="add_task" id="leads_status_save" class="btn-primary" value="create"/>
                              </div> -->
                              <!--  </form> -->
                           </div>
                        </div>
                        <div class="modal-footer">
                                 <a href="#" data-dismiss="modal">Close</a>
                                 <input type="submit"  name="add_task" id="leads_status_save" class="btn-primary" value="create"/>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- source edit -->
               <div class="edit_status">
                  <?php foreach ($leads_status as $key => $value) { ?>
                  <div id="leads_status_edit_<?php echo $value['id'];?>" class="modal fade" role="dialog">
                     <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                           <div class="modal-header">
                              <button type="button" class="close close_<?php echo $value['id'];?>" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Edit Status</h4>
                           </div>
                           <div class="modal-body">
                              <div class=" new-task-teams">
                                 <!-- <form id="add_new_team" method="post" action="<?php echo base_url()?>/leads/add_new_source" enctype="multipart/form-data"> -->
                                 <div class="form-group">
                                    <label>New Status</label>
                                    <input type="text" class="form-control" name="new_team" id="new_lead_status_<?php echo $value['id'];?>" placeholder="Enter Source name" value="<?php echo $value['status_name'];?>">
                                    <label for="new_team" generated="true" class="edit_status_error_<?php echo $value['id'];?>" style="color: red;"></label>
                                 </div>
                                 <!-- <div class="form-group create-btn">
                                    <input type="submit" name="add_task" data-id="<?php echo $value['id']?>" class="btn-primary edit_status_save" value="Update"/>
                                 </div> -->
                                 <!--  </form> -->
                              </div>
                           </div>
                            <div class="modal-footer">
                                 <a href="#" data-dismiss="modal">Close</a>
                                 <input type="submit" name="add_task" data-id="<?php echo $value['id']?>" class="btn-primary edit_status_save" />
                            </div>

                        </div>
                     </div>
                  </div>
                  <?php } ?>
               </div>
            </div>
            <!-- end of leads status -->
            <!-- switch kanban -->
            <div id="kanban_tab" style="display: none;" class="tap-content  lead-summary2 floating_set new-leadss">

               <div class="lead-summary2 floating_set new-kanban01">
                  <div class="sort-by floating_set">
                     <span>Sort By:</span> 
                     <strong>
                        <span id="date_created" class="sortby_filter_kanban"> Data Created </span> <!--already had but include it --><span id="kan_ban_order" class="sortby_filter_kanban"> Kan Ban Order </span><!-- end -->  <span id="last_contact" class="sortby_filter_kanban"> Last Contact </span>
                     </strong>
                  </div>
                  <!--success message -->
                  <div class="status_succ status_msg" style="display: none;"><div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Status have been changed successfully...</span></div></div></div></div>
                  <!-- success message -->
                  <div class="lead-summary3 floating_set sortby_kanban new-change-summary">
          
                                          
                     <?php foreach($leads_status as $ls_value) { ?>
                     <div class="new-row-data1 width-junk">
<div class="new-quote-lead">
                        <div class="junk-box">
                           <div class="trial-stated-title">
                              <h4> <?php echo $ls_value['status_name'];?></h4>
                           </div>
                           <div class="back-set02 draglist" id="sit-drag_<?php echo $ls_value['id'];?>">
                              <?php 
                                 $ls_id = $ls_value['id'];
                                 /** 13-07-2018 for permission **/
      
/** for permission 16-07-2018 **/

/** end of permission 16-07-2018 **/
//                          $leads_rec=$this->db->query("SELECT * FROM leads li where li.user_id=".$it_session_id." and li.lead_status='".$ls_id."'
// UNION
// SELECT * FROM leads lis where lis.user_id!=".$it_session_id." and lis.public='on' and lis.lead_status='".$ls_id."'
// UNION
// SELECT * FROM leads tls where tls.user_id!=".$it_session_id." and tls.lead_status='".$ls_id."' and (FIND_IN_SET('".$it_session_id."',assigned) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `dept`, ',') REGEXP ',($res_dept_num),') ) ")->result_array();
                               // $leads_rec = $this->db->query("select * from leads where lead_status=".$ls_id." and user_id=".$_SESSION['id']." ")->result_array();
$result=array();
   array_push($result, $it_session_id);
   $reassign_firm_id=$its_firm_admin_id;
 $tot_val=1;
    for($z=0;$z<$tot_val;$z++)
    {
      if($reassign_firm_id==0)
      {
        $tot_val=0;
      }
      else
      {
          $get_this_qry=$this->db->query("select * from user where id=".$reassign_firm_id)->row_array();
          if(!empty($get_this_qry)){
             array_push($result, $get_this_qry['id']);
          $new_created_id=$get_this_qry['firm_admin_id'];
          $reassign_firm_id=$get_this_qry['firm_admin_id'];
          }
          else
          {
            $tot_val=0;
          }
         
      }

    }
     $res=(!empty($result) && count($result) > 1)?implode(',',array_filter(array_unique($result))):'0';
if($_SESSION['role']==6)
{
  $leads_rec=$this->db->query("SELECT * FROM leads li where li.user_id=".$it_session_id." and  li.lead_status='".$ls_id."' 
UNION
SELECT * FROM leads lis where lis.user_id in($res) and public='on' and lis.lead_status='".$ls_id."'
UNION
SELECT * FROM leads tls where tls.user_id in($res) and (FIND_IN_SET('".$it_session_id."',assigned) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `dept`, ',') REGEXP ',($res_dept_num),') ) and tls.lead_status='".$ls_id."'
")->result_array();
}
else if($_SESSION['role']==4)
{
   
  $sql=$this->db->query("select * from client where user_id=".$_SESSION['id']." ")->row_array();
  $its_id=$sql['id'];
  $leads_rec=$this->db->query("SELECT * FROM leads li where li.company=".$its_id." and  li.lead_status='".$ls_id."'  UNION
SELECT * FROM leads lis where lis.user_id in($res) and public='on' and  lis.lead_status='".$ls_id."' ")->result_array();

}
else if($_SESSION['role']==5 || $_SESSION['role']==3 || $_SESSION['role']==2)
{

 // $res=implode(',', $over_all_res);
$leads_rec=$this->db->query("SELECT * FROM leads li where li.user_id in ($res) and li.lead_status='".$ls_id."'
UNION
SELECT * FROM leads lis where lis.user_id in($res) and public='on' and lis.lead_status='".$ls_id."'")->result_array();
}
else
{
 
 // $res=implode(',', $over_all_res);
$leads_rec=$this->db->query("SELECT * FROM leads li where li.user_id in ($res) and li.lead_status='".$ls_id."'
UNION
SELECT * FROM leads lis where lis.user_id in($res) and public='on' and lis.lead_status='".$ls_id."' ")->result_array();
   // $data['leads']=$this->db->query("SELECT * FROM leads li where li.user_id=".$it_session_id." ")->result_array();
}
                                 if(!empty($leads_rec)){
                                 foreach ($leads_rec as $key => $value) {
                                      $getUserProfilepic = $this->Common_mdl->getUserProfilepic($value['assigned']); 
                                 
                                     /* if($value['source']==1)
                                                         {
                                                            $value['source'] = 'Google';
                                                         }elseif($value['source']==2)
                                                         {
                                                            $value['source'] = 'Facebook';
                                                         }else{
                                                            $value['source'] = '';
                                                         }*/
                                 
                                                          $source_name = $this->Common_mdl->GetAllWithWhere('source','id',$value['source']);
                                                         $value['source'] = $source_name[0]['source_name'];
                                                         if($value['source']!='')
                                                         {
                                                            $value['source'] = $value['source'];
                                                         }else{
                                                            $value['source'] = '-';
                                                         }
                                 
                                    ?>
                              <div class="user-leads1" id="<?php echo $value['id'] ?>" >
                                 <!-- <h3 data-target="#profile_lead_<?php echo $value['id'];?>" data-toggle="modal"><img src="<?php echo $getUserProfilepic;?>" alt="img">
                                    <?php echo "#".$value['id'].'-'.$value['name'];?> 
                                 </h3> -->
                                  <h3> <a href="<?php echo base_url().'leads/leads_detailed_tab/'.$value['id'];?>" >
                                    <span><?php echo $value['name'];?></span> <img src="<?php echo $getUserProfilepic;?>" alt="img">
                                   <strong><?php echo "#".$value['id'].'-'.$value['name'];?> </strong> 
                                 </a></h3> 
                                 <div class="source-google">
                                    <div class="source-set1">
                                       <span>Source <?php echo $value['source'];?></span>
                                    </div>
                                    <div class="source-set1 source-set3 ">
                                       <span>Created <?php  $date=date('Y-m-d H:i:s',$value['createdTime']);
                                          echo time_elapsed_string($date);?></span>
                                    </div>
                                    <!-- rspt 18-04-2018 for get attached count -->
                                    <?php 
                                       $res=$this->db->query("select * from leads_attachments where id=".$value['id']."")->result_array();
                                       //echo count($res)."count count";
                                        ?>
                             <!--        <div class="text-mutes">
                                       <div class="icon-card d-inline-block ">
                                          <i class="icofont icofont-share text-muted "></i>
                                          <span class="text-muted m-l-10 ">
                                          <?php /** for attched count */
                                             echo count($res);
                                             ?>                            
                                          </span>
                                       </div>
                                       <div class="icon-card d-inline-block p-l-20 ">
                                          <i class="icofont icofont-heart-alt text-c-pink "></i>
                                          <span class="text-c-pink m-l-10 ">0</span>
                                       </div>
                                    </div> -->
                                    <!-- rspt 18-04-2018 for tags shown-->
                                    <div class="tinytiny">
                                       <strong>
                                       <?php $tag =explode(',', $value['tags']);
                                          foreach ($tag as $t_key => $t_value) { ?>
                                       <a href="javascript:;"><?php echo $t_value;?></a><?php } ?>
                                       </strong>
                                    </div>
                                    <!-- end 18-04-2018 -->
                                 </div>
                              </div>
                              <?php }} else{ ?> 
                             <!--  <div class="new-row-data1 col-xs-12 col-sm-4"> No records Found </div> -->
                              <?php } ?>
                           </div>
                        </div>
                     </div>
                   </div>
                     <?php } ?>
                 
               </div>
               <!-- end kanban -->
               <!-- import lead -->
               <!-- <div id="import_lead" class="tab-pane fade lead-summary2 floating_set new-leadss">
                  <div class="newbies">
                    <div class="download-sample">
                       <a href="<?php echo base_url()?>uploads/csv/sample_import_leads.csv">DOWNLOAD SAMPLE</a>
                    </div>
                    <div class="term-condition1">
                       <p>1. Your CSV data should be in the format below. The first line fof your CSV file should be the column headers as in the table example. Also make sure that your file is UTF_8 to avoid unnecessary encoding problems.</p>
                       <p>2. if the column you are trying to import is date make sure that is formatted in format Y-m-d(2017-10-19)</p>
                       <p class="dupli-data">3. Dupolicate email rows wont be imported</p>
                    </div>
                    <div class="table-lead1">
                       <table>
                          <thead>
                             <tr>
                                <th>Name</th>
                                <th>Company</th>
                                <th>Position</th>
                                <th>Description</th>
                                <th>Country</th>
                                <th>Zip</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Website</th>
                                <th>Phonenumber</th>
                                <th>Tags</th>
                                <th>Public</th>
                                <th>Contact Today</th>
                                <th>Contact Date</th>
                             </tr>
                          </thead>
                          <tbody>
                             <tr>
                                <td>Test</td>
                                <td>Test Company</td>
                                <td>Client</td>
                                <td>Sample Description</td>
                                <td>Test Country</td>
                                <td>698523</td>
                                <td>Test City</td>
                                <td>Test State</td>
                                <td>28, 15th Lane, City</td>
                                <td>test@gmail.com</td>
                                <td>www.testcompany.com</td>
                                <td>9874563210</td>
                                <td>tag1,tag2</td>
                                <td>on / off</td>
                                <td>on / off</td>
                                <td>2018-03-21</td>
                             </tr>
                          </tbody>
                       </table>
                    </div>
                    <form name="import_leads" id="import_leads" action="<?php echo base_url()?>leads/import_leads" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="kanpan" id="kanpan" value="0">
                    <div class="file-upload-lead">
                       <div class="formdata1">
                          <label>choose CSV File</label>
                          
                          <input type="file" name="file" id="file" accept=".csv"/>
                       </div>
                       <div class="formdata1">
                          <label>Source</label>
                          <select name="source" id="source">
                             <option value=''>Nothing selected</option>
                             <?php foreach($source as $lsource_value) { ?>
                             <option value='<?php echo $lsource_value['id'];?>'><?php echo $lsource_value['source_name'];?></option>
                             <?php } ?>
                           
                          </select>
                       </div>
                       <div class="formdata1">
                          <label>Status</label>
                          <select name="lead_status" id="lead_status">
                             <option value="">Nothing selected</option>
                             <?php foreach($leads_status as $ls_value) { ?>
                             <option value='<?php echo $ls_value['id'];?>'><?php echo $ls_value['status_name'];?></option>
                             <?php } ?>
                          </select>
                       </div>
                       <div class="formdata1">
                          <label>Responsible</label>
                          <select name="assigned" id="assigned">
                             <option value="">Nothing selected</option>
                             <?php
                     if(isset($staff_form) && $staff_form!='') 
                     {
                       foreach($staff_form as $value) 
                       {
                     ?>
                             <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
                             <?php
                     }
                     }
                     ?>
                          </select>
                       </div>
                    </div>
                    <input type="hidden" name="kanpan" id="kanpan" value="0">
                    <div class="modal-footer">
                       <a href="#" data-dismiss="modal">Close</a>
                       <input type="submit" name="submit" value="SUBMIT">
                    </div>
                  </form>
                  </div>
                  </div> --> <!-- import lead -->
            </div>
            <!-- tab-content -->
         </div>
      </div>
   </div>
   <!-- leads-section -->
   <!-- Modal -->
   <div id="new-lead" class="modal fade <?php if($_SESSION['permission']['Leads']['create']!=1){ ?> permission_deined <?php } ?>" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Add New Lead</h4>
            </div>
            <div class="modal-body">
               <form id="leads_form" name="leads_form" action="<?php echo base_url()?>leads/addLeads" method="post">
                  <input type="hidden" name="kanpan" id="kanpan" value="0">
                  <div class="lead-popupsection1 floating_set">
                     <div class="col-xs-12 col-sm-3  lead-form1">
                        <label>Lead status</label>
                        <select name="lead_status" id="lead_status">
                           <option value="">Nothing selected</option>
                           <?php foreach($leads_status as $ls_value) { ?>
                           <option value='<?php echo $ls_value['id'];?>'><?php echo $ls_value['status_name'];?></option>
                           <?php } ?>
                        </select>
                     </div>
                     <div class="col-xs-12 col-sm-3  lead-form1">
                        <label>Source</label>
                        <select name="source" id="source">
                           <option value=''>Nothing selected</option>
                           <?php foreach($source as $lsource_value) { ?>
                           <option value='<?php echo $lsource_value['id'];?>'><?php echo $lsource_value['source_name'];?></option>
                           <?php } ?>
                           <!-- <option value='1'>Google</option>
                              <option value-'2'>Facebook</option> -->
                        </select>
                     </div>
                     <div class="col-xs-12 col-sm-3  lead-form1">
                        <label>Assigned</label>
                        <select name="assigned" id="assigned">
                           <option value="">Nothing selected</option>
                           <?php
                              if(isset($staff_form) && $staff_form!='') 
                              {
                               foreach($staff_form as $value) 
                               {
                              ?>
                           <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                     </div>
                     <div class="col-xs-12 col-sm-3  lead-form1">
                        <label>Notify to(Manager)</label>
                        <select name="review_manager[]" id="review_manager" multiple="multiple">
                           <option value="">Nothing selected</option>
                           <?php
                              if(isset($manager) && $manager!='') 
                              {
                               foreach($manager as $value) 
                               {
                              ?>
                           <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                     </div>
                  </div>
                  <div class="tags-allow1 floating_set">
                     <h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
                     <input type="text" value="" name="tags" id="tags" class="tags" />
                  </div>
                  <div class="lead-popupsection2 floating_set">
                     <div class="formgroup col-xs-12 col-sm-6">
                        <label>Name</label>
                        <input type="text" name="name" id="name">
                     </div>
                     <div class="formgroup col-xs-12 col-sm-6">
                        <label>Address</label>
                        <textarea rows="1" name="address" id="address"></textarea>
                     </div>
                     <div class="formgroup col-xs-12 col-sm-6">
                        <label>Position</label>
                        <input type="text" name="position" id="position">
                     </div>
                     <div class="formgroup col-xs-12 col-sm-6">
                        <label>City</label>
                        <input type="text" name="city" id="city">
                     </div>
                     <div class="formgroup col-xs-12 col-sm-6">
                        <label>Email Address</label>
                        <input type="text" name="email_address" id="email_address">
                     </div>
                     <div class="formgroup col-xs-12 col-sm-6">
                        <label>State</label>
                        <input type="text" name="state" id="state">
                     </div>
                     <div class="formgroup col-xs-12 col-sm-6">
                        <label>Website</label>
                        <input type="text" name="website" id="website">
                     </div>
                     <div class="formgroup col-xs-12 col-sm-6">
                        <label>Country</label>
                        <select name="country" id="country">
                           <option value=''>Nothing Selected</option>
                           <?php foreach ($countries as $key => $value) { ?>
                           <option value="<?php echo $value['id']?>"><?php echo $value['name'];?></option>
                           <?php }?>
                        </select>
                     </div>
                     <div class="formgroup col-xs-12 col-sm-6">
                        <label>Phone</label>
                        <input type="text" name="phone" id="phone">
                     </div>
                     <div class="formgroup col-xs-12 col-sm-6 zip-code-alert1">
                        <label>Zip Code</label>
                        <input type="number" name="zip_code" id="zip_code">
                     </div>
                     <div class="formgroup col-xs-12 col-sm-6
">
                        <label>Company</label>
                        <input type="text" name="company" id="company">

                     </div>
                     <div class="formgroup col-xs-12 col-sm-6">
                        <label>Default Language</label>
                        <select name="default_language" id="default_language">
                           <option value="">System Default</option>
                           <?php foreach ($Language as $l_key => $l_value) { ?>
                           <option value="<?php echo $l_value['id'];?>"><?php echo $l_value['name'];?></option>
                           <?php }?>
                        </select>
                     </div>
                     <div class="formgroup col-sm-12">
                        <label>Description</label>
                        <textarea rows="3" name="description" id="description"></textarea>
                     </div>
                     <div class="formgroup checkbox-select1 col-sm-12 border-checkbox-section">
                        <!-- <p>
                           <input type="checkbox" name="public" id="public">
                           <label>Public</label>
                           </p>
                           <p>
                           <input type="checkbox" name="contact_today" checked id="contact_today">
                           <label>Contacted Today</label>
                           </p> -->
                        <div class="border-checkbox-group border-checkbox-group-primary">
                           <input type="checkbox" name="public" id="public">
                           <label class="border-checkbox-label" for="public">Public</label>
                        </div>
                        <div class="border-checkbox-group border-checkbox-group-primary">
                           <input type="checkbox" name="contact_today" checked id="contact_today">
                           <label class="border-checkbox-label" for="contact_today">Contacted Today</label>
                        </div>
                     </div>
                     <div class="dedicated-sup col-sm-12 selectDate" style="display:none;">
                        <label><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Choose contact Date</label>
                        <input type="text" name="contact_date" id="contact_date" class="datepicker" placeholder="Select your date" />
                     </div>
                  </div>
            </div>
            <div class="modal-footer">
            <a href="#"  data-dismiss="modal">Close</a>
            <input type="submit" name="save" id="save" value="Save">
            </div>
            </form>
         </div>
      </div>
   </div>
   <!-- modal -->
   <!-- Edit modal-->
   <?php foreach ($leads as $key => $value) { ?>
   <?php /* ***********************************************************/
      $country = $value['country'];
      $l_status = $value['lead_status'];
      $status_name = $this->Common_mdl->select_record('leads_status','id',$l_status);
      $country_name = $this->Common_mdl->select_record('countries','id',$country);
      $languages = $this->Common_mdl->select_record('languages','id',$value['default_language']);
      $assigned = $this->Common_mdl->select_record('user','id',$value['assigned']);
      
      ?>
   <input type="hidden" id="leads_id" name="leads_id" value="<?php echo $value['id'];?>"> 
   <div class="modal fade show leads_popup" id="profile_lead_<?php echo $value['id'];?>" role="dialog">
      <div class="modal-dialog modal-md">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title"><?php echo "#".$value['id'].' - '.$value['name'];?></h4>
            </div>
            <div class="lead-deadline1">
               <div class="deadline-crm1  floating_set ">
                  <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                     <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab"  href="#profile_<?php echo $value['id'];?>">Profile</a>
                     </li>
                     <li class="nav-item it1" value="1"><a class="nav-link" data-toggle="tab" href="#proposal_<?php echo $value['id'];?>">Proposal</a></li>
                     <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tasks_<?php echo $value['id'];?>">Tasks</a></li>
                     <li class="nav-item "><a class="nav-link" data-toggle="tab" href="#attachments_<?php echo $value['id'];?>">Attachments</a></li>
                     <li class="nav-item  "><a class="nav-link" data-toggle="tab" href="#reminder_<?php echo $value['id'];?>">Reminders</a></li>
                     <li class="nav-item  "><a class="nav-link" data-toggle="tab" href="#notes_<?php echo $value['id'];?>">Notes</a></li>
                     <li class="nav-item "><a class="nav-link" data-toggle="tab" href="#activity-log_<?php echo $value['id'];?>">Activity Log</a></li>
                  </ul>
               </div>
            </div>
            <div class="profile-edit-crm">
               <div class="tab-content sort-leads1">
                  <div id="profile_<?php echo $value['id'];?>" class="tab-pane fade in active">
                     <div class="edit-section">
                        <div class="mark-lost1 floating_set">
                           <div class="pull-left edit-pull-left">
                              <ul>
                                 <li><a href="#" data-toggle="modal" data-target="#update_lead_<?php echo $value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"> Edit <i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></i></a></li>
                                 <li>
                                    <a href="#" class="edit-toggle1">More <span class="caret"></span></a>
                                    <ul class="proposal-down">
                                       <li><a href="#" id="lost" data-id="<?php echo $value['id'];?>" class="statuschange">Mark as lost</a></li>
                                       <li><a href="#" id="junk" data-id="<?php echo $value['id'];?>" class="statuschange"> Mark as junk</a></li>
                                       <li><a href="<?php echo base_url().'leads/delete/'.$value['id'].'/0';?>" onclick="return confirm('Are you sure want to delete');" > Delete Lead</a></li>
                                    </ul>
                                 </li>
                              </ul>
                           </div>
                           <!-- <div class="pull-right convert-custom">
                              <a href="#"> Convert to customer </a>
                              </div> -->
                        </div>
                     </div>
                     <div class="floating_set general-inform1">
                       <!--  <div class="alert alert-success succ" style="display:none;">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        </div> -->


                        <div class="modal-alertsuccess alert alert-success succ" style="display:none;">
                          <div class="newupdate_alert">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                           <div class="pop-realted1">
                           <div class="position-alert1">
                           Your task was successfully added.
                           </div></div>
                           </div>
                        </div>

                        <div class="col-sm-4 custom-fields11">
                           <div class="custom-danger">
                              <h4>Information</h4>
                              <div class="form-status1">
                                 <span>Lead Status</span>
                                 <strong class="leadstatus lead_status_<?php echo $value['id']; ?>"><?php echo $status_name['status_name'];?></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Position</span>
                                 <strong><?php echo $value['position'];?></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Email Address</span>
                                 <strong><a href="#"><?php echo $value['email_address'];?></a></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Website</span>
                                 <strong><a href="#"><?php echo $value['website'];?></a></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Phone</span>
                                 <strong><a href="#"><?php echo $value['phone'];?></a></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Company</span>
                                 <strong><?php echo $value['company'];?></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Address</span>
                                 <strong><?php echo $value['address'];?></strong>
                              </div>
                              <div class="form-status1">
                                 <span>City</span>
                                 <strong><?php echo $value['city'];?></strong>
                              </div>
                              <div class="form-status1">
                                 <span>State</span>
                                 <strong><?php echo $value['state'];?></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Country</span>
                                 <strong><?php echo $country_name['name'];?></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Zip code</span>
                                 <strong><?php echo $value['zip_code'];?></strong>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4 custom-fields11 general-leads1">
                           <div class="custom-danger">
                              <h4>General Information</h4>
                              <!-- <div class="form-status1">
                                 <span>Lead Status</span>
                                 <strong>New</strong>
                                 </div>   -->
                              <div class="form-status1">
                                 <span>Source</span>
                                 <strong><?php /*if($value['source']==1)
                                    {
                                      $value['source'] = 'Google';
                                    }elseif($value['source']==2)
                                    {
                                      $value['source'] = 'Facebook';
                                    }else{
                                      $value['source'] = '';
                                    } */
                                     $source_name = $this->Common_mdl->GetAllWithWhere('source','id',$value['source']);
                                     if(!empty($source_name)){
                                       $value['source'] = $source_name[0]['source_name'];
                                        if($value['source']!='')
                                        {
                                          $value['source'] = $value['source'];
                                        }else{
                                          $value['source'] = '-';
                                        }
                                     }
                                     else
                                     {
                                      $value['source']='-';
                                     }
                                    
                                    echo $value['source'];
                                    ?></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Default Language</span>
                                 <strong><?php echo $languages['name'];?></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Assigned</span>
                                 <strong><?php echo $assigned['crm_name'];?></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Tags</span>
                                 <strong>
                                 <?php $tag =explode(',', $value['tags']);
                                    foreach ($tag as $t_key => $t_value) { ?>
                                 <a href="#"><?php echo $t_value;?></a><?php } ?></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Created</span>
                                 <strong><?php  $date=date('Y-m-d H:i:s',$value['createdTime']);
                                    echo time_elapsed_string($date);?></strong>
                              </div>
                              <div class="form-status1">
                                 <span>Last Contact</span>
                                 <strong>-</strong>
                              </div>
                              <div class="form-status1">
                                 <span>Public</span>
                                 <strong><?php if($value['public']!=''){ echo $value['public']; }else{ echo 'No'; }?></strong>
                              </div>
                           </div>
                        </div>
                        <!-- rspt 18-04-2018 convert to customer -->
                        <div class="col-sm-4">
                           <button type="button" id="convert_to_customer" name="convert_to_customer" data-id="<?php echo $value['id'];?>" data-status="<?php echo $status_name['status_name'];?>" class="convert_to_customer" <?php if(strtolower($status_name['status_name'])=='customer'){?> style="display: none;" <?php }?> >   Convert To Customer</button>
                        </div>
                        <!-- 18-04-2018 end -->
                        <!-- <div class="col-sm-4 custom-fields11">
                           <div class="custom-danger">
                             <h4>Custom Fields</h4>
                             <div class="form-status1">
                               <span>Dedicated support</span>
                               <strong>-</strong>
                             </div>  
                           </div>
                           </div> -->
                        <div class="description-lead1 col-sm-12">
                           <span>Description</span>
                           <p><?php echo $value['description'];?> </p>
                        </div>
                        <div class="activity1 col-sm-12">
                           <h4>Latest Activity</h4>
                           <!-- <span>Gust Toy - created lead</span> -->
                           <span><?php  $date=date('Y-m-d H:i:s',$latestrec[0]['CreatedTime']);
                              echo time_elapsed_string($date);?></span>
                           <span><?php
                              $l_id = $latestrec[0]['module_id'];
                                $l_name = $this->Common_mdl->select_record('leads','id',$l_id);
                              
                               echo $l_name['name'] . ' - ' . $latestrec[0]['log'];?></span>
                        </div>
                     </div>
                  </div>
                  <!-- profile -->
                  <div id="proposal_<?php echo $value['id'];?>" class="tab-pane fade">
                     <div class="new-proposal1 floating_set">
                        <a href="<?php echo base_url();?>leads/proposal/<?php echo $value['id'].'/leads';?>">New proposal</a>
                     </div>
                     <div class="proposal-table1 floating_set">
                        <table id="example" class="display data-available-1" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th>Proposal</th>
                                 <th>Subject</th>
                                 <th>Total</th>
                                 <th>Date</th>
                                 <th>Open Till</th>
                                 <th>Tags</th>
                                 <th>Date created</th>
                                 <th>Status</th>
                              </tr>
                           </thead>
                           <tbody>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- 2tab -->
                  <div id="tasks_<?php echo $value['id'];?>" class="tab-pane fade">
                     <div class="new-proposal1 floating_set">
                        <a href="#"  data-toggle="modal" data-target="#addnew_task_lead_<?php echo $value['id'];?>">New Task</a>
                        <select name="task_export" id="task_export" class="task_export" data-id="<?php echo $value['id'];?>">
                           <option value="">EXPORT</option>
                           <option value="csv">CSV</option>
                           <option value="pdf">PDF</option>
                           <option value="html">HTML</option>
                        </select>
                     </div>
                     <div class="proposal-table1 floating_set task_leadss" id="task_leadss_<?php echo $value['id'];?>">
                        <table id="example1_<?php echo $value['id'];?>" class="display data-available-1 example1" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th>Subject</th>
                                 <th>Start Date</th>
                                 <th>End Date</th>
                                 <th>Tag</th>
                                 <th>Assigned To</th>
                                 <th>Priority</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tfoot class="ex_data1">
<tr>
<th>
</th>
</tr>
</tfoot>
                           <tbody>
                              <?php 
                                 $task_sql = $this->db->query("select * from add_new_task where related_to='leads' and lead_id=".$value['id']."" )->result_array();
                                 foreach ($task_sql as $task_sql_key => $task_sql_value) {
                                   $assigned_staff_id = $task_sql_value['worker'];
                                   $getUserProfilepic = $this->Common_mdl->getUserProfilepic($task_sql_value['worker']); 
                                   $user_name = $this->Common_mdl->select_record('user','id',$assigned_staff_id);
                                 ?>
                              <tr>
                                 <td> <?php echo $task_sql_value['subject'];?></td>
                                 <td> <?php echo $task_sql_value['start_date'];?></td>
                                 <td> <?php echo $task_sql_value['end_date'];?></td>
                                 <td> <?php echo str_replace(',',' , ',$task_sql_value['tag']);?></td>
                                 <td> <img src="<?php echo $getUserProfilepic;?>" height="50" width="50"></td>
                                 <td><?php echo $task_sql_value['priority'];?></td>
                                 <td>
                                    <p class="action_01">
                                       <a href="#" onclick="return delete_lead_task('<?php echo $value['id'];?>,<?php echo $task_sql_value['id'];?>');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                       <a href="#" data-toggle="modal" data-target="#edit_task_lead_<?php echo $task_sql_value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                    </p>
                                 </td>
                              </tr>
                              <?php } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- 3tab -->
                  <div id="attachments_<?php echo $value['id'];?>" class="fileupload tab-pane fade">
                     <form name="l_attachment<?php echo $value['id'];?>" id="l_attachment<?php echo $value['id'];?>" method="post" enctype="multipart/form-data">
                        <div class=" upload_input<?php echo $value['id'];?>">
                           <input type="file" name="lead_attachments" id="lead_attachments<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>"  class="lead_attachments">
                        </div>
                        <!-- <div class="cloud-upload1">
                           <a href="#"><i class="fa fa-cloud-upload fa-6" aria-hidden="true"></i> Choose from Dropbox</a>
                           </div> -->
                        <!-- <input type="hidden" name="l_a_id" id="l_a_id" value="<?php echo $value['id'];?>"> -->
                        <div class="l_att">
                           <?php $att_rec = $this->Common_mdl->GetAllWithWhere('leads_attachments','lead_id',$value['id']);
                              foreach ($att_rec as $att_key => $att_value) { ?>
                           <div class="mtop " id="lead_attachments">
                              <div class="row">
                                 <div class="display-block lead-attachment-wrapper">
                                    <div class="col-md-10">
                                       <div class="pull-left"><i class="mime mime-pdf"></i></div>
                                       <a href="<?php echo base_url().'uploads/leads/attachments/'.$att_value['attachments'];?>" target="_blank"><?php echo $att_value['attachments'];?></a>
                                       <p class="text-muted"></p>
                                    </div>
                                    <div class="col-md-2 text-right"><a href="#" class="text-danger" onclick="delete_lead_attachment(<?php echo $att_value['id'].','.$value['id'];?>); return false;"><i class="fa fa fa-times"></i></a></div>
                                    <div class="clearfix"></div>
                                    <hr>
                                 </div>
                              </div>
                           </div>
                           <?php }?>
                        </div>
                     </form>
                  </div>
                  <!-- 4tab -->
                  <div id="reminder_<?php echo $value['id'];?>" class="tab-pane fade">
                     <div class="new-proposal1 floating_set">
                        <a href="#" data-toggle="modal" data-target="#add-reminders_<?php echo $value['id'];?>"><i class="fa fa-bell-o fa-6" aria-hidden="true"></i> Add Lead Reminder</a>
                        <select name="reminder_export" id="reminder_export" class="reminder_export" data-id="<?php echo $value['id'];?>">
                           <option value="">EXPORT</option>
                           <option value="csv">CSV</option>
                           <option value="pdf">PDF</option>
                           <option value="html">HTML</option>
                        </select>
                     </div>
                     <div class="proposal-table1 floating_set response_res">
                        <table id="example2" class="data-available-1 display example2" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th>Description</th>
                                 <th>Date</th>
                                 <th>Remind</th>
                                 <th>Option</th>
                              </tr>
                           </thead>
                           <tfoot class="ex_data1">
<tr>
<th>
</th>
</tr>
</tfoot>
                           <tbody>
                              <?php 
                                 $sql = $this->db->query("select * from lead_reminder where lead_id = ".$value['id']."")->result_array();
                                 foreach ($sql as $sql_key => $sql_value) {
                                   $assigned_staff_id = $sql_value['assigned_staff_id'];
                                   $user_name = $this->Common_mdl->select_record('user','id',$assigned_staff_id);
                                 ?>
                              <tr>
                                 <td> <?php echo $sql_value['description'];?></td>
                                 <td> <?php echo $sql_value['reminder_date'];?></td>
                                 <td> <?php echo $user_name['crm_name'];?></td>
                                 <td>
                                    <p class="action_01">
                                       <a href="#" onclick="return delete_reminder('<?php echo $value['id'];?>,<?php echo $sql_value['id'];?>');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                       <a href="#" data-toggle="modal" data-target="#edit-reminders_<?php echo $sql_value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                    </p>
                                 </td>
                              </tr>
                              <?php } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- 5tab -->
                  <div id="notes_<?php echo $value['id'];?>" class="tab-pane fade">
                     <div class="notes-leads floating_set">
                        <form>
                           <div class="notes-list01">
                              <div class="notetextarea1">
                                 <textarea rows="3" id="note_rec_<?php echo $value['id'];?>" name="note_rec" class="note_val"></textarea>
                                 <input type="submit" name="add note" value="Add Note" class="add_note_rec" data-id="<?php echo $value['id'];?>">
                              </div>
                              <div id="error_note<?php echo $value['id'];?>" class="error" style="color: red;"></div>
                              <div class="dedicated-sup col-sm-12 note_date_<?php echo $value['id'];?>" style="display:none;">
                                 <label><i class="fa fa-pencil fa-6" aria-hidden="true"></i> contact Date</label>
                                 <input type="text" name="note_contact_date" id="note_contact_date_<?php echo $value['id'];?>" class="datepicker note_d" placeholder="Select your date" />
                              </div>
                              <div class="checked-unicode">
                                 <div class="form-group">
                                    <input type="radio" name="note_contact" class="note_contact" value="got_touch" data-id="<?php echo $value['id'];?>" > <label>I got in touch with this lead</label>
                                 </div>
                                 <div class="form-group">
                                    <input type="radio" name="note_contact" checked="checked" class="note_contact" data-id="<?php echo $value['id'];?>" value="not_touch"><label>I have not contacted this lead</label>
                                 </div>
                              </div>
                           </div>
                           <div class="add_note_rec_load">
                              <?php 
                                 $leads_notes = $this->db->query(" select * from leads_notes where lead_id ='".$value["id"]."' order by id desc")->result_array();
                                 foreach ($leads_notes as $notes_key => $notes_value) { 
                                 $getUserProfilepic = $this->Common_mdl->getUserProfilepic($notes_value['user_id']); 
                                 $u_name = $this->Common_mdl->select_record('user','id',$notes_value['user_id']);
                                 
                                 ?>
                              <div class="gust-toy01">
                                 <div class="checkbox-profile1">
                                    <img src="<?php echo $getUserProfilepic;?>" alt="User-Profile-Image">
                                 </div>
                                 <div class="timeline-lead01">
                                    <span>Note added: <?php 
                                       if($notes_value['createdTime']!=''){
                                       echo date('Y-m-d H:i:s',$notes_value['createdTime']); } ?></span>
                                    <strong><?php echo $u_name["crm_name"];?></strong>
                                    <p> <?php echo $notes_value['notes'];?></p>
                                 </div>
                                 <div class="edit-timeline">
                                    <!--        <a href="#"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                       -->       <a href="#" class="delete_note" data-id="<?php echo $notes_value["id"].'-'.$value['id'];?>"><i class="fa fa-times fa-6" aria-hidden="true"></i></a>
                                 </div>
                              </div>
                              <?php } ?>
                           </div>
                     </div>
                  </div>
                  <!-- 6tab -->
                  <div id="activity-log_<?php echo $value['id'];?>" class="tab-pane fade">
                  <div class="enter-activity-log floating_set ">
                  <div class="leads_activity_log">
                  <?php 
                     $latestrecs = $this->db->query(" select * from activity_log where module_id='".$value["id"]."' and module='Lead' order by id asc")->result_array();
                     foreach ($latestrecs as $late_key => $late_value) { ?>
                  <div class="activity-created1">
                  <span><?php  $date=date('Y-m-d H:i:s',$late_value['CreatedTime']);
                     echo time_elapsed_string($date);?></span>
                  <span><?php
                     $l_id = $late_value['module_id'];
                     $l_name = $this->Common_mdl->select_record('leads','id',$l_id);
                     
                       echo $l_name['name'] . ' - ' . $late_value['log'];?></span>
                  </div>
                  <?php } ?>
                  </div>
                  <div class="notetextarea1">
                  <textarea rows="3" placeholder="Enter Activity" name="activity_log" id="activity_log_<?php echo $value['id'];?>" class="activity_log"></textarea>
                  <div id="error<?php echo $value['id'];?>" class="error" style="color: red;"></div>
                  <input type="submit" name="add_activity" id="add_activity" data-id="<?php echo $value['id'];?>" class="add_activity" value="Add Activity" >
                  </div>
                  </div>  
                  </div> <!-- 7tab -->
               </div>
               <!-- tab-content -->
            </div>
            <!-- profile-edit-crm -->      
            <!-- ********************************* -->
         </div>
      </div>
   </div>
   <div class="modal fade modal-reminder reminder-modal-lead-4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="add-reminders_<?php echo $value['id'];?>">
   <div class="modal-dialog" role="document">
   <div class="modal-content">
   <form action="#" id="add_leads_reminder" name="add_leads_reminder" class="add_leads_reminder" method="post" >
   <div class="modal-header">
   <button type="button" class="close close-reminder-modal" data-rel-id="4" data-rel-type="lead" aria-label="Close" value=""><span aria-hidden="true" data-dismiss="modal">×</span></button>
   <h4 class="modal-title" id="myModalLabel"><i class="fa fa-question-circle" data-toggle="tooltip" title="This option allows you to never forget anything about your customers." data-placement="bottom"></i> Add lead reminder</h4>
   </div>
   <div class="modal-body">
   <div class="row">
   <div class="col-md-12">
   <div class="form-group" app-field-wrapper="date">
   <label for="date" class="control-label"> <small class="req text-danger">* </small>Date to be notified</label>
   <div class="input-group date">
   <!--  <input type="text" id="add_spl-datepicker" name ="reminder_date"  class="form-control spl-datepicker spl-datepicker_<?php echo $value['id'];?>" value=""> -->
   <input class="form-control datepicker fields spl-datepicker_<?php echo $value['id'];?>" type="text" name="reminder_date" id=""   placeholder="" value=""/>
   <div class="input-group-addon">
   <i class="fa fa-calendar calendar-icon"></i>
   </div>
   <div id="reminder_date_err_<?php echo $value['id'];?>" style="color:red"></div>
   </div>
   </div>
   <div class="form-group" app-field-wrapper="staff">
   <label for="staff" class="control-label"> <small class="req text-danger">* </small>Set reminder to</label>
   <div class="btn-group">
   <select id="staff_<?php echo $value['id'];?>" name="staff" class="selectpicker" data-show-subtext="true" data-live-search="true">
   <?php foreach ($staffs as $s_key => $s_value) {?>
   <option value="<?php echo $s_value['id'];?>"><?php echo $s_value['crm_name'];?></option>
   <?php } ?>
   </select>
   </div>
   </div>
   <div class="form-group" app-field-wrapper="description"><label for="description" class="control-label">Description</label>
   <textarea id="reminder_description_<?php echo $value['id'];?>" name="reminder_description" class="form-control" rows="4"></textarea>
   <div id="description_err_<?php echo $value['id'];?>" style="color:red"></div>
   </div>
   <div class="form-group">
   <div class="checkbox checkbox-primary">
   <input type="checkbox" name="notify_by_email_<?php echo $value['id'];?>" id="notify_by_email_<?php echo $value['id'];?>" value="">
   <label for="notify_by_email">Send also an email for this reminder</label>
   </div>
   </div>
   </div>
   </div>
   </div>
   <div class="modal-footer">
   <button type="button" class="btn btn-default close-reminder-modal" data-rel-id="4" data-rel-type="lead" value="">Close</button>
   <button type="submit" class="btn btn-info reminder_save" value="" id="reminder_save"  data-id="<?php echo  $value['id'];?>">Save</button>
   </div>
   </form>
   </div>
   </div>
   </div>
   <div class="reminder_popups_<?php echo $value['id'];?>">
      <?php 
         $edit_reminder = $this->db->query("select * from lead_reminder where lead_id = ".$value['id']."")->result_array();
         foreach($edit_reminder as $e_k_remidner => $e_k_value){ 
                 
                 $reminder_date  = date("Y-m-d", strtotime(implode('/', array_reverse(explode('-', $e_k_value['reminder_date'])))));
         
           ?>
      <div class="modal fade modal-reminder reminder-modal-lead-4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="edit-reminders_<?php echo $e_k_value['id'];?>">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <form action="#" id="add_leads_reminder" name="add_leads_reminder" class="add_leads_reminder" method="post" >
                  <div class="modal-header">
                     <button type="button" class="close close-reminder-modal" data-rel-id="4" data-rel-type="lead" aria-label="Close" value=""><span aria-hidden="true" data-dismiss="modal">×</span></button>
                     <h4 class="modal-title" id="myModalLabel"><i class="fa fa-question-circle" data-toggle="tooltip" title="This option allows you to never forget anything about your customers." data-placement="bottom"></i> Edit lead reminder</h4>
                  </div>
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group" app-field-wrapper="date">
                              <label for="date" class="control-label"> <small class="req text-danger">* </small>Date to be notified</label>
                              <div class="input-group date">
                                 <!-- edit_spl-datepicker -->
                                 <input type="text" id="" name ="edit_reminder_date"  class="form-control datepicker edit_spl-datepicker_<?php echo $e_k_value['id'];?>" value="<?php echo $reminder_date;?>">
                                 <div class="input-group-addon">
                                    <i class="fa fa-calendar calendar-icon"></i>
                                 </div>
                                 <div id="edit_reminder_date_err_<?php echo $e_k_value['id'];?>" style="color:red"></div>
                              </div>
                           </div>
                           <div class="form-group" app-field-wrapper="staff">
                              <label for="staff" class="control-label"> <small class="req text-danger">* </small>Set reminder to</label>
                              <div class="btn-group">
                                 <select id="edit_staff_<?php echo $e_k_value['id'];?>" name="edit_staff" class="selectpicker" data-show-subtext="true" data-live-search="true">
                                    <?php foreach ($staffs as $s_key => $s_value) {?>
                                    <option value="<?php echo $s_value['id'];?>" <?php if($s_value['id']==$e_k_value['assigned_staff_id']){ echo 'selected="selected"'; }?>><?php echo $s_value['crm_name'];?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group" app-field-wrapper="description">
                              <label for="description" class="control-label">Description</label>
                              <textarea id="edit_reminder_description_<?php echo $e_k_value['id'];?>" name="edit_reminder_description_<?php echo $e_k_value['id'];?>" class="form-control" rows="4"><?php echo $e_k_value['description'];?></textarea>
                              <div id="description_err_<?php echo $e_k_value['id'];?>" style="color:red"></div>
                           </div>
                           <div class="form-group">
                              <div class="checkbox checkbox-primary">
                                 <input type="checkbox" name="edit_notify_by_email_<?php echo $e_k_value['id'];?>" id="edit_notify_by_email_<?php echo $e_k_value['id'];?>" value=""  <?php if($e_k_value['email_sent']=='yes') { echo 'checked="checked"'; }?> >
                                 <label for="notify_by_email">Send also an email for this reminder</label>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-default close-reminder-modal" data-rel-id="4" data-rel-type="lead" value="">Close</button>
                     <button type="submit" class="btn btn-info edit_reminder_save" value="" id="edit_reminder_save"  data-id="<?php echo  $e_k_value['id'];?>"  data-value="<?php echo $value['id'];?>">Save</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <?php } ?> 
   </div>
   <div class="test_test"></div>
   <!-- Modal -->
   <div id="addnew_task_lead_<?php echo $value['id'];?>" class="addnew_task_lead1  modal fade" role="dialog" >
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Add new task</h4>
            </div>
            <form name="lead_task" id="lead_task_<?php echo $value['id'];?>" class="add_lead_task" method="post" enctype='multipart/form-data' data-id="<?php echo $value['id'];?>">
               <div class="modal-body">
                  <div class="attached-popup1">
                     <div class="form-group check-options cus-checkbox1">
                        <input type="checkbox" name="public" value="Public" id="p-check_<?php echo $value['id'];?>">
                        <label for="p-check">Public </label>
                        <input type="checkbox" name="billable" value="Billable" checked="" id="p-check1_<?php echo $value['id'];?>">
                        <label for="p-check1"> Billable</label>
                     </div>
                     <div class="form-group attach-files file-attached1">
                        <input type="file" id="inputfile_<?php echo $value['id'];?>" class="task_images" data-id="<?php echo $value['id'];?>" name="attach_file">Attach File 
                     </div>
                  </div>
               </div>
               <div class="lead-popupsection2 proposal-data1 floating_set">
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>* Subject</label>
                     <input type="text" name="task_sub_<?php echo $value['id'];?>" id="task_sub_<?php echo $value['id'];?>">
                     <div class="task_sub_err" id="task_sub_err_<?php echo $value['id'];?>" style="color:red;"></div>
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Hourly Rate</label>
                     <input type="text" name="hour_rate_<?php echo $value['id'];?>" id="hour_rate_<?php echo $value['id'];?>">
                  </div>
                  <div class="form-group  col-xs-12 col-sm-6 date_birth">
                     <label>Start Date</label>
                     <div class="form-birth05">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                        <input class="form-control datepicker fields" type="text" name="start_date_<?php echo $value['id'];?>" id="start_date_<?php echo $value['id'];?>"   placeholder="Start date" value=""/>
                        <div class="task_start_err" id="task_start_err_<?php echo $value['id'];?>" style="color:red;" ></div>
                     </div>
                  </div>
                  <div class="form-group  col-xs-12 col-sm-6 date_birth">
                     <label>Due Date</label>
                     <div class="form-birth05">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                        <input class="form-control datepicker fields" type="text" name="due_date_<?php echo $value['id'];?>" id="due_date_<?php echo $value['id'];?>"  placeholder="Due date" value=""/>
                     </div>
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Priority</label>
                     <select name="priority" class="form-control valid" id="priority_<?php echo $value['id'];?>">
                        <option value="low">Low</option>
                        <option value="medium">Medium</option>
                        <option value="high">High</option>
                        <option value="super_urgent">Super Urgent</option>
                     </select>
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Repeat every Week</label>
                     <select name = "repeat" id="repeat_<?php echo $value['id'];?>" class="repeated_too" data-id="<?php echo $value['id'];?>">
                        <option value=""></option>
                        <option value="one_week">week</option>
                        <option value="two_week">2 week</option>
                        <option value="one_month">1 Month</option>
                        <option value="two_month">2 Months</option>
                        <option value="three_month">3 Months</option>
                        <option value="six_month">6 Months</option>
                        <option value="one_yr">1 yr</option>
                        <option value="custom">Custom</option>
                     </select>
                  </div>
                  <div class="form-group col-xs-12 col-sm-6 rep_no" style="display:none;" id="rep_no_<?php echo $value['id'];?>">
                     <input class="form-control fields" type="number" name="repeat_no_<?php echo $value['id'];?>" id="repeat_no_<?php echo $value['id'];?>"  placeholder="" value="1"/>
                  </div>
                  <div class="form-group col-xs-12 col-sm-6 rep_cnt" style="display:none;" id="rep_cnt_<?php echo $value['id'];?>">
                     <select class="selectpicker" data-live-search="true" id="durations_<?php echo $value['id'];?>">
                        <option value="days" >Day(s)</option>
                        <option value="weeks" >Week(s)</option>
                        <option value="months" >Month(s)</option>
                        <option value="years" >Year(s)</option>
                     </select>
                  </div>
                  <div class="form-group  col-xs-12  col-sm-12 date_birth ends_on_div" id="ends_on_div_<?php echo $value['id'];?>" style="display:none;">
                     <label>Ends On (Leave blank for never)</label>
                     <div class="form-birth05">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                        <input class="form-control datepicker fields" type="text" name="ends_on_<?php echo $value['id'];?>" id="ends_on_<?php echo $value['id'];?>"  placeholder="Ends on date" value=""/>
                     </div>
                  </div>
                  <div class="form-group col-xs-12 col-sm-6">
                     <label>Related To</label>
                     <select class="selectpicker" data-live-search="true" id="related_to_<?php echo $value['id'];?>">
                        <option value="leads">Leads</option>
                        <option value="client">Client</option>
                     </select>
                  </div>
                  <div class="form-group col-xs-12 col-sm-6">
                     <label>Lead</label>
                     <select class="selectpicker" data-live-search="true" id="lead_to_<?php echo $value['id'];?>" >
                        <option value="<?php echo $value['id'];?>" ><?php echo $assigned['crm_email_id'];?></option>
                     </select>
                  </div>
                  <div class="tags-allow1 floating_set">
                     <h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
                     <input type="text" value="" name="tags_<?php echo $value['id'];?>" id="tags_<?php echo $value['id'];?>" class="tags" />
                  </div>
                  <div class="formgroup col-sm-12">
                     <label>Description</label>
                     <textarea rows="3" name="description" id="descr_<?php echo $value['id'];?>"></textarea>
                  </div>
               </div>
         </div>
         <div class="modal-footer">
         <a href="#" data-dismiss="modal">Close</a>
         <input type="submit" name="save" id="save" class="task_leads"  data-id="<?php echo $value['id'];?>" value="Save">
         </div>
         </form>
      </div>
   </div>
   </div>
</div>
<!-- rspt 18-04-2018 -->  
<div class="task_edit_popup_<?php echo $value['id'];?>">
   <?php $edit_tasks =  $this->db->query("select * from add_new_task where related_to='leads' and lead_id=".$value['id']."")->result_array();
      foreach ($edit_tasks as $t_key => $t_value) { ?>
   <!-- Modal -->
   <div id="edit_task_lead_<?php echo $t_value['id'];?>" class="addnew_task_lead1  modal fade" role="dialog" >
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Edit task</h4>
            </div>
            <form name="lead_task" id="lead_task_<?php echo $t_value['id'];?>" class="add_lead_task" method="post" enctype='multipart/form-data' data-id="<?php echo $t_value['id'];?>">
               <div class="modal-body">
                  <div class="attached-popup1">
                     <div class="form-group check-options cus-checkbox1">
                        <input type="checkbox" name="public" value="Public" id="t_edit_p-check_<?php echo $t_value['id'];?>"  <?php if(isset($t_value['public']) && $t_value['public']=='Public') {?> checked="checked"<?php } ?>>
                        <label for="p-check">Public </label>
                        <input type="checkbox" name="billable" value="Billable" checked="" id="t_edit_p-check1_<?php echo $t_value['id'];?>" <?php if(isset($t_value['billable']) && $t_value['billable']=='Billable') {?> checked="checked"<?php } ?>>
                        <label for="p-check1"> Billable</label>
                     </div>
                     <div class="form-group attach-files file-attached1">
                        <input type="file" id="t_edit_inputfile_<?php echo $t_value['id'];?>" class="e_task_images" data-id="<?php echo $t_value['id'];?>" name="attach_file">Attach File 
                     </div>
                     <?php if($t_value['attach_file']!=''){ ?><img src="<?php echo base_url();?>uploads/leads/tasks/<?php echo $t_value['attach_file'];?>" height="50" width="50"> <?php }?>
                  </div>
               </div>
               <div class="lead-popupsection2 proposal-data1 floating_set">
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>* Subject</label>
                     <input type="text" name="t_edit_task_sub_<?php echo $t_value['id'];?>" id="t_edit_task_sub_<?php echo $t_value['id'];?>" value="<?php if(isset($t_value['subject']) && ($t_value['subject']!='') ){ echo $t_value['subject'];}?>">
                     <div class="t_edit_task_sub_err" id="t_edit_task_sub_err_<?php echo $t_value['id'];?>" style="color:red;"></div>
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Hourly Rate</label>
                     <input type="text" name="t_edit_hour_rate_<?php echo $t_value['id'];?>" id="t_edit_hour_rate_<?php echo $t_value['id'];?>" value="<?php if(isset($t_value['hour_rate']) && ($t_value['hour_rate']!='') ){ echo $t_value['hour_rate'];}?>">
                  </div>
                  <div class="form-group  col-xs-12 col-sm-6 date_birth">
                     <label>Start Date</label>
                     <div class="form-birth05">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                        <input class="form-control datepicker fields" type="text" name="t_edit_start_date_<?php echo $t_value['id'];?>" id="t_edit_start_date_<?php echo $t_value['id'];?>"   placeholder="Start date"  value="<?php if(isset($t_value['start_date']) && ($t_value['start_date']!='') ){ echo $t_value['start_date'];}?>"/>
                        <div class="t_edit_task_start_err" id="t_edit_task_start_err_<?php echo $t_value['id'];?>" style="color:red;" ></div>
                     </div>
                  </div>
                  <div class="form-group  col-xs-12 col-sm-6 date_birth">
                     <label>Due Date</label>
                     <div class="form-birth05">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                        <input class="form-control datepicker fields" type="text" name="t_edit_due_date_<?php echo $t_value['id'];?>" id="t_edit_due_date_<?php echo $t_value['id'];?>"  placeholder="Due date" value="<?php if(isset($t_value['end_date']) && ($t_value['end_date']!='') ){ echo $t_value['end_date'];}?>"/>
                     </div>
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Priority</label>
                     <select name="t_edit_priority" class="form-control valid" id="t_edit_priority_<?php echo $t_value['id'];?>">
                        <option value="low" <?php if(isset($t_value['priority']) && $t_value['priority']=='low') {?> selected="selected"<?php } ?>>Low</option>
                        <option value="medium" <?php if(isset($t_value['priority']) && $t_value['priority']=='medium') {?> selected="selected"<?php } ?>>Medium</option>
                        <option value="high"  <?php if(isset($t_value['priority']) && $t_value['priority']=='high') {?> selected="selected"<?php } ?>>High</option>
                        <option value="super_urgent" <?php if(isset($t_value['priority']) && $t_value['priority']=='super_urgent') {?> selected="selected"<?php } ?>>Super Urgent</option>
                     </select>
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Repeat every Week</label>
                     <select name = "t_edit_repeat" id="t_edit_repeat_<?php echo $t_value['id'];?>" class="t_edit_repeated_too" data-id="<?php echo $t_value['id'];?>">
                        <option value=""></option>
                        <option value="one_week" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='one_week') {?> selected="selected"<?php } ?>>week</option>
                        <option value="two_week" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='two_week') {?> selected="selected"<?php } ?>>2 week</option>
                        <option value="one_month" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='one_month') {?> selected="selected"<?php } ?>>1 Month</option>
                        <option value="two_month" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='two_month') {?> selected="selected"<?php } ?>>2 Months</option>
                        <option value="three_month" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='three_month') {?> selected="selected"<?php } ?>>3 Months</option>
                        <option value="six_month" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='six_month') {?> selected="selected"<?php } ?>>6 Months</option>
                        <option value="one_yr" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='one_yr') {?> selected="selected"<?php } ?>>1 yr</option>
                        <option value="custom" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='custom') {?> selected="selected"<?php } ?>>Custom</option>
                     </select>
                  </div>
                  <?php if($t_value['repeat_no']!=''){?>
                  <div class="form-group col-xs-12 col-sm-6 rep_no" id="t_edit_rep_no_<?php echo $t_value['id'];?>">
                     <input class="form-control fields" type="number" name="t_edit_repeat_no_<?php echo $t_value['id'];?>" id="t_edit_repeat_no_<?php echo $t_value['id'];?>"  placeholder="" value="<?php echo $t_value['repeat_no'];?>"/>
                  </div>
                  <?php } ?>
                  <?php if($t_value['durations']!=''){?>
                  <div class="form-group col-xs-12 col-sm-6 rep_cnt" id="t_edit_rep_cnt_<?php echo $t_value['id'];?>">
                     <select class="selectpicker" data-live-search="true" id="t_edit_durations_<?php echo $t_value['id'];?>">
                        <option value="days" <?php if(isset($t_value['durations']) && $t_value['durations']=='days') {?> selected="selected"<?php } ?>>Day(s)</option>
                        <option value="weeks" <?php if(isset($t_value['durations']) && $t_value['durations']=='weeks') {?> selected="selected"<?php } ?>>Week(s)</option>
                        <option value="months" <?php if(isset($t_value['durations']) && $t_value['durations']=='months') {?> selected="selected"<?php } ?>>Month(s)</option>
                        <option value="years" <?php if(isset($t_value['durations']) && $t_value['durations']=='years') {?> selected="selected"<?php } ?>>Year(s)</option>
                     </select>
                  </div>
                  <?php } ?>
                  <?php if($t_value['ends_on']!=''){?>
                  <div class="form-group  col-xs-12  col-sm-12 date_birth ends_on_div" id="t_edit_ends_on_div_<?php echo $t_value['id'];?>">
                     <label>Ends On (Leave blank for never)</label>
                     <div class="form-birth05">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                        <input class="form-control datepicker fields" type="text" name="t_edit_ends_on_<?php echo $t_value['id'];?>" id="t_edit_ends_on_<?php echo $t_value['id'];?>"  placeholder="Ends on date" value="<?php echo $t_value['ends_on'];?>"/>
                     </div>
                  </div>
                  <?php } ?>
                  <div class="form-group col-xs-12 col-sm-6">
                     <label>Related To</label>
                     <select class="selectpicker" data-live-search="true" id="t_edit_related_to_<?php echo $t_value['id'];?>">
                        <option value="leads" <?php if(isset($t_value['related_to']) && $t_value['related_to']=='leads') {?> selected="selected"<?php } ?>>Leads</option>
                        <option value="client" <?php if(isset($t_value['related_to']) && $t_value['related_to']=='client') {?> selected="selected"<?php } ?>>Client</option>
                     </select>
                  </div>
                  <div class="form-group col-xs-12 col-sm-6">
                     <label>Lead</label>
                     <select class="selectpicker" data-live-search="true" id="t_edit_lead_to_<?php echo $t_value['id'];?>" >
                        <option value="<?php echo $t_value['worker'];?>" ><?php echo $assigned['crm_email_id'];?></option>
                     </select>
                  </div>
                  <div class="tags-allow1 floating_set">
                     <h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
                     <input type="text" value="<?php echo $t_value['tag'];?>" name="t_edit_tags_<?php echo $t_value['id'];?>" id="t_edit_tags_<?php echo $t_value['id'];?>" class="tags" />
                  </div>
                  <div class="formgroup col-sm-12">
                     <label>Description</label>
                     <textarea rows="3" name="t_edit_description" id="t_edit_descr_<?php echo $t_value['id'];?>"><?php echo $t_value['description'];?></textarea>
                  </div>
               </div>
         </div>
         <div class="modal-footer">
         <a href="#" data-dismiss="modal">Close</a>
         <input type="submit" name="save" id="save" class="edit_task_leads"  data-id="<?php echo $t_value['id'];?>" value="Save" data-value="<?php echo $value['id'];?>">
         </div>
         </form>
      </div>
   </div>
</div>
<input type="hidden" name="t_edit_img_name_<?php echo $t_value['id'];?>" id="t_edit_img_name_<?php echo $t_value['id'];?>" value="<?php echo $t_value['attach_file'];?>">
<?php } ?>
</div>
<input type="hidden" name="img_name_<?php echo $value['id'];?>" id="img_name_<?php echo $value['id'];?>" value="">
<?php // } ?>
<?php /*************************************************************/ ?>
<div id="update_lead_<?php echo $value['id'];?>" class="modal fade leads_popup" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Lead</h4>
         </div>
         <div class="modal-body">
            <form id="edit_leads_form" name="edit_leads_form" action="<?php echo base_url()?>leads/editLeads/<?php echo $value['id'];?>" method="post" class="edit_leads_form">
               <input type="hidden" name="kanpan" id="kanpan" value="0">
               <div class="lead-popupsection1 floating_set">
                  <div class="col-xs-12 col-sm-4  lead-form1">
                     <label>Lead status</label>
                     <select name="lead_status" id="lead_status">
                        <option value="">Nothing selected</option>
                        <?php foreach($leads_status as $ls_value) { ?>
                        <option value='<?php echo $ls_value['id'];?>'><?php echo $ls_value['status_name'];?></option>
                        <?php } ?>
                        <!-- <option value='1' <?php  if($value['lead_status']=='1'){ echo "selected='selected'"; }?>>Customer</option> -->
                     </select>
                  </div>
                  <div class="col-xs-12 col-sm-4  lead-form1">
                     <label>Source</label>
                     <select name="source" id="source" class="<?php echo $value['source'];?>">
                        <option value=''>Nothing selected</option>
                        <?php foreach($source as $lsource_value) { ?>
                        <option value='<?php echo $lsource_value['id'];?>' <?php  if($value['source']==$lsource_value['source_name']){ echo "selected='selected'"; }?>><?php echo $lsource_value['source_name'];?></option>
                        <?php } ?>
                        <!-- <option value='1' <?php  if($value['source']=='1'){ echo "selected='selected'"; }?>>Google</option>
                           <option value-'2' <?php  if($value['source']=='2'){ echo "selected='selected'"; }?>>Facebook</option> -->
                     </select>
                  </div>
                  <div class="col-xs-12 col-sm-4  lead-form1">
                     <label>Assigned</label>
                     <select name="assigned" id="assigned">
                        <option value="">Nothing selected</option>
                        <?php
                           if(isset($staff_form) && $staff_form!='') 
                           {
                            foreach($staff_form as $values) 
                            {
                           ?>
                        <option value="<?php echo $values['id']; ?>" <?php  if($value['assigned']==$values['id']) { echo "selected='selected'"; } ?> ><?php echo $values["crm_name"]; ?></option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>
               <div class="tags-allow1 floating_set">
                  <h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
                  <input type="text" value="<?php echo $value['tags'];?>" name="tags" id="tags" class="tags" />
               </div>
               <div class="lead-popupsection2 floating_set">
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Name</label>
                     <input type="text" name="name" id="name"  value="<?php echo $value['name'];?>">
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Address</label>
                     <textarea rows="1" name="address" id="address"><?php echo $value['address'];?></textarea>
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Position</label>
                     <input type="text" name="position" id="position" value="<?php echo $value['position'];?>">
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>City</label>
                     <input type="text" name="city" id="city" value="<?php echo $value['city'];?>">
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Email Address</label>
                     <input type="text" name="email_address" id="email_address" value="<?php echo $value['email_address'];?>">
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>State</label>
                     <input type="text" name="state" id="state" value="<?php echo $value['state'];?>">
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Website</label>
                     <input type="text" name="website" id="website" value="<?php echo $value['website'];?>">
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Country</label>
                     <select name="country" id="country">
                        <option value=''>Nothing Selected</option>
                        <?php foreach ($countries as $c_key => $c_value) { ?>
                        <option value="<?php echo $c_value['id']?>" <?php if($value['country']==$c_value['id']){ echo "selected='selected'"; } ?>><?php echo $c_value['name'];?></option>
                        <?php }?>
                     </select>
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label class="col-sm-4 col-form-label">Phone</label>
                     <div class="col-sm-8">
                     <input type="text" name="country_code" class="country_code" style="width:80px;" value="" placeholder="Code">
                     <input type="text" name="phone" id="phone" class="telephone_number" value="<?php echo $value['phone'];?>">
                     </div>
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6 min-height_quote">
                     <label>Zip Code</label>
                     <input type="number" name="zip_code" id="zip_code" value="<?php echo $value['zip_code'];?>">
                  </div>
                  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Company</label>
                     <input type="text" name="company" id="company" value="<?php echo $value['company'];?>">
                  </div>
                  <!-- <div class="formgroup col-xs-12 col-sm-6">
                     <label>Default Language</label>
                     <select name="default_language" id="default_language">
                        <option value="">System Default</option>
                        <?php foreach ($Language as $l_key => $l_value) { ?>
                        <option value="<?php echo $l_value['id'];?>" <?php if($l_value['id']==$value['default_language']){ echo "selected='selected'"; } ?>><?php echo $l_value['name'];?></option>
                        <?php }?>
                     </select>
                  </div> -->
                  <div class="formgroup col-sm-12">
                     <label>Description</label>
                     <textarea rows="3" name="description" id="description"><?php echo $value['description'];?></textarea>
                  </div>
                  <div class="formgroup checkbox-select1 col-sm-12">
                     <p>
                        <input type="checkbox" name="public" id="public" <?php if($value['public']=='on'){ echo 'checked'; } ?>>
                        <label>Public</label>
                     </p>
                     <p>
                        <input type="checkbox" name="contact_today" <?php if($value['contact_today']=='on'){ echo 'checked'; } ?> id="contact_update_today" class="contact_update_today" data-id="<?php echo $value['id'];?>">
                        <label>Contacted Today</label>
                     </p>
                  </div>
                  <div class="dedicated-sup col-sm-12 selectDate_update" <?php if($value['contact_today']!='') { ?> style="display:none;"<?php }else{ ?>style="display:block;"<?php  } ?> id="selectDate_update_<?php echo $value['id'];?>">
                     <label><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Choose contact Date</label>
                     <input type="text" name="contact_update_date" id="contact_update_date" class="datepicker" value="<?php echo $value['contact_date'];?>" placeholder="Select your date" />
                  </div>
               </div>
               <div class="col-sm-12 form-submit-lead text-right">
                  <a href="#"  data-dismiss="modal">Close</a>
                  <input type="submit" name="save" id="save" value="Save">
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<!-- Edit modal-->
<!-- Modal -->
<div id="import-lead" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Import Lead</h4>
         </div>
         <div class="modal-body  <?php if($_SESSION['permission']['Import_leads'][0]['create']!='1'){ ?> permission_deined <?php } ?>">
            <div class="download-sample">
               <a href="<?php echo base_url()?>uploads/csv/sample_import_leads.csv">DOWNLOAD SAMPLE</a>
            </div>
            <div class="term-condition1">
               <p>1. Your CSV data should be in the format below. The first line fof your CSV file should be the column headers as in the table example. Also make sure that your file is UTF_8 to avoid unnecessary encoding problems.</p>
               <p>2. if the column you are trying to import is date make sure that is formatted in format Y-m-d(2017-10-19)</p>
               <p class="dupli-data">3. Duplicate email rows wont be imported</p>
            </div>
            <div class="table-lead1">
               <table>
                  <thead>
                     <tr>
                        <th>Name</th>
                        <th>Company</th>
                        <th>Position</th>
                        <th>Description</th>
                        <th>Country</th>
                        <th>Zip</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Address</th>
                        <th>Email</th>
                        <th>Website</th>
                        <th>Phonenumber</th>
                        <th>Tags</th>
                        <th>Public</th>
                        <th>Contact Today</th>
                        <th>Contact Date</th>
                     </tr>
                  </thead>
<tfoot class="ex_data1">
<tr>
<th>
</th>
</tr>
</tfoot>
                  <tbody>
                     <tr>
                                <td>Test</td>
                                <td>Test Company</td>
                                <td>Client</td>
                                <td>Sample Description</td>
                                <td>Test Country</td>
                                <td>698523</td>
                                <td>Test City</td>
                                <td>Test State</td>
                                <td>28, 15th Lane, City</td>
                                <td>test@gmail.com</td>
                                <td>www.testcompany.com</td>
                                <td>9874563210</td>
                                <td>tag1,tag2</td>
                                <td>on / off</td>
                                <td>on / off</td>
                                <td>2018-03-21</td>
                             </tr>
                  </tbody>
               </table>
            </div>
            <form name="import_leads" id="import_leads" action="<?php echo base_url()?>leads/import_leads" method="post" enctype="multipart/form-data">
               <input type="hidden" name="kanpan" id="kanpan" value="0">
               <div class="file-upload-lead">
                  <div class="formdata1">
                     <label>choose CSV File</label>
                     <!-- <input type="file" name="file"> --> 
                     <div class="custom_upload">
                     <input type="file" name="file" id="file" accept=".csv"/>

                   </div>
                    <label  class='error custom_file_error'> </label>
                  </div>
                  <div class="formdata1">
                     <label>Source</label>
                      <div class="dropdown-sin-11">
                     <select name="source[]" id="source"  multiple="multiple" >
                      <!--   <option value=''>Nothing selected</option> -->
                        <?php /*foreach($source as $lsource_value) { ?>
                        <option value='<?php echo $lsource_value['id'];?>'><?php echo $lsource_value['source_name'];?></option>
                        <?php } */?>
                       <?php     /*$i=0;*/
                                 foreach($source as $lsource_value) { ?>
                                 <option value='<?php echo $lsource_value['id'];?>' ><?php echo $lsource_value['source_name'];?></option>
                                 <?php 
                                 $i++; } ?>
                     </select>
                     </div>
                  </div>
                  <div class="formdata1">
                     <label>Status</label>
                      <div class="dropdown-sin-12">
                     <select name="lead_status" id="lead_status">                       
                        <option value="">Nothing selected</option>
                        <?php foreach($leads_status as $ls_value) { ?>
                        <option value='<?php echo $ls_value['id'];?>'><?php echo $ls_value['status_name'];?></option>
                        <?php } ?>
                     </select>
                     </div>
                  </div>
                  <div class="formdata1">
                     <label>Responsible</label>
                     <div class="dropdown-sin-13 lead-form-st">
                     <select name="assigned[]" id="assigned" class="import_section" multiple="multiple" placeholder="Select Responsible">
                       <!--  <option value="">Nothing selected</option> -->
                        <?php
                           /* if(isset($staff_form) && $staff_form!='') 
                           {
                             foreach($staff_form as $value) 
                             {
                           ?>
                        <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
                        <?php
                           }
                           } */
                           ?>
                              <?php
                                    if(isset($staff_form) && $staff_form!='') 
                                    { ?>
                                      <option disabled>Staff</option> 
                                      <?php 
                                       $i=0;
                                     foreach($staff_form as $value) 
                                     {
                                    ?>
                                 <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
                                 <?php
                                 $i++;
                                    }
                                    }
                               if(isset($team) && $team!='') 
                                    { ?>
                                      <option disabled>Team</option> 
                                      <?php 
                                       $i=0;
                                     foreach($team as $value) 
                                     {
                                    ?>
                                 <option value="tm_<?php echo $value['id']; ?>" ><?php echo $value["team"]; ?></option>
                                 <?php
                                 $i++;
                                    }
                                    }
                                  if(isset($department) && $department!='') 
                                    { ?>
                                      <option disabled>Department</option> 
                                      <?php 
                                       $i=0;
                                     foreach($department as $value) 
                                     {
                                    ?>
                                 <option value="de_<?php echo $value['id']; ?>"><?php echo $value["new_dept"]; ?></option>
                                 <?php
                                 $i++;
                                    }
                                    }
                                    ?>
                     </select>
                     </div>
                  </div>
                  <div class="formdata1">
                     <label> Notify to(Manager)</label>
                     <div class="dropdown-sin-14 lead-form-st">
                     <select name="review_manager[]" id="review_manager" class="import_section" multiple="multiple" placeholder="Select Responsible">
                     
                           <?php
                              if(isset($manager) && $manager!='') 
                              {
                               foreach($manager as $value) 
                               {
                              ?>
                           <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
                           <?php
                              }
                              }
                              ?>
                     </select>
                     </div>
               </div>
               <input type="hidden" name="kanpan" id="kanpan" value="0">
               <div class="modal-footer">
                  <a href="#" data-dismiss="modal">Close</a>
                  <input type="submit" name="submit" value="SUBMIT">
                  <input type="reset" style="display: none;">
               </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>



<div class="modal fade" id="confirm_archive" role="dialog" style="display: none;">
                                  <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Confirmation</h4>
                                      </div>
                                    <div class="modal-body">
                                  <input type="hidden" name="archive_leads_id" id="archive_leads_id" value="">
                                      <p>Do you want to archive this lead?</p>
                                      </div>
                                      <div class="modal-footer">
                                       <button type="button" class="btn btn-default con_btn" onClick="archive_leads_action()">yes</button>
                                        <button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>


<div class="modal fade" id="leads_deleteconfirmation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <inpu type="hidden"  id="delete_leads_id" value="">
          <p> Are you sure want to delete This Lead ?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default"  onclick="delete_leads_action()" data-dismiss="modal">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>

    </div>
  </div>

<div class="modal fade" id="source_deleteconfirmation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <inpu type="hidden"  id="delete_source_id" value="">
          <p> Are you sure want to delete this Source?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" onclick="delete_source_action()" data-dismiss="modal">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>

    </div>
  </div>
<div class="modal fade" id="status_deleteconfirmation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <inpu type="hidden"  id="delete_status_id" value="">
          <p> Are you sure want to delete this status ?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default"  onclick="delete_status_action()" data-dismiss="modal">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>

    </div>
  </div>

<!--   <div class="file-import-lead">
   <input type="import" name="submit" value="IMPORT">
      <input type="submit" name="submit" value="SUBMIT">
   </div> ->
   
   
   </form>
   </div>
   </div>
   </div>
   </div>
   <!- modal -->
<style type="text/css">
   .ui-datepicker
   {
   z-index: 9999999999 !important;
   }
   .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}
tfoot {
    display: table-header-group;
}
</style>
<!-- Modal -->
<?php $this->load->view('includes/footer');?>
<script>
//it's for status filter table 
function stop_filter()
{ 

 $("th").on("click.DT",function (e) {  
      //stop Propagation if clciked outsidemask
      //becasue we want to sort locally here
        if (!$(e.target).hasClass('sortMask')) {         
          e.stopImmediatePropagation();
        }
      });
}
//it's for status filter table 
</script>
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
 --><link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
 --><link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/user_page/materialize.js"></script>
<!-- for drag and drop -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Sortable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/sortable-custom.js"></script>
<!-- end of drag and drop -->

<script type="text/javascript">


var table;
var source_table;
var status_table;
function initialize_alltask_datatable()
{
  
  <?php 
          $column_setting = Firm_column_settings('lead_source');


        ?>
         var column_order = <?php echo $column_setting['order'] ?>;
        
        var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;

        var column_ordering = [];

        $.each(column_order,function(i,v){

          var index = $('#alltask thead  th.'+v).index();

          if(index!==-1)
          {
            column_ordering.push(index);
          }
        });
   var numCols = $('#alltask thead th').length;  
    source_table = $('#alltask').DataTable({

           "dom": '<"toolbar-table" B>lfrtip',
           buttons: [
            {
                extend: 'colvis',
                columns: ':not(.Exc-colvis)'
            }
          ],
          order: [],//ITS FOR DISABLE SORTING
          columnDefs: 
          [
            {"orderable": false,"targets": ['source_chk_TH','action_TH']}
          ],

        
          fixedHeader: {
            header: true,
            footer: true
        },
         
         colReorder: 
          {
            realtime: false,
            order:column_ordering,
            fixedColumnsLeft : 1,
            fixedColumnsRight:1

          },
          initComplete: function () { 


                                 var api = this.api();

              api.columns('.hasFilter').every( function () {

                        var column = this;
                        var TH = $(column.header());
                        var Filter_Select = TH.find('.filter_check');
                        ///alert(Filter_Select.length);
                        // alert('check');
                    if( !Filter_Select.length )
                    {
                         // alert('check1');
                      Filter_Select = $('<select multiple="true" class="filter_check" style="display:none"></select>').appendTo( TH );
                      var unique_data = [];
                        column.nodes().each( function ( d, j ) { 
                          var dataSearch = $(d).attr('data-search');
                          
                            if( jQuery.inArray(dataSearch, unique_data) === -1 )
                            {
                              //console.log(d);
                              Filter_Select.append( '<option  value="'+dataSearch+'">'+dataSearch+'</option>' );
                              unique_data.push( dataSearch );
                            }

                        });
                    }
                    

                    Filter_Select.on( 'change', function () {

                        var search =  $(this).val(); 
                        //console.log( search );
                        if(search.length) search= '^('+search.join('|') +')$'; 

                       var  class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);  

                       var cur_column = api.column( '.'+class_name+'_TH' );
                       
                        cur_column.search( search, true, false ).draw();
                    } );
                      
                        //console.log(select.attr('style'));
                        Filter_Select.formSelect(); 

                   
                      }); 
        }

    });


     ColVis_Hide( source_table , hidden_coulmns );

     // Filter_IconWrap();

      Change_Sorting_Event( source_table ); 

      ColReorder_Backend_Update ( source_table , 'lead_source' );

      ColVis_Backend_Update ( source_table , 'lead_source' );


         var Reset_filter = "<li><button id='reset_filter_button1' >Reset Filter</button></li>";
      
$("#alltask_wrapper .toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

$('#reset_filter_button1').click(Trigger_To_Reset_Filter1);




}


function initialize_alltask_status_datatable()
{
  
  <?php 
          $column_setting = Firm_column_settings('lead_status');


        ?>
         var column_order = <?php echo $column_setting['order'] ?>;
        
        var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;

        var column_ordering = [];

        $.each(column_order,function(i,v){

          var index = $('#alltask_status thead  th.'+v).index();

          if(index!==-1)
          {
            column_ordering.push(index);
          }
        });
   var numCols = $('#alltask_status thead th').length;  
    status_table = $('#alltask_status').DataTable({

           "dom": '<"toolbar-table" B>lfrtip',
           buttons: [
            {
                extend: 'colvis',
                columns: ':not(.Exc-colvis)'
            }
          ],
          order: [],//ITS FOR DISABLE SORTING
          columnDefs: 
          [
            {"orderable": false,"targets": ['status_chk_TH','action_TH']}
          ],

        
          fixedHeader: {
            header: true,
            footer: true
        },
         
         colReorder: 
          {
            realtime: false,
            order:column_ordering,
            fixedColumnsLeft : 1,
            fixedColumnsRight:1

          },
          initComplete: function () { 


                                 var api = this.api();

              api.columns('.hasFilter').every( function () {

                        var column = this;
                        var TH = $(column.header());
                        var Filter_Select = TH.find('.filter_check');
                        ///alert(Filter_Select.length);
                        // alert('check');
                    if( !Filter_Select.length )
                    {
                         // alert('check1');
                      Filter_Select = $('<select multiple="true" class="filter_check" style="display:none"></select>').appendTo( TH );
                      var unique_data = [];
                        column.nodes().each( function ( d, j ) { 
                          var dataSearch = $(d).attr('data-search');
                          
                            if( jQuery.inArray(dataSearch, unique_data) === -1 )
                            {
                              //console.log(d);
                              Filter_Select.append( '<option  value="'+dataSearch+'">'+dataSearch+'</option>' );
                              unique_data.push( dataSearch );
                            }

                        });
                    }
                    

                    Filter_Select.on( 'change', function () {

                        var search =  $(this).val(); 
                        //console.log( search );
                        if(search.length) search= '^('+search.join('|') +')$'; 

                       var  class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);  

                       var cur_column = api.column( '.'+class_name+'_TH' );
                       
                        cur_column.search( search, true, false ).draw();
                    } );
                      
                        //console.log(select.attr('style'));
                        Filter_Select.formSelect(); 

                   
                      }); 
        }

    });


     ColVis_Hide( status_table , hidden_coulmns );

     // Filter_IconWrap();

      Change_Sorting_Event( status_table ); 

      ColReorder_Backend_Update ( status_table , 'lead_status' );

      ColVis_Backend_Update ( status_table , 'lead_status' );


         var Reset_filter = "<li><button id='reset_filter_button2' >Reset Filter</button></li>";
      
$("#alltask_status_wrapper .toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

$('#reset_filter_button2').click(Trigger_To_Reset_Filter2);




}


function initialize_datatable(response=null)
{     
  <?php 
          $column_setting = Firm_column_settings('leads');

        ?>
        if(response==null)
            {
        var column_order = <?php echo $column_setting['order'] ?>;
        
        var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;
          }
          else
          {
            var  order=[];
  var hidden=[];
     
     JSON.parse(response.order, (key, value) => {

      order.push(value);
     
     });
       JSON.parse(response.hidden, (key, value) => {
        hidden.push(value);
     
     });

       //var newArr = 

        var column_order = order.slice(0, -1);
        
        var hidden_coulmns =  hidden.slice(0, -1);

          }

      //  console.log(column_order);

        var column_ordering = [];

        $.each(column_order,function(i,v){

          var index = $('#all_leads thead  th.'+v).index();

          if(index!==-1)
          {
            column_ordering.push(index);
          }
        });

      stop_filter();
      var check=0;
      var check1=0;
      var numCols = $('#all_leads thead th').length;  

    //  alert(column_ordering.length);
   //   alert(numCols); 
       table = $('#all_leads').DataTable({

         "dom": '<"toolbar-table" B>lfrtip',
           buttons: [
            {
                extend: 'colvis',
                columns: ':not(.Exc-colvis)'
            }
          ],
          order: [],//ITS FOR DISABLE SORTING
          columnDefs: 
          [
            {"orderable": false,"targets": ['lead_chk_TH','action_TH']}
          ],

        
          fixedHeader: {
            header: true,
            footer: true
        },
         
         colReorder: 
          {
            realtime: false,
            order:column_ordering,
            fixedColumnsLeft : 2,
            fixedColumnsRight:1

          },
          initComplete: function () { 


                                 var api = this.api();

              api.columns('.hasFilter').every( function () {

                        var column = this;
                        var TH = $(column.header());
                        var Filter_Select = TH.find('.filter_check');
                        ///alert(Filter_Select.length);
                        // alert('check');
                    if( !Filter_Select.length )
                    {
                         // alert('check1');
                      Filter_Select = $('<select multiple="true" class="filter_check" style="display:none"></select>').appendTo( TH );
                      var unique_data = [];
                        column.nodes().each( function ( d, j ) { 
                          var dataSearch = $(d).attr('data-search');
                          
                            if( jQuery.inArray(dataSearch, unique_data) === -1 )
                            {
                              //console.log(d);
                              Filter_Select.append( '<option  value="'+dataSearch+'">'+dataSearch+'</option>' );
                              unique_data.push( dataSearch );
                            }

                        });
                    }
                    

                    Filter_Select.on( 'change', function () {

                        var search =  $(this).val(); 
                        //console.log( search );
                        if(search.length) search= '^('+search.join('|') +')$'; 

                       var  class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);  

                       var cur_column = api.column( '.'+class_name+'_TH' );
                       
                        cur_column.search( search, true, false ).draw();
                    } );
                      
                        //console.log(select.attr('style'));
                        Filter_Select.formSelect(); 

                   
                      }); 

                 
                 

       

        }
    });
             ColVis_Hide( table , hidden_coulmns );

      Filter_IconWrap();

      Change_Sorting_Event( table ); 

      ColReorder_Backend_Update ( table , 'leads' );

      ColVis_Backend_Update ( table , 'leads' );


          $(document).on('click', '.status_filter_new', function(){

           // alert('check');
          $('.status_filter_new').removeClass('archive_help'); 
         $(this).addClass('archive_help');
         

          var col = $.trim( $(this).attr('data-searchCol') );
          var val = $.trim( $(this).attr('data-id') );
          console.log('before trigger');
          Trigger_To_Reset_Filter();
          console.log('after trigger before search');
          table.column('.'+col).search(val,true,false).draw();
          console.log('after  search');
        });
/*
      for(j=2;j<numCols;j++){  
          $('#leads_'+j).on('change', function(){  
           //alert('ok');
            var result=$(this).attr('id').split('_');      
             var c=result[1]; 
           //  alert(c);        
              var search = [];              
              $.each($('#leads_'+c+ ' option:selected'), function(){                
                  search.push($(this).val());
              });      
              search = search.join('|');             
              if(c==8){               
                c=Number(c) + 1;
              } 

              if(c==2){               
                c=Number(c) + 1;
              } 

              if(c==10){               
                c=Number(c) + 1;
              } 
              if(c==12){               
                c=Number(c) + 1;
              } 
//alert(c);
                         
              table.column(c).search(search, true, false).draw();  
          });
       }*/


   $('.toolbar-table select').on('change',function(){
        if( $(this).attr('id') != 'export_report' )
        {
          var search =  $(this).val(); 

          if(search.length) search= '^('+search.join('|') +')$'; 

          var col = $.trim( $(this).attr('data-searchCol') );

          //console.log(col+"value"+val);

          table.column('.'+col).search(search,true,false).draw();

        }
      });

// $("th .themicond").on('click',function(e) {
//  // alert("ok");
//   if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
//   $(this).parent().find('.dropdown-content').removeClass('Show_content');
//   }else{
//    $('.dropdown-content').removeClass('Show_content');
//    $(this).parent().find('.dropdown-content').addClass('Show_content');
//   }
//   $(this).parent().find('.select-wrapper').toggleClass('special');
//       if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
//        // alert('yes');

//       }else{
//         $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
//         $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );

//       }
// });



  // Reset js by Ram//

      var Reset_filter = "<li><button id='reset_filter_button' >Reset Filter</button></li>";
      
$("div.toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

$('#reset_filter_button').click(Trigger_To_Reset_Filter);

      // End Reset//


}
     function  Trigger_To_Reset_Filter()
              {
                  $('.LoadingImage').show();

                  $('div.toolbar-table .toolbar-search li.dropdown-chose').each(function(){
                    $(this).trigger('click');
                  });

                  $('#select_all_leads').prop('checked',false);
                  $('#select_all_leads').trigger('change');

                  MainStatus_Tab ="all_task";
                  Redraw_Table(table);

                 // Redraw_Table(TaskTable_Instance);
                   
              }

               function  Trigger_To_Reset_Filter1()
              {
                  $('.LoadingImage').show();

                  $('div.toolbar-table .toolbar-search li.dropdown-chose').each(function(){
                    $(this).trigger('click');
                  });

                  $('#select_all_source').prop('checked',false);
                  $('#select_all_source').trigger('change');

                  MainStatus_Tab ="all_task";
                  Redraw_Table(source_table);

                 // Redraw_Table(TaskTable_Instance);
                   
              }

                      function  Trigger_To_Reset_Filter2()
              {
                  $('.LoadingImage').show();

                  $('div.toolbar-table .toolbar-search li.dropdown-chose').each(function(){
                    $(this).trigger('click');
                  });

                  $('#select_all_status').prop('checked',false);
                  $('#select_all_status').trigger('change');

                  MainStatus_Tab ="all_task";
                  Redraw_Table(status_table);

                 // Redraw_Table(TaskTable_Instance);
                   
              }

function get_selected_rows(table_obj,checkbox_cls)
{ 
 
    var ids=[];

    table_obj.column(0).nodes().to$().each(function(index) {
      if($(this).find("."+checkbox_cls).is(":checked"))
      {
        var data_id=checkbox_cls.replace("_checkbox","");

        ids.push($(this).find("."+checkbox_cls).attr("data-"+data_id+"-id"));
      }
    });
    return ids;
}


function delete_leads(obj)
{ 
  $("#delete_leads_id").val(JSON.stringify([$(obj).attr('data-id')]));
}
function archive_leads(obj)
{  
  $("#archive_leads_id").val(JSON.stringify([$(obj).attr('data-id')]));
}
function delete_leads_action()
{       var ids=$("#delete_leads_id").val();
//alert(ids);
        $("#leads_deleteconfirmation").modal("hide");
        $.ajax({  
        type: "POST",
        url: "<?php echo base_url().'Leads/delete_leads';?>",        
        data: {'ids':ids},
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {

          $(".LoadingImage").hide();

          if(parseInt(data))
          {
             $(".popup_info_box .position-alert1").html("Delete  successfully ...");
             $(".popup_info_box").show();
             setTimeout(function(){location.reload();},1500);
          }
        }
      });
}
function archive_leads_action()
{
  var id=$("#archive_leads_id").val();
  $("#confirm_archive").modal("hide");
  $.ajax({
          url: '<?php echo base_url();?>leads/archive_leads',
          type : 'POST',
          data : {'id':id},                    
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data) {
             
               $(".LoadingImage").hide();
               if(parseInt(data))
          {
             $(".popup_info_box .position-alert1").html("Archive  successfully ...");
             $(".popup_info_box").show();
             setTimeout(function(){location.reload();},1500);
          }
            }
          });
}


function delete_source(obj)
{
  $("#delete_source_id").val(JSON.stringify([$(obj).attr("data-id")]));
}


function delete_source_action()
{
  var id=$("#delete_source_id").val();
  $.ajax({
        type: "POST",
        url: "<?php echo base_url().'Leads/delete_source';?>",
        cache: false,
        data: {"ids":id},
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          $(".LoadingImage").hide();
         if(parseInt(data))
          {
             $(".popup_info_box .position-alert1").html("Delete  successfully ...");
             $(".popup_info_box").show();
             setTimeout(function(){location.reload();},1500);
          }          
        }
      });
}
function delete_status(obj)
{
  $("#delete_status_id").val(JSON.stringify([$(obj).attr("data-id")]));
}

function delete_status_action()
{
  var id=$("#delete_status_id").val();
  $.ajax({
        type: "POST",
        url: "<?php echo base_url().'Leads/delete_status';?>",
        cache: false,
        data: {"ids":id},
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          $(".LoadingImage").hide();
           if(parseInt(data))
          {
             $(".popup_info_box .position-alert1").html("Delete  successfully ...");
             $(".popup_info_box").show();
             setTimeout(function(){location.reload();},1500);
          }           
        }
      });
}

function toggle_action_buttons(i,cls)
{
if(cls=="leads_checkbox")
{

  var active_status=$(".archive_help").length;


    if(i)
    { 
      $("#assign_member_button").show();
      if(active_status>0 && $('.archive_help').attr('data-status')=="Archive")$("#unarchive_leads_button").show();
      else $("#archive_leads_button").show();
      $("#delete_leads_button").show();

    }
    else
    {
      $("#assign_member_button").hide();
      if(active_status>0 && $('.archive_help').attr('data-status')=="Archive")$("#unarchive_leads_button").hide();
      else $("#archive_leads_button").hide();
      $("#delete_leads_button").hide();
    }
 
   

}
else if(cls=="source_checkbox")
{
  if(i)$("#delete_source_button").show();
  else $("#delete_source_button").hide();
}
else if(cls=="status_checkbox")
{
  if(i)$("#delete_status_button").show();
  else $("#delete_status_button").hide();
}

}

  $(document).ready(function (){
    

    initialize_datatable();

    initialize_alltask_datatable();

     initialize_alltask_status_datatable();


    /** for leads source **/
   // source_table=$("#alltask").DataTable({
   
   // });
   // status_table=$("#alltask_status").DataTable({
  
   // });

$('.tap-toggle').click(function(){
      $('.tap-toggle').removeClass("active");   
      $('.tap-content').attr("style","display:none");    
      var id=$(this).attr("data-tap");      
      $(id).attr("style","display:block");
      $(this).addClass("active");

    });

$("#delete_leads_button").click(function(){
  var ids_array=get_selected_rows(table,"leads_checkbox");
  $("#delete_leads_id").val(JSON.stringify(ids_array));
});

$("#archive_leads_button").click(function(){
  var ids_array=get_selected_rows(table,"leads_checkbox");
  $("#archive_leads_id").val(JSON.stringify(ids_array));
});

$("#unarchive_leads_button").click(function(){
  var ids_array=get_selected_rows(table,"leads_checkbox");
  var id=JSON.stringify(ids_array);
  $.ajax({
          url: '<?php echo base_url();?>leads/unarchive_leads',
          type : 'POST',
          data : {'id':id},                    
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data) {             
               $(".LoadingImage").hide();
                if(parseInt(data))
          {
             $(".popup_info_box .position-alert1").html("Delete  successfully ...");
             $(".popup_info_box").show();
             setTimeout(function(){location.reload();},1500);
          } 
            }
          });
});

  $(document).on('click','#delete_source_button', function() {
    var id=get_selected_rows(source_table,"source_checkboxo");
    $("#delete_source_id").val(JSON.stringify(id));    
});
$(document).on('click','#delete_status_button', function() {  
    var id=get_selected_rows(status_table,"status_checkbox");    
    $("#delete_status_id").val(JSON.stringify(id));
});

     
   
   // $('#spl-datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   //$('.spl-datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
    $("body").delegate(".spl-datepicker", "focusin", function(){
          $(this).datepicker({ dateFormat: 'dd/mm/yy' }).val();
           //var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
   
      });
   
     $( ".leads_popup" ).on('shown.bs.modal', function(){
   
       $("body").css("overflow-y", "hidden");
   });
   
      $('.leads_popup').on('hidden.bs.modal', function () {
       // do something…
        $("body").css({"overflow":"auto"});
    })
    
   
   
        //   var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
   /*$( ".datepicker" ).datepicker({
          dateFormat: 'yy-mm-dd'
      });*/
   
   $('.tags').tagsinput({
        allowDuplicates: true
      });
      
      $('.tags').on('itemAdded', function(item, tag) {
          $('.items').html('');
   var tags = $('.tags').tagsinput('items');
        
      });



   });
   
   $("#contact_today").click(function(){
    if($("#contact_today").is(':checked')){
      $(".selectDate").css('display','none');
    }else{
      $(".selectDate").css('display','block');
    }
   });
   
   $(".contact_update_today").click(function(){
    var id = $(this).attr("data-id");
    if($(this).is(':checked')){
   
      $("#selectDate_update_"+id).css('display','none');
    }else{
      $("#selectDate_update_"+id).css('display','block');
    }
   });
   
    // $("#all_leads").dataTable({
    //       "iDisplayLength": 10,
    //       "scrollX": true
    //    });



</script>
<script type="text/javascript">
  $(document).ready(function(){    
    $.validator.setDefaults({
        ignore: []
    });
});
   $(document).ready(function(){
   
      $( "#leads_form" ).validate({
         errorPlacement: function(error, element) {
                  if (element.attr("name") == "lead_status" )
                    error.insertAfter(".dropdown-sin-1");
                  else if (element.attr("name") == "source[]" )
                    error.insertAfter(".dropdown-sin-2");
                  else if (element.attr("name") == "assigned[]" )
                    error.insertAfter(".dropdown-sin-3");
             
                else
                    error.insertAfter(element);
            },
        rules: {
          lead_status:"required",
         
          "source[]": "required",  
          "assignees[]":"required",
          name: "required",
           address:"required",
          description:"required",
           email_address:{required: true,email:true},
           phone : {required:true,
                         number: true},
 
           company: {
            required:true,
           //   required: function(element) {
         
           //   if ($("#company").val()=='') {
           //      return false;
           //   }
           //   else {
           //   return true;
           //   }
           // },

   },
          //company:"required",
        },
        messages: {
          lead_status: "Please enter your Status",
          "source[]": "Please enter your Source",
          "assignees[]": "Please Selet Assign",
          name: "Please enter your Name",
          address:"Please enter your address",
          description:"Please enter your description",
          email_address:"Please enter Your Email",
          phone:"Please enter Your Phone no",
          company:"Please Select Company",
        },
        highlight: function(element) {
        $(element).parent().parent().addClass('add-form-space');
      },
      unhighlight: function(element) {
        $(element).parent().parent().removeClass('add-form-space');
      }, submitHandler: function(form) {
       // alert('check');
          var Assignees=[];
      
          $('.add_tree .comboTreeItemTitle input[type="checkbox"]:checked').each(function(){
            var id = $(this).closest('.add_tree .comboTreeItemTitle').attr('data-id');
            var id = id.split('_');
            Assignees.push( id[0] );
             });
        
           var assign_role = Assignees.join(',');
           $('#assign_role').val(assign_role);
             $(form).attr('action', '<?php echo base_url()?>leads/addLeads');
              $(form)[0].submit();            
   

      }
        
      });
   
   
      $( ".edit_leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });
   
   
   $('.reminder_save').click(function(e){
   e.preventDefault();
   //alert('ggg');
   var id = $(this).attr("data-id");
   //alert(id);
   var rem_date = $(".spl-datepicker_"+id).val();
   var rem_desc = $("#reminder_description_"+id).val();
   var staff_id = $("#staff_"+id).val();
   if($("#notify_by_email_"+id).prop('checked') == true){
   var email_sent = "yes";
   }else{
   var email_sent = "no";
   }
   //alert(email_sent);
   var data = {};
   data['reminder_date'] = rem_date;
   data['reminder_desc'] = rem_desc;
   data['lead_id'] = id;
   data['rel_type'] = 'Leads';
   data['email_sent'] = email_sent;
   data['staff_id'] = staff_id;
   var i=0;
   if(rem_date=='')
   {
     i=1;
     $('#reminder_date_err_'+id).html('Please select reminder date');
   }else{
     i=0;
     $('#reminder_date_err_'+id).html('');
   }
   if(rem_desc=='')
   {
     j=1;
     $('#description_err_'+id).html('Please enter description');
   }else{
     j=0;
     $('#description_err_'+id).html('');
   }
   
   
   if(i==0 && j==0){
       $(".LoadingImage").show();
   
       $.ajax({
       url: '<?php echo base_url();?>leads/add_new_reminder/',
       type : 'POST',
       data : data,
       success: function(data) {
           $(".LoadingImage").hide();
   
         $(".spl-datepicker_"+id).val('');
         $("#reminder_description_"+id).val('');
         $("#staff_"+id).val('');        
         $('#notify_by_email_'+id).prop('checked', false); // Unchecks it
   
         $('#add-reminders_'+id).modal('hide');
   
         $('.response_res').html(data);
         /** rspt 18-04-2018 **/
         $('.reminder_popups_'+id).html('');
         $('.reminder_popups_'+id).load("<?php echo base_url(); ?>leads/reminder_edit_poup/"+id);
         /** end rspt 18-04-2018 **/
   
        // ('#example2').DataTable()
           var tabletask1 =$(".example2").dataTable({
            "iDisplayLength": 10,
         responsive: true
         });
           /** for permission **/
            // access_permission_function();
             /** endof permisison **/
       },
       });
   }
   
   });
   
   
   $('.edit_reminder_save').click(function(e){
   e.preventDefault();
   
   //alert('ggg');
   var id = $(this).attr("data-id");
   var l_id = $(this).attr("data-value");
   //alert(id);
   var rem_date = $(".edit_spl-datepicker_"+id).val();
   var rem_desc = $("#edit_reminder_description_"+id).val();
   var staff_id = $("#edit_staff_"+id).val();
   var l_id = l_id;
   if($("#edit_notify_by_email_"+id).prop('checked') == true){
   var email_sent = "yes";
   }else{
   var email_sent = "no";
   }
   //alert(email_sent);
   var data = {};
   data['reminder_date'] = rem_date;
   data['reminder_desc'] = rem_desc;
   data['id'] = id;
   data['rel_type'] = 'Leads';
   data['email_sent'] = email_sent;
   data['staff_id'] = staff_id;
   data['lead_id'] = l_id;
   var i=0;
   
   if(rem_date=='')
   {
     i=1;
     $('#edit_reminder_date_err_'+id).html('Please select reminder date');
   }else{
     i=0;
     $('#edit_reminder_date_err_'+id).html();
   }
   if(rem_desc=='')
   {
     j=1;
     $('#edit_description_err_'+id).html('Please enter description');
   }else{
     j=0;
     $('#edit_description_err_'+id).html();
   }
   
   
   if(i==0 && j==0){
                 $(".LoadingImage").show();
   
       $.ajax({
       url: '<?php echo base_url();?>leads/edit_reminder/',
       type : 'POST',
       data : data,
       success: function(data) {
           $(".LoadingImage").hide();
   
         $(".edit_spl-datepicker_"+id).val('');
         $("#edit_reminder_description_"+id).val('');
         $("#edit_staff_"+id).val('');        
         $('#edit_notify_by_email_'+id).prop('checked', false); // Unchecks it
   
         $('#edit-reminders_'+id).modal('hide');
   
         $('.response_res').html(data);
        // ('#example2').DataTable()
           var tabletask1 =$(".example2").dataTable({
            "iDisplayLength": 10,
         responsive: true
         });
           /** for permission **/
          //   access_permission_function();
             /** endof permisison **/
       },
       });
   }
   
   });
   
   
   });
   
  
      $(document).on("click",".sortby_filter",function(){ 
      $(".LoadingImage").show();
   
      var sortby_val = $(this).attr("id");
      var table_name='leads';
      var data={};
      data['sortby_val'] = sortby_val;
      $.ajax({
      url: '<?php echo base_url();?>leads/sort_by/',
      type : 'POST',
      data : data,
      success: function(data) { 


   $(".sortrec").html(data);   

   var all_Codes = $.ajax({
    type: "POST",
     dataType: "json", 
     data:{           
           table_name:table_name
          }, 
          url: "<?=base_url()?>leads/get_hidden_data", async: false}).responseJSON;

   console.log(all_Codes);
  
   initialize_datatable(all_Codes);

   /** for permission **/
          //   access_permission_function();
             /** endof permisison **/ 
                     $(".LoadingImage").hide();
      },
      });
   
   });


   // $("#import_leads").validate({

   //      errorPlacement: function(error, element) {
   //              if (element.attr("name") == "company" )
   //                  error.insertAfter(".dropdown-sin-4");
   //              else if (element.attr("name") == "lead_status" )
   //                  error.insertAfter(".dropdown-sin-11");
   //                else if (element.attr("name") == "source[]" )
   //                  error.insertAfter(".dropdown-sin-12");
   //                else if (element.attr("name") == "assigned[]" )
   //                  error.insertAfter(".dropdown-sin-13");
             
   //              else
   //                  error.insertAfter(element);
   //          },
   //   rules: {
   //      file: {required: true, accept: "csv"},
   
   //       lead_status:"required",
         
   //        "source[]": "required",  
   //        "assigned[]":"required",
   //       // source: "required",  
   //        name: "required",  
   //      },
   //      messages: {
   //        file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},
   
   //       lead_status: "Please enter your Status",
   //        "source[]": "Please enter your Source",
   //        "assigned[]": "Please enter your Assign",
   //        name: "Please enter your Name"
   //      },
   
   // });
                            
    $(document).ready(function(){    
    $.validator.setDefaults({
        ignore: []
    });
});
   $(document).ready(function(){

/*    jQuery.validator.addMethod("validate_unique", function(value, element) {
    //return this.optional(element) || (parseFloat(value) > 0);
    var formData = new FormData($("#import_leads")[0]);
    console.log(formdata);
    $.ajax({url: '<?php echo base_url();?>leads/check_mainids_importfile/',
      dataType : 'json',
      type : 'POST',
     data : formData,
     contentType : false,
     processData : false,
      success:function(response)
      {
        
      }


    });
    return false;

}, "Please Check Mail Id, Duplicates are Found.");*/
   
      $( "#import_leads" ).validate({
        
          rules: {
          file: {required: true, accept: "csv"},  
          lead_status:"required",         
          "source": "required",  
          "assigned[]":"required",
          "review_manager[]":"required"        
          //company:"required",
        },
        messages: {
          lead_status: "Please enter your Status",
          "source[]": "Please enter your Source",
          "assigned[]": "Please enter your Assign",
          "review_manager[]":"Please Select manager",
          file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},
        },errorPlacement: function(error, element) {
                 if (element.attr("name") == "lead_status" )
                    error.insertAfter(".dropdown-sin-11");
                  else if (element.attr("name") == "source[]" )
                    error.insertAfter(".dropdown-sin-12");
                  else if (element.attr("name") == "assigned[]" )
                    error.insertAfter(".dropdown-sin-13"); 
                    else if (element.attr("name") == "review_manager[]" )
                    error.insertAfter(".dropdown-sin-14");
                    else if (element.attr("name") == "file" )
                     $("input[name=file]").closest(".formdata1").append(error);                     
                else
                    error.insertAfter(element);
            },submitHandler: function(form)
        {
          var formData = new FormData($("#import_leads")[0]);
           $.ajax({
          url: '<?php echo base_url();?>leads/import_leads/',
           dataType : 'json',
           type : 'POST',
           data : formData,
           contentType : false,
           processData : false,
           beforeSend:function(){$(".LoadingImage").show();},
      success:function(data)
      { 
        
       if(data['status']=="0")
       {
        //alert(data['file']);
        if(data['file'].length)
        {
         $(".custom_file_error").text(data['file']);
         $(".custom_file_error").show();
        }
        else
        {
          $(".custom_file_error").text('');
        }
        $(".LoadingImage").hide();
       }
       else
       {
        $(".LoadingImage").hide();
        //$("#import_leads").trigger("reset");
        //alert("INSIDE");
        $(".popup_info_box .position-alert1").html("Import successfully Finished...");
        $(".popup_info_box").show();
        setTimeout(function(){location.reload();},1000);        
       }
      }

    });
        } 
        

          
      });

    });
</script>
<script type="text/javascript">




   $(document).ready(function(){

      $('.edit-toggle1').click(function(){
        $('.proposal-down').slideToggle(300);
      })
  
   
   
   $(".statuschange").click(function(){
   var status_val = $(this).attr("id");
   var id = $(this).attr("data-id");
   var data={};
   data['status_val'] = status_val;
   data['id'] = id;
   
   $.ajax({
      url: '<?php echo base_url();?>leads/status_update/',
      type : 'POST',
      data : data,
      success: function(data) {
        $('.succ').css('display','block');
        $('.succ').html('Status has been updated successfully');
         setTimeout(function() {
    $('.succ').fadeOut('fast');
   }, 1000);
          var cnt = data.split("-");
          $("#Lost").html(cnt[0]);
          $("#Junk").html(cnt[1]);
        $(".LoadingImage").hide();
   $(".proposal-down").css('display','none');
   $('.leadstatus').html(status_val);
      },
      });
   });
   
   
    // $(".status_filter").click(function(){
  $(document).on('click','.status_filter_new_all',function(){

      $('.status_filter_new').removeClass('intro');
    //  $(this).addClass("intro");
    $(".LoadingImage").show();
      var filter_val = 'all';
     $('#leads_status_us').val('');
      var data={};
   data['filter_val'] = filter_val;
   $.ajax({
      url: '<?php echo base_url();?>leads/filter_status/',
      type : 'POST',
      data : data,
      success: function(data) {

        $(".LoadingImage").hide();
       $(".sortrec").html(data);  

    

initialize_datatable();






   // $("#all_leads").dataTable({
   // "iDisplayLength": 10,
   // });
/*13.06.2018 */

    // var table = $('#all_leads').DataTable();
    /** for permission **/
             access_permission_function();
             /** endof permisison **/
    
    /*13.06.2018 */
      },
      });
   });


/*
    $(document).on('click','.status_filter_new',function(){
     
      $('.status_filter_new').removeClass('intro');
      $(this).addClass("intro");
      $("#archive_leads_button").hide();
    $(".LoadingImage").show();
      var filter_val = $(this).attr("id");
      $('#leads_status_us').val(filter_val);
      var data={};
   data['filter_val'] = filter_val;
   $.ajax({
      url: '<?php echo base_url();?>leads/filter_status/',
      type : 'POST',
      data : data,
      success: function(data) {

        $(".LoadingImage").hide();
        $(".sortrec").html(data);
      
        initialize_datatable();
   // $("#all_leads").dataTable({
   // "iDisplayLength": 10,
   // });
      
   /*13.06.2018 */
   /** for permission **/
           //  access_permission_function();
             /** endof permisison **/

/*13.06.2018 */
   /*   },
      });

   }); */
   

















    $('.add_activity').click(function(e){
      e.preventDefault();
      var i = $(this).attr("data-id");
      var values = $("#activity_log_"+i).val();
      if(values!=''){
      $(".error").hide();
      var data={};
   data['values'] = values;
   data['id'] = i;
   data['module'] = "Lead";
   $.ajax({
        url: '<?php echo base_url();?>leads/add_activity_log/',
        type : 'POST',
        data : data,
        success: function(data) {
          $(".LoadingImage").hide();
          $('.activity_log').val('');
    $(".leads_activity_log").html(data);   
        },
      });
   }else{
   $("#error"+i).html('<span>Please Enter Your Activity log.</span>');
   }
    });
   
   
   
    /* 09-03-2018 */
   $(".note_contact").click(function(){
   var i = $(this).attr("data-id");
   var radioValue = $("input[name='note_contact']:checked").val();
   /*alert(radioValue);
   alert(i);*/
            if(radioValue=="got_touch"){
               $('.note_date_'+i).css('display','block');
            }else{
              $('.note_date_'+i).css('display','none');
            }
   });
   
   $('.add_note_rec').click(function(e){
   e.preventDefault();
   $(".LoadingImage").show();
   
   var i = $(this).attr("data-id");
   var val = $("#note_rec_"+i).val();
   var contact_date = $("#note_contact_date_"+i).val();
   //alert(val);
   if(val!='')
   {
   $(".error").hide();
      var data={};
   data['values'] = val;
   data['id'] = i;
   data['contact_date'] = contact_date;
   $.ajax({
        url: '<?php echo base_url();?>leads/add_note/',
        type : 'POST',
        data : data,
        success: function(data) {
          $(".LoadingImage").hide();
          $('.note_val').val('');
          $('.note_d').val('');
          $(".note_contact").prop('checked', false);
   
          $('.note_date_'+i).css('display','none');
    $(".add_note_rec_load").html(data);   
        },
      });
   }else{
   $("#error_note"+i).html('<span>Please Enter Your Notes.</span>');
   }
   });
   
   $(document).on('click','.delete_note',function(e){
   $(".LoadingImage").show();
   
   var i = $(this).attr("data-id");
   //var lead_id = $(this).val();
   var data={};
   data['note_id'] = i;
   $.ajax({
        url: '<?php echo base_url();?>leads/delete_note/',
        type : 'POST',
        data : data,
        success: function(data) {
          $(".LoadingImage").hide();
          $('.note_val').val('');
          $('.note_d').val('');
          $(".note_contact").prop('checked', false);
   
          $('.note_date_'+i).css('display','none');
    $(".add_note_rec_load").html(data);   
        },
      });
   
   });
   
   $('.lead_attachments').on('change', function () {
      $(".LoadingImage").show();
   
        var i = $(this).attr("data-id");
   
                   var file_data = $('#lead_attachments'+i).prop('files')[0];
                   var form_data = new FormData();
                   form_data.append('file', file_data);
                   form_data.append('id', i);
                    var data={};
      data['form_data'] = form_data;
      data['id'] = i;
   
                   $.ajax({
                        url: '<?php echo base_url();?>leads/lead_attachment/', // point to server-side controller method
                     
                       dataType: 'text', // what to expect back from the server
                       cache: false,
                       contentType: false,
                       processData: false,
                       data: form_data,
                       type: 'post',
                       success: function (response) {
                          $(".LoadingImage").hide();
                          $('.lead_attachments').val('');
                           $('.l_att').html(response); // display success response from the server
                       },
                       error: function (response) {
                            $(".LoadingImage").hide();
                          $('.lead_attachments').val('');
   
                       }
                   });
               });
   
   function delete_lead_attachment(id,l_id)
   {
   if(confirm('Are you sure want to delete?')){
   $(".LoadingImage").show();
   var data={};
   data['a_id'] = id;
   data['l_id'] = l_id;
   
   $.ajax({
         url: '<?php echo base_url();?>leads/delete_lead_attachment/', // point to server-side controller method
      
        dataType: 'text', // what to expect back from the server
       
        data: data,
        type: 'post',
        success: function (response) {
            $(".LoadingImage").hide();
            $('.l_att').html(response); // display success response from the server
        },
        error: function (response) {
              $(".LoadingImage").hide();
        }
    });
   }
   }
   
   function delete_reminder(l_id,id){
           if(confirm('Are you sure want to delete?')){
           $(".LoadingImage").show();
           var srt = l_id.split(',');
           var data={};
           data['r_id'] = srt[1];
           data['l_id'] = srt[0];
               $.ajax({
                 url: '<?php echo base_url();?>leads/delete_reminder/', // point to server-side controller method
                 dataType: 'text', // what to expect back from the server
                 data: data,
                 type: 'post',
                 success: function (response) {
                 $(".LoadingImage").hide();
                 $('.response_res').html(response); // display success response from the server
                   var tabletask1 =$(".example2").dataTable({
                   "iDisplayLength": 10,
                   responsive: true
                   });
                   /** for permission **/
          //   access_permission_function();
             /** endof permisison **/
                 },
                 error: function (response) {
                 $(".LoadingImage").hide();
                 }
               });
           }
   }
   
     $(document).on('change','.reminder_export',function(e){
       var type=$(this).val();
         var leadId = $(this).attr("data-id");
    if(type=='csv')
    {
    window.location.href="<?php echo base_url().'leads/reminder_excel?leadid="+leadId+"'?>";
    }else if(type=='pdf')
    {
    window.location.href="<?php echo base_url().'leads/reminder_pdf?leadid="+leadId+"'?>";
    }else if(type=='html')
    {
    window.open(
    "<?php echo base_url().'leads/reminder_html?leadid="+leadId+"'?>",
    '_blank' // <- This is what makes it open in a new window.
    );
    // window.location.href="<?php echo base_url().'user/task_html?status="+status+"&priority="+priority+"'?>";
    }
   
   
     });
   
     $(document).on('change','.task_export',function(e){
       var type=$(this).val();
         var leadId = $(this).attr("data-id");
    if(type=='csv')
    {
    window.location.href="<?php echo base_url().'leads/l_task_excel?leadid="+leadId+"'?>";
    }else if(type=='pdf')
    {
    window.location.href="<?php echo base_url().'leads/l_task_pdf?leadid="+leadId+"'?>";
    }else if(type=='html')
    {
    window.open(
    "<?php echo base_url().'leads/l_task_html?leadid="+leadId+"'?>",
    '_blank' // <- This is what makes it open in a new window.
    );
    // window.location.href="<?php echo base_url().'user/task_html?status="+status+"&priority="+priority+"'?>";
    }
   
   
     });
   
    // $(document).on('change','.task_leads',function(e)){
     $(document).on("click", ".task_leads", function(event){
     event.preventDefault();
   
     var id = $(this).attr("data-id");
     //var formdata =  new FormData("#lead_task_"+leadId);
   
     if($("#p-check_"+id).prop('checked') == true){
     var public_tas = "Public";
     }else{
     var public_tas = "";
     }
   
     if($("#p-check1_"+id).prop('checked') == true){
     var billable_tas = "Billable";
     }else{
     var billable_tas = "";
     }
     if($("#repeat_no_"+id).val()!='')
     {
       var repeat_no = $("#repeat_no_"+id).val();
     }else{
       var repeat_no = '';
     }
   
     if($("#durations_"+id).val()!='')
     {
       var durations = $("#durations_"+id).val();
     }else{
       var durations = '';
     }
     if($("#ends_on_"+id).val()!='')
     {
       var ends_on = $("#ends_on_"+id).val();
     }else{
       var ends_on = '';
     }
   if($("#img_name_"+id).val()!='')
   {
   var file = $("#img_name_"+id).val();
   }else{
   var file = '';
   }
   if($("#task_sub_"+id).val()!='' && $("#start_date_"+id).val()){
   //console.log(file_data);
     var data = {};
     data['task_sub'] = $("#task_sub_"+id).val();
     data['hour_rate'] = $("#hour_rate_"+id).val();
     data['start_date'] = $("#start_date_"+id).val();
     data['due_date'] = $("#due_date_"+id).val();
     data['priority'] = $("#priority_"+id).val();
     data['repeat'] = $("#repeat_"+id).val();
     data['related_to'] = $("#related_to_"+id).val();
     data['lead_to'] = $("#lead_to_"+id).val();
     data['tags'] = $("#tags_"+id).val();
     data['description'] = $("#descr_"+id).val();
     data['repeat_no'] = repeat_no;
     data['durations'] = durations;
     data['ends_on'] = ends_on;
     data['billable'] = billable_tas;
     data['public'] = public_tas;
     data['lead_id'] = id;
     data['attach_file'] = file;
   
     //data['file_data'] = $('#inputfile_'+id).prop('files')[0];   
   
   
     $.ajax({
         url: '<?php echo base_url();?>leads/leads_task/',
         type: 'post',
          data: data,
         success: function (data, status)
         {
           $('#task_leadss_'+id).html(data);
           $('#addnew_task_lead_'+id).modal('hide');
           $("#task_sub_"+id).val('');
           $("#hour_rate_"+id).val('');
           $("#start_date_"+id).val('');
           $("#due_date_"+id).val('');
           $("#priority_"+id).val('');
           $("#repeat_"+id).val('');
           $("#related_to_"+id).val('');
           $("#lead_to_"+id).val('');
           $("#tags_"+id).val('');
           $("#descr_"+id).val('');
           $("#repeat_no_"+id).val('1');
           $("#durations_"+id).val('');
           $("#ends_on_"+id).val('');
           $('#inputfile_'+id).val('');
           $("#img_name_"+id).val('');
   
         var tabletask1 =$("#example1_"+id).dataTable({
            "iDisplayLength": 10,
         responsive: true
         });
             /** for permission **/
          //   access_permission_function();
             /** endof permisison **/
          $("#task_sub_err_"+id).hide();
          $("#task_start_err_"+id).hide();
   
          /** rspt 18-04-2018 **/
         $('.task_edit_popup_'+id).html('');
         $('.task_edit_popup_'+id).load("<?php echo base_url(); ?>leads/task_edit_poup/"+id);
          /** end 18-04-2018 **/
         },
         error: function (xhr, desc, err)
         {
   
   
         }
     }); 
     } else {
       //alert($("#task_sub_"+id).val());
      // alert($("#start_date_"+id).val());
       if($("#task_sub_"+id).val()==''){
         $("#task_sub_err_"+id).html('Enter Task Subject');
       }else{
         $("#task_sub_err_"+id).hide();
       }
       if($("#start_date_"+id).val()==''){
         $("#task_start_err_"+id).html('Enter Task Start Date');
       }
       else{
          $("#task_start_err_"+id).hide();
       }
     }
     });
   
   
    $('.task_images').on('change', function () {
         var id = $(this).attr("data-id");
   
                    var file_data = $('#inputfile_'+id).prop('files')[0];
                   //console.log(file_data);
                    var form_data = new FormData();
                   form_data.append('file', file_data);
                    $.ajax({
                      //  url: 'ajaxupload/upload_file', // point to server-side controller method
                           url: '<?php echo base_url();?>leads/leads_task_image/', // point to server-side controller method
                      
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                         $('#img_name').html(response);
                        },
                        error: function (response) {
                        }
                    });
                });
     
   
   
   $(document).on('change','.repeated_too',function(){
       var id = $(this).attr("data-id");
   
   var x_cal = $(this).val();
   if(x_cal!='' && x_cal!='custom')
   {
     $('#ends_on_div_'+id).css('display','block');
     $('#rep_no_'+id).css('display','none');
     $('#rep_cnt_'+id).css('display','none');
   }else if(x_cal!='' && x_cal=='custom'){
     $('#rep_no_'+id).css('display','block');
     $('#rep_cnt_'+id).css('display','block');
     $('#ends_on_div_'+id).css('display','block');
   }else{
     $('#ends_on_div_'+id).css('display','none');
     $('#rep_no_'+id).css('display','none');
     $('#rep_cnt_'+id).css('display','none');
   }
   });
   
   function delete_lead_task(l_id)
   {
   // alert(l_id)
             if(confirm('Are you sure want to delete?')){
   
           var srt = l_id.split(',');
           var data={};
           data['t_id'] = srt[1];
           data['l_id'] = srt[0];
   
                  $.ajax({
                 url: '<?php echo base_url();?>leads/delete_task/', // point to server-side controller method
                 dataType: 'text', // what to expect back from the server
                 data: data,
                 type: 'post',
                 success: function (response) {
                 $(".LoadingImage").hide();
                 $('#task_leadss_'+l_id).html(response); // display success response from the server
                   var tabletask1 =$("#example1_"+l_id).dataTable({
                   "iDisplayLength": 10,
                   responsive: true
                   });
                   /** for permission **/
            // access_permission_function();
             /** endof permisison **/
                 },
                 error: function (response) {
                 $(".LoadingImage").hide();
                 }
               });
                }
   
   }
 });
</script>

<script type="text/javascript">






   $(document).ready(function() {
    




       setTimeout(function(){ $('.popup_success_msg').remove(); }, 3000);
    //$('#kanban_tab').load('leads/kanban_test');
   // $('#kanban_tab').load('leads/sample_test');
   
   $('.tags').tagsinput({
        allowDuplicates: true
      });
      
      $('.tags').on('itemAdded', function(item, tag) {
          $('.items').html('');
    var tags = $('.tags').tagsinput('items');
        
      });
   
   
      $( function() {
      $('#date_of_creation1,#date_of_creation2').datepicker();
    } );
   
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;
   
    slider.oninput = function() {
    output.innerHTML = this.value;
    }
   
    CKEDITOR.replace('editor4');
   
   });
   
   
      $(document).on("click", ".edit_task_leads", function(event){
      event.preventDefault();
   
      var id = $(this).attr("data-id");
      alert(id);
      var l_id = $(this).attr("data-value");
      //var formdata =  new FormData("#lead_task_"+leadId);
   
      if($("#t_edit_p-check_"+id).prop('checked') == true){
      var public_tas = "Public";
      }else{
      var public_tas = "";
      }
   
      if($("#t_edit_p-check1_"+id).prop('checked') == true){
      var billable_tas = "Billable";
      }else{
      var billable_tas = "";
      }
      if($("#t_edit_repeat_no_"+id).val()!='')
      {
        var repeat_no = $("#t_edit_repeat_no_"+id).val();
      }else{
        var repeat_no = '';
      }
   
      if($("#t_edit_durations_"+id).val()!='')
      {
        var durations = $("#t_edit_durations_"+id).val();
      }else{
        var durations = '';
      }
      if($("#t_edit_ends_on_"+id).val()!='')
      {
        var ends_on = $("#t_edit_ends_on_"+id).val();
      }else{
        var ends_on = '';
      }
   if($("#t_edit_img_name_"+id).val()!='')
   {
   var file = $("#t_edit_img_name_"+id).val();
   }else{
    var file = '';
   }
   if($("#t_edit_task_sub_"+id).val()!='' && $("#t_edit_start_date_"+id).val()){
   //console.log(file_data);
      var data = {};
      data['task_sub'] = $("#t_edit_task_sub_"+id).val();
      data['hour_rate'] = $("#t_edit_hour_rate_"+id).val();
      data['start_date'] = $("#t_edit_start_date_"+id).val();
      data['due_date'] = $("#t_edit_due_date_"+id).val();
      data['priority'] = $("#t_edit_priority_"+id).val();
      data['repeat'] = $("#t_edit_repeat_"+id).val();
      data['related_to'] = $("#t_edit_related_to_"+id).val();
      data['lead_to'] = $("#t_edit_lead_to_"+id).val();
      data['tags'] = $("#t_edit_tags_"+id).val();
      data['description'] = $("#t_edit_descr_"+id).val();
      data['repeat_no'] = repeat_no;
      data['durations'] = durations;
      data['ends_on'] = ends_on;
      data['billable'] = billable_tas;
      data['public'] = public_tas;
      data['lead_id'] = l_id;
      data['attach_file'] = file;
      data['attach_file'] = file;
      data['id'] = id;
   
      //data['file_data'] = $('#inputfile_'+id).prop('files')[0];   
   
   
      $.ajax({
          url: '<?php echo base_url();?>leads/update_task/',
          type: 'post',
           data: data,
          success: function (data, status)
          {
            $('#task_leadss_'+l_id).html(data);
            $('#edit_task_lead_'+id).modal('hide');
   
   
          var tabletask1 =$("#example1_"+l_id).dataTable({
             "iDisplayLength": 10,
          responsive: true
          });
             /** for permission **/
           //  access_permission_function();
             /** endof permisison **/
           $("#t_edit_task_sub_err_"+id).hide();
           $("#t_edit_task_start_err_"+id).hide();
          },
          error: function (xhr, desc, err)
          {
   
   
          }
      }); 
      } else {
        //alert($("#task_sub_"+id).val());
       // alert($("#start_date_"+id).val());
        if($("#t_edit_task_sub_"+id).val()==''){
          $("#t_edit_task_sub_err_"+id).html('Enter Task Subject');
        }else{
          $("#t_edit_task_sub_err_"+id).hide();
        }
        if($("#t_edit_start_date_"+id).val()==''){
          $("#t_edit_task_start_err_"+id).html('Enter Task Start Date');
        }
        else{
           $("#t_edit_task_start_err_"+id).hide();
        }
      }
      });
   
   
   $('.e_task_images').on('change', function () {
          var id = $(this).attr("data-id");
   //alert(id);
                     var file_data = $('#t_edit_inputfile_'+id).prop('files')[0];
                    //console.log(file_data);
                     var form_data = new FormData();
                    form_data.append('file', file_data);
                     $.ajax({
                       //  url: 'ajaxupload/upload_file', // point to server-side controller method
                            url: '<?php echo base_url();?>leads/leads_task_image/', // point to server-side controller method
                       
                         dataType: 'text', // what to expect back from the server
                         cache: false,
                         contentType: false,
                         processData: false,
                         data: form_data,
                         type: 'post',
                         success: function (response) {
                          $('#t_edit_img_name_'+id).val(response);
                         },
                         error: function (response) {
                         }
                     });
                 });
      
   
   
   $(document).on('change','.t_edit_repeated_too',function(){
        var id = $(this).attr("data-id");
   
    var x_cal = $(this).val();
    if(x_cal!='' && x_cal!='custom')
    {
      $('#t_edit_ends_on_div_'+id).css('display','block');
      $('#t_edit_rep_no_'+id).css('display','none');
      $('#t_edit_rep_cnt_'+id).css('display','none');
    }else if(x_cal!='' && x_cal=='custom'){
      $('#t_edit_rep_no_'+id).css('display','block');
      $('#t_edit_rep_cnt_'+id).css('display','block');
      $('#t_edit_ends_on_div_'+id).css('display','block');
    }else{
      $('#t_edit_ends_on_div_'+id).css('display','none');
      $('#t_edit_rep_no_'+id).css('display','none');
      $('#t_edit_rep_cnt_'+id).css('display','none');
    }
   });
   
   /** rspt convert to customer 18-04-2018 */
   $(document).on('click','.convert_to_customer',function(){
      var id=$(this).attr('data-id');
      var status=$(this).attr('data-status');
      // alert(id);
      // alert(status);
      var data={};
      data['id']=id;
      data['status']=status;
      $.ajax({
          url: '<?php echo base_url();?>leads/convert_to_customer/',
          type: 'post',
           data: data,
          success: function (data, status)
          {
            if(data!='wrong')
            {
              $('.convert_to_customer').css('display','none');
              //$('.lead_status_'+id).html(data);
              $('.lead_status_'+id).each(function() {
                    $(this).html(data);
                });
   
            }
            
          }
        });
   });
   /** end of 18-04-2018 **/
</script>
<script type="text/javascript">
   //$(".sortby_filter_kanban").click(function(){
    $(document).on("click",".sortby_filter_kanban",function(){ 
         $(".LoadingImage").show();
   
     var sortby_val = $(this).attr("id");
     var data={};
     data['sortby_val'] = sortby_val;
     $.ajax({
     url: '<?php echo base_url();?>leads/sort_by_kanban/',
     type : 'POST',
     data : data,
     success: function(data) {
        $(".LoadingImage").hide();
   
     $(".sortby_kanban").html(data);   
   
     },
     });
   
   });
   
   
   
   
   $('#leads_source_save').click(function(){
   var lead_source=$("#new_lead_source").val();
   if(lead_source=='')
   {
     $(".add_source_error").html('Please Enter Your Source');
   }
   else
   {  $("#leads_source_add").modal('hide');

      $(".add_source_error").html('');
       var data={};
   data['new_team']=lead_source;
   $.ajax({
      url: '<?php echo base_url();?>leads/leads_add_new_source/',
      type: 'post',
       data: data,
       beforeSend:function(){$(".LoadingImage").show();},
      success: function (data, status)
      {
         
         $('.leads_source').html(data);
       //  $('.leads_source').load('leads_source_table');
        $('.edit_source').load("<?php echo base_url(); ?>leads/leads_source_editpopup");
          source_table=$("#alltask").dataTable({
               // "iDisplayLength": 10,
              // "scrollX": true,
           });
           /** for permission **/
             //access_permission_function();
             /** endof permisison **/
             $(".LoadingImage").hide();
      }
    });
   
   }
   
   });
   
   //$('.edit_source_save').click(function(){
   $(document).on('click','.edit_source_save',function(){
   var id=$(this).attr('data-id');
   //alert(id);
   var lead_source=$("#new_lead_source_"+id).val();
   if(lead_source=='')
   {
     $(".edit_source_error_"+id).html('Please Enter Your Source');
   }
   else
   {
      $(".edit_source_error_"+id).html('');
       var data={};
   data['new_team']=lead_source;
   $.ajax({
      url: '<?php echo base_url();?>leads/leads_updatesource/'+id,
      type: 'post',
       data: data,
       beforeSend:function(){$(".LoadingImage").show();},
      success: function (data, status)
      {
         $('#leads_source_edit_'+id).modal('hide');
        
         
         $('.leads_source').html(data);
         $('.edit_source').html('');
         $('.edit_source').load("<?php echo base_url(); ?>leads/leads_source_editpopup");
         source_table= $("#alltask").DataTable({
               // "iDisplayLength": 10,
              // "scrollX": true,
           });
           /** for permission **/
            // access_permission_function();
             /** endof permisison **/
              $(".LoadingImage").hide();

      }
    });
   
   }
   
   });
   
   //$('.leads_source_delete').click(function(){
   $(document).on('click','.leads_source_delete',function(){
   var id=$(this).attr('data-id');
   //alert(id);
   var lead_source=$("#new_lead_source_"+id).val();
   //if (confirm('Are you sure want to delete')) {
       var data={};
   data['del']='delete';
   $.ajax({
      url: '<?php echo base_url();?>leads/leads_source_delete/'+id,
      type: 'post',
       data: data,
      success: function (data, status)
      {
         $('#leads_source_edit_'+id).modal('hide');
         $('.leads_source').html(data);
            source_table=$("#alltask").DataTable({
              //  "iDisplayLength": 10,
              // "scrollX": true,
           });
             /** for permission **/
             //access_permission_function();
             /** endof permisison **/
         $('.edit_source').html('');
         $('.edit_source').load("<?php echo base_url(); ?>leads/leads_source_editpopup");
       
      }
    });
   
   //}
   
   });
   /** end of leads source **/
   
   /** for leads status **/
   $('#leads_status_save').click(function(){
   var lead_status=$("#new_lead_status").val();
   if(lead_status=='')
   {
     $(".add_status_error").html('Please Enter Your Status');
   }
   else
   {
      $(".add_status_error").html('');
      $('#leads_status_add').modal('hide');
       var data={};
   data['new_team']=lead_status;
   $.ajax({
      url: '<?php echo base_url();?>leads/leads_add_new_status/',
      type: 'post',
       data: data,
       beforeSend:function(){$(".LoadingImage").show();},
      success: function (data, status)
      {
         

         $('.leads_status').html(data);
       //  $('.leads_source').load('leads_source_table');
        $('.edit_status').load("<?php echo base_url(); ?>leads/leads_status_editpopup ");

          status_table=$("#alltask_status").DataTable({
              //  "iDisplayLength": 10,
              // "scrollX": true,
           });
           /** for permission **/
            // access_permission_function();
             /** endof permisison **/
             $(".LoadingImage").hide();
      }
    });
   
   }
   
   });
   
   //$('.edit_source_save').click(function(){
   $(document).on('click','.edit_status_save',function(){
   var id=$(this).attr('data-id');
   //alert(id);
   var lead_status=$("#new_lead_status_"+id).val();
   if(lead_status=='')
   {
     $(".edit_status_error_"+id).html('Please Enter Your Status');
   }
   else
   {
      $(".edit_status_error_"+id).html('');
       var data={};
   data['new_team']=lead_status;
   $.ajax({
      url: '<?php echo base_url();?>leads/leads_updatestatus/'+id,
      type: 'post',
       data: data,
       beforeSend:function(){
        $(".LoadingImage").show();
       },
      success: function (data, status)
      {
         $('#leads_source_edit_'+id).modal('hide');
         $('.modal-backdrop.show').hide();
         $('.leads_status').html(data);
         $('.edit_status').html('');
         $('.edit_status').load("<?php echo base_url(); ?>leads/leads_status_editpopup");
           status_table=$("#alltask_status").DataTable({
              //  "iDisplayLength": 10,
              // "scrollX": true,
           });
              /** for permission **/
            // access_permission_function();
             /** endof permisison **/
             $(".LoadingImage").hide();
      }
    });
   
   }
   
   });
   
   //$('.leads_source_delete').click(function(){
   $(document).on('click','.leads_status_delete',function(){
   var id=$(this).attr('data-id');
   //alert(id);
   
   //if (confirm('Are you sure want to delete')) {
       var data={};
   data['del']='delete';
   $.ajax({
      url: '<?php echo base_url();?>leads/leads_status_delete/'+id,
      type: 'post',
       data: data,
      success: function (data, status)
      {
         $('#leads_source_edit_'+id).modal('hide');
         $('.leads_status').html(data);
         $('.edit_status').html('');
         $('.edit_status').load("<?php echo base_url(); ?>leads/leads_status_editpopup");
          status_table=$("#alltask_status").DataTable({
              //  "iDisplayLength": 10,
              // "scrollX": true,
           });
                       /**rspt for permission **/
                      // access_permission_function();
                       /** endof permisison **/
      }
    });
   
   //}
   
   });
   
   /** end leads status **/
   
   // $(document).ready(function(){
   // $("#reset_function").click(function(){
   
   // $("#leads_form").reset();
   // });});
   function clear_form_elements(ele) {
   
   $(ele).find(':input').each(function() {
   
    switch(this.type) {
        case 'password':
        case 'select-multiple':
        case 'select-one':
        case 'text':
        case 'textarea':
            $(this).val('');
            break;
        case 'checkbox':
        case 'radio':
            this.checked = false;
    }
   
   });
   
   $('.dropdown-chose-list').each(function(){
    // alert('zzz');
     $(this).html('<span>Nothing Selected</span>'); // this is correct but with validation
   
    });
   $(".selectDate").css('display','block');
   
   }
</script>
<!--- *********************************************** -->
 <!--   Datatable JS by Ram -->

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url()?>assets/js/user_page/materialize.js"></script> 

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>

<script src="<?php echo base_url()?>assets/js/custom/buttons.colVis.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom/datatable_extension.js"></script>
 <!--   End JS by Ram -->


<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>

<!--Tree Select -->

<script src="<?php echo base_url();?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/icontains.js"></script>

<script type="text/javascript">
         /** 29-08-2018 **/
      
         var tree_select = <?php echo json_encode( GetAssignees_SelectBox() ); ?>;        

         $('.tree_select').comboTree({ 
            source : tree_select,
            isMultiple: true
         });

      
          $('.add_tree .comboTreeItemTitle').click(function(){ 
            var id = $(this).attr('data-id');
            var id = id.split('_');

            $('.add_tree .comboTreeItemTitle').each(function(){
            var id1 = $(this).attr('data-id');
            var id1 = id1.split('_');
            //console.log(id[1]+"=="+id1[1]);
            if(id[1]==id1[1])
            {
               $(this).toggleClass('disabled');
            }
            });
            $(this).removeClass('disabled');
         });

</script>

<!--/ Tree Select -->

<script>

//$("#new_lead  select option").prop("selected", false);

   var Random = Mock.Random;
   var json1 = Mock.mock({
     "data|10-50": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled|1-2": true,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
   
   $('.dropdown-mul-1').dropdown({
     data: json1.data,
     limitCount: 40,
     multipleMode: 'label',
     choice: function () {
       // console.log(arguments,this);
     }
   });
   
   var json2 = Mock.mock({
     "data|10000-10000": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled": false,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
   
   $('.dropdown-mul-2').dropdown({
     limitCount: 5,
     searchable: false
   });
   
   $('.dropdown-sin-1').dropdown({
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   
   $('.dropdown-sin-2').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
       $('.dropdown-sin-3').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   $('.dropdown-sin-4').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   $('.dropdown-sin-22').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });

      $('.dropdown-sin-11').dropdown({
     readOnly: true,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   
   $('.dropdown-sin-12').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
       $('.dropdown-sin-13, .dropdown-sin-14').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });

     $('.dropdown-sin-32').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
    $('.dropdown-sin-33').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
    $(".review_manager_select_div").dropdown({
      limitCount:5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
      });
    $(".source_select_div").dropdown({
      limitCount:5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
      });
</script>
<!-- **************************************************-->
<script type="text/javascript">
//$(document).ready(function () {
     // $(".save_assign_staff").click(function(){
   // $(document).on('click','.save_assign_staff',function(e){
   //  alert('zzz');
   // });
   $(document).on('click','.save_assign_staff',function(e){
      var id = $(this).attr("data-id");
      var data = {};

      // var countries =$("#workers"+id ).val();
      /* alert($("#workers"+id ).val());
        $.each($(".workers option:selected"), function(){            
            countries.push($(this).val());
        });*/





      data['lead_id'] = id;
      data['assign_role']=$('#adduser_txt_'+id).val();
      // data['worker'] = countries;
      $(".LoadingImage").show();
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>Leads/update_assignees/",
               data: data,
               success: function(response) {
                $(".LoadingImage").hide();
                $('.dashboard_success_message_staff').show();
                 setTimeout(function(){ $('.dashboard_success_message_staff').hide(); }, 2000);

                  // alert(response); die();
               $('#task_'+id).html(response);

               },
            });
   });
    // $(".save_assign_source").click(function(){
      $(document).on('click','.save_assign_source',function(){
      var id = $(this).attr("data-id");
      var data = {};
       var countries =$("#source_new"+id ).val();
      /* alert($("#workers"+id ).val());
        $.each($(".workers option:selected"), function(){            
            countries.push($(this).val());
        });*/
      data['lead_id'] = id;
      data['source'] = countries;
       $(".LoadingImage").show();
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>Leads/update_assignees_source/",
               data: data,
               success: function(response) {
                 $(".LoadingImage").hide();
                  // alert(response); die();
                $('.dashboard_success_message_source').show();
                 setTimeout(function(){ $('.dashboard_success_message_source').hide(); }, 2000);
               $('#source_'+id).html(response);
               },
            });
   });

    // $('.change_lead_status').change(function(){
      $(document).on('change','.change_lead_status',function(){
 $(".LoadingImage").show();
      var id = $(this).attr("data-id");

         var stat = $(this).find(':selected').attr('data-statusname');
       var that = $(this);
        if(stat=='')return;
      var data = {};
       var countries =$(this).val();
          data['lead_id'] = id;
      data['status'] = countries;
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>Leads/update_assignees_status/",
               data: data,
               success: function(response) {
                 $(".LoadingImage").hide();
                $('.dashboard_success_message').show();
                 setTimeout(function(){ $('.dashboard_success_message').hide(); }, 2000);
                 $('.leads_status_count').load("<?php echo base_url(); ?>leads/leads_status_count");
                 var leads_status_us=$('#leads_status_us').val();
               //  alert('leads_status_us');
                 if(leads_status_us!='')
                 {
                      var data={};
                       data['filter_val'] = leads_status_us;
                       $.ajax({
                          url: '<?php echo base_url();?>leads/filter_status/',
                          type : 'POST',
                          data : data,
                          success: function(data) {
                       
                          //  $(".LoadingImage").hide();
                             /** for active status **/
                              $(".status_filter_new").each(function() {
                                var id=$(this).attr('id');
                                if(id==leads_status_us)
                                {
                                  $(this).addClass('intro');
                                }

                              });
                            /** end of active status **/
                       $(".sortrec").html(data);   
                       // $("#all_leads").dataTable({
                       // "iDisplayLength": 10,
                       // });
                       /*13.06.2018*/
                       
      initialize_datatable();
                       
                   




                       /** for permission **/
                      // access_permission_function();
                       /** endof permisison **/
                         
                       /*13.06.2018 */
                          },
                          });

                 }
                 /** for kanban ajax update **/

                   var sortby_val = '';
                   var data={};
                   data['sortby_val'] = sortby_val;
                   $.ajax({
                   url: '<?php echo base_url();?>leads/sort_by_kanban/',
                   type : 'POST',
                   data : data,
                   success: function(data) {
                    //  $(".LoadingImage").hide();
                 
                   $(".sortby_kanban").html(data);   
                 
                   },
                   });
                 /** end of kanban update **/
                 // location.reload();
                 
               },
            });
            //    alert(stat);
           var cell = that.closest('td');
                cell.attr('data-search',stat);
                
                table.cell( cell ).invalidate().draw();
    });
//});
</script>
<script type="text/javascript">





  $(document).ready(function () {
   
    $(function () {
     // alert('zz');
        $(".draglist").sortable({
            connectWith: ".draglist",
            cursor: "move"
        }).disableSelection();
        
        $(".draglist").sortable({
                   update: function( event, ui ) {
                  // alert(ui.item[0].id);
                   leads_id=ui.item[0].id;
                 var id = $(this).attr("id");
                 var array = id.split('_');
                 var lead_status_id=array[1];
               //  alert(lead_status_id);
                 var data = {};
               //  var lead_id =leads_id;
                    data['lead_id'] = leads_id;
                data['status'] = lead_status_id;
                $(".LoadingImage").show();
                    $.ajax({
                       type: "POST",
                       url: "<?php echo base_url();?>Leads/update_assignees_status/",
                       data: data,
                       success: function(response) {
                        $(".LoadingImage").hide();
                        // $('.status_msg').show();
                        // setTimeout(function(){ $('.status_msg').hide(); }, 2000);
                          $('.dashboard_success_message').show();
                 setTimeout(function(){ $('.dashboard_success_message').hide(); }, 2000);


                        $('.leads_status_count').load("<?php echo base_url(); ?>leads/leads_status_count");
                    $(".sortrec").html(response);   
                       // table=$("#all_leads").dataTable({
                       // "iDisplayLength": 10,
                       // });
                       /** for permission **/
                      // access_permission_function();
                       /** endof permisison **/ 

                         // location.reload();
                       //  alert('zz');
                       
                       },
                    });
             
                }
        });

    });



 $(".archive_source").click(function(){
    var id=this.id;
            $.ajax({
                    url: 'http://remindoo.uk/leads/archive_source',
                    type : 'POST',
                    data : { 'id':id},                    
                      beforeSend: function() {
                        $(".LoadingImage").show();
                      },
                      success: function(data) {
                        //alert(data);
                        if(data==1){
                         $("#my_Modal_"+id).hide();
                         $(".LoadingImage").hide();
                         location.reload();
                        }
                      }

                    });
          });

});
</script>
<script type="text/javascript">













 function confirm(id)
{
$('#delete_user'+id).show();
return false;
}
 function confirm_source(id)
{
$('#delete_particular_source'+id).show();
return false;
}

function confirm_status(id)
{
  $('#delete_particular_status'+id).show();
return false;
}



$(document).on('click','#close',function(e)
   {
   $('.alert-success').hide();
   return false;
   });  

$(document).ready(function() {
 //$('.leads_checkbox').on('click', function() {




 


 /* 30.05.2018*/

 //$('#assign_member_button').on('click', function() {
   $(document).on('click','#assign_member_button', function() {
   // alert('zzz');
  var leads = [];
  $(".leads_checkbox:checked").each(function() {
    leads.push($(this).data('leads-id'));
   // alert($(this).data('leads-id'));
  });
  if(leads.length <=0) {
     $('.alert-danger-check').show();
  } else {
    // $('#delete_all_leads'+leads).show();
    // $('.delete_yes').click(function() {
       var selected_leads_values = leads.join(",");

       $('#task_assign').modal({show: true, backdrop: 'static', keyboard: false});
       $(".assigned_staff").click(function(){
        $("#task_assign").modal("hide");
       var assign_to= $(".workers_assign").val();

       var formData={'task_id':selected_leads_values,'staff_id':assign_to};



         $.ajax({
        type: "POST",
        url: "<?php echo base_url().'Leads/alltasks_assign';?>",
        cache: false,
        data: formData,
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          $(".LoadingImage").hide();
          var json = JSON.parse(data); 
          status=json['status'];
          $(".staff-added").show();
          if(status=='1'){
           setTimeout(function(){ $(".staff-added").hide(); location.reload(); }, 3000); 
          }           
        }
      });

  });
     }
});


 /* 30.05.2018 */






 //$('#delete_source_button').on('click', function() {
 


 //$('#delete_status').on('click', function() {
   





});
  $( document ).ready(function() {
   $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy',minDate:0, changeMonth: true,
        changeYear: true,  }).val();
  //$(document).on('change','.othercus',function(e){
  });

 $( document ).ready(function() {


  

  <?php 
if(isset($_GET)){  $status=$_GET['status']; ?>
  $('.<?php echo $status; ?>').trigger('click');
<?php 
  } ?>



 /** 14-08-2018 **/
  $('.country_code').on('change input', function() {
  $(this).val($(this).val().replace(/([^+0-9]+)/gi, ''));
});
 $('.telephone_number').on('change input', function() {
  $(this).val($(this).val().replace(/([^0-9]+)/gi, ''));
});
 /** end of 14-08-2018 **/



});






$(document).on('change','#select_all_leads, #select_all_source, #select_all_status',function(){
            //alert("select All");
            var id=$(this).attr("id").trim();
            var table_obj;
            var checkbox_cls;

            if(id=="select_all_leads")
              { 
            //     alert("inside - leads all");
              // alert(typeof(table_obj));
                table_obj=table;
                checkbox_cls="leads_checkbox";
              }
              else if(id=="select_all_source")
                {
                  table_obj=source_table;
                  checkbox_cls="source_checkbox";
                }
                else if(id=="select_all_status")
                  {
                    table_obj=status_table;
                    checkbox_cls="status_checkbox";
                  }

                var all_checked=$(this).is(":checked");
                
                $(".LoadingImage").show();
                //alert(typeof(table_obj));
                table_obj.column(0).nodes().to$().each(function(index) {
                  if(all_checked)
                  { 

                    $(this).find("."+checkbox_cls).prop("checked",true);
                    toggle_action_buttons(1,checkbox_cls);
                  }
                  else
                  {
                    $(this).find("."+checkbox_cls).prop("checked",false);
                    toggle_action_buttons(0,checkbox_cls);
                  }
                });
                $(".LoadingImage").hide();

          });

           $(document).on('change','.leads_checkbox, .source_checkbox, .status_checkbox',function(){
           // alert("single checkbox");
            var table_obj;
            var checkbox_cls;
            if($(this).hasClass('leads_checkbox'))
            {
              table_obj=table;
              checkbox_cls="leads_checkbox";
            }  
            else if($(this).hasClass('source_checkbox'))
            {
              table_obj=source_table;
              checkbox_cls="source_checkbox"; 
            } 
            else if($(this).hasClass('status_checkbox'))
            {
              table_obj=status_table;
              checkbox_cls="status_checkbox";
            }
            ///alert(typeof(table_obj));
            var tot_count=0;
            var check_count=0;
            table_obj.column(0).nodes().to$().each(function(index) {

              if($(this).find("."+checkbox_cls).is(":checked"))
              {
                check_count++;
              }
            tot_count++; 
            });  

            var all_checkbox=checkbox_cls.replace("_checkbox","").trim();   
             //any one is checked
             if(check_count)
             {
              toggle_action_buttons(1,checkbox_cls);
             }
             else
             {
              toggle_action_buttons(0,checkbox_cls);

             }
             //for check select all checkbox
            if(tot_count==check_count)
            {
              $("#select_all_"+all_checkbox).prop("checked",true);
            } 
            else
            {
              $("#select_all_"+all_checkbox).prop("checked",false);
            }

           });






</script>
<script type="text/javascript">
$(".user_change").click(function(){


       var id = $(this).data('target');
       id=id.split('_');
       id=id[1];
       var main_id=id;
       var task_name='LEADS';
       var Assignees=[];


       // var manager_select = $(this).closest('form').find("select[name='manager[]']");
       // var workers_select = $(this).closest('form').find("select[name='worker[]']");
     // var new_div=$(this).closest('form').find('.new_task_div').attr('id');
      
    //  alert(new_div);

   // var new_div=$(this).closest('form').find('.new_task_div').attr('id');

      if(id!='')
      {

        $.ajax({
          url : "<?php echo base_url()?>leads/get_assigness",
          type : "POST",
          data : {"id":id,"task_name":task_name},
          dataType : "json",        
          beforeSend : function(){
            $(".LoadingImage").show();
          },
          success: function(data){
            $(".LoadingImage").hide();  

        
           
                // adduser_<?php echo $value['id'];?>


                      $('#adduser_'+main_id+' .comboTreeItemTitle').click(function(){ 

          Assignees=[];

            var id = $(this).attr('data-id');
            var id = id.split('_');

            $('#adduser_'+main_id+' .comboTreeItemTitle').each(function(){
            var id1 = $(this).attr('data-id');
            var id1 = id1.split('_');
            //console.log(id[1]+"=="+id1[1]);
            if(id[1]==id1[1])
            {
               $(this).toggleClass('disabled');
            }


            });
            $(this).removeClass('disabled');

              $('#adduser_'+main_id+' .comboTreeItemTitle input[type="checkbox"]:checked').each(function(){
            var id = $(this).closest('#adduser_'+main_id+' .comboTreeItemTitle').attr('data-id');
            var id = id.split('_');
            Assignees.push( id[0] );
             });

          var assign_role = Assignees.join(',');
       $('#adduser_txt_'+main_id).val(assign_role);

         });
                      


                        var arr1 = data.asssign_group;
                        var assign_check = data.assign_check; 
                        //alert(assign_check);
                 var unique = arr1.filter(function(itm, i, arr1) {
              return i == arr1.indexOf(itm);
          });
                 
       $('#adduser_'+main_id+' .comboTreeItemTitle').each(function(){
      $(this).find("input:checked").trigger('click');
               var id1 = $(this).attr('data-id');
                var id = id1.split('_');
             if(jQuery.inArray(id[0], unique) !== -1)
               {
                  $(this).find("input").trigger('click');
                  if(assign_check==0)
                  {
                     $(this).toggleClass('disabled');
                  }
                  Assignees.push( id[0] );
              }
                    
               });

 


   

         
         var assign_role = Assignees.join(',');
       $('#adduser_txt_'+main_id).val(assign_role);
         //alert(Assignees);
  
          }
      });
      
      }   
});


</script>


<!-- <script type="text/javascript">
    $(document).ready(function(){
      function leadmasonry(){
       $('.masonry-container').masonry({
          itemSelector: '.width-junk',
          percentPosition:true,
          columnWidth: '.grid-sizer',
          percentPosition: true
        });
       }

       $(document).on( 'click', '.nav-item a', function() {
        alert('hi');
         setTimeout(function(){ 
          leadmasonry();
           }, 600);
       });

    })
</script> -->
               <!-- 

               <script type="text/javascript">
    $(document).ready(function(){
    var $gridlead = $('.lead-summary3 .masonry-container').masonry({
      itemSelector: '   .new-row-data1',
      percentPosition: true,
      columnWidth: '.lead-summary3  .grid-sizer' 
    });
   
   setTimeout(function(){ $gridlead.masonry('layout'); }, 600);
});
  </script> -->