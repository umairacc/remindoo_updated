<?php $this->load->view('includes/header');?>
<div class="pcoded-content import-pcoded">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
              
                  <!--start-->

                <!--   <div class="import-container-1">
                    <div class="container">
                  <div class="page-header card">
                                        <div class="align-items-end">
                                                <div class="page-header-title">
                                                    <i class="icofont icofont-upload-alt bg-c-lite-green"></i>
                                                    <div class="d-inline">
                                                        <h4>File Upload</h4>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                     </div>
                                    </div>  -->

                  <div class="title_page02 fileupload_01 ">
                     <div class="container">
                        
                        <?php if(!empty($status)){
                           echo '<div class="alert alert-danger">'.$status.'</div>';
                           } ?>
                        <div class="panel panel-default">
						              
                          <div class="real-csvfile">
                          <div class="equql-important">
						                <h2>Import CSV File Data </h2>
                          </div>

                          <div class="csv-sample01">
                           <a href='<?= base_url() ?>User/downloadFile'>Download Sample CSV File</a> 
                         </div>
                       </div>
                           <div class="csv-sample-data">
                           <div class="panel-body">
                              <form action="" method="post" enctype="multipart/form-data" id="importFrm">
                                 <div class="upload_input09">
                                    <input type="file" name="file" id="file" accept=".csv"/>
                                 </div>
                                 <input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT">
                              </form>
                           </div>
                        </div>
					</div>
                     </div>
                  </div>
                  <!-- close -->
                  
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<!-- success popup-->

<!-- 
<div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Import clients</h4>
         </div>
         <div class="modal-body">
            
         </div>
      </div>
   </div>
</div>
-->

<div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<button type="button" class="close" data-dismiss="modal">×</button>
<div class="pop-realted1">
    <div class="position-alert1">
     <div class="form-group" id="messages"> </div>
	 </div>
    </div>
    </div>

	
<!-- ajax loader -->
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style>
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
   <h1>Warning!!</h1>
   <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
   <div class="iew-container">
      <ul class="iew-download">
         <li>
            <a href="http://www.google.com/chrome/">
               <img src="assets/images/browser/chrome.png" alt="Chrome">
               <div>Chrome</div>
            </a>
         </li>
         <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
               <img src="assets/images/browser/firefox.png" alt="Firefox">
               <div>Firefox</div>
            </a>
         </li>
         <li>
            <a href="http://www.opera.com">
               <img src="assets/images/browser/opera.png" alt="Opera">
               <div>Opera</div>
            </a>
         </li>
         <li>
            <a href="https://www.apple.com/safari/">
               <img src="assets/images/browser/safari.png" alt="Safari">
               <div>Safari</div>
            </a>
         </li>
         <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
               <img src="assets/images/browser/ie.png" alt="">
               <div>IE (9 & above)</div>
            </a>
         </li>
      </ul>
   </div>
   <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->




<!-- j-pro js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>

<!-- modernizr js -->


<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>


<script>
   $( document ).ready(function() {
   
       var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
       // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#1abc9c',
               jackColor: '#fff',
               size: 'small'
           });
       });
   
       $('#accordion_close').on('click', function(){
               $('#accordion').slideToggle(300);
               $(this).toggleClass('accordion_down');
       });
   });
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   $("#importFrm").validate({
     
          ignore: false,
   
                           rules: {
                          
                           file: {required: true, accept: "csv"},
                           
                           },
                           errorElement: "span" , 
                           errorClass: "field-error",                             
                            messages: {
                             
                             file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},
                            
                            },
                            
   
                           
                           submitHandler: function(form) {
                               var formData = new FormData($("#importFrm")[0]);
                               
   
                               $(".LoadingImage").show();
   
                               $.ajax({
                                   url: '<?php echo base_url();?>User/upload_file',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) {
                                     $(".LoadingImage").hide();
                                       //alert(data);
                                       if(data > 0){
                                       //if(data == 1{
                                          // $('#new_user')[0].reset();
                                            $('#messages').html('<p class="common-tag"><b>'+data+'</b> <p class="success-msg">Client has successfully imported.Please click on </p> <span class="view-btn"><a class="view-client" href="<?php echo base_url()."user"?>">View Clients</a></span></p>');
                                           $('#confirm-submit').modal('toggle');
   
                                       }
                                       else if(data == 0){
                                       //else if(data == 0 || data == 2){
                                          $('#messages').html('<p>Error Occured! </p>');
                                           $('#confirm-submit').modal('toggle');
                                       }
                                   $(".LoadingImage").hide();
                                   },
                                   error: function() { $('.alert-danger').show();
                                           $('.alert-success').hide();}
                               });
   
                               return false;
                           } ,
                            invalidHandler: function(e, validator) {
              if(validator.errorList.length)
           $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
   
           }
                            
                       });
   
   
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
     $("#country").change(function(){
     var country_id = $(this).val();
     //alert(country_id);
   
       $.ajax({
   
         url:"<?php echo base_url().'Client/state';?>",
         data:{"country_id":country_id},
         type:"POST",
         success:function(data){
           //alert('hi');
           $("#state").append(data);
           
         }
   
       });
     });
      $("#state").change(function(){
     var state_id = $(this).val();
     //alert(country_id);
   
       $.ajax({
   
         url:"<?php echo base_url().'Client/city';?>",
         data:{"state_id":state_id},
         type:"POST",
         success:function(data){
           //alert('hi');
           $("#city").append(data);
           
         }
   
       });
     });
   });
</script>
</body>
</html>