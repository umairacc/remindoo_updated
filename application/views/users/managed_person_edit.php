<?php $this->load->view('includes/header');?>
<style>
.outline {
  border-color: red;
}
.outline-off {
  border-color: gray;
}
  /*span.newonoff {
  background: #4680ff;
  padding: 6px 15px;
  display: inline-block;
  color: #fff;
  border-radius: 5px;
  }*/
  button.btn.btn-info.btn-lg.newonoff {
  padding: 3px 10px;
  height: initial;
  font-size: 15px;
  border-radius: 5px;
  }
  /* 18 Jan */

.header-navbar .navbar-wrapper .navbar-container .header-notification .show-notification {
    height: 300px;
    overflow: auto;
}

.all_user-section.floating_set.proposal-commontab a.nav-link.active {
    background: #999 !important;
    color: #fff !important;
}
.all_user-section.floating_set.proposal-commontab a.nav-link {
    background: transparent;
    color: #555 !important;
    font-size: 13px;
    text-transform: capitalize;
    border-right: 1px solid #ccc;
}
.all_user-section.floating_set.proposal-commontab li:last-child a.nav-link {
    border-right: none;
}
.proposal-commontab ul.nav.nav-tabs.all_user1.md-tabs.floating_set.u-dashboard
{
  border: 1px solid #ccc;
  padding: 0;
}
.create-proposal {
    float: right;
    margin-top: 10px;
    height: 24px;
}
a.create-proposalbtn {
    background: #4680ff;
    color: #fff;
    padding: 5px 10px 9px;
    height: auto;
    text-transform: capitalize;
    font-size: 13px;
    font-weight: 600;
}
.year-status {
    display: block;
    padding: 15px 0;
}
span.lt-year {
    display: inline-block;
    font-size: 19px;
    text-transform: capitalize;
    font-weight: 600;
}
ul.status-list {
    display: inline-block;
    padding-left: 15px;
}
ul.status-list li {
    display: inline-block;
    padding: 0 5px;
    font-size: 14px;
    text-transform: capitalize;
    position: relative;
}
ul.status-list li:after {
    position: absolute;
    width: 1px;
    content: "";
    background: #333;
    height: 15px;
    right: 0;
    top: 4px;
}
.inner-tab li.nav-item {
    display: inline-block;
    width: calc(100% / 6.10);
}
.inner-tab .nav-tabs .slide {
    background: transparent;
}
.inner-tab a.nav-link {
    font-size: 60px !important;
    font-weight: 600;
}
.inner-tab ul.nav.nav-tabs.md-tabs.floating_set {
    border: 1px solid #ccc;
}
.inner-tab {
    margin-top: 30px;
}

/* 19 Jan */

.inner-tab .slide {
    position: unset;
}
.inner-tab .slide {
    font-size: 21px;
    position: unset;
    height: auto;
    width: auto;
}
span.value-text {
    font-size: 14px;
    text-transform: capitalize !important;
    display: inline-block;
    vertical-align: text-top;
    background: #FFB424;
    padding: 3px 8px;
    border-radius: 4px;
    color: #fff;
}
span.value-text.v1 {
    background: #00B2F2;
}
span.value-text.v2 {
    background: #F20094;
}
span.value-text.v3 {
    background: #A9CA42;
}
span.value-text.v4 {
    background: #F01C22;
}
span.value-text.v5 {
    background: #BD44EF;
}
ul.status-list li:last-child:after {
    display: none;
}
.table-responsive.proposal-info table {
    width: 100%;
}
span.info-num {
    background: #B9CFDC;
    color: #fff;
    font-size: 15px;
    text-transform: uppercase;
    padding: 2px 7px;
    border-radius: 6px;
    display: inline-block;
}
.info-num + p {
    display: inline;
    color: #437CA6;
    text-transform: capitalize;
    font-size: 18px;
    font-weight: 600;
}
a.edit-option {
    color: #A09F9E;
    padding: 0 5px 0 0px;
    cursor: pointer;
}
p.com-name {
    margin: 0;
    padding-top: 10px;
    font-size: 15px;
    text-transform: uppercase;

}
p.com-name a {
    border-bottom: 1px solid;
    display: inline-block;
    font-size: 17px;
    margin-right: 10px;
}
.table-header th {
    color: #939CA7;
    font-weight: normal;
    text-transform: capitalize;
    font-size: 14px;
    padding: 10px;
}
.table-responsive.proposal-info {
    margin-top: 10px;
}
span.tax-inner {
    font-size: 15px;
    text-transform: uppercase;
    text-align: center;
}
.proposal-info td {
    padding: 10px;
}
span.amount i.fa.fa-gbp {
    padding-right: 5px;
}
.acc-tax p {
    font-size: 14px;
    display: block;
    text-transform: capitalize;
}
span.sent-month {
    font-size: 14px;
}
td i.fa.fa-check {
    color: #2DB538;
    font-size: 30px;
    display: block;
    padding-left: 10px;
}
span.accept {
    color: #868585;
    font-size: 13px;
}
.proposal-info tr {
    border-bottom: 10px solid transparent;
}
tr.table-header
{
  border-bottom: none;
}
span.amount {
    font-size: 15px;
    font-weight: 600;
}

/* responsive */

@media(max-width:1199px){

  .inner-tab a.nav-link {
    font-size: 45px !important;
    font-weight: 600;
  }
  span.value-text
  {
    font-size: 13px;
  }
  .proposal-info td, .proposal-info th {
    padding: 10px;
    white-space: normal;
  }

}
@media(max-width:1023px){

  .inner-tab li.nav-item {
    display: inline-block;
    width: initial;
    margin: 0 !important;
    padding: 0;
  }
  .inner-tab .slide {
    font-size: 15px;
    position: unset;
    height: auto;
    width: auto;
    padding: 5px 0;
  }
  .info-num + p
  {
    font-size: 16px;
  }
  p.com-name a
  {
    font-size: 15px;
  }
  span.amount {
    font-size: 14px;
    font-weight: 600;
  }
  span.tax-inner
  {
    font-size: 14px;
  }
  .acc-tax p, span.sent-month
  {
    font-size: 13px;
  }
@media(max-width:767px){
    .proposal-commontab ul.nav.nav-tabs.all_user1.md-tabs.floating_set.u-dashboard
    {
      border: none;
    }
    .all_user-section.floating_set.proposal-commontab a.nav-link
    {
      border: 1px solid #ccc;
    }
    .all_user-section.floating_set.proposal-commontab li:last-child a.nav-link {
      border-right: 1px solid #ccc;
    }
    .create-proposal
    {
      margin-top: 15px;
    }
    .inner-tab ul.nav.nav-tabs.md-tabs.floating_set {
      border: none;
    }
    .proposal-info td, .proposal-info th {
    padding: 10px;
    white-space: nowrap;
    }
  }
</style>

<div class="pcoded-content">
  <div class="pcoded-inner-content">
    <!-- Main-body start -->
    <div class="main-body">
      <div class="page-wrapper">
        <!-- Page body start -->
        <div class="page-body">
          <div class="row">
            <div class="col-sm-12">
              <!-- Register your self card start -->
              <div class="card">
                <!-- admin start-->
                <div class="client_section col-xs-12 floating_set">
                  
                    
                    
                    </div> <!-- all-clients -->
                  <div class="all_user-section floating_set proposal-commontab">
                    
                    <div class="create-proposal">
                      <a href="<?php echo base_url();?>user/managed_person_list/" class="create-proposalbtn" >Persons List</a>
                    </div>
                   
                    <div class="all_user-section2 floating_set">
                      <div class="tab-content">
                        <div id="allusers" class="tab-pane fade in active">
                        
    <form method="post" action="<?php echo base_url('User/managed_update/');?>">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="form-group">
                    <label class="col-md-3">Managed Person</label>
                    <div class="col-md-9">
                        <input type="text" name="person_name" class="form-control" value="<?php echo $product['name']; ?>" required>
                        <input type="hidden" name="person_id" class="form-control" value="<?php echo $product['id']; ?>" required>

                    </div>
                </div>
            </div>
            
            <div class="col-md-8 col-md-offset-2 pull-right">
                <input type="submit" name="Save" class="btn">
            </div>
        </div>
        
    </form>

                      </div>
                    </div>
                    <!-- admin close -->


                  </div>
                  <!-- Register your self card end -->
                </div>
              </div>
            </div>
            <!-- Page body end -->
          </div>
        </div>
        <!-- Main-body end -->
        <div id="styleSelector">
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>


<?php $this->load->view('users/column_setting_view');?>
<?php $this->load->view('includes/footer');?>

<script >
  
 $(function () {
  $("#error").hide();

  $("#btnSubmit").click(function () {
    var textVal = $("#firstname").val();
    if(textVal == '') {
      $("#firstname").addClass("outline");
    } 
    $("#firstname").focus();
    $("#error").show();
  });
});
</script>
 


