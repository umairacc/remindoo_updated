<!DOCTYPE html>
<html>
<head>
  <style>
  
         #pdf {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#pdf td, #pdf th {
    border: 1px solid #ddd;
    padding: 8px;
}

#pdf tr:nth-child(even){background-color: #f2f2f2;}

#pdf td
{
  font-size: 13px;
}

#pdf th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #9E9E9E;
    color: white;
    font-size: 14px !important;
    white-space: nowrap;
}

p{
  color: #D7CCC8;
}

caption{
  color: #555;
  font-size: 22px;
  margin-bottom: 20px;
}

  </style>
</head>
<body>
   
<table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%" cellpadding="10" style="border-collapse: collapse; margin-top:50px;">
                                <thead>
                                  <tr class="text-uppercase">
                                   <tr class="text-uppercase">
                                                      <th style="text-align:left;border: 1px solid #ddd;">
                                                       S.no
                                                      </th>
                                                      <th style="text-align:left;border: 1px solid #ddd;">Timer</th>
                                                      <th style="text-align:left;border: 1px solid #ddd;">Task Name</th>
                                                      <th style="text-align:left;border: 1px solid #ddd;">Start Date</th>
                                                      <th style="text-align:left;border: 1px solid #ddd;">Due Date</th>
                                                      <th style="text-align:left;border: 1px solid #ddd;">Status</th>
                                                      <th style="text-align:left;border: 1px solid #ddd;">Priority</th>
                                                      <th style="text-align:left;border: 1px solid #ddd;">Tag</th>
                                                 <th style="text-align:left;border: 1px solid #ddd;">Assignee</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php 

                                                   $i =1;
                                                   foreach ($task_list as $key => $value) {

                                                             $ex_val= $this->Common_mdl->get_module_user_data('TASK',$value['id']);
                                
                                                    $var_array=array();
                                                     if(count($ex_val)>0)
                                                             {
                                                              foreach ($ex_val as $key => $value1) {
                                                            
                                                               $user_name = $this->Common_mdl->select_record('user','id',$value1);
                                                                array_push($var_array, $user_name['crm_name']);
                                                               }
                                                              
                                                             }


                                                  $Assignee= implode(',',$var_array);


                                                    if($value['related_to']=='tasks'){
                                                      error_reporting(0);
                                                 
           

                                                      if($value['worker']=='')
                                                      {
                                                      $value['worker'] = 0;
                                                      }
                                                      $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
                                                   
                                                       $status_val1='';
                                                    $status_val1=$this->Common_mdl->select_record('task_status','id',$value['task_status']);
                                                    $stat=$status_val1['status_name'];

                                                      $exp_tag = explode(',', $value['tag']);
                                                      $explode_worker=explode(',',$value['worker']);
                                                      ?>
                                                   <tr id="<?php echo $value['id']; ?>">
                                                      <td style="text-align:left;border: 1px solid #ddd;">
                                                        <?php //echo $i;?>
                                                      </td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo date('Y-m-d H:i:s', $value['created_date']);?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo ucfirst($value['subject']);?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo $value['start_date'];?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo $value['end_date'];?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo $stat;?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo $value['priority'];?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"> <?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                                                            echo $tagName = $this->Common_mdl->getTag($exp_tag_value).' ';
                                                            }?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;">
                                                        <?php echo $Assignee;?>
                                                         <!-- <?php
                                                            foreach($explode_worker as $key => $val){
                                                              $query = $this->db->query('select * from user where id='.$val.'')->row_array();
                                                           echo $query['crm_name'].' ';
                                                            ?>
                                                         
                                                         <?php } ?> --></td>
                                                       <!--   <td class="user_imgs">
                                                      <?php
                                                         foreach($staff as $key => $val){     
                                                         ?>
                                                      <img src="<?php echo base_url().'uploads/'.$val['profile'];?>" alt="img" height='50' width='50'>
                                                      <?php } ?>
                                                      <a href="javascript:;" data-toggle="modal" data-target="#adduser" class="adduser1"><i class="fa fa-plus"></i></a>
                                                   </td> -->
                                                     
                                                     
                                                   </tr>
                                                   <?php 
                                                   if($value['sub_task_id']!=''){
                                                    $sub_task=explode(',',$value['sub_task_id']);
                                                      for($i=0;$i<count($sub_task);$i++){
                                                          $sub_task_details=$this->Common_mdl->select_record('add_new_task','id',$sub_task[$i]);

                                                          

                                                            if($sub_task_details['worker']=='')
                                                      {
                                                      $sub_task_details['worker'] = 0;
                                                      }
                                                      $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$sub_task_details['worker'].")")->result_array();

                                                      $status_val1='';
                                                    $status_val1=$this->Common_mdl->select_record('task_status','id',$value['task_status']);
                                                    $stat=$status_val1['status_name'];
                                                    
                                                      $exp_tag = explode(',', $sub_task_details['tag']);
                                                      $explode_worker=explode(',',$sub_task_details['worker']);
                                                      ?>
                                                   <tr id="<?php echo $sub_task_details['id']; ?>">
                                                      <td style="text-align:left;border: 1px solid #ddd;">
                                                        -
                                                      </td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo date('Y-m-d H:i:s', $sub_task_details['created_date']);?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo ucfirst($sub_task_details['subject']);?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo $sub_task_details['start_date'];?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo $sub_task_details['end_date'];?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo $stat;?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo $sub_task_details['priority'];?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"> <?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                                                            echo $tagName = $this->Common_mdl->getTag($exp_tag_value).' ';
                                                            }?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><!--  <?php
                                                            foreach($explode_worker as $key => $val){
                                                              $query = $this->db->query('select * from user where id='.$val.'')->row_array();
                                                           echo $query['crm_name'].' ';
                                                            ?>
                                                         
                                                         <?php } ?> -->
                                                         <?php echo $Assignee;?>
                                                         </td>                                             
                                                     
                                                     
                                                   </tr>
                                                    <?php   }


                                                   }

                                                   ?>                                                   
                                                   <?php 
                                                      $i++;}} ?>
                                                   <!-- <tr>
                                                      <td></td>
                                                   </tr> -->
                                                </tbody>
                                             </table>
</body>
</html>