<?php

   $role = $this->Common_mdl->getRole($_SESSION['id']);
   
   function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
   }

   function calculate_test($a,$b){
$difference = $b-$a;

$second = 1;
$minute = 60*$second;
$hour   = 60*$minute;

$ans["hour"]   = floor(($difference)/$hour);
$ans["minute"] = floor((($difference)%$hour)/$minute);
$ans["second"] = floor(((($difference)%$hour)%$minute)/$second);
//echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

$test=$ans["hour"].":".$ans["minute"].":".$ans["second"];
return $test;
}
function time_to_sec($time) {
list($h, $m, $s) = explode (":", $time);
$seconds = 0;
$seconds += (intval($h) * 3600);
$seconds += (intval($m) * 60);
$seconds += (intval($s));
return $seconds;
}
function sec_to_time($sec) {
return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}   
$t_re='';
$task_name='';
foreach ($today_details as $today) {
  $task_name=$today['subject'];
$res=explode(',',$today['time_start_pause']);
    //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
    $res1=array_chunk($res,2);
    $result_value=array();
    foreach($res1 as $rre_key => $rre_value)
    {
       $abc=$rre_value;
       if(count($abc)>1){
       if($abc[1]!='')
       {
          $ret_val=calculate_test($abc[0],$abc[1]);
          array_push($result_value, $ret_val) ;
       }
       else
       {
        $pause='';
          $ret_val=calculate_test($abc[0],time());
           array_push($result_value, $ret_val) ;
       }
      }
      else
      {
        $pause='';
          $ret_val=calculate_test($abc[0],time());
           array_push($result_value, $ret_val) ;
      }

    }
    $time_tot=0;
     foreach ($result_value as $re_key => $re_value) {
        $time_tot+=time_to_sec($re_value) ;
     }
     $hr_min_sec=sec_to_time($time_tot);
     $hr_explode=explode(':',$hr_min_sec);
     $h=$hr_explode[0];
     $m="0.".$hr_explode[1];
     $hours=$h+$m;
    // $t="['".$today['crm_name']."',".$hours.",'Stffs Name:".$today['crm_name'].",Working Hours:".$hours."']";
     $t="['".$today['crm_name']."',".$hours."]";
     $t_re.=empty($t_re)?$t:",".$t;
}
?>

<?php $this->load->view('includes/header');?>

<style>
   /* bootstrap hack: fix content width inside hidden tabs */
.tab-content > .tab-pane,
.pill-content > .pill-pane {
display: block;     /* undo display:none          */
height: 0;          /* height:0 is also invisible */ 
overflow-y: hidden;
overflow-x: hidden; /* no-overflow                */
}
.tab-content > .active,
.pill-content > .active {
height: auto;       /* let the content decide it  */
} /* bootstrap hack end */
div#Tabs_priority {
    padding-top: 15px;
}
#chartdiv {
  width: 100%;
  height: 500px;
  font-size: 11px;
}

.amcharts-pie-slice {
  transform: scale(1);
  transform-origin: 50% 50%;
  transition-duration: 0.3s;
  transition: all .3s ease-out;
  -webkit-transition: all .3s ease-out;
  -moz-transition: all .3s ease-out;
  -o-transition: all .3s ease-out;
  cursor: pointer;
  box-shadow: 0 0 30px 0 #000;
}

.amcharts-pie-slice:hover {
  transform: scale(1.1);
  filter: url(#shadow);
}
div#legenddiv
{
      height: 50px !important;
    width: 100% !important;
    position: absolute;
    top: 70% !important;
    opacity: 1;
    visibility: visible;
    white-space: nowrap;
}
</style>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/mintScrollbar.css">
<link href="<?php echo base_url();?>assets/css/timeline-style.css" type="text/css" rel="stylesheet" />
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              
                              <div id="append_content">
                                 <div class="all_user-section2 floating_set fr-reports">
                                       <div class="tab-pane fade active show" id="task-reports" aria-expanded="true">
                                          <div class="card-block-small">
                                             <div class="left-sireport col-xs-12 line-chart123">
                                              
                                             <h6>
                                                         <?php echo $task_name; ?>
                                                         
                                                      </h6>
                                                <div class="card-block fileters_client new123">
                                             <div id="monthly_task" style="width: 1200px; height: 500px"></div>
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                       
                                       
                                    </div> <!--tab content close -->
                                    <!-- home-->
                                    <!-- frozen -->
                                 </div>
                                 <!-- ul after div -->
                              </div>
                           </div>
                           <!-- admin close -->
                        </div>
                        <!-- Register your self card end -->
                     </div>
                  </div>
               </div>
               <!-- Page body end -->
            </div>
         </div>
         <!-- Main-body end -->
         <div id="styleSelector">
         </div>
      </div>
   </div>
</div>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<?php $this->load->view('reports/reports_scripts');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.scrollbox.min.js"></script>-->
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.min.js"></script>


<script type="text/javascript">
$( document ).ready(function() {
    $(".prodiv12").niceScroll({

 zindex: "auto",
  cursoropacitymin: 1,
  cursoropacitymax: 1,
  cursorcolor: "#ccc",
  cursorwidth: "6px",
  cursorborder: "1px solid #fff",
  cursorborderradius: "5px",
  scrollspeed: 60,
  mousescrollstep: 8 * 3,
  // options here

});
});
</script>
<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var dataTable = new google.visualization.DataTable();
       dataTable.addColumn('string', 'Staff Name');
       dataTable.addColumn('number', 'Working Hour ');
        // A column for custom tooltip content
        //dataTable.addColumn({type: 'string', role: 'tooltip'});
        dataTable.addRows([<?php echo  $t_re;?>]);
         var options = {
        title: "Working Hours Time Sheet.",
       width: 1000,
        height: 500,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
        var chart = new google.visualization.ColumnChart(document.getElementById('monthly_task'));
        chart.draw(dataTable, options);
      }
</script>

<script type="text/javascript">
   $(document).ready(function () {
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var ev = document.createEvent('Event');
    ev.initEvent('resize', true, true);
    window.dispatchEvent(ev);
  });
   });
</script>