<?php $this->load->view('includes/login_header');
?>
   <body>
   <div class="login_user">
      <div class="j-wrapper j-wrapper-400">
      <div class="login_logo text-center"><img class="img-fluid" src="<?php echo base_url();?>assets/images/logo.png" alt="Theme-Logo"></div>
         <form action="http://remindoo.org/CRMTool/login/login_chk" method="post" class="j-pro" id="login_form" novalidate="novalidate">
            <!-- end /.header -->
            <center></center>
            <div class="j-content">
               <!-- start password -->
               <div class="j-unit">
                  <div class="j-input">
                     <label class="j-icon-right" for="password">
                     <i class="icofont icofont-lock"></i>
                     </label>
                     <input type="password" id="password" name="password" placeholder="password">
                  </div>
               </div>
               <div class="j-response"></div>
               <!-- end response from server -->
            </div>
            <!-- end /.content -->
            <div class="j-footer">
               <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <!-- end /.footer -->
         </form>
      </div>
     </div>
     <?php $this->load->view('includes/login_footer');?>
<!-- <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>
  $(document).ready(function(){ 
      $("#login_form").validate({
            // Rules for form validation
            rules : {
                "username" : {
                    required : true
                },                        
                "password" : {
                    required : true
                },
                  
            },
          
            messages : {
                "username" : {
                    required : 'Username Required'                           
                },    
                "password" : {
                    required : 'Password Required'                            
                },                        
                        
            },

        });

      $("#forgotpwd_form").validate({
      rules : {
                "email" : {
                    required : true,
                    email : true
                }, 
            },
            messages : {
                "email" : {
                    required : 'Email Id Required'                           
                },      
            },
      });
      $("#resetPwd").validate({
        rules : {
                "newpwd" : {
                    required : true,
                }, 
                "confirmpwd" : {
                    required : true,
                    equalTo: "#newpwd"
                }, 
            },
            messages : {
                "newpwd" : {
                    required : 'New Password Required'                           
                }, 
                "confirmpwd" : {
                    required : 'Confirm Password Required'                           
                },      
            },
      });
  });
  </script>
