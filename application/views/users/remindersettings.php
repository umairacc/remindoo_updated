<?php 

if($_SESSION['firm_id'] == '0')
{
   $this->load->view('super_admin/superAdmin_header');
}
else
{
   $this->load->view('includes/header');
}

?>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
<style type="text/css">
  .sp-replacer {
  width: 275px;
  border-radius: 50px;
  background: #fff;
 }
 .sp-preview{
  width: 230px;
  height: 23px;    
 }
 
 .sp-preview, .sp-alpha, .sp-thumb-el {
  background-image: none;
  margin: 2px 0px 2px 10px;  
  }

  .sp-dd{
    margin-left: 5px;
  }

</style>

<?php 

      $service = array();

      $service_list = $this->Common_mdl->getServicelist();       

      if(!empty($this->uri->segment(3)))
      {
         foreach ($service_list as $key => $value) 
         {
            if($value['id'] == $this->uri->segment(3))
            {
              $service[$value['id']] = $value['service_name'];
            }
         }
      }
      else
      {
         foreach ($service_list as $key => $value) 
         {
            $service[$value['id']] = $value['service_name'];
         }
      }

      $reminders = array();

      foreach($service as $key => $value) 
      { 
         $reminders[$key][$value] = array();                                
      } 
    
      if(count($reminder_setting)>0)
      { 
         foreach ($reminder_setting as $key1 => $value1) 
         {    
             foreach($service as $key => $value)
             {
                 if($value1['service_id'] == $key)
                 {
                    $reminders[$key][$value][] = $value1;
                 }
             }  
         }
      }   
?>

<div class="modal-alertsuccess alert succ popup_info_msg" style="<?php if($_SESSION['msg']!=""){ echo "display: block"; }else { echo "display: none"; } ?>">
   <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
   <div class="pop-realted1">
   <div class="position-alert1"> 
   <?php if($_SESSION['msg']!=""){ echo $_SESSION['msg']; } ?>      
   </div>
   </div>
   </div>
</div>


<div class="pcoded-content tasktimetop serviceremindercls">
  <div class="pcoded-inner-content">

            <div class="card propose-template" style="display: block;">
            <!-- admin start-->
            <div class="client_section col-xs-12 floating_set">
             
              </div> <!-- all-clients -->
              <div class="client_section user-dashboard-section1 floating_set">
                <div class="all_user-section floating_set proposal-commontab tsk-timeframe">
				
               <div class="deadline-crm1 floating_set">  
                  <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard client-top-nav">
                    <li class="nav-item active">
                      <a class="nav-link"  href="javascript:void(0)">Reminder Settings</a>
                      <div class="slide"></div>
                    </li>
                  </ul>
               </div>  
              </div>

              <?php foreach($reminders as $key => $value) {

                       foreach($value as $key1 => $values) {
               ?>
                 
                <div class="reminder_sett">
                  <div class="card-header col-md-12" style="margin-top: 0px;">
                    <div class="reminder_content remind-cis">                      
                      <div class="deadline-crm1 floating_set">
                         <div class="toggle-adds1">
                             <h3><?php echo $key1; ?></h3>
                             <?php if($_SESSION['permission']['Reminder_Settings']['create'] == '1') { ?>
                             <span class="due-date f-right"><a href="javascript:;" class="btn-add-task add_reminder" data-service="<?php echo $key; ?>">
                              <i class="fa fa-plus"></i> add reminder</a></span>
                              <?php } ?> <!-- data-toggle="modal" data-target="#add-reminder" -->
                         </div>
                      </div>
                       
                      <?php if(count($values)>0) { ?>

                      <div class="reminder-setting floating_set">  
                        <div class="email-sec floating_set">
                          <?php 

                          foreach($values as $value1) {                           
                            
                          if($value1['due']=='due_by'){
                          $due='Before';
                          }elseif($value1['due']=='overdue_by' ){
                          $due='After';
                          }

                          ?>
                          <div class="email-innersec"> 
                            <div class="email-sec1">                  
                              <span class="due-date"><?php echo $due.' '.$value1['days'].' '?>day(s)</span>
                                <?php if($_SESSION['permission']['Reminder_Settings']['edit'] == '1' || $_SESSION['permission']['Reminder_Settings']['delete'] == '1'){ ?>
                                  <a href="javascript:;" class="date-editop reminder_popup" data-due="<?php if($value1['due']=='due_by'){  echo 'due in'; }elseif($value1['due']=='overdue_by' ){   echo 'overdue'; } ?>" data-service="<?php echo $value1['service_id'];?>" data-id="<?php echo $value1['id']; ?>" data-duevalue="<?php echo $value1['due']; ?>" data-duedays="<?php echo $value1['days'];?>" data-subject="<?php echo $value1['subject'];?>" ><i class="fa fa-edit"></i> edit</a>
                                  <?php } ?> <!-- data-message="<?php echo html_entity_decode($value1['message']);?>" onclick="getColor('<?php echo $value1['id']; ?>','<?php echo $value1['headline_color'];?>');" data-toggle="modal" data-target="<?php echo "#edit-reminder_".$value1['id']; ?>" -->
                              <div class="position-es">
                                  <i class="fa fa-envelope"></i>
                                </div>
                            </div>
                          </div>                         
                          <?php } ?>                        
                        </div>
                      </div> 
                      <?php } ?>                  
                    </div>
                  </div>
                </div>                
              <?php } } ?>
                </div>
              </div>
         </div>
    </div> 

 
 

  <?php 

  if($_SESSION['firm_id'] == '0')
  {
     $this->load->view('super_admin/superAdmin_footer');
  }
  else
  {
     $this->load->view('includes/footer');
  }

  ?>   
 

<script type="text/javascript">

   $(document).on('click','#close_info_msg',function()
    { 
       $('.popup_info_msg').hide();  
    });

    $(document).on('click','.add_reminder,.reminder_popup',function()
    { 
        var service_id = $(this).data('service');
        var reminder_id = $(this).data('id');

        if(typeof(reminder_id) == 'undefined')
        {
          var $url = '<?php echo base_url().'user/reminder/'; ?>'+service_id;
        }
        else
        {
          var $url = '<?php echo base_url().'user/reminder/'; ?>'+service_id+'/'+reminder_id;
        }

        var windowObjectReference;
        var WindowFeatures = "width=1450,height=800,left=300,top=100,resizable=yes,menubar=no,titlebar=no,status=no,location=no";
        $(".LoadingImage").show();
        windowObjectReference = window.open($url, '', WindowFeatures);

        var timer = setInterval(function() 
        {
            if(windowObjectReference.closed) 
            {  
                clearInterval(timer);               
                location.reload();               
            }  
        }, 1000); 
    });


  /*$(document).ready(function()
  {
      var service_id = '<?php echo $this->uri->segment(3); ?>';
      
      if(service_id!='')
      { 
        get_reminders(service_id);
      }
    
  });*/

 /* function get_reminders(id)
  {
     $.ajax(
     {
          url: '<?php echo base_url(); ?>/User/reminder_settings/'+id,
          type: 'post',
          data: {'id':id},
          beforeSend: function() 
          {
               $(".LoadingImage").show();
          },
          success: function(response)
          { 
              $(".reminder_content").html('');
              $(".reminder_content").html(response);
              $(".LoadingImage").hide();
          }
     });
  }
*/

</script>