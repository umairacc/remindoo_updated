<?php $this->load->view('includes/new_header'); ?>

<!-- <link rel="stylesheet" type="text/css" href="<?php //echo base_url()
                                                  ?>assets/css/email.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css"> -->

<!--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">

<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css">-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.dataTables.min.css">

<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css?ver=2">-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">


<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/colReorder.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/tree_select/style.css?ver=2">

<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"> -->

<style type="text/css">
  .add_class {
    display: none;
  }

  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
  }

  .show {
    display: block !important;
  }

  tfoot {
    display: table-header-group;
  }

  .top-leads .junk-lead strong {
    width: 36px !important;
  }

  a.individual_timer_btn {
    background: #c077ef;
    padding: 4px 15px;
    color: #fff;
    display: inline-block;
    line-height: 18px;
    border-radius: 5px;
    font-weight: 600;
    vertical-align: top;
  }

  .top-leads.fr-task .tsk-animation1 .junk-lead.color-junk2 {
    min-width: 139px;
  }

  /* Fix modal hidden issue after opening modal for 2nd time */
  /* Ritesh - 13062020 */

  .adduserpopup+.modal-backdrop {
    z-index: 1039 !important;
  }

  .adduserpopup.in {
    display: block !important;
  }

  @media (min-width: 768px) {
    .new_layoutalign .top-leads.fr-task .tsk-animation1 .tsk-color4 {
      min-width: 119px;
    }
  }

  .comboTreeInputWrapper input.comboTreeInputBox {
    margin-bottom: 10px;
  }

  .comboTreeDropDownContainer {
    padding-top: 0;
  }    

 .notification-bubble{
    background: red;
  }

  .blink {
    animation: blink 2s steps(5, start) infinite;
    -webkit-animation: blink 1s steps(5, start) infinite;
  }
  @keyframes blink {
    to {
      visibility: hidden;
    }
  }
  @-webkit-keyframes blink {
    to {
      visibility: hidden;
    }
  }

/* TOOLTIP */
.msg-content {
  position: absolute;
  top: 22px;
  left: -6px;
  width: 500px;
  padding: 15px;
  max-height: 0px;
  overflow: hidden;
  visibility: hidden;
  transition: 0.5s all ease;
  background-color: #fff !important;
  z-index: 999;
  border: 2px solid #d4d4d4 !important;
  border-radius: 5px;
  /* max-height: 500px; */
}
.msg-wrapper:hover .msg-content {
  max-height: 300px;
  overflow: visible;
  overflow-y: hidden;
  visibility: visible;
}
.upper.msg-content {
	top: auto;
	bottom: 22px;
}
.msg-content .temp-notification{
  max-height: 200px;
  overflow: auto;
  margin-bottom: 10px;
}
.msg-content .temp-notification::-webkit-scrollbar{
  width: 10px;
}

.msg-wrapper {
  position:relative;
  width:auto;
  display:inline-block;
  margin-left:5px;
  cursor:pointer;
}
.phone-wrapper{
  display: inline-block;
  cursor: pointer;
}
.phone-wrapper i.fa.fa-phone{
  color: #fff;
}
.msg-wrapper .msg-content table{
  margin-bottom: 10px;
}
.msg-wrapper .msg-content table tr td:first-child{
  text-align: center;
  font-weight: bold;
  font-size: 15px;
}
.msg-wrapper .msg-content table tr td p{
  text-align: justify;
  text-transform: capitalize;
  display: inline-block;
}

.msg-wrapper a {
  color: white;
}
.msg-wrapper a.mark_as_read{
  color: #000;
  font-weight: 600;
  border: 2px solid #d4d4d4;
  padding: 5px;
  border-radius: 5px;
  background: transparent;
  text-transform: capitalize;
  display: inline-block;
  margin-left: 180px;
  box-shadow: 0 2px 5px 0 rgb(0 0 0 / 16%), 0 2px 10px 0 rgb(0 0 0 / 12%) !important;
}
.msg-wrapper a.mark_as_read:hover{
  background-color: #d4d4d4;
  color: #000000;
}
/* /TOOLTIP */

.int-due{
  border-bottom: 1px solid;
}

span.int-due-title {
    font-weight: 700;
}

span.ext-due-title {
    font-weight: 700;
}

.task_table_th{
  opacity: 0;
}
/* #assignuser_46658 {
    z-index: 99 !important;
} */
</style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/custom/search-scroll.css">
<div class="dummy_content" style="display: none;"></div>

<!-- Modal Content Starts -->
<div class="modal fade" id="my_Modal" role="dialog">
  <div class="modal-dialog modal-archive-sub-task">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header header-archive-sub-task">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body body-archive-sub-task">
        <input type="hidden" name="archive_task_id" id="archive_task_id" value="">
        <input type="hidden" name="status_value" id="status_value" value="">
        <p>Do You Want to Archive this Task?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="archive_action()">yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">no</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteconfirmation" role="dialog">
  <div class="modal-dialog modal-delete-sub-task">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header header-delete-sub-task">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body body-delete-sub-task">
        <input type="hidden" name="delete_task_id" id="delete_task_id" value="">
        <p> Are you sure you want to delete ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="confirm_delete" data-dismiss="modal" onclick="delete_action()">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="review_send" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="delete_task_id" id="delete_task_id" value="">
        <p> Are you sure want send review ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="confirm_delete" data-dismiss="modal" onclick="review_action()">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="markBillable-Popup" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="billable_task_id" id="billable_task_id" value="">
        <p> Are you sure want change from Non Billable to Billable ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="billable_send" data-dismiss="modal" onclick="billable_action()">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="individual_timer_popup" role="dialog">
  <div class="modal-dialog modal-view-task">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header header-view-task">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">
          <i class="icofont icofont-users-alt-4"></i> Performance of users

        </h4>
      </div>
      <div class="modal-body modal-body-view">

      </div>

    </div>
  </div>
</div>

<!-- Birthday Modal Starts -->
<div class="modal fade" id="popup_birthday_mail" role="dialog" data-backdrop="static" style="display: none;">
	<div class="modal-dialog modal-birthday-mail">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header modal-birthday-mail-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Send Mail</h4>
			</div>
      <form action="<?php echo base_url(); ?>User/send_mail_sms" id="send_mail_form" method="post" name="send_mail_sms_form">
			  <div class="modal-body modal-birthday-mail-body">
          <div class="email-cc">
            <label>Called On:</label>
            <input type="date" name="date" value="<?php echo (new DateTime())->format('Y-m-d'); ?>">
          </div>
          <div class="email-cc">
            <label>Time:</label>
            <input type="time" name="time" value="<?php echo date("h:i:s") ?>" >
          </div>
          <div class="email-cc">
            <label>Telephone No.:</label>
            <input type="tel" name="tel" id="crm-tel">
          </div>
          <div class="email-cc">
            <label>To:</label>
            <div id="send_emails">
              <div class="single_mail" id="single_mail">
                <input type="email" name="email" class="myBdayEmailCc" id="myBdayEmailCc" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}">
              </div>
            </div>
            <a href="javascript:;" class="add-email-cc" id="bdayBtnCc">add email</a>
          </div>
          <div class="email-cc-respond">
            <label>Responded:</label>
            <input type="radio" name="result" value="yes" /> Yes
            <input type="radio" name="result" value="no" /> No
          </div>
          <div class="msg-email">
            <label>Message:</label>
            <textarea name="msg" id="bday_notification_msg" placeholder="Type your text here..."></textarea>
          </div>
        </div>			
        <div class="modal-footer modal-birthday-footer">
            <input type="hidden" name="mail_ids" id="recivers_id">
            <input type="hidden" name="data_task_id" id="data_task_id">
            <input type="hidden" name="data_client_id" id="data_client_id">
            <button type="button" id="save_to_timeline" class="btn btn-default">save</button>
            <button type="submit" class="btn btn-default">send email</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
		</div>

	</div>
</div>
<!-- Birthday Modal ends -->
<!-- Modal Content Ends -->


<?php
  $service_display = '';
  $role = $this->Common_mdl->getRole($_SESSION['id']);

  function convertToHoursMins($time, $format = '%02d:%02d')
  {
    if ($time < 1) {
      return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
  }

  function calculate_test($a, $b)
  {
    $difference = $b - $a;

    $second = 1;
    $minute = 60 * $second;
    $hour   = 60 * $minute;

    $ans["hour"]   = floor(($difference) / $hour);
    $ans["minute"] = floor((($difference) % $hour) / $minute);
    $ans["second"] = floor(((($difference) % $hour) % $minute) / $second);
    //echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

    $test = $ans["hour"] . ":" . $ans["minute"] . ":" . $ans["second"];
    return $test;
  }

  function time_to_sec($time)
  {
    list($h, $m, $s) = explode(":", $time);
    $seconds = 0;
    $seconds += (intval($h) * 3600);
    $seconds += (intval($m) * 60);
    $seconds += (intval($s));
    return $seconds;
  }

  function sec_to_time($sec)
  {
    return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
  }

?>

<style>
  /** 28-06-2018 **/
  .time {
    padding: 8px;
    font-weight: bold;
  }


  select.close-test-04 {
    display: none;
  }

  /** 04-06-2018 **/
  span.demo {
    padding: 0 10px;
  }

  button.btn.btn-info.btn-lg.newonoff {
    padding: 3px 10px;
    height: initial;
    font-size: 15px;
    border-radius: 5px;
  }

  img.user_imgs {
    width: 38px;
    height: 38px;
    border-radius: 50%;
    display: inline-block;
  }


  #adduser label {
    text-transform: capitalize;
    font-size: 14px;
  }

  span.hours-left {
    background: #f76480;
    color: #fff;
    padding: 3px 10px;
    border-radius: 3px;
    margin-right: 15px;
  }

  .dropdown12 button.btn.btn-primary {
    background: #ccc;
    color: #555;
    font-size: 13px;
    padding: 4px 10px 4px;
    margin-top: -2px;
    border-color: transparent;
  }

  select+.dropdown:hover .dropdown-menu {
    display: none;
  }

  select+.dropdown12 .dropdown-menu {
    padding: 0 !important;
  }

  select+.dropdown12 .dropdown-menu a {
    color: #000 !important;
    padding: 7px 10px !important;
    border: none !important;
    font-size: 14px;
  }

  select+.dropdown12 .dropdown-menu li {
    border: none !important;
    padding: 0px !important;
  }

  select+.dropdown12 .dropdown-menu a:hover {
    background: #4c7ffe;
    color: #fff !important;
  }

  span.created-date {
    color: gray;
    padding-left: 10px;
    font-size: 13px;
    font-weight: 600;
  }

  span.created-date i.fa.fa-clock-o {
    padding: 0 5px;
  }

  .dropdown12 span.caret {
    right: -2px;
    z-index: 99999;
    border-top: 4px solid #555;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    border-bottom: 4px solid transparent;
    top: 12px;
    position: relative;
  }

  span.timer {
    padding: 0 10px;
    color: red;
  }

  body {
    font-family: "Arial", Helvetica, sans-serif;
    text-align: center;
  }

  #controls {
    font-size: 12px;
  }

  #time {
    font-size: 150%;
  }
</style>

<?php

// $for_main_task_timer=$result_status_array=array();
// $for_user_edit_per=array();     
// if(count($for_user_edit_permission)>0){
// foreach ($for_user_edit_permission as $edit_per_key => $edit_per_value) {
//  array_push($for_user_edit_per, $edit_per_value['id']);
// }
// }
?>
<!-- end of 29-06-2018 -->
<div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
  <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
    <div class="pop-realted1">
      <div class="position-alert1">
        Please! Select Record...
      </div>
    </div>
  </div>
</div>
<div id="action_result" class="modal-alertsuccess alert succ dashboard_success_message" style="display:none;">
  <div class="newupdate_alert"> <a href="#" class="close" id="close_action_result">×</a>
    <div class="pop-realted1">
      <div class="position-alert1">
        Success !!! Staff Assign have been changed successfully...
      </div>
    </div>
  </div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/progress-circle.css">

<div class="pcoded-content card-removes" id="task_list_view">
  <div class="pcoded-inner-content">
    <!-- Main-body start -->
    <div class="main-body">
      <div class="page-wrapper">
        <!-- Page body start -->
        <div class="page-body rem-tasks">
          <div class="row">
            <div class="col-sm-12">
              <!-- Register your self card start -->
              <div class="card">
                <!-- admin start-->
                <div class="client_section col-xs-12 floating_set newtaslcls">
                  <input type="hidden" name="assigneed_value" id="assigneed_value" value="">
                </div>
                <div class="page-heading">Tasks</div>
                <div class="page-tabs">
                  <ul id="task_active_wrap" class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard client-top-nav">

                    <li class="nav-item">
                      <a class="nav-link active" href="<?php echo base_url(); ?>user/task_list">All Tasks</a>
                      <div class="slide"></div>
                    </li>

                    <?php if ($_SESSION['permission']['Task']['create'] == 1) { ?>
                      <li class="nav-item">
                        <a class="nav-link nav-link-tasks " href="<?php echo base_url(); ?>user/new_task">Create task</a>
                        <div class="slide"></div>
                      </li>

                      <!-- <li class="nav-item">
                        <a class="nav-link nav-link-tasks " href="<?php echo base_url(); ?>task">Task Timeline</a>
                        <div class="slide"></div>
                      </li> -->

                    <?php }
                    if ($_SESSION['user_type'] == 'FA') { ?>
                      <li class="nav-item">
                        <a class="nav-link nav-link-tasks " href="<?php echo base_url(); ?>Task_Status/task_progress_view">Task Progress Status</a>
                        <div class="slide"></div>
                      </li>
                    <?php }

                    if ($_SESSION['permission']['Task']['edit'] == 1) { ?>
                      
                      <li class="nav-item">
                        <a class="nav-link nav-link-tasks " href="<?php echo base_url(); ?>user/task_list_kanban">Switch To Kanban</a>
                        <div class="slide"></div>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link nav-link-tasks archive-task" href="<?php echo base_url(); ?>user/task_list/archive_task">Archive Task</a>
                        <div class="slide"></div>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link nav-link-tasks completed-task" href="<?php echo base_url(); ?>user/task_list/complete_task">Completed Task</a>
                        <div class="slide"></div>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link nav-link-tasks" data-toggle="modal" data-target="#import-task">Import tasks</a>
                        <div class="slide"></div>
                      </li>

                    <?php } ?>
                    <li id="headertask_timer" style="display: none;margin-top:10px !important;margin-left:10px !important;" class='dropdown comclass'>Task Timer:&nbsp;&nbsp;<label class="hours"></label>:<label class="minutes"></label>:<label class="seconds"></label></li>
                    <li id="headertask_timer_msg" style="display: none;margin-top:12px !important;margin-left:10px !important;color:red;" class='dropdown comclass'></li>
                  </ul>
                </div>
                <!-- all-clients -->
                <div class="all_user-section floating_set clientredesign">
                  <div class="deadline-crm1 floating_set tsk-crte">
                    <div class="top-leads fr-task cfssc">
                      <!-- <div class="lead-data1 pull-right tsk-animation1">

                        <div class="page-data-block">

                          <div class="data-counts rem-card">

                            <?php
                            $task_count = $all_count = 0;
                            $task_count = $task_list_count;

                            // if($status_condition==0){
                            $result_status_array = $task_status;


                            ?>

                            <div class="data-count">
                              <a href="javascript:void(0);">
                                <span class="count"><?php echo $task_count; ?></span>
                                <p>All</p>
                              </a>
                            </div>


                            <?php
                            // } else{

                            //  $result_status_array=$this->Common_mdl->selectRecord('task_status','id',$status_condition);
                            // }
                            //print_r($get_datalist);

                            
                            foreach ($result_status_array as $key => $status_val) {

                              if (in_array($status_val['id'], array_keys($get_datalist))) {
                                $status_count = $get_datalist[$status_val['id']];
                              } else {
                                $status_count = 0;
                              }
                              //
                              //    echo $status_val['status_name'];

                            ?>



                              <div class="data-count task_status_val nots_task_<?php echo $status_val['id'] ?>" data-id="<?php echo $status_val['id'] ?>" data-searchCol="status_TH">
                                <span class="count"><?php echo $status_count; ?></span>
                                <p class="task_status_val_1 status_filter" data-id="<?php echo $status_val['status_name'] ?>"><?php echo $status_val['status_name'] ?></p>
                              </div>

                            <?php } ?>

                          </div>
                        </div>
                      </div> -->
                      <!-- <div class="nav-search-wrapper"> -->

                        <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard don1 nav-task renewdesign">                        
                          <li class="nav-item">
                            <a class="nav-link  color1 color-all tap_click active" href="javascript:void(0);" data-id="All">All
                              <span class="all-number"><?php echo $task_count; ?></span></a>
                            <div class="slide"></div>
                          </li>
                          <?php
                            if(isset($result_status_array[0]['status_name'])){
                            ?>
                            <li class="nav-item">
                              <a class="nav-link color2 color-start tap_click " href="javascript:void(0);" data-id="1" data-child-id="active_checkbox"><?php echo $result_status_array[0]['status_name'] ?>
                              <span class="start-number"><?php echo $get_datalist[$result_status_array[0]['id']]; ?></span></a>
                              <div class="slide"></div>
                            </li>
                            <?php
                            }?>
                            <?php
                            if(isset($result_status_array[1]['status_name'])){
                            ?>
                          <li class="nav-item">
                            <a class="nav-link color3 color-progress tap_click" href="javascript:void(0);" data-id="|0|2" data-child-id="inactive_checkbox"><?php echo $result_status_array[1]['status_name'] ?>
                            <span class="progress-number"><?php echo $get_datalist[$result_status_array[1]['id']]; ?></span></a>
                            <div class="slide"></div>
                          </li>
                          <?php
                            }?>
                          <?php
                            if(isset($result_status_array[2]['status_name'])){
                            ?>
                          <li class="nav-item">
                            <a class="nav-link color5 color-feedback tap_click" data-id="5" href="javascript:void(0);" data-child-id="archive_checkbox"><?php echo $result_status_array[2]['status_name'] ?>
                            <span class="feedback-number"><?php echo $get_datalist[$result_status_array[2]['id']]; ?></span></a>
                            <div class="slide"></div>
                          </li>
                          <?php
                            }?>
                        </ul>
                      <!-- </div> -->
                      <div class="f-right temp-task">
                        <div class="assign_delete">
                          <button type="button" data-toggle="modal" data-target="#markBillable-Popup" id="mark_billable" class="btn   mark_billable_btn" style="display: none;">Mark Billable</button>

                            <?php  if( $_SESSION['permission']['Task']['create']==1) { ?>
                            <button type="button" id="addSubtask-Btn" data-toggle="modal" data-target="#addSubTask-Popup" class="btn" style="display: none;">Add Sub Task </button>
                            <?php }   if( $_SESSION['permission']['Task']['edit']==1){ ?>
                            <button type="button" data-toggle="modal" data-target="#assignTask-Popup" id="task_assign_btn" class="btn   task_assign_btn" style="display: none;">Reassign</button>

                            <button type="button" id="archive_task" class="archive_task_button del-tsk12 " data-status="4" data-toggle="modal" data-target="#my_Modal" style="display: none;"><i class="fa fa-archive fa-6" aria-hidden="true"></i>Archive </button>
                            <?php } ?>
                            <!--   
          <button type="button"  data-toggle="modal" data-target="#review_send" id="btn_review_send" class="btn   for_user_review_send" style="display: none;">Send Review</button> -->

                            <?php if($_SESSION['permission']['Task']['delete']=='1'){ ?>
                            <button type="button" data-toggle="modal" data-target="#deleteconfirmation" id="delete_task" class="del-tsk12 for_user_permission_delete" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete </button>
                            <?php } ?>
                            <button type="button" id="unarchive_task" class="archive_task_button del-tsk12 " data-status="unarchive" style="display: none;"><i class="fa fa-archive fa-6" aria-hidden="true"></i>Unarchive </button>
                        </div>
                    </div>
                    </div>
                  </div>

                  <div class="all_user-section2 floating_set <?php if ($_SESSION['permission']['Task']['view'] != '1') { ?> permission_deined<?php } ?>">
                    <div class="page-data-block pt0">
                      <div class="rem-card rem-card-task">
                        <div class="tab-content">
                          <div class="filter-data for-reportdash" id="filter-report-data">
                            <div class="filter-head">
                              <div class="filter-head-inner">
                                <h4>FILTERED DATA: </h4>
                                <div id="container2" class="panel-body box-container">
                                </div>                                
                              </div>
                              <div>
                                <button class="btn btn-danger f-right filter-clear" id="clear_container">clear</button>
                              </div>
                            </div>                            
                          </div>
                          <div id="alltasks" class="data_active_task tab-pane fade in active">
                            <div class="all_task_counts">
                            </div>
                            <input type="hidden" name="for_status_us" id="for_status_us">
                            <input type="hidden" value="" id="for_status_id">
                            <input type="hidden" value="" id="colwise_filter_val">
                            <div class="client_section3  floating_set">
                              <div id="status_succ"></div>
                              <div class="all-usera1  user-dashboard-section1 for_task_status button_visibility">

                                <table class="table client_table1 all_task_table text-center display nowrap tasklistseccls" id="alltask" cellspacing="0" width="100%" data-status="notstarted">
                                  <thead>
                                    <?php $this->load->view('users/task_table_header'); ?>

                                  </thead>

                                </table>
                                <input type="hidden" class="rows_selected" id="select_alltask_count">
                              </div>
                            </div>
                          </div>
                          <!-- home-->
                          <!-- new_user -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- admin close -->
                </div>
                <!-- Register your self card end -->
              </div>
            </div>
          </div>
          <!-- Page body end -->
        </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>


<div id="adduser_{0}" class="adduserpopup modal-reassign-status modal fade" role="dialog">
  <div class="modal-dialog modal-assign-task">

    <div class="modal-content">
      <div class="modal-header modal-header-task">
        <button type="button" class="close close_popup">&times;</button>
        <h4 class="modal-title" style="text-align-last: center">Assign top</h4>
      </div>
      <div class="modal-body modal-body-task">
        <input type="text" class="tree_select tree_select_task" name="assignees[]" placeholder="Select">
        <div class="formcon formconnew">
          <label class="reason-task">Reason</label>
          <select name="assign_task_status" class="form-control assign_task_status assign_status_modal" id="assign_task_status_{0}">
            <option value="">Select</option>
            <?php foreach ($assign_task_status as $key => $value1) {
              if ($value1['status_name'] != '') { ?>
                <option value="<?php echo $value1['id'] ?>"><?php echo $value1['status_name'] ?></option>
            <?php }
            } ?>
          </select>
          <div class="atagsection">

          </div>
        </div>
        <div class="formcon">
          <label class="desc-task">Description</label>
          <textarea rows="5" type="text" class="form-control desc-task-area" name="description" id="description_{0}" placeholder="Write something"></textarea>
        </div>
      </div>
      <div class="modal-footer profileEdit">
        <input type="hidden" name="hidden">
        <a href="javascript;" id="acompany_name" data-dismiss="modal" data-id="{0}" class="save_assign_staff">save</a>
      </div>
    </div>
  </div>
</div>
<div class="task_status_html"></div>

<div class="modal fade" id="import-task" role="dialog">
  <div class="modal-dialog modal-import-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header modal-import-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Import Task</h4>
      </div>
      <div class="modal-body modal-import-body">
        <!-- content update after page load source from tasks/import viewfile -->
      </div>
    </div>
  </div>
</div>

<div id="addSubTask-Popup" class="modal fade" role="dialog">
  <div class="modal-dialog modal-add-sub-task">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header header-add-sub-task">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align-last: center">Add Sub Task</h4>
      </div>
      <div class="modal-body body-add-sub-task">
        <input id="subTaskName" placeholder="Task Name" class="addSubtask">
      </div>
      <div class="sub_task_error" style="color:red"></div>
      <div class="modal-footer profileEdit">
        <a href="javascript:;" id="saveSubtaskbutton" class="save-add-sub-task">save</a>
        <a href="javascript:;" data-dismiss="modal" class="close-add-sub-task">Close</a>
      </div>
    </div>
  </div>
</div>

<div id="assignTask-Popup" class="modal fade" role="dialog">
  <div class="modal-dialog modal-assign-sub-task">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header header-assign-sub-task">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
      </div>
      <div class="modal-body body-assign-sub-task">

        <input type="text" class="tree_select" name="assignees[]" placeholder="Select">

      </div>
      <div class="modal-footer profileEdit">
        <input type="hidden" name="hidden">
        <a href="javascript;" id="assign_tsk_btn" data-dismiss="modal" class="assign_tsk_btn">save</a>
      </div>
    </div>
  </div>
</div>
<div class="modal-alertsuccess alert info_popup" style="display:none;">
	<div class="newupdate_alert">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
		<div class="pop-realted1">
			<div class="position-alert1">

			</div>
		</div>
	</div>
</div>

<input type="hidden" id="active_timer_id" value="">



<?php $this->load->view('includes/session_timeout'); ?>
<?php $this->load->view('includes/footer'); ?>


<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->

<script src="<?php echo base_url() ?>assets/js/task/bootstrap.min.js"></script>

<!--<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->

<script src="<?php echo base_url() ?>assets/js/task/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/user_page/materialize.js"></script>

<!--<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>-->

<script src="<?php echo base_url() ?>assets/js/task/dataTables.buttons.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/custom/buttons.colVis.js"></script>

<!--<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>-->

<script src="<?php echo base_url() ?>assets/js/task/dataTables.colReorder.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/custom/datatable_extension.js"></script>

<!-- For Export Buttom -->

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>-->

<script src="<?php echo base_url() ?>assets/js/task/jszip.min.js"></script>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>-->

<script src="<?php echo base_url() ?>assets/js/task/pdfmake.min.js"></script>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>-->

<script src="<?php echo base_url() ?>assets/js/task/vfs_fonts.js"></script>

<!--<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>-->

<script src="<?php echo base_url() ?>assets/js/task/buttons.html5.min.js"></script>
<!--<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>-->

<script src="<?php echo base_url() ?>assets/js/task/buttons.print.min.js"></script>

<!-- For Export Buttom -->

<!-- <script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>  -->

<script src="<?php echo base_url(); ?>assets/js/jquery.dropdown.js"></script>

<!-- <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script> -->

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<!-- For Date fields sorting  -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/custom/datatable_cus_sorting.js"></script>
<!-- For Date fields sorting  -->

<script src="<?php echo base_url() ?>assets/js/tinymce.min.js"></script>

<script>  
  var tinymce_config = {    
    plugins: ' preview  paste importcss searchreplace autolink     visualblocks visualchars fullscreen image link table  hr pagebreak nonbreaking anchor toc  advlist lists imagetools textpattern noneditable ',
    menubar: 'file edit view insert format tools table',
    toolbar: 'fontselect fontsizeselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | insertfile image  link | numlist bullist  | hr | fullscreen  preview ',
    file_picker_types: "image"
  };

  $(document).ready(function(){    
    tinymce_config['selector'] = '#bday_notification_msg';
    tinymce.init( tinymce_config );  
  });
</script>

<script type="text/javascript">
  var export_hiddencolumns = [':visible:not(.subtask_toggle_TH, .select_row_TH, .action_TH, .SELECT_ROW_TH, .ACTION_TH, .SUBTASK_TOGGLE_TH )'];

  var DataTable_Task_Export_Buttons = [$.extend(true, {}, ExportCells_Text, {
      text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>Pdf',
      extend: 'pdfHtml5',
      orientation: 'landscape', //portrait 
      pageSize: 'A4',
      exportOptions: {
        columns: export_hiddencolumns,

      },
      // customize: function (doc) {                                  
      // doc.pageMargins = [20,60,20,30];
      // // Set the font size fot the entire document
      // doc.defaultStyle.fontSize = 7;
      // // Set the fontsize for the table header
      // doc.styles.tableHeader.fontSize = 7;            
      // // To use predefined layouts uncomment the line below and comment the custom lines below
      // // doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
      // var objLayout = {};
      // objLayout['hLineWidth'] = function(i) { return .5; };
      // objLayout['vLineWidth'] = function(i) { return .5; };
      // objLayout['hLineColor'] = function(i) { return '#aaa'; };
      // objLayout['vLineColor'] = function(i) { return '#aaa'; };
      // objLayout['paddingLeft'] = function(i) { return 4; };
      // objLayout['paddingRight'] = function(i) { return 4; };
      // doc.content[0].layout = objLayout;
      // }

    }),
    $.extend(true, {}, ExportCells_Text, {
      text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>Csv',
      extend: 'csv',
      exportOptions: {
        columns: export_hiddencolumns,
      }
    }),
    $.extend(true, {}, ExportCells_Text, {
      text: '<i class="fa fa-print" aria-hidden="true"></i>Print',
      extend: 'print',
      exportOptions: {
        columns: export_hiddencolumns,
      }
    }),
  ];

  var vars = {};
  var row_vars = [];

  var reassign_status_poupup = '<div id="assignuser_{0}" class="adduserpopup03 adduserstatuspopup  modal fade" role="dialog">                              <div class="modal-dialog modal-dialog-status">           <div class="modal-content">                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title" style="text-align-last: center">Reason of Status</h4>                                    </div>                                <div class="modal-body delete_st_reason">                                  <input type="text" name="assignuser_staff" class="assignuser_staff_class" id="assignuser_staff_{0}"  >                             <div style="color: red" class="assign_staff_div"></div>                                    </div>                                <input type="hidden" name="assignuser_staffid" class="assignuser_staffid_class" id="assignuser_staffid_{0}"  >                                  <input type="hidden" name="update_staffid" class="update_staffid_class" id="update_staffid_{0}"  >          <div class="modal-footer profileEdit delete_st_reason">                                       <a href="javascript:;" id="assignuser_staff"  data-id="{0}" class="assignuser_staff">save</a>                                    </div>                                    <div class="modal-footer profileEdit delete_div_reason" style="display: none;">                                   <a href="javascript:;" id="assignuser_staff"  data-id="{0}" class="assignuser_staff">ok</a>                                         <a href="javascript;" data-dismiss="modal" data-id="{0}" >cancel</a>                                    </div>                                 </div>                              </div>                           </div>';

  $(document).ready(function() {
    $.fn.dataTable.moment('DD-MM-YYYY');

    /*$('.tags').tagsinput({
        allowDuplicates: true
      });*/

    $('.dropdown-sin-2').dropdown({
      limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
    $('.dropdown-sin-7').dropdown({
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });

    $('.dropdown-sin-25').dropdown({
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
    $('.dropdown-sin-27').dropdown({
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
    $('.dropdown-sin-29').dropdown({
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });

  });

  $(document).ready(function() {

    $("#confirm-submit.confirm12").on("shown.bs.modal", function() {

      // alert('hi'); 

      $('div#pcoded + div + div + .modal-backdrop.fade.show').css('z-index', '9999');

    });

    $("#confirm-submit.confirm12").on("hidden.bs.modal", function() {

      // alert('hi'); 

      $('div#pcoded + div + div + .modal-backdrop.fade.show').css('z-index', '1040');

    })
  });
</script>

<script type="text/javascript">
  function getSelectedRow() {
    var alltask = [];

    TaskTable_Instance.column(1).nodes().to$().each(function(index) {
      if ($(this).find(".alltask_checkbox").is(":checked")) {
        alltask.push($(this).find(".alltask_checkbox").attr('data-alltask-id'));
      }
    });

    return alltask;
  }

  var TaskTable_Instance;
  var MainStatus_Tab = 'all_task';
  /*single and multiple row task archive*/
  $('.archive_task_button').on('click', function() {

    var alltask = getSelectedRow();

    if (alltask.length <= 0) {
      $('.popup_info_msg').show();
      $(".popup_info_msg .position-alert1").html("Please Selecte Any Records..");
    } else {
      // console.log(alltask);
      $("#archive_task_id").val(JSON.stringify(alltask));
      $("#status_value").val($(this).data('status'));
      //unarchive does not had conform box
      if ($(this).data('status') == "unarchive") archive_action();
    }
  });

  function archive_action() { //model yes button click
    var id = $("#archive_task_id").val();
    var status = $("#status_value").val();
    $.ajax({
      url: '<?php echo base_url(); ?>user/archive_update',
      type: 'POST',
      data: {
        'id': id,
        'status': status
      },
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {
        $(".LoadingImage").hide();
        $(".popup_info_msg").show();
        $(".popup_info_msg .position-alert1").html('Success !!! Task Archive successfully...');
        //    console.log(data);
        setTimeout(function() {
          $(".popup_info_msg").hide();
          location.reload();
        }, 1500);
      }
    });
  }

  function archieve_click(val) { //single button click
    //  alert('ok');
    //  alert($(val).attr('id'));
    $("#archive_task_id").val(JSON.stringify([$(val).attr('id')]));
    $("#status_value").val($(val).data('status'));
    //alert($("#archive_task_id").val());
    //alert($("#my_Modal").attr('class'));
  }
  /*single and multiple row task archive*/

  function humanise(diff) {

    // The string we're working with to create the representation 
    var str = '';
    // Map lengths of `diff` to different time periods 
    var values = [
      [' year', 365],
      [' month', 30],
      [' day', 1]
    ];
    // Iterate over the values... 
    for (var i = 0; i < values.length; i++) {
      var amount = Math.floor(diff / values[i][1]);
      // ... and find the largest time value that fits into the diff 
      if (amount >= 1) {
        // If we match, add to the string ('s' is for pluralization) 
        str += amount + values[i][0] + (amount > 1 ? 's' : '') + ' ';
        // and subtract from the diff 
        diff -= amount * values[i][1];
      } else {
        str += amount + values[i][0] + ' ';
      }

    }
    return str;
  }
</script>

<?php

?>
<script></script>

<script>
  // $(document).on('click','.sendtoreview',function(){
  //   var id=$(this).data("id");

  //   var timer=0;
  //   timer+=parseInt($(".timer_"+id+" .hours").text());
  //   timer+=parseInt($(".timer_"+id+" .minutes").text());
  //   timer+=parseInt($(".timer_"+id+" .seconds").text());
  // //alert(timer);
  // if(timer<=0){$(".popup_info_msg").show();$(".popup_info_msg .position-alert1").html("You Don't Have Worked Time..");return;}
  //   $.ajax({
  //             url: '<?php echo base_url(); ?>user/task_statusChange/',
  //             type: 'post',
  //             data: { 'rec_id':id,'status':'5'},
  //             timeout: 3000,
  //             success: function( data ){
  //                $(".popup_info_msg").show();
  //                $('.popup_info_msg .position-alert1').html('Success !!! Request Send successfully...');
  //                setTimeout(function () {
  //                 $(".popup_info_msg").hide();
  //                 }, 1500);

  //                 },
  //                 error: function( errorThrown ){
  //                     console.log( errorThrown );
  //                 }
  //             });
  // });
  function AddClass_SettingPopup() {
    $('button.Button_List').closest('.dt-button-collection').addClass('Settings_List');
    $('button.buttons-columnVisibility').closest('.dt-button-collection').addClass('columnVisibility_List');
  }

  $(document).ready(function() {

    last_filter_data_request = "";
    // $.ajax({
    //               url: '<?php echo base_url(); ?>Test_slowness/ajax_task',
    //               type : 'POST',
    //                 beforeSend: function() {
    //                   $(".LoadingImage").show();
    //                 },
    //                 success: function(data) {
    //                   $('alltask tbody').html('').html(data.task_table);
    //                   $(".LoadingImage").hide();
    //                 }

    //               });
    /*for COLUMN oredering*/
    <?php
      $column_setting = Firm_column_settings('task_list');
    ?>
    var column_order = <?php echo $column_setting['order'] ?>;

    //console.log( column_order );

    //   var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;

    var column_ordering = [];

    $.each(column_order, function(i, v) {

      var index = $('#alltask thead th').index($('.' + v));

      if (index !== -1) {
        // alert(index);
        column_ordering.push(index);
      }
    });

    // console.log('a-'+column_ordering);
    /*for COLUMN oredering*/
    // table_sorting_trigger();
    //          "dom": '<"toolbar-table" <"#table-buttons.dropdown.table-buttons"<"#table-buttonsList.dropdown-menu.table-buttonsList"B>> >lfrtip',

    humanise();

    var timer_unique_data = [];
    var subject_unique_data = [];
    var startdate_unique_data = [];
    var duedate_unique_data = [];
    var services_unique_data = [];
    var tag_unique_data = [];
    var company_unique_data = [];

    var pageLength = <?php echo get_firm_page_length() ?>;

    TaskTable_Instance = $('#alltask').DataTable({

      // "pageLength": parseInt(pageLength),
      "pageLength": 25,
      // "lengthChange": false,
      "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
      "deferRender": true,
      "processing": true,
      "serverSide": true,
       dom: 'Bfrtip',
       stateSave: true,
       "stateDuration": 60 * 60 * 3600,
      'ajax': {
        'url': '<?php echo base_url() ?>Task/get_task_ajax_data',
        'type': 'POST',
        beforeSend: Show_LoadingImg,
        'data': function(data) {
          // get_th();

          data.top_status_search  = $('#for_status_id').val()
          data.status_search      = $('.statuswise_filter').val();
          data.priority_search    = $('.prioritywise_filter').val();
          data.billable_search    = $('.billable_filter').val();
          data.colwise_filter_val = $('#colwise_filter_val').val();
          data.filter_by          = '<?php echo $_GET['filter_by'] ?>';
          data.menu_status_val    = '<?php echo ($status_condition != 0) ? $status_condition : ""  ?>';

          // data.column_ordering = column_ordering;
        },
        complete: function(res) {
          $('.task_table_th').css("opacity","1");
          $('select[name="alltask_length"]').val(res.responseJSON.rowperpage);
          Hide_LoadingImg();

          var filter_by = '<?php echo $_GET['filter_by'] ?>';
          if(filter_by != ''){
            $('.filter-data.for-reportdash').empty();
            var html  = '';
                html += '<div class="filter-head">';
                html += '<h4>FILTERED DATA</h4>';
                html += '<button class="btn btn-danger f-right clear-filtered" id="clear_container">clear</button>';
                html += '<div id="container2" class="panel-body box-container">';
                html += '<div class="btn btn-default box-item"><?php echo base64_decode($_GET['un']); ?><span class="remove" data-targetth=".assignto_TH" data-index="0">x</span></div></div>';
                html += '</div>';            
            $('.filter-data.for-reportdash').append(html);
            $('.filter-data.for-reportdash').css('display', 'block');
          }

          <?php if (isset($_SESSION['logout_pause'])) { ?>
            $('div .per_timerplay_<?php echo $_SESSION['timer_task_id']; ?> .time_pause1').trigger('click');
          <?php } ?>
        }
      },

      //"deferLoading":'10',

      "language": {
        "emptyTable": "No Task  assigned to you"
      },

      "dom": '<"toolbar-table" B>lfrtip',

      buttons: [{
        extend: 'collection',
        background: false,
        text: '<i class="fa fa-cog" aria-hidden="true"></i>',
        className: 'Settings_Button',
        buttons: [{
            text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
            className: 'Button_List',
            action: function(e, dt, node, config) {
              Trigger_To_Reset_Filter();
            }
          },
          {
            extend: 'colvis',
            text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
            columns: ':not(.Exc-colvis)',
            className: 'Button_List',
            prefixButtons: [{
              text: 'X',
              className: 'close'
            }]
          },
          DataTable_Task_Export_Buttons
        ]
      }],

      createdRow: function(row, data, dataIndex) {

        //alert($( row ).find('td:eq(0)').find('.key_val').val());

        // Set the data-status attribute, and add a class
        //var new_column_ordering = '';
        if ($(row).find('td:eq(0)').find('.key_val').val() !== undefined) {

          var new_column_ordering = $(row).find('td:eq(0)').find('.key_val').val().split(",");
        }
        //get_th()
        var ci = 0;

        $.each(new_column_ordering, function(idx, val) {
          row_vars["col" + val + "_obj"] = $(row).find('td:eq(' + idx + ')');
        });

        var tg_class_name = row_vars['col0_obj'].find('.first_td_class').val();
        var tg_tsk_id = row_vars['col0_obj'].find('.first_td_id').val();
        row_vars['col0_obj'].addClass(tg_class_name);
        row_vars['col0_obj'].attr('data-id', tg_tsk_id);

        row_vars['col0_obj'].parent('tr').attr({
          'class': 'count_section',
          "id": tg_tsk_id
        });

        $(row).find('td:eq(1)').addClass('select_row_TD');

        // th_attr = $( row ).find('td:eq(0)').find('name["th_attr"]').val()

        var col2_data_hour = row_vars['col2_obj'].find('.stopwatch').attr('data-hour');
        var col2_data_min = row_vars['col2_obj'].find('.stopwatch').attr('data-min');
        var col2_data_sec = row_vars['col2_obj'].find('.stopwatch').attr('data-sec');
        var col2_data_pauseon = row_vars['col2_obj'].find('.stopwatch').attr('data-pauseon');
        // row_vars['col2_obj'].addClass('timer_TD timer_class');
        //  row_vars['col2_obj'].attr('data-search',String(col2_data_hour).padStart(2, '0')+' : '+String(col2_data_min).padStart(2, '0')+' : '+ String(col2_data_sec).padStart(2, '0'));

        vars['hours_' + tg_tsk_id] = col2_data_hour;
        vars['minutes_' + tg_tsk_id] = col2_data_min;
        vars['seconds_' + tg_tsk_id] = col2_data_sec;
        vars['milliseconds_' + tg_tsk_id] = 0;
        vars['data_pause_' + tg_tsk_id] = col2_data_pauseon;

        $.each(new_column_ordering, function(idx, val) {

          if (idx != 0 && idx != 1 && idx != new_column_ordering.length - 1)
            row_vars["col" + val + "_obj"].attr({
              "data-search": row_vars["col" + val + "_obj"].find('input[name="th_attr"]').val(),
              "class": row_vars["col" + val + "_obj"].find('input[name="th_class"]').val(),
            });

          if (row_vars["col" + val + "_obj"].find('input[name="th_id"]').val() !== undefined) {
            row_vars["col" + val + "_obj"].attr({
              "id": row_vars["col" + val + "_obj"].find('input[name="th_id"]').val(),
            });
          }
        });
      },
      order: [], //ITS FOR DISABLE SORTING

      //colReorder: true,

      // colReorder: {
      //   realtime: false,
      //   // order:column_ordering,
      //   fixedColumnsLeft: 2,
      //   fixedColumnsRight: 1

      // },

      columnDefs: [
        // {"visible": false,"targets": hidden_coulmns },
        {
          "type": "date",
          "targets": ["duedate_TH", "intduedate_TH"]
          // "targets": ["duedate_TH"]
        },
        {
          "orderable": false,
          "targets": ['subtask_toggle_TH', 'select_row_TH', 'timer_TH', 'action_TH']
        },

      ],

      "iDisplayLength": 10,
      "drawCallback": function(settings) {

        $('#select_alltask').prop('checked', false);
        working_hours_timer();
        hide_th();
      },
    });

    //   TaskTable_Instance.fnClearTable().fnDestroy();
    // //  $('#alltask').dataTable().fnDestroy();
    //  TaskTable_Instance= $('#alltask').dataTable();
    // ColVis_Hide( TaskTable_Instance , hidden_coulmns );

    function get_th() {
      var unique_data = [];
      $('#colwise_filter_val').val("");

      TaskTable_Instance.columns().every(function() {

        $(this.header()).find('.dropdown-content li').each(function() {

          //console.log('check');

          if ($(this).find('input:checked').length) {
            var class_name = $(this).closest('th').attr('class').match(/\w+(?=)/);
            class_name = class_name[0];
            // var index = $('#alltask thead th').index($('.'+class_name));

            var dataSearch = $(this).attr('data-value') + '//' + class_name;

            if (jQuery.inArray(dataSearch, unique_data) === -1) {
              unique_data.push(dataSearch);
            }

            $('#colwise_filter_val').val(unique_data);
            //$(this).trigger('click');    
          }
        });
      });
      TaskTable_Instance.draw();
    }

    function hide_th() {
      // var hidden_coulmns = $.ajax({
      //   type: "GET",
      //   dataType: "json",
      //   url: "<?= base_url() ?>Task/get_hidden_value",
      //   async: false
      // }).responseJSON;

      // $.each(hidden_coulmns, function(i, v) {
      //   hidden_coulmns[i] = '.' + v;
      // });
      // // console.log(TaskTable_Instance);

      // TaskTable_Instance.columns(hidden_coulmns).visible(false, false);
    }

    Filter_IconWrap();

    Change_Sorting_Event(TaskTable_Instance);

    ColReorder_Backend_Update(TaskTable_Instance, 'task_list');

    // ColVis_Backend_Update(TaskTable_Instance, 'task_list');

    $('.filter-data.for-reportdash #clear_container').on('click', function() {
      Redraw_Table(TaskTable_Instance);
    });

    $(document).on('click', '.Settings_Button', AddClass_SettingPopup);
    $(document).on('click', '.Button_List', AddClass_SettingPopup);

    $(document).on('click', '.dt-button.close', function() {
      $('.dt-buttons').find('.dt-button-collection').detach();
      $('.dt-buttons').find('.dt-button-background').detach();
    });

    $(document).on('mousedown', 'th .themicond', function() {

      var TH = $(this).closest('th');
      var table = 'alltask';
      var column = TH.attr('data-column');

      if (!TH.find('select').length && last_filter_data_request != column) {
        if (column) {
          //console.log('chk');
          last_filter_data_request = column;

          $.ajax({
            url: "<?php echo base_url() ?>Test_slowness/new_test_ajax/",
            type: 'post',
            dataType: 'json',
            data: {
              "column": column
            },
            success: function(data) {
              console.log(data);
              var Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo(TH);

              $.each(data, function(i, v) {

                var d_Sel = "";
                Filter_Select.append('<option  value="' + v + '">' + v + '</option>');
              });

              make_column_filter(Filter_Select);
              TH.find('img.themicond').trigger('click');
            }
          });
        }
      } else {
        make_column_filter(TH.find('.filter_check'));
      }
    });

    function make_column_filter(Filter_Select) {
      var table = 'alltask';

      Filter_Select.on('change', function() {

        //add or remove thi select box value from filter section
        config_filter_section($(this));

        get_th();

      });

      Filter_Select.formSelect();
    }

    $(document).on('click', '.details-control', function() {

      //  alert("check");

      var tr = $(this).closest('tr');
      var row = TaskTable_Instance.row(tr);

      if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
      } else {
        var id = $(this).data('id');

        // alert(id);
        $.ajax({
          url: '<?php echo base_url(); ?>Sub_Task/sub_task_get',
          type: 'POST',
          data: {
            'id': id
          },
          beforeSend: function() {
            $(".LoadingImage").show();
          },
          success: function(data) {

            var parentTableThW = [];
            $('#alltask thead th').each(function() {
              parentTableThW.push($(this).outerWidth());
            });
            $(".dummy_content").html(data);
            $('.dummy_content').find('tr').each(function() {
              var i = 0;

              var sub_val = $(this).closest('.test_check').find('.subTaskcheckbox').data('alltask-id');

              vars['hours_' + sub_val] = $('#trhours_' + sub_val).val();
              vars['minutes_' + sub_val] = $('#trmin_' + sub_val).val();
              vars['seconds_' + sub_val] = $('#trsec_' + sub_val).val();
              vars['milliseconds_' + sub_val] = 0;
              vars['data_pause_' + sub_val] = $('#trpause_' + sub_val).val();

              $(this).find('td').each(function() {

                console.log(parentTableThW[i]);

                $(this).css({
                  width: parentTableThW[i]
                });
                i++;

              });
            });

            $(".LoadingImage").hide();
            row.child($(".dummy_content").html()).show();

            tr.addClass('shown');
            working_hours_timer(id);

            /* Custom code added on 16122020 */
            if($('#active_timer_id').val() != ''){
              var cur_timer_id = $('#active_timer_id').val();  
              $('.for_timer_start_pause').css('display', 'none');
              $('.per_timerplay_'+cur_timer_id).css('display', 'block');
            }else{
              $('.for_timer_start_pause').css('display', 'block');
            }
            /* /Custom code added on 16122020 */
          }
        });
        // Open this row
      }

    });

    var get_val = '';
    // var Status_sel = '<li><div class="toolbar-search"><select id="statuswise_filter" class="statuswise_filter" multiple="true" data-searchCol="status_TH" placeholder="Select Status">';
    // <?php foreach ($task_status as $key => $status_val) { ?>

    //    Status_sel += '<option value="<?php echo $status_val["id"] ?>">';
    //    get_val = "<?php echo $status_val["status_name"] ?>";
    //    Status_sel += get_val;
    //    Status_sel += '</option>';

    //  <?php } ?>

    //  Status_sel += '</select></div></li>';

    //  var Priority_sel = '<li><div class="toolbar-search"><select multiple="true" id="prioritywise_filter" class="prioritywise_filter" data-searchCol="priority_TH" placeholder="Select Priority"><option value="">By Priority</option><option value="low">Low</option><option value="medium">Medium</option><option value="high">High</option><option value="super_urgent">Super Urgent</option></select></div></li>';

     var Export_sel = '<li><div class="toolbar-search"><select id="export_report" class="export_report"><option value="">Export</option><option value="excel">Excel</option><option value="pdf">PDF</option><option value="html">HTML</option></select></div></li>';

     var Assignee_sel = '<li><div class="toolbar-search"><select multiple="true" data-searchCol="assignto_TH"  placeholder="Select Assignee" name="assignee" id="assignee" class="workers" ><?php if (is_array($staff_form)) { ?><option disabled>Staff</option><?php foreach ($staff_form as $s_key => $s_val) { ?><option value="<?php echo $s_val['crm_name']; ?>" ><?php echo $s_val['crm_name']; ?></option><?php }
                                                                                                                                                                                                                                                                                                                                                                                                            } ?></select></div></li>';

    //  var Billable_sel = '<li><div class="toolbar-search"><select multiple="true" data-searchCol="billable_TH" class="billable_filter" placeholder="Select Billable" > <option value="Billable">Billable</option><option value="Non Billable">Non Billable </option></select> </div> </li>';

    //  $("div.toolbar-table").prepend('<div class="filter-task1"><h2>Filter:</h2><ul>' + Status_sel + Priority_sel + Billable_sel + '</ul></div> ');

    /*this button for toggle settings*/
    /* $("div#table-buttons").prepend('<button class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cogs" aria-hidden="true"></i></button>');*/
    /*this button for toggle settings*/

    $('.toolbar-search').dropdown({
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });

    $(document).on('click', '.task_status_val', function() {

      var col = $.trim($(this).attr('data-searchCol'));
      var val = $.trim($(this).attr('data-id'));

      $('#for_status_id').val(val);

      $('#alltask_wrapper  div a.dropdown-clear-all').trigger('click');
      //console.log(col+'-'+val);

      //Trigger_To_Reset_Filter();

      MainStatus_Tab = val;

      TaskTable_Instance.column('.' + col).search(val, true, false).draw();
    });

    $('.toolbar-table select').on('change', function() {
      if ($(this).attr('id') != 'export_report') {
        var search = $(this).val();

        if (search.length) search = '^(' + search.join('|') + ')$';

        var col = $.trim($(this).attr('data-searchCol'));

        //console.log(col+"value"+val);
        TaskTable_Instance.column('.' + col).search(search, true, false).draw();
      }
    });

    function Trigger_To_Reset_Filter() {

      $('#select_alltask').prop('checked', false);
      $('#select_alltask').trigger('change');

      $('div.toolbar-table .toolbar-search li.dropdown-chose').each(function() {
        $(this).trigger('click');
      });

      MainStatus_Tab = "all_task";

      Redraw_Table(TaskTable_Instance);

      // Redraw_Table(TaskTable_Instance);

    }
  });
</script>

<script>
  $(document).ready(function() {

    $('#dropdown2').on('change', function() {
      // table.columns(5).search( this.value ).draw();
      var filterstatus = $(this).val();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>tasksummary/taskFilter",
        data: {
          filterstatus: filterstatus
        },
        success: function(response) {
          $(".all-usera1").html(response);
        },
        //async:false,
      });
    });
  });
</script>

<!-- <script>
    $('#checkbox-temp').change(function(){
    if(this.checked){
      alert("I am Checked");
    }
    else {
      alert("I am Not Checked");
    }
  });
</script> -->

<script>
  /*may be old required ment code*/

  $(document).on('change', '.test_status', function() {
    //$(".task_status").change(function(){
    var rec_id = $(this).data('id');
    var stat = $(this).val();
    $.ajax({
      url: '<?php echo base_url(); ?>user/test_statuschange/',
      type: 'post',
      data: {
        'rec_id': rec_id,
        'status': stat
      },
      timeout: 3000,
      success: function(data) {        
        $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() {
          $('#status_succ');
        });

        //   setTimeout(resetAll,3000);
        /* if(stat=='3'){                                  
            //$this.closest('td').next('td').html('Active');
            $('#frozen'+rec_id).html('Frozen');

        } else {
            $this.closest('td').next('td').html('Inactive');
        }*/
        
      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  });


  $(document).ready(function() {

    $(document).on('change', ".export_report", function() {
      //$("#prioritywise_filter").change(function(){
      var data = {};
      data['priority'] = $('.prioritywise_filter').val();
      data['status'] = $('.statuswise_filter').val();
      //data['status'] ='';
      // alert( data['status'] );
      data['d_type'] = $(this).val();
      file_download(data);
      return false;
    });

    function file_download(data) {
      var type = data.d_type;
      var status = data.status;
      var priority = data.priority;
      if (type == 'excel') {
        window.location.href = "<?php echo base_url() . 'user/task_excel?status="+status+"&priority="+priority+"' ?>";
      } else if (type == 'pdf') {
        window.location.href = "<?php echo base_url() . 'user/task_pdf?status="+status+"&priority="+priority+"' ?>";
      } else if (type == 'html') {
        window.open(
          "<?php echo base_url() . 'user/task_html?status="+status+"&priority="+priority+"' ?>",
          '_blank' // <- This is what makes it open in a new window.
        );
        // window.location.href="<?php echo base_url() . 'user/task_html?status="+status+"&priority="+priority+"' ?>";
      }
    }
  });


  $(document).on("click", ".assignuser_staff", function() {

    var id = $(this).attr("data-id");

    // $('.task_status_html').html('').html(reassign_status_poupup.format(id));
    // $('.adduserstatuspopup').modal('show');
    var data = {};
    $('.assign_staff_div').html('');

    data['task_id'] = id;
    data['assignuser_staff'] = $('#assignuser_staff_' + id).val();
    data['assignuser_staffid'] = $('#assignuser_staffid_' + id).val();
    data['type'] = $('#update_staffid_' + id).val();

    if ($('#assignuser_staff_' + id).val() != '' && data['type'] != '3') {
      $(".LoadingImage").show();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>user/assign_status_change/",
        data: data,
        success: function(response) {
          $('#assignuser_' + id + ' .close').trigger('click');

          $('#adduser_' + id + ' #assign_task_status_' + id).html('').html(response);
          $(".LoadingImage").hide();
        },
      });
    } else if (data['type'] == '3') {
      $(".LoadingImage").show();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>user/assign_status_change/",
        data: data,
        success: function(response) {
          $('#assignuser_' + id + ' .close').trigger('click');

          $('#adduser_' + id + ' #assign_task_status_' + id).html('').html(response);
          $(".LoadingImage").hide();
        },
      });
    } else {
      $('.assign_staff_div').html('Enter Status');
    }
  });

  $(document).on("click", ".assignuser_change", function(event) {
    $('.assign_staff_div').html('');

    var id = $(this).attr("data-id");

    $('.task_status_html').html('').html(reassign_status_poupup.format(id));
    $('.adduserstatuspopup').modal('show');

    $('#assignuser_staff_' + id).val('');
    $('#assignuser_staffid_' + id).val('');
    $('#update_staffid_' + id).val('');

    $('#assignuser_' + id + ' h4').text('Reason Status');
    $('.delete_st_reason').css('display', 'block');
    $('.delete_div_reason').css('display', 'none');
    $('#update_staffid_' + id).val('1');

  });

  $(document).on("click", ".edit_user_change", function(e) {
    $('.assign_staff_div').html('');

    var id = $(this).attr("data-id");
    $('.task_status_html').html('').html(reassign_status_poupup.format(id));
    $('.adduserstatuspopup').modal('show');
    var data = {};

    $('#assignuser_staff_' + id).val('');
    $('#assignuser_staffid_' + id).val('');
    $('#update_staffid_' + id).val('');

    var data_status = $('#assign_task_status_' + id).val();
    $('#assignuser_' + id + ' h4').text('Reason Status');
    $('.delete_st_reason').css('display', 'block');
    $('.delete_div_reason').css('display', 'none');
    var data_text = $('#assign_task_status_' + id + ' option:selected').text();

    if (data_status != '') {
      $('#assignuser_staff_' + id).val(data_text);
      $('#assignuser_staffid_' + id).val(data_status);
      $('#update_staffid_' + id).val('2');
    }
  });

  $(document).on("change", ".assign_task_status", function() {
    var id = $(this).attr('id');
    id = id.split('_');
    var val = $(this).val();
    if (val != "") {
      $('#edit_user_change_' + id[3]).css('display', 'block');
      $('#delete_user_change_' + id[3]).css('display', 'block');
      $(this).closest('.formconnew').addClass('add_edit_class');
    } else {
      $('#edit_user_change_' + id[3]).css('display', 'none');
      $('#delete_user_change_' + id[3]).css('display', 'none');
      $(this).closest('.formconnew').removeClass('add_edit_class');
    }
  });

  $(document).on("click", ".delete_user_change", function(e) {

    $('.assign_staff_div').html('');

    var id = $(this).attr("data-id");
    $('.task_status_html').html('').html(reassign_status_poupup.format(id));
    $('.adduserstatuspopup').modal('show');

    $('#assignuser_staffid_' + id).val('');
    $('#update_staffid_' + id).val('');

    var data = {};
    var data_status = $('#assign_task_status_' + id).val();
    // alert(data_status);
    $('.delete_st_reason').css('display', 'none');
    $('.delete_div_reason').css('display', 'block');
    $('#assignuser_' + id + ' h4').text('Do you Want Delete the status');
    var data_text = $('#assign_task_status_' + id + ' option:selected').text();

    if (data_status != '') {
      $('#assignuser_staffid_' + id).val(data_status);
      $('#update_staffid_' + id).val('3');
    }
  });

  $(document).on("click", ".save_assign_staff", function() {
    var id = $(this).attr("data-id");
    var data = {};
    var Assignees = [];
    // var countries =$("#workers"+id ).val();

    $('#adduser_' + id + ' .comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
      var id1 = $(this).closest('.comboTreeItemTitle').attr('data-id');
      var id2 = id1.split('_');
      Assignees.push(id2[0]);
    });
    //  data['assignee'] = Assignees

    var assign_role = Assignees.join(',');

    // alert(assign_role);
    data['task_id'] = id;
    data['assign_role'] = assign_role;
    data['assign_task_status'] = $('#assign_task_status_' + id).val();

    data['description'] = $('#description_' + id).val();

    // alert($('#description_'+id).val());

    // return;

    $(".LoadingImage").show();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>user/update_assignees/",
      data: data,
      success: function(response) {
        $(".LoadingImage").hide();
        //$('#task_'+id).html(response);
        $('.task_' + id).html(response);

        var label = $('.task_' + id).find('strong').contents().filter(function() {
          return this.nodeType == 3;
        }).text().trim();

        $('#adduser_' + id).modal('hide');

        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        var cell = $('.task_' + id).parent('td');
        cell.attr('data-search', label);

        $('.popup_info_msg').show();
        $(".popup_info_msg .position-alert1").html('Success !!! Staff Assign have been changed successfully...');
        setTimeout(function() {
          $('.popup_info_msg').hide();
        }, 1500);
      },
    });
  });

  $(function() {

    var hours = minutes = seconds = milliseconds = 0;
    var prev_hours = prev_minutes = prev_seconds = prev_milliseconds = undefined;
    var timeUpdate;

    // Start/Pause/Resume button onClick
    $("#start_pause_resume").button().click(function() {
      // Start button
      if ($(this).text() == "Start") { // check button label
        $(this).html("<span class='ui-button-text'>Pause</span>");
        updateTime(0, 0, 0, 0);
      }
      // Pause button
      else if ($(this).text() == "Pause") {
        clearInterval(timeUpdate);
        $(this).html("<span class='ui-button-text'>Resume</span>");
      }
      // Resume button    
      else if ($(this).text() == "Resume") {
        prev_hours = parseInt($("#hours").html());
        prev_minutes = parseInt($("#minutes").html());
        prev_seconds = parseInt($("#seconds").html());
        prev_milliseconds = parseInt($("#milliseconds").html());

        updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds);

        $(this).html("<span class='ui-button-text'>Pause</span>");
      }
    });

    // Reset button onClick subject
    $("#reset").button().click(function() {
      if (timeUpdate) clearInterval(timeUpdate);
      setStopwatch(0, 0, 0, 0);
      $("#start_pause_resume").html("<span class='ui-button-text'>Start</span>");
    });

    // Update time in stopwatch periodically - every 25ms
    function updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds) {
      var startTime = new Date(); // fetch current time

      timeUpdate = setInterval(function() {
        var timeElapsed = new Date().getTime() - startTime.getTime(); // calculate the time elapsed in milliseconds

        // calculate hours                
        hours = parseInt(timeElapsed / 1000 / 60 / 60) + prev_hours;

        // calculate minutes
        minutes = parseInt(timeElapsed / 1000 / 60) + prev_minutes;
        if (minutes > 60) minutes %= 60;

        // calculate seconds
        seconds = parseInt(timeElapsed / 1000) + prev_seconds;
        if (seconds > 60) seconds %= 60;

        // calculate milliseconds 
        milliseconds = timeElapsed + prev_milliseconds;
        if (milliseconds > 1000) milliseconds %= 1000;

        // set the stopwatch
        setStopwatch(hours, minutes, seconds, milliseconds);

      }, 25); // update time in stopwatch after every 25ms

    }

    // Set the time in stopwatch
    function setStopwatch(hours, minutes, seconds, milliseconds) {
      $("#hours").html(prependZero(hours, 2));
      $("#minutes").html(prependZero(minutes, 2));
      $("#seconds").html(prependZero(seconds, 2));
      $("#milliseconds").html(prependZero(milliseconds, 3));
    }

    // Prepend zeros to the digits in stopwatch
    function prependZero(time, length) {
      time = new String(time); // stringify time
      return new Array(Math.max(length - time.length + 1, 0)).join("0") + time;
    }
  });

  /*  function myFunction() {
     document.getElementById("myDropdown").classList.toggle("show");
    }
    */

  // Set the date we're counting down to
  var countDownDate = new Date("Sep 4, 2017 15:37:25").getTime();

  // Update the count down every 1 second
  var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    //alert(distance);
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    //document.getElementById("demo").innerHTML = days + "d " + hours + "h "
    var result = humanise(days) + hours + "h " +
      minutes + "m " + seconds + "s ";
    // console.log(result);
    $(".demos").html(result);

    // If the count down is over, write some text 
    if (distance < 0) {
      // alert('fff');
      //  clearInterval(x);
      $(".demos").html('EXPIRED');
      //document.getElementById("demo").innerHTML = "EXPIRED";
    }
  }, 1000);
</script>

<!--  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/> -->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.workout-timer.css" />
<!-- Example-page styles -->
<style>
  body {
    margin-bottom: 40px;
    background-color: #ECF0F1;
  }

  .event-log {
    width: 100%;
    margin: 20px 0;
  }

  .code-example {
    margin: 20px 0;
  }
</style>

<script>
  // var vars = {};
  //var time_count=0;

  function working_hours_timer(sub_task = null) {

    $('.stopwatch').each(function() {


      if (sub_task == null) {

        var element = $(this);
      } else {

        var subtask_id = $(this).closest('tr .test_check').find('.subTaskcheckbox').data('alltask-id');

        var element = $(this).closest('tr .subtask_check_' + sub_task + '_' + subtask_id).find('.stopwatch');
      }


      var running = element.data('autostart');
      var for_date = element.data('date');

      var task_id = element.data('id');

      var data_pause = element.data('pauseon');
      var data_pause = vars['data_pause_' + task_id];

      var hours = element.data('hour');
      var minutes = element.data('min');
      var seconds = element.data('sec');
      var milliseconds = element.data('mili');

      if (hours == '0') {
        var hours = vars['hours_' + task_id];
      }
      if (minutes == '0') {
        var minutes = vars['minutes_' + task_id];
      }
      if (seconds == '0') {
        var seconds = vars['seconds_' + task_id];

      }
      if (milliseconds == '0') {

        var milliseconds = vars['milliseconds_' + task_id];
      }


      element.closest('tr').find('td .hours-left').html('').html(parseInt(hours) + ':' + parseInt(minutes) + ' hours');


      var start = element.data('start');
      var end = element.data('end');

      var hoursElement = element.find('.hours');
      var minutesElement = element.find('.minutes');
      var secondsElement = element.find('.seconds');
      var millisecondsElement = element.find('.milliseconds');
      var toggleElement = element.find('.toggle');
      var resetElement = element.find('.reset');
      var pauseText = toggleElement.data('pausetext');
      var resumeText = toggleElement.data('resumetext');
      var startText = toggleElement.text();


      var timer;

      function prependZero(time, length) {


        time = '' + (time | 0);


        while (time.length < length) time = '0' + time;
        return time;

      }

      function setStopwatch(hours, minutes, seconds, milliseconds) {

        hoursElement.text(prependZero(hours, 2));
        minutesElement.text(prependZero(minutes, 2));
        secondsElement.text(prependZero(seconds, 2));
        millisecondsElement.text(prependZero(milliseconds, 3));
      }

      // Update time in stopwatch periodically - every 25ms
      function runTimer() {
        // Using ES5 Date.now() to get current timestamp            
        var startTime = Date.now();


        if (hours != 0 && minutes != 0 && seconds != 0 && milliseconds != 0) {
          var startTime = Date.now();
        } else {

          var startTime = Date.now();
        }


        // alert("before float"+milliseconds);

        var prevHours = parseFloat(hours);
        var prevMinutes = parseFloat(minutes);
        var prevSeconds = parseFloat(seconds);
        var prevMilliseconds = parseFloat(milliseconds);

        //alert("insidetimer"+prevMilliseconds);
        var tm = {};

        tm["hh_" + task_id] = tm["mm_" + task_id] = tm["ss_" + task_id] = tm["flag_" + task_id] = tm["flag1_" + task_id] = tm["flag2_" + task_id] = 00;
        //  tm["ss_" +task_id]= 60- prevSeconds;
        //console.log(tm["hh_" +task_id]);



        timer = setInterval(function() {

          var timeElapsed = Date.now() - startTime;



          if (tm["ss_" + task_id] == 60) {
            //  console.log('chk');
            tm["ss_" + task_id] = 00;

            if (tm["flag1_" + task_id] == 0) {
              tm["mm_" + task_id] = prevMinutes + 1;
              tm["flag1_" + task_id] = 1;
            } else {
              tm["mm_" + task_id] += 1;
            }

            // console.log(tm["mm" +task_id]);
            prevMinutes = 0;
            prevSeconds = 0;
          }

          if (tm["mm_" + task_id] == 60) {
            tm["mm_" + task_id] = 00;

            if (tm["flag2_" + task_id] == 0) {
              tm["hh_" + task_id] = prevHours + 1;
              tm["flag2_" + task_id] = 1;
            } else {
              tm["hh_" + task_id] += 1;
            }

            prevMinutes = 0;
            prevHours = 0;
          }

          hours = tm["hh_" + task_id] + prevHours;
          minutes = tm["mm_" + task_id] + prevMinutes;
          seconds = tm["ss_" + task_id] + prevSeconds;
          milliseconds = 0;          

          element.attr('data-hour', hours);
          element.attr('data-min', minutes);
          element.attr('data-sec', seconds);
          element.attr('data-mili', milliseconds);
          element.attr('data-pauseon', '');


          vars['hours_' + task_id] = hours;
          vars['minutes_' + task_id] = minutes;
          vars['seconds_' + task_id] = seconds;
          vars['milliseconds_' + task_id] = milliseconds;
          vars['data_pause_' + task_id] = '';




          element.closest('td').attr('data-search', parseInt(hours) + ':' + parseInt(minutes) + ':' + parseInt(seconds));

          setStopwatch(hours, minutes, seconds, milliseconds);

          if (tm["flag_" + task_id] == 0) {
            tm["ss_" + task_id] = prevSeconds;
            tm["flag_" + task_id] = 1;
            prevSeconds = 0;
          }

          tm["ss_" + task_id]++;
        }, 1000);
      }

      // Split out timer functions into functions.
      // Easier to read and write down responsibilities
      function run() {
        $(this).attr('pause', '');
        running = true;
        runTimer();
        if (start == '') {

        }
        toggleElement.text(pauseText);
        toggleElement.attr('data-current', pauseText);
      }

      function pause() {
        running = false;
        clearTimeout(timer);

        toggleElement.text(resumeText);
        toggleElement.attr('data-current', resumeText);
        element.attr('data-pauseon', 'on');
        vars['data_pause_' + task_id] = 'on';

        $.ajax({
          url: '<?php echo base_url(); ?>user/task_countdown_update/',
          type: 'post',
          data: {
            'task_id': task_id,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds,
            'milliseconds': milliseconds,
            'pause': 'on'
          },
          timeout: 3000,
          success: function(data) {
            
          }
        });

      }

      function reset() {
        running = false;
        pause();
        hours = minutes = seconds = milliseconds = 0;
        setStopwatch(hours, minutes, seconds, milliseconds);
        toggleElement.text(startText);
        toggleElement.attr('data-current', startText);
      }
      //  And button handlers merely call out the responsibilities
      toggleElement.on('click', function() {
        if (running) {
          //  alert("true");
          pause();
        } else {
          // alert("false");
          run();
        }
      });

      resetElement.on('click', function() {
        reset();
      });

      demo();

      function demo() {
        running = false;

        //  hours =74; minutes=37;seconds =53; milliseconds = 764;

        element.attr('data-hour', hours);
        element.attr('data-min', minutes);
        element.attr('data-sec', seconds);
        element.attr('data-mili', milliseconds);

        vars['hours_' + task_id] = hours;
        vars['minutes_' + task_id] = minutes;
        vars['seconds_' + task_id] = seconds;
        vars['milliseconds_' + task_id] = milliseconds;



        if (hours != 0 || minutes != 0 || seconds != 0) {
          if (data_pause == '') {

            run();
          }
        }

        setStopwatch(hours, minutes, seconds, milliseconds);

      }




    });


  }
  // Init timers
  $(document).ready(function() {

    $(document).on('click', '.paginate_button', function() {
      working_hours_timer();
    });

  });
  /***************************************************/


  /*****************************************************/
  $(document).on('click', '.workout-timer__play-pause', function() {
    //$('.workout-timer').workoutTimer();
    var id = $(this).attr("data-id");
    var txt = $("#counter_" + id).html();
    var data = {};
    data['id'] = id;
    data['time'] = txt;
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>user/update_timer/",
      data: data,
      success: function(response) {
        // $('#task_'+id).html(response);
      },
    });
    /* alert(id);
     alert(txt);*/
  });
</script>

<script>
  // Syntax highlighting
  $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });
</script>

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript">
  $('#alltask thead').on('click', '#overall_tasks', function(e) {
    if ($(this).is(':checked')) {
      $(".assign_delete").show();
    } else {
      $(".assign_delete").hide();
    }
    $('#alltask .overall_tasks').not(this).prop('checked', this.checked);
  });
</script>

<script type="text/javascript">
  function delete_task(del) {
    $("#delete_task_id").val(JSON.stringify([$(del).data('id')]));
  }

  function delete_action() {

    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'user/alltasks_delete'; ?>",
      data: {
        'id': $("#delete_task_id").val()
      },
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {
        $(".LoadingImage").hide();
        $(".popup_info_msg").show();
        $('.popup_info_msg .position-alert1').html('Success !!! Task Successfully Deleted..');
        setTimeout(function() {
          $(".popup_info_msg").hide();
          location.reload();
        }, 1500);
      }
    });
  }


  function billable_action() {

    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'Task/billable_action'; ?>",
      data: {
        'id': $("#billable_task_id").val()
      },
      beforeSend: function() {

        $(".LoadingImage").show();
      },
      success: function(data) {

        $(".LoadingImage").hide();

        $(".popup_info_msg").show();
        $('.popup_info_msg .position-alert1').html('Success !!! Task Successfully Update..');
        setTimeout(function() {
          $(".popup_info_msg").hide();
          location.reload();
        }, 1500);


      }
    });
  }







  $(document).ready(function() {
    //var table = $('#alltask').DataTable();           
    var tmp = [];
    $('#alltask tbody').on('click', '.overall_tasks', function(e) {
      $(".assign_delete").show();
      $(".overall_tasks").each(function() {
        if ($(this).is(':checked')) {

          var result = $(this).attr('id').split('_');
          var checked = result[2];
          tmp.push(checked);
        }
      });
      // alert(tmp);    

    });


    $(document).on('click', ".assign_tsk_btn", function() {
      //   alert('assign');
      var alltask = getSelectedRow();
      var Assignees = [];
      if (alltask.length <= 0) {
        $('.popup_info_msg').show();
        $(".popup_info_msg .position-alert1").html("Please Selecte Any Records..");
      } else {

        var selected_alltask_values = alltask.join(",");

        //alert('check');

        $('#assignTask-Popup .comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
          //  console.log('sss');
          var id1 = $(this).closest('.comboTreeItemTitle').attr('data-id');
          var id2 = id1.split('_');
          Assignees.push(id2[0]);
        });
        //  data['assignee'] = Assignees

        var assign_role = Assignees.join(',');
        // alert(assign_role);
        // return;

        var formData = {
          'task_id': selected_alltask_values,
          'assign_role': assign_role
        };
        // alert(selected_alltask_values);
        // return;

        $.ajax({
          type: "POST",
          url: "<?php echo base_url() . 'user/update_assignees1'; ?>",
          cache: false,
          data: formData,
          beforeSend: function() {
            $(".LoadingImage").show();
          },
          success: function(data) {
            $(".LoadingImage").hide();
            // var json = JSON.parse(data); 
            // status=json['status'];
            $(".popup_info_msg").show();
            $(".popup_info_msg .position-alert1").html(" Staff Assigned Successfully...");
            // return;
            setTimeout(function() {
              $(".popup_info_msg").hide();
              // location.reload();
            }, 3000);


          }
        });


      }
    });


    $(document).on('click', "#assign_member", function() {
      //   alert('assign');
      var alltask = getSelectedRow();
      if (alltask.length <= 0) {
        $('.popup_info_msg').show();
        $(".popup_info_msg .position-alert1").html("Please Selecte Any Records..");
      } else {

        var selected_alltask_values = alltask.join(",");


        $(".assigned_staff").click(function() {
          var assign_to = $(".workers_assign").val();

          var formData = {
            'task_id': selected_alltask_values,
            'staff_id': assign_to
          };
          // alert(assign_to);
          $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'user/alltasks_assign'; ?>",
            cache: false,
            data: formData,
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data) {
              $(".LoadingImage").hide();
              var json = JSON.parse(data);
              status = json['status'];
              $(".popup_info_msg").show();
              $(".popup_info_msg .position-alert1").html(" Staff Assigned Successfully...");
              if (status == '1') {
                setTimeout(function() {
                  $(".popup_info_msg").hide();
                  location.reload();
                }, 3000);
              }
            }
          });

        });

      }
    });


    $(document).on('click', "#saveSubtaskbutton", function() {

      $('.sub_task_error').html('');


      var name = $("#addSubTask-Popup").find("#subTaskName").val();

      if (name == '') {
        $('.sub_task_error').html('Enter Subject');
        return;
      } else {

        var selected = getSelectedRow();

        var ids = JSON.stringify(selected);

        $.ajax({
          url: "<?php echo base_url() ?>Task/addSubTask",
          type: "POST",
          data: {
            'name': name,
            'ids': ids
          },
          beforeSend: function() {
            $(".LoadingImage").show();
          },
          success: function(res) {
            $(".LoadingImage").hide();

            console.log(selected);

            $.each(selected, function(i, v) {
              var data1 = {};

              var tr = $("#" + v);
              var row = TaskTable_Instance.row(tr);

              var toggleElement = tr.find('.stopwatch .toggle');
              data1['status'] = '2';
              data1['rec_id'] = v;
              tr.find(".task_status").val('2');

              var all_qCodes = $.ajax({
                type: "POST",
                data: data1,
                dataType: "json",
                url: "<?= base_url() ?>user/task_statusChange",
                async: false
              }).responseJSON;

              if (toggleElement.hasClass('time_pause1')) {
                toggleElement.trigger('click');
                // data1['change_id']='1';
              }



              if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
              } else {
                tr.find("td:nth-child(1)").addClass("details-control");
              }

            });
            $("#addSubTask-Popup").modal('hide');
          }
        });

      }


    });

    $(document).on('change', ".subTaskcheckbox", function() {
      var con = $(this).is(":checked");
      var sel = $(this).closest('tr').find('select.task_status');
      var strikeout = $(this).closest('tr').find('td.subject_td');

      if (con) {
        sel.val('5');
        sel.trigger('change');
        strikeout.addClass('strikeout');
      } else {

        sel.val('2');

        sel.trigger('change');
        strikeout.removeClass('strikeout');

      }
    });



  });

  $(".overall_tasks").each(function() {
    if ($(this).is(':checked')) {

      var result = $(this).attr('id').split('_');
      var checked = result[2];
      tmp.push(checked);
    }
  });
</script>

<script>
   $(document).ready(function(){
    modifyNavBarStatus();
   }) 
   function modifyNavBarStatus(){
     var status = '';
    <?php
      if(isset($status) && ($status != '')){?>
        var status = "<?php echo $status; ?>";
    <?php }?>

    if(status === "4"){
      $("#task_active_wrap li a.active").removeClass("active").addClass("nav-link-tasks"); 
         $(".archive-task").addClass("active"); 
    }else if(status === "5"){
      $("#task_active_wrap li a.active").removeClass("active").addClass("nav-link-tasks"); 
         $(".completed-task").addClass("active"); 
    }
   }
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $(document).on('click', '#delete_task', function() {
      var alltask = getSelectedRow();
      $("#delete_task_id").val(JSON.stringify(alltask));
    });

    $(document).on('click', '.mark_billable_btn', function() {
      var alltask = getSelectedRow();
      $("#billable_task_id").val(JSON.stringify(alltask));
    });
  });

  $(document).ready(function() {
    var date = $('.datepicker').datepicker({
      dateFormat: 'dd-mm-yy'
    }).val();

    // Multiple swithces
    var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));

    elem.forEach(function(html) {
      var switchery = new Switchery(html, {
        color: '#1abc9c',
        jackColor: '#fff',
        size: 'small'
      });
    });

    $('#accordion_close').on('click', function() {
      $('#accordion').slideToggle(300);
      $(this).toggleClass('accordion_down');
    });

  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $("#country").change(function() {
      var country_id = $(this).val();
      //alert(country_id);

      $.ajax({
        url: "<?php echo base_url() . 'Client/state'; ?>",
        data: {
          "country_id": country_id
        },
        type: "POST",
        success: function(data) {
          //alert('hi');
          $("#state").append(data);
        }
      });
    });
    $("#state").change(function() {
      var state_id = $(this).val();
      //alert(country_id);

      $.ajax({
        url: "<?php echo base_url() . 'Client/city'; ?>",
        data: {
          "state_id": state_id
        },
        type: "POST",
        success: function(data) {
          //alert('hi');
          $("#city").append(data);
        }
      });
    });
  });

  $(document).ready(function() {

    <?php
    if (isset($_SESSION['firm_seen'])) {
      if ($_SESSION['firm_seen'] == '2') { ?>
        $("div.nots_task_2").trigger('click');
      <?php } ?>

      <?php
      if ($_SESSION['firm_seen'] == '1') { ?>
        $("div.nots_task_1").trigger('click');
      <?php } ?>

      <?php
      if ($_SESSION['firm_seen'] == '5') { ?>
        $("div.nots_task_5").trigger('click');
      <?php } ?>

      <?php
      if ($_SESSION['firm_seen'] == '4') { ?>
        $("div.nots_task_4").trigger('click');
      <?php } ?>

      <?php
      if ($_SESSION['firm_seen'] == 'low') { ?>
        $("#prioritywise_filter").val('low').trigger('change');
      <?php } ?>

      <?php
      if ($_SESSION['firm_seen'] == 'medium') { ?>
        $("#prioritywise_filter").val('medium').trigger('change');
      <?php } ?>

      <?php
      if ($_SESSION['firm_seen'] == 'high') { ?>
        $("#prioritywise_filter").val('high').trigger('change');
      <?php } ?>

      <?php
      if ($_SESSION['firm_seen'] == 'super_urgent') { ?>
        $("#prioritywise_filter").val('super_urgent').trigger('change');
      <?php } ?>

    <?php } ?>
  });
</script>

<!-- for customize permission style -->
<script type="text/javascript">
  $(document).on('change', '.task_status', function(e) {

    var toggleElement1 = $(this).closest('tr').find('.stopwatch');

    var rec_id = $(this).data('id');
    var data1 = {};
    var stat = $.trim($(this).val());

    if (stat == '5') {
      var sub_check = toggleElement1.attr('data-subid');
      // if( $(this).closest('tr').hasClass('shown')){
      //       $(this).closest('tr').find('.details-control').trigger('click');
      //    }
      if (sub_check != 0) {
        var res = sub_check.split(",");
        $.each(res, function(key, val) {

          var toggleElement = $('tr .subtask_check_' + rec_id + '_' + val).find('.stopwatch .toggle');

          if (toggleElement.hasClass('time_pause1')) {
            var url1 = "<?php echo base_url(); ?>user/task_timer_start_pause_individual";
            toggleElement.trigger('click');

          } else {
            var url1 = "";
          }
          //else{
          data1['task_id'] = val;
          data1['status'] = '5';
          var user_type = "<?php echo $_SESSION['user_type'] ?>";
          if (user_type == 'FA') {
            data1['change_id'] = 1;
          }
          var all_qCodes = $.ajax({
            type: "POST",
            data: data1,
            dataType: "json",
            url: url1,
            async: false
          }).responseJSON;
          // }
        });
      }
    }

    var toggleElement = $(this).closest('tr').find('.stopwatch .toggle');
    if (stat == '5' && toggleElement.hasClass('time_pause1')) {

      toggleElement.trigger('click');

    } else {
      data1['rec_id'] = rec_id;
      data1['status'] = stat;

      var that = $(this);
      if (stat == '') return;
      $.ajax({
        url: '<?php echo base_url(); ?>user/task_statusChange/',
        type: 'post',
        data: data1,

        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {

          //alert($(this).attr('data-id').val());

          //console.log($(this));

          $(".LoadingImage").hide();
          if (data == 2) {
            $(".popup_info_msg").show();
            $('.popup_info_msg .position-alert1').html('You are has been remove from this task successfully...');
            setTimeout(function() {
              $(".popup_info_msg").hide();
            }, 1500);
            location.reload();
          } else {

            $(".popup_info_msg").show();
            $('.popup_info_msg .position-alert1').html('Success !!! Task status have been changed successfully...');

            // var status_condition='<?php echo $status_condition ?>';
            // if(status_condition==0)
            // {
            $('.taskpagedashboard').load("<?php echo base_url(); ?>user/task_summary_data");

            var task_status_arr = '<?php echo json_encode($task_status) ?>';

            // $.each(JSON.parse(task_status_arr), function(item,i){
            //      console.log(i.id);
            //   });

            var idx = $.map(JSON.parse(task_status_arr), function(item, i) {
              if (item.id == stat)
                return item.status_name;
            })[0];
            //console.log(idx);

            //console.log(task_status_arr);

            var cell = that.closest('td');
            cell.attr('data-search', idx);
            // }

            //  TaskTable_Instance.cell( cell ).invalidate().draw();

            setTimeout(function() {
              $(".popup_info_msg").hide();
            }, 1500);

            /* Add or Remove Archive / completed Link in menu */
            var recent_array = [];

            $(".count_section").find("td .task_status").each(function() {
              recent_array.push($(this).val());
            })
          }
        },
        error: function(errorThrown) {
          console.log(errorThrown);
        }
      });
    }
  });

  $(document).on('change', '.progress_status', function(e) {

    var rec_id = $(this).data('id');
    var data1 = {};
    var stat = $.trim($(this).val());
    var state_text = (stat != '') ? $(this).find("option:selected").text() : '-';

    data1['rec_id'] = rec_id;
    data1['status'] = stat;

    var that = $(this);
    if (stat == '') return;
    $.ajax({
      url: '<?php echo base_url(); ?>user/progress_statusChange/',
      type: 'post',
      data: data1,

      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {
        $(".LoadingImage").hide();
        $(".popup_info_msg").show();
        $('.popup_info_msg .position-alert1').html('Success !!! Process Task status have been changed successfully...');
        // $('.taskpagedashboard').load("<?php echo base_url(); ?>user/task_summary_data");

        var cell = that.closest('td');
        cell.attr('data-search', state_text);
        setTimeout(function() {
          $(".popup_info_msg").hide();
        }, 1500);
      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  });




  $(document).on('change', '.task_priority', function() {

    var rec_id = $(this).data('id');
    var priority = $.trim($(this).val());
    var that = $(this);
    if (priority == '') return;
    $.ajax({
      url: '<?php echo base_url(); ?>user/task_priorityChange/',
      type: 'post',
      data: {
        'rec_id': rec_id,
        'priority': priority
      },
      timeout: 3000,
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {

        $(".LoadingImage").hide();
        $(".popup_info_msg").show();
        $('.popup_info_msg .position-alert1').html('Success !!! Task Priority have been changed successfully...');
        setTimeout(function() {
          $(".popup_info_msg").hide();
        }, 1500);

        var cell = that.closest('td');
        cell.attr('data-search', priority);
        // TaskTable_Instance.cell( cell ).invalidate().draw();

      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  });

  $('.suspent').click(function() {
    if ($(this).is(':checked'))
      var stat = '1';
    else
      var stat = '0';
    var rec_id = $(this).val();
    var $this = $(this);
    $.ajax({
      url: '<?php echo base_url(); ?>user/suspentChange/',
      type: 'post',
      data: {
        'rec_id': rec_id,
        'status': stat
      },
      success: function(data) {
        //alert('ggg');
        $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
        if (stat == '1') {
          $this.closest('td').next('td').html('Payment');
        } else {
          $this.closest('td').next('td').html('Non payment');
        }
      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  });

  $('#alluser').on('change', '.status', function() {
    //e.preventDefault();  
    var rec_id = $(this).data('id');
    var stat = $(this).val();
    $.ajax({
      url: '<?php echo base_url(); ?>user/statusChange/',
      type: 'post',
      data: {
        'rec_id': rec_id,
        'status': stat
      },
      timeout: 3000,
      success: function(data) {
        //alert('ggg');
        $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() {
          $('#status_succ');
        });
        // setTimeout(resetAll,3000);
        if (stat == '3') {
          //$this.closest('td').next('td').html('Active');
          $('#frozen' + rec_id).html('Frozen');
        } else {
          $this.closest('td').next('td').html('Inactive');
        }
      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  });


  $(document).on("change", "#select_alltask", function(event) {

    var table = $(this).attr("id").replace("select_", "");
    var checked = this.checked;
    var count = 0;
    var task_id = 0;
    var user_type = "<?php echo $_SESSION['user_type'] ?>";
    var archive_tab = '<?php echo ($status_condition != 0) ? $status_condition : "0"  ?>';

    $(".LoadingImage").show();

    TaskTable_Instance.column(1, {
        search: 'applied'
      })
      .nodes()
      .to$().each(function() {

        $(this).find('.alltask_checkbox').prop('checked', checked);

        console.log(MainStatus_Tab);

        if (checked) {
          $(this).find('.alltask_checkbox').prop('checked', true);
          //   console.log($(".billable_filter").val());
          var checkboxTask = document.getElementById('select_alltask');
          var element = document.querySelector(".scroll_table");
          element.classList.add("scroll_table_btn");

          if (archive_tab == "4") $("#unarchive_task").show();
          else $('#archive_task').show();
          if ($(".billable_filter").val() == "Non Billable") $(".mark_billable_btn").show();
          else $(".mark_billable_btn").hide();
          $("#assign_member").show();
          // $("#btn_review_send").show();

          $("#delete_task").show();
          $("#addSubtask-Btn").show();
          $('.task_assign_btn').show();

        } else {
          var checkboxTask = document.getElementById('select_alltask');
          var element = document.querySelector(".scroll_table");
          element.classList.remove("scroll_table_btn");

          $(this).find('.alltask_checkbox').prop('checked', false);
          $(".mark_billable_btn").hide();
          $('#unarchive_task').hide();
          $("#archive_task").hide();
          $("#addSubtask-Btn").hide();
          $('.task_assign_btn').hide();
          $("#assign_member").hide();
          $("#delete_task").hide();
          // $("#btn_review_send").hide();       
        }
      });
    $(".LoadingImage").hide();
    //$("#select_"+table+"_count").val($("input."+table+"_checkbox:checked").length + " Selected");
  });

  $(document).on("change", ".alltask_checkbox", function(event) {
    var count = 0;
    var tr = 0;
    var ck_tr = 0;
    var task_id = 0;
    var user_type = "<?php echo $_SESSION['user_type'] ?>";
    var archive_tab = '<?php echo ($status_condition != 0) ? $status_condition : "0"  ?>';

    console.log(archive_tab);
    TaskTable_Instance.column(1).nodes().to$().each(function(index) {

      if ($(this).find(".alltask_checkbox").is(":checked")) {
        var checkbox = document.getElementById('checkbox-temp');
        if(checkbox.checked  === false){
          var element = document.querySelector(".scroll_table");
          element.classList.add("scroll_table_btn");
        }
          // var element = document.querySelector(".scroll_table");
          // console.log(element,"element")
          // element.classList.remove("scroll_table_btn");
        ck_tr++;

        var status_val = '';
        var table_id = $(this).closest('tr');
        status_val = table_id.find(".task_status").val();

      }      
      var checkbox = $('.alltask_checkbox').is(':checked');
      if(checkbox  === true){
        $(".scroll_table").addClass("scroll_table_btn");
      } else {
        $(".scroll_table").removeClass("scroll_table_btn");
      }

      tr++;

    });
    //  alert("re"+$("#alltask").attr("data-status")+ck_tr);
    if (tr == ck_tr) {
      $("#select_alltask").prop("checked", true);
    } else {
      $("#select_alltask").prop("checked", false);
    }
    if (ck_tr) {
      // alert(user_type);
      if (archive_tab == "4") $("#unarchive_task").show();
      else $('#archive_task').show();
      $("#assign_member").show();
      $("#addSubtask-Btn").show();
      $('.task_assign_btn').show();
      $("#delete_task").show();
      if ($(".billable_filter").val() == "Non Billable") $(".mark_billable_btn").show();
      else $(".mark_billable_btn").hide();
      // if(count==0 && user_type!='FA'){
      //   // alert('hai');
      //  $("#btn_review_send").show();
      // }
      // else
      // {
      //   $("#btn_review_send").hide();
      // }
    } else {
      $('#unarchive_task').hide();
      $("#archive_task").hide();
      $("#assign_member").hide();
      $("#addSubtask-Btn").hide();
      $('.task_assign_btn').hide();
      $("#delete_task").hide();
      $(".mark_billable_btn").hide();
      //$("#btn_review_send").hide();
    }

    $("#select_alltask_count").val($("input.alltask_checkbox:checked").length + " Selected");

  });

  $("#close_action_result").click(function() {
    $(".popup_info_msg").hide();
  }); //for close mssage box
  $("#close_info_msg").click(function() {
    $(".popup_info_msg").hide();
  });
</script>

<!-- <script type="text/javascript">
  const getClass = () => {
    var checkbox = document.getElementById('checkbox-temp');
    console.log(checkbox,"checkbox");
    console.log(checkbox.checked,"reault")
    if(checkbox.checked){
      console.log("inside ifff")
      var element = document.querySelector(".scroll_table");
      console.log(element,"element")
      element.classList.add("scroll_table_btn");
    }
  }
</script> -->

<script type="text/javascript">
  $(document).ready(function() {   

    var fruits = [];

    $('.data_active_task th').each(function() {
      fruits.push($(this).outerWidth());
      //console.log(fruits);
    });


    /*$(document).on("click",".controls .for_timer_start_pause",function() {
       $(this).toggleClass('time_pause1');*/

    //  if($(this).hasClass('time_pause1'))
    //  {
    //  $('.for_timer_start_pause').hide();
    //   $(this).show();
    // }


    /* console.log('chk');
     });*/

      
  });
</script>

<script src="<?php echo base_url(); ?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url(); ?>assets/tree_select/icontains.js"></script>

<script type="text/javascript">
  //string format function -- use 'hello {0}'.format('demo')  -> result : 'hello demo'
  String.prototype.format = String.prototype.f = function() {
    var s = this,
      i = arguments.length;

    console.log(arguments);

    while (i--) {
      s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
  };

  $(document).on("click", ".individual_timer_btn", function(event) {
    event.preventDefault();

    var id = $(this).attr('data-id');

    $.ajax({
      url: "<?php echo base_url() ?>Task/get_individual_timer",
      type: "POST",
      data: {
        'task_id': id
      },
      dataType: 'json',
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(res) {
        $('#individual_timer_popup').find('modal-body').html('');
        if (res != 0) {
          $('#individual_timer_popup').modal('show');
          $('#individual_timer_popup').find('.modal-body').html(res.holdproduct);
        }
        $(".LoadingImage").hide();
      }
    });

  });

  $(document).on("click", ".user_change", function(event) {
    
    var id = $(this).data('target');
    id = id.split('_');
    id = id[1];
    var main_id = id;
    //alert(main_id);
    var task_name = 'TASK';
    var Assignees = [];

    // $('.adduserpopup').modal('show');

    var user_popup = $('.adduserpopup');
    // user_popup.modal('hide');

    //  $('.modal-backdrop .fade .in').removeClass('modal-backdrop fade in');
    // $( /\{0}/g ).replaceAll( main_id );

    if (id != '') {
      //user_popup.modal('show');
      user_popup.attr('id', 'adduser_' + main_id);

      $('#adduser_' + main_id).modal('show');

      $('#adduser_' + main_id).appendTo("body");

      user_popup.find('.modal-body select').attr('id', 'assign_task_status_' + main_id);
      user_popup.find('.modal-body select').val("");
      user_popup.find('.modal-body textarea').attr('id', 'description_' + main_id);
      user_popup.find('.modal-body textarea').val("");
      user_popup.find('.modal-footer .save_assign_staff').attr('data-id', main_id);
      user_popup.find('.modal-body select').val("");

      var popup_anchor_tag = ' <a href="javascript:;" id="" data-id="{0}"  class="assignuser_change "><i class="fa fa-plus"></i></a><a style="display: none" href="javascript:;" id="edit_user_change_{0}" data-id="{0}"  class="edit_user_change "><i class="fa fa-pencil"></i></a><a style="display: none" href="javascript:;" id="delete_user_change_{0}" data-id="{0}"  class="delete_user_change "><i class="fa fa-trash"></i></a>';
      $('.atagsection').html('').html(popup_anchor_tag.format(main_id));

      $.ajax({
        url: "<?php echo base_url() ?>User/get_assigness",
        type: "POST",
        data: {
          "id": id,
          "task_name": task_name
        },
        dataType: "json",
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          console.log(data.assign_check);
          if (data.assign_check == 2) {
            $(".popup_info_msg").show();
            $('.popup_info_msg .position-alert1').html('You are has been remove from this task successfully...');
            setTimeout(function() {
              $(".popup_info_msg").hide();
            }, 1500);
            location.reload();
          } else if (data.assign_check == 1) {
            $(".LoadingImage").hide();

            //alert("check1");
            //$('#adduser_'+main_id+' .comboTreeItemTitle').click(function(){ 
            $(document).on("click", '#adduser_' + main_id + ' .comboTreeItemTitle', function(event) {
              Assignees = [];

              var id = $(this).attr('data-id');
              var id = id.split('_');

              $('#adduser_' + main_id + ' .comboTreeItemTitle').each(function() {
                var id1 = $(this).attr('data-id');
                var id1 = id1.split('_');
                console.log(id[1] + "==" + id1[1]);
                if (id[1] == id1[1]) {
                  $(this).toggleClass('disabled');
                }
              });
              $(this).removeClass('disabled');

              $('#adduser_' + main_id + ' .comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
                var id = $(this).closest('#adduser_' + main_id + ' .comboTreeItemTitle').attr('data-id');
                var id = id.split('_');
                Assignees.push(id[0]);
              });

              var assign_role = Assignees.join(',');
              $('#adduser_txt_' + main_id).val(assign_role);

            });

            var arr1 = data.asssign_group;
            var assign_check = data.assign_check;
            //alert(assign_check);
            var unique = arr1.filter(function(itm, i, arr1) {
              return i == arr1.indexOf(itm);
            });

            $('#adduser_' + main_id + ' .comboTreeItemTitle').each(function() {

              $(this).find("input:checked").trigger('click');

              var id1 = $(this).attr('data-id');
              var id = id1.split('_');
              if (jQuery.inArray(id[0], unique) !== -1) {
                $(this).find("input").trigger('click');
                // if(assign_check==0)
                //  {
                //     $(this).toggleClass('disabled');
                //  }
                Assignees.push(id[0]);
              }

            });

            var assign_role = Assignees.join(',');
            $('#adduser_txt_' + main_id).val(assign_role);
            //alert(Assignees);
          }
        }
      });
    }
  });

  function prependZero(time, length) {
    time = new String(time); // stringify time
    return new Array(Math.max(length - time.length + 1, 0)).join("0") + time;
  }
  setInterval(function() {
    $.ajax({
      url: '<?php echo base_url(); ?>user/main_task_timer/',
      type: 'post',
      dataType: "json",
      success: function(data) {
        //  var result= $.parseJSON(data);
        //  alert('updated');
        let time_val = '';
        let element = '';
        let hoursElement = '';
        let secondsElement = '';
        let millisecondsElement = '';
        $.each(data, function(i, v) {

          time_val = v.split(',');
          // console.log(time_val[3]);

          element = $('#' + time_val[3]).find('.stopwatch');
          //  console.log(element.data('subid'));

          if (element.data('subid') != 0) {

            element.attr('data-hour', time_val[0]);
            element.attr('data-min', time_val[1]);
            element.attr('data-sec', time_val[2]);

            element.attr('data-pauseon', '');

            hoursElement = element.find('.hours');
            minutesElement = element.find('.minutes');
            secondsElement = element.find('.seconds');
            millisecondsElement = element.find('.milliseconds');

            hoursElement.text(prependZero(time_val[0], 2));
            minutesElement.text(prependZero(time_val[1], 2));
            secondsElement.text(prependZero(time_val[2], 2));
            millisecondsElement.text(prependZero(milliseconds, 3));

            element.closest('td').attr('data-search', prependZero(time_val[0], 2) + " : " + prependZero(time_val[1], 2) + " : " + prependZero(time_val[2], 2));

          }
        });
        //console.log(result);
      }
    });
  //}, 300000);
}, 600000);


  $(document).on('click', ".missing_info, .misclose", function() {
    console.log('ok');
    $(this).closest('tr').find('.missing_details').toggleClass('show');
  });
  $(document).mouseup(function(e) {
    var container = $(".missing_details.show");
    // if the target of the click isn't the container nor a descendant of the container
    if (container.length && !container.is(e.target) && container.has(e.target).length === 0) {
      $('.missing_details').removeClass('show');
    }
  });

  $('.close_popup').click(function() {

    //  console.log('check');
    var id = $(this).parents('.adduserpopup').attr('id');
    // console.log(id);
    //  return;
    id = id.split('_');
    $('#adduser_' + id[1]).modal('hide');

    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
    //alert("HHHH");

  });
</script>

<script>
$(document).on('click', ".for_timer_start_pause", function() {
  var trid    = $(this).attr('id'),
      time    = $('.timer_'+trid+' > span').text();
      trhours = parseInt(time.substring(0, 2)),
      trmin   = parseInt(time.substring(2, 4)),
      trsec   = parseInt(time.substring(4, 6));
 
  if($('#active_timer_id').val() == ''){
    //Play
    $('#active_timer_id').val(trid);
    $('.for_timer_start_pause').css('display', 'none');
    $('#per_timerplay_'+trid).css('display', 'block');

    var timerVal = setInterval(function () {    
      if($('#active_timer_id').val() != ''){
        $('#headertask_timer > .hours').text($('.timer_'+trid+' > .hours').text());
        $('#headertask_timer > .minutes').text($('.timer_'+trid+' > .minutes').text());
        $('#headertask_timer > .seconds').text($('.timer_'+trid+' > .seconds').text());
        var parent_subject = $('.timer_'+trid).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().find('.count_section > .subject_TD > input').val();
            task_subject   = $('.timer_'+trid).parent().parent().parent().find('.subject_TD > p > a').html();
        $('#headertask_timer_msg').text(parent_subject+' : '+task_subject);
      }else{
        clearInterval(timerVal);
      }
    },100);
  }else{
    //Pause
    $('#active_timer_id').val('');
    $('.for_timer_start_pause').css('display', 'block');
  }
});

$('body').on('click', '.clear-filtered', function() {
  window.location.href = "/user/task_list";
});

$('body').on('click', '.mark_as_read', function() {
  var task_id = $(this).attr('data-id');
  $(this).parent().parent().remove();
  $.ajax({
    url: '<?php echo base_url(); ?>task/has_viewed',
    type: 'POST',
    data: {
      'task_id': task_id
    },
    success: function(data) {
      alert("Marked as read");            
    }
  });
});

</script>

<script>
      $('#alltask').on( 'processing.dt', function ( e, settings, processing ) {
        var lgt = $("#container2 div").length;
        if ( lgt >= 1){
          $(".scroll_table").addClass("scroll_table_filter");
        }else{
          $(".scroll_table").removeClass("scroll_table_filter");
        }
      } );

      $( ".remove, .filter-clear" ).click(function(e) {
        var lgt = $("#container2 div").length;
        if ( lgt >= 1){
          $(".scroll_table").addClass("scroll_table_filter");
        }else{
          $(".scroll_table").removeClass("scroll_table_filter");
        }
      });

      $('#alltask').on( 'processing.dt', function ( e, settings, processing ) {
        if($(".scroll_table.scroll_table_btn").hasClass("scroll_table_filter")){
          $(".scroll_table").addClass("scroll_table_filter_data");
        }else if($(".scroll_table.scroll_table_filter").hasClass("scroll_table_btn")){
          $(".scroll_table").addClass("scroll_table_filter_data");
        }else{
          $(".scroll_table").removeClass("scroll_table_filter_data");
        }
      });
</script>

<script type="text/javascript">
   $("body").on("mouseover",".msg-wrapper",function(){
        var hT = $(this).offset().top,
            hH = $(this).outerHeight(),
            h_c = $(this).find('.msg-content').outerHeight(),
            wH = $(window).height();
        if ($(window).scrollTop() < hT + hH - wH + h_c) {
            $(this).find('.msg-content').addClass("upper");
        }
      });
      $("body").on("mouseleave",".msg-wrapper", function(){
        $(this).find('.msg-content').removeClass("upper");
      })
</script> 

<script type="text/javascript" src="<?php echo base_url()?>assets/js/custom/search_scroll.js"></script>
<?php $this->load->view('tasks/import_task'); ?>
<script src="<?php echo base_url();?>assets/js/custom/internal_deadlines.js?<?=time()?>" type="text/javascript"></script>


<script>
  $(document).on('click', '.phone-class', function() {
    var mobile = $(this).attr('data-mobile');
    var phone = $(this).attr('data-phone');
    var landline = $(this).attr('data-landline');
    if(!mobile){
      mobile = phone;
    }
    if(!mobile){
      mobile = landline;
    }
    var email = $(this).attr('data-email');
    var contact_id = $(this).attr('data-contact_id');
    var client_id = $(this).attr('data-client_id');
    var task_id = $(this).attr('data-task-id');
    $('.single_mail').each(function(index, value) {
      if($(this).attr('id') != 'single_mail'){
        $(this).remove();
      }
    });
    $('#crm-tel').val(mobile);
    $('#myBdayEmailCc').val(email);
    $("#recivers_id").val(contact_id);
    $("#data_task_id").val(task_id);
    $("#data_client_id").val(client_id);
  });

  $("input[name='result']").click(function(){
    if($('input:radio[name=result]:checked').val() == "yes"){
      $.ajax({
				url: "<?php echo base_url() ?>user/get_email_temp",
				type: "POST",
				data: {
					'id': 160
				},
				success: function(res) {
					var da = JSON.parse(res);
					console.log(da);
					// $("#notification_sub").html(da['subject']);
					//$("#notification_msg").html(); 
					//CKEDITOR.instances.notification_msg.setData(da['body']);
					tinymce.get("bday_notification_msg").setContent(da['body']);
				}
			});
    }
    else{
      $.ajax({
				url: "<?php echo base_url() ?>user/get_email_temp",
				type: "POST",
				data: {
					'id': 159
				},
				success: function(res) {
					var da = JSON.parse(res);
					console.log(da);
					// $("#notification_sub").html(da['subject']);
					//$("#notification_msg").html(); 
					//CKEDITOR.instances.notification_msg.setData(da['body']);
					tinymce.get("bday_notification_msg").setContent(da['body']);
				}
			});
    }
  });

  $('#send_mail_form').validate({
			ignore: false,
			errorPlacement: function(error, element) {
				if ($(element).attr('name') == 'mail_ids') {
					$("#mail_text_error").html(error.prop('outerHTML'));
				} else {
					error.insertAfter(element);
				}
			},
			rules: {
				mail_ids: {
					required: true
				},
				sub: {
					required: true
				}
			},
			messages: {
				mail_ids: {
					required: "Mail Id Required"
				}
			},
			submitHandler: function(form) {
				var msg = tinyMCE.get('bday_notification_msg').getContent(); //CKEDITOR.instances.notification_msg.getData();
				var all_mails = [];
				$('.myBdayEmailCc').each(function(index, value) {
          if($(this).val()){
            all_mails.push($(this).val());
          }
        });
				var id = $("#recivers_id").val();
        if(msg){
          $.ajax({
            url: "<?php echo base_url() ?>user/send_mail_sms",
            type: "POST",
            data: {
              "sub": 'Information Chaser',
              "msg": msg,
              "mail_ids": id,
              'all_mails' : all_mails,
              "status": "send"
            },
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(response) {
              $(".LoadingImage").hide();
              $('.info_popup').show();
              $('.info_popup .position-alert1').html('Mail Send Successfully..');
            }
          });
        }
			}
		});

    $(document).on('click', '#save_to_timeline', function() {
      var msg = tinyMCE.get('bday_notification_msg').getContent();
      var task_id = $("#data_task_id").val();
      var client_id = $("#data_client_id").val();
      $.ajax({
        url: "<?php echo base_url() ?>user/save_info_chaser_to_timeline",
        type: "POST",
        data: {
          "msg": 'Information Chaser: '+msg,
          "task_id": task_id,
          "client_id": client_id
        },
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(response) {
          $(".LoadingImage").hide();
          $('.info_popup').show();
          $('.info_popup .position-alert1').html('Successfully saved in timeline..');
        }
      });
    });

    $(document).on('click', '#bdayBtnCc', function() {
      $('#send_emails').append('<div class="single_mail"><input type="email" name="email" class="myBdayEmailCc" id="" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"></div>');
    });
</script>