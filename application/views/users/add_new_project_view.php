<?php $this->load->view('includes/header');?>



<section class="client-details-view various-section03 floating_set">
	<div class="client-inform-data1 floating_set">
		<h2><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add New Project</h2>
		
		<div class="proposal-data1 floating_set">

		<form>	
		<div class="floating_set data-float01">	
			<div class="col-sm-6 proposal-form-left">
				<div class="customer-edit01">
				<div class="form-group">
					<label>* Project Name</label>
					<input type="text" name="name">
				</div>
				
				<div class="form-group">
					<label>* Customer</label>
					<select class="selectpicker" data-live-search="true" >
					<option>USD</option>
					<option>EUR </option>
					</select>
				</div>	

				<div class="form-group progress-range-project">
					<!-- <div class="calculate-range">
							<input type="checkbox" name="checkbox">
							<label>Calculate progress through tasks</label>
					</div> -->

					<div class="calculate-range checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="Language" value="HTML">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Calculate progress through tasks</span>
					</label>
					</div>
					
					<div class="slidecontainer">
					<p>progress: <span id="demo"></span>%</p>
					<input type="range" min="1" max="100" value="50" class="slider" id="myRange">
					</div>	
				</div>		


				
				<div class="full-calendar-time1">

				<div class="form-group">
					<label>* Billing Type</label>
					<select class="selectpicker1" >
					<option>Project Hour</option>
					<option>Fixed Rate </option>
					</select>
				</div>	

				<div class="form-group">
					<label>Status</label>
					<select class="selectpicker1" >
					<option>In Progress</option>
					<option>Not Started </option>
					</select>
				</div>	
			</div>

				<div class="form-group">
					<label>Rate Per Hour</label>
					<input type="number" name="hour">
				</div>	

				<div class="full-calendar-time1">

					<div class="form-group">
					<label>Estimated Hours</label>
					<input type="number" name="hour">
				</div>	

				<div class="form-group">
					<label>Members</label>
					<select class="selectpicker" data-live-search="true" >
					<option>Admin test</option>
					<option>admin</option>
					</select>
				</div>	

				<div class="form-group  date_birth">
				<label>* Start Date</label>
				<div class="form-birth05">
				<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
				<input class="form-control datepicker fields" type="text" name="date_of_creation" id="date_of_creation1" placeholder="Incorporation date" value=""/>
				</div>
				</div> 
					<div class="form-group  date_birth">
				<label>Deadline</label>
				<div class="form-birth05">
				<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
				<input class="form-control datepicker fields" type="text" name="date_of_creation" id="date_of_creation2" placeholder="Incorporation date" value=""/>
				</div>
				</div> 

			


				</div>	



				<div class="tags-allow1">
					<h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
					<input type="text" value="" class="tags" />
				</div>	

				<div class="form-group">
					<label>Description</label>
					<textarea id="editor4"></textarea>
				</div>	

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="Language" value="HTML">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Send project created email</span>
					</label>
					</div>
				</div>
			</div>	<!-- col-sm-6 -->	 

			<div class="col-sm-6 proposal-form-right">
					
					<div class="project-setting02">
						<h2>Project settings</h2>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="Language" value="HTML">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view tasks</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="Language" value="HTML">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view tasks</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="Language" value="HTML" checked="">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to edit tasks (only tasks created from contact)</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="Language" value="HTML">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view task comments</span>
					</label>
					</div>

			</div>		



		</div>
		</div>	


		<div class="form-submit-bt01 text-right">
				<button type="button" name="button">Save & Send</button>
				<button type="submit" name="button">Save</button>
		</div>	






				</form>
				</div>
					
					
			
	
</div>
</section>			


<?php $this->load->view('includes/footer');?>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
 <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>


<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
	$('.tags').tagsinput({
    	allowDuplicates: true
    });
    
    $('.tags').on('itemAdded', function(item, tag) {
        $('.items').html('');
		var tags = $('.tags').tagsinput('items');
      
    });


    $( function() {
    $('#date_of_creation1,#date_of_creation2').datepicker();
  } );

		var slider = document.getElementById("myRange");
		var output = document.getElementById("demo");
		output.innerHTML = slider.value;

		slider.oninput = function() {
		output.innerHTML = this.value;
		}

		CKEDITOR.replace('editor4');

});
</script>