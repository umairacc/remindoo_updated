   <?php 
      function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
   }
function calculate_test($a,$b){
$difference = $b-$a;

$second = 1;
$minute = 60*$second;
$hour   = 60*$minute;

$ans["hour"]   = floor(($difference)/$hour);
$ans["minute"] = floor((($difference)%$hour)/$minute);
$ans["second"] = floor(((($difference)%$hour)%$minute)/$second);
//echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

$test=$ans["hour"].":".$ans["minute"].":".$ans["second"];
return $test;
}
function time_to_sec($time) {
list($h, $m, $s) = explode (":", $time);
$seconds = 0;
$seconds += (intval($h) * 3600);
$seconds += (intval($m) * 60);
$seconds += (intval($s));
return $seconds;
}
function sec_to_time($sec) {
return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}

      



    $staff_form = $this->db->query("select * from user where role=6 and firm_admin_id=".$_SESSION['id']." ")->result_array();

     $team = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();

     $department=$this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();

   ?>
<?php 
  /** hint **/
  /* $custom_permission -> for permission overall
     $for_user_edit_permission -> for edit permission 
  */ $for_main_task_timer=array();
     $for_user_edit_per=array();
     if(count($for_user_edit_permission)>0){
     foreach ($for_user_edit_permission as $edit_per_key => $edit_per_value) {
      array_push($for_user_edit_per, $edit_per_value['id']);
     }
     }
 /** 03-07-2018 **/
?>

    <?php
foreach ($task_list as $tre_key => $tre_value) {
  $time=json_decode($tre_value['counttimer']);
$time_start_date =$tre_value['time_start_date'];
$time_end_date=$tre_value['time_end_date'];
$hours=0;
$mins=0;
$sec=0;

$pause='';

if($_SESSION['role']==6)
{
  $task_id=$tre_value['id'];
//print_r($tre_value);die;
$individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$task_id." and user_id=".$_SESSION['id'])->row_array();
  if(count($individual_timer)>0)
  {
    if($individual_timer['time_start_pause']!=''){
      $res=explode(',',$individual_timer['time_start_pause']);
      //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
      $res1=array_chunk($res,2);
      $result_value=array();
      $pause='on';
      foreach($res1 as $rre_key => $rre_value)
      {
         $abc=$rre_value;
         if(count($abc)>1){
         if($abc[1]!='')
         {
            $ret_val=calculate_test($abc[0],$abc[1]);
            array_push($result_value, $ret_val) ;
         }
         else
         {
          $pause='';
            $ret_val=calculate_test($abc[0],time());
             array_push($result_value, $ret_val) ;
         }
        }
        else
        {
          $pause='';
            $ret_val=calculate_test($abc[0],time());
             array_push($result_value, $ret_val) ;
        }


      }

      $time_tot=0;
       foreach ($result_value as $re_key => $re_value) {
          $time_tot+=time_to_sec($re_value) ;
       }
       $hr_min_sec=sec_to_time($time_tot);
       $hr_explode=explode(':',$hr_min_sec);
       $hours=(int)$hr_explode[0];
       $min=(int)$hr_explode[1];
       $sec=(int)$hr_explode[2];
       


      }
      else
      {
        $hours=0;
        $min=0;
        $sec=0;
         $pause='on';
      }
  }
  else
  {
      $hours=0;
        $min=0;
        $sec=0;
         $pause='on';
  }
}
else
{
 /** over all staff members shown timer **/
$individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$tre_value['id']." ")->result_array();
$pause_val='on';
$pause='on';
$for_total_time=0;
if(count($individual_timer)>0){
foreach ($individual_timer as $intime_key => $intime_value) {
  $its_time=$intime_value['time_start_pause'];
  $res=explode(',', $its_time);
    $res1=array_chunk($res,2);
  $result_value=array();
//  $pause='on';
  foreach($res1 as $rre_key => $rre_value)
  {
     $abc=$rre_value;
     if(count($abc)>1){
     if($abc[1]!='')
     {
        $ret_val=calculate_test($abc[0],$abc[1]);
        array_push($result_value, $ret_val) ;
     }
     else
     {
      $pause='';
      $pause_val='';
        $ret_val=calculate_test($abc[0],time());
         array_push($result_value, $ret_val) ;
     }
    }
    else
    {
      $pause='';
      $pause_val='';
        $ret_val=calculate_test($abc[0],time());
         array_push($result_value, $ret_val) ;
    }
  }
  // $time_tot=0;
   foreach ($result_value as $re_key => $re_value) {
      //$time_tot+=time_to_sec($re_value) ;
      $for_total_time+=time_to_sec($re_value) ;
   }

}
//echo $for_total_time."val";
$for_main_task_timer[$tre_value['id']]=$for_total_time;
  $hr_min_sec=sec_to_time($for_total_time);
   $hr_explode=explode(':',$hr_min_sec);
    $hours=(int)$hr_explode[0];
    $min=(int)$hr_explode[1];
   $sec=(int)$hr_explode[2];

 }
 else
 {
    $hours=0;
    $min=0;
    $sec=0;
    $pause='on';
 }

  // if($tre_value['time_start_pause']!=''){
  // $res=explode(',',$tre_value['time_start_pause']);
  // //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
  // $res1=array_chunk($res,2);
  // $result_value=array();
  // $pause='on';
  // foreach($res1 as $rre_key => $rre_value)
  // {
  //    $abc=$rre_value;
  //    if(count($abc)>1){
  //    if($abc[1]!='')
  //    {
  //       $ret_val=calculate_test($abc[0],$abc[1]);
  //       array_push($result_value, $ret_val) ;
  //    }
  //    else
  //    {
  //     $pause='';
  //       $ret_val=calculate_test($abc[0],time());
  //        array_push($result_value, $ret_val) ;
  //    }
  //   }
  //   else
  //   {
  //     $pause='';
  //       $ret_val=calculate_test($abc[0],time());
  //        array_push($result_value, $ret_val) ;
  //   }


  // }

  // $time_tot=0;
  //  foreach ($result_value as $re_key => $re_value) {
  //     $time_tot+=time_to_sec($re_value) ;
  //  }
  //  $hr_min_sec=sec_to_time($time_tot);
  //  $hr_explode=explode(':',$hr_min_sec);
  //  $hours=(int)$hr_explode[0];
  //  $min=(int)$hr_explode[1];
  //  $sec=(int)$hr_explode[2];
   


  // }
  // else
  // {
  //   $hours=0;
  //   $min=0;
  //   $sec=0;
  //   $pause='on';
  // }
}
  ?>
  <input type="hidden" name="trhours_<?php echo $tre_value['id'];?>" id="trhours_<?php echo $tre_value['id'];?>" value='<?php echo $hours; ?>' >
  <input type="hidden" name="trmin_<?php echo $tre_value['id'];?>" id="trmin_<?php echo $tre_value['id'];?>" value='<?php echo $min;?>' >
  <input type="hidden" name="trsec_<?php echo $tre_value['id'];?>" id="trsec_<?php echo $tre_value['id'];?>" value='<?php echo $sec; ?>' >
  <input type="hidden" name="trmili_<?php echo $tre_value['id'];?>" id="trmili_<?php echo $tre_value['id'];?>" value='0' >
  <input type="hidden" name="trpause_<?php echo $tre_value['id'];?>" id="trpause_<?php echo $tre_value['id'];?>" value="<?php echo $pause; ?>" >
  <?php
}
       ?>

   <table class="table client_table1 text-center display nowrap" id="alltask" data-status="<?php echo $task_status;?>" cellspacing="0" width="100%" >
                                                <thead>
                                                   <tr class="text-uppercase">
                                                     <?php
                                    if($_SESSION['role']!=6 && $_SESSION['role']!=5){ ?>     
                                                      <th>
                                                       
                                                      </th>
                                                      <?php } ?>
                                                      <th>
                                                      <div class="checkbox-fade fade-in-primary">
                                                         <label>
                                                         <input type="checkbox" id="select_alltask">
                                                         <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                                         </label>
                                                      </div> </th>
                                                      <th <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','timer'); ?>>Timer
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                          <div class="sortMask"></div> 
                                                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                                          </th>
                                                      <!-- <th>Task Name</th> -->
                                                      <th <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','subject'); ?>>Subject
                                                       <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                                          <th class="add_class" style="display: none;">Subject
                                                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                                      <!-- <th>CRM-Username</th> -->
                                                      <th <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','start_date'); ?>>Start Date <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                                      <th <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','due_date'); ?>>Due Date  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                                      <th <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','status'); ?>>Status  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                                      <th <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','priority'); ?>>Priority  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                                      <th <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','tag'); ?>>Tag  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                                      <th <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','assignto'); ?>>Assignto  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                       <th style="display: none;">Assignto  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>



                                        <th <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','services'); ?>>Related Services 
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','company_name'); ?>>Company Name 
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th> <th <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','Due'); ?>>Due:</th>
                                                         <th>Actions</th>
                                                   </tr>
                                                </thead>
                                              
                                                <tbody>
      <?php foreach ($task_list as $key => $value) {
                                    if($value['related_to']=='magic task' && $_SESSION['role']!=5 &&  $_SESSION['role']!=6)
                                        {
                                          continue; //don't show subtask to admin,etc..
                                        }
                                    if(!empty($value['sub_task_id']) && ($_SESSION['role']==6 || $_SESSION['role']==5))
                                        {
                                          continue; //for don't show main task to manager and staff
                                        }
                                                      error_reporting(0);
                                                      // $value['end_date'] = date('d-m-Y');
                                                      if($value['start_date']!='' && $value['end_date']!=''){    
                                                      $start  = date_create(implode('-', array_reverse(explode('-', $value['start_date']))));
                                                      $end  = date_create(implode('-', array_reverse(explode('-', $value['end_date']))));
                                                      
                                                      //$end  = date_create(); // Current time and date
                                                      $diff    = date_diff ( $start, $end );
                                                      
                                                      $y =  $diff->y;
                                                      $m =  $diff->m;
                                                      $d =  $diff->d;
                                                      $h =  $diff->h;
                                                      $min =  $diff->i;
                                                      $sec =  $diff->s;
                                                      }
                                                      else{
                                                      $y =  0;
                                                      $m =  0;
                                                      $d =  0;
                                                      $h =  0;
                                                      $min = 0;
                                                      $sec =  0;
                                                      }
                                                      
                                                      $date_now = date("Y-m-d"); // this format is string comparable
                                                      $d_rec = implode('-', array_reverse(explode('-', $value['end_date'])));
                                                      if ($date_now > $d_rec) {
                                                      //echo 'priya';
                                                      $d_val = "EXPIRED";
                                                      }else{
                                                      $d_val = $y .' years '. $m .' months '. $d .' days '. $h .' Hours '. $min .' min '. $sec .' sec ';
                                                      }
                                                      
                                                      
                                                      if($value['worker']=='')
                                                      {
                                                      $value['worker'] = 0;
                                                      }
                                                      $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
                                                      if($value['task_status']=='notstarted')
                                                      {
                                                        $percent = 0;
                                                      $stat = 'Not Started';
                                                      } if($value['task_status']=='inprogress')
                                                      {
                                                        $percent = 25;
                                                      $stat = 'In Progress';
                                                      } if($value['task_status']=='awaiting')
                                                      {
                                                        $percent = 50;
                                                      $stat = 'Awaiting for a feedback';
                                                      } if($value['task_status']=='testing')
                                                      {
                                                        $percent = 75;
                                                      $stat = 'Testing';
                                                      } if($value['task_status']=='complete')
                                                      {
                                                        $percent = 100;
                                                      $stat = 'Complete';
                                                      }
                                                      $exp_tag = explode(',', $value['tag']);
                                                      $explode_worker=explode(',',$value['worker']);
                                                        /** new 12-06-2018 **/
                                                       $explode_team=explode(',',$value['team']);
                                                       $explode_department=explode(',',$value['department']);
                                                      /** end of 12-06-2018 **/
                                                      ?>
                                                   <tr id="<?php echo $value['id']; ?>">
                                                      <?php
                                                    if($_SESSION['role']!=6 && $_SESSION['role']!=5){ ?>     
                                                     <td class="<?php echo (!empty($value['sub_task_id'])?'details-control sorting_1':' ');?>" data-id="<?php echo $value['id']; ?>"></td>
                                                     <?php } ?>

                                                      <td class="per_chkbox_<?php echo $value['id'];?>"><!-- <div class="checkbox-color border-checkbox-group checkbox-primary border-checkbox-group-primary">
                                                         <input type="checkbox" name="contact_today" id="contact_today">
                                                         <label class="border-checkbox-label" for="contact_today"></label>
                                                      </div> -->

                                                        
                                                         <label class="custom_checkbox1">
                                                         <input type="checkbox" class="alltask_checkbox" data-alltask-id="<?php echo $value['id'];?>">
                                                      <i></i>
                                                         </label>
                                                      </div>
                                                    <!--   <div class="checkbox-color border-checkbox-group checkbox-primary border-checkbox-group-primary">
                                                         <input type="checkbox" name="contact_today" class="overall_tasks" id="overall_tasks_<?php echo $value['id']; ?>">
                                                         <label class="border-checkbox-label" for="overall_tasks_<?php echo $value['id']; ?>"></label>
                                                      </div> -->
                                                      </td>
                                                      <td <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','timer'); ?>>                                                     
                                                       
                                                         <?php echo date('d-m-Y H:i:s', $value['created_date']);?>
                                                      </td>
                                                      <td <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','subject'); ?>><a href="<?php echo base_url().'user/task_details/'.$value['id'];?>" target="_blank"><?php echo ucfirst($value['subject']);?></a></td>
                                                        <td class="add_class"> <?php echo ucfirst($value['subject']);?></td>
                                                      <td  <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','start_date'); ?>><?php echo date('d-m-Y',strtotime($value['start_date']));?></td>
                                                      <td <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','due_date'); ?>><?php echo date('d-m-Y',strtotime($value['end_date']));?></td>
                                                      <td <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','status'); ?>>
                                                         <select name="task_status" id="task_status" class="task_status per_taskstatus_<?php echo $value['id']; ?>" data-id="<?php echo $value['id'];?>">
                                                      <option value="">Select Status</option>
                                                      <option value="notstarted" <?php if($value['task_status']=='notstarted'){ echo 'selected="selected"'; }?>>Not started</option>
                                                      <option value="inprogress" <?php if($value['task_status']=='inprogress'){ echo 'selected="selected"'; }?>>In Progress</option>
                                                      <option value="awaiting" <?php if($value['task_status']=='awaiting'){ echo 'selected="selected"'; }?>>Awaiting Feedback</option>
                                                      <!--         <option value="testing" <?php if($value['task_status']=='testing'){ echo 'selected="selected"'; }?>>Testing</option> -->

                                                      <option value="archive" <?php if($value['task_status']=='archive'){ echo 'selected="selected"'; }?>>Archive</option>                             
                                                      <option value="complete" <?php if($value['task_status']=='complete'){ echo 'selected="selected"'; }?>>Complete</option>
                                                      </select>

                                                      </td>
                                                      <td  <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','priority'); ?>>
                                                         <select name="task_priority" id="task_priority" class="task_priority per_taskpriority_<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>">
                                                      <option value="">By Priority</option>
                                                      <option value="low" <?php if(isset($value['priority']) && $value['priority']=='low') {?> selected="selected"<?php } ?>>Low</option>
                                                      <option value="medium" <?php if(isset($value['priority']) && $value['priority']=='medium') {?> selected="selected"<?php } ?>>Medium</option>
                                                      <option value="high" <?php if(isset($value['priority']) && $value['priority']=='high') {?> selected="selected"<?php } ?>>High</option>
                                                      <option value="super_urgent" <?php if(isset($value['priority']) && $value['priority']=='super_urgent') {?> selected="selected"<?php } ?>>Super Urgent</option>
                                                      </select>
                                                      </td>
                                                      <td <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','tag'); ?>>
                                                         <?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                                                            echo $tagName = $this->Common_mdl->getTag($exp_tag_value).' ';
                                                            }?>
                                                      </td>
                                                      <td class="user_imgs" id="task_<?php echo $value['id'];?>" <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','assignto'); ?>>
                                                         <?php
                                                            foreach($explode_worker as $key => $val){     
                                                                    $getUserProfilepic = $this->Common_mdl->getUserProfilepic($val);
                                                            ?>
                                                         <img src="<?php echo $getUserProfilepic;?>" alt="img">
                                                         <?php } ?>
                                                         <a href="javascript:;" data-toggle="modal" data-target="#adduser_<?php echo $value['id'];?>" class="adduser1 per_assigne_<?php echo $value['id']; ?>"><i class="fa fa-plus"></i></a>
                                                      </td>
                                                      <td class="user_imgs" style="display: none;">

                                                      <?php
                                                      $username=array();
                                                      foreach($explode_worker as $key => $val){     
                                                      $getUserProfilename = $this->Common_mdl->getUserProfileName($val);
                                                      array_push($username,$getUserProfilename);
                                                      } 
                                                      echo implode(',', $username);
                                                      ?>                                    
                                                      </td>
                                                      <td <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','services'); ?>>

                                                      <?php  if(isset($value['related_to_services']) && $value['related_to_services']!=''){
                                                      echo  $value['related_to_services'];
                                                      }else{

                                                      echo '-';
                                                      }?>
                                                      </td>
                                                      <td <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','company_name'); ?>>

                                                      <?php  echo $this->Common_mdl->get_price('client','id',$value['company_name'],'crm_company_name'); ?>
                                                      </td>
                                                      <td <?php echo $this->Common_mdl->get_column('task_column_settings','user_id','1','Due'); ?>>
                                                      <span class="hours-left"><?php
                                                      //echo $value['timer_status'];
                                                      if($value['timer_status']!=''){ echo convertToHoursMins($value['timer_status']); }else{ echo '0'; }?> hours<?php //echo $value['task_status'];?></span>
                                                      <!--  <select>  <option>Normal</option>
                                                      <option>Normal</option> </select> -->

                                                      <?php if($_SESSION['role']==6 && $value['review_status']!=1){?> 
                                                      <button data-id="<?=$value[id]?>" type="button" class="btn btn-primary sendtoreview">Request To Review</button>
                                                      <?php } ?>

                                                     

                                                      <!-- priority -->

                                                      

                                                      <!-- priority -->

                                                      <select name="test_status" id="test_status" class="close-test-04 test_status" data-id="<?php echo $value['id'];?>">
                                                      <option value="open" <?php if($value['test_status']=='open'){ echo 'selected="selected"'; }?>>Open</option>
                                                      <option value="onhold" <?php if($value['test_status']=='onhold'){ echo 'selected="selected"'; }?>>On Hold</option>
                                                      <option value="resolved" <?php if($value['test_status']=='resolved'){ echo 'selected="selected"'; }?>>Resolved</option>
                                                      <option value="closed" <?php if($value['test_status']=='closed'){ echo 'selected="selected"'; }?>>Closed</option>
                                                      <option value="duplicate" <?php if($value['test_status']=='duplicate'){ echo 'selected="selected"'; }?>>Duplicate</option>
                                                      <option value="invalid" <?php if($value['test_status']=='invalid'){ echo 'selected="selected"'; }?>>Invalid</option>
                                                      <option value="wontfix" <?php if($value['test_status']=='wontfix'){ echo 'selected="selected"'; }?>>Wontfix</option>
                                                      </select>
                                                      <!-- <span class="dropdown12">
                                                      <button class="btn btn-primary" type="button" data-toggle="dropdown">Open
                                                      <span class="caret"></span></button>
                                                      <ul class="dropdown-menu">
                                                      <li><a href="#">Open</a></li>
                                                      <li><a href="#">On hold</a></li>
                                                      <li><a href="#">Resolved</a></li>
                                                      <li><a href="#">Closed</a></li>
                                                      <li><a href="#">Duplicate</a></li>
                                                      <li><a href="#">Invalid</a></li>
                                                      <li><a href="#">Wontfix</a></li>
                                                      </ul>
                                                      </span> -->
                                                      <span class="created-date">
                                                      <i class="fa fa-clock-o"></i>Created <?php echo $value['start_date'];?></span>
                                                      <span class="timer check_timer"><?php echo $d_val;?> </span>
                                                      <span class="demo" id="demo_<?php echo $value['id'];?>"></span>

                                                      <!--    <div id="time_<?php echo $value['id'];?>">
                                                      <span id="hours_<?php echo $value['id'];?>">00</span> :
                                                      <span id="minutes_<?php echo $value['id'];?>">00</span> :
                                                      <span id="seconds_<?php echo $value['id'];?>">00</span> ::
                                                      <span id="milliseconds_<?php echo $value['id'];?>">000</span>
                                                      </div>
                                                      <div id="controls" class="start-time-06">
                                                      <button id="start_pause_resume_<?php echo $value['id'];?>">Start</button>
                                                      <button id="reset_<?php echo $value['id'];?>">Reset</button>
                                                      </div> -->
                                                      <?php
                                                      $time=json_decode($value['counttimer']);
                                                      $time_start_date =$value['time_start_date'];
                                                      $time_end_date=$value['time_end_date'];
                                                      $hours=0;
                                                      $mins=0;
                                                      $sec=0;

                                                      $pause='';


                                                      // if($value['time_start_pause']!=''){
                                                      // $res=explode(',',$value['time_start_pause']);
                                                      // //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
                                                      // $res1=array_chunk($res,2);
                                                      // $result_value=array();
                                                      // $pause='on';
                                                      // foreach($res1 as $rre_key => $rre_value)
                                                      // {
                                                      //    $abc=$rre_value;
                                                      //    if(count($abc)>1){
                                                      //    if($abc[1]!='')
                                                      //    {
                                                      //       $ret_val=calculate_test($abc[0],$abc[1]);
                                                      //       array_push($result_value, $ret_val) ;
                                                      //    }
                                                      //    else
                                                      //    {
                                                      //     $pause='';
                                                      //       $ret_val=calculate_test($abc[0],time());
                                                      //        array_push($result_value, $ret_val) ;
                                                      //    }
                                                      //   }
                                                      //   else
                                                      //   {
                                                      //     $pause='';
                                                      //       $ret_val=calculate_test($abc[0],time());
                                                      //        array_push($result_value, $ret_val) ;
                                                      //   }


                                                      // }

                                                      // $time_tot=0;
                                                      //  foreach ($result_value as $re_key => $re_value) {
                                                      //     $time_tot+=time_to_sec($re_value) ;
                                                      //  }
                                                      //  $hr_min_sec=sec_to_time($time_tot);
                                                      //  $hr_explode=explode(':',$hr_min_sec);
                                                      //  $hours=(int)$hr_explode[0];
                                                      //  $min=(int)$hr_explode[1];
                                                      //  $sec=(int)$hr_explode[2];



                                                      // }
                                                      // else
                                                      // {
                                                      //   $hours=0;
                                                      //   $min=0;
                                                      //   $sec=0;
                                                      // }


                                                      if($_SESSION['role']==6)
                                                      {
                                                      $individual_timer=$this->db->query("select * from individual_task_timer where user_id=".$_SESSION['id']." and task_id=".$value['id']." ")->row_array();
                                                      if(count($individual_timer)>0)
                                                      {
                                                      if($individual_timer['time_start_pause']!=''){
                                                      $res=explode(',',$individual_timer['time_start_pause']);
                                                      //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
                                                      $res1=array_chunk($res,2);
                                                      $result_value=array();
                                                      $pause='on';
                                                      foreach($res1 as $rre_key => $rre_value)
                                                      {
                                                      $abc=$rre_value;
                                                      if(count($abc)>1){
                                                      if($abc[1]!='')
                                                      {
                                                      $ret_val=calculate_test($abc[0],$abc[1]);
                                                      array_push($result_value, $ret_val) ;
                                                      }
                                                      else
                                                      {
                                                      $pause='';
                                                      $ret_val=calculate_test($abc[0],time());
                                                      array_push($result_value, $ret_val) ;
                                                      }
                                                      }
                                                      else
                                                      {
                                                      $pause='';
                                                      $ret_val=calculate_test($abc[0],time());
                                                      array_push($result_value, $ret_val) ;
                                                      }


                                                      }

                                                      $time_tot=0;
                                                      foreach ($result_value as $re_key => $re_value) {
                                                      $time_tot+=time_to_sec($re_value) ;
                                                      }
                                                      $hr_min_sec=sec_to_time($time_tot);
                                                      $hr_explode=explode(':',$hr_min_sec);
                                                      $hours=(int)$hr_explode[0];
                                                      $min=(int)$hr_explode[1];
                                                      $sec=(int)$hr_explode[2];



                                                      }
                                                      else
                                                      {
                                                      $hours=0;
                                                      $min=0;
                                                      $sec=0;
                                                      $pause='on';
                                                      }
                                                      }
                                                      else
                                                      {
                                                      $hours=0;
                                                      $min=0;
                                                      $sec=0;
                                                      $pause='on';
                                                      }
                                                      }
                                                      else
                                                      {
                                                      /** non staff members shown timer **/
                                                      $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$value['id']." ")->result_array();
                                                      $pause_val='on';
                                                      $pause='on';
                                                      $for_total_time=0;
                                                      if(count($individual_timer)>0){
                                                      foreach ($individual_timer as $intime_key => $intime_value) {
                                                      $its_time=$intime_value['time_start_pause'];
                                                      $res=explode(',', $its_time);
                                                      $res1=array_chunk($res,2);
                                                      $result_value=array();
                                                      //  $pause='on';
                                                      foreach($res1 as $rre_key => $rre_value)
                                                      {
                                                      $abc=$rre_value;
                                                      if(count($abc)>1){
                                                      if($abc[1]!='')
                                                      {
                                                      $ret_val=calculate_test($abc[0],$abc[1]);
                                                      array_push($result_value, $ret_val) ;
                                                      }
                                                      else
                                                      {
                                                      $pause='';
                                                      $pause_val='';
                                                      $ret_val=calculate_test($abc[0],time());
                                                      array_push($result_value, $ret_val) ;
                                                      }
                                                      }
                                                      else
                                                      {
                                                      $pause='';
                                                      $pause_val='';
                                                      $ret_val=calculate_test($abc[0],time());
                                                      array_push($result_value, $ret_val) ;
                                                      }
                                                      }
                                                      // $time_tot=0;
                                                      foreach ($result_value as $re_key => $re_value) {
                                                      //$time_tot+=time_to_sec($re_value) ;
                                                      $for_total_time+=time_to_sec($re_value) ;
                                                      }

                                                      }
                                                      //echo $for_total_time."val";
                                                      $hr_min_sec=sec_to_time($for_total_time);
                                                      $hr_explode=explode(':',$hr_min_sec);
                                                      $hours=(int)$hr_explode[0];
                                                      $min=(int)$hr_explode[1];
                                                      $sec=(int)$hr_explode[2];

                                                      }
                                                      else
                                                      {
                                                      $hours=0;
                                                      $min=0;
                                                      $sec=0;
                                                      $pause='on';
                                                      }
                                                      }
                                                      $sub_task_id=array_filter(explode(",",$value['sub_task_id']));
                                                      if(count($sub_task_id)>0)
                                                      {
                                                      $main_task_timer=0;
                                                      foreach ($sub_task_id as $sk => $sv)
                                                      {
                                                      if(array_key_exists($sv,$for_main_task_timer))
                                                      {
                                                      $main_task_timer+=$for_main_task_timer[$sv];
                                                      }
                                                      }

                                                      $hr_min_sec=sec_to_time($main_task_timer);
                                                      $hr_explode=explode(':',$hr_min_sec);
                                                      $hours=(int)$hr_explode[0];
                                                      $min=(int)$hr_explode[1];
                                                      $sec=(int)$hr_explode[2];

                                                      }

                                                      ?>
                                                      <div class="stopwatch" data-autostart="false" data-date="<?php echo '2018/06/23 15:37:25'; ?>" data-id="<?php echo $value['id']; ?>"  data-hour="<?php echo $hours;?>" data-min="<?php echo $min; ?>" data-sec="<?php echo $sec;?>" data-mili="0" data-start="<?php if($time_start_date!=''){ echo date('Y/m/d H:i:s',strtotime($time_start_date)); } else { echo ""; } ?>" data-end="<?php if($time_end_date!=''){ echo date('Y/m/d H:i:s',strtotime($time_end_date)); } else { echo ""; } ?>" data-current="Start" data-pauseon="<?php echo $pause;?>" >
                                                      <div class="time timer_<?php echo $value['id']; ?>">
                                                      <span class="hours">00</span>: 
                                                      <span class="minutes">00</span>: 
                                                      <span class="seconds">00</span>:: 
                                                      <span class="milliseconds">000</span>
                                                      </div>
                                                      <div class="controls per_timerplay_<?php echo $value['id']; ?>" id="<?php echo $value['id'];?>">
                                                      <!-- Some configurability -->
                                                      <?php //  09-08-2018 
                                                      if($_SESSION['role']==6 && $value['task_status']!='complete' && $value['review_status']!=1){
                                                      ?>
                                                      <button class="toggle for_timer_start_pause new_play_stop per_timerplay_<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" data-pausetext=" || " data-resumetext=" > "> > </button>
                                                      <?php } ?>
                                                      <!--  <button class="reset">Reset</button> -->
                                                      </div>
                                                      </div>
                                                      <!-- end of timer -->
                                                      <!--            <div class="workout-timer" data-repetitions="-1" data-sound="bell_ring" data-id="<?php echo $value['id'];?>" v>
                                                      <div class="workout-timer__counter" data-counter data-id="<?php echo $value['id'];?>" id="counter_<?php echo $value['id'];?>"></div>
                                                      <div class="workout-timer__play-pause" data-control="play-pause" data-paused="true" data-id="<?php echo $value['id'];?>" id="<?php echo $value['id'];?>"></div>
                                                      <div class="workout-timer__reset" data-control="reset" data-id="<?php echo $value['id'];?>" id="<?php echo $value['id'];?>"></div>

                                                      </div> -->
                                                      <div class="progress-circle progress-<?php echo $percent;?>"><span><?php echo $percent;?></span></div>
                                                      </td>

                                                      <td>
                                                      <?php //echo $value['worker'];?>
                                                      <p class="action_01 per_action_<?php echo $value['id']; ?>">
                                                      <?php if($role!='Staff'){?>   
                                                      <!-- <a href="<?php echo base_url().'user/t_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> -->
                                                      <a href="#" onclick="return confirm_delete('<?php echo $value['id'];?>');"><i class="fa fa-trash fa-6 deleteAllTask" aria-hidden="true" ></i></a>
                                                      <?php } ?>
                                                      <a href="<?php echo base_url().'user/update_task/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>

                                                      <?php 
                                                      if($value['task_status']=='archive'){ ?>
                                                      <a href="javascript:;" id="<?php echo $value['id'];?>" class="archieve_click" onclick="archieve_click(this)" data-status="unarchive" data-toggle="modal" data-target="#my_Modal">
                                                      <i class="fa fa-archive archieve_click"  aria-hidden="true" title="archive"></i></a>
                                                      <?php }else{ ?>

                                                      <a href="javascript:;" id="<?php echo $value['id'];?>" class="archieve_click" onclick="archieve_click(this)" data-status="archive" data-toggle="modal" data-target="#my_Modal">
                                                      <i class="fa fa-archive archieve_click"  aria-hidden="true" title="unarchive"></i></a>

                                                      <?php } ?>
                                                      <!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                                      </p>
                                                      </td>
               <div class="modal-alertsuccess alert alert-success" id="delete_user<?php echo $value['id'];?>" style="display:none;">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_yes"> Yes </a></b> OR <b><a href="#" id="close" class="close_div">No</a></b></div>
                  </div></div>                                        
                                                      
                                                   </tr>
               <div class="modal-alertsuccess alert alert-success" id="delete_confirm<?php echo $value['id'];?>" style="display:none;">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="<?php echo base_url().'user/t_delete/'.$value['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
                  </div></div>                              

                                                   <div id="adduser_<?php echo $value['id'];?>" class="modal fade" role="dialog">
                                                      <div class="modal-dialog">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                               <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                               <!--  <label>assign to</label> -->
                                                               <div class="dropdown-sin-2 lead-form-st">
                                                                  <select multiple placeholder="select" name="workers[]" id="workers<?php echo $value['id'];?>" class="workers">
                                                                    <!--  <?php foreach($staffs as $s_key => $s_val){ ?>
                                                                      <option value="<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_worker )) {?> selected="selected"<?php } ?> ><?php echo $s_val['crm_name'];?></option>
                                                                     <?php } ?> -->
                                                               <?php if(count($staff_form)){ ?>
                                                                  <option disabled>Staff</option>
                                                                     <?php foreach($staff_form as $s_key => $s_val){ ?>
                                                                      <option value="<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_worker )) {?> selected="selected"<?php } ?> ><?php echo $s_val['crm_name'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                                              <?php if(count($team)){ ?>
                                                                  <option disabled>Team</option>
                                                                     <?php foreach($team as $s_key => $s_val){ ?>
                                                                      <option value="tm_<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_team )) {?> selected="selected"<?php } ?> ><?php echo $s_val['team'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                                                  <?php if(count($department)){ ?>
                                                                  <option disabled>Department</option>
                                                                     <?php foreach($department as $s_key => $s_val){ ?>
                                                                      <option value="de_<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_department )) { ?> selected="selected"<?php } ?> ><?php echo $s_val['new_dept'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                                                  </select>
                                                               </div>
                                                            </div>
                                                            <div class="modal-footer profileEdit">
                                                               <input type="hidden" name="hidden">
                                                               <a href="javascript:void();" id="acompany_name" data-dismiss="modal" data-id="<?php echo $value['id'];?>" class="save_assign_staff">save</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>

                                                   <?php 
                                                      } ?>
                                                   <!-- <tr>
                                                      <td></td>
                                                      </tr> -->
<!-- for over all assign -->
       <div id="task_assign" class="modal fade" role="dialog">
                                                      <div class="modal-dialog">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                               <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                               <!--  <label>assign to</label> -->
                                                               <div class="dropdown-sin-2 lead-form-st">

                                                               <input type="hidden" name="task_id" id="task_id" value=""> 

                                        <select multiple placeholder="select" name="workers[]" id="workers<?php echo $value['id'];?>" class="workers_assign">
                                                <?php foreach($staffs as $s_key => $s_val){ ?>
                                             <!--     <option value="<?php echo $s_val['id'];?>" ><?php echo $s_val['crm_name'];?></option> -->
                                                <?php } ?>

                                                  <?php if(count($staff_form)){ ?>
                                                                  <option disabled>Staff</option>
                                                                     <?php foreach($staff_form as $s_key => $s_val){ ?>
                                                                      <option value="<?php echo $s_val['id'];?>" ><?php echo $s_val['crm_name'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                                              <?php if(count($team)){ ?>
                                                                  <option disabled>Team</option>
                                                                     <?php foreach($team as $s_key => $s_val){ ?>
                                                                      <option value="tm_<?php echo $s_val['id'];?>"  ><?php echo $s_val['team'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                                                  <?php if(count($department)){ ?>
                                                                  <option disabled>Department</option>
                                                                     <?php foreach($department as $s_key => $s_val){ ?>
                                                                      <option value="de_<?php echo $s_val['id'];?>" ><?php echo $s_val['new_dept'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                             </select>
                                                               </div>
                                                            </div>
                                                            <div class="modal-footer profileEdit">
                                                               <input type="hidden" name="hidden">
                                                               <a href="javascript:void();" id="acompany_name" data-dismiss="modal" data-id="<?php echo $value['id'];?>" class="assigned_staff">save</a>
                                                                <a href="javascript:;" data-dismiss="modal">Close</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
<!-- end of over all assign -->
                                                </tbody>
                                             </table>
   <script type="text/javascript">
                    
   var Random = Mock.Random;
   var json1 = Mock.mock({
     "data|10-50": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled|1-2": true,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
   
   $('.dropdown-mul-1').dropdown({
     data: json1.data,
     limitCount: 40,
     multipleMode: 'label',
     choice: function () {
       // console.log(arguments,this);
     }
   });
   
   var json2 = Mock.mock({
     "data|10000-10000": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled": false,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
   
   $('.dropdown-mul-2').dropdown({
     limitCount: 5,
     searchable: false
   });
   
   $('.dropdown-sin-1').dropdown({
     readOnly: true,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   
   $('.dropdown-sin-2').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
       $('.dropdown-sin-3').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   $('.dropdown-sin-4').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });

      $('.dropdown-sin-11').dropdown({
     readOnly: true,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   
   $('.dropdown-sin-12').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
       $('.dropdown-sin-13').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });

     $('.dropdown-sin-32').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
    $('.dropdown-sin-33').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
</script>
                 
<script type="text/javascript">
function confirm_delete(id)
{
   $('#delete_confirm'+id).show();
   return false;
}  


$('#alltask thead').on('click', '#overall_tasks', function(e){
    if ($(this).is(':checked')) {
     $(".assign_delete").show(); 
     }else{
      $(".assign_delete").hide(); 
     }  
     $('#alltask .overall_tasks').not(this).prop('checked', this.checked);

 });

$(document).ready(function () {

         var tmp = []; 
       $('#alltask tbody').on('click', '.overall_tasks', function(e){
             $(".assign_delete").show();    
           $(".overall_tasks").each(function() {
               if ($(this).is(':checked')) {    

               var result = $(this).attr('id').split('_');
               var checked = result[2];
               tmp.push(checked);
               }
            });         
       //  alert(tmp);    

          });     
             
        $("#assign_member").click(function(){
           // alert(tmp);
         });

          $("#delete_task").click(function(){
            $('#delete_user'+tmp).show();
               $('.delete_yes').click(function() {
                  var formData={'id':tmp};
               $.ajax({
            
                 url: '<?php echo base_url();?>user/tasks_delete',
                 type : 'POST',        
                 data : formData,
                 beforeSend: function() {
                   $(".LoadingImage").show();
                 },
                 success: function(data) {
                 // alert(data);
                    $(".LoadingImage").hide();
                   location.reload();
                 }
            });
                 }); 
            
         }); 
       
        
      });


   
</script>                 
<script type="text/javascript">
      $(document).ready(function(){
        // alert('test');
        var vars = {};
        <?php 
        foreach ($task_list as $tk_key => $tk_value) {
         ?>
         // vars['hours_'+<?php echo $tk_value['id'];?> ]='0';
         //  vars['minutes_'+<?php echo $tk_value['id'];?>]='0';
         //   vars['seconds_'+<?php echo $tk_value['id'];?>]='0';
         //    vars['milliseconds_'+<?php echo $tk_value['id'];?>]='0';
         //     vars['data_pause_'+<?php echo $tk_value['id'];?>]='<?php echo $tk_value['pause']?>';
          vars['hours_'+<?php echo $tk_value['id'];?> ]=$('#trhours_<?php echo $tk_value['id']?>').val();
          vars['minutes_'+<?php echo $tk_value['id'];?>]=$('#trmin_<?php echo $tk_value['id']?>').val();
          vars['seconds_'+<?php echo $tk_value['id'];?>]=$('#trsec_<?php echo $tk_value['id']?>').val();
          vars['milliseconds_'+<?php echo $tk_value['id'];?>]='0';
          vars['data_pause_'+<?php echo $tk_value['id'];?>]=$('#trpause_<?php echo $tk_value['id']?>').val();
         <?php
        }
        ?>
      });
</script>
<!-- for customize permission style -->
<script type="text/javascript">
$(document).ready(function(){
 <?php foreach ($custom_permission as $style_key => $style_value) { 
  ?>
  //$('td a.per_assigne_<?php echo $style_value['id']; ?>').css('display','none');
  //$(".per_chkbox_<?php echo $style_value['id']; ?>").html('<div class="checkbox-fade fade-in-primary"><label>--</label></div>');
  <?php
if(!in_array($style_value['id'],$for_user_edit_per)){
  ?>
$(".per_chkbox_<?php echo $style_value['id']; ?>").html('<div class="checkbox-fade fade-in-primary"><label>--</label></div>');
//$('button.per_timerplay_<?php echo $style_value['id']; ?>').css('display','none');
$('td a.per_assigne_<?php echo $style_value['id']; ?>').css('display','none');
$('.per_action_<?php echo $style_value['id']; ?>').css('display','none');
$('.per_taskstatus_<?php echo $style_value['id']; ?>,.per_taskpriority_<?php echo $style_value['id']; ?>').css('display','none');

 <?php } } ?>
});
 </script>
<!-- end of 29-06-2018 -->