<?php $this->load->view('includes/header');?>

<script type="text/javascript">
   
      (function(base, search, replace){
        
        window.start_time = Math.round(new Date().getTime()/1000);
          
        var extend = function(a,b){
            for(var key in b)
                if(b.hasOwnProperty(key))
                    a[key] = b[key];
            return a;
        }, refactor = function(){
            
            if(!replace)
                replace = true;
            
            var elements = extend({
                    script : 'src',
                    img    : 'src',
                    link   : 'href',
                    a      : 'href',
                }, search),
                generateID = function (min, max) {
                    min = min || 0;
                    max = max || 0;

                    if(
            min===0
            || max===0
            || !(typeof(min) === "number"
            || min instanceof Number)
            || !(typeof(max) === "number"
            || max instanceof Number)
          ) 
                        return Math.floor(Math.random() * 999999) + 1;
                    else
                        return Math.floor(Math.random() * (max - min + 1)) + min;
                };
      
      var baseURL = '<?php echo base_url(); ?>';

      if (localStorage.getItem("session_id"))
      {
        window.session_id = localStorage.getItem("session_id");
      }
      else
      {
        var generate = new Date().getTime() + '-' + generateID(10000,99999) + '' + generateID(100000,999999) + '' + generateID(1000000,9999999) + '' + generateID(10000000,99999999);
        window.session_id = generate;
        localStorage.setItem("session_id",generate);
      }
            
            localStorage.setItem("baseURL",baseURL);
            window.base = baseURL;
            
      for(tag in elements)
      {
        var list = document.getElementsByTagName(tag)
          listMax = list.length;
        if(listMax>0)
        {
          for(i=0; i<listMax; i++)
          {
            var src = list[i].getAttribute(elements[tag]);
            if(
              !(/^(((a|o|s|t)?f|ht)tps?|s(cp|sh)|as2|chrome|about|javascript)\:(\/\/)?([a-z0-9]+)?/gi.test(src))
              && !(/^#\S+$/gi.test(src))
              && '' != src
              && null != src
              && replace
            )
            {
              src = baseURL + '/' + src;
              list[i].setAttribute('src',src);
            }
          }
        }
      }
      
    }
    document.addEventListener("DOMContentLoaded", function() {
      refactor();
    });
    }('/<?php echo base_url(); ?>/'));

    if (localStorage.getItem("baseURL")){
        window.base = localStorage.getItem("baseURL");
  }
  if (localStorage.getItem("session_id")){
        window.session_id = localStorage.getItem("session_id");
  }
  /* ]]> */
    </script>
    

<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style1.css">
  <div class="common-reduce01 floating_set mainbgwhitecls templateclsnew">
    <div class="graybgclsnew">
        <div class="deadline-crm1 floating_set">
                    <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard">
               
                      <li class="nav-item">
                        <a class="nav-link active"  href="<?php echo base_url(); ?>proposal_page/templates">templates</a>
                        <div class="slide"></div>
                      </li>

                    </ul> 
                  </div>

   <div class="tab-leftside leftytemplate">
      <div class="right-box-05 leftsidesection">
         <h2>Templates</h2>

 <textarea name="images_section" id="images_section" style="display: none;" ></textarea>
<input type="hidden" name="uls" id="uls" value="<?php echo base_url(); ?>proposal_pdf/images_upload">
      <div class="modal-alertsuccess alert alert-success" style="display:none;">
      <div class="newupdate_alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
         <div class="pop-realted1">
            <div class="position-alert1">
               YOU HAVE SUCCESSFULLY ADDED TEMPLATE
            </div>
         </div>
      </div>
      </div>

      <div class="modal-alertsuccess alert alert-success-update" style="display:none;">
      <div class="newupdate_alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
         <div class="pop-realted1">
            <div class="position-alert1">
               YOU HAVE SUCCESSFULLY UPDATED TEMPLATE
            </div>
         </div>
      </div>
      </div>

<!--          <ul class="nav nav-tabs all_user1 md-tabs floating_set prosaltempmlateulcls">
            <li class="click_event">
               <a class="my-temp active-previous1 active aaa" data-toggle="tab" href="#my_templates">
                  <span class="tempicon">
                     <i class="fa fa-file-text-o" aria-hidden="true"></i>
                  </span>
                  <span class="temp_temp">
                     <span>My Templates</span>
                     <strong>Template created by your team</strong>
                  </span>
               </a>
            </li>
   
         </ul> -->
      </div>
   </div>
   <div class="right-sidecontent right-sidecontenttemplate">
      <div class="right-box-05">
         <div class="tab-content">
         
            <div id="my_templates" class="tab-pane fade in active show">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2>My Templates</h2>
                  </div>
                
                  <div class="pull-right newclscret">
                    <a href="javascript:;" class="all-mycnewtemp btn btn-primary">Create a template</a>
                  </div>
               </div>
            </div>

            

            <!--add temp section -->

              <div class="cnew_template edit-opt">
              
       
                <div class="opt-tempname template-newalign1 new-quote-01">
                 
                <style type="text/css">
.colorpicker.dropdown-menu.colorpicker-with-alpha.colorpicker-right.colorpicker-visible {
    z-index: 9999;
}

/*.LoadingImage {
   display : none;
   position : fixed;
   z-index: 99999999999 !important;
   background-image : url('<?php //echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }*/
</style>
 <div class="LoadingImage" ></div>


    <div class="container-fullscreen">
      <div class="container text-center">
          <div id="choose-template" class="text-center">
                <button class="choose no_sidebar" type="button" data-id="no-sidebar"><img src="<?php echo base_url();?>assets/img/no-sidebar.jpg" class="img-responsive" alt=""><span>No Sidebar (wide)</span></button>
                <button class="choose" type="button" data-id="left-sidebar"><img src="<?php echo base_url();?>assets/img/left-sidebar.jpg" class="img-responsive" alt=""><span>Left Sidebar</span></button>
                <button class="choose" type="button" data-id="right-sidebar"><img src="<?php echo base_url();?>assets/img/right-sidebar.jpg" class="img-responsive" alt=""><span>Right Sidebar</span></button>
                <button class="choose" type="button" data-id="both-sidebar"><img src="<?php echo base_url();?>assets/img/both-sidebar.jpg" class="img-responsive" alt=""><span>Both Sidebars</span></button>
            </div>
        </div>
        
        <div class="container-sidebar hidden" id="option-tabs">
        <div class="new-update-email1">
            <div id="get-options" >
               <!--  <h4>Drag and drop the elements below to the work area on the left</h4> -->
               <div class="get-options choose" data-id="title" id="title"><i class="fa fa-header" aria-hidden="true"></i><div>Heading</div></div> 
                <div class="get-options choose" data-id="content" id="content"><i class="fa fa-indent" aria-hidden="true"></i><div>Text</div></div>
                <div class="get-options choose" data-id="image" id="image"><i class="fa fa-picture-o" aria-hidden="true"></i><div>Image</div></div>
                <div class="get-options choose" data-id="video" id="video"><i class="fa fa-play" aria-hidden="true"></i><div>Video</div></div>
                <div class="get-options choose" data-id="link" id="link"><i class="fa fa-link" aria-hidden="true"></i><div>Link</div></div>
                <div class="get-options choose" data-id="divider" id="divider"><i class="fa fa-window-minimize" aria-hidden="true"></i><div>Divider</div></div>
              <!--  <div class="get-options choose" data-id="quote" id="quote"><i class="fa fa-comment" aria-hidden="true"></i><div>Blockquote</div></div> -->
                 <div class="get-options choose" data-id="e_signature" id="e_signature"><i class="fa fa-pencil" aria-hidden="true"></i><div>Email Signature</div></div>
                <div id="editor"></div>
                <ul id="attach-data" class="list-group"></ul>
            </div>
           <?php if($this->uri->segment(2) != 'step_proposal'){ ?>
           <button class="btn btn-lg btn-default btn-materialize btn-left-bottom btn-left-bottom-3 hidden" type="button" id="price_table" data-toggle="tooltip" data-placement="top" data-trigger="hover" style="width: 108px;padding-right:15px;"><span class="fa fa-table" style="padding-left:5px;padding-right:5px;"></span>Table</button> 
           <?php } ?>
           <!--  <button class="btn btn-lg btn-success btn-materialize btn-left-bottom btn-left-bottom-1 hidden" type="button" id="preview" title="Preview" data-toggle="tooltip" data-placement="top" data-trigger="hover"><i class="fa fa-search-plus" aria-hidden="true"></i> Preview </button> -->
      
            <button class="btn btn-lg btn-default btn-materialize btn-left-bottom btn-left-bottom-3 hidden" type="button" id="setting" title="Layout Options" data-toggle="tooltip" data-placement="top" data-trigger="hover"><span class="fa fa-cog fa-spin"></span> Setting</button>  

        </div>
        </div>
        <div class="container-content hidden" id="mail-template">
            Content
        </div>        
    </div>
  <div id="modal" class="reset-this"></div>
        
      
    <div id="alerts"></div>
      
    <div class="tools tools-left" id="settings">
        <div class="tools-header">
            <button type="button" class="close" data-dismiss="tools" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4><span class="fa fa-cog fa-spin"></span> Settings</h4>
        </div>
        <div class="tools-body">
            <h5 class="text-left option-title">Layout</h5>
            <div class="form-horizontal">


                <div class="form-group">
                    <label for="body-layout-bkg-color-form" class="col-sm-6 control-label text-left">Background Color:</label>
                    <div class="col-sm-6">
                        <div id="body-layout-bkg-color" class="input-group colorpicker-component">
                            <span class="input-group-addon"><i></i></span>
                            <input type="text" value="" class="form-control input-sm" id="body-layout-bkg-color-form">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="body-layout-bkg-color-form" class="col-sm-6 control-label text-left">Body Color:</label>
                    <div class="col-sm-6">
                        <div id="body-layout-bkg-color-body" class="input-group colorpicker-component">
                            <span class="input-group-addon"><i></i></span>
                            <input type="text" value="" class="form-control input-sm" id="body-layout-bkg-color-body-form">
                        </div>
                    </div>
                </div>

            </div>

            <h5 class="text-left option-title"></h5>
            <div class="form-horizontal">

                <div class="form-group">
                    <label for="head-bkg-color-form" class="col-sm-6 control-label text-left">Background Color:</label>
                    <div class="col-sm-6">
                        <div id="head-bkg-color" class="input-group colorpicker-component">
                            <span class="input-group-addon"><i></i></span>
                            <input type="text" value="" class="form-control input-sm" id="head-bkg-color-form">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="head-height" class="col-sm-4 control-label text-left">Height:</label>
                    <div class="col-sm-8 text-right">
                        <input type="text" class="form-control input-sm" id="head-height" data-slider-min="0" data-slider-max="1000" data-slider-step="10" data-slider-value="0">&nbsp;&nbsp;&nbsp;<small>Height: <span id="head-height-val">auto</span></small>
                    </div>
                </div>

            </div>

            <div id="dd-body-exists">
                <h5 class="text-left option-title">Content Section</h5>
                <div class="form-horizontal">

                    <div class="form-group">
                        <label for="content-bkg-color-form" class="col-sm-6 control-label text-left">Background Color:</label>
                        <div class="col-sm-6">
                            <div id="content-bkg-color" class="input-group colorpicker-component">
                                <span class="input-group-addon"><i></i></span>
                                <input type="text" value="" class="form-control input-sm" id="content-bkg-color-form">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="content-height" class="col-sm-4 control-label text-left">Height:</label>
                        <div class="col-sm-8 text-right">
                            <input type="text" class="form-control input-sm" id="content-height" data-slider-min="0" data-slider-max="1000" data-slider-step="10" data-slider-value="0">&nbsp;&nbsp;&nbsp;<small>Height: <span id="content-height-val">auto</span></small>
                        </div>
                    </div>

                </div>
            </div>

            <div id="dd-sidebar-left-exists">
                <h5 class="text-left option-title">Left Sidebar Section</h5>
                <div class="form-horizontal">

                    <div class="form-group">
                        <label for="left-bkg-color-form" class="col-sm-6 control-label text-left">Background Color:</label>
                        <div class="col-sm-6">
                            <div id="left-bkg-color" class="input-group colorpicker-component">
                                <span class="input-group-addon"><i></i></span>
                                <input type="text" value="" class="form-control input-sm" id="left-bkg-color-form">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="left-height" class="col-sm-4 control-label text-left">Height:</label>
                        <div class="col-sm-8 text-right">
                            <input type="text" class="form-control input-sm" id="left-height" data-slider-min="0" data-slider-max="1000" data-slider-step="10" data-slider-value="0">&nbsp;&nbsp;&nbsp;<small>Height: <span id="left-height-val">auto</span></small>
                        </div>
                    </div>

                </div>
            </div>

            <div id="dd-sidebar-right-exists">
                <h5 class="text-left option-title">Right Sidebar Section</h5>
                <div class="form-horizontal">

                    <div class="form-group">
                        <label for="right-bkg-color-form" class="col-sm-6 control-label text-left">Background Color:</label>
                        <div class="col-sm-6">
                            <div id="right-bkg-color" class="input-group colorpicker-component">
                                <span class="input-group-addon"><i></i></span>
                                <input type="text" value="" class="form-control input-sm" id="right-bkg-color-form">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="right-height" class="col-sm-4 control-label text-left">Height:</label>
                        <div class="col-sm-8 text-right">
                            <input type="text" class="form-control input-sm" id="right-height" data-slider-min="0" data-slider-max="1000" data-slider-step="10" data-slider-value="0">&nbsp;&nbsp;&nbsp;<small>Height: <span id="right-height-val">auto</span></small>
                        </div>
                    </div>

                </div>
            </div>

            <h5 class="text-left option-title">Footer Section</h5>
            <div class="form-horizontal">

                <div class="form-group">
                    <label for="footer-bkg-color-form" class="col-sm-6 control-label text-left">Background Color:</label>
                    <div class="col-sm-6">
                        <div id="footer-bkg-color" class="input-group colorpicker-component">
                            <span class="input-group-addon"><i></i></span>
                            <input type="text" value="" class="form-control input-sm" id="footer-bkg-color-form">
                        </div>
                    </div>
                </div> 

                <div class="form-group">
                    <label for="footer-height" class="col-sm-4 control-label text-left">Height:</label>
                    <div class="col-sm-8 text-right">
                        <input type="text" class="form-control input-sm" id="footer-height" data-slider-min="0" data-slider-max="1000" data-slider-step="10" data-slider-value="0">&nbsp;&nbsp;&nbsp;<small>Height: <span id="footer-height-val">auto</span></small>
                    </div>
                </div>
            </div>
        </div>
        <div class="tools-footer">
            <div class="button-group text-center">
                <button class="btn btn-success btn-sm" data-dismiss="tools" type="button" id="send-message"><span class="glyphicon glyphicon-ok"></span> I'm Done</button>
                <button class="btn btn-warning btn-sm" type="button" id="test"><span class="glyphicon glyphicon-envelope"></span> Send Test</button>
                <button class="btn btn-danger btn-sm" type="button" id="delete"><i class="fa fa-delete fa-5"></i> Delete Project</button>
            </div>
        </div>
    </div>



                </div>
              </div>
           
            </div>

            <!-- table -->   

            </div> <!-- 3tab -->
          </div>
         </div>
      </div>
      <!-- tabcontent -->
   <!-- right -->   
   <!-- modal --> 
   <?php //include('popup_sections.php'); ?>
   <!-- modal -->    
</div>
<!-- common-reduce -->
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<?php include('template_scripts.php');?>

<!-- <div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 99999999999 !important;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style> -->
<script type="text/javascript">

    $(".all-cnewtemp").click(function(){  
      $("#backto_template").show(); 
      $('div#all_tempaltes .template-details').hide();   
      $('.cnew_template').show();
      $("#backto_mytemplate").hide();
    });

     $(".all-mycnewtemp").click(function()
     { 
        $("#backto_mytemplate").show();
        $('div#my_templates .template-details').hide();
        $('.cnew_template').show();
        $("#backto_template").hide();   
        $(".all-mycnewtemp").hide();    
    });

   
</script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script>
   $(document).ready(function() {    

    $("#backto_mytemplate").show();
        $('div#my_templates .template-details').hide();
        $('.cnew_template').show();
        $("#backto_template").hide();   
        $(".all-mycnewtemp").hide();    


       $('#display_service1').DataTable({
         "scrollX": true
     });
   
        $('#display_service2').DataTable({
         "scrollX": true
     });
         $('#display_service3').DataTable({
         "scrollX": true
     });
   
       $('#display_service4').DataTable({
         // "scrollX": true
     }); 
   
        $('#display_service5').DataTable({
         // "scrollX": true
     }); 
   
     
     
     //set button id on click to hide first modal
   $("#hide_category1").on( "click", function() {
           $('#service_edit_1').modal('hide');  
   });
   //trigger next modal
   $("#hide_category1").on( "click", function() {
           $('#add-new-category').modal('show');  
   });
   
   $("#hide_category2").on( "click", function() {
           $('#service_edit_1').modal('show');  
   });
      
   } );

   $(document).ready(function() {
   $( ".choose:first" ).trigger( "click" );
  });

  
</script>
<script type="text/javascript">
     $(document).ready(function() {

      $(window).scroll(function() {
        var sticky = $('#option-tabs'),
          scroll = $(window).scrollTop();
         
        if (scroll >= 40) { 
          sticky.addClass('fixed'); }
        else { 
         sticky.removeClass('fixed');

      }
      });
      $(window).scroll(function() {
       var sticky1 = $('#mail-template, .container-fullscreen'),
         scroll = $(window).scrollTop();
        
       if (scroll >= 40) { 
         sticky1.addClass('fixed1'); }
       else { 
        sticky1.removeClass('fixed1');

      }
      });
    });

     $(document).on('click','.click_event',function(){
      var href=$(this).find('.aaa').attr('href');
      $(href).find('.template-details').show();
      $(".cnew_template").hide();
     });
</script>