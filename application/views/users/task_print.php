<!--  <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool//assets/css/dataTables.css">
 <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/common_style.css?ver=4"> -->
 <!DOCTYPE html>
<html>
  <head>
  <title>CRM </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon"> 
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>

          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <style>
       table#alluser th, table#alluser td {
          /*padding: 10px;*/
          text-align: left;
          white-space: nowrap;
          font-family: 'Roboto', sans-serif;
          font-size: 13px;
          border: 1px solid #eee;
      }
      button.data-turn99.newonoff {
       background: transparent;
       border: none;
      }
      button.print-btn {
          background: #2ea7ec;
          color: #fff;
          border: none;
          padding: 8px 12px 7px;
          margin: 20px 30px;
          border-radius: 3px;
         }
         i.fa.fa-print {
          font-size: 22px;
      }
    </style>
  </head>
  <body>
    <?php $th_css = 'style="border: 1px solid #eee;padding: 10px;font-size: 13px;white-space: nowrap;font-family: "Roboto", sans-serif;text-align: left;"';
    $tr_css = 'style="border: 1px solid #eee;padding: 10px;font-size: 13px;white-space: nowrap;"';
    ?>

<div class="dataTables_scroll">
   <button class="print-btn print_data_cls" ><i class="fa fa-print" aria-hidden="true"></i></button>

   <div class="dataTables_scrollBody" style="position: relative;width: 80%;
    margin: 50px auto;">
       
   <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%" cellpadding="10" style="border: 1px solid #eee;border-collapse:collapse;">
                                <thead>
                                  <tr class="text-uppercase">
                                   <tr class="text-uppercase">
                                                      <th style="border: 1px solid #eee;white-space: nowrap;">
                                                       S.no
                                                      </th>
                                                      <th style="border: 1px solid #eee;white-space: nowrap;">Timer</th>
                                                      <th style="border: 1px solid #eee;white-space: nowrap;">Task Name</th>
                                                      <th style="border: 1px solid #eee;white-space: nowrap;">Start Date</th>
                                                      <th style="border: 1px solid #eee;white-space: nowrap;">Due Date</th>
                                                      <th style="border: 1px solid #eee;white-space: nowrap;">Status</th>
                                                      <th style="border: 1px solid #eee;white-space: nowrap;">Priority</th>
                                                      <th style="border: 1px solid #eee;white-space: nowrap;">Tag</th>
                                                      <th style="border: 1px solid #eee;white-space: nowrap;">Assignees</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php 

                                                   $i =1;
                                                   foreach ($task_list as $key => $value) {

                                                       $ex_val= $this->Common_mdl->get_module_user_data('TASK',$value['id']);
                                
                                                    $var_array=array();
                                                     if(count($ex_val)>0)
                                                             {
                                                              foreach ($ex_val as $key => $value1) {
                                                            
                                                               $user_name = $this->Common_mdl->select_record('user','id',$value1);
                                                                array_push($var_array, $user_name['crm_name']);
                                                               }
                                                              
                                                             }


                                                  $Assignee= implode(',',$var_array);

                                                      error_reporting(0);
                                                 
                                                    if($value['related_to']=='tasks'){

                                                      if($value['worker']=='')
                                                      {
                                                      $value['worker'] = 0;
                                                      }
                                                      $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();

                                                     
                                                     $status_val1='';
                                                    $status_val1=$this->Common_mdl->select_record('task_status','id',$value['task_status']);
                                                    $stat=$status_val1['status_name'];
             


                                                  
                                                      $exp_tag = explode(',', $value['tag']);
                                                      $explode_worker=explode(',',$value['worker']);


                                                      ?>
                                                   <tr id="<?php echo $value['id']; ?>">
                                                      <td  style="border: 1px solid #eee;white-space: nowrap;">
                                                        <?php //echo $i;?>
                                                      </td>
                                                      <td  style="border: 1px solid #eee;white-space: nowrap;"><?php echo date('Y-m-d H:i:s', $value['created_date']);?></td>
                                                      <td style="border: 1px solid #eee;"><?php echo ucfirst($value['subject']);?></td>
                                                      <td style="border: 1px solid #eee;"><?php echo $value['start_date'];?></td>
                                                      <td style="border: 1px solid #eee;"><?php echo $value['end_date'];?></td>
                                                      <td style="border: 1px solid #eee;"><?php echo $stat;?></td>
                                                      <td style="border: 1px solid #eee;"><?php echo $value['priority'];?></td>
                                                      <td style="border: 1px solid #eee;"> <?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                                                            echo $tagName = $this->Common_mdl->getTag($exp_tag_value).' ';
                                                            }?></td>
                                                      <td style="white-space:normal;border: 1px solid #eee;"> <!-- <?php
                                                            foreach($explode_worker as $key => $val){
                                                            $getUserProfilepic = $this->Common_mdl->getUserProfilepic($val);
                                                            ?> -->

                                                             <?php echo $Assignee;?>
                                                             
                                                         <img src="<?php echo $getUserProfilepic;?>" alt="img" height="25" width="25" style="border-radius:50%;border:1px solid #ddd;">
                                                         <?php } ?></td>
                                                     
                                                     
                                                   </tr>

                                                     <?php 
                                                   if($value['sub_task_id']!=''){
                                                    $sub_task=explode(',',$value['sub_task_id']);
                                                      for($i=0;$i<count($sub_task);$i++){
                                                          $sub_task_details=$this->Common_mdl->select_record('add_new_task','id',$sub_task[$i]);

                                                          

                                                            if($sub_task_details['worker']=='')
                                                      {
                                                      $sub_task_details['worker'] = 0;
                                                      }
                                                      $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$sub_task_details['worker'].")")->result_array();
                                                      
                                                            $status_val1='';
                                                    $status_val1=$this->Common_mdl->select_record('task_status','id',$value['task_status']);
                                                    $stat=$status_val1['status_name'];

                                                      $exp_tag = explode(',', $sub_task_details['tag']);
                                                      $explode_worker=explode(',',$sub_task_details['worker']);
                                                      ?>
                                                   <tr id="<?php echo $sub_task_details['id']; ?>">
                                                      <td style="text-align:left;border: 1px solid #ddd;">
                                                        -
                                                      </td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo date('Y-m-d H:i:s', $sub_task_details['created_date']);?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo ucfirst($sub_task_details['subject']);?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo $sub_task_details['start_date'];?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo $sub_task_details['end_date'];?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo $stat;?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"><?php echo $sub_task_details['priority'];?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;"> <?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                                                            echo $tagName = $this->Common_mdl->getTag($exp_tag_value).' ';
                                                            }?></td>
                                                      <td style="text-align:left;border: 1px solid #ddd;">  <?php echo $Assignee;?><!-- <?php
                                                            foreach($explode_worker as $key => $val){
                                                              $query = $this->db->query('select * from user where id='.$val.'')->row_array();
                                                           echo $query['crm_name'].' ';
                                                            ?>
                                                         
                                                         <?php } ?> --></td>                                             
                                                     
                                                     
                                                   </tr>
                                                    <?php   } }                                               ?>       

                                                   
                                                   <?php 
                                                      $i++;}} ?>
                                                   <!-- <tr>
                                                      <td></td>
                                                   </tr> -->
                                                </tbody>
                                             </table>
   </div>
</div>
</body>
</html>
<script>
  $(document).on('click',".print_data_cls",function(){
 

      var divContents = $(".dataTables_scrollBody").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>Users List</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();


  });
</script>