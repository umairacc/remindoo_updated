<?php


function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

                                 if(!empty($leads_rec)){

                                 foreach (array_values($leads_rec) as $leads_rec_key => $value) {

                                  if($leads_rec_key < 10) {
                                      $getUserProfilepic = $this->Common_mdl->getUserProfilepic($value['worker']);                                  
                                    ?>
                              <div class="user-leads1 kanban_block <?php echo $value['subject'];?>" id="<?php echo $value['id'] ?>" >
                           
                                <h3> 
                                    <a href="<?php echo base_url().'user/task_details/'.$value['id'];?>">
                                        <span class="search_word"><?php echo $value['subject'];?> </span>
                                        <!-- <img src="<?php echo $getUserProfilepic;?>" alt="img"> -->
                                    </a>
                                    <div class="kanban-icon-load">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </h3> 
                                <div class="kanban-detail-data-load">
                                <div class="kanban-title-month">
                                    <h3> 
                                        <span class="search_word">
                                        <?php 
                                            $ex_val= $this->Common_mdl->get_module_user_data('TASK',$value['id']);
                                            //$ex_val=Get_Module_Assigees( 'TASK' ,$value['id'] );
                                            $var_array=array();
                                            if(count($ex_val) > 0)
                                                {
                                                foreach ($ex_val as $key => $value1) {

                                                $user_name = $this->Common_mdl->select_record('user','id',$value1);
                                                array_push($var_array, $user_name['crm_name']);
                                                }

                                                }
                                            $Assignee= implode(',',$var_array);

                                            echo $Assignee
                                        ;?> </span>

                                    </h3> 
                                    <div class="source-set1 source-set3 ">
                                        <input type="hidden" id="status_<?php echo $value['id'] ?>" name="" value="<?php echo $ts_key;?>" >
                                        <span class="search_word">Created <?php  $date=date('Y-m-d H:i:s',$value['created_date']); //echo $etime = time() - strtotime($date);;
                                            echo time_elapsed_string($date);?></span>
                                        </div>
                                </div>

                                    <div class="source-google">
                                        <div class="source-set1">
                                        
                                        </div>
                                        
                                                    </div>
                                    <!-- <div class="src-comment">
                                        <?php 
                                        $tsk_res=$this->db->query("select * from task_comments where task_id=".$value['id']."")->result_array();
                                        ?>
                                        <span><i class="fa fa-comments" aria-hidden="true"></i><p class="countclsnew"><?php echo count($tsk_res);?></p></span>
                                        
                                        
                    
                                        <div class="tinytiny">
                                            <strong class="search_word">
                                        <?php  $tag =explode(',', $value['tag']);
                                            foreach ($tag as $t_key => $t_value) { ?>
                                        <a href="javascript:;"><?php if($t_value==2){ echo "To DO"; }if($t_value==1){ echo "Bug";} ?></a><?php } ?>
                                        </strong> 
                                        </div> -->
                                        <!-- end 18-04-2018 -->
                                    <!-- </div> -->
                                </div>
                                    <!-- <div class="kanban-img">
                                        <img class="kanban-img-bot" src="<?php echo $getUserProfilepic;?>" alt="img">
                                    </div> -->
                              </div>
                              <?php }
                              }
                             }else{ ?> 
                          
                              <?php } ?>

<script type="text/javascript">
    $(".kanban-icon-load").click(function(){
        $(this).parent().parent().find(".kanban-detail-data-load").slideToggle();
    });
</script>