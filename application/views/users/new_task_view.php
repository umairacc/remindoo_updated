<?php 
   $this->load->view('includes/new_header'); 
   $client_id = $_GET['client_id'] ?? null;
   
?>
<link href="<?php echo base_url(); ?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/tree_select/style.css?ver=2">
<!-- tinymce editor -->
<!-- <link rel="stylesheet" type="text/css" href="https://www.tiny.cloud/css/codepen.min.css"> -->
<!-- tinymce editor -->

<link href="<?php echo base_url(); ?>assets/css/timepicki.css" rel="stylesheet">
<style type="text/css">
   .checkbox-error label::before {
      border: 2px solid red !important;
   }

   .position-alert1 h4 {
      border-bottom: 1px solid #ddd;
      padding-bottom: 10px;
      margin-bottom: 15px;
   }

   #summary_new p {
      border-bottom: 1px solid #ddd;
      padding: 10px 0px;
      margin: 0;
      color: green;
   }

   #summary_val p {
      color: green;
   }

   label.remider_option_error.error {
      text-align: center;
      padding-left: 0 !important;
   }

   .picker-category.form-group.border-checkbox-section.form-radio {
      padding: 0px 15px;
   }

   .inc_exc_error {
      text-align: center;
      padding-left: 0 !important;
      color: red;
   }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/custom/search-scroll.css">
<?php

// $team = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();
$service_filter = $assign_group = array();

$service_filter = $this->db->query("select `services` from admin_setting where firm_id=" . $_SESSION['firm_id'] . " ")->row_array();
$service_filter = json_decode($service_filter['services'], true);

$service_filter = array_keys(array_diff($service_filter, array(0)));

$department = $this->db->query("select * from department_permission where create_by=" . $_SESSION['id'] . " ")->result_array();

$timeline_task_section = isset($timeline_task_section) ? $timeline_task_section : '';
$task_timeline_description = '';
$task_timeline_comapny = '';

if ($timeline_task_section != '') {
   $timeline_task_section = explode('_', $timeline_task_section);

   if ($timeline_task_section[0] == 'notes') {
      $get_data_timeline = $this->db->query('select * from timeline_services_notes_added where id=' . $timeline_task_section[1])->row_array();

      $service_type              = $get_data_timeline['services_type'];
      $task_timeline_description = $get_data_timeline['notes'];
      $timeline_user             = $get_data_timeline['user_id'];
   } else {
      $get_log_data = $this->db->get_where('activity_log', "id=" . $timeline_task_section[1])->row_array();
      $task_timeline_description  = $get_log_data['log'];
      $timeline_user              = $get_log_data['user_id'];
   }
   if ($timeline_user != 0) {
      $records['company_id'] = $this->Common_mdl->get_field_value('client', 'id', 'user_id', $timeline_user);
      $records['firm_id'] = $this->Common_mdl->get_field_value('client', 'firm_id', 'user_id', $timeline_user);
      $timeline_user = $records['company_id'];
      $sql_val = $this->db->query("SELECT * FROM firm_assignees where  module_id = " . $records['company_id'] . " AND module_name='CLIENT' ")->result_array();

      $assign_group = array_column($sql_val, 'assignees');
   }
}

$noti_task_section = isset($noti_task_section) ? $noti_task_section : '';
// $task_timeline_description='';
// $task_timeline_comapny='';
if ($noti_task_section != '') {
   $get_data_timeline = $this->db->query('select *  from notification_management where id="' . $noti_task_section . '" ')->row_array();

   $get_task_data = $this->db->query('select * from  add_new_task where id=' . $get_data_timeline['task_id'] . ' ')->row_array();
   $task_timeline_description = $get_data_timeline['content'];

   $records['company_id'] = (isset($get_task_data['company_name']) && $get_task_data['company_name'] != '') ? $get_task_data['company_name'] : '';
   $timeline_user = $records['company_id'];

   $sql_val = $this->db->query("SELECT * FROM firm_assignees where  module_id = " . $get_data_timeline['task_id'] . " AND module_name='TASK'")->result_array();

   $assign_group = array_column($sql_val, 'assignees');
}

$user_id = '';

if (isset($timeline_user)) {
   $user_id = $timeline_user;
}

if ($client_id) {
   $client = $this->db->query("SELECT * FROM client WHERE user_id = {$client_id}")->row_array();
   $user_id = $client['id'];
}

?>

<div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
   <div class="newupdate_alert">
      <a href="#" class="close" id="close_info_msg">×</a>
      <div class="pop-realted1">
         <div class="position-alert1">
            Please! Select Record...
         </div>
      </div>
   </div>
</div>

<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body rem-tasks">
               <div class="row">
                  <!--start-->
                  <div class="col-sm-12 common_form_section2">
                     <div class="modal-alertsuccess alert alert-success" style="display:none;">
                        <div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 Your task was successfully added.
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="modal-alertsuccess  alert alert-danger" style="display:none;">
                        <div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 Please fill the required fields.
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- model success msg contect -->
                     <div class="modal-alertsuccess alert alert-danger-check succ dashboard_success_message1" style="display:none;">
                        <div class="newupdate_alert"> <a href="#" class="close" data-dismiss="modal">×</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 <h4>Summary</h4>
                                 <div id="summary_new"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- end of success msg content -->
                     <div class="page-heading">Create New Task</div>
                     <form id="add_new_task" method="post" action="" enctype="multipart/form-data">
                        <div class="deadline-crm1 floating_set page-tabs">
                           <ul id="task_active_wrap" class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard client-top-nav create-nav-btns">

                           <li class="nav-item">
                              <a class="nav-link" href="<?php echo base_url(); ?>user/task_list">All Tasks</a>
                              <div class="slide"></div>
                           </li>

                           <?php if ($_SESSION['permission']['Task']['create'] == 1) { ?>
                              <li class="nav-item">
                                 <a class="nav-link active" href="<?php echo base_url(); ?>user/new_task">Create task</a>
                                 <div class="slide"></div>
                              </li>

                              <!-- <li class="nav-item">
                                 <a class="nav-link " href="<?php echo base_url(); ?>task">Task Timeline</a>
                                 <div class="slide"></div>
                              </li> -->

                           <?php }
                           if ($_SESSION['user_type'] == 'FA') { ?>
                              <li class="nav-item">
                                 <a class="nav-link " href="<?php echo base_url(); ?>Task_Status/task_progress_view">Task Progress Status</a>
                                 <div class="slide"></div>
                              </li>
                           <?php }

                           if ($_SESSION['permission']['Task']['edit'] == 1) { ?>
                              
                              <li class="nav-item">
                                 <a class="nav-link " href="<?php echo base_url(); ?>user/task_list_kanban">Switch To Kanban</a>
                                 <div class="slide"></div>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link " href="<?php echo base_url(); ?>user/task_list/archive_task">Archive Task</a>
                                 <div class="slide"></div>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link " href="<?php echo base_url(); ?>user/task_list/complete_task">Completed Task</a>
                                 <div class="slide"></div>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" data-toggle="modal" data-target="#import-task">Import tasks</a>
                                 <div class="slide"></div>
                              </li>
                              <?php } ?>
                           </ul>
                        </div>
                        <div class="public-bill01 adnewtsk <?php if ($_SESSION['permission']['Task']['create'] != '1') { ?> permission_deined <?php } ?>">
                           <div class="add_new_post04">
                              <div class="row create-bill-wrapper">
                                 <div class="col-md-2 create-reset-btn">
                                    <div class="p-t-2 task-form-btn create-btn new_task-bts1 nw-tsk f-right text-right">
                                       <!-- <input type="submit" name="add_task" class="btn btn-succuess" value="create" /> -->
                                       <input type="button" name="button" value="reset" onclick="clear_form_elements('#add_new_task')">
                                    </div>
                                 </div>
                                 <div class="page-data-block col-md-10 attach-public-block">
                                    <div class="rem-card">
                                       <div class="col-sm-12 attached-popup1">
                                          <div class="form-group check-options">
                                             <div class="checkbox-color3" style="display: none;">
                                                <label class="custom_checkbox1">
                                                   <input type="checkbox" name="public" value="Public" id="p-check">
                                                   <i></i> </label>
                                                <span>Public </span>
                                             </div>

                                             <div class="checkbox-color3">
                                                <label class="custom_checkbox1"><input type="checkbox" name="billable" value="Billable" checked id="p-check1"><i></i></label>
                                                <span>Billable</span>
                                             </div>
                                          </div>

                                          <!-- 16-06-2018 we remove multiple="multiple" -->
                                          <div class="form-group attach-files f-right">
                                             <div id="input_container" style="width: 0px; height: 0px; overflow: hidden">
                                                <input type="file" id="inputfile" name="attach_file[]" multiple="multiple" onchange="readURL(this);" />
                                             </div>
                                             <div class="button" onclick="uploadss();">attach file</div>
                                             <span id="preview_image_name"></span>
                                             <!--  <img src="<?php echo base_url() . 'uploads/profile_image/avatar' ?>" id="preview_image" > -->
                                          </div>

                                          <!-- end of 16-06-2018 -->
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row subject-desc-wrapper">
                                 <div class="page-data-block col-md-6">
                                    <div class="rem-card">
                                       <div class="space-equal-data spaceleftcls attach-task-new">
                                          <input type="hidden" name="timeline_task_id" id="timeline_task_id" value="<?php if (isset($timeline_task_section)) {
                                                                                                                        echo $timeline_task_section;
                                                                                                                     } ?>">

                                          <input type="hidden" name="timeline_task_id" id="timeline_task_id" value="<?php if (isset($timeline_task_section)) {
                                                                                                                        echo $timeline_task_section;
                                                                                                                     } ?>">
                                          <!--</div> -->
                                          <div class="form-group">
                                             <label>Subject</label>
                                             <input type="text" class="form-control clr-check-client" name="subject" placeholder="Enter Subject" value="">
                                          </div>
                                          <div class="row new-tsk01">
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                   <label>Start Date</label>
                                                   <div class="input-group date">
                                                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input type='text' class="clr-check-client" id='txtFromDate' name="startdate" placeholder="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>" />
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-sm-6">
                                                <div class="form-group ">
                                                   <label>End Date</label>
                                                   <div class="input-group date">
                                                      <span class="input-group-addon "><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input type='text' class="clr-check-client" id='txtToDate' name="enddate" placeholder="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>" />
                                                   </div>
                                                </div>
                                             </div>

                                             <div class="form-group widthfifty">
                                                <label>Priority</label>
                                                <select name="priority" class="form-control clr-check-select" id="priority">
                                                   <option value="low">Low</option>
                                                   <option value="medium">Medium</option>
                                                   <option value="high">High</option>
                                                   <option value="super_urgent">Super Urgent</option>
                                                </select>
                                             </div>

                                             <div class="form-group widthfifty">
                                                <label>Task status*</label>
                                                <select name="task_status" class="form-control clr-check-select" id="task_status">
                                                   <?php foreach ($task_status as $key => $status_val) { ?>
                                                      <option value="<?php echo $status_val['id']; ?>"><?php echo $status_val['status_name']; ?></option>

                                                   <?php } ?>

                                                   <option value="archive" <?php if (isset($_SESSION['sta_tus']) &&  $_SESSION['sta_tus'] == 'archive') { ?> selected="selected" <?php } ?>>Archive</option>

                                                </select>
                                             </div>

                                             <!-- for realated to services 16-10-2018 -->
                                             <div class="form-group widthfifty even">
                                                <label>Related to Service</label>
                                                <div class="dropdown-sin-5 lead-form-st">
                                                   <select class="form-control clr-check-select-cus" name="related_to_services" id='related_to_services' placeholder="Select any Services">
                                                      <option value="">None</option>
                                                      <option disabled>Services</option>
                                                      <?php
                                                      $work_flow = array();
                                                      $settings_val = $this->db->query('select * from service_lists where id !=16 ')->result_array();

                                                      $work_flow = $this->Service_model->get_firm_workflow();

                                                      foreach ($settings_val as $set_key => $set_value) {
                                                         if (in_array($set_value['id'], $service_filter)) {
                                                      ?>
                                                            <option value="ser_<?php echo $set_value['id']; ?>" <?php if (isset($service_type) && $service_type != '') {
                                                                                                                     if (strtolower($service_type) == $set_value['id']) { ?> selected="selected" <?php }
                                                                                                                                                                                             } ?>><?php echo $set_value['service_name']; ?></option>
                                                      <?php
                                                         }
                                                      }
                                                      ?>
                                                      <option disabled>Work Flow</option>
                                                      <?php
                                                      if (count($work_flow) > 0) {
                                                         foreach ($work_flow as $key => $wf_value) { ?>
                                                            <option value="wor_<?php echo $wf_value['id']; ?>"><?php echo $wf_value['service_name']; ?></option>
                                                      <?php }
                                                      }
                                                      ?>
                                                   </select>
                                                </div>

                                                <a href="javascript:;" id="exc_service_button" data-target="#exc_service_popup" data-toggle='modal' style="display: none;" class="exc-btn">Exclude Steps</a>

                                             </div>

                                             <div class="form-group">
                                                <label for="estimated_time">Estimated Time</label>
                                                <input type="number" class="clr-check-select" name="estimated_time" id="estimated_time" value="" min="0"
                                                   placeholder="Estimated no. of hours">
                                             </div>

                                             <div class="form-group">
                                                <label>Choose Client/Firm</label>
                                                <div class="dropdown-sin-5">
                                                   <select class="form-control clr-check-select-cus" name="company_name" id="company_name">
                                                      <!-- <input type="text" name="company" id="company"> -->
                                                      <option value="">Select Company</option>
                                                      <?php
                                                      $query1 = $this->db->query("SELECT * FROM `user` where autosave_status!='1' and user_type='FC' and crm_name!='' and firm_id=" . $_SESSION['firm_id'] . " order by id DESC");

                                                      $results1 = $query1->result_array();
                                                      $res = array();
                                                      foreach ($results1 as $key => $value) {
                                                         array_push($res, $value['id']);
                                                      }
                                                      if (!empty($res)) {
                                                         $im_val = implode(',', $res);
                                                         // echo $im_val;
                                                         $query = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (" . $im_val . ") order by id desc ");
                                                         $results = $query->result_array();
                                                      } else {
                                                         $results = 0;
                                                         $query = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id=" . $_SESSION['id'] . " order by id desc ");
                                                         $results = $query->result_array();
                                                      }
                                                      if (count($results) > 0) {
                                                         foreach ($results as $key => $val) { 
                                                            $is_client_selected = '';

                                                            // if (isset($_GET['client_id']) && $val['id'] == $_GET['client_id']) {
                                                            //    $is_client_selected = 'selected';
                                                            // }

                                                            if ( (isset($records['company_id']) && ($records['company_id'] == $val['id'])) 
                                                            || $client_id == $val['user_id']  ) {
                                                               $is_client_selected = 'selected';
                                                            }
                                                            ?>
                                                            <option value="<?php echo $val['id']; ?>" <?php echo $is_client_selected; ?>><?php echo $val["crm_company_name"]; ?></option>
                                                      <?php
                                                         }
                                                      }  ?>
                                                   </select>
                                                </div>
                                                <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id ?>">
                                             </div>

                                             <div class="new_task_div" id="tree_div1">

                                                <!-- end of related services 16-10-2018 -->
                                                <div id="base_projects"></div>
                                                <div class="form-group widthfifty">
                                                   <label>Assign to(worker)</label>
                                                   <input type="text" id="tree_select1" class="tree_select" name="assignees[]" placeholder="Select">
                                                   <input type="hidden" id="assign_role" name="assign_role">
                                                   <!-- </div> -->
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="page-data-block col-md-6">
                                    <div class="rem-card">
                                       <div class="space-equal-data">
                                          <div class="form-group">
                                             <label>Description</label>
                                             <textarea rows="5" type="text" class="form-control" name="description" id="description" placeholder="Write something"><?php echo $task_timeline_description; ?></textarea>
                                          </div>

                                          <div class="form-group recurring-create-btn">
                                             <div>
                                                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Make this recuring task</button>
                                                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalone">Remind me about the task</button>
                                             </div>
                                             <div>
                                                <input type="submit" name="add_task" class="btn btn-succuess input-create" value="create" />
                                             </div>
                                             </div>
                                          <div class="form-group widthfifty even tag-design">
                                             <h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
                                             <input type="text" value="" name="tags" id="tags" class="tags" />
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                                 <!-- col-sm-6-->
                                 <!-- Modal -->
                                 <div class="modal fade recurring-msg common-schedule-msg1" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
                                    <div class="modal-dialog modal-recurring-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header modal-recurring-header">
                                             <!-- <button type="button" data-dismiss="modal" class="close">&times;</button> -->
                                             <h4 class="modal-title">Schedule Recurring Task</h4>
                                          </div>
                                          <div class="modal-body modal-recurring-body">

                                             <div class="row">
                                                <div class="col-sm-6 form-group common-repeats1 ">
                                                   <label class="label-column1 col-form-label">From:</label>
                                                   <div class="label-column2 input-group date">
                                                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input type='text' class="required1" id='modalFromDate' name="sche_startdate" placeholder="dd-mm-yyyy" />
                                                   </div>
                                                </div>
                                                <div class="col-sm-6 form-group common-repeats1 ">
                                                   <label class="label-column1 col-form-label">Create time:</label>
                                                   <div class="label-column2  input-group bootstrap-timepicker timepicker">
                                                      <input id="timepicker1" type="text" class="form-control input-small required1" name="sche_send_time">
                                                      <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span> -->
                                                   </div>
                                                </div>
                                                <div class="form-group col-sm-6 every-week01 floating_set">
                                                   <label class="label-column1  col-form-label">Every:</label>
                                                   <div class="label-column2">
                                                      <!-- <input type="text" class="form-control week-text" name="every" placeholder="" value=""> -->
                                                      <input type="number" class="form-control week-text required1" name="every" min='1' value="1" id="every" placeholder="" value="">
                                                      <!-- <p>weeks</p> -->
                                                   </div>
                                                </div>

                                                <div class="form-group col-sm-6">
                                                   <label class="label-column1 col-form-label">Repeats Type:</label>
                                                   <div class="label-column2">
                                                      <select name="repeats" class="form-control repeats" id="repeats" data-date="" data-day="" data-month="">
                                                         <option value="day">Day</option>
                                                         <option value="week">Week</option>
                                                         <option value="month">Month</option>
                                                         <option value="year">Year</option>
                                                      </select>
                                                   </div>
                                                </div>

                                                <div id="weekly" class="every_popup1" style="display: none;">
                                                   <div class="form-group days-list floating_set ">
                                                      <label class="label-column1 col-form-label">On:</label>
                                                      <div class="label-column2">
                                                         <div class="days-left">
                                                            <div class="checkbox-color checkbox-primary">
                                                               <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Monday" id="day-check2"><label class="border-checkbox-label" for="day-check2">Mon</label>
                                                            </div>
                                                            <div class="checkbox-color checkbox-primary">
                                                               <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Tuesday" id="day-check3"><label class="border-checkbox-label" for="day-check3">Tue</label>
                                                            </div>
                                                            <div class="checkbox-color checkbox-primary">
                                                               <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Wednesday" id="day-check4"><label class="border-checkbox-label" for="day-check4">Wed</label>
                                                            </div>
                                                            <div class="checkbox-color checkbox-primary">
                                                               <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Thursday" id="day-check5"><label class="border-checkbox-label" for="day-check5">Thur</label>
                                                            </div>
                                                            <div class="checkbox-color checkbox-primary">
                                                               <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Friday" id="day-check6"><label class="border-checkbox-label" for="day-check6">Fri</label>
                                                            </div>
                                                            <div class="checkbox-color checkbox-primary">
                                                               <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Saturday" id="day-check7"><label class="border-checkbox-label" for="day-check7">Sat</label>
                                                            </div>
                                                            <div class="checkbox-color checkbox-primary">
                                                               <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Sunday" id="day-check1">
                                                               <label for="day-check1" class="border-checkbox-label">Sun</label>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <label class="error empty-week-day"></label>
                                                   </div>
                                                </div>
                                                <div class="col-sm-12  form-group left-align radio_button01">
                                                   <label class="label-column1 left-label">Ends:</label>
                                                   <div class="forms-option label-column2">
                                                      <div class="radio-width01">
                                                         <input type="radio" name="end" value="after" id="end-date">
                                                         <label for="end-date">After</label>
                                                         <!-- <input type="text" class="form-control message-count" name="message" placeholder="" value="" > -->
                                                         <input type="number" class="form-control message-count" id="message" name="message" placeholder="" value="">
                                                         <span>messages</span>
                                                      </div>
                                                      <div class="radio-width01 on-optin">
                                                         <input type="radio" name="end" value="on" id="end-date3">
                                                         <label for="end-date3">On </label>
                                                         <div class="input-group date">
                                                            <!-- <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
                                                            <input type='text' class="" id='modalendDate' name="sche_on_date" data-year="" data-day="" data-month="" />
                                                         </div>
                                                      </div>
                                                      <div class="no-end radio-width01">
                                                         <input type="radio" name="end" value="noend" id="end-date1" checked="checked">
                                                         <label for="end-date1">No end date</label>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class=" left-align col-sm-12">
                                                   <label class="label-column1 left-label">Summary:</label>
                                                   <div class="label-column2">
                                                      <p id="summary"></p>
                                                      <input type="hidden" name="recuring_sus_msg" id="recuring_sus_msg">
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="modal-footer modal-recurring-footer">
                                             <button type="button" name="button" class="btn btn-default Save_Schedule disabled">Schedule</button>
                                             <button type="button" data-dismiss="modal" class="close">Cancel</button>
                                             <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> -->
                                             <!-- <button type="button" data-dismiss="modal" >Close</button> -->
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <!-- modal close-->
                                 <!-- Modal one-->
                                 <div class="modal fade common-schedule-msg1 remind-me-picker" id="myModalone" role="dialog">
                                    <div class="modal-dialog modal-remind-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header modal-remind-header">
                                             <button type="button" data-dismiss="modal" class="close">&times;</button>
                                             <h4 class="modal-title">Remind Me</h4>
                                          </div>
                                          <div class="modal-body modal-remind-body">
                                             <div class="picker-category form-group border-checkbox-section form-radio ">
                                                <label class="remider_option_error error"></label>
                                                <!-- display: none; for no functionlity to this -->
                                                <div class="hour-rate1" style="display: none;">
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" name="radio" id="repeat1" value="nobody"><label for="repeat1">Only if nobody responds</label>
                                                   </div>
                                                </div>
                                                <div class="hour-rate1">
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" name="radio" id="repeat2" value="1hour"><label for="repeat2">In 1 hour</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" name="radio" id="repeat3" value="2hour"><label for="repeat3">In 2 hour</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" name="radio" id="repeat4" value="4hour"><label for="repeat4">In 4 hour</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" name="radio" id="repeat5" value="5hour"><label for="repeat5">In 6 hour</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" name="radio" id="repeat6" value="8hour"><label for="repeat6">In 8 hour</label>
                                                   </div>
                                                </div>
                                                <div class="hour-rate1">
                                                   <div class="checkbox-color checkbox-primary marricls">
                                                      <input type="checkbox" name="radio" id="repeat7" class="remind_poup_data" value="tomorrow_morning"><label for="repeat7">Tomorrow Morning </label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" value="tomorrow_afternoon" name="radio" id="repeat8"><label for="repeat8">Tomorrow Afternoon </label>
                                                   </div>
                                                </div>
                                                <div class="hour-rate1">
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat9" class="remind_poup_data" value="1day"><label for="repeat9">In 1 day</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat10" class="remind_poup_data" value="2day"><label for="repeat10">In 2 days</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat11" class="remind_poup_data" value="4day"><label for="repeat11">In 4 days</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat12" class="remind_poup_data" value="6day"><label for="repeat12">In 6 days</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat13" class="remind_poup_data" value="8day"><label for="repeat13">In 8 days</label>
                                                   </div>
                                                </div>
                                                <div class="hour-rate1">
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat14" class="remind_poup_data" value="1week"><label for="repeat14">In 1 week</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat15" class="remind_poup_data" value="2week"><label for="repeat15">In 2 weeks</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat16" class="remind_poup_data" value="4week"><label for="repeat16">In 4 weeks</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat17" class="remind_poup_data" value="6week"><label for="repeat17">In 6 weeks</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat18" class="remind_poup_data" value="8week"><label for="repeat18">In 8 weeks</label>
                                                   </div>
                                                </div>
                                                <div class="hour-rate1">
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat19" class="remind_poup_data" value="1month"><label for="repeat19">In 1 month</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat20" class="remind_poup_data" value="2month"><label for="repeat20">In 2 months</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat21" class="remind_poup_data" value="4month"><label for="repeat21">In 4 months</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat22" class="remind_poup_data" value="6month"><label for="repeat22">In 6 months</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat23" class="remind_poup_data" value="8month"><label for="repeat23">In 8 months</label>
                                                   </div>
                                                </div>
                                             </div>
                                             <input type="hidden" name="for_reminder_chk_box" id="for_reminder_chk_box">
                                             <!-- form-group -->
                                             <div class="picker-category exam-picker">
                                                <h3>
                                                   At a specific time <span>(Examples: "Wednesday", "3 days from now")</span></h2>
                                                   <div class="specific-picker">
                                                      <div class="form-group">
                                                         <label class="col-form-label">Starts:</label>
                                                         <div class="control-start1">
                                                            <div class="input-group date">
                                                               <span class="input-group-addon "><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input type='text' class="form-control required_check" id='specific_time' name="specific_time"           
                                                               placeholder="dd-mm-yyyy" />
                                                            </div>
                                                         </div>
                                                      </div>

                                                      <div class="form-group">
                                                         <label class="col-form-label">Send Time:</label>
                                                         <div class="input-group control-start1">
                                                            <input type="text" name="specific_timepic" id="timepicker2" class="form-control  required_check">
                                                         </div>
                                                      </div>

                                                   </div>
                                                   <div class="col-summar">
                                                      <label>Summary:</label>
                                                      <!-- <strong>12 weeks, onWednesday at 7:26: PM</strong> -->
                                                      <p id="summary_val"></p>
                                                      <input type="hidden" name="reminder_msg" id="reminder_msg">
                                                   </div>
                                             </div>
                                          </div>
                                          <!-- modal-body -->
                                          <div class="modal-footer modal-remind-footer">
                                             <!-- <button type="button" name="button"  class="btn btn-default">Confirm</button> -->
                                             <button type="button" data-dismiss="modal">Close</button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- modal close-->
                              </div>
                              <!-- service exclude popup-->
                              <div class="modal fade step_accordion-popup" id="exc_service_popup">
                                 <div class="modal-dialog modal-service-dialog">
                                    <div class="modal-content">
                                       <div class="modal-header modal-service-header">
                                          <h4 style="display: inline-block;"> Exclude Services Steps</h4>
                                          <button type="button" class="close" data-dismiss="modal">×</button>
                                       </div>
                                       <div class="card-block accordion-block">
                                       </div>
                                       <div class="modal-footer modal-service-footer">
                                          <a href="#" data-dismiss="modal">Close</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- service exclude popup-->
                     </form>
                  </div>
               </div>
               <!-- close -->
            </div>
         </div>
         <!-- Page body end -->
      </div>
   </div>
   <!-- Main-body end -->
   <div id="styleSelector">
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="import-task" role="dialog">
   <div class="modal-dialog modal-import-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header modal-import-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Import Task</h4>
         </div>
         <div class="modal-body <?php if ($_SESSION['permission']['Task']['create'] != '1') { ?> permission_deined <?php } ?>">
            <!-- content update after page load source from tasks/import viewfile -->
         </div>
      </div>
   </div>
</div>

<?php $this->load->view('includes/session_timeout'); ?>

<!-- Warning Section Ends -->
<!-- Required Jquery -->
<div class="modal fade recurring-msg common-schedule-msg1" id="import-task12" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Import CSV File Data</h4>
         </div>
         <div class="modal-body">
         </div>
         <div class="modal-footer">
            <button type="button" name="button" class="btn btn-default submitBtn">Confirm</button>
            <a href="#" data-dismiss="modal">Cancel</a>
         </div>
      </div>
   </div>
</div>
<?php $this->load->view('includes/footer'); ?>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>


<!-- tinymce editor -->
<!-- <script src="<?php echo base_url(); ?>assets/js/tinymce.min.js"></script> -->
<!-- tinymce editor -->

<!-- 17-09-2018 -->

<!-- end of 17-09-2018 -->

<script type="text/javascript">
   $('.tags').tagsinput({
      allowDuplicates: true
   });
</script>
<script src="<?php echo base_url(); ?>assets/js/timepicki.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
   $('.dropdown-sin-4').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });
   $('.dropdown-sin-5').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

   /*timepic trigger for on change function */
   $('#timepicker1').timepicki({
      start_time: ["10", "00", "AM"],
      step_size_minutes: 5,
      on_change: function(elm) {
         $(elm).trigger('change');
      }
   });

   $('#timepicker2').timepicki({
      start_time: ["10", "00", "AM"],
      step_size_minutes: 5,
      on_change: function(elm) {
         $(elm).trigger('change');
      }
   });
   /**/
</script>
<script>
   var Random = Mock.Random;
   var json1 = Mock.mock({
      "data|10-50": [{
         name: function() {
            return Random.name(true)
         },
         "id|+1": 1,
         "disabled|1-2": true,
         groupName: 'Group Name',
         "groupId|1-4": 1,
         "selected": false
      }]
   });

   $('.dropdown-mul-1').dropdown({
      data: json1.data,
      limitCount: 40,
      multipleMode: 'label',
      choice: function() {
         // console.log(arguments,this);
      }
   });

   var json2 = Mock.mock({
      "data|10000-10000": [{
         name: function() {
            return Random.name(true)
         },
         "id|+1": 1,
         "disabled": false,
         groupName: 'Group Name',
         "groupId|1-4": 1,
         "selected": false
      }]
   });

   $('.dropdown-mul-2').dropdown({
      limitCount: 5,
      searchable: false
   });

   $('.dropdown-sin-1').dropdown({
      readOnly: true,
      input: '<input type="text" maxLength="20" placeholder="Search">'
   });

   $('.dropdown-sin-2').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
   });

   $('.dropdown-sin-255').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
   });

   $('.dropdown-sin-3').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
   });
</script>

<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script>
   $(document).ready(function() {
      /* tinymce editor*/
      tinymce_config['images_upload_url'] = "<?php echo base_url(); ?>Firm/uplode_mail_header_image";
      tinymce_config['selector'] = '#description';

      tinymce.init(tinymce_config);
      /* tinymce editor*/

      /*
        $('input[id$="timepicker1"]').inputmask({
          mask: "h:s t\\m",
          placeholder: "hh:mm am",
          alias: "datetime",
          hourFormat: "12"
      });*/
      $('input[name$="text"]').inputmask({
         mask: "h:s t\\m",
         placeholder: "hh:mm am",
         alias: "datetime",
         hourFormat: "12"
      });
      // $('#timepicker1').timepicker();
      $("#txtFromDate").datepicker({
         dateFormat: 'dd-mm-yy',
         changeMonth: true,
         changeYear: true,
         numberOfMonths: 1,
         minDate: 0,
         onSelect: function(selected) {
            $("#txtToDate").datepicker("option", "minDate", selected)
         }
      });
      $("#txtToDate").datepicker({
         dateFormat: 'dd-mm-yy',
         changeMonth: true,
         changeYear: true,
         numberOfMonths: 1,
         minDate: 0,
         onSelect: function(selected) {
            $("#txtFromDate").datepicker("option", "maxDate", selected)
         }
      });


   });




   $(document).ready(function() {

      $('#accordion_close').on('click', function() {
         $('#accordion').slideToggle(300);
         $(this).toggleClass('accordion_down');
      });


      $("#repeats").change(function() {

         $('#weekly').find("input[type='checkbox']").each(function() {
            $(this).prop("checked", false);
         });

         var vals = $(this).val();

         if (vals == 'week') {
            $('#weekly').show();
         } else {
            $('#weekly').hide();
         }


      });




      $(document).ready(function() {
         $(".dob_picker").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: 0,
            changeMonth: true,
            changeYear: true,
         }).val();
         //$(document).on('change','.othercus',function(e){
      });



   });

   <?php if (!empty($_GET['c'])) { ?>

      $(document).ready(function() {
         $('[name="company_name"]').parent().find('.dropdown-main ul li[data-value="' + <?php echo $_GET['c']; ?> + '"]').trigger('click');
         $('#user_id').val(<?php echo $_GET['c']; ?>);
      });

   <?php } ?>
</script>
<script src="<?php echo base_url() ?>assets/js/task/recurring_reminder.js"></script>
<script type="text/javascript">
   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-36251023-1']);
   _gaq.push(['_setDomainName', 'jqueryscript.net']);
   _gaq.push(['_trackPageview']);

   (function() {
      var ga = document.createElement('script');
      ga.type = 'text/javascript';
      ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(ga, s);
   })();
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
      $.validator.setDefaults({
         ignore: []
      });
   });
   $(document).ready(function() {

      $("#add_new_task").validate({

         ignore: false,

         errorPlacement: function(error, element) {
            if (element.attr("name") == "company_name")
               error.insertAfter(".dropdown-sin-4");
            else if (element.attr("name") == "worker[]")
               error.insertAfter(".dropdown-sin-2");
            else if (element.attr("name") == "manager[]")
               error.insertAfter(".dropdown-sin-3");
            else
               error.insertAfter(element);
         },
         rules: {
            // task_name:{required:true},
            subject: {
               required: true
            },
            company_name: {
               required: true
            },
            task_status: {
               required: true
            },
            startdate: {
               required: true
            },
            enddate: {
               required: true
            },
            "assignees[]": "required",

         },
         errorElement: "span",
         errorClass: "field-error",
         messages: {
            task_name: "Required Field",
            subject: "Required field",
            company_name: "Required field",
            task_status: "Required Field",
            startdate: "Required Field",
            enddate: "Required Field",
            "assignees[]": "Required Field",

         },
         submitHandler: function(form) {

            var new_array = [];
            var Assignees = [];
            $('input[class="remind_poup_data"]:checked').each(function() {
               new_array.push(this.value);
            });

            var for_reminder_chk = new_array.join(',');
            $('#for_reminder_chk_box').val(for_reminder_chk);

            $('.comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
               var id = $(this).closest('.comboTreeItemTitle').attr('data-id');
               var id = id.split('_');
               Assignees.push(id[0]);
            });
            //  data['assignee'] = Assignees

            var assign_role = Assignees.join(',');
            $('#assign_role').val(assign_role);

            var formData = new FormData($("#add_new_task")[0]);

            var description_value = tinyMCE.get('description').getContent();
            formData.append("description_value", description_value);

            //console.log(formData);return;
            //  $(".LoadingImage").show();

            $.ajax({
               url: '<?php echo base_url(); ?>user/add_new_task/',
               type: 'POST',
               data: formData,
               contentType: false,
               processData: false,
               beforeSend: function() {
                  $(".LoadingImage").show();
               },
               success: function(data) {
                  //alert(data);
                  //  aldateert(data);
                  if (parseInt(data)) {
                     //  location.reload(true);
                     $(".LoadingImage").hide();
                     $(".popup_info_msg").show();
                     $(".popup_info_msg .position-alert1").html("Task Added successfully..");
                     setTimeout(function() {
                        window.location.replace("<?php echo base_url(); ?>user/task_list");
                     }, 1000);
                  }
               },
               error: function() {
                  $('.alert-danger').show();
                  $('.alert-success').hide();
               }
            });
            return false;
         }
      });
   });
</script>
<script type="text/javascript">
   function uploadss() {
      $('#inputfile').click();
   }
</script>
<script>
   $(document).ready(function() {
      $('#search_company').keyup(function() {
         // alert('text');
         var query = $(this).val();
         if (query != '') {
            $.ajax({
               url: "<?php echo base_url(); ?>user/search_companies/",
               method: "POST",
               data: {
                  query: query
               },
               success: function(data) {
                  $('#searchresult').fadeIn();
                  $('#searchresult').html(data);
               }
            });
         }
         if (query == '') {
            $('#searchresult').fadeOut();
         }
      });
      $(document).on('click', 'li', function() {
         $('#search_company').val($(this).text());
         $('#user_id').val($(this).data('id'));
         $('#searchresult').fadeOut();
      });
   });
</script>
<script type="text/javascript">
   function readURL(input) {

      var zz = input.files.length;
      var fruits = [];
      var fruits_new = [];
      var i = 0
      if (zz > 0) {
         for (i = 0; i < zz; i++) {
            // alert(i);
            if (input.files && input.files[i]) {
               // $('#sample').append('<input type="file" name="attach_file[]" id="sample" value="'+input+'">');
               var reader = new FileReader();
               fruits.push(input.files[i].name);
               fruits_new.push(input.files[i]);

            }
         }
      }
      var join = fruits.join(',');
      $('#preview_image_name').html(join);
   }


   $(document).ready(function() {


      var date = $('.datepicker').datepicker({
         dateFormat: 'dd-mm-yy',
         minDate: 0,
         changeMonth: true,
         changeYear: true,
      }).val();

      $('.dropdown-sin-25').dropdown({
         //limitCount: 5,
         input: '<input type="text" maxLength="20" placeholder="Search">'
      });
      $('.dropdown-sin-27').dropdown({
         //limitCount: 5,
         input: '<input type="text" maxLength="20" placeholder="Search">'
      });
      $('.dropdown-sin-29').dropdown({
         //limitCount: 5,
         input: '<input type="text" maxLength="20" placeholder="Search">'
      });
      $('.dropdown-sin-31').dropdown({
         //limitCount: 5,
         input: '<input type="text" maxLength="20" placeholder="Search">'
      });
   });
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->

<script type="text/javascript">
   $(document).ready(function() {
      $("#country").change(function() {
         var country_id = $(this).val();
         //alert(country_id);

         $.ajax({

            url: "<?php echo base_url() . 'Client/state'; ?>",
            data: {
               "country_id": country_id
            },
            type: "POST",
            success: function(data) {
               //alert('hi');
               $("#state").append(data);

            }

         });
      });
      $("#state").change(function() {
         var state_id = $(this).val();
         //alert(country_id);

         $.ajax({

            url: "<?php echo base_url() . 'Client/city'; ?>",
            data: {
               "state_id": state_id
            },
            type: "POST",
            success: function(data) {
               //alert('hi');
               $("#city").append(data);

            }

         });
      });






   });



   /** for reset button **/
   function clear_form_elements(ele) {

      $(ele).not("#myModal").find(':input').each(function() {


         console.log($(this).attr('id'));

         switch (this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'file':
               if ($(this).attr('name') != 'startdate') {
                  $(this).val("");
               }
               break;
            case 'textarea':
               $(this).val('');
               break;
            case 'checkbox':
            case 'radio':
               this.checked = false;
         }

      });


      //alert($(ele).find("file").attr('class'));
      $("#preview_image_name").html('');


      $(ele).not("#myModal").find('select').each(function() {
         $(this).val([]);
      });


      $('.dropdown-main li').each(function() {
         $(this).removeClass('dropdown-chose');
      });
      $('.dropdown-chose-list').each(function() {
         $(this).html('<span class="placeholder">select</span>');
      });



   }
   /** end of reset button **/

   $(document).on("click", ".save_assign_staff", function() {

      var id = $(this).attr("data-id");
      var data = {};
      var countries = $("#workers" + id).val();

      data['task_id'] = id;
      data['worker'] = countries;
      $(".LoadingImage").show();
      $.ajax({
         type: "POST",
         url: "<?php echo base_url(); ?>user/update_assignees/",
         data: data,
         success: function(response) {
            // alert(response); die();
            $(".LoadingImage").hide();
            $('#task_' + id).html(response);
            $('.task_' + id).html(response);
            $('.dashboard_success_message1').show();
            setTimeout(function() {
               $('.dashboard_success_message1').hide();
            }, 2000);
         },
      });
   });


   $(".save_assign_staff1").click(function() {

      alert('ok');
      var id = $(this).attr("data-id");
      var data = {};
      var countries = $("#workers1" + id).val();

      data['task1_id'] = id;
      data['worker'] = countries;
      $.ajax({
         type: "POST",
         url: "<?php echo base_url(); ?>user/update_assignees1/",
         data: data,
         success: function(response) {
            // alert(response); die();
            $('#task1_' + id).html(response);
         },
      });
   });


   function staff_click(staff) {
      alert($(staff).attr("data-id"));
   }
   $("#close_info_msg").click(function() {
      $(".popup_info_msg").hide();
   });

   $(document).on('click', '.modal .hasDatepicker', function() {
      $('.ui-datepicker').addClass('uidate_visible');
   });

   $(document).on('click', '.modal button[data-dismiss="modal"]', function() {
      $('.ui-datepicker').removeClass('uidate_visible');
   });


   $("select[id='related_to_services']").change(function() {
      var service = $(this).val();
      if (service != '') {
         $.ajax({
				type: "post",
				url: "<?php echo base_url('Service/get_service_estimated_time') ?>",
				data: {service_id : service},
				success: function (response) {
					$('#estimated_time').val(response);
				}
         });   
         
         $.ajax({
            url: "<?php echo base_url() ?>Tasksummary/get_services_options",
            type: "post",
            data: {
               "services": service,
               'action': 'new_inc_exclude'
            },
            beforeSend: function() {
               $(".LoadingImage").show();
            },
            success: function(res) {
               $(".LoadingImage").hide();
               $('#exc_service_popup .card-block.accordion-block').html(res);
               $("#exc_service_button").show();
            }
         });
      } else {
         $("#exc_service_button").hide();
      }
   });
</script>

<script>
	$(document).ready(function(){
		$(".clr-check-client").keyup(function(){
			var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-client-outline");
			}else{
				$(this).removeClass("clr-check-client-outline");
			}
		});

      $(".clr-check-client").each(function(i){
         if($(this).val() !== ""){
         $(this).addClass("clr-check-client-outline");
         }  
      })

		$(".clr-check-select").change(function(){
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		});

      $(".clr-check-select").each(function(i){
         if($(this).val() !== ""){
         $(this).addClass("clr-check-select-client-outline");
         }  
      })

      $(".clr-check-select-cus").change(function(){
         $(this).next().addClass("clr-check-select-cus-wrap");
      })

      $(".clr-check-select-cus").each(function(){
         if($(this).val() !== ""){
         $(this).next().addClass("clr-check-select-cus-wrap");
      }  
    })
});

</script>


<?php $this->load->view('tasks/import_task'); ?>
<script type="text/javascript">
   $(document).ready(function() {
      var arr = <?php echo json_encode($assign_group); ?>;

      $('#tree_div1 .comboTreeItemTitle').each(function() {

         var id1 = $(this).attr('data-id');

         var id = id1.split('_');
         console.log(id);
         if (jQuery.inArray(id[0], arr) !== -1) {
            $(this).find("input").trigger('click');

         }

      });
   });
</script>
</body>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/custom/search_scroll.js"></script>

</html>