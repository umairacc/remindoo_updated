<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');
   ?>
<!-- management block -->
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
         <div class="deadline-crm1 floating_set single-txt12">
                                    <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                       <li class="nav-item">
                                          Deadline Manager
                                          <div class="slide"></div>
                                       </li>
                                    </ul>
          </div>
            <div class="card deadline-block">
               <div class="payrolltax floating rev-op">
                 <div class="search-vat1">
                    <label>Search:</label>
                    <input type="text" name="search">
                 </div>
                 <div class="company-search1">
                    <div class="vat-shipping">
                       <label>Company Name</label> 
                       <select name="companysearch" id="companysearch" class="search">
                          <option value="">All</option>
                          <option value="2">zts</option>
                          <option value="">abc company</option>
                       </select>
                    </div>
                    <div class="vat-shipping">
                       <label>Company Type</label>
                       <select name="legal_form" class="  fields search" id="legal_form">
                          <option value="" data-id="1">All</option>
                          <option value="Private Limited company" data-id="2">Private Limited company</option>
                          <option value="Public Limited company" data-id="3">Public Limited company</option>
                          <option value="Limited Liability Partnership" data-id="4">Limited Liability Partnership</option>
                          <option value="Partnership" data-id="5">Partnership</option>
                          <option value="Self Assessment" data-id="6">Self Assessment</option>
                          <option value="Trust" data-id="7">Trust</option>
                          <option value="Charity" data-id="8">Charity</option>
                          <option value="Other" data-id="9">Other</option>
                       </select>
                    </div>
                    <div class="vat-shipping">
                       <label>Deadline Type</label>
                       <select class="fields search12" id="legal_form12">
                          <option value="">Due date form last 1 month</option>
                          <option value="">Due date form last 2 months</option>
                          <option value="">Due date form last 4 months</option>
                          <option value="">Due date form last 6 months</option>
                          <option value="from2">select custom due date</option>
                       </select>
                       <div class="from2option">
                          <div class="row">
                             <div class="col-sm-6">
                                <div class="form-group">
                                   <label>From</label>
                                       <div class="input-group date">
                                          <span class="input-group-addon "><span class="icofont icofont-ui-calendar"></span></span>
                                          <input type="text" class="datepicker hasDatepicker" id="txtFromDate" name="startdate" placeholder="dd-mm-yyyy">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group ">
                                       <label>To</label>
                                       <div class="input-group date">
                                          <span class="input-group-addon "><span class="icofont icofont-ui-calendar"></span></span>
                                          <input type="text" class="datepicker hasDatepicker" id="txtToDate" name="enddate" placeholder="dd-mm-yyyy">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="vat-shipping">
                        <div class="form-group checkbox-color checkbox-primary">
                           <input type="checkbox" name="public" value="Public" id="p-check">
                           <label for="p-check">Due </label>
                           <input type="checkbox" name="billable" value="Billable" checked="" id="p-check1">
                           <label for="p-check1">Over Due</label>
                        </div>
                        </div>
                     </div>
                  </div>

               <!-- collapse panel -->
               <div class="day-list">
                  <!-- <div class="f-days">
                     <div class="panel-head" ddata-parent="#accordion" data-toggle="collapse" data-target="#demo">
                        Today
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                     </div>
                     <div class="panel-body panel-collapse collapse" id="demo">
                        No Deadlines to view
                     </div>
                  </div>
                  <div class="f-week">
                     <div class="panel-head" data-parent="#accordion" data-toggle="collapse" data-target="#demo1">
                        This Week
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                     </div>
                     <div class="panel-body panel-collapse collapse" id="demo1">
                        No Deadlines to view
                     </div>
                  </div>
                  <div class="f-month">
                     <div class="panel-head" data-parent="#accordion" data-toggle="collapse" data-target="#demo2">
                        This Month
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                     </div>
                     <div class="panel-body panel-collapse collapse" id="demo2">
                        No Deadlines to view
                     </div>
                  </div>
                  <div class="n-month">
                     <div class="panel-head" data-parent="#accordion" data-toggle="collapse" data-target="#demo3">
                        Next Month
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                     </div>
                     <div class="panel-body panel-collapse collapse" id="demo3">
                        No Deadlines to view
                     </div>
                  </div> -->
                  <div class="card-block accordion-block color-accordion-block">
                                                        <div id="accordion" role="tablist" aria-multiselectable="true">
                                                            <div class="accordion-panel">
                                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                                    <h3 class="card-title accordion-title">
                                                                    <a class="accordion-msg" data-toggle="collapse"
                                                                       data-parent="#accordion" href="#collapseOne"
                                                                       aria-expanded="true" aria-controls="collapseOne">
                                                                        Today
                                                                    </a>
                                                                </h3>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="accordion-content accordion-desc">
                                                                        <p>
                                                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has
                                                                            survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                                                                            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="accordion-panel">
                                                                <div class="accordion-heading" role="tab" id="headingTwo">
                                                                    <h3 class="card-title accordion-title">
                                                                    <a class="accordion-msg" data-toggle="collapse"
                                                                       data-parent="#accordion" href="#collapseTwo"
                                                                       aria-expanded="false"
                                                                       aria-controls="collapseTwo">
                                                                        This week
                                                                    </a>
                                                                </h3>
                                                                </div>
                                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                    <div class="accordion-content accordion-desc">
                                                                        <p>
                                                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has
                                                                            survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                                                                            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="accordion-panel">
                                                                <div class=" accordion-heading" role="tab" id="headingThree">
                                                                    <h3 class="card-title accordion-title">
                                                                    <a class="accordion-msg" data-toggle="collapse"
                                                                       data-parent="#accordion" href="#collapseThree"
                                                                       aria-expanded="false"
                                                                       aria-controls="collapseThree">
                                                                        This Month
                                                                    </a>
                                                                </h3>
                                                                </div>
                                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                    <div class="accordion-content accordion-desc">
                                                                        <p>
                                                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has
                                                                            survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                                                                            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="accordion-panel">
                                                                <div class=" accordion-heading" role="tab" id="headingFour">
                                                                    <h3 class="card-title accordion-title">
                                                                    <a class="accordion-msg" data-toggle="collapse"
                                                                       data-parent="#accordion" href="#collapseFour"
                                                                       aria-expanded="false"
                                                                       aria-controls="collapseThree">
                                                                        This Year
                                                                    </a>
                                                                </h3>
                                                                </div>
                                                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                    <div class="accordion-content accordion-desc">
                                                                        <p>
                                                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has
                                                                            survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                                                                            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
               </div>
               <!-- collapse panel -->

               <!-- company details -->

               <div class="deadlines-types1">
     <div class="accident-toggle"><h2>MR ACCIDENT SOLUTIONS LIMITED</h2></div>
     <div class="investigation-01">
            <div class="color-switches trading-swithch2">
               <button type="button" name="service_fil[]" id="vat" class="btn btn-sm btn-toggle" data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>VAT</label>
            </div>
            <div class="color-switches trading-swithch3">
               <button type="button" name="service_fil[]" id="payroll" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Payroll</label>
            </div>
            <div class="color-switches trading-swithch4">
               <button type="button" name="service_fil[]" id="accounts" class="btn btn-sm  btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Accounts</label>
            </div>
            <div class="color-switches trading-swithch5">
               <button type="button" name="service_fil[]" id="conf_statement" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Confirmation Statement</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button" name="service_fil[]" id="company_tax_return" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Company Tax Return</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button" name="service_fil[]" id="personal_tax_return" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Personal Tax Return</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button" name="service_fil[]" id="workplace" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>WorkPlace Pension - AE</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button" name="service_fil[]" id="cis" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>CIS - Contractor</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button" name="service_fil[]" id="cissub" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>CIS - Sub Contractor</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button" name="service_fil[]" id="p11d" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>P11D</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button" name="service_fil[]" id="bookkeep" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Bookkeeping</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button" name="service_fil[]" id="management" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Management Accounts</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button" name="service_fil[]" id="investgate" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Investigation Insurance</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button" name="service_fil[]" id="registered" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Registered Address</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button" name="service_fil[]" id="taxadvice" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Tax Advice</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button" name="service_fil[]" id="taxinvest" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Tax Investigation</label>
            </div>
      </div>
         </div>

         <!-- company details -->

         <!-- info table -->

         <table class="table client_table1 text-center dataTable no-footer" id="servicedead14" role="grid" aria-describedby="newinactives_info">
               <thead>
                  <tr class="text-uppercase" role="row">
                     <th class="sorting_asc" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Profile: activate to sort column descending" style="width: 0px;">Client Name</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 0px;">Client Type</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending" style="width: 0px;">VAT</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 0px;">Payroll</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="User Type: activate to sort column ascending" style="width: 0px;">Accounts</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="User Type: activate to sort column ascending" style="width: 0px;">Confirmation statement</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="User Type: activate to sort column ascending" style="width: 0px;">Company Tax Return</th>
                  </tr>
               </thead>
               <tbody>
                  <tr role="row" class="">
                     <td><a href="javascript:;" data-toggle="modal" data-target="#desk-popup">abc company</a></td>
                     <td>Private Limited company</td>
                     <td>Private Limited company</td>
                     <td>Private Limited company</td>
                     <td>Private Limited company</td>
                     <td>Private Limited company</td>
                     <td>Private Limited company</td>
                  </tr>
                  <tr role="row" class="">
                     <td><a href="javascript:;">abc company</a></td>
                     <td>Private Limited company</td>
                     <td>Private Limited company</td>
                     <td>Private Limited company</td>
                     <td>Private Limited company</td>
                     <td>Private Limited company</td>
                     <td>Private Limited company</td>
                  </tr>
               </tbody>
            </table>

         <!-- info table -->

               



            </div>
         </div>
      </div>
   </div>
</div>
<!-- management block -->

<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
   
         //   var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
   /*$( ".datepicker" ).datepicker({
          dateFormat: 'yy-mm-dd'
      });*/
   
   $('.tags').tagsinput({
       allowDuplicates: true
      });
      
      $('.tags').on('itemAdded', function(item, tag) {
          $('.items').html('');
   var tags = $('.tags').tagsinput('items');
        
      });
   });
   
   $("#contact_today").click(function(){
     if($("#contact_today").is(':checked')){
       $(".selectDate").css('display','none');
     }else{
       $(".selectDate").css('display','block');
     }
   });
   
   $(".contact_update_today").click(function(){
     var id = $(this).attr("data-id");
     if($(this).is(':checked')){
   
       $("#selectDate_update_"+id).css('display','none');
     }else{
       $("#selectDate_update_"+id).css('display','block');
     }
   });
   
    $("#all_leads").dataTable({
          "iDisplayLength": 10,
       });
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
   
      $( "#leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });
   
   
      $( ".edit_leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });
   });
   
   
   $(".sortby_filter").click(function(){
         $(".LoadingImage").show();
   
      var sortby_val = $(this).attr("id");
      var data={};
      data['sortby_val'] = sortby_val;
      $.ajax({
      url: '<?php echo base_url();?>leads/sort_by/',
      type : 'POST',
      data : data,
      success: function(data) {
        $(".LoadingImage").hide();
   
      $(".sortrec").html(data);   
      $("#all_leads").dataTable({
      "iDisplayLength": 10,
      });
      },
      });
   
   });
   $("#import_leads").validate({
     rules: {
        file: {required: true, accept: "csv"},
   
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},
   
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
   
   });
                            
   
</script>


 <!-- Modal one-->
   <div class="modal fade common-schedule-msg1 remind-me-picker" id="desk-popup" role="dialog">
      <div class="modal-dialog">
          <!-- Modal content-->
           <div class="modal-content">
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Reminder</h4>
            </div>
            <div class="modal-body">
              <!-- reminders -->

              <div class="tax-details">
            <table class="table client_table1 text-center dataTable no-footer frclr" id="servicedead12" role="grid" aria-describedby="newinactives_info">
               <thead>
                  <tr class="text-uppercase" role="row">
                     <th class="sorting_asc" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Profile: activate to sort column descending" style="width: 0px;">#</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 0px;">deadline type</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending" style="width: 0px;">reminder (days before)</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 0px;">enabled</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="User Type: activate to sort column ascending" style="width: 0px;">email</th>
                  </tr>
               </thead>
               <tbody>
                  <tr role="row" class="">
                     <td class="user_imgs sorting_1">1</td>
                     <td>VAT</td>
                     <td>0</td>
                     <td><select name="priority" class="form-control valid" id="priority">
                              <option value="low">Low</option>
                              <option value="medium">Medium</option>
                              <option value="high">High</option>
                              <option value="super urgent">Super Urgent</option>
                           </select></td>
                     <td><select name="priority" class="form-control valid" id="priority">
                              <option value="low">Low</option>
                              <option value="medium">Medium</option>
                              <option value="high">High</option>
                              <option value="super urgent">Super Urgent</option>
                           </select></td>
                  </tr>
                  <tr role="row" class="">
                     <td class="user_imgs sorting_1">1</td>
                     <td>Payroll</td>
                     <td>0</td>
                     <td><select name="priority" class="form-control valid" id="priority">
                              <option value="low">Low</option>
                              <option value="medium">Medium</option>
                              <option value="high">High</option>
                              <option value="super urgent">Super Urgent</option>
                           </select></td>
                     <td><select name="priority" class="form-control valid" id="priority">
                              <option value="low">Low</option>
                              <option value="medium">Medium</option>
                              <option value="high">High</option>
                              <option value="super urgent">Super Urgent</option>
                           </select></td>
                  </tr>
                  <tr role="row" class="">
                     <td class="user_imgs sorting_1">1</td>
                     <td>Accounts</td>
                     <td>0</td>
                     <td><select name="priority" class="form-control valid" id="priority">
                              <option value="low">Low</option>
                              <option value="medium">Medium</option>
                              <option value="high">High</option>
                              <option value="super urgent">Super Urgent</option>
                           </select></td>
                     <td><select name="priority" class="form-control valid" id="priority">
                              <option value="low">Low</option>
                              <option value="medium">Medium</option>
                              <option value="high">High</option>
                              <option value="super urgent">Super Urgent</option>
                           </select></td>
                  </tr>
                  <tr role="row" class="">
                     <td class="user_imgs sorting_1">1</td>
                     <td>corporation tax</td>
                     <td>0</td>
                     <td><select name="priority" class="form-control valid" id="priority">
                              <option value="low">Low</option>
                              <option value="medium">Medium</option>
                              <option value="high">High</option>
                              <option value="super urgent">Super Urgent</option>
                           </select></td>
                     <td><select name="priority" class="form-control valid" id="priority">
                              <option value="low">Low</option>
                              <option value="medium">Medium</option>
                              <option value="high">High</option>
                              <option value="super urgent">Super Urgent</option>
                           </select></td>
                  </tr>
                  <tr role="row" class="">
                     <td class="user_imgs sorting_1">1</td>
                     <td>self assesment</td>
                     <td>0</td>
                     <td><select name="priority" class="form-control valid" id="priority">
                              <option value="low">Low</option>
                              <option value="medium">Medium</option>
                              <option value="high">High</option>
                              <option value="super urgent">Super Urgent</option>
                           </select></td>
                     <td><select name="priority" class="form-control valid" id="priority">
                              <option value="low">Low</option>
                              <option value="medium">Medium</option>
                              <option value="high">High</option>
                              <option value="super urgent">Super Urgent</option>
                           </select></td>
                  </tr>
                  <tr role="row" class="">
                     <td class="user_imgs sorting_1">1</td>
                     <td>confimation statement</td>
                     <td>0</td>
                     <td><select name="priority" class="form-control valid" id="priority">
                              <option value="low">Low</option>
                              <option value="medium">Medium</option>
                              <option value="high">High</option>
                              <option value="super urgent">Super Urgent</option>
                           </select></td>
                     <td><select name="priority" class="form-control valid" id="priority">
                              <option value="low">Low</option>
                              <option value="medium">Medium</option>
                              <option value="high">High</option>
                              <option value="super urgent">Super Urgent</option>
                           </select></td>
                  </tr>
               </tbody>
            </table>
            </div>
          <!-- reminders -->
            </div>
            <div class="modal-footer">
                  <button type="button" name="button"  class="btn btn-default submitBtn">Confirm</button>
                  <a  class="btn btn-danger" href="#" data-dismiss="modal">Cancel</a>
            </div>
          </div>
      </div>
    </div>




<script type="text/javascript">
   $(document).ready(function(){
       $('.edit-toggle1').click(function(){
         $('.proposal-down').slideToggle(300);
       });

        $("#legal_form12").change(function() {
          var val = $(this).val();
          if(val === "from2") {
            // alert('hi');
              $(".from2option").show();
          }
          else {
              $(".from2option").hide();
          }
          });

        $("#servicedead12").dataTable({
         // "iDisplayLength": 10 ,
         // "scrollX": true
         
         });

          $("#servicedead14").dataTable({
         // "iDisplayLength": 10 ,
         "scrollX": true
         
         });        
   })
</script>