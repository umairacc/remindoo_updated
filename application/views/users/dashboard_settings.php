<?php $this->load->view('includes/header');?>
   <style type="text/css">
     .signed-change4
     {
      background-image: url(../assets/images/new_update10.png);
      background-color: #99d9ea;

     }
     .signed-change5{
        background-image: url(../assets/images/new_update11.png);
        background-position: right 15px center;
        padding-left: 12px;
        padding-right: 30px;
        background-color: #00a2e8;
     }
   </style>
	<section class="client-details-view hidden-user01 floating_set columsettingcls colsettingclsnew dashboardsettcls">
		<div class="information-tab floating_set">
			<div class="deadline-crm1 ">
               <ol class="nav nav-tabs all_user1 md-tabs">
                 <li class="nav-item form_info">
                        <a class="nav-link form_info active" data-toggle="tab" href="#allusers">Dashboard settings</a>
                      </li>
                   <!--    <li class="nav-item ">
                        <a class="nav-link " data-toggle="tab" href="#alltasks">Task column settings</a>
                      </li> -->
               </ol>
                    <div class="tab-content">
                    <!--  <div id="alltasks" class="tab-pane">
							<form class="client-firm-info1 validation fr-colsetting fr-colsettingfloatcls " method="post" action="" enctype="multipart/form-data">
								<div class="information-tab floating_set">
									<div class="deadline-crm1 ">
										<div class="Footer common-clienttab pull-right">
											<div class="change-client-bts1">
												<input type="submit" class="signed-change1" id="save_exit" value ="Save & Exit">
											</div>
										</div>
										</div>

										<div class="space-required">
											<div class="document-center client-infom-1 floating_set">
												<div class="Companies_House floating_set">
													<div class="pull-left">
														<h2>Task Column Setting</h2>
													</div>
												</div>
												<div class="form-group row name_fields">
													<label class="col-sm-4 col-form-label">Timer</label>
														<div class="col-sm-8">
															<input type="checkbox" class="js-small f-right" name="timer" id="company_name1" value="1" >
														</div>
												</div>
											</div>
										</div>
								</div>
							</form>
                     </div> -->
                     <div id="allusers" class="tab-pane active">
                    	<form class="client-firm-info1 validation fr-colsetting fr-colsettingfloatcls " method="post" action="<?php echo base_url(); ?>sample/settings_insert" enctype="multipart/form-data">
								<div class="information-tab floating_set">
									<div class="deadline-crm1 ">
										<div class="Footer common-clienttab pull-right">
											<div class="change-client-bts1">
												<input type="submit" class="signed-change1" id="save_exit" value ="Save & Exit">
											</div>
										</div>
										</div>

										<div class="space-required">
											<div class="document-center client-infom-1 floating_set">
												<div class="Companies_House floating_set">
													<div class="pull-left">
														<h2>Dashboard Settings</h2>
													</div>
												</div>
												<div class="form-group row name_fields">
													<label class="col-sm-4 col-form-label">Basic Details</label>
														<div class="col-sm-8">
															<input type="checkbox" class="js-small f-right" name="basic_details" id="company_name1" value="1" <?php if(isset($settings['dashboard_basic_details']) && $settings['dashboard_basic_details']=='1'){ ?> checked="checked" <?php } ?>>
														</div>
												</div>

												<div class="form-group row name_fields">
													<label class="col-sm-4 col-form-label">Task Details</label>
														<div class="col-sm-8">
															<input type="checkbox" class="js-small f-right" name="task_details" id="company_name1" value="1" <?php if(isset($settings['dashboard_task_details']) && $settings['dashboard_task_details']=='1'){ ?> checked="checked" <?php } ?> >
														</div>
												</div>

												<div class="form-group row name_fields">
													<label class="col-sm-4 col-form-label">Client Details</label>
														<div class="col-sm-8">
															<input type="checkbox" class="js-small f-right" name="client_details" id="company_name1" value="1" <?php if(isset($settings['dashboard_client_details']) && $settings['dashboard_client_details']=='1'){ ?> checked="checked" <?php } ?> >
														</div>
												</div>

												<div class="form-group row name_fields">
													<label class="col-sm-4 col-form-label">Todo Items</label>
														<div class="col-sm-8">
															<input type="checkbox" class="js-small f-right" name="todo_items" id="company_name1" value="1" <?php if(isset($settings['dashboard_todo_items']) && $settings['dashboard_todo_items']=='1'){ ?> checked="checked" <?php } ?>>
														</div>
												</div>

												<div class="form-group row name_fields">
													<label class="col-sm-4 col-form-label">Proposal</label>
														<div class="col-sm-8">
															<input type="checkbox" class="js-small f-right" name="proposal" id="company_name1" value="1" <?php if(isset($settings['dashboard_proposal']) && $settings['dashboard_proposal']=='1'){ ?> checked="checked" <?php } ?>>
														</div>
												</div>

												<div class="form-group row name_fields">
													<label class="col-sm-4 col-form-label">CRM</label>
														<div class="col-sm-8">
															<input type="checkbox" class="js-small f-right" name="crm" id="company_name1" value="1" <?php if(isset($settings['dashboard_crm']) && $settings['dashboard_crm']=='1'){ ?> checked="checked" <?php } ?> >
														</div>
												</div>

												<div class="form-group row name_fields">
													<label class="col-sm-4 col-form-label">Timeline</label>
														<div class="col-sm-8">
															<input type="checkbox" class="js-small f-right" name="timeline" id="company_name1" value="1" <?php if(isset($settings['dashboard_timeline']) && $settings['dashboard_timeline']=='1'){ ?> checked="checked" <?php } ?>>
														</div>
												</div>

												<div class="form-group row name_fields">
													<label class="col-sm-4 col-form-label">Service Details</label>
														<div class="col-sm-8">
															<input type="checkbox" class="js-small f-right" name="service" id="company_name1" value="1" <?php if(isset($settings['dashboard_service_details']) && $settings['dashboard_service_details']=='1'){ ?> checked="checked" <?php } ?>>
														</div>
												</div>

												<div class="form-group row name_fields">
													<label class="col-sm-4 col-form-label">Deadline Details</label>
														<div class="col-sm-8">
															<input type="checkbox" class="js-small f-right" name="deadline" id="company_name1" value="1" <?php if(isset($settings['dashboard_deadline_details']) && $settings['dashboard_deadline_details']=='1'){ ?> checked="checked" <?php } ?>>
														</div>
												</div>

												<div class="form-group row name_fields">
													<label class="col-sm-4 col-form-label">Reminder Details</label>
														<div class="col-sm-8">
															<input type="checkbox" class="js-small f-right" name="reminder" id="company_name1" value="1" <?php if(isset($settings['dashboard_reminder_details']) && $settings['dashboard_reminder_details']=='1'){ ?> checked="checked" <?php } ?> >
														</div>
												</div>
											</div>
										</div>
								</div>
							</form>
                     </div>
                </div>
            </div>
         </div>
     </section>
     <?php $this->load->view('includes/footer');?>
</body>
</html>