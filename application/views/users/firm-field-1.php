<?php 
   

    if( !empty( $_SESSION['is_superAdmin_login'] ) )
    {
      $this->load->view('super_admin/superAdmin_header');
    }
    else
    {
      $this->load->view('includes/header');
    }


   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');
   $userid = $this->session->userdata('id'); 
   ?>
<style type="text/css">
   .to-do-list.sortable-moves
   {
    padding-left: 0;
   }
   .pcoded-content
   {
     float: left !important;
     width:100%;
   }
</style>
<!-- show info  -->
  <div class="modal-alertsuccess alert info_popup" style="display:none;">
    <div class="newupdate_alert">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
     <div class="pop-realted1">
        <div class="position-alert1 info-text">
         </div>
        </div>
     </div>
  </div >
  <!-- end show info  -->

 <div class="modal fade" id="new-field" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Field</h4>
        </div>
         <form id="field_form" class="validation" method="post" action="" enctype="multipart/form-data"> 
           <div class="modal-body">
               <div class="field-name1 form-group">
               <label>field name</label>
               <input type="text" id="name" name="name">
            </div>
            <div class="field-type1 ">
               <label>field type</label>
              <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                <option value="input">Input</option>
                                <option value="number">Number</option>
                                <option value="textarea">Textarea</option>
                                <option value="select">Select</option>                            
                                <option value="date_picker">Date Picker</option>
                            </select>            
            <div id="options_wrapper" style="display:none;">
          <div class="form-group" app-field-wrapper="options">
            <label for="options" class="control-label"> 
                <small class="req text-danger">*</small>Options</label>
                <textarea id="options" name="options" class="form-control" rows="3"></textarea></div>  
            </div>
            </div>
            <input type="hidden" name="fieldto" id="fieldto" value="">
            <input type="hidden" name="firm_name" id="firm_name" value="">
            
        </div>
        <div class="modal-footer">
          <div class="modal-save">
              <input type="submit" class="add_new_field" value="Save" id="submit"/>
            </div>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> 
     </form>
      </div>
    </div>
  </div>
                      <div class="firmmytodoclstop">
                        <div class="firmmytodoclsbottom">

                        <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Accounts
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable accounts" id="accounts">
                                             <?php
                                               foreach($firm_fields as $firm){ 
                                                if($firm['section_name']=='accounts'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>
                                                     <?php
                                                 //  if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if($firm['custom_id']==0){
                                                   ?>

                                                    <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                   <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>                                                  
                                                   </div> 
                                                   <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                  <?php //}  
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> </a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="accounts" id="accounts"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div>  


                     <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Bookkeeping
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                          <div class="sortable bookkeeping" id="bookkeeping">
                                             <?php
                                               foreach($firm_fields as $firm){
                                                if($firm['section_name']=='bookkeeping'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>
                                                     <?php
                                                  // if($firm['edit_option']==1){
                                                   ?> 

                                                   <?php  
                                                   if($firm['custom_id']==0){
                                                   ?>

                                                    <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                   <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>                                                  
                                                   </div> 
                                                   <?php } ?>

                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                  <?php //} 
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> </a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="bookkeeping" id="bookkeeping"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div>


                        <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Confirmation Statement
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable conf_stmt" id="conf_stmt">
                                             <?php
                                               foreach($firm_fields as $firm){ 

                                                if($firm['section_name']=='conf_stmt'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>    <?php
                                                  // if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if( $firm['custom_id']==0 ) {
                                                   ?>

                                                   <div class="f-right">
                                                       <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                         <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                   </div> 
                                                    <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                   <?php //}  
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> </a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($$custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($$custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($$custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($$custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($$custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="confirmation" id="conf_stmt"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div>
                        <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Company Tax Return
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable company_tax_return" id="company_tax_return">
                                             <?php
                                               foreach($firm_fields as $firm){ 

                                                if($firm['section_name']=='company_tax_return'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>    <?php
                                                  // if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if( $firm['custom_id']==0 ) {
                                                   ?>

                                                   <div class="f-right">
                                                       <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                         <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                   </div> 
                                                    <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                   <?php //}  
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> </a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($$custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($$custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($$custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($$custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($$custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="company_tax_return" id="company_tax_return"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div>

                        <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 CIS Contractor/Sub Contractor
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable Cis_CisSub_Contractor" id="Cis_CisSub_Contractor">
                                             <?php
                                               foreach($firm_fields as $firm){ 

                                                if($firm['section_name']=='Cis_CisSub_Contractor'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>    <?php
                                                  // if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if( $firm['custom_id']==0 ) {
                                                   ?>

                                                   <div class="f-right">
                                                       <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                         <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                   </div> 
                                                    <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                   <?php //}  
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> </a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($$custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($$custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($$custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($$custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($$custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="Cis_CisSub_Contractor" id="Cis_CisSub_Contractor"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div>

                         <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Investigation Insurance
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable investigation_insurance" id="investigation_insurance">
                                             <?php
                                               foreach($firm_fields as $firm){ 

                                                if($firm['section_name']=='investigation_insurance'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>
                                                     <?php
                                                   //if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if($firm['custom_id']==0){
                                                   ?>
                                                    <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                   <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                   </div> 
                                                   <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                   <?php //}  
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> </a>
                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> </a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="investigation" id="investigation_insurance"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div> 
                        <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Management Accounts
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                          <div class="sortable management_account" id="management_account">
                                             <?php
                                               foreach($firm_fields as $firm){
                                                if($firm['section_name']=='management_account'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>
                                                     <?php
                                                  // if($firm['edit_option']==1){
                                                   ?> 

                                                   <?php  
                                                   if($firm['custom_id']==0){
                                                   ?>

                                                    <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                   <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>                                                  
                                                   </div> 
                                                   <?php } ?>

                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                  <?php //} 
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> </a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="management" id="management_account"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div>


                         <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Payroll
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable payroll" id="payroll">
                                             <?php
                                               foreach($firm_fields as $firm){ 
                                                if($firm['section_name']=='payroll'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>

                                                     <?php
                                                 //  if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if($firm['custom_id']==0){
                                                   ?>

                                                    <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                   <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                   </div> 
                                                   <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                   <?php //}  
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> edit</a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="payroll" id="payroll"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div> 
                         <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Personal Tax Return
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable personal_tax" id="personal_tax">
                                             <?php
                                               foreach($firm_fields as $firm){ 

                                                if($firm['section_name']=='personal_tax'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>
                                                  <?php
                                                  // if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if( $firm['custom_id']==0 ){
                                                   ?>

                                                    <div class="f-right">
                                                       <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                         <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                   </div> 
                                                   <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                   <?php //}  
                                                   if( $firm['custom_id']!=0 ){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> </a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="personal_tax" id="personal_tax"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div>

                        <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 P11D
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable p11d" id="p11d">
                                             <?php
                                               foreach($firm_fields as $firm){ 
                                                if($firm['section_name']=='P11d'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>

                                                     <?php
                                                //   if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if($firm['custom_id']==0){
                                                   ?>
                                                    <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                   <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>                                                  
                                                   </div> 
                                                   <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                   <?php //} 
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> edit</a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="payroll" id="P11d"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div> 
                        <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Registered Office
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable registered_office" id="registered_office">
                                             <?php
                                               foreach($firm_fields as $firm){ 

                                                if($firm['section_name']=='registered_office'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>
                                                     <?php
                                                   //if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if($firm['custom_id']==0){
                                                   ?>
                                                    <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                   <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                   </div> 
                                                   <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                   <?php //}  
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> </a>
                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> </a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="registered_office" id="registered_office"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div> 

                        <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Tax advice/investigation
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable TaxAdvice_Investigation" id="TaxAdvice_Investigation">
                                             <?php
                                               foreach($firm_fields as $firm){ 

                                                if($firm['section_name']=='TaxAdvice_Investigation'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>
                                                     <?php
                                                   //if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if($firm['custom_id']==0){
                                                   ?>
                                                    <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                   <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                   </div> 
                                                   <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                   <?php //}  
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> </a>
                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> </a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="TaxAdvice_Investigation" id="TaxAdvice_Investigation"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div> 


                    
                        <!-- To Do Card List card end -->
                     <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Vat Return
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable vat_return" id="vat_return">
                                             <?php
                                               foreach($firm_fields as $firm){ 

                                                if($firm['section_name']=='vat_return'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>

                                                     <?php
                                                  // if($firm['edit_option']==1){
                                                   ?> 

                                                   <?php  
                                                   if($firm['custom_id']==0){
                                                   ?>

                                                    <div class="f-right">
                                                       <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                         <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                   </div> 
                                                    <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                   <?php //} 
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> edit</a>
                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="vat" id="vat_return"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div> 
                         <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Work Place
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable work_place" id="work_place">
                                             <?php
                                               foreach($firm_fields as $firm){ 
                                                if($firm['section_name']=='work_place'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>

                                                     <?php
                                                 //  if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if($firm['custom_id']==0){
                                                   ?>
                                                    <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                   <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                   </div> 
                                                   <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                   <?php //}  
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> edit</a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="payroll" id="work_place"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div> 



                     <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Important Details
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable imp_details" id="imp_details">
                                             <?php
                                               foreach( $firm_fields as $firm ){ 

                                                if($firm['section_name']=='imp_details'){?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>
                                                  <?php
                                                 //  if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if( $firm['custom_id']==0 ){
                                                   ?>
                                                    <div class="f-right">
                                                       <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i></a>
                                                         <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                   </div> 
                                                    <?php  
                                                  }
                                                   ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                      <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                      <div class="panel-body">
                                                      <label>field name</label>
                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                      <span class="error" style="display:none;">Enter a name</span>
                                                      <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                      <div class="save-can">
                                                      <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                      <a href="javascript:;" class="cancel-btns">cancel</a>
                                                      </div>
                                                      </div>
                                                      </form>
                                                    </div>
                                                   <?php //}  
                                                   if($firm['custom_id']!=0 )
                                                   {
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> </a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?>
                                                   </textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  

                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                                <div class="add-extrafield">
                                                <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="customers" id="imp_details"><i class="fa fa-plus"></i></span>
                                                </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div>
                   

                    
                        <div class="col-xl-6 col-sm-6 lf-mytodo">
                        <!-- To Do Card List card start -->
                        <div class="card fr-hgt">
                           <div class="card-header">
                              <h5>
                                 Others
                                 <span class="f-right">
                                   
                                 </span>
                              </h5>
                           </div>
                           <div class="card-block widnewtodo">
                              <div class="tab-content tabs card-block">
                                 <div class="tab-pane active" id="home1" role="tabpanel">
                                    <div class="new-task" id="draggablePanelList">
                                       <div class="no-todo">
                                      
                                          <div class="sortable others" id="others">
                                             <?php
                                               foreach($firm_fields as $firm){ 
                                                if($firm['section_name']=='others'){ ?>
                                             <div class="to-do-list card-sub" id="<?php echo $firm['id']; ?>">
                                                   <span><?php echo $firm['field_name']; ?></span>

                                                     <?php
                                                 //  if($firm['edit_option']==1){
                                                   ?> 
                                                   <?php  
                                                   if($firm['custom_id']==0){
                                                   ?>

                                                    <div class="f-right">
                                                       <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="edit_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                         <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_fields(this);">
                                                       <i class="fa fa-pencil-square" aria-hidden="true"></i> </a>
                                                   </div> 
                                                   <?php } ?>
                                                    <div class="firm-fields-div edit-fieldname_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                    <div class="panel-heading">edit information</div>
                                                    <div class="alert alert-success" style="display:none;"
                                                    ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                    <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                    <form class="validation edit_field_form"  id="edit_field_name_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                    <div class="panel-body">
                                                    <label>field name</label>
                                                    <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">
                                                    <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">
                                                    <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">          
                                                    <span class="error" style="display:none;">Enter a name</span>
                                                    <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                    <div class="save-can">
                                                    <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                    <a href="javascript:;" class="cancel-btns">cancel</a>
                                                    </div>
                                                    </div>
                                                    </form>
                                                    </div>
                                                   <?php //}  
                                                   if($firm['custom_id']!=0){
                                                    $custom=$this->Common_mdl->firm_options($firm['custom_id']);
                                                   ?>
                                                   <div class="f-right">
                                                   <a href="javascript:;" class="edit_position" id="<?php echo $firm['id']; ?>" onclick="click_function(this);">
                                                   <i class="fa fa-pencil-square " aria-hidden="true"></i> edit</a>

                                                     <a href="javascript:;" class="delete_position" id="<?php echo $firm['id']; ?>" onclick="delete_function(this);">
                                                   <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                                   </div>  

                                                   <div class="firm-fields-div edit-field_<?php echo $firm['id']; ?> hide" style="display: none;">
                                                   <div class="panel-heading">edit information</div>
                                                   <div class="alert alert-success" style="display:none;"
                                                   ><a href="#" class="close">&times;</a>Field Updated Successfully.</div>
                                                   <div class="alert alert-danger" style="display:none;"><a href="#" class="close">&times;</a>New Field Not Added.</div>
                                                   <form class="validation edit_field_form"  id="edit_field_form_<?php echo $firm['id']; ?>" method="post" action="" enctype="multipart/form-data"> 
                                                   <div class="panel-body">
                                                   <label>field name</label>

                                                      <input type="text" name="name" id="name" value="<?php echo $firm['field_name']; ?>">

                                                      <input type="hidden" name="firm_id" id="firm_id" value="<?php echo $firm['id']; ?>">

                                                      <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $firm['custom_id']; ?>">                                               
                                                   <span class="error" style="display:none;">Enter a name</span>
                                                   <label>field Type</label>
                                                   <select name="type" id="type" class="selectpicker" data-width="100%" data-none-selected-text="Nothing selected" tabindex="-98">
                                                   <option value="input" <?php if($custom['type']=='input'){  ?> selected="selected" <?php } ?> >Input</option>
                                                   <option value="number" <?php if($custom['type']=='number'){  ?> selected="selected" <?php } ?> >Number</option>
                                                   <option value="textarea" <?php if($custom['type']=='textarea'){  ?> selected="selected" <?php } ?>>Textarea</option>
                                                   <option value="select" <?php if($custom['type']=='select'){  ?> selected="selected" <?php } ?>>Select</option>
                                                   <!-- <option value="checkbox">Checkbox</option> -->
                                                   <option value="date_picker" <?php if($custom['type']=='date_picker'){  ?> selected="selected" <?php } ?>>Date Picker</option>
                                                   </select>
                                                   <div id="options_wrapper" style="display:none;">
                                                   <div class="form-group" app-field-wrapper="options">
                                                   <label for="options" class="control-label"> 
                                                   <small class="req text-danger">*</small>Options</label>
                                                   <textarea id="options" name="options" class="form-control" rows="3"><?php  echo $custom['options']; ?></textarea></div>  
                                                   </div>
                                                   <input type="hidden" name="firm_name" id="firm_name" value="<?php  $firm['section_name'] ?>">              
                                                   <div class="save-can">
                                                   <input type="submit" class="add_edit_field" value="Save" id="submit"/>
                                                   <a href="javascript:;" class="cancel-btns">cancel</a>
                                                   </div>
                                                   </div>
                                                   </form>
                                                </div>
                                                   <?php } ?>  
                                                 </div> 
                                               <?php } } ?>  
                                            </div>
                                             <div class="add-extrafield">
                                             <span class="ex-field" data-target="#new-field" data-toggle="modal" data-field="other" id="others"><i class="fa fa-plus"></i></span>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                        </div> 
                     
                     
                        
                         
                      </div>
                    </div>
<?php

if( !empty( $_SESSION['is_superAdmin_login'] ) )
  {
    $this->load->view('super_admin/superAdmin_footer');
  }
  else
  {      
    $this->load->view('includes/footer');
  }


?>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/Sortable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/sortable-custom.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  var duedate_fields = [94,50,23,104,200,203,59,54,111,33,135,63,217,142,120];
    
    duedate_fields.forEach(function(v,i){
      $('.delete_position[id="'+v+'"]').addClass('disabled');
    });

$("#field_form").validate({
  
       ignore: false,

      
                        rules: {
                        name: {required: true},
                        
                        },
                        errorElement: "span" , 
                        errorClass: "field-error",                             
                         messages: {
                          name: "Enter a name",
                         },
                      
                        submitHandler: function(form) {

                         //  alert('ok');
                            var formData = new FormData($("#field_form")[0]);
                            
                            $(".LoadingImage").show();

                            $.ajax({
                                url: '<?php echo base_url();?>user/field_insert/',
                                dataType : 'json',
                                type : 'POST',
                                data : formData,
                                contentType : false,
                                processData : false,
                                success: function(data) {
                                    console.log(data);
                                $(".LoadingImage").hide();
                                if(data == 1)
                                    {
                                       $('.info_popup .info-text').html('Field Added Success Fully!..');
                                    }
                                    else
                                    {
                                       $('.info_popup .info-text').html('Some thing oops try again!..');
                                    }
                                  $('.info_popup').show();
                                 // setTimeout(function(){location.reload();},1000); 
                                },
                                error: function() { $('.alert-danger').show();
                                        $('.alert-success').hide();}
                            });

                            return false;
                        } ,
                         invalidHandler: function(e, validator) {
           if(validator.errorList.length)
        $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')

        }
                         
                    });




   <?php
   foreach($firm_fields as $firm){  ?>

$("#edit_field_form_<?php echo $firm['id']; ?>").validate({
  
       ignore: false,

      
                        rules: {
                        name: {required: true},
                        
                        },
                        errorElement: "span" , 
                        errorClass: "field-error",                             
                         messages: {
                          name: "Enter a name",
                         },
                      
                        submitHandler: function(form) {

                          // alert('ok');
                            var formData = new FormData($("#edit_field_form_<?php echo $firm['id']; ?>")[0]);
                            
                            $(".LoadingImage").show();

                            $.ajax({
                                url: '<?php echo base_url();?>user/field_update/',
                                dataType : 'json',
                                type : 'POST',
                                data : formData,
                                contentType : false,
                                processData : false,
                                success: function(data) {
                                    console.log(data);
                                    $(".LoadingImage").hide();
                                     var parent = $("#edit_field_form_<?php echo $firm['id']; ?>").closest('.firm-fields-div');
                                    if(data == 1)
                                    {
                                      parent.find('.alert-success').show();                                        
                                      parent.find('.alert-danger').hide();                                  
                                    }
                                    else
                                    {                                      
                                        parent.find('.alert-danger').show();
                                        parent.find('.alert-success').hide();
                                    }
                                    setTimeout(function(){location.reload();},1000); 
                                },
                                error: function() { $('.alert-danger').show();
                                        $('.alert-success').hide();}
                            });

                            return false;
                        } ,
                         invalidHandler: function(e, validator) {
           if(validator.errorList.length)
        $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')

        }
                         
                    });




$("#edit_field_name_<?php echo $firm['id']; ?>").validate({  
                      ignore: false,      
                        rules: {
                        name: {required: true},
                        
                        },
                        errorElement: "span" , 
                        errorClass: "field-error",                             
                         messages: {
                          name: "Enter a name",
                         },
                      
                        submitHandler: function(form) {
                            var formData = new FormData($("#edit_field_name_<?php echo $firm['id']; ?>")[0]);
                            
                            $(".LoadingImage").show();
                            $.ajax({
                                url: '<?php echo base_url();?>user/fieldname_update/',
                                dataType : 'json',
                                type : 'POST',
                                data : formData,
                                contentType : false,
                                processData : false,
                                success: function(data) {
                                    console.log(data);
                                    $(".LoadingImage").hide();
                                    var parent = $("#edit_field_name_<?php echo $firm['id']; ?>").closest('.firm-fields-div');
                                    if(data == 1)
                                    {
                                      parent.find('.alert-success').show();                                        
                                      parent.find('.alert-danger').hide();                                  
                                    }
                                    else
                                    {                                      
                                        parent.find('.alert-danger').show();
                                        parent.find('.alert-success').hide();
                                    }
                                    setTimeout(function(){location.reload();},1000);  
                                },
                                error: function() { $('.alert-danger').show();
                                        $('.alert-success').hide();}
                            });

                            return false;
                        } ,
                         invalidHandler: function(e, validator) {
           if(validator.errorList.length)
        $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')

        }
                         
                    });

<?php } ?>

});



   $( function() {
   
    $( ".imp_details" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.imp_details ").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });   

        $( ".conf_stmt" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.conf_stmt ").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  

           $( ".company_tax_return" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.company_tax_return ").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  
          $( ".Cis_CisSub_Contractor" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.Cis_CisSub_Contractor ").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  


          $( ".personal_tax" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.personal_tax ").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  




             $( ".vat_return" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.vat_return ").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  


             $( ".management_account" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.management_account ").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  
                        $( ".bookkeeping" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.bookkeeping ").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  

   $( ".investigation_insurance" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.investigation_insurance ").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  

   $( ".registered_office" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.registered_office ").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  
    $( ".TaxAdvice_Investigation" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.TaxAdvice_Investigation ").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      }); 

    $( ".assign_to" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.assign_to ").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  

      $( ".others" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.others").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });      
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  



      $( ".payroll" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.payroll").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });

            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  


       $( ".accounts" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.accounts").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  
  $( ".work_place" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.work_place").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  



      $( ".p11d" ).sortable(
   {
        update: function(event, ui) {       
            var arr = [];
            $("div.p11d").each(function() { 
                 var idsInOrder = [];      
                 var ul_id = $(this).attr('id');
                $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
                    idsInOrder.push($(this).attr('id')) 
                });
                  arr = idsInOrder;   
            });        
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/update_firm_field",
              data:  { 'datas' : arr }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                 $(".LoadingImage").hide();               
              }
            });
        }
      });  
   });


$(".ex-field").on('click',function() { 
        var fields = $(this).data('field');
       $('#fieldto').val(fields);
       var firm_name=$(this).attr('id');
           $('#firm_name').val(firm_name);
      });


$(".selectpicker").change(function(){
var rec = $(this).val();
if(rec=='select' || rec=='checkbox' )
{
   $(this).next().show();    
}
else{
    $(this).next().hide();        
}
}); 


function click_function(click){
var id=$(click).attr('id');
var rec=$(click).parents('.sortable').find('.edit-field_'+id).find('.selectpicker').val();
if(rec=='select' || rec=='checkbox' )
{
  $(click).parents('.sortable').find('.edit-field_'+id).find('#options_wrapper').show(); 
}
else{
   $(click).parents('.sortable').find('.edit-field_'+id).find('#options_wrapper').hide();
}
$(click).parents('.sortable').find('.edit-field_'+id).show();
}


function delete_function(del){

var id=$(del).attr('id');
var action = function(){ 
$('#Confirmation_popup').modal('hide'); 
        $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>User/delete_firm_field",
              data:  { 'id' : id }, 
              beforeSend: function() {
                 $(".LoadingImage").show();
              },
              success: function(data){
                $(".LoadingImage").hide();  
                $('.info_popup .info-text').html('Deleted Success Fully..');
                $('.info_popup').show();
                setTimeout(function(){location.reload();},1000);
                              
              }
            });
      };
  Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to delete this field?'});  
  $('#Confirmation_popup').modal('show');
}
function edit_fields(edit)
{
  var id=$(edit).attr('id');
  $(edit).parents('.sortable').find('.edit-fieldname_'+id).show();
}

function delete_fields(del){

  var id=$(del).attr('id');

  var action = function(){
    $('#Confirmation_popup').modal('hide');
     $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>User/deletefirm",
                data:  { 'id' : id }, 
                beforeSend: function() {
                   $(".LoadingImage").show();
                },
                success: function(data){

                  $(".LoadingImage").hide();  
                  $('.info_popup .info-text').html('Deleted Success Fully..');
                  $('.info_popup').show();
                  setTimeout(function(){location.reload();},1000);            
                
                }
              });
  };
  Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to delete this field...!'});  
  $('#Confirmation_popup').modal('show');
}



$(document).on("click",".cancel-btns", function(){
  $(this).parents('.firm-fields-div').hide();
});
$(document).on("click","a.close",function(){
  $(this).closest('div.alert').hide();
});
   

   </script>