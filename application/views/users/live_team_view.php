<?php $this->load->view('includes/header'); ?>
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
  .add_class{
    display: none;
  }
  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}

  tfoot {
    display: table-header-group;
}
</style>

<!-- <div class="container">
  <h2>Modal Example</h2>
  <!-- Trigger the modal with a button -->
 <!--  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

  <!-- Modal -->
  <!-- <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
    <!--   <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div> --> 



  <!-- <div class="modal fade" id="Bulkmy_Modal" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input type="hidden" name="archive_task_id" id="Bulkarchive_task_id" value="">
          <p>Do You Want Archive this Task?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default Bulkarchive_task" >yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div> -->

<style>

/** 28-06-2018 **/
.time{
	    padding: 8px;
    font-weight: bold;
}
.new_play_stop{
	    width: 20px;
    height: 20px;
    background: #000;
    color: #fff;
    line-height: initial;
    text-align: center;
    font-size: 15px;
    vertical-align: top;
    border-radius: 50%;
    margin: 7px 0 0 0;
}
/** for timer style 28-06-2018 **/
   /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   /** 04-06-2018 **/
   select.close-test-04{
    display: none;
   }
   /** 04-06-2018 **/
   span.demo {
   padding: 0 10px;
   }
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   img.user_imgs {
   width: 38px;
   height: 38px;
   border-radius: 50%;
   display: inline-block;
   }


a.adduser1 {
    background: #38b87c;
    color: #fff;
    padding: 3px 7px 3px;
    border-radius: 6px;
    vertical-align: middle;
    display: inline-block;
}
   #adduser label {
   text-transform: capitalize;
   font-size: 14px;
   }
  span.hours-left {
    background: #f76480;
    color: #fff;
    padding: 3px 10px;
    border-radius: 3px;
    margin-right: 15px;
}
   .dropdown12 button.btn.btn-primary {
   background: #ccc;
   color: #555;
   font-size: 13px;
   padding: 4px 10px 4px;
   margin-top: -2px;
   border-color:transparent;
   }
   select + .dropdown:hover .dropdown-menu
   {
   display: none;
   }
   select + .dropdown12 .dropdown-menu
   {
   padding: 0 !important;
   }
   select + .dropdown12 .dropdown-menu a {
   color: #000 !important;
   padding: 7px 10px !important;
   border: none !important;
   font-size: 14px;
   }
   select + .dropdown12 .dropdown-menu li {
   border: none !important;
   padding: 0px !important;
   }
   select + .dropdown12 .dropdown-menu a:hover {
   background:#4c7ffe ;
   color: #fff !important;
   }
   span.created-date {
   color: gray;
   padding-left: 10px;
   font-size: 13px;
   font-weight: 600;
   }
   span.created-date i.fa.fa-clock-o {
   padding: 0 5px;
   }
   .dropdown12 span.caret {
   right: -2px;
   z-index: 99999;
   border-top: 4px solid #555;
   border-left: 4px solid transparent;
   border-right: 4px solid transparent;
   border-bottom: 4px solid transparent;
   top: 12px;
   position: relative;
   }
   span.timer {
   padding: 0 10px;
   }
   body {
   font-family:"Arial", Helvetica, sans-serif;
   text-align: center;
   }
   #controls{
   font-size: 12px;
   }
   #time {
   font-size: 150%;
   }
</style>
<?php
   function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
   }

   function calculate_test($a,$b){
$difference = $b-$a;

$second = 1;
$minute = 60*$second;
$hour   = 60*$minute;

$ans["hour"]   = floor(($difference)/$hour);
$ans["minute"] = floor((($difference)%$hour)/$minute);
$ans["second"] = floor(((($difference)%$hour)%$minute)/$second);
//echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

$test=$ans["hour"].":".$ans["minute"].":".$ans["second"];
return $test;
}
function time_to_sec($time) {
list($h, $m, $s) = explode (":", $time);
$seconds = 0;
$seconds += (intval($h) * 3600);
$seconds += (intval($m) * 60);
$seconds += (intval($s));
return $seconds;
}
function sec_to_time($sec) {
return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}

 ?>

<link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/responsive.dataTables.css">
<link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/progress-circle.css">

<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section col-xs-12 floating_set">
                           
                           <div class="all_user-section floating_set">
                              
<!-- success message -->
            
<!-- end of success message -->


                              <div class="all_user-section2 floating_set">
                                 <div class="tab-content">
                                    <div id="alltasks" class="tab-pane fade in active">
                                    




<table  class="table client_table1 all_task_table text-center display nowrap">
<thead><tr><th>User Name</th><th>Task Name</th><th>Time Spent<th>status</th></tr></thead><tbody>
<?php 
foreach ($live_worker as $v) 
{

?>
<tr><td><?=$v['crm_name']?></td><td><?=$v['subject']?></td><td>
<?php

$hours=0;
$mins=0;
$sec=0;
$min=0;
$pause='';
       if($v['time_start_pause']!=''){
    $res=explode(',',$v['time_start_pause']);
    //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
    $res1=array_chunk($res,2);
    $result_value=array();
    $pause='on';
    foreach($res1 as $rre_key => $rre_value)
    {
       $abc=$rre_value;
       if(count($abc)>1){
       if($abc[1]!='')
       {
          $ret_val=calculate_test($abc[0],$abc[1]);
          array_push($result_value, $ret_val) ;
       }
       else
       {
        $pause='';
          $ret_val=calculate_test($abc[0],time());
           array_push($result_value, $ret_val) ;
       }
      }
      else
      {
        $pause='';
          $ret_val=calculate_test($abc[0],time());
           array_push($result_value, $ret_val) ;
      }

?>

<input type="hidden" name="trhours_<?php echo $v['id'];?>" id="trhours_<?php echo $v['id'];?>" value='<?php echo $hours; ?>' >
  <input type="hidden" name="trmin_<?php echo $v['id'];?>" id="trmin_<?php echo $v['id'];?>" value='<?php echo $min;?>' >
  <input type="hidden" name="trsec_<?php echo $v['id'];?>" id="trsec_<?php echo $v['id'];?>" value='<?php echo $sec; ?>' >
  <input type="hidden" name="trmili_<?php echo $v['id'];?>" id="trmili_<?php echo $v['id'];?>" value='0' >
  <input type="hidden" name="trpause_<?php echo $v['id'];?>" id="trpause_<?php echo $v['id'];?>" value="<?php echo $pause; ?>" >

  <?php  }


    $time_tot=0;
     foreach ($result_value as $re_key => $re_value)
      {
        $time_tot+=time_to_sec($re_value);
     }
     $hr_min_sec=sec_to_time($time_tot);
     $hr_explode=explode(':',$hr_min_sec);
     $hours=(int)$hr_explode[0];
     $min=(int)$hr_explode[1];
     $sec=(int)$hr_explode[2];
   }
?>
<div class="stopwatch" data-autostart="false" data-date="<?php echo '2018/06/23 15:37:25'; ?>" data-id="<?php echo $v['id']; ?>"  data-hour="<?php echo $hours;?>" data-min="<?php echo $min; ?>" data-sec="<?php echo $sec;?>" data-mili="0"  data-current="Start" data-pauseon="<?php echo $pause;?>" >
    <div class="time">
        <span class="hours"></span> : 
        <span class="minutes"></span> : 
        <span class="seconds"></span> :: 
        <span class="milliseconds"></span>
    </div>
    
</div></td><td>In Prograss</td></tr>
<?php } ?>

</tbody>
</table>
                                      
                                       <div class="client_section3  floating_set">
                                          <div id="status_succ"></div>
                                        
                           </div>
                           <!-- admin close -->
                        </div>
                        <!-- Register your self card end -->
                     </div>
                  </div>
               </div>
               <!-- Page body end -->
            </div>
         </div>
         <!-- Main-body end -->
         <div id="styleSelector">
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<!-- <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>  -->
<!-- <script src="<?php echo base_url();?>js/jquery.workout-timer.js"></script> -->
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.tagsinput.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
 <script type="text/javascript" language="javascript" src="http://remindoo.org/CRMTool/assets/js/dataTables.responsive.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/js/jquery.tagsinput.js"></script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script> 
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>

<script>



 
      $(document).ready(function(){

     $('.stopwatch').each(function (){

        var element = $(this);
        var running = element.data('autostart');
        var for_date = element.data('date');
       
        var task_id=element.data('id');

        var data_pause=element.data('pauseon');
       
        var hours=element.data('hour');
        var minutes=element.data('min');
        var seconds=element.data('sec');
        var milliseconds=element.data('mili');
      var hoursElement = element.find('.hours');
        var minutesElement = element.find('.minutes');
        var secondsElement = element.find('.seconds');
        var millisecondsElement = element.find('.milliseconds');

        function prependZero(time, length) {

          //  alert('zzz');
            // Quick way to turn number to string is to prepend it with a string
            // Also, a quick way to turn floats to integers is to complement with 0
            time = '' + (time | 0);
            // And strings have length too. Prepend 0 until right.
            while (time.length < length) time = '0' + time;
            return time;
        }

        function setStopwatch(hours, minutes, seconds, milliseconds) {
            // Using text(). html() will construct HTML when it finds one, overhead.
            hoursElement.text(prependZero(hours, 2));
            minutesElement.text(prependZero(minutes, 2));
            secondsElement.text(prependZero(seconds, 2));
            millisecondsElement.text(prependZero(milliseconds, 3));
        }

        // Update time in stopwatch periodically - every 25ms
        function runTimer() {
            // Using ES5 Date.now() to get current timestamp            
            var startTime = Date.now();
         
        //  var startTime=1530000373832;

        //  //var startTime=1529926802;
        //  var startTime=new Date("2016/06/25 00:00:00");  
        // var startTime = new Date("June 23, 2018 15:37:25").getTime();
        if(hours!=0 && minutes!=0 && seconds!=0 && milliseconds!=0)
        {
             var startTime = Date.now();
        }
        else
        {
           // hours=$('#trhours_'+task_id).val();
           //  $minutes=$('#trmin_'+task_id).val();
           //  seconds=$('#trsec_'+task_id).val();
           //  milliseconds=$('#trmili_'+task_id).val();
            //var startTime = new Date("June 23, 2018 15:37:25").getTime();
//            var startTime = new Date("2018/06/23 15:37:25").getTime();
           //var startTime = new Date(for_date).getTime();
           var startTime = Date.now();
        }
        
            var prevHours = hours;
            var prevMinutes = minutes;
            var prevSeconds = seconds;
            var prevMilliseconds = milliseconds;



            timer = setInterval(function () {
               // var timeElapsed = Date.now() - startTime;
                var timeElapsed = Date.now() - startTime;
               
                hours = (timeElapsed / 3600000) + prevHours;
                minutes = ((timeElapsed / 60000) + prevMinutes) % 60;
                seconds = ((timeElapsed / 1000) + prevSeconds) % 60;
                milliseconds = (timeElapsed + prevMilliseconds) % 1000;
            element.attr('data-hour',hours);
            element.attr('data-min',minutes);
            element.attr('data-sec',seconds);
            element.attr('data-mili',milliseconds);
            element.attr('data-pauseon','');

           // $('#trpause_'+task_id).val('');

            // $('#trhours_'+task_id).val(hours);
            // $('#trmin_'+task_id).val(minutes);
            // $('#trsec_'+task_id).val(seconds);
            // $('#trmili_'+task_id).val(milliseconds);

                setStopwatch(hours, minutes, seconds, milliseconds);
          //   $.ajax({
          //   url: '<?php echo base_url();?>user/task_countdown_update/',
          //   type: 'post',
          //   data: { 'task_id':task_id,'hours':hours,'minutes':minutes,'seconds':seconds,'milliseconds':milliseconds },
          //   timeout: 3000,
          //   success: function( data ){
          //   //  alert('updated');
          //   }
          // });

            }, 25);
        }  runTimer();

        // Split out timer functions into functions.
        // Easier to read and write down responsibilities
       


    });

             
  
   });
</script>


    
<!-- end of 29-06-2018 -->
<!-- 30-07-2018 -->

<!-- end of 30-07-2018 -->