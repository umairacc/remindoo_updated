<style>
 /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   .payment-table td {
      padding: 10px;
  }
  .payment-table tr {
    border: 1px solid #ccc;
}
</style>
<!-- <div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <!--<div class="main-body">
         <div class="page-wrapper">
            <div style="width: 700px;margin: 0 auto;background: #fff;padding: 30px;box-shadow:0 1px 11px 0 rgba(0, 0, 0, 0.12);box-shadow:0 1px 11px 0 rgba(0, 0, 0, 0.12);"> -->
               <table class="invoice-details generate-invoice" border="1" style="width:100%;">
                  <tr>
                    <td> 
                        <table style="border: 1px solid #e9ecef;width:100%;border-collapse: collapse;">
                          <tr>
                            <td  style="padding: 10px;border-bottom: 1px solid #eee;">
                              <h3 style="font-size: 1.75rem;margin-bottom: 5px;margin-top: 0;"><i class="fa fa-gbp" aria-hidden="true"></i><?php echo $grand_total; ?></h3>
                              <h5 style="font-size: 14px;margin: 0;">Amount due <span>(GBP)</span></h5>
                            </td>
                          </tr>
                          <tr>
                            <td style="padding: 10px;border-right: 1px solid #eee;    border-bottom: 1px solid #ddd;"><h3 class="upon-res" style="font-size: 18px;margin: 0;">Upon Reciept</h3>
                            <h6 style="font-size: 14px;margin: 10px 0;">Due</h6></td>
                            <td style="padding: 10px;    border-bottom: 1px solid #ddd;">
                            <?php if(isset($invoice_no)) { ?> 
                               <span class="due-date12" style="font-size: 17px;padding-bottom: 5px;display: block;"><?php echo $invoice_no; ?></span>
                              <h5 style="font-size: 14px;    margin: 5px 0;" class="invoice-no">invoice no</h5>
                            <?php } ?>
                             
                            </td>
                          </tr>
                          <tr>
                            
                          <tr>
                        </table>
                   
                    </td>
                  </tr>
                  <tr>
                    <td>
                    <!--   <div class="generate-top"> -->
                       <!--  <span class="project-name" style="float: left;width: 50%;font-size: 29px;text-transform: capitalize;"> -->
                          invoice
                          <!-- <span style="display: block;">project name</span> -->
                      <!--   </span> -->
                        <span class="date-sec" style="float: left;width: 50%;text-align: right;line-height: 74px;">
                        <?php if(isset($invoice_date)) { ?> 
                           <input style="width: 100px;padding-left: 10px;height: 30px;font-size: 14px;" type"text" class="g-date" value="<?php echo $invoice_date; ?>"> 
                        <?php } ?>

                        <?php if(isset($invoice_startdate)) { ?> 
                           <input style="width: 100px;padding-left: 10px;height: 30px;font-size: 14px;" type"text" class="g-date" value="<?php echo $invoice_startdate; ?>"> 
                        <?php } ?>
                         
                        </span>
                    <!--  </div> -->
                    </td>
                  </tr>
                  <tr>
                  <td>
                 
                             <table style="width:100%;border-collapse: collapse;">
                                <tr>
                                 <th style="background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">Item</th>
                                   <th style="background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">description</th>
                                   <th style="background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">qty</th>
                                   <th style="background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">price</th>
                                   <th style="width:5%;background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">amount</th>
                                </tr>
                              <?php foreach ($staff_datas as $st_key => $st_value) {
                                if($st_value['assign']=='assign')
                                {
                                   $itmname = $st_value['name'];
                                }
                                if($st_value['assign']=='team')
                                {
                                   $itmname = $st_value['name']."--".$st_value['team'];
                                }
                                if($st_value['assign']=='department')
                                {
                                   $itmname = $st_value['name']."--".$st_value['team']."--".$st_value['team'];  
                                }
                                 
                                $descr = $task_id."--".$task_name;
                  $qty = $diffence;// for start end date
                  $uprice = $st_value['rate'];
                  $amt_gbp = $st_value['overall_rate']; ?>    
                                <tr>
                                 <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"><?php echo $itmname; ?></td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"><?php echo $descr; ?></td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"><?php echo $qty; ?></td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"><i class="fa fa-gbp"></i> <?php echo $uprice; ?></td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"> <i class="fa fa-gbp"></i> <?php echo $amt_gbp; ?></td>
                                </tr>
                              <?php } ?>  
                          
                              </table>                
                        <table style="border: 1px solid #e9ecef;width:100%;">
                          <tr>
                          <th style="background: transparent;padding: 10px;border: none;text-align: right;font-size: 15px;">sub total</th>
                          <td style="border-right: none;background: transparent;border: none;text-align: right;padding: 10px;"><i class="fa fa-gbp">  <?php echo $sub_tot; ?></i></td>
                          </tr>
                          <tr>
                          <th tyle="background: transparent;padding: 10px;border: none;text-align: right;font-size: 15px;">total</th>
                          <td style="border-right: none;background: transparent;border: none;text-align: right;padding: 10px;"><i class="fa fa-gbp"> <?php echo $sub_tot; ?></i></td>
                          </tr>
                        </table>                    
                  </td>
                  </tr>
               
              </table>
           

<!-- ajax loader -->

</body>
</html>