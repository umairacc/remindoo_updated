<?php $this->load->view('includes/header');?>
<link href="<?php echo base_url();?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />
<div class="pcoded-content adminsettingcls">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <!--start-->
               <div class="title_page01 floating">
                  <div class="modal-alertsuccess alert alert-success" style="display:none;"
                     >
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                     <div class="pop-realted1">
                        <div class="position-alert1">
                           Your record was successfully updated.
                        </div>
                     </div>
                  </div>
                  <div class="modal-alertsuccess alert alert-danger" style="display:none;">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                     <div class="pop-realted1">
                        <div class="position-alert1">
                           Please fill the required fields.
                        </div>
                     </div>
                  </div>

             <div class="deadline-crm1 floating_set" style="display: none;">
                   <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                 <li class="nav-item form_info">
                        <a class="nav-link form_info active" data-toggle="tab" href="#adminprofile">Admin Profile</a>
                      </li>
                      <li class="nav-item ">
                        <a class="nav-link " data-toggle="tab" href="#adminsettings">Admin settings</a>
                      </li>
               </ul>
             </div>


                  <div class="service_view01 upload-file05">

                     

                      <div class="tab-content"> 
<div id="adminsettings" class="tab-pane active adminsettingtabcls " >
      <form class="client-firm-info1 validation fr-colsetting fr-colsettingfloatcls" method="post" action="<?php echo base_url().'sample/admin_settings'?>" enctype="multipart/form-data">
                    <div class="information-tab floating_set">
      <div class="deadline-crm1 ">
               <div class="Footer common-clienttab pull-right">
               <div class="change-client-bts1">
                  <input type="submit" class="signed-change1" id="save_exit" value ="Save & Exit">
               </div>
            </div>
            </div>

              <div class="space-required">
      <div class="document-center client-infom-1 floating_set divicls">
         <div class="Companies_House floating_set">
            <div class="pull-left widthclsset">
               <h2>Admin Setting</h2>
            </div>
          
         </div>
               <div class="form-group row name_fields">
                     <label class="col-sm-10 col-form-label">Service Reminders</label>
                     <div class="col-sm-2 dislaynpnecls">
                        <input type="checkbox" class="js-small f-right" name="service_reminder" id="" value="1" <?php if(isset($admin_set[0]['service_reminder']) && ($admin_set[0]['service_reminder']=='1') ){ ?> checked="checked"<?php } ?>>
                     </div>
                  </div>

                  <div class="form-group row name_fields">
                     <label class="col-sm-10 col-form-label">Missing Details send option in userlist</label>
                     <div class="col-sm-2 dislaynpnecls">
                        <input type="checkbox" class="js-small f-right" name="missing_details" id="" value="1" <?php if(isset($admin_set[0]['missing_details']) && ($admin_set[0]['missing_details']=='1') ){ ?> checked="checked"<?php } ?>>
                     </div>
                  </div>


                       <div class="form-group row name_fields">
                     <label class="col-sm-10 col-form-label">Automatically Create Missing Details Task</label>
                     <div class="col-sm-2 dislaynpnecls">
                        <input type="checkbox" class="js-small f-right" name="missing_task" id="" value="1" <?php if(isset($admin_set[0]['missing_task']) && ($admin_set[0]['missing_task']=='1') ){ ?> checked="checked"<?php } ?>>
                     </div>
                  </div>

                       <div class="form-group row name_fields">
                     <label class="col-sm-10 col-form-label">Sms meeting Reminders</label>
                     <div class="col-sm-2 dislaynpnecls">
                        <input type="checkbox" class="js-small f-right" name="meeting_reminder" id="" value="1" <?php if(isset($admin_set[0]['meeting_reminder']) && ($admin_set[0]['meeting_reminder']=='1') ){ ?> checked="checked"<?php } ?>>
                     </div>
                  </div>


                   <div class="form-group row name_fields" style="display: none;">
                     <label class="col-sm-10 col-form-label">Deadline Day Sms To manager</label>
                     <div class="col-sm-2 dislaynpnecls">
                        <input type="checkbox" class="js-small f-right" name="deadline_reminder" id="" value="1" <?php if(isset($admin_set[0]['deadline_reminder']) && ($admin_set[0]['deadline_reminder']=='1') ){ ?> checked="checked"<?php } ?>>
                     </div>
                  </div>

                  </div>

                  </div>
                  </div>
                  </form>
</div>
                 </div>
               </div>
               <!-- close -->
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>

<?php $this->load->view('includes/footer');?>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script>
$( document ).ready(function() {
    // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#1abc9c',
               jackColor: '#fff',
               size: 'small'
           });
       });
    });
</script>