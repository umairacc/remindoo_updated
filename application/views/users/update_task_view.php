<?php $this->load->view('includes/new_header');?>
<link href="<?php echo base_url();?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />

<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree_select/style.css?ver=2">
 <!-- tinymce editor -->
<!-- <link rel="stylesheet" type="text/css" href="https://www.tiny.cloud/css/codepen.min.css"> -->
<!-- tinymce editor -->

<link href="<?php echo base_url();?>assets/css/timepicki.css" rel="stylesheet">

<style>
  
.position-alert1 h4 {
    border-bottom: 1px solid #ddd;
    padding-bottom: 10px;
    margin-bottom: 15px;
}
#summary_new p {
    border-bottom: 1px solid #ddd;
    padding: 10px 0px;
    margin: 0;
    color: green;
}
#summary_val p
{
  color: green;
}
label.remider_option_error.error {
    text-align: center;
    padding-left: 0 !important;
}
.picker-category.form-group.border-checkbox-section.form-radio {
    padding: 0px 30px 0px 30px;
}
</style>
<?php 

$option='';
function validImage($file) {
   $size = getimagesize($file);
   return (strtolower(substr($size['mime'], 0, 5)) == 'image' ? true : false);  
}

   $team = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();
    $service_filter=array();

     $service_filter = $this->db->query("select `services` from admin_setting where firm_id=".$_SESSION['firm_id']." ")->row_array();
    $service_filter=json_decode($service_filter['services'],true);

      $service_filter=array_keys(array_diff($service_filter,array(0)));

   
   $department=$this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();
   
   $for_create_id='';

   ?>

<?php
  

   ?>



<?php


  $lead_option="";
  $option='';
  $related_to_service_action = '';
  $billable_disable='';
  $client_disable='';
  $ignore='';
  $sub_option='';

  $settings_val = $this->db->query('select * from tblemailtemplates where id in(4,3,5,6,7,8,10,11,12,13,14,15)')->result_array();

  $new_array = array_column( $settings_val , 'type' );

  $related_to_service = $task_form[0]['related_to_services'];

  if( $task_form[0]['related_to']=='sub_task' )
  {
    $option='disabled';
    $sub_option="display:none";
    $related_to_service_action = "display:none";
  }

   
 else if( $task_form[0]['related_to']=='leads')
  {
    $lead_option="display:none";
    $ignore='ignore';
    $related_to_service_action = "display:none";


  }
  else if($related_to_service=='0' || $related_to_service=='')
  {
    $related_to_service_action = "display:none";
  }
  else
  {
    // $related_to_service_action = "";
    $lead_option="";
     $ignore='';
     $sub_option="";
  }

  // if($related_to_service==0)
  // {

  //   $related_to_service_action = "display:none";
  // }

//echo  $related_to_service_action;
?>




  <!-- normal style 03-10-2018 -->
  <style type="text/css">
      /** for add steps button 03-10-2018 **/
   input.extra_steps_options
   {
    background: #f76480;
    border-color: #f76480;
   }
  </style>
  <!-- end of 03-10-2018 -->
 <!-- for include and exclude msg options -->
  <div class="modal-alertsuccess  alert alert-success-include-exclude" style="display:none;">
      <div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <div class="pop-realted1">
          <div class="position-alert1 include_exclude_success_msg"> Your task was successfully updated.
          </div>
       </div>
    </div></div>

<div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
       <div class="newupdate_alert">  <a href="#" class="close" id="close_info_msg">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
               Please! Select Record...
         </div>
         </div></div>
</div>
<!-- end of include and exclude options -->
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <!--start-->
                  <div class="col-sm-12 common_form_section1 was-sucess edit-tsk">
                     <!-- model success msg contect -->
                     <div class="modal-alertsuccess alert alert-danger-check succ dashboard_success_message1" style="display:none;">
                        <div class="newupdate_alert"><a href="#" class="close" data-dismiss="modal">×</a>
                        <div class="pop-realted1">
                           <div class="position-alert1">
                           <h4>Summary</h4> 
                              <div id="summary_new"></div>
<!--                               <input type="hidden" name="recuring_sus_msg" id="recuring_sus_msg">
 -->                           </div></div>
                        </div>
                     </div>
                     <!-- end of success msg content -->
                     <form id="update_new_task" method="post" action="" enctype="multipart/form-data">
                        <div class="deadline-crm1 floating_set single-txt12">
                           <ul class="nav nav-tabs all_user1 md-tabs pull-left">
                              <li class="nav-item">
                                 Edit Task
                                 <div class="slide"></div>
                              </li>
                           </ul>
                           <!-- modal close-->
                           <!-- <div class="form-group create-btn col-xs-12 f-right">
                              <input type="hidden" name="task_id" value="<?php if( !empty($task_form[0]['id'] ) ){ echo $task_form[0]['id'];}?>">
                              <input type="submit" name="add_task" class="btn-primary" value="Update"/>
                             
                              <input type="hidden" name="inex_option" id="inex_option" value="">
                           </div> -->
                        </div>

                        <div class="modal-alertsuccess  alert alert-success" style="display:none;">
                         <div class="newupdate_alert">  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1"> 
                              Your task was successfully updated.
                              </div>
                            </div>
                           </div>
                        </div>

                        <div class="modal-alertsuccess alert alert-danger" style="display:none;">
                           <div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                             <div class="pop-realted1">
                                <div class="position-alert1">
                                   Please fill the required fields.
                                </div>
                             </div>
                           </div>
                        </div>

                        <div class="col-xs-12 new-task add_new_post04 updtask update-task-wrapper">
                           <div class="col-xs-12 add_new_post04">
                              <div class="edit-update-task">
                                 <!-- <div class="form-group create-btn col-xs-12 f-right">
                                    <input type="hidden" name="task_id" value="<?php if( !empty($task_form[0]['id'] ) ){ echo $task_form[0]['id'];}?>">
                                    <input type="submit" name="add_task" class="btn-primary" value="Update"/>
                                 
                                    <input type="hidden" name="inex_option" id="inex_option" value="">
                                 </div> -->
                              <div class="row update-header-wrap">
                                 <div class="col-xs-12 attached-popup1">
                                    <div class="form-group check-options checkbox-color checkbox-primary" style="<?php echo $lead_option ?>">
                                       <div style="display: none;">
                                          <input type="checkbox"  name="public" value="Public" id="p-check" <?php if(isset($task_form[0]['public']) && $task_form[0]['public']=='Public') {?> checked="checked"<?php } ?>>
                                          <label for="p-check">Public </label>
                                       </div>
                                       
                                       <input type="checkbox"  name="billable" value="Billable" id="p-check1" <?php if(isset($task_form[0]['billable']) && $task_form[0]['billable']=='Billable') { ?> checked="checked" <?php } ?> >
                                       <label for="p-check1"> Billable</label>

                                  

                                       <?php if(isset($task_form[0]['billable']) && ($task_form[0]['billable']) && ($task_form[0]['task_status']=='5') =='Billable' &&  empty($task_form[0]['sub_task_id'])) { ?>
                                          <a href="<?php echo base_url(); ?>invoice/EditInvoice/<?php echo $task_form[0]['invoice_id']; ?>" class="btn btn-success" target="_blank" >View Invoice</a>
                                          <?php 
                                          }else{
                                             if(isset($task_form[0]['billable']) && ($task_form[0]['billable']) && ($task_form[0]['task_status']=='5') =='Billable' &&  !empty($task_form[0]['sub_task_id'])) { 
                                                $check_array = $this->db->query("SELECT task_status FROM add_new_task WHERE id  IN(".$task_form[0]['sub_task_id'].")")->result_array();
                                                
                                                if(is_array($check_array)){                                                               
                                                   if(count($check_array>0)){
                                                      $filter_val = array_column($check_array, 'task_status');
                                                      $result     = array_diff($filter_val,array('5'));

                                                      if(count($result)==0){ ?>
                                                         <a href="<?php echo base_url(); ?>invoice/EditInvoice/<?php echo $task_form[0]['invoice_id']; ?>" class="btn btn-success" target="_blank" >View Invoice</a><?php
                                                      }
                                                   }
                                                }
                                             }
                                          } ?>
                                    </div>
									
									
                                    
                                 </div>
								 
								  <div class="form-group attach-files f-right update-attach-hidden">
                                       <div id="input_container" style="width: 0px; height: 0px; overflow: hidden"><input type="file" id="inputfile" name="attach_file[]" multiple="multiple" onchange="readURL(this);" /></div>
                                       <div class="button" onclick="uploadss();">attach file</div>
                                       <span id="preview_image_name"></span>
                                       <div class="attachedfileshow">
                                       <?php if(isset($task_form[0]['attach_file']) && ($task_form[0]['attach_file']!='') ){ //echo $task_form[0]['attach_file'];
                                          $ex_img_val=explode(',',$task_form[0]['attach_file']);
                                                foreach ($ex_img_val as $img_key => $img_value) {

                                          }
                                            }?>
                                       <input type="hidden" name="image_name" id="image_name" value="<?php if(isset($task_form[0]['attach_file']) && ($task_form[0]['attach_file']!='') ){ echo $task_form[0]['attach_file'];}?>">
                                        <?php if(isset($task_form[0]['attach_file']) && ($task_form[0]['attach_file']!='') ){ ?>
                                       <div class="jFiler-items jFiler-row">
                                           <ul class="jFiler-items-list jFiler-items-grid">
                                                                      <?php 
                                      foreach ($ex_img_val as $img_key => $img_value) {
                                        ?>

                                        <?php

                                        if(is_array(getimagesize($img_value))){


                                        ?>
                                              <li class="jFiler-item for_img_<?php echo $img_key; ?>" data-jfiler-index="3" style="">
                                               <input type="hidden" name="already_upload_img[]" value="<?php echo $img_value; ?>" >
                                                 <div class="jFiler-item-container">
                                                    <div class="jFiler-item-inner">
                                                       <div class="jFiler-item-thumb">
                                                          <div class="jFiler-item-status"></div>
                                                          <div class="jFiler-item-info">                                        
                                                                                         
                                                          </div>
                                                          <div class="jFiler-item-thumb-image"><img src="<?php echo $img_value;?>" draggable="false"></div>
                                                       
                                                    </div>
                                                    <div class="jFiler-item-assets jFiler-row">
                                                          <ul class="list-inline pull-left">
                                                             <li>
                                                                <div class="jFiler-jProgressBar" style="display: none;">
                                                                   <div class="bar"></div>
                                                                </div>
                                                               
                                                             </li>
                                                          </ul>
                                                          <ul class="list-inline pull-right">
                                                             <li><a class="icon-jfi-trash jFiler-item-trash-action remove_file" data-id="<?php echo $img_key ?>"></a></li>
                                                          </ul>
                                                       </div>
                                                 </div>
                                              </li>
                                       <?php }else{ ?>
                                              <li class="jFiler-item jFiler-no-thumbnail for_img_<?php echo $img_key; ?>" data-jfiler-index="2" style="">
                                               <input type="hidden" name="already_upload_img[]" value="<?php echo $img_value; ?>" >
                                                 <div class="jFiler-item-container">
                                                    <div class="jFiler-item-inner">
                                                       <div class="jFiler-item-thumb">
                                                          <div class="jFiler-item-status"></div>
                                                          <div class="jFiler-item-info">                                                               </div>
                                                          <div class="jFiler-item-thumb-image"><span class="jFiler-icon-file f-file f-file-ext-odt" style="background-color: rgb(63, 79, 211);">.odt</span></div>
                                                       </div>
                                                       <div class="jFiler-item-assets jFiler-row">
                                                          <ul class="list-inline pull-left">
                                                             <li>
                                                                <div class="jFiler-jProgressBar" style="display: none;">
                                                                   <div class="bar"></div>
                                                                </div>
                                                               
                                                             </li>
                                                          </ul>
                                                          <ul class="list-inline pull-right">
                                                             <li><a class="icon-jfi-trash jFiler-item-trash-action remove_file" data-id="<?php echo $img_key ?>" ></a></li>
                                                          </ul>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </li>
                                             <?php } ?>
                                       <?php } ?>
                                           </ul>
                                        </div>
                                        <?php } ?>
                                      </div>
                                    </div>
                                    </div>
									
                                    </div>



                                 <div class="space-equal-data12 new-array-tsk col-xs-12 update-sub-desc-wrapper">
                                    <div class="space-equal-data col-sm-6 attach-task-new">
                                       <div class="form-group">
                                          <label>Subject</label>
                                          <input type="text" class="form-control clr-check-client" name="subject" placeholder="Enter Subject" value="<?php if(isset($task_form[0]['subject']) && ($task_form[0]['subject']!='') ){ echo $task_form[0]['subject'];}?>">
                                       </div>
                                       <div class="row new-tsk01">
                                          <div class="col-sm-6">
                                             <div class="form-group date_birth">
                                                <label>Start Date</label>
                                                <div class="start-date1">
                                                   <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                   <input type='text' class="clr-check-client" id='txtFromDate' name="startdate" value="<?php if(isset($task_form[0]['start_date']) && ($task_form[0]['start_date']!='') ){ echo date('d-m-Y',strtotime($task_form[0]['start_date']));}?>"/>
                                                   <span class="glyphicon glyphicon-calendar"></span>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="form-group date_birth">
                                                <label>End Date</label>
                                                <div class="start-date1">
                                                   <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                   <input type='text' class="clr-check-client" id='txtToDate' name="enddate" value="<?php if(isset($task_form[0]['end_date']) && ($task_form[0]['end_date']!='') ){ echo  date('d-m-Y',strtotime($task_form[0]['end_date']));}?>"/>
                                                   <span class="glyphicon glyphicon-calendar"></span>
                                                </div>
                                             </div>
                                          </div>
                                       
                                       <div class="form-group widthfifty">
                                          <label>Priority</label>
                                          <select name="priority" class="form-control clr-check-select" id="priority">
                                             <option value="low" <?php if(isset($task_form[0]['priority']) && $task_form[0]['priority']=='low') {?> selected="selected"<?php } ?>>Low</option>
                                             <option value="medium" <?php if(isset($task_form[0]['priority']) && $task_form[0]['priority']=='medium') {?> selected="selected"<?php } ?>>Medium</option>
                                             <option value="high" <?php if(isset($task_form[0]['priority']) && $task_form[0]['priority']=='high') {?> selected="selected"<?php } ?>>High</option>
                                             <option value="super_urgent" <?php if(isset($task_form[0]['priority']) && $task_form[0]['priority']=='super_urgent') {?> selected="selected"<?php } ?>>Super Urgent</option>
                                          </select>
                                       </div>
                              
                                    

                                          <div class="form-group widthfifty">
                                          <label>Task status*</label>
                                          <select name="task_status" class="form-control clr-check-select" id="task_status">

                                           <?php foreach($task_status as $key => $status_val) { ?>

                                       <option value="<?php echo $status_val['id']; ?>" <?php if($task_form[0]['task_status'] ==$status_val['id']){ echo 'selected="selected"'; }?> ><?php echo $status_val['status_name']; ?></option>

                                        <?php } ?>

                       
                                          </select>
                                       </div>





                                         <!-- for realated to services 16-10-2018 -->
                                     <div style="<?php echo $lead_option ?> <?php echo $sub_option ?>" class="form-group widthfifty even ">
                                       <label>Related to Services</label>
                                          <?php
                                         $style_services='';

                                          if($task_form[0]['related_to'] == 'sub_task')
                                          { 
                                            $style_services="pointer-events:none;";
                                          }

                                         $settings_val=$this->db->query('select * from service_lists where id !=16 ')->result_array();
                                 
                                          ?>

                                        <div class="dropdown-sin-5 lead-form-st " style="<?php echo $style_services; ?>" >

                                        <select class="form-control clr-check-select" name="related_to_services" id='related_to_services' placeholder="Select" style="<?php echo $style_services; ?>">
                                        
                                        <option value=''>None</option>       
                                          <option  disabled>Services</option>                            

                                          <?php      
                                           $sel1="";
                                              $work_flow = $this->Service_model->get_firm_workflow();  
                                 
                                  foreach($settings_val as $set_key => $set_value)
                                   { 
                                     if(in_array($set_value['id'], $service_filter)){

                                    $sel = '';

                                  if( !empty( $related_to_service ) & ("ser_".$set_value['id'] == $related_to_service || $set_value['id'] == $related_to_service)) 
                                  { 
                                      $sel = 'selected="selected"';
                                  } 
                                 


                                    ?>
                                    <option value="ser_<?php echo $set_value['id']; ?>"  <?php echo $sel?> > <?php echo $set_value['service_name']; ?></option>

                                    <?php
                                    } }


                                      ?>
                                      <option  disabled>Work Flow</option>
                                             <?php 
                                       if(!empty($work_flow )){
                                       foreach ($work_flow as $key => $wf_value) {

                                          if( !empty( $related_to_service ) & "wor_".$wf_value['id'] == $related_to_service ) 
                                          { 
                                              $sel1 = 'selected="selected"';
                                          } 

                                        ?>

                                       <option value="wor_<?php echo $wf_value['id']; ?>" <?php echo $sel1 ?>><?php echo $wf_value['service_name']; ?></option>

                                       
                                     <?php } }
                                       ?>
                                       
                                       </select>
                                       </div>

                                       
                                       <a href="javascript:;" id="inc_exc_options" data-action = "inc_exc" data-services="<?php echo $related_to_service; ?>"  data-id="<?php echo $task_form[0]['id']; ?>" class="exc-btn services_options" style="<?php echo $related_to_service_action?>" > Include & Exclude Steps </a> 
                                

                                        <a href="javascript:;" name="added_extra_steps" id="added_extra_steps" data-services="<?php echo $related_to_service; ?>" data-action="extra_steps" data-id="<?php echo $task_form[0]['id']; ?>" class="exc-btn extra_steps_options" style="<?php echo $related_to_service_action?>" > Add Steps </a> 



                                     
                                    </div>

                                    <div class="form-group">
                                       <label for="estimated_time">Estimated Time</label>
                                       <input type="number" class="clr-check-select" name="estimated_time" id="estimated_time" value="<?php echo $task_form[0]['estimated_time'] ?>" min="0" placeholder="Estimated no. of hours">
                                    </div>

                                    <div style="<?php echo $lead_option ?><?php echo $sub_option ?>" class="form-group">
                                          <label>Choose Client/Firm</label>
                                          <div class="dropdown-sin-4 for_staff_permission">
                                             <select name="company_name" id="company_name" class="<?php echo $ignore ?> clr-check-select">
                                                <option value="">Select Company</option>
                                                <!-- <input type="text" name="company" id="company"> for_create_id-->
                                                <?php 
                                                
                                                     $query1=$this->db->query("SELECT * FROM `user` where autosave_status!='1' and user_type='FC' and crm_name!='' and firm_id=".$_SESSION['firm_id']." order by id DESC");
                                                   
                                                   $results1 = $query1->result_array();
                                                   $res=array();
                                                   foreach ($results1 as $key => $value) {
                                                   array_push($res, $value['id']);
                                                   }
                                                   if(!empty($res)){
                                                   $im_val=implode(',',$res);
                                                   // echo $im_val;
                                                   $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (".$im_val.") order by id desc ");    
                                                   $results = $query->result_array();  
                                                   }
                                                   else{
                                                   $results=0;
                                                   
                                                   $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id=".$_SESSION['id']." order by id desc ");    
                                                   $results = $query->result_array();  
                                                   }  
                                                      if(count($results) > 0)  
                                                      {  
                                                         foreach($results as $key => $val)
                                                         {  ?>
                                                <option value="<?php echo $val['id'];?>"  <?php if(isset($task_form[0]['company_name'])){     
                                                   if($task_form[0]['company_name']==$val['id']){ ?>
                                                   selected="selected"
                                                   <?php } } ?>
                                                   ><?php echo $val["crm_company_name"];?></option>
                                                <?php
                                                   }  
                                                   }  ?>
                                             </select>
                                          </div>
                                          <input type="hidden"  name="user_id" id="user_id" value="<?php echo $task_form[0]['company_name']; ?>">
                                       </div>
                                    <!-- end of related services 16-10-2018 -->
                                       <!-- <div id="base_projects"> </div> -->
                                       <!-- by client changes
                                       <div class="form-group widthfifty even">
                                          <label>Tag</label>
                                          <div class="dropdown-sin-2">
                                             <select style="display:none" name="tag[]" id="manager" multiple placeholder="Select">
                                                <?php 
                                                   $exp_tag=explode(',',$task_form[0]['tag']);
                                                   
                                                   foreach($tag as $tags) {
                                                                                  ?>
                                                <option value="<?php echo $tags['id']?>"  <?php if(isset($task_form[0]['tag']) && in_array( $tags['id'] ,$exp_tag )) {?> selected="selected"<?php } ?>><?php echo $tags['tag'];?></option>
                                                <?php } ?>
                                             </select>
                                          </div>
                                       </div> -->

                                       <div style="<?php echo $lead_option ?>" class="form-group widthfifty" >
                                          <label>Assign to (worker)</label>
                                          <input type="text" id="tree_select1" class="tree_select" name="assignees[]" placeholder="Select">                                              

                                          <input type="hidden" id="assign_role" name="assign_role" >

                                          <!--  <input type="text" class="form-control" name="worker" id="worker" placeholder="" value=""> -->
                                          <!--  <div class="dropdown-sin-2">
                                             <select style="display:none" multiple placeholder="Select">
                                                 <option value="1">sandyy</option>
                                                 <option value="2">kumar</option>
                                                 <option value="3">tesreds</option>
                                                 <option value="4">fgdsf</option>
                                             </select>
                                             </div> -->
                                          <?php  //print_r($task_form[0]['worker']);
                                             if(isset($task_form[0]['worker'])){
                                               $explode_worker=explode(',',$task_form[0]['worker']);
                                             }
                                             if(isset($task_form[0]['team'])){
                                               $explode_team=explode(',',$task_form[0]['team']);
                                             }
                                             if(isset($task_form[0]['department'])){
                                               $explode_department=explode(',',$task_form[0]['department']);
                                             }
                                             ?>
                                         <!--  <div class="dropdown-sin-2 lead-form-st for_staff_permission_worker">
                                             <select style="display:none" name="worker[]" id="worker" multiple placeholder="Select">
                                                <?php
                                                   if(isset($staff_form) && $staff_form!='') {
                                                         ?>
                                                <option disabled>Staff</option>
                                                <?php
                                                   foreach($staff_form as $value) {
                                                   ?>
                                                <option value="<?php echo $value['id']; ?>" <?php if(isset($task_form[0]['worker']) && in_array( $value['id'] ,$explode_worker )) {?> selected="selected"<?php } ?>><?php echo $value["crm_name"]; ?></option>
                                                <?php
                                                   }
                                                   }
                                                   if(isset($team) && $team!='') {
                                                   ?>
                                                <option disabled>Team</option>
                                                <?php
                                                   foreach($team as $value) {
                                                   ?>
                                                <option value="tm_<?php echo $value['id']; ?>" <?php if(isset($task_form[0]['team']) && in_array( $value['id'] ,$explode_team )) {?> selected="selected"<?php } ?>><?php echo $value["team"]; ?></option>
                                                <?php
                                                   }
                                                   }
                                                   if(isset($department) && $department!='') {
                                                   ?>
                                                <option disabled>Department</option>
                                                <?php
                                                   foreach($department as $value) {
                                                   ?>
                                                <option value="de_<?php echo $value['id']; ?>" <?php if(isset($task_form[0]['department']) && in_array( $value['id'] ,$explode_department )) {?> selected="selected"<?php } ?>><?php echo $value["new_dept"]; ?></option>
                                                <?php
                                                   }
                                                   }
                                                   ?>
                                             </select>
                                          </div> -->
                                       </div>
                                       <!-- <div class="form-group">
                                          <label>Notify to(manager)</label>
                                          <input type="text" class="form-control" name="manager" id="manager" placeholder="" value="<?php if(isset($task_form[0]['manager']) && ($task_form[0]['manager']!='') ){ echo $task_form[0]['manager'];}?>">
                                          </div> -->
                                    <!--    <div class="form-group widthfifty even <?php echo $option; ?>" >
                                          <label>Notify to(Manager)</label>
                                          <?php  //print_r($task_form[0]['worker']);
                                             if(isset($task_form[0]['manager'])){
                                               $explode_manager=explode(',',$task_form[0]['manager']);
                                             }
                                             ?>
                                          <div class="dropdown-sin-3 for_staff_permission_manager">
                                             <select style="display:none" name="manager[]" id="manager" multiple placeholder="Select">
                                                <?php
                                                   if(isset($manager) && $manager!='') {
                                                   
                                                   foreach($manager as $value) {
                                                   ?>
                                                <option value="<?php echo $value['id']; ?>" <?php if(isset($task_form[0]['manager']) && in_array( $value['id'] ,$explode_manager )) {?> selected="selected"<?php } ?>><?php echo $value["crm_name"]; ?></option>
                                                <?php
                                                   }
                                                   }
                                                   ?>
                                             </select>
                                          </div>
                                       </div> -->
									   </div>
                                       
                                       
                                       
                                       <!-- notify -->
                                    </div>
                                    <div class="space-equal-data col-sm-6">
                                       <!--   <div class="form-group">
                                          <label>Client/Firm</label>
                                             <input type="text" name="company_name" id="search_company" class="form-control" placeholder="Search Users Name" value="<?php if(isset($task_form[0]['company_name']) && ($task_form[0]['company_name']!='') ){ echo $task_form[0]['company_name'];}?>" />  
                                             <input type="hidden" value="<?php if(isset($task_form[0]['user_id']) && ($task_form[0]['user_id']!='') ){ echo $task_form[0]['user_id'];}?>" name="user_id" id="user_id">
                                             <div id="searchresult"></div>
                                          </div> -->
                                       
                                       
                                       <div class="form-group">
                                          <label>Description</label>
                                          <textarea type="text" class="form-control" name="description" id="description" placeholder="Write something"><?php echo !empty($task_form[0]['description'])? $task_form[0]['description']:''; ?>
                                          </textarea>
                                       </div>
                                       <div class="update-task-btn-wrapper">
                                          <div style="<?php echo $sub_option ?>" class="form-group">
                                             <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Make this recuring task</button>
                                             <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalone">Remind me about the task</button>
                                          </div>
                                          <div class="form-group create-btn col-xs-12 f-right">
                                             <input type="hidden" name="task_id" value="<?php if( !empty($task_form[0]['id'] ) ){ echo $task_form[0]['id'];}?>">
                                             <input type="submit" name="add_task" class="btn-primary" value="Update"/>                                 
                                             <input type="hidden" name="inex_option" id="inex_option" value="">
                                          </div>
                                       </div>
                                       <div class="form-group widthfifty even tag-design">
                                          <h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
                                          <input type="text" value="<?php echo $task_form[0]['tag']?> " name="tag" id="tags" class="tags" data-role="tagsinput" />
                                    </div>
                                       <!-- <div class="form-group cus-permission radio_button01">
                                          <input type="hidden"  name="customize" value="1" id="cus-per">
                                          
                                          
                                          <label for="cus-per">Customize Permissions</label>
                                          </div> -->
                                       
                                      
                                 </div>
                                 <!-- Modal -->
                                 <div class="modal fade recurring-msg common-schedule-msg1 complet-msg11" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
                                    <div class="modal-dialog modal-recurring-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header modal-recurring-header">
                                             <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                                             <h4 class="modal-title">Schedule Recurring Task</h4>
                                          </div>
                                          <div class="modal-body modal-recurring-body">
                                             <div class="col-xs-12 col-sm-6 left-cal form-group common-repeats1">
                                                <div class="form-group date_birth">
                                                   <label class="label-column1 col-form-label">From:</label>
                                                   <div class="start-date1 label-column2">
                                                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input type='text' class="required1" id='modalFromDate' name="sche_startdate" value="<?php echo $task_form[0]['sche_start_date']; ?>" />
                                                      <!-- for ref 23-06-2018 -->
                                                      <input type="hidden" name="for_recuring_start" id="for_recuring_start" value="<?php echo $task_form[0]['sche_start_date']; ?>">
                                                      <!-- end of ref task recurring 23-06-2018 -->
                                                   </div>
                                                   <!-- <span class="glyphicon glyphicon-calendar"></span> -->
                                                </div>
                                             </div>
                                             <!--        <div class="col-xs-12 col-sm-6 right-cal form-group common-repeats1">
                                                <div class="form-group date_birth">
                                                   <label class="label-column1 col-form-label">Send time:</label>
                                                   <div class="input-group bootstrap-timepicker timepicker label-column2">
                                                      <input id="timepicker1" type="text" class="form-control" name="sche_send_time" value="<?php echo $task_form[0]['sche_send_time']; ?>">
                                                  
                                                   </div>
                                                </div>
                                                </div> -->
                                             <div class="col-sm-6 form-group common-repeats1 ">
                                                <label class="label-column1 col-form-label">Send time:</label>
                                                <div class="label-column2  input-group bootstrap-timepicker timepicker">
                                                   <input id="timepicker1" type="text" class="form-control input-small required1" name="sche_send_time" value="<?php echo $task_form[0]['sche_send_time']; ?>" >
                                                   <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span> -->
                                                </div>
                                             </div>

                                                <div class="form-group col-sm-6 every-week01 floating_set">
                                                      <label class="label-column1  col-form-label">Every:</label>
                                                      <div class="label-column2">
                                                         <!-- <input type="text" class="form-control week-text" name="every" placeholder="" value=""> -->
                                                         <input type="number" class="form-control week-text required1"  name="every"  id="every" placeholder="" value="<?php echo $task_form[0]["sche_every_week"];?>">
                                                         <!-- <p>weeks</p> -->
                                                      </div>
                                                   </div>

                                             <div class="form-group col-sm-6">
                                                <label class="label-column1 col-form-label left-label">Repeats:</label>
                                                <div class="label-column2">
                                                   <select name="repeats" class="form-control repeats required1" id="repeats" data-date="" data-day="" data-month="">
                                                   <option value="">Select nothing</option>
                                                      <option value="week" <?php if($task_form[0]['sche_repeats']=="week"){ echo "selected"; } ?>>Weekly</option>
                                                      <option value="day"  <?php if($task_form[0]['sche_repeats']=="day"){ echo "selected"; } ?> >Day</option>
                                                      <option value="month"  <?php if($task_form[0]['sche_repeats']=="month"){ echo "selected"; } ?>>Monthly</option>
                                                      <option value="year"  <?php if($task_form[0]['sche_repeats']=="year"){ echo "selected"; } ?> >Yearly</option>
                                                   </select>
                                                   <!-- 23-06-2018 -->
                                                   <input type="hidden" name="recurring_repeats" id="recurring_repeats" value="<?php echo $task_form[0]['sche_repeats']; ?>">
                                                   <!--end 23-06-2018 -->
                                                </div>
                                             </div>

                                             <input type="hidden" name="every_sch_on" id="every_sch_on" value="<?php echo $task_form[0]['sche_on']; ?>">
                                             <!-- for recurring task 23-06-2018 -->
                                             <input type="hidden" name="recurring_every" id="recurring_every" value="<?php echo $task_form[0]["sche_every_week"];?>">
                                             <!-- <input type="hidden" name="recurring_every_date" id="recurring_every_date" value="<?php echo $task_form[0]["every_date"]; ?>"> -->
                                             <!-- end of recurring -->
                                            <?php 
                                                $sche_on = [];
                                               if($task_form[0]["sche_repeats"]=="week" && isset($task_form[0]['sche_on']))
                                               {
                                                $sche_on = explode(',',$task_form[0]['sche_on']);   
                                                }
                                               ?>
                                            <div id="weekly" class="every_popup1" style="<?php echo ($task_form[0]["sche_repeats"]=="week")?'display: block':'display: none';?>">
                                                <!-- <div class="form-group left-align">
                                                   <label class=" label-column1 col-form-label left-label">Every</label>
                                                   <div class="label-column2">
                                                      <input type="text" class="form-control week-text" name="every" placeholder="" value="">
                                                      <p>weeks</p>
                                                   </div>
                                                </div> -->
                                                <div class="form-group days-list floating_set">
                                                   <label class="left-label label-column1 col-form-label">On:</label>
                                                   <div class="label-column2">
                                                   <div class="days-left">
                                                    
                                                      <div class="checkbox-color checkbox-primary">
                                                         <input type="checkbox"  class="dayss border-checkbox" name="days[]" value="Monday" id="day-check2"
                                                         <?php if(isset($task_form[0]["sche_on"])&& in_array( "Monday" ,$sche_on )) { echo "checked";} ?>
                                                         >
                                                         <label class="border-checkbox-label" for="day-check2">Mon</label>
                                                      </div>
                                                      <div class="checkbox-color checkbox-primary">
                                                         <input type="checkbox"  class="dayss border-checkbox" name="days[]" value="Tuesday" id="day-check3" <?php if(isset($task_form[0]["sche_on"])&& in_array( "Tuesday" ,$sche_on )) { echo "checked";} ?>><label class="border-checkbox-label" for="day-check3" >Tue</label> 
                                                      </div>
                                                      <div class="checkbox-color checkbox-primary">
                                                         <input type="checkbox"  class="dayss border-checkbox" name="days[]" value="Wednesday" id="day-check4"
                                                         <?php if(isset($task_form[0]["sche_on"])&& in_array( "Wednesday" ,$sche_on )) { echo "checked";} ?>
                                                         >
                                                         <label class="border-checkbox-label" for="day-check4">Wed</label>
                                                      </div>
                                                      <div class="checkbox-color checkbox-primary">
                                                         <input type="checkbox"  class="dayss border-checkbox" name="days[]" value="Thursday" id="day-check5" 
                                                         <?php if(isset($task_form[0]["sche_on"])&& in_array( "Thursday" ,$sche_on )) { echo "checked";} ?>
                                                         >
                                                        <label class="border-checkbox-label" for="day-check5">Thur</label>
                                                      </div>
                                                      <div class="checkbox-color checkbox-primary">                       
                                                         <input type="checkbox"  class="dayss border-checkbox" name="days[]" value="Friday" id="day-check6"
                                                          <?php if(isset($task_form[0]["sche_on"])&& in_array( "Friday" ,$sche_on )) { echo "checked";} ?>
                                                          >

                                                         <label class="border-checkbox-label" for="day-check6">Fri</label>
                                                      </div>
                                                      <div class="direct-chk checkbox-color checkbox-primary">
                                                         <input type="checkbox"  class="dayss border-checkbox" name="days[]" value="Saturday" id="day-check7" <?php if(isset($task_form[0]["sche_on"])&& in_array( "Saturday" ,$sche_on )) { echo "checked";} ?>><label class="border-checkbox-label" for="day-check7">Sat</label>
                                                      </div>
                                                   </div>
                                                     <div class="checkbox-color checkbox-primary">  
                                                         <input type="checkbox" class="dayss border-checkbox"  name="days[]" value="Sunday" id="day-check1" 
                                                         <?php if(isset($task_form[0]["sche_on"])&& in_array( "Sunday" ,$sche_on )) { echo "checked";} ?>
                                                         >
                                                         <label class="border-checkbox-label" for="day-check1">Sun</label>
                                                      </div>
                                                   </div>
                                                   <label class="error empty-week-day"></label>
                                                </div>
                                               
                                             </div>
                                             <!-- 23-06-2018 for_recurring task -->
                                             <input type="hidden" name="recurring_end" id="recurring_end" value="<?php echo $task_form[0]['sche_ends']; ?>">
                                             <input type="hidden" name="recurring_msg" id="recurring_msg" value="<?php echo $task_form[0]['sche_after_msg']; ?>">
                                             <input type="hidden" name="recurring_end_date" id="recurring_end_date" value="<?php echo $task_form[0]['sche_on_date']; ?>">
                                             <!--end of recurring task -->
                                             <div class="form-group left-align radio_button01">
                                                <label class="left-label label-column1 col-form-label">Ends:</label>
                                                <div class="forms-option start-ac1 label-column2">
                                                   <div class="after-ac">
                                                      <input type="radio"  name="end" value="after" id="end-date" <?php if($task_form[0]['sche_ends']=='after'){ echo "checked"; } ?>>
                                                      <label for="end-date">After</label>
                                                      <input type="text" class="form-control message-count" name="message"
                                                      id="message" placeholder="" <?php if($task_form[0]['sche_ends']=='after'){ ?> value="<?php echo $task_form[0]['sche_after_msg']; ?>" <?php } ?> >
                                                      <span>messages</span>  
                                                   </div>
                                                   <div class="on-optin radio-width01">
                                                      <input type="radio"  name="end" value="on" id="end-date3"  <?php if($task_form[0]['sche_ends']=='on'){ echo "checked"; } ?>>
                                                      <label for="end-date3">On </label>
                                                      <div class="start-date1 date_birth input-group date">
                                                         <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                         <input type='text' class="" id='modalendDate' name="sche_on_date" <?php if($task_form[0]['sche_ends']=='on'){ ?> value="<?php
                                                         if($task_form[0]['sche_on_date']!='0000-00-00')
                                                         {
                                                          echo                                                         date("d-m-Y", strtotime($task_form[0]['sche_on_date']))
                                                         ; 
                                                         } ?>" <?php } ?>  data-year="" data-day="" data-month="" />
                                                      </div>
                                                   </div>
                                                   <div class="no-end radio-width01">
                                                      <input type="radio"  name="end" value="noend" id="end-date1" <?php if($task_form[0]['sche_ends']=='noend'){ echo "checked"; } ?> >
                                                      <label for="end-date1">No end date</label>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="form-group left-align">
                                                <label class="left-label label-column1 col-form-label">Summary:</label>
                                                <div id="summary"><?php echo $task_form[0]['recuring_sus_msg']; ?></div>
                                                <input type="hidden" name="recuring_sus_msg" id="recuring_sus_msg">
                                             </div>
                                          </div>
                                          <div class="modal-footer modal-recurring-footer">          
                                             
                                             <button type="button" name="button"  class="btn btn-default Save_Schedule" >Schedule</button>
                                             <button type="button" data-dismiss="modal" class="close">Cancel</button>                               
                                             <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- modal close-->
                                 <!--- ********************* 18-06-2018 *******************-->
                                 <!-- Modal one-->
                                 <div class="modal fade common-schedule-msg1 remind-me-picker" id="myModalone" role="dialog">
                                    <div class="modal-dialog modal-remind-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header modal-remind-header">
                                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                                             <h4 class="modal-title">Remind Me</h4>
                                          </div>
                                          <div class="modal-body modal-remind-body">
                                             <div class="picker-category form-group border-checkbox-section form-radio ">
                                             <label class="remider_option_error error"></label>
                                             <!-- display: none; for no functionlity to this -->

                                                <div class="hour-rate1" style="display: none;">
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" name="radio" id="repeat1" value="nobody" <?php if(in_array('nobody', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat1">Only if nobody responds</label>
                                                   </div>
                                                </div>
                                                <div class="hour-rate1">
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" name="radio"  id="repeat2" value="1hour" <?php if(in_array('1hour', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat2">In 1 hour</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" name="radio"   id="repeat3" value="2hour" <?php if(in_array('2hour', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?> ><label for="repeat3">In 2 hour</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" name="radio"  id="repeat4" value="4hour" <?php if(in_array('4hour', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat4">In 4 hour</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" name="radio"  id="repeat5" value="5hour" <?php if(in_array('5hour', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat5">In 6 hour</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" name="radio"  id="repeat6" value="8hour" <?php if(in_array('8hour', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat6">In 8 hour</label>
                                                   </div>
                                                </div>
                                                <div class="hour-rate1">
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio"   id="repeat7" class="remind_poup_data" value="tomorrow_morning" <?php if(in_array('tomorrow_morning', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat7">Tomorrow Morning </label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" value="tomorrow_afternoon" name="radio"  id="repeat8" <?php if(in_array('tomorrow_afternoon', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat8">Tomorrow Afternoon </label>
                                                   </div>
                                                </div>
                                                <div class="hour-rate1">
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio"  id="repeat9" class="remind_poup_data" value="1day" <?php if(in_array('1day', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?> ><label for="repeat9">In 1 day</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="remind_poup_data" value="2day" name="radio" id="repeat10" <?php if(in_array('2day', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat10"  >In 2 days</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox"  class="remind_poup_data" value="4day" name="radio"   id="repeat11" <?php if(in_array('4day', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat11">In 4 days</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio"  id="repeat12" class="remind_poup_data" value="6day" <?php if(in_array('6day', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat12">In 6 days</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio"  id="repeat13" class="remind_poup_data" value="8day" <?php if(in_array('8day', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?> ><label for="repeat13">In 8 days</label>
                                                   </div>
                                                </div>
                                                <div class="hour-rate1">
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio"  id="repeat14" class="remind_poup_data" value="1week" <?php if(in_array('1week', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat14">In 1 week</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio"   id="repeat15" class="remind_poup_data" value="2week" <?php if(in_array('2week', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?> ><label for="repeat15">In 2 weeks</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio"   id="repeat16" class="remind_poup_data" value="4week" <?php if(in_array('4week', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?> ><label for="repeat16">In 4 weeks</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio"  id="repeat17" class="remind_poup_data" value="6week" <?php if(in_array('6week', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?> ><label for="repeat17">In 6 weeks</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio"   id="repeat18" class="remind_poup_data" value="8week" <?php if(in_array('8week', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat18">In 8 weeks</label>
                                                   </div>
                                                </div>
                                                <div class="hour-rate1">
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio"  id="repeat19" class="remind_poup_data" value="1month" <?php if(in_array('1month', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat19">In 1 month</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat20" class="remind_poup_data" value="2month" <?php if(in_array('2month', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?> ><label for="repeat20">In 2 months</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio" id="repeat21" class="remind_poup_data" value="4month" <?php if(in_array('4month', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?>><label for="repeat21">In 4 months</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio"   id="repeat22" class="remind_poup_data" value="6month" <?php if(in_array('6month', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?> ><label for="repeat22">In 6 months</label>
                                                   </div>
                                                   <div class="checkbox-color checkbox-primary">
                                                      <input type="checkbox" name="radio"   id="repeat23" class="remind_poup_data" <?php if(in_array('8month', explode(',', $task_form[0]['reminder_chk_box'] ))){ echo "checked"; } ?> value="8month"><label for="repeat23">In 8 months</label>
                                                   </div>
                                                </div>
                                             </div>
                                             <input type="hidden" name="for_reminder_chk_box" id="for_reminder_chk_box">
                                             <!-- form-group -->
                                             <div class="picker-category exam-picker">
                                                <h3>
                                                   At a specific time <span>(Examples: "Wednesday", "3 days from now")</span>
                                                </h3>
                                                <div class="specific-picker">
                                                   <div class="form-group">
                                                      <label class="col-form-label">Starts:</label>
                                                      <div class="control-start1">
                                                         <div class="input-group date">
                                                            <span class="input-group-addon "><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input type='text' class="form-control required_check" id='specific_time' name="specific_time" placeholder="dd-mm-yyyy"  value="<?php echo $task_form[0]['reminder_specific_time']; ?>" />
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="form-group">
                                                      <label class="col-form-label">Send Time:</label>
                                                      <div class="input-group control-start1" >
                                                         <input type="text" name="specific_timepic" id="timepicker2" class="form-control required_check" value="<?php echo $task_form[0]['reminder_specific_timepic']; ?>">
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-summar">
                                                   <label>Summary:</label>
                                                   <!-- <strong>12 weeks, onWednesday at 7:26: PM</strong> -->
                                                   <div id="summary_val"><?php echo $task_form[0]['reminder_msg']; ?></div>
                                                   <input type="hidden" name="reminder_msg" id="reminder_msg">
                                                </div>
                                             </div>
                                          </div>
                                          <!-- modal-body -->
                                          <div class="modal-footer modal-remind-footer">
                                             <!-- <button type="button" name="button"  class="btn btn-default submitBtn_newremind">Confirm</button> -->
                                             <a href="#" data-dismiss="modal">Close</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- modal close-->
                                 <!-- *************************** -->
                              </div>
                           </div>
                           <!-- for row -->
                           <input type="hidden" name="inc_exc_ids" value='nochanges'>
                     </form>
                     </div>
                  </div>
                  <!-- close -->
               </div>
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<!-- ajax loader -->
<!-- <div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style> -->
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<!-- tinymce editor -->
<!-- <script src="<?php echo base_url();?>assets/js/tinymce.min.js"></script> -->
<!-- tinymce editor -->

<script type="text/javascript">
$( document ).ready(function() {

  $('.tags').tagsinput({
        allowDuplicates: true
      });
 // $("#related_to_services").change(function(){alert($('.tags').val());});
if($("#recurring_end").val() == "on")
{
 var date = $("#recurring_end_date").val();
 if(date!='0000-00-00')
 {
    date = date.split('-');
    var new_date = new Date(date[0],date[1]-1,date[2]);
    new_date = new_date.toDateString().split(' ');
    $("#modalendDate").attr('data-day',new_date[2]);
    $("#modalendDate").attr('data-month',new_date[1]);
    $("#modalendDate").attr('data-year',new_date[3]);
 }
}

         
        } );</script>
}
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
   <h1>Warning!!</h1>
   <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
   <div class="iew-container">
      <ul class="iew-download">
         <li>
            <a href="http://www.google.com/chrome/">
               <img src="assets/images/browser/chrome.png" alt="Chrome">
               <div>Chrome</div>
            </a>
         </li>
         <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
               <img src="assets/images/browser/firefox.png" alt="Firefox">
               <div>Firefox</div>
            </a>
         </li>
         <li>
            <a href="http://www.opera.com">
               <img src="assets/images/browser/opera.png" alt="Opera">
               <div>Opera</div>
            </a>
         </li>
         <li>
            <a href="https://www.apple.com/safari/">
               <img src="assets/images/browser/safari.png" alt="Safari">
               <div>Safari</div>
            </a>
         </li>
         <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
               <img src="assets/images/browser/ie.png" alt="">
               <div>IE (9 & above)</div>
            </a>
         </li>
      </ul>
   </div>
   <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->

<!-- 01-10-2018 for popup included confirmation -->
   <div class="modal fade step-pagination1" id="for_included_options" role="dialog">
      <div class="modal-dialog modal-dialog-service">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header modal-header-service">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title include_exclude_title">Included Options</h4>
            </div>
            <div class="modal-body modal-body-service">
            <div class="include_exclude_content"></div>
            </div>
            <div class="modal-footer modal-footer-service">
            <input type="hidden" id="includeorexclude" name="includeorexclude" class="includeorexclude" >
               <button style="display: none;" type="button" name="button"  class="btn btn-default include_confirm" data-id="<?php echo $task_form[0]['id']; ?>" data-services="<?php echo $related_to_service; ?>" >Confirm</button>
               <a href="#" data-dismiss="modal">Close</a>
            </div>
         </div>
      </div>
   </div>
<!-- end of 01-10-2018 -->
<!-- edit step details -->
 <div class="modal fade" id="EditStepDetails" role="dialog">
    <div class="modal-dialog modal-edit-step-dialog">
   <!--  <form name="form1" action="<?php //echo base_url(); ?>service/service_add" method="post"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header modal-edit-step-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Step Title</h4>
        </div>
        <div class="modal-body modal-edit-step-body">
        <label>Step Title</label>
        <input type="hidden" name="edit_id" id="edit_id" value="">
         <input type="text" name="step_content" id="step_content" value="">
         <input type="hidden" name="step_type" id="step_type" value="">
        </div>
        <div class="modal-footer modal-edit-step-footer">
         <button type="button" id="steps_details_edit">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
     <!--  </form> -->
    </div>
  </div> 

<!-- add new steps -->
  <div class="modal fade" id="addContents" role="dialog">
    <div class="modal-dialog modal-step-dialog">
   <!--  <form name="form1" action="<?php //echo base_url(); ?>service/service_add" method="post"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header modal-step-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Steps</h4>
        </div>
        <div class="modal-body modal-step-body">
        <label>Steps</label>
        <input type="hidden" name="stepid" id="stepid" value="">
        <input type="hidden" name="step_typeid" id="step_typeid" value="">
         <input type="text" name="steps_content" id="steps_content" value="">
        </div>
        <div class="modal-footer modal-step-footer">
         <button type="button" id="add_steps_section">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
     <!--  </form> -->
    </div>
  </div>

    <div class="modal fade" id="deleteSteps" role="dialog">
    <div class="modal-dialog modal-del-step-dialog">
   <!--  <form name="form1" action="<?php //echo base_url(); ?>service/steps_delete" method="post"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header modal-del-step-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Steps</h4>
        </div>
        <div class="modal-body modal-del-step-body">
        Do you want to delete?
        <!-- <label>Service Name</label>-->
         <input type="hidden" name="id" id="steps_id" value=""> 
         <input type="hidden" name="steptype_id" id="steptype_id" value=""> 
        </div>
        <div class="modal-footer modal-del-step-footer">
         <button type="button" name="button" id="steps_delete">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">no</button>
        </div>
      </div>
     <!--  </form> -->
    </div>
  </div>



    <div class="modal fade" id="delete_Steps" role="dialog">
    <div class="modal-dialog modal-del-step-dialog">
   <!--  <form name="form1" action="<?php //echo base_url(); ?>service/steps_delete" method="post"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header modal-del-step-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Steps</h4>
        </div>
        <div class="modal-body modal-del-step-body">
        Do you want to delete?
        <!-- <label>Service Name</label>-->
         <input type="hidden" name="id" id="step_details_id" value=""> 
         <input type="hidden" name="id" id="step_details_type" value=""> 
        </div>
        <div class="modal-footer modal-del-step-footer">
         <button type="button" name="button" id="steps_details_delete">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">no</button>
        </div>
      </div>
     <!--  </form> -->
    </div>
  </div>



<div class="modal fade" id="addStep" role="dialog">
    <div class="modal-dialog modal-step-dialog">
   <!--  <form name="form1" action="<?php //echo base_url(); ?>service/service_add" method="post"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header modal-step-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Step Title</h4>
        </div>
        <div class="modal-body modal-step-body">
        <label>Step Title</label>
        <input type="hidden" name="service_id" id="services_id" value="">
         <input type="text" name="service_title" id="service_title" value="">
        </div>
        <div class="modal-footer modal-step-footer">
         <button type="button" id="steps_add">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
     <!--  </form> -->
    </div>
  </div>


  <div class="modal fade" id="EditStep" role="dialog">
    <div class="modal-dialog modal-edit-step-dialog">
   <!--  <form name="form1" action="<?php //echo base_url(); ?>service/service_add" method="post"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header modal-edit-step-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Step Title</h4>
        </div>
        <div class="modal-body modal-edit-step-body">
        <label>Step Title</label>
        <input type="hidden" name="service_id" id="step_id" value="">
        <input type="hidden" name="step_type_id" id="step_type_id" value="">
         <input type="text" name="service_title" id="title" value="" required="">
        </div>
        <div class="modal-footer modal-edit-step-footer">
         <button type="button" id="steps_edit">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
     <!--  </form> -->
    </div>
  </div> 
<!--  03-10-2018 -->

<!-- end of 03-10-2018 -->


<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="<?php echo base_url(); ?>assets/js/timepicki.js"></script>

<script>
    /*timepic trigger for on change function */
      $('#timepicker1').timepicki( {start_time: ["10", "00", "AM"],step_size_minutes:5,on_change:function(elm){$(elm).trigger('change'); } } );

      $('#timepicker2').timepicki( {step_size_minutes:5,on_change:function(elm){$(elm).trigger('change'); } } );
      /**/
   var Random = Mock.Random;
   var json1 = Mock.mock({
     "data|10-50": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled|1-2": true,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
   
   $('.dropdown-mul-1').dropdown({
     data: json1.data,
     limitCount: 40,
     multipleMode: 'label',
     choice: function () {
       // console.log(arguments,this);
     }
   });
   
   var json2 = Mock.mock({
     "data|10000-10000": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled": false,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
   
   $('.dropdown-mul-2').dropdown({
     limitCount: 5,
     searchable: false
   });
   
   $('.dropdown-sin-1').dropdown({
     readOnly: true,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   
   $('.dropdown-sin-2').dropdown({
    // limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
     $('.dropdown-sin-3').dropdown({
    // limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
</script>
<script type="text/javascript">
   $('.dropdown-sin-4').dropdown({
     // limitCount: 5,
      input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
    });
      $('.dropdown-sin-5').dropdown({
     // limitCount: 5,
      input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
    });
</script>
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">
   //  $('#timepicker1').timepicker();
</script> 
<script>
   $(document).ready(function(){
   /*$('input[id$="timepicker1"]').inputmask({
    mask: "h:s t\\m",
    placeholder: "hh:mm xm",
    alias: "datetime",
    hourFormat: "12"
   });*/
   $('input[name$="text"]').inputmask({
    mask: "h:s t\\m",
    placeholder: "hh:mm xm",
    alias: "datetime",
    hourFormat: "12"
   });
   
     $("#txtFromDate").datepicker({
         dateFormat: 'dd-mm-yy',
         numberOfMonths: 1,
          minDate:0,
          changeMonth: true,
        changeYear: true,
          onSelect: function(selected) {
           $("#txtToDate").datepicker("option","minDate", selected)
         }
     });
     $("#txtToDate").datepicker({ 
         dateFormat: 'dd-mm-yy',
         numberOfMonths: 1,
          minDate:0,
          changeMonth: true,
        changeYear: true,
         onSelect: function(selected) {
            $("#txtFromDate").datepicker("option","maxDate", selected)
         }
     });  
   
   });
   
   
   
   

     
         //var date = $('#modalFromDate').datepicker({ dateFormat: 'dd-mm-yy' }).val();
         /*$('#modalendDate').datepicker({ dateFormat: 'dd-mm-yy',minDate:0, changeMonth: true,
        changeYear: true, }).val();*/
        
        
         // Multiple swithces
        /* var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
         elem.forEach(function(html) {
             var switchery = new Switchery(html, {
                 color: '#1abc9c',
                 jackColor: '#fff',
                 size: 'small'
             });
         });
   
         $('#accordion_close').on('click', function(){
                 $('#accordion').slideToggle(300);
                 $(this).toggleClass('accordion_down');
         });*/

   $("#repeats").change(function(){

    var vals=$(this).val();
    /** 23-06-2018 **/
    var recurring_repeats=$("#recurring_repeats").val();
   if(recurring_repeats!=vals)
   {
    $("#recurring_repeats").val('changed');
   }

    $('#weekly').find("input[type='checkbox']").each(function(){
        $(this).prop("checked",false);
      });

       var vals=$(this).val();

       if( vals == 'week' )
       {
        $('#weekly').show();
       }
       else 
       {
        $('#weekly').hide();
       }




   /** end of 23-06-2018 **/
   /*if(vals=='week'){
      $('#weekly').html('<div class="form-group  days-list floating_set"><label class="label-column1 col-form-label">On</label><div class="label-column2"><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox dayss"  name="days[]" value="Sunday" id="day-check01"><label for="day-check01" class="border-checkbox-label">Sun</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Monday" id="day-check02"><label for="day-check02" class="border-checkbox-label">Mon</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Tuesday" id="day-check03"><label for="day-check03" class="border-checkbox-label">Tue</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Wednesday" id="day-check04"><label for="day-check04" class="border-checkbox-label">Wed</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Thursday" id="day-check05"><label for="day-check05" class="border-checkbox-label">Thur</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Friday" id="day-check06"><label for="day-check06" class="border-checkbox-label">Fri</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox dayss" name="days[]" value="Saturday" id="day-check07"><label for="day-check07" class="border-checkbox-label">Sat</label></div></div></div>');
   
   }else
   {
    $('#weekly').html('');
   }*/
   
   });
   
     
</script>
<script src="<?php echo base_url()?>assets/js/task/recurring_reminder.js"></script>
<script type="text/javascript">
   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-36251023-1']);
   _gaq.push(['_setDomainName', 'jqueryscript.net']);
   _gaq.push(['_trackPageview']);
   
   (function() {
     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
   })();
   
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){    
     $.validator.setDefaults({
         ignore: []
     });
   });
    $(document).ready(function(){

  /* tinymce editor*/
  tinymce_config['images_upload_url'] = "<?php echo base_url();?>Firm/uplode_mail_header_image";
  tinymce_config['selector']          = '#description';

  tinymce.init( tinymce_config );
  /* tinymce editor*/


    $("#update_new_task").validate({
      
          // ignore: false,
           ignore: ".ignore",
    
           /*onfocusout: function(element) {
            if ( !this.checkable(element)) {
                this.element(element);
            }
        },*/
       // errorClass: "error text-warning",
        //validClass: "success text-success",
        /*highlight: function (element, errorClass) {
            //alert('em');
           // $(element).fadeOut(function () {
               // $(element).fadeIn();
            //});
        },*/
        errorPlacement: function(error, element) {
          alert('inside errorPlacement'+element.attr('class'));

                 if (element.attr("name") == "company_name" )
                     error.insertAfter(".dropdown-sin-4");
                      else if (element.attr("name") == "worker[]" )
                     error.insertAfter(".dropdown-sin-2");
                 /*  else if (element.attr("name") == "manager[]" )
                     error.insertAfter(".dropdown-sin-3"); */
                 
                 else
                     error.insertAfter(element);
             },
                            rules: {
                            task_name:{required:true},
                            subject: {required: true},
                            company_name: {required: true},
                            task_status:{required:true},
                            startdate:{required:true},
                            enddate:{required:true},
                              "assignees[]": "required",  
                          /*   "manager[]":"required", */
                            },
                            errorElement: "span" , 
                            errorClass: "field-error",                             
                             messages: {
                             task_name:"Required Field",
                              subject: "Required field",
                              company_name: "Required field",
                            task_status:"Required Field",
                            startdate:"Required Field",
                            enddate:"Required Field",
                               "assignees[]": "Required Field",
                          /*   "manager[]": "Required Field", */
                             },
                             
                            submitHandler: function(form) {

                               //$("input[name=tag]").val( $(".tags").val() ); alert($(".tags").val());return;

                                  var new_array=[];
                                 var Assignees=[];
                             $('input[class="remind_poup_data"]:checked').each(function() {
                                  new_array.push(this.value);
                               });
                              var new_arry1= [];
                              var elm_present = 0;
                             $('.include_exclude_content').find("input[name='inc_exc_steps[]']").each(function(){
                              elm_present = 1;
                               
                              if($(this).is(':checked'))
                              {
                                new_arry1.push( $(this).val() );
                              }

                             });                             
                             if(elm_present) $("input[name='inc_exc_ids']").val( new_arry1.join(',') );
                             
                            

                             var for_reminder_chk = new_array.join(',');
                             $('#for_reminder_chk_box').val(for_reminder_chk);


                             $('.comboTreeItemTitle input[type="checkbox"]:checked').each(function(){
                               var id = $(this).closest('.comboTreeItemTitle').attr('data-id');
                               var id = id.split('_');
                              Assignees.push( id[0] );
                           });
                         //  data['assignee'] = Assignees


                            var assign_role = Assignees.join(',');
                            $('#assign_role').val(assign_role);
                           var description_value = tinyMCE.get('description').getContent();
                            

   
                                var formData = new FormData($("#update_new_task")[0]);

                                formData.append("description_value", description_value);
                                
    
                                $(".LoadingImage").show();
    
                                $.ajax({
                                    url: '<?php echo base_url();?>user/update_new_task/',
                                    dataType : 'json',
                                    type : 'POST',
                                    data : formData,
                                    contentType : false,
                                    processData : false,
                                    success: function(data) {
                                        
                                        if(data == 1){
                                           
                                           // $('#new_user')[0].reset();
                                            $(".LoadingImage").hide();
                                            $('.popup_info_msg').show();
                                            $('.popup_info_msg .position-alert1').html("Task Update successfully..");
                                            var Taskid = $("input[name='task_id']").val().trim();
                                            setTimeout(function(){window.location.href="<?php echo base_url(); ?>user/task_details/"+Taskid;},1000);
                                            
                                            
    
                                        }
                                        else{
                                           // alert('failed');
                                           $(".LoadingImage").hide();
                                            $('.alert-danger').show();
                                            $('.alert-success').hide();
                                        }
                                    
                                    },
                                    error: function() { $('.alert-danger').show();
                                            $('.alert-success').hide();}
                                });
    
                                return false;
                            } ,
                             invalidHandler: function(e, validator) {
               if(validator.errorList.length)
            $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
    
            }
                             
                        });
    
    
    });
</script>
<script type="text/javascript">
   function uploadss(){
   $('#inputfile').click();
   }
</script>
<script>  
   $(document).ready(function(){  
        $('#search_company').keyup(function(){  
         // alert('text');
             var query = $(this).val();  
             if(query != '')  
             {  
                  $.ajax({  
                       url:"<?php echo base_url();?>user/search_companies/",  
                       method:"POST",  
                       data:{query:query},  
                       success:function(data)  
                       {  
                            $('#searchresult').fadeIn();  
                            $('#searchresult').html(data);  
                       }
                  });  
             }
             if(query=='')
             {
               $('#searchresult').fadeOut();
             }  
        });  
        $(document).on('click', 'li', function(){  
             $('#search_company').val($(this).text()); 
             $('#user_id').val($(this).data('id')); 
             $('#searchresult').fadeOut();  
        });  
   });  
   
   

   
   
 
   
   $(document).on('click','.remove_file',function(){
    $('#image_name').val('');
    //$('.extra_view_40').hide();
      var id=$(this).attr('data-id');
    $('.for_img_'+id).remove();
   });
   
   function readURL(input) {
    // if (input.files && input.files[0]) {
    //     var reader = new FileReader();
   
    //     reader.onload = function (e) {
    //    //   alert(input.files[0].name);
    //    $('#preview_image_name').html(input.files[0].name);
    //       $('#image_name').val('');
    // $('.extra_view_40').hide();
    //         $('#preview_image')
    //             .attr('src', e.target.result)
    //             .width(225)
    //             .height(225);
    //     };
   
    //     reader.readAsDataURL(input.files[0]);
    // }
    var zz=input.files.length;
      var fruits = [];
      var fruits_new = [];
   var i=0
   if(zz>0){
      for(i=0;i<zz;i++){
     // alert(i);
    if (input.files && input.files[i]) {
      // $('#sample').append('<input type="file" name="attach_file[]" id="sample" value="'+input+'">');
        var reader = new FileReader();
    fruits.push(input.files[i].name);
        fruits_new.push(input.files[i]);
   
    }
    }
   }
   var join=fruits.join(',');
    $('#preview_image_name').html(join);
   }
   
   
</script> 
<script type="text/javascript">
   $(document).ready(function(){
   
     
   
     // $(document).on( 'click', 'a#mobile-collapse', function() {
   
       $("a#mobile-collapse").click(function () {
   
       $('#pcoded.pcoded.iscollapsed').toggleClass('addlay');
   
     });
   
     // $(document).on( 'click', '.addlay .nav-left .data1 li:not(.addlay .nav-left .data1 li .column-setting0)', function() {
   
       $(".nav-left .data1 li.comclass a.com12").click(function () {
   
         // alert('hi');
   
   
       $(this).parent('.nav-left .data1 li.comclass').find('ul.dropdown-menu1').slideToggle();
       $(this).parent('.nav-left .data1 li.comclass').siblings().find('ul.dropdown-menu1:visible').slideUp();
       
   
     });
   
       //  $('.column-setting0').click(function () {
   
       //   alert('hi');
   
       // });
     $(document).on( 'click', 'li.column-setting0', function() {
   
   
       $(this).children('ul.dropdown-menu.hide-dropdown01').slideToggle();
   
     });
   });
      $(".dropdown-sin-4").find('.dropdown-main > ul > li').click(function(){
         
       var custom=$(this).attr('data-value');
       var customclass=$(this).attr('class');
       var task_name='CLIENT';

      $('#user_id').val(custom);
      //for un select
      $(".dropdown-sin-3 .dropdown-main ul li.dropdown-chose").trigger("click");
      $(".dropdown-sin-2 .dropdown-main ul li.dropdown-chose").trigger("click");
      $("#worker option").prop("selected",false);
      $("#manager option").prop("selected",false);
      //for un select

      var id=custom;
      if(id!='')
      {
       // alert(id);

        $.ajax({
          url : "<?php echo base_url()?>user/get_company_assign",
          type : "POST",
          data : {"id":id,"task_name":task_name},
          dataType : "json",        
          beforeSend : function(){
            $(".LoadingImage").show();
          },
          success: function(data){
            $(".LoadingImage").hide();  

            var arr1 = data.asssign_group;

              var unique = arr1.filter(function(itm, i, arr1) {
              return i == arr1.indexOf(itm);
          });
               // var arr=<?php echo json_encode($asssign_group); ?>;
            
            console.log(unique);

    $('.comboTreeItemTitle').each(function(){
      $(this).find("input:checked").trigger('click');
               var id1 = $(this).attr('data-id');
                var id = id1.split('_');
             if(jQuery.inArray(id[0], unique) !== -1)
               {
                  $(this).find("input").trigger('click');
                     
              }
                    
               });
         


            // for(i=0;i < manager.length;i++)
            // {
            //   $("#manager option[value="+manager[i]+"]").attr("selected",true);
            //   //$(".dropdown-sin-3 .dropdown-main ul li[data-value="+manager[i]+"]").addClass("dropdown-chose");
            //   $(".dropdown-sin-3 .dropdown-main ul li[data-value="+manager[i]+"]").trigger("click");
            //   //alert("manager-sin"+$(".dropdown-sin-3 .dropdown-main ul li[data-value="+manager[i]+"]").attr("class"));
            // }
            
            // var assign = data.assignee;

            // for(i=0;i < assign.length;i++)
            // {
            //   $("#worker option[value="+assign[i]+"]").attr("selected",true);
            //   //$(".dropdown-sin-2 .dropdown-main ul li[data-value="+assign[i]+"]").addClass("dropdown-chose");
            //   $(".dropdown-sin-2 .dropdown-main ul li[data-value="+assign[i]+"]").trigger("click");
            //   //alert("assign-sin"+$(".dropdown-sin-2 .dropdown-main ul li[data-value="+assign[i]+"]").attr("class"));
            // }

  
          }
      });
      
      }  
         });

/** 01-10-2018 **/

$(document).on('click','.services_options,.extra_steps_options',function(){

 var ex_checked = {};

   var services=$(this).attr('data-services'); //action id;
   var action=$(this).attr('data-action');
   var task_id=$(this).attr('data-id');

   $('.includeorexclude').val(action);
   
  

$('.include_exclude_content').find("input[name='inc_exc_steps[]']").each(function(){  
    ex_checked[$(this).val()] = $(this).is(":checked");  
});



  /* if(action=='included_options')
   {
   	$('.include_exclude_title').html('Included Option');
    $('.include_confirm').css('display','');
   }
   else if(action=='excluded_options')
   {
   	$('.include_exclude_title').html('Excluded Options');
    $('.include_confirm').css('display','');
   }
   else
   {
    $('.include_exclude_title').html('Add New Steps');
    $('.include_confirm').css('display','none');
   }*/


   $.ajax({
     url: '<?php echo base_url(); ?>Tasksummary/get_services_options',
     type:'post',
     data:{'services': services,'action':action,'task_id':task_id},
      beforeSend: function() {
                        $(".LoadingImage").show();
                      },    
     success:function(response){
            
            //alert('data');
            
            //$('#for_included_options').modal('show',{backdrop: 'static', keyboard: false});
            
           // console.log(response);
         
            $('.include_exclude_content').html(response);

$.each(ex_checked,function(i,v)
{
  
  $('.include_exclude_content').find("input[value='"+i+"']").prop("checked",v);
});


 $('#for_included_options').modal({
                        backdrop: 'static',
                        keyboard: true, 
                        show: true
                }); 



            $(".LoadingImage").hide();
     },
   });

});
/** 03-10-2018 for added extra steps **/


/** end of 03-10-2018 **/

  $(document).ready(function(){ 
         $(document).on('click','i.fa.fa-angle-double-down',function() {
     //   alert($(this).attr('class')+"parent length"+$(this).parent('.inside-step').find('.dropdown_con').length);
       // alert($(this).parent('.inside-step').find('.dropdown_con').attr('class'));
         $(this).parent('.inside-step').find('.dropdown_con').slideToggle();
    });
    //$("input[name$='servicestatus']").click(function() {
      $(document).on('click',"input[name$='servicestatus']",function(){
          var test = $(this).val();
             if(test=='1'){
            $(".default").show();
            $(".custom").hide();
          }else{
            $(".custom").show();
              $(".default").hide();
          }
      }); 
    });

 

/** end of 01-10-2018 **/

/** 03-10-2018 for add extra steps form **/

         $("i.fa.fa-angle-double-down").click(function() {
                      $(this).parent('.inside-step').find('.dropdown_con').slideToggle();
                      });
                      //$(".add_step").click(function(){ 
                        $(document).on('click','.add_step',function(){
                          $('#service_title').css('border-color','#cccccc');
                       $("#addStep").find('#service_title').val('');                       
                        $("#addStep").find('#services_id').val($(this).attr('id'));
                        $("#addStep").modal('show');
                      });
                      //$(".title_delete").click(function(){
                        $(document).on('click','.title_delete',function(){
                       var id=$(this).attr('id');
                       var type=$(this).attr('data-type');
                       $("#deleteSteps").find('#steps_id').val($(this).attr('id')); 
                      $("#deleteSteps").find('#steptype_id').val(type); 
                       $("#deleteSteps").modal('show');           
                      });
                      //$(".title_edit").click(function(){
                        $(document).on('click','.title_edit',function(){

                          $('#title').css('border-color','#cccccc');
                         var id=$(this).attr('id');  
                         var type=$(this).attr('data-type');                      
                           $.ajax({
                              url: '<?php echo base_url(); ?>Service/steps_details',
                              type : 'POST',
                              data : {'id':id },                    
                              beforeSend: function() {
                              $(".LoadingImage").show();
                              },
                              success: function(data) {
                                 $(".LoadingImage").hide();
                                var json = JSON.parse(data);             
                                  title=json['title'];
                                  id=json['id'];
                                  console.log(id);
                                   console.log(title);
                                  $("#EditStep").find('#step_id').val(id);
                                   $("#EditStep").find('#step_type_id').val(type);
                                  $("#EditStep").find('#title').val(title);
                                  $("#EditStep").modal('show');
                              }
                        });                      
                      });

                       //$(".steps_adds").click(function(){
                        $(document).on('click','.steps_adds',function(){
                         // alert($(this).attr('id'));
                         $("#addContents").find('#steps_content').val('');
                          $("#addContents").find('#stepid').val($(this).attr('id'));
                          $("#addContents").find('#step_typeid').val($(this).data('val'));
                          $("#addContents").modal('show');
                      });
                         //$(".step_section_delete").click(function(){
                          $(document).on('click','.step_section_delete',function(){
                        var id=$(this).attr('id');
                        var type=$(this).attr('data-type');
                        $("#delete_Steps").find('#step_details_id').val(id);
                         $("#delete_Steps").find('#step_details_type').val(type);
                           $("#delete_Steps").modal('show');
                      });

                           //$(".step_section_edit").click(function(){
                            $(document).on('click','.step_section_edit',function(){
                   var id=$(this).attr('id');
                   var type=$(this).attr('data-type');
                      $.ajax({
                        url: '<?php echo base_url(); ?>Service/steps_secion_details',
                        type : 'POST',
                        data : {'id':id},                    
                        beforeSend: function() {
                        $(".LoadingImage").show();
                        },
                        success: function(data) {
                           $(".LoadingImage").hide();
                          var json = JSON.parse(data);             
                            title=json['step_name'];
                            id=json['id'];  
                            $("#EditStepDetails").find('#edit_id').val(id);
                            $("#EditStepDetails").find('#step_content').val(title);
                            $("#EditStepDetails").find('#step_type').val(type);
                            $("#EditStepDetails").modal('show');
                        }
                  });
                });

$(document).ready(function(){
 function edit_service(edit){
      var id=$(edit).attr('id');  
       $.ajax({
                url: '<?php echo base_url(); ?>Service/service_details',
                type : 'POST',
                data : {'id':id },                    
                  beforeSend: function() {
                    $(".LoadingImage").show();
                  },
                  success: function(data) {
                      $(".LoadingImage").hide();
                      $(".services_edit").html('');
                      $(".services_edit").append(data);
                      /** 03-10-2018 **/   $('#for_included_options').modal('hide');
               $('.modal-backdrop.show').hide(); /** end 03-10-2018 **/
             
                    
                  }
          });
    }
 
      function delete_service(del){
      var id=$(del).attr('id');
      $("#deleteService").find('#service_id').val(id);
      $("#deleteService").modal('show');   
     }


     //$("#steps_add").click(function(){
      $(document).on('click','#steps_add',function(){
      var id=$("#services_id").val();
      id1=id.split("_");
      if(id1.hasOwnProperty(1) && id1[0]=='wor')
      {
        id=id1[1];
        var is_workflow=1;
      }
      else
      {
        if(id1.hasOwnProperty(1))
        {
         id=id1[1];
        }
        else
        {
           id=id1[0];
        }
        var is_workflow=0;
      }

      var title=$("#service_title").val();
      if(title!=''){
            $.ajax({
                url: '<?php echo base_url(); ?>Service/add_title',
                type : 'POST',
                data : {'id':id,'title':title ,'is_workflow':is_workflow},                    
                  beforeSend: function() {
                    $(".LoadingImage").show();
                  },
                  success: function(data) {
                    $(".LoadingImage").hide();

                    $(".services_edit").html('');
                     $(".services_edit").append(data);
                     /** 03-10-2018 **/   $('#for_included_options').modal('hide');
               $('.modal-backdrop.show').hide(); /** end 03-10-2018 **/
                      $("#addStep").modal('hide');
             
                    // console.log(data);
                    //location.reload();
                  }
          });
        }
        else
        {
          $('#service_title').css('border-color','red');
        }
     });
 
     $(document).on('keyup','#service_title,#title,#steps_content',function(){
    $(this).css('border-color','#cccccc');
   });

 //$("#steps_delete").click(function(){
  $(document).on('click','#steps_delete',function(){
var id=$("#deleteSteps").find('#steps_id').val();
var is_workflow=$("#deleteSteps").find('#steptype_id').val();
  $.ajax({
                url: '<?php echo base_url(); ?>/Service/edit_delete_steps',
                type : 'POST',
               data : {'id':id ,'data': {'status':1} ,'is_workflow':is_workflow},                 
                  beforeSend: function() {
                    $(".LoadingImage").show();
                  },
                  success: function(data) {
                    $(".LoadingImage").hide();
                     $(".services_edit").html('');
                     $(".services_edit").append(data);
                     /** 03-10-2018 **/   $('#for_included_options').modal('hide');
               $('.modal-backdrop.show').hide(); /** end 03-10-2018 **/
                    $("#deleteSteps").modal('hide');
                         
                    }
                });
 });

 //$("#steps_edit").click(function(){
  $(document).on('click','#steps_edit',function(){
  var step_id=$("#step_id").val();
  var is_workflow=$("#step_type_id").val();
  var title={ 'title' : $("#title").val() };
  if($("#title").val()!=''){
     $.ajax({
                url: '<?php echo base_url(); ?>/Service/edit_delete_steps',
                type : 'POST',
                data : {'id':step_id,'data':title,'is_workflow':is_workflow},              
                  beforeSend: function() {
                    $(".LoadingImage").show();
                  },
                  success: function(data) {
                       $(".LoadingImage").hide();
                       $(".services_edit").html('');
                       $(".services_edit").append(data);
                       /** 03-10-2018 **/   $('#for_included_options').modal('hide');
               $('.modal-backdrop.show').hide(); /** end 03-10-2018 **/
                      $("#EditStep").modal('hide');

         },

               });
   }
   else
   {
    $('#title').css('border-color','red');
   }
       });

//$("#add_steps_section").click(function(){
  $(document).on('click','#add_steps_section',function(){
  var step_id=$("#stepid").val();
  var type_id=$("#step_typeid").val();
  var steps_title=$("#steps_content").val();
  var is_workflow=0;
  if(type_id==1)
  {
   is_workflow=1; 
  }
  if(steps_title!=''){
     $.ajax({
          url: '<?php echo base_url(); ?>Service/steps_secion_add',
          type : 'POST',
          data : {'id':step_id,'step_content':steps_title,'is_workflow':is_workflow },                    
          beforeSend: function() {
          $(".LoadingImage").show();
          },
          success: function(data) {
             $(".LoadingImage").hide();
              $("#addContents").modal('hide');
                $(".services_edit").html('');
                $(".services_edit").append(data);  
                /** 03-10-2018 **/   $('#for_included_options').modal('hide');
               $('.modal-backdrop.show').hide(); /** end 03-10-2018 **/                 
         
          },
    });
   }else
   {
    $('#steps_content').css('border-color','red');
   }
});

//$("#steps_details_delete").click(function(){
  $(document).on('click','#steps_details_delete',function(){
 var steps_details_id=$("#step_details_id").val();
  var is_workflow=$("#step_details_type").val();
 var data = {'status' : '1'};
 //alert(steps_details_id);
  $.ajax({
                url: '<?php echo base_url(); ?>/Service/steps_section_details_update_delete',
                type : 'POST',
                data : {'id':steps_details_id ,'data':data , 'is_workflow':is_workflow},                
                  beforeSend: function() {
                    $(".LoadingImage").show();
                  },
                  success: function(data) {
                   $(".LoadingImage").hide();
                   $("#delete_Steps").modal('hide');
                  $(".services_edit").html('');
                  $(".services_edit").append(data);     
                  /** 03-10-2018 **/   $('#for_included_options').modal('hide');
               $('.modal-backdrop.show').hide(); /** end 03-10-2018 **/              
                  $("i.fa.fa-angle-double-down").click(function() {
                  $(this).parent('.inside-step').find('.dropdown_con').slideToggle();
                  });
                
                  }
                });
});

$("#steps_details_edit").click(function(){
var id=$('#edit_id').val();
var step_content=$('#step_content').val();
var is_workflow=$('#step_type').val();
if(step_content!=''){
$.ajax({
      url: '<?php echo base_url(); ?>Service/steps_section_details_update_delete',
      type : 'POST',
     data : {'id':id,'data':{'step_content':step_content},'is_workflow':is_workflow },           
      beforeSend: function() {
      $(".LoadingImage").show();
      },
      success: function(data) {
         $(".LoadingImage").hide();
         $("#EditStepDetails").modal('hide');

              $(".services_edit").html('');
                  $(".services_edit").append(data);     
                  /** 03-10-2018 **/   $('#for_included_options').modal('hide');
               $('.modal-backdrop.show').hide(); /** end 03-10-2018 **/              
                
      }
});
}
else
{
  $('#step_content').css('border-color','red');
}

});
});


$("#related_to_services").on("change",function()
{       //alert(typeof($(this).val()));
        var it_val=$(this).val();
        console.log(it_val);
        if(it_val !='')
        {
         $.ajax({
            type: "post",
            url: "<?php echo base_url('Service/get_service_estimated_time') ?>",
            data: {service_id : it_val},
            success: function (response) {
               $('#estimated_time').val(response);
            }
         });   

          $('#inc_exc_options').attr('data-services',it_val); 
          $('#added_extra_steps').attr('data-services',it_val);
          $('.include_confirm').attr('data-services',it_val);
          $('#inc_exc_options').trigger('click');
          $('#inc_exc_options ,#added_extra_steps').show();
        }
        else
        {
           $('#inc_exc_options, #added_extra_steps').hide();
        }

});

 $("#close_info_msg").click(function(){$(".popup_info_msg").hide(); });

</script>

<script>
	$(document).ready(function(){
		$(".clr-check-client").keyup(function(){
			var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-client-outline");
			}else{
				$(this).removeClass("clr-check-client-outline");
			}
		});
    $(".clr-check-client").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-client-outline");
      }  
    })

		$(".clr-check-select").change(function(){
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		});
    $(".clr-check-select").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-select-client-outline");
      }  
    })


	});

</script>

<script src="<?php echo base_url();?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/icontains.js"></script>
<script type="text/javascript">
         var tree_select = <?php echo json_encode( GetAssignees_SelectBox() ); ?>;
         $('.tree_select').comboTree({
            source : tree_select,
            isMultiple: true
         });
           var arr=<?php echo json_encode($asssign_group); ?>;
           
               $('.comboTreeItemTitle').click(function(){
            var id = $(this).attr('data-id');
            var id = id.split('_');
               $('.comboTreeItemTitle').each(function(){
               var id1 = $(this).attr('data-id');
               var id1 = id1.split('_');
               //console.log(id[1]+"=="+id1[1]);
                  if(id[1]==id1[1])
                  {
                     $(this).toggleClass('disabled');
                  }
               });
                $(this).removeClass('disabled');
         });



    $('.comboTreeItemTitle').each(function(){

      var id1 = $(this).attr('data-id');
      var id = id1.split('_');
      if(jQuery.inArray(id[0], arr) !== -1)
      {
        $(this).find("input").trigger('click');
      }
                    
      });
         

     
         </script>
</body>
</html>