<?php
$tsk_id     = $task_form[0]['id']; 
$tdata      = $this->Common_mdl->taskTimerDetails($tsk_id);
$utask      = $tdata['utask'];
$curr_timer = $tdata['curr_timer'];
$tot_timer  = $tdata['tot_timer'];
?>

<?php $this->load->view('includes/new_header'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/switchery/css/switchery.min.css">
<style>
    .task-details-description a {
        color: blue;
    }
</style>
<?php
$tasks_status        = $this->Common_mdl->GetAllWithWhere('task_status', 'firm_id', '0');
$step_name           = '';
$for_main_task_timer = array();
$for_user_edit_per   = array();   ?>

<link href="<?php echo base_url(); ?>assets/css/timeline-style.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/tree_select/style.css?ver=2">
<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/counter/counter-style.css"> -->
<?php
$sql_val = $this->db->query("SELECT * FROM firm_assignees where module_id = " . $task_form[0]['id'] . " AND module_name='TASK'")->result_array();

foreach ($sql_val as $key1 => $value1) {
    $assign_data[] = $value1['assignees'];
}
$role = $this->Common_mdl->getRole($_SESSION['id']);

function convertToHoursMins($time, $format = '%02d:%02d')
{
    if ($time < 1) {
        return;
    }
    $hours   = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}

/** for timer 02-07-2018 **/
function calculate_test($a, $b)
{
    $difference    = $b - $a;
    $second        = 1;
    $minute        = 60 * $second;
    $hour          = 60 * $minute;
    $ans["hour"]   = floor(($difference) / $hour);
    $ans["minute"] = floor((($difference) % $hour) / $minute);
    $ans["second"] = floor(((($difference) % $hour) % $minute) / $second);
    $test          = $ans["hour"] . ":" . $ans["minute"] . ":" . $ans["second"];
    return $test;
}
function time_to_sec($time)
{
    list($h, $m, $s) = explode(":", $time);
    $seconds  = 0;
    $seconds += (intval($h) * 3600);
    $seconds += (intval($m) * 60);
    $seconds += (intval($s));
    return $seconds;
}
function sec_to_time($sec)
{
    return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}
/** end of 02-07-2018 **/
$get_val            = [];
$new_explode_worker = [];
$remove_result      = [];
$all_task_id        = array($task_form[0]['id']);
$explode_worker     = array();
/* This explode_worker get currently  assigned user in  this task */
$explode_worker     = Get_Module_Assigees('TASK',  $task_form[0]['id']);
$sub_task_id        = array_filter(explode(",", $task_form[0]['sub_task_id']));
$all_task_id        = (!empty($sub_task_id)) ? array_merge($all_task_id, $sub_task_id) : $all_task_id;

if (!empty($all_task_id)) {
    $all_task_id = implode(",", $all_task_id);
    $get_val     = $this->Common_mdl->getall_wherein_records('individual_task_timer', 'task_id', $all_task_id);
    $get_val     = array_filter(array_column($get_val, 'user_id'));
}
$new_explode_worker = $get_val;
$remove_result      = array_diff($new_explode_worker, $explode_worker);
/* This explode_worker who are all assigned user in this task */
$explode_worker     = array_unique(array_merge($explode_worker, $get_val));
$user_details       = $this->Common_mdl->select_record('user', 'id', $userid);
$task_ID            = $task_form[0]['id'];
$revision           = $this->db->query('select count(*) as total from task_revisions where task_id = ' . $task_ID . '')->row_array();
$task_settings      = $this->Common_mdl->GetAllWithWhere('task_setting', 'task_id', $task_form[0]['id']);

if (count($task_settings)) {
    $allow_comments = $task_settings[0]['comment'];
    $task_timer     = $task_settings[0]['task_timer'];
    $attach_file    = $task_settings[0]['attach_file'];
    $edit_task      = $task_settings[0]['user_edit_task'];
} else {
    $allow_comments = 0;
    $task_timer     = 0;
    $attach_file    = 0;
    $edit_task      = 0;
}
$edit_task = $task_form[0]['customize'];
/** end of assign dropdown **/
?>
<style>
    /** 28-06-2018 **/
    .time {
        padding: 8px;
        font-weight: bold;
    }

    .new_play_stop {
        width: 20px;
        height: 20px;
        background: #000;
        color: #fff;
        line-height: initial;
        text-align: center;
        font-size: 15px;
        vertical-align: top;
        border-radius: 50%;
        margin: 7px 0 0 0;
    }

    /** for timer style 28-06-2018 **/
    .task-ul {
        padding: 30px;
    }

    .task-li {
        padding: 20px;
    }

    .timer_class select {
        width: 49%;
        margin: 15px 0 10px 0;
    }

    .card .timer_header span {
        display: inline-block;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/progress-circle.css">

<div class="modal-alertsuccess  alert info-popup" style="display:none;">
    <div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <div class="pop-realted1">
            <div class="position-alert1"> Select User Updated Successfully.
            </div>
        </div>
    </div>
</div>
<div class="modal-alertsuccess  alert alert-success-assignee" style="display:none;">
    <div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <div class="pop-realted1">
            <div class="position-alert1"> Assignee users Updated Successfully.
            </div>
        </div>
    </div>
</div>
<!-- Comment Modal Starts -->
<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-comment">
        <div class="modal-content">
            <div class="modal-header modal-header-comment">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body modal-body-comment">
                <textarea rows="5" type="text" id="add_comments" class="form-control add_comments" placeholder="Add Comments"></textarea>
            </div>
            <div class="modal-footer modal-footer-comment">
                <button type="button" class="btn btn-primary comments_btn taskclsbutton" data-dismiss="modal">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- Comment Modal ends -->
<!-- Reply Modal Starts -->
<div class="modal fade" id="replyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-reply">
        <div class="modal-content">
            <div class="modal-header modal-header-reply">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body modal-body-reply">
                <div id="rd_modal_html"></div>
            </div>
        </div>
    </div>
</div>
<!-- Reply Modal ends -->
<!-- Delete Modal Starts -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-delete">
        <div class="modal-content">
            <div class="modal-header modal-header-delete">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body modal-body-delete">
                <div id="delete_modal_html"></div>
            </div>
            <div class="modal-footer modal-footer-delete">
                <button type="button" class="btn btn-default">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<!-- Delete Modal ends -->
<!-- for task view restriction -->
<!-- end of 03-07-2018 -->
<!-- for timer load 02-07-2018  -->
<?php
foreach ($task_form as $tre_key => $tre_value) {
    $time             = json_decode($tre_value['counttimer']);
    $time_start_date  = $tre_value['time_start_date'];
    $time_end_date    = $tre_value['time_end_date'];
    $hours            = 0;
    $mins             = 0;
    $sec              = 0;
    $pause            = '';
    $timer_condition  = ($_SESSION['user_type'] == 'FU') ? "AND user_id=" . $_SESSION['id'] . "" : "";
    $individual_timer = $this->db->query("SELECT `time_start_pause`,`task_id` from individual_task_timer where task_id=" . $tre_value['id'] . " " . $timer_condition . "")->row_array();
    
    if (is_array($individual_timer)) {
        if (count($individual_timer) > 0) {
            if ($individual_timer['time_start_pause'] != '') {
                $res          = explode(',', $individual_timer['time_start_pause']);                
                $res1         = array_chunk($res, 2);
                $result_value = array();
                $pause        = 'on';

                foreach ($res1 as $rre_key => $rre_value) {
                    $abc = $rre_value;
                    if (count($abc) > 1) {
                        if ($abc[1] != '') {
                            $ret_val = calculate_test($abc[0], $abc[1]);
                            array_push($result_value, $ret_val);
                        } else {
                            $pause   = '';
                            $ret_val = calculate_test($abc[0], time());
                            array_push($result_value, $ret_val);
                        }
                    } else {
                        $pause   = '';
                        $ret_val = calculate_test($abc[0], time());
                        array_push($result_value, $ret_val);
                    }
                }
                $time_tot = 0;
                foreach ($result_value as $re_key => $re_value) {
                    $time_tot += time_to_sec($re_value);
                }
                $hr_min_sec = sec_to_time($time_tot);
                $hr_explode = explode(':', $hr_min_sec);
                $hours      = (int)$hr_explode[0];
                $min        = (int)$hr_explode[1];
                $sec        = (int)$hr_explode[2];
            } else {
                $hours = 0;
                $min   = 0;
                $sec   = 0;
                $pause = 'on';
            }
        } else {
            $hours = 0;
            $min   = 0;
            $sec   = 0;
            $pause = 'on';
        }
    } else {
        $hours = 0;
        $min   = 0;
        $sec   = 0;
        $pause = 'on';
    }

    if ($task_form[0]['sub_task_id'] != '') {
        $res = $task_form[0]['id'] . ',' . $task_form[0]['sub_task_id'];
        $data['task_list'] = $this->db->query("
            SELECT * 
            FROM add_new_task AS tl 
            where tl.id in ($res) 
            and tl.firm_id = " . $_SESSION['firm_id'] . " 
            order by id desc
        ")->result_array();
        $dat_val = $this->Task_creating_model->maintask_timer($data['task_list'], 0, 1);   
        $dat_val = explode(",", $dat_val[0]);
        unset($dat_val[4]);
        $sub_time   = implode(":", $dat_val);
        $hr_explode = explode(':', $sub_time);
        $hours      = (int)$hr_explode[0];
        $min        = (int)$hr_explode[1];
        $sec        = (int)$hr_explode[2];
    } ?>
    <input type="hidden" name="trhours_<?php echo $tre_value['id']; ?>" id="trhours_<?php echo $tre_value['id']; ?>" value='<?php echo $hours; ?>'>
    <input type="hidden" name="trmin_<?php echo $tre_value['id']; ?>" id="trmin_<?php echo $tre_value['id']; ?>" value='<?php echo $min; ?>'>
    <input type="hidden" name="trsec_<?php echo $tre_value['id']; ?>" id="trsec_<?php echo $tre_value['id']; ?>" value='<?php echo $sec; ?>'>
    <input type="hidden" name="trmili_<?php echo $tre_value['id']; ?>" id="trmili_<?php echo $tre_value['id']; ?>" value='0'>
    <input type="hidden" name="trpause_<?php echo $tre_value['id']; ?>" id="trpause_<?php echo $tre_value['id']; ?>" value="<?php echo $pause; ?>"><?php
} ?>
<!-- end of 02-07-2018 -->
<div class="modal-alertsuccess alert alert-success" id="delete_alltask<?php echo $task_form[0]['id']; ?>" style="display:none;">
    <div class="newupdate_alert newupdate-alert-delete"> <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
        <div class="pop-realted1">
            <div class="position-alert1">
                <h4 class="modal-title" style="text-align-last: center">Are you sure you want to delete ?</h4>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="hidden">
                <a class="btn btn-default" href="<?php echo base_url() . 'user/t_delete/' . $task_form[0]['id']; ?>">Yes</a>
                <a class="btn btn-primary" href="#" id="close">No</a>
            </div>
        </div>
    </div>
</div>

<div id="adduser_<?php echo $task_form[0]['id']; ?>" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-assign-task">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-assign-task">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align-last: center">Assign too</h4>
            </div>
            <div class="modal-body modal-body-assign-task">
                <input type="text" class="tree_select" name="assignees[]" placeholder="Select">
                <input type="hidden" id="assign_role" name="assign_role">
                <div class="formcon formconnew">
                    <label>Reason</label>
                    <select name="assign_task_status" class="form-control assign_task_status" id="assign_task_status_<?php echo $task_form[0]['id']; ?>">
                        <option value="">select</option>
                        <?php foreach ($assign_task_status as $key => $value1) {
                            if ($value1['status_name'] != '') { ?>
                                <option value="<?php echo $value1['id'] ?>"><?php echo $value1['status_name'] ?></option>
                        <?php }
                        } ?>
                    </select>
                    <a href="javascript:;" id="" data-id="<?php echo $task_form[0]['id']; ?>" data-toggle="modal" data-target="#assignuser_<?php echo $task_form[0]['id']; ?>" class="assignuser_change "><i class="fa fa-plus"></i></a>

                    <a style="display: none" href="javascript:;" id="edit_user_change_<?php echo $task_form[0]['id']; ?>" data-id="<?php echo $task_form[0]['id']; ?>" data-toggle="modal" data-target="#assignuser_<?php echo $task_form[0]['id']; ?>" class="edit_user_change "><i class="fa fa-pencil"></i></a>

                    <a style="display: none" href="javascript:;" id="delete_user_change_<?php echo $task_form[0]['id']; ?>" data-id="<?php echo $task_form[0]['id']; ?>" data-toggle="modal" data-target="#assignuser_<?php echo $task_form[0]['id']; ?>" class="delete_user_change "><i class="fa fa-trash"></i></a>
                </div>
                <div class="formcon">
                    <label>Description</label>
                    <textarea rows="5" type="text" class="form-control" name="description" id="description_<?php echo $task_form[0]['id']; ?>" placeholder="Write something"></textarea>
                </div>
            </div>
            <div class="modal-footer profileEdit">
                <input type="hidden" name="hidden">
                <a href="javascript;" id="acompany_name" data-dismiss="modal" data-id="<?php echo $task_form[0]['id']; ?>" class="save_assign_staff">save</a>
            </div>
        </div>
    </div>
</div>

<div id="assignuser_<?php echo $task_form[0]['id']; ?>" class="adduserpopup03 task-reassign-modal modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-reason">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-reason">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align-last: center">Reason Status</h4>
            </div>
            <div class="modal-body delete_st_reason">
                <input type="text" name="assignuser_staff" class="assignuser_staff_class" id="assignuser_staff_<?php echo $task_form[0]['id']; ?>">
                <div style="color: red" class="assign_staff_div"></div>
            </div>
            <input type="hidden" name="assignuser_staffid" class="assignuser_staffid_class" id="assignuser_staffid_<?php echo $task_form[0]['id']; ?>">
            <input type="hidden" name="update_staffid" class="update_staffid_class" id="update_staffid_<?php echo $task_form[0]['id']; ?>">
            <div class="modal-footer profileEdit delete_st_reason">
                <!--   <input type="hidden" name="hidden"> -->
                <a href="javascript:;" id="assignuser_staff" data-id="<?php echo $task_form[0]['id']; ?>" class="assignuser_staff">save</a>
            </div>
            <div class="modal-footer profileEdit delete_div_reason" style="display: none;">
                <!--   <input type="hidden" name="hidden"> -->
                <a href="javascript:;" id="assignuser_staff" data-id="<?php echo $task_form[0]['id']; ?>" class="assignuser_staff">ok</a>
                <a href="javascript;" data-dismiss="modal" data-id="<?php echo $task_form[0]['id']; ?>">cancel</a>
            </div>
        </div>
    </div>
</div>

<div id="attach-screenshot" class="modal fade new-adds-categories" role="dialog">
    <div class="modal-dialog modal-dialog-upload">
        <!-- Modal content-->
        <form class="form-horizontal" action="<?php echo base_url() ?>tasksummary/UploadAttachFile" method="post" name="upload_excel" enctype="multipart/form-data" id="import_form">
            <div class="modal-content">
                <div class="modal-header modal-header-upload">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Upload Attach File</h4>
                </div>
                <div class="modal-body modal-body-upload">
                    <div class="add-input-service">
                        <label>Attach File</label>
                        <div class="custom_upload">
                            <label for="file" class="other-file"></label>
                            <input type="file" name="attach_file[]" id="file" class="input-large" required="required" multiple="multiple">
                        </div>
                    </div>
                    <span id="preview_image_name"></span>
                    <?php if (isset($task_form[0]['attach_file']) && ($task_form[0]['attach_file'] != '')) { //echo $task_form[0]['attach_file'];
                        $ex_img_val = explode(',', $task_form[0]['attach_file']);
                        foreach ($ex_img_val as $img_key => $img_value) { ?>
                            <div class="for_img_<?php echo $img_key; ?>">
                                <input type="hidden" name="already_upload_img[]" value="<?php echo $img_value; ?>">
                            </div><?php
                        }
                    } ?>
                </div>
                <!-- moadl=body -->
                <div class="modal-footer modal-footer-upload">
                    <a href="#" data-dismiss="modal">Cancel</a>
                    <button type="submit" id="submit" name="Import" class="btn btn-primary button-loading">Upload & Notify</button>
                    <input type="hidden" name="taskID" value="<?php echo $task_form[0]['id']; ?>">
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal-alertsuccess alert alert-success-check succ popup_success_msg" style="display:none;">
    <div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
        <div class="pop-realted1">
            <div class="position-alert1">
                Task Status have been changed successfully...
            </div>
        </div>
    </div>
</div>
<div class="pcoded-content card12 tasktop">
    <div class="pcoded-inner-content">
        <!-- Main-body start -->
        <div class="main-body taskbottom">
            <div class="page-wrapper">
                <div class="deadline-crm1 floating_set single-txt12 ddline nav-task-wrapper">
                    <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                        <li class="nav-item nav-item-task">
                            Task Details
                            <div class="slide"></div>
                        </li>
                    </ul>
                </div>                
                <!-- Page body start -->
                <div class="page-body task-d tsk-details1">
                    <div class="row">
                        <!-- Task-detail-right start -->
                        <?php if ($task_form[0]['start_date'] != '' && $task_form[0]['end_date'] != '') {
                            $start = date_create(implode('-', array_reverse(explode('-', $task_form[0]['start_date']))));
                            $end   = date_create(implode('-', array_reverse(explode('-', $task_form[0]['end_date']))));                            
                            $diff  = date_diff($start, $end);
                            $y     = $diff->y;
                            $m     = $diff->m;
                            $d     = $diff->d;
                            $h     = $diff->h;
                            $min   = $diff->i;
                            $sec   = $diff->s;
                        } else {
                            $y   = 0;
                            $m   = 0;
                            $d   = 0;
                            $h   = 0;
                            $min = 0;
                            $sec = 0;
                        }
                        $date_now = date("Y-m-d"); // this format is string comparable
                        $d_rec = implode('-', array_reverse(explode('-', $task_form[0]['end_date'])));
                        if ($date_now > $d_rec) {
                            //echo 'priya';
                            $d_val = "EXPIRED";
                        } else {
                            $d_val = $y . ' years ' . $m . ' months ' . $d . ' days ' . $h . ' Hours ' . $min . ' min ' . $sec . ' sec ';
                        } ?>
                        <input type="hidden" name="task_id" class="task_id" value="<?php echo $task_form[0]['id']; ?>">
                        <div class="col-xl-4 col-lg-12 push-xl-8 task-detail-right task-detail-right-wrapper">
                            <div class="card cardfull task-timer-div" id="task-timer-div" <?php if ($task_timer != 1) { ?> style="display: none;" <?php } ?>>
                                <div class="card-footer">
                                    <div class="f-right d-flex ">
                                        <?php if ($_SESSION['permission']['Task']['edit'] == '1') { ?>
                                            <span class="view_edit_delete_edit">
                                                <a href="<?php echo base_url() . 'user/update_task/' . $task_form[0]['id']; ?>" class="text-muted m-r-10 f-16" title="Edit"><i class="icofont icofont-edit"></i> </a>
                                            </span>
                                        <?php }
                                        if ($_SESSION['permission']['Task']['delete'] == '1') { ?>
                                            <span class="m-r-10 view_edit_delete">
                                                <a href="javascript:void(0)" class="text-muted f-16" onclick="return alltask_delete('<?php echo $task_form[0]['id']; ?>');" title="Delete"><i class="icofont icofont-ui-delete"></i></a>
                                            </span>
                                        <?php } ?>
                                    </div>
                                    <div class="f-left">
                                        <span class=" txt-primary"> <i class="icofont icofont-chart-line-alt"></i>
                                            Status:</span>
                                        <div class="dropdown-secondary dropdown timer_class d-inline-block">
                                            <select name="status" class="status_task" id="status_task" <?php if ($_SESSION['permission']['Task']['edit'] != '1') { ?> disabled="disabled" <?php } ?>>
                                                <?php $sel_task = $task_form[0]['task_status']; ?>
                                                <?php foreach ($task_status as $key => $status_val) { ?>
                                                    <option value="<?php echo $status_val['id']; ?>" <?php 
                                                    if ($sel_task == $status_val['id']) {
                                                        echo 'selected="selected"';
                                                    } ?>><?php echo $status_val['status_name']; ?></option><?php 
                                                } ?>
                                            </select>
                                            <select name="progress_status" id="progress_status" class="progress_status" data-id="<?php echo  $task_ID; ?>" <?php if ($_SESSION['permission']['Task']['edit'] != '1') { ?> disabled="disabled" <?php } ?>>
                                                <option value="">Select Progress Status</option>
                                                <?php foreach ($progress_status as $key => $status_val) { ?>
                                                    <option value="<?php echo $status_val['id']; ?>" <?php 
                                                    if ($task_form[0]['progress_status'] == $status_val['id']) {
                                                        echo 'selected="selected"';
                                                    } ?>><?php 
                                                    echo $status_val['status_name']; ?>
                                                    </option><?php 
                                                } ?>
                                            </select>
                                            <input type="hidden" class="hiden_status" value="<?php echo $task_form[0]['task_status']; ?>">
                                            <!-- for view invoice 29-09-2018 -->
                                            <!-- 29-09-2018 for invoice shown button -->
                                            <?php
                                            if (isset($task_form[0]['billable']) && ($task_form[0]['billable']) && ($task_form[0]['task_status'] == '5') == 'Billable' &&  empty($task_form[0]['sub_task_id'])) { ?>
                                                <a href="<?php echo base_url(); ?>invoice/EditInvoice/<?php echo $task_form[0]['invoice_id']; ?>" class="btn btn-success invoice-btn" target="_blank">View Invoice</a>
                                                <?php
                                            } else {
                                                if (isset($task_form[0]['billable']) && ($task_form[0]['billable']) && ($task_form[0]['task_status'] == '5') == 'Billable' &&  !empty($task_form[0]['sub_task_id'])) {
                                                    $check_array = $this->db->query("SELECT `task_status` FROM add_new_task where id  in (" . $task_form[0]['sub_task_id'] . ")")->result_array();
                                                    if (count($check_array) > 0) {
                                                        $filter_val = array_column($check_array, 'task_status');
                                                        $result = array_diff($filter_val, array('5'));
                                                        if (count($result) == 0) { ?>
                                                            <a href="<?php echo base_url(); ?>invoice/EditInvoice/<?php echo $task_form[0]['invoice_id']; ?>" class="btn btn-success invoice-btn" target="_blank">View Invoice</a><?php
                                                        }
                                                    }
                                                }
                                            } ?>
                                            <!-- end of 29-09-2018 -->
                                            <!-- for view invoice 29-09-2018 -->
                                            <!-- end of dropdown menu -->
                                        </div>
                                    </div>
                                </div>
                            </div><?php
                            if ($task_form[0]['related_to_services'] != '') {
                                $settings_val = $this->db->query("select * from service_lists where services_subnames='" . $task_form[0]['related_to_services'] . "' ")->row_array();
                            } else {
                                $settings_val['service_name'] = '-';
                            } ?>
                            </tr>
                            <?php $updated_at = $task_form[0]['updated_date']; ?>
                            <div class="card" style="<?php $result1 = array();
                                if ($task_form[0]['task_status'] == '5' && !empty($task_form[0]['sub_task_id'])) {

                                    $check_array = $this->db->query("SELECT `task_status` FROM add_new_task where id  in (" . $task_form[0]['sub_task_id'] . ")")->result_array();
                                    if (count($check_array) > 0) {
                                        $filter_val = array_column($check_array, 'task_status');

                                        $result1 = array_diff($filter_val, array('5'));
                                    }
                                    // print_r(count($result));


                                    if (count($result1) == 0) { ?> display: block; <?php } else { ?> display:none<?php }
                                } else  if ($task_form[0]['task_status'] == '5' && $task_form[0]['related_to'] != 'leads' && empty($task_form[0]['sub_task_id'])) { ?> display:block<?php } else { ?> display:none <?php  } ?>">
                                <div class="card-header">
                                    <h5 class="card-header-text"><i class="icofont icofont-wheel m-r-10"></i> Invoice</h5>
                                </div>
                                <div class="card-block task-setting">
                                    <div class="form-group for_edit_permission_settings">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label class="f-left">Send Invoice</label>
                                            </div>
                                        </div>
                                        <div class="row text-center">
                                            <div class="col-sm-12 for_edit_settings">
                                                <button type="button" class="btn btn-default waves-effect p-l-40 p-r-40 m-r-30 reset-btn Send-invoice" id="<?php echo $task_form[0]['invoice_id']; ?>">Send
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                            <div class="card cardfull for_user_assign">
                                <div class="card-header" id="user-assign-task" data-toggle="collapse" data-target="#assigned_user">
                                <!-- <button class="task-detail-timer" id="start_button">timer</button> -->
                                <button class="task-detail-timer timer_button" id="start_button">Start</button>
                                <button class="task-detail-timer timer_button" id="timer_submit">Stop</button>
                                    <div class="stopwatch-wrapper">
                                        <i class="icofont icofont-clock-time m-r-10"></i>
                                        <?php $tot_tmr = explode(':', $tot_timer); ?>                                      

                                        <div class          = "stopwatch" 
                                             data-autostart = "false" 
                                             data-date      = "<?php echo '2018/06/23 15:37:25'; ?>" 
                                             data-id        = "<?php echo $task_form[0]['id']; ?>" 
                                             data-hour      = "<?php echo $tot_tmr[0]; ?>"
                                             data-min       = "<?php echo $tot_tmr[1]; ?>" 
                                             data-sec       = "<?php echo $tot_tmr[2]; ?>" 
                                             data-mili      = "0"                                              
                                             data-current   = "Start" 
                                             data-pauseon   = "<?php echo $pause; ?>">                                            
                                            <div class="time timer_<?php echo $task_form[0]['id']; ?>">
                                                <span class="hours">00</span>:
                                                <span class="minutes">00</span>:
                                                <span class="seconds">00</span>
                                            </div>
                                            <div class="controls per_timerplay_<?php echo $task_form[0]['id']; ?>" id="<?php echo $task_form[0]['id']; ?>"></div>
                                        </div>
                                    </div>
                                    <div>
                                        <h5 class="card-header-text"><i class="icofont icofont-users-alt-4"></i> Assigned users</h5>
                                    </div>
                                </div>
                                <div class="card-block user-box assign-user" id="assigned_user" class="collapse"><?php
                                    if(!empty($utask)){ 
                                        foreach($utask as $staff_id=>$log){ 
                                            $removed_class = "";
                                            $removed_user = "";
                    
                                            if (in_array($s_val, $remove_result)) {                    
                                                $removed_class = "removed_class";
                                                $removed_user = "(Removed)";
                                            } ?>
                                            <div class="media <?php echo $removed_class ?>">
                                                <div class="media-left media-middle photo-table">
                                                    <input type="hidden" class="staff_id" id="staff_id" value="<?php echo $staff_id; ?>">
                                                    <a href="#">
                                                      <img class="rt-comment-img" src="<?php //echo $getUserProfilepic; ?>" alt="Not Found" onerror="this.src='<?php echo  base_url() ?>assets/images/avatar-3.jpg';">                                                   
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="rt-text-comment-wrap"><?php
                                                        echo $log['name'];
                                                        echo " " . $removed_user; ?>
                                                    </h6>
                                                    <p> <?php echo $log['role']; ?> </p>
                                                </div>
                                                <div>
                                                    <!-- its timer -->
                                                    <?php $tbreak = explode(':', $log['time']); ?>
                                                    <div class          = "stopwatch" 
                                                         data-autostart = "false" 
                                                         data-id        = "<?php echo $staff_id; ?>" 
                                                         data-hour      = "<?php echo $tbreak[0]; ?>" 
                                                         data-min       = "<?php echo $tbreak[1]; ?>" 
                                                         data-sec       = "<?php echo $tbreak[2]; ?>" 
                                                         data-current   = "Start" 
                                                         data-pauseon   = "<?php echo $pause; ?>">
                                                      
                                                        <div class = "time">
                                                            <span class = "hours"></span> :
                                                            <span class = "minutes"></span> :
                                                            <span class = "seconds"></span>
                                                        </div>
                                                    </div>
                                                    <!-- end of timer -->
                                                </div>
                                            </div><?php
                                        }
                                    } ?>                                                                                                                                                                                                                                                                                                                                                                                                                                    
                                 </div>
                            </div>
                            <div class="card cardfull">
                                <div class="card-header">
                                    <h5 class="card-header-text"><i class="icofont icofont-wheel m-r-10"></i> Task settings</h5>
                                    <div class="dropdown-secondary dropdown d-inline-block test1">
                                        <button class="btn btn-sm btn-primary dropdown-toggle waves-light" type="button" id="dropdown3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icofont icofont-navigation-menu"></i></button>
                                        <div class="dropdown-menu" aria-labelledby="dropdown3" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                            <!--   <a class="dropdown-item waves-light waves-effect" href="#!"><i class="icofont icofont-checked m-r-10"></i>Check in</a> -->
                                            <a class="dropdown-item waves-light waves-effect attach-screenshot" href="javascript:;" data-toggle="modal" data-target="#attach-screenshot" <?php if ($attach_file != 1) { ?> style="display: none;" <?php } ?>><i class="icofont icofont-attachment m-r-10"></i>Attach screenshot</a>
                                            <?php if ($_SESSION['permission']['Task']['edit'] == 1) { ?>
                                                <a class="dropdown-item waves-light waves-effect for_edit_permission_reassign" href="javascript:;" data-toggle="modal" data-target="#adduser_<?php echo $task_form[0]['id']; ?>"><i class="icofont icofont-rotation m-r-10"></i>Reassign</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item waves-light waves-effect" href="<?php echo base_url() . 'user/update_task/' . $task_form[0]['id']; ?>"><i class="icofont icofont-edit-alt m-r-10"></i>Edit task</a>
                                            <?php }
                                            if ($_SESSION['permission']['Task']['delete'] == 1) { ?>

                                                <a class="dropdown-item waves-light waves-effect for_edit_permission_remove" href="javascript:void(0)" onclick="return alltask_delete('<?php echo $task_form[0]['id']; ?>');"><i class="icofont icofont-close m-r-10"></i>Remove</a>
                                            <?php } ?>
                                        </div>
                                        <!-- end of dropdown menu -->
                                    </div>
                                </div>
                                <?php if ($_SESSION['permission']['Task']['edit'] == 1) {  ?>
                                    <form id="addtasksetting" method="post">
                                        <div class="card-block task-setting">
                                            <div class="form-group for_edit_permission_settings">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label class="f-left">Allow comments</label>
                                                        <input type="checkbox" name="hidden_cmts" class="js-small f-right unsel-task allow_comments for_switchery" id="allow_comments" data-switchery="true" style="display: none;" value="1" <?php if ($allow_comments == 1) {echo "checked";} ?>>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label class="f-left">Use task timer</label>
                                                        <input type="checkbox" name="hidden_task_timer" class="js-small f-right unsel-task task_timer for_switchery" id="task_timer" data-switchery="true" style="display: none;" value="1" <?php if ($task_timer == 1) {echo "checked";} ?>>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label class="f-left">Allow attachments</label>
                                                        <input type="checkbox" name="hidden_alw_attach" class="js-small f-right unsel-task alw_attach for_switchery" id="alw_attach" data-switchery="true" style="display: none;" value="1" <?php if ($attach_file == 1) {echo "checked";} ?>>
                                                    </div>
                                                </div>
                                                <div class="row text-center">
                                                    <div class="col-sm-12 for_edit_settings">
                                                        <button type="button" class="btn btn-default waves-effect p-l-40 p-r-40  m-r-30 reset-btn" id="reset-btn" onclick="clear_form_elements('#addtasksetting')">Reset
                                                        </button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light p-l-40 p-r-40">Save</button>
                                                        <input type="hidden" name="task_setting_id" value="<?php echo $task_form[0]['id']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                <?php } ?>
                            </div>
                            <div class="card cardfull">
                                <div class="card-header">
                                    <h5 class="card-header-text"><i class="icofont icofont-certificate-alt-2 m-r-10"></i> Revisions</h5>
                                </div>
                                <div class="card-block revision-block">
                                    <div class="form-group">
                                        <div class="row">
                                            <?php if ($task_form[0]['created_date'] != '') {
                                                $revision_task = date('Y-m-d H:i:s', $task_form[0]['created_date']);
                                            } ?>
                                            <ul class="media-list revision-blc">
                                                <li class="media d-flex m-b-15">
                                                    <div class="p-l-15 p-r-20 d-inline-block v-middle"><a href="#" class="btn btn-outline-primary btn-lg txt-muted btn-icon"><i class="icon-ghost f-18 v-middle"></i></a></div>
                                                    <div class="d-inline-block">
                                                        <div class="media-annotation"><?php if ($task_form[0]['created_date'] != '') {echo time_elapsed_string($revision_task); } ?></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end of edit permission -->
                            <div class="card cardfull attach-file-div" id="attach-file-div" <?php if ($attach_file != 1) { ?> style="display: none;" <?php } ?>>
                                <div class="card-header">
                                    <!-- <h5 class="card-header-text"><i class="icofont icofont-attachment"></i> Attached files</h5> -->
                                    <a class="dropdown-item waves-light waves-effect" href="javascript:;" data-toggle="modal" data-target="#attach-screenshot"><i class="icofont icofont-attachment m-r-10"></i> Click here to Attach Files</a>
                                </div>
                                <?php if (isset($task_form[0]['attach_file']) && ($task_form[0]['attach_file'] != '')) { ?>
                                    <div class="card-block task-attachment">
                                        <ul class="media-list">
                                            <?php
                                            $ex_val = explode(',', $task_form[0]['attach_file']);
                                            if (count($ex_val) > 0) {
                                                foreach ($ex_val as $img_key => $img_value) {?>
                                                    <li class="media d-flex m-b-10">
                                                        <div class="m-r-20 v-middle">
                                                            <i class="icofont icofont-file-word f-28 text-muted"></i>
                                                        </div>
                                                        <div class="media-body">
                                                            <a href="#" class="m-b-5 d-block"> <?php echo $img_value; ?> </a>
                                                            <div class="text-muted"></div>
                                                        </div>
                                                        <div class="f-right v-middle text-muted">                                                            
                                                            <a href="<?php echo $img_value ?>" download><i class="icofont icofont-download-alt f-18"></i></a>
                                                            <a href="<?php echo $img_value; ?>" target="_blank">View</a>
                                                            <?php if ($_SESSION['permission']['Task']['delete'] == '1') { ?>
                                                            <a href="javascript:void(0)" data-name="<?php echo $img_value ?>" class="remove_file" data-task_id="<?php echo $task_form[0]['id'] ?>" data-id="<?php echo $img_key ?>">Remove</a><?php } ?>                                                            
                                                        </div>
                                                    </li>
                                            <?php }
                                            } ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <!-- Task-detail-right start -->
                        <!-- Task-detail-left start -->
                        <div class="col-xl-8 col-lg-12 pull-xl-4 task-details-wrapper">
                            <div class="card cardfull">
                                <div class="card-header">
                                    <div class="task-timer-header-wrap">
                                        <h5><i class="icofont icofont-tasks-alt m-r-5"></i> Subject :</h5>
                                        <!-- <button class="btn btn-sm btn-primary f-right"><i class="icofont icofont-ui-alarm"></i>Check in</button> -->
                                        <p><?php                                            
                                        $client_href = !empty($this->Common_mdl->GetAllWithWhere('client', 'id', $task_form[0]['company_name'])[0]['user_id']) ? base_url() . 'client/client_infomation/' . $this->Common_mdl->GetAllWithWhere('client', 'id', $task_form[0]['company_name'])[0]['user_id'] : 'javascript:void(0);';
                                        $target = ($client_href == 'javascript:void(0);') ? '' : 'target="_blank"';
                                        echo !empty($task_form[0]['subject']) ? '<a href="' . $client_href . '"  ' . $target . '>' . ucwords($task_form[0]['subject']) . '</a>' : ''; ?> </p>
                                    </div>
                                    <?php $timeArr = explode(':', $curr_timer); ?>
                                    <div class="time countup" style="display: inline-flex;" id="countup<?php echo $task_form[0]['id']; ?>">
                                        <span id="hour" class="timeel hours"><?php echo isset($timeArr[0]) ? $timeArr[0] : '00'; ?></span>
                                        <span>:</span>
                                        <span id="min" class="timeel minutes"><?php echo isset($timeArr[1]) ? $timeArr[1] : '00'; ?></span>
                                        <span>:</span>
                                        <span id="sec" class="timeel seconds"><?php echo isset($timeArr[2]) ? $timeArr[2] : '00'; ?></span>
                                    </div>
                                </div>
                                <div class="card-block martopcls">
                                    <div class="">
                                        <div class="m-b-20 task-details-description">
                                            <h6 class="sub-title m-b-15">Description</h6>
                                            <p>
                                                <?php echo !empty($task_form[0]['description']) ? $task_form[0]['description'] : ''; ?>
                                            </p>
                                        </div>
                                        <div class="m-b-20 card-tskbg">
                                            <div class="table-responsive m-t-20">
                                                <table class="table m-b-0 f-14 b-solid requid-table">
                                                    <thead>
                                                        <tr class="text-uppercase">
                                                            <th>#</th>
                                                            <th>Task :
                                                                <?php
                                                                if ($task_form[0]['related_to_services'] != '' && $task_form[0]['related_to_services'] != '0' &&  $task_form[0]['related_to'] != 'sub_task') {
                                                                    $services = trim($task_form[0]['related_to_services']);
                                                                    $services = explode("_", $services);
                                                                    $task_id = isset($task_form[0]['task_id']) ? $task_form[0]['task_id'] : '';

                                                                    if (isset($services[1]) && $services[0] == 'wor') {
                                                                        $table = 'work_flow';
                                                                        $service_id = $services[1];
                                                                    } else {
                                                                        $table = 'service_lists';
                                                                        $service_id = (isset($services[1])) ? $services[1] : $services[0];
                                                                    }
                                                                    $services_name = $this->db->query("select * from " . $table . " where id=" . $service_id . "  ")->row_array();                                                                    
                                                                    if (isset($services_name['service_name']) && $services_name['service_name'] != '') {
                                                                        $step_name = $services_name['service_name'];
                                                                    }
                                                                } ?>
                                                                <?php echo $task_form[0]['subject'] ?>
                                                            </th>
                                                            <th>Due Date</th>
                                                            <!-- <th>Description</th> -->
                                                        </tr>
                                                    </thead>                                                    
                                                    <tbody class="text-center text-muted"><?php
                                                        if ($task_form[0]['sub_task_id'] != '') {
                                                            $exval = explode(',', $task_form[0]['sub_task_id']);

                                                            foreach ($exval as $ex_key => $ex_value) {
                                                                $get_data = $this->db->query('select * from add_new_task where id=' . $ex_value . ' ')->row_array();
                                                                $checked = '';
                                                                if ($get_data['task_status'] == '5') {
                                                                    $checked = "checked='checked'";
                                                                }
                                                                if ($task_form[0]['related_to_services'] != '' && $task_form[0]['related_to_services'] != '0' &&  $task_form[0]['related_to'] != 'sub_task') {
                                                                    $result = $this->Service_model->get_steps($get_data['services_main_id'], 'id');
                                                                    if (!empty($result) && isset($result[0]['id'])) {
                                                                        $steps_details = $this->Service_model->get_sub_steps($result[0]['id'], 'step_id'); ?>
                                                                        <tr>
                                                                            <td>
                                                                                <?php echo $ex_key + 1; ?>
                                                                            </td>
                                                                            <td>
                                                                                <div><label class="custom_checkbox1"> <input type="checkbox" class="subTaskcheckbox" <?php echo $checked ?> data-alltask-id="<?php echo $get_data['id'] ?> "> <i></i> </label>
                                                                                    <?php echo $result[0]['title']; ?>
                                                                                </div><?php
                                                                                $ii = 1;
                                                                                foreach ($steps_details as $key => $details_rec) { ?>
                                                                                    <div class="steps-details-parent">
                                                                                        <div class="child-services">
                                                                                            <span> <?php echo $i . "." . $ii . ")  " . $details_rec['step_content']; ?></span>
                                                                                        </div>
                                                                                    </div>
                                                                                <?php
                                                                                    $ii++;
                                                                                } ?>
                                                                            </td>
                                                                            <td>
                                                                                <div class="date-timer-wrapper">
                                                                                    <div> <i class="icofont icofont-ui-calendar"></i>&nbsp;
                                                                                        <?php echo date("d / m / Y", strtotime($get_data['end_date'])); ?></div>
                                                                                    <?php
                                                                                    if ($get_data['counttimer'] != "") {
                                                                                        $couter_ar = json_decode($get_data['counttimer'], true); ?>
                                                                                        <div class="time">
                                                                                            <span class="hours"><?php echo date("h", $couter_ar["hours"]); ?></span> : <span class="minutes"><?php echo date("i", $couter_ar["minutes"]); ?></span> : <span class="seconds"><?php echo date("s", $couter_ar["seconds"]); ?></span>
                                                                                        </div>
                                                                                    <?php } else { ?>
                                                                                        <div class="time">
                                                                                            <span class="hours">00</span> : <span class="minutes">00</span> : <span class="seconds">00</span>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </td>                                                                            
                                                                        </tr>
                                                                    <?php
                                                                    }
                                                                } else { ?>
                                                                    <tr>
                                                                        <td><?php echo $ex_key + 1; ?></td>
                                                                        <td>
                                                                            <div><label class="custom_checkbox1"> <input type="checkbox" class="subTaskcheckbox" <?php echo $checked ?> data-alltask-id="<?php echo $get_data['id'] ?> "> <i></i> </label><?php echo $get_data['subject']; ?></div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="date-timer-wrapper">
                                                                                <div><i class="icofont icofont-ui-calendar"></i>&nbsp;
                                                                                    <?php echo $get_data['end_date']; ?></div>
                                                                                <div class="task-timer-dt">Timer</div>
                                                                            </div>
                                                                        </td>                                                                        
                                                                    </tr>
                                                            <?php }
                                                            }
                                                        } else { ?>
                                                            <tr>
                                                                <td>1</td>
                                                                <td><?php echo $task_form[0]['subject']; ?></td>
                                                                <td>
                                                                    <div class="date-timer-wrapper">
                                                                        <div><i class="icofont icofont-ui-calendar"></i>&nbsp;
                                                                            <?php echo $task_form[0]['end_date']; ?></div>
                                                                        <div class="task-timer-dt">Timer</div>
                                                                    </div>
                                                                </td>
                                                                <!-- <td> <?php echo !empty($task_form[0]['description']) ? $task_form[0]['description'] : ''; ?> </td> -->
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card cardfull comment-block comments-div taskcommentcls" id="comments-div" <?php if ($allow_comments != 1) { ?> style="display:none;" <?php } ?>>
                                    <div class="card-header">
                                        <h5 class="card-header-text"><i class="icofont icofont-comment m-r-5"></i> Comments</h5>
                                        <button type="submit" class="btn btn-primary btn-task-comment" data-toggle="modal" data-target="#commentModal">add comments</button>                                        
                                    </div>
                                    <?php include('task_comments_html.php'); ?>
                                </div>
                                <div class="client_section user-dashboard-section1 floating_set">
                                    <div class="all_user-section floating_set proposal-commontab">
                                        <div class="row history tasktime tasktimenew">
                                            <div class="card-header">
                                                <h5>Timeline</h5>
                                            </div>
                                            <div class="mar_bottls matter_notes1">
                                                <h5><i class="icofont icofont-file-text m-r-5"></i></h5>
                                                <input class="form-control form-control-lg" type="text" id="search-criteria" placeholder="Search notes">
                                            </div>
                                            <div class="card">
                                                <div class="row">
                                                    <div class="col-12 post cmt_class"><?php
                                                        function getValue($arr, $indexes, $tasks_status)
                                                        {
                                                            foreach ($arr as $dataset) {
                                                                if ($dataset['id'] == $indexes[1]) {
                                                                    if (is_numeric($dataset[$indexes[0]]) && isset($tasks_status[$dataset[$indexes[0]] - 1]) && $indexes[0] == 'task_status') {
                                                                        $dataset[$indexes[0]] = ucwords($tasks_status[$dataset[$indexes[0]] - 1]['status_name']);
                                                                    } else if ($indexes[0] == 'task_status' && !isset($tasks_status[$dataset[$indexes[0]] - 1])) {
                                                                        return false;
                                                                    }
                                                                    return $dataset[$indexes[0]];
                                                                }
                                                            }
                                                        }

                                                        foreach ($activity_log as $activity) {
                                                            $username    = '';
                                                            $task_status = '';

                                                            if ($activity['module_id'] != '' && is_numeric($activity['module_id'])) {
                                                                $get_data       = $this->db->query('select * from add_new_task where id=' . $activity['module_id'] . ' ')->row_array();
                                                                $username       = $this->Common_mdl->getUserProfileName($activity['user_id']);
                                                                $task_status    = getValue($task_form, ['task_status', $activity['module_id']], $tasks_status);
                                                                $task_startdate = getValue($task_form, ['start_date', $activity['module_id']], '');
                                                                $task_startdate = !empty(strtotime($task_startdate)) ? date('d-m-Y', strtotime($task_startdate)) : '';
                                                                $task_enddate   = getValue($task_form, ['end_date', $activity['module_id']], '');
                                                                $task_enddate   = !empty(strtotime($task_enddate)) ? date('d-m-Y', strtotime($task_enddate)) : '';
                                                                $company_name   = getValue($task_form, ['company_name', $activity['module_id']], '');
                                                                $related_to     = getValue($task_form, ['related_to', $activity['module_id']], '');
                                                            } ?>

                                                            <div class="cd-timeline-block <?php echo $get_data['id']; ?>">
                                                                <div class="cd-timeline-icon bg-primary">
                                                                    <i class="icofont icofont-tasks-alt"></i>
                                                                </div>
                                                                <div class="cd-timeline-content card_main">
                                                                    <?php if (!empty($task_status)) { ?>
                                                                        <div class="p-0">
                                                                            <div class="btn-group dropdown-split-primary"><button type="button" class="btn btn-primary search_word"><?php echo $task_status; ?></button></div>
                                                                        </div>
                                                                    <?php } ?>
                                                                    <div class="p-20 search_word">
                                                                        <h6><?php echo $get_data['subject']; ?> </h6>
                                                                        <div class="timeline-details search_word">
                                                                            <a href="javascript:void(0);"> <i class="icofont icofont-ui-calendar"></i><span>Start Date : <?php echo $task_startdate; ?></span> </a>
                                                                            <a href="javascript:void(0);"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo $task_enddate; ?></span> </a>
                                                                            <p class="m-t-0 search_word">Company Name : <?php
                                                                                if (isset($company_name) && is_numeric($company_name) &&  $company_name != '' && $related_to != 'leads') {
                                                                                    echo $this->Common_mdl->getcompany_name($company_name);
                                                                                } else {
                                                                                    echo '-';
                                                                                } ?>
                                                                            </p>
                                                                            <p class="m-t-0 search_word"> Staffs:
                                                                                <span class="colrred"><?php
                                                                                    $explode_worker = Get_Module_Assigees('TASK', $activity['module_id']);
                                                                                    $names          = array();

                                                                                    foreach ($explode_worker as $key => $val) {
                                                                                        $getUserProfilename = $this->Common_mdl->getUserProfileName($val);
                                                                                        array_push($names, $getUserProfilename);
                                                                                    }
                                                                                    $variable = array_filter($names);
                                                                                    if (!empty($variable)) {
                                                                                        echo implode(',', $names);
                                                                                    } else {
                                                                                        echo "Not Assigned";
                                                                                    } ?> 
                                                                                </span>
                                                                            </p>
                                                                            <p class="m-t-0 search_word">
                                                                                <?php if ($activity['sub_module'] == 'Task_Review') {
                                                                                    echo " <span class='colrred'>" . $username . '</span>   ' . $activity['log'];
                                                                                } else {
                                                                                    echo $activity['log'] . " <span class='colrred'>" . $username . '</span>';
                                                                                }
                                                                                ?> </p>
                                                                            <p class="m-t-0 search_word">Created At:<?php echo date('Y-m-d h:i:sa', $activity['CreatedTime']); ?> </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><?php   
                                                        }
                                                        if ($task_form[0]['sub_task_id'] != '') {
                                                            $exval = explode(',', $task_form[0]['sub_task_id']);

                                                            foreach ($exval as $ex_key => $ex_value) {
                                                                $ass_val = array();
                                                                $ass_val = Get_Module_Assigees('TASK', $ex_value);
                                                                $get_data = $this->db->query('select * from add_new_task where id=' . $ex_value . ' ')->row_array(); ?>

                                                                <div class="cd-timeline-block <?php echo $get_data['id']; ?>">
                                                                    <div class="cd-timeline-icon bg-primary">
                                                                        <i class="icofont icofont-tasks-alt"></i>
                                                                    </div>
                                                                    <div class="cd-timeline-content card_main">
                                                                        <div class="p-20 search_word">
                                                                            <h6><?php echo $get_data['subject']; ?> </h6>
                                                                            <div class="timeline-details search_word">
                                                                                <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Start Date : <?php echo (strtotime($get_data['start_date']) != '' ?  date('d-m-Y', strtotime($get_data['start_date'])) : ''); ?></span> </a>
                                                                                <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($get_data['end_date']) != '' ?  date('d-m-Y', strtotime($get_data['end_date'])) : ''); ?></span> </a>
                                                                                <p class="m-t-0 search_word">Company Name : <?php 
                                                                                    if ($get_data['related_to'] != 'leads') {
                                                                                        if ($get_data['company_name'] != '' && is_numeric($get_data['company_name'])) {
                                                                                            echo $this->Common_mdl->getcompany_name($get_data['company_name']);
                                                                                        }
                                                                                    } else {
                                                                                        $lead_cm = $this->Common_mdl->GetAllWithWhere('leads', 'id', $get_data['lead_id']);
                                                                                        //  GetAllWithWhere
                                                                                        echo isset($lead_cm[0][' company']) ?: $lead_cm[0][' company'];
                                                                                    }?>
                                                                                </p>
                                                                                <p class="m-t-0 search_word"> Staffs:
                                                                                    <span class="colrred"><?php
                                                                                        $explode_worker = $ass_val;
                                                                                        $username = array();
                                                                                        foreach ($explode_worker as $key => $val) {
                                                                                            $getUserProfilename = $this->Common_mdl->getUserProfileName($val);
                                                                                            array_push($username, $getUserProfilename);
                                                                                        }
                                                                                        $variable = array_filter($username);
                                                                                        if (!empty($variable)) {
                                                                                            echo implode(',', $username);
                                                                                        } else {
                                                                                            echo "Not Assigned";
                                                                                        } ?> 
                                                                                    </span>
                                                                                </p>
                                                                                <p class="m-t-0 search_word">Status:<?php echo $get_data['task_status']; ?> </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><?php
                                                            }
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" class="Ses_ID" value="<?php echo $userid; ?>">
                            <input type="hidden" class="Ses_name" value="<?php echo $user_details['username']; ?>">
                        </div>
                        <!-- Task-detail-left end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$data = $this->db->query("SELECT * FROM update_client")->result_array();
foreach ($data as $row) { ?>
    <!-- Modal -->
    <div id="myModal<?php echo $row['user_id']; ?>" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">CRM-Updated fields</h4>
                </div>
                <div class="modal-body">
                    <?php if (isset($row['company_name']) && ($row['company_name'] != '0')) { ?>
                        <p>Company Name: <span><?php echo $row['company_name']; ?></span></p>
                    <?php } ?>
                    <?php if (isset($row['company_url']) && ($row['company_url'] != '0')) { ?>
                        <p>Company URL: <span><?php echo $row['company_url']; ?></span></p>
                    <?php } ?>
                    <?php if (isset($row['officers_url']) && ($row['officers_url'] != '0')) { ?>
                        <p>Officers URL: <span><?php echo $row['officers_url']; ?></span></p>
                    <?php } ?>
                    <?php if (isset($row['incorporation_date']) && ($row['incorporation_date'] != '0')) { ?>
                        <p>Incorporation Date: <span><?php echo $row['incorporation_date']; ?></span></p>
                    <?php } ?>
                    <?php if (isset($row['register_address']) && ($row['register_address'] != '0')) { ?>
                        <p>Registered Address: <span><?php echo $row['register_address']; ?></span></p>
                    <?php }  ?>
                    <?php if (isset($row['company_status']) && ($row['company_status'] != '0')) { ?>
                        <p>Company Status: <span><?php echo $row['company_status']; ?></span></p>
                    <?php }  ?>
                    <?php if (isset($row['company_type']) && ($row['company_type'] != '0')) { ?>
                        <p>Company Type: <span><?php echo $row['company_type']; ?></span></p>
                    <?php } ?>
                    <?php if (isset($row['accounts_periodend']) && ($row['accounts_periodend'] != '0')) { ?>
                        <p>Accounts Period End: <span><?php echo $row['accounts_periodend']; ?></span></p>
                    <?php } ?>
                    <?php if (isset($row['hmrc_yearend']) && ($row['hmrc_yearend'] != '0')) { ?>
                        <p>HMRC Year End: <span><?php echo $row['hmrc_yearend']; ?></span></p>
                    <?php } ?>
                    <?php if (isset($row['ch_accounts_next_due']) && ($row['ch_accounts_next_due'] != '0')) { ?>
                        <p>CH Accounts Next Due: <span><?php echo $row['ch_accounts_next_due']; ?></span></p>
                    <?php } ?>
                    <?php if (isset($row['confirmation_statement_date']) && ($row['confirmation_statement_date'] != '0')) { ?>
                        <p>Confirmation Statement Date: <span><?php echo $row['confirmation_statement_date']; ?></span></p>
                    <?php } ?>
                    <?php if (isset($row['confirmation_statement_due_date']) && ($row['confirmation_statement_due_date'] != '0')) { ?>
                        <p>Confirmation Statement Due: <span><?php echo $row['confirmation_statement_due_date']; ?></span></p>
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->

<?php } ?>
<?php $this->load->view('includes/session_timeout'); ?>
<?php $this->load->view('includes/footer'); ?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<!-- <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>  -->
<script src="<?php echo base_url(); ?>js/jquery.workout-timer.js"></script>
<!-- for jquery date time format js -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/date.format.js"></script>
<!-- end of js 03-07-2018 -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/dataTables.responsive.min.js"></script>
<script>
    function humanise(diff) {

        // The string we're working with to create the representation 
        var str = '';
        // Map lengths of `diff` to different time periods 
        var values = [
            [' year', 365],
            [' month', 30],
            [' day', 1]
        ];
        // Iterate over the values... 
        for (var i = 0; i < values.length; i++) {
            var amount = Math.floor(diff / values[i][1]);
            // ... and find the largest time value that fits into the diff 
            if (amount >= 1) {
                // If we match, add to the string ('s' is for pluralization) 
                str += amount + values[i][0] + (amount > 1 ? 's' : '') + ' ';
                // and subtract from the diff 
                diff -= amount * values[i][1];
            } else {
                str += amount + values[i][0] + ' ';
            }
        }
        return str;
    }
</script>
<?php

?><script></script>
<script>
    $(document).ready(function() {

        humanise();
        tinymce_config['images_upload_url'] = "<?php echo base_url(); ?>Firm/uplode_mail_header_image";
        tinymce_config['selector'] = '#add_comments';
        tinymce.init(tinymce_config);

        var tabletask1 = $("#alltask").dataTable({
            "iDisplayLength": 10,
            "scrollX": true,
            "dom": '<"toolbar-table">lfrtip',
            responsive: true
        });
        $("div.toolbar-table").html('<div class="filter-task1"><h2>Filter:</h2><ul><li><img src="<?php echo  base_url() ?>assets/images/by-date2.png" alt="data"><select id="statuswise_filter" class="statuswise_filter"><option value="">By Status</option> <option value="notstarted">Not started</option><option value="inprogress">In Progress</option><option value="awaiting">Awaiting Feedback</option><option value="testing">Testing</option><option value="complete">Complete</option></select></li><li><img src="<?php echo  base_url() ?>assets/images/by-date3.png" alt="data"><select id="prioritywise_filter" class="prioritywise_filter"><option value="">By Priority</option><option value="low">Low</option><option value="medium">Medium</option><option value="high">High</option><option value="super_urgent">Super Urgent</option></select></li><li><select id="export_report" class="export_report"><option value="">Export</option><option value="excel">Excel</option><option value="pdf">PDF</option><option value="html">HTML</option></select></li></ul></div>');

        $("#inprogresses").dataTable({
            "iDisplayLength": 10,
        });
        $("#notstarted").dataTable({
            "iDisplayLength": 10,
        });
        $("#started").dataTable({
            "iDisplayLength": 10,
        });

        $(document).on('change', '.task_status', function() {
            //$(".task_status").change(function(){
            var rec_id = $(this).data('id');
            var stat = $(this).val();
            $.ajax({
                url: '<?php echo base_url(); ?>user/task_statusChange/',
                type: 'post',
                data: {
                    'rec_id': rec_id,
                    'status': stat
                },
                timeout: 3000,
                success: function(data) {
                    //alert('ggg');
                    if (data == 2) {
                        $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! You are has been remove from this task successfully...</span></div></div></div>').hide().fadeIn(1500, function() {
                            $('#status_succ');
                        });
                        setTimeout(resetAll, 3000);
                        window.location.href = "<?php echo base_url() ?>user/task_list";

                    } else {
                        $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Task status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() {
                            $('#status_succ');
                        });
                        setTimeout(resetAll, 3000);
                    }                    
                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
        });
        
        $('#alluser').on('change', '.status', function() {        
            var rec_id = $(this).data('id');
            var stat = $(this).val();

            $.ajax({
                url: '<?php echo base_url(); ?>user/statusChange/',
                type: 'post',
                data: {
                    'rec_id': rec_id,
                    'status': stat
                },
                timeout: 3000,
                success: function(data) {
                    $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() {
                        $('#status_succ');
                    });
                    setTimeout(resetAll, 3000);
                    if (stat == '3') {                        
                        $('#frozen' + rec_id).html('Frozen');
                    } else {
                        $this.closest('td').next('td').html('Inactive');
                    }
                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
        });

        // payment/non payment status
        $('.suspent').click(function() {
            if ($(this).is(':checked'))
                var stat = '1';
            else
                var stat = '0';
            var rec_id = $(this).val();
            var $this = $(this);
            $.ajax({
                url: '<?php echo base_url(); ?>user/suspentChange/',
                type: 'post',
                data: {
                    'rec_id': rec_id,
                    'status': stat
                },
                success: function(data) {
                    $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
                    if (stat == '1') {
                        $this.closest('td').next('td').html('Payment');
                    } else {
                        $this.closest('td').next('td').html('Non payment');
                    }
                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function() {
        var table = $('#alltask').DataTable();

        $('#dropdown2').on('change', function() {
            var filterstatus = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>tasksummary/taskFilter",
                data: {
                    filterstatus: filterstatus
                },
                success: function(response) {
                    $(".all-usera1").html(response);
                },                
            });
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('.dropdown-sin-2').dropdown({
            limitCount: 5,
            input: '<input type="text" maxLength="20" placeholder="Search">'
        });
        $("#bulkDelete").on('click', function() { // bulk checked
            var status = this.checked;

            $(".deleteRow").each(function() {
                $(this).prop("checked", status);
            });
            if (status == true) {
                $('#deleteTriger').show();
            } else {
                $('#deleteTriger').hide();
            }
        });

        $(".deleteRow").on('click', function() { // bulk checked
            var status = this.checked;
            if (status == true) {
                $('#deleteTriger').show();
            } else {
                $('#deleteTriger').hide();
            }
        });

        $('#deleteTriger').on("click", function(event) { // triggering delete one by one
            if ($('.deleteRow:checked').length > 0) { // at-least one checkbox checked
                var ids = [];
                $('.deleteRow').each(function() {
                    if ($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });
                var ids_string = ids.toString(); // array to string conversion 
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>user/task_delete/",
                    data: {
                        data_ids: ids_string
                    },
                    success: function(response) {
                        var emp_ids = response.split(",");
                        for (var i = 0; i < emp_ids.length; i++) {
                            $("#" + emp_ids[i]).remove();
                        }
                    },
                });
            }
        });

        $(document).on('change', ".statuswise_filter", function() {
            var data = {};
            data['status'] = $(this).val();
            data['priority'] = $('.prioritywise_filter').val();
            get_result(data);
            return false;
        });
        $(document).on('change', ".prioritywise_filter", function() {
            var data = {};
            data['priority'] = $(this).val();
            data['status'] = $('.statuswise_filter').val();
            get_result(data);
            return false;
        });

        $(document).on('change', ".export_report", function() {
            var data = {};
            data['priority'] = $('.prioritywise_filter').val();
            data['status'] = $('.statuswise_filter').val();
            data['d_type'] = $(this).val();
            file_download(data);
            return false;
        });

        function file_download(data) {
            var type = data.d_type;
            var status = data.status;
            var priority = data.priority;
            if (type == 'excel') {
                window.location.href = "<?php echo base_url() . 'user/task_excel?status="+status+"&priority="+priority+"' ?>";
            } else if (type == 'pdf') {
                window.location.href = "<?php echo base_url() . 'user/task_pdf?status="+status+"&priority="+priority+"' ?>";
            } else if (type == 'html') {
                window.open(
                    "<?php echo base_url() . 'user/task_html?status="+status+"&priority="+priority+"' ?>",
                    '_blank' // <- This is what makes it open in a new window.
                );                
            }
        }

        function get_result(data) {
            $(".LoadingImage").show();
            var pri = data.priority;
            var sta = data.status;            
            var ns  = '';
            var ip  = '';
            var af  = '';
            var ts  = '';
            var cm  = '';

            if (sta == 'notstarted') {
                ns = 'selected="selected"';
            }
            if (sta == 'inprogress') {
                ip = 'selected="selected"';
            }
            if (sta == 'awaiting') {
                af = 'selected="selected"';
            }
            if (sta == 'testing') {
                ts = 'selected="selected"';
            }
            if (sta == 'complete') {
                cm = 'selected="selected"';
            }
            var lw = '';
            var md = '';
            var hi = '';
            var su = '';

            if (pri == 'low') {
                lw = 'selected="selected"';
            }
            if (pri == 'medium') {
                md = 'selected="selected"';
            }
            if (pri == 'high') {
                hi = 'selected="selected"';
            }
            if (pri == 'super_urgent') {
                su = 'selected="selected"';
            }

            $.ajax({
                url: '<?php echo base_url(); ?>user/filter_task/',
                type: "POST",
                data: data,
                success: function(data) {
                    $(".LoadingImage").hide();
                    $(".all-usera1").html(data);

                    var tabletask1 = $("#alltask").dataTable({
                        "iDisplayLength": 10,
                        "scrollX": true,
                        "dom": '<"toolbar-table">lfrtip',
                        responsive: true
                    });
                    $("div.toolbar-table").html('<div class="filter-task1"><h2>Filter:</h2><ul><li><img src="<?php echo  base_url() ?>assets/images/by-date2.png" alt="data"><select id="statuswise_filter" class="statuswise_filter"><option value="">By Status</option> <option value="notstarted" ' + ns + ' >Not started</option><option value="inprogress" ' + ip + ' >In Progress</option><option value="awaiting"' + af + '>Awaiting Feedback</option><option value="testing" ' + ts + '>Testing</option><option value="complete" ' + cm + '>Complete</option></select></li><li><img src="<?php echo  base_url() ?>assets/images/by-date3.png" alt="data"><select id="prioritywise_filter" class="prioritywise_filter"><option value="">By Priority</option><option value="low" ' + lw + '>Low</option><option value="medium" ' + md + '>Medium</option><option value="high" ' + hi + '>High</option><option value="super_urgent" ' + su + '>Super Urgent</option></select></li><li><select id="export_report" class="export_report"><option value="">Export</option><option value="excel">Excel</option><option value="pdf">PDF</option><option value="html">HTML</option></select></li></ul></div>');
                }
            });
        }
    });

    $(document).on("click", ".assignuser_staff", function() {
        var id = $(this).attr("data-id");
        var data = {};
        $('.assign_staff_div').html('');

        data['task_id'] = id;
        data['assignuser_staff'] = $('#assignuser_staff_' + id).val();
        data['assignuser_staffid'] = $('#assignuser_staffid_' + id).val();
        data['type'] = $('#update_staffid_' + id).val();

        if ($('#assignuser_staff_' + id).val() != '' && data['type'] != '3') {
            $(".LoadingImage").show();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>user/assign_status_change/",
                data: data,
                success: function(response) {
                    $('#assignuser_' + id + ' .close').trigger('click');

                    $('#adduser_' + id + ' #assign_task_status_' + id).html('').html(response);
                    $(".LoadingImage").hide();
                },
            });
        } else if (data['type'] == '3') {
            $(".LoadingImage").show();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>user/assign_status_change/",
                data: data,
                success: function(response) {
                    $('#assignuser_' + id + ' .close').trigger('click');

                    $('#adduser_' + id + ' #assign_task_status_' + id).html('').html(response);
                    $(".LoadingImage").hide();
                },
            });
        } else {
            $('.assign_staff_div').html('Enter Status');
        }
    });

    $(".assignuser_change").click(function() {
        $('.assign_staff_div').html('');

        var id = $(this).attr("data-id");

        $('#assignuser_staff_' + id).val('');
        $('#assignuser_staffid_' + id).val('');
        $('#update_staffid_' + id).val('');
        $('#assignuser_' + id + ' h4').text('Reason Status');
        $('.delete_st_reason').css('display', 'block');
        $('.delete_div_reason').css('display', 'none');
        $('#update_staffid_' + id).val('1');
    });

    $(document).on("click", ".edit_user_change", function() {
        $('.assign_staff_div').html('');

        var id = $(this).attr("data-id");
        var data = {};

        $('#assignuser_staff_' + id).val('');
        $('#assignuser_staffid_' + id).val('');
        $('#update_staffid_' + id).val('');

        var data_status = $('#assign_task_status_' + id).val();
        $('#assignuser_' + id + ' h4').text('Reason Status');
        $('.delete_st_reason').css('display', 'block');
        $('.delete_div_reason').css('display', 'none');
        var data_text = $('#assign_task_status_' + id + ' option:selected').text();

        if (data_status != '') {
            $('#assignuser_staff_' + id).val(data_text);
            $('#assignuser_staffid_' + id).val(data_status);
            $('#update_staffid_' + id).val('2');
        }
    });
    $(document).on("change", ".assign_task_status", function() {
        var id = $(this).attr('id');
        id = id.split('_');
        var val = $(this).val();
        if (val != "") {
            $('#edit_user_change_' + id[3]).css('display', 'block');
            $('#delete_user_change_' + id[3]).css('display', 'block');
            $(this).closest('.formconnew').addClass('add_edit_class');
        } else {
            $('#edit_user_change_' + id[3]).css('display', 'none');
            $('#delete_user_change_' + id[3]).css('display', 'none');
            $(this).closest('.formconnew').removeClass('add_edit_class');
        }
    });

    $(document).on("click", ".delete_user_change", function() {
        $('.assign_staff_div').html('');

        var id = $(this).attr("data-id");

        $('#assignuser_staffid_' + id).val('');
        $('#update_staffid_' + id).val('');

        var data = {};
        var data_status = $('#assign_task_status_' + id).val();
        $('.delete_st_reason').css('display', 'none');
        $('.delete_div_reason').css('display', 'block');
        $('#assignuser_' + id + ' h4').text('Do you Want Delete the status');
        var data_text = $('#assign_task_status_' + id + ' option:selected').text();

        if (data_status != '') {
            $('#assignuser_staffid_' + id).val(data_status);
            $('#update_staffid_' + id).val('3');
        }
    });  

    $(".save_assign_staff").click(function() {
        var id = $(this).attr("data-id");
        var data = {};       
        data['task_id'] = id;
        var Assignees = [];
        data['assign_task_status'] = $('#assign_task_status').val();
        data['description'] = $('#description').val();
        $('.comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
            var id = $(this).closest('.comboTreeItemTitle').attr('data-id');
            var id = id.split('_');
            Assignees.push(id[0]);
        });
        
        var assign_role = Assignees.join(',');
        $('#assign_role').val(assign_role);
        data['assign_role'] = assign_role;

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>user/update_assignees/",
            data: data,
            success: function(response) {
                $('.alert-success-assignee').show();
                setTimeout(function() {
                    $('.alert-success-assignee').hide();
                }, 1000);
                $('#task_' + id).html(response);
                $('.for_user_assign').load('<?php echo base_url(); ?>user/task_details_assigne_get/' + id);
            },
        });
    });

    $(function() {
        var hours = minutes = seconds = milliseconds = 0;
        var prev_hours = prev_minutes = prev_seconds = prev_milliseconds = undefined;
        var timeUpdate;

        // Start/Pause/Resume button onClick
        $("#start_pause_resume").button().click(function() {
            // Start button
            if ($(this).text() == "Start") { // check button label
                $(this).html("<span class='ui-button-text'>Pause</span>");
                updateTime(0, 0, 0, 0);
            }
            // Pause button
            else if ($(this).text() == "Pause") {
                clearInterval(timeUpdate);
                $(this).html("<span class='ui-button-text'>Resume</span>");
            }
            // Resume button    
            else if ($(this).text() == "Resume") {
                prev_hours = parseInt($("#hours").html());
                prev_minutes = parseInt($("#minutes").html());
                prev_seconds = parseInt($("#seconds").html());
                prev_milliseconds = parseInt($("#milliseconds").html());

                updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds);

                $(this).html("<span class='ui-button-text'>Pause</span>");
            }
        });

        // Reset button onClick
        $("#reset").button().click(function() {
            if (timeUpdate) clearInterval(timeUpdate);
            setStopwatch(0, 0, 0, 0);
            $("#start_pause_resume").html("<span class='ui-button-text'>Start</span>");
        });

        // Update time in stopwatch periodically - every 25ms
        function updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds) {
            var startTime = new Date(); // fetch current time

            timeUpdate = setInterval(function() {
                var timeElapsed = new Date().getTime() - startTime.getTime(); // calculate the time elapsed in milliseconds

                // calculate hours                
                hours = parseInt(timeElapsed / 1000 / 60 / 60) + prev_hours;

                // calculate minutes
                minutes = parseInt(timeElapsed / 1000 / 60) + prev_minutes;
                if (minutes > 60) minutes %= 60;

                // calculate seconds
                seconds = parseInt(timeElapsed / 1000) + prev_seconds;
                if (seconds > 60) seconds %= 60;

                // calculate milliseconds 
                milliseconds = timeElapsed + prev_milliseconds;
                if (milliseconds > 1000) milliseconds %= 1000;

                // set the stopwatch
                setStopwatch(hours, minutes, seconds, milliseconds);

            }, 25); // update time in stopwatch after every 25ms
        }

        // Set the time in stopwatch
        function setStopwatch(hours, minutes, seconds, milliseconds) {
            $("#hours").html(prependZero(hours, 2));
            $("#minutes").html(prependZero(minutes, 2));
            $("#seconds").html(prependZero(seconds, 2));
            $("#milliseconds").html(prependZero(milliseconds, 3));
        }

        // Prepend zeros to the digits in stopwatch
        function prependZero(time, length) {
            time = new String(time); // stringify time
            return new Array(Math.max(length - time.length + 1, 0)).join("0") + time;
        }
    });

    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    // Set the date we're counting down to
    var countDownDate = new Date("Sep 4, 2017 15:37:25").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {
        // Get todays date and time
        var now = new Date().getTime();
        // Find the distance between now an the count down date
        var distance = countDownDate - now;
        //alert(distance);
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        // Output the result in an element with id="demo"
        //document.getElementById("demo").innerHTML = days + "d " + hours + "h "
        var result = humanise(days) + hours + "h " +
            minutes + "m " + seconds + "s ";
        // console.log(result);
        $(".demos").html(result);

        // If the count down is over, write some text 
        if (distance < 0) {
            $(".demos").html('EXPIRED');
        }
    }, 1000);
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.workout-timer.css" />
<!-- Example-page styles -->
<style>
    body {
        margin-bottom: 100px;
        background-color: #ECF0F1;
    }

    .event-log {
        width: 100%;
        margin: 20px 0;
    }

    .code-example {
        margin: 20px 0;
    }
</style>

<script>
    // Init timers
    $(document).ready(function() {
        // alert('test');
        $('.workout-timer').workoutTimer();
    })
    $(document).on('click', '.paginate_button', function() {
        $('.workout-timer').workoutTimer();

    });
    $(document).on('click', '.sorting_1', function() {
        $('.workout-timer').workoutTimer();

    });
    $(document).on('click', '.workout-timer__play-pause', function() {
        //$('.workout-timer').workoutTimer();
        var id = $(this).attr("data-id");
        var txt = $("#counter_" + id).html();
        var data = {};
        data['id'] = id;
        data['time'] = txt;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>user/update_timer/",
            data: data,
            success: function(response) {
                // $('#task_'+id).html(response);
            },
        });
    });
</script>

<script>
    // Syntax highlighting
    $('pre code').each(function(i, block) {
        hljs.highlightBlock(block);
    });
</script>

<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.dropdown-display').click(function() {
            $('.dropdown-main').slideToggle(300);
        })
    })
</script>
<!-- Line through Sub Task -->
<script>
    $(document).ready(function() {
        $('.subTaskcheckbox').click(function() {
            $(this).parent().parent().toggleClass("sub-task-cancel");
        });
        $('.subTaskcheckbox:checked').each(function() {
            $(this).parent().parent().toggleClass("sub-task-cancel");
        });
    });
</script>
<!-- line through Sub Task ends -->

<script type="text/javascript">
    var count_tinymce = 0;

    function newPostReply(commentId) {
        var html = '<div class="card-block" ><div id="task_details"></div><div class="md-float-material d-flex"><div class="col-md-12 btn-add-task"><div class="input-group input-group-button"><textarea rows="5" type="text" id="add_reply-' + count_tinymce + '" class="form-control add_comments add_replies" placeholder="Add Comments"></textarea><!-- <span class="input-group-addon btn btn-primary" id="basic-addon1"><i class="icofont icofont-plus f-w-600"></i> Add Comments</span> --><button type="submit" class="input-group-addon btn btn-primary comments_btn comment_reply_btn taskclsbutton" id="basic-addon1" style="height: 40px; display: block;" count_tinymce="' + count_tinymce + '" data-dismiss="modal"><i class="icofont icofont-plus f-w-600"></i> Reply</button><input type="hidden" class="task_id" id="reply_task_id" value="<?php echo $task_ID; ?>"><input type="hidden" name="comment_id" class="commentId" id="replycommentId" ><input type="hidden" class="us_ID replyus_ID" value="<?php echo $user_details['id']; ?>" ><input type="hidden" class="us_NAME replyus_NAME" value="<?php echo $user_details['username']; ?>" ></div></div></div></div>';
        $('#rd_modal_html').html(html);

        $('#replyModal').modal('show');

        tinymce_config['images_upload_url'] = "<?php echo base_url(); ?>Firm/uplode_mail_header_image";
        tinymce_config['selector'] = '#add_reply-' + count_tinymce;
        tinymce.init(tinymce_config);
        count_tinymce++;
        $('#replycommentId').val(commentId);
        $('#add_replies').focus();
    }

    function deleteComment(commentId) {
        console.log(commentId, "commentID")
        var html = '<div><p>Do you really want to delete this comment ?</p></div>';
        $('#delete_modal_html').html(html);
        $("#deleteModal").modal('show');
    }

    function postReply(commentId) {
        // alert(commentId)
        var html = '<div class="card-block" ><div id="task_details"></div><div class="md-float-material d-flex"><div class="col-md-12 btn-add-task"><div class="input-group input-group-button"><textarea rows="5" type="text" id="add_reply-' + count_tinymce + '" class="form-control add_comments add_replies" placeholder="Add Comments"></textarea><!-- <span class="input-group-addon btn btn-primary" id="basic-addon1"><i class="icofont icofont-plus f-w-600"></i> Add Comments</span> --><button type="submit" class="input-group-addon btn btn-primary comments_btn comment_reply_btn taskclsbutton" id="basic-addon1" style="height: 40px;" count_tinymce="' + count_tinymce + '"><i class="icofont icofont-plus f-w-600"></i> Reply</button><input type="hidden" class="task_id" id="reply_task_id" value="<?php echo $task_ID; ?>"><input type="hidden" name="comment_id" class="commentId" id="replycommentId" ><input type="hidden" class="us_ID replyus_ID" value="<?php echo $user_details['id']; ?>" ><input type="hidden" class="us_NAME replyus_NAME" value="<?php echo $user_details['username']; ?>" ></div></div></div></div>'
        $('#reply-span-' + commentId).html(html);
        tinymce_config['images_upload_url'] = "<?php echo base_url(); ?>Firm/uplode_mail_header_image";
        tinymce_config['selector'] = '#add_reply-' + count_tinymce;
        tinymce.init(tinymce_config);
        count_tinymce++;
        $('#replycommentId').val(commentId);
        $('#add_replies').focus();
    }

    function editPost(c) {
        // var aa = $(this).data(c);
        $(c).closest('.reply_div').find('.cmt-msg').hide();
        $(c).closest('.reply_div').find('.edit_msg').show();
        $(c).closest('.action-div').find('.reply-span').hide();
        $(c).closest('.action-div').find('.edit-span').hide();
        $(c).closest('.action-div').find('.save-span').show();
    }

    function savePost(cmt_id, d) {
        var primary_id = $(d).closest('.reply_div').find('.primary_id').val();
        var megs = $(d).closest('.reply_div').find('.edit_msg').val();
        
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'Tasksummary/task_UpdateComments'; ?>",
            data: {
                "primary_id": primary_id,
                "megs": megs,
            },
            beforeSend: function() {
                $(".LoadingImage").show();
            },
            success: function(res) {
                $(".LoadingImage").hide();
                var task__id = '<?php echo $this->uri->segment(3); ?>';
                console.log(task_id);
                listComment(task__id);
            }
        });
    }

    $('.comments_btn').on('click', function() {
        var t_id = $('.task_id').val();
        var cmt_val = tinyMCE.get('add_comments').getContent();
        var Ses_ID = $('.Ses_ID').val();
        var Ses_name = $('.Ses_name').val();
        var cmt_id = $('#commentId').val();
        
        if (cmt_val != '') {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() . 'Tasksummary/task_AddComments'; ?>",
                data: {
                    "t_id": t_id,
                    "cmt_id": cmt_id,
                    "cmt_val": cmt_val,
                    "Ses_ID": Ses_ID,
                    "Ses_name": Ses_name
                },
                beforeSend: function() {
                    $(".LoadingImage").show();
                },
                success: function(response) {
                    $(".LoadingImage").hide();  
                    $('.cmt_class').html('');
                    $('.cmt_class').append(response);

                    listComment(t_id);                    
                    $('#commentId').val("");
                    $('#add_comments').val("");
                    tinyMCE.activeEditor.setContent('');
                    commentId();                    
                }
            });

        } else {
            $('#add_comments').css('border-color', 'red');
        }
    });

    $(document).on('keyup', '#add_comments', function() {
        $('#add_comments').css('border-color', '#cccccc');
    });

    $(document).ready(function() {
        var t_id = $('.task_id').val();
        listComment(t_id);
    });

    function listComment(task_id) {        
        $.post("<?php echo base_url() . 'Tasksummary/Comment_list'; ?>/" + task_id,
            function(data) {
                var data = JSON.parse(data);
                var comments = "";
                var replies = "";
                var item = "";
                var parent = -1;
                var results = new Array();
                var list = $("<ul class='outer-comment'>");
                var user_id = "<?php echo $_SESSION['id'] ?>";
                var item = $("<li>").html(comments);
                var myarray = [];
                for (var i = 0;
                    (i < data.length); i++) {
                    if (data[i]['parent_comment_id'] != '0') {
                        myarray.push(data[i]['parent_comment_id']);
                    }
                }

                for (var i = 0;
                    (i < data.length); i++) {
                    var commentId = data[i]['id'];
                    parent = data[i]['parent_comment_id'];
                    var owner_id = data[i]['user_id'];
                    
                    if (jQuery.inArray(commentId, myarray) != '-1') {
                        var classes = 'contain_child';
                    } else {
                        var classes = '++';
                    }
                    
                    if (parent == '0') {                        
                        /** 03-07-2018 **/
                        var newD;
                        var zz = data[i]['created_at'];
                        var today = new Date(zz * 1000);
                        var for_date = dateFormat(today, "dd-mm-yyyy hh:MM tt ");
                        /** end of 03-07-2018 **/
                        var edit_per = '<?php echo $_SESSION['permission']['Task']['edit'] ?>';
                        comments = "<div class='media mt-2 reply_div edit_post-msg1 " + classes + "' id='reply_div' data-id=" + commentId + ">" +
                            "<a class='media-left' href='#'>" +
                            "<img class='media-object img-radius comment-img' src='<?php echo  base_url() ?>assets/images/avatar-2.jpg' alt='Generic placeholder image'></a>" +
                            "<div class='media-body'>" +
                            "<h6 class='media-heading txt-primary user_title'>" + "<span class='text-comment-wrap'>" + data[i]['name'] + "</span>" + "<span class='f-12 text-muted m-l-5'>" + for_date + "</span></h6>" +
                            "<input type='text' class='primary_id' id='primary_id' value='" + data[i]['id'] + "' style='display:none;'>" +
                            "<textarea type='text' class='edit_msg' id='edit_msg' style='display:none;'>" + data[i]['message'] + "</textarea>" +
                            "<span class='cmt-msg'>" + data[i]['message'] + " </span>" +
                            "<div class='m-t-10 m-b-25 action-div'>" +
                            "<span class='f-14 save-span' style='display:none'><a href='javascript:;' class='m-r-10 f-12 btn btn-sm btn-primary' onClick='savePost(" + commentId + ", this)' >Save</a></span>";

                        if (user_id != owner_id) {
                            comments += "<span class='edit-span'><a href='javascript:;' class='m-r-10 f-12 editPost' onClick='editPost(this)' >Edit</a> </span>";
                            comments += "<span class='reply-span' id='reply-span-" + commentId + "'><a href='javascript:;' class='m-r-10 f-12' onClick='newPostReply(" + commentId + ")'>Reply</a></span>";
                        }
                        if (user_id == owner_id && edit_per == 1) {
                            comments += "<span class='edit-span'><a href='javascript:;' class='m-r-10 f-12 editPost' onClick='editPost(this)' >Edit</a> </span>";
                        }
                        comments +=
                            "</div><hr></div></div>";

                        var item = $("<li>").html(comments);
                        list.append(item);
                        var reply_list = $('<ul class="sub_commands" >');
                        item.append(reply_list);
                        listReplies(commentId, data, reply_list);
                    } 
                }
                $('#task_details').html(list);
            });
    }

    function listReplies(commentId, data, list) {
        var edit_per = '<?php echo $_SESSION['permission']['Task']['edit'] ?>';
        var user_id = "<?php echo $_SESSION['id'] ?>";
        var myarray = [];
        for (var i = 0;
            (i < data.length); i++) {
            if (data[i]['parent_comment_id'] != '0') {
                myarray.push(data[i]['parent_comment_id']);
            }
        }

        for (var i = 0;
            (i < data.length); i++) {

            var replyId = data[i]['id'];
            var owner_id = data[i]['user_id'];

            if (jQuery.inArray(replyId, myarray) != '-1') {
                var classes = 'contain_child';
            } else {
                var classes = '**';
            }

            if (commentId == data[i]['parent_comment_id']) {
                /** 03-07-2018 **/
                var newD;
                var zz = data[i]['created_at'];
                var today = new Date(zz * 1000);
                var for_date = dateFormat(today, "dd-mm-yyyy hh:MM tt ");
                /** end of 03-07-2018 **/
                comments = "<div class='media mt-2 reply_div edit_post-msg2 " + classes + " ' id='reply_div' data-id=" + replyId + ">" +
                    "<a class='media-left' href='#''>" +
                    "<img class='media-object img-radius comment-img' src='<?php echo  base_url() ?>assets/images/avatar-2.jpg' alt='Generic placeholder image'></a>" +
                    "<div class='media-body'>" +
                    "<h6 class='media-heading txt-primary user_title'>" + "<span class='text-comment-wrap'>" + data[i]['name'] + "</span>" + "<span class='f-12 text-muted m-l-5'>" + for_date + "</span></h6>" +
                    "<input type='text' class='primary_id' id='primary_id' value='" + data[i]['id'] + "' style='display:none;'>" +
                    "<textarea type='text' class='edit_msg' id='edit_msg' style='display:none;'>" + data[i]['message'] + "</textarea>" +
                    "<span class='cmt-msg'>" + data[i]['message'] + " </span>" +
                    "<div class='m-t-10 m-b-25 action-div'>" +
                    "<span class='f-14 save-span' style='display:none'><a href='javascript:;' class='m-r-10 f-12 btn btn-sm btn-primary' onClick='savePost(" + commentId + ", this)' >Save</a></span>";

                if (user_id != owner_id) {
                    comments += "<span class='edit-span'><a href='javascript:;' class='m-r-10 f-12 editPost' onClick='editPost(this)' >Edit</a> </span>";
                    comments += "<span class='reply-span' id='reply-span-" + replyId + "'><a href='javascript:;' class='m-r-10 f-12' onClick='postReply(" + replyId + ")' >Reply</a></span>";
                }
                if (user_id == owner_id && edit_per == 1) {
                    comments += "<span class='edit-span'><a href='javascript:;' class='m-r-10 f-12 editPost' onClick='editPost(this)' >Edit</a> </span>";
                }
                comments += "</div><hr></div></div>";

                var item = $("<li style='padding-left: 55px;'>").html(comments);
                var reply_list = $('<ul>');
                list.append(item);
                item.append(reply_list);
                listReplies(data[i].id, data, reply_list);
            }
        }
    }

    $(document).ready(function() {
        $('.status_task').on('change', function() {
            var selectValue = $(this).find(":selected").val();
            var task_id = $('.task_id').val();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url() . 'user/task_statusChange'; ?>",
                data: {
                    "status": selectValue,
                    "rec_id": task_id
                },
                beforeSend: function() {
                    $(".LoadingImage").show();
                },
                success: function(data) {
                    $(".LoadingImage").hide();
                    $(".alert-success-check").show();
                    $('.alert-success-check .position-alert1').html('').html('Success !!!  Task status have been changed successfully...');
                    setTimeout(function() {
                        location.reload();
                    }, 500);
                }
            });
        });

        $(document).on('change', '.progress_status', function(e) {
            var rec_id = $(this).data('id');
            var data1 = {};
            var stat = $.trim($(this).val());
            data1['rec_id'] = rec_id;
            data1['status'] = stat;
            var that = $(this);
            if (stat == '') return;

            $.ajax({
                url: '<?php echo base_url(); ?>user/progress_statusChange/',
                type: 'post',
                data: data1,
                beforeSend: function() {
                    $(".LoadingImage").show();
                },
                success: function(data) {
                    $(".LoadingImage").hide();
                    $(".alert-success-check").show();
                    $('.alert-success-check .position-alert1').html('').html('Success !!! Progress  status have been changed successfully...');
                    
                    setTimeout(function() {
                        $(".alert-success-check").hide();
                    }, 1500);
                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        function Update_TaskSettings() {
            var formdata = new FormData($('#addtasksetting')[0]);

            $.ajax({
                url: "<?php echo base_url() ?>tasksummary/addTaskSetting",
                type: 'POST',
                data: formdata,
                processData: false,
                contentType: false,
                success: function(data) {
                    if (parseInt(data)) {
                        $('.info-popup .pop-realted1').html('Settings Update Successfully!..');
                        $('.info-popup').show();
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                }
            });
        }

        $("#addtasksetting").submit(function() {
            $('#Confirmation_popup').modal('show');
            $('#Confirmation_popup .information').html('Are you sure you Want To Update ?');
            $('.Confirmation_OkButton').unbind().click(Update_TaskSettings);
            $('.Confirmation_OkButton').html('Save');
            return false; //don't remove 
        });
    });

    function clear_form_elements1(ele) {
        $(ele).find(':input').each(function() {
            switch (this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                case 'text':
                case 'textarea':
                    $(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
            }
        });

        $('.dropdown-chose-list').each(function() {
            $(this).html('<span>Nothing Selected</span>'); // this is correct but with validation
        });
        $(".selectDate").css('display', 'block');
    }

    function clear_form_elements(ele) {

        $('.for_switchery').each(function() {            
            if ($(this).is(':checked')) {
                $(this).trigger("click");
            }
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function() { 
        <?php $ct = explode(':', $curr_timer); ?>

        localStorage.setItem('hr', <?php echo isset($ct[0]) ? $ct[0] : '00'; ?>);
        localStorage.setItem('min', <?php echo isset($ct[1]) ? $ct[1] : '00'; ?>);
        localStorage.setItem('sec', <?php echo isset($ct[2]) ? $ct[2] : '00'; ?>);
        
        if('<?php echo $curr_timer; ?>' != '00:00:00'){
            $("#start_button").hide();
            $('#timer_submit').show();
        }else{
            $("#start_button").show();
            $('#timer_submit').hide();
        }

        var vars = {};
        <?php
        foreach ($task_form as $tk_key => $tk_value) { ?>            
            vars['hours_' + <?php echo $tk_value['id']; ?>] = $('#trhours_<?php echo $tk_value['id'] ?>').val();
            vars['minutes_' + <?php echo $tk_value['id']; ?>] = $('#trmin_<?php echo $tk_value['id'] ?>').val();
            vars['seconds_' + <?php echo $tk_value['id']; ?>] = $('#trsec_<?php echo $tk_value['id'] ?>').val();
            vars['milliseconds_' + <?php echo $tk_value['id']; ?>] = '0';
            vars['data_pause_' + <?php echo $tk_value['id']; ?>] = $('#trpause_<?php echo $tk_value['id'] ?>').val(); <?php
        }        
        $task_val = (isset($task_form[0]['id'])) ? $task_form[0]['id'] : '';
        $explode_worker = Get_Module_Assigees('TASK',  $task_val);
        /** for task worker **/
        if (count($explode_worker) > 0) {
            foreach ($explode_worker as $s_key => $s_val) { ?>
                vars['hours_' + <?php echo $s_val; ?>] = $('#trhours_<?php echo $s_val; ?>').val();
                vars['minutes_' + <?php echo $s_val; ?>] = $('#trmin_<?php echo $s_val; ?>').val();
                vars['seconds_' + <?php echo $s_val; ?>] = $('#trsec_<?php echo $s_val; ?>').val();
                vars['milliseconds_' + <?php echo $s_val; ?>] = '0';
                vars['data_pause_' + <?php echo $s_val; ?>] = $('#trpause_<?php echo $s_val; ?>').val(); <?php
            }
        }
        /** 09-08-2018 for wrker **/
        ?>
        $('.stopwatch').each(function() {    
            // Cache very important elements, especially the ones used always
            var element = $(this);
            var running = element.data('autostart');
            var for_date = element.data('date');
            var task_id = element.data('id');
            var data_pause = element.data('pauseon');
            var data_pause = vars['data_pause_' + task_id];
            var hours = element.data('hour');
            var minutes = element.data('min');
            var seconds = element.data('sec');
            var milliseconds = element.data('mili');

            if (hours == '0') {
                var hours = vars['hours_' + task_id];
            }
            if (minutes == '0') {
                var minutes = vars['minutes_' + task_id];
            }
            if (seconds == '0') {
                var seconds = vars['seconds_' + task_id];

            }
            if (milliseconds == '0') {
                var milliseconds = vars['milliseconds_' + task_id];
            }
            var start = element.data('start');
            var end = element.data('end');
            var hoursElement = element.find('.hours');
            var minutesElement = element.find('.minutes');
            var secondsElement = element.find('.seconds');
            var millisecondsElement = element.find('.milliseconds');
            var toggleElement = element.find('.toggle');
            var resetElement = element.find('.reset');
            var pauseText = toggleElement.data('pausetext');
            var resumeText = toggleElement.data('resumetext');
            var startText = toggleElement.text();

            // And it's better to keep the state of time in variables 
            // than parsing them from the html.
            //var hours, minutes, seconds, milliseconds, timer;
            var timer;

            function prependZero(time, length) {                
                // Quick way to turn number to string is to prepend it with a string
                // Also, a quick way to turn floats to integers is to complement with 0
                time = '' + (time | 0);
                // And strings have length too. Prepend 0 until right.
                while (time.length < length) time = '0' + time;
                return time;
            }

            function setStopwatch(hours, minutes, seconds, milliseconds) {
                // Using text(). html() will construct HTML when it finds one, overhead.
                hoursElement.text(prependZero(hours, 2));
                minutesElement.text(prependZero(minutes, 2));
                secondsElement.text(prependZero(seconds, 2));
                millisecondsElement.text(prependZero(milliseconds, 3));
            }

            // Update time in stopwatch periodically - every 25ms
            function runTimer() {
                // Using ES5 Date.now() to get current timestamp            
                var startTime = Date.now();                
                if (hours != 0 && minutes != 0 && seconds != 0 && milliseconds != 0) {
                    var startTime = Date.now();
                } else {                    
                    var startTime = Date.now();
                }
                var prevHours = hours;
                var prevMinutes = minutes;
                var prevSeconds = seconds;
                var prevMilliseconds = milliseconds;
                timer = setInterval(function() {
                    // var timeElapsed = Date.now() - startTime;
                    var timeElapsed = Date.now() - startTime;
                    hours = (timeElapsed / 3600000) + prevHours;
                    minutes = ((timeElapsed / 60000) + prevMinutes) % 60;
                    seconds = ((timeElapsed / 1000) + prevSeconds) % 60;
                    milliseconds = (timeElapsed + prevMilliseconds) % 1000;
                    element.attr('data-hour', hours);
                    element.attr('data-min', minutes);
                    element.attr('data-sec', seconds);
                    element.attr('data-mili', milliseconds);
                    element.attr('data-pauseon', '');
                    vars['hours_' + task_id] = hours;
                    vars['minutes_' + task_id] = minutes;
                    vars['seconds_' + task_id] = seconds;
                    vars['milliseconds_' + task_id] = milliseconds;
                    vars['data_pause_' + task_id] = '';                    

                    setStopwatch(hours, minutes, seconds, milliseconds);  
                }, 25);
            }

            // Split out timer functions into functions.
            // Easier to read and write down responsibilities
            function run() {
                $(this).attr('pause', '');
                running = true;
                runTimer();
                
                toggleElement.text(pauseText);
                toggleElement.attr('data-current', pauseText);
            }

            function pause() {
                running = false;
                clearTimeout(timer);
                toggleElement.text(resumeText);
                toggleElement.attr('data-current', resumeText);
                element.attr('data-pauseon', 'on');                
                vars['data_pause_' + task_id] = 'on';
                
                $.ajax({
                    url: '<?php echo base_url(); ?>user/task_countdown_update/',
                    type: 'post',
                    data: {
                        'task_id': task_id,
                        'hours': hours,
                        'minutes': minutes,
                        'seconds': seconds,
                        'milliseconds': milliseconds,
                        'pause': 'on'
                    },
                    timeout: 3000,
                    success: function(data) {

                    }
                });
            }

            function reset() {
                running = false;
                pause();
                hours = minutes = seconds = milliseconds = 0;
                setStopwatch(hours, minutes, seconds, milliseconds);
                toggleElement.text(startText);
                toggleElement.attr('data-current', startText);
            }

            //  And button handlers merely call out the responsibilities
            toggleElement.on('click', function() {
                (running) ? pause(): run();
            });

            resetElement.on('click', function() {
                reset();
            });

            demo();

            function demo() {
                running = false;
                element.attr('data-hour', hours);
                element.attr('data-min', minutes);
                element.attr('data-sec', seconds);
                element.attr('data-mili', milliseconds);
                vars['hours_' + task_id] = hours;
                vars['minutes_' + task_id] = minutes;
                vars['seconds_' + task_id] = seconds;
                vars['milliseconds_' + task_id] = milliseconds;

                if (hours != 0 || minutes != 0 || seconds != 0) {
                    if (data_pause == '') {
                        run();
                    }
                }
                setStopwatch(hours, minutes, seconds, milliseconds);
            }
        });
    });
    /*$(document).on('click', '.for_timer_start_pause', function() {
        var task_id = $(this).attr('id');
        
        $.ajax({
            url: '<?php echo base_url(); ?>user/task_timer_start_pause/',
            type: 'post',
            data: {
                'task_id': task_id
            },
            timeout: 3000,
            success: function(data) {
                
            }
        });
        $.ajax({
            url: '<?php echo base_url(); ?>user/task_timer_start_pause_individual/',
            type: 'post',
            data: {
                'task_id': task_id
            },
            timeout: 3000,
            success: function(data) {
                
            }
        });

    });*/
    /** for timer add and updtae time **/
    $(document).on('click', '.remove_file', function(e) {
        $('#image_name').val('');
        var id = $(this).attr('data-id');
        $('.for_img_' + id).remove();

        var file_name = $(this).data('name');
        var task_id = $(this).data('task_id');
        $.ajax({
            url: '<?php echo base_url(); ?>Tasksummary/delete_fileupload/',
            type: 'post',
            data: {
                'task_id': task_id,
                'file_name': file_name,
            },

            success: function(data) {
                location.reload();
            }
        });
    });

    function alltask_delete(id) {
        $('#delete_alltask' + id).show();
        return false;
    }
    $(document).on('click', '#close', function(e) {
        $('.alert-success').hide();
        return false;
    });
</script>
<script type="text/javascript">
    function clear_check_box_data() {
        $(".selectuser").each(function() {
            $(this).prop("checked", false);
        });
    }
    $(document).ready(function() {
        $('.datepicker').datetimepicker({
            format: 'dd/mm/yyyy'
        });
    });

    $('#search-criteria').keyup(function() {
        var txt = $('#search-criteria').val();
        $(".cd-timeline-block").hide();
        $('.search_word').each(function() {
            if ($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1) {
                $(this).show();
                $(this).parents('.cd-timeline-block').show();
            }
        });
    });

    $(".Send-invoice").click(function() {
        var id = $(this).attr('id');
        window.open('<?php echo base_url(); ?>/invoice/EditInvoice/' + id, '_blank');
    });
</script>
<script src="<?php echo base_url(); ?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url(); ?>assets/tree_select/icontains.js"></script>
<script type="text/javascript">
    var tree_select = <?php echo json_encode(GetAssignees_SelectBox()); ?>;
    $('.tree_select').comboTree({
        source: tree_select,
        isMultiple: true
    });
    var arr = <?php echo json_encode($assign_data); ?>;

    $('.comboTreeItemTitle').click(function() {
        var id = $(this).attr('data-id');
        var id = id.split('_');
        $('.comboTreeItemTitle').each(function() {
            var id1 = $(this).attr('data-id');
            var id1 = id1.split('_');
            
            if (id[1] == id1[1]) {
                $(this).toggleClass('disabled');
            }
        });
        $(this).removeClass('disabled');
    });

    $('.comboTreeItemTitle').each(function() {
        var id1 = $(this).attr('data-id');
        var id = id1.split('_');
        if (jQuery.inArray(id[0], arr) !== -1) {
            $(this).find("input").trigger('click');
        }
    });

    $(document).on('change', ".subTaskcheckbox", function() {
        var con = $(this).is(":checked");
        var task_id = $(this).attr('data-alltask-id');
        if (con) {
            var status = 5;
        } else {
            var status = 2;
        }

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'user/task_statusChange'; ?>",
            data: {
                "status": status,
                "rec_id": task_id
            },
            beforeSend: function() {
                $(".LoadingImage").show();
            },
            success: function(data) {
                $(".LoadingImage").hide();
                $(".alert-success-check").show();
                $('.alert-success-check .position-alert1').html('').html('Success !!!  Task status have been changed successfully...');
                setTimeout(function() {
                    location.reload();
                }, 500);
            }
        });
    });

    $(document).on('click', ".comment_reply_btn", function() {
        var t_id = $('#reply_task_id').val();
        var count_tinymce = $(this).attr('count_tinymce');
        var cmt_val = tinyMCE.get('add_reply-' + count_tinymce).getContent();
        var Ses_ID = $('.Ses_ID').val();
        var Ses_name = $('.Ses_name').val();
        var cmt_id = $('#replycommentId').val();
        if (cmt_val != '') {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() . 'Tasksummary/task_AddComments'; ?>",
                data: {
                    "t_id": t_id,
                    "cmt_id": cmt_id,
                    "cmt_val": cmt_val,
                    "Ses_ID": Ses_ID,
                    "Ses_name": Ses_name
                },
                beforeSend: function() {
                    $(".LoadingImage").show();
                },
                success: function(response) {
                    $(".LoadingImage").hide();   
                    $('.cmt_class').html('');
                    $('.cmt_class').append(response);

                    listComment(t_id);                    
                    $('#replycommentId').val("");
                    $('#add_reply').val("");
                    $('#reply-span-' + cmt_id).html('<a href="javascript:;" class="m-r-10 f-12" onclick="postReply(' + cmt_id + ')">Reply</a>');
                    commentId();                    
                }
            });
        } else {
            $('#add_reply').css('border-color', 'red');
        }
    });
</script>
<script src="<?php echo base_url(); ?>assets/js/counter/counter-script.js"></script>
<!-- for new timer -->
<script type="text/javascript"> 
    $(document).on('click', '.task-detail-timer', function() {
        var task_id = <?php echo $tsk_id; ?>;
        $.ajax({
            url: '<?php echo base_url(); ?>user/task_timer_start_pause/',
            type: 'post',
            data: {
                'task_id': task_id
            },
            timeout: 3000,
            success: function(data) {
                
            }
        });
        
        $.ajax({
            url: '<?php echo base_url(); ?>user/task_timer_start_pause_individual/',
            type: 'post',
            data: {
                'task_id': task_id
            },
            timeout: 3000,
            success: function(data) {
                
            }
        });        
    });
    
</script>
<!-- end of new timer -->
