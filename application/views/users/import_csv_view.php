<?php $this->load->view('includes/header');?>
<style type="text/css">
	div.messages .alert {
		margin-top: 20px;
		display: none;
	}
</style>

<link href="<?php echo base_url();?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />

<section class="client-details-view various-section03 floating_set card-removes pcoded-content">
	<div class="client-inform-data1 floating_set">

		<div class="deadline-crm1 floating_set">
			<?php $this->load->view('clients/component/navigation.php', [ 'active' => 'import_client' ]) ?>
		</div>	

		<div class="information-tab floating_set card p-t-2 client-import">
			<h4>Upload a CSV file</h4>

			<div class="messages">
				<div class="alert alert-success">
					<strong> <span></span> Client Import Success Fully..</strong> 
		  		</div>
		  		<div class="alert alert-danger">
		  			<strong> </strong>
		  		</div>
		  	</div>

			<div class="upload-step2 upload-custom">
				<form action="" method="post" enctype="multipart/form-data" id="importFrm">
					<div class="row <?php if($_SESSION['permission']['Import_Client']['create']!='1'){ ?> permission_deined <?php } ?>">
						<div class="col-sm-4">
							<div class="client-template-file1">
								<span>Step 1. Download our clients template file.</span>
								<div class="start-dats">
									<p>Start by downloading our contacts CSV(Comma Separated Values) template file. <br>
									This file has the correct column headings we need to import your contact data.</p>
									<a href="<?= base_url() ?>uploads/csv/sampeCSVformat.csv"> <i class="fa fa-download fa-6" aria-hidden="true"></i> Download custom template file</a>
								</div>
							</div>
						</div>	
						<div class="col-sm-4">
							<div class="client-template-file1">
								<span>Step 2. Copy Data into template file</span>
								<div class="start-dats"><p>Copy your data into template file using excel or any other spreadsheet editor. <br>
								Make sure the contact data you copy matches the column headings provided in the template. <br>
								Do not change the column headings in th etemplate file.</p>
								</div>
							</div>
						</div>	
						<div class="col-sm-4">
							<div class="client-template-file1 import-file-01">
								<span>Step 3. Import the populated template file</span>
								<div class="start-dats"><strong>Select a CSV file:</strong>
								<div class="custom_upload">
									<label for="cccv"></label>
									<input type="file" name="file"  accept=".csv" id="cccv">
								</div>

								<!-- <div class="error">Please select a .csv file for Client data.</div> -->
								<div class="import-section4">
									<button type="submit" name="importSubmit"><i class="fa fa-download fa-6" aria-hidden="true"></i> Import CSV</button>
								</div>
								<!-- <button type="button"><i class="fa fa-ban fa-6" aria-hidden="true"></i> Cancel</button> -->
								</div>
							</div>	
						</div>	
					</div>	
				</form>
			</div>

			<div class="information-tab static-field1 floating_set">
				<h4>Static Fields</h4>
				<div class="upload-step1 table-responsive">		
					<table id="example3" style="width:100%;">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Mandatory</th>
								<th>Supported Format</th>
							</tr>
						</thead>
						<tfoot class="ex_data1">
							<tr>
								<th>
								</th>
							</tr>
						</tfoot>
						<tbody>
							<tr>
								<td>Company Name</td>
								<td>Yes</td>
								<td>ACCTAX</td>
							</tr>
							<tr>
								<td>Company Type</td>
								<td>Yes</td>
								<td>
									LTD <br>
									PLTD <br>
									PLC <br>
									LLP <br>
									P`ship <br>
									S.A <br>
									TT <br>
									Charity<br>
									Other
								</td>
							</tr>
							<tr>
								<td>Company Number</td>
								<td>Yes</td>
								<td>
								12431254623
								</td>
							</tr>
							<tr>
								<td>Contact Person Name</td>
								<td>Yes</td>
								<td>
								Waqas
								</td>
							</tr>
							<tr>
								<td>Main E-mail</td>
								<td>Yes</td>
								<td>admin@domain.com</td>
							</tr>
							<tr>
								<td>Address Line1</td>
								<td>No</td>
								<td>CITY</td>
							</tr>
							<tr>
								<td>Address Line2</td>
								<td>No</td>
								<td>STREET</td>
							</tr>

							<tr>
								<td>City/Town</td>
								<td>No</td>
								<td>London Park</td>
							</tr>

							<tr>
								<td>Post Code</td>
								<td>No</td>
								<td>12ACD</td>
							</tr>

							<tr>
								<td>Country</td>
								<td>No</td>
								<td>LONDAN</td>
							</tr>
								
							<tr>
								<td>Phone</td>
								<td>No</td>
								<td>9786453120</td>
							</tr>								
							<tr>
								<td>Website</td>
								<td>No</td>
								<td>https://remindoo.uk/</td>
							</tr>
							
							<tr>
								<td>Business Start Date</td>
								<td>No</td>
								<td>2018-02-23</td>
							</tr>

							<tr>
								<td>Book Start Date</td>
								<td>No</td>
								<td>2018-02-23</td>
							</tr>

							<tr>
								<td>Year End Date</td>
								<td>No</td>
								<td>2018-02-2</td>
							</tr>

							<tr>
								<td>Company Reg. No</td>
								<td>No</td>
								<td>88</td>
							</tr>

							<tr>
								<td>UTR No.</td>
								<td>No</td>
								<td>8</td>
							</tr>

							<tr>
								<td>VAT Submit Type</td>
								<td>No</td>
								<td>admin</td>
							</tr>

							<tr>
								<td>VAT Reg. No</td>
								<td>No</td>
								<td>9889</td>
							</tr>

							<tr>
								<td>VAT Reg. Date</td>
								<td>No</td>
								<td>2018-02-23</td>
							</tr>	
							<tr>
								<td> Date of Birth</td>
								<td>No</td>
								<td>2018-02-26</td>
							</tr>	

							<tr>
								<td>Insurance No.</td>
								<td>No</td>
								<td>AA123456B</td>
							</tr>	

							<tr>
								<td>Account Office Reference</td>
								<td>No</td>
								<td>AA123456B</td>
							</tr>	

							<tr>
								<td>PAYE Reference Number</td>
								<td>No</td>
								<td>AA123456B</td>
							</tr>	
						
						</tbody>
					</table>
				</div>			
			</div>	

		</div>

	</div>
</section>		

<div class="modal-alertsuccess  alert alert-success-reminder succs" id="confirm-submit" style="display: none;">
	<div class="newupdate_alert">
		<a href="#" class="close">&times;</a>
		<div class="pop-realted1">
			<div class="form-group" id="messages">
			</div>
		</div>
	</div>
</div>

<div class="modal-alertsuccess alert  alert-danger" id="confirm-submit-new" style="display: none;">
	<div class="newupdate_alert">
		<a href="#" class="close">&times;</a>
		<div class="pop-realted1">
			<div class="form-group" id="error_message"> </div>
		</div>
	</div>
</div>













	
<?php $this->load->view('includes/footer');?>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="http:/resources/demos/style.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.fileuploads.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/jquery.filer.min.js"></script>
 <script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script> 

<!--  <script type="text/javascript">

CKEDITOR.replace('editor4');
</script>
<script type="text/javascript">
	$( function() {
    $( "#datepicker39,#datepicker40,#datepicker41,#datepicker42,#datepicker43").datepicker({
    	dateFormat: "dd-mm-yy"
    });
  } );
</script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   $("#importFrm").validate({
     
          ignore: false,
   
                           rules: {
                           file: {required: true, accept: "csv"},
                           },
                           errorElement: "span" , 
                           errorClass: "field-error",                             
	                       messages: 
	                       {
	                         file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},
	                       },
                           submitHandler: function(form) {
                               var formData = new FormData($("#importFrm")[0]);
                                  
                               $(".LoadingImage").show();
   
                               $.ajax({
                                   url: '<?php echo base_url();?>client/Import_ClientFrom_Csv',
                                   /*dataType : 'json',*/
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) {                                   	
                                   	$(".LoadingImage").hide();                                       
                                       if(data > 0)
                                       {
                                       		$('div.messages .alert-danger').hide();
                                            $('div.messages .alert-success span').html( data );
                                            $('div.messages .alert-success').show();
                                            setTimeout(
                                            	function(){
                                            		window.location.href="<?php echo base_url().'user'?>";
                                            	},1000);
                                       }
                                       else if(data == 0)
                                       {	$('div.messages .alert-success').hide();                                       
                                          $('div.messages .alert-danger').html('<b>Error Occured! Wrong File Format or File Is Empty. </b>');
                                          $('div.messages .alert-danger').show();
                                       }
                                       else
                                       {    
                                       		data = JSON.parse( data );	                                   	
	                                       	var zz=1;
	                                       	var text = '';
											$.map(data, function(value, index) 
											{
												$.map(value, function(value, index){
										    		text += '<p>'+value+' in <b> Record '+zz+'</b></p>';
												});
												zz++;
											});
											$('div.messages .alert-success').hide();
											$('div.messages .alert-danger').html( text ); 
									    	$('div.messages .alert-danger').show();
                                       }
                                   },
                                   error: function() 
                                   {
	                                    $('.alert-danger').show();
	                                    $('.alert-success').hide();
                                	}
                               });                             
                           } 
                       });
   
   
   });
$(document).on('click','#confirm-submit .close , #confirm-submit-new .close',function(){	
	$(this).closest('div.alert').hide();	
});

$(function() {
//----- OPEN
$('[data-popup-open]').on('click', function(e)  {
var targeted_popup_class = jQuery(this).attr('data-popup-open');
$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
e.preventDefault();
});
//----- CLOSE
$('[data-popup-close]').on('click', function(e)  {
var targeted_popup_class = jQuery(this).attr('data-popup-close');
$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
e.preventDefault();
});
});
</script>
</script>