<?php $this->load->view('includes/header');
$succ = $this->session->flashdata('success');?>
<style>
   /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   div + script + .modal-backdrop ~ .modal-backdrop {display:none}
   div + .modal-backdrop ~ .modal-backdrop {display:none}
   .up-cusfields .modal-body .form-group:nth-child(2)
   {
        float: left;
        width: 100%;
        margin-top: 15px;
   }
   .up-cusfields .modal-body .form-group:nth-child(2) select
   {
    width: 100%;
   }
   .up-cusfields .modal-body .form-group ~ .up-cusfields .modal-body .form-group:nth-child(2) {
    margin-bottom: 0;
  }
  .up-cusfields .modal-body .form-group .col-sm-7 {
    padding-right: 0;
  }
  .up-cusfields .modal-body .form-group .col-form-label {
    padding-left: 0;
  }

</style>
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                           <!-- <div class="all-clients floating_set">
                              <div class="tab_section_01 floating_set">
                              <ul class="client_tab">
                               <li class="active"><a href="<?php echo  base_url()?>user">All Clients</a></li>
                               <li><a href="<?php echo  base_url()?>client/addnewclient">Add new client</a></li>
                              </ul> 
                              
                              <ul class="client_tab">
                              <li><a href="#">Email All</a></li>
                              <li><a href="#">filters</a></li>
                              </ul> 
                              </div> 
                              
                              <div class="text-left search_section1 floating_set">
                              <div class="search_box01">
                              
                              <div class="pcoded-search">
                              
                                                           <span class="searchbar-toggle">  </span>
                              
                                                           <div class="pcoded-search-box ">
                              
                                                               <input type="text" placeholder="Search">
                              
                                                               <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
                              
                                                           </div>
                              
                                                       </div>
                              
                              </div>
                              </div>
                              
                              
                              
                              </div> <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#allusers">Custom fields</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                                 <div class="count-value1 pull-right ">
                                    <button id="deleteTriger" class="deleteTri" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete</button>
                                 </div>
                              </div>
                               <?php if($succ){?>
                                 <div class="modal-alertsuccess alert alert-success"
                                    >
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <div class="pop-realted1">
                                       <div class="position-alert1">
                                          <?php echo $succ; ?>
                                       </div>
                                    </div>
                                 </div>
                                 <?php } ?>
                              <div class="all_user-section2 floating_set">
                                 <div class="tab-content">
                                    <div id="allusers" class="tab-pane fade in active">
                                       <div class="client_section3 table-responsive floating_set ">
                                          <div id="status_succ"></div>
                                          <div class="all-usera1">
                                             <table class="table client_table1 text-center display nowrap" id="allstaff" cellspacing="0" width="100%">
                                                <thead>
                                                   <tr class="text-uppercase">
                                                      <th>Name</th>
                                                      <th>Belongsto</th>
                                                      <th>Type</th>
                                                      <th>Active</th>
                                                      <th>Options</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php foreach ($custom_list as $getallUserkey => $getallUservalue) {
                                                      if($getallUservalue['active']==1){
                                                          $status_id = 'checkboxid1';  
                                                          $chked = 'selected';
                                                          $status_val =1;
                                                          $active='Active';
                                                      }elseif($getallUservalue['active']==0 ){
                                                          $status_id = 'checkboxid2';  
                                                          $chked = 'selected';
                                                          $status_val = 0;
                                                          $active='Inactive';
                                                      } 
                                                      ?>
                                                   <tr>
                                                      <td><?php echo ucfirst($getallUservalue['name']);?></td>
                                                      <td>
                                                         <?php echo $getallUservalue['fieldto'];?>
                                                      </td>
                                                      <td><?php echo $getallUservalue['type'];?></td>
                                                      <td>
                                                         <!-- <p class="onoff" ><input type="checkbox" name="status<?php echo $getallUservalue['id'];?>" value="<?php echo $getallUservalue['id'];?>" id="<?php echo $status_id;?>" class="status" <?php echo $chked;?> ><label for="<?php echo $status_id;?>"></label></p> 
                                                            <p class="onoff"><input type="checkbox" name="status<?php echo $getallUservalue['id'];?>" value="<?php echo $getallUservalue['id'];?>" id="status<?php echo $getallUservalue['id'];?>" class="status" <?php echo $chked;?>><label for="status<?php echo $getallUservalue['id'];?>"></label></p> -->  
                                                         <select name="status<?php echo $getallUservalue['id'];?>" id="status<?php echo $getallUservalue['id'];?>" class="status" data-id="<?php echo $getallUservalue['id'];?>">
                                                            <option value="1" <?php if(isset($getallUservalue['active']) && $getallUservalue['active']==1) {?> selected="selected"<?php } ?> >Active</option>
                                                            <option value="0" <?php if(isset($getallUservalue['active']) && ($getallUservalue['active']==0 )) {?> selected="selected"<?php } ?>>Inactive</option>
                                                         </select>
                                                      </td>
                                                      <td>
                                                         <p class="action_01">
                                                            <a href="<?php echo base_url().'/user/custom_delete/'.$getallUservalue['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                                         <!--    <a href="<?php echo base_url().'user/edit_custom_fields/'.$getallUservalue['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a> -->
                                                         <a href="javascript:;" data-toggle="modal" data-target="#update_customfields_<?php echo $getallUservalue['id']; ?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                                            <!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                                         </p>
                                                      </td>
                                                   </tr>
                                                   <?php } ?>
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- home-->
                                 </div>
                              </div>
                              <!-- admin close -->
                           </div>
                           <!-- Register your self card end -->
                        </div>
                     </div>
                  </div>
                  <!-- Page body end -->
               </div>
            </div>
            <!-- Main-body end -->
            <div id="styleSelector">
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>

<!-- update custom_firm_fields -->
 <?php foreach ($custom_list as $getallUserkey => $getallUservalue) { ?>
 <div class="modal fade up-cusfields" id="update_customfields_<?php echo $getallUservalue['id']; ?>" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Field</h4>
        </div>

      
        <?php 
         $data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user','role','1');
        
        $data['custom_fields'] = $this->Common_mdl->GetAllWithWhere('tblcustomfields','id',$getallUservalue['id']);
        
        $this->load->view('users/new_field_new',$data); ?>
      </div>
    </div>
  </div>
  <?php } ?>
<!--e nd of custom firm fields -->

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script>
   $(document).ready(function() {
   
   
   var dataTable =  $("#allstaff").DataTable({
       "iDisplayLength": 20,
     "scrollX": true,
    "dom": '<"top"fl>rt<"bottom"ip><"clear">'
       
   
        
     });
   
   
   $("#frozens").dataTable({
        "iDisplayLength": 20,
         //"scrollX": true,
     "dom": '<"top"fl>rt<"bottom"ip><"clear">'
     });
   
    
   
   
   $("#bulkDelete").on('click',function() { // bulk checked
        var status = this.checked;
   
        $(".deleteRow").each( function() {
          $(this).prop("checked",status);
   
        });
         if(status==true)
         {
           $('#deleteTriger').show();
         } else {
           $('#deleteTriger').hide();
         }
      });
   
   $(".deleteRow").on('click',function() { // bulk checked
   
         var status = this.checked;
   
         if(status==true)
         {
           $('#deleteTriger').show();
         } else {
           $('#deleteTriger').hide();
         }
       });
      
      $('#deleteTriger').on("click", function(event){ // triggering delete one by one
        
        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
          var ids = [];
          $('.deleteRow').each(function(){
            if($(this).is(':checked')) { 
              ids.push($(this).val());
            }
          });
          var ids_string = ids.toString();  // array to string conversion 
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>user/multiple_staff_delete/",
            data: {data_ids:ids_string},
            success: function(response) {
              //dataTable.draw(); // redrawing datatable
              var emp_ids = response.split(",");
    for (var i=0; i < emp_ids.length; i++ ) {
   
      $("#"+emp_ids[i]).remove(); 
    }
    
            },
            //async:false,
          });
        }
      });
   
   
   
   //$(".status").change(function(e){
   $('#allstaff').on('change','.status',function () {
   //e.preventDefault();
   
   
    var rec_id = $(this).data('id');
    var stat = $(this).val();
   
   $.ajax({
         url: '<?php echo base_url();?>user/custom_field_statusChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         timeout: 3000,
         success: function( data ){
          //alert('ggg');
             $("#status_succ").html('<div class="modal-alertsuccess alert alert-success-userid card borderless-card" id="timeout"><a href="#" class="close" data-dismiss="alert" aria-label="close">x</a><div class="pop-realted1"> <div class="position-alert1"><div class="position-alert1"><span>Success !!! User status have been changed successfully...</span></div></div></div></div>').hide().fadeIn(0, function() { $('#status_succ'); });
             setTimeout(function(){ $('#status_succ').html(''); }, 2000);
             setTimeout(resetAll,3000);


           
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
    });
   
   // payment/non payment status
   $('.suspent').click(function() {
     if($(this).is(':checked'))
         var stat = '1';
     else
         var stat = '0';
     var rec_id = $(this).val();
     var $this = $(this);
      $.ajax({
         url: '<?php echo base_url();?>user/suspentChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         success: function( data ){
          //alert('ggg');
             $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
             if(stat=='1'){
              
               $this.closest('td').next('td').html('Payment');
   
             } else {
               $this.closest('td').next('td').html('Non payment');
             }
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
   });
   });
   
</script>
<script>
   /* var notes = {};
   
   <?php
      $data = $this->db->query("SELECT * FROM update_client")->result_array();
      foreach($data as $row) {
      
        echo 'notes["'.$row['user_id'].'"] = "<p>company status:'.$row['company_status'].'</p><br><p>account period end:'.$row['accounts_periodend'].'</p>";';
      
       //echo 'notes["'.$row['user_id'].'"] = "'.(isset($row['company_status'])&&($row['company_status']!=0)) ? .'<p>company status:'.$row['company_status'].'</p><br><p>account period end:'.$row['accounts_periodend'].'</p>";';
      
        //echo 'notes["'.$row['user_id'].'"]'= (isset($row['company_status'])&&($row['company_status']!=0)) ? $row['company_status']:'';
      }
      ?>
   
   $(document).on("click", ".noteslink", function() {
     var id = $(this).data("rowid");
     $("#myModalnote .modal-body").html(notes[id]);
     $("#myModalnote").modal("show");
   });
   */
$(document).ready(function(){
setTimeout(function () {
  $('.modal-alertsuccess').hide();
             }, 2000);
});
   
   
   
</script>
<div class="text12345"></div>