				<!-- <div class="invoice-crm1 disk-redesign1">
				   <div class="top-leads fr-task cfssc redesign-lead-wrapper">
				      <div class="lead-data1 pull-right leadsG leads_status_count"> -->

				         <!-- <div class="page-data-block">

				            <div class="data-counts rem-card">

				               <div class="data-count">
				                  <a href="<?php echo base_url(); ?>user/task_list">
				                     <span class="count"><?php echo $tasks['task_count']; ?></span>
				                     <p>Tasks</p>
				                  </a>
				               </div>

				               <div class="data-count">
				                  <a href="<?php echo base_url(); ?>Firm_dashboard/Complete">
				                     <span class="count"><?php echo $closed_tasks['task_count']; ?></span>
				                     <p>Completed Tasks</p>
				                  </a>
				               </div>

				               <div class="data-count">
				                  <a href="<?php echo base_url(); ?>Firm_dashboard/inprogress">
				                     <span class="count"><?php echo $pending_tasks['task_count']; ?></span>
				                     <p>Inprogress Tasks</p>
				                  </a>
				               </div>

				               <div class="data-count">
				                  <a href="<?php echo base_url(); ?>Firm_dashboard/notstarted">
				                     <span class="count"><?php echo $cancelled_tasks['task_count']; ?></span>
				                     <p>Not Started Tasks</p>
				                  </a>
				               </div>

				            </div>

				         </div> -->

						 <div class="desk-dashboard mydisk-realign1 floating_set">
							<div class="page-data-block pt0">
								<div class="rem-card impo">
									<div class="number-mydisk floating_set">
										<div class="count-mydisk_task">
											<div class="target-disk">
												<a href="<?php echo base_url(); ?>Firm_dashboard/All_client">
													<span>Tasks</span>
													<div class="count-pos-01">
														<strong><?php echo $tasks['task_count']; ?></strong>
													</div>
												</a>
											</div>
										</div>

										<div class="count-mydisk_task">
											<div class="target-disk">
												<a href="<?php echo base_url(); ?>Firm_dashboard/Active_client">
													<span>Completed Tasks</span>
													<div class="count-pos-01">
														<strong><?php echo $closed_tasks['task_count']; ?></strong>
													</div>
												</a>
											</div>
										</div>


										<div class="count-mydisk_task">
											<div class="target-disk">
												<a href="<?php echo base_url(); ?>Firm_dashboard/Frozen_client">
													<span>Inprogress Tasks</span>
													<div class="count-pos-01">
														<strong><?php echo $pending_tasks['task_count']; ?></strong>
													</div>
												</a>
											</div>
										</div>

										<div class="count-mydisk_task">
											<div class="target-disk">
												<a href="<?php echo base_url(); ?>Firm_dashboard/Inactive_client">
													<span>Not Started Tasks</span>
													<div class="count-pos-01">
														<strong><?php echo $cancelled_tasks['task_count']; ?></strong>
													</div>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

				      <!-- </div>
				   </div>
				</div> -->


				<div class="cards12 space-disk1 col-xs-12 mydisk-realign1">

				   <div class="page-data-block lf-mytsk pt0 my-tasks">
				      <!-- To Do List card start -->
				      <div class="rem-card fr-hgt">
				         <div class="rem-card-header">
							<h5 class="rem-card-titile">My Tasks</h5>
							<a class="filter-btn-task-dash filter-btn-task" data-id="1">Due Today</a>
							<a class="filter-btn-task-dash filter-btn-task" data-id="7">Due in One Week</a>
							<a class="filter-btn-task-dash filter-btn-task" data-id="14">Due in Two Week</a>
							<a class="filter-btn-task-dash filter-btn-task" data-id="21">Due in Three Week</a>
							<a class="filter-btn-task-dash filter-btn-task" data-id="30">Due in One Month</a>
				            <a class="rem-btn rem-btn-view" target="_blank" href="<?php echo base_url(); ?>Firm_dashboard/notstarted">Viewall</a>
				         </div>
				         <div class="rem-card-body">
				            <!-- <h3> Status</h3> -->
				            <h3 <?php
                              if (empty($task_list)) {
                                 echo "style='display:none;'";
                              } ?>>Task Name <span class="f-right">Start Date / End Date</span></h3>
				            <?php
                        if (empty($task_list)) { ?>

				               <div class="new-task12">
				                  No Tasks Available
				               </div>
				            <?php  } else { ?>
				               <div class="new-task">

				                 <?php
									foreach ($task_list as $task) {

										$assignee_det = $this->Common_mdl->getAssigneefordashtask('TASK', $task['id']);
										//print_r($assignee_det);
										//echo $assignee_det[0]['user_id'];
									?>
										
									<span class="task-date"> 
										<?php
										if (strtotime($task['end_date']) < time()) {
											$date = date('y-m-j&\nb\sp;g:i:s');

											$start_date = date('Y-m-d', strtotime($task['start_date']));
											echo $end_date = date('d-m-Y', strtotime($task['end_date']));

											$date1 = strtotime('' . $start_date . ' 00:00:00');

											$date2 = strtotime('' . $end_date . ' 00:00:01');

											$today = time();

											$dateDiff = $date2 - $date1;

											$dateDiffForToday = $today - $date1;

											$percentage = $dateDiffForToday / $dateDiff * 100;

											$percentageRounded = round($percentage);
										
										?>
									</span>
				                        <span class="percentage-code1 perecent-task-code">overdue</span>
				                     <?php
                                 } else {

                                    $percentageRounded = '0';
									
                                 ?>
				                        <span class="percentage-code1"> <?php echo $percentageRounded . '%';  ?> </span>
				                     <?php
								}
                                 ?>


				                     <div class="to-do-list to-do-list-tasks">
				                        <div class="checkbox-fade fade-in-primary">
				                           <label class="check-task">
				                              <span><a href="<?php echo base_url(); ?>user/task_details/<?php echo $task['id']; ?>"><?php echo $task['subject']; ?></a></span>
				                              <span class="bg-c-pink">
				                                 <?php

                                             $username = array();
											 if(count($assignee_det)>0){
                                             foreach ($assignee_det as $key => $val) {
                                                array_push($username, ucwords($val['crm_name']));
                                             }
                                             echo implode(',', $username);} ?>
				                              </span>
				                              <div class="progress">
				                                 <div class="progress-bar bg-c-blue" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentageRounded . '%'; ?>">
				                                 </div>
				                              </div>
				                           </label>
				                        </div>
				                        <span class="f-right end-dateop"></span>
				                        <span class="f-right end-dateop date-right"><?php echo date('d-m-Y', strtotime($task['start_date'])); ?>/<?php echo date('d-m-Y', strtotime($task['end_date'])); ?></span>
				                        <!-- <div class="f-right">
                                       <a href="#" class="delete_todolist"><i class="icofont icofont-ui-delete"></i></a>
                                       </div> -->
				                     </div>
				                  <?php }  ?>
				               </div>
				            <?php } ?>
				         </div>
				      </div>
				      <!-- To Do List card end -->
				   </div>
				</div>