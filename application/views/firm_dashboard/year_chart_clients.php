<div id="chart_Donut11" style="width: 100%; height: 300px;"></div>

<script type="text/javascript">

      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart1);

      function drawChart1() 
      {
            var data = google.visualization.arrayToDataTable([
            ['Status','link','Count'],
            ['Private Limited Company','<?php echo base_url(); ?>Firm_dashboard/private_function',<?php echo $Private['limit_count']; ?>],
            ['Public Limited Company', '<?php echo base_url(); ?>Firm_dashboard/public_function',<?php echo $Public['limit_count']; ?>],
            ['Limited Liability Partnership', '<?php echo base_url(); ?>Firm_dashboard/limited_function', <?php echo $limited['limit_count']; ?>],
            ['Partnership', '<?php echo base_url(); ?>Firm_dashboard/partnership', <?php echo $Partnership['limit_count']; ?>],
            ['Self Assessment', '<?php echo base_url(); ?>Firm_dashboard/self_assesment', <?php echo $self['limit_count']; ?>],
            ['Trust', '<?php echo base_url(); ?>Firm_dashboard/trust', <?php echo $Trust['limit_count']; ?>],
            ['Charity', '<?php echo base_url(); ?>Firm_dashboard/charity', <?php echo $Charity['limit_count']; ?>],
            ['Other', '<?php echo base_url(); ?>Firm_dashboard/other', <?php echo $Other['limit_count']; ?>],
          ]);

          var view = new google.visualization.DataView(data);
           view.setColumns([0, 2]);

          var options = {
           title: 'Client Lists',
           pieHole: 0.4,
          };

          var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
          chart.draw(view, options);

          var selectHandler = function(e) {
          //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), options.title, "_blank");
          }
          // Add our selection handler.
          google.visualization.events.addListener(chart, 'select', selectHandler);
      }
      
</script>