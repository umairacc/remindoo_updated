
<?php 

$this->load->view('includes/new_header');
$succ = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$userid = $this->session->userdata('id');

// foreach($unpaid_invoice as $key => $value) 
// { 
//   if($value['invoice_duedate'] == 'Expired')
//   {
//      unset($unpaid_invoice[$key]);
//   }
// }  


?>
<link href="<?php echo base_url() ?>assets/css/timeline-style.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url() ?>assets/css/dashboard-chart.css" type="text/css" rel="stylesheet" />
<style type="text/css">
   .zen-livechat {
      position: fixed;
      bottom: 50px;
      right: 20px;
      box-shadow: none;
      background-color: #1F73B7 !important;
      color: #FFFFFF !important;
      fill: #FFFFFF !important;
      padding: 8px 20px;
      border-radius: 5px;
      font-weight: 600;
      font-size: 16px;
      cursor: pointer;
   }

   .zen-livechat i {
      margin-right: 5px;
   }


   div#sidebar.chat_particular {
      right: 0;
      bottom: 0;
      left: auto;
      position: fixed;
      max-height: 400px;
      top: auto;
      overflow: auto;
      width: 310px;
      box-shadow: 0 1px 11px 0 rgba(0, 0, 0, 0.12);
      border: none;
      z-index: 999;
      background: #fff;
      height: initial;
   }

   .card.card_main.p-fixed.users-main {
      top: 0;
      max-height: initial;
      overflow: initial;
      position: relative;
      border: none;
      box-shadow: initial;
      width: 100%;
   }

   .chat-page .chat-box {
      margin-right: 0;
      margin-left: 0;
      right: 320px;
   }

   .ui-widget.ui-widget-content {
      right: 20px !important;
      left: auto !important;
   }

   .to-do-list.sortable-moves {
      padding-left: 0;
   }

   .pcoded-content {
      float: left !important;
      width: 100%;
   }

   .hide_classes {
      display: none !important;
   }

   .hide_check {
      display: none !important;
   }

   .active {
      display: block !important;
   }

   /*.card
   {
     width: 500px;
     margin: auto;
   }   */

   .breadcrumb-header span {
      color: green;
   }

   /* Due to start modal */
   #due_to_start_tasks table td:last-child{
      width:30%;
      text-align:right;
   }

   #due_to_start_tasks h4.modal-title { 
      text-align: center;
      width: 100%;
   }

   .user-detail-card.rem-card.due-card {
      flex-direction: column;
      align-items: flex-start;
   }

   .due-card table tbody th {
      padding: 6px 15px;
   }

   #clients_link, #leads_link{
      cursor: pointer;
   }
</style>
<?php
// $service_list = $this->Common_mdl->getServicelist();  
// $service_charges = $this->Common_mdl->getClientsServiceCharge($getCompany); 
// $current_date = date('Y-m-d'); 
// $columns_check = array('1'=> 'crm_confirmation_statement_due_date','2' =>'crm_ch_accounts_next_due','3'=>'crm_accounts_tax_date_hmrc','4'=>'crm_personal_due_date_return','5' => 'crm_vat_due_date','6' => 'crm_rti_deadline','7' => 'crm_pension_subm_due_date', '8' => '', '9' => '','10'=>'crm_next_booking_date','11' => 'crm_next_p11d_return_due','12' => 'crm_next_manage_acc_date','13' => 'crm_insurance_renew_date','14'=>'crm_registered_renew_date','15'=>'crm_investigation_end_date','16' => 'crm_investigation_end_date');   

// $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

// foreach($other_services as $key => $value) 
// {
//    $columns_check[$value['id']] = 'crm_'.$value['services_subnames'].'_statement_date';  
// }
?>
<div class="modal fade" id="todoinfo" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="position-alert1"></div>
         </div>
      </div>
   </div>
</div>
<button type="button" class="todoinfo" data-toggle="modal" data-target="#todoinfo" style="display: none;"></button>
<!-- management block -->

<div class="modal-alertsuccess alert succ popup_info_msg" style="<?php if ($_SESSION['success_msg'] != "") {
                                                                     echo "display: block";
                                                                  } else {
                                                                     echo "display: none";
                                                                  } ?>">
   <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
      <div class="pop-realted1">
         <div class="position-alert1">
            <?php if ($_SESSION['success_msg'] != "") {
               echo $_SESSION['success_msg'];
            } ?>
         </div>
      </div>
   </div>
</div>

<div class="pcoded-content pcode-mydisk">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body myDesk">
         <div class="page-wrapper">
            <div class="card desk-dashboard">
               <div class="page-heading">My Desk</div>
               <div class="inner-tab123">
                  <div class="deadline-crm1 floating_set page-tabs">
                     <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard client-top-nav all-nav-btns" id=""> 
                        <!-- id = proposal_dashboard -->
                        <!-- <li><button id="run_bulk_query">Bulk Query</button></li> -->
                        <li class="nav-item <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_basic_details');
                                             if ($query == 0) { ?>hide_check<?php } else { ?>basic active checking<?php } ?>">
                           <!-- <a class="nav-link" data-toggle="tab" href="#Basics"> -->
                           <a href="javascript:;" class="nav-link active" data-tag="Basics">
                              Basics
                           </a>
                        </li>
                       <!-- <li class="nav-item task_class <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_task_details');
                                                         if ($query == 0) { ?>hide_check<?php } else { ?>task checking<?php } ?>">
                           <a class="nav-link" data-toggle="tab" href="#Tasks">
                           <a href="javascript:;" class="nav-link tasks" data-tag="Tasks">
                              Tasks
                           </a>
                        </li> -->
                        <li class="nav-item to_do_class <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_task_details');
                                                         if ($query == 0) { ?>hide_check<?php } else { ?>task checking<?php } ?>">
                           <!-- <a class="nav-link" data-toggle="tab" href="#Tasks"> -->
                           <a href="javascript:;" class="nav-link tasks" data-tag="Todolist">
                              To Do List
                           </a>
                        </li>
                        <li class="nav-item client_class <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_client_details');
                                                         if ($query == 0) { ?>hide_check<?php } else { ?>client checking<?php } ?>">
                           <!-- <a class="nav-link" data-toggle="tab" href="#Clients"> -->
                           <a href="javascript:;" class="nav-link" data-tag="Clients">
                              Clients
                           </a>
                        </li>
                        <li class="nav-item proposal_class <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_proposal');
                                                            if ($query == 0) { ?>hide_check<?php } else { ?>proposals checking<?php } ?>">
                           <!-- <a class="nav-link" data-toggle="tab"  href="#Proposals"> -->
                           <a href="javascript:;" class="nav-link" data-tag="Proposals">
                              Proposals
                           </a>
                        </li>
                        <!-- <li class="nav-item deadline_class <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_deadline_details');
                                                            if ($query == 0) { ?>hide_check<?php } else { ?>  checking <?php } ?>">
                           <a class="nav-link" data-toggle="tab"  href="#Deadline-Manager">
                           <a href="javascript:;" class="nav-link deads" data-tag="Deadline-Manager">
                              Deadline Manager
                           </a>
                        </li> -->
                        <!--<li class="nav-item reminder_class <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_reminder_details');
                                                            if ($query == 0) { ?>hide_check<?php } else { ?>reminder checking <?php } ?>" id="rem">
                           <a class="nav-link" data-toggle="tab"  href="#Reminder">
                           <a href="javascript:;" class="nav-link" data-tag="Reminder">
                              Reminder
                           </a>
                        </li>-->

                        <li class="nav-item  <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_timeline');
                                             if ($query == 0) { ?>hide_check<?php } else { ?>Timeline checking <?php } ?>">
                           <!-- <a class="nav-link for_tabs for_allactivity_section_ajax" data-toggle="tab"  href="#newtimelineservices">  -->
                           <a href="javascript:;" class="nav-link  for_tabs for_allactivity_section_ajax" data-tag="newtimelineservices">
                              Timeline Services</a></li>
                     </ul>
                  </div>
                  <div class="tab-content">

                     <div id="newtimelineservices" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_timeline');
                                                                        if ($query == 0) { ?>hide_classes<?php } else { ?> active <?php } ?>">
                        <div class="card-block" id="allactivitylog_sections_info">
                        </div>
                     </div>


                     <!--Reminder Tab-->
                     <div id="Reminder" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_reminder_details');
                                                               if ($query == 0) { ?>hide_classes<?php } else { ?>  <?php } ?>">




                        <?php //print_r($service_charges);
                        // $this->load->view('Deadline_manager/reminder_invoice_table',array('service_list'=>$service_list,'service_charges'=>$service_charges)); 
                        ?>

                     </div>


                     <!-- Reminder Tab -->

                     <!-- tab1 -->
                     <div id="Basics" class="proposal_send tab-pane fade in <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_basic_details');
                                                                              if ($query == 0) { ?>hide_classes<?php } else { ?> active <?php } ?>">
                        <!-- pro summary -->

                        <div class="invoice-crm1 disk-redesign1">
                           <div class="top-leads fr-task cfssc redesign-lead-wrapper">
                              <div class="lead-data1 pull-right leadsG leads_status_count">
                                 <!-- <div class="page-data-block"> -->

                                    <!-- <div class="data-counts rem-card">

                                       <div class="data-count">
                                          <a href="<?php echo base_url(); ?>Firm_dashboard/Active_client">
                                             <span class="count"><?php echo ($active_client); ?></span>
                                             <p>Active Clients</p>
                                          </a>
                                       </div>

                                       <div class="data-count">
                                          <a href="<?php echo base_url(); ?>Firm_dashboard/Complete">
                                             <span class="count"><?php echo ($closed_tasks); ?></span>
                                             <p>Completed Tasks</p>
                                          </a>
                                       </div>

                                       <div class="data-count">
                                          <a href="<?php echo base_url(); ?>Firm_dashboard/Inactive_client">
                                             <span class="count"><?php echo ($inactive_client); ?></span>
                                             <p>Inactive Clients</p>
                                          </a>
                                       </div>

                                       <div class="data-count">
                                          <a href="<?php echo base_url(); ?>Firm_dashboard/leads">
                                             <span class="count"><?php echo ($total_leads); ?></span>
                                             <p>Overall Leads</p>
                                          </a>
                                       </div>

                                    </div> -->

                                 <!-- </div> -->
                              </div>
                           </div>
                        </div>

                        <div class="floating_set space-disk1">
                           <!-- <div class="page-data-block pt0">
                              <div class="user-detail-card rem-card">
                                 <div class="height-profile">
                                    <div class="height-profile1 text-left"> <?php $getUserProfilepic = $this->Common_mdl->getUserProfilepic($userid); ?><img src="<?php echo $getUserProfilepic; ?> " alt="" class="img-fluid p-b-10"></div>
                                 </div>
                                 <div class="user-detail">
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Name :</h6>
                                       </div>
                                       <div class="col-sm-7">
                                          <h6 class="m-b-30"><?php
                                                               $user_role = $this->Common_mdl->select_record('user', 'id', $userid);
                                                               echo ucwords($user_role['crm_name']); ?></h6>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-email"></i>EMP ID</h6>
                                       </div>
                                       <div class="col-sm-7">
                                          <h6 class="m-b-30"><a href="mailto:dummy@example.com"><?php echo '#' . $userid; ?></a></h6>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-home"></i>Role ID :</h6>
                                       </div>
                                       <div class="col-sm-7">
                                          <h6 class="m-b-30"><?php echo ucfirst($this->Common_mdl->get_price('organisation_tree', 'user_id', $userid, 'title')); ?></h6>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-touch-phone"></i>Phone :</h6>
                                       </div>
                                       <div class="col-sm-7">
                                          <h6 class="m-b-30"> <?php echo $user_role['crm_phone_number']; ?></h6>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-web"></i>Website :</h6>
                                       </div>
                                       <div class="col-sm-7">
                                          <h6 class="m-b-30"><a href="<?php echo $user_role['crm_business_website']; ?>"><?php echo $user_role['crm_business_website']; ?></a></h6>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>                   -->

                           <!-- Chart Div -->                     
                           <div class="dash-tab-wrapper">
                              <div class="dash-tabs">
                                 <div class="dash-tabs-top">
                                    <div class="dash-tabs-title">
                                       <span>tasks</span>
                                          <form>
                                             <select class="dash-tasks" name="tasks" id="tasks">
                                                <option value="time frame">Time Frame</option>
                                                <option value="today">Today</option>
                                                <option value="this week">This Week</option>
                                                <option value="last week">Last Week</option>
                                                <option value="next week">Next Week</option>
                                                <option value="last days">Last 30 Days</option>
                                                <option value="next days">Next 30 Days</option>
                                             </select>
                                          </form>
                                    </div>
                                    <div class="dash-tab-chart">
                                       <div id="donut_single_tasks" class="dash-chart-inner-txt" style="width: 100px; height: 100px;"></div> 
                                       <div class="chart-data">
                                          <span id="total_task_count"></span>
                                          <span id="total_user_task_count"></span>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="dash-tabs-top">
                                    <div class="dash-tabs-title">
                                       <span>time logged</span>
                                          <form>
                                             <select class="dash-tasks" name="tasks" id="tasks">
                                                <option value="time frame">Time Frame</option>
                                                <option value="today">Today</option>
                                                <option value="this week">This Week</option>
                                                <option value="last week">Last Week</option>
                                                <option value="next week">Next Week</option>
                                                <option value="last days">Last 30 Days</option>
                                                <option value="next days">Next 30 Days</option>
                                             </select>
                                          </form>
                                    </div>
                                    <div class="dash-tab-chart">
                                       <div id="donut_single_time" class="dash-chart-inner-txt" style="width: 100px; height: 100px;"></div> 
                                       <div class="chart-data">
                                          <span id="total_time"></span>
                                          <span id="total_user_time"></span>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="dash-tabs-top" id="clients_link" data-href="/user/assigned">
                                    <div class="dash-tabs-title">
                                       <span>clients</span>
                                          <form>
                                             <select class="dash-clients" name="clients" id="clients">
                                                <option value="time frame">Time Frame</option>
                                                <option value="today">Today</option>
                                                <option value="this week">This Week</option>
                                                <option value="last week">Last Week</option>
                                                <option value="next week">Next Week</option>
                                                <option value="last days">Last 30 Days</option>
                                                <option value="next days">Next 30 Days</option>
                                             </select>
                                          </form>
                                    </div>
                                    <div class="dash-tab-chart">
                                       <div id="donut_single_clients" class="dash-chart-inner-txt" style="width: 100px; height: 100px;"></div> 
                                       <div class="chart-data">
                                          <span id="total_client_count"></span>
                                          <span id="total_user_client_count"></span>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="dash-tabs-top" id="leads_link" data-href="/leads/assigned">
                                    <div class="dash-tabs-title">
                                       <span>leads</span>
                                          <form>
                                             <select class="dash-pros" name="prospects" id="prospects">
                                                <option value="time frame">Time Frame</option>
                                                <option value="today">Today</option>
                                                <option value="this week">This Week</option>
                                                <option value="last week">Last Week</option>
                                                <option value="next week">Next Week</option>
                                                <option value="last days">Last 30 Days</option>
                                                <option value="next days">Next 30 Days</option>
                                             </select>
                                          </form>
                                    </div>
                                    <div class="dash-tab-chart">
                                       <div id="donut_single_leads" class="dash-chart-inner-txt" style="width: 100px; height: 100px;"></div> 
                                       <div class="chart-data">
                                          <span id="total_lead_count"></span>
                                          <span id="total_user_lead_count"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="dash-chart-wrapper">
                                 <div class="dash-chart-wrapper-1">
                                    <div class="dash-chart-upper">
                                       <div class="dash-timeline">
                                          <div class="timeline-logged">
                                             <span>time logged</span>
                                             <form>
                                                <select class="dash-time" name="time" id="time">
                                                   <option value="time frame">Time Frame</option>
                                                   <option value="today">Today</option>
                                                   <option value="this week">This Week</option>
                                                   <option value="last week">Last Week</option>
                                                   <option value="next week">Next Week</option>
                                                   <option value="last days">Last 30 Days</option>
                                                   <option value="next days">Next 30 Days</option>
                                                </select>
                                             </form>
                                          </div>
                                          <div id="donut_single_5" class="dash-chart-inner-txt" style="width: 100px; height: 100px; margin: 0 auto"></div>
                                       </div>
                                       <div class="dash-deadline">
                                          <div class="deadline-logged">
                                             <span>deadlines</span>
                                             <form>
                                                <select class="dash-dead" name="deadlines" id="deadlines">
                                                   <option value="time frame">Time Frame</option>
                                                   <option value="today">Today</option>
                                                   <option value="this week">This Week</option>
                                                   <option value="last week">Last Week</option>
                                                   <option value="next week">Next Week</option>
                                                   <option value="last days">Last 30 Days</option>
                                                   <option value="next days">Next 30 Days</option>
                                                </select>
                                             </form>
                                          </div>
                                          <div id="donut_single_6" class="dash-chart-inner-txt" style="width: 100px; height: 100px; margin: 0 auto"></div>
                                       </div>
                                    </div>
                                    <div class="dash-chart-lower">
                                       <div class="dash-tabs-title">
                                          <span>accounts</span>
                                          <form>
                                                <select class="dash-acc" name="accounts" id="accounts">
                                                   <option value="time frame">Time Frame</option>
                                                   <option value="today">Today</option>
                                                   <option value="this week">This Week</option>
                                                   <option value="last week">Last Week</option>
                                                   <option value="next week">Next Week</option>
                                                   <option value="last days">Last 30 Days</option>
                                                   <option value="next days">Next 30 Days</option>
                                                </select>
                                             </form>
                                       </div>
                                       <div class="dash-tab-chart">
                                          <div class="chart-7-outer">
                                             <div id="donut_single_7" class="dash-chart-inner-txt" style="width: 100px; height: 100px;"></div> 
                                          </div>
                                          <div class="chart-data">
                                             <div><span>total</span><span>55</span></div>
                                             <div><span>pending</span><span>10</span></div>
                                             <div><span>client queries</span><span>15</span></div>
                                             <div><span>W.I.P</span><span>20</span></div>
                                             <div><span>approval await</span><span>5</span></div>
                                             <div><span>completed</span><span>5</span></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="dash-chart-wrapper-2"></div>
                              </div>

                              <!-- Services Over Due Section -->
                              <div class="dash-chart-wrapper service-overdue">
                                 <div class="dash-chart-wrapper-1">
                                    <div class="dash-chart-upper">
                                       <div class="dash-timeline">
                                          <div class="timeline-logged">
                                             <span>time logged</span>
                                             <form>
                                                <select class="dash-log" name="log" id="log">
                                                   <option value="time frame">Time Frame</option>
                                                   <option value="today">Today</option>
                                                   <option value="this week">This Week</option>
                                                   <option value="last week">Last Week</option>
                                                   <option value="next week">Next Week</option>
                                                   <option value="last days">Last 30 Days</option>
                                                   <option value="next days">Next 30 Days</option>
                                                </select>
                                             </form>
                                          </div>
                                          <div id="donut_single_14" class="dash-chart-inner-txt" style="width: 100px; height: 100px; margin: 0 auto"></div>
                                       </div>
                                       <div class="dash-deadline">
                                          <div class="deadline-logged">
                                             <span>deadlines</span>
                                             <form>
                                                <select class="dash-dead-line" name="deadlineoverdue" id="deadlineoverdue">
                                                   <option value="time frame">Time Frame</option>
                                                   <option value="today">Today</option>
                                                   <option value="this week">This Week</option>
                                                   <option value="last week">Last Week</option>
                                                   <option value="next week">Next Week</option>
                                                   <option value="last days">Last 30 Days</option>
                                                   <option value="next days">Next 30 Days</option>
                                                </select>
                                             </form>
                                          </div>
                                          <div id="donut_single_15" class="dash-chart-inner-txt" style="width: 100px; height: 100px; margin: 0 auto"></div>
                                       </div>
                                    </div>
                                    <div class="dash-chart-lower">
                                       <div class="dash-tabs-title">
                                          <span>accounts</span>
                                             <form>
                                                <select class="dash-acc-over" name="accountsoverdue" id="accountsoverdue" >                                                
                                                   <option value="time frame">Time Frame</option>
                                                   <option value="today">Today</option>
                                                   <option value="this week">This Week</option>
                                                   <option value="last week">Last Week</option>
                                                   <option value="next week">Next Week</option>
                                                   <option value="last days">Last 30 Days</option>
                                                   <option value="next days">Next 30 Days</option>
                                                </select>
                                             </form>
                                       </div>
                                       <div class="dash-tab-chart">
                                          <div class="chart-16-outer">
                                             <div id="donut_single_16" class="dash-chart-inner-txt" style="width: 100px; height: 100px;"></div> 
                                          </div>
                                          <div class="chart-data">
                                             <div><span>total</span><span>55</span></div>
                                             <div><span>pending</span><span>10</span></div>
                                             <div><span>client queries</span><span>15</span></div>
                                             <div><span>W.I.P</span><span>20</span></div>
                                             <div><span>approval await</span><span>5</span></div>
                                             <div><span>completed</span><span>5</span></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="dash-chart-wrapper-2"></div>
                              </div>
                              <!-- Services End -->
                              <div class="dash-accounts-wrapper">
                              <?php
                                 $i = 101;
                                 foreach($all_services as $service){
                              ?>
                                 <div class="dash-tabs-top">
                                    <div class="dash-tabs-title">
                                       <span><?php echo $service['service_name'];?></span>
                                          <form>
                                                <select class="dash-account" name="account" id="account">
                                                   <option value="time frame">Time Frame</option>
                                                   <option value="today">Today</option>
                                                   <option value="this week">This Week</option>
                                                   <option value="last week">Last Week</option>
                                                   <option value="next week">Next Week</option>
                                                   <option value="last days">Last 30 Days</option>
                                                   <option value="next days">Next 30 Days</option>
                                                </select>
                                             </form>
                                    </div>
                                    <div class="dash-tab-chart">
                                       <div class="chart-8-outer">
                                          <div id="donut_single_<?php echo $i;?>" class="dash-chart-inner-txt" style="width: 100px; height: 100px;"></div> 
                                       </div>
                                       <div class="chart-data service-chart-data" service-id="<?php echo $service['id'];?>">
                                          <div><span></span><span>total</span><span class="total-count-task">55</span></div>
                                          <div><span></span><span>pending</span><span class="pending-count-task">10</span></div>
                                          <div><span></span><span>client queries</span><span class="client-queried-count-task">15</span></div>
                                          <div><span></span><span>W.I.P</span><span class="progress-count-task">20</span></div>
                                          <div><span></span><span>approval await</span><span class="await-count-task">5</span></div>
                                          <div><span></span><span>completed</span><span class="completed-count-task">5</span></div>
                                       </div>
                                    </div>
                                 </div> 
                              <?php
                                 $i++;
                                 }
                              
                              ?>
                              </div>
                           </div>

                           <!-- Chart div end -->

                           <!-- Summery Start -->

                           <!-- summary end -->
                        </div><?php
                        if($due_to_start){
                           if(count($due_to_start) > 0){ ?>
                              <div class="floating_set space-disk1">
                                 <div class="page-data-block pt0">                              
                                    <div class="user-detail-card rem-card due-card">
                                       <h4 class="due-title">Following tasks assigned to you are due to start</h4>
                                       <table>
                                          <tr>
                                             <th>SL</th>
                                             <th>Subject</th>
                                             <th>Task started</th> 
                                          </tr><?php
                                          $i = 1;
                                          foreach($due_to_start as $dts){ ?>
                                             <tr>
                                                <td><?php echo $i; ?></td>
                                                <td>
                                                   <a href="/user/task_details/<?php echo $dts['ID']; ?>" target="_blank">
                                                      <?php echo $dts['SUBJECT']; ?> (due to start <?php echo $dts['START_DATE']; ?>)
                                                   </a>
                                                </td>
                                                <td><input type="checkbox" class="due-to-start" value="<?php echo $dts['ID']; ?>"></td>
                                             </tr><?php
                                             $i++;
                                          } ?>
                                       </table>
                                    </div>
                                 </div>  
                              </div><?php
                           } 
                        }?>
                        <!-- pro summary -->
                        <!--chat div-->
                        <div class="zen-livechat">
                           <i class="fa fa-comments-o" aria-hidden="true"></i>
                           <span> Chat</span>
                        </div>
                        <!--closed chat div-->
                     </div>
                     <!-- tab2-->
                     <div id="Tasks" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_task_details');
                                                            if ($query == 0) { ?> hide_classes  <?php } ?>">



                     </div>
                     <!-- tab 3-->
                     <div id="Todolist" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_task_details');
                                                            if ($query == 0) { ?> hide_classes  <?php } ?>">



                     </div>
                     <!-- tab 4-->
                     <div id="Clients" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_client_details');
                                                            if ($query == 0) { ?> hide_classes <?php } ?>">


                     </div>
                     <!--tab 5-->
                     <div id="Proposals" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_proposal');
                                                               if ($query == 0) { ?> hide_classes <?php } ?>">
                        <div class="row col-12">
                           <div class="page-data-block dashboard-proposal-tab">
                              <div class="rem-card desk-dashboard teo mydisk-realign1">
                                 <div class="rem-card-header">
                                    <h5 class="rem-card-titile">Proposals</h5>
                                 </div>
                                 <span class="time-frame"><label>Time Frame:</label>
                                 <select id="proposal">
                                       <option value="month">This Month</option>
                                       <option value="year">This Year</option>
                                    </select>
                                 </span>
                                 
                                 <div class="card-block proposal">
                                    <div id="piechart" style="width: 100%; height: 309px;"></div>
                                 </div>
                              </div>
                           </div>

                        </div>


                     </div>
                     <!--tab 6-->
                     <div id="Deadline-Manager" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_deadline_details');
                                                                     if ($query == 0) { ?> hide_classes <?php } ?>">


                     </div>
                  </div>
               </div>
               <!-- chart sec -->
               <div class="chat_sec">
               </div>
            </div>
         </div>
         <!-- management block -->
         <div class="modal fade" id="myTodo" role="dialog">
            <div class="modal-dialog">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Add New Todo's</h4>
                  </div>
                  <div class="modal-body">
                     <label>Todo's Name</label>
                     <input type="text" name="todo_name" id="todo_name" value="">
                     <span id="er_todoname"></span>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-default" id="todo_save">Save</button>
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
               </div>
            </div>
         </div>
         <div id="edit_delete_modals">
            <?php
            foreach ($todolist as $todo) {
            ?>
               <div class="modal fade edittodo_view" id="EditTodo_<?php echo $todo['id']; ?>" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title">Edit Todos</h4>
                        </div>
                        <div class="modal-body">
                           <input type="hidden" name="id" id="todo_id" value="<?php echo $todo['id']; ?>">
                           <input type="text" name="todo_name" class="todo_name" value="<?php echo $todo['todo_name']; ?>">
                           <span id="er_todoname_<?php echo $todo['id']; ?>"></span>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-default save" id="<?php echo $todo['id']; ?>">Save</button>
                           <button type="button" class="btn btn-default close" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal fade" id="DeleteTodo_<?php echo $todo['id']; ?>" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title">Delete todos</h4>
                        </div>
                        <div class="modal-body">
                           <p>Do you want to Delete Todo's ?</p>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-default delete" id="<?php echo $todo['id']; ?>">Yes</button>
                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                  </div>
               </div>
            <?php } ?>
         </div>
       

         <!-- Due to start popup -->
         <!-- Trigger the modal with a button -->
         <button type="button" id="due_tasks" style="display:none;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#due_to_start_tasks">Open Modal</button><?php
         if($due_to_start){
            if(count($due_to_start) > 0){ ?>
               <!-- Modal -->
               <div id="due_to_start_tasks" class="modal fade" role="dialog">
                  <div class="modal-dialog modal-startup">

                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title">
                              <div>Hello <?php echo $username; ?>,</div>
                              <div>Following tasks assigned to you are due to start</div>
                           </h4>
                        </div>
                        <div class="modal-body">
                           <table><?php
                              foreach($due_to_start as $dts){ ?>
                                 <tr>
                                    <td>
                                       <a href="/user/task_details/<?php echo $dts['ID']; ?>" target="_blank">
                                          <?php echo $dts['SUBJECT']; ?> (due to start <?php echo $dts['START_DATE']; ?>)
                                       </a>
                                    </td>
                                    <td>
                                       <input type="checkbox" class="due-to-start" value="<?php echo $dts['ID']; ?>">
                                       <label for="<?php echo $dts['ID']; ?>">Task started?</label><br>
                                    </td>
                                 </tr><?php
                              } ?>
                           </table>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Due to start popup --><?php
            } 
         }?>

         <?php $this->load->view('includes/session_timeout'); ?>
         <?php $this->load->view('includes/footer'); ?>
         <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
         <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
         <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
         <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
         <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
         <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
         <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

         <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/todo.js"></script>
         <!-- jquery sortable js -->
         <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Sortable.js"></script>
         <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sortable-custom.js"></script>
         <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
         <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom-google-chart.js"></script>

         <script src="<?php echo base_url(); ?>assets/js/RGraph.common.core.js"></script>
         <script src="<?php echo base_url(); ?>assets/js/RGraph.bar.js"></script>
         <script src="<?php echo base_url(); ?>assets/js/RGraph.common.dynamic.js"></script>
         <script type="text/javascript">
            $(document).ready(function() {

               $('#filter_client').val('month');
               $('#proposal').val('month');
               $('#filter_deadline').val('month');

               $(document).on('click', '.tasks', function() {
                  $('div .tab-content').find('div #home1').addClass('active');
               });
               $(document).on('click', '.deads', function() {
                  setTimeout(function() {
                     $('#filter_deadline').val('month').trigger('change');
                  }, 1000)
               });
               $(".checking").first().find('a').trigger('click');


               // $(".hide_check").each(function(){
               //    $(this).next('li').trigger('click');
               //     console.log($(this).next('li').attr('class'));
               // });



               // console.log( "ready!" );

               // var sec_id=$(".hide_classes").next().attr('id');

               // console.log(sec_id);

               // console.log($('#proposal_dashboard a[href="#'+sec_id+'"]').attr('class'));

               // $('#proposal_dashboard a[href="#'+sec_id+'"]').trigger('click');

            });





            $(document).ready(function() {

               //   var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
               /*$( ".datepicker" ).datepicker({
                      dateFormat: 'yy-mm-dd'
                  });*/

               $('.tags').tagsinput({
                  allowDuplicates: true
               });

               $('.tags').on('itemAdded', function(item, tag) {
                  $('.items').html('');
                  var tags = $('.tags').tagsinput('items');

               });
            });

            // $("#run_bulk_query").click(function() {
            //    $.ajax({
            //       url: '<?php echo base_url(); ?>leads/runbulkquery/',
            //       type: 'POST',
            //       success: function(data) {
            //          console.log(data);
            //          //alert(data);
            //       },
            //    });
            // });

            $("#contact_today").click(function() {
               if ($("#contact_today").is(':checked')) {
                  $(".selectDate").css('display', 'none');
               } else {
                  $(".selectDate").css('display', 'block');
               }
            });

            $(".contact_update_today").click(function() {
               var id = $(this).attr("data-id");
               if ($(this).is(':checked')) {

                  $("#selectDate_update_" + id).css('display', 'none');
               } else {
                  $("#selectDate_update_" + id).css('display', 'block');
               }
            });

            $("#all_leads").dataTable({
               "iDisplayLength": 10,
            });
         </script>
         <script type="text/javascript">
            $(document).ready(function() {

               $("#leads_form").validate({
                  rules: {
                     lead_status: "required",
                     source: "required",
                     name: "required",
                  },
                  messages: {
                     lead_status: "Please enter your Status",
                     source: "Please enter your Source",
                     name: "Please enter your Name"
                  },

               });


               $(".edit_leads_form").validate({
                  rules: {
                     lead_status: "required",
                     source: "required",
                     name: "required",
                  },
                  messages: {
                     lead_status: "Please enter your Status",
                     source: "Please enter your Source",
                     name: "Please enter your Name"
                  },

               });
            });


            $(".sortby_filter").click(function() {
               $(".LoadingImage").show();

               var sortby_val = $(this).attr("id");
               var data = {};
               data['sortby_val'] = sortby_val;
               $.ajax({
                  url: '<?php echo base_url(); ?>leads/sort_by/',
                  type: 'POST',
                  data: data,
                  success: function(data) {
                     $(".LoadingImage").hide();

                     $(".sortrec").html(data);
                     $("#all_leads").dataTable({
                        "iDisplayLength": 10,
                     });
                  },
               });

            });
            $("#import_leads").validate({
               rules: {
                  file: {
                     required: true,
                     accept: "csv"
                  },

                  lead_status: "required",
                  source: "required",
                  name: "required",
               },
               messages: {
                  file: {
                     required: 'Required!',
                     accept: 'Please upload a file with .csv extension'
                  },

                  lead_status: "Please enter your Status",
                  source: "Please enter your Source",
                  name: "Please enter your Name"
               },

            });
         </script>
         <script type="text/javascript">
            $(document).ready(function() {
               $('.edit-toggle1').click(function() {
                  $('.proposal-down').slideToggle(300);
               });

               $("#legal_form12").change(function() {
                  var val = $(this).val();
                  if (val === "from2") {
                     // alert('hi');
                     $(".from2option").show();
                  } else {
                     $(".from2option").hide();
                  }
               });
               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>Login/send_email_notification",
                  success: function(data) {}
               });
            });
         </script>
         <script>
            $(function() {

               $(".sortable").sortable({
                  update: function(event, ui) {
                     var arr = [];
                     $("div.sortable ").each(function() {
                        var idsInOrder = [];
                        // idsInOrder.push($(this).attr('id')) 
                        var ul_id = $(this).attr('id');
                        //  alert(ul_id);

                        $("div#" + $(this).attr('id') + " div.card-sub").each(function() {
                           idsInOrder.push($(this).attr('id'))
                        });
                        // alert(idsInOrder);
                        // arr.push({[ul_id]:idsInOrder});   
                        arr[ul_id] = idsInOrder;
                     });

                     $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Firm_dashboard/update_type_todolist",
                        data: {
                           'datas': arr
                        },
                        beforeSend: function() {
                           $(".LoadingImage").show();
                        },
                        success: function(data) {
                           $(".LoadingImage").hide();
                        }
                     });
                  }
               });
               //$( ".sortable" ).disableSelection();

            });

            function check_todoname(todoname, recid) {
               if (recid != "") {
                  var er = "#er_todoname";
                  er = er.concat('_' + recid);
               } else {
                  var er = "#er_todoname";
               }

               if (todoname != "") {
                  $(er).html('');
                  $.ajax({
                     url: "<?php echo base_url(); ?>Firm_dashboard/Check_Distinct",
                     type: "POST",
                     data: {
                        todoname: todoname,
                        id: recid
                     },

                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(count) {
                        if (count > 0) {
                           $(er).html('Already Exists.').css('color', 'red');
                           $(".LoadingImage").hide();
                        } else {
                           $(er).html('');

                           if (recid == "") {
                              $.ajax({
                                 type: "POST",
                                 url: "<?php echo base_url(); ?>Firm_dashboard/todolist_add",
                                 data: {
                                    'todo_name': todoname
                                 },

                                 success: function(data) {
                                    var json = JSON.parse(data);
                                    $("#myTodo").find('.close').trigger('click');
                                    //$("#myTodo").modal('hide');
                                    $(".LoadingImage").hide();

                                    if (json['status'] > 0) {
                                       var content = json['content'];
                                       var all_edit_delete_modals = json['all_edit_delete_modals'];
                                       var view_all = json['view_all'];
                                       $(".sortable").html('');
                                       $(".sortable").append(content);
                                       $('#edit_delete_modals').html('');
                                       $('#edit_delete_modals').html(all_edit_delete_modals);
                                       $('div .tab-content').find('div #profile1').children('div .new-task').html("");
                                       $('div .tab-content').find('div #profile1').children('div .new-task').html(view_all);
                                       $('#todoinfo').find('.position-alert1').html("");
                                       $('#todoinfo').find('.position-alert1').html('Todo Added Successfully!');
                                       $('.todoinfo').trigger('click');
                                    } else {
                                       $('#todoinfo').find('.position-alert1').html("");
                                       $('#todoinfo').find('.position-alert1').html('Process Failed');
                                       $('.todoinfo').trigger('click');
                                    }
                                 }
                              });
                           } else if (recid != "") {
                              $.ajax({
                                 type: "POST",
                                 url: "<?php echo base_url(); ?>Firm_dashboard/todolist_edit",
                                 data: {
                                    'todo_name': todoname,
                                    'id': recid
                                 },

                                 success: function(data) {
                                    var json = JSON.parse(data);

                                    $(".LoadingImage").hide();
                                    $(".edittodo_view").hide();
                                    $("#EditTodo_" + $(this).attr('id') + "").modal('hide');
                                    $("#EditTodo_" + recid).find(".close").trigger('click');

                                    if (json['status'] == 1) {
                                       var content = json['content'];
                                       var complete_data = json['completed_content'];
                                       var view_all = json['view_all'];
                                       $(".sortable").html('');
                                       $(".sortable").append(content);
                                       $(".completed_todos").html('');
                                       $(".completed_todos").append(complete_data);
                                       $('div .tab-content').find('div #profile1').children('div .new-task').html("");
                                       $('div .tab-content').find('div #profile1').children('div .new-task').html(view_all);
                                       $('#todoinfo').find('.position-alert1').html("");
                                       $('#todoinfo').find('.position-alert1').html('Todo Updated Successfully!');
                                       $('.todoinfo').trigger('click');
                                    } else {
                                       $('#todoinfo').find('.position-alert1').html("");
                                       $('#todoinfo').find('.position-alert1').html('Process Failed');
                                       $('.todoinfo').trigger('click');
                                    }
                                 }
                              });
                           }
                        }
                     }
                  });
               } else {
                  $(er).html('Required.').css('color', 'red');
                  status = 0;
               }

               return true;
            }

            $("#todo_save").click(function() {
               var todo_name = $("#todo_name").val();
               var id = "";
               check_todoname(todo_name, id);
            });

            function todo_status(todo) {
               var id = $(todo).attr('id');

               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>Firm_dashboard/todos_status",
                  data: {
                     'id': id
                  },

                  beforeSend: function() {
                     $(".LoadingImage").show();
                  },
                  success: function(data) {
                     var json = JSON.parse(data);
                     $(".LoadingImage").hide();
                     if (json['status'] == 1) {
                        var content = json['content'];
                        var complete_data = json['completed_content'];
                        var view_all = json['view_all'];
                        $(".sortable").html('');
                        $(".sortable").append(content);
                        $(".completed_todos").html('');
                        $(".completed_todos").append(complete_data);
                        $('div .tab-content').find('div #profile1').children('div .new-task').html("");
                        $('div .tab-content').find('div #profile1').children('div .new-task').html(view_all);
                        $('#todoinfo').find('.position-alert1').html("");
                        $('#todoinfo').find('.position-alert1').html('Todo Status Updated Successfully!');
                        $('.todoinfo').trigger('click');
                     } else {
                        $('#todoinfo').find('.position-alert1').html("");
                        $('#todoinfo').find('.position-alert1').html('Process Failed');
                        $('.todoinfo').trigger('click');
                     }
                  }
               });
            }

            $(document).on('click', '.save', function() {
               var id = $(this).attr('id');
               var todo_name = $("div#EditTodo_" + $(this).attr('id') + " .todo_name").val();
               check_todoname(todo_name, id);
            });

            $(document).on('click', '.delete', function() {
               var id = $(this).attr('id');
               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>Firm_dashboard/todolist_delete",
                  data: {
                     'id': id
                  },

                  beforeSend: function() {
                     $(".LoadingImage").show();
                  },
                  success: function(data) {
                     $("#DeleteTodo_" + id).find('.modal-header').children('.close').trigger('click');
                     $(".LoadingImage").hide();

                     if (data == 1) {
                        $('#todoinfo').find('.position-alert1').html("");
                        $('#todoinfo').find('.position-alert1').html('Todo Deleted Successfully!');
                        $('.todoinfo').trigger('click');
                        setTimeout(function() {
                           location.reload();
                        }, 2000);
                     } else {
                        $('#todoinfo').find('.position-alert1').html("");
                        $('#todoinfo').find('.position-alert1').html('Process Failed');
                        $('.todoinfo').trigger('click');
                     }
                  }
               });
            });

            $(document).on('change', '#filter_client', function() {
               var id = $(this).val();

               if (id == 'month') {
                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Firm_dashboard/client_filter",
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(data) {
                        $(".fileters_client").html('');
                        $(".fileters_client").append(data);
                        $(".LoadingImage").hide();
                     }
                  });
               } else {
                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Firm_dashboard/client_filter_year",
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(data) {
                        $(".fileters_client").html('');
                        $(".fileters_client").html(data);
                        $(".LoadingImage").hide();
                     }
                  });
               }
            });

            $("#filter_deadline").change(function() {
               var id = $(this).val();
               if ($(this).val() == 'year') {
                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Firm_dashboard/deadline_filter_year",
                     data: {
                        'id': id
                     },
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(data) {
                        // console.log(data);   
                        $(".deadline").html('');
                        $(".deadline").append(data);
                        $(".LoadingImage").hide();
                     }
                  });
               } else {
                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Firm_dashboard/deadline_filter_month",
                     data: {
                        'id': id
                     },
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(data) {
                        // console.log(data);   
                        $(".deadline").html('');
                        $(".deadline").append(data);
                        $(".LoadingImage").hide();
                     }
                  });
               }
            });

            $("#proposal").change(function() {
               var id = $(this).val();
               if ($(this).val() == 'year') {
                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Firm_dashboard/filter_proposal_year",
                     data: {
                        'id': id
                     },
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(data) {
                        // console.log(data);   
                        $(".proposal").html('');
                        $(".proposal").append(data);
                        $(".LoadingImage").hide();
                     }
                  });
               } else {
                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Firm_dashboard/filter_proposal_month",
                     data: {
                        'id': id
                     },
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(data) {
                        // console.log(data);   
                        $(".proposal").html('');
                        $(".proposal").append(data);
                        $(".LoadingImage").hide();
                     }
                  });
               }
            });


            google.charts.load('current', {
               'packages': ['corechart']
            });
            google.charts.setOnLoadCallback(drawChart2);

            function drawChart2() {

               var data = google.visualization.arrayToDataTable([
                  ['Task', 'link', 'Hours per Day'],
                  ['Accepted', '<?php echo base_url(); ?>Firm_dashboard/accept', <?php echo $accept_proposal; ?>],
                  ['In Discussion', '<?php echo base_url(); ?>Firm_dashboard/indiscussion', <?php echo $indiscussion_proposal; ?>],
                  ['Sent', '<?php echo base_url(); ?>Firm_dashboard/sent', <?php echo $sent_proposal; ?>],
                  ['Viewed', '<?php echo base_url(); ?>Firm_dashboard/view', <?php echo $viewed_proposal; ?>],
                  ['Declined', '<?php echo base_url(); ?>Firm_dashboard/decline', <?php echo $declined_proposal; ?>],
                  ['Archived', '<?php echo base_url(); ?>Firm_dashboard/archieve', <?php echo $archive_proposal; ?>],
                  ['Draft', '<?php echo base_url(); ?>Firm_dashboard/draft', <?php echo $draft_proposal; ?>]
               ]);

               var view = new google.visualization.DataView(data);
               view.setColumns([0, 2]);

               var options = {
                  title: 'My Proposal Activities'
               };

               var chart = new google.visualization.PieChart(document.getElementById('piechart'));

               chart.draw(view, options);

               var selectHandler = function(e) {
                  //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
                  window.open(data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
               }
               // Add our selection handler.
               google.visualization.events.addListener(chart, 'select', selectHandler);
            }
         </script>
         <script type="text/javascript">
            google.charts.load("current", {
               packages: ["corechart"]
            });
            google.charts.setOnLoadCallback(drawChart1);

            function drawChart1() {
               var data = google.visualization.arrayToDataTable([
                  ['Status', 'link', 'Count'],
                  ['Active Client', '<?php echo base_url(); ?>Firm_dashboard/Active_client', <?php echo $active['client_count']; ?>],
                  ['Inactive Client', '<?php echo base_url(); ?>Firm_dashboard/Inactive_client', <?php echo $inactive['client_count']; ?>],
                  ['Frozen Client', '<?php echo base_url(); ?>Firm_dashboard/Frozen_client', <?php echo $frozen['client_count']; ?>],
               ]);
               var view = new google.visualization.DataView(data);
               view.setColumns([0, 2]);
               var options = {
                  title: 'Client Lists',
                  pieHole: 0.4,
               };
               var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
               chart.draw(view, options);
               var selectHandler = function(e) {
                  //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
                  window.open(data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
               }
               // Add our selection handler.
               google.visualization.events.addListener(chart, 'select', selectHandler);
            }
         </script>
         <p id="myValueHolder"></p>
         <?php
         //$this->load->view('Deadline_manager/reminder_invoice_popup'); 
         ?>

         <script type="text/javascript">
            $(document).on('change', ".deadline_status", function() {
               var values = this.id;
               var rid = values.split('_');

               if (this.value == 'enable') {
                  var stat = "on";
               } else {
                  var stat = "off";
               }

               $(".LoadingImage").show();
               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>Deadline_manager/service_update/",
                  data: {
                     'values': values,
                     'stat': stat
                  },
                  success: function(response) {
                     $(".LoadingImage").hide();
                     if (response == 1) {
                        $("#suc_" + rid[1]).html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>');
                        setTimeout(function() {
                           $("#suc_" + rid[1]).html('');
                        }, 2000);
                     } else {
                        $("#suc_" + rid[1]).html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span style="color:red;">' + response + '</span></div></div></div>');
                     }
                  },
               });

            });

            $(document).on('change', ".deadline_status_email", function() {
               var values = this.id;
               var rid = values.split('_');

               if (this.value == 'yes') {
                  var stat = "on";
               } else {
                  var stat = "off";
               }

               $(".LoadingImage").show();

               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>Deadline_manager/service_update_mail/",
                  data: {
                     'values': values,
                     'stat': stat
                  },
                  success: function(response) {
                     $(".LoadingImage").hide();
                     if (response == 1) {
                        $("#suc_" + rid[1]).html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>');
                        setTimeout(function() {
                           $("#suc_" + rid[1]).html('');
                        }, 2000);
                     } else {
                        $("#suc_" + rid[1]).html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span style="color:red;">' + response + '</span></div></div></div>');
                     }
                  },
               });
            });


            $(document).on('click', '.for_allactivity_section_ajax', function() {

               alert('ok');
               var user_id = '<?php echo $_SESSION['id']; ?>';

               $('.LoadingImage').show();
               $.ajax({
                  url: "<?php echo base_url(); ?>client/jquery_append_client_timeline_services/" + user_id + "/allactivity",
                  success: function(result) {
                     console.log(result);
                     $('#allactivitylog_sections_info').append(result);

                     console.log($('#allactivitylog_sections_info').html());

                     var mylist = $('#allactivitylog_sections_info').find('#services');
                     var listitems = $('#allactivitylog_sections_info').find('#services').children('.sorting').get();
                     listitems.sort(function(a, b) {
                        var compA = $(a).attr('id').toUpperCase();
                        var compB = $(b).attr('id').toUpperCase();
                        return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
                     })
                     $.each(listitems, function(idx, itm) {
                        mylist.append(itm);
                        //alert(itm);
                     });

                     setTimeout(function() {
                        $('.LoadingImage').hide();
                     }, 2000);
                  }

               });
               //$('.LoadingImage').hide();                

            });

            $(document).on('click', '#close_info_msg', function() {
               $('.popup_info_msg').hide();
            });

            $(document).ready(function() {
               $('.pull-left .nav-item a').click(function() {
                  $('.pull-left .nav-item a').removeClass('active');
                  $(this).addClass('active');
                  var tagid = $(this).data('tag');
                  $('.tab-pane').removeClass('active').addClass('hide');
                  $('#' + tagid).addClass('active').removeClass('hide');
               });

               <?php if (!empty($_SESSION['success_msg'])) { ?>

                  $(".reminder").first().find('a').trigger('click');

               <?php } ?>
            });
         </script>


         <!-- 
 <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> -->

         <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/chat.js"></script>
         <script type="text/javascript">
            $(document).ready(function() {

              
            $.ajax({
               url: '<?php echo base_url(); ?>firm_dashboard/task_counts',
               type: 'POST',
               data: {
                  'data' : 1,              
               },
               success: function(data) {
                  var user_tasks = JSON.parse(data);
                  var i = 101;
                  $('.service-chart-data').each(function(index, value) {
                     var service_id = $(this).attr('service-id');
                     var total = 0;
                     var pending = 0;
                     var query = 0;
                     var progress = 0;
                     var awaiting = 0;
                     var completed = 0;
                     $.each(user_tasks, function(key, value) {
                        var related_to_service = value.related_to_services;
                        var zero_element = 0;
                        var first_element = 1;
                        var services = related_to_service.split("_");
                        if(first_element in services){
                           var related_service_id = services[1];
                        }
                        else{
                           var related_service_id = services[0];
                        }
                        if(service_id == related_service_id){
                           total++;
                           if(value.task_status == 1){
                              pending++;
                           }
                           else if(value.task_status == 2){
                              progress++;
                           }
                           else if(value.task_status == 3){
                              awaiting++;
                           }
                           else if(value.task_status == 5){
                              completed++;
                           }
                           else{
                              query++;
                           }
                        }
                     });
                     $(this).find(".total-count-task").html(total);
                     $(this).find(".pending-count-task").html(pending);
                     $(this).find(".client-queried-count-task").html(query);
                     $(this).find(".progress-count-task").html(progress);
                     $(this).find(".await-count-task").html(awaiting);
                     $(this).find(".completed-count-task").html(completed);

                     var data = google.visualization.arrayToDataTable([
                        ['Task', 'Statuses'],
                        ['Pending', pending],
                        ['Client Query', query],
                        ['WIP', progress],
                        ['Approval Await', awaiting],
                        ['Completed', completed]
                     ]);

                     var options = {
                        title: 'Tasks',
                        pieHole: 0.7,
                        pieSliceTextStyle: {
                           color: 'black',
                        },
                        legend: 'none',
                        backgroundColor: 'transparent',
                        colors: ['#ee1a21','#808080','#ffc810','#a9cb40','#008000']
                     };
                        
                     var chart = new google.visualization.PieChart(document.getElementById('donut_single_'+i));
                     chart.draw(data, options);

                     i++;

                  });
               }
            });

            $.ajax({
               url: '<?php echo base_url(); ?>firm_dashboard/header_pie_charts_counts',
               type: 'POST',
               data: {
                  'data' : 1,              
               },
               success: function(data) {
                  var header_activity_counts = JSON.parse(data);
                  initialise_heder_pie_charts(header_activity_counts.total_tasks, header_activity_counts.user_total_tasks,'Task',header_activity_counts.user_name);
                  $('#total_task_count').html(header_activity_counts.total_tasks);
                  $('#total_user_task_count').html(header_activity_counts.user_name+'('+header_activity_counts.user_total_tasks+')');
                  initialise_heder_pie_charts(header_activity_counts.total_clients, header_activity_counts.user_total_clients,'Client',header_activity_counts.user_name);
                  $('#total_client_count').html(header_activity_counts.total_clients);
                  $('#total_user_client_count').html(header_activity_counts.user_name+'('+header_activity_counts.user_total_clients+')');
                  initialise_heder_pie_charts(header_activity_counts.total_leads, header_activity_counts.user_total_leads,'Lead',header_activity_counts.user_name);
                  $('#total_lead_count').html(header_activity_counts.total_leads);
                  $('#total_user_lead_count').html(header_activity_counts.user_name+'('+header_activity_counts.user_total_leads+')');
                  var total_time = header_activity_counts.total_time;
                  var total_user_time = header_activity_counts.total_user_time;
                  var total_display_time = secondsTimeSpanToHMS(total_time);
                  var total_user_display_time = secondsTimeSpanToHMS(total_user_time);
                  initialise_heder_pie_charts(total_time, total_user_time,'Time',header_activity_counts.user_name);
                  $('#total_time').html(total_display_time);
                  $('#total_user_time').html(header_activity_counts.user_name+'('+total_user_display_time+')');
               }
            });

            function initialise_heder_pie_charts(total_count, user_count, module_name, user_name){

               var user_count_percent = (user_count/total_count) * 100;
               var data = google.visualization.arrayToDataTable([
                        ['Module', module_name],
                        ['Total', 100],
                        [user_name, user_count_percent]
                     ]);

                     if(module_name == 'Task'){
                        var pie_colors = ['#fcdbb6','#f5982d'];
                        var pie_id = 'donut_single_tasks';
                     }
                     else if(module_name == 'Client'){ 
                        var pie_colors = ['#b3eafe','#02b2f2'];
                        var pie_id = 'donut_single_clients';
                     }  
                     else if(module_name == 'Lead'){
                        var pie_colors = ['#e9bbf7','#c143e9'];
                        var pie_id = 'donut_single_leads';
                     } 
                     else if(module_name == 'Time'){
                        var pie_colors = ['#dbe9af','#a9cb40'];
                        var pie_id = 'donut_single_time';
                     }
                     var options = {
                        title: module_name,
                        pieHole: 0.7,
                        pieSliceTextStyle: {
                           color: 'black',
                        },
                        legend: 'none',
                        backgroundColor: 'transparent',
                        colors: pie_colors
                     }; 
                     var chart = new google.visualization.PieChart(document.getElementById(pie_id));
                     chart.draw(data, options);
            }

            function secondsTimeSpanToHMS(s) {
               var h = Math.floor(s / 3600); //Get whole hours
               s -= h * 3600;
               var m = Math.floor(s / 60); //Get remaining minutes
               s -= m * 60;
               return h + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); 
            }
               

               $('.edit-toggle1').click(function() {
                  $('.proposal-down').slideToggle(300);
               });

               $("#legal_form12").change(function() {
                  var val = $(this).val();
                  if (val === "from2") {
                     // alert('hi');
                     $(".from2option").show();
                  } else {
                     $(".from2option").hide();
                  }
               });

               $(document).on('click', '.filter-btn-task-dash', function(){
                  val = $(this).data('id');
                  $('.chat_sec').html('');
                  $('.LoadingImage').show();
                  var data = {};
                  data['assigned_task'] = '<?php echo json_encode($assigned_task) ?>';
                  data['assigned_task_filter'] = val;
                  $.ajax({
                     url: '<?php echo base_url(); ?>Dashboard/task_class_value/',
                     type: 'POST',
                     dataType: 'json',
                     data: data,
                     success: function(data) {
                        //alert(data);
                        $('.LoadingImage').hide();

                        $('#Tasks').html('').html(data.task_class_details);
                        // console.log(data);

                     }
                  });
               });

               $('.task_class').click(function() {
                  $('.chat_sec').html('');
                  $('.LoadingImage').show();
                  var data = {};
                  data['assigned_task'] = '<?php echo json_encode($assigned_task) ?>';
                  data['assigned_task_filter'] = 'none';
                  $.ajax({
                     url: '<?php echo base_url(); ?>Dashboard/task_class_value/',
                     type: 'POST',
                     dataType: 'json',
                     data: data,
                     success: function(data) {
                        //alert(data);
                        $('.LoadingImage').hide();

                        $('#Tasks').html('').html(data.task_class_details);
                        // console.log(data);

                     }
                  });

               });

               $('.to_do_class').click(function() {
                  $('.chat_sec').html('');
                  $('.LoadingImage').show();
                  var data = {};
                  data['todolist'] = '<?php echo json_encode($todolist) ?>';
                  $.ajax({
                     url: '<?php echo base_url(); ?>Dashboard/todolist_class_value/',
                     type: 'POST',
                     dataType: 'json',
                     data: data,
                     success: function(data) {
                        console.log(data.task_class_details);
                        $('.LoadingImage').hide();

                        $('#Todolist').html('').html(data.task_class_details);
                        // console.log(data);

                     }
                  });

               });


               $('.client_class').click(function() {
                  $('.chat_sec').html('');
                  $('.LoadingImage').show();
                  var data = {};
                  data['client_userid'] = <?php echo json_encode($client_userid) ?>;
                  data['assigned_client'] = <?php echo json_encode($assigned_client) ?>;
                  $.ajax({
                     url: '<?php echo base_url(); ?>Dashboard/client_class_value/',
                     type: 'POST',
                     dataType: 'json',
                     data: data,
                     success: function(data) {
                        //alert(data);
                        $('.LoadingImage').hide();

                        $('#Clients').html('').html(data.client_class_details);
                        // console.log(data);

                     }
                  });

               });


               $('.deadline_class').click(function() {
                  $('.chat_sec').html('');
                  $('.LoadingImage').show();
                  var data = {};
                  data['client_userid'] = <?php echo json_encode($client_userid) ?>;
                  $.ajax({
                     url: '<?php echo base_url(); ?>Dashboard/deadline_class_value/',
                     type: 'POST',
                     dataType: 'json',
                     data: data,
                     success: function(data) {
                        //alert(data);
                        $('.LoadingImage').hide();

                        $('#Deadline-Manager').html('').html(data.deadline_class_details);
                        // console.log(data);

                     }
                  });

               });



               $('.reminder_class').click(function() {
                  $('.chat_sec').html('');
                  $('.LoadingImage').show();
                  var data = {};
                  data['client_userid'] = <?php echo json_encode($client_userid) ?>;
                  $.ajax({
                     url: '<?php echo base_url(); ?>Dashboard/reminder_class_value/',
                     type: 'POST',
                     dataType: 'json',
                     data: data,
                     success: function(data) {
                        //alert(data);
                        $('.LoadingImage').hide();

                        $('#Reminder').html('').html(data.reminder_class_details);
                        //console.log(data);

                     }
                  });

               });



               $('.zen-livechat').click(function() {
                  $('.LoadingImage').show();
                  $.ajax({
                     url: '<?php echo base_url(); ?>User_chat/index/1',
                     type: 'POST',
                     // dataType:'json',
                     // data : data,
                     success: function(data) {
                        //alert(data);
                        $('.LoadingImage').hide();

                        $('.chat_sec').html('').html(data);
                        chat_close_click();

                     }
                  });

               });

               function chat_close_click() {
                  $('.chat_close').click(function() {
                     $('.chat_sec').html('');
                  });

               }




               $("#legal_form12").change(function() {
                  var val = $(this).val();
                  if (val === "from2") {
                     // alert('hi');
                     $(".from2option").show();
                  } else {
                     $(".from2option").hide();
                  }
               });


               $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({
                  //  "scrollX": true
               });
            })
   
   if(window.localStorage.getItem('curdate_<?php echo $_SESSION['id']; ?>')!='<?php echo date('Y-m-d'); ?>'){
      $('#due_tasks').trigger('click');   
      window.localStorage.setItem("curdate_<?php echo $_SESSION['id']; ?>", '<?php echo date('Y-m-d'); ?>');
   }
   
   $(".due-to-start").change(function(){
      var id = $(this).val();
      
      if(this.checked) {
         $.ajax({
            url: '<?php echo base_url(); ?>task/task_initiated',
            type: 'POST',
            data: {
               'task_id' : id,
               'status'  : '1'               
            },
            success: function(data) {
               alert("Marked as started");
            }
         });
      }else{
         $.ajax({
            url: '<?php echo base_url(); ?>task/task_initiated',
            type: 'POST',
            data: {
               'task_id' : id,
               'status'  : '0'
            },
            success: function(data) {
               alert("Marked as not started");
            }
         });
      }      
   });
 
 </script>
 <!-- Charts Start -->
    <!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
    
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     60],
          ['',     40],
        ]);

        var options = {
          pieHole: 0.7,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#ee1a21','#e7c1c2']          
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_2'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#a9cb40']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_5'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#ee1a21']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_6'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#4580fe']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_7'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
         ['Language', 'Speakers (in millions)'],
          ['German',  5.85],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#4580fe']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_8'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#eeab54']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_9'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#75c94f']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_10'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#fd6186']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_11'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#eeab54']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_12'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#4580fe']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_13'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#a9cb40']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_14'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#ee1a21']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_15'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#4580fe']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_16'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#eeab54']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_17'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#4580fe']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_18'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#75c94f']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_19'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#fd6186']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_20'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#eeab54']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_21'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#4580fe']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_22'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#eeab54']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_23'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['',     100],
        ]);

        var options = {
          pieHole: 0.7,
         //  is3D: true,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none',
          backgroundColor: 'transparent',
          colors: ['#4580fe']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_24'));
        chart.draw(data, options);
      }
      $(document).on('click', '#clients_link, #leads_link', function(e) {
         e.preventDefault();
	      window.location.href = $(this).attr('data-href');
	    });

    </script>


         <!--    <script type="text/javascript" src="<?php echo base_url(); ?>bower_components/jquery/js/jquery.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.79/jquery.form-validator.js"></script> -->