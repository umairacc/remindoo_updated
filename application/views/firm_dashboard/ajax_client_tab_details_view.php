	<!-- <div class="invoice-crm1 disk-redesign1">
		<div class="top-leads fr-task cfssc redesign-lead-wrapper">
			<div class="lead-data1 pull-right leadsG leads_status_count">

				<div class="page-data-block">

					<div class="data-counts rem-card">

						<div class="data-count">
							<a href="<?php echo base_url(); ?>invoice">
								<span class="count"><?php echo count($invoice); ?></span>
								<p>Invoices</p>
							</a>
						</div>

						<div class="data-count">
							<a href="<?php echo base_url(); ?>invoice?type=1">
								<span class="count"><?php echo $approved_invoice; ?></span>
								<p>Paid Invoices</p>
							</a>
						</div>

						<div class="data-count">
							<a href="<?php echo base_url(); ?>invoice?type=2">
								<span class="count"><?php echo $unpaid_invoice; ?></span>
								<p>Unpaid Invoices</p>
							</a>
						</div>

						<div class="data-count">
							<a href="<?php echo base_url(); ?>invoice?type=3">
								<span class="count"><?php echo $cancel_invoice; ?></span>
								<p>Cancelled Invoices</p>
							</a>
						</div>

					</div>

				</div>



			</div>
		</div>
	</div> -->

	<div class="desk-dashboard mydisk-realign1 floating_set dashboard_client_tab">
		<div class="page-data-block">
			<div class="rem-card impo">
				<div class="number-mydisk floating_set">
					<div class="count-mydisk1">
						<div class="target-disk">
							<a href="<?php echo base_url(); ?>Firm_dashboard/All_client">
								<span>Total Clients</span>
								<div class="count-pos-01">
									<strong><?php echo count($client); ?></strong>
								</div>
							</a>
						</div>
					</div>

					<div class="count-mydisk1">
						<div class="target-disk">
							<a href="<?php echo base_url(); ?>Firm_dashboard/Active_client">
								<span>Active Clients</span>
								<div class="count-pos-01">
									<strong><?php echo $active_client; ?></strong>
								</div>
							</a>
						</div>
					</div>


					<div class="count-mydisk1">
						<div class="target-disk">
							<a href="<?php echo base_url(); ?>Firm_dashboard/Frozen_client">
								<span>Frozen Clients</span>
								<div class="count-pos-01">
									<strong><?php echo $frozen_client; ?></strong>
								</div>
							</a>
						</div>
					</div>

					<div class="count-mydisk1">
						<div class="target-disk">
							<a href="<?php echo base_url(); ?>Firm_dashboard/Inactive_client">
								<span>Inactive Clients</span>
								<div class="count-pos-01">
									<strong><?php echo $inactive_client; ?></strong>
								</div>
							</a>
						</div>
					</div>

					<div class="count-mydisk1">
						<div class="target-disk">
							<a href="<?php echo base_url(); ?>user">
								<span>Added This Month</span>
								<div class="count-pos-01">
									<strong><?php echo $new_client; ?></strong>
								</div>
							</a>
						</div>
					</div>

				</div>

				<div class="card desk-dashboard fr-bg12 widmar col-xs-12 col-md-6 mydisk-realign1 client-frame">
					<div class="card">
						<div class="card-header">
							<h5>Clients</h5>
						</div>
						<span class="time-frame"><label>Time Frame:</label>
							<select id="filter_client">
								<option value="year">This Year</option>
								<option value="month">This Month</option>
							</select>
							<div class="card-block fileters_client">
								<div id="chart_Donut11" style="width: 100%; height: 309px;"></div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		google.charts.load("current", {
			packages: ["corechart"]
		});
		google.charts.setOnLoadCallback(drawChart1);

		function drawChart1() {
			var data = google.visualization.arrayToDataTable([
				['Status', 'link', 'Count'],
				['Active Client', '<?php echo base_url(); ?>Firm_dashboard/Active_client', <?php echo $active; ?>],
				['Inactive Client', '<?php echo base_url(); ?>Firm_dashboard/Inactive_client', <?php echo $inactive; ?>],
				['Frozen Client', '<?php echo base_url(); ?>Firm_dashboard/Frozen_client', <?php echo $frozen; ?>],
			]);
			var view = new google.visualization.DataView(data);
			view.setColumns([0, 2]);
			var options = {
				title: 'Client Lists',
				pieHole: 0.4,
			};
			var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
			chart.draw(view, options);
			var selectHandler = function(e) {
				//  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
				window.open(data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
			}
			// Add our selection handler.
			google.visualization.events.addListener(chart, 'select', selectHandler);
		}
	</script>