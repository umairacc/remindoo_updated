 <div id="chart_Donut11" style="width: 100%; height: 300px;"></div>




<script type="text/javascript">

      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart1);
      function drawChart1() {
         var data = google.visualization.arrayToDataTable([
       ['Status','link','Count'],
       ['Active Client','<?php echo base_url(); ?>Firm_dashboard/Active_client',<?php echo $active['client_count']; ?>],
       ['Inactive Client', '<?php echo base_url(); ?>Firm_dashboard/Inactive_client',<?php echo $inactive['client_count']; ?>],
       ['Frozen Client', '<?php echo base_url(); ?>Firm_dashboard/Frozen_client', <?php echo $frozen['client_count']; ?>],
     ]);

     var view = new google.visualization.DataView(data);
        view.setColumns([0, 2]);
  
    var options = {
      title: 'Client Lists',
      pieHole: 0.4,
    };
  
    var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
      chart.draw(view, options);

        var selectHandler = function(e) {
       //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
         window.open( data.getValue(chart.getSelection()[0]['row'], 1), options.title, "_blank");
        }
        // Add our selection handler.
        google.visualization.events.addListener(chart, 'select', selectHandler);
  }
</script>