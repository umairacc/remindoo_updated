  <div class=" job-card">

     <div class="row">
        <div class="col-md-12">
           <div class="page-data-block">
              <div class="rem-card">
                 <?php

                  $deadlines = array();

                  $service_list = $this->Common_mdl->getServicelist();
                  // $service_charges = $this->Common_mdl->getClientsServiceCharge($getCompany); 
                  $current_date = date('Y-m-d');
                  $columns_check = array('1' => 'crm_confirmation_statement_due_date', '2' => 'crm_ch_accounts_next_due', '3' => 'crm_accounts_tax_date_hmrc', '4' => 'crm_personal_due_date_return', '5' => 'crm_vat_due_date', '6' => 'crm_rti_deadline', '7' => 'crm_pension_subm_due_date', '8' => '', '9' => '', '10' => 'crm_next_booking_date', '11' => 'crm_next_p11d_return_due', '12' => 'crm_next_manage_acc_date', '13' => 'crm_insurance_renew_date', '14' => 'crm_registered_renew_date', '15' => 'crm_investigation_end_date', '16' => 'crm_investigation_end_date');

                  $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

                  foreach ($other_services as $key => $value) {
                     $columns_check[$value['id']] = 'crm_' . $value['services_subnames'] . '_statement_date';
                  }


                  $icpfont_class = array('icofont-presentation-alt st-icon bg-c-yellow', 'icofont-files st-icon bg-c-blue', 'icofont-paper st-icon bg-c-green', 'icofont-file-document st-icon bg-c-pink', 'icofont-file-text st-icon bg-c-yellow', 'icofont-file-document st-icon bg-c-blue');
                  $count = 0;
                  foreach ($service_list as $key => $value) {

                     if ($count / 6 == '1') {
                        $count = 0;
                     }
                  ?>

                    <div class="col-md-12 col-xl-4">
                       <div class="card widget-statstic-card">
                          <div class="card-header">
                             <div class="media">
                                <div class="media-body media-middle">
                                   <div class="company-name">
                                      <p><a href="<?php echo base_url() . 'Firm_dashboard/service/' . $value['id']; ?>"><?php echo $value['service_name']; ?></a></p>
                                   </div>
                                </div>
                             </div>
                          </div>
                          <div class="card-block">
                             <i class="icofont <?php echo $icpfont_class[$count]; ?>"></i>
                             <ul class="text-muted">
                                <?php
                                 foreach ($oneyear as $date) {
                                    // echo $date['crm_vat_due_date']."-fgf-";
                                    if ($date[$columns_check[$value['id']]] != '') { ?>
                                      <li><?php echo $date['crm_company_name']; ?>-<?php echo (strtotime($date[$columns_check[$value['id']]])) != '' ?  date('Y-m-d', strtotime($date[$columns_check[$value['id']]])) : '0'; ?></li>
                                <?php }
                                 } ?>
                             </ul>
                          </div>
                       </div>
                    </div>

                 <?php $count++;
                  } ?>
              </div>
           </div>
        </div>
     </div>
  </div>
  <!-- /Job Card -->


  <!-- fullwidth map -->
  <div class="floating_set set-timenone" style="display: none">
     <div class="desk-dashboard widmar floating_set mydisk-realign1">
        <div class="card-header">
           <h5>Deadlines</h5>
        </div>
        <span class="time-frame"><label>Time Frame:</label>
           <select id="filter_deadline">
              <option value="month">This Month</option>
              <option value="year">This Year</option>
           </select>
           <div class="card-block deadline">
              <div id="columnchart_values" style="width: 100%; height: 410px;"></div>
           </div>

     </div>
     <!-- fullwidth map-->
  </div>
  <!--tab end-->
  </div>
  <script type="text/javascript">
     function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'Value');

        <?php

         $deadlines_arr = "";

         foreach ($deadlines as $key => $value) {
            $deadlines_arr .= '["' . $key . '",' . $value['deadline_count'] . '],';
         }

         $deadlines_arr = substr_replace($deadlines_arr, '', -1);

         ?>

        data.addRows([<?php echo $deadlines_arr; ?>]);


        // Set chart options  
        var chart = new google.visualization.ColumnChart(document.querySelector('#columnchart_values'));

        google.visualization.events.addListener(chart, 'select', function() {
           var selection = chart.getSelection();
           if (selection.length) {

              var row = selection[0].row;
              document.querySelector('#myValueHolder').innerHTML = data.getValue(row, 1);

              var view = new google.visualization.DataView(data);
              view.setColumns([0, 1, {
                 type: 'string',
                 role: 'style',
                 calc: function(dt, i) {
                    // alert(row);
                    if (i == row) {
                       load(row);
                    }
                    return (i == row) ? '' : null;
                 }
              }]);

              chart.draw(view, {
                 height: 460,
                 width: 600
              });
           }
        });

        chart.draw(data, {
           height: 460,
           width: 1200
        });
     }
     google.load('visualization', '1', {
        packages: ['corechart'],
        callback: drawChart
     });

     function load(row) {
        //alert(row);
        var index = row;
        switch (index) {
           case 0:
              window.open('<?php echo base_url(); ?>Firm_dashboard/confirm_statement', '_blank');
              break;
           case 1:
              window.open('<?php echo base_url(); ?>Firm_dashboard/accounts', '_blank');
              break;
           case 2:
              window.open('<?php echo base_url(); ?>Firm_dashboard/personal_tax', '_blank');
              break;
           case 3:
              window.open('<?php echo base_url(); ?>Firm_dashboard/vat', '_blank');
              break;
           case 4:
              window.open('<?php echo base_url(); ?>Firm_dashboard/payroll', '_blank');
              break;
           case 5:
              window.open('<?php echo base_url(); ?>Firm_dashboard/workplace', '_blank');
              break;
           case 6:
              window.open('<?php echo base_url(); ?>Firm_dashboard/cis', '_blank');
              break;
           case 7:
              window.open('<?php echo base_url(); ?>Firm_dashboard/cis_sub', '_blank');
              break;
           case 8:
              window.open('<?php echo base_url(); ?>Firm_dashboard/p11d', '_blank');
              break;
           case 9:
              window.open('<?php echo base_url(); ?>Firm_dashboard/management', '_blank');
              break;
           case 10:
              window.open('<?php echo base_url(); ?>Firm_dashboard/bookkeeping', '_blank');
              break;
           case 11:
              window.open('<?php echo base_url(); ?>Firm_dashboard/investication', '_blank');
              break;
           case 12:
              window.open('<?php echo base_url(); ?>Firm_dashboard/register', '_blank');
              break;
           case 13:
              window.open('<?php echo base_url(); ?>Firm_dashboard/tax', '_blank');
              break;
           case 14:
              window.open('<?php echo base_url(); ?>Firm_dashboard/tax_inves', '_blank');
              break;
        }
     }
  </script>