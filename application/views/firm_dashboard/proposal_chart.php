<div id="piechart" style="width: 100%; height: 300px;"></div>
<script type="text/javascript">
	         google.charts.load('current', {'packages':['corechart']});
     google.charts.setOnLoadCallback(drawChart2);
  
     function drawChart2() {
  
        var data = google.visualization.arrayToDataTable([
          ['Task', 'link','Hours per Day'],
          ['Accepted',  '<?php echo base_url(); ?>Firm_dashboard/accept',  <?php echo count($accept_proposal); ?>],
          ['In Discussion',  '<?php echo base_url(); ?>Firm_dashboard/indiscussion',  <?php echo count($indiscussion_proposal); ?>],
          ['Sent', '<?php echo base_url(); ?>Firm_dashboard/sent', <?php echo count($sent_proposal); ?>],
          ['Viewed', '<?php echo base_url(); ?>Firm_dashboard/view', <?php echo count($viewed_proposal); ?>],
          ['Declined',  '<?php echo base_url(); ?>Firm_dashboard/decline',  <?php echo count($declined_proposal); ?>],
          ['Archived',   '<?php echo base_url(); ?>Firm_dashboard/archieve', <?php echo count($archive_proposal); ?>],
          ['Draft',   '<?php echo base_url(); ?>Firm_dashboard/draft', <?php echo count($draft_proposal); ?>]
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 2]);
  
       var options = {
         title: 'My Proposal Activities'
       };
  
       var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  
        chart.draw(view, options);

        var selectHandler = function(e) {
       //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
         window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
        }
        // Add our selection handler.
        google.visualization.events.addListener(chart, 'select', selectHandler);
     }
  
  
</script>
