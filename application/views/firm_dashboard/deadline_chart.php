
<div id="columnchart_values" style="width: 100%; height: 350px;"></div>


<script type="text/javascript">
 function drawChart () {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', 'Value');

    <?php 

    $deadlines_arr = "";

    foreach($deadlines as $key => $value) 
    { 
       $deadlines_arr .= '["'.$key.'",'.$value['deadline_count'].'],';
    }  

    $deadlines_arr = substr_replace($deadlines_arr,'', -1);
   
    ?>
    
    data.addRows([<?php echo $deadlines_arr; ?>]); 

    // Set chart options
   

    
    var chart = new google.visualization.ColumnChart(document.querySelector('#columnchart_values'));
    
    google.visualization.events.addListener(chart, 'select', function () {
        var selection = chart.getSelection();
        if (selection.length) { 
      
            var row = selection[0].row;
            document.querySelector('#myValueHolder').innerHTML = data.getValue(row, 1);
            
            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1, {
                type: 'string',
                role: 'style',
                calc: function (dt, i) {
                 // alert(row);
                 if(i == row){
                  load(row);
               }
                    return (i == row) ? '' : null;
                }
            }]);
            
            chart.draw(view, {
                height: 400,
                width: 600
            });
        }
    });
    
    chart.draw(data, {
        height: 400,
        width: 1200
    });
}
google.load('visualization', '1', {packages:['corechart'], callback: drawChart});
  </script>
