				<div class="invoice-crm1 disk-redesign1">
				   <div class="top-leads fr-task cfssc redesign-lead-wrapper">
				      <div class="lead-data1 pull-right leadsG leads_status_count">

				         <!-- <div class="page-data-block">

				            <div class="data-counts rem-card">

				               <div class="data-count">
				                  <a href="<?php echo base_url(); ?>user/task_list">
				                     <span class="count"><?php echo $tasks['task_count']; ?></span>
				                     <p>Tasks</p>
				                  </a>
				               </div>

				               <div class="data-count">
				                  <a href="<?php echo base_url(); ?>Firm_dashboard/Complete">
				                     <span class="count"><?php echo $closed_tasks['task_count']; ?></span>
				                     <p>Completed Tasks</p>
				                  </a>
				               </div>

				               <div class="data-count">
				                  <a href="<?php echo base_url(); ?>Firm_dashboard/inprogress">
				                     <span class="count"><?php echo $pending_tasks['task_count']; ?></span>
				                     <p>Inprogress Tasks</p>
				                  </a>
				               </div>

				               <div class="data-count">
				                  <a href="<?php echo base_url(); ?>Firm_dashboard/notstarted">
				                     <span class="count"><?php echo $cancelled_tasks['task_count']; ?></span>
				                     <p>Not Started Tasks</p>
				                  </a>
				               </div>

				            </div>

				         </div> -->



				      </div>
				   </div>
				</div>


				<div class="cards12 space-disk1 col-xs-12 mydisk-realign1">
				   <div class="page-data-block lf-mytodo  mydisk-view pt0 my-to-do" <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_todo_items');
                                                                        if ($query == 0) { ?> style="display:none;" <?php } else { ?>proposal checking<?php } ?>>
				      <!-- To Do Card List card start -->
				      <div class="rem-card fr-hgt">
				         <div class="rem-card-header">
				            <h5 class="rem-card-titile">My To Do items</h5>
				            <ul class="nav nav-tabs  tabs" role="tablist">
				               <li class="nav-item">
				                  <a class="active rem-btn" data-toggle="tab" href="#home1" role="tab">New To Do</a>
				               </li>
				               <li class="nav-item">
				                  <a class="rem-btn" data-toggle="tab" href="#profile1" role="tab">View All</a>
				               </li>
				            </ul>
				         </div>
				         <div class="widnewtodo">
				            <div class="tab-content tabs card-block">
				               <div class="tab-pane in hide active" id="home1" role="tabpanel">
				                  <div class="new-task align-todos" id="draggablePanelList">
				                     <div class="no-todo">

				                        <h5 class="bg-c-green12"> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Latest to do's <button type="button" class="rem-btn f-right" data-toggle="modal" data-target="#myTodo">New Todo's</button></h5>

				                        <div class="sortable edit_after" id="5">
				                           <?php
                                       foreach ($todo_list as $todo) { ?>
				                              <div class="to-do-list card-sub" id="<?php echo $todo['id']; ?>">
				                                 <div class="top-disk01">
				                                    <label class="check-task custom_checkbox1">
				                                       <input type="checkbox" value="" id="<?php echo $todo['id']; ?>" onclick="todo_status(this)"> <i></i> </label>

				                                    <span><?php echo ucwords($todo['todo_name']); ?></span>
				                                 </div>
				                                 <div class="f-right">
				                                    <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
				                                    <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
				                                 </div>
				                              </div>
				                           <?php } ?>
				                        </div>
				                     </div>
				                     <h5 class="bg-c-green12"> <i class="fa fa-check"></i> Latest finished to do's</h5>
				                     <div class="completed_todos">
				                        <?php
                                    foreach ($todo_completedlist as $todo) { ?>
				                           <div class="to-do-list card-sub" id="1">
				                              <div class="top-disk01">
				                                 <label class="check-task done-task custom_checkbox1">

				                                    <input type="checkbox" value="" checked="checked" id="<?php echo $todo['id']; ?>" onclick="todo_status(this)"> <i></i> </label>

				                                 <span><?php echo ucwords($todo['todo_name']); ?></span>

				                              </div>
				                              <div class="f-right">
				                                 <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
				                                 <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
				                              </div>
				                           </div>
				                        <?php } ?>
				                        <!--  <p>No todos found</p> -->
				                     </div>
				                  </div>
				               </div>
				               <div class="tab-pane" id="profile1" role="tabpanel">
				                  <div class="new-task">
				                     <?php
                                 foreach ($todolist as $todo) { ?>
				                        <div class="to-do-list sortable-moves">
				                           <div class="to-do-list card-sub" id="1">
				                              <div class="top-disk01">
				                                 <label class="custom_checkbox1 check-task <?php if ($todo['status'] == 'completed') { ?> done-task <?php } ?>">
				                                    <input type="checkbox" value="" <?php if ($todo['status'] == 'completed') { ?> checked="checked" <?php } ?> id="<?php echo $todo['id']; ?>" onclick="todo_status(this)"> <i></i></label>

				                                 <span><?php echo ucwords($todo['todo_name']); ?></span>

				                              </div>
				                              <div class="f-right">
				                                 <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
				                                 <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
				                              </div>
				                           </div>
				                        </div>
				                     <?php } ?>
				                  </div>
				               </div>
				            </div>
				         </div>
				      </div>
				      <!-- To Do Card List card end -->
				   </div>

				</div>