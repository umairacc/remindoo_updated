<?php //echo $_SESSION['id']; 
?>

<?php $this->load->view('includes/new_header');
$succ = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$userid = $this->session->userdata('id');

foreach ($unpaid_invoice as $key => $value) {
   if ($value['invoice_duedate'] == 'Expired') {
      unset($unpaid_invoice[$key]);
   }
}

?>
<link href="https://remindoo.uk//assets/css/timeline-style.css" type="text/css" rel="stylesheet" />
<style type="text/css">
   div#sidebar.chat_particular {
      right: 0;
      bottom: 0;
      left: auto;
      position: fixed;
      max-height: 400px;
      top: auto;
      overflow: auto;
      width: 310px;
      box-shadow: 0 1px 11px 0 rgba(0, 0, 0, 0.12);
      border: none;
      z-index: 999;
      background: #fff;
      height: initial;
   }

   .card.card_main.p-fixed.users-main {
      top: 0;
      max-height: initial;
      overflow: initial;
      position: relative;
      border: none;
      box-shadow: initial;
      width: 100%;
   }

   .chat-page .chat-box {
      margin-right: 0;
      margin-left: 0;
      right: 320px;
   }

   .ui-widget.ui-widget-content {
      right: 20px !important;
      left: auto !important;
   }

   .to-do-list.sortable-moves {
      padding-left: 0;
   }

   .pcoded-content {
      float: left !important;
      width: 100%;
   }

   .hide_classes {
      display: none !important;
   }

   .hide_check {
      display: none !important;
   }

   .active {
      display: block !important;
   }

   /*.card
   {
     width: 500px;
     margin: auto;
   }   */

   .breadcrumb-header span {
      color: green;
   }
</style>
<?php
$service_list = $this->Common_mdl->getServicelist();
$service_charges = $this->Common_mdl->getClientsServiceCharge($getCompany);
$current_date = date('Y-m-d');
$columns_check = array('1' => 'crm_confirmation_statement_due_date', '2' => 'crm_ch_accounts_next_due', '3' => 'crm_accounts_tax_date_hmrc', '4' => 'crm_personal_due_date_return', '5' => 'crm_vat_due_date', '6' => 'crm_rti_deadline', '7' => 'crm_pension_subm_due_date', '8' => '', '9' => '', '10' => 'crm_next_booking_date', '11' => 'crm_next_p11d_return_due', '12' => 'crm_next_manage_acc_date', '13' => 'crm_insurance_renew_date', '14' => 'crm_registered_renew_date', '15' => 'crm_investigation_end_date', '16' => 'crm_investigation_end_date');

$other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

foreach ($other_services as $key => $value) {
   $columns_check[$value['id']] = 'crm_' . $value['services_subnames'] . '_statement_date';
}
?>
<div class="modal fade" id="todoinfo" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="position-alert1"></div>
         </div>
      </div>
   </div>
</div>
<button type="button" class="todoinfo" data-toggle="modal" data-target="#todoinfo" style="display: none;"></button>
<!-- management block -->

<div class="modal-alertsuccess alert succ popup_info_msg" style="<?php if ($_SESSION['success_msg'] != "") {
                                                                     echo "display: block";
                                                                  } else {
                                                                     echo "display: none";
                                                                  } ?>">
   <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
      <div class="pop-realted1">
         <div class="position-alert1">
            <?php if ($_SESSION['success_msg'] != "") {
               echo $_SESSION['success_msg'];
            } ?>
         </div>
      </div>
   </div>
</div>

<div class="pcoded-content pcode-mydisk">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body myDesk">
         <div class="page-wrapper">
            <div class="card desk-dashboard">
               <div class="page-heading">My Desk</div>
               <div class="inner-tab123">
                  <div class="deadline-crm1 floating_set">
                     <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard" id="proposal_dashboard">
                        <li class="nav-item <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_basic_details');
                                             if ($query == 0) { ?>hide_check<?php } else { ?>basic active checking<?php } ?>">
                           <!-- <a class="nav-link" data-toggle="tab" href="#Basics"> -->
                           <a href="javascript:;" class="nav-link active" data-tag="Basics">
                              Basics
                           </a>
                        </li>
                        <li class="nav-item <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_task_details');
                                             if ($query == 0) { ?>hide_check<?php } else { ?>task checking<?php } ?>">
                           <!-- <a class="nav-link" data-toggle="tab" href="#Tasks"> -->
                           <a href="javascript:;" class="nav-link tasks" data-tag="Tasks">
                              Tasks
                           </a>
                        </li>
                        <li class="nav-item <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_client_details');
                                             if ($query == 0) { ?>hide_check<?php } else { ?>client checking<?php } ?>">
                           <!-- <a class="nav-link" data-toggle="tab" href="#Clients"> -->
                           <a href="javascript:;" class="nav-link" data-tag="Clients">
                              Clients
                           </a>
                        </li>
                        <li class="nav-item <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_proposal');
                                             if ($query == 0) { ?>hide_check<?php } else { ?>proposals checking<?php } ?>">
                           <!-- <a class="nav-link" data-toggle="tab"  href="#Proposals"> -->
                           <a href="javascript:;" class="nav-link" data-tag="Proposals">
                              Proposals
                           </a>
                        </li>
                        <li class="nav-item <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_deadline_details');
                                             if ($query == 0) { ?>hide_check<?php } else { ?>  checking <?php } ?>">
                           <!-- <a class="nav-link" data-toggle="tab"  href="#Deadline-Manager"> -->
                           <a href="javascript:;" class="nav-link deads" data-tag="Deadline-Manager">
                              Deadline Manager
                           </a>
                        </li>
                        <li class="nav-item <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_reminder_details');
                                             if ($query == 0) { ?>hide_check<?php } else { ?>reminder checking <?php } ?>" id="rem">
                           <!-- <a class="nav-link" data-toggle="tab"  href="#Reminder"> -->
                           <a href="javascript:;" class="nav-link" data-tag="Reminder">
                              Reminder
                           </a>
                        </li>

                        <li class="nav-item  <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_timeline');
                                             if ($query == 0) { ?>hide_check<?php } else { ?>Timeline checking <?php } ?>">
                           <!-- <a class="nav-link for_tabs for_allactivity_section_ajax" data-toggle="tab"  href="#newtimelineservices">  -->
                           <a href="javascript:;" class="nav-link  for_tabs for_allactivity_section_ajax" data-tag="newtimelineservices">
                              Timeline Services</a></li>
                     </ul>
                  </div>
                  <div class="tab-content">

                     <div id="newtimelineservices" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_timeline');
                                                                        if ($query == 0) { ?>hide_classes<?php } else { ?> active <?php } ?>">
                        <div class="card-block" id="allactivitylog_sections_info">
                        </div>
                     </div>


                     <!--Reminder Tab-->
                     <div id="Reminder" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_reminder_details');
                                                               if ($query == 0) { ?>hide_classes<?php } else { ?>  <?php } ?>">

                        <?php $this->load->view('Deadline_manager/reminder_invoice_table', array('service_list' => $service_list, 'service_charges' => $service_charges)); ?>

                     </div>


                     <!-- Reminder Tab -->

                     <!-- tab1 -->
                     <div id="Basics" class="proposal_send tab-pane fade in <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_basic_details');
                                                                              if ($query == 0) { ?>hide_classes<?php } else { ?> active <?php } ?>">
                        <!-- pro summary -->

                        <div class="invoice-crm1 disk-redesign1">
                           <div class="top-leads fr-task cfssc redesign-lead-wrapper">
                              <div class="lead-data1 pull-right leadsG leads_status_count">

                                 <div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
                                    <div class="lead-point1">
                                       <a href="<?php echo base_url(); ?>Firm_dashboard/Active_client">
                                          <strong><?php echo ($active_client['client_count']); ?></strong>
                                          <span class="status_filter">Active Client(s)</span>
                                       </a>
                                    </div>
                                 </div>

                                 <div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
                                    <div class="lead-point1">
                                       <a href="<?php echo base_url(); ?>Firm_dashboard/Complete">
                                          <strong><?php echo ($closed_tasks['task_count']); ?></strong>
                                          <span class="status_filter">Completed task</span>
                                       </a>
                                    </div>
                                 </div>

                                 <div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
                                    <div class="lead-point1">
                                       <a href="<?php echo base_url(); ?>Firm_dashboard/Inactive_client">
                                          <strong><?php echo ($inactive_client['client_count']); ?></strong>
                                          <span class="status_filter">Inactive Client(s)</span>
                                       </a>
                                    </div>
                                 </div>

                                 <div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
                                    <div class="lead-point1">
                                       <a href="<?php echo base_url(); ?>leads">
                                          <strong><?php echo (count($total_leads)); ?></strong>
                                          <span class="status_filter">Over All Leads</span>
                                       </a>
                                    </div>
                                 </div>


                              </div>
                           </div>
                        </div>

                        <div class="floating_set space-disk1">
                           <div class="col-md-12 col-xl-6">
                              <div class="card">
                                 <div class="card-block user-detail-card">
                                    <div class="row">
                                       <div class="col-sm-4">
                                          <div class="height-profile">
                                             <div class="height-profile1"> <?php $getUserProfilepic = $this->Common_mdl->getUserProfilepic($userid); ?><img src="<?php echo $getUserProfilepic; ?> " alt="" class="img-fluid p-b-10"></div>
                                          </div>
                                       </div>
                                       <div class="col-sm-8 user-detail">
                                          <div class="row">
                                             <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Name :</h6>
                                             </div>
                                             <div class="col-sm-7">
                                                <h6 class="m-b-30"><?php
                                                                     $user_role = $this->Common_mdl->select_record('user', 'id', $userid);
                                                                     echo ucwords($user_role['crm_name']); ?></h6>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-email"></i>EMP ID</h6>
                                             </div>
                                             <div class="col-sm-7">
                                                <h6 class="m-b-30"><a href="mailto:dummy@example.com"><?php echo '#' . $userid; ?></a></h6>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-home"></i>Role ID :</h6>
                                             </div>
                                             <div class="col-sm-7">
                                                <h6 class="m-b-30"><?php echo ucfirst($this->Common_mdl->get_price('organisation_tree', 'user_id', $userid, 'title')); ?></h6>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-touch-phone"></i>Phone :</h6>
                                             </div>
                                             <div class="col-sm-7">
                                                <h6 class="m-b-30"> <?php echo $user_role['crm_phone_number']; ?></h6>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30"><i class="icofont icofont-web"></i>Website :</h6>
                                             </div>
                                             <div class="col-sm-7">
                                                <h6 class="m-b-30"><a href="<?php echo $user_role['crm_business_website']; ?>"><?php echo $user_role['crm_business_website']; ?></a></h6>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- Summery Start -->

                           <!-- summary end -->
                        </div>
                        <!-- pro summary -->

                        <!--Start chat -->
                        <div class="pcoded-content">
                           <div class="pcoded-inner-content chat-page">
                              <!-- Main-body start -->
                              <div class="main-body">
                                 <div class="page-wrapper">
                                    <div class="deadline-block">
                                       <div id="main-chat" class="container-fluid">

                                          <div class="page-body">
                                             <div class="row">
                                                <div class="chat-box">
                                                   <ul class="text-right boxs">

                                                   </ul>
                                                   <div id="sidebar" class="users p-chat-user chat_particular">
                                                      <div class="had-container">
                                                         <div class="card card_main p-fixed users-main ">
                                                            <div class="user-box">
                                                               <div class="card-block">
                                                                  <div class="right-icon-control">
                                                                     <input type="text" class="form-control  search-text" placeholder="Search Friend">
                                                                     <div class="form-icon">
                                                                        <i class="icofont icofont-search"></i>
                                                                     </div>
                                                                  </div>
                                                               </div>

                                                               <div class="user-groups group_section">
                                                                  <h6>Groups</h6>

                                                                  <!-- for member of the group -->
                                                                  <div class="created_group_list">
                                                                     <div class="group_append"></div>
                                                                     <?php $group_data = $this->db->query("SELECT * FROM chat_group where find_in_set('" . $_SESSION['id'] . "',members)  and status='' and firm_id=" . $_SESSION['firm_id'] . "")->result_array(); ?>
                                                                     <?php foreach ($group_data as $key => $value) {
                                                                        $group_member_data = $this->db->query("SELECT * FROM user where id in (" . $value['members'] . "," . $value['create_by'] . ") ")->result_array();
                                                                        $member_names = array();
                                                                        foreach ($group_member_data as $men_key => $mem_value) {
                                                                           array_push($member_names, $mem_value['crm_name']);
                                                                        }
                                                                     ?>
                                                                        <div class="media userlist-box box_<?php echo $value['id']; ?>" data-id="<?php echo $value['id']; ?>" data-username="<?php echo $value['groupname']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $value['groupname']; ?>" data-group="group" data-member="<?php echo implode(',', $member_names); ?>" data-creater="<?php echo $value['create_by']; ?>" data-session="<?php echo $_SESSION['id']; ?>">
                                                                           <div class="media-body">
                                                                              <div class="f-13 chat-header"><?php echo $value['groupname']; ?></div>
                                                                           </div>
                                                                           <div class="badge badge-primary new_message_count_<?php echo $value['id']; ?>"></div>
                                                                        </div>
                                                                     <?php
                                                                     } ?>
                                                                  </div>

                                                               </div>





                                                               <!-- for admin and support -->

                                                               <?php if (!empty($getallstaff) && count($getallstaff) > 0) { ?>
                                                                  <!-- end of admin support -->
                                                                  <div class="user-groups staff_section">

                                                                     <h6>Staff(s)</h6>
                                                                     <!-- for staff -->

                                                                     <div class="staff_append"></div>
                                                                     <?php
                                                                     $i = 1;

                                                                     $newTime = strtotime('-10 minutes');


                                                                     foreach ($getallstaff as $getallUser_key => $getallUser_value) {

                                                                        if ($getallUser_value['id'] != $_SESSION['id']) {



                                                                           $user_login = $this->db->query(" SELECT `id` FROM user WHERE id =" . $getallUser_value['id'] . " and last_login_time >=" . $newTime . " and firm_id=" . $_SESSION['firm_id'] . " ")->result_array();

                                                                           if (!empty($user_login)) {

                                                                              $status = "online";
                                                                              $status_color = "bg-success";
                                                                           } else {
                                                                              $status = "offline";
                                                                              $status_color = "bg-danger";
                                                                           }
                                                                           //echo $status;
                                                                     ?>
                                                                           <div class="media userlist-box box_<?php echo $getallUser_value['id']; ?>" data-id="<?php echo $getallUser_value['id']; ?>" data-status="<?php echo $status; ?>" data-username="<?php echo $getallUser_value['crm_name']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $getallUser_value['crm_name']; ?>">
                                                                              <a class="media-left" href="#!">
                                                                                 <img class="media-object" src="<?php echo base_url(); ?>uploads/<?php echo $getallUser_value['crm_profile_pic']; ?>" onerror="this.src='<?php echo base_url(); ?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                                                 <div class="live-status <?php echo $status_color; ?>"></div>
                                                                              </a>
                                                                              <div class="media-body">
                                                                                 <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                                                              </div>
                                                                              <div class="badge badge-primary new_message_count_<?php echo $getallUser_value['id']; ?>"></div>
                                                                           </div>
                                                                     <?php
                                                                           $i++;
                                                                        }
                                                                     } ?>
                                                                     <!-- end of staff -->
                                                                  </div>
                                                               <?php } ?>



                                                               <!-- for client -->
                                                               <?php if (!empty($getallUser)) { ?>
                                                                  <div class="user-groups client_section">
                                                                     <h6>Client(s)</h6>
                                                                     <div class="client_append"></div>
                                                                     <?php
                                                                     $i = 1;

                                                                     foreach ($getallUser as $getallUser_key => $getallUser_value) {
                                                                        if ($getallUser_value['id'] != $_SESSION['id']) {

                                                                           $user_login = $this->db->query("SELECT * FROM activity_log where user_id=" . $getallUser_value['id'] . " AND module='Login'  order by id DESC")->result_array();
                                                                           if (!empty($user_login)) {
                                                                              $login_time = $user_login[0]['CreatedTime'];
                                                                              $user_logout = $this->db->query("SELECT * FROM activity_log where user_id=" . $getallUser_value['id'] . " AND module='Logout'  order by id DESC")->result_array();

                                                                              $logout_time = isset($user_logout[0]['CreatedTime']) ? $user_logout[0]['CreatedTime'] : '0';
                                                                              $status = "";
                                                                              if ($login_time > $logout_time) {
                                                                                 if (date('Y-m-d', $login_time) == date('Y-m-d')) {
                                                                                    $status = "online";
                                                                                    $status_color = "bg-success";
                                                                                 } else {
                                                                                    $status = "offline";
                                                                                    $status_color = "bg-danger";
                                                                                 }
                                                                              } else {
                                                                                 $status = "offline";
                                                                                 $status_color = "bg-danger";
                                                                              }
                                                                           } else {
                                                                              $status = "offline";
                                                                              $status_color = "bg-danger";
                                                                           }
                                                                           //echo $status;
                                                                     ?>
                                                                           <div class="media userlist-box box_<?php echo $getallUser_value['id']; ?>" data-id="<?php echo $getallUser_value['id']; ?>" data-status="<?php echo $status; ?>" data-username="<?php echo $getallUser_value['crm_name']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $getallUser_value['crm_name']; ?>">
                                                                              <a class="media-left" href="#!">
                                                                                 <img class="media-object" src="<?php echo base_url(); ?>uploads/<?php echo $getallUser_value['crm_profile_pic']; ?>" onerror="this.src='<?php echo base_url(); ?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                                                 <div class="live-status <?php echo $status_color; ?>"></div>
                                                                              </a>
                                                                              <div class="media-body">
                                                                                 <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name'];
                                                                                                               if ($getallUser_value['user_type'] == 'FA') {
                                                                                                                  echo '(Firm Admin)';
                                                                                                               } ?></div>
                                                                              </div>
                                                                              <div class="badge badge-primary new_message_count_<?php echo $getallUser_value['id']; ?>"></div>
                                                                           </div>
                                                                     <?php
                                                                           $i++;
                                                                        }
                                                                     } ?>
                                                                  </div>
                                                                  <!-- for client -->
                                                               <?php } ?>

                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="page-error">
                                                <div class="card text-center">
                                                   <div class="card-block">
                                                      <div class="m-t-10">
                                                         <i class="icofont icofont-warning text-white bg-c-yellow"></i>
                                                         <h4 class="f-w-600 m-t-25">Not supported</h4>
                                                         <p class="text-muted m-b-0">Chat not supported in this device</p>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <!--End chat -->
                     </div>
                     <!-- tab2-->
                     <div id="Tasks" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_task_details');
                                                            if ($query == 0) { ?> hide_classes  <?php } ?>">


                        <div class="invoice-crm1 disk-redesign1">
                           <div class="top-leads fr-task cfssc redesign-lead-wrapper">
                              <div class="lead-data1 pull-right leadsG leads_status_count">

                                 <div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
                                    <div class="lead-point1">
                                       <a href="<?php echo base_url(); ?>user/task_list">
                                          <strong><?php echo $tasks['task_count']; ?></strong>
                                          <span class="status_filter">Tasks</span>
                                       </a>
                                    </div>
                                 </div>

                                 <div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
                                    <div class="lead-point1">
                                       <a href="<?php echo base_url(); ?>Firm_dashboard/Complete">
                                          <strong><?php echo $closed_tasks['task_count']; ?> </strong>
                                          <span class="status_filter">Completed Tasks</span>
                                       </a>
                                    </div>
                                 </div>

                                 <div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
                                    <div class="lead-point1">
                                       <a href="<?php echo base_url(); ?>Firm_dashboard/inprogress">
                                          <strong><?php echo $pending_tasks['task_count']; ?> </strong>
                                          <span class="status_filter">Inprogress Tasks</span>
                                       </a>
                                    </div>
                                 </div>

                                 <div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
                                    <div class="lead-point1">
                                       <a href="<?php echo base_url(); ?>Firm_dashboard/notstarted">
                                          <strong><?php echo $cancelled_tasks['task_count']; ?> </strong>
                                          <span class="status_filter">Not Started Tasks</span>
                                       </a>
                                    </div>
                                 </div>

                              </div>
                           </div>
                        </div>


                        <div class="cards12 space-disk1 col-xs-12 mydisk-realign1">
                           <div class="col-xl-6 col-sm-6 lf-mytsk">
                              <!-- To Do List card start -->
                              <div class="card fr-hgt">
                                 <div class="card-header">
                                    <h5>My Tasks</h5>
                                    <!-- <div class="card-header-right"> <i class="icofont icofont-spinner-alt-5"></i> </div> -->
                                 </div>
                                 <div class="card-block">
                                    <!-- <h3> Status</h3> -->
                                    <h3 <?php
                                          if (empty($task_list)) {
                                             echo "style='display:none;'";
                                          } ?>>Task Name <span class="f-right">Start Date / End Date</span></h3>
                                    <?php
                                    if (empty($task_list)) { ?>

                                       <div class="new-task12">
                                          No Tasks Available
                                       </div>
                                    <?php  } else { ?>
                                       <div class="new-task">

                                          <?php
                                          foreach ($task_list as $task) {

                                             $assignee_det = $this->Common_mdl->getAssignee('TASK', $task['id']);


                                             if (strtotime($task['end_date']) < time()) {
                                                $date = date('y-m-j&\nb\sp;g:i:s');

                                                $start_date = date('Y-m-d', strtotime($task['start_date']));
                                                echo $end_date = date('d-m-Y', strtotime($task['end_date']));

                                                $date1 = strtotime('' . $start_date . ' 00:00:00');

                                                $date2 = strtotime('' . $end_date . ' 00:00:00');

                                                $today = time();

                                                $dateDiff = $date2 - $date1;

                                                $dateDiffForToday = $today - $date1;

                                                $percentage = $dateDiffForToday / $dateDiff * 100;

                                                $percentageRounded = round($percentage);

                                          ?>

                                                <span class="percentage-code1">overdue</span>
                                             <?php
                                             } else {

                                                $percentageRounded = '0';
                                             ?>
                                                <span class="percentage-code1"> <?php echo $percentageRounded . '%';  ?> </span>
                                             <?php
                                             }
                                             ?>


                                             <div class="to-do-list">
                                                <div class="checkbox-fade fade-in-primary">
                                                   <label class="check-task">
                                                      <span><a href="<?php echo base_url(); ?>user/task_details/<?php echo $task['id']; ?>"><?php echo $task['subject']; ?></a></span>
                                                      <span class="bg-c-pink">
                                                         <?php

                                                         $username = array();

                                                         foreach ($assignee_det as $key => $val) {
                                                            array_push($username, ucwords($val['crm_name']));
                                                         }
                                                         echo implode(',', $username); ?>
                                                      </span>
                                                      <div class="progress">
                                                         <div class="progress-bar bg-c-blue" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentageRounded . '%'; ?>">
                                                         </div>
                                                      </div>
                                                   </label>
                                                </div>
                                                <span class="f-right end-dateop"></span>
                                                <span class="f-right end-dateop"><?php echo date('d-m-Y', strtotime($task['start_date'])); ?>/<?php echo date('d-m-Y', strtotime($task['end_date'])); ?></span>
                                                <!-- <div class="f-right">
                                       <a href="#" class="delete_todolist"><i class="icofont icofont-ui-delete"></i></a>
                                       </div> -->
                                             </div>
                                          <?php }  ?>
                                       </div>
                                    <?php } ?>
                                 </div>
                              </div>
                              <!-- To Do List card end -->
                           </div>
                           <div class="col-xl-6 col-sm-6 lf-mytodo  mydisk-view" <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_todo_items');
                                                                                 if ($query == 0) { ?> style="display:none;" <?php } else { ?>proposal checking<?php } ?>>
                              <!-- To Do Card List card start -->
                              <div class="card fr-hgt">
                                 <div class="card-header">
                                    <h5>
                                       My To Do items
                                       <span class="f-right">
                                          <ul class="nav nav-tabs  tabs" role="tablist">
                                             <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">New To Do</a>
                                             </li>
                                             <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">View All</a>
                                             </li>
                                          </ul>
                                       </span>
                                    </h5>
                                 </div>
                                 <div class="widnewtodo">
                                    <div class="tab-content tabs card-block">
                                       <div class="tab-pane in hide active" id="home1" role="tabpanel">
                                          <div class="new-task align-todos" id="draggablePanelList">
                                             <div class="no-todo">

                                                <h5 class="bg-c-green12"> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Latest to do's <button type="button" class="btn btn-primary btn-sm f-right" data-toggle="modal" data-target="#myTodo">New Todo's</button></h5>

                                                <div class="sortable edit_after" id="5">
                                                   <?php
                                                   foreach ($todo_list as $todo) { ?>
                                                      <div class="to-do-list card-sub" id="<?php echo $todo['id']; ?>">
                                                         <div class="top-disk01">
                                                            <label class="check-task custom_checkbox1">
                                                               <input type="checkbox" value="" id="<?php echo $todo['id']; ?>" onclick="todo_status(this)"> <i></i> </label>

                                                            <span><?php echo ucwords($todo['todo_name']); ?></span>
                                                         </div>
                                                         <div class="f-right">
                                                            <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
                                                            <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
                                                         </div>
                                                      </div>
                                                   <?php } ?>
                                                </div>
                                             </div>
                                             <h5 class="bg-c-green12"> <i class="fa fa-check"></i> Latest finished to do's</h5>
                                             <div class="completed_todos">
                                                <?php
                                                foreach ($todo_completedlist as $todo) { ?>
                                                   <div class="to-do-list card-sub" id="1">
                                                      <div class="top-disk01">
                                                         <label class="check-task done-task custom_checkbox1">

                                                            <input type="checkbox" value="" checked="checked" id="<?php echo $todo['id']; ?>" onclick="todo_status(this)"> <i></i> </label>

                                                         <span><?php echo ucwords($todo['todo_name']); ?></span>

                                                      </div>
                                                      <div class="f-right">
                                                         <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
                                                         <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                <!--  <p>No todos found</p> -->
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane" id="profile1" role="tabpanel">
                                          <div class="new-task">
                                             <?php
                                             foreach ($todolist as $todo) { ?>
                                                <div class="to-do-list sortable-moves">
                                                   <div class="to-do-list card-sub" id="1">
                                                      <div class="top-disk01">
                                                         <label class="custom_checkbox1 check-task <?php if ($todo['status'] == 'completed') { ?> done-task <?php } ?>">
                                                            <input type="checkbox" value="" <?php if ($todo['status'] == 'completed') { ?> checked="checked" <?php } ?> id="<?php echo $todo['id']; ?>" onclick="todo_status(this)"> <i></i></label>

                                                         <span><?php echo ucwords($todo['todo_name']); ?></span>

                                                      </div>
                                                      <div class="f-right">
                                                         <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
                                                         <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             <?php } ?>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- To Do Card List card end -->
                           </div>
                        </div>
                     </div>
                     <!-- tab 3-->
                     <div id="Clients" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_client_details');
                                                            if ($query == 0) { ?> hide_classes <?php } ?>">


                        <div class="invoice-crm1 disk-redesign1">
                           <div class="top-leads fr-task cfssc redesign-lead-wrapper">
                              <div class="lead-data1 pull-right leadsG leads_status_count">

                                 <div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
                                    <div class="lead-point1">
                                       <a href="<?php echo base_url(); ?>invoice">
                                          <strong><?php echo count($invoice); ?></strong>
                                          <span class="status_filter">Invoices</span>
                                       </a>
                                    </div>
                                 </div>

                                 <div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
                                    <div class="lead-point1">
                                       <a href="<?php echo base_url(); ?>invoice?type=1">
                                          <strong><?php echo count($approved_invoice); ?></strong>
                                          <span class="status_filter">Paid Invoices</span>
                                       </a>
                                    </div>
                                 </div>

                                 <div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
                                    <div class="lead-point1">
                                       <a href="<?php echo base_url(); ?>invoice?type=2">
                                          <strong><?php echo count($unpaid_invoice); ?></strong>
                                          <span class="status_filter">Unpaid Invoices</span>
                                       </a>
                                    </div>
                                 </div>

                                 <div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1">
                                    <div class="lead-point1">
                                       <a href="<?php echo base_url(); ?>invoice?type=3">
                                          <strong><?php echo count($cancel_invoice); ?></strong>
                                          <span class="status_filter">Cancelled Invoices</span>
                                       </a>
                                    </div>
                                 </div>

                              </div>
                           </div>
                        </div>

                        <div class="card desk-dashboard mydisk-realign1 floating_set">




                           <div class="row impo">

                              <div class="number-mydisk floating_set">
                                 <div class="count-mydisk1">
                                    <div class="target-disk">
                                       <a href="<?php echo base_url(); ?>Firm_dashboard/All_client">
                                          <span>Number of Client(s)</span>
                                          <div class="count-pos-01">
                                             <strong><?php echo count($client); ?></strong>
                                          </div>
                                       </a>
                                    </div>
                                 </div>

                                 <div class="count-mydisk1">
                                    <div class="target-disk">
                                       <a href="<?php echo base_url(); ?>Firm_dashboard/Active_client">
                                          <span>Number of Active Client(s)</span>
                                          <div class="count-pos-01">
                                             <strong><?php echo $active_client['client_count']; ?></strong>
                                          </div>
                                       </a>
                                    </div>
                                 </div>


                                 <div class="count-mydisk1">
                                    <div class="target-disk">
                                       <a href="<?php echo base_url(); ?>Firm_dashboard/Frozen_client">
                                          <span>Frozen Client(s)</span>
                                          <div class="count-pos-01">
                                             <strong><?php echo $frozen_client['client_count']; ?></strong>
                                          </div>
                                       </a>
                                    </div>
                                 </div>

                                 <div class="count-mydisk1">
                                    <div class="target-disk">
                                       <a href="<?php echo base_url(); ?>Firm_dashboard/Inactive_client">
                                          <span>Non Active Client(s)</span>
                                          <div class="count-pos-01">
                                             <strong><?php echo $inactive_client['client_count']; ?></strong>
                                          </div>
                                       </a>
                                    </div>
                                 </div>

                                 <div class="count-mydisk1">
                                    <div class="target-disk">
                                       <a href="<?php echo base_url(); ?>user">
                                          <span>New added This month</span>
                                          <div class="count-pos-01">
                                             <strong><?php echo $new_client['client_count']; ?></strong>
                                          </div>
                                       </a>
                                    </div>
                                 </div>

                              </div>

                              <div class="card desk-dashboard fr-bg12 widmar col-xs-12 col-md-6 mydisk-realign1">
                                 <div class="card">
                                    <div class="card-header">
                                       <h5>Clients</h5>
                                    </div>
                                    <span class="time-frame"><label>Time Frame:</label>
                                       <select id="filter_client">
                                          <option value="year">This Year</option>
                                          <option value="month">This Month</option>
                                       </select>
                                       <div class="card-block fileters_client">
                                          <div id="chart_Donut11" style="width: 100%; height: 309px;"></div>
                                       </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!--tab 4-->
                     <div id="Proposals" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_proposal');
                                                               if ($query == 0) { ?> hide_classes <?php } ?>">
                        <!--chart sec -->
                        <div class="row col-12">
                           <div class="card desk-dashboard teo fr-bg12 col-xs-12 col-md-6 mydisk-realign1">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Proposals</h5>
                                 </div>
                                 <span class="time-frame"><label>Time Frame:</label>
                                    <select id="proposal">
                                       <option value="month">This Month</option>
                                       <option value="year">This Year</option>
                                    </select>
                                    <div class="card-block proposal">
                                       <div id="piechart" style="width: 100%; height: 309px;"></div>
                                    </div>
                              </div>
                           </div>
                           <!--     <div class="card desk-dashboard teo fr-bg12 widmar col-xs-12 col-md-6">
                        <div class="card">
                         <div class="accident-toggle">
                           <h2>Timesheets</h2>
                         </div>
                         <span class="time-frame"><label>Time Frame:</label>
                         <select>
                           <option>This Month</option>
                           <option>This Year</option>
                         </select>
                         <div class="card-block">
                           <div id="chart_Donut" style="width: 100%; height: 309px;"></div>
                         </div>
                         </div>
                        </div> -->
                        </div>
                     </div>
                     <!--tab 5-->
                     <div id="Deadline-Manager" class="tab-pane fade <?php $query = $this->Common_mdl->get_price('settings', 'user_id', '1', 'dashboard_deadline_details');
                                                                     if ($query == 0) { ?> hide_classes <?php } ?>">

                        <div class=" job-card">

                           <?php

                           $icpfont_class = array('icofont-presentation-alt st-icon bg-c-yellow', 'icofont-files st-icon bg-c-blue', 'icofont-paper st-icon bg-c-green', 'icofont-file-document st-icon bg-c-pink', 'icofont-file-text st-icon bg-c-yellow', 'icofont-file-document st-icon bg-c-blue');
                           $count = 0;
                           foreach ($service_list as $key => $value) {

                              if ($count / 6 == '1') {
                                 $count = 0;
                              }
                           ?>

                              <div class="col-md-12 col-xl-4">
                                 <div class="card widget-statstic-card">
                                    <div class="card-header">
                                       <div class="media">
                                          <div class="media-body media-middle">
                                             <div class="company-name">
                                                <p><a href="<?php echo base_url() . 'Firm_dashboard/service/' . $value['id']; ?>"><?php echo $value['service_name']; ?></a></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="card-block">
                                       <i class="icofont <?php echo $icpfont_class[$count]; ?>"></i>
                                       <ul class="text-muted">
                                          <?php
                                          foreach ($oneyear as $date) {
                                             // echo $date['crm_vat_due_date']."-fgf-";
                                             if ($date[$columns_check[$value['id']]] != '') { ?>
                                                <li><?php echo $date['crm_company_name']; ?>-<?php echo (strtotime($date[$columns_check[$value['id']]])) != '' ?  date('Y-m-d', strtotime($date[$columns_check[$value['id']]])) : '0'; ?></li>
                                          <?php }
                                          } ?>
                                       </ul>
                                    </div>
                                 </div>
                              </div>

                           <?php $count++;
                           } ?>
                        </div>
                        <!-- fullwidth map -->
                        <div class="floating_set set-timenone">
                           <div class="desk-dashboard widmar floating_set mydisk-realign1">
                              <div class="card-header">
                                 <h5>Deadlines</h5>
                              </div>
                              <span class="time-frame"><label>Time Frame:</label>
                                 <select id="filter_deadline">
                                    <option value="month">This Month</option>
                                    <option value="year">This Year</option>
                                 </select>
                                 <div class="card-block deadline">
                                    <div id="columnchart_values" style="width: 100%; height: 410px;"></div>
                                 </div>

                           </div>
                           <!-- fullwidth map-->
                        </div>
                        <!--tab end-->
                     </div>
                  </div>
               </div>
               <!-- chart sec -->
            </div>
         </div>
         <!-- management block -->
         <div class="modal fade" id="myTodo" role="dialog">
            <div class="modal-dialog">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Add New Todo's</h4>
                  </div>
                  <div class="modal-body">
                     <label>Todo's Name</label>
                     <input type="text" name="todo_name" id="todo_name" value="">
                     <span id="er_todoname"></span>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-default" id="todo_save">Save</button>
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
               </div>
            </div>
         </div>
         <div id="edit_delete_modals">
            <?php
            foreach ($todolist as $todo) {
            ?>
               <div class="modal fade edittodo_view" id="EditTodo_<?php echo $todo['id']; ?>" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title">Edit Todos</h4>
                        </div>
                        <div class="modal-body">
                           <input type="hidden" name="id" id="todo_id" value="<?php echo $todo['id']; ?>">
                           <input type="text" name="todo_name" class="todo_name" value="<?php echo $todo['todo_name']; ?>">
                           <span id="er_todoname_<?php echo $todo['id']; ?>"></span>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-default save" id="<?php echo $todo['id']; ?>">Save</button>
                           <button type="button" class="btn btn-default close" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal fade" id="DeleteTodo_<?php echo $todo['id']; ?>" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title">Delete todos</h4>
                        </div>
                        <div class="modal-body">
                           <p>Do you want to Delete Todo's ?</p>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-default delete" id="<?php echo $todo['id']; ?>">Yes</button>
                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                  </div>
               </div>
            <?php } ?>
         </div>
         <?php $this->load->view('includes/footer'); ?>
         <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
         <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
         <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
         <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
         <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
         <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
         <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

         <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/todo.js"></script>
         <!-- jquery sortable js -->
         <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Sortable.js"></script>
         <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sortable-custom.js"></script>
         <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
         <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom-google-chart.js"></script>

         <script src="<?php echo base_url(); ?>assets/js/RGraph.common.core.js"></script>
         <script src="<?php echo base_url(); ?>assets/js/RGraph.bar.js"></script>
         <script src="<?php echo base_url(); ?>assets/js/RGraph.common.dynamic.js"></script>
         <script type="text/javascript">
            $(document).ready(function() {

               $('#filter_client').val('month');
               $('#proposal').val('month');
               $('#filter_deadline').val('month');

               $(document).on('click', '.tasks', function() {
                  $('div .tab-content').find('div #home1').addClass('active');
               });
               $(document).on('click', '.deads', function() {
                  setTimeout(function() {
                     $('#filter_deadline').val('month').trigger('change');
                  }, 1000)
               });
               $(".checking").first().find('a').trigger('click');


               // $(".hide_check").each(function(){
               //    $(this).next('li').trigger('click');
               //     console.log($(this).next('li').attr('class'));
               // });



               // console.log( "ready!" );

               // var sec_id=$(".hide_classes").next().attr('id');

               // console.log(sec_id);

               // console.log($('#proposal_dashboard a[href="#'+sec_id+'"]').attr('class'));

               // $('#proposal_dashboard a[href="#'+sec_id+'"]').trigger('click');

            });





            $(document).ready(function() {

               //   var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
               /*$( ".datepicker" ).datepicker({
                      dateFormat: 'yy-mm-dd'
                  });*/

               $('.tags').tagsinput({
                  allowDuplicates: true
               });

               $('.tags').on('itemAdded', function(item, tag) {
                  $('.items').html('');
                  var tags = $('.tags').tagsinput('items');

               });
            });

            $("#contact_today").click(function() {
               if ($("#contact_today").is(':checked')) {
                  $(".selectDate").css('display', 'none');
               } else {
                  $(".selectDate").css('display', 'block');
               }
            });

            $(".contact_update_today").click(function() {
               var id = $(this).attr("data-id");
               if ($(this).is(':checked')) {

                  $("#selectDate_update_" + id).css('display', 'none');
               } else {
                  $("#selectDate_update_" + id).css('display', 'block');
               }
            });

            $("#all_leads").dataTable({
               "iDisplayLength": 10,
            });
         </script>
         <script type="text/javascript">
            $(document).ready(function() {

               $("#leads_form").validate({
                  rules: {
                     lead_status: "required",
                     source: "required",
                     name: "required",
                  },
                  messages: {
                     lead_status: "Please enter your Status",
                     source: "Please enter your Source",
                     name: "Please enter your Name"
                  },

               });


               $(".edit_leads_form").validate({
                  rules: {
                     lead_status: "required",
                     source: "required",
                     name: "required",
                  },
                  messages: {
                     lead_status: "Please enter your Status",
                     source: "Please enter your Source",
                     name: "Please enter your Name"
                  },

               });
            });


            $(".sortby_filter").click(function() {
               $(".LoadingImage").show();

               var sortby_val = $(this).attr("id");
               var data = {};
               data['sortby_val'] = sortby_val;
               $.ajax({
                  url: '<?php echo base_url(); ?>leads/sort_by/',
                  type: 'POST',
                  data: data,
                  success: function(data) {
                     $(".LoadingImage").hide();

                     $(".sortrec").html(data);
                     $("#all_leads").dataTable({
                        "iDisplayLength": 10,
                     });
                  },
               });

            });
            $("#import_leads").validate({
               rules: {
                  file: {
                     required: true,
                     accept: "csv"
                  },

                  lead_status: "required",
                  source: "required",
                  name: "required",
               },
               messages: {
                  file: {
                     required: 'Required!',
                     accept: 'Please upload a file with .csv extension'
                  },

                  lead_status: "Please enter your Status",
                  source: "Please enter your Source",
                  name: "Please enter your Name"
               },

            });
         </script>
         <script type="text/javascript">
            $(document).ready(function() {
               $('.edit-toggle1').click(function() {
                  $('.proposal-down').slideToggle(300);
               });

               $("#legal_form12").change(function() {
                  var val = $(this).val();
                  if (val === "from2") {
                     // alert('hi');
                     $(".from2option").show();
                  } else {
                     $(".from2option").hide();
                  }
               });
            })
         </script>
         <script>
            $(function() {

               $(".sortable").sortable({
                  update: function(event, ui) {
                     var arr = [];
                     $("div.sortable ").each(function() {
                        var idsInOrder = [];
                        // idsInOrder.push($(this).attr('id')) 
                        var ul_id = $(this).attr('id');
                        //  alert(ul_id);

                        $("div#" + $(this).attr('id') + " div.card-sub").each(function() {
                           idsInOrder.push($(this).attr('id'))
                        });
                        // alert(idsInOrder);
                        // arr.push({[ul_id]:idsInOrder});   
                        arr[ul_id] = idsInOrder;
                     });

                     $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Firm_dashboard/update_type_todolist",
                        data: {
                           'datas': arr
                        },
                        beforeSend: function() {
                           $(".LoadingImage").show();
                        },
                        success: function(data) {
                           $(".LoadingImage").hide();
                        }
                     });
                  }
               });
               //$( ".sortable" ).disableSelection();

            });

            function check_todoname(todoname, recid) {
               if (recid != "") {
                  var er = "#er_todoname";
                  er = er.concat('_' + recid);
               } else {
                  var er = "#er_todoname";
               }

               if (todoname != "") {
                  $(er).html('');
                  $.ajax({
                     url: "<?php echo base_url(); ?>Firm_dashboard/Check_Distinct",
                     type: "POST",
                     data: {
                        todoname: todoname,
                        id: recid
                     },

                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(count) {
                        if (count > 0) {
                           $(er).html('Already Exists.').css('color', 'red');
                           $(".LoadingImage").hide();
                        } else {
                           $(er).html('');

                           if (recid == "") {
                              $.ajax({
                                 type: "POST",
                                 url: "<?php echo base_url(); ?>Firm_dashboard/todolist_add",
                                 data: {
                                    'todo_name': todoname
                                 },

                                 success: function(data) {
                                    var json = JSON.parse(data);

                                    $("#myTodo").modal('hide');
                                    $(".LoadingImage").hide();

                                    if (json['status'] > 0) {
                                       var content = json['content'];
                                       var all_edit_delete_modals = json['all_edit_delete_modals'];
                                       var view_all = json['view_all'];
                                       $(".sortable").html('');
                                       $(".sortable").append(content);
                                       $('#edit_delete_modals').html('');
                                       $('#edit_delete_modals').html(all_edit_delete_modals);
                                       $('div .tab-content').find('div #profile1').children('div .new-task').html("");
                                       $('div .tab-content').find('div #profile1').children('div .new-task').html(view_all);
                                       $('#todoinfo').find('.position-alert1').html("");
                                       $('#todoinfo').find('.position-alert1').html('Todo Added Successfully!');
                                       $('.todoinfo').trigger('click');
                                    } else {
                                       $('#todoinfo').find('.position-alert1').html("");
                                       $('#todoinfo').find('.position-alert1').html('Process Failed');
                                       $('.todoinfo').trigger('click');
                                    }
                                 }
                              });
                           } else if (recid != "") {
                              $.ajax({
                                 type: "POST",
                                 url: "<?php echo base_url(); ?>Firm_dashboard/todolist_edit",
                                 data: {
                                    'todo_name': todoname,
                                    'id': recid
                                 },

                                 success: function(data) {
                                    var json = JSON.parse(data);

                                    $(".LoadingImage").hide();
                                    $(".edittodo_view").hide();
                                    $("#EditTodo_" + $(this).attr('id') + "").modal('hide');
                                    $("#EditTodo_" + recid).find(".close").trigger('click');

                                    if (json['status'] == 1) {
                                       var content = json['content'];
                                       var complete_data = json['completed_content'];
                                       var view_all = json['view_all'];
                                       $(".sortable").html('');
                                       $(".sortable").append(content);
                                       $(".completed_todos").html('');
                                       $(".completed_todos").append(complete_data);
                                       $('div .tab-content').find('div #profile1').children('div .new-task').html("");
                                       $('div .tab-content').find('div #profile1').children('div .new-task').html(view_all);
                                       $('#todoinfo').find('.position-alert1').html("");
                                       $('#todoinfo').find('.position-alert1').html('Todo Updated Successfully!');
                                       $('.todoinfo').trigger('click');
                                    } else {
                                       $('#todoinfo').find('.position-alert1').html("");
                                       $('#todoinfo').find('.position-alert1').html('Process Failed');
                                       $('.todoinfo').trigger('click');
                                    }
                                 }
                              });
                           }
                        }
                     }
                  });
               } else {
                  $(er).html('Required.').css('color', 'red');
                  status = 0;
               }

               return true;
            }

            $("#todo_save").click(function() {
               var todo_name = $("#todo_name").val();
               var id = "";
               check_todoname(todo_name, id);
            });

            function todo_status(todo) {
               var id = $(todo).attr('id');

               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>Firm_dashboard/todos_status",
                  data: {
                     'id': id
                  },

                  beforeSend: function() {
                     $(".LoadingImage").show();
                  },
                  success: function(data) {
                     var json = JSON.parse(data);
                     $(".LoadingImage").hide();
                     if (json['status'] == 1) {
                        var content = json['content'];
                        var complete_data = json['completed_content'];
                        var view_all = json['view_all'];
                        $(".sortable").html('');
                        $(".sortable").append(content);
                        $(".completed_todos").html('');
                        $(".completed_todos").append(complete_data);
                        $('div .tab-content').find('div #profile1').children('div .new-task').html("");
                        $('div .tab-content').find('div #profile1').children('div .new-task').html(view_all);
                        $('#todoinfo').find('.position-alert1').html("");
                        $('#todoinfo').find('.position-alert1').html('Todo Status Updated Successfully!');
                        $('.todoinfo').trigger('click');
                     } else {
                        $('#todoinfo').find('.position-alert1').html("");
                        $('#todoinfo').find('.position-alert1').html('Process Failed');
                        $('.todoinfo').trigger('click');
                     }
                  }
               });
            }

            $(document).on('click', '.save', function() {
               var id = $(this).attr('id');
               var todo_name = $("div#EditTodo_" + $(this).attr('id') + " .todo_name").val();
               check_todoname(todo_name, id);
            });

            $(document).on('click', '.delete', function() {
               var id = $(this).attr('id');
               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>Firm_dashboard/todolist_delete",
                  data: {
                     'id': id
                  },

                  beforeSend: function() {
                     $(".LoadingImage").show();
                  },
                  success: function(data) {
                     $("#DeleteTodo_" + id).find('.modal-header').children('.close').trigger('click');
                     $(".LoadingImage").hide();

                     if (data == 1) {
                        $('#todoinfo').find('.position-alert1').html("");
                        $('#todoinfo').find('.position-alert1').html('Todo Deleted Successfully!');
                        $('.todoinfo').trigger('click');
                        setTimeout(function() {
                           location.reload();
                        }, 2000);
                     } else {
                        $('#todoinfo').find('.position-alert1').html("");
                        $('#todoinfo').find('.position-alert1').html('Process Failed');
                        $('.todoinfo').trigger('click');
                     }
                  }
               });
            });

            $(document).on('change', '#filter_client', function() {
               var id = $(this).val();

               if (id == 'month') {
                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Firm_dashboard/client_filter",
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(data) {
                        $(".fileters_client").html('');
                        $(".fileters_client").append(data);
                        $(".LoadingImage").hide();
                     }
                  });
               } else {
                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Firm_dashboard/client_filter_year",
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(data) {
                        $(".fileters_client").html('');
                        $(".fileters_client").html(data);
                        $(".LoadingImage").hide();
                     }
                  });
               }
            });

            $("#filter_deadline").change(function() {
               var id = $(this).val();
               if ($(this).val() == 'year') {
                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Firm_dashboard/deadline_filter_year",
                     data: {
                        'id': id
                     },
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(data) {
                        // console.log(data);   
                        $(".deadline").html('');
                        $(".deadline").append(data);
                        $(".LoadingImage").hide();
                     }
                  });
               } else {
                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Firm_dashboard/deadline_filter_month",
                     data: {
                        'id': id
                     },
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(data) {
                        // console.log(data);   
                        $(".deadline").html('');
                        $(".deadline").append(data);
                        $(".LoadingImage").hide();
                     }
                  });
               }
            });

            $("#proposal").change(function() {
               var id = $(this).val();
               if ($(this).val() == 'year') {
                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Firm_dashboard/filter_proposal_year",
                     data: {
                        'id': id
                     },
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(data) {
                        // console.log(data);   
                        $(".proposal").html('');
                        $(".proposal").append(data);
                        $(".LoadingImage").hide();
                     }
                  });
               } else {
                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Firm_dashboard/filter_proposal_month",
                     data: {
                        'id': id
                     },
                     beforeSend: function() {
                        $(".LoadingImage").show();
                     },
                     success: function(data) {
                        // console.log(data);   
                        $(".proposal").html('');
                        $(".proposal").append(data);
                        $(".LoadingImage").hide();
                     }
                  });
               }
            });


            google.charts.load('current', {
               'packages': ['corechart']
            });
            google.charts.setOnLoadCallback(drawChart2);

            function drawChart2() {

               var data = google.visualization.arrayToDataTable([
                  ['Task', 'link', 'Hours per Day'],
                  ['Accepted', '<?php echo base_url(); ?>Firm_dashboard/accept', <?php echo count($accept_proposal); ?>],
                  ['In Discussion', '<?php echo base_url(); ?>Firm_dashboard/indiscussion', <?php echo count($indiscussion_proposal); ?>],
                  ['Sent', '<?php echo base_url(); ?>Firm_dashboard/sent', <?php echo count($sent_proposal); ?>],
                  ['Viewed', '<?php echo base_url(); ?>Firm_dashboard/view', <?php echo count($viewed_proposal); ?>],
                  ['Declined', '<?php echo base_url(); ?>Firm_dashboard/decline', <?php echo count($declined_proposal); ?>],
                  ['Archived', '<?php echo base_url(); ?>Firm_dashboard/archieve', <?php echo count($archive_proposal); ?>],
                  ['Draft', '<?php echo base_url(); ?>Firm_dashboard/draft', <?php echo count($draft_proposal); ?>]
               ]);

               var view = new google.visualization.DataView(data);
               view.setColumns([0, 2]);

               var options = {
                  title: 'My Proposal Activities'
               };

               var chart = new google.visualization.PieChart(document.getElementById('piechart'));

               chart.draw(view, options);

               var selectHandler = function(e) {
                  //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
                  window.open(data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
               }
               // Add our selection handler.
               google.visualization.events.addListener(chart, 'select', selectHandler);
            }
         </script>
         <script type="text/javascript">
            google.charts.load("current", {
               packages: ["corechart"]
            });
            google.charts.setOnLoadCallback(drawChart1);

            function drawChart1() {
               var data = google.visualization.arrayToDataTable([
                  ['Status', 'link', 'Count'],
                  ['Active Client', '<?php echo base_url(); ?>Firm_dashboard/Active_client', <?php echo $active['client_count']; ?>],
                  ['Inactive Client', '<?php echo base_url(); ?>Firm_dashboard/Inactive_client', <?php echo $inactive['client_count']; ?>],
                  ['Frozen Client', '<?php echo base_url(); ?>Firm_dashboard/Frozen_client', <?php echo $frozen['client_count']; ?>],
               ]);
               var view = new google.visualization.DataView(data);
               view.setColumns([0, 2]);
               var options = {
                  title: 'Client Lists',
                  pieHole: 0.4,
               };
               var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
               chart.draw(view, options);
               var selectHandler = function(e) {
                  //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
                  window.open(data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
               }
               // Add our selection handler.
               google.visualization.events.addListener(chart, 'select', selectHandler);
            }
         </script>
         <p id="myValueHolder"></p>
         <script type="text/javascript">
            function drawChart() {
               var data = new google.visualization.DataTable();
               data.addColumn('string', 'Month');
               data.addColumn('number', 'Value');

               <?php

               $deadlines_arr = "";

               foreach ($deadlines as $key => $value) {
                  $deadlines_arr .= '["' . $key . '",' . $value['deadline_count'] . '],';
               }

               $deadlines_arr = substr_replace($deadlines_arr, '', -1);

               ?>

               data.addRows([<?php echo $deadlines_arr; ?>]);


               // Set chart options  
               var chart = new google.visualization.ColumnChart(document.querySelector('#columnchart_values'));

               google.visualization.events.addListener(chart, 'select', function() {
                  var selection = chart.getSelection();
                  if (selection.length) {

                     var row = selection[0].row;
                     document.querySelector('#myValueHolder').innerHTML = data.getValue(row, 1);

                     var view = new google.visualization.DataView(data);
                     view.setColumns([0, 1, {
                        type: 'string',
                        role: 'style',
                        calc: function(dt, i) {
                           // alert(row);
                           if (i == row) {
                              load(row);
                           }
                           return (i == row) ? '' : null;
                        }
                     }]);

                     chart.draw(view, {
                        height: 460,
                        width: 600
                     });
                  }
               });

               chart.draw(data, {
                  height: 460,
                  width: 1200
               });
            }
            google.load('visualization', '1', {
               packages: ['corechart'],
               callback: drawChart
            });

            function load(row) {
               //alert(row);
               var index = row;
               switch (index) {
                  case 0:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/confirm_statement', '_blank');
                     break;
                  case 1:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/accounts', '_blank');
                     break;
                  case 2:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/personal_tax', '_blank');
                     break;
                  case 3:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/vat', '_blank');
                     break;
                  case 4:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/payroll', '_blank');
                     break;
                  case 5:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/workplace', '_blank');
                     break;
                  case 6:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/cis', '_blank');
                     break;
                  case 7:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/cis_sub', '_blank');
                     break;
                  case 8:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/p11d', '_blank');
                     break;
                  case 9:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/management', '_blank');
                     break;
                  case 10:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/bookkeeping', '_blank');
                     break;
                  case 11:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/investication', '_blank');
                     break;
                  case 12:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/register', '_blank');
                     break;
                  case 13:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/tax', '_blank');
                     break;
                  case 14:
                     window.open('<?php echo base_url(); ?>Firm_dashboard/tax_inves', '_blank');
                     break;
               }
            }
         </script>
         <script type="text/javascript">
            $(document).ready(function() {

               // $( "#tabs112" ).tabs({

               // });

               // $("#tabs112 ul.md-tabs.tabs112 li").click(function(){
               //  drawChart();
               //   drawChart1();
               //    drawChart2();

               // });


            });
         </script>

         <?php $this->load->view('Deadline_manager/reminder_invoice_popup'); ?>

         <script type="text/javascript">
            $(document).on('change', ".deadline_status", function() {
               var values = this.id;
               var rid = values.split('_');

               if (this.value == 'enable') {
                  var stat = "on";
               } else {
                  var stat = "off";
               }

               $(".LoadingImage").show();
               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>Deadline_manager/service_update/",
                  data: {
                     'values': values,
                     'stat': stat
                  },
                  success: function(response) {
                     $(".LoadingImage").hide();
                     if (response == 1) {
                        $("#suc_" + rid[1]).html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>');
                        setTimeout(function() {
                           $("#suc_" + rid[1]).html('');
                        }, 2000);
                     } else {
                        $("#suc_" + rid[1]).html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span style="color:red;">' + response + '</span></div></div></div>');
                     }
                  },
               });

            });

            $(document).on('change', ".deadline_status_email", function() {
               var values = this.id;
               var rid = values.split('_');

               if (this.value == 'yes') {
                  var stat = "on";
               } else {
                  var stat = "off";
               }

               $(".LoadingImage").show();

               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>Deadline_manager/service_update_mail/",
                  data: {
                     'values': values,
                     'stat': stat
                  },
                  success: function(response) {
                     $(".LoadingImage").hide();
                     if (response == 1) {
                        $("#suc_" + rid[1]).html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>');
                        setTimeout(function() {
                           $("#suc_" + rid[1]).html('');
                        }, 2000);
                     } else {
                        $("#suc_" + rid[1]).html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span style="color:red;">' + response + '</span></div></div></div>');
                     }
                  },
               });
            });


            $(document).on('click', '.for_allactivity_section_ajax', function() {

               alert('ok');
               var user_id = '<?php echo $_SESSION['id']; ?>';

               $('.LoadingImage').show();
               $.ajax({
                  url: "<?php echo base_url(); ?>client/jquery_append_client_timeline_services/" + user_id + "/allactivity",
                  success: function(result) {
                     console.log(result);
                     $('#allactivitylog_sections_info').append(result);

                     console.log($('#allactivitylog_sections_info').html());

                     var mylist = $('#allactivitylog_sections_info').find('#services');
                     var listitems = $('#allactivitylog_sections_info').find('#services').children('.sorting').get();
                     listitems.sort(function(a, b) {
                        var compA = $(a).attr('id').toUpperCase();
                        var compB = $(b).attr('id').toUpperCase();
                        return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
                     })
                     $.each(listitems, function(idx, itm) {
                        mylist.append(itm);
                        //alert(itm);
                     });

                     setTimeout(function() {
                        $('.LoadingImage').hide();
                     }, 2000);
                  }

               });
               //$('.LoadingImage').hide();                

            });

            $(document).on('click', '#close_info_msg', function() {
               $('.popup_info_msg').hide();
            });

            $(document).ready(function() {
               $('.pull-left .nav-item a').click(function() {
                  $('.pull-left .nav-item a').removeClass('active');
                  $(this).addClass('active');
                  var tagid = $(this).data('tag');
                  $('.tab-pane').removeClass('active').addClass('hide');
                  $('#' + tagid).addClass('active').removeClass('hide');
               });

               <?php if (!empty($_SESSION['success_msg'])) { ?>

                  $(".reminder").first().find('a').trigger('click');

               <?php } ?>
            });
         </script>



         <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
         <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
         <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
         <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
         <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
         <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
         <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
         <!-- <script src="<?php echo base_url(); ?>assets/js/mmc-common.js"></script> -->
         <!--   <script src="<?php echo base_url(); ?>assets/js/mmc-chat.js"></script> -->
         <?php $this->load->view('User_chat/mmc-common_js'); ?>
         <?php $this->load->view('User_chat/mmc-chat_js'); ?>
         <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/chat.js"></script>
         <script type="text/javascript">
            $(document).ready(function() {
               $('.edit-toggle1').click(function() {
                  $('.proposal-down').slideToggle(300);
               });

               $("#legal_form12").change(function() {
                  var val = $(this).val();
                  if (val === "from2") {
                     // alert('hi');
                     $(".from2option").show();
                  } else {
                     $(".from2option").hide();
                  }
               });

               $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({
                  //  "scrollX": true
               });
            })
         </script>
         <script type="text/javascript">
            $('.create_group').on('click', function(e) {


               $('.chat_user_list').toggle();
               $('#group_name').val('');
               var obj = $(this);

               $('.chat_user_list').find('div #selectator_create_users_list').find('.selectator_selected_items .selectator_selected_item_remove').trigger('click').trigger('click');



               if ($('.chat_user_list').is(":visible") == false) {
                  //console.log('chk');
                  obj.html('').html('Create Group <i class="fa fa-plus" aria-hidden="true"></i></a>');
               } else {
                  obj.html('').html('Create Group <i class="fa fa-minus" aria-hidden="true"></i></a>');
               }

               //$('.chat_user_list').show();

            });
            $('.add_create_group').on('click', function() {
               $('.groupname_error_div').html('');
               $('.user_error_div').html('');

               var group_name = $('#group_name').val();
               var ids = $('#create_users_list').val();

               if (group_name == '') {
                  $('.groupname_error_div').html('Enter Group Name');
                  // $('.ids_error_div').html('Select Group Members');
               } else {

                  if (ids.length > 0) {
                     //alert(checkboxValues.join(', '));
                     var data = {};
                     data['name'] = group_name;
                     data['member'] = ids.join(',');
                     $('.LoadingImage').show();
                     $.ajax({
                        url: '<?php echo base_url(); ?>User_chat/create_group/',
                        type: 'POST',
                        data: data,
                        success: function(data) {
                           //alert(data);
                           $('.LoadingImage').hide();
                           console.log(data);
                           if (data != 'wrong') {
                              location.reload();
                              // $('.created_group_list').append(data);
                              // $('.chat_user_list').hide();
                           }

                        }
                     });
                  } else {
                     $('.user_error_div').html('Select Group Members');
                  }

               }


            });

            $('.group-info').on('click', function() {
               // alert('zzzz');
               //   $('.rsptrspt').html('zzzz');
               $('.edit_user_error_div').html('');
               $('.edit_groupname_error_div').html('');
            });

            $('.edit_create_group').on('click', function() {
               $('.edit_user_error_div').html('');
               $('.edit_groupname_error_div').html('');

               var id = $(this).attr('data-id');
               var group_name = $('#group_name_' + id).val();
               if (group_name == '') {
                  $('.edit_groupname_error_div').html('Enter Group Name');
               } else {
                  //     var checkboxValues = [];
                  // $('input[name="our_group_data_'+id+'"]:checked').each(function(index, elem) {
                  //     checkboxValues.push($(elem).val());
                  // });

                  var ids = $('#update_users_list_' + id).val();
                  if (ids.length > 0) {
                     //alert(checkboxValues.join(', '));
                     var data = {};
                     data['name'] = group_name;
                     data['member'] = ids.join(',');
                     $('.LoadingImage').show();
                     $.ajax({
                        url: '<?php echo base_url(); ?>User_chat/update_group/' + id,
                        type: 'POST',
                        data: data,
                        success: function(data) {
                           //alert(data);
                           $('.LoadingImage').hide();
                           //alert(data);
                           location.reload();
                           // console.log(data);
                           // if(data!='wrong'){
                           //   $('.created_group_list').append(data);
                           //   $('.chat_user_list').hide();
                           // }

                        }
                     });
                  } else {
                     $('.edit_user_error_div').html('Select Group Members');
                  }

               }

            });

            $(document).on('click', '.delete_group', function() {
               var group_id = $(this).attr('data-id');
               var data = {};
               data['group_id'] = group_id;
               $('.LoadingImage').show();
               $.ajax({
                  url: '<?php echo base_url(); ?>User_chat/delete_group_chat/' + group_id,
                  type: 'POST',
                  data: data,
                  success: function(data) {

                     $('.LoadingImage').hide();
                     $('.box_' + group_id).hide();
                     $('#popup_group_edit_' + group_id).modal('hide');
                     $('.chatbox_' + group_id).hide();
                     $('.modal-backdrop.show').hide();
                  }
               });

            });
         </script>

         <script src="<?php echo base_url() ?>assets/menudropdown/fm.selectator.jquery.js"></script>
         <script type="text/javascript">
            $(function() {
               var $activate_selectator = $('#activate_selectator4');
               $activate_selectator.click(function() {
                  var $select = $('.users_list');
                  if ($select.data('selectator') === undefined) {
                     $select.selectator({
                        showAllOptionsOnFocus: true,
                        searchFields: 'value text subtitle right',
                        placeholder: 'Members',
                     });
                     $activate_selectator.val('destroy');
                  } else {
                     $select.selectator('destroy');
                     $activate_selectator.val('activate');
                  }
               });
               $activate_selectator.trigger('click');
            });
         </script>