<?php $this->load->view('includes/header');?>
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link" href="javascript:;">Reports</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                              </div>
                              <div class="all_user-section2 floating_set">
                                 <div class="tab-content">
                                    <div class="reports-tabs">
                                      <div class="card-comment ">
                                         <div class="card-block-small">
                                          <div class="left-sireport col-xs-12 col-md-6 col-md-3">
                                            <div class="comment-desc">
                                               <h6>Clients</h6>
                                               <p class="text-muted ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                                               <div class="comment-btn">
                                                  <button class="btn bg-c-blue btn-round btn-comment" data-toggle="modal" data-target="#client_download">Run</button>
                                                   <button class="btn bg-c-pink btn-round btn-comment" data-toggle="modal" data-target="#client_customization">Customise</button>
                                               </div>
                                            </div>
                                          </div>


                                          <div class="left-sireport col-xs-12 col-md-6 col-md-3">
                                            <div class="comment-desc">
                                               <h6>Luciano Durk</h6>
                                               <p class="text-muted ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                                               <div class="comment-btn">
                                                  <button class="btn bg-c-blue btn-round btn-comment ">Run</button>
                                                  <button class="btn bg-c-pink btn-round btn-comment ">Customise</button>
                                               </div>
                                            </div>
                                          </div>
                                          <div class="left-sireport col-xs-12 col-md-6 col-md-3">
                                            <div class="comment-desc">
                                               <h6>Luciano Durk</h6>
                                               <p class="text-muted ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                                               <div class="comment-btn">
                                                  <button class="btn bg-c-blue btn-round btn-comment ">Run</button>
                                                  <button class="btn bg-c-pink btn-round btn-comment ">Customise</button>
                                               </div>
                                            </div>
                                          </div>
                                         </div>
                                      </div>
                                    </div>
                                    <!-- home-->
                                    <!-- frozen -->
                                 </div>
                              </div>
                              <!-- admin close -->
                           </div>
                           <!-- Register your self card end -->
                        </div>
                     </div>
                  </div>
                  <!-- Page body end -->
               </div>
            </div>
            <!-- Main-body end -->
            <div id="styleSelector">
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<!-- email all -->
<input type="hidden" name="user_id" id="user_id" value="">

 <div class="modal fade" id="client_download" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reports</h4>
        </div>
        <div class="modal-body">        
               <a href="<?php echo base_url(); ?>Reports/chart" class="btn btn-primary" id="client_pdf">Pdf</a>
               <a href="<?php echo base_url(); ?>Reports/excel_download" class="btn btn-primary">Excel</a>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


<!-- filter add -->

  <div class="modal fade" id="client_customization" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Customization</h4>
        </div>
        <div class="modal-body">
        <div class="col-md-12 cust1-popup">
         <div class="col-md-6">
          <label>From Date</label>
          <input type="text" class="date-picker" id="from_date" value="">
        </div>
         <div class="col-md-6">
          <label>To Date</label>
          <input type="text" class="date-picker" id="to_date" value="">
         </div>
       </div>
        <div class="col-md-12 cust-popup">
        <div class="col-md-6">
          <label>Priority</label>  
          </div>
          <div class="col-md-6">       
            <select id="legal_form">          
            <option value="all">All</option>
            <option value="Private Limited company">Private Limited company</option>
            <option value="Public Limited company">Public Limited company</option>
            <option value="Limited Liability Partnership">Limited Liability Partnership</option>
            <option value="Partnership">Partnership</option>
            <option value="Self Assessment">Self Assessment</option>
            <option value="Trust">Trust</option>
            <option value="Charity">Charity</option>
            <option value="Other">Other</option>
            </select>
        </div>
        </div>
         <div class="col-md-12 cust-popup">
         <div class="col-md-6">
          <label>Status</label>
          </div>
          <div class="col-md-6">
          <select id="status" name="stati">
          <option value="all" hidden="">Any</option>          
          <option value="1">Active</option>
          <option value="2">In-Active</option>
          <option value="3">Fozen</option>
          </select> 
          </div>
        </div>         
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default select_template" id="client_customisation">Search</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>




<div class="card-block fileters_client">
<div id="chart_Donut11" style="width: 100%; height: 309px;"></div>
</div>

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/cdns/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['bar']}]}"></script>
<script type="text/javascript">
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Legal Form', 'Count'],
      ['Private Limited company',     <?php echo $Private['limit_count']; ?>],
      ['Public Limited company',      <?php echo $Public['limit_count']; ?>],
      ['Limited Liability Partnership',  <?php echo $limited['limit_count']; ?>],
      ['Partnership', <?php echo $Partnership['limit_count']; ?>],
      ['Self Assessment',    <?php echo $self['limit_count']; ?>],
      ['Trust',<?php echo $Trust['limit_count']; ?>],
      ['Charity',<?php echo $Charity['limit_count']; ?>],
       ['Other',<?php echo $Other['limit_count']; ?>],
    ]);
  
    var options = {
      title: 'Client Lists',
      pieHole: 0.4,
    };
  
    var chart_div = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
    var chart = new google.visualization.PieChart(chart_Donut11);
    google.visualization.events.addListener(chart, 'ready', function () {
    chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
    //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
    var image=chart.getImageURI();
       $(document).ready(function(){   
                 $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>/Reports/pdf_download",         
                        data: {image: image},
                        success: 
                             function(data){
                    
                             }
                         });
                  });
  });
    chart.draw(data, options);
  }

</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.tagsinput.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/cdns/jquery.dataTables.min.js"></script> 
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/js/jquery.tagsinput.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
    $( function() {
    $( ".date-picker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
    });
  });

$("#client_customisation").click(function(){
 var fromdate=$("#from_date").val();
 var todate=$("#to_date").val();
 var legal_form=$("#legal_form").val();
 var status=$("#status").val();
 var formData={'fromdate':fromdate,'todate':todate,'legal_form':legal_form,'status':status}
$.ajax({
      type: "POST",
      url: "<?php echo base_url();?>/Reports/Client_customization",         
      data: formData,
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {
       // alert('ok');
          $(".LoadingImage").hide(); 
          if(data!='1'){         
            $(".fileters_client").html('');
            $(".fileters_client").append(data);    
            $("#client_customization").modal('hide');            
          }
            $("#client_download").modal('show');          
      }
    });

});
</script>