<div class="modal fade" id="popup-2" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Feedback Form</h4>
      </div>

      <div class="modal-body">
        <form action="<?php echo base_url(); ?>User/feedMail" id="feed_form" method="post" name="feed_form">
          <style>
            input.error {
              border: 1px dotted red;
            }

            label.error {
              width: 100%;
              color: red;
              font-style: italic;
              margin-left: 120px;
              margin-bottom: 5px;
            }
          </style>

          <label><b>Your content</b></label>
          <textarea name="feed_msg" id="feed_msg" placeholder="Type your text here..."></textarea>
          <!-- <br><br> -->

          <div class="modal-footer">
            <div class="feedback-submit mail_bts1">
              <div class="feed-submit text-left">
                <input id="feed" name="submit" type="submit" value="submit">
              </div>
            </div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>

        </form>
      </div>

    </div>

  </div>
</div>



<div id="Confirmation_popup" class="modal fade" role="dialog" style="display:none;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align-last: center">Confirmation</h4>
      </div>
      <div class="modal-body">
        <label class="information"></label>
      </div>
      <div class="modal-footer">
        <button class="Confirmation_OkButton">Save</button>
        <button class="Confirmation_CancelButton" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<input type="hidden" name="user_id" id="user_id" value="<?php if (isset($client[0]['user_id']) && ($client[0]['user_id'] != '')) {
                                                          echo $client[0]['user_id'];
                                                        } ?>" class="fields">

<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/cdns/moment.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/cdns/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common_script.js"></script>
<!--       <script src="<?php echo base_url(); ?>assets/js/mock.js"></script> -->
<script src="<?php echo base_url(); ?>assets/js/hierarchy-select.min.js"></script>

<?php $url = $this->uri->segment('2'); ?>
<?php if ($url == 'step_proposal' || $url == 'copy_proposal' || $url == 'template_design' || $url == 'templates') { ?>
  <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<?php } ?>

<script src="<?php echo base_url(); ?>assets/cdns/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dropdown.js"></script>


<script type="text/javascript">
  $(document).ready(function() {
    /*close button action for server response popup*/
    $("#server_response_popup_close").click(function() {
      $('div.modal-alertsuccess').hide();
    });
    /**/
    $(".theme-loader").remove();

    $('.dropdown-sin-21234').dropdown({
      limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });

    $('th').on("click.DT", function(e) {
      //stop Propagation if clciked outsidemask
      //becasue we want to sort locally here
      if (!$(e.target).hasClass('sortMask')) {
        e.stopImmediatePropagation();
      }
    });
  });

  $(document).ready(function() {
    $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");
    $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");

    $(document).on('click', '.remove_notify,.archive_notify,#confirm_button,.read_notify', function() {
      //  alert('ok');
      var type = $("#notification_type").val();
      var section = $("#notification_section").val();
      var users = $("#notification_users").val();
      var id = $("#notification_id").val();
      var source = $("#SorceFrom").val();

      var formData = {
        'type': type,
        'section': section,
        'users': users,
        'id': id,
        'source': source
      };
      $.ajax({
        url: '<?php echo base_url(); ?>Sample/notification_section',
        type: 'POST',
        data: formData,
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          if (data == '1') {
            /*****/
            $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");
            $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");
            //location.reload();
          }
          $('#notification_modal .close_popup').trigger('click');

          if (users != "" && (section == 'proposal' || section == 'deadline' || section == 'invoice' || section == 'repeat-invoice')) {
            if (section == 'proposal') {
              <?php //$_SESSION['firm_seen'] = 'accept'; 
              ?>
            }
            window.location.href = users;
          }

          var uri = '<?php echo $_SERVER['REQUEST_URI'] ?>';

          if (uri == '/Notification/notification_list') {
            $('.alert-success').find('.position-alert1').html('');
            $('.alert-success').find('.position-alert1').html('Updated Successfully!');
            $('.alert-success').show();
            setTimeout(function() {
              $('.alert-success').hide();
              location.reload();
            }, 3000);
          }

          $(".LoadingImage").hide();
        }
      });
    });

    $(document).on("click", ".notifi_status", function(e) {
      var task_id = $(this).data('task_id');
      var id = $(this).data('id');

      $.ajax({
        url: '<?php echo base_url(); ?>user/update_notification/',
        type: 'post',

        data: {
          'id': id
        },

        success: function(data) {
          window.location.href = "<?php echo base_url() ?>user/task_details/" + task_id;
        }
      });
    });


    $(document).on("click", ".doc_notifi_status", function(e) {
      //  var task_id=$(this).data('task_id');
      var id = $(this).data('id');
      var link = $(this).data('link');

      $.ajax({
        url: '<?php echo base_url(); ?>user/update_notification/',
        type: 'post',

        data: {
          'id': id
        },

        success: function(data) {
          if (link != '') {
            window.location.href = "<?php echo base_url() ?>Documents/?dir=/" + link;

          } else {
            window.location.href = "<?php echo base_url() ?>Documents";
          }
        }
      });
    });


    $(document).on("click", ".leads_notifi_status", function(e) {
      var leads_id = $(this).data('leads_id');
      var id = $(this).data('id');

      $.ajax({
        url: '<?php echo base_url(); ?>user/update_notification/',
        type: 'post',

        data: {
          'id': id
        },

        success: function(data) {
          window.location.href = "<?php echo base_url() ?>leads/leads_detailed_tab/" + leads_id;
        }
      });
    });

    $('.select-dropdown span input[type="checkbox"]').wrap('<label class="custom_checkbox1"></label>');
    $('.custom_checkbox1 input').after("<i></i>")
    //           var config ={
    //           language:'en',
    //           uiColor:'#fff',
    //           height:300,
    //           toolbarCanCollapse:true,
    //           toolbarLocation:'bottom'
    //           };
    //            CKEDITOR.replace('feed_msg' , config); 
    // //}catch(e){alert(e);}
  });
</script>

<script>
  var switcheryElm = [];
  $(document).ready(function() {

    var date = $('.datepicker').datepicker({
      dateFormat: 'dd-mm-yy',
      minDate: 0
    }).val();

    // Multiple swithces
    var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));

    var index = 1;
    elem.forEach(function(html) {
      var switchery = new Switchery(html, {
        color: '#22b14c',
        jackColor: '#fff',
        size: 'small'
      });
      //var index =((!html.getAttribute('id')) ? '1' : html.getAttribute('id') );
      ///console.log(html.getAttribute('id') );
      html.setAttribute('data-switcheryId', index);
      switcheryElm[index] = switchery;
      index++;
    });

    $('#accordion_close').on('click', function() {
      $('#accordion').slideToggle(300);
      $(this).toggleClass('accordion_down');
    });

    $('.chat-single-box .chat-header .close').on('click', function() {
      $('.chat-single-box').hide();
    });

    var resrow = $('#responsive-reorder').DataTable({
      rowReorder: {
        selector: 'td:nth-child(2)'
      },
      responsive: true
    });
  });
</script>
</body>
<!-- chat box -->
<!-- modal 1-->
<div class="modal fade all_layout_modal" id="default-Modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Search Companines House</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group input-group-button input-group-primary modal-search">
          <input type="text" class="form-control" placeholder="Search here..." id="searchCompany">
          <button class="btn btn-primary input-group-addon" id="basic-addon1">
            <i class="ti-search" aria-hidden="true"></i>
          </button>
        </div>
        <div class="british_gas1 trading_limited" id="searchresult">
        </div>
        <div class="british_view" style="display:none" id="companyprofile">
        </div>
        <div class="main_contacts" id="selectcompany" style="display:none">
          <h3>british gas trading limited</h3>
          <div class="trading_tables">
            <div class="trading_rate">
              <p>
                <span><strong>BARBARO,Gab</strong></span>
                <span>Born 1971</span>
                <span><a href="#">Use as Main Contact</a></span>
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- modalbody -->
    </div>
  </div>
</div>
<!-- modal-close -->
<!--modal 2-->
<div class="modal fade all_layout_modals" id="exist_person" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Select Existing Person</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="main_contacts" id="main_contacts" style="display:block">
        </div>
      </div>
      <!-- modalbody -->
    </div>
  </div>
</div>
<!-- modal-close -->
<!-- modal 2-->
<div id="Confirmation_popup" class="modal fade" role="dialog" style="display:none;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align-last: center">Confirmation</h4>
      </div>
      <div class="modal-body">
        <label class="information"></label>
      </div>
      <div class="modal-footer">
        <button class="Confirmation_OkButton">Save</button>
        <button class="Confirmation_CancelButton" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</html>
<script>
  //class="modal fade all_layout_modal show"
  function company_model_close() {
    //console.log('aaa');
    $('.all_layout_modal').modal('hide');
    //   $('button.close').trigger('click');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();

    $("#default-Modal").hide();
    // $('.modal-backdrop.show').hide();
    // $('.modal-backdrop.show').each(function(){
    //  $(this).remove();
    // });
  }

  function debounce(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this,
        args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

  $('#searchCompany').keyup(debounce(function() {
    $(".form_heading").html("<h2>Adding Client via Companies House</h2>");
    var $this = $(this);
    var term = $this.val();
    type_check(term);
  }, 500));

  function type_check(term) {
    $.ajax({
      url: '<?php echo base_url(); ?>client/SearchCompany/',
      type: 'post',
      data: {
        'term': term
      },
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {
        $("#searchresult").html(data);
        $(".LoadingImage").hide();
      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  }

  function checked(term) {
    $.ajax({
      url: '<?php echo base_url(); ?>client/SearchCompany/',
      type: 'post',
      data: {
        'term': term
      },
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {
        $("#searchresult").html(data);
        $(".LoadingImage").hide();
      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  }

  function addmorecontact(id) {
    $(".all_layout_modal").modal('hide');
    $(".LoadingImage").show();
    $(".modal-backdrop").css("display", "none");
    var url = $(location).attr('href').split("/").splice(0, 8).join("/");
    var segments = url.split('/');
    var fun = segments[5];
    var values = '<?php echo $_SESSION['roleId']; ?>';
    //alert(values);
    if (fun != '' && fun != 'addnewclient' && fun != 'addnewclient#') {

      <?php // if($_SESSION['roleId']=='1'){ 
      ?>
      // $(location).attr('href', '<?php echo base_url(); ?>firm/add_firm/'+id);
      <?php //}else{ 
      ?>

      $(location).attr('href', '<?php echo base_url(); ?>client/addnewclient/' + id);
      <?php //} 
      ?>
    } else {
      <?php // if($_SESSION['roleId']=='1'){ 
      ?>
      // $(location).attr('href', '<?php echo base_url(); ?>firm/add_firm/'+id);
      <?php // }else{ 
      ?>
      $(location).attr('href', '<?php echo base_url(); ?>client/addnewclient/' + id);
      <?php  //} 
      ?>
      $("#companyss").attr("href", "#");

      $('.contact_form').show();
      $('.all_layout_modal').modal('hide');

      $('.nav-link').removeClass('active');
      //   $('.main_contacttab').addClass('active');
      //$('.contact_form').show();

      $("#company").removeClass("show");
      $("#company").removeClass("active");
      $("#required_information").removeClass("active");
      $(".main_contacttab").addClass("active");
      $("#main_Contact").addClass("active");
      $("#main_Contact").addClass("show");
    }


  }

  var xhr = null;

  function getCompanyRec(companyNo) {
    $(".LoadingImage").show();
    if (xhr != null) {
      xhr.abort();
      xhr = null;
    }

    xhr = $.ajax({
      url: '<?php echo base_url(); ?>client/CompanyDetails/',
      type: 'post',
      data: {
        'companyNo': companyNo
      },
      success: function(data) {
        //alert(data);
        $("#searchresult").hide();
        $('#searchCompany').prop('readonly', true);
        $('#companyprofile').show();
        $("#companyprofile").html(data);
        $(".LoadingImage").hide();

      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  }


  function getCompanyView(companyNo) {
    //alert(companyNo);
    $('.modal-search').hide();
    $(".LoadingImage").show();
    var user_id = $("#user_id").val();

    if (xhr != null) {
      xhr.abort();
      xhr = null;
    }


    xhr = $.ajax({
      url: '<?php echo base_url(); ?>client/selectcompany/',
      type: 'post',
      dataType: 'JSON',
      data: {
        'companyNo': companyNo,
        'user_id': user_id
      },
      success: function(data) {
        //alert(data.html);
        $("#searchresult").hide();
        $('#companyprofile').hide();
        $('#selectcompany').show();
        $("#selectcompany").html(data.html);
        $('.main_contacts').html(data.html);
        // append form field value
        $("#company_house").val('1');
        //alert($("#company_house").val());
        $("#company_name").val(data.company_name);
        $("#company_name1").val(data.company_name);
        $("#company_number").val(data.company_number);
        $("#companynumber").val(data.company_number);
        $("#company_url").val(data.company_url);
        $("#company_url_anchor").attr("href", data.company_url);
        $("#officers_url").val(data.officers_url);
        $("#officers_url_anchor").attr("href", data.officers_url);
        $("#company_status").val(data.company_status);
        $("#company_type").val(data.company_type);

        // console.log('comapny type ');
        // console.log(data.company_type);
        // console.log('comapny type test ');
        //$("#company_sic").append(data.company_sic);
        $("#company_sic").val(data.company_sic);
        $("#sic_codes").val(data.sic_codes);

        $("#register_address").val(data.address1 + "\n" + data.address2 + "\n" + data.locality + "\n" + data.postal);
        $("#allocation_holder").val(data.allocation_holder);
        $("#date_of_creation").val(data.date_of_creation);
        $("#period_end_on").val(data.period_end_on);
        $("#next_made_up_to").val(data.next_made_up_to);
        $("#next_due").val(data.next_due);
        $("#accounts_due_date_hmrc").val(data.accounts_due_date_hmrc);
        $("#accounts_tax_date_hmrc").val(data.accounts_tax_date_hmrc);
        $("#confirm_next_made_up_to").val(data.confirm_next_made_up_to);
        $("#confirm_next_due").val(data.confirm_next_due);

        $("#tradingas").val(data.company_name);
        $("#business_details_nature_of_business").val(data.nature_business);
        $("#user_id").val(data.user_id);

        //  $(".edit-button-confim").show(); //for hide edit option button 
        $(".LoadingImage").hide();

      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  }


  function getmaincontact(companyNo, appointments, i, user_id, obj) {
    $(".LoadingImage").show();

    var data = {
      'companyNo': companyNo,
      'appointments': appointments,
      'count': i,
      'user_id': user_id
    };
    var exist_contact = 0;
    var modal = $(obj).closest('.all_layout_modal');

    if (modal.attr('id') == "company_house_contact") //check it is edit page popup
    {
      exist_contact = 1;
      var cnt = $("#append_cnt").val();

      if (cnt == '') {
        cnt = 1;
      } else {
        cnt = parseInt(cnt) + 1;
      }
      data['cnt'] = cnt;
      $("#append_cnt").val(cnt);

    }

    xhr = $.ajax({
      url: '<?php echo base_url() ?>client/maincontact/',
      type: 'post',
      data: data,
      success: function(data) {

        modal.find('#selectcompany #usecontact' + i).html('<span class="succ_contact"><a href="#">Added</a></span>');

        if (exist_contact) {

          $('.contact_form').append(data);
          if (i == 'First') {
            $(".contact_form .common_div_remove-" + cnt).find('.make_primary_section').trigger('click');
          }
          /** 05-07-2018 rs **/
          $('.make_a_primary').each(function() {
            var countofdiv = $('.make_a_primary').length;
            if (countofdiv > 1) {
              var id = $(this).attr('id').split('-')[1];

              $('.for_remove-' + id).remove();
              $('.for_row_count-' + id).append('<div class="btn btn-danger remove for_remove-' + id + '"><a href="javascript:void(0)" class="contact_remove" id="remove-' + id + '">Remove</a></div>');
            }
          });
          var start = new Date();
          start.setFullYear(start.getFullYear() - 70);
          var end = new Date();
          end.setFullYear(end.getFullYear());
          $(".date_picker_dob").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            yearRange: start.getFullYear() + ':' + end.getFullYear()
          }).val();
          modal.modal('hide');
          sorting_contact_person($('.contact_form').find(".CONTENT_CONTACT_PERSON").filter(":last"));
        }
        console.log(typeof trigger_contact_person_validation);
        if (typeof trigger_contact_person_validation == "function") {
          trigger_contact_person_validation();
        }
        $(".LoadingImage").hide();

      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  }

  function backto_company() {
    $('#companyprofile').show();
    $('.main_contacts').hide();
  }
</script>

<script type="text/javascript">
  $(function() {

    var headheight = $('.header-navbar').outerHeight();
    var navbar = $('.remin-admin');
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {

    log_update();





    $("a#mobile-collapse").click(function() {
      $('#pcoded.pcoded.iscollapsed').toggleClass('addlay');
    });

    $(".nav-left .data1 li.comclass a.com12").click(function() {
      $(this).parent('.nav-left .data1 li.comclass').find('ul.dropdown-menu1').slideToggle();
      $(this).parent('.nav-left .data1 li.comclass').siblings().find('ul.dropdown-menu1:visible').slideUp();

    });



    var test = $('body').height();
    $('test').scrollTop(test);

  });
</script>

<?php




if ($_SESSION['permission']['Task']['view'] == 1) {

  $res2 = Get_Assigned_Datas('TASK');
  $res = (!empty($res2)) ? implode(',', array_filter(array_unique($res2))) : '-1';
  $data_task_list = $result_val = array();

  if ($res != '') {

    $data_task_list = $this->db->query("SELECT * FROM add_new_task AS tl where tl.id in ($res) and tl.firm_id = " . $_SESSION['firm_id'] . " and related_to!='sub_task' order by id desc")->result_array();
  }
  $result_val = array_values(array_unique(array_column($data_task_list, 'task_status')));



  if (in_array('4', $result_val)) { ?>
    <script>
      $(document).ready(function() {
        var need_url = '<?php echo base_url() ?>user/task_list';

        $('.dropdown-menu1 li a').each(function() {


          if ($(this).attr('href') == need_url) {
            $(this).closest('ul').append('<li class="column-setting0 atleta archive_li"><a href="<?php echo base_url() ?>user/task_list/archive_task">Archive Task</a>   </li>');
          }


        })
      });
    </script>
  <?php   }
  //0print_r($result_val);
  if (in_array('5', $result_val)) {
    // echo "chk";

  ?>

    <script>
      $(document).ready(function() {
        var need_url = '<?php echo base_url() ?>user/task_list';
        console.log(need_url);
        $('.dropdown-menu1 li a').each(function() {


          if ($(this).attr('href') == need_url) {
            // $(this).parent('.dropdown-menu').append('  <li class="column-setting0 atleta"><a href="https://remindoo.uk/Task">Tasks Timeline</a>   </li>');
            $(this).closest('ul').append('<li class="column-setting0 atleta completed_li"><a href="<?php echo base_url() ?>user/task_list/complete_task">Completed Task</a>   </li>');
          }



        })
      });
    </script>

<?php }
}





?>


<script type="text/javascript">
  $(document).ready(function() {
    $('.dataTables_wrapper').each(function() {
      var datatable_id = $(this).attr('id');
      var datatable_id_cur = datatable_id.replace('_wrapper', '');
      if ($(this).find('.dataTables_scrollBody').prop("scrollHeight") > $(this).find('.dataTables_scrollBody').height()) {
        $(this).find('.dataTables_scrollHeadInner').css({
          "padding-right": "21px",
          "min-width": "calc(100% - 21px)"
        });
      }
      $('#' + datatable_id_cur).DataTable().columns.adjust();
      setTimeout(function() {
        $('#' + datatable_id_cur).DataTable().columns.adjust();
      }, 1000);
    });


    // $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
    // var target_id = $(this).attr('href');
    // var href = target_id.substring(target_id.indexOf("#") + 1); 
    // var datatable_id = $("#"+href).find('table').attr('id');
    // var datatable_id_cur = datatable_id.replace('_wrapper','');
    // if($(this).find('.dataTables_scrollBody').prop("scrollHeight") > $(this).find('.dataTables_scrollBody').height()){
    //     $(this).find('.dataTables_scrollHeadInner').css({"padding-right": "21px", "min-width": "calc(100% - 21px)"}); 
    // }
    // $('#'+ datatable_id_cur).DataTable().columns.adjust();
    // setTimeout(function(){$('#'+ datatable_id_cur).DataTable().columns.adjust();}, 1000);

    // });

  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $(document).on('show.bs.modal', '.modal', function(event) {
      var zIndex = 1040 + (10 * $('.modal:visible').length);
      $(this).css('z-index', zIndex);
      setTimeout(function() {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
      }, 0);
    });

    // $('.modal').modal({backdrop: 'static', keyboard: false,show: false});

    $(document).on('click', '.modal .hasDatepicker', function() {
      $('.ui-datepicker').addClass('uidate_visible');
    });

    $(document).on('click', '.modal button[data-dismiss="modal"]', function() {
      $('.ui-datepicker').removeClass('uidate_visible');
    });
  });

  $(document).ready(function() {


    $(".modal-alertsuccess").each(function() {
      if ($(this).find('.newupdate_alert').length < 1) {
        $(this).wrapInner("<div class='newupdate_alert'></div>");
      }
    });
    /*importent for datatable scroll*/
    $(document).ready(function() {
      $('table.dataTable').wrap('<div class="scroll_table"></div>');

      $(document).on('click', '.multiselect .selectBox', function(e) {
        e.stopImmediatePropagation();
        $('#checkboxes').show();
      });

      $('.dataTables_wrapper .toolbar-table').addClass('select_visibles');

    });
    /*important for datatable scroll*/

    $(document).mouseup(function(e) {
      var container = $(".multiple-select-dropdown");
      // if the target of the click isn't the container nor a descendant of the container
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        //console.log('ok');
        $('.themicond').removeClass('shown');
        $(".multiple-select-dropdown").removeClass('Show_content');
      }

    });
    $(document).on('click', '.dt-button-background', function(e) {
      $('.dt-buttons').find('.dt-button-collection').detach();
      $('.dt-buttons').find('.dt-button-background').detach();
    });

    $('th .themicond').on('click', function(e) {
      if ($(this).find(".dropdown-content").hasClass("Show_content")) {
        alert('showcontent is visible');
        $(this).parent().find('.dropdown-content').removeClass('Show_content');
      }
    });


    $('.modal').on("shown.bs.modal", function() {
      var winheight1 = $(this).find('.modal-dialog').outerHeight();

      if (winheight1 < 400) {
        $(this).find('.modal-dialog').addClass('normal_heights');
      }
    });

    $('.modal').on("hide.bs.modal", function() {
      var winheight1 = $(this).find('.modal-dialog').outerHeight();

      if (winheight1 < 400) {
        $(this).find('.modal-dialog').removeClass('normal_heights');
      }
    });


  });
  var Conf_Confirm_Popup = function(conf) {

    if ('OkButton' in conf) {
      var text = ('Text' in conf.OkButton ? conf.OkButton.Text : 'Yes');
      var fun = ('Handler' in conf.OkButton ? conf.OkButton.Handler : function() {});

      $('#Confirmation_popup .Confirmation_OkButton').html(text).unbind().on('click', fun);

    }

    if ('CancelButton' in conf) {
      var text = ('Text' in conf.CancelButton ? conf.CancelButton.Text : 'No');
      var fun = ('Handler' in conf.CancelButton ? conf.CancelButton.Handler : function() {});

      $('#Confirmation_popup .Confirmation_CancelButton').html(text).unbind().on('click', fun);
    }

    if ('Info' in conf) {
      $('#Confirmation_popup .information').html(conf.Info);
    }

  };
  var Show_LoadingImg = function() {
    $('.LoadingImage').show();
  };
  var Hide_LoadingImg = function() {
    $('.LoadingImage').hide();
  };






  function check_name(obj, id, event, target_url) {
    var name = $(obj).val();

    if (name != "") {
      $.ajax({
        url: target_url,
        type: 'POST',
        data: {
          name: name,
          id: id
        },
        async: false,

        beforeSend: function() {
          $('.LoadingImage').show();
        },

        success: function(data) {
          var resp = JSON.parse(data);
          $(".LoadingImage").hide();

          if (resp.count != 0) {
            $(obj).next('#name_er').html('');
            $(obj).next('#name_er').html('Already Exists.');
            event.preventDefault();
          } else {
            $(obj).next('#name_er').html('');
            $('.LoadingImage').show();
          }
        }
      });
    }
  }

  if ($(window).width() < 767) {
    $(".dropdown-menu1").each(function() {
      $(this).closest('li').find('.dropdown-toggle').addClass("drop_opens");
    });

    $('.drop_opens').on('click', function(e) {
      $(this).next('.dropdown-menu1').slideToggle(300);
    });
  }


  function log_update() {
    $.ajax({
      url: '<?php echo base_url(); ?>Login/login_time_update/',
      type: 'POST',
      dataType: 'json',

    });
  }
  // Login check update //
  var log_updatetime = 10 * 60 * 1000;

  window.setInterval(function() {

    log_update();

  }, log_updatetime);

  //End  Login check update //
</script>