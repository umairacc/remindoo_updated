<!DOCTYPE html>
<html lang="en">
   <head>
      <title>CRM </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="description" content="#">
      <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
      <meta name="author" content="#">
      <!-- Favicon icon -->
      
      <!-- Google font-->
      <link href="<?php echo base_url(); ?>assets/cdns/css.css" rel="stylesheet">
      <!-- Required Fremwork -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/bootstrap/css/bootstrap.min.css">
      <!-- themify-icons line icon -->
      
     <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">
      <!-- ico font -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icon/icofont/css/icofont.css">
      <!-- flag icon framework css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/flag-icon/flag-icon.min.css">
      <!-- Menu-Search css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/menu-search/css/component.css">
      <!-- jpro forms css -->
      
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/j-pro/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/j-pro/css/j-pro-modern.css">
      <!-- Style.css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/common_style.css?ver=4">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/common_responsive.css?ver=2">
      <!-- Google reCaptcha -->
      
   </head>