<?php
Class Common_mdl extends CI_Model {

   /*Coded By Ajith*/
   /*=======================================================*/
    public function Get_ClientReferral_Code( $data )
    {
        $code = base_url().'referral/'.slug_it( $data , ['delimiter' => '','replacements'=>['/\//'=>''] ] );

        $result = $this->db->query("SELECT id FROM client WHERE crm_other_send_invit_link='".$code."'")->num_rows();

        $i=1;
        while( $result )
        {
            $code =$code.'_'.$i;  
            $result = $this->db->query("SELECT id FROM client WHERE crm_other_send_invit_link='".$code."'")->num_rows();
            $i++;
        }
        return $code;
    }

    public function Get_ClientReferral_Tree( $client_id )
    {
        $Client = $this->select_record("client","id",$client_id); 
        $source = ['name'=>$Client['crm_company_name']];

        $Childs  = $this->db->query("SELECT crm_company_name FROM client WHERE crm_refered_by=".$client_id)->result_array();

        if( count($Childs) > 0 )
        {
            $childers = [];
            foreach ($Childs as $key => $value)
            {
                $childers[] = ['name'=>$value['crm_company_name']];
            }
            $source['children'] = $childers;
        }

        if( !empty( $Client['crm_refered_by'] ) )
        {
            $parent = $this->select_record( "client" , "id" , $Client['crm_refered_by'] ); 
            $source = ['name'=>$parent['crm_company_name'],'children'=>[$source] ];
        }    

        return  $source;
    }

    public function Company_types()
    {
        $Company_types = array(
                'LTD'    => 'Private Limited company',
                'PLTD'   => 'Public Limited company',
                'plc'    => 'Public Limited company',
                'LLP'    => 'Limited Liability Partnership',
                'P`ship' => 'Partnership',
                'S.A'    => 'Self Assessment',
                'TT'     =>  'Trust',
                'Charity'=>  'Charity',
                'other'  =>  'Other'
              );
              
        return array_change_key_case($Company_types);
    }

    public function Update_Client_assignee($Client_id , array $assignees)
    {

      $this->db->query("delete from firm_assignees where firm_id=".$_SESSION['firm_id']." and module_name='CLIENT' and module_id=".$Client_id);

      foreach ($assignees as $key => $value)
      {
        $this->db->insert("firm_assignees",['module_name'=>"CLIENT",'module_id'=>$Client_id,"firm_id"=>$_SESSION['firm_id'],'assignees'=>$value]);
      }

    }

    public function Get_Deleted_FirmsIds()
    {
        $this->db->where('is_deleted',1);
        $data = $this->db->get('firm')->result_array();
        return implode(',' , array_column( $data , 'firm_id' ) );
    }

    public function Create_Log( $Module_name , $Module_id , $Log , $User_id='' , $Sub_module = '')
    {
    $data = [ 'module'      => $Module_name,
           'module_id'   => $Module_id,
           'log'         => $Log,
           'user_id'        => $User_id,
           'sub_module'  => $Sub_module,
           'CreatedTime' => time()
         ];
    $this->db->insert("activity_log" , $data );        

    return $this->db->insert_id();
    }

    public function get_FirmEnabled_Services()
    {
       $firm_data = $this->select_record( 'admin_setting' , 'firm_id' , $_SESSION['firm_id'] );
       $services  = json_decode( $firm_data['services'] ,true );
       return array_intersect( $services , [1] );
    }

    public function GetActiveFirm_Clients()
    {
        $this->db->select('client.id,client.firm_id');
        $this->db->from('client');
        $this->db->join('firm' ,'firm.firm_id=client.firm_id and firm.is_deleted!=1','inner');        
        $this->db->where('client.crm_company_number!=','');
        $this->db->order_by('client.firm_id', 'ASC');
        $results = $this->db->get()->result_array();
        //echo $this->db->last_query();
        return $results;
    }
    public function UpdateExit_or_Insert($table_n ,array $data ,array $condition = [])
    {   
        $is_Ex = 0;

        if( !empty( $condition ) )
        {
            $this->db->where( $condition );
            $is_Ex = $this->db->get( $table_n )->num_rows();
        }

        if( !$is_Ex )
        {
            $this->db->insert( $table_n , $data );
            return $this->db->insert_id();
        }
        else
        {
            $this->db->update( $table_n , $data , $condition );
            return $this->db->affected_rows();
        }

    }
    public function ReceiveCompanyHouseNotification(array $client_ids )
    {
        $this->db->where( 'status!=' , 0 );
        $this->db->where( 'module' , 'client');
        $this->db->where( 'notification_type' ,'CH');
        $this->db->where_in( 'sender_id' , $client_ids );
        return $this->db->get('notification_management')->result_array();
    }
    public function Increase_Decrease_ClientCounts( $firm_id , $type )
    {
        $client_limit = $this->Check_Firm_Clients_limit( $firm_id );

        if( $client_limit != 'unlimited' )
        {
            if( $type == '+1' )
            {
                $this->db->where( 'firm_id' , $firm_id );
                $this->db->set( 'no_of_clients_allowed' , 'no_of_clients_allowed+1' , FALSE );
                $this->db->update( 'firm' );
            }
            else if( $type == '-1' && $client_limit > 0 )
            {
                $this->Decrement_Firm_Clients_limit( $firm_id );
            }
        }
    }
    public function Check_Firm_User_limit( $firm_id )
    {
        $user_limit = $this->db->select( 'no_of_users_allowed' )
                              ->get_where( 'firm' , 'firm_id='.$firm_id )
                              ->row_array();
        return $user_limit['no_of_users_allowed'];                       

    }
    public function Increase_Decrease_UserCounts( $firm_id , $type )
    {
        $user_limit = $this->Check_Firm_User_limit( $firm_id );

        if( $user_limit != 'unlimited' )
        {
            if( $type == '+1' )
            {
                $this->db->where( 'firm_id' , $firm_id );
                $this->db->set( 'no_of_users_allowed' , 'no_of_users_allowed+1' , FALSE );
                $this->db->update( 'firm' );
            }
            else if( $type == '-1' && $user_limit > 0 )
            {   
                $this->db->where( 'firm_id' , $firm_id );
                $this->db->set( 'no_of_users_allowed' , 'no_of_users_allowed-1' , FALSE );    
                $this->db->update( 'firm' );
            }
        }
    } 
    /*public function MArray_GroupBy( array $source , $by )
    {

    }*/

   

    /*=======================================================*/


     public function get_upload_options($path)
    {  
        //upload an image options
        $config = array(
           'upload_path' => './'.$path,
           'allowed_types' => "gif|jpg|png|jpeg|jPG|JPG|docx|doc|pdf|xls",
           'overwrite' => FALSE,
           );
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        return $config;
    }


public function insert( $table, $post ) {
    //echo "<pre>"; print_r($post); exit;
    $this->db->insert($table, $post);
    //echo $this->db->last_query(); exit;
    if ($this->db->affected_rows() > 0) {
        $insertId = $this->db->insert_id();
        return $insertId;
    }
    else {
        return 0;
    }

    }
public function update( $table, $data, $field, $value )
    {

        $update = $this->db->where($field, $value)->update($table ,$data);
       // echo $this->db->last_query();//die;
        
        if ($this->db->affected_rows() > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }



    public function updatewithaffectedrow_count( $table, $data, $field, $value )
    {

        $update = $this->db->where($field, $value)->update($table ,$data);
        //echo $this->db->last_query();die;
        
        if ($update){
            return $this->db->affected_rows();
        }
        else {
            return 0;
        }
    }
 public function delete($table,$field,$id)
    {  
    $this->db->where($field, $id);
  $q = $this->db->delete($table);
  
    if ($this->db->affected_rows() > 0) {
            return 1;
        }
        else {
            return 0;
        }
    
    }

public function getallrecords($table){
            
        $query=$this->db->query("SELECT * from $table order by id desc");    
        return $query->result_array();

    }

    public function getall($table){
            
        $query=$this->db->query("SELECT * from $table order by parent_id asc");    
        return $query->result_array();

    }
    public function get_ClientMain_Contact( $UserId)
    {
        $query = $this->db->query("SELECT * from client_contacts where client_id = ".$UserId." ")->row_array();    
        return $query;
    }
public function select_record($table,$feild,$id){
            
        $query=$this->db->query("SELECT * from $table where $feild='$id' " );  
        //echo $this->db->last_query(); 
        return $query->row_array();

    }

    public function GetAllWithWhere($table,$feild,$id){
            
        $query=$this->db->query("SELECT * from $table where $feild='$id' " );  
        //echo $this->db->last_query(); 
        return $query->result_array();

    }

    /** 30-05-2018 for two where function **/
  public function GetAllWithWheretwo($table,$feild,$id,$feild1,$id1){
            
        $query=$this->db->query("SELECT * from $table where $feild='$id' and $feild1='$id1' " );  
        //echo $this->db->last_query(); 
        return $query->result_array();

    }


      public function GetAllWithWheretworow($table,$feild,$id,$feild1,$id1)
      {
            
        $query=$this->db->query("SELECT * from $table where $feild='$id' and $feild1='$id1' " );  
        echo $this->db->last_query(); 
        return $query->row_array();

    }



      public function GetAllWithWheretwoorderBy($table,$feild,$id,$feild1,$id1){
            
        $query=$this->db->query("SELECT * from $table where $feild='$id' and $feild1='$id1' order by id desc" );  
        echo $this->db->last_query(); 
        return $query->result_array();

    }
    /** end of 30-05-2018 **/
    /** 24-08-2018 for two where function with order by **/
        public function GetAllWithWhere_order_desc($table,$feild,$id,$orderby){
            
        $query=$this->db->query("SELECT * from $table where $feild='$id' order by $orderby desc " );  
        //echo $this->db->last_query(); 
        return $query->result_array();

    }

  public function GetAllWithWheretwo_order_desc($table,$feild,$id,$feild1,$id1,$orderby){
            
        $query=$this->db->query("SELECT * from $table where $feild='$id' and $feild1='$id1' order by $orderby desc " );  
        echo $this->db->last_query(); 
        return $query->result_array();

    }
    /** end of 30-05-2018 **/

    public function GetAllWithWhereorderBy($table,$feild,$id){
            
        $query=$this->db->query("SELECT * from $table where $feild='$id' order by id desc" );  
        //echo $this->db->last_query(); 
        return $query->result_array();

    }

    public function GetAllWithExceptField($table,$feild,$id){
            
        $query=$this->db->query("SELECT * from $table where $feild!='$id' and 'crm_email_id'!='' " );  
        //echo $this->db->last_query(); 
        return $query->result_array();

    }
    public function GetAllWithExceptFieldOrder($table,$feild,$id,$order){
            
        $query=$this->db->query("SELECT * from $table where $feild!='$id' order by id $order" );  
        //echo $this->db->last_query(); 
        return $query->result_array();

    }

public function login_check($table,$username,$pwd,$usernamefield,$passwordfield)
{
    $condition=array($usernamefield=>$username,$passwordfield=>$pwd);
    $this->db->where($condition);
    $query = $this->db->get($table);
    //echo $this->db->last_query();die;

    $this->db->where($condition);
    $query = $this->db->get($table);


    if ($query->num_rows() == 0) {
     return 0;
     }else{
        return $query->row_array();
     } 

}

public function pagination($per_page, $page,$total_rec) 
{ 
    $num_rows = $per_page;
    $res_count = $total_rec;
    $url = 'index/';
    
    $config = array();
    $config["base_url"] = base_url().$url;
    $config["total_rows"] = $num_rows;
    $config["per_page"] = $per_page;
    $config['use_page_numbers'] = TRUE;
    $config['num_links'] = $num_rows;
    $config['display_prev_link'] = TRUE;
    $config['full_tag_open'] = '<div class="pagination-buttons">';
    $config['full_tag_close'] = '</div>';
    $config['first_link'] = 'First';
    $config['last_link'] = 'Last';
    $config['cur_tag_open'] = '&nbsp;<span class="button active current pgn_links">';
    $config['cur_tag_close'] = '</span>';
    $config['num_tag_open'] = '<span class="button button_pagination_nav">';
    $config['num_tag_close'] = '</span>&nbsp;';
    $config['next_link'] = 'Next';
    $config['next_tag_open'] = '<span class="button button_pagination_nav">';
    $config['next_tag_close'] = '</span>&nbsp;';
    $config['prev_link'] = 'Previous';
    $config['prev_tag_open'] = '<span class="button button_pagination_nav">';
    $config['prev_tag_close'] = '</span>&nbsp;';
    $this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();

    $start = ( $page == 1 ) ? 0 : ($page*$per_page)-$per_page;

    //if($num_rows != "" || $num_rows != 0){
        $data['pgtext'] = ($start+1).'-'.( ( $per_page > $res_count ) ? ( $num_rows ) : ( $start+$per_page ) ).' of '. $num_rows.' Results';
    /*} else {
        $data['pgtext'] = '';
    }*/
    $data['db_data'] = $res['db_data'];


    $data['pglinks'] = explode('&nbsp;',$str_links );
    return $data;
}

function do_upload( $file, $path )
    {



        $files['tmp'] = $file;
       // print_r($files['tmp']);die;
        $config = $this->get_upload_options($path);
        $type = explode('.', $files['tmp']['name']);
        $_FILES['tmp']['name'] = time().'.'.end($type);
        $_FILES['tmp']['type']= $files['tmp']['type'];
        $_FILES['tmp']['tmp_name']= $files['tmp']['tmp_name'];
        $_FILES['tmp']['error']= $files['tmp']['error'];
        $_FILES['tmp']['size']= $files['tmp']['size'];    
        $this->upload->do_upload('tmp');
        $data = $this->upload->data();
        return $data['file_name'];
    }

function CategoryBasedRec($table,$fieldName,$categoryIds)
{
    $record = $this->db->query("select * from $table where $fieldName in ( ".implode(',', array_map('intval', $categoryIds))." )")->result_array();
    return $record;
}

function GetRowCount( $table )
{
    $query = $this->db->query("select * from $table");
    return $query->num_rows();
}
 function getUserDetails(){
        
        $response = array();
           
    $this->db->select('user_id,company_name,telephone_number,email,website,date_of_birth');     
    $q = $this->db->get('client');
        $response = $q->result_array();
        
        return $response;
    }

function getRole($userId)
{
    $query = $this->db->query("select role from user where id='$userId'");
    $roleId = $query->row_array();
    $role_id = $roleId['role'];
    $Rolequery = $this->db->query("select role from roles_section where id='$role_id'");
    $role = $Rolequery->row_array();
    return $role['role'];

}

function getUserProfilepic($userId)
{
    $query = $this->db->query("select * from user where id='".$userId."'");
    $row = $query->row_array();
    $crm_profile_pic = $row['crm_profile_pic'];
    if($crm_profile_pic!='')
    {
       $profile = base_url().'uploads/'.$row['crm_profile_pic'];
    }else {
        $profile = base_url().'uploads/unknown.png';
    }
    return $profile;
}

function getUserProfileName($userId)
{
    $query = $this->db->query("select * from user where id='$userId'");
    $row = $query->row_array();
    $crm_name = $row['crm_name'];
    // if($crm_profile_pic!='')
    // {
    //    $profile = base_url().'uploads/'.$row['crm_profile_pic'];
    // }else {
    //     $profile = base_url().'uploads/unknown.png';
    // }
    return $crm_name;
}

function getTag($tagId)
{
    $query = $this->db->query("select * from Tag where id='$tagId'");
    $row = $query->row_array();
    return $row['tag'];
}

function getfilterRec($status)
{
     $curDate = date('d-m-Y');
    if($status=='notstarted' || $status=='inprogress' || $status=='awaiting' || $status=='testing' || $status=='complete')
    {
        $query = $this->db->query("select * from add_new_task where task_status='$status'");
       
    }
    else if($status == 'today_task')
    {
        $query = $this->db->query("select * from add_new_task where start_date='$curDate'");
    }
    else if($status == 'due_date_passed')
    {
        $query = $this->db->query("select * from add_new_task where end_date <'$curDate'");

    }else if($status == 'upcoming_task')
    {
        $query = $this->db->query("select * from add_new_task where start_date > '$curDate'");
    }
    else if($status == 'assinged_to_me')
    {
        $assignedId = $this->getassignedTaskId('assigned');
        $query = $this->db->query("SELECT * FROM add_new_task WHERE id  IN ( $assignedId )");
    }
  
    else if($status == 'not_assigned')
    {
        $assignedId = $this->getassignedTaskId('not_assigned');

        $query = $this->db->query("SELECT * FROM add_new_task WHERE id  IN ( ".$assignedId." )");
    }

    else if($status == 'billable')
    {
        $query = $this->db->query("select * from add_new_task where billable ='Billable'"); 

    }
   /* else if($status == 'billed')
    {
        $query = $this->db->query("select * from add_new_task where billable ='billed'");

    }
    else if($status == 'not_billed')
    {
        $query = $this->db->query("select * from add_new_task where billable ='not_billed'");
    }
    else if($status == 'recuring')
    {

    }
    else if($status == 'assinged_im_followin')
    {

    }*/
    //echo $this->db->last_query();die;
     $response = $query->result_array();
        
    return $response;
}

function getassignedTaskId($rec)
{
    $allrec = $this->getallrecords('add_new_task');
    $ids = array();
    if($rec=='assigned'){
    foreach ($allrec as $key => $value) {
    $assignRec = $value['worker'].','.$value['manager'];
    $exp_work = explode(',', $assignRec);
    $userId = $_SESSION['userId'];
    
    if(in_array($userId, $exp_work))
    {
        $ids[] = $value['id'];
    }
    }
    }else{
    foreach ($allrec as $key => $value) {
    $assignRec = $value['worker'].','.$value['manager'];
    $exp_work = explode(',', $assignRec);
    $userId = $_SESSION['userId'];
    if(!in_array($userId, $exp_work))
    {
        $ids[] = $value['id'];
    }
    }  
    }

    return implode(',', $ids);

}

function getUsername($id)
{
        $query = $this->db->query("select * from user where id ='$id'"); 
        return $query->row_array();
}

function numToOrdinalWord($num)
{
    $first_word = array('eth','Primary','Secondary','Third','Fouth','Fifth','Sixth','Seventh','Eighth','Ninth','Tenth','Elevents','Twelfth','Thirteenth','Fourteenth','Fifteenth','Sixteenth','Seventeenth','Eighteenth','Nineteenth','Twentieth');
    $second_word =array('','','Twenty','Thirthy','Forty','Fifty');

    if($num <= 20)
        return $first_word[$num];

    $first_num = substr($num,-1,1);
    $second_num = substr($num,-2,1);

    return $string = str_replace('y-eth','ieth',$second_word[$second_num].'-'.$first_word[$first_num]);
}
//end class


public function add($data)
    {
        if (isset($data['disabled']))
        {
            $fields['active'] = 0;
        }
        else
        {
            $fields['active'] = 1;
        }

        if (isset($data['required']))
        {
            $fields['required'] = 1;
        }
        else
        {
            $fields['required'] = 0;
        }
        /*
        if (isset($data['show_on_pdf'])) {
            if (in_array($data['fieldto'], $this->pdf_fields)) {
                $data['show_on_pdf'] = 1;
            } else {
                $data['show_on_pdf'] = 0;
            }
        } else {
            $data['show_on_pdf'] = 0;
        }
          if (isset($data['disalow_client_to_edit'])) {
            $data['disalow_client_to_edit'] = 1;
        } else {
            $data['disalow_client_to_edit'] = 0;
        }
        if (isset($data['show_on_table'])) {
            $data['show_on_table'] = 1;
        } else {
            $data['show_on_table'] = 0;
        }

        if (isset($data['only_admin'])) {
            $data['only_admin'] = 1;
        } else {
            $data['only_admin'] = 0;
        }
        if (isset($data['show_on_client_portal'])) {
            if (in_array($data['fieldto'], $this->client_portal_fields)) {
                $data['show_on_client_portal'] = 1;
            } else {
                $data['show_on_client_portal'] = 0;
            }
        } else {
            $data['show_on_client_portal'] = 0;
        }
       if ($data['field_order'] == '') {
            $data['field_order'] = 0;
        }*/

        $fields['slug'] = slug_it('company' . '_' . $data['name'], ['delimiter' => '_'] );
        $slugs_total = total_rows('tblcustomfields', array('slug'=>$fields['slug']));
        if ($slugs_total > 0)
        {
            $fields['slug'] .= '_'.($slugs_total + 1);
        }
        $fields['show_on_pdf']            = 1;
        $fields['show_on_client_portal']  = 1;
        $fields['show_on_table']          = 1;
        $fields['only_admin']             = 0;
        $fields['disalow_client_to_edit'] = 0;

        $fields['type']         = $data['type'];
        $fields['options']      = $data['options'];
        $fields['name']         = $data['name'];        
        $fields['section_name'] = $data['section_name'];
        $fields['sub_content']  = $data['sub_content'];
        $fields['firm_id']      = $data['firm_id'];

        $this->db->insert('tblcustomfields', $fields);
        $insert_id = $this->db->insert_id();

        $cnt_order_query = "SELECT MAX(cnt_order) AS cnt_order FROM client_fields_management WHERE firm_id=".$data['firm_id']." AND section='".trim( $data['section_name'] )."' and content_name='".trim( $data['sub_content'] )."' ";

        $list_page_query = "SELECT MAX(list_page_order) as list_page_order  FROM client_fields_management WHERE firm_id=".$data['firm_id'];

        $section_value = $this->db->query( $cnt_order_query )->row_array();
        $list_page_order = $this->db->query( $list_page_query )->row_array();

               
        $datas['section']           = $data['section_name'];
        $datas['content_name']      = $data['sub_content'];
        $datas['label']             = $data['name'];
        $datas['cnt_order']         = $section_value['cnt_order'] + 1;
        $datas['cnt_visibility']    = 1; 
        $datas['field_propriety']   = $insert_id;   
        $datas['field_type']        = 1;
        $datas['list_page_order']   = $list_page_order['list_page_order'] + 1;
        $datas['list_page_visibility'] = 1; 
        $datas['firm_id']           = $data['firm_id'];

        $this->db->insert( 'client_fields_management' , $datas );

        if ($insert_id)
        {
            //logActivity('New Custom Field Added [' . $data['name'] . ']');
            return $insert_id;
        }

        return false;
    }

    /**
     * @param array $_POST data
     * @param client_request is this request from the customer area
     * @return integer Insert ID
     * Add new client to database
     */
    public function add_field($data, $client_or_lead_convert_request = false)
    {
        
     

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

       // $data                = do_action('before_client_added', $data);
        //$this->db->insert('tblclients', $data);
        $userid = $this->db->insert_id();
        if ($userid) {
            if (isset($custom_fields)) {
                $_custom_fields = $custom_fields;
                // Possible request from the register area with 2 types of custom fields for contact and for comapny/customer
                if (count($custom_fields) == 2) {
                    unset($custom_fields);
                    $custom_fields['customers']                = $_custom_fields['customers'];
                    $contact_data['custom_fields']['contacts'] = $_custom_fields['contacts'];
                } elseif (count($custom_fields) == 1) {
                    if (isset($_custom_fields['contacts'])) {
                        $contact_data['custom_fields']['contacts'] = $_custom_fields['contacts'];
                        unset($custom_fields);
                    }
                }
                handle_custom_fields_post_one($userid, $custom_fields);
            }
          
           
            $_new_client_log = $data['company'];
          
           
        }

        return $userid;
    }

    /**
     * @param  array $_POST data
     * @param  integer ID
     * @return boolean
     * Update client informations
     */
    public function update_field($data, $id, $client_request = false)
    {
        $affectedRows = 0;
        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            
            if (handle_custom_fields_post_one($id, $custom_fields)) {
                $affectedRows++;
            }
            unset($data['custom_fields']);
        }

        

        $_data = array(
            'userid' => $id,
            'data' => $data,
        );

        $data  = $_data['data'];
       // $this->db->where('userid', $id);
       // $this->db->update('tblclients', $data);

        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
        }

        if ($affectedRows > 0) {
           
            return true;
        }

        return false;
    }

     public function orderby($table,$feild,$id,$order){
            
        $query=$this->db->query("SELECT * from $table where $feild='$id' order by id $order" );  
        //echo $this->db->last_query(); 
        return $query->result_array();

    }

    public function getCustomFieldRec($rel_id,$to)
    {
        $arr_val = array();
        $s = $this->db->query("select * from tblcustomfieldsvalues where relid = '$rel_id' and section_name = '$to'")->result_array();
        foreach ($s as $key => $value) {
        $f_id = $s['fieldid'];
        $val = $this->db->query("select * from tblcustomfields where id = '$f_id'")->row_array();
        $arr_val = array($val['name']=>$value['value']);
        }
        return $arr_val;
    }

        public function do_upload_option( $file, $path )
    {
        $files['tmp'] = $file;
        //print_r($files['tmp']);die;
        $config = $this->get_upload_options1($path);
        //$type = explode('.', $files['tmp']['name']);
        $_FILES['tmp']['name'] = $files['tmp']['name'];
        $_FILES['tmp']['type']= $files['tmp']['type'];
        $_FILES['tmp']['tmp_name']= $files['tmp']['tmp_name'];
        $_FILES['tmp']['error']= $files['tmp']['error'];
        $_FILES['tmp']['size']= $files['tmp']['size'];    
        $this->upload->do_upload('tmp');
        $data = $this->upload->data();
        return $data['file_name'];
    }    

    public function get_upload_options1($path)
    {
        //upload an image options
        $config = array(
           'upload_path' => './'.$path,
           'allowed_types' => "doc|docx|pdf|jpeg|png|jpg|txt|xls|xlsx|csv",
           'max_size' => '2048',
           'overwrite' => FALSE,
           );
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        return $config;
    }

    public function selectRecord($table, $con, $field)
    {
        $query=$this->db->query("SELECT * from $table where $field='$con' " );  
        //echo $this->db->last_query(); die();
        return $query->result_array();
    }

    public function updateFields($table, $con, $field, $data)
   {
        $query = $this->db->where($con, $field)->update($table, $data);
        //echo $this->db->last_query(); die;
        if($query > 0)
        {
            return true;
        } else {
            return false;
        }
   }

   public function deleteFields($table, $con, $fld)
   {
        $config = array($con => $fld);
        $query = $this->db->where($config)->delete($table);
        //echo $this->db->last_query(); die;
        if($query)
        {
            return true;
        } else {
            return false;
        }
   }

   public function UpdateStatus($table, $con, $field, $status, $statusValue)
   {
        $this->db->set($status, $statusValue);

        $config = array($con => $field);
        $query = $this->db->where($config)->update($table);
        //echo $this->db->last_query(); die;
        if($query > 0)
        {
            return true;
        } else {
            return false;
        }
   }

   public function insertCSV($table,$data){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
   }

   public function referedPersonFind($id){
    $query=$this->db->query("select crm_first_name from client where id='".$id."'")->row_array();
   // $query1=$this->db->query("select crm_first_name from user where id='".$query['user_id']."'")->row_array();
    echo $query['crm_first_name'];
   }
   public function ClientDetailsByLegalForm($legal_form,$last_Year_start,$last_month_end)
   {    
       $this->load->helper('comman');

       $Assigned_Client = Get_Assigned_Datas('CLIENT'); 
       $assigned_client = implode(',',$Assigned_Client);

       $results = $this->db->query('SELECT count(*) as limit_count from client WHERE autosave_status=0 and firm_id = "'.$_SESSION['firm_id'].'" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'" and crm_legal_form = "'.$legal_form.'" AND FIND_IN_SET(id,"'.$assigned_client.'") ORDER BY id desc ')->result_array();      

       return $results[0];    
   }

    public function ClientDetailsByMonth($last_Year_start,$last_month_end,$status_condition = false)
    {   
        $this->load->helper('comman');

        $Assigned_Client = Get_Assigned_Datas('CLIENT'); 
        $assigned_client = implode(',',$Assigned_Client);

        if(!empty($status_condition))
        {
            $status = $status_condition;
        }
        else
        {
            $status = "";
        }

        $results=$this->db->query('SELECT count(*) as client_count FROM user LEFT JOIN client ON user.id = client.user_id where user.user_type = "FC" AND user.autosave_status!="1" and user.crm_name!="" and user.firm_id = "'.$_SESSION['firm_id'].'" '.$status.' AND FIND_IN_SET(client.id,"'.$assigned_client.'") and from_unixtime(user.CreatedTime) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'" ORDER BY user.id desc')->row_array();           
          
        return $results;    
    }

   public function get_field_value($tablename,$fieldname,$conditionfield,$conditionvalue)
   {
    if($conditionvalue!=''){
         $query=$this->db->query("SELECT * from $tablename where $conditionfield=$conditionvalue" );  
            //echo $this->db->last_query(); 
            $res=$query->row_array();
            return $res[$fieldname];

       }
       else
       {
        return '';
       }
    }
    /** 22-10-2018 **/
   public function get_field_value_mul($tablename,$fieldname,$conditionfield,$conditionvalue,$conditionfield1,$conditionvalue1)
   {
    if($conditionvalue!=''){
         $query=$this->db->query("SELECT * from $tablename where $conditionfield=$conditionvalue and $conditionfield1=$conditionvalue1 " );  
            //echo $this->db->last_query(); 
            $res=$query->row_array();
            return $res[$fieldname];

       }
       else
       {
        return '';
       }
    }
    /** 22-10-2018 **/
    /** 29-08-2018 **/
    public function geturl_image_or_not($extension)
    {
         $allowed =  array('gif','png','jpg','jpeg','webp','svg');
                 if(in_array(strtolower($extension),$allowed))
                  {
                     return "image";
                  }
                  else
                  {
                    return "another";
                  }

    }




    public function get_crm_name($id){
        $result=$this->db->query("select crm_name from user where id='".$id."'")->row_array();

        return $result['crm_name'];
    }


      public function get_crm_email($id){
        $result=$this->db->query("select crm_email_id from user where id='".$id."'")->row_array();

        return $result['crm_email_id'];
    }
    /** end of 29-08-2018 **/

    /** 14-09-2018 **/
     public function get_client_contact_primary_email($id){
        $result=$this->db->query("select main_email from client_contacts where client_id='".$id."' and make_primary=1 ")->row_array();

        return $result['main_email'];
    }
    /** en dof 14-09-2018 **/

    public function get_order_details($id)
    {
        if(empty($_SESSION['firm_id']))
        {
           $firm_id = $this->get_price('proposals','id',$this->uri->segment(3),'firm_id');
        }
        else
        {
           $firm_id = $_SESSION['firm_id']; 
        }

        $field_name = "order_no_firm".$firm_id;

        $query=$this->db->query("select ".$field_name." as order_no from firm_default_fields where id='".$id."'")->row_array();
        return $query['order_no'];
    }

    public function get_name_details($id)
    {
        return '';

        /*for new update
        if(empty($_SESSION['firm_id']))
        {
           $firm_id = $this->get_price('proposals','id',$this->uri->segment(3),'firm_id');
        }
        else
        {
           $firm_id = $_SESSION['firm_id']; 
        }

        $field_name = "name_firm".$firm_id;
        $query=$this->db->query("select ".$field_name." as field_name from firm_default_fields where id='".$id."'")->row_array();
        return $query['field_name'];*/
    }

    public function get_delete_details($id){

        $field_name = "is_deleted_firm".$_SESSION['firm_id'];

        $query=$this->db->query("select ".$field_name." as delete_field from firm_default_fields where id='".$id."'")->row_array();
        if($query['delete_field']=='1'){
            $content="style=display:none;";
        }else{
            $content='';
        }
        return $content;
    }

    public function Get_Client_fields_ManagementSettings( $firm_id )
    {
        $result = $this->FirmFields_model->Customised_Fields( $firm_id );
        $ex_where = '';
        if( !empty( $result['customised_ids'] ) )
        {
            $ex_where = " AND id NOT IN(".$result['customised_ids'].")";
        } 

        $datas = $this->db->query("SELECT * FROM client_fields_management WHERE field_type=0 AND firm_id IN (0,".$firm_id.") ".$ex_where)->result_array();
        $return = [];
        foreach ($datas as $key => $value)
        {
            $return['Setting_Label'][$value['field_propriety']]     =   $value['label'];
            $return['Setting_Order'][$value['field_propriety']]     =   $value['cnt_order'];
            $return['Setting_Delete'][$value['field_propriety']]    =   ($value['cnt_visibility']!=1?' style="display:none" ':"");             

        }
        return $return;
    }

    public function getUserID($proposal_id)
    {
        $company_name = $this->db->query('SELECT company_name FROM proposals WHERE id = "'.$proposal_id.'"')->row_array();
        $user_id = $this->db->query('SELECT user_id FROM client WHERE crm_company_name = "'.$company_name['company_name'].'"')->row_array();

        return $user_id['user_id'];
    }



    public function firm_options($custom_id){
        $query=$this->db->query("select * from tblcustomfields where id='".$custom_id."'")->row_array();
        return $query;
    }

    /** 20-10-2018 **/
    public function mulwhr_update( $table, $data, $field, $value, $field1, $value1)
    {

        $update = $this->db->where($field, $value)->where($field1, $value1)->update($table ,$data);
        //echo $this->db->last_query();die;
        
        if ($this->db->affected_rows() > 0) {
            return 1;
        }
        else {
            return 0;
        }
    }
    /** end of 20-10-2018 **/

    public function empty_field_find($user_id)
    {

        $notification_array=array();

      //  echo "select * from client where 'user_id'='".$user_id."'";
        $query1=$this->db->query("select * from client where user_id='".$user_id."'")->row_array();

        $client_service = array_fill_keys( ['conf_statement','accounts','company_tax_return','personal_tax_return','payroll','workplace','vat','cis','cissub','p11d','bookkeep','management','investgate','registered','taxadvice','taxinvest'],["tab"=>0] );
        $ck_client_service = array_intersect_key( $query1 , $client_service );

        foreach($ck_client_service as $key => $value)
        {
            if( gettype($value)=="string" &&  !empty( json_decode( $value  , true ) ) )
              {
                 $client_service[ $key ] = array_replace( $client_service[ $key ], json_decode( $value  , true ) );
              }
        }

        // echo $query1;
        // echo '<pre>';print_r($query1);

        $client_contacts=$this->db->query("select * from client_contacts where client_id='".$user_id."' and make_primary=1 ")->row_array();

        if(empty($client_contacts['main_email']))
        {
           array_push($notification_array,'Primary Contact Person  Mail Id');   
        }

        //  if($query1['crm_name']==''){
        //    array_push($notification_array,'Name');   
        // }

        if(empty($client_contacts['mobile']))
        {
           array_push($notification_array,'Primary Contact Person Mobile Number');   
        }

        if($query1['crm_incorporation_date']==''){
            array_push($notification_array,'Incorporation Date');
        }

        if($query1['crm_registered_in']==''){
            array_push($notification_array,'Registered in');
        }


        if($query1['crm_company_sic']==''){
            array_push($notification_array,'Company Sic');
        }

        //inside unused content #detailss 
        /*if($query1['crm_company_utr']==''){
            array_push($notification_array,'Company Utr Number');
        }*/


        /*if($query1['crm_ni_number']==''){
            array_push($notification_array,'National Insurance Number');
        }*/

        //inside accounds
        if( ( $client_service['accounts']['tab']==='on' || $client_service['conf_statement']['tab']==='on' ) && $query1['crm_companies_house_authorisation_code']=='')
        {
            array_push($notification_array,'Company Authorisation Code');
        }

       

        if($client_service['conf_statement']['tab']==='on')
        {
            if($query1['crm_confirmation_statement_date']=='')
            {
                array_push($notification_array,'Confirmation statement Next Statement date');
            }
            if($query1['crm_confirmation_statement_due_date']=='')
            {
                array_push($notification_array,'Confirmation statement Due date');
            }
        }

        
        if($client_service['accounts']['tab']==='on')
        {
            if($query1['crm_hmrc_yearend']=='')
            {
                array_push($notification_array,'Accounts Due date');
            }            
        }

       
        if($client_service['company_tax_return']['tab']==='on')
        {
            if($query1['crm_accounts_due_date_hmrc']=='')
            {
                array_push($notification_array,'Tax Payment date to HMRC');
            }           
        }

        if($client_service['personal_tax_return']['tab']==='on') 
        {
            if($query1['crm_personal_tax_return_date']=='')
            {
                array_push($notification_array,'Due Date on paper Return');
            }            
        }
        if($client_service['cis']['tab']==='on')
        {
            if($query1['crm_cis_contractor_start_date']=='')
            {
                array_push($notification_array,'CIS Start Date');
            }           
        }

        if($client_service['cissub']['tab']==='on') 
        {
            if($query1['crm_cis_subcontractor_start_date']=='')
            {
                array_push($notification_array,'CIS Sub Contractor Start Date');
            }            
        }


        
        if($client_service['payroll']['tab']==='on')
        {
            //pre crm_rti_deadline
            if($query1['crm_payroll_run_date']=='')
            {
                array_push($notification_array,'Pay Roll/ RTI Due/Deadline Date');
            }            
        }


        if($client_service['workplace']['tab']==='on') 
        {
            if($query1['crm_pension_subm_due_date']=='')
            {
                array_push($notification_array,'Pension Submission Due date');
            }          
        }

        if($client_service['vat']['tab']==='on') 
        {
            //pre crm_vat_due_date
            if($query1['crm_vat_quarters']=='')
            {
                array_push($notification_array,'Vat Due date');
            }            
        }


        
        if($client_service['p11d']['tab']==='on')
        {
            if($query1['crm_next_p11d_return_due']=='')
            {
                array_push($notification_array,'P11D Due Date');
            }           
        }

        

        if($client_service['bookkeep']['tab']==='on')
        {
            if($query1['crm_next_booking_date']=='')
            {
                array_push($notification_array,'Next Booking date');
            }           
        }

        

        if($client_service['management']['tab']==='on')
        {
            if($query1['crm_next_manage_acc_date']=='')
            {
                array_push($notification_array,'Next Management Account Due date');
            }           
        }

        
        if($client_service['investgate']['tab']==='on')
        {
            if($query1['crm_insurance_renew_date']==''){
                array_push($notification_array,'Insurance Renew date');
            }           
        }


        
        if($client_service['registered']['tab']==='on') 
        {
            if($query1['crm_registered_renew_date' ]=='')
            {
                array_push($notification_array,'Registered Renew date');
            }           
        }

       

        if($client_service['taxadvice']['tab']==='on') 
        {
            if($query1['crm_investigation_end_date']=='')
            {
                array_push($notification_array,'Investigation End date');
            }           
        }      
        return $notification_array;
    }



  public function header_notification()
  {
        $result=array();    
        $Company_types = $this->Company_types();

        $query = $this->db->query('select * from client where firm_id='.$_SESSION['firm_id'])->result_array();
        $up    = array('id','crm_company_name','crm_company_number','crm_incorporation_date','crm_register_address','crm_company_type','accounts_next_made_up_to','accounts_next_due','confirmation_next_made_up_to','confirmation_next_due');

        foreach($query as $data)
        {   
            $sample = $fields = [];

            array_push($sample, '');
            array_push($sample, $data['crm_company_name']);
            array_push($sample, $data['crm_company_number']);
            array_push($sample, $data['crm_incorporation_date']);
            array_push($sample, $data['crm_register_address']);
            $conf_statement = json_decode( $data['conf_statement'] , true );
            $accounts       = json_decode( $data['accounts'] , true );
           

            array_push($sample,$data['crm_company_type']);
            
            
                array_push($sample, (!empty($accounts['tab']) ? $data['crm_hmrc_yearend'] : '') );

                array_push($sample,  (!empty($accounts['tab']) && !empty($data['crm_ch_accounts_next_due'])?  date('Y-m-d',strtotime($data['crm_ch_accounts_next_due'])) : ''));
            

            
                array_push($sample,  (!empty($conf_statement['tab'])?   date('Y-m-d',strtotime($data['crm_confirmation_statement_date'])) : ''));

                array_push($sample, (!empty($conf_statement['tab']) && !empty($data['crm_confirmation_statement_due_date']) ?  date('Y-m-d',strtotime($data['crm_confirmation_statement_due_date'])) : ''));   
            

            $c=array_combine($up, $sample);

            
            if( !class_exists('CompanyHouse_Model') )
            {
                $this->load->model('CompanyHouse_Model');    
            }
            $source = $this->CompanyHouse_Model->Get_Company_Details( $data['crm_company_number'] );

            if( !isset($source['error']) )
            {
                $company_details = $source['response'];

                array_push($fields,$data['id']);

                if(isset($company_details['company_name']) && $company_details['company_name']!=''){ $company_name=$company_details['company_name']; }else{ $company_name='';  }
                array_push($fields,$company_name);

                if(isset($company_details['company_number']) && $company_details['company_number']!=''){ $company_number=$company_details['company_number']; }else{ $company_number='';  }

                array_push($fields,$company_number);  
              
                if(isset($company_details['date_of_creation']) && $company_details['date_of_creation']!=''){ $date_of_creation=$company_details['date_of_creation']; }else{ $date_of_creation='';  }

                array_push($fields,date('d-m-Y',strtotime($date_of_creation)));

                if(isset($company_details['registered_office_address']['address_line_1']) && $company_details['registered_office_address']['address_line_1']!=''){ $address_line_1=$company_details['registered_office_address']['address_line_1']; }else{ $address_line_1='';  }

                if(isset($company_details['registered_office_address']['address_line_2']) && $company_details['registered_office_address']['address_line_2']!=''){ $address_line_2=$company_details['registered_office_address']['address_line_2']; }else{ $address_line_2='';  }

                if(isset($company_details['registered_office_address']['postal_code']) && $company_details['registered_office_address']['postal_code']!=''){ $postal_code=$company_details['registered_office_address']['postal_code']; }else{ $postal_code='';  }

                if(isset($company_details['registered_office_address']['locality']) && $company_details['registered_office_address']['locality']!=''){ $locality=$company_details['registered_office_address']['locality']; }else{ $locality='';  }   

                $next_made_up_to= (!empty($accounts['tab'])  && !empty($company_details['accounts']['next_made_up_to'])? $company_details['accounts']['next_made_up_to']:''); 

                $next_due = (!empty($accounts['tab'])  && !empty($company_details['accounts']['next_due'])? $company_details['accounts']['next_due']:'');

                $confirmation_next_made_up_to= (!empty($conf_statement['tab']) && !empty($company_details['confirmation_statement']['next_made_up_to'])? $company_details['confirmation_statement']['next_made_up_to']:'');

                $confirmation_next_due = (!empty($conf_statement['tab']) && !empty($company_details['confirmation_statement']['next_due']) ? $company_details['confirmation_statement']['next_due']:'');                  

                $address = $address_line_1.$address_line_2.$locality.$postal_code;
              
                array_push($fields,$address);  
              
              if(isset($company_details['type']) && $company_details['type']!='')
                { 
                    $type=$Company_types[ $company_details['type'] ]; 
                }
                else
                {
                    $type=''; 
                }

                array_push($fields,$type); 
                array_push($fields,$next_made_up_to);
                array_push($fields,$next_due);
                array_push($fields,$confirmation_next_made_up_to);
                array_push($fields,$confirmation_next_due);

                $c1        = array_combine($up, $fields);
                $pre_array = array_diff($c1,$c);                 

                if( count( $pre_array ) > 1)
                {
                   array_push($result,$pre_array); 
                }
            } 
        }        
        return $result;
    }

      public function object_2_array($result)
{
    $array = array();
    if(!empty($result)){
    foreach ($result as $key=>$value)
    {

       # if $value is an array then
        if (is_array($value))
        {
            #you are feeding an array to object_2_array function it could potentially be a perpetual loop.
            $array[$key]=$this->object_2_array($value);
        }

       # if $value is not an array then (it also includes objects)
        else
        {
       # if $value is an object then
        if (is_object($value))
        {
            $array[$key]=$this->object_2_array($value);
        } else {

            $array[$key]=$value;
}
        }
    }
  }


    return $array;
}

public function getcompany_name($id){
   // echo $id;

  $query= $this->db->query("select * from client where id=".$id."")->row_array();
  return $query['crm_company_name'];
}

public function getcompanyname($id){

    //echo "select * from client where user_id=".$id."";
  $query= $this->db->query("select crm_company_name from client where user_id=".$id."")->row_array();
  return $query['crm_company_name'];
}



public function get_manager_notification($id){
   // echo "select * from Manager_notification where FIND_IN_SET(".$id.", manager_id)";

    $result=$this->db->query("select * from Manager_notification where FIND_IN_SET(".$id.", manager_id) and status='1' and overall_status=0 and views=0")->result_array();
    return $result;

}

public function get_price($table,$field,$value,$retur){
   // echo "select ".$retur." from ".$table." where ".$field."='".$value."'";
    $query=$this->db->query("select ".$retur." from ".$table." where ".$field."='".$value."'")->row_array();
    return $query[$retur];
}

public function get_price2($table,$field,$value,$field1,$value1,$retur){
   // echo "select ".$retur." from ".$table." where ".$field."='".$value."'";
    $query=$this->db->query("select ".$retur." from ".$table." where ".$field."='".$value."'")->row_array();
    return $query[$retur];
}

public function get_checked_value($role_id,$module,$option){
    if($option=='view'){
        $pos=0;
    }else if($option=='create'){
        $pos=1;
    }else if($option=='edit'){
        $pos=2;
    }else{
        $pos=3;
    }
    //echo "select ".$module." from role_permission1 where role=".$role."";
    
    $query=$this->db->query("select ".$module." from role_permission1 where firm_id='".$_SESSION['firm_id']."' and role_id=".$role_id."")->row_array();



    $value=explode(',',$query[$module]);

   // echo count($value);

    if(count($value) > 1){

         return $value[$pos];
    }else{
         return 0;
    }

   
}

public function section_menu( $menu , $user_id )
{
    $query = $this->db->query("select ".$menu." from section_settings where user_id='".$user_id."'")->row_array();
    return $query[$menu];
}  


public function get_column($table,$field,$value,$retur){
   // echo "select ".$retur." from ".$table." where ".$field."='".$value."'";
    $query=$this->db->query("select ".$retur." from ".$table." where ".$field."='".$value."'")->row_array();
    if($query[$retur]=='1'){
        $value='';
    }else{
        $value='style="display:none"';
    }
    return $value;
}


   public function user_team_dept(){
        $team_num=array();
           $for_cus_team=$this->db->query("select * from team_assign_staff where FIND_IN_SET('".$_SESSION['id']."',staff_id)")->result_array();
        foreach ($for_cus_team as $cu_team_key => $cu_team_value) 
        {
            array_push($team_num,$cu_team_value['team_id']);
        }
        if(!empty($team_num) && count($team_num)>0)
        {
            $res_team_num=implode('|', $team_num);
            $res_team_num1=implode(',', $team_num);
        }
        else
        {
            $res_team_num='0';
            $res_team_num1='0';
        }

        $department_num=array();
        $for_cus_department=$this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
        foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
            array_push($department_num,$cu_dept_value['depart_id']);
        }
        if(!empty($department_num) && count($department_num)>0)
        {
            $res_dept_num=implode('|', $department_num);
            $res_dept_num1=implode(',', $department_num);
        }
        else
        {
            $res_dept_num='0';
            $res_dept_num1='0';
        }

     
        if(count($team_num) !='0')
        {
             $teams=implode(',',$team_num);
             $sql="or find_in_set('".$teams."',team )";
        }
        else
        {
             $teams=0;
             $sql='';
        }

        if(count($team_num) !='0')
        {
             $depts=implode(',',$department_num);
             $sql1="or  find_in_set('".$depts."' , department )";
        }
        else
        {
            $depts=0;
            $sql1='';
        }



       //  echo "select subject,task_status from add_new_task where  task_status!='complete' and ( find_in_set ( '".$_SESSION['id']."',worker ) ".$sql." ".$sql1." or find_in_set ( '".$_SESSION['id']."',manager ) )  ORDER BY `id` DESC Limit 0, 5";


        $query=$this->db->query("select subject,task_status,id from add_new_task where  task_status!='5' and (find_in_set ( '".$_SESSION['id']."',worker ) ".$sql." ".$sql1." or  find_in_set ( '".$_SESSION['id']."',manager ) ) and views='0' ORDER BY `id` DESC Limit 0, 5")->result_array();

        return $query;
    } 


    public function archive_check($id,$type)
    {
        $query=$this->db->query("select * from notification_archive where type='".$type."' and notification_id='".$id."'")->row_array();

        if(isset($query['type']) && $query['type']!='')
        {
            $value = '0';
        }
        else if(count($query) == 0)
        {
            $value = '2';
        }        
        else
        {
            $value = '1';
        }
 
        return $value;
    } 

       public function archive_check_details($id,$type,$details)
       {
            $query=$this->db->query("select * from notification_archive where type='".$type."' and notification_id='".$id."' and details='".$details."'")->row_array();
            if($query['type']!='')
            {
                $value='0';
            }
            else
            {
                $value='1';
            }
            return $value;
        }     

    public function archive_check1($id,$type){
        $query=$this->db->query("select * from notification_archive where type='".$type."' and notification_id='".$id."'")->row_array();

        echo "select * from notification_archive where type='".$type."' and notification_id='".$id."'";
        if($query['type']!=''){
            $value='0';
        }else{
            $value='1';
        }
        return $value;
    }   

// public function get_working_hours($task_id,$user_id){ 

    
// $t_re='';
// $task_name='';

// //echo "select time_start_pause from individual_task_timer where  task_id=".$task_id." and user_id in (".$user_id.") ";
//  $today=$this->db->query("select time_start_pause from individual_task_timer where  task_id=".$task_id." and user_id in (".$user_id.") " )->row_array();

// if(!empty($today['time_start_pause'])){
// $res=explode(',',$today['time_start_pause']);
//  //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
//     $res1=array_chunk($res,2);   
//     $result_value=array();
//     foreach($res1 as $rre_key => $rre_value)
//     {      
//        $abc=$rre_value; 
//        if(count($abc)>1){
//        if($abc[1]!='')
//        {       
//           $ret_val=$this->calculate_test($abc[0],$abc[1]);
//           array_push($result_value, $ret_val) ;
//        }
//        else
//        {       
//         $pause='';
//           $ret_val=$this->calculate_test($abc[0],time());
//            array_push($result_value, $ret_val) ;
//        }
//       }
//       else
//       {      

//         $pause='';
//           $ret_val=$this->calculate_test($abc[0],time());
//            array_push($result_value, $ret_val) ;
//       }

//     }
//     $time_tot=0;
//      foreach ($result_value as $re_key => $re_value) {
//         $time_tot+=$this->time_to_sec($re_value) ;
//      }
//      $hr_min_sec=$this->sec_to_time($time_tot);
//      $hr_explode=explode(':',$hr_min_sec);
//      $h=$hr_explode[0];
//      $m="0.".$hr_explode[1];
//      $hours=$h+$m;
//     // $t="['".$today['crm_name']."',".$hours.",'Stffs Name:".$today['crm_name'].",Working Hours:".$hours."']";
//      $t="".$hours."";
//      $t_re.=empty($t_re)?$t:",".$t;
// //}
// }else{
//     $t_re='Not Started Task';
// }
// return $t_re;

// }


public function get_working_hours($task_id,$user_id){    
    //$user_id="111";
    $today_record=$this->db->query("select time_start_pause from individual_task_timer where  task_id in (".$task_id.") and user_id in (".$user_id.") " )->result_array();

    $check=array();
    foreach($today_record as $today1){
           array_push($check,$today1);
     }

     // print_r(count($check));
     // exit;
    if(count($check) > 1){
        
        $sums=array();
        $i=0;
         foreach($check as $time){       
            array_push($sums,$this->timers($time['time_start_pause']));     
         }   
         $sum=array_sum($sums);
    }else{
        if(count($check) > 0){
            foreach($check as $time){          
                $sum=$this->timers($time['time_start_pause']);
            }
        }else{
            $sum='Not Started Task';
        }
    }
    return $sum;
}
function timers($time){
        $t_re='';
        $task_name='';
        if(!empty($time)){
        $res=explode(',',$time);
        //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
        $res1=array_chunk($res,2);   
        $result_value=array();
        foreach($res1 as $rre_key => $rre_value)
        {      
            $abc=$rre_value; 
            if(count($abc)>1){
                if($abc[1]!='')
                {       
                    $ret_val=$this->calculate_test($abc[0],$abc[1]);
                    array_push($result_value, $ret_val) ;
                }
                else
                {       
                    $pause='';
                    $ret_val=$this->calculate_test($abc[0],time());
                    array_push($result_value, $ret_val) ;
                }
            }
            else
            { 
                $pause='';
                $ret_val=$this->calculate_test($abc[0],time());
                array_push($result_value, $ret_val) ;
            }

        }
        $time_tot=0;
        foreach ($result_value as $re_key => $re_value) {
             $time_tot+=$this->time_to_sec($re_value) ;
        }
            $hr_min_sec=$this->sec_to_time($time_tot);
            $hr_explode=explode(':',$hr_min_sec);
            $h=$hr_explode[0];
            $m="0.".$hr_explode[1];
            $hours=$h+$m;
            $t="".$hours."";
            $t_re.=empty($t_re)?$t:",".$t; 
        }else{
            $t_re='Not Started Task';
        }
        return $t_re;
}

function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
   }

   function calculate_test($a,$b){
$difference = $b-$a;

$second = 1;
$minute = 60*$second;
$hour   = 60*$minute;

$ans["hour"]   = floor(($difference)/$hour);
$ans["minute"] = floor((($difference)%$hour)/$minute);
$ans["second"] = floor(((($difference)%$hour)%$minute)/$second);
//echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

$test=$ans["hour"].":".$ans["minute"].":".$ans["second"];
return $test;
}
function time_to_sec($time) {
list($h, $m, $s) = explode (":", $time);
$seconds = 0;
$seconds += (intval($h) * 3600);
$seconds += (intval($m) * 60);
$seconds += (intval($s));
return $seconds;
}
function sec_to_time($sec) {
return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}  

  public function director_details(){

            $user_id=array();
            $managers=array();
            $array_client_id=array();

          array_push($user_id,$_SESSION['id']);
            $director_document=$this->Common_mdl->GetAllWithWhere('document_record','create_by',$_SESSION['id']);

            $manager_details=$this->db->query("select * from user where  firm_admin_id =".$_SESSION['id']." and role='5'")->result_array();

                     
           if(count($manager_details) > 0){
              
                foreach($manager_details as $manager){
                      array_push($managers,$manager['id']);
                }
                $man_doc=implode(',',$managers);

            }else{
               $man_doc=0;
            }
             //Director Assign Manager

            $dir_manager_details=$this->db->query("select * from assign_director where director='".$_SESSION['id']."'")->row_array();        

            if(count($dir_manager_details)>0){
            $manager=explode(',',$dir_manager_details['manager']);
            $director_manager=$dir_manager_details['manager'];
              for($i=0;$i<count($manager);$i++){
              array_push($user_id,$manager[$i]);
              }
            }else{
               $director_manager=0;
            }  
            //Manager Under Staff Details

            $staff_details=$this->db->query("select * from user where (firm_admin_id in (".$man_doc.") or firm_admin_id in (".$director_manager.") ) and role='6'")->result_array();  

          
            $staffs_details=array();           

            if(count($staff_details)>0){
                $staffs_details=array();
                foreach($staff_details as $staff){
                array_push($staffs_details,$staff['id']);
                }

                $stf_doc=implode(',',$staffs_details);
            }else{
                $stf_doc=0;
            }
            //Manager assign staff

             $man_staff_details=$this->db->query("select * from assign_manager where manager in (".$man_doc.") ")->row_array();

             if(count($man_staff_details) > 0){

                 array_push($user_id,$man_staff_details['staff']);

                 $manager_staff=$man_staff_details['staff'];

             }else{

                 $manager_staff=0;
             }
             //****
         
            $team_num=array();
            $for_cus_team=$this->db->query("select * from team_assign_staff where (staff_id in ('".$stf_doc."') or staff_id in ('".$manager_staff."') )")->result_array();
            foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
            array_push($team_num,$cu_team_value['team_id']);
            } 

            if(!empty($team_num) && count($team_num)>0){
               $res_team_num=implode('|', $team_num);
               $res_team_num1=implode(',', $team_num);
            }
            else
            {
               $res_team_num='0';
               $res_team_num1='0';
            }
            $department_num=array();
            $for_cus_department=$this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
            foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
            array_push($department_num,$cu_dept_value['depart_id']);
            }
            if(!empty($department_num) && count($department_num)>0){
              $res_dept_num=implode('|', $department_num);
              $res_dept_num1=implode(',', $department_num);
            }
            else
            {
                $res_dept_num='0';
                $res_dept_num1='0';
            }

            /* responsible team **/

            $responsible_team=$this->db->query("select * from responsible_team where team in (".$res_team_num1.") and team!='' " )->result_array();
            if(!empty($responsible_team)){
            foreach ($responsible_team as $retm_key => $retm_value) {
            array_push($array_client_id,$retm_value['client_id']);
            }
            }
            /** responsible department **/
            $responsible_depart=$this->db->query("select * from responsible_department where depart in (".$res_dept_num1.") and depart!='' ")->result_array();
            if(!empty($responsible_depart)){
            foreach ($responsible_depart as $redekey => $rede_value) {
            array_push($array_client_id, $rede_value['client_id']);
            }

            }
            /** responsible user **/
            $responsible_staff=$this->db->query("select * from responsible_user where (manager_reviewer in  (".$man_doc.") or  manager_reviewer in (".$director_manager.") or manager_reviewer in (".$stf_doc.") or manager_reviewer in (".$manager_staff.") ) ")->result_array();
            if(!empty($responsible_staff)){
            foreach ($responsible_staff as $rest_key => $rest_value) {
            array_push($array_client_id, $rest_value['client_id']);
            }
            }


          $responsible_management=$this->db->query("select * from responsible_user where (assign_managed in (".$man_doc.") or  assign_managed in (".$director_manager.") or assign_managed in (".$stf_doc.") or assign_managed in (".$manager_staff.")) ")->result_array();
          if(!empty($responsible_management)){
          foreach ($responsible_management as $rest_key => $rest_value) {
          array_push($array_client_id, $rest_value['client_id']);
          }
          }

          $get_login_user=$this->db->query("select * from user where firm_admin_id=".$_SESSION['id']." and role='4' AND autosave_status!='1' ")->result_array();
            if(!empty($get_login_user))
            {
             foreach ($get_login_user as $lg_key => $lg_value) {
              array_push($array_client_id, $lg_value['id']);
              }
            }
            /** 06-08-2018 **/
            if(!empty($array_client_id))
            {
            $result_var=implode(',', array_unique($array_client_id));
            }
            else
            {
            $result_var='0';
            }      

         $user_ids=implode(',',array_merge($user_id,$managers,$staffs_details,$array_client_id)); 

                        $this->db->where_in('id', $user_ids);
           $user_data = $this->db->get( "user" )->result_array();
             foreach ($user_data as $user) {
                    if( isset( $user['documents'] ) && $user['documents'] != '' ){
                        return $user['documents'];
                    } else {
                    return $this->createUserDocumentPath( $user['id'] );
                    }
                } 
    }


    public function manager_details(){
        $user_id=array();
            $staffs_details=array();
            $array_client_id=array();
            array_push($user_id,$_SESSION['id']);
            //Director created Documents
        //    $manager_document=$this->Common_mdl->GetAllWithWhere('document_record','create_by',$_SESSION['id']); 
            //Manager Under Staff Details
            $staff_details=$this->db->query("select * from user where firm_admin_id='".$_SESSION['id']."'  and role='6'")->result_array();  
            if(count($staff_details) > 0){             
              foreach($staff_details as $staff){
                  array_push($staffs_details,$staff['id']);
              } 
              $stf_doc=implode(',',$staffs_details);
            }else{
              $stf_doc=0;
            }
            //Manager assign staff
             $man_staff_details=$this->db->query("select * from assign_manager where manager = '".$_SESSION['id']."'")->row_array();
              if(count( $man_staff_details) > 0){
               $manager_staff_implode=explode(',',$man_staff_details['staff']);            
               $manager_staff=$man_staff_details['staff'];
               $m_staff=explode(',',$man_staff_details['staff']);

               for($i=0;$i<count($m_staff);$i++){
                  array_push($user_id,$m_staff[$i]);
               }  

             }else{
               $manager_staff=0;
             }
        //    $staff_document=$this->db->query("select * from document_record where (create_by in (".$stf_doc.") or create_by in (".$manager_staff.") )")->result_array(); 
            $team_num=array();
            $for_cus_team=$this->db->query("select * from team_assign_staff where (staff_id in ('".$stf_doc."') or staff_id in ('".$manager_staff."') )")->result_array();
            foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
            array_push($team_num,$cu_team_value['team_id']);
            } 

            if(!empty($team_num) && count($team_num)>0){
               $res_team_num=implode('|', $team_num);
               $res_team_num1=implode(',', $team_num);
            }
            else
            {
               $res_team_num='0';
               $res_team_num1='0';
            }
            $department_num=array();
            $for_cus_department=$this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
            foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
            array_push($department_num,$cu_dept_value['depart_id']);
            }
            if(!empty($department_num) && count($department_num)>0){
              $res_dept_num=implode('|', $department_num);
              $res_dept_num1=implode(',', $department_num);
            }
            else
            {
                $res_dept_num='0';
                $res_dept_num1='0';
            }

            /* responsible team **/

            $responsible_team=$this->db->query("select * from responsible_team where team in (".$res_team_num1.") and team!='' " )->result_array();
            if(!empty($responsible_team)){
            foreach ($responsible_team as $retm_key => $retm_value) {
            array_push($array_client_id,$retm_value['client_id']);
            }
            }
            /** responsible department **/
            $responsible_depart=$this->db->query("select * from responsible_department where depart in (".$res_dept_num1.") and depart!='' ")->result_array();
            if(!empty($responsible_depart)){
            foreach ($responsible_depart as $redekey => $rede_value) {
            array_push($array_client_id, $rede_value['client_id']);
            }

            }
            /** responsible user **/
            $responsible_staff=$this->db->query("select * from responsible_user where ( manager_reviewer in (".$stf_doc.") or manager_reviewer in (".$manager_staff.") ) ")->result_array();
            if(!empty($responsible_staff)){
            foreach ($responsible_staff as $rest_key => $rest_value) {
            array_push($array_client_id, $rest_value['client_id']);
            }
            }

          $responsible_management=$this->db->query("select * from responsible_user where ( assign_managed in (".$stf_doc.") or assign_managed in (".$manager_staff.")) ")->result_array();
          if(!empty($responsible_management)){
          foreach ($responsible_management as $rest_key => $rest_value) {
          array_push($array_client_id, $rest_value['client_id']);
          }
          }

          $get_login_user=$this->db->query("select * from user where firm_admin_id=".$_SESSION['id']." and role='4' AND autosave_status!='1' ")->result_array();
            if(!empty($get_login_user))
            {
             foreach ($get_login_user as $lg_key => $lg_value) {
              array_push($array_client_id, $lg_value['id']);
              }
            }
            /** 06-08-2018 **/
            if(!empty($array_client_id))
            {
            $result_var=implode(',', array_unique($array_client_id));
            }
            else
            {
            $result_var='0';
            }

            $user_ids=implode(',',array_merge($user_id,$staffs_details,$array_client_id));     
          
                        $this->db->where_in('id', $user_ids);
           $user_data = $this->db->get( "user" )->result_array();
             foreach ($user_data as $user) {
                    if( isset( $user['documents'] ) && $user['documents'] != '' ){
                        return $user['documents'];
                    } else {
                    return $this->createUserDocumentPath( $user['id'] );
                    }
                } 
    }


    public function staff_details(){
            $user_id=array();
            $array_client_id=array();
            array_push($user_id,$_SESSION['id']); 
 //$staff_document=$this->Common_mdl->GetAllWithWhere('document_record','create_by',$_SESSION['id']); 
            $team_num=array();
            $for_cus_team=$this->db->query("select * from team_assign_staff where FIND_IN_SET('".$_SESSION['id']."',staff_id)")->result_array();
            foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
            array_push($team_num,$cu_team_value['team_id']);
            } 

            if(!empty($team_num) && count($team_num)>0){
               $res_team_num=implode('|', $team_num);
               $res_team_num1=implode(',', $team_num);
            }
            else
            {
               $res_team_num='0';
               $res_team_num1='0';
            }
            $department_num=array();
            $for_cus_department=$this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
            foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
            array_push($department_num,$cu_dept_value['depart_id']);
            }
            if(!empty($department_num) && count($department_num)>0){
              $res_dept_num=implode('|', $department_num);
              $res_dept_num1=implode(',', $department_num);
            }
            else
            {
                $res_dept_num='0';
                $res_dept_num1='0';
            }

            /* responsible team **/

            $responsible_team=$this->db->query("select * from responsible_team where team in (".$res_team_num1.") and team!='' " )->result_array();
            if(!empty($responsible_team)){
            foreach ($responsible_team as $retm_key => $retm_value) {
            array_push($array_client_id,$retm_value['client_id']);
            }
            }
            /** responsible department **/
            $responsible_depart=$this->db->query("select * from responsible_department where depart in (".$res_dept_num1.") and depart!='' ")->result_array();
            if(!empty($responsible_depart)){
            foreach ($responsible_depart as $redekey => $rede_value) {
            array_push($array_client_id, $rede_value['client_id']);
            }

            }
            /** responsible user **/
            $responsible_staff=$this->db->query("select * from responsible_user where manager_reviewer='".$_SESSION['id']."'")->result_array();
            if(!empty($responsible_staff)){
            foreach ($responsible_staff as $rest_key => $rest_value) {
            array_push($array_client_id, $rest_value['client_id']);
            }
            }

          $responsible_management=$this->db->query("select * from responsible_user where  assign_managed='".$_SESSION['id']."'")->result_array();
          if(!empty($responsible_management)){
          foreach ($responsible_management as $rest_key => $rest_value) {
          array_push($array_client_id, $rest_value['client_id']);
          }
          }

          $get_login_user=$this->db->query("select * from user where firm_admin_id=".$_SESSION['id']." and role='4' AND autosave_status!='1' ")->result_array();
            if(!empty($get_login_user))
            {
             foreach ($get_login_user as $lg_key => $lg_value) {
              array_push($array_client_id, $lg_value['id']);
              }
            }
            /** 06-08-2018 **/
            if(!empty($array_client_id))
            {
            $result_var=implode(',', array_unique($array_client_id));
            }
            else
            {
            $result_var='0';
            }


             $user_ids=implode(',',array_merge($user_id,$array_client_id));     
          
                        $this->db->where_in('id', $user_ids);
           $user_data = $this->db->get( "user" )->result_array();
             foreach ($user_data as $user) {
                    if( isset( $user['documents'] ) && $user['documents'] != '' ){
                        return $user['documents'];
                    } else {
                    return $this->createUserDocumentPath( $user['id'] );
                    }
                } 
    }

    public function team_members_get($get_team){

         foreach (explode(',',$get_team) as $tm_key => $tm_value) {
            $get_team_data=$this->Common_mdl->GetAllWithWhere('team','id',$tm_value);
                if(count($get_team_data)>0){
                   $team_name=$get_team_data[0]['team'];
                   $get_teamstaff_data=$this->Common_mdl->GetAllWithWhere('team_assign_staff','team_id',$tm_value);
                    if(count($get_teamstaff_data)>0){
                       $staff_ids=$get_teamstaff_data[0]['staff_id'];
                      if($staff_ids!='')
                      {
                                return $staff_ids;
                      }
                   }
                }
         }
    }

     public function dept_members_get($get_department){
        foreach (explode(',', $get_department) as $de_key => $de_value) {
             $get_dept_data=$this->Common_mdl->GetAllWithWhere('department_permission','id',$de_value);
            if(count($get_dept_data)>0){
                $department_name=$get_dept_data[0]['new_dept'];
              //  echo $department_name."--dept--";
               $get_deptteam_data=$this->Common_mdl->GetAllWithWhere('department_assign_team','depart_id',$de_value);
                if(count($get_deptteam_data)>0){
                    $departmentteam_id=$get_deptteam_data[0]['team_id'];
                 //   echo $departmentteam_id;
                    foreach (explode(',',$departmentteam_id) as $tm_key => $tm_value) {
                        $get_team_data=$this->Common_mdl->GetAllWithWhere('team','id',$tm_value);
                        if(count($get_team_data)>0){
                           $team_name=$get_team_data[0]['team'];                         
                           $get_teamstaff_data=$this->Common_mdl->GetAllWithWhere('team_assign_staff','team_id',$tm_value);
                            if(count($get_teamstaff_data)>0){
                                $staff_ids=$get_teamstaff_data[0]['staff_id'];
                                if($staff_ids!='')
                                {
                                     return $staff_ids;
                                }
                            }
                        }
                     }
                }
            }
        }
    }

    public function staff_dept_find($id){   
        $query=$this->db->query("select team_id from team_assign_staff where FIND_IN_SET('".$id."',staff_id)")->row_array();
        $department=$this->db->query("select depart_id from department_assign_team where FIND_IN_SET('".$query['team_id']."',team_id)")->row_array();
        $dept_name=$this->db->query("select new_dept from department_permission where id=".$department['depart_id']."")->row_array(); 
        $team_name=$this->db->query("select team from team where id=".$query['team_id']."")->row_array();
        return $dept_name['new_dept'];
    }

    public function staff_team_find($id){ 
        $query=$this->db->query("select team_id from team_assign_staff where FIND_IN_SET('".$id."',staff_id)")->row_array();           
        $team_name=$this->db->query("select team from team where id=".$query['team_id']."")->row_array();
        return $team_name['team'];
    }

    public function activitylogfunction($id,$start_date=false,$end_date=false){

        if(!empty($start_date) && !empty($end_date))
        {
            $dur_cond = "AND from_unixtime(CreatedTime) BETWEEN '".$start_date."' AND '".$end_date."'";
        }
        else
        {
            $dur_cond = "";
        }

        $query=$this->db->query("select * from activity_log where user_id in (".$id.") ".$dur_cond." order by id desc")->result_array();
        return $query;
    }
    
    public function Send_Notification( $Module , $Type , $ReceiverUserID , $Content,$Sender=false,$Task_id=false )
    {   
        $Content = addslashes( $Content );
        $data=array('module'=>$Module, 'notification_type'=>$Type,'receiver_user_id'=>$ReceiverUserID,'status' =>1,'content'=>$Content,'sender_id'=>$Sender,'task_id'=>$Task_id,'Createdtime'=>time());
 

        $this->db->insert("notification_management",$data);

        return $this->db->insert_id();
    }
    public function ReceiveNotification( $Module , $ReceiverUserID )
    { 
        $return = $this->db->query("select * from notification_management where module='".$Module."' and receiver_user_id = ".$ReceiverUserID." and status IN(1,2)")->result_array();
      
        return $return;
    }

    public function Task_Members_Notify( $Task_id , $Notify_Type , $Notify_Content  )
    {
        $session_user=$this->db->query("select crm_email_id from user where id=".$_SESSION['id']."")->row_array();

        $mail_user=$session_user['crm_email_id'];
        $task_data=$this->db->query("select * from add_new_task where id=".$Task_id)->row_array();
        
        $Notify_Content = str_replace('{TASK_SUBJECT}', $task_data['subject'] , $Notify_Content );


         $Assigned_Task = Get_Module_Assigees('TASK',$Task_id);

         // print_r($_SESSION['userId']);
         // print_r($Assigned_Task);
         // die;

         if($Notify_Type=='Task_Review')
         {
         if (false !== $key = array_search($_SESSION['userId'], $Assigned_Task)) {
            if($key!=0)
            {
              $Assigned_Task1=$Assigned_Task[$key-1];
              $Assigned_Task=array();
              $Assigned_Task[]=$Assigned_Task1;
            }
            else
            {
                 $Assigned_Task=array();
            }
           
    //do something
            }
        }
            //  print_r($Assigned_Task);
            // die;
        //  print_r($_SESSION['userId']);
        //  echo '<br>';
        // print_r($Assigned_Task);die;
        // $Assigned_Task = implode(',', $Assigned_Task );


        // $assign=explode(',',$task_data['worker']);
        // $team=explode(',',$task_data['team']);
        // $department=explode(',',$task_data['department']);
        // $manager=explode(',',$task_data['manager']);
        // $get_assigned=implode(',',$assign);    
        // $get_team=implode(',',$team);
        // $get_department=implode(',',$department);  
        // $get_manager=implode(',',$manager);

        //  if($get_manager!='')
        //          {
        //           foreach (explode(',',$get_manager) as $key => $value) 
        //           {
        //               $get_user_data=$this->Common_mdl->GetAllWithWhere('user','id',$value);
        //                    if(count($get_user_data)>0)
        //                    {
        //                         $it_name=$get_user_data[0]['crm_name'];
        //                         $it_email=$get_user_data[0]['crm_email_id'];
        //                         if($it_email!='' && $it_email!=$mail_user)
        //                         {
        //                             $this->Send_Notification('Task', $Notify_Type , $get_user_data[0]['id'] , $Notify_Content );

        //                            $this->SendEmail_Notify( $Notify_Type , $Notify_Content ,$Task_id,$it_name,$it_email);
        //                         }
        //                    }
        //           }
               
        //          }
/** end of manager **/
if(isset($_SESSION['id']))
{
    $sender=$_SESSION['id'];
}
else
{
    $sender=0;
}
   if($Assigned_Task!='')
                 {
                      foreach ($Assigned_Task as $key => $value) 
                      {
                          $get_user_data=$this->Common_mdl->GetAllWithWhere('user','id',$value);
                          //print_r($get_user_data);
                         // echo 'work';
                               if(count($get_user_data)>0)
                               {
                                 $it_name=$get_user_data[0]['crm_name'];
                                 $it_email=$get_user_data[0]['crm_email_id'];
                              
                                    $this->Send_Notification('Task', $Notify_Type , $get_user_data[0]['id'] , $Notify_Content,$sender ,$Task_id);
                                if($it_email!='')
                                {   

                                    $this->SendEmail_Notify( $Notify_Type , $Notify_Content ,$Task_id,$it_name,$it_email);
                                }
                               }
                      }
               
                 }
                 //  if($get_team!='')
                 // {
                 //  foreach (explode(',',$get_team) as $tm_key => $tm_value) {

                 //     $get_team_data=$this->Common_mdl->GetAllWithWhere('team','id',$tm_value);
                 //     if(count($get_team_data)>0){
                 //       $team_name=$get_team_data[0]['team'];
                 //       $get_teamstaff_data=$this->Common_mdl->GetAllWithWhere('team_assign_staff','team_id',$tm_value);
                 //       if(count($get_teamstaff_data)>0){
                 //           $staff_ids=$get_teamstaff_data[0]['staff_id'];
                 //          if($staff_ids!='')
                 //          {
                 //              foreach (explode(',',$staff_ids) as $key => $value) {
                 //                      $get_user_data=$this->Common_mdl->GetAllWithWhere('user','id',$value);
                 //     if(count($get_user_data)>0){
                 //       $it_name=$get_user_data[0]['crm_name'];
                 //     $it_email=$get_user_data[0]['crm_email_id'];
                 //      if($it_email!='' && $it_email!=$mail_user){
                 //        //  echo $it_email;
                 //                $this->Send_Notification('Task', $Notify_Type , $get_user_data[0]['id'] , $Notify_Content );

                 //                   $this->SendEmail_Notify( $Notify_Type , $Notify_Content ,$Task_id,$it_name,$it_email);
                 //      }
                 //     }
                 //              }
                 //          }
                 //       }
                 //   }
                 //  }
                
                 // }

                 // if($get_department!='')
                 // {
                 //   foreach (explode(',', $get_department) as $de_key => $de_value) {
                 //        $get_dept_data=$this->Common_mdl->GetAllWithWhere('department_permission','id',$de_value);
                 //   if(count($get_dept_data)>0){
                 //        $department_name=$get_dept_data[0]['new_dept'];
                 //      //  echo $department_name."--dept--";
                 //       $get_deptteam_data=$this->Common_mdl->GetAllWithWhere('department_assign_team','depart_id',$de_value);
                 //   if(count($get_deptteam_data)>0){
                 //        $departmentteam_id=$get_deptteam_data[0]['team_id'];
                 //     //   echo $departmentteam_id;
                 //        foreach (explode(',',$departmentteam_id) as $tm_key => $tm_value) {
                 //           $get_team_data=$this->Common_mdl->GetAllWithWhere('team','id',$tm_value);
                 //   if(count($get_team_data)>0){
                 //           $team_name=$get_team_data[0]['team'];
                         
                 //           $get_teamstaff_data=$this->Common_mdl->GetAllWithWhere('team_assign_staff','team_id',$tm_value);
                 //           if(count($get_teamstaff_data)>0){
                 //          $staff_ids=$get_teamstaff_data[0]['staff_id'];
                 //          if($staff_ids!='')
                 //          {
                 //              foreach (explode(',',$staff_ids) as $key => $value) {
                 //                $get_user_data=$this->Common_mdl->GetAllWithWhere('user','id',$value);
                 //     if(count($get_user_data)>0){
                 //       $it_name=$get_user_data[0]['crm_name'];
                 //     $it_email=$get_user_data[0]['crm_email_id'];
                 //      if($it_email!='' && $it_email!=$mail_user){                        
                 //            $this->Send_Notification('Task', $Notify_Type , $get_user_data[0]['id'] , $Notify_Content );

                 //                   $this->SendEmail_Notify( $Notify_Type , $Notify_Content ,$Task_id,$it_name,$it_email);
                 //      }
                 //     }
                 //              }
                 //          }
                 //        }
                 //        }
                 //     }
                 //    }
                 //    }
                 //    }
                 // }
    }


    public function role_identify(){    

        // $role_name=$this->db->query("select role from Role where id='".$_SESSION['roleId']."' and firm_id in ('".$_SESSION['firm_admin_id']."','".$_SESSION['id']."') ")->row_array();  
        
        $role_name=$this->db->query("select role from roles_section where id='".$_SESSION['roleId']."' and firm_id in ('".$_SESSION['firm_id']."','".$_SESSION['id']."') ")->row_array();  

//echo "select * from role_permission1 where role_id='".$_SESSION['roleId']."' and firm_id in ('".$_SESSION['firm_admin_id']."','".$_SESSION['id']."') ";
         //exit;
       // echo $role_name['role'];
      //  $query=$this->db->query("select * from role_permission1 where role_id='".$_SESSION['roleId']."' and firm_id in ('".$_SESSION['firm_admin_id']."','".$_SESSION['id']."') ")->row_array();
        $query=$this->db->query("select * from role_permission1 where role_id='".$_SESSION['roleId']."'")->row_array();
        $values = array();
        $default_array = array('view','create','edit','delete');
        $key_values = array_keys($query);
        for($i=4;$i<=46 ;$i++){

            $permission=explode(',',$query[$key_values[$i]]); 
         
            if(count($permission)<= 1){   

                $permission=array(0,0,0,0); 

            }else{   

                $permission=explode(',',$query[$key_values[$i]]); 
            }           

            $c = array_combine($default_array, $permission); 
            $values[$key_values[$i]] = $c;              
        }      
        $_SESSION['role_name']=$role_name['role'];
        $_SESSION['permission']=$values;   
    }


    public function permission_role(){
          $role_name=$this->db->query("select role from Role where id='".$_SESSION['roleId']."'")->row_array();
          //echo $role_name['role'];
            echo '<pre>';
            print_r($_SESSION['permission']);
      
    }


    public function permission_count($v){
      //  print_r($v[0]);
       // exit;
        $value=implode(',',$v);
        $val_array=explode(',',$value);
        $vals = array_count_values($val_array);       
        return $vals;
    }


    public function permission_check($role_id,$module){ 
        $query=$this->db->query("select ".$module." from role_permission1 where role_id=".$role_id."")->row_array();
        $val_array=explode(',',$query[$module]);    
        $vals = array_count_values($val_array); 
         if(isset($vals[1]) && $vals[1]>= 1){
             $ch='';
         }else{
            $ch='style=display:none';
         }
         return $ch;
    }   

    function SendEmail_Notify( $Subject , $Content ,$task_id,$it_name,$it_email)
    {
             //echo 'check';
            if($Subject=='Task_Review')
              {
                $content1=explode("///",$Content);
                $Subject=$content1[2];
                $Content=$content1[1];

                 $EMAIL_TEMPLATE = $this->getTemplates(17); 

              }  
               else if($Subject=='Task_Status_Change')
              {
               
                 $EMAIL_TEMPLATE = $this->getTemplates(15); 
              } 

              else if($Subject==' Task_Command_Added')
              {
               
                 $EMAIL_TEMPLATE = $this->getTemplates(13); 
              } 
              else if($Subject=='Task_Attachment_Uploaded')
              {
               
                 $EMAIL_TEMPLATE = $this->getTemplates(14); 
              } 

             

              $check_val=$this->Common_mdl->select_record('add_new_task','id',$task_id);
              $title=explode("-",$check_val['subject']);

            //  print_r($EMAIL_TEMPLATE);
              // print_r($title);

               if(!empty($EMAIL_TEMPLATE))
              {
                $EMAIL_TEMPLATE = end($EMAIL_TEMPLATE);

                     
                  
                    $data2['username'] = $it_name;
                  $email_subject  = html_entity_decode($EMAIL_TEMPLATE['subject']);
                  $data2['email_contents']  = html_entity_decode($EMAIL_TEMPLATE['body']);

               
                
                  $a1  =   array(
                                  ':: Task Name::'=>(isset($title[0]))?$title[0]:'',       
                                  ':: Task Link::'=>base_url().'/user/task_details/'.$task_id,
                                  ' :: Staff Name::'=>$Content
                                );

                  $data2['email_contents'] =  strtr($data2['email_contents'] ,$a1);
                  $data2['title']= html_entity_decode($EMAIL_TEMPLATE['title']);
                  //email template
                  $body = $this->load->view('remainder_email.php',$data2, TRUE);

                 // echo $body;
                 // exit;
                 
                       $sender_id=$this->select_record('admin_setting','firm_id', $check_val['firm_id']);
                       if(!empty($sender_id) && $sender_id['company_email']!='' )
                       {

                        $sender_id=$sender_id['company_email'];
                       }
                       else
                       {
                        $sender_id='info@remindoo.org';
                       }

                        $this->load->library('email');
                        $this->email->set_mailtype("html");
                        //$this->mailsettings();
                        $this->email->from($sender_id);
                        //$this->email->to('suganya.techleaf@gmail.com');
                        $this->email->to($it_email);
                        $this->email->subject(strip_tags($email_subject)); 
                        $this->email->message($body);
                        $send = $this->email->send();
              }

            // $data2['notes_section']  =   'Hai '.$it_name.'<br/><br/>'.$Content.'<br/><br/><a href="'.base_url().'/user/task_details/'.$task_id.'"> Click here to view task </a><br/><br/><br/>Thanks,';
            
            // $Subject = str_replace('_', " ", $Subject );

            // $body = $this->load->view('empty_mail_content.php',$data2, TRUE);          
          
            // $this->load->library('email');
            // $this->email->set_mailtype("html");
            // //$this->mailsettings();
            // $this->email->from('info@remindoo.org');
            // //$this->email->to('suganya.techleaf@gmail.com');
            // $this->email->to($it_email);
            // $this->email->subject( $Subject ); 
            // $this->email->message($body);
            // $send = $this->email->send();
          //  echo $send;

    }

    /*GET USER"S DOCUMENTS*/
    function getUserDocumentPath( $userId ){

        if( $userId != '' ){
            $this->db->where( "id", $userId );
            $user_data = $this->db->get( "user" )->row_array();
            
            if( isset( $user_data['documents'] ) && $user_data['documents'] != '' ){
                return $user_data['documents'];
            } else {
                return $this->createUserDocumentPath( $userId );
            }
        } else {
            return false;
        }

    }

    /*GET USER"S DOCUMENTS*/
    function createUserDocumentPath( $userId ){

        if( $userId != '' ){
            $rand = $this->randomText();
            $dir = $_SERVER['DOCUMENT_ROOT'] .'/documents/'. $rand;
            if( mkdir($dir, 0755) ){
                $this->db->where( 'id', $userId );
                $this->db->update( 'user', array( 'documents' => $rand ) );
                return $rand;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /*GENERATE RANDOM TEXT */
    function randomText( $cnt = false ){
        $length= 20;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $randomString = $randomString;

        return $randomString;
    }

    


    function getAllUserDocumentPath(  ){

       // if( $userId != '' ){
           // $this->db->where( "id", $userId );
            $user_data = $this->db->get( "user" )->result_array();
             foreach ($user_data as $user) {
                    if( isset( $user['documents'] ) && $user['documents'] != '' ){
                        return $user['documents'];
                    } else {
                    return $this->createUserDocumentPath( $userId );
                    }
                } 
    }

    public function add_assignto($assignee,$id,$module,$update=false)
    {
            $final_result=array();
          if(isset($_SESSION['id']))
            {
                $sender=$_SESSION['id'];
            }
            else
            {
                $sender=0;
            }

        $Assigned_Task = $this->db->query('SELECT `assignees` FROM firm_assignees WHERE firm_id = "'.$_SESSION['firm_id'].'" AND module_name = "'.$module.'" AND module_id = "'.$id.'"')->result_array();  
     $result_val=$get_user= $Return=$insert_user_val=array();

  if($assignee!='' && $id!='' )
  {
    if($update==1)
      {

        $Assigned_Task = $this->db->query('SELECT `assignees` FROM firm_assignees WHERE firm_id = "'.$_SESSION['firm_id'].'" AND module_name = "'.$module.'" AND module_id = "'.$id.'"')->result_array();  
        $Assigned_Task= array_column($Assigned_Task, 'assignees');
     
        $insert_result = array_diff( explode(",",$assignee) , $Assigned_Task );
        $delete_result = array_diff( $Assigned_Task , explode(",",$assignee) );


      if(!empty($delete_result))
      {
        $final_result =explode(':', implode(':',$delete_result) );
         $final_result = implode(',',$final_result);

        $final_result = $this->db->query("select user_id from organisation_tree where id IN (".$final_result.") ")->result_array();
        $final_result = array_unique( array_column( $final_result,'user_id') );
        }
       
      

        foreach ($delete_result as $key => $value)
        {
             
            $this->db->where('module_name',$module);
            $this->db->where('module_id',$id);
            $this->db->where('assignees',$value);
            $this->db->delete('firm_assignees');
        }

        foreach ($insert_result as $key => $value)
        {

            $data = ['firm_id'=>$_SESSION['firm_id'],'module_name'=>$module,'module_id'=>$id,'assignees'=>$value ];

            $this->db->insert('firm_assignees',$data);

        }

      
        $task_val=$this->select_record('add_new_task','id',$id);
        $get_task_name=$task_val['subject'];


      if(!empty($insert_result))
      {
        $insert_result =explode(':', implode(':',$insert_result) );
         $insert_result = implode(',',$insert_result);

        $insert_user_val = $this->db->query("select user_id from organisation_tree where id IN (".$insert_result.") ")->result_array();
        $insert_user_val = array_unique( array_column( $insert_user_val,'user_id') );
        }
        
        if(!empty($insert_user_val) && $module=='TASK'){
             $Notify_Content = "You Have Assign to ".$get_task_name." Task";
             foreach ($insert_user_val as $key => $value) {
                 $username[]=$this->Common_mdl->getUserProfileName($value); 
                  $get_user_data=$this->Common_mdl->GetAllWithWhere('user','id',$value);
                           if(count($get_user_data)>0){
                              $userId =$get_user_data[0]['id'];
                             $it_name=$get_user_data[0]['crm_name'];
                           $it_email=$get_user_data[0]['crm_email_id'];
                         
                                if($it_email!='')
                                {   

                                    $this->SendEmail_Notify( 'Task Assign', $Notify_Content ,$id,$it_name,$it_email);
                                }

                            $this->Send_Notification( 'Task' ,'Task Assign', $userId , $Notify_Content,$sender,$id );


                             $Sent_users[] = $userId;
                            
                           }
                    }

            $Log = $task_val['subject']." was assigned to ".implode(",",$username)." by ";
            $this->Create_Log( 'Task' , $id , $Log , $sender);
            
                } 


        if(!empty($final_result) && $module=='TASK'){
             $Notify_Content = "You Removed from the ".$get_task_name." Task";
             foreach ($final_result as $key => $value) {
                 $usernames[]=$this->Common_mdl->getUserProfileName($value); 
                  $get_user_data=$this->Common_mdl->GetAllWithWhere('user','id',$value);
                           if(count($get_user_data)>0){
                              $userId =$get_user_data[0]['id'];
                             $it_name=$get_user_data[0]['crm_name'];
                           $it_email=$get_user_data[0]['crm_email_id'];
                         
                                if($it_email!='')
                                {   

                                    $this->SendEmail_Notify( 'Task_Remove', $Notify_Content ,$id,$it_name,$it_email);
                                }

                            $this->Send_Notification( 'Task' ,'Task_Remove', $userId , $Notify_Content,$sender,$id );


                             $Sent_users[] = $userId;
                            
                           }
                    }

          ///  $Log = $task_val['subject']." was removed ".implode(",",$username);
             $Log = implode(",",$usernames)." was removed from the ".$task_val['subject']." by ";
            $this->Create_Log( 'Task' , $id , $Log , $sender);
            
                } 

    }
    else
    {

      foreach (explode(",",$assignee) as $key => $value)
        {

          $data = ['firm_id'=>$_SESSION['firm_id'],'module_name'=>$module,'module_id'=>$id,'assignees'=>$value ];         
          $this->db->insert('firm_assignees',$data);
        }


    }
     if($module=='TASK'){

        if(count(explode(",",$assignee) ) > 0)
        {
            $var=implode(",",explode(":",$assignee));   

            $insert_Return = $this->db->query("select user_id from organisation_tree where id IN (".$var.") ")->result_array();
             $insert_Return = array_unique( array_column( $insert_Return,'user_id') );

             
        $this->update_task_assignDetails($insert_Return,$id);
                            }
        }

  }

   
  return true;
}

         public function usertask_modify($assignee,$id,$table,$module)
         {

        $old_task= $this->GetAllWithWhere($table,'lead_id',$id);
       // $data['assignees']=$assignee;

        if(count($old_task)>0)
        {
                
                // if($table=='add_new_task')
                // {
                //      foreach ($old_task as $key => $value) {

                //         if($value['task_status']=='notstarted' || $value['task_status']=='archive' )
                //         {
                //     $this->add_assignto($assignee,$value['id'],$module,1);
                //          }

                // }

                // }
                // else
                // {
                     foreach ($old_task as $key => $value) {


                    $this->add_assignto($assignee,$value['id'],$module,1);

                }

               // }
               
        }
        }
        public function assign_check($id)
        {
        $count=0;
        $flag=0;
        $assign_check =  $this->db->query("select `task_status` from add_new_task where related_to='leads' and lead_id=".$id."")->result_array();
             if(count($assign_check)>0)
             {
              $assign_check = array_column($assign_check, 'task_status');
             // $target = array('', 'notstarted');

              foreach ($assign_check as $key => $value) {
                if($value=='' || $value=='1')
                {
                  $count++;
                }
               // # code...
              }

              if(count($assign_check)==$count)
              {
                $flag=1;
              }
            }
            else
            {
               $flag=1;
            }
            return $flag;
         }

      public function getReminderTemplates()
      {
          $sql = "SELECT * FROM email_templates WHERE firm_id IN('".$_SESSION['firm_id']."','0') AND reminder_heading!='' AND status = '1'";          
            
          $email_templates = $this->db->query($sql)->result_array();

          if($_SESSION['firm_id'] != 0)
          {
             $record = $this->db->query("SELECT super_admin_owned FROM email_templates WHERE firm_id = '".$_SESSION['firm_id']."' AND reminder_heading!='' AND status = '1'")->result_array();
          
             foreach ($email_templates as $key => $value) 
             {
                foreach ($record as $value1) 
                {
                   if($value['id'] == $value1['super_admin_owned'])
                   {
                      unset($email_templates[$key]);
                   }
                }
             }
          }

          return $email_templates;
      }
      //$Action for get specific action email template
      public function getTemplates($Action=0,$actions=false,$firm_id=false)
      {
          $ExCon="";

          if($Action!=0 && $Action != "")
          {
            $ExCon= " and action_id=".$Action;
          }  

          if(!empty($actions))
          {
             $module = ' AND FIND_IN_SET(action_id,"'.$actions.'")';
          }
          else
          {
             $module = '';
          }
          if(isset($_SESSION['firm_id']))
          {
            $firm_id=$_SESSION['firm_id'];
          }
    

          $sql = "SELECT * FROM email_templates WHERE firm_id IN('".$firm_id."','0') AND reminder_heading IS NULL AND status = '1' ".$ExCon." ".$module." ORDER BY id ASC";
           
          $email_templates = $this->db->query($sql)->result_array();

          if($firm_id != 0)
          {
             $record = $this->db->query("SELECT super_admin_owned FROM email_templates WHERE firm_id = '".$firm_id."' AND reminder_heading IS NULL AND status = '1' ".$ExCon." ".$module." ORDER BY id ASC")->result_array();
          
             foreach ($email_templates as $key => $value) 
             {
                foreach ($record as $value1) 
                {
                   if($value['id'] == $value1['super_admin_owned'])
                   {
                      unset($email_templates[$key]);
                   }
                }
             }
          }

          return $email_templates;
      }

      public function getproposal_templates()
      {
          $sql = "SELECT * FROM proposal_template WHERE firm_id = '0' ORDER BY id ASC";
          $proposal_templates = array();
           
          $templates = $this->db->query($sql)->result_array();

          if($_SESSION['firm_id'] != '0')
          {
             $record = $this->db->query("SELECT * FROM proposal_template WHERE firm_id = '".$_SESSION['firm_id']."' ORDER BY id ASC")->result_array();
             $proposal_templates['firm'] = $record;
          
             foreach ($templates as $key => $value) 
             {
                foreach ($record as $value1) 
                {
                   if($value['id'] == $value1['super_admin_owned'])
                   {
                      unset($templates[$key]);
                   }                  
                }
             }

             $proposal_templates['all'] = $templates;
          }
          else
          {
             $proposal_templates['firm'] = $templates;
          }                    

          return $proposal_templates;        
      }

      public function dynamic_status($table)
      {
      $get_status=$this->db->query('SELECT * FROM '.$table.' where firm_id="0" or firm_id='.$_SESSION['firm_id'].'')->result_array();
            $s_owned=array_column($get_status,'is_superadmin_owned');
             $s_delete=array_column($get_status,'is_delete');
             $s_id=array_column($get_status,'id');

             foreach ($s_delete as $key => $value) {
              if (false !== $key1 = array_search($value, $s_id)) {
                  unset($s_id[$key]);
                  unset($s_id[$key1]);
              } 
             }
              foreach ($s_owned as $key => $value) {
              if (false !== $key1 = array_search($value, $s_id)) {
                  unset($s_id[$key1]);
              } 
             }
             $s_id=(!empty($s_id))?implode(',',array_filter(array_unique($s_id))):'-1';

           $data=$this->db->query('SELECT * FROM '.$table.' where id in ('.$s_id.')  order by id asc')->result_array();
           return $data;
       }

       public function getAssignee($module_name,$module_id)
       {
          $data = array();
          
          $recent_assignees_id = $this->db->query('SELECT max(id) FROM firm_assignees WHERE firm_id = "'.$_SESSION['firm_id'].'" AND module_name = "'.$module_name.'" AND module_id = "'.$module_id.'"')->result_array();   
          $assignees = $this->db->query('SELECT assignees FROM firm_assignees WHERE id = "'.$recent_assignees_id[0]['max(id)'].'"')->row_array();     
          $assign_to = explode(':',$assignees['assignees']);
           
          foreach ($assign_to as $key => $value) 
          {
             $org_det = $this->Common_mdl->GetAllWithWhere('organisation_tree','id',$value); 
             $assignees_det = $this->db->query('SELECT user.crm_profile_pic,user.crm_name,user.id FROM user LEFT JOIN organisation_tree ON user.id = organisation_tree.user_id WHERE user_id = "'.$org_det[0]['user_id'].'" ')->row_array();
             $data[$key] = $assignees_det;
          }

          return $data;
       }

       public function getServiceCharge($service_id,$user_id)
       {         
           $invoice_ids = $this->db->query("SELECT invoice_id FROM add_new_task WHERE related_to_services = '".$service_id."' AND user_id = '".$user_id."' ORDER BY id DESC")->result_array();
           $service_charge = 0;

           foreach($invoice_ids as $key => $value) 
           {
               $invoice = $this->db->query("SELECT grand_total FROM amount_details WHERE client_id = '".$value['invoice_id']."' AND payment_status = 'pending'")->row_array(); 
               $service_charge += $invoice['grand_total'];
           }

           $service_list = $this->select_record('service_lists','id',$service_id);
           $service_name = $service_list['service_name'];           
           $proposal_invoices = $this->db->query("SELECT * FROM invoices WHERE item_name LIKE '%".$service_name."%' AND to_id = '".$user_id."' AND status = 'sent' ORDER BY id DESC")->result_array();

           $proposal_charge = 0;

           foreach ($proposal_invoices as $key => $value) 
           {
               $invoice_status = $this->db->query("SELECT amount_details.payment_status,amount_details.VAT,amount_details.adjust_to_tax FROM amount_details LEFT JOIN Invoice_details ON amount_details.client_id = Invoice_details.client_id WHERE Invoice_details.proposal_id = '".$value['proposal_id']."'")->row_array(); 
               
               if($invoice_status['payment_status'] == 'pending')
               {   
                   if(strpos($value['item_name'], ',') != false || strpos($value['item_name'], ',') == '0')
                   { 
                       $services = explode(',', $value['item_name']);
                       $price = explode(',', $value['price']);
                       $qty = explode(',', $value['qty']);
                       $tax = explode(',', $value['tax']);
                       $discount = explode(',', $value['discount']);
                       $index = array_search($service_name, $services);

                       if(!empty($index) || $index == '0')
                       { 
                          $proposal_charge += (($price[$index]*$qty[$index])+($tax[$index]+$invoice_status['VAT']+$invoice_status['adjust_to_tax'])) - $discount[$index];
                       }
                   }
                   else
                   { 
                       $proposal_charge += (($value['price']*$value['qty'])+($value['tax']+$invoice_status['VAT']+$invoice_status['adjust_to_tax'])) - $value['discount'];
                   }
               }                  
           }
      
           return round(($service_charge+$proposal_charge),2);
       }

       public function getFirmClients()
       {
           $clients = $this->db->query('SELECT id FROM user WHERE firm_id = "'.$_SESSION['firm_id'].'" AND user_type = "FC"')->result_array();
           
           foreach($clients as $key => $value) 
           {
              $arr[] = $value['id'];
           }

           return $arr;
       }

       public function getFirmUsers()
       {
           $users = $this->db->query('SELECT id FROM user WHERE firm_id = "'.$_SESSION['firm_id'].'" AND user_type != "FC"')->result_array();
           
           foreach($users as $key => $value) 
           {
              $arr[] = $value['id'];
           }

           return $arr;
       }

       public function check_distinct($table,$field,$value,$id)
       {   
           $sql = "SELECT count(*) as count FROM ".$table." WHERE ".$field." LIKE '".trim($value)."'";

           if($table == 'todos_list')
           {
              $sql .= "AND user_id = '".$_SESSION['id']."'";
           }
           else
           {
              $sql .= "AND firm_id = '".$_SESSION['firm_id']."'";
           }           

           if(!empty($id))
           {
              $sql = $sql." AND id!='".$id."'";
           }
        
           $data = $this->db->query($sql)->row_array(); 
           
           return $data;
       }

       public function getClientsDeadlinesCount($clients_user_ids,$start,$end)
       {
           $columns_check = array('1'=> 'crm_confirmation_statement_due_date','2' =>'crm_ch_accounts_next_due','3'=>'crm_accounts_tax_date_hmrc','4'=>'crm_personal_due_date_return','5' => 'crm_vat_due_date','6' => 'crm_rti_deadline','7' => 'crm_pension_subm_due_date','8' =>'crm_registered_renew_date','9' =>'','10'=>'crm_next_booking_date','11' => 'crm_next_p11d_return_due','12' => 'crm_next_manage_acc_date','13' => 'crm_insurance_renew_date','14'=>'crm_registered_renew_date','15'=>'crm_investigation_end_date','16' => 'crm_investigation_end_date');  

           $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

           foreach($other_services as $key => $value) 
           {
              $columns_check[$value['id']] = 'crm_'.$value['services_subnames'].'_statement_date';
           }         

           $service_list = $this->getServicelist(); 

           foreach ($service_list as $key => $value) 
           { 
              if($value['id'] != '9')
              { 
                 $data['deadlines'][$value['service_name']] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE FIND_IN_SET(user_id,"'.$clients_user_ids.'") and crm_company_name !="" and (('.$columns_check[$value['id']].' between "'.$start.'" AND "'.$end.'"))')->row_array(); 
              }
              else if($value['id'] == '9')
              {
                 $data['deadlines'][$value['service_name']] = 0;
              }
           }
           
           return $data;
       }

       public function getClientsServiceCharge($companys)
       {
           $firm_currency = $this->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'crm_currency');
           $data = array();
           $service_list = $this->getServicelist();          
           $columns = array('1'=> 'conf_statement','2' =>'accounts','3'=>'company_tax_return','4'=>'personal_tax_return','5' => 'vat','6' => 'payroll','7' => 'workplace', '8' => 'cis', '9' => 'cissub','10'=>'bookkeep','11' => 'p11d','12' => 'management','13' => 'investgate','14'=>'registered','15'=>'taxinvest','16' => 'taxadvice');

           $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

           foreach($other_services as $key => $value) 
           { 
              $columns[$value['id']] = $value['services_subnames'];
           }

           foreach ($companys as $getCompanykey => $getCompanyvalue) 
           {
               $getusername=$this->select_record('user','id',$getCompanyvalue['user_id']);
               
               $data[$getCompanykey]['id'] = $getCompanyvalue['id'];
               $data[$getCompanykey]['crm_name'] = $getusername['crm_name'];
               $data[$getCompanykey]['crm_legal_form'] = $getCompanyvalue['crm_legal_form']; 
               $data[$getCompanykey]['client_subscribed_services'] = $getCompanyvalue['client_subscribed_services'];   

               $client_subscribed_services = json_decode($getCompanyvalue['client_subscribed_services']); 
               $item_name = explode(',', $client_subscribed_services->item_name); 
               $quantity = explode(',',$client_subscribed_services->quantity);
               $unit_price = explode(',',$client_subscribed_services->amount);
               $discount = explode(',',$client_subscribed_services->discount);        
               $tax_amount = explode(',',$client_subscribed_services->tax_amount);

               foreach ($service_list as $key => $value) 
               { 
                  $charge = $this->getServiceCharge($value['id'],$getCompanyvalue['user_id']);
                  (isset(json_decode($getCompanyvalue[$columns[$value['id']]])->tab) && json_decode($getCompanyvalue[$columns[$value['id']]])->tab == 'on' && $charge>0) ? $data[$getCompanykey][$value['service_name']]['charge'] = $charge." ".$firm_currency : $data[$getCompanykey][$value['service_name']]['charge'] = "-";
                  (isset(json_decode($getCompanyvalue[$columns[$value['id']]])->tab) && json_decode($getCompanyvalue[$columns[$value['id']]])->tab == 'on')?$data[$getCompanykey][$value['service_name']]['enable'] = 'enable' : $data[$getCompanykey][$value['service_name']]['enable'] = '';
                 
                  if(!empty($client_subscribed_services) && in_array($value['service_name'], $item_name))
                  {                          
                      $keys = array_search($value['service_name'], $item_name);
                      $qty = $quantity[$keys];
                      $uprice = $unit_price[$keys];
                      $dis = $discount[$keys]; 
                      $taxamount = $tax_amount[$keys];
                      $amt = (($uprice*$qty)+($taxamount)) - $dis;

                      (isset(json_decode($getCompanyvalue[$columns[$value['id']]])->tab) && json_decode($getCompanyvalue[$columns[$value['id']]])->tab == 'on') ? $data[$getCompanykey][$value['service_name']]['basic'] = $amt." ".$firm_currency : $data[$getCompanykey][$value['service_name']]['basic'] = "-";
                  }
                  else
                  {   
                      $data[$getCompanykey][$value['service_name']]['basic'] = "-";
                  }                    
              }       
           }

           return $data;
       }

        public function createparentDirectory($user_id) {

            if (!defined('DOCUMENT_ROOT')) {
            define("DOCUMENT_ROOT", $_SERVER['DOCUMENT_ROOT'].'/');
             }
            $startDirectory = 'documents';
            $filePath1 = $startDirectory.'/'.$_SESSION['firm_id'];
            $filePath2 = $filePath1.'/'.$user_id; 
         //   echo 'ch';

           // echo  $filePath1 ;

            if (file_exists($filePath1)) {
               //  echo 'ch1';


                if (!file_exists($filePath2)) {
                  //  echo 'ch1';

                    return mkdir($filePath2, 0755);

                }
                else
                {
                    return false;
                }

            }
            else
            {       
                       mkdir($filePath1, 0755);

                if ( !file_exists($filePath2)) {
                   

                    return mkdir($filePath2, 0755);

                }


                //return 1;


            }

    }

    public function getServicelist()
    {
        $firm_services = $this->db->query('SELECT services FROM admin_setting WHERE firm_id = "'.$_SESSION['firm_id'].'"')->row_array();       
        $services = explode(',',$firm_services['services']);
       
        foreach ($services as $key => $value)
        {
           $value = trim(str_replace('{', '', $value));
           $value = trim(str_replace('}', '', $value));

           $val = explode(':',$value);
           $val[1] = str_replace('"', '', $val[1]);         

           if($val[1] == '1')
           { 
             $enabled_services[] = str_replace('"', '', $val[0]);
           }  
        }
     
        $enabled_services = implode(',', array_values($enabled_services));
        $service_list = $this->db->query('SELECT * FROM service_lists WHERE FIND_IN_SET(id,"'.$enabled_services.'") ORDER BY id')->result_array(); 
        return $service_list;
    }

    public function getServicedue($start,$end=false)
    {        
        if($end != false)
        {
           $cond = 'BETWEEN "'.$start.'" AND "'.$end.'"';
        }
        else
        {
           $cond = ' = "'.$start.'"';
        }

        $columns_check = array('1'=> 'crm_confirmation_statement_due_date','2' =>'crm_ch_accounts_next_due','3'=>'crm_accounts_tax_date_hmrc','4'=>'crm_personal_due_date_return','5' => 'crm_vat_due_date','6' => 'crm_rti_deadline','7' => 'crm_pension_subm_due_date','8' =>'','9' =>'','10'=>'crm_next_booking_date','11' => 'crm_next_p11d_return_due','12' => 'crm_next_manage_acc_date','13' => 'crm_insurance_renew_date','14'=>'crm_registered_renew_date','15'=>'crm_investigation_end_date','16' => 'crm_investigation_end_date');  

        $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

        foreach($other_services as $key => $value) 
        {            
           $columns_check[$value['id']] = 'crm_'.$value['services_subnames'].'_statement_date';          
        }
        
        if(!empty($_SESSION['client_uid']))
        {
            $services = $this->getClientServices($_SESSION['client_uid']); 
        }
        else
        {
            $services = $this->getServicelist();
        }

        $checks = "";
               
        if(count($services)>0)
        {
            $checks = 'AND (';

            foreach ($services as $key => $value) 
            {
               if($value['id'] == '6')
               {
                   if($end != false)
                   {
                      $checks .= '('.$columns_check[$value['id']].' BETWEEN "'.date('d-m-Y',strtotime($start)).'" AND "'.date('d-m-Y',strtotime($end)).'") OR ';                  
                   }
                   else
                   {
                      $checks .= '('.$columns_check[$value['id']].' = "'.date('d-m-Y',strtotime($start)).'") OR ';
                   }                 
               }
               else if($value['id'] != '8' && $value['id'] != '9' && $value['id'] != '6')
               {
                   $checks .= '('.$columns_check[$value['id']].' '.$cond.') OR ';               
               }          
            }

            $or_pos = strrpos($checks, 'OR');
            $checks = substr_replace($checks,'',$or_pos);  
          
            $checks .= ')';
        }
        
        return $checks;
    }

    public function add_deadline_notification()
    {
        $date_comp = date('Y-m-d');     

        $this->load->helper(['comman']);
        $assigned_client = Get_Assigned_Datas('CLIENT');
        $arr = array();

        foreach ($assigned_client as $key => $value) 
        {
            $rec = $this->Common_mdl->select_record('client','id', $value);
            
            if(!empty($rec['user_id']))
            {
              $arr[] = $rec['user_id'];
            }
        }
        
        $clients_user_ids = implode(',', $arr); 
        $conditions = $this->getServicedue($date_comp);
        
        $deadline_records= $this->db->query('SELECT * FROM client WHERE FIND_IN_SET(user_id,"'.$clients_user_ids.'") and crm_company_name !="" '.$conditions.'')->result_array(); 

        $services = $this->getServicelist();
        $columns_check = array('1'=> 'crm_confirmation_statement_due_date','2' =>'crm_ch_accounts_next_due','3'=>'crm_accounts_tax_date_hmrc','4'=>'crm_personal_due_date_return','5' => 'crm_vat_due_date','6' => 'crm_rti_deadline','7' => 'crm_pension_subm_due_date','10'=>'crm_next_booking_date','11' => 'crm_next_p11d_return_due','12' => 'crm_next_manage_acc_date','13' => 'crm_insurance_renew_date','14'=>'crm_registered_renew_date','15'=>'crm_investigation_end_date','16' => 'crm_investigation_end_date'); 
        $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

        foreach($other_services as $key => $value) 
        {            
           $columns_check[$value['id']] = 'crm_'.$value['services_subnames'].'_statement_date';          
        }

        foreach($deadline_records as $key => $value) 
        {
           foreach ($services as $key1 => $value1) 
           {
              if($value1['id'] == '6')
              {
                $date = date('d-m-Y',strtotime($date_comp));
              }
              else
              {
                $date = $date_comp;
              }

              if(!empty($value[$columns_check[$value1['id']]]) && $value[$columns_check[$value1['id']]] == $date)
              {
                $content = ucwords($value['crm_company_name']).' Has '.$value1['service_name'].' Due On '.$value[$columns_check[$value1['id']]];
                $this->Send_Notification('Deadline_Manager','deadline',$_SESSION['id'],$content,$value['user_id']);
              }  
           }           
        }

        return true;
    }

    public function generateRandomString($length = 100)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) 
        {
           $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function getClientServices($client_uid)
    { 
        $getCompanyvalue = $this->Common_mdl->select_record('client','user_id', $client_uid);

        $columns = array('1'=> 'conf_statement','2' =>'accounts','3'=>'company_tax_return','4'=>'personal_tax_return','5' => 'vat','6' => 'payroll','7' => 'workplace', '8' => 'cis', '9' => 'cissub','10'=>'bookkeep','11' => 'p11d','12' => 'management','13' => 'investgate','14'=>'registered','15'=>'taxinvest','16' => 'taxadvice');

        $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

        foreach($other_services as $key => $value) 
        { 
           $columns[$value['id']] = $value['services_subnames'];
        }

        $arr = array();

        foreach ($columns as $key => $value) 
        {
            if(isset(json_decode($getCompanyvalue[$value])->tab) && $getCompanyvalue[$value] != '')
            {
                $arr[] = $key;
            }    
        }
        
        $firm_services = $this->getServicelist();
        $firm_enabled_services = array();        
     
        $firm_enabled_services = array_map(array($this,'firm_enabled'),$firm_services);   
        $arr = array_intersect($arr, $firm_enabled_services);

        $enabled_services = implode(',', $arr);
        $services = $this->db->query('SELECT * FROM service_lists WHERE FIND_IN_SET(id,"'.$enabled_services.'") ORDER BY id')->result_array();

        return $services;       
    }

    function firm_enabled($val)
    {          
       return $val['id'];
    }   

    public function Curl_Call($url,$data,$secret_key,$method)
    {       
        $this->load->library('Curl'); 
        //  Setting URL To Fetch Data From
        $this->curl->create($url);          
        //  Authorization
        $this->curl->http_header('Authorization: Bearer '.$secret_key.'');
        //  To Receive Data Returned From Server
        $this->curl->option('returntransfer', 1);   
        //  To Retrieve Server Related Data
        $this->curl->option('HEADER', false);
        //  To Set Time For Process Timeout
        $this->curl->option('connecttimeout', 600); 
        //SSL Verify
        $this->curl->ssl(true); 

        if($method == 'POST')
        {       
           $this->curl->post($data);  //data
        }
        else if($method == 'GET')
        {
           $this->curl->option('post', false);
        } 

        //  To Execute 'option' Array Into cURL Library & Store Returned Data Into $data
        $data = $this->curl->execute(); 
        $info = $this->curl->debug();
      
        if($data == FALSE && $info['http_code'] != '200')
        {
            $data = array();
            $data['error'] = $info['error_msg'];
            $data['http_code'] = $info['http_code'];
        }
        
        return $data;               
    }

    public function Firm_Remaining_ClientCount( $firm_id )
    {
        $this->db->where('firm_id' , $firm_id );
        $data = $this->db->get('client')->num_rows();

        $clients_count = $this->select_record('firm','firm_id',$firm_id);    
        return $clients_count['no_of_clients_allowed'] - $data;
    }


    public function Check_Firm_Clients_limit($firm_id)
    {
        $clients_count = $this->get_price('firm','firm_id',$firm_id,'no_of_clients_allowed');

        return $clients_count;
    }

    public function Decrement_Firm_Clients_limit($firm_id)
    {
        $clients_count = $this->get_price('firm','firm_id',$firm_id,'no_of_clients_allowed');

        if($clients_count>0)
        {
           $count = $clients_count - 1;
        }
        else
        {
           $count = 0;
        }

        $this->db->update('firm',['no_of_clients_allowed' => $count],'firm_id = "'.$firm_id.'"');

        return true;
    }

   
     public function get_module_user_data($module,$module_id)
    {
       $result_val=$get_user= $Return=array();
                              $s_v=$this->db->query("select assignees from firm_assignees where module_name='".$module."' and module_id=".$module_id)->result_array();
                              $s_v = array_column($s_v,'assignees');
                            // print_r($s_v);
                             // exit;
                              foreach ($s_v as $key => $sv_value) {
                                //echo $sv_value;
                                $tmp=explode(":",$sv_value);
                               // $tmp = explode('.', $file_name);
                                $result_val[] = end($tmp);

                             //  print_r($result_val);
                               //$get_id[$key]=$result_val;
                              // 
                                # code...
                              }
                             //  print_r($result_val);
                         
                              //$data =explode(':', implode(':',$data) );
                             $get_user = implode(',',$result_val);
                                if(!empty($get_user))
                              {
                                $Return = $this->db->query("select user_id from organisation_tree where id IN (".$get_user.") ")->result_array();
                                $Return = array_unique( array_column( $Return,'user_id') );
                              }
                 return $Return;
    }
     



public function document_function( $client_id , $Notify_Type ,$file_name)
//public function document_function()
{
    $client_id =$this->Common_mdl->select_record('client','user_id', $client_id );
   $get_userid= Get_Module_Assigees('Client', $client_id['id']);

   if(isset($_SESSION['id']))
{
    $sender=$_SESSION['id'];
}
else
{
    $sender=0;
}

    $get_user=$this->Common_mdl->GetAllWithWhere('user','id',$_SESSION['id']);
   
     if($Notify_Type=='delete_file')
     {
        $Notify_Content=$file_name.' File was deleted by '.$get_user[0]['crm_name'].'in '.$client_id['crm_company_name'];
        $email_subject='Document File Delete Notifiction';
        $title='Files was Deleted in Documents';
     }
       if($Notify_Type=='delete_directory')
     {
        $Notify_Content=$file_name.' Directory was deleted by '.$get_user[0]['crm_name'].'in '.$client_id['crm_company_name'];
        $email_subject='Document Directory Delete Notifiction';
        $title='Directory was  Deleted in Documents';
     }
      if($Notify_Type=='directory_added')
     {
        $Notify_Content=$file_name.' Directory was added by '.$get_user[0]['crm_name'].'in '.$client_id['crm_company_name'];
        $email_subject='Document Directory Added Notifiction';
        $title='Directory was Added in Documents';
     }
      if($Notify_Type=='file_added')
     {
        $Notify_Content=$file_name.' File was added by '.$get_user[0]['crm_name'].'in '.$client_id['crm_company_name'];
        $email_subject='Document File added Notifiction';
        $title='File was Added in Documents';
     }


       if($get_userid!='')
                 {
                      foreach ($get_userid as $key => $value) 
                      {
                          $get_user_data=$this->Common_mdl->GetAllWithWhere('user','id',$value);
                          //print_r($get_user_data);
                         // echo 'work';
                               if(count($get_user_data)>0)
                               {
                                 $it_name=$get_user_data[0]['crm_name'];
                                 $it_email=$get_user_data[0]['crm_email_id'];
                              
                                    $this->Send_Notification('Documents', $Notify_Type , $get_user_data[0]['id'] , $Notify_Content,$sender);
                                if($it_email!='')
                                {   

                         $data2['email_contents'] =  $Notify_Content;
                          $data2['title']= $title;
                          //email template
                          $body = $this->load->view('remainder_email.php',$data2, TRUE);

                    if(!empty($get_user_data) && $get_user_data[0]['company_email']!='' )
                       {

                        $sender_id=$get_user_data[0]['company_email'];
                       }
                       else
                       {
                        $sender_id='info@remindoo.org';
                       }


                 // echo $body;
                 // exit;
                 
                       
                        $this->load->library('email');
                        $this->email->set_mailtype("html");
                        //$this->mailsettings();
                        $this->email->from($sender_id);
                        //$this->email->to('suganya.techleaf@gmail.com');
                        $this->email->to($it_email);
                        $this->email->subject(strip_tags($email_subject)); 
                        $this->email->message($body);
                        $send = $this->email->send();
                                }
                               }
                      }
               
                  }

                }

                public function update_task_assignDetails(array $assign,$task_id)
                {
                /** for update assigne data maintaine added and removed users 20-10-2018 **/
                            $it_assign = $assign;
                            // print_r( $it_assign);
                            // exit;
                            // $it_team = $assign['team'];
                            // $it_department = $assign['department'];
                            //$get_prev_assigne_data=$this->Task_section_model->get_assigne_data('staff',$task_id);
                          /** for staff assignee **/
                           $get_prev_assigne_data=$this->db->query("select * from task_assignee_add_remove_list where task_id=".$task_id."  ")->result_array();

                            $get_prev_assigne=array();
                            $get_prev_ids=array();
                            $get_added_removed_date=array();
                            $get_prev_status=array();
                            $get_prev_working_hrs=array();
                            if(count($get_prev_assigne_data)>0)
                            {
                              foreach ($get_prev_assigne_data as $prev_key => $prev_value) {
                                array_push($get_prev_assigne, $prev_value['assignee_ids']);
                                array_push($get_prev_ids, $prev_value['id']);
                                array_push($get_added_removed_date, $prev_value['added_removed_date']);
                                array_push($get_prev_status, $prev_value['status']);
                                array_push($get_prev_working_hrs,$prev_value['removed_workers_timing']);
                              }
                            }            
                            if(count($get_prev_assigne_data)>0)
                            {
                              $ex_assign=$get_prev_assigne;
                              $ex_result=array_diff($ex_assign,$it_assign); //find remove person
                             if(count($ex_result)>0)
                             {
                              foreach ($ex_result as $ex_key => $ex_value) {
                                $its_key = array_search ($ex_value, $ex_assign);
                                if($get_prev_status[$its_key]!='removed')
                                {
                                  $add_remove_list=array();
                                                     /** if it removed added a overall timing **/
                                    $get_individual_timer_data=$this->db->query("select * from individual_task_timer where user_id=".$get_prev_assigne[$ex_key]." and task_id=".$task_id." ")->row_array();
                                     $res_timing=$this->Task_section_model->calculate_timing_status_removed($get_individual_timer_data['time_start_pause']);
                                   // echo $res."jhdfhgf gjhggjhgh";
                                  /** end of overall timing **/
                                  if($get_prev_working_hrs[$its_key]!='')
                                  {
                                  $add_remove_list['removed_workers_timing']=$get_prev_working_hrs[$its_key].",".$res_timing;
                                  }
                                  else
                                  {
                                    $add_remove_list['removed_workers_timing']=$res_timing;
                                  }
                                  $add_remove_list['status']='removed';
                                  $add_remove_list['added_removed_date']=$get_added_removed_date[$its_key].",".time();
                                  $this->Common_mdl->update('task_assignee_add_remove_list',$add_remove_list,'id',$get_prev_ids[$ex_key]);
                                }
                              }
                             }
                            }
                            foreach ($it_assign as $itas_key => $itas_value) {
                              $get_count=$this->db->query("SELECT count(*) as it_count,GROUP_CONCAT(id) as dbid,added_removed_date,status FROM `task_assignee_add_remove_list` where assignee_ids=".$itas_value." and task_id=".$task_id." ")->row_array();
                              if($get_count['it_count']==0)
                              {
                                $add_remove_list=array();
                                  $add_remove_list['task_id']=$task_id;
                                  $add_remove_list['assignee_ids']=$itas_value;
                                  //$add_remove_list['staff_team_department']='staff';
                                  $add_remove_list['status']='active';
                                  $add_remove_list['removed_workers_timing']='';
                                  $add_remove_list['added_removed_date']=time();
                                  $add_remove_list['created_by']=$_SESSION['id'];
                                  $add_remove_list['created_time']=time();
                                  $this->Common_mdl->insert('task_assignee_add_remove_list',$add_remove_list);
                              }
                              else
                              {
                                $it_id=$get_count['dbid'];
                                if($get_count['status']!='active'){
                                  $add_remove_list=array();
                                  $add_remove_list['status']='active';
                                  $add_remove_list['added_removed_date']=$get_count['added_removed_date'].",".time();
                                  $this->Common_mdl->update('task_assignee_add_remove_list',$add_remove_list,'id',$it_id);
                               }
                              }
                            }

                          }
        public function assignees_delete($module,$id)
        {

             
            $this->db->where('module_name',$module);
            $this->db->where('module_id',$id);
            $this->db->delete('firm_assignees');
        
        }

      



}
?>