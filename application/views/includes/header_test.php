
<!DOCTYPE html>

<html lang="en">
   <head>
      <title>CRM </title>
      <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!-- Meta -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="description" content="#">
      <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
      <meta name="author" content="#">
      <!-- Favicon icon -->
      <link rel="icon" href="http://remindoo.org/CRMTool/assets/images/favicon.ico" type="image/x-icon">
      <!-- Google font-->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
      <!-- Required Fremwork -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/bootstrap/css/bootstrap.min.css">
      <!-- themify-icons line icon -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/icon/themify-icons/themify-icons.css">
      <!-- ico font -->
      <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/icon/icofont/css/icofont.css">
      <!-- flag icon framework css -->
      <!-- Menu-Search css -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/menu-search/css/component.css">
      <!-- jpro forms css -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/j-pro/css/demo.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/j-pro/css/font-awesome.min.css">
      <!--  <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/j-pro/css/j-pro-modern.css"> -->
      <!-- Style.css -->
      <link type="text/css" href="http://remindoo.org/CRMTool/bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/style.css">
  <!--     <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/advance-elements/css/bootstrap-datetimepicker.css"> -->
      <!-- Date-range picker css  -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/datedropper/css/datedropper.min.css" />
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
     <!--  <link rel="stylesheet" href="https:/resources/demos/style.css"> -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/switchery/css/switchery.min.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/jquery.mCustomScrollbar.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/owl.carousel/css/owl.carousel.css">
      <!--     <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/dataTables.bootstrap4.min.css">
         <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/buttons.dataTables.min.css?ver=2">
         <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/responsive.bootstrap4.min.css"> -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/ui_style.css?ver=3">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/common_style.css?ver=4">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/common_responsive.css?ver=2">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/css/dataTables.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/css/jquery.dropdown.css">
      <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
     <!--  <link rel="stylesheet" type="text/css" href="https://bootswatch.com/superhero/bootstrap.min.css"> -->
      <link href="<?php echo base_url(); ?>css/jquery.signaturepad.css" rel="stylesheet">
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
      <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/css/bootstrap-colorpicker.min.css">
      <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/css/bootstrap-slider.min.css">
      <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/plugins/medium-editor/medium-editor.min.css">
      <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/plugins/medium-editor/template.min.css">
      <!-- <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/css/style1.css"> -->
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo  base_url()?>/assets/icon/simple-line-icons/css/simple-line-icons.css">
      <link rel="stylesheet" type="text/css" href="<?php echo  base_url()?>/assets/icon/icofont/css/icofont.css">
      <style>
         .common_form_section1  .col-xs-12.col-sm-6.col-md-6 {
         float: left !important;
         }
         .common_form_section1 {
         background: #fff;
         box-shadow: 1px 2px 5px #dbdbdb;
         border-radius: 5px;
         padding: 30px;
         }
         .common_form_section1 label{
         display: block;
         margin-bottom: 7px;
         font-size: 15px;
         }
         .common_form_section1 input{
         width: 100%;
         }
         .col-sm-12{
         float: left;
         }
         .col-sm-8 {
         float: left;
         }
      </style>
   </head>
   <div class="LoadingImage">
   <div class="preloader3 loader-block">
      <div class="circ1 loader-primary loader-md"></div>
      <div class="circ2 loader-primary loader-md"></div>
      <div class="circ3 loader-primary loader-md"></div>
      <div class="circ4 loader-primary loader-md"></div>
   </div>
   </div>
   <style>
      .LoadingImage {
      display : none;
      position : fixed;
      z-index: 100;
      /*background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');*/
      background-color:rgba(65, 56, 57, 0.4);
      opacity : 1;
      background-repeat : no-repeat;
      background-position : center;
      left : 0;
      bottom : 0;
      right : 0;
      top : 0;
      }
   </style>
   <body>
    