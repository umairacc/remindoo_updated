<!DOCTYPE html>
<?php $uri = $this->uri->segment('2');
$isBasicTheme = ( isset( $theme ) && $theme == 'basic' ) ? true : false;
$this->Common_mdl->role_identify();
$Company_types = array();
?>
<html lang="en">
   <head>
      <title>CRM </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="description" content="#">
      <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
      <meta name="author" content="#">
      <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon"> 
      <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet"> -->
      <link href="<?php echo base_url();?>assets/cdns/css.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icon/themify-icons/themify-icons.css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/cdns/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/common_style.css?ver=4">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/common_responsive.css?ver=2">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ui_style.css?ver=3">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/menu-search/css/component.css">      
      <?php if( !$isBasicTheme ) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/j-pro/css/demo.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/j-pro/css/font-awesome.min.css">          
        <link type="text/css" href="<?php echo base_url();?>bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.css">         
        <link rel="stylesheet" href="<?php echo base_url();?>assets/cdns/jquery-ui.css">    
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/owl.carousel/css/owl.carousel.css">    
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/css/dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/css/jquery.dropdown.css">
        <link href="<?php echo base_url();?>assets/cdns/jquerysctipttop.css" rel="stylesheet" type="text/css">    
        <link href="<?php echo base_url(); ?>css/jquery.signaturepad.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/cdns/spectrum.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/datedropper/css/datedropper.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/switchery/css/switchery.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-slider.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/medium-editor/medium-editor.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/medium-editor/template.min.css">   
      <?php } ?>
        <link rel="stylesheet" type="text/css" href="<?php echo  base_url()?>/assets/icon/simple-line-icons/css/simple-line-icons.css">
        <link rel="stylesheet" type="text/css" href="<?php echo  base_url()?>/assets/icon/icofont/css/icofont.css">   
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/mobile_responsive.css?ver=2">   
      <style>
         .common_form_section1  .col-xs-12.col-sm-6.col-md-6 {
         float: left !important;
         }
         .common_form_section1 {
         background: #fff;
         box-shadow: 1px 2px 5px #dbdbdb;
         border-radius: 5px;
         padding: 30px;
         }
         .common_form_section1 label{
         display: block;
         margin-bottom: 7px;
         font-size: 15px;
         }
         .common_form_section1 input{
         width: 100%;
         }
         .col-sm-12{
         float: left;
         }
         .col-sm-8 {
         float: left;
         }
      </style>
      <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/custom/opt-profile.css?<?php echo time(); ?>">
   </head>
   <div class="LoadingImage">
   <div class="preloader3 loader-block">
      <div class="circ1 loader-primary loader-md"></div>
      <div class="circ2 loader-primary loader-md"></div>
      <div class="circ3 loader-primary loader-md"></div>
      <div class="circ4 loader-primary loader-md"></div>
   </div>
   </div>
   <div id="action_result" class="modal-alertsuccess alert succ dashboard_success_message" style="display:none;">
      <div class="newupdate_alert">
          <a href="#" class="close" id="close_action_result">×</a>
            <div class="pop-realted1">
                <div class="position-alert1">
                    Success !!! Staff Assign have been changed successfully...
                </div>
            </div>
      </div>
</div>

   <div id="action_result" class="modal-alertsuccess alert succ proposal_success_message" style="display:none;">
   <div class="newupdate_alert">
         <a href="#" class="close" id="close_action_result">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              Proposal Send Successfully!!!
         </div>
         </div>
         </div>
</div>
   <style>
      .LoadingImage {
      display : none;
      position : fixed;
      z-index: 100;
      /*background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');*/
      background-color:rgba(65, 56, 57, 0.4);
      opacity : 1;
      background-repeat : no-repeat;
      background-position : center;
      left : 0;
      bottom : 0;
      right : 0;
      top : 0;
      }
   </style>
   <body>
      <?php 
         $uri_seg_menu = $this->uri->segment('1'); 
         $uri_seg_menu1 = $this->uri->segment('2'); 
         $uri_seg_menu2 = $this->uri->segment('3');         
         $active_class1='';
         $active_class2='';
         $active_class3='';
         $active_class4='';
         $active_class5='';
         $active_class6='';
         $active_class7='';
         $active_class8='';
         $active_class9='';
         $active_class10='';
         $active_class11='';
         $active_class12='';
         $active_class13='';
         $active_class14='';
         $active_class15='';         
         $active_class20 ='';
         $active_class31='';
         $active_class32='';
         $active_class33='';
         $active_class34='';       
         $active_class35='';
         $active_class36='';         
         $client_list ='';
         $import_csv ='';
         $addnew ='';
         $addnew_cmp ='';
         $timeline='';         
         if($uri_seg_menu=='document')
         {
          $active_class9="head-active";
         }
         if($uri_seg_menu == 'timeline')
         {
          $active_class5 = "head-active";
          $timeline = "head-active";
         }         
          if($uri_seg_menu1 == 'column_setting_new')
          {
            $active_class12 = "head-active";
            $active_class8 = "head-active";
          }
          if($uri_seg_menu1 == 'team_management'){
             $active_class13 = "head-active";
          }
          if($uri_seg_menu1 == 'staff_custom_form')
          {
            $active_class14 = "head-active";
            $active_class8 = "head-active";         
          }
          if($uri_seg_menu1 == 'user_profile')
          {
            $active_class15 = "head-active";
            $active_class8 = "head-active";
          }
          if($uri_seg_menu1 == 'lead_settings')
          {
            $active_class35 = "head-active";
            $active_class8 = "head-active";
          }
          if($uri_seg_menu1 == 'new_task') {
          $active_class20 = "head-active";
          $active_class2 = "head-active";
         }
          if($uri_seg_menu1 == 'task_list') 
         {
           $active_class15 = "head-active";
          $active_class2 = "head-active";
         }
         /** for chat **/
         if($uri_seg_menu == 'user_chat')
         {
             $active_class36 = "head-active";
             $active_class8 = "head-active";
         }
         /** end of chat **/
         
         /** rspt 19-04-2018 leads heading **/
         if($uri_seg_menu=='leads' || $uri_seg_menu=='web_to_lead')
         {
         // $active_class3 = "head-active";
           if(($uri_seg_menu1=='' || $uri_seg_menu1 == 'kanban') && $uri_seg_menu == 'leads')
           {
             $active_class31 = "head-active";
             $active_class3 = "head-active";
           }
           if($uri_seg_menu1 == 'web_leads_view')
           {
              $active_class32 = "head-active";
              $active_class33 = "head-active";
              $active_class3 = "head-active";
           }
           if($uri_seg_menu=='web_to_lead' && $uri_seg_menu1=='')
           {
             $active_class32 = "head-active";
             $active_class34 = "head-active";
             $active_class3 = "head-active";
           }
           if($uri_seg_menu1 == 'web_leads_update_contents')
           {
             $active_class32 = "head-active";
             $active_class3 = "head-active";
           }
           if($uri_seg_menu1 == 'lead_settings')
           {
           //  $active_class35 = "head-active";
           }
           if($uri_seg_menu1 == 'add_status' || $uri_seg_menu1 == 'add_source' || $uri_seg_menu1 == 'update_status' || $uri_seg_menu1 == 'update_source')
           {
             // $active_class35 = "head-active";
           }         
         }
         /** for proposal **/
         
         if($uri_seg_menu=='proposal')
         {
           $active_class4 = "head-active";
         }
         
         /** end 19-04-2018 **/
         //echo $active_class2;die;        
         
         if($uri_seg_menu == 'user' && $uri_seg_menu1 == ''){
            $active_class5 = "head-active";
            $client_list = "head-active";
         }
         if($uri_seg_menu1=='addnewclient')
         {
            $active_class5 = "head-active";
            $addnew = "head-active";
         }if($uri_seg_menu1=='import_csv')
         {
           $active_class5 = "head-active";
           $import_csv = "head-active";
         }
         if($uri_seg_menu2!='' && $uri_seg_menu1)
         {
          if($uri_seg_menu=='user'){
             $active_class5 = "head-active";
             $addnew_cmp = "head-active";
          }            
         }
          /*if($uri_seg_menu == 'client' || $uri_seg_menu == 'Client'){
          $active_class5 = "head-active";
         } if($uri_seg_menu == 'user' && $uri_seg_menu1 == 'import_csv'){
          $active_class5 = "head-active";
         }*/ 
         if($uri_seg_menu1 == 'admin_setting' || $uri_seg_menu1 == 'firm_field' || $uri_seg_menu1 == 'column_setting' || $uri_seg_menu1 == 'custom_fields'){
             $active_class8 = "head-active";
            if($uri_seg_menu1 == 'admin_setting')
                  $active_class10 = "head-active";
            if($uri_seg_menu1 == 'firm_field')
                  $active_class11 = "head-active";
         }
          if($uri_seg_menu1 == 'team_management' || $uri_seg_menu1 == 'staff_list' || $uri_seg_menu == 'department' || $uri_seg_menu == 'team' ){
             $active_class8 = "head-active";
         } if($uri_seg_menu == 'staff' && $uri_seg_menu1 == ''){
             $active_class8 = "head-active";
         }
         if($uri_seg_menu == 'staff' && $uri_seg_menu1 == 'update_staff_details'){
               $active_class1 = "head-active";
         }
           // 31/05/2018 add active class pradeep
           // My disk
          if($uri_seg_menu =='Firm_dashboard')
          {
            $active_class1 = "head-active";
          }
          if($uri_seg_menu =='staff' && $uri_seg_menu1 == 'Dashboard')
          {
            $active_class1 = "head-active";
          }
          // Tasks
           if(($uri_seg_menu1=='new_task' || $uri_seg_menu1 == 'task_list') && $uri_seg_menu == 'user')
           {
             $active_class2 = "head-active";
             
           }
           // CRM
           if($uri_seg_menu =='leads' || $uri_seg_menu == 'web_to_lead')
          {
            $active_class3 = "head-active";
          }
          if($uri_seg_menu =='web_to_lead' && $uri_seg_menu1 == 'web_leads_view')
          {
            $active_class3 = "head-active";
          }
          // proposal
          if($uri_seg_menu =='proposal')
          {
            $active_class4 = "head-active";
          }
          // clients
          if($uri_seg_menu =='user' || $uri_seg_menu == 'user#' || $uri_seg_menu == 'timeline')
          {
            $active_class5 = "head-active";
          }
          if($uri_seg_menu =='client' && $uri_seg_menu1 == 'addnewclient')
          {
            $active_class5 = "head-active";
          }
          if($uri_seg_menu =='user' && $uri_seg_menu1 == 'import_csv')
          {
            $active_class5 = "head-active";
          }
          //Deadline Manager
          if($uri_seg_menu =='Deadline_manager')
          {
            $active_class6 = "head-active";
          }
          //Reports
          if($uri_seg_menu =='reports')
          {
            $active_class7 = "head-active";
          }
          //settings
          if($uri_seg_menu =='user' && $uri_seg_menu1 == 'admin_setting')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='staff' && $uri_seg_menu1 == 'staff_custom_form')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='Tickets')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='leads' && $uri_seg_menu1 == 'lead_settings')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='user_chat')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='user' && $uri_seg_menu1 == 'firm_field')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='user' && $uri_seg_menu1 == 'column_setting_new')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='team' && $uri_seg_menu1 == 'team_management')
          {
            $active_class8 = "head-active";
          }
          // Documents
          if($uri_seg_menu =='documents')
          {
            $active_class9 = "head-active";
          }
          //Invoice
          if($uri_seg_menu =='Invoice')
          {
            $active_class10 = "head-active";
          }
         ?>

                   <?php ?>
    <?php  ?>
      <!-- Pre-loader start -->
      <div class="theme-loader">
         <div class="ball-scale">
            <div class='contain'>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
            </div>
         </div>
      </div>

      <?php //} ?>
      <!-- Pre-loader end -->
      <div id="pcoded" class="pcoded" >
      <div class="pcoded-overlay-box"></div>
      <div class="pcoded-container navbar-wrapper">
      <nav class="navbar header-navbar pcoded-header">
         <div class="navbar-wrapper">
            <div class="navbar-logo">
				<a class="mobile-menu" id="mobile-collapse" href="javascript:void(0);">
					<i class="ti-menu"></i>
				</a>
               <a href="<?php echo base_url();?>">
               <img class="img-fluid" src="<?php echo base_url();?>assets/images/color_logo.png" alt="Theme-Logo" />
               </a>
               <a class="mobile-options">
               <i class="ti-more"></i>
               </a>
            </div>
            <div class="navbar-container container-fluid">
               <div class="nav-left ">
                <ul class='header-dynamic-cnt dynamic_header' id="owl-demo1">
                  <?php

                    $PARENT_LI = "<li class='dropdown comclass'><a href='{URL}' class='dropdown-toggle'> <img src='{ICON_PATH}' class='menicon'>{MENU_LABEL} </a> {CHILD_CONTENT} </li>";

                    $CHILD_UL = "<ul class='dropdown-menu1'>{CHILD_CONTENT}</ul>";

                    $CHILD_LI ="<li class='column-setting0 atleta'><a href='{URL}'>{MENU_LABEL}</a> {CHILD_CONTENT}  </li>";

                    echo getHeader( $PARENT_LI , $CHILD_UL , $CHILD_LI );

                  ?>
                  </ul>
               </div>
               <ul class="nav-right">
                  <?php 
                      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where 
                        firm_admin_id = ".$_SESSION['id']." and role = 4 and crm_name !=''")->row_array();
                      $us_id =  explode(',', $sql['us_id']);
                      $date_comp=date('Y-m-d');
                      $date_exp=date('F d, Y');
                      $date_ver=date('d-m-Y');                   
                      $deadline_records= $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date= '".$date_comp."') or (crm_ch_accounts_next_due = '".$date_comp."') or (crm_accounts_tax_date_hmrc = '".$date_comp."' ) or (crm_personal_due_date_return = '".$date_comp."' ) or (crm_vat_due_date = '".$date_comp."' ) or (crm_rti_deadline = '".$date_ver."')  or (crm_pension_subm_due_date = '".$date_comp."') or (crm_next_p11d_return_due = '".$date_comp."')  or (crm_next_manage_acc_date = '".$date_comp."') or (crm_next_booking_date = '".$date_comp."' ) or (crm_insurance_renew_date = '".$date_comp."')  or (crm_registered_renew_date = '".$date_comp."' ) or (crm_investigation_end_date = '".$date_comp."' )) ")->result_array();                    
                      $query3=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  
                      if($query3=='0'){
                        $count5=count($deadline_records);
                      }else{
                        $count5=0;
                      }
                      $query=$this->Common_mdl->section_menu('client_notification',$_SESSION['id']);  
                        if($query=='0'){ 
                             $noti=$this->db->query("select * from user where autosave_status='1' order by id desc")->result_array();
                             $count1=count($noti);
                        }else{
                             $count1=0;
                        }

                      $query1=$this->Common_mdl->section_menu('invoice_notification',$_SESSION['id']);  
                      if($query1=='0'){
                        $invoice_noti = $this->db->query('select * from Invoice_details where send_statements="send"')->result_array(); 
                        $repeatinvoice_noti = $this->db->query('select * from repeatInvoice_details where send_statements="send"')->result_array();
                        $count2=count($invoice_noti);
                        $count3=count($repeatinvoice_noti);
                      }else{
                        $count2=0;
                        $count3=0;
                      }

                      $query2=$this->Common_mdl->section_menu('companyhouse_notification',$_SESSION['id']);  
                      if($query2=='0'){
                         $queries=$this->Common_mdl->header_notification(); 
                         $count4=count($queries);
                      }else{
                          $count4=0;
                      }
                      if($_SESSION['role']=='5'){ 
                          $query10=$this->Common_mdl->section_menu('companyhouse_notification',$_SESSION['id']);
                            if(empty($query10) || $query10=='0'){                          
                               $queries1=$this->Common_mdl->get_manager_notification($_SESSION['id']); 
                               $xxx=count($queries1);
                               $tsk_re=$this->db->query("select mn.user_id,mn.id,u.crm_name,ant.subject,ant.id as task_id from Manager_notification as mn left join add_new_task ant on ant.id=mn.task_id left join user u on u.id=mn.user_id where mn.status=0 and  FIND_IN_SET($_SESSION[id],manager_id) and mn.review_status=0 and mn.views=0")->result_array();
                              $tsk_re_tot=count($tsk_re); 
                           }else{
                               $xxx=0;
                               $tsk_re_tot=0;
                               $count14=0;
                           }                         
                         }else{
                          $xxx=0;
                          $tsk_re_tot=0;                          
                         }       

                      $rejected_tsk=$this->db->query("select mn.user_id,mn.message,mn.id,u.crm_name,ant.subject from Manager_notification as mn left join add_new_task ant on ant.id=mn.task_id left join user u on u.id=mn.user_id where mn.status=0 and mn.review_status=2 and mn.user_id=$_SESSION[id] and mn.overall_status=0")->result_array();

                      $count14=count($rejected_tsk); 

                      $query11=$this->Common_mdl->section_menu('task_notification',$_SESSION['id']);
                        if(empty($query11) || $query11=='0')
                        {   
                            $task_notify=$this->Common_mdl->user_team_dept(); 
                            $task_count=count( $task_notify);
                        }
                        else
                        {
                            $task_count=0;
                        }               
                           ?>
                  <li class="header-notification">
                     <a href="#">
                       <i class="ti-bell"></i>
                       <span class="badge bg-c-pink sectionsss">
                         <?php echo $count1+$count2+$count3+$count5+$count4+$xxx+$tsk_re_tot+$count14+$task_count; ?>                   
                       </span>
                     </a>  


              <div class="show-notification  header_notify">
                  <div class="notification-title">
                      <h6>Notifications</h6>

                      <label class="label"><?php echo $count1+$count2+$count3+$count5+$count4+$xxx+$tsk_re_tot+$count14+$task_count; ?></label>
                  </div>

                  <ul class="nav nav-tabs">
                      <li class="nav-item">
                          <a class="nav-link active" data-toggle="tab" href="#nmenuo">Home</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#nmenut">Archive</a>
                      </li>
                    <!--   <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#nmenuth">Menu 2</a>
                      </li> -->
                  </ul>
                  <!-- Tab panes -->
                  <div class="tab-content">
                   <div class="tab-pane" id="nmenut">
                        <div class="company_notification">
                        <ul >
                          <?php
                              $con='';
                              $query11=$this->Common_mdl->section_menu('task_notification',$_SESSION['id']);
                              if(empty($query11) || $query11=='0'){   
                                $task_notify=$this->Common_mdl->user_team_dept();
                                  foreach ($task_notify as $open_task) {
                                      $stat=$this->Common_mdl->archive_check($open_task['id'],'task');
                                        if($stat==1){
                                           $con='style="display:none"';
                                        }else{
                                          $con='style="display:block"';
                                         } 
                                              ?>
                                  <li class="open_tsk" data-id="<?php echo $open_task['id']?>" <?php echo $con; ?>>
                                    <div class="media">
                                      <div class="media-body" >
                                           <b>Your <?php echo $open_task['subject']?> Task Assigned to you </b>
                                      </div>
                                        <div class="media-footer">
                                 <!--    </div> -->
                                            <a href="javascript:;" data-toggle="modal" data-target="#forward_task" id="forward-notification"  data-id="<?php echo $open_task['id']?>" data-type="forward" data-section="task"><i class="fa fa-share" aria-hidden="true"></i>
                                            </a> 
                                            <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $open_task['id']?>" data-type="archive" data-section="task"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                            <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $open_task['id']?>" id="remove-notification" data-type="remove" data-section="task"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                      </div>
                                  </li>
                          <?php 
                             }                           
                                }   ?>
                         <?php
                        foreach ($rejected_tsk as $v) { ?>
                        <li class="rejected_tsk" data-id="<?php echo $v['id']?>">
                           <div class="media">
                              <div class="media-body" >
<!--                               <button class="close_notification" data-id="<?php echo $v['id']?>">X</button>
 -->                                 <b>Your <?=$v['subject']?> Review is Decline</b>
                                 <p><?=$v['message']?></p>
                               </div>
                           </div>
                        </li>
                        <?php }?>
                        <?php           
                       /*** Client Service Request Notifications ***/
                       /*** Task Review Request ***/   
                         
                        /*** Task Review Request ***/

                         /*** Manager Task Accept Notifications ***/
                        if($_SESSION['role']=='5'){ 
                             $queries2=$this->Common_mdl->get_manager_notification($_SESSION['id']);                           
                              foreach($queries2 as $que){ 

                                  $service=ucwords(str_replace("_"," ",$que['service']));

                                  $crm_name=$this->Common_mdl->get_crm_name($que['user_id']);

                                    $stat=$this->Common_mdl->archive_check($que['id'],'manager_notification');
                                     
                                    if($stat=='0'){
                                      $con='style="display:none;"';
                                   }else{
                                    $con='style="display:block"';
                                   }  
                                ?>
                              <li <?php $query=$this->Common_mdl->section_menu('manager_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                                  <div class="media">
                                    <div class="media-body">
                                        <a href="javascript:;" data-service="<?php echo $que['service']; ?>" id="<?php echo $que['user_id']; ?>" data-id="<?php echo $que['id']; ?>" class="client_request" data-toggle="modal" data-target="#manager_service_accept">                                    

                                        <?php  echo $service.' Request From '.$crm_name;  ?>  </a>
                                    </div>
                                   <div class="media-footer">
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $que['id']?>" data-type="archive" data-section="manager_notification"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $que['id']?>" id="remove-notification" data-type="remove" data-section='manager_notification'><i class="fa fa-times" aria-hidden="true"></i></a>
                                   </div>
                                  </div>
                            </li>
                              <?php } 
                                if(isset($tsk_re)){
                                     foreach($tsk_re as $v){ 
                                       $stat=$this->Common_mdl->archive_check($v['id'],'managerreview_notification');
                                       if($stat=='1'){
                                $con='style="display:none;"';
                             }else{
                              $con='style="display:block"';
                             }  
                                      ?>              
                              <li <?php $query=$this->Common_mdl->section_menu('manager_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                               <div class="media">
                                <div class="media-body">                                                 
                                    <a href="javascript:void(0);" data-id="<?php echo $v['id']?>" class="staff_request" data-toggle="modal" data-target="#manager_task_review">
                                     <?php  echo  $v['subject']." Task Review From " .$v['crm_name'];  ?>  </a>
                                  </div>
                               <div class="media-footer">
                                 <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $v['id']?>" data-type="archive" data-section="managerreview_notification"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $v['id']?>" id="remove-notification" data-type="remove" data-section='managerreview_notification'><i class="fa fa-times" aria-hidden="true"></i></a>
                                </div>
                               </div>
                              </li>
                           <?php  
                                    } 
                                }
                        }  
                        ?>
                      <?php 
                          $noti=$this->db->query("select * from user where autosave_status='1' order by id desc")->result_array();
                          foreach($noti as $key => $val){

                                 $stat=$this->Common_mdl->archive_check($v['id'],'user');
                                 if($stat=='1'){
                                $con='style="display:none;"';
                             }else{
                              $con='style="display:block"';
                             }  
                            ?>
                            <li <?php $query=$this->Common_mdl->section_menu('client_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                               <div class="media">
                                  <div class="media-body">
                                     <a href="<?php echo base_url().'Client/client_info/'.$val['id'];?>">
                                        <h5 class="notification-user">Add Clients</h5>
                                        <span class="notification-time"><?php 
                                        if(isset($val['CreatedTime']) && $val['CreatedTime']!=''){ // 22-08-2018 
                                           $date=date('Y-m-d H:i:s',$val['CreatedTime']);
                                           echo time_elapsed_string($date);
                                           }?>  Status:<?php echo $stat; ?></span>
                                     </a> 
                                     </div>
                                       <div class="media-footer">
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $val['id']?>" data-type="archive" data-section="user"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $val['id']?>" id="remove-notification" data-type="remove" data-section='user'><i class="fa fa-times" aria-hidden="true"></i></a>
                                  </div>
                               </div>
                            </li>
                            <?php } ?>
                        <?php 
                        $queries=$this->Common_mdl->header_notification();  
                        $i=1;                      
                        foreach($queries as  $value){    
                       // echo $i;                      
                          $company_name=$this->Common_mdl->getcompany_name($value['id']);
                           if(isset($value['crm_company_name']) && ($value['crm_company_name']!='') || (isset($value['crm_company_number']) && $value['crm_company_number']!='') || ( isset($value['crm_incorporation_date']) && $value['crm_incorporation_date']!='') || (isset($value['crm_register_address']) && $value['crm_register_address']!='')  || (isset($value['crm_company_type']) &&  $value['crm_company_type']!='') || (isset($value['accounts_next_made_up_to']) && $value['accounts_next_made_up_to']!='') || (isset($value['accounts_next_due']) && $value['accounts_next_due']!='') || (isset($value['confirmation_next_made_up_to']) && $value['confirmation_next_made_up_to']!='') || (isset($value['confirmation_next_due']) && $value['confirmation_next_due']!='')){
                               $stat=$this->Common_mdl->archive_check($value['id'],'company_house');
                               $query=$this->Common_mdl->section_menu('companyhouse_notification',$_SESSION['id']);  

                               if($stat=='1'){
                                  $con='style="display:none;"';
                               }else{
                                  $con='style="display:block"';
                               } 
                        ?>
                         <li <?php $query=$this->Common_mdl->section_menu('companyhouse_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="javascript:;" class="companyhouse_update" id="<?php echo $value['id']; ?>" 
                                 data-companyname="<?php if(isset($value['crm_company_name']) && $value['crm_company_name']!=''){ echo $value['crm_company_name'];  }else{ echo ''; }?>"
                                 data-companynumber="<?php if(isset($value['crm_company_number']) && $value['crm_company_number']!=''){ echo $value['crm_company_number']; }else{ echo ''; }?>" 
                                 data-incorporationdate="<?php if(isset($value['crm_incorporation_date']) && $value['crm_incorporation_date']!=''){ echo $value['crm_incorporation_date']; } ?>"
                                 data-registeraddress="<?php if(isset($value['crm_register_address']) && $value['crm_register_address']!=''){ echo $value['crm_register_address']; } ?>" 
                                 data-companytype="<?php if(isset($value['crm_company_type']) && $value['crm_company_type']!=''){ echo $value['crm_company_type']; } ?>"
                                 data-accounts_next_made_up_to="<?php if(isset($value['accounts_next_made_up_to']) && $value['accounts_next_made_up_to']!=''){ echo $value['accounts_next_made_up_to']; } ?>"

                                 data-accounts_next_due="<?php if(isset($value['accounts_next_due']) && $value['accounts_next_due']!=''){ echo $value['accounts_next_due']; } ?>"

                                 data-confirmation_next_made_up_to="<?php if(isset($value['confirmation_next_made_up_to']) && $value['confirmation_next_made_up_to']!=''){ echo $value['confirmation_next_made_up_to']; } ?>"

                                 data-confirmation_next_due="<?php if(isset($value['confirmation_next_due']) && $value['confirmation_next_due']!=''){ echo $value['confirmation_next_due']; } ?>">
                                    <h5 class="notification-user">Update available on-<?php echo $company_name; ?> <?php echo $stat; ?></h5>
                                 </a>   
                              </div>
                              <div class="media-footer">
                                  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $value['id']?>" id="remove-notification" data-type="remove" data-section='company_house'><i class="fa fa-times" aria-hidden="true"></i></a>                                 
                                  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $value['id']?>" data-type="archive" data-section="company_house"> <i class="fa fa-archive" aria-hidden="true"></i></a>                               
                             </div>
                           </div>
                        </li>

                        <?php }}  ?>


                        <?php foreach($deadline_records as $deadline){ ?>

                        <?php  if($deadline['crm_confirmation_statement_due_date']==$date_comp && $deadline['crm_confirmation_statement_due_date']!=''){
                                $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_confirm_stmt');
                                if($stat==1){
                                   $con='style="display:none"';
                                }else{
                                  $con='style="display:block"';
                                 } 

                          ?>
                         <li  <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_confirmation_statement_due_date']) && $deadline['crm_confirmation_statement_due_date']!=''){ // 22-08-2018 

                                      echo 'Confirmation Statement Expires on'.$date=date('Y-m-d',strtotime($deadline['crm_confirmation_statement_due_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>  
                                 </div>  
                                   <div class="media-footer">
                                  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_confirm_stmt"> <i class="fa fa-archive" aria-hidden="true"></i></a>                                
                                
                               </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_ch_accounts_next_due']==$date_comp && $deadline['crm_ch_accounts_next_due']!=''){
                                $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_accounts');
                          if($stat==1){
                            $con='style="display:none"';
                          }else{
                              $con='style="display:block"';
                          } 
                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                                <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_ch_accounts_next_due']) && $deadline['crm_ch_accounts_next_due']!=''){ // 22-08-2018 
                                      echo 'Accounts Expires on'.$date=date('Y-m-d',strtotime($deadline['crm_ch_accounts_next_due']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>
                               </div>
                               <div class="media-footer">
                                    <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_accounts"> <i class="fa fa-archive" aria-hidden="true"></i></a>  
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_accounts_tax_date_hmrc']==$date_comp && $deadline['crm_accounts_tax_date_hmrc']!=''){
                                $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_company_tax');
                          if($stat==1){
                            $con='style="display:none"';
                          }else{
                              $con='style="display:block"';
                             } 
                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?>>
                           <div class="media">
                                <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_accounts_tax_date_hmrc']) && $deadline['crm_accounts_tax_date_hmrc']!=''){ // 22-08-2018 
                                       echo 'Tax Return Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_accounts_tax_date_hmrc']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a> 
                                 </div>
                                   <div class="media-footer">
                                 <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_company_tax"> <i class="fa fa-archive" aria-hidden="true"></i></a>                         
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_personal_due_date_return']==$date_comp && $deadline['crm_personal_due_date_return']!=''){

                                $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_persona_tax');
                          if($stat==1){
                            $con='style="display:none"';
                          }else{
                              $con='style="display:block"';
                             } 

                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?><?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_personal_due_date_return']) && $deadline['crm_personal_due_date_return']!=''){ // 22-08-2018 
                                       echo 'Personal Tax Expires on'.$date=date('Y-m-d',strtotime($deadline['crm_personal_due_date_return']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a> 
                                 </div>
                                   <div class="media-footer">
                                        <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_persona_tax"> <i class="fa fa-archive" aria-hidden="true"></i></a>                                
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_vat_due_date']==$date_comp && $deadline['crm_vat_due_date']!=''){
                                $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_vat');
                                if($stat==1){
                                     $con='style="display:none"';
                                }else{
                              $con='style="display:block"';
                             } 
                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?><?php echo $con; ?>>
                           <div class="media">
                             <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_vat_due_date']) && $deadline['crm_vat_due_date']!=''){ // 22-08-2018 
                                       echo 'Vat return Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_vat_due_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a> 
                                  </div>
                                   <div class="media-footer">
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_vat"> <i class="fa fa-archive" aria-hidden="true"></i></a>                           
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_rti_deadline']==$date_ver && $deadline['crm_rti_deadline']!=''){
                                $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_payroll');
                                if($stat==1){
                                  $con='style="display:none"';
                                }else{
                              $con='style="display:block"';
                             } 
                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?><?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_rti_deadline']) && $deadline['crm_rti_deadline']!=''){ // 22-08-2018 
                                       echo 'Payroll Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_rti_deadline']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>
                                  </div>
                                   <div class="media-footer">
                                        <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_payroll"> <i class="fa fa-archive" aria-hidden="true"></i></a>                                 
                                
                              </div>
                           </div>
                        </li>
                          <?php } ?>
                          <?php  if($deadline['crm_pension_subm_due_date']==$date_comp && $deadline['crm_pension_subm_due_date']!=''){
                                $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_workplace');
                              if($stat==1){
                                $con='style="display:none"';
                              }else{
                              $con='style="display:block"';
                             } 
                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?><?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_pension_subm_due_date']) && $deadline['crm_pension_subm_due_date']!=''){ // 22-08-2018 
                                       echo 'Work Place-Pension Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_pension_subm_due_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>  
                                  </div>
                                   <div class="media-footer">
                                        <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_workplace"> <i class="fa fa-archive" aria-hidden="true"></i></a>  
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                            <?php  if($deadline['crm_next_p11d_return_due']==$date_comp && $deadline['crm_next_p11d_return_due']!=''){
                                    $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_p11d');
                                    if($stat==1){
                                       $con='style="display:none"';
                                    }else{
                              $con='style="display:block"';
                             } 
                          ?>
                             <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                             <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_next_p11d_return_due']) && $deadline['crm_next_p11d_return_due']!=''){ // 22-08-2018 
                                       echo 'P11D Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_next_p11d_return_due']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>  
                                  </div>
                                   <div class="media-footer">     

                                       <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_p11d"> <i class="fa fa-archive" aria-hidden="true"></i></a>                        
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_next_manage_acc_date']==$date_comp && $deadline['crm_next_manage_acc_date']!=''){
                                $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_management');
                                if($stat==1){
                                   $con='style="display:none"';
                                }else{
                              $con='style="display:block"';
                             } 
                                ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?><?php echo $con; ?>>
                           <div class="media">
                               <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php 
                                    if(isset($deadline['crm_next_manage_acc_date']) && $deadline['crm_next_manage_acc_date']!=''){ // 22-08-2018 
                                       echo 'Management Accounts Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_next_manage_acc_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>  
                                  </div>
                                   <div class="media-footer">
                                        <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_management"> <i class="fa fa-archive" aria-hidden="true"></i></a>                               
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_next_booking_date']==$date_comp && $deadline['crm_next_booking_date']!=''){
                                $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_booking');
                                if($stat==1){
                                $con='style="display:none"';
                                }else{
                              $con='style="display:block"';
                             } ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?><?php echo $con; ?>>
                           <div class="media">
                           <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php 
                                    if(isset($deadline['crm_next_booking_date']) && $deadline['crm_next_booking_date']!=''){ // 22-08-2018 
                                      echo 'Book Keeping Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_next_booking_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>
                                  </div>
                                   <div class="media-footer">   

                                    <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_booking"> <i class="fa fa-archive" aria-hidden="true"></i></a>                                    
                                
                              </div>
                           </div>
                        </li>
                          <?php } ?>
                          <?php  if($deadline['crm_insurance_renew_date']==$date_comp && $deadline['crm_insurance_renew_date']!=''){
                                $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','data_insurance');
                                if($stat==1){
                                    $con='style="display:none"';
                                }else{
                              $con='style="display:block"';
                             } 
                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?><?php echo $con; ?>>
                           <div class="media">
                             <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php 
                                    if(isset($deadline['crm_insurance_renew_date']) && $deadline['crm_insurance_renew_date']!=''){ // 22-08-2018 
                                       echo 'Investigation Insurance Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_insurance_renew_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a> 
                                  </div>
                                   <div class="media-footer">    
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="data_insurance"> <i class="fa fa-archive" aria-hidden="true"></i></a>                                  
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_registered_renew_date']==$date_comp && $deadline['crm_registered_renew_date']!=''){
                                $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_registered');
                          if($stat==1){
                            $con='style="display:none"';
                          }else{
                              $con='style="display:block"';
                             } 
                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?><?php echo $con; ?>>
                           <div class="media">
                               <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php 
                                    if(isset($deadline['crm_registered_renew_date']) && $deadline['crm_registered_renew_date']!=''){ // 22-08-2018 
                                      echo 'Registered Access Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_registered_renew_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>
                                  </div>
                                   <div class="media-footer"> 

                                    <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_registered"> <i class="fa fa-archive" aria-hidden="true"></i></a>                                      
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_investigation_end_date']==$date_comp && $deadline['crm_investigation_end_date']!=''){
                              $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_investigation');
                          if($stat==1){
                            $con='style="display:none"';
                          }else{
                              $con='style="display:block"';
                             } 
                          ?>
                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?> </h5>
                                    <span class="notification-time"><?php 
                                    if(isset($deadline['crm_investigation_end_date']) && $deadline['crm_investigation_end_date']!=''){ // 22-08-2018 
                                       echo 'Tax Advice/Investigation Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_investigation_end_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>  
                                  </div>
                                   <div class="media-footer">                             
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_investigation"> <i class="fa fa-archive" aria-hidden="true"></i></a>         
                              </div>
                           </div>
                        </li>
                         <?php } ?>
                         <?php } ?>
                        <!--  Invoice Send Statements  -->   
                        <?php
                           if($id = $this->session->userdata('admin_id'))
                           {
                            $useremail = $this->db->query('select crm_email_id,role from user where id='.$id.'')->row_array();
                            
                              if($useremail['role'] == '1')
                              {
                                $invoice_bill = $this->db->query('select * from Invoice_details where send_statements="send" and views=0')->result_array();
                                foreach ($invoice_bill as $invoice) { 
                                    $stat=$this->Common_mdl->archive_check($invoice['id'],'invoice');
                                  if($stat=='1'){
                                     $con='style="display:none;"';
                                  } 
                                  ?>
                                    <li <?php $query=$this->Common_mdl->section_menu('invoice_notification',$_SESSION['id']);  
                                    if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                                       <div class="media">
                                          <div class="media-body">
                                             <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$invoice['client_id'];?>">
                                                <h5 class="notification-user">Invoice</h5>
                                                <span class="notification-time"><?php 
                                                if(isset($invoice['created_at'])&& $invoice['created_at']!='0000-00-00 00:00:00'){
                                                   $date=date('Y-m-d H:i:s',$invoice['created_at']);
                                                   echo time_elapsed_string($date);
                                                 }
                                                   ?><?php echo $stat; ?></span>
                                             </a>
                                              </div>
                                   <div class="media-footer">
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $invoice['id']?>" data-type="archive" data-section="invoice"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $invoice['id']?>" id="remove-notification" data-type="remove" data-section='invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                                          </div>
                                       </div>
                                    </li>
                        <?php } ?>
                        <?php $rep_invoice = $this->db->query('select * from repeatInvoice_details where send_statements="send" and views=0')->result_array();
                           foreach ($rep_invoice as $repeat_invoice) {

                             $stat=$this->Common_mdl->archive_check($repeat_invoice['client_id'],'repeat-invoice');
                             if($stat=='1'){
                                $con='style="display:none;"';
                             } 
                              ?>
                        <li <?php $query=$this->Common_mdl->section_menu('invoice_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$repeat_invoice['client_id'];?>">
                                    <h5 class="notification-user">Invoice</h5>
                                    <span class="notification-time"><?php 
                                       $date=date('Y-m-d H:i:s',$repeat_invoice['created_at']);
                                       echo time_elapsed_string($date);?></span>
                                 </a>
                                  </div>
                                   <div class="media-footer">

                                 <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $repeat_invoice['client_id']; ?>" data-type="archive" data-section="repeat-invoice"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                           <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $repeat_invoice['client_id']; ?>" id="remove-notification" data-type="remove" data-section='repeat-invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                              </div>
                           </div>
                        </li>
                        <?php } } 
                           if($useremail['crm_email_id'])
                           { 
                             $invoice_info = $this->db->query('select * from Invoice_details where client_email="'.$useremail['crm_email_id'].'" and send_statements="send" and views=0')->row_array(); 
                             if($invoice_info) { ?>
                        <li <?php $query=$this->Common_mdl->section_menu('invoice_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$invoice_info['client_id'];?>">
                                    <h5 class="notification-user">Invoice</h5>
                                    <span class="notification-time"><?php 
                                       $date=date('Y-m-d H:i:s',$invoice_info['created_at']);
                                       echo time_elapsed_string($date);?></span>
                                 </a>
                                  </div>
                                   <div class="media-footer">

                                   <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $invoice_info['id']?>" data-type="archive" data-section="invoice"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                           <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $invoice_info['id']?>" id="remove-notification" data-type="remove" data-section='invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                              </div>
                           </div>
                        </li>
                        <?php } 
                           $Repeatinvoice_info = $this->db->query('select * from repeatInvoice_details where client_email="'.$useremail['crm_email_id'].'" and send_statements="send" and views=0')->row_array(); 
                           if($Repeatinvoice_info) { ?>
                        <li <?php $query=$this->Common_mdl->section_menu('invoice_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$Repeatinvoice_info['client_id'];?>">
                                    <h5 class="notification-user">Invoice</h5>
                                    <span class="notification-time"><?php 
                                       $date=date('Y-m-d H:i:s',$Repeatinvoice_info['created_at']);
                                       echo time_elapsed_string($date);?></span>
                                 </a>
                                  </div>
                                   <div class="media-footer">

                                <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $Repeatinvoice_info['client_id']?>" data-type="archive" data-section="repeat-invoice"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $Repeatinvoice_info['client_id']?>" id="remove-notification" data-type="remove" data-section='repeat-invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                              </div>
                           </div>
                        </li>
                        <?php } }
                           } ?>
                     </ul>
                   </div>
                   </div>
                      <div class="tab-pane active" id="nmenuo">
                        <div class="company_notification">
                        <ul >
                          <?php
                           $query11=$this->Common_mdl->section_menu('task_notification',$_SESSION['id']);
                            if(empty($query11) || $query11=='0'){   
                              $task_notify=$this->Common_mdl->user_team_dept();
                                foreach ($task_notify as $open_task) {  

                                $stat=$this->Common_mdl->archive_check($open_task['id'],'task');
                                if($stat==0){
                                  $con='style="display:none"';
                                }else{
                                  $con='style="display:block"';
                                }
                                ?>
                          <li class="open_tsk" data-id="<?php echo $open_task['id']?>" <?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body" >
                              
                              <b>Your <?php echo $open_task['subject']?> Task Assigned to you </b>
                               </div>
                          
                                   <div class="media-footer">
                            
                              <a href="javascript:;" data-toggle="modal" data-target="#forward_task" id="forward-notification"  data-id="<?php echo $open_task['id']?>" data-type="forward" data-section="task"><i class="fa fa-share" aria-hidden="true"></i></a> 
                              <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $open_task['id']?>" data-type="archive" data-section="task"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                              <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $open_task['id']?>" id="remove-notification" data-type="remove" data-section="task"><i class="fa fa-times" aria-hidden="true"></i></a>
                              </div>
                              </div>
                        </li>
                      <?php 
                        }                           
                        } 
                      ?>
                         <?php
                        foreach ($rejected_tsk as $v) { ?>
                        <li class="rejected_tsk" data-id="<?php echo $v['id']?>">
                           <div class="media">
                              <div class="media-body" >
<!--                               <button class="close_notification" data-id="<?php echo $v['id']?>">X</button>
 -->                                 <b>Your <?=$v['subject']?> Review is Decline</b>
                                 <p><?=$v['message']?></p>
                               </div>
                           </div>
                        </li>
                        <?php }?>
                        <?php           
                       /*** Client Service Request Notifications ***/
                       /*** Task Review Request ***/   
                         
                        /*** Task Review Request ***/

                         /*** Manager Task Accept Notifications ***/
                        if($_SESSION['role']=='5'){ 
                             $queries2=$this->Common_mdl->get_manager_notification($_SESSION['id']);                           
                              foreach($queries2 as $que){ 

                                  $service=ucwords(str_replace("_"," ",$que['service']));

                                  $crm_name=$this->Common_mdl->get_crm_name($que['user_id']);

                                    $stat=$this->Common_mdl->archive_check($que['id'],'manager_notification');
                                     if($stat=='1'){
                                  $con='style="display:none"';
                                }else{
                                  $con='style="display:block"';
                                } 
                                ?>
                              <li <?php $query=$this->Common_mdl->section_menu('manager_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                                  <div class="media">
                                    <div class="media-body">
                                        <a href="javascript:;" data-service="<?php echo $que['service']; ?>" id="<?php echo $que['user_id']; ?>" class="client_request" data-id="<?php echo $que['id']; ?>" data-toggle="modal" data-target="#manager_service_accept">                                    

                                        <?php  echo $service.' Request From '.$crm_name;  ?> </a>
                                   </div>
                                   <div class="media-footer">
                             
                                  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $que['id']?>" data-type="archive" data-section="manager_notification"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $que['id']?>" id="remove-notification" data-type="remove" data-section='manager_notification'><i class="fa fa-times" aria-hidden="true"></i></a>
                                  </div>
                                  </div>
                            </li>
                              <?php } 
                                if(isset($tsk_re)){
                                     foreach($tsk_re as $v){ 
                                       $stat=$this->Common_mdl->archive_check($v['id'],'managerreview_notification');
                                         if($stat==0){
                                  $con='style="display:none"';
                                }else{
                                  $con='style="display:block"';
                                } 
                                      ?>              
                              <li <?php $query=$this->Common_mdl->section_menu('manager_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                               <div class="media">
                                <div class="media-body">                                                 
                                    <a href="javascript:void(0);" data-id="<?php echo $v['id']?>" class="staff_request" data-toggle="modal" data-target="#manager_task_review">
                                     <?php  echo  $v['subject']." Task Review From " .$v['crm_name'];  ?>  Status:<?php echo $stat; ?></a>
                                  </div>
                                   <div class="media-footer">
                                 <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $v['id']?>" data-type="archive" data-section="managerreview_notification"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $v['id']?>" id="remove-notification" data-type="remove" data-section='managerreview_notification'><i class="fa fa-times" aria-hidden="true"></i></a>
                                  </div>
                                  </div>
                              </li>
                           <?php  
                                    } 
                                }
                        }  
                        ?>
                      <?php 
                          $noti=$this->db->query("select * from user where autosave_status='1' order by id desc")->result_array();
                          foreach($noti as $key => $val){

                                 $stat=$this->Common_mdl->archive_check($v['id'],'user');
                                   if($stat==0){
                                  $con='style="display:none"';
                                }else{
                                  $con='style="display:block"';
                                } 
                            ?>
                            <li <?php $query=$this->Common_mdl->section_menu('client_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                               <div class="media">
                                  <div class="media-body">
                                     <a href="<?php echo base_url().'Client/client_info/'.$val['id'];?>">
                                        <h5 class="notification-user">Add Clients</h5>
                                        <span class="notification-time"><?php 
                                        if(isset($val['CreatedTime']) && $val['CreatedTime']!=''){ // 22-08-2018 
                                           $date=date('Y-m-d H:i:s',$val['CreatedTime']);
                                           echo time_elapsed_string($date);
                                           }?>  Status:<?php echo $stat; ?></span>
                                     </a> 
                                      </div>
                                   <div class="media-footer">
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $val['id']?>" data-type="archive" data-section="user"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $val['id']?>" id="remove-notification" data-type="remove" data-section='user'><i class="fa fa-times" aria-hidden="true"></i></a>
                                  </div>
                               </div>
                            </li>
                            <?php } ?>
                        <?php 
                        $queries=$this->Common_mdl->header_notification();                        
                        foreach($queries as  $value){                          
                          $company_name=$this->Common_mdl->getcompany_name($value['id']);
                           if(isset($value['crm_company_name']) && ($value['crm_company_name']!='') || (isset($value['crm_company_number']) && $value['crm_company_number']!='') || ( isset($value['crm_incorporation_date']) && $value['crm_incorporation_date']!='') || (isset($value['crm_register_address']) && $value['crm_register_address']!='')  || (isset($value['crm_company_type']) &&  $value['crm_company_type']!='') || (isset($value['accounts_next_made_up_to']) && $value['accounts_next_made_up_to']!='') || (isset($value['accounts_next_due']) && $value['accounts_next_due']!='') || (isset($value['confirmation_next_made_up_to']) && $value['confirmation_next_made_up_to']!='') || (isset($value['confirmation_next_due']) && $value['confirmation_next_due']!='')){


                               $stat=$this->Common_mdl->archive_check($value['id'],'company_house');

                               if($stat==0){
                                $con='style="display:none"';
                               }else{
                                    $con='style="display:block"';
                               }
                        ?>
                         <li <?php $query=$this->Common_mdl->section_menu('companyhouse_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="javascript:;" class="companyhouse_update" id="<?php echo $value['id']; ?>" 
                                 data-companyname="<?php if(isset($value['crm_company_name']) && $value['crm_company_name']!=''){ echo $value['crm_company_name'];  }else{ echo ''; }?>"
                                 data-companynumber="<?php if(isset($value['crm_company_number']) && $value['crm_company_number']!=''){ echo $value['crm_company_number']; }else{ echo ''; }?>" 
                                 data-incorporationdate="<?php if(isset($value['crm_incorporation_date']) && $value['crm_incorporation_date']!=''){ echo $value['crm_incorporation_date']; } ?>"
                                 data-registeraddress="<?php if(isset($value['crm_register_address']) && $value['crm_register_address']!=''){ echo $value['crm_register_address']; } ?>" 
                                 data-companytype="<?php if(isset($value['crm_company_type']) && $value['crm_company_type']!=''){ echo $value['crm_company_type']; } ?>"
                                 data-accounts_next_made_up_to="<?php if(isset($value['accounts_next_made_up_to']) && $value['accounts_next_made_up_to']!=''){ echo $value['accounts_next_made_up_to']; } ?>"
                                 data-accounts_next_due="<?php if(isset($value['accounts_next_due']) && $value['accounts_next_due']!=''){ echo $value['accounts_next_due']; } ?>"
                                 data-confirmation_next_made_up_to="<?php if(isset($value['confirmation_next_made_up_to']) && $value['confirmation_next_made_up_to']!=''){ echo $value['confirmation_next_made_up_to']; } ?>"
                                 data-confirmation_next_due="<?php if(isset($value['confirmation_next_due']) && $value['confirmation_next_due']!=''){ echo $value['confirmation_next_due']; } ?>" >
                                    <h5 class="notification-user">Update available on-<?php echo $company_name; ?> <?php echo $stat; ?></h5>
                                 </a>
                               <!--   <a href="#" data-toggle="modal" data-target="#modalarchive" data-id="<?php echo $value['id']?>" data-type="service" > <i class="fa fa-archive" aria-hidden="true"></i></a>      -->                       
                                
                              </div>
                              <div class="media-footer"> 
                                  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $value['id']?>" id="remove-notification" data-type="remove" data-section='company_house'><i class="fa fa-times" aria-hidden="true"></i></a>                                  
                                  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $value['id']?>" data-type="archive" data-section="company_house"> <i class="fa fa-archive" aria-hidden="true"></i></a>

                               
                             </div>
                           </div>
                        </li>

                        <?php }}  ?>


                        <?php foreach($deadline_records as $deadline){ ?>

                        <?php  if($deadline['crm_confirmation_statement_due_date']==$date_comp && $deadline['crm_confirmation_statement_due_date']!=''){
                          $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_confirm_stmt');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }
                          ?>
                         <li  <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_confirmation_statement_due_date']) && $deadline['crm_confirmation_statement_due_date']!=''){ // 22-08-2018 

                                      echo 'Confirmation Statement Expires on'.$date=date('Y-m-d',strtotime($deadline['crm_confirmation_statement_due_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>    
                                  </div>
                                   <div class="media-footer">
                                  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_confirm_stmt"> <i class="fa fa-archive" aria-hidden="true"></i></a>                                
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_ch_accounts_next_due']==$date_comp && $deadline['crm_ch_accounts_next_due']!=''){

                              $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_accounts');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }

                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                                <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_ch_accounts_next_due']) && $deadline['crm_ch_accounts_next_due']!=''){ // 22-08-2018 
                                      echo 'Accounts Expires on'.$date=date('Y-m-d',strtotime($deadline['crm_ch_accounts_next_due']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>
                               </div>
                               <div class="media-footer">
                                    <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_accounts"> <i class="fa fa-archive" aria-hidden="true"></i></a>  
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_accounts_tax_date_hmrc']==$date_comp && $deadline['crm_accounts_tax_date_hmrc']!=''){

                          $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_company_tax');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }

                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                                <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_accounts_tax_date_hmrc']) && $deadline['crm_accounts_tax_date_hmrc']!=''){ // 22-08-2018 
                                       echo 'Tax Return Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_accounts_tax_date_hmrc']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>
                                  </div>
                                   <div class="media-footer">    

                                 <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_company_tax"> <i class="fa fa-archive" aria-hidden="true"></i></a>                         
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_personal_due_date_return']==$date_comp && $deadline['crm_personal_due_date_return']!=''){

                              $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_persona_tax');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }

                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_personal_due_date_return']) && $deadline['crm_personal_due_date_return']!=''){ // 22-08-2018 
                                       echo 'Personal Tax Expires on'.$date=date('Y-m-d',strtotime($deadline['crm_personal_due_date_return']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a> 
                                  </div>
                                   <div class="media-footer">
                                        <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_persona_tax"> <i class="fa fa-archive" aria-hidden="true"></i></a>                                
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_vat_due_date']==$date_comp && $deadline['crm_vat_due_date']!=''){

                          $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_vat');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }

                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                             <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_vat_due_date']) && $deadline['crm_vat_due_date']!=''){ // 22-08-2018 
                                       echo 'Vat return Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_vat_due_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a> 
                                  </div>
                                   <div class="media-footer">
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_vat"> <i class="fa fa-archive" aria-hidden="true"></i></a>                           
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_rti_deadline']==$date_ver && $deadline['crm_rti_deadline']!=''){

                              $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_payroll');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }

                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_rti_deadline']) && $deadline['crm_rti_deadline']!=''){ // 22-08-2018 
                                       echo 'Payroll Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_rti_deadline']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>
                                  </div>
                                   <div class="media-footer">
                                        <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_payroll"> <i class="fa fa-archive" aria-hidden="true"></i></a>                                 
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_pension_subm_due_date']==$date_comp && $deadline['crm_pension_subm_due_date']!=''){

                              $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_workplace');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }

                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_pension_subm_due_date']) && $deadline['crm_pension_subm_due_date']!=''){ // 22-08-2018 
                                       echo 'Work Place-Pension Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_pension_subm_due_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>  
                                  </div>
                                   <div class="media-footer">
                                        <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_workplace"> <i class="fa fa-archive" aria-hidden="true"></i></a>  
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                            <?php  if($deadline['crm_next_p11d_return_due']==$date_comp && $deadline['crm_next_p11d_return_due']!=''){

                                $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_p11d');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }

                          ?>
                             <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                             <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php //echo $deadline['crm_company_name']; ?><?php 
                                    if(isset($deadline['crm_next_p11d_return_due']) && $deadline['crm_next_p11d_return_due']!=''){ // 22-08-2018 
                                       echo 'P11D Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_next_p11d_return_due']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>  
                                  </div>
                                   <div class="media-footer">       

                                       <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_p11d"> <i class="fa fa-archive" aria-hidden="true"></i></a>                        
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_next_manage_acc_date']==$date_comp && $deadline['crm_next_manage_acc_date']!=''){

                              $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_management');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }

                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                               <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php 
                                    if(isset($deadline['crm_next_manage_acc_date']) && $deadline['crm_next_manage_acc_date']!=''){ // 22-08-2018 
                                       echo 'Management Accounts Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_next_manage_acc_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a> 
                                  </div>
                                   <div class="media-footer">

                                        <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_management"> <i class="fa fa-archive" aria-hidden="true"></i></a>                               
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_next_booking_date']==$date_comp && $deadline['crm_next_booking_date']!=''){

                              $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_booking');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }

                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?> <?php echo $con; ?>>
                           <div class="media">
                           <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php 
                                    if(isset($deadline['crm_next_booking_date']) && $deadline['crm_next_booking_date']!=''){ // 22-08-2018 
                                      echo 'Book Keeping Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_next_booking_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>
                                  </div>
                                   <div class="media-footer">    

                                    <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_booking"> <i class="fa fa-archive" aria-hidden="true"></i></a>                                    
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_insurance_renew_date']==$date_comp && $deadline['crm_insurance_renew_date']!=''){
                              $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','data_insurance');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }

                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                             <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php 
                                    if(isset($deadline['crm_insurance_renew_date']) && $deadline['crm_insurance_renew_date']!=''){ // 22-08-2018 
                                       echo 'Investigation Insurance Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_insurance_renew_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>  
                                  </div>
                                   <div class="media-footer">    

                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="data_insurance"> <i class="fa fa-archive" aria-hidden="true"></i></a>                                  
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_registered_renew_date']==$date_comp && $deadline['crm_registered_renew_date']!=''){
                              $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_registered');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }

                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                               <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?></h5>
                                    <span class="notification-time"><?php 
                                    if(isset($deadline['crm_registered_renew_date']) && $deadline['crm_registered_renew_date']!=''){ // 22-08-2018 
                                      echo 'Registered Access Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_registered_renew_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>  
                                  </div>
                                   <div class="media-footer">
                                    <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_registered"> <i class="fa fa-archive" aria-hidden="true"></i></a>                                      
                                
                              </div>
                           </div>
                        </li>

                          <?php } ?>
                          <?php  if($deadline['crm_investigation_end_date']==$date_comp && $deadline['crm_investigation_end_date']!=''){

                              $stat=$this->Common_mdl->archive_check_details($deadline['id'],'deadline','deadline_investigation');
                          if($stat==0){
                            $con='style="display:none"';
                          }else{
                            $con='style="display:block"';
                          }
                          ?>
                           <li <?php $query=$this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Client/client_info/'.$deadline['user_id'];?>">
                                    <h5 class="notification-user"><?php echo $deadline['crm_company_name'].'-Service Notification'; ?> </h5>
                                    <span class="notification-time"><?php 
                                    if(isset($deadline['crm_investigation_end_date']) && $deadline['crm_investigation_end_date']!=''){ // 22-08-2018 
                                       echo 'Tax Advice/Investigation Expires on'. $date=date('Y-m-d',strtotime($deadline['crm_investigation_end_date']));
                                      // echo time_elapsed_string($date);
                                       }?></span>
                                 </a>    
                                  </div>
                                   <div class="media-footer">                           
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $deadline['id']?>" data-type="archive" data-section="deadline" data-user="deadline_investigation"> <i class="fa fa-archive" aria-hidden="true"></i></a>         
                              </div>
                           </div>
                        </li>
                         <?php } ?>
                         <?php } ?>
                        <!--  Invoice Send Statements  -->   
                        <?php
                           if($id = $this->session->userdata('admin_id'))
                           {
                            $useremail = $this->db->query('select crm_email_id,role from user where id='.$id.'')->row_array();
                            
                              if($useremail['role'] == '1')
                              {
                                $invoice_bill = $this->db->query('select * from Invoice_details where send_statements="send" and views=0')->result_array();
                                foreach ($invoice_bill as $invoice) { 
                                    $stat=$this->Common_mdl->archive_check($invoice['id'],'invoice');
                               
                                    if($stat==0){
                                      $con='style="display:none"';
                                    }

                                  ?>
                                    <li <?php $query=$this->Common_mdl->section_menu('invoice_notification',$_SESSION['id']);  
                                    if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                                       <div class="media">
                                          <div class="media-body">
                                             <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$invoice['client_id'];?>">
                                                <h5 class="notification-user">Invoice</h5>
                                                <span class="notification-time"><?php 
                                                if(isset($invoice['created_at'])&& $invoice['created_at']!='0000-00-00 00:00:00'){
                                                   $date=date('Y-m-d H:i:s',$invoice['created_at']);
                                                   echo time_elapsed_string($date);
                                                 }
                                                   ?></span>
                                             </a>
                                              </div>
                                   <div class="media-footer">
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $invoice['id']?>" data-type="archive" data-section="invoice"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $invoice['id']?>" id="remove-notification" data-type="remove" data-section='invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                                          </div>
                                       </div>
                                    </li>
                        <?php } ?>
                        <?php $rep_invoice = $this->db->query('select * from repeatInvoice_details where send_statements="send" and views=0')->result_array();
                           foreach ($rep_invoice as $repeat_invoice) {

                             $stat=$this->Common_mdl->archive_check($repeat_invoice['client_id'],'repeat-invoice');
                             if($stat==0){
                                      $con='style="display:none"';
                                    }
                              ?>
                        <li <?php $query=$this->Common_mdl->section_menu('invoice_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$repeat_invoice['client_id'];?>">
                                    <h5 class="notification-user">Invoice</h5>
                                    <span class="notification-time"><?php 
                                       $date=date('Y-m-d H:i:s',$repeat_invoice['created_at']);
                                       echo time_elapsed_string($date);?> <?php echo $stat; ?></span>
                                 </a> 
                                  </div>
                                   <div class="media-footer">
                                 <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $repeat_invoice['client_id']; ?>" data-type="archive" data-section="repeat-invoice"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                           <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $repeat_invoice['client_id']; ?>" id="remove-notification" data-type="remove" data-section='repeat-invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                              </div>
                           </div>
                        </li>
                        <?php } } 
                           if($useremail['crm_email_id'])
                           { 
                             $invoice_info = $this->db->query('select * from Invoice_details where client_email="'.$useremail['crm_email_id'].'" and send_statements="send" and views=0')->row_array(); 
                             if($invoice_info) { ?>
                        <li <?php $query=$this->Common_mdl->section_menu('invoice_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$invoice_info['client_id'];?>">
                                    <h5 class="notification-user">Invoice</h5>
                                    <span class="notification-time"><?php 
                                       $date=date('Y-m-d H:i:s',$invoice_info['created_at']);
                                       echo time_elapsed_string($date);?></span>
                                 </a>
                                  </div>
                                   <div class="media-footer">
                                   <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $invoice_info['id']?>" data-type="archive" data-section="invoice"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                           <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $invoice_info['id']?>" id="remove-notification" data-type="remove" data-section='invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                              </div>
                           </div>
                        </li>
                        <?php } 
                           $Repeatinvoice_info = $this->db->query('select * from repeatInvoice_details where client_email="'.$useremail['crm_email_id'].'" and send_statements="send" and views=0')->row_array(); 
                           if($Repeatinvoice_info) { ?>
                        <li <?php $query=$this->Common_mdl->section_menu('invoice_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$Repeatinvoice_info['client_id'];?>">
                                    <h5 class="notification-user">Invoice</h5>
                                    <span class="notification-time"><?php 
                                       $date=date('Y-m-d H:i:s',$Repeatinvoice_info['created_at']);
                                       echo time_elapsed_string($date);?></span>
                                 </a>
                                  </div>
                                   <div class="media-footer">
                                <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $Repeatinvoice_info['client_id']?>" data-type="archive" data-section="repeat-invoice"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $Repeatinvoice_info['client_id']?>" id="remove-notification" data-type="remove" data-section='repeat-invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                              </div>
                           </div>
                        </li>
                        <?php } }
                           } ?>
                     </ul>
                   </div>
                 </div>
                  <?php $result=$this->db->query('select * from user where id="'.$_SESSION['id'].'"')->result_array();?>
                  <li class="user-profile header-notification">
                     <a href="#!">
                     <?php   $getUserProfilepic = $this->Common_mdl->getUserProfilepic($_SESSION['id']);
                        ?>
                     <img src="<?php echo $getUserProfilepic;?>" alt="img">
                     <?php /*
                        if(isset($result['crm_profile_pic'])&&$result['crm_profile_pic']!=''){ ?>
                     <img src="<?php echo base_url();?>uploads/<?php echo $result['crm_profile_pic']?>" class="img-radius" alt="User-Profile-Image">
                     <?php } else { ?>
                     <img src="<?php echo base_url();?>assets/images/no-avatar.png" class="img-radius" alt="User-Profile-Image">
                     <?php } */?>
                     <span><?php 
                        if(isset($_SESSION['username'])&&$_SESSION['username']!=''){ echo $result[0]['username']; } ?> </span>
                     <i class="ti-angle-down"></i>
                     </a>
                     <ul class="tgl-opt">
                        <?php 
                        if($_SESSION['role']==1)
                        {
                          ?>
                        <li class="atleta">
                           <a href="<?php echo base_url();?>User/admin_profileSetting">
                           <i class="ti-layout-sidebar-left"></i> Admin Profile
                           </a> 
                        </li>
                        <?php } ?>
                        <li class="atleta">
                           <a href="<?php echo base_url();?>login/logout">
                           <i class="ti-layout-sidebar-left"></i> Logout
                           </a> 
                        </li>

                     </ul>
                  </li>
               </ul>
               <!-- search -->
               <div id="morphsearch" class="morphsearch">
                  <form class="morphsearch-form">
                     <input class="morphsearch-input" type="search" placeholder="Search..." />
                     <button class="morphsearch-submit" type="submit">Search</button>
                  </form>
                  <div class="morphsearch-content">
                     <div class="dummy-column">
                        <h2>People</h2>
                        <a class="dummy-media-object" href="#!">
                           <img src="<?php echo base_url();?>assets/images/avatar-1.jpg" alt="Sara Soueidan" />
                           <h3>Sara Soueidan</h3>
                        </a>
                        <a class="dummy-media-object" href="#!">
                           <img src="<?php echo base_url();?>assets/images/avatar-2.jpg" alt="Shaun Dona" />
                           <h3>Shaun Dona</h3>
                        </a>
                     </div>
                     <div class="dummy-column">
                        <h2>Popular</h2>
                        <a class="dummy-media-object" href="#!">
                           <img src="<?php echo base_url();?>assets/images/avatar-3.jpg" alt="PagePreloadingEffect" />
                           <h3>Page Preloading Effect</h3>
                        </a>
                        <a class="dummy-media-object" href="#!">
                           <img src="<?php echo base_url();?>assets/images/avatar-4.jpg" alt="DraggableDualViewSlideshow" />
                           <h3>Draggable Dual-View Slideshow</h3>
                        </a>
                     </div>
                     <div class="dummy-column">
                        <h2>Recent</h2>
                        <a class="dummy-media-object" href="#!">
                           <img src="<?php echo base_url();?>assets/images/avatar-5.jpg" alt="TooltipStylesInspiration" />
                           <h3>Tooltip Styles Inspiration</h3>
                        </a>
                        <a class="dummy-media-object" href="#!">
                           <img src="<?php echo base_url();?>assets/images/avatar-6.jpg" alt="NotificationStyles" />
                           <h3>Notification Styles Inspiration</h3>
                        </a>
                     </div>
                  </div>
                  <!-- /morphsearch-content -->
                  <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>
               </div>
               <!-- search end -->
            </div>
         </div>
      </nav>
      <div class="pcoded-main-container">
      <div class="pcoded-wrapper">
      <nav class="pcoded-navbar">
         <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
         <div class="pcoded-inner-navbar main-menu">
            <div class="">
               <div class="main-menu-header">
                  <img class="img-40 img-radius" src="<?php echo base_url();?>assets/images/avatar-4.jpg" alt="User-Profile-Image">
                  <div class="user-details">
                     <span><?php 
                        if(isset($_SESSION['username'])&&$_SESSION['username']!=''){ echo $_SESSION['username']; } ?></span>
                     <span id="more-details">UX Designer<i class="ti-angle-down"></i></span>
                  </div>
               </div>
               <div class="main-menu-content">
                  <ul>
                     <li class="more-details">
                        <!-- <a href="user-profile.html"><i class="ti-user"></i>View Profile</a>
                           <a href="#!"><i class="ti-settings"></i>Settings</a> -->
                        <a href="<?php echo base_url();?>login/logout"><i class="ti-layout-sidebar-left"></i>Logout</a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="pcoded-search">
               <span class="searchbar-toggle">  </span>
               <div class="pcoded-search-box ">
                  <input type="text" placeholder="Search">
                  <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
               </div>
            </div>
            <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation"></div>
            <ul class="pcoded-item pcoded-left-item">
               <li class="pcoded-hasmenu1">
                  <a href="<?php echo base_url().'user'?>">
                  <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                  <span class="pcoded-mtext">Dashboard</span>
                  </a>
               </li>
            </ul>
            <ul class="pcoded-item pcoded-left-item performance_list">
               <li class="pcoded-hasmenu3">
                  <a href="<?php echo base_url().'user/column_setting_new'?>">
                  <span class="pcoded-micon"><i class="fa fa-comment fa-6" aria-hidden="true"></i><b>c</b></span>
                  <span class="pcoded-mtext">cloumn setting</span>
                  </a>
               </li>
            </ul>
            <ul class="pcoded-item pcoded-left-item">
               <li class="pcoded-hasmenu pcoded-hasmenu4">
                  <a href="javascript:void(0)">
                  <span class="pcoded-micon"><i class="fa fa-plus fa-6" aria-hidden="true"></i><b>P</b></span>
                  <span class="pcoded-mtext">Clients</span>
                  </a>
                  <ul class="pcoded-submenu">
                     <li class="">
                        <a href="<?php echo base_url()?>client/add_client">
                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                        <span class="pcoded-mtext" data-i18n="nav.dash.default">Add clients</span>
                        <span class="pcoded-mcaret"></span>
                        </a>
                     </li>
                     <li class="">
                        <a href="<?php echo base_url()?>client/addnewclient">
                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                        <span class="pcoded-mtext" data-i18n="nav.dash.ecommerce">Add client from companyhouse</span>
                        <span class="pcoded-mcaret"></span>
                        </a>
                     </li>
                  </ul>
               </li>
            </ul>
            <ul class="pcoded-item pcoded-left-item performance_list">
               <li class="pcoded-hasmenu5">
                  <a href="<?php echo base_url()?>user/import_csv">
                  <span class="pcoded-micon"><i class="fa fa-user fa-6" aria-hidden="true"></i><b>P</b></span>
                  <span class="pcoded-mtext">import clients</span>
                  </a>
               </li>
            </ul>
         </div>
      </nav>
      <?php 
         function time_elapsed_string($datetime, $full = false) {
         $now = new DateTime;
         $ago = new DateTime($datetime);
         $diff = $now->diff($ago);
         
         $diff->w = floor($diff->d / 7);
         $diff->d -= $diff->w * 7;
         
         $string = array(
             'y' => 'year',
             'm' => 'month',
             'w' => 'week',
             'd' => 'day',
             'h' => 'hour',
             'i' => 'minute',
             's' => 'second',
         );
         foreach ($string as $k => &$v) {
             if ($diff->$k) {
                 $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
             } else {
                 unset($string[$k]);
             }
         }
         
         if (!$full) $string = array_slice($string, 0, 1);
         return $string ? implode(', ', $string) . ' ago' : 'just now';
         }
         ?>  
         <?php 
         $noti=$this->db->query("select * from user where autosave_status='1' order by id desc")->result_array();
      foreach($noti as $key => $val){?>
      <div id="modaldelete<?php echo $val['id']?>" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure want to delete?</p>
               </div>
               <div class="modal-footer">
                  <a href="<?php echo base_url().'user/delete/'.$val['id'];?>"><button type="button" class="btn btn-default">Delete</button></a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <?php }?>
      <?php if(isset($invoice_info['client_id'])){ ?>
      <div id="modalInvoicedelete<?php if(isset($invoice_info['client_id'])){ echo $invoice_info['client_id']; }?>" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure want to delete?</p>
               </div>
               <div class="modal-footer">
                  <a href="<?php echo base_url().'invoice/DeleteInvoice/'.$invoice_info['client_id'];?>"><button type="button" class="btn btn-default">Delete</button></a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <?php } ?>
      <?php if(isset($Repeatinvoice_info['client_id'])){ ?>
      <div id="modalrepeatInvoicedelete<?php if(isset($Repeatinvoice_info['client_id'])){ echo $Repeatinvoice_info['client_id']; }?>" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure want to delete?</p>
               </div>
               <div class="modal-footer">
                  <a href="<?php echo base_url().'invoice/DeleteRepeatInvoice/'.$Repeatinvoice_info['client_id'];?>"><button type="button" class="btn btn-default">Delete</button></a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <?php } ?>
      <?php if($useremail['role'] == '1') {                                    
         foreach ($invoice_bill as $invoice) { ?> 
      <div id="modalInvoiceBilldelete<?php echo $invoice['client_id']?>" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure want to delete?</p>
               </div>
               <div class="modal-footer">
                  <a href="<?php echo base_url().'invoice/DeleteInvoice/'.$invoice['client_id'];?>"><button type="button" class="btn btn-default">Delete</button></a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <?php }  ?>
      <?php foreach ($rep_invoice as $repeat_invoice) { ?>
      <div id="modalInvoiceRepBilldelete<?php echo $repeat_invoice['client_id']?>" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure want to delete?</p>
               </div>
               <div class="modal-footer">
                  <a href="<?php echo base_url().'invoice/DeleteRepeatInvoice/'.$repeat_invoice['client_id'];?>"><button type="button" class="btn btn-default">Delete</button></a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <?php } } ?>