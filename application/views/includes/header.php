<!DOCTYPE html>

<?php $uri = $this->uri->segment('2');
$isBasicTheme = (isset($theme) && $theme == 'basic') ? true : false;

if ($_SESSION['user_type'] != 'FC') {
   $this->Common_mdl->role_identify();
}

$Company_types = array();
$this->load->helper(['comman']);
$assigned_client = Get_Assigned_Datas('CLIENT');
$head_menu_url = 0;
$head_menus = $this->db->query(" SELECT * FROM `module_mangement`")->result_array();

if (count($assigned_client) > 0) {
   /*foreach ($assigned_client as $key => $value) {
      $rec = $this->Invoice_model->selectRecord('client', 'id', $value);
      
      if (!empty($rec['user_id'])) {
         $arr[] = $rec['user_id'];
      }
   }
   $clients_user_ids = (!empty($arr) ? implode(',', $arr) : '');*/
   
   $assigned_client_list = implode(',', $assigned_client);
   $rec                  = $this->Invoice_model->selectAllRecords('client', 'id', $assigned_client_list);
   $arr                  = [];
   
   foreach ($rec as $r) {
      if (!empty($r['user_id'])) {
         $arr[] = $r['user_id'];
      }
   }
   $clients_user_ids = (!empty($arr) ? implode(',', $arr) : '');
}

/*Coded By Ajith*/
$NOTFI_TEM = '<li class="open_tsk">
<div class="media">
<div class="media-body">
{content}
</div>
<div class="media-footer">
<a href="#notification_modal" data-toggle="modal" data-id="{id}" id="archive-notification" data-type="archive" data-section="{notification_type}" data-source="notification_management"><i class="fa fa-archive" aria-hidden="true"></i></a>
<a href="#notification_modal" data-toggle="modal" data-id="{id}" id="remove-notification" data-type="remove" data-section="{notification_type}" data-source="notification_management"><i class="fa fa-times" aria-hidden="true"></i></a>
</div>
</div>
</li>';
$companyhouse_notification = [];

if (!empty($assigned_client)) {
   $result = $this->Common_mdl->section_menu('companyhouse_notification', $_SESSION['id']);
   if ($result == '0') {
      $companyhouse_notification   = $this->Common_mdl->ReceiveCompanyHouseNotification($assigned_client);
   }
}
$companyhouse_notification_count = count($companyhouse_notification);
/**/
?>
<html lang="en">


<head>
   <title>CRM </title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <meta name="description" content="#">
   <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
   <meta name="author" content="#">
   <link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon">
   <link href="<?php echo base_url(); ?>assets/cdns/googleapis-css.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/bootstrap/css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icon/themify-icons/themify-icons.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/cdns/font-awesome.min.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/common_style.css?ver=5.13">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/common_responsive.css?ver=2.1">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/ui_style.css?ver=5.13">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/menu-search/css/component.css">
   <?php if (!$isBasicTheme) { ?>
      
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/j-pro/css/demo.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/j-pro/css/font-awesome.min.css">
      <link type="text/css" href="<?php echo base_url(); ?>bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/cdns/jquery-ui.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/owl.carousel/css/owl.carousel.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/css/dataTables.css">
      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"> -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/cdns/buttons.dataTables.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/css/jquery.dropdown.css">
      <link href="<?php echo base_url(); ?>assets/cdns/jquerysctipttop.css" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url(); ?>css/jquery.signaturepad.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/cdns/spectrum.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/datedropper/css/datedropper.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/switchery/css/switchery.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-colorpicker.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-slider.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/medium-editor/medium-editor.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/medium-editor/template.min.css">

      <!-- tinymce editor -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/cdns/codepen.min.css">
      <!-- tinymce editor -->

   <?php } ?>
   
   <link rel="stylesheet" type="text/css" href="<?php echo  base_url() ?>/assets/icon/simple-line-icons/css/simple-line-icons.css">
   <link rel="stylesheet" type="text/css" href="<?php echo  base_url() ?>/assets/icon/icofont/css/icofont.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/mobile_responsive.css?ver=2">
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/custom/opt-profile.css?<?php echo time(); ?>">
   <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">   -->
   <style>
      .common_form_section1 .col-xs-12.col-sm-6.col-md-6 {
         float: left !important;
      }

      .common_form_section1 {
         background: #fff;
         box-shadow: 1px 2px 5px #dbdbdb;
         border-radius: 5px;
         padding: 30px;
      }

      .common_form_section1 label {
         display: block;
         margin-bottom: 7px;
         font-size: 15px;
      }

      .common_form_section1 input {
         width: 100%;
      }

      .col-sm-12 {
         float: left;
      }

      .col-sm-8 {
         float: left;
      }

      #nmenuo .media-body * {
         color: #00a2e8;
         font-weight: bold;
      }

      #rmenut .media-body * {
         opacity: 0.5;
         font-weight: bold;
      }
   </style>
   <script type="text/javascript">
      var base_url = "<?php echo base_url() ?>";
   </script>

   <!-- <script>
      // Add active class to the current button (highlight it)
      var header = document.getElementById("sidebar-temp");
      var btns = header.getElementsByClassName("side-bar-list");
      for (var i = 0; i < btns.length; i++) {
         btns[i].addEventListener("click", function() {
         var current = document.getElementsByClassName("active");
         current[0].className = current[0].className.replace(" active", "");
         this.className += " active";
      });
      }
   </script> -->
   
</head>
<div class="LoadingImage">
   <div class="preloader3 loader-block">
      <div class="circ1 loader-primary loader-md"></div>
      <div class="circ2 loader-primary loader-md"></div>
      <div class="circ3 loader-primary loader-md"></div>
      <div class="circ4 loader-primary loader-md"></div>
   </div>
</div>
<?php if ($msg = $this->session->flashdata('server_response')) { ?>
   <div class="modal-alertsuccess alert succ dashboard_success_message">
      <div class="newupdate_alert">
         <a href="#" class="close" id="server_response_popup_close">×</a>
         <div class="pop-realted1">
            <div class="position-alert1">
               <?php echo $msg; ?>
            </div>
         </div>
      </div>
   </div>
<?php } ?>
<div id="action_result" class="modal-alertsuccess alert succ dashboard_success_message" style="display:none;">
   <div class="newupdate_alert">
      <a href="#" class="close" id="close_action_result">×</a>
      <div class="pop-realted1">
         <div class="position-alert1">
            Success !!! Staff Assign have been changed successfully...
         </div>
      </div>
   </div>
</div>

<div id="action_result" class="modal-alertsuccess alert succ proposal_success_message" style="display:none;">
   <div class="newupdate_alert">
      <a href="#" class="close" id="close_action_result">×</a>
      <div class="pop-realted1">
         <div class="position-alert1">
            Proposal Send Successfully!!!
         </div>
      </div>
   </div>
</div>
<style>
   .LoadingImage {
      display: none;
      position: fixed;
      z-index: 100;
      /*background-image : url('<?php echo site_url() ?>assets/images/ajax-loader.gif');*/
      background-color: rgba(65, 56, 57, 0.4);
      opacity: 1;
      background-repeat: no-repeat;
      background-position: center;
      left: 0;
      bottom: 0;
      right: 0;
      top: 0;
   }
</style>

<body>

   <?php
   $uri_seg_menu = $this->uri->segment('1');
   $uri_seg_menu1 = $this->uri->segment('2');
   $uri_seg_menu2 = $this->uri->segment('3');
   $active_class1 = '';
   $active_class2 = '';
   $active_class3 = '';
   $active_class4 = '';
   $active_class5 = '';
   $active_class6 = '';
   $active_class7 = '';
   $active_class8 = '';
   $active_class9 = '';
   $active_class10 = '';
   $active_class11 = '';
   $active_class12 = '';
   $active_class13 = '';
   $active_class14 = '';
   $active_class15 = '';
   $active_class20 = '';
   $active_class31 = '';
   $active_class32 = '';
   $active_class33 = '';
   $active_class34 = '';
   $active_class35 = '';
   $active_class36 = '';
   $client_list = '';
   $import_csv = '';
   $addnew = '';
   $addnew_cmp = '';
   $timeline = '';
   if ($uri_seg_menu == 'document') {
      $active_class9 = "head-active";
   }
   if ($uri_seg_menu == 'timeline') {
      $active_class5 = "head-active";
      $timeline = "head-active";
   }
   if ($uri_seg_menu1 == 'column_setting_new') {
      $active_class12 = "head-active";
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu1 == 'team_management') {
      $active_class13 = "head-active";
   }
   if ($uri_seg_menu1 == 'staff_custom_form') {
      $active_class14 = "head-active";
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu1 == 'user_profile') {
      $active_class15 = "head-active";
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu1 == 'lead_settings') {
      $active_class35 = "head-active";
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu1 == 'new_task') {
      $active_class20 = "head-active";
      $active_class2 = "head-active";
   }
   if ($uri_seg_menu1 == 'task_list') {
      $active_class15 = "head-active";
      $active_class2 = "head-active";
   }
   /** for chat **/
   if ($uri_seg_menu == 'user_chat') {
      $active_class36 = "head-active";
      $active_class8 = "head-active";
   }
   /** end of chat **/
   /** rspt 19-04-2018 leads heading **/
   if ($uri_seg_menu == 'leads' || $uri_seg_menu == 'web_to_lead') {
      // $active_class3 = "head-active";
      if (($uri_seg_menu1 == '' || $uri_seg_menu1 == 'kanban') && $uri_seg_menu == 'leads') {
         $active_class31 = "head-active";
         $active_class3 = "head-active";
      }
      if ($uri_seg_menu1 == 'web_leads_view') {
         $active_class32 = "head-active";
         $active_class33 = "head-active";
         $active_class3 = "head-active";
      }
      if ($uri_seg_menu == 'web_to_lead' && $uri_seg_menu1 == '') {
         $active_class32 = "head-active";
         $active_class34 = "head-active";
         $active_class3 = "head-active";
      }
      if ($uri_seg_menu1 == 'web_leads_update_contents') {
         $active_class32 = "head-active";
         $active_class3 = "head-active";
      }
      if ($uri_seg_menu1 == 'lead_settings') {
         //  $active_class35 = "head-active";
      }
      if ($uri_seg_menu1 == 'add_status' || $uri_seg_menu1 == 'add_source' || $uri_seg_menu1 == 'update_status' || $uri_seg_menu1 == 'update_source') {
         // $active_class35 = "head-active";
      }
   }
   /** for proposal **/
   if ($uri_seg_menu == 'proposal') {
      $active_class4 = "head-active";
   }
   /** end 19-04-2018 **/
   //echo $active_class2;die; 
   if ($uri_seg_menu == 'user' && $uri_seg_menu1 == '') {
      $active_class5 = "head-active";
      $client_list = "head-active";
   }
   if ($uri_seg_menu1 == 'addnewclient') {
      $active_class5 = "head-active";
      $addnew = "head-active";
   }
   if ($uri_seg_menu1 == 'import_csv') {
      $active_class5 = "head-active";
      $import_csv = "head-active";
   }
   if ($uri_seg_menu2 != '' && $uri_seg_menu1) {
      if ($uri_seg_menu == 'user') {
         $active_class5 = "head-active";
         $addnew_cmp = "head-active";
      }
   }
   /*if($uri_seg_menu == 'client' || $uri_seg_menu == 'Client'){
          $active_class5 = "head-active";
         } if($uri_seg_menu == 'user' && $uri_seg_menu1 == 'import_csv'){
          $active_class5 = "head-active";
         }*/
   if ($uri_seg_menu1 == 'admin_setting' || $uri_seg_menu1 == 'firm_field' || $uri_seg_menu1 == 'column_setting' || $uri_seg_menu1 == 'custom_fields') {
      $active_class8 = "head-active";
      if ($uri_seg_menu1 == 'admin_setting')
         $active_class10 = "head-active";
      if ($uri_seg_menu1 == 'firm_field')
         $active_class11 = "head-active";
   }
   if ($uri_seg_menu1 == 'team_management' || $uri_seg_menu1 == 'staff_list' || $uri_seg_menu == 'department' || $uri_seg_menu == 'team') {
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu == 'staff' && $uri_seg_menu1 == '') {
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu == 'staff' && $uri_seg_menu1 == 'update_staff_details') {
      $active_class1 = "head-active";
   }
   // 31/05/2018 add active class pradeep
   // My disk
   if ($uri_seg_menu == 'Firm_dashboard') {
      $active_class1 = "head-active";
   }
   if ($uri_seg_menu == 'staff' && $uri_seg_menu1 == 'Dashboard') {
      $active_class1 = "head-active";
   }
   // Tasks
   if (($uri_seg_menu1 == 'new_task' || $uri_seg_menu1 == 'task_list') && $uri_seg_menu == 'user') {
      $active_class2 = "head-active";
   }
   // CRM
   if ($uri_seg_menu == 'leads' || $uri_seg_menu == 'web_to_lead') {
      $active_class3 = "head-active";
   }
   if ($uri_seg_menu == 'web_to_lead' && $uri_seg_menu1 == 'web_leads_view') {
      $active_class3 = "head-active";
   }
   // proposal
   if ($uri_seg_menu == 'proposal') {
      $active_class4 = "head-active";
   }
   // clients
   if ($uri_seg_menu == 'user' || $uri_seg_menu == 'user#' || $uri_seg_menu == 'timeline') {
      $active_class5 = "head-active";
   }
   if ($uri_seg_menu == 'client' && $uri_seg_menu1 == 'addnewclient') {
      $active_class5 = "head-active";
   }
   if ($uri_seg_menu == 'user' && $uri_seg_menu1 == 'import_csv') {
      $active_class5 = "head-active";
   }
   //Deadline Manager
   if ($uri_seg_menu == 'Deadline_manager') {
      $active_class6 = "head-active";
   }
   //Reports
   if ($uri_seg_menu == 'reports') {
      $active_class7 = "head-active";
   }
   //settings
   if ($uri_seg_menu == 'user' && $uri_seg_menu1 == 'admin_setting') {
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu == 'staff' && $uri_seg_menu1 == 'staff_custom_form') {
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu == 'Tickets') {
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu == 'leads' && $uri_seg_menu1 == 'lead_settings') {
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu == 'user_chat') {
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu == 'user' && $uri_seg_menu1 == 'firm_field') {
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu == 'user' && $uri_seg_menu1 == 'column_setting_new') {
      $active_class8 = "head-active";
   }
   if ($uri_seg_menu == 'team' && $uri_seg_menu1 == 'team_management') {
      $active_class8 = "head-active";
   }
   // Documents
   if ($uri_seg_menu == 'documents') {
      $active_class9 = "head-active";
   }
   //Invoice
   if ($uri_seg_menu == 'Invoice') {
      $active_class10 = "head-active";
   }
   ?>

   <?php ?>
   <?php  ?>
   
   <!-- Pre-loader start -->
   <div class="theme-loader">
      <div class="ball-scale">
         <div class='contain'>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
         </div>
      </div>
   </div>

   <?php //} 
   ?>
   <!-- Pre-loader end -->
   <div id="pcoded" class="pcoded">
      <div class="pcoded-overlay-box"></div>
      <div class="pcoded-container navbar-wrapper">
         <nav class="navbar header-navbar pcoded-header">
            <div class="navbar-wrapper">
               <div class="navbar-logo">
                  <a class="mobile-menu" id="mobile-collapse" href="javascript:void(0);">
                     <i class="ti-menu"></i>
                  </a>
                  <a href="<?php echo base_url(); ?>">
                     <img class="img-fluid" src="<?php echo base_url(); ?>assets/images/logo_white.png" alt="Theme-Logo" />
                  </a>
                  <a class="mobile-options">
                     <i class="ti-more"></i>
                  </a>
               </div>
               <button id="togglesidebar">
                  <span class="toggle-bar"></span>
                  <span class="toggle-bar"></span>
                  <span class="toggle-bar"></span>
               </button>
               <div class="navbar-container container-fluid">
                  <div class="nav-left ">
                     <ul class='header-dynamic-cnt dynamic_header' id="owl-demo1">
                        <?php

                        $data['menus'] = $this->db->query('select * from menu_manage WHERE firm_id="' . $_SESSION['firm_id'] . '" ')->row_array();

                        if (empty($data['menus'])) {
                           $admin_data = $this->db->query('select `menu_details` from menu_manage WHERE firm_id="0" ')->row_array();

                           $admin_data['firm_id'] = $_SESSION['firm_id'];
                           $this->db->insert('menu_manage', $admin_data);
                        }

                        $PARENT_LI = "<li class='dropdown comclass'><a href='{URL}' class='dropdown-toggle'> <img src='{ICON_PATH}' class='menicon'>{MENU_LABEL} </a> {CHILD_CONTENT} </li>";

                        $CHILD_UL = "<ul class='dropdown-menu1'>{CHILD_CONTENT}</ul>";

                        $CHILD_LI = "<li class='column-setting0 atleta'><a href='{URL}'>{MENU_LABEL}</a> {CHILD_CONTENT}  </li>";

                        echo getHeader($PARENT_LI, $CHILD_UL, $CHILD_LI);

                        ?>

                        <?php

                        if ($_SESSION['user_type'] == 'FA' && $_SESSION['firm_id'] != '0') {

                           $plan = $this->Common_mdl->GetAllWithWhere('firm', 'firm_id', $_SESSION['firm_id']);

                           if (!empty($plan[0]['plan_details'])) {
                              $plan_details = json_decode($plan[0]['plan_details']);
                              $plan = $this->Common_mdl->GetAllWithWhere('subscription_plan', 'id', $plan_details->plan_id);

                              if ($plan[0]['unlimited'] == "" || $plan[0]['limited'] == "") {
                        ?>
                                 <li class='dropdown comclass'><a href='<?php echo base_url() . 'home/Add_Clients/' . $plan_details->plan_id . '/' . $_SESSION['firm_id'] . ''; ?>' class='dropdown-toggle' target='_blank'> <img src='<?php echo base_url(); ?>uploads/menu_icons/1551368496.jpg' class='menicon'>Upgrade</a></li>
                        <?php }
                           }
                        } ?>
                     </ul>
                  </div>

                  <ul class="nav-right">
                     <?php

                     $notification_query =  $this->db->query("select * from notification_management where  receiver_user_id = " . $_SESSION['id'] . " and status IN(1,2)")->result_array();
                     $query = $this->Common_mdl->section_menu('client_notification', $_SESSION['id']);
                     if ($query == '0') {
                        $noti = $this->db->query("select * from user where autosave_status='1' order by id desc")->result_array();
                        $count1 = count($noti);
                     } else {
                        $count1 = 0;
                     }

                     $query1 = $this->Common_mdl->section_menu('invoice_notification', $_SESSION['id']);
                     if ($query1 == '0' && !empty($clients_user_ids)) {
                        /*$invoice_noti = $this->db->query('SELECT * from Invoice_details where send_statements="send" AND client_email IN (' . $clients_user_ids . ') and views=0')->result_array();
                        $repeatinvoice_noti = $this->db->query('SELECT * from repeatInvoice_details where send_statements="send" AND client_email IN (' . $clients_user_ids . ') and views=0')->result_array();
                        $invoice_notify = $this->db->query('SELECT * from Invoice_details where send_statements="send" AND client_email IN (' . $clients_user_ids . ') and views = "1"')->result_array();
                        $repeatinvoice_notify = $this->db->query('SELECT * from repeatInvoice_details where send_statements="send" AND client_email IN (' . $clients_user_ids . ') and views = "1"')->result_array();*/

                        $invNoti              = $this->db->query('SELECT * from Invoice_details where send_statements="send" AND client_email IN (' . $clients_user_ids . ') and views IN(0,1)')->result_array();
                        $invRep               = $this->db->query('SELECT * from repeatInvoice_details where send_statements="send" AND client_email IN (' . $clients_user_ids . ') and views IN(0,1)')->result_array();
                        $invoice_noti         = []; 
                        $invoice_notify       = [];
                        $repeatinvoice_noti   = [];
                        $repeatinvoice_notify = [];

                        foreach($invNoti as $inv){
                           if($inv['views'] == 0){
                              $invoice_noti[] = $inv;
                           }else if($inv['views'] == 1){
                              $invoice_notify = $inv;
                           }
                        }

                        foreach($invRep as $inv){
                           if($inv['views'] == 0){
                              $repeatinvoice_noti[] = $inv;
                           }else if($inv['views'] == 1){
                              $repeatinvoice_notify = $inv;
                           }
                        }

                        $count2 = count($invoice_noti) + count($invoice_notify);
                        $count3 = count($repeatinvoice_noti) + count($repeatinvoice_notify);
                     } else {
                        $count2 = 0;
                        $count3 = 0;
                     } ?>
                     <li class="header-notification">
                        <a href="#">
                           <i class="ti-bell"></i>
                           <span class="badge bg-c-pink sectionsss">
                              <?php echo $count1 + $count2 + $count3 + count($notification_query) + $companyhouse_notification_count; ?>
                           </span>
                        </a>
                        <div class="show-notification  header_notify">
                        </div>

                        <?php $result = $this->db->query('select * from user where id="' . $_SESSION['id'] . '"')->row_array(); ?>
                     <li class="user-profile header-notification">
                        <a href="#!">
                           <?php $getUserProfilepic = $this->Common_mdl->getUserProfilepic($_SESSION['id']); ?>
                           
                           <img src="<?php echo $getUserProfilepic; ?>" alt="img">

                           <span><?php
                                 if (isset($_SESSION['crm_name']) && $_SESSION['crm_name'] != '') {
                                    echo $result['crm_name'];
                                 } ?> </span>
                           <i class="ti-angle-down"></i>
                        </a>
                        <ul class="tgl-opt">
                           <li class="atleta">
                              <a href="<?php echo base_url(); ?>staff/user_profile">
                                 <i class="ti-layout-sidebar-left"></i> MY Profile
                              </a>
                           </li>
                           <?php
                           if (isset($_SESSION['SA_TO_FA']) && $_SESSION['SA_TO_FA'] == 1) {
                           ?>
                              <li class="atleta">
                                 <a href="<?php echo base_url(); ?>Super_admin/Switch_to_SuperAdmin">
                                    <i class="ti-layout-sidebar-left"></i> Switch To Super Admin
                                 </a>
                              </li>
                           <?php } else { ?>
                              <li class="atleta">
                                 <a href="<?php echo base_url(); ?>login/logout">
                                    <i class="ti-layout-sidebar-left"></i> Logout
                                 </a>
                              </li>
                           <?php } ?>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>
         </nav>

         <div class="modal-alertsuccess alert succ header_popup_info_msg" style="display:none;">
            <div class="header_newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
               <div class="header_pop-realted1">
                  <div class="header_position-alert1">
                     Please! Select Record...
                  </div>
               </div>
            </div>
         </div>

         <div class="pcoded-main-container">
            <div class="wrapper sidemenu-wrapper">
               <!-- Sidebar -->
               <nav id="sidebar">
                  <ul class="list-unstyled components" id="sidebar-temp">
                     <li class="side-bar-list"><a href="<?php echo base_url(); ?>firm_dashboard"><i class="fa fa-dashboard"></i> My Desk</a></li>
                     <li class="side-bar-list active">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> <i class="fa fa-tasks" aria-hidden="true"></i> Tasks</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                           <li><a href="<?php echo base_url(); ?>user/new_task"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Create a Task</a></li>
                           <li><a href="<?php echo base_url(); ?>user/task_list"><i class="fa fa-angle-right"></i> All Task</a></li>
                           <!-- <li><a href="<?php echo base_url(); ?>Task"><i class="fa fa-angle-right"></i> Tasks Timeline</a></li> -->
                           <li><a href="<?php echo base_url(); ?>Task_Status/task_progress_view"><i class="fa fa-angle-right"></i> Task Progress Status</a></li>
                           <li><a href="<?php echo base_url(); ?>user/task_list_kanban"><i class="fa fa-angle-right"></i> Switch To Kanban</a></li>
                           <li><a href="<?php echo base_url(); ?>user/task_list/archive_task"><i class="fa fa-angle-right"></i> Archive Task</a></li>
                           <li><a href="<?php echo base_url(); ?>user/task_list/complete_task"><i class="fa fa-angle-right"></i> Completed Task</a></li>
                        </ul>
                     </li>
                     <li class="side-bar-list">
                        <a href="#leadsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-list-alt"></i> Leads</a>
                        <ul class="collapse list-unstyled" id="leadsSubmenu">
                           <li><a href="<?php echo base_url(); ?>leads#tab-new_lead"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Create a Lead</a></li>
                           <li><a href="<?php echo base_url(); ?>leads"><i class="fa fa-angle-right"></i> Manage Leads</a></li>
                           <li><a href="<?php echo base_url(); ?>web_to_lead/web_leads_view"><i class="fa fa-angle-right"></i> Web to Lead</a></li>
                        </ul>
                     </li>
                     <li class="side-bar-list">
                        <a href="#proposalSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-file-text"></i> Proposal</a>
                        <ul class="collapse list-unstyled" id="proposalSubmenu">
                           <li><a href="<?php echo base_url(); ?>proposal_page/step_proposal"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Create Proposal</a></li>
                           <li><a href="<?php echo base_url(); ?>proposal"><i class="fa fa-angle-right"></i> Proposal Dashboard</a></li>
                           <li><a href="<?php echo base_url(); ?>Proposal/fee_schedule"><i class="fa fa-angle-right"></i> Package</a></li>
                           <li><a href="<?php echo base_url(); ?>proposal_page/templates"><i class="fa fa-angle-right"></i> Templates</a></li>
                           <li><a href="<?php echo base_url(); ?>proposals/pricelist"><i class="fa fa-cog"></i> Settings</a></li>
                           <li><a href="<?php echo base_url(); ?>proposals/history"><i class="fa fa-history" aria-hidden="true"></i> History</a></li>
                        </ul>
                     </li>
                     <li class="side-bar-list">
                        <a href="#clientsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-user-plus" aria-hidden="true"></i> Clients</a>
                        <ul class="collapse list-unstyled" id="clientsSubmenu">
                           <li><a href="<?php echo base_url(); ?>user"><i class="fa fa-angle-right"></i> Clients List</a></li>
                           <li><a href="#default-Modal" data-toggle="modal"><i class="fa fa-plus" aria-hidden="true"></i> Add from Companies House</a></li>
                           <li><a href="<?php echo base_url(); ?>client/addnewclient"><i class="fa fa-plus" aria-hidden="true"></i> Add a Client - Manually</a></li>
                           <li><a href="<?php echo base_url(); ?>user/import_csv"><i class="fa fa-angle-right"></i> Import Client</a></li>
                           <!-- <li><a href="<?php echo base_url(); ?>timeline"><i class="fa fa-angle-right"></i> Services Timeline</a></li> -->
                        </ul>
                     </li>
                     <li class="side-bar-list">
                        <a href="#toolsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-cogs"></i> Tools</a>
                        <ul class="collapse list-unstyled" id="toolsSubmenu">
                           <li><a href="<?php echo base_url(); ?>Documents"><i class="fa fa-file-text-o" aria-hidden="true"></i> Documents</a></li>
                           <li><a href="<?php echo base_url(); ?>reports"><i class="fa fa-angle-right"></i> Reports</a></li>
                           <li><a href="<?php echo base_url(); ?>Deadline_manager/"><i class="fa fa-angle-right"></i> Deadline Manager</a></li>
                           <li><a href="<?php echo base_url(); ?>Tickets/index"><i class="fa fa-ticket" aria-hidden="true"></i> Tickets</a></li>
                           <li><a href="<?php echo base_url(); ?>user_chat"><i class="fa fa-comments-o" aria-hidden="true"></i> Chats</a></li>
                           <li><a href="#popup-2" data-toggle="modal"><i class="fa fa-angle-right"></i> Give us Feedback</a></li>
                        </ul>
                     </li>
                     <li class="side-bar-list">
                        <a href="#settingsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-cog"></i> Settings</a>
                        <ul class="collapse list-unstyled" id="settingsSubmenu">
                           <li><a href="<?php echo base_url(); ?>service"><i class="fa fa-angle-right"></i> Services &amp; Workflows</a></li>
                           <li class="">
                              <a href="#teamSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-users"></i> Team Management</a>
                              <ul class="collapse list-unstyled" id="teamSubmenu">
                                 <li><a href="<?php echo base_url(); ?>Role_Assign"><i class="fa fa-angle-right"></i> Assign Role</a></li>
                                 <li><a href="<?php echo base_url(); ?>user/staff_list"><i class="fa fa-angle-right"></i> Staff List</a></li>
                                 <li><a href="<?php echo base_url(); ?>team/organisation_tree"><i class="fa fa-angle-right"></i> Organisation Tree</a></li>
                              </ul>
                           </li><?php
                                 if ($_SESSION['user_type'] == 'FA' || $_SESSION['user_type'] == 'SA') { ?>
                              <li><a href="<?php echo base_url(); ?>InternalDeadlines"><i class="fa fa-angle-right"></i> Internal &amp; Deadlines</a></li><?php
                                                                                                                                                         } ?>
                           <li><a href="<?php echo base_url(); ?>Email_template"><i class="fa fa-angle-right"></i> Email Templates</a></li>
                           <li><a href="<?php echo base_url(); ?>user/Service_reminder_settings"><i class="fa fa-angle-right"></i> Reminders Setting</a></li>
                           <li class="">
                              <a href="#advancedSettingSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-wrench"></i> Advance Settings</a>
                              <ul class="collapse list-unstyled" id="advancedSettingSubmenu">
                                 <li><a href="<?php echo base_url(); ?>user/notification"><i class="fa fa-angle-right"></i> Notifications Settings</a></li>
                                 <li class="">
                                    <a href="#addBoxSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-plus"></i> Add a Box</a>
                                    <ul class="collapse list-unstyled" id="addBoxSubmenu">
                                       <li><a href="<?php echo base_url(); ?>user/firm_field_check"><i class="fa fa-angle-right"></i> Check Form Field</a></li>
                                       <li><a href="<?php echo base_url(); ?>staff/staff_custom_form"><i class="fa fa-angle-right"></i> Staff Custom Field</a></li>
                                    </ul>
                                 </li>
                                 <li><a href="<?php echo base_url(); ?>user/admin_setting"><i class="fa fa-angle-right"></i> Firm Admin Settings</a></li>
                                 <li><a href="<?php echo base_url(); ?>staff/user_profile/"><i class="fa fa-angle-right"></i> My Profile</a></li>
                              </ul>
                           </li>
                        </ul>
                     </li>
                     <li class="side-bar-list">
                        <a href="#invoiceSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-files-o" aria-hidden="true"></i> Invoices</a>
                        <ul class="collapse list-unstyled" id="invoiceSubmenu">
                           <li><a href="<?php echo base_url(); ?>invoice/NewInvoice"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Create an Invoice</a></li>
                           <li><a href="<?php echo base_url(); ?>invoice/RepeatInvoice"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Create a Repeating Invoice</a></li>
                           <li><a href="<?php echo base_url(); ?>Invoice"><i class="fa fa-file-o"></i> Invoices</a></li>
                           <li><a href="<?php echo base_url(); ?>invoice/GeneralSettings"><i class="fa fa-cog"></i> Invoice General Settings</a></li>
                        </ul>
                     </li>
                     <li class="side-bar-list"><a href="<?php echo base_url(); ?>LiveTeam"><i class="fa fa-users"></i> Live Team</a></li>
                     <li class="side-bar-list"><a href="<?php echo base_url(); ?>EmailLogs"><i class="fa fa-envelope-o" aria-hidden="true"></i> Email Logs</a></li>
                     <li class="side-bar-list"><a href="<?php echo base_url(); ?>EmailLogs/QueuedReminders"><i class="fa fa-bell-o" aria-hidden="true"></i> Queued Reminders</a></li>
                     <li class="side-bar-list"><a href="<?php echo base_url(); ?>home/Add_Clients/11/258"><i class="fa fa-line-chart"></i> Upgrade</a></li>
                  </ul>
               </nav>
               <!-- Page Content -->
            </div>
            <div class="pcoded-wrapper">
               <nav class="pcoded-navbar">
                  <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                  <div class="pcoded-inner-navbar main-menu">
                     <div class="">
                        <div class="main-menu-header">
                           <img class="img-40 img-radius" src="<?php echo base_url(); ?>assets/images/avatar-4.jpg" alt="User-Profile-Image">
                           <div class="user-details">
                              <span><?php
                                    if (isset($_SESSION['username']) && $_SESSION['username'] != '') {
                                       echo $_SESSION['username'];
                                    } ?></span>
                              <span id="more-details">UX Designer<i class="ti-angle-down"></i></span>
                           </div>
                        </div>
                        <div class="main-menu-content">
                           <ul>
                              <li class="more-details">
                                 <!-- <a href="user-profile.html"><i class="ti-user"></i>View Profile</a>
                           <a href="#!"><i class="ti-settings"></i>Settings</a> -->
                                 <a href="<?php echo base_url(); ?>login/logout"><i class="ti-layout-sidebar-left"></i>Logout</a>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="pcoded-search">
                        <span class="searchbar-toggle"> </span>
                        <div class="pcoded-search-box ">
                           <input type="text" placeholder="Search">
                           <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
                        </div>
                     </div>
                     <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation"></div>
                     <ul class="pcoded-item pcoded-left-item">
                        <li class="pcoded-hasmenu1">
                           <a href="<?php echo base_url() . 'user' ?>">
                              <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                              <span class="pcoded-mtext">Dashboard</span>
                           </a>
                        </li>
                     </ul>
                     <ul class="pcoded-item pcoded-left-item performance_list">
                        <li class="pcoded-hasmenu3">
                           <a href="<?php echo base_url() . 'user/column_setting_new' ?>">
                              <span class="pcoded-micon"><i class="fa fa-comment fa-6" aria-hidden="true"></i><b>c</b></span>
                              <span class="pcoded-mtext">column setting</span>
                           </a>
                        </li>
                     </ul>
                     <ul class="pcoded-item pcoded-left-item">
                        <li class="pcoded-hasmenu pcoded-hasmenu4">
                           <a href="javascript:void(0)">
                              <span class="pcoded-micon"><i class="fa fa-plus fa-6" aria-hidden="true"></i><b>P</b></span>
                              <span class="pcoded-mtext">Clients</span>
                           </a>
                           <ul class="pcoded-submenu">
                              <li class="">
                                 <a href="<?php echo base_url() ?>client/add_client">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext" data-i18n="nav.dash.default">Add clients</span>
                                    <span class="pcoded-mcaret"></span>
                                 </a>
                              </li>
                              <li class="">
                                 <a href="<?php echo base_url() ?>client/addnewclient">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext" data-i18n="nav.dash.ecommerce">Add client from companyhouse</span>
                                    <span class="pcoded-mcaret"></span>
                                 </a>
                              </li>
                           </ul>
                        </li>
                     </ul>
                     <ul class="pcoded-item pcoded-left-item performance_list">
                        <li class="pcoded-hasmenu5">
                           <a href="<?php echo base_url() ?>user/import_csv">
                              <span class="pcoded-micon"><i class="fa fa-user fa-6" aria-hidden="true"></i><b>P</b></span>
                              <span class="pcoded-mtext">import clients</span>
                           </a>
                        </li>
                     </ul>
                  </div>
               </nav>
               <?php
               function time_elapsed_string($datetime, $full = false)
               {
                  $now = new DateTime;
                  $ago = new DateTime($datetime);
                  $diff = $now->diff($ago);

                  $diff->w = floor($diff->d / 7);
                  $diff->d -= $diff->w * 7;

                  $string = array(
                     'y' => 'year',
                     'm' => 'month',
                     'w' => 'week',
                     'd' => 'day',
                     'h' => 'hour',
                     'i' => 'minute',
                     's' => 'second',
                  );
                  foreach ($string as $k => &$v) {
                     if ($diff->$k) {
                        $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                     } else {
                        unset($string[$k]);
                     }
                  }

                  if (!$full) $string = array_slice($string, 0, 1);
                  return $string ? implode(', ', $string) . ' ago' : 'just now';
               }
               ?>
               <?php
               $noti = $this->db->query("select * from user where autosave_status='1' order by id desc")->result_array();
               foreach ($noti as $key => $val) { ?>
                  <div id="modaldelete<?php echo $val['id'] ?>" class="modal fade" role="dialog">
                     <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                           <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
                           </div>
                           <div class="modal-body">
                              <p>Are you sure want to delete?</p>
                           </div>
                           <div class="modal-footer">
                              <a href="<?php echo base_url() . 'user/delete/' . $val['id']; ?>"><button type="button" class="btn btn-default">Delete</button></a>
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                           </div>
                        </div>
                     </div>
                  </div>
               <?php } ?>
               <?php if (isset($invoice_info['client_id'])) { ?>
                  <div id="modalInvoicedelete<?php if (isset($invoice_info['client_id'])) {
                                                echo $invoice_info['client_id'];
                                             } ?>" class="modal fade" role="dialog">
                     <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                           <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
                           </div>
                           <div class="modal-body">
                              <p>Are you sure want to delete?</p>
                           </div>
                           <div class="modal-footer">
                              <a href="<?php echo base_url() . 'invoice/DeleteInvoice/' . $invoice_info['client_id']; ?>"><button type="button" class="btn btn-default">Delete</button></a>
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                           </div>
                        </div>
                     </div>
                  </div>
               <?php } ?>
               <?php if (isset($Repeatinvoice_info['client_id'])) { ?>
                  <div id="modalrepeatInvoicedelete<?php if (isset($Repeatinvoice_info['client_id'])) {
                                                      echo $Repeatinvoice_info['client_id'];
                                                   } ?>" class="modal fade" role="dialog">
                     <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                           <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
                           </div>
                           <div class="modal-body">
                              <p>Are you sure want to delete?</p>
                           </div>
                           <div class="modal-footer">
                              <a href="<?php echo base_url() . 'invoice/DeleteRepeatInvoice/' . $Repeatinvoice_info['client_id']; ?>"><button type="button" class="btn btn-default">Delete</button></a>
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                           </div>
                        </div>
                     </div>
                  </div>
               <?php } ?>
               <?php //if($useremail['role'] == '1') { 
               if (isset($invoice_noti) && count($invoice_noti) > 0) {
                  foreach ($invoice_noti as $invoice) { ?>
                     <div id="modalInvoiceBilldelete<?php echo $invoice['client_id'] ?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                           <!-- Modal content-->
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
                              </div>
                              <div class="modal-body">
                                 <p>Are you sure want to delete?</p>
                              </div>
                              <div class="modal-footer">
                                 <a href="<?php echo base_url() . 'invoice/DeleteInvoice/' . $invoice['client_id']; ?>"><button type="button" class="btn btn-default">Delete</button></a>
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  <?php }
               }

               if (isset($invoice_notify) && count($invoice_notify) > 0) {
                  foreach ($invoice_notify as $invoice) { ?>
                     <div id="modalInvoiceBilldelete<?php echo $invoice['client_id'] ?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                           <!-- Modal content-->
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
                              </div>
                              <div class="modal-body">
                                 <p>Are you sure want to delete?</p>
                              </div>
                              <div class="modal-footer">
                                 <a href="<?php echo base_url() . 'invoice/DeleteInvoice/' . $invoice['client_id']; ?>"><button type="button" class="btn btn-default">Delete</button></a>
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                           </div>
                        </div>
                     </div>
               <?php  }
               }  ?>



               <?php if (isset($repeatinvoice_noti) && count($repeatinvoice_noti) > 0) {
                  foreach ($repeatinvoice_noti as $repeat_invoice) { ?>
                     <div id="modalInvoiceRepBilldelete<?php echo $repeat_invoice['client_id'] ?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                           <!-- Modal content-->
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
                              </div>
                              <div class="modal-body">
                                 <p>Are you sure want to delete?</p>
                              </div>
                              <div class="modal-footer">
                                 <a href="<?php echo base_url() . 'invoice/DeleteRepeatInvoice/' . $repeat_invoice['client_id']; ?>"><button type="button" class="btn btn-default">Delete</button></a>
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  <?php }
               }

               if (isset($repeatinvoice_notify) && count($repeatinvoice_notify) > 0) {
                  foreach ($repeatinvoice_notify as $repeat_invoice) { ?>
                     <div id="modalInvoiceRepBilldelete<?php echo $repeat_invoice['client_id'] ?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                           <!-- Modal content-->
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
                              </div>
                              <div class="modal-body">
                                 <p>Are you sure want to delete?</p>
                              </div>
                              <div class="modal-footer">
                                 <a href="<?php echo base_url() . 'invoice/DeleteRepeatInvoice/' . $repeat_invoice['client_id']; ?>"><button type="button" class="btn btn-default">Delete</button></a>
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                           </div>
                        </div>
                     </div>
               <?php   }
               }    ?>