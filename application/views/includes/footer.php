
  <?php $fun = $this->uri->segment(2);?>

</style>
<div class="modal fade" id="manager_service_accept" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input type="hidden" name="services_name" id="services_name" value="">
         <input type="hidden" name="service_user_id" id="service_user_id" value="">
         <input type="hidden" name="notifications_id" id="notifications_id" value="">
          <p>Do you want to accept Service Request?.</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="accept_service">Yes</button>
         <button type="button" class="btn btn-primary" id="send_proposal">Send Proposal</button>
          <button type="button" class="btn btn-default close_popup" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>
  <div class="modal fade" id="notification_modal" role="dialog">
    <div class="modal-dialog modal-notify">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">  
          <input type="hidden" name="notification_id" id="notification_id" value="">
          <input type="hidden" name="notification_type" id="notification_type" value="">
          <input type="hidden" name="notification_section" id="notification_section" value="">
          <input type="hidden" name="notification_users" id="notification_users" value="">
          <input type="hidden" name="SorceFrom" id="SorceFrom" value="">

          <p id="show_message">Do you want to accept Service Request?.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="confirm_button">Yes</button>
          <button type="button" class="btn btn-default close_popup" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>



  <div id="review_task" class="modal fade" role="dialog">
<div class="modal-dialog">
 <!-- Modal content-->
 <div class="modal-content">
    <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h4 class="modal-title" style="text-align-last: center">Task</h4>
    </div>
    <div class="modal-body">
          <p class="task_info">Task Review by </p>


          <input type="hidden" name="review_task_id" id="review_task_id">
          <input type="hidden" name="review_notification_type" id="review_notification_type">
          <input type="hidden" name="review_notification_users" id="review_notification_users">
          <input type="hidden" name="review_notification_section" id="review_notification_section">
          <input type="hidden" name="review_notification_id" id="review_notification_id">
          <input type="hidden" name="review_notification_source" id="review_notification_source">
       </div>
    </div>
    <div class="modal-footer profileEdit">
        <button type="button" class="btn btn-primary" id="review_accept_btn">Accept</button>
         <button type="button" class="btn btn-primary" id="send_review_btn" style="<?php if($_SESSION['user_type']=='FA'){ echo 'display:none'; } ?>">Send Higher</button>
          <button type="button" class="btn btn-default close_popup" data-dismiss="modal">Close</button>
    </div>
 </div>
</div>




<div class="modal fade" id="manager_task_review" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>

        <div class="modal-body">
        <input type="hidden" id="review_notification_id">

         <p>Do you want to accept Service Request?.</p>
         <br>
         <b>Notes</b>
         <textarea id="review_response_notes"></textarea>
        </div>

        <div class="modal-footer" id="model_footer_button">
          <button type="button" class="btn btn-primary"  onclick="review_response('1')">Yes</button>
          <button type="button" class="btn btn-default" onclick="review_response('0')">No</button>
          <!-- <button type="button" class="btn btn-default close_popup" data-dismiss="modal" onclick="review_response(1)">No</button> -->
        </div>

      </div>
      
    </div>
  </div>


<div class="modal fade" id="popup-2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Feedback Form</h4>
        </div>

        <div class="modal-body">
      <form action="<?php echo base_url();?>User/feedMail" id="feed_form" method="post" name="feed_form">
         <style>
            input.error {
            border: 1px dotted red;
            }
            label.error{
            width: 100%;
            color: red;
            font-style: italic;
            margin-left: 120px;
            margin-bottom: 5px;
            }
         </style>
  
         <label><b>Your content</b></label>
         <textarea name="feed_msg" id="feed_msg" placeholder="Type your text here..."></textarea>
         <!-- <br><br> -->  

         <div class="modal-footer">
          <div class="feedback-submit mail_bts1">
            <div class="feed-submit text-left">
               <input id="feed" name="submit" type="submit" value="submit">
            </div>
         </div>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>      
  
      </form>
    </div>
        
      </div>
      
    </div>
  </div>



  <div id="Confirmation_popup" class="modal fade" role="dialog" style="display:none;">
      <div class="modal-dialog modal-dialog-confirm">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header modal-header-confirm">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title" style="text-align-last: center">Confirmation</h4>
          </div>
          <div class="modal-body modal-body-confirm">
             <label class="information"></label>
          </div>
          <div class="modal-footer modal-footer-confirm">             
              <button class="Confirmation_OkButton">Save</button>
              <button class="Confirmation_CancelButton" data-dismiss="modal">No</button>              
          </div>
        </div>
    </div>
  </div>



      <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields">
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script> 
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script> 
      <script src="<?php echo base_url();?>assets/cdns/jquery-ui.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/cdns/moment.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/cdns/jquery.validate.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>  
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script> 
     
      <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
      <!-- jquery slimscroll js -->
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
      <!-- modernizr js -->
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
      <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/cdns/jquery.dataTables.min.js"></script> 

      <script type="text/javascript" src="<?php echo base_url();?>assets/js/datedropper.min.js"></script>
      
      <!-- Custom js -->

      <script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script> 
      <script>

        function getCookie(name) {
            var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
            return v ? v[2] : null;
        }
        function setCookie(name, value, days) {
            var d = new Date;
            d.setTime(d.getTime() + 24*60*60*1000*days);
            document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
        }      

        $(document).ready(function() {
          //Close Side Bar on load
          
          var issidebaractive = getCookie('sideactive');
          console.log(issidebaractive+"isactive");
          
          
          
          if (issidebaractive == 'true'){
            console.log("activefound");
            $("#sidebar").addClass("active");
            $('.pcoded-content').addClass('push-left');
          }
          
          $("#togglesidebar").click(function(){
            $("#sidebar").toggleClass("active");
            if ( $("#sidebar").hasClass("active") ){
              setCookie('sideactive', 'true', 365);
            } else {
              setCookie('sideactive', 'false', 365);
            }
            $('.pcoded-content').toggleClass('push-left');
          });
          
          $("#sidebar").removeClass("active");
          $('.pcoded-content').removeClass('push-left');
            // $('#sidebarCollapse').on('click', function() {
            //   // open or close navbar
            //   $('#sidebar').toggleClass('active');
            //   // open or close navbar
            //   $('.pcoded-content').toggleClass('push-left');
            //   // close dropdowns
            //   $('.collapse.in').toggleClass('in');
            //   // and also adjust aria-expanded attributes we use for the open/closed arrows
            //   // in our CSS
            //   $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            // });

        });
      </script>
      <script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
      <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/masonry.pkgd.min.js"></script> -->
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script> 
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  
      <script src="<?php echo base_url();?>assets/js/mock.js"></script>
      <script src="<?php echo base_url();?>assets/js/hierarchy-select.min.js"></script>

  
    <?php $url = $this->uri->segment('2'); ?>
    <?php if ($url == 'step_proposal' || $url == 'copy_proposal' || $url == 'template_design' || $url=='templates') { ?>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <?php } ?>
    <script src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/debounce.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-slider.min.js"></script>
    <script src="<?php echo base_url();?>assets/cdns/spectrum.min.js"></script>
    <script src="<?php echo base_url();?>assets/cdns/medium-editor.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/cookie/jquery.cookie.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/creative.tools.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/html2canvas.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/image-edit.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/editor.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css"><script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>

    <!-- <script src="https://cdn.ckeditor.com/4.11.1/standard-all/ckeditor.js"></script> -->

  <!-- for new timer -->  
  <script type="text/javascript">

    $(document).ready(function()
    {

      

      <?php if(isset($_SESSION['timer']) && !empty($_SESSION['timer'])){ ?>
    
          var exist_seconds = '<?php echo $_SESSION['totalSeconds']; ?>';
          if(exist_seconds == '' || exist_seconds == null)
          {
             exist_seconds = 0;
          }

          var timer_start = new Date('<?php echo $_SESSION['timer']; ?>'*1000).toLocaleString("en-UK", {timeZone: "<?php echo timezone; ?>"}); 
          var timezone = new Date().toLocaleString("en-UK", {timeZone: "<?php echo timezone; ?>"});
          var diff = new Date(timezone).getTime() - new Date(timer_start).getTime(); 
         
          var seconds = Math.abs(diff/1000);  
          window.totalSeconds = Number(seconds)+Number(exist_seconds);
           
          $('#headertask_timer').css('display','block');
          window.intervalid = setInterval(function(){ getTimer(); },1000);

      <?php } ?>
      $(document).on('mouseover','#headertask_timer',function()
      { 
        $('#headertask_timer').css('text-decoration-line','underline');
        $('#headertask_timer').css('cursor','pointer');
        $('#headertask_timer_msg').css('display','block');
      });

      $(document).on('mouseout','#headertask_timer',function()
      { 
        $('#headertask_timer').css('text-decoration-line','none');
        $('#headertask_timer').css('cursor','auto');
        $('#headertask_timer_msg').css('display','none');
      });

      
      

      $(document).on('click','.controls .for_timer_start_pause',function()
      {                 
         $(this).toggleClass('time_pause1');  

         var toggleElement = $(this).closest('tr').find('.task_status');
         var tr = $(this).closest('tr');
         var task_id=$(this).attr('id');
         var task_parent_id=tr.attr("parent-id");

         var data1={}; 

         if($('.controls .per_timerplay_'+task_id).hasClass('time_pause1'))
         { 
            data1['timer'] = 'start';
            window.hoursLabel = ($('.timer_'+task_id).parent('.stopwatch').data('hour')) != "" ? $('.timer_'+task_id).parent('.stopwatch').data('hour') : "0";
            window.minutesLabel = ($('.timer_'+task_id).parent('.stopwatch').data('min')) != "" ? $('.timer_'+task_id).parent('.stopwatch').data('min') : "0";
            window.secondsLabel = ($('.timer_'+task_id).parent('.stopwatch').data('sec')) != "" ? $('.timer_'+task_id).parent('.stopwatch').data('sec') : "0"; 
            window.totalSeconds = Number(hoursLabel*3600)+Number(minutesLabel*60)+Number(secondsLabel);        
            data1['totalSeconds'] = totalSeconds;    
            window.intervalid = setInterval(function(){ getTimer(); },1000);  
            $('#headertask_timer').css('display','block');          
         }
         else
         { 
            data1['timer'] = 'stop';
         }

         $.ajax(
         {

            url: '<?php echo base_url();?>user/task_timer_start_pause/',
            type: 'post',
            data: { 'task_id':task_id,'task_parent_id':task_parent_id },
            dataType: "json",
            /*timeout: 3000,*/

            success: function( data )
            { 
              console.log(data);
              $('#headertask_timer_msg').text(data['parent_subject'] + ' : ' + data['task_subject']);
            //  alert('updated');
            }
            
         });  

         if(toggleElement.val()=='5')
         {
            var user_type="<?php echo $_SESSION['user_type'] ?>";
            if(user_type=='FA')
            {                          
                data1['change_id']=1;                          
            }
            data1['status']=toggleElement.val();
         }

         data1['task_id']=task_id;  
       
         $.ajax(
         {
            url: '<?php echo base_url();?>user/task_timer_start_pause_individual/',
            type: 'post',
            data: data1,
            /*timeout: 3000,*/
            success: function( data )
            {     
              //alert(data);
               if($('.controls .per_timerplay_'+task_id).hasClass('time_pause1'))
               {
                $('#headertask_timer').css('display','none'); 
                               
                cloned_timer(task_id);                 

                 $('.controls  .for_timer_start_pause').css('display','none');
                 $('.controls  .per_timerplay_'+task_id).css('display','block');           
               }
               else if(!$('.controls .per_timerplay_'+task_id).hasClass('time_pause1'))
               {
                $('#custom_timer').empty();
                $('#custom_timer').remove();
                clearInterval(timer);
                $('.controls  .for_timer_start_pause').css('display','block');
                                  
                clearInterval(intervalid);
                 $('#headertask_timer').css('display','none');
                 $('#headertask_timer .hours').html('');
                 $('#headertask_timer .minutes').html('');
                 $('#headertask_timer .seconds').html('');
                 user_timer_view();
               }
               else
               {
                 
                clearInterval(intervalid);
                 $('#headertask_timer').css('display','none');
                 $('#headertask_timer .hours').html('');
                 $('#headertask_timer .minutes').html('');
                 $('#headertask_timer .seconds').html('');
                 user_timer_view();   

                 <?php if(isset($_SESSION['logout_pause'])){ ?>
                  
                   window.location.href = '<?php echo base_url().'login/logout'; ?>';

                 <?php }else{ ?>

                 //location.reload();   

                 <?php } ?>         
               }   
            },
            complete: function(){
              /*Company : Appy Codes ||| Coder: Amit Das||| Date : 16-06-2020*/
              if(!$('.controls .per_timerplay_'+task_id).hasClass('time_pause1')){
                $.ajax(
                {

                    url: '<?php echo base_url();?>user/update_the_parent_timer/',
                    type: 'post',
                    data: { 'parent_id':task_parent_id },
                    /*timeout: 3000,*/

                    success: function(data)
                    { 
                      var data = jQuery.parseJSON(data);
                      if(data.success){
                        $('.timer_'+task_parent_id).find('.hours').text(data.hours<10?"0"+data.hours:data.hours);
                        $('.timer_'+task_parent_id).find('.minutes').text(data.min<10?"0"+data.min:data.min);
                        $('.timer_'+task_parent_id).find('.seconds').text(data.sec<10?"0"+data.sec:data.sec);
                      }
                    }
                    
                });
              }
              
            }                       
         });    

         
        
      });
    
      
      
      function cloned_timer(task_id){
        if($('.controls .per_timerplay_'+$(this).attr("data-taskid")).hasClass('time_pause1')){          
          $('#custom_timer').empty();
          $('#custom_timer').remove();
          clearInterval(timer);
          return false;
        }else{
          $.ajax({
              url  : '<?php echo base_url();?>user/my_active_tasks',           
              type : 'POST',
              dataType: "json",
              //data : {'this_timer':$('#this_timer').val()},         
              success: function(data) {                
                //alert(data);
                if(data.length > 1){
                  var html = "";
                  $(".tbl-custom-timer").empty();
                  $('#this_timer').val(task_id);
                  html += "<tr><th>Client name</th><th>Task name</th></tr>";
                  $.each(data, function(i, item) {
                    if(item.task_id != task_id){
                      html += "<tr><td>"+item.crm_company_name+"</td><td>"+item.subject+"</td></tr>";
                    }
                  });
                  if(html != ""){
                    $(".tbl-custom-timer").append(html);
                    $('#modal_custom_timer').modal('toggle');
                  }
                  return false;
                }
              },
              error: function() {
                    
              }
          });

          var last_time = '';
          var timer = setInterval(function() {
            if(last_time == ""){
              var btn_txt = "PAUSE";
            }else if(last_time == $('#headertask_timer .seconds').text()){
              var btn_txt = "START"
            }else{
              var btn_txt = "PAUSE";
            }         

            //var btn_txt = "PAUSE";
            last_time   = $('#headertask_timer .seconds').text();

            if($('#headertask_timer .hours').text() != ""){
              if(!$('#custom_timer').length){
                var timer_html  = '<div id="custom_timer" class="custom-timer">';
                    timer_html +=   '<div class="message">';
                    timer_html +=     '<div id="content">';
                    timer_html +=       '<div>';
                    timer_html +=         '<span class="custom-hours" id="tmh">'+$('#headertask_timer .hours').text()+':</span>';
                    timer_html +=         '<span class="custom-minutes" id="tmm">'+$('#headertask_timer .minutes').text()+':</span>';
                    timer_html +=         '<span class="custom-seconds" id="tms">'+$('#headertask_timer .seconds').text()+'</span>';
                    timer_html +=       '</div>';
                    // timer_html +=       '<div>';
                    // timer_html +=         '<span class="custom-timer-btn"><a href="javascript:void(0)" id="btn_custom_timer_stp" data-taskid='+task_id+'>'+btn_txt+'</a></span>';
                    // timer_html +=       '</div>';
                    timer_html +=   '</div>';
                    timer_html += '</div>';  

                $('#custom_timer').empty();
                $('#custom_timer').remove();
                    
                $('.renewdesign').append(timer_html);
                //$('.renewdesign').prepend(timer_html);
                //$(timer_html).insertBefore('.renewdesign');
              }else{
                $("#tmh").text($('#headertask_timer .hours').text()+':');
                $("#tmm").text($('#headertask_timer .minutes').text()+':');
                $("#tms").text($('#headertask_timer .seconds').text());
                $('#btn_custom_timer_stp').attr('data-taskid', task_id);
                $('#btn_custom_timer_stp').text(btn_txt);
              }
            }else{
              $('#custom_timer').empty();
              $('#custom_timer').remove();
              clearInterval(timer);
              return false;
            }
          }, 1000);
        }        
      }

      

      function getTimer()
      { 
          ++totalSeconds;
          /*$('#headertask_timer .hours').html(pad(parseInt(totalSeconds/3600)));

          if(parseInt(totalSeconds/60) > '60')
          { 
             $('#headertask_timer .minutes').html(pad(parseInt(parseInt(totalSeconds/60)%60)));
          }
          else
          { 
             $('#headertask_timer .minutes').html(pad(parseInt(totalSeconds/60)));
          }
          
          $('#headertask_timer .seconds').html(pad(totalSeconds%60));*/
      }

      function pad(val)
      {
          var valString = val + "";
          if(valString.length < 2)
          {
              return "0" + valString;
          }
          else
          {
              return valString;
          }
      }

      function user_timer_view()
      {
         var new_alltask = [];
         TaskTable_Instance.column(1).nodes().to$().each(function(index) 
         {           
            new_alltask.push( $(this).find(".alltask_checkbox").attr('data-alltask-id') );          
         });
         
         new_alltask=new_alltask.join(",");

         $.ajax(
         {
             url:"<?php echo base_url()?>Task/get_user_view_timer",
             type:"POST",
             data:{'task_id':new_alltask},
             dataType:'json',
             beforeSend : function ()
             {
               //$(".LoadingImage").show();
             },
             success:function(res)
             {
            //   $.each(res,function(i,v){

                  $.each(res,function(i,v){

                   var t_style=(v.timer_style)?'block':'none';

                $('.controls  .per_timerplay_'+v.task_id).css('display',t_style);

               });

             //  });

             }
         });               
      }  

    });

  </script>
  <!-- end of new timer -->    

  <script type="text/javascript"> 



$(document).ready(function (){
  /*close button action for server response popup*/
  $("#server_response_popup_close").click(function(){ $('div.modal-alertsuccess').hide(); });
  /**/
        $(".theme-loader").remove();
      
      /*importent for header link  popup */
          $(".dynamic_header").find("li a[href^='#']").attr('data-toggle',"modal");

          $(".dynamic_header").find("li a[href^='#']").click(function(){
            
            if($(this).attr('href').indexOf("#default-Modal") != -1 )
            {

              $.get("<?php echo base_url();?>client/Get_ClientLimit", function(data, status){
                
                if( !(data.result > 0 || data.result == 'unlimited') )
                {
                  var text = '<div class="alert alert-danger"><strong>Your Firm Client Limit Exited</strong> Please Upgrate Your client limits.</div>';
                  $('.all_layout_modal#default-Modal').find('div.modal-body').html(text);
                }

              },"json");

              $('#searchCompany').prop('readonly',false).val('');
              $('#searchresult').text('').show();
              $('#companyprofile').text('');
              $('#selectcompany').hide();
              $('.all_layout_modal .close').show();        
            }

        });  


     $('.dropdown-sin-21234').dropdown({
         limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });




      $('th').on("click.DT", function (e) {
      //stop Propagation if clciked outsidemask
      //becasue we want to sort locally here
        if (!$(e.target).hasClass('sortMask')) {
          e.stopImmediatePropagation();
        }
      });
 $(document).on("click",".close_notification",function(){})
});

function review_response(res)
{
  var id=$("#review_notification_id").val();
  var notes=$("#review_response_notes").val();
  $.ajax({
    url:"<?=base_url()?>user/manager_review_response",
    data:{"mn_id":id,"res":res,"notes":notes},
    type:"POST",
    success:function(res){location.reload();}
    });
}

$(document).ready(function() {

  $(document).on("click","#btn_custom_timer_stp",function(e){
    if($('.controls .per_timerplay_'+$(this).attr("data-taskid")).hasClass('time_pause1')){
      $('.per_timerplay_'+$(this).attr("data-taskid")).click();      
      $('#custom_timer').empty();
      $('#custom_timer').remove(); 
      clearInterval(timer);
    }        
  });
  
  $(document).on("click",".notifi_status",function(e){
      var task_id=$(this).data('task_id');
      var id=$(this).data('id');

      // console.log(task_id);
      // return;
     
      $.ajax({
        url: '<?php echo base_url();?>user/update_notification/',
              type: 'post',

              data: { 'id':id },
     
              success: function( data ){
               window.location.href = "<?php echo base_url()?>user/task_details/"+task_id;
              }
       });
    });

  

  $(document).on("click",".doc_notifi_status",function(e){
    //  var task_id=$(this).data('task_id');
      var id=$(this).data('id');
       var link=$(this).data('link');
     
      $.ajax({
        url: '<?php echo base_url();?>user/update_notification/',
              type: 'post',

              data: { 'id':id },
     
              success: function( data ){
                if(link!=''){
                   window.location.href = "<?php echo base_url()?>Documents/?dir=/"+link;

                }
                else
                {
                   window.location.href = "<?php echo base_url()?>Documents";
                }
              
              }
       });
    });


  $(document).on("click",".leads_notifi_status",function(e){
      var leads_id=$(this).data('leads_id');
      var id=$(this).data('id');
     
      $.ajax({
        url: '<?php echo base_url();?>user/update_notification/',
              type: 'post',

              data: { 'id':id },
     
              success: function( data ){
               window.location.href = "<?php echo base_url()?>leads/leads_detailed_tab/"+leads_id;
              }
       });
    });
  


  $('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
$('.custom_checkbox1 input').after( "<i></i>" )


//}catch(e){alert(e);}
        });
        </script>


    <script>
    var switcheryElm = [];
    $( document ).ready(function() {   

        var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy',minDate:0 }).val();

        // Multiple swithces
        var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));

        var index = 1;
        elem.forEach(function(html) {
            var switchery = new Switchery(html, {
                color: '#22b14c',
                jackColor: '#fff',
                size: 'small'
            });
            //var index =((!html.getAttribute('id')) ? '1' : html.getAttribute('id') );
            ///console.log(html.getAttribute('id') );
            html.setAttribute('data-switcheryId',index);
            switcheryElm[index] = switchery;
            index++;
        });

        $('#accordion_close').on('click', function(){
                $('#accordion').slideToggle(300);
                $(this).toggleClass('accordion_down');
        });


        $('.chat-single-box .chat-header .close').on('click', function(){
          $('.chat-single-box').hide();
        });

    
    var resrow = $('#responsive-reorder').DataTable({
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    });
});
$(".rejected_tsk").click(function(){
  $.ajax({
    url:"<?= base_url()?>user/rejected_tsk_seen_status/"+$(this).attr("data-id"),
    type:'get',
    success:function(){location.reload();}
  })
});
</script>
</body>
<!-- chat box -->
  <!-- modal 1-->
  
      <div class="modal fade all_layout_modal" id="default-Modal" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header modal-search-header">
                  <h4 class="modal-title">Search Companies House</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body modal-search-body">
                 
                  <div class="input-group input-group-button input-group-primary modal-search">
                     <input type="text" class="form-control" placeholder="Search here..." id="searchCompany">
                     <button class="btn btn-primary input-group-addon" id="basic-addon1">
                     <i class="ti-search" aria-hidden="true"></i>
                     </button>
                  </div>
                  <div class="british_gas1 trading_limited" id="searchresult">
                  </div>
                  <div class="british_view" style="display:none" id="companyprofile">                     
                  </div>
                  <div class="main_contacts" id="selectcompany" style="display:none">
                     <h3>british gas trading limited</h3>
                     <div class="trading_tables">
                        <div class="trading_rate">
                           <p>
                              <span><strong>BARBARO,Gab</strong></span>
                              <span>Born 1971</span>
                              <span><a href="#">Use as Main Contact</a></span>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- modalbody -->
            </div>
         </div>
      </div>
        <!-- modal-close -->
        <!--modal 2-->
      <div class="modal fade all_layout_modals" id="exist_person" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Select Existing Person</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="main_contacts" id="main_contacts" style="display:block">     
                  </div>
               </div>
               <!-- modalbody -->
            </div>
         </div>
      </div>
          <!-- modal-close -->
          <!-- modal 2-->
<div id="Confirmation_popup" class="modal fade" role="dialog" style="display:none;">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title" style="text-align-last: center">Confirmation</h4>
          </div>
          <div class="modal-body">
             <label class="information"></label>
          </div>
          <div class="modal-footer">             
              <button class="Confirmation_OkButton">Save</button>
              <button class="Confirmation_CancelButton" data-dismiss="modal">Close</button>              
          </div>
        </div>
    </div>
  </div>

</html>
<script>
//class="modal fade all_layout_modal show"
function company_model_close(){
  //console.log('aaa');
    $('.all_layout_modal').modal('hide');
 //   $('button.close').trigger('click');
    $('body').removeClass('modal-open');
  $('.modal-backdrop').remove();

  $("#default-Modal").hide();
    // $('.modal-backdrop.show').hide();
  // $('.modal-backdrop.show').each(function(){
  //  $(this).remove();
  // });
}

function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

$('#searchCompany').keyup(debounce(function(){
  $(".form_heading").html("<h2>Adding Client via Companies House</h2>");
        var $this=$(this);
        var term =$this.val() ;
        type_check(term);
    },500));

function type_check(term)
{ 
     $.ajax({
          url: '<?php echo base_url();?>client/SearchCompany/',
          type: 'post',
          data: { 'term':term },
          beforeSend : function()    {           
              $(".LoadingImage").show();
           },
          success: function( data ){
              $("#searchresult").html(data);
               $(".LoadingImage").hide();
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });
}


 

 function checked(term){
  $.ajax({
          url: '<?php echo base_url();?>client/SearchCompany/',
          type: 'post',
          data: { 'term':term },
          beforeSend : function()    {           
             $(".LoadingImage").show();
           },
          success: function( data ){
              $("#searchresult").html(data);
               $(".LoadingImage").hide();
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });

 }

function addmorecontact(id)
{
    $(".all_layout_modal").modal('hide');
    $(".LoadingImage").show();
    $(".modal-backdrop").css("display","none");
    var url = $(location).attr('href').split("/").splice(0, 8).join("/");

    var segments = url.split( '/' );
    var fun = segments[5];
    var values= '<?php echo $_SESSION['roleId']; ?>';
    //alert(values);
if(fun!='' && fun !='addnewclient'  && fun !='addnewclient#')
{

  <?php // if($_SESSION['roleId']=='1'){ ?>
   // $(location).attr('href', '<?php echo base_url();?>firm/add_firm/'+id);
 <?php //}else{ ?>

    $(location).attr('href', '<?php echo base_url();?>client/addnewclient/'+id);
<?php //} ?>
 

   
}  else{
    <?php // if($_SESSION['roleId']=='1'){ ?>
   // $(location).attr('href', '<?php echo base_url();?>firm/add_firm/'+id);
   <?php // }else{ ?>
   $(location).attr('href', '<?php echo base_url();?>client/addnewclient/'+id);
    <?php  //} ?>
   $("#companyss").attr("href","#");

$('.contact_form').show();
 $('.all_layout_modal').modal('hide');
 
     $('.nav-link').removeClass('active');
  //   $('.main_contacttab').addClass('active');
//$('.contact_form').show();

    $("#company").removeClass("show");
    $("#company").removeClass("active");
    $("#required_information").removeClass("active");
    $(".main_contacttab").addClass("active");
    $("#main_Contact").addClass("active");
    $("#main_Contact").addClass("show"); 
}      
                

}

var xhr = null;
   function getCompanyRec(companyNo)
   {
      $(".LoadingImage").show();
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
       
      xhr = $.ajax({
          url: '<?php echo base_url();?>client/CompanyDetails/',
          type: 'post',
          data: { 'companyNo':companyNo },
          success: function( data ){
              //alert(data);
              $("#searchresult").hide();
              $('#searchCompany').prop('readonly',true);
              $('#companyprofile').show();
              $("#companyprofile").html(data);
              $(".LoadingImage").hide();
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });}


      function getCompanyView(companyNo)
   {
      //alert(companyNo);
      $('.modal-search').hide();
      $(".LoadingImage").show();
     var user_id= $("#user_id").val();
     
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
   
       
      xhr = $.ajax({
          url: '<?php echo base_url();?>client/selectcompany/',
          type: 'post',
          dataType: 'JSON',
          data: { 'companyNo':companyNo,'user_id': user_id},
          success: function( data ){
              //alert(data.html);
              $("#searchresult").hide();
              $('#companyprofile').hide();
              $('#selectcompany').show();
              $("#selectcompany").html(data.html);
              $('.main_contacts').html(data.html);
   // append form field value
   $("#company_house").val('1');
   //alert($("#company_house").val());
   $("#company_name").val(data.company_name);
   $("#company_name1").val(data.company_name);
   $("#company_number").val(data.company_number);
   $("#companynumber").val(data.company_number);
   $("#company_url").val(data.company_url);
   $("#company_url_anchor").attr("href",data.company_url);
   $("#officers_url").val(data.officers_url);
   $("#officers_url_anchor").attr("href",data.officers_url);
   $("#company_status").val(data.company_status);
   $("#company_type").val(data.company_type);

   // console.log('comapny type ');
   // console.log(data.company_type);
   // console.log('comapny type test ');
   //$("#company_sic").append(data.company_sic);
   $("#company_sic").val(data.company_sic);
   $("#sic_codes").val(data.sic_codes);
   
   $("#register_address").val(data.address1+"\n"+data.address2+"\n"+data.locality+"\n"+data.postal);
   $("#allocation_holder").val(data.allocation_holder);
   $("#date_of_creation").val(data.date_of_creation);
   $("#period_end_on").val(data.period_end_on);
   $("#next_made_up_to").val(data.next_made_up_to);
   $("#next_due").val(data.next_due);
   $("#accounts_due_date_hmrc").val(data.accounts_due_date_hmrc);
   $("#accounts_tax_date_hmrc").val(data.accounts_tax_date_hmrc);
   $("#confirm_next_made_up_to").val(data.confirm_next_made_up_to);
   $("#confirm_next_due").val(data.confirm_next_due);
   
   $("#tradingas").val(data.company_name);
   $("#business_details_nature_of_business").val(data.nature_business);
   $("#user_id").val(data.user_id);

 //  $(".edit-button-confim").show(); //for hide edit option button 
   $(".LoadingImage").hide();
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });}
   
   
       function getmaincontact(companyNo,appointments,i,user_id,obj)
   {
      $(".LoadingImage").show();

        var data = { 'companyNo':companyNo,'appointments': appointments,'count':i,'user_id':user_id};
        var exist_contact = 0;
        var modal = $(obj).closest('.all_layout_modal');

        if( modal.attr('id') == "company_house_contact" ) //check it is edit page popup
        {
          exist_contact = 1;
          var cnt = $("#append_cnt").val();

            if(cnt=='')
            {
              cnt = 1;
            }
           else
           {
            cnt = parseInt(cnt)+1;
           }
           data['cnt'] = cnt;
        $("#append_cnt").val(cnt);

        }
 
      xhr = $.ajax({
          url: '<?php echo base_url()?>client/maincontact/',
          type: 'post',
          data: data,
          success: function( data ){

            modal.find('#selectcompany #usecontact'+i).html('<span class="succ_contact"><a href="#">Added</a></span>');

            if(exist_contact)
            {

              $('.contact_form').append(data);

                    if(i=='First')
                    {
                           $(".contact_form .common_div_remove-"+cnt).find('.make_primary_section').trigger('click');
                    }
                  /** 05-07-2018 rs **/
                  $('.make_a_primary').each(function(){
                     var countofdiv=$('.make_a_primary').length;
                     if(countofdiv>1){
                     var id=$(this).attr('id').split('-')[1];

                      $('.for_remove-'+id).remove();
                       $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
                  }
                  });
                  var start = new Date();
              start.setFullYear(start.getFullYear() - 70);
              var end = new Date();
              end.setFullYear(end.getFullYear());
                $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true,
                  changeYear: true,yearRange: start.getFullYear() + ':' + end.getFullYear() }).val();
                modal.modal('hide');
                sorting_contact_person( $('.contact_form').find(".CONTENT_CONTACT_PERSON").filter(":last") );
              }
              console.log(typeof trigger_contact_person_validation);
              if( typeof trigger_contact_person_validation == "function")
              {
               trigger_contact_person_validation(); 
               move_ccp_contents();
              }
            $(".LoadingImage").hide();

                      },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });
    }
function backto_company()
{
   $('#companyprofile').show();
   $('.main_contacts').hide();
}
 
</script>

<script type="text/javascript">
   
    $(function(){
      
      var headheight = $('.header-navbar').outerHeight();
      var navbar = $('.remin-admin');
  });
   
</script>

<script type="text/javascript">



  
  
  $(document).ready(function(){   

     $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");
     $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");      
       




    log_update(); 

     



      $("a#mobile-collapse").click(function () {
          $('#pcoded.pcoded.iscollapsed').toggleClass('addlay');
    });
     
      $(".nav-left .data1 li.comclass a.com12").click(function () {
      $(this).parent('.nav-left .data1 li.comclass').find('ul.dropdown-menu1').slideToggle();
      $(this).parent('.nav-left .data1 li.comclass').siblings().find('ul.dropdown-menu1:visible').slideUp();      

    });
    

    
      var test = $('body').height();
      $('test').scrollTop(test);

   });

  $(".client_request").click(function(){
    //alert('ok');
    $("#services_name").val($(this).data('service'));
    $("#service_user_id").val($(this).attr('id'));
    $("#notifications_id").val($(this).data('id'));

    console.log($(this).data('service'));
     console.log($(this).attr('id'));
      console.log($(this).data('id'));
    $("#manager_service_accept").modal('show');
  });
$("#enable_reassign").change(function(){
  if($(this).prop("checked"))
  {   $(".review_notes").hide();
     $("#reassign_selectbox").show();
  }else{
     $("#reassign_selectbox").hide();
     $(".review_notes").show();
  } 
  });

  $(".staff_request").click(function(){
    //alert('ok');
    $("#review_notification_id").val($(this).data('id'));
  });

  $("#accept_service").click(function(){
    //alert('ok');
    var services_name=$("#services_name").val();
   var user_id=$("#service_user_id").val();
   var id=$("#notifications_id").val();
   var data={'user_id':user_id,'service_name':services_name,'id':id}

   $.ajax({
       url: '<?php echo base_url();?>Sample/service_update/',
    type: "POST",
    data: data,
    success: function(data)  
    {
      if(data=='1'){
        $(".close_popup").trigger('click');
      }

    }
   });
  });


  $("#send_proposal").click(function(){
        var services_name=$("#services_name").val();
   var user_id=$("#service_user_id").val();
   var id=$("#notifications_id").val();
   var data={'user_id':user_id,'service_name':services_name,'id':id}

   $.ajax({
       url: '<?php echo base_url();?>Sample/proposal_add/',
    type: "POST",
    data: data,
    success: function(data)  
    {
      if(data!=' '){
        $(".close_popup").trigger('click');
        $(".proposal_success_message").show();
         $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");
         $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");
        setTimeout(function(){ $(".proposal_success_message").hide(); }, 3000);
      }
    }
   });
  });





/*hide archive button inside the archive tab*/
  $("#nmenut").find("a[id='archive-notification']").hide();
/*hide archive button inside the archive tab*/

$(document).on("click","#remove-notification,#archive-notification,#forward-notification",function(){
  var type=$(this).data('type');
  var id=$(this).data('id');
  var section=$(this).data('section');
  var users=$(this).data('user');
  var source  = $(this).data('source');

  /*console.log(id);
  console.log(section);
  console.log(users);*/
  if(type=="remove"){
    $("#confirm_button").removeAttr('class'); 
    $("#show_message").html('Do you want to remove notification?');
    $("#confirm_button").addClass('btn btn-primary remove_notify');
    $("#notification_id").val(id);
    $("#notification_modal").modal('show');   
    $("#notification_type").val(type);
    $("#notification_section").val(section);
    $("#notification_users").val(users);
    $("#SorceFrom").val(source);
  }else if(type=="archive"){
    $("#confirm_button").removeAttr('class');
    $("#show_message").html('Do you want to archive notification?');
    $("#confirm_button").addClass('btn btn-primary archive_notify');
    $("#notification_id").val(id);
    $("#notification_modal").modal('show');
    $("#notification_type").val(type);
    $("#notification_section").val(section);
    $("#notification_users").val(users);
    $("#SorceFrom").val(source);
  }else if(type=="read"){
    $("#confirm_button").removeAttr('class');
    $("#show_message").html('Do you want to forward notification?');
    $("#confirm_button").addClass('btn btn-primary read_notify');
    $("#notification_id").val(id);
    $("#notification_modal").modal('show');
    $("#notification_type").val(type);
    $("#notification_section").val(section);
    $("#notification_users").val(users);
    $("#SorceFrom").val(source);
  }else{
    $("#forward").removeAttr('class');
    $("#show_message").html('Do you want to forward notification?');
    $("#forward").addClass('btn btn-primary forward_notify');
    $("#fwd_notification_id").val(id);
    $("#forward_task").modal('show');
    $("#fwd_notification_type").val(type);
    $("#fwd_notification_section").val(section);
    $("#fwd_notification_users").val(users);
    $("#SorceFrom").val(source);
  }
});


$(document).on("click","#review-notification",function(){

  // var type=$(this).data('type');
  var task_id=$(this).data('task_id');
   var task_ifo=$(this).data('task_info');
   $('#review_task_id').val(task_id);
  $('.task_info').html('').html(task_ifo);

        var type=$(this).data('type');
        var id=$(this).data('id');
        var section=$(this).data('section');
        var users=$(this).data('user');
        var source  = $(this).data('source');


      $("#review_notification_type").val(type);
      $("#review_notification_section").val(section);
   $("#review_notification_users").val(users);
    $("#review_notification_id").val(id);
  $("#review_notification_source").val(source);



  //alert(task_id);
  // var section=$(this).data('section');
  // var users=$(this).data('user');
  // var source  = $(this).data('source');

  
});



$(document).on('click','.remove_notify,.archive_notify,#confirm_button,.read_notify',function(){
 //  alert('ok');
    var type   =$("#notification_type").val();
    var section=$("#notification_section").val();
    var users  =$("#notification_users").val();
    var id     =$("#notification_id").val();
    var source = $("#SorceFrom").val();
  
    var formData={'type':type,'section':section,'users':users,'id':id,'source':source};
    $.ajax({
    url: '<?php echo base_url(); ?>Sample/notification_section',
    type : 'POST',
    data : formData,                    
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(response) {
        console.log(response);
        if($.trim(response)=='1'){
         /*****/    $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");
             $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");          
          //location.reload();
        } 
        $('#notification_modal .close_popup').trigger('click');

        if(users!="" && (section == 'proposal' || section == 'deadline' || section == 'invoice' || section == 'repeat-invoice'))
        { 
            if(section == 'proposal')
            {
              <?php //$_SESSION['firm_seen'] = 'accept'; ?>
            }
            window.location.href = users;
        }         

        var uri = '<?php echo $_SERVER['REQUEST_URI'] ?>';

        if(uri == '/Notification/notification_list')
        {
           $('.alert-success').find('.position-alert1').html('');
           $('.alert-success').find('.position-alert1').html('Updated Successfully!');
           $('.alert-success').show();
           setTimeout(function(){  $('.alert-success').hide(); location.reload(); }, 3000);
        }

        $(".LoadingImage").hide();
      }
    });
});



$(document).on('click','.forward_notify',function(){
    var worker=$("#workers").val();
    var task_id=$("#fwd_notification_id").val();
      var data = {};
      data['task_id'] = task_id;
      data['worker'] = worker;
      $(".LoadingImage").show();
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/update_assignees/",
               data: data,
               success: function(response) {
                  // alert(response); die();
                  $(".LoadingImage").hide();
               //$('#task_'+id).html(response);
             //  $('.task_'+id).html(response);
                    $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>sample/update_task_view/",
               data: {'id':task_id},
               success: function(response) {

                /*****/    $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");

                $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");
  $(".LoadingImage").hide();
                 //     $('#action_result').show();
                 //  $("#action_result .position-alert1").html('Success !!! Staff Assign have been changed successfully...');
                 // setTimeout(function(){ $('#action_result').hide();location.reload(); }, 1500);

                  }

                });



               
               },
            });
});
</script>

<?php





 $uri_seg_menu = $this->uri->segment('1'); 


// echo $uri_seg_menu;

 if(strtolower($uri_seg_menu) =='proposal' ||  strtolower($this->uri->segment(2)) =='task_list' ){ }else {  ?>



  <!-- tinymce editor -->
      <script src="<?php echo base_url();?>assets/js/tinymce.min.js"></script>

      <script type="text/javascript">
        /*For editor config*/
        var tinymce_config = {    
          plugins: ' preview  paste importcss searchreplace autolink     visualblocks visualchars fullscreen image link table  hr pagebreak nonbreaking anchor toc  advlist lists imagetools textpattern noneditable ',
          menubar: 'file edit view insert format tools table',
          toolbar: 'fontselect fontsizeselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | insertfile image  link | numlist bullist  | hr | fullscreen  preview ',
          file_picker_types: "image",
          height: 300,
          relative_urls : false,
          remove_script_host : false,
          convert_urls : true,
        };  
        /*For editor config*/

        $(document).ready(function(){   


        /*For editor config*/
        tinymce_config['selector']          = '#feed_msg';
        tinymce.init( tinymce_config );  
        /*For editor config*/

        });
      </script>

    <!-- tinymce editor -->
<?php }

//echo $this->uri->segment(2);
if($this->uri->segment(2)!='proposal' || $this->uri->segment(2)!='proposal_feedback'){
//if($_SESSION['roleId']=='4'){ // only for client login users.
//if($_SESSION['roleId']=='1' || $_SESSION['roleId']=='4' ){ //remove 12 and put 4 
if( strtolower($uri_seg_menu)=='tickets'){ // REMOVED BY AJITH  $uri_seg_menu=='task_list' || 
  ?>  
<?php

             $login_user_id=$_SESSION['userId'];
        if($_SESSION['user_type']=='FU' || $_SESSION['user_type']=='FA')
        {
           
          
            $client_data=$this->db->query("SELECT * FROM user where user_type='FU' AND autosave_status!='1' and crm_name!='' and id=".$login_user_id." order by id DESC")->result_array();
           
        
            $data['getalladmin']=$this->db->query("SELECT * FROM user where crm_name!='' and user_type='FA' and firm_id=".$_SESSION['firm_id']." order by id DESC")->result_array();
            $user_val=array('-1');

          


                  $ques=Get_Assigned_Datas('CLIENT');
                foreach ($ques as $key => $value) {
                 $res1=Get_Module_Assigees( 'CLIENT' , $value );
                 if (in_array($_SESSION['userId'], $res1))
         
                  {
                    $user_val[]=$value;
                    }
                 }


        

                   $task_user_val=$this->db->query("SELECT * FROM client where id in (".implode(',',$user_val).")  order by id DESC")->result_array();
                   $task_user_val = array_column($task_user_val,'user_id');
                  // $task_user_val = implode(',',$task_user_val);

             
                    $data['getallstaff']=array();
                    $data['getallUser']=array();
          
                    
          
            $newarray=array_values(array_unique($task_user_val));
            if(!empty($newarray)){

                $data['getallUser']=$this->db->query("SELECT * FROM user where id in (".implode(',',$newarray).") and firm_id='".$_SESSION['firm_id']."' and user_type='FC' order by id DESC")->result_array();

                $data['getallstaff']=$this->db->query("SELECT * FROM user where  firm_id='".$_SESSION['firm_id']."' and user_type!='FC' and user_type!='SA' order by id DESC")->result_array();

               

                if($_SESSION['user_type']=='FA')
                {

                    $data['getallUser']=$this->db->query("SELECT * FROM user  where firm_id='".$_SESSION['firm_id']."' and user_type='FC' order by id DESC")->result_array();

                   $data_superadmin=$this->db->query("SELECT * FROM user where id ='1' and user_type!='FC' order by id DESC")->row_array();

                  $data['getallstaff'][]= $data_superadmin;
                }

               
            }
           
        }else{
            $data['getallstaff']=array();
            $data['getallUser']=array();
            $data['getalladmin']=$this->db->query("SELECT * FROM user where crm_name!='' and user_type='FA' and firm_id=".$_SESSION['firm_id']." order by id DESC")->result_array();
            $data['getallUser']=$this->db->query("SELECT * FROM user where id='".$login_user_id."'  and firm_id='".$_SESSION['firm_id']."' and user_type='FC' order by id DESC")->result_array();

        }

        
        $data['column_setting']=$this->Common_mdl->getallrecords('column_setting_new');
        $data['client'] =$this->db->query('select * from column_setting_new where id=1')->row_array();
        $data['feedback'] =$this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' "  )->result();



       //  $this->load->view('User_chat/chat_page_new',$data); 
      
       ?>

 <script type="text/javascript">
$(document).ready(function(){
  //alert('zzz');
//$('#main-chat #sidebar .user-box .userlist-box').trigger('click');
//$('#main-chat .chat-box').find('.chat-single-box .mini i').trigger('click');
//alert($('#main-chat .chat-box').find('.chat-single-box .mini').length+".trigger('click')");
//BY AJITH FOr show all chat box 
});
</script>
<?php } 
//} 
} ?>



<script type="text/javascript">
  $(document).ready(function(){
      $('.dataTables_wrapper').each(function(){
var datatable_id = $(this).attr('id');
var datatable_id_cur = datatable_id.replace('_wrapper','');
if($(this).find('.dataTables_scrollBody').prop("scrollHeight") > $(this).find('.dataTables_scrollBody').height()){
    $(this).find('.dataTables_scrollHeadInner').css({"padding-right": "21px", "min-width": "calc(100% - 21px)"}); 
}
$('#'+ datatable_id_cur).DataTable().columns.adjust();
setTimeout(function(){$('#'+ datatable_id_cur).DataTable().columns.adjust();}, 1000);
});


// $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
// var target_id = $(this).attr('href');
// var href = target_id.substring(target_id.indexOf("#") + 1); 
// var datatable_id = $("#"+href).find('table').attr('id');
// var datatable_id_cur = datatable_id.replace('_wrapper','');
// if($(this).find('.dataTables_scrollBody').prop("scrollHeight") > $(this).find('.dataTables_scrollBody').height()){
//     $(this).find('.dataTables_scrollHeadInner').css({"padding-right": "21px", "min-width": "calc(100% - 21px)"}); 
// }
// $('#'+ datatable_id_cur).DataTable().columns.adjust();
// setTimeout(function(){$('#'+ datatable_id_cur).DataTable().columns.adjust();}, 1000);

// });

});
</script>

<script type="text/javascript">
  $(document).ready(function(){
      $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });

      // $('.modal').modal({backdrop: 'static', keyboard: false,show: false});

    $(document).on('click', '.modal .hasDatepicker', function(){
    $('.ui-datepicker').addClass('uidate_visible');
    });

    $(document).on('click', '.modal button[data-dismiss="modal"]', function(){
    $('.ui-datepicker').removeClass('uidate_visible');
    });
  });

  $(document).ready(function(){


      $(".modal-alertsuccess").each(function(){ 
          if($(this).find('.newupdate_alert').length < 1){
            $(this).wrapInner( "<div class='newupdate_alert'></div>");
          } 
      });
    /*importent for datatable scroll*/
    $(document).ready(function(){ 
      $('table.dataTable').wrap('<div class="scroll_table"></div>');

    $(document).on('click', '.multiselect .selectBox', function(e) {
    e.stopImmediatePropagation();
    $('#checkboxes').show();
    });

    $(document).on('click', '.Settings_Button', function(e) {
        var sessionid = <?php echo $_SESSION['id']; ?>;
      if(sessionid == 4100){
        $('.buttons-colvis').css('display','block');
      }else{
        $('.buttons-colvis').css('display','none');
      }
    });

    $('.dataTables_wrapper .toolbar-table').addClass('select_visibles');

    });
    /*important for datatable scroll*/

    $(document).mouseup(function(e) {
    var container = $(".multiple-select-dropdown");
    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        //console.log('ok');
        $('.themicond').removeClass('shown');
        $(".multiple-select-dropdown").removeClass('Show_content');
    }

    });
    $(document).on('click','.dt-button-background',function(e){
         $('.dt-buttons').find('.dt-button-collection').detach();
          $('.dt-buttons').find('.dt-button-background').detach();
    });

    $('th .themicond').on('click', function(e) {
        if ($(this).find(".dropdown-content").hasClass("Show_content")) {
          alert('showcontent is visible');
        $(this).parent().find('.dropdown-content').removeClass('Show_content');
    }
    });
 

      $('.modal').on("shown.bs.modal", function() {
           var winheight1 = $(this).find('.modal-dialog').outerHeight();
          
           if(winheight1 < 400) {
                   $(this).find('.modal-dialog').addClass('normal_heights');
            }
      }); 

      $('.modal').on("hide.bs.modal", function() {
           var winheight1 = $(this).find('.modal-dialog').outerHeight();
          
           if(winheight1 < 400) {
                   $(this).find('.modal-dialog').removeClass('normal_heights');
            }
      });           
    

  });
var Conf_Confirm_Popup =   function(conf){

    if( 'OkButton' in conf ) 
    {
      var text = ('Text' in conf.OkButton   ? conf.OkButton.Text : 'Yes' );
      var fun = ('Handler' in conf.OkButton ? conf.OkButton.Handler : function(){} );

      $('#Confirmation_popup .Confirmation_OkButton').html(text).unbind().on('click', fun );

    }

     if( 'CancelButton' in  conf ) 
    {
      var text = ('Text' in conf.CancelButton ? conf.CancelButton.Text : 'No' );
      var fun = ('Handler' in  conf.CancelButton ? conf.CancelButton.Handler : function(){} );
    
      $('#Confirmation_popup .Confirmation_CancelButton').html(text).unbind().on('click', fun);
    }

     if( 'Info' in conf )
    {
      $('#Confirmation_popup .information').html( conf.Info );
    }

  };
  var Show_LoadingImg = function()
  {
    $('.LoadingImage').show();
  };
  var Hide_LoadingImg = function()
  {
    $('.LoadingImage').hide();
  };
  

    $(document).on('click','#send_review_btn',function(){

      // alert('ccc');
       var rec_id = $('#review_task_id').val();
        var  task_id=[];
       task_id.push(rec_id);
         $.ajax({
        type: "POST",
        url: "<?php echo base_url().'user/manager_notify';?>",        
        data: {task_id:task_id,},
        beforeSend: function() {

          $(".LoadingImage").show();
        },
        success: function(data) {
          $(".LoadingImage").hide();

          $('#review_task .close_popup').trigger('click');

               $(".header_popup_info_msg").show();
               $('.header_popup_info_msg .header_position-alert1').html('').html('Success !!! Task  have been Send successfully...');
               
          
                
                //TaskTable_Instance.cell( cell ).invalidate().draw();


               setTimeout(function () {
                $(".header_popup_info_msg").hide();
                }, 1500);
        }
      });


    });

  $(document).on('click','#review_accept_btn',function(){

    var type   =$("#review_notification_type").val();
    var section=$("#review_notification_section").val();
    var users  =$("#review_notification_users").val();
    var id     =$("#review_notification_id").val();
   var source  =$("#review_notification_source").val();


         var rec_id = $('#review_task_id').val();

             $.ajax({
            url: '<?php echo base_url();?>user/task_statusChange/',
            type: 'post',
            data:{'rec_id':rec_id,'status':'complete',},
            
            beforeSend:function(){$(".LoadingImage").show();},
            success: function( data ){
             
               $(".LoadingImage").hide();

               var formData={'type':type,'section':section,'users':users,'id':id,'source':source};
            $.ajax({
            url: '<?php echo base_url(); ?>Sample/notification_section',
            type : 'POST',
            data : formData,                    
              beforeSend: function() {
                $(".LoadingImage").show();
              },
              success: function(data) {
                if(data=='1'){
                 /*****/    $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");
                     $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");
                   $(".LoadingImage").hide();
                  //location.reload();
                }
              }
            });
                  $('#review_task .close_popup').trigger('click');

               $(".header_popup_info_msg").show();
               $('.header_popup_info_msg .header_position-alert1').html('').html('Success !!! Task status have been changed successfully...');
               
          
                
                //TaskTable_Instance.cell( cell ).invalidate().draw();


               setTimeout(function () {
                $(".header_popup_info_msg").hide();
                }, 1500);
              // $('#review_task .close_popup').trigger('click');
             
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });  

});

  function check_name(obj,id,event,target_url)
  {
      var name = $(obj).val();  

      if(name!="")
      { 
          $.ajax(
          {
             url:target_url,
             type:'POST',
             data:{name:name,id:id},
             async:false,

             beforeSend: function() 
             {
                $('.LoadingImage').show();
             },

             success:function(data)
             {  
                var resp = JSON.parse(data);  
                $(".LoadingImage").hide(); 

                if(resp.count != 0) 
                {                     
                   $(obj).next('#name_er').html('');            
                   $(obj).next('#name_er').html('Already Exists.'); 
                   event.preventDefault(); 
                }
                else
                {                
                   $(obj).next('#name_er').html('');
                   $('.LoadingImage').show();
                }              
             }
         });                  
      }    
  }

    if ($(window).width() < 767) {
      $( ".dropdown-menu1" ).each(function() {
      $(this).closest('li').find('.dropdown-toggle').addClass( "drop_opens" );
    });

      $('.drop_opens').on('click', function(e) {
          $(this).next('.dropdown-menu1').slideToggle(300);
      });
    }


 function log_update()
      {
         $.ajax({
         url: '<?php echo base_url();?>Login/login_time_update/',
         type : 'POST',
         dataType:'json',
       
       });
      }
// Login check update //
var log_updatetime = 10 * 60 * 1000;

    window.setInterval(function(){

   log_update();
                    
  }, log_updatetime );

    //End  Login check update //

    function timer_killer(id){      
      if($('.per_timerplay_'+id).trigger('click')){
        if ($('#tk_'+id) == "Stop"){
          $('#tk_'+id).text("Start");
        }else{
          $('#tk_'+id).text("Stop");
        }
      }
    }    

  $(document).on('click','#kill_timers',function(){ 
    $.ajax({
      url  : '<?php echo base_url();?>user/kill_timers',           
      type : 'POST',
      dataType: "json",
      data : {'this_timer':$('#this_timer').val()},                 
      success: function(data) {                
        $('#modal_custom_timer').modal('toggle');
        location.reload();
      },
      error: function() {
            
      }
    });
  });

  $(document).on('click','#stop_long_timer',function(){ 
    $.ajax({
      url  : '<?php echo base_url();?>user/kill_long_timers',           
      type : 'POST',
      dataType: "json",
      success: function(data) {                
        $('#modal_long_timer').modal('toggle');
        location.reload();
      },
      error: function() {
            
      }
    });
  });    
    
  function cpopup(data){
    if(data.length > 1){            
      var html = "";
      $(".tbl-custom-timer").empty();
      html += "<tr><th>Client name</th><th>Task name</th></tr>";
      $.each(data, function(i, item) {                            
        html += "<tr><td>"+item.crm_company_name+"</td><td>"+item.subject+"</td></tr>";              
      });
      if(html != ""){
        $(".tbl-custom-timer").append(html);
        $('#modal_long_timer').modal('toggle');
      }
      return false;
    }
  }

  $(function() {
    $(".user-profile.header-notification > ul").hide();
    $(document).on('click', '.user-profile.header-notification',function(){  
      $(".user-profile.header-notification > ul").toggle();     
    }); 
  });

  $( document ).ready(function() {

    

    var flag = "";
    
    setInterval(function(){ 
      if(flag == "" || flag == 1){
        console.log("New request");
        flag = 0;
        $.ajax({
          url  : '<?php echo base_url();?>user/long_running_tasks',           
          type : 'POST',
          dataType: "json",
          success: function(data) {   
            console.log(data);
            if(data != ""){
              if(localStorage.getItem("last_updated") != null){
                var dif = (Date.now() - parseInt(localStorage.getItem("last_updated")) ) / 1000 / 60 / 60;
                
                if( dif >= 3){
                  localStorage.setItem("last_updated", Date.now());
                  alert("Yes1");
                  cpopup(data);              
                }
              }else{
                localStorage.setItem("last_updated", Date.now());
                alert("Yes2");
                cpopup(data);
              }
            }    
            flag = 1;      
          },
          error: function() {
            flag = 0;
          }
        });
      }else{
        flag = 0;
        console.log("Skip this call as last request is still pending");
      }
    },600000);    
    //}, 18000);    
  });
</script>
<style>
table th, table td{
	text-align: center
}
</style>
<!-- Modal -->
<div id="modal_custom_timer" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-timer">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header modal-header-timer">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">You are already working on</h4>
      </div>
      <div class="modal-body modal-body-timer">
        <table class="tbl-custom-timer"></table>
        <input type="hidden" id="this_timer" value="">
        <input type="button" id="kill_timers" class="btn btn-default" value="Stop All">
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>
  </div>
</div>

<!-- Modal -->
<div id="modal_long_timer" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-long-timer">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">You have been working on</h4>
      </div>
      <div class="modal-body">
        <table class="tbl-custom-timer"></table>
        <input type="button" id="continue_long_timer" class="btn btn-default" data-dismiss="modal" value="Continue">
        <input type="button" id="stop_long_timer" class="btn btn-default" value="Stop Timer">
      </div>
    </div>
  </div>
</div>
