<!DOCTYPE html>
<html lang="en">

<head>
	<title>CRM </title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="#">
	<meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
	<meta name="author" content="#">
	<link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon">
	<link href="<?php echo base_url(); ?>assets/cdns/css.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icon/themify-icons/themify-icons.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/cdns/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/common_style.css?ver=4">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/common_responsive.css?ver=2">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/ui_style.css?ver=3">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/menu-search/css/component.css">
	<link rel="stylesheet" type="text/css" href="<?php echo  base_url() ?>/assets/icon/simple-line-icons/css/simple-line-icons.css">
	<link rel="stylesheet" type="text/css" href="<?php echo  base_url() ?>/assets/icon/icofont/css/icofont.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/mobile_responsive.css?ver=2">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dropdown.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/css/dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/custom/opt-profile.css?<?php echo time(); ?>">
	<style>
		.common_form_section1 .col-xs-12.col-sm-6.col-md-6 {
			float: left !important;
		}

		.common_form_section1 {
			background: #fff;
			box-shadow: 1px 2px 5px #dbdbdb;
			border-radius: 5px;
			padding: 30px;
		}

		.common_form_section1 label {
			display: block;
			margin-bottom: 7px;
			font-size: 15px;
		}

		.common_form_section1 input {
			width: 100%;
		}

		.col-sm-12 {
			float: left;
		}

		.col-sm-8 {
			float: left;
		}
	</style>
</head>

<body>
	<div class="LoadingImage">
		<div class="preloader3 loader-block">
			<div class="circ1 loader-primary loader-md"></div>
			<div class="circ2 loader-primary loader-md"></div>
			<div class="circ3 loader-primary loader-md"></div>
			<div class="circ4 loader-primary loader-md"></div>
		</div>
	</div>
	<style>
		.LoadingImage {
			display: none;
			position: fixed;
			z-index: 100;
			/*background-image : url('<?php echo site_url() ?>assets/images/ajax-loader.gif');*/
			background-color: rgba(65, 56, 57, 0.4);
			opacity: 1;
			background-repeat: no-repeat;
			background-position: center;
			left: 0;
			bottom: 0;
			right: 0;
			top: 0;
		}
	</style>
	<!-- Pre-loader start -->
	<div class="theme-loader">
		<div class="ball-scale">
			<div class='contain'>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
			</div>
		</div>
	</div>

	<?php //} 
	?>
	<!-- Pre-loader end -->
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<nav class="navbar header-navbar pcoded-header">
				<div class="navbar-wrapper">
					<div class="navbar-logo">
						<a class="mobile-menu" id="mobile-collapse" href="javascript:void(0);">
							<i class="ti-menu"></i>
						</a>
						<a href="<?php echo base_url(); ?>">
							<img class="img-fluid" src="<?php echo base_url(); ?>assets/images/color_logo.png" alt="Theme-Logo" />
						</a>
						<a class="mobile-options">
							<i class="ti-more"></i>
						</a>
					</div>
					<button id="togglesidebar">
						<span class="toggle-bar"></span>
						<span class="toggle-bar"></span>
						<span class="toggle-bar"></span>
					</button>
					<div class="navbar-container container-fluid">
						<div class="nav-left">
							<ul class="data1" id="owl-demo1">
								<li class="item atleta comclass"><a href="<?php echo base_url() ?>Overview" class=" mydisk-img1">
										<img class="menicon" src="<?php echo base_url(); ?>assets/images/m1.jpg" alt="menuclsicon" />My Desk</a>
								</li>
								<li class="item atleta comclass"><a href="<?php echo base_url() ?>Invoices" class=" mydisk-img1">
										<img class="menicon" src="<?php echo base_url(); ?>uploads/menu_icons/1551713659.jpg" alt="menuclsicon" />Invoice</a>
								</li>
								<li class="item atleta comclass"><a href="<?php echo base_url() ?>Documents" class=" mydisk-img1">
										<img class="menicon" src="<?php echo base_url(); ?>uploads/menu_icons/1551700627.jpg" alt="menuclsicon" />Documents</a>
								</li>
								<li class="item atleta comclass"><a href="<?php echo base_url() ?>Tickets" class=" mydisk-img1">
										<img class="menicon" src="<?php echo base_url(); ?>uploads/menu_icons/1551700316.jpg" alt="menuclsicon" />Tickets</a>
								</li>

								<li class="item atleta comclass"><a href="<?php echo base_url() ?>user_chat" class=" mydisk-img1">
										<img class="menicon" src="<?php echo base_url(); ?>uploads/menu_icons/1551713659.jpg" alt="menuclsicon" />Chats</a>
								</li>


							</ul>
						</div>
						<div class="nav-right">
							<ul style="max-width: 200px ;">
								<li class="user-profile header-notification" style="width: 230px; float: right;">
									<a href="#!">
										<?php $getUserProfilepic = $this->Common_mdl->getUserProfilepic($_SESSION['client_uid']); ?>
										<img style="margin-right: 2px;" src="<?php echo $getUserProfilepic; ?>" alt="img">
										<span style="width: 140px; max-width: 200px; text-align: right;"><?php echo $_SESSION['username'] ?> </span>
										<i class="ti-angle-down"></i>
									</a>
									<ul class="tgl-opt">
										<li class="atleta">
											<a class="show_profile_content" href="javascript:void(0);">
												<i class="ti-layout-sidebar-left"></i>Profile
											</a>
										</li>
										<li class="atleta">
											<a href="<?php echo base_url() ?>login/logout">
												<i class="ti-layout-sidebar-left"></i> Logout
											</a>
										</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</nav>

			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">