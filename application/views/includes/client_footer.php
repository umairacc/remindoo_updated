<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/jquery/js/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<!--     <script src="<?php echo base_url(); ?>assets/js/timezones.full.js"></script> -->
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- j-pro js -->
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/pages/j-pro/js/jquery.ui.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/cdns/jquery.dataTables.min.js"></script>
<!-- <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.2/js/dataTables.fixedHeader.min.js"></script> -->
<!--    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.min.js"></script> -->
<!--   <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/daterangepicker.js"></script> -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datedropper.min.js"></script>
<!-- <script type="text/javascript" src="https://remindoo.uk/assets/js/masonry.pkgd.min.js"></script> -->
<!-- Custom js -->
<!--    <script src="<?php echo base_url(); ?>assets/pages/wysiwyg-editor/wysiwyg-editor.js"></script>  -->
<script src="<?php echo base_url(); ?>assets/js/pcoded.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/demo-12.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common_script.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<!--   <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom-picker.js"></script>  -->
<script src="<?php echo base_url(); ?>assets/js/mock.js"></script>
<script src="<?php echo base_url(); ?>assets/js/hierarchy-select.min.js"></script>
<!-- <script src="https://use.fontawesome.com/86c8941095.js"></script> -->
<script src="<?php echo base_url(); ?>assets/js/bootbox.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/debounce.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<!-- 
<script src="<?php echo base_url(); ?>assets/js/bootstrap-slider.min.js"></script> -->
<script src="<?php echo base_url(); ?>assets/cdns/spectrum.min.js"></script>
<script src="<?php echo base_url(); ?>assets/cdns/medium-editor.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/creative.tools.js"></script>
<script src="<?php echo base_url(); ?>assets/js/html2canvas.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image-edit.js"></script>
<script src="<?php echo base_url(); ?>assets/js/editor.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/email.css">
<script src="<?php echo base_url(); ?>assets/cdns/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/cdns/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dropdown.js"></script>
<script type="text/javascript">
      $(function() {

            var headheight = $('.header-navbar').outerHeight();
            var navbar = $('.remin-admin');
      });
</script>

<script type="text/javascript">
      $(document).ready(function() {
            $("a#mobile-collapse").click(function() {
                  $('#pcoded.pcoded.iscollapsed').toggleClass('addlay');
            });

            $(".nav-left .data1 li.comclass a.com12").click(function() {
                  $(this).parent('.nav-left .data1 li.comclass').find('ul.dropdown-menu1').slideToggle();
                  $(this).parent('.nav-left .data1 li.comclass').siblings().find('ul.dropdown-menu1:visible').slideUp();

            });

            var test = $('body').height();
            $('test').scrollTop(test);

            $(document).on('show.bs.modal', '.modal', function(event) {
                  var zIndex = 1040 + (10 * $('.modal:visible').length);
                  $(this).css('z-index', zIndex);
                  setTimeout(function() {
                        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                  }, 0);
            });

            $('.modal').on("shown.bs.modal", function() {
                  var winheight1 = $(this).find('.modal-dialog').outerHeight();

                  if (winheight1 < 400) {
                        $(this).find('.modal-dialog').addClass('normal_heights');
                  }
            });

            $('.modal').on("hide.bs.modal", function() {
                  var winheight1 = $(this).find('.modal-dialog').outerHeight();

                  if (winheight1 < 400) {
                        $(this).find('.modal-dialog').removeClass('normal_heights');
                  }
            });

      });
</script>