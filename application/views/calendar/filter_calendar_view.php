<?php
if(isset($getclient_event)) {
    if($getclient_event['crm_confirmation_statement_due_date'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' => $getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_confirmation_statement_due_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$getclient_event['user_id'].' '.'service-conformation-statement'.' '.'search-crm_confirmation_statement_due_date'],  
                              );
  }
  if($getclient_event['crm_ch_accounts_next_due'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' => $getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_ch_accounts_next_due'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$getclient_event['user_id'].' '.'service-accounts'.' '.'search-crm_ch_accounts_next_due'],
                              );
  }
  if($getclient_event['crm_accounts_tax_date_hmrc'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' =>$getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_accounts_tax_date_hmrc'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                    'className'=> ['event-'.$getclient_event['user_id'].' '.'service-company-tax'.' '.'search-crm_accounts_tax_date_hmrc'],  
                              );
  }
  if($getclient_event['crm_personal_due_date_return'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' => $getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_personal_due_date_return'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$getclient_event['user_id'].' '.'service-personal-tax'.' '.'search-crm_personal_due_date_return'],         
                              );
  }
  if($getclient_event['crm_vat_due_date'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' => $getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_vat_due_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$getclient_event['user_id'].' '.'service-vat-return'.' '.'search-crm_vat_due_date'], 
                              );
  }
  if($getclient_event['crm_rti_deadline'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' => $getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_rti_deadline'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$getclient_event['user_id'].' '.'service-payroll'.' '.'search-crm_rti_deadline'],    
                              );
  }
  if($getclient_event['crm_pension_subm_due_date'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' => $getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_pension_subm_due_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$getclient_event['user_id'].' '.'service-work-place-pension-ae'.' '.'search-crm_pension_subm_due_date'],    
                              );
  }
  if($getclient_event['crm_next_p11d_return_due'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' => $getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_next_p11d_return_due'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$getclient_event['user_id'].' '.'service-p11d'.' '.'search-crm_next_p11d_return_due'],    
                              );
  }
  if($getclient_event['crm_next_manage_acc_date'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' => $getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_next_manage_acc_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$getclient_event['user_id'].' '.'service-management-account'.' '.'search-crm_next_manage_acc_date'],  
                              );
  }
  if($getclient_event['crm_next_booking_date'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' => $getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_next_booking_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$getclient_event['user_id'].' '.'service-bookkeeping'.' '.'search-crm_next_booking_date'],
                              );
  }
  if($getclient_event['crm_insurance_renew_date'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' => $getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_insurance_renew_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$getclient_event['user_id'].' '.'service-investigation-insurance'.' '.'search-crm_insurance_renew_date'],  
                              );
  }
  if($getclient_event['crm_registered_renew_date'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' => $getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_registered_renew_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$getclient_event['user_id'].' '.'service-registed-address'.' '.'search-crm_registered_renew_date'], 
                              );
  }
  if($getclient_event['crm_investigation_end_date'] != '') {
    $client_details[] = array('id' => $getclient_event['user_id'],
                                  'title' => $getclient_event['crm_legal_form'].'-'.$getclient_event['crm_company_name'],
                                  'start' => $getclient_event['crm_investigation_end_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$getclient_event['user_id'] .' '.'service-tax-investigation'.' '.'search-crm_investigation_end_dayte'],  
                              );
  }

}
  

  $client_json = json_encode($client_details);

  echo $client_json;
//print_r($client_json);
//print_r($getclient_event['crm_company_name']);
?>


