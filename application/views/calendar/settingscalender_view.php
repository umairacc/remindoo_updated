<?php $this->load->view('includes/header');?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fullcalendar12.css"> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fullcalendar.print.css" media='print'>
<link rel="stylesheet" type="text/css" href="https://kidsysco.github.io/jquery-ui-month-picker/MonthPicker.min.css"> 
<link href="<?php echo base_url();?>assets/css/timepicki.css" rel="stylesheet">
<style type="text/css">
  .row.append_notify
  {
    padding-top: 10px;
  }
  .row.for_extra_email_fields
  {
    padding-top: 10px;
  }
  li.parsley-type
  {
    color: red;
  }
  .edit_newevent_form .modal-body {
    max-height: calc(100vh - 180px);
       overflow: auto;
    }
</style>
<div class="pcoded-content floating_datas">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body floating_set">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
                  <!--start-->
            <?php echo $this->session->flashdata('success'); ?>    
                  <div class="col-sm-12 common_form_section1 was-sucess edit-tsk">
                  <div class="deadline-crm1 floating_set single-txt12">
                     <ul class="nav nav-tabs all_user1 md-tabs pull-left">
                        <li class="nav-item">
                           Settings
                         <div class="slide"></div>
                        </li>
                     </ul>
                  </div> 

                     <div class="setting-calender floating_set">
                            <div class="setting-search button_searchs">
                              
                    <?php $getallUser=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' order by id DESC")->result_array();
                    ?>     
                      <div class="form-group ">       
                        <label>Client</label>
                        <div class="dropdown-sin-5">
                        <select id="company_name" name="company_name" placeholder="Select Company">
                          <option value="">All</option>
                          <?php  foreach ($getallUser as $key => $value) {
                        $client_info=$this->db->query("SELECT * FROM client where user_id='".$value['id']."'" )->result_array(); 
                        if($client_info[0]['crm_company_name'] != '') { ?>
                            <option value=<?php echo $client_info[0]['user_id'];?>><?php if($client_info[0]['crm_company_name'] != '') { echo $client_info[0]['crm_company_name']; } ?></option>
                        
                      <?php  } } ?> 
                        </select>  
                        </div>
                      </div>       
                              
                      <div class="form-group">
                          <label>Type</label>
                          <!-- <select id="company_type" name="company_type" placeholder="Select Company Type">
                          <option value="">All</option>
                      <?php foreach ($getallUser as $key => $value1) {
                        $client_info1=$this->db->query("SELECT * FROM client where user_id='".$value1['id']."'" )->result_array();

                        if($client_info1[0]['crm_company_type'] != '') { ?>
                         ?>
                          <option value=<?php echo $client_info1[0]['crm_company_type'];?>><?php if($client_info1[0]['crm_company_type']!='') { echo $client_info1[0]['crm_company_type']; } ?></option>
                      <?php  } } ?>
                        </select> -->
                        <div class="dropdown-sin-6">
                        <select id="company_type" name="company_type" placeholder="Select Company Type">
                          <option value="">All</option>
                          <option value="2" data-id="Private Limited company">Private Limited company</option>
                           <option value="3" data-id="Public Limited company">Public Limited company</option>
                           <option value="4" data-id="Limited Liability Partnership">Limited Liability Partnership</option>
                           <option value="5" data-id="Partnership">Partnership</option>
                           <option value="6" data-id="Self Assessment">Self Assessment</option>
                           <option value="7" data-id="Trust">Trust</option>
                           <option value="8" data-id="Charity">Charity</option>
                           <option value="9" data-id="Other">Other</option>
                        </select>  
                        </div> 
                      </div>                              
                              <button class="btn btn-primary search_btn" id="search_btn">search</button>
                              <button class="btn btn-success addnew_event_popup" data-toggle="modal" data-target="#addnewcontact">Add new Event</button>

                               <button class="btn btn-success addnew_meeting" data-toggle="modal" data-target="#addnewmeeting">Add Meeting</button>
                            </div>
                                               
                                       <div class="col-xl-3 col-md-12">
                                          <div id="external-events" class="post_search">
                                                  <h6 class="post-titles">My Calenders</h6>
                                                  <!-- for calender events -->
                                              <div class="calendar-checks">
                                                      <label class="custom_checkbox1">
                                                          <input type="checkbox" class="calender_checkbox reminders_check" value="on" name="events_chks" id="event_allevent" data-id="all_event" checked="checked" >
                                                          <i></i>
                                                        </label>
                                                          <span>All Events</span>
                                              </div>
                                             
                                              <div class="calendar-checks">
                                                      <label class="custom_checkbox1">
                                                          <input type="checkbox" class="calender_checkbox reminders_check" name="events_chks" id="event_reminder" value="on" data-id="reminder" >
                                                          <i></i>
                                                          </label>
                                                         <span>Reminders</span>
                                                      </label>
                                                  </div>

                                              <div class="calendar-checks">
                                                      <label class="custom_checkbox1">
                                                          <input type="checkbox" class="calender_checkbox reminders_check" name="events_chks" id="event_meetings" value="on" data-id="meetings" >
                                                          <i></i></label>
                                                         <span>Meetings</span>
                                                  </div>


                                              </div>
                                              <!-- end of calender events -->
                                          </div>
                                        <div class="col-xl-9 col-md-12">
                                              <div id='calendar' class="calendar post_search g_calendar"></div>
                                        </div>
                              
                                             
                     </div>
                  </div>
                  <!-- close -->
                
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
   </div>
</div>

<form name="contact_form" id="contact_form" action="<?php echo base_url()?>Calendar/addCalendar_event" method="post">
   <div class="modal fade" id="addnewcontact" role="dialog">
       <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Add New Event</h4>
               </div>
               <div class="modal-body">
               <div class="row">
                  <div class="col-xs-12 col-md-12 form-group">
                           <label>Add Title</label>
                           <input type="text" name="event_title" id="event_title" class="form-control" data-parsley-required="true" >
                        </div>
               </div>
                <div class="row">
                  <div class=" col-sm-3 no_padd form-group">
                           <input type="text" name="from_date" id="from_date" class="form-control frto_datepicker" placeholder="Start Date" data-parsley-required="true">
                  </div>
                  <div class=" col-sm-3 no_padd form-group">
                           <input type="text" name="from_timepic" id="start_timepicker1" class="form-control start_timepicker" placeholder="Start Time" >
                  </div>
                  <div class=" col-sm-1 no_pa form-group">
                        To
                  </div>                
                  <div class=" col-sm-3 no_padd form-group">
                           <input type="text" name="to_date" id="to_date" class="form-control frto_datepicker" placeholder="To Date">
                  </div>
                  <div class=" col-sm-3 no_padd form-group">
                           <input type="text" name="to_timepic" id="to_timepicker2" class="form-control start_timepicker" placeholder="To Time">
                  </div>
                </div>
                <div class="row">
                 <div class="col-sm-3 form-group">
                          <div class="checkbox-fade fade-in-primary m-t-10">
                                  <label>
                                      <input type="checkbox" class="add_event" value="on" name="all_days" id="event_allevent" data-id="all_event">
                                      <span class="cr">
                                          <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                      </span>
                                      <span>All Days</span>
                                  </label>
                          </div>
                 </div>
                 <div class="col-sm-9 form-group">
                     <select name="event_repeats" id="event_repeats" class="form-control">
                       <option value="">Doesn't Repeat</option>
                       <option value="Daily">Daily</option>
                       <option value="Weekly">Weekly</option>
                       <option value="Monthly">Monthly</option>
                       <option value="Annually">Annually</option>
                     </select>
                </div>
                </div>
                <!-- divided elemnt -->
                <div class="divi divi_cls col-xs-12 col-md-12">
                     <div class="row">                 
                        <div class="col-sm-7 form-group">
                           <label>Event Location</label>
                           <div class="sep-sec">
                               <input type="text" name="add_location" id="add_location" class="form-control" placeholder="Enter Location" >
                           </div>
                        </div>  
                        <div class="col-sm-5 form-group">
                          <label>Notification</label>
                          <div class="sep-sec">
                            <button type="button" class="btn btn-primary Notification_add">Add Notification</button>
                           </div>
                        </div>                    
                     </div>  
                     <div class="row">                 
                        <!-- <div class="col-sm-12 form-group">
                           <label>Notification</label> -->
                           <div class="notification_data">
                           <!-- div content -->
                        <!--      <div class="row append_notify">
                               <div class="col-sm-2">
                                 <select><option>asds</option></select>
                               </div>
                               <div class="col-sm-2">
                                 <input type="text" class="form-control" name="">
                               </div>
                                 <div class="col-sm-2">
                                 <select><option>asds</option></select>
                               </div>
                               <div class="col-sm-2">
                                  <button type="button" class="btn-danger form-control">X</button>
                               </div>
                             </div> -->
                             <!-- end of div content append -->
                           </div>
                           
                        <!-- </div> -->                      
                     </div> 
                     <div class="row">                 
                        <div class="col-sm-7 form-group">
                           <label>Email</label>
                           <div class="sep-sec">
                               <input type="text" name="notification_mail[]" id="notification_mail" class="form-control notification_mail"  placeholder="Enter Email Address" data-parsley-required="true" data-parsley-type="email" data-parsley-validate-full-width-characters="true"  >
                           </div>
                           <!-- <div class="extra_email_adds efefefg"></div> -->
                        </div>  
                        <div class="col-sm-5 blue_button form-group">
                          <label>Additional Info</label>
                           <div class="sep-sec">
                              <button type="button" class="btn-default extra_email_ids" >Add Email Ids</button>
                           </div>
                        </div>                    
                     </div>
                     <div class="rownewe">
                       <div class="extra_email_adds col-sm-12"></div>
                     </div>
                        <div class="rowddd">                 
                        <div class="col-sm-12 form-group">
                           <label>Description</label>
                           <div class="sep-sec">
                                 <textarea rows="5" type="text" class="form-control" name="notification_description" id="notification_description" placeholder="Write something" ></textarea>
                           </div>
                        </div>                      
                     </div>   
                </div>
                <!-- need line -->                
                <!-- <div class="divi col-xs-12 col-md-4">
                     <div class="row">                 
                        <div class="col-sm-12 form-group">
                           
                        </div>
                      
                     </div>  
                </div>  --> 
                <!-- end of divided element -->    
                                    
               </div>
               <div class="modal-footer">
                  <button type="submit" name="button"  class="btn btn-primary" id="submitBtn">save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
             </div>
         </div>
   </div>
  </form> 
 <!-- modal close-->



 <!-- Modal -->
 <form name="contact_formzz" id="contact_formzz" action="<?php echo base_url()?>Calendar/addCalendar" method="post">
   <div class="modal fade" id="addnewcontact_old" role="dialog">
       <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Add New Event</h4>
               </div>
               <div class="modal-body">
                     <div class="divi col-xs-12 col-md-6">
                        <div class=" form-group">
                           <label>Add Title</label>
                           <select name="designation" class="form-control repeats valid" id="repeats" data-date="" data-day="" data-month="" data-parsley-required="true">
                              <option value="">Select Designation</option>
                              <option value="1">sales</option>
                              <option value="2">business</option>
                              <option value="3">low officer</option>
                              <option value="4">typist</option>
                              <option value="5">director</option>
                           </select>
                        </div>
                        <div class=" form-group mr-mrs">
                           <label>First Name</label>
                           <div class="sep-sec">
                           <select name="before_surname" class="form-control repeats valid" id="repeats" data-date="" data-day="" data-month="">
                              <option value="Mr">Mr</option>
                              <option value="Ms">Ms</option>
                              <option value="Mrs">Mrs</option>
                           </select>
                           <input type="text" name="firstname" class="form-control" placeholder="Enter Firstname" data-parsley-required="true" data-parsley-minlength="3">
                           </div>
                        </div>
                        <div class=" form-group">
                           <label>Middle Name</label>
                           <input type="text" name="middlename" class="form-control" placeholder="Enter  Middlename" data-parsley-required="true" data-parsley-minlength="3">
                        </div>
                        <div class=" form-group">
                           <label>Last Name</label>
                           <input type="text" name="lastname" class="form-control" placeholder="Enter lastname" data-parsley-required="true" data-parsley-minlength="3">
                        </div>
                        <div class=" form-group">
                           <label>Contact Email</label>
                           <input type="email" name="contact_email" class="form-control" placeholder="Enter Name" data-parsley-trigger="change" data-parsley-required="true">
                        </div>
                        <div class=" form-group">
                           <label>Phone</label>
                           <input type="text" name="phone" class="form-control" placeholder="Enter Name" data-parsley-required="true" data-parsley-length="[10,10]">
                        </div>
                     </div>    
                      <div class="divi col-xs-12 col-md-6">
                        <div class=" form-group">
                           <label>Address Line 1</label>
                           <textarea name="addressline1" data-parsley-required="true"></textarea>
                        </div>
                        <div class=" form-group">
                           <label>Address Line 2</label>
                           <textarea name="addressline2" data-parsley-required="true"></textarea>
                        </div>
                        <div class=" form-group">
                           <label>City/Town</label>
                           <input type="text" name="city_town" class="form-control" placeholder="Enter Name" data-parsley-required="true">
                        </div>
                        <div class=" form-group">
                           <label>Postcode</label>
                           <input type="text" name="postcode" class="form-control" placeholder="Enter Name" data-parsley-required="true">
                        </div>
                        <div class=" form-group">
                           <label>County</label>
                           <input type="text" name="county" class="form-control" placeholder="Enter Name" data-parsley-required="true">
                        </div>
                        <div class=" form-group">
                          <label>Country</label>
                          <select name="country" class="form-control repeats valid" id="repeats" data-date="" data-day="" data-month="" data-parsley-required="true">
                      <?php $country = $this->db->query('SELECT * FROM countries')->result_array();
                        if(!empty($country)) { ?>
                          <option value="">Select Country</option>
                        <?php foreach ($country as $key => $value) { ?> 
                            
                            <option value="<?php echo $value['name'];?>"><?php echo $value['name'];?></option>
                      <?php } } ?> 
                          </select>
                        </div>
                      </div>                 
               </div>
               <div class="modal-footer">
                  <button type="submit" name="button"  class="btn btn-primary" id="submitBtn">save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
             </div>
         </div>
   </div>
  </form> 
 <!-- modal close-->

 <!-- Modal -->
 <form name="cmytype_form" id="cmytype_form" action="#" method="post">
   <div class="modal fade showcmy-type-pop" id="showcmy-type-pop" role="dialog">
       <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Add New Event</h4>
               </div>
               <div class="modal-body">
                     <div class="divi col-xs-12 col-md-6">
                        <div class="form-group">
                           <label>Company Name</label>
                           <input type="text" class="form-control cmyname" id="cmyname">
                        </div>
                        <div class=" form-group">
                           <label>Email</label>
                           <div class="sep-sec">
                           <input type="text" class="form-control cmyemail" id="cmyemail">
                           </div>
                        </div>
                         <div class=" form-group">
                           <label>Service Name</label>
                           <input type="text" class="form-control service-name" id="service-name">
                        </div>
                        
                     </div>    
                      <div class="divi col-xs-12 col-md-6">
                        <div class=" form-group">
                           <label>Company Type</label>
                           <input type="text" class="form-control cmytype" id="cmytype">
                        </div>
                        <div class=" form-group">
                           <label>Created</label>
                           <input type="text" class="form-control cmycreated" id="cmycreated">
                        </div>
                        
                        <div class=" form-group">
                           <label>Service Due Date</label>
                           <input type="text" class="form-control service-due" id="service-due">
                        </div>
                        <!-- <div class=" form-group">
                           <label>City/Town</label>
                           <input type="text" name="city_town" class="form-control" placeholder="Enter Name" data-parsley-required="true">
                        </div> -->
                        
                        
                      </div>                 
               </div>
               <!-- <div class="modal-footer">
                  <button type="submit" name="button"  class="btn btn-primary" id="submitBtn">save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div> -->
             </div>
         </div>
   </div>
  </form> 
 <!-- modal close-->   


  <?php 

  foreach ($getClient_info as $key => $get_client) {  

    ?>    
<input type="hidden" class="client_cmy_id" value="<?php echo $get_client['user_id'] ?>">
  <?php   
     if ($get_client['crm_confirmation_statement_due_date'] !='') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_confirmation_statement_due_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',  
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-conformation-statement'.' '.'search-crm_confirmation_statement_due_date'],  
                              );
      }
      if ($get_client['crm_ch_accounts_next_due'] !='') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_ch_accounts_next_due'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff', 
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-accounts'.' '.'search-crm_ch_accounts_next_due'],     
                              );
      }
      if ($get_client['crm_accounts_tax_date_hmrc'] !='') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_accounts_tax_date_hmrc'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-company-tax'.' '.'search-crm_accounts_tax_date_hmrc'],       
                              );
      }
      if ($get_client['crm_personal_due_date_return'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_personal_due_date_return'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-personal-tax'.' '.'search-crm_personal_due_date_return'],            
                              );
      }
      if ($get_client['crm_vat_due_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_vat_due_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-vat-return'.' '.'search-crm_vat_due_date'],            
                              );
      }
      if ($get_client['crm_rti_deadline'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_rti_deadline'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-payroll'.' '.'search-crm_rti_deadline'],            
                              );
      }
      if ($get_client['crm_pension_subm_due_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_pension_subm_due_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-work-place-pension-ae'.' '.'search-crm_pension_subm_due_date'],             
                              );
      }
      if ($get_client['crm_next_p11d_return_due'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_next_p11d_return_due'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-p11d'.' '.'search-crm_next_p11d_return_due'],            
                              );
      }
      if ($get_client['crm_next_manage_acc_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_next_manage_acc_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-management-account'.' '.'search-crm_next_manage_acc_date'],            
                              );
      }
      if ($get_client['crm_next_booking_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_next_booking_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-bookkeeping'.' '.'search-crm_next_booking_date'],            
                              );
      }
      if ($get_client['crm_insurance_renew_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_insurance_renew_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff' ,
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-investigation-insurance'.' '.'search-crm_insurance_renew_date'],            
                              );
      }
      if ($get_client['crm_registered_renew_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_registered_renew_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-registed-address'.' '.'search-crm_registered_renew_date'],            
                              );
      }
      if ($get_client['crm_investigation_end_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_investigation_end_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'] .' '.'service-tax-investigation'.' '.'search-crm_investigation_end_dayte'],           
                                  
                              );
      }
                           
}
/** timeline schedule id 19-09-2018 **/
if(isset($timeline_schedule_data) && $timeline_schedule_data!='')
{
  foreach ($timeline_schedule_data as $sch_key => $sch_value) {
    $start_date=$sch_value['comments_for_reference'];
      $title=$this->Common_mdl->get_field_value('client','crm_company_name','user_id',$sch_value['user_id']);
    if($title==''){
      $title=$this->Common_mdl->get_crm_name($sch_value['user_id']);
    }
     $client_details[] = array('id' => 'sch_'.$sch_value['id'],
                          'title' => $title,
                          'start' => $start_date,
                          'editable' => true,
                          'borderColor'=> '#FC6180',
                          'backgroundColor'=> '#FC6180',
                          'textColor'=>'#fff',
                          'className'=> ['sch_vent-'.$sch_value['id'].' '.'for_timeline_services'],
                          'idName'=>['rspt'],
                          );   
    ?>
     <!-- for timeline schedule popup -->
         <div class="modal fade timeline_services_popup" id="timeline_services_popup-<?php echo $sch_value['id']; ?>" role="dialog">
             <div class="modal-dialog modal-md">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Schedule Timeline</h4>
                     </div>
                     <div class="modal-body">
                       <div class="divi col-xs-12 col-md-12">                        
                          <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Client/Company Name</label></div>
                             <div class="col-sm-8"><?php echo $title;?></div>
                          </div>
                          <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Notes</label></div>
                             <div class="col-sm-8"><?php print_r($sch_value['notes']); ?></div>
                          </div>
                       </div>             
                     </div>
              
                   </div>
               </div>
         </div>
    <!-- endof timeline schedule popup -->
    <?php 
  }
}

if(isset($timeline_schedule_id) && $timeline_schedule_id!='' && is_numeric($timeline_schedule_id))
{
  $get_schedule_data=$this->db->query('select * from timeline_services_notes_added where id='.$timeline_schedule_id.' ')->row_array();
  if(count($get_schedule_data)>0)
  {
    $start_date=date('Y-m-d');
    if($get_schedule_data['comments_for_reference']!='')
    {
      $start_date=$get_schedule_data['comments_for_reference'];
    }
    $title=$this->Common_mdl->get_field_value('client','crm_company_name','user_id',$get_schedule_data['user_id']);
    if($title==''){
      $title=$this->Common_mdl->get_crm_name($get_schedule_data['user_id']);
    }
   if($get_schedule_data['comments_for_reference']=='') // avoid dupliaction in update option records
    {
  $client_details[] = array('id' => 'sch_'.$get_schedule_data['id'],
                          'title' => $title,
                          'start' => $start_date,
                          'editable' => true,
                          'borderColor'=> '#FC6180',
                          'backgroundColor'=> '#FC6180',
                          'textColor'=>'#fff',
                          'className'=> ['sch_vent-'.$get_schedule_data['id'].' '.'for_timeline_services'],
                          'idName'=>['rspt'],
                          );

    }
  }
}
/** end of timeline scedule id 19-09-2018 **/
/** for task remindewr section 20-09-2018**/
if(isset($get_task_reminders) && count($get_task_reminders)>0)
{
  foreach ($get_task_reminders as $task_key => $task_value) {
    $get_reminder_datetime=$task_value['for_reminder_chk_box'];
    $dates_time=array();
    $res_array=(array)json_decode($get_reminder_datetime);
    $reminders_dates=array_values($res_array);
    if(count($reminders_dates)>0)
    {
      foreach ($reminders_dates as $rem_key => $rem_value) {
        $res=explode('//',$rem_value);
        $time = $res[1];
$timeto24 = DateTime::createFromFormat('g:i a', $time);
$change_time=$timeto24->format('H:i:s');//17:04
$changedate_time=$res[0]."T".$change_time;
$client_details[] = array('id' => 'taskreminder_'.$task_value['id'],
                          'title' => $task_value['subject'],
                          'start' => $changedate_time,
                          'editable' => false,
                          'borderColor'=> '#FFB64D',
                          'backgroundColor'=> '#FFB64D',
                          'textColor'=>'#fff',
                          'className'=> ['taskreminder-'.$task_value['id'].' '.'for_reminder_task'],
                          
                          );
      }
    }
    ?>
       <!-- for reminder task popup -->
         <div class="modal fade reminder_task_popup" id="reminder_task_popup-<?php echo $task_value['id']; ?>" role="dialog">
             <div class="modal-dialog modal-md">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">TasK Reminder</h4>
                     </div>
                     <div class="modal-body">
                       <div class="divi col-xs-12 col-md-12">                        
                          <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Task Name</label></div>
                             <div class="col-sm-8"><?php echo $task_value['subject'];?></div>
                          </div>
                          <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Description</label></div>
                             <div class="col-sm-8"><?php echo ($task_value['description']!='')?$task_value['description']:'-'; ?></div>
                          </div>
                       </div>             
                     </div>
              
                   </div>
               </div>
         </div>
    <!-- endof reminder task popup -->
    <?php
  }
}
/** end of reminder for task section  20-09-2018 **/
/** reminder for leads section  20-09-2018 **/
if(isset($get_leads_reminders) && count($get_leads_reminders)>0)
{
foreach ($get_leads_reminders as $leads_key => $leads_value) {
     $leads_name=$this->Common_mdl->get_field_value('leads','name','id',$leads_value['lead_id']);
  $client_details[] = array('id' => 'leads_'.$leads_value['id'],
                          'title' => $leads_name,
                          'start' => date('Y-m-d',strtotime($leads_value['reminder_date'])),
                          'editable' => false,
                          'borderColor'=> '#FFB64D',
                          'backgroundColor'=> '#FFB64D',
                          'textColor'=>'#fff',
                          'className'=> ['leads-'.$leads_value['id'].' '.'for_reminder_leads'],
                           );

  ?>
   <!-- for reminder task popup -->
        <div class="modal fade reminder_leads_popup" id="reminder_leads_popup-<?php echo $leads_value['id']; ?>" role="dialog">
             <div class="modal-dialog modal-md">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Leads Reminders</h4>
                     </div>
                     <div class="modal-body">
                       <div class="divi col-xs-12 col-md-12">                        
                          <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Leads Name</label></div>
                             <div class="col-sm-8"><?php echo $leads_name;?></div>
                          </div>
                          <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Description</label></div>
                             <div class="col-sm-8"><?php echo ($leads_value['description']!='')?$leads_value['description']:'-'; ?></div>
                          </div>
                       </div>             
                     </div>
              
                   </div>
               </div>
        </div>
    <!-- endof reminder task popup -->
  <?php
  }
}
/** end of for reminder leads section 20-09-2018 **/


/* Meeting Section */

if(isset($get_meeting_event) && count($get_meeting_event)>0)
{
foreach ($get_meeting_event as $meeting_key => $meeting_value) {
    
  $client_details[] = array('id' => 'meeting_'.$meeting_value['id'],
                          'title' => 'Meeting',
                          'start' => date('Y-m-d',strtotime($meeting_value['date'])),
                          'editable' => false,
                          'borderColor'=> '#FFB64D',
                          'backgroundColor'=> '#FFB64D',
                          'textColor'=>'#fff',
                          'className'=> ['meeting-'.$meeting_value['id'].' '.'for_meeting'],
                           );

  ?>
   <!-- for reminder task popup -->
        <div class="modal fade meeting_popup" id="meeting_popup-<?php echo $meeting_value['id']; ?>" role="dialog">
             <div class="modal-dialog modal-md">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Meeting</h4>
                     </div>
                     <div class="modal-body">
                       <div class="divi col-xs-12 col-md-12">                        
                          <div class="form-group row name_fields">
                           <!--   <div class="col-sm-4"><label>Meeting</label></div> -->
                             <div class="col-sm-8">Client Name:
                             <?php echo $this->Common_mdl->getUserProfileName($meeting_value['user_id']); ?> 
                             </div>
                              <div class="col-sm-8">Staff Name:
                             <?php echo $this->Common_mdl->getUserProfileName($meeting_value['create_by']); ?> 
                             </div>
                               <div class="col-sm-8">Meeting Date:
                             <?php echo $meeting_value['date']; ?> 
                             </div>
                          </div>
                          <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Description</label></div>
                             <div class="col-sm-8"><?php echo ($meeting_value['notes']!='')?$meeting_value['notes']:'-'; ?></div>
                          </div>
                       </div>             
                     </div>
              
                   </div>
               </div>
        </div>
    <!-- endof reminder task popup -->
  <?php
  }
}
/** End of Meeting Section **/

/** 22-09-2018 **/
if(isset($get_newevent_data) && count($get_newevent_data)>0)
{
  foreach ($get_newevent_data as $event_key => $event_value) {

if($event_value['to_date']==''){
  if($event_value['from_time']!=''){
$time = $event_value['from_time'];
$timeto24 = DateTime::createFromFormat('g:i a', $time);
$change_time=$timeto24->format('H:i:s');//17:04
$changedate_time=date('Y-m-d',strtotime($event_value['from_date']))."T".$change_time;
}
else
{
$changedate_time=date('Y-m-d',strtotime($event_value['from_date']));
}
  if($event_value['all_days']!='')
  {
    $client_details[] = array('id' => 'fornewone'.$event_value['id'],
                          'title' => $event_value['event_title'],
                         // 'dow' => [0,1,2,3,4,5,6 ],
                          'start' => $changedate_time,
                          'editable' => false,
                          'borderColor'=> '#00BFFF',
                          'backgroundColor'=> '#00BFFF',
                          'textColor'=>'#fff',
                          'className'=> ['fornewone-'.$event_value['id'].' '.'for_add_newevent'],
                          'strick'=>false,
                          // 'range'=>array(
                          //   'start'=>'2018-10-10',
                          //   ),
                          );
  }
  else
  {
    $client_details[] = array('id' => 'fornewone'.$event_value['id'],
                          'title' => $event_value['event_title'],
                          'start' => $changedate_time,
                          'editable' => false,
                          'borderColor'=> '#00BFFF',
                          'backgroundColor'=> '#00BFFF',
                          'textColor'=>'#fff',
                          'className'=> ['fornewone-'.$event_value['id'].' '.'for_add_newevent'],                          
                          );
  }

// $client_details[] = array('id' => 'fornewone'.$event_value['id'],
//                           'title' => $event_value['event_title'],
//                           'start' => $changedate_time,
//                           'editable' => false,
//                           'borderColor'=> '#00BFFF',
//                           'backgroundColor'=> '#00BFFF',
//                           'textColor'=>'#fff',
//                           'className'=> ['fornewone-'.$event_value['id'].' '.'for_add_newevent'],
                          
//                           );
}
else
{
   if($event_value['from_time']!=''){
$time = $event_value['from_time'];
$timeto24 = DateTime::createFromFormat('g:i a', $time);
$change_time=$timeto24->format('H:i:s');//17:04
$changedate_time=date('Y-m-d',strtotime($event_value['from_date']))."T".$change_time;
}
else
{
$changedate_time=date('Y-m-d',strtotime($event_value['from_date']));
}
   if($event_value['to_time']!=''){
$time = $event_value['to_time'];
$timeto24 = DateTime::createFromFormat('g:i a', $time);
$change_time=$timeto24->format('H:i:s');//17:04
$changedate_time_end=date('Y-m-d',strtotime($event_value['to_date']))."T".$change_time;
}
else
{
$changedate_time_end=date('Y-m-d',strtotime($event_value['to_date']));
}
$client_details[] = array('id' => 'fornewone'.$event_value['id'],
                          'title' => $event_value['event_title'],
                          'start' => $changedate_time,
                          'end'=>$changedate_time_end,
                          'editable' => false,
                          'borderColor'=> '#00BFFF',
                          'backgroundColor'=> '#00BFFF',
                          'textColor'=>'#fff',
                          'className'=> ['fornewone-'.$event_value['id'].' '.'for_add_newevent'],
                          
                          );

}
?>
<!-- for reminder task popup -->
        <div class="modal fade reminder_leads_popup" id="reminder_event_popup-<?php echo $event_value['id']; ?>" role="dialog">
             <div class="modal-dialog modal-md">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="btn btn-primary for_event_change" data-id="<?php echo $event_value['id']; ?>">Change Event</button>

                        
                        <h4 class="modal-title">Event Data</h4>
                          <button type="button" class="close rsptrspt reminder_event_close_popup-<?php echo $event_value['id']; ?>" data-dismiss="modal">&times;</button>
                        <!-- <h4 class="modal-title"></h4> -->
                     </div>
                     <div class="modal-body">
                       <div class="divi col-xs-12 col-md-12">                        
                          <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Event Title</label></div>
                             <div class="col-sm-8"><?php echo $event_value['event_title'];?></div>
                          </div>
                           <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Event Timing</label></div>
                             <div class="col-sm-8"><?php 
                             if($event_value['from_date']!='')
                             {
                              echo date('d-m-Y',strtotime($event_value['from_date']));
                             }
                             if($event_value['from_time']!='')
                             {
                              echo " - ".$event_value['from_time'];
                             }
                             if($event_value['to_date']!='')
                             {
                              echo " To ".date('m-d-Y',strtotime($event_value['to_date']));
                             }
                             if($event_value['to_date']!='' && $event_value['to_time']!='')
                             {
                              echo " - ".$event_value['to_time'];
                             }
                             ?></div>
                          </div>
                          <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Event Repeats</label></div>
                             <div class="col-sm-8">
                               <div><?php echo ($event_value['event_repeats']=='on')?'All Days':''; ?></div>
                               <div><?php echo $event_value['event_repeats']; ?></div>
                              </div>
                          </div>
                          <?php if($event_value['add_location']!=''){ ?>
                          <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Event Location</label></div>
                             <div class="col-sm-8"><?php echo $event_value['add_location'];?></div>
                          </div>
                          <?php } ?>
                          <?php if($event_value['notification_status']!=''){
                            $notify_status=explode(',',$event_value['notification_status']);
                            $notify_timing=explode(',',$event_value['notification_timing']);
                            $notify_notes=explode(',',$event_value['notification_notes']);
                           ?>
                          <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Event Notification</label></div>
                             <div class="col-sm-8">
                              <?php 
                                foreach ($notify_status as $notify_key => $notify_value) {
                                  ?>
                                  <div class="col-sm-3"><?php echo $notify_value; ?></div>
                                  <div class="col-sm-2"><?php echo (isset($notify_timing[$notify_key]))?$notify_timing[$notify_key]:''; ?></div>
                                  <div class="col-sm-2"><?php echo (isset($notify_notes[$notify_key]))?$notify_notes[$notify_key]:''; ?></div>
                                  <?php                                  
                                }
                              ?>
                              </div>
                          </div>
                          <?php } ?>
                          <?php if($event_value['notification_mail']){
                            $res_mails=explode(',',$event_value['notification_mail']);
                           ?>
                            <div class="form-group row name_fields">
                              <div class="col-sm-4"><label>Event Notification Email</label></div>
                               <div class="col-sm-8">
                            <?php foreach ($res_mails as $mail_key => $mail_value) { ?>
                              <div><?php echo $mail_value; ?></div>
                            <?php }  ?>
                              </div>
                             </div>
                           <?php } ?>
                          <div class="form-group row name_fields">
                             <div class="col-sm-4"><label>Description</label></div>
                             <div class="col-sm-8"><?php echo ($event_value['notification_description']!='')?$event_value['notification_description']:'-'; ?></div>
                          </div>
                       </div>             
                     </div>
                     <!-- modal footer -->
                     <!-- end of modal footer -->
                  </div>
               </div>
        </div>
    <!-- endof reminder task popup -->
    <!-- edit popup -->
<form name="contact_form_edit" id="contact_form_edit" action="<?php echo base_url()?>Calendar/addCalendar_event_edit" method="post"> 
   <div class="modal fade edit_newevent_form" id="addnewcontactedit-<?php echo $event_value['id'];?>" role="dialog">
       <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Add New Event</h4>
               </div>
               <div class="modal-body">
               <div class="row">
                  <div class="col-xs-12 col-md-12 form-group">
                           <label>Add Title</label>
                           <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_value['id']; ?>">
                           <input type="text" name="event_title" id="event_title" class="form-control" data-parsley-required="true" value="<?php echo $event_value['event_title']; ?>" >
                        </div>
               </div>
                <div class="row">
                  <div class=" col-sm-2 form-group">
                           <input type="text" name="from_date" id="from_date" class="form-control frto_datepicker" placeholder="Start Date" data-parsley-required="true" value="<?php echo $event_value['from_date']; ?>">
                  </div>
                  <div class=" col-sm-2 form-group">
                           <input type="text" name="from_timepic" id="start_timepicker1" class="form-control start_timepicker" placeholder="Start Time" value="<?php echo $event_value['from_time']; ?>" >
                  </div>
                  <div class=" col-sm-1 form-group">
                        To
                  </div>                
                  <div class=" col-sm-2 form-group">
                           <input type="text" name="to_date" id="to_date" class="form-control frto_datepicker" placeholder="To Date" value="<?php echo $event_value['to_date']; ?>">
                  </div>
                  <div class=" col-sm-2 form-group">
                           <input type="text" name="to_timepic" id="to_timepicker2" class="form-control start_timepicker" placeholder="To Time" value="<?php echo $event_value['to_time']; ?>">
                  </div>
                </div>
                <div class="row">
                 <div class="col-sm-2 form-group">
                          <div class="checkbox-fade fade-in-primary m-t-10">
                                  <label>
                                      <input type="checkbox" class="add_event" value="on" name="all_days" id="event_allevent" data-id="all_event" <?php if($event_value['all_days']=='on'){ ?> checked="checked" <?php } ?> >
                                      <span class="cr">
                                          <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                      </span>
                                      <span>All Days</span>
                                  </label>
                          </div>
                 </div>
                 <div class="col-sm-4 form-group">
                     <select name="event_repeats" id="event_repeats" class="form-control">
                       <option value="">Doesn't Repeat</option>
                       <option value="Daily" <?php if($event_value['event_repeats']=='Daily'){ echo "selected"; } ?> >Daily</option>
                       <option value="Weekly" <?php if($event_value['event_repeats']=='Weekly'){ echo "selected"; } ?> >Weekly</option>
                       <option value="Monthly" <?php if($event_value['event_repeats']=='Monthly'){ echo "selected"; } ?> >Monthly</option>
                       <option value="Annually" <?php if($event_value['event_repeats']=='Annually'){ echo "selected"; } ?> >Annually</option>
                     </select>
                </div>
                </div>
                <!-- divided elemnt -->
                <div class="divi col-xs-12 col-md-8">
                     <div class="row">                 
                        <div class="col-sm-12 form-group">
                           <label>Event Location</label>
                           <div class="sep-sec">
                               <input type="text" name="add_location" id="add_location" class="form-control" placeholder="Enter Location" value="<?php echo $event_value['add_location']; ?>">
                           </div>
                        </div>                      
                     </div>  
                     <div class="row">                 
                        <div class="col-sm-12 form-group">
                           <label>Notification</label>
                           <div class="notification_data">
                           <!-- div content -->
                  <?php 
                  /** sample notification  **/
                  //echo $event_value['notification_status']."---";
                  if($event_value['notification_status']!='')
                  {
                    $timing=explode(',',$event_value['notification_timing']);
                    $notes=explode(',',$event_value['notification_notes']);
                    $i=1;
                    foreach (explode(',',$event_value['notification_status']) as $not_key => $not_value) { ?>
                       <div class="row_cls append_notify"><div class="col-sm-4"><select class="form-control notification_status" name="notification_status[<?php echo $i; ?>]"><option value="">None</option><option value="notification" <?php if($not_value=='notification'){ echo "selected"; }  ?> >Notification</option><option value="email" <?php if($not_value=='email'){ echo "selected"; }  ?> >Email</option></select></div><div class="col-sm-2"><input type="number" class="form-control notification_timing" name="notification_timing[<?php echo $i; ?>]" id="notification_timing" value="<?php echo (isset($timing[$not_key]))?$timing[$not_key]:'' ?>" ></div><div class="col-sm-4"><select class="form-control notification_notes" name="notification_notes[<?php echo $i; ?>]"><option value="">None</option><option value="hours" <?php if(isset($notes[$not_key]) && $notes[$not_key]=='hours' ){ echo "selected";} ?>>Hours</option><option value="minutes" <?php if(isset($notes[$not_key]) && $notes[$not_key]=='minutes' ){ echo "selected";} ?> >Minutes</option></select></div><div class="col-sm-2"><button type="button" class="btn-danger form-control remove_field">X</button></div></div>
                    <?php  
                    $i++;                    
                    }

                  }
                  ?>
    
                             <!-- end of div content append -->
                           </div>
                           <div class="sep-sec">
                            <button type="button" class="btn btn-primary Notification_add">Add Notification</button>
                           </div>
                        </div>                      
                     </div> 
                     <div class="row">                 
                        <div class="col-sm-12 form-group">
                           <label>Email</label>
                           <div class="sep-sec">
                           <?php $emailids=explode(',',$event_value['notification_mail']); ?>
                               <input type="text" name="notification_mail[]" id="notification_mail" class="form-control notification_mail"  placeholder="Enter Email Address" data-parsley-required="true" data-parsley-type="email" data-parsley-validate-full-width-characters="true" value="<?php echo (isset($emailids[0]))?$emailids[0]:''; ?>"  >
                           </div>
                           <div class="extra_email_adds">
                           <?php if(count($emailids)>0){ foreach ($emailids as $em_key => $em_value) {
                            if($em_key!=0){
                                ?>
                                    <div class="row for_extra_email_fields"><div class="col-sm-10"><input type="text" name="notification_mail[]" id="notification_mail" class="form-control notification_mail" placeholder="Enter Email" data-parsley-required="true" data-parsley-type="email" data-parsley-validate-full-width-characters="true" ></div><div class="col-sm-2"><button type="button" class="btn-danger form-control remove_field">X</button></div></div>
                                <?php
                              }
                             } } ?>
          
                           </div>
                        </div>                      
                     </div>
                        <div class="row">                 
                        <div class="col-sm-12 form-group">
                           <label>Description</label>
                           <div class="sep-sec">
                                 <textarea rows="5" type="text" class="form-control" name="notification_description-<?php echo $event_value['id']; ?>" id="notification_description" placeholder="Write something" ><?php echo $event_value['notification_description']; ?></textarea>
                           </div>
                        </div>                      
                     </div>   
                </div>
                <!-- need line -->                
                <div class="divi col-xs-12 col-md-4">
                     <div class="row">                 
                        <div class="col-sm-12 form-group">
                           <label>Additional Info</label>
                           <div class="sep-sec">
                              <button type="button" class="btn-default extra_email_ids" >Add Email Ids</button>
                           </div>
                        </div>
                      
                     </div>  
                </div>  
                <!-- end of divided element -->    
                                    
               </div>
               <div class="modal-footer">
                  <button type="submit" name="button"  class="btn btn-primary" id="submitBtn">save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
             </div>
         </div>
   </div>
</form>
 <!-- modal close-->
    <!-- end of edit popup -->
    <?php
  }
}
/** end of 22-09-2018 **/
  //print_r( $client_details );die;

  if($client_details !='') {
    $client_json = json_encode($client_details);
  } 

//echo "<pre>";
// print_r($client_json);  
  // echo "</pre>";
 ?>   
<!-- ajax loader -->
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style>




 <div class="modal fade" id="addnewmeeting" role="dialog">
    <div class="modal-dialog">
    <form action="<?php echo base_url(); ?>/Client/client_timeline_meeting_add" id="meeting_form" method="post">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Meeting</h4>
        </div>
        <div class="modal-body">
         <label>Client List</label>
<div class="dropdown-sin-2 lead-form-st">
<select placeholder="select" name="user_id" id="meeting_client" class="workers">
<?php foreach($user_client_list as $user){ ?>
<option value="<?php echo $user['id']; ?>"><?php echo $user['crm_name']; ?></option>
<?php } ?>
</select>
</div>
<textarea id="editor61" name="notes" placeholder="Start typing to leave a note...">                    
</textarea>
<span class="meeting_errormsg error_msg" style="color: red;"></span>  


<input type="text" class="datepicker"  name="meeting_date" id="meeting_date" value="">   

<input type="hidden" class=""  name="update_calllog_id" id="update_calllog_id" value=""> 
  

        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-primary meeting_add">Save</button>  
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
 --><script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- j-pro js -->
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
 --><script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 -->

<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/fullcalendar.min.js"></script>
<!-- timepicker -->
<script src="<?php echo base_url(); ?>assets/js/timepicki.js"></script>
<!-- end of timepicker -->

<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>


<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>



<script type="text/javascript">

 $('.dropdown-sin-2').dropdown({
      limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });



$( document ).ready(function() {
          CKEDITOR.editorConfig = function (config) {
          config.language = 'es';
          config.uiColor = '#fff';
          config.height = 300;
          config.toolbarCanCollapse = true;
          config.toolbarLocation = 'bottom';
          };
          
           CKEDITOR.replace( 'notification_description'); 



            CKEDITOR.editorConfig = function (config) {
          config.language = 'es';
          config.uiColor = '#fff';
          config.height = 300;
          config.toolbarCanCollapse = true;
          config.toolbarLocation = 'bottom';
          };
          
           CKEDITOR.replace( 'editor61'); 

/** for multiple ckeditor **/
<?php 
if(isset($get_newevent_data) && count($get_newevent_data)>0)
{
  foreach ($get_newevent_data as $event_key => $event_value) {
    ?>
    CKEDITOR.replace( 'notification_description-<?php echo $event_value['id']; ?>'); 
    <?php     
  }
}
?>
/** end of multiple ckeditor 27-09-2018 **/

         
        } );</script>

<script>
   var Random = Mock.Random;
   var json1 = Mock.mock({
     "data|10-50": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled|1-2": true,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
   
   $('.dropdown-mul-1').dropdown({
     data: json1.data,
     limitCount: 40,
     multipleMode: 'label',
     choice: function () {
       // console.log(arguments,this);
     }
   });
   
   var json2 = Mock.mock({
     "data|10000-10000": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled": false,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
        
   $('.dropdown-mul-2').dropdown({
     limitCount: 5,
     searchable: false
   });
   
   $('.dropdown-sin-1').dropdown({
     readOnly: true,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   
   $('.dropdown-sin-2').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
</script>
<script type="text/javascript">
   $('#timepicker1').timepicker();  
</script> 

<script type="text/javascript">
   // var Current_date = moment().format('YYYY-DD-MM');
      "use strict";
  $(document).ready(function() {
      $('#external-events .fc-event').each(function() {
          // store data so the calendar knows to render an event upon drop
          $(this).data('event', {
              title: $.trim($(this).text()), // use the element's text as the event title
              stick: true // maintain when user navigates (see docs on the renderEvent method)

          });
          // make the event draggable using jQuery UI
          $(this).draggable({
              zIndex: 999,
              revert: false, // will cause the event to go back to its
              revertDuration: 0 //  original position after the drag
          });    

      });
     $('#calendar').fullCalendar({
        
        disableDragging: true,
          header: {
              left: 'prev,next ',
              center: 'title',
              right: 'month,agendaWeek,agendaDay,listMonth,listYear'
          },
          editable: true,
          droppable: true,
          buttonText: {
              listMonth: 'List Month',
              listYear: 'List Year',
              listWeek: 'List Week',
              listDay: 'List Day'
           },

          events: 
            <?php echo $client_json; ?>,
              eventDrop: function(event,date,dayDelta,minuteDelta,allDay,revertFunc) {
             var id=event.id;
             var evnt_start=event.start.toString();
             var datestring = date.toString();
                           
             $.ajax({
                url: "<?php echo base_url(); ?>calendar/update_timeline_schedule_data",
                type: "post",
              // data: {'id':event.id,'dayDelta':dayDelta},
              data: {'id':id,'date':evnt_start,'datediff':datestring,},
              success:function(response)
              {
                //alert(response);
              }
             });
      /**
         * perform ajax call for db update
         */            

        // alert(
        //     event.title + " was moved " +
        //     dayDelta + " days and " +
        //     minuteDelta + " minutes."
        // );

        // if (allDay) {
        //     alert("Event is now all-day");
        // }else{
        //     alert("Event has a time-of-day");
        // }

        // if (!confirm("Are you sure about this change?")) {
        //     revertFunc();
        // }

           }

      });

     $(document).on('click', '.fc-event div', function() {
     // alert('zzz');
      
       var company_type = $(this).text().trim();       
       if($(this).parent('a').is('[class*="event-"]')){           

         var check = "event-";     

         var check1 = "service-";     

          var check2 = "search-";     
     // $('[class^="event-"], [class*=" event-"]').each(function () {    
        // Get array of class names   
        var cls = $(this).parent('a').attr('class').split(' ');       
        for (var i = 0; i < cls.length; i++) {
            // Iterate over the class and log it if it matches
            if (cls[i].indexOf(check) > -1) {        
                var company_type =cls[i].slice(check.length, cls[i].length);
            } 

            if (cls[i].indexOf(check1) > -1) {        
                var service =cls[i].slice(check1.length, cls[i].length);
            } 

             if (cls[i].indexOf(check2) > -1) {        
                var search_value =cls[i].slice(check2.length, cls[i].length);
            }       
        }    
 //   });
     

        var service_name=service.toUpperCase();
        var res = service_name.replace("-", " ");
      // alert(company_type); 
        // $('#showcmy-type-pop').modal('show');
        $.ajax({
        type: "POST",
        // dataType: 'json',
        url: "<?php echo base_url().'Calendar/filterCompanyType';?>",
        data: {"company_type":company_type,'search_value':search_value},
        success: function(response) { 
        //alert(response);
          $('#showcmy-type-pop').modal('show');
          
          var con = $.parseJSON(response)
           //console.log( con.crm_company_name );
          $('#cmyname').val(con.crm_company_name);
          $('#cmyemail').val(con.crm_email);
          $('#cmytype').val(con.crm_legal_form);
          $('#cmycreated').val(con.created_date);
           $('#service-name').val(res);
           if(search_value=='crm_confirmation_statement_due_date'){
             $('#service-due').val(con.crm_confirmation_statement_due_date);
           }
            if(search_value=='crm_next_manage_acc_date'){
              $('#service-due').val(con.crm_next_manage_acc_date);
           }
            if(search_value=='crm_next_booking_date'){
            $('#service-due').val(con.crm_next_booking_date);
           }
            if(search_value=='crm_vat_due_date'){
            $('#service-due').val(con.crm_vat_due_date);
           }
            if(search_value=='crm_pension_subm_due_date'){
            $('#service-due').val(con.crm_pension_subm_due_date);
           }
            if(search_value=='crm_ch_accounts_next_due'){
            $('#service-due').val(con.crm_ch_accounts_next_due);
           }
            if(search_value=='crm_accounts_tax_date_hmrc'){
            $('#service-due').val(con.crm_accounts_tax_date_hmrc);
           }
            if(search_value=='crm_personal_due_date_return'){
            $('#service-due').val(con.crm_personal_due_date_return);
           }
            if(search_value=='crm_rti_deadline'){
            $('#service-due').val(con.crm_rti_deadline);
           }
            if(search_value=='crm_next_p11d_return_due'){
            $('#service-due').val(con.crm_next_p11d_return_due);
           }
            if(search_value=='crm_insurance_renew_date'){
            $('#service-due').val(con.crm_insurance_renew_date);
           }
            if(search_value=='crm_registered_renew_date'){
            $('#service-due').val(con.crm_registered_renew_date);
           }
            if(search_value=='crm_investigation_end_date'){
            $('#service-due').val(con.crm_investigation_end_date);
           } 
          
        }
        
      }); 
          }

      });   

      /** 20-09-2018 **/
       $(document).on('click', '.for_timeline_services div', function() {
              // alert('xz');
            if($(this).parent('a').is('[class*="sch_vent-"]')){ 
              var check='sch_vent-';
              var company_type='';
                var cls = $(this).parent('a').attr('class').split(' ');       
        for (var i = 0; i < cls.length; i++) {
            // Iterate over the class and log it if it matches
            if (cls[i].indexOf(check) > -1) {        
                 company_type =cls[i].slice(check.length, cls[i].length);
            } 
        }    
        //alert(company_type);
                $('#timeline_services_popup-'+company_type).modal('show');
             }
        });
      /** end of 20-09-2018 **/ 
      /** 20-09-2018 **/
       $(document).on('click', '.for_reminder_task div', function() {
              // alert('xz');
            if($(this).parent('a').is('[class*="taskreminder-"]')){ 
              var check='taskreminder-';
              var company_type='';
                var cls = $(this).parent('a').attr('class').split(' ');       
        for (var i = 0; i < cls.length; i++) {
            // Iterate over the class and log it if it matches
            if (cls[i].indexOf(check) > -1) {        
                 company_type =cls[i].slice(check.length, cls[i].length);
            } 
        }    
        //alert(company_type);
                $('#reminder_task_popup-'+company_type).modal('show');
             }
        });
      /** end of 20-09-2018 **/ 

      


         $(document).on('click', '.for_meeting div', function() {
            //   alert('xz');
            if($(this).parent('a').is('[class*="meeting-"]')){ 
              var check='meeting-';
              var company_type='';
                var cls = $(this).parent('a').attr('class').split(' ');       
        for (var i = 0; i < cls.length; i++) {
            // Iterate over the class and log it if it matches
            if (cls[i].indexOf(check) > -1) {        
                 company_type =cls[i].slice(check.length, cls[i].length);
            } 
        }    
      //  alert(company_type);
                $('#meeting_popup-'+company_type).modal('show');
             }
        });





      /** 20-09-2018 **/
       $(document).on('click', '.for_reminder_leads div', function() {
              // alert('xz');
            if($(this).parent('a').is('[class*="leads-"]')){ 
              var check='leads-';
              var company_type='';
                var cls = $(this).parent('a').attr('class').split(' ');       
        for (var i = 0; i < cls.length; i++) {
            // Iterate over the class and log it if it matches
            if (cls[i].indexOf(check) > -1) {        
                 company_type =cls[i].slice(check.length, cls[i].length);
            } 
        }    
        //alert(company_type);
                $('#reminder_leads_popup-'+company_type).modal('show');
             }
        });
      /** end of 20-09-2018 **/ 
          $(document).on('click', '.for_add_newevent div', function() {
              // alert('xz');
            if($(this).parent('a').is('[class*="fornewone-"]')){ 
              var check='fornewone-';
              var company_type='';
                var cls = $(this).parent('a').attr('class').split(' ');       
        for (var i = 0; i < cls.length; i++) {
            // Iterate over the class and log it if it matches
            if (cls[i].indexOf(check) > -1) {        
                 company_type =cls[i].slice(check.length, cls[i].length);
            } 
        }    
        //alert(company_type);
                $('#reminder_event_popup-'+company_type).modal('show');
             }
        });
      
 
  });

</script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/parsley.css') ?>">
<script src="<?php echo base_url('assets/js/parsley.min.js') ?>"></script>
<script type="text/javascript" src="https://kidsysco.github.io/jquery-ui-month-picker/MonthPicker.min.js"></script>
<script type="text/javascript">
   $('#contact_form').parsley();
   $('#contact_form_edit').parsley();
</script>
<style type="text/css">
  .parsley-required {
  color: red;
  }
</style>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script type="text/javascript">
 $('.dropdown-sin-5').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });
 $('.dropdown-sin-6').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching1" placeholder="Search">'
   });  
</script>

<script type="text/javascript">
  $(document).ready(function() {

    <?php
    $start_date_es=date('Y-m-d');
if(isset($timeline_schedule_id) && $timeline_schedule_id!='' && is_numeric($timeline_schedule_id))
{
  $get_schedule_data=$this->db->query('select * from timeline_services_notes_added where id='.$timeline_schedule_id.' ')->row_array();
  if(count($get_schedule_data)>0)
  {
    $start_date_es=date('Y-m-d');
    if($get_schedule_data['comments_for_reference']!='')
    {
      $start_date_es=$get_schedule_data['comments_for_reference'];
    }
  }
}
?>
    var moment = $('#calendar').fullCalendar('gotoDate', '<?php echo $start_date_es;?>');

    $(".fc-header-toolbar .fc-center h2").attr('id', 'fc-datepicker-header').before('<input size="1" style="height: 0px; width:0px; border: 0px;" id="fc-datepicker" value="" />');

    $("#fc-datepicker").MonthPicker({
      Button: false,
      //MinMonth: "+10y",
      //MaxMonth: "+10y",
      OnAfterMenuClose: function() {
        var d = $("#fc-datepicker").MonthPicker('Validate')
        console.log(d)
        if (d !== null){
          $('#calendar').fullCalendar('gotoDate', d);
        }
      }
    });
    //$("td.month-picker-previous a").removeClass('ui-state-disabled')
    $("#fc-datepicker-header").click(function() {
      $('#fc-datepicker').MonthPicker('Open')
    });

    $('#search_btn').on('click', function() {
      
      var cmy_name = $('#company_name').find("option:selected").val();
      var cmytype = $('#company_type').find("option:selected").val();
console.log(cmytype);
console.log(cmy_name);
      if(cmy_name !='' || cmytype !='') 
      {
        $('#calendar').fullCalendar('destroy');
            // $.ajax({
      //   type: "POST",
      //   // dataType: 'json',
      //   url: "<?php echo base_url().'Calendar/CheckCompanyName';?>",
      //   data: {"cmy_name":cmy_name},
      //   success: function(data) { 
      //   alert(data);

      //   // var calendar = $('#calendar').fullCalendar('getCalendar');
      //   // var m = calendar.moment();
      //   // alert(m);
      //   // $('#calendar').fullCalendar('removeEvents');
      //   $('#calendar').fullCalendar(data);
      //   }
        
      // });

$('#calendar').fullCalendar({
    disableDragging: true,
    header: {
        left: 'prev,next ',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listMonth,listYear'
    },
    editable: true,
    buttonText: {
              listMonth: 'List Month',
              listYear: 'List Year',
              listWeek: 'List Week',
              listDay: 'List Day'
           },
    events: {
        url: '<?php echo base_url(); ?>Calendar/CheckCompanyName',
        cache: false,
        lazyFetching:true,
        type: 'POST',
        data: {
            cmy_name: cmy_name, cmytype: cmytype, 
        },
        error: function () {
            alert('there was an error while fetching events!');
        },               
    },
    dayClick: function (date, allDay, jsEvent, view) {

        displayDayEvents(date, allDay, jsEvent, view);
    },
    eventRender: function (event, element) {
      // alert('okk')
        element.find('.fc-event-time').hide();
        element.attr('title', event.tip);
    }
});

      }
  
         


    });

     // $('.fc-day-grid-event').on('click', function() {
     //  // $('#addnewcontact').modal('show');
     // });

     $(".calender_checkbox").click(function(){
  //alert('aaaa');
    var attr_event=$(this).attr('id');
    $('.calender_checkbox').each(function(){
      //alert('zzz');
      $(this).prop("checked",false);  
    });
    $(this).prop("checked",true);
    // if(attr_event=='all_event')
    // {
        $('#calendar').fullCalendar('destroy');
              $('#calendar').fullCalendar({
              disableDragging: true,
              header: {
                  left: 'prev,next ',
                  center: 'title',
                  right: 'month,agendaWeek,agendaDay,listMonth,listYear'
              },
              editable: true,
              buttonText: {
                        listMonth: 'List Month',
                        listYear: 'List Year',
                        listWeek: 'List Week',
                        listDay: 'List Day'
                     },
              events: {
                  url: '<?php echo base_url(); ?>Calendar/calendar_events',
                  cache: false,
                  lazyFetching:true,
                  type: 'POST',
                  data: {
                      event: attr_event, 
                  },
                  error: function () {
                      alert('there was an error while fetching events!');
                  },               
              },
              dayClick: function (date, allDay, jsEvent, view) {

                  displayDayEvents(date, allDay, jsEvent, view);
              },
              eventRender: function (event, element) {
                // alert('okk')
                  element.find('.fc-event-time').hide();
                  element.attr('title', event.tip);
              }
        });

    //}

    });

  });


$(document).ready(function(){
    $('.frto_datepicker').datepicker({ dateFormat: 'dd-mm-yy',minDate:0, changeMonth: true,
        changeYear: true, });
    $('#start_timepicker1').timepicki();
    $('#to_timepicker2').timepicki();
    $('.start_timepicker').timepicki();
   // $('#to_timepicker2').timepicki();
});
  


</script>
<!-- 22-09-2018 -->
<script>
    $(document).ready(function() {




        $( function() {
  /*  $( "#datepicker1,#datepicker2,#datepicker3,#datepicker4,#datepicker5,#datepicker6,#datepicker7,#datepicker8,#datepicker9,#datepicker10,#datepicker11,#datepicker12,#datepicker13,#datepicker14,#datepicker15,#datepicker16,#datepicker17,#datepicker18,#datepicker19,#datepicker20,#datepicker21,#datepicker22,#datepicker23,#datepicker24,#datepicker25,#datepicker26,#datepicker27,#datepicker28,#datepicker29,#datepicker30,#datepicker31,#datepicker32,#datepicker33,#datepicker34,#datepicker35,#datepicker36,#datepicker37,#datepicker38,#datepicker39,#letter_sign,#confirmation_next_reminder,#accounts_next_reminder_date,#personal_next_reminder_date,#datepicker,.datepicker,#personal_tax_return_date,#p11d_due_date").datepicker({ dateFormat: "yyyy-mm-dd" });*/
  $(".datepicker").datepicker({dateFormat: 'd MM, y'});
  } );




    var max_fields_limit      = 10; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $('.Notification_add').click(function(e){ //click event on add more fields button having class add_more_button
        e.preventDefault();
        if(x < max_fields_limit){ //check conditions
            x++; //counter increment
            $('.notification_data').append('<div class="row_cls append_notify"><div class="col-sm-4"><select class="form-control notification_status" name="notification_status['+x+']"><option value="">None</option><option value="notification">Notification</option><option value="email">Email</option></select></div><div class="col-sm-3"><input type="number" class="form-control notification_timing" name="notification_timing['+x+']" id="notification_timing"></div><div class="col-sm-4"><select class="form-control notification_notes" name="notification_notes['+x+']" ><option value="">None</option><option value="hours">Hours</option><option value="minutes">Minutes</option></select></div><div class="col-sm-1 last_button"><button type="button" class="btn-danger form-control remove_field">X</button></div></div>'); //add input field
        }
    });  
    $('.notification_data').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parents('div.append_notify').remove(); x--;
    });
  /** for extra email ids **/
    var max_fields_limit_mail     = 10; //set limit for maximum input fields
    var y = 1; //initialize counter for text box
    $('.extra_email_ids').click(function(e){ //click event on add more fields button having class add_more_button
        e.preventDefault();
        if(y < max_fields_limit_mail){ //check conditions
            y++; //counter increment
            $('.extra_email_adds').append('<div class="row for_extra_email_fields"><div class="col-sm-11 pad_leftno"><input type="text" name="notification_mail[]" id="notification_mail" class="form-control notification_mail" placeholder="Enter Email" data-parsley-required="true" data-parsley-type="email" data-parsley-validate-full-width-characters="true" ></div><div class="col-sm-1 last_button1"><button type="button" class="btn-danger form-control remove_field">X</button></div></div>'); //add input field
        }
    });  
    $('.extra_email_adds').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parents('div.for_extra_email_fields').remove(); y--;
    });
  /** end of email ids **/

  window.Parsley.addValidator('validateFullWidthCharacters', {
  validateString: function(_value) {
    regex = /^([a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/gm;
    return regex.test(_value);
  },
  messages: {
    en: 'Please enter a valid email address.'
  }
}); 

});

$(document).on('click','.for_event_change',function(){
  var zz=$(this).attr('data-id');
  $('.reminder_event_close_popup-'+zz).trigger('click');
  //$('#reminder_event_popup-'+zz).modal('toggle');
  $('#addnewcontactedit-'+zz).modal('toggle');
  //$('#addnewcontactedit-'+zz).show();
});




$(".meeting_add").click(function(){

  alert('ok');

  var notes=CKEDITOR.instances['editor61'].getData();



var update_calllog_id=$("#update_calllog_id").val();
  var client=$("#meeting_client").val();
//alert(client);
  var meeting_date=$("#meeting_date").val();
 //// alert(meeting_date);

  if(client!=''){ 
//alert('client ok');
  if(notes == '')
  {
    $('.meeting_errormsg').html('Enter Your Notes');
  }
  else
  { 

    //alert('notes ok');

      $.ajax({
                  url: '<?php echo base_url();?>client/client_timeline_meeting_add/',
                  dataType : 'json',
                  type : 'POST',
                  data : {'notes':notes,'user_id':client,'update_calllog_id':update_calllog_id,'meeting_date':meeting_date},
                  success: function(data) {
                   //alert('zzz');
                    CKEDITOR.instances['editor61'].setData('');
                    $("#meeting_date").val('');
                    
                     $("#addnewmeeting").modal('hide');

                     location.reload();

                     },
                });

  }

}else{
    $('.meeting_errormsg').html('Please chhose Client');
}
});

</script>




<!-- 22-09-2018 -->


</body>
</html>