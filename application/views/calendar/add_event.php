<?php $this->load->view('includes/header');?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fullcalendar12.css"> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fullcalendar.print.css" media='print'>
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <!--start-->
            <?php echo $this->session->flashdata('success'); ?>    
                  <div class="col-sm-12 common_form_section1 was-sucess edit-tsk ad-events">
                  <div class="deadline-crm1 floating_set single-txt12">
                     <ul class="nav nav-tabs all_user1 md-tabs pull-left">
                        <li class="nav-item">
                           add event
                         <div class="slide"></div>
                        </li>
                     </ul>
                  </div> 

                     <div class="col-xs-12 new-task add_new_post04 setting-calender">

                        <div class="card-block">
                          <div class="row">
                                                    
                                        <div class="col-xs-12 col-md-12 add-events">
                                            <h3 class="card-title accordion-title">add tittle</h3>
                                              <div class="top-sec">
                                                <div class="add-titile">
                                                  <input type="text" name="add" placeholder="Add tittle">
                                                  <button class="btn btn-primary">save</button>
                                                </div>
                                                <div class="time-zone">
                                                    <input type="text" class="type1">
                                                    <input type="text" class="type2">
                                                    <span class="ad-tit">to</span>
                                                     <input type="text" class="type2">
                                                    <input type="text" class="type1">
                                                    <a href="javascript:;" class="dif-zone"> time zone</a>
                                                </div>
                                                <div class="select-day">
                                                  <div class="checkbox-fade fade-in-primary">
                                                              <label>
                                                              <input type="checkbox" id="select_all_user">
                                                              <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                                              All day</label>
                                                    </div>
                                                    <select>
                                                      <option>Doesn't repeat</option>
                                                      <option>Doesn't repeat</option>
                                                      <option>Doesn't repeat</option>
                                                    </select>
                                                </div>
                                              </div> 

                                        </div>

                                         <div class="col-xs-12 col-md-6 sub-event">
                                          <div class="event-details">
                                          <h3 class="card-title accordion-title">event details</h3>
                                            <div class="single-field">
                                              <i class="fa fa-map-marker" aria-hidden="true"></i>
                                              <input type="text" class="add-loc">
                                            </div>
                                            <div class="single-field">
                                              <i class="fa fa-bell" aria-hidden="true"></i>
                                              <div class="notify-op1">
                                              <div class="notify-op">
                                                 <select>
                                                      <option>Notification</option>
                                                      <option>Doesn't repeat</option>
                                                      <option>Doesn't repeat</option>
                                                  </select>
                                                  <input type="text" class="type2">
                                                  <select>
                                                      <option>minutes</option>
                                                      <option>Doesn't repeat</option>
                                                      <option>Doesn't repeat</option>
                                                  </select>
                                              </div>
                                              <div class="notify-op">
                                                 <select>
                                                      <option>Email</option>
                                                      <option>Doesn't repeat</option>
                                                      <option>Doesn't repeat</option>
                                                  </select>
                                                  <input type="text" class="type2">
                                                  <select>
                                                      <option>minutes</option>
                                                      <option>Doesn't repeat</option>
                                                      <option>Doesn't repeat</option>
                                                  </select>
                                              </div>
                                              <a href="javascript:;" class="dif-zone"> add notification</a>
                                              </div>
                                            </div>
                                             <div class="single-field">
                                              <i class="fa fa-map-marker" aria-hidden="true"></i>
                                              <span class="sl-mailid">addlive@gmail.com</span>
                                            </div>
                                            <div class="single-field">
                                              <i class="fa fa-suitcase" aria-hidden="true"></i>
                                              <select>
                                                      <option>Busy</option>
                                                      <option>Doesn't repeat</option>
                                                      <option>Doesn't repeat</option>
                                              </select>
                                              <select>
                                                      <option>Default visibility</option>
                                                      <option>Doesn't repeat</option>
                                                      <option>Doesn't repeat</option>
                                              </select>
                                              <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="single-field">
                                              <i class="fa fa-align-left" aria-hidden="true"></i>
                                              <textarea id="editor1"></textarea>
                                            </div>
                                          </div>
                                          </div>
                                          <div class="col-xs-12 col-md-6 sub-event">
                                            <div class="event-details">
                                            <h3 class="card-title accordion-title">guests</h3>
                                              <div class="single-field">
                                              <input type="text" class="add-loc">
                                              <button class="btn btn-primary">add email</button>
                                            </div>
                                             <div class="single-field">
                                              <label>guests can:</label>
                                              <div class="checkbox-fade fade-in-primary1">
                                              <label>
                                                <input type="checkbox" id="select_all_user">
                                                <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                                 Modify event</label>
                                                </div>
                                                <div class="checkbox-fade fade-in-primary2">
                                                <label>
                                                  <input type="checkbox" id="select_all_user">
                                                <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                                 Invite others</label>
                                                 </div>
                                                 <div class="checkbox-fade fade-in-primary3">
                                                 <label>
                                                  <input type="checkbox" id="select_all_user">
                                                <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                                 See guest list</label>
                                                 </div>
                                            </div>
                                            </div>
                                          </div>
                            </div>
                        </div>
                                             
                     </div>
                  </div>
                  <!-- close -->
               </div>
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
   </div>
</div>




 
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style>
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- j-pro js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/fullcalendar.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/calendar.js"></script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script>
 $( document ).ready(function() {

  CKEDITOR.replace( 'editor1');

 });

   var Random = Mock.Random;
   var json1 = Mock.mock({
     "data|10-50": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled|1-2": true,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
   
   $('.dropdown-mul-1').dropdown({
     data: json1.data,
     limitCount: 40,
     multipleMode: 'label',
     choice: function () {
       // console.log(arguments,this);
     }
   });
   
   var json2 = Mock.mock({
     "data|10000-10000": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled": false,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
        
   $('.dropdown-mul-2').dropdown({
     limitCount: 5,
     searchable: false
   });
   
   $('.dropdown-sin-1').dropdown({
     readOnly: true,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   
   $('.dropdown-sin-2').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
</script>
<script type="text/javascript">
   $('#timepicker1').timepicker();  
</script> 

<script type="text/javascript">
   // var Current_date = moment().format('YYYY-DD-MM');
      "use strict";
  $(document).ready(function() {
      $('#external-events .fc-event').each(function() {
          // store data so the calendar knows to render an event upon drop
          $(this).data('event', {
              title: $.trim($(this).text()), // use the element's text as the event title
              stick: true // maintain when user navigates (see docs on the renderEvent method)

          });
          // make the event draggable using jQuery UI
          $(this).draggable({
              zIndex: 999,
              revert: false, // will cause the event to go back to its
              revertDuration: 0 //  original position after the drag
          });    

      });
     $('#calendar').fullCalendar({
        
        disableDragging: true,
          header: {
              left: 'prev,next ',
              center: 'title',
              right: 'month,agendaWeek,agendaDay,listMonth'
          },
          editable: false,
          droppable: false,

          events: 
            <?php echo $client_json; ?>,

      });

     $(document).on('click', '.fc-event div', function() {
      
       var company_type = $(this).text().trim();       
       if($(this).parent('a').is('[class*="event-"]')){           

         var check = "event-";     

         var check1 = "service-";     

          var check2 = "search-";     
     // $('[class^="event-"], [class*=" event-"]').each(function () {    
        // Get array of class names   
        var cls = $(this).parent('a').attr('class').split(' ');       
        for (var i = 0; i < cls.length; i++) {
            // Iterate over the class and log it if it matches
            if (cls[i].indexOf(check) > -1) {        
                var company_type =cls[i].slice(check.length, cls[i].length);
            } 

            if (cls[i].indexOf(check1) > -1) {        
                var service =cls[i].slice(check1.length, cls[i].length);
            } 

             if (cls[i].indexOf(check2) > -1) {        
                var search_value =cls[i].slice(check2.length, cls[i].length);
            }       
        }    
 //   });
       }
        var service_name=service.toUpperCase();
        var res = service_name.replace("-", " ");
      // alert(company_type); 
        // $('#showcmy-type-pop').modal('show');
        $.ajax({
        type: "POST",
        // dataType: 'json',
        url: "<?php echo base_url().'Calendar/filterCompanyType';?>",
        data: {"company_type":company_type,'search_value':search_value},
        success: function(response) { 
        //alert(response);
          $('#showcmy-type-pop').modal('show');
          
          var con = $.parseJSON(response)
           //console.log( con.crm_company_name );
          $('#cmyname').val(con.crm_company_name);
          $('#cmyemail').val(con.crm_email);
          $('#cmytype').val(con.crm_legal_form);
          $('#cmycreated').val(con.created_date);
           $('#service-name').val(res);
           if(search_value=='crm_confirmation_statement_due_date'){
             $('#service-due').val(con.crm_confirmation_statement_due_date);
           }
            if(search_value=='crm_next_manage_acc_date'){
              $('#service-due').val(con.crm_next_manage_acc_date);
           }
            if(search_value=='crm_next_booking_date'){
            $('#service-due').val(con.crm_next_booking_date);
           }
            if(search_value=='crm_vat_due_date'){
            $('#service-due').val(con.crm_vat_due_date);
           }
            if(search_value=='crm_pension_subm_due_date'){
            $('#service-due').val(con.crm_pension_subm_due_date);
           }
            if(search_value=='crm_ch_accounts_next_due'){
            $('#service-due').val(con.crm_ch_accounts_next_due);
           }
            if(search_value=='crm_accounts_tax_date_hmrc'){
            $('#service-due').val(con.crm_accounts_tax_date_hmrc);
           }
            if(search_value=='crm_personal_due_date_return'){
            $('#service-due').val(con.crm_personal_due_date_return);
           }
            if(search_value=='crm_rti_deadline'){
            $('#service-due').val(con.crm_rti_deadline);
           }
            if(search_value=='crm_next_p11d_return_due'){
            $('#service-due').val(con.crm_next_p11d_return_due);
           }
            if(search_value=='crm_insurance_renew_date'){
            $('#service-due').val(con.crm_insurance_renew_date);
           }
            if(search_value=='crm_registered_renew_date'){
            $('#service-due').val(con.crm_registered_renew_date);
           }
            if(search_value=='crm_investigation_end_date'){
            $('#service-due').val(con.crm_investigation_end_date);
           } 
          
        }
        
      }); 

      });    
 
  });

</script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/parsley.css') ?>">
<script src="<?php echo base_url('assets/js/parsley.min.js') ?>"></script>
<script type="text/javascript">
   $('#contact_form').parsley();
</script>
<style type="text/css">
  .parsley-required {
  color: red;
  }
</style>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script type="text/javascript">
 $('.dropdown-sin-5').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });
 $('.dropdown-sin-6').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching1" placeholder="Search">'
   });  
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#search_btn').on('click', function() {
      $('#calendar').fullCalendar('destroy');
      var cmy_name = $('#company_name').find("option:selected").val();
      var cmytype = $('#company_type').find("option:selected").val();
console.log(cmytype);
      if(cmy_name !='' || cmytype !='') 
      {
            // $.ajax({
      //   type: "POST",
      //   // dataType: 'json',
      //   url: "<?php echo base_url().'Calendar/CheckCompanyName';?>",
      //   data: {"cmy_name":cmy_name},
      //   success: function(data) { 
      //   alert(data);

      //   // var calendar = $('#calendar').fullCalendar('getCalendar');
      //   // var m = calendar.moment();
      //   // alert(m);
      //   // $('#calendar').fullCalendar('removeEvents');
      //   $('#calendar').fullCalendar(data);
      //   }
        
      // });

$('#calendar').fullCalendar({
    disableDragging: true,
    header: {
        left: 'prev,next ',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listMonth'
    },
    editable: true,
    events: {
        url: 'Calendar/CheckCompanyName',
        cache: false,
        lazyFetching:true,
        type: 'POST',
        data: {
            cmy_name: cmy_name, cmytype: cmytype, 
        },
        // error: function () {
        //     alert('there was an error while fetching events!');
        // },               
    },
    dayClick: function (date, allDay, jsEvent, view) {

        displayDayEvents(date, allDay, jsEvent, view);
    },
    eventRender: function (event, element) {
      // alert('okk')
        element.find('.fc-event-time').hide();
        element.attr('title', event.tip);
    }
});

      }
  
         


    });

     // $('.fc-day-grid-event').on('click', function() {
     //  // $('#addnewcontact').modal('show');
     // });

  });


  


</script>


</body>
</html>

