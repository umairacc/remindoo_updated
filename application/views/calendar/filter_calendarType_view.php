<?php
  
if(isset($getClient_type)) {

  foreach ($getClient_type as $key => $clientType) {  ?>
      

  <?php  if ($clientType['crm_confirmation_statement_due_date'] !='') {
        $client_info[] = array('id' => $clientType['user_id'],
                                  'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_confirmation_statement_due_date'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$clientType['user_id'].' '.'service-conformation-statement'.' '.'search-crm_confirmation_statement_due_date'],  
                              );
      }
      if ($clientType['crm_ch_accounts_next_due'] !='') {
        $client_info[] = array('id' => $clientType['user_id'],
                                  'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_ch_accounts_next_due'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$clientType['user_id'].' '.'service-accounts'.' '.'search-crm_ch_accounts_next_due'],
                              );
      }
      if ($clientType['crm_accounts_tax_date_hmrc'] !='') {
        $client_info[] = array('id' => $clientType['user_id'],
                                  'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_accounts_tax_date_hmrc'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$clientType['user_id'].' '.'service-company-tax'.' '.'search-crm_accounts_tax_date_hmrc'], 
                              );
      }
      if ($clientType['crm_personal_due_date_return'] != '') {
        $client_info[] = array('id' => $clientType['user_id'],
                                 'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_personal_due_date_return'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$clientType['user_id'].' '.'service-personal-tax'.' '.'search-crm_personal_due_date_return'], 
                              );
      }
      if ($clientType['crm_vat_due_date'] != '') {
        $client_info[] = array('id' => $clientType['user_id'],
                                'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_vat_due_date'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$clientType['user_id'].' '.'service-vat-return'.' '.'search-crm_vat_due_date'], 
                              );
      }
      if ($clientType['crm_rti_deadline'] != '') {
        $client_info[] = array('id' => $clientType['user_id'],
                                  'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_rti_deadline'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$clientType['user_id'].' '.'service-payroll'.' '.'search-crm_rti_deadline'],    
                              );
      }
      if ($clientType['crm_pension_subm_due_date'] != '') {
        $client_info[] = array('id' => $clientType['user_id'],
                                 'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_pension_subm_due_date'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$clientType['user_id'].' '.'service-work-place-pension-ae'.' '.'search-crm_pension_subm_due_date'], 
                              );
      }
      if ($clientType['crm_next_p11d_return_due'] != '') {
        $client_info[] = array('id' => $clientType['user_id'],
                                 'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_next_p11d_return_due'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                     'className'=> ['event-'.$clientType['user_id'].' '.'service-p11d'.' '.'search-crm_next_p11d_return_due'],  
                              );
      }
      if ($clientType['crm_next_manage_acc_date'] != '') {
        $client_info[] = array('id' => $clientType['user_id'],
                                 'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_next_manage_acc_date'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                    'className'=> ['event-'.$clientType['user_id'].' '.'service-management-account'.' '.'search-crm_next_manage_acc_date'],  
                              );
      }
      if ($clientType['crm_next_booking_date'] != '') {
        $client_info[] = array('id' => $clientType['user_id'],
                                  'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_next_booking_date'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$clientType['user_id'].' '.'service-bookkeeping'.' '.'search-crm_next_booking_date'],
                              );
      }
      if ($clientType['crm_insurance_renew_date'] != '') {
        $client_info[] = array('id' => $clientType['user_id'],
                                 'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_insurance_renew_date'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$clientType['user_id'].' '.'service-investigation-insurance'.' '.'search-crm_insurance_renew_date'], 
                              );
      }
      if ($clientType['crm_registered_renew_date'] != '') {
        $client_info[] = array('id' => $clientType['user_id'],
                                 'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_registered_renew_date'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                   'className'=> ['event-'.$clientType['user_id'].' '.'service-registed-address'.' '.'search-crm_registered_renew_date'], 
                              );
      }
      if ($clientType['crm_investigation_end_date'] != '') {
        $client_info[] = array('id' => $clientType['user_id'],
                                 'title' => $clientType['crm_legal_form'].'-'.$clientType['crm_company_name'],
                                  'start' => $clientType['crm_investigation_end_date'],
                                  'editable' => true,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                    'className'=> ['event-'.$clientType['user_id'] .' '.'service-tax-investigation'.' '.'search-crm_investigation_end_dayte'], 
                              );
      }



  }  
  
  if(isset($timeline_schedule_data) && $timeline_schedule_data!='')
{
  foreach ($timeline_schedule_data as $sch_key => $sch_value) {
    $start_date=$sch_value['comments_for_reference'];
      $title=$this->Common_mdl->get_field_value('client','crm_company_name','user_id',$sch_value['user_id']);
    if($title==''){
      $title=$this->Common_mdl->get_crm_name($sch_value['user_id']);
    }
     $client_info[] = array('id' => 'sch_'.$sch_value['id'],
                          'title' => $title,
                          'start' => $start_date,
                          'editable' => true,
                          'borderColor'=> '#FC6180',
                          'backgroundColor'=> '#FC6180',
                          'textColor'=>'#fff',
                          'className'=> ['sch_vent-'.$sch_value['id'].' '.'for_timeline_services'],
                          'idName'=>['rspt'],
                          );   
  }
}

    $clientType_json = json_encode($client_info);   
  
  echo $clientType_json;  

   



}


?>
