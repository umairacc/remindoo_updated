<?php
$client_details=array();
if(isset($getClient_info) && count($getClient_info)>0){
 foreach ($getClient_info as $key => $get_client) {  

    ?>    

  <?php   
     if ($get_client['crm_confirmation_statement_due_date'] !='') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_confirmation_statement_due_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',  
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-conformation-statement'.' '.'search-crm_confirmation_statement_due_date'],  
                              );
      }
      if ($get_client['crm_ch_accounts_next_due'] !='') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_ch_accounts_next_due'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff', 
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-accounts'.' '.'search-crm_ch_accounts_next_due'],     
                              );
      }
      if ($get_client['crm_accounts_tax_date_hmrc'] !='') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_accounts_tax_date_hmrc'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-company-tax'.' '.'search-crm_accounts_tax_date_hmrc'],       
                              );
      }
      if ($get_client['crm_personal_due_date_return'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_personal_due_date_return'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-personal-tax'.' '.'search-crm_personal_due_date_return'],            
                              );
      }
      if ($get_client['crm_vat_due_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_vat_due_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-vat-return'.' '.'search-crm_vat_due_date'],            
                              );
      }
      if ($get_client['crm_rti_deadline'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_rti_deadline'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-payroll'.' '.'search-crm_rti_deadline'],            
                              );
      }
      if ($get_client['crm_pension_subm_due_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_pension_subm_due_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-work-place-pension-ae'.' '.'search-crm_pension_subm_due_date'],             
                              );
      }
      if ($get_client['crm_next_p11d_return_due'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_next_p11d_return_due'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-p11d'.' '.'search-crm_next_p11d_return_due'],            
                              );
      }
      if ($get_client['crm_next_manage_acc_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_next_manage_acc_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-management-account'.' '.'search-crm_next_manage_acc_date'],            
                              );
      }
      if ($get_client['crm_next_booking_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_next_booking_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-bookkeeping'.' '.'search-crm_next_booking_date'],            
                              );
      }
      if ($get_client['crm_insurance_renew_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_insurance_renew_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff' ,
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-investigation-insurance'.' '.'search-crm_insurance_renew_date'],            
                              );
      }
      if ($get_client['crm_registered_renew_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_registered_renew_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'].' '.'service-registed-address'.' '.'search-crm_registered_renew_date'],            
                              );
      }
      if ($get_client['crm_investigation_end_date'] != '') {
        $client_details[] = array('id' => $get_client['user_id'],
                                  'title' => $get_client['crm_legal_form'].'-'.$get_client['crm_company_name'],
                                  'start' => $get_client['crm_investigation_end_date'],
                                  'editable' => false,
                                  'borderColor' => '#4680ff',
                                  'backgroundColor' => '#4680ff',
                                  'textColor' => '#fff',
                                  'className'=> ['event-'.$get_client['user_id'] .' '.'service-tax-investigation'.' '.'search-crm_investigation_end_dayte'],           
                                  
                              );
      }
                           
}
}
/** timeline schedule id 19-09-2018 **/
if(isset($timeline_schedule_data) && $timeline_schedule_data!='')
{
  foreach ($timeline_schedule_data as $sch_key => $sch_value) {
    $start_date=$sch_value['comments_for_reference'];
      $title=$this->Common_mdl->get_field_value('client','crm_company_name','user_id',$sch_value['user_id']);
    if($title==''){
      $title=$this->Common_mdl->get_crm_name($sch_value['user_id']);
    }
     $client_details[] = array('id' => 'sch_'.$sch_value['id'],
                          'title' => $title,
                          'start' => $start_date,
                          'editable' => true,
                          'borderColor'=> '#FC6180',
                          'backgroundColor'=> '#FC6180',
                          'textColor'=>'#fff',
                          'className'=> ['sch_vent-'.$sch_value['id'].' '.'for_timeline_services'],
                          'idName'=>['rspt'],
                          );   
    
  }
}

if(isset($timeline_schedule_id) && $timeline_schedule_id!='' && is_numeric($timeline_schedule_id))
{
  $get_schedule_data=$this->db->query('select * from timeline_services_notes_added where id='.$timeline_schedule_id.' ')->row_array();
  if(count($get_schedule_data)>0)
  {
    $start_date=date('Y-m-d');
    if($get_schedule_data['comments_for_reference']!='')
    {
      $start_date=$get_schedule_data['comments_for_reference'];
    }
    $title=$this->Common_mdl->get_field_value('client','crm_company_name','user_id',$get_schedule_data['user_id']);
    if($title==''){
      $title=$this->Common_mdl->get_crm_name($get_schedule_data['user_id']);
    }
   if($get_schedule_data['comments_for_reference']=='') // avoid dupliaction in update option records
    {
  $client_details[] = array('id' => 'sch_'.$get_schedule_data['id'],
                          'title' => $title,
                          'start' => $start_date,
                          'editable' => true,
                          'borderColor'=> '#FC6180',
                          'backgroundColor'=> '#FC6180',
                          'textColor'=>'#fff',
                          'className'=> ['sch_vent-'.$get_schedule_data['id'].' '.'for_timeline_services'],
                          'idName'=>['rspt'],
                          );

    }
  }
}
/** end of timeline scedule id 19-09-2018 **/
/** for task remindewr section 20-09-2018**/
if(isset($get_task_reminders) && count($get_task_reminders)>0)
{
  foreach ($get_task_reminders as $task_key => $task_value) {
    $get_reminder_datetime=$task_value['for_reminder_chk_box'];
    $dates_time=array();
    $res_array=(array)json_decode($get_reminder_datetime);
    $reminders_dates=array_values($res_array);
    if(count($reminders_dates)>0)
    {
      foreach ($reminders_dates as $rem_key => $rem_value) {
        $res=explode('//',$rem_value);
        $time = $res[1];
$timeto24 = DateTime::createFromFormat('g:i a', $time);
$change_time=$timeto24->format('H:i:s');//17:04
$changedate_time=$res[0]."T".$change_time;
$client_details[] = array('id' => 'taskreminder_'.$task_value['id'],
                          'title' => $task_value['subject'],
                          'start' => $changedate_time,
                          'editable' => false,
                          'borderColor'=> '#FFB64D',
                          'backgroundColor'=> '#FFB64D',
                          'textColor'=>'#fff',
                          'className'=> ['taskreminder-'.$task_value['id'].' '.'for_reminder_task'],
                          
                          );
      }
    }
  
  }
}
/** end of reminder for task section  20-09-2018 **/
/** reminder for leads section  20-09-2018 **/
if(isset($get_leads_reminders) && count($get_leads_reminders)>0)
{
foreach ($get_leads_reminders as $leads_key => $leads_value) {
     $leads_name=$this->Common_mdl->get_field_value('leads','name','id',$leads_value['lead_id']);
  $client_details[] = array('id' => 'leads_'.$leads_value['id'],
                          'title' => $leads_name,
                          'start' => date('Y-m-d',strtotime($leads_value['reminder_date'])),
                          'editable' => false,
                          'borderColor'=> '#FFB64D',
                          'backgroundColor'=> '#FFB64D',
                          'textColor'=>'#fff',
                          'className'=> ['leads-'.$leads_value['id'].' '.'for_reminder_leads'],
                           );

  
  }
}



if(isset($get_meeting_event) && count($get_meeting_event)>0)
{
foreach ($get_meeting_event as $meeting_key => $meeting_value) {
    
  $client_details[] = array('id' => 'meeting_'.$meeting_value['id'],
                          'title' => 'Meeting',
                          'start' => date('Y-m-d',strtotime($meeting_value['date'])),
                          'editable' => false,
                          'borderColor'=> '#FFB64D',
                          'backgroundColor'=> '#FFB64D',
                          'textColor'=>'#fff',
                          'className'=> ['meeting-'.$meeting_value['id'].' '.'for_meeting'],
                           );

  }
}



  

  $client_json = json_encode($client_details);

  echo $client_json;
//print_r($client_json);
//print_r($getclient_event['crm_company_name']);
?>


