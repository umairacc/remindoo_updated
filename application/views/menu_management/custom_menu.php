 <?php
     if( !empty( $_SESSION['is_superAdmin_login'] ) )
    {   
      $this->load->view('super_admin/superAdmin_header');
    }
    else
    {
      $this->load->view('includes/header');
    }
?>


    <div class="modal fade" id="deleteSteps" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Menu</h4>
        </div>
        <div class="modal-body">
        Do you want to delete?
       
        </div>
        <div class="modal-footer">
         <button type="button" name="button" id="menu_delete">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">no</button>
        </div>
      </div>
    </div>
  </div>

 <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/js/bs-iconpicker/css/bootstrap-iconpicker.min.css" rel="stylesheet">

       <!--  <link href="<?php echo base_url()?>assets/menudropdown/fm.selectator.jquery.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            #wrapper {
            padding: 0;
            max-width: 1024px;
            margin: 0 auto;
            background-color: #fff;
        }
        #wrapper_inner {
            padding: 1rem 5rem 7rem;
            max-width: 1024px;
            margin: 0 auto;
        }
        #icon1 {
            width: 250px;
            height: 36px;
        }
        </style>
 -->

    
        <div class="modal-alertsuccess alert alert-danger-check succ staff-added dashboard_success_message_staff" style="display:none;">
      <div class="newupdate_alert">   <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              Updated Sucessfully...
         </div></div>
         </div>
   </div>
        


        <div class="container">
         <!--    <div class="row">
                <div class="col-md-12"><h2>Demo</h2></div>
            </div> -->
            <div class="row martopsecti">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix"><h5 class="pull-left">Menu</h5>
                            <div class="pull-right">
                                <!-- <button id="btnReload" type="button" class="btn btn-default">
                                    <i class="glyphicon glyphicon-triangle-right"></i> Load Data</button> -->
                            </div>
                        </div>
                        <div class="panel-body" id="cont">
                            <ul id="myEditor" class="sortableLists list-group menusetleft">
                            </ul>
                        </div>
                    </div>
                    
                   <!--  <div class="form-group"><textarea id="out" class="form-control" cols="50" rows="10"></textarea>
                    </div> -->
                    <input type="hidden" id="menu_id" name="menu_id" value="<?php echo (isset($menus['id']))?$menus['id']:'' ?>">
                </div>
                <div class="col-md-6 menusetright">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Edit item</div>
                        <div class="panel-body">
                            <form id="frmEdit" class="form-horizontal">
                                <div class="form-group">
                                    <label for="text" class="col-sm-2 control-label">Text</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" class="form-control item-menu" name="text" id="text" placeholder="Text">
                                            <div class="text_div" style="color:red"></div>
                                          <!--   <div class="input-group-btn">
                                                <button type="button" id="myEditor_icon" class="btn btn-default" data-iconset="fontawesome"></button>
                                            </div> -->
                                            <input type="hidden" id="icon" name="icon" class="item-menu">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="href" class="col-sm-2 control-label">URL</label>
                                    <div class="col-sm-10">
                                       <!--  <input type="text" class="form-control item-menu"  name="href" placeholder="URL"> -->
                                            <div class="form-group link_type_toggle" style="">
                                   
                                            <div id="selectBox-pdiv">
                                                    <select name="href" id="href" class="form-control item-menu">
                                                        <option value="">Select URL</option>
                                                        <?php
                                                        foreach ($modules as $datas) 
                                                        { $optionVal = trim($datas['module_name']);
                                            ?>
                                          <option value="<?php echo $datas['module_mangement_id']; ?>"> <?php echo $optionVal;?> </option>      
                                          <?php
                                            }
                                                        ?>
                                                    </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                   <div class="form-group" id="icon_content">
                                      <label for="target" class="col-sm-2 control-label">Upload Icon</label>
                                        <div class="col-sm-10 custom_upload">
                                          <input type="file" name="menu_icon" class="menu_icon">
                                        </div>
                                        <div class="position-preview position-previewimg"><img style="display: none;" src="" class="file1_preview"></div>

                                         <input type="hidden" id="reduce-image" name="reduce_image" / >

                                         <div id="img_error" style="color:#FF0000;"></div>
                                       
                                  </div>

              


                                <div class="form-group" style="display:none ">
                                    <label for="target" class="col-sm-2 control-label">Target</label>
                                    <div class="col-sm-10">
                                        <select name="target" id="target" class="form-control item-menu">
                                            <option value="_self">Self</option>
                                            <option value="_blank">Blank</option>
                                            <option value="_top">Top</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" style="display: none ">
                                    <label for="title" class="col-sm-2 control-label">Tooltip</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="title" class="form-control item-menu" id="title" placeholder="Tooltip">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="panel-footer">
                            <button type="button" id="btnUpdate" class="btn btn-primary" disabled><i class="fa fa-refresh"></i> Update</button>
                            <?php if(isset($_SESSION['permission']['Menu_Management_Settings']['create']) && $_SESSION['permission']['Menu_Management_Settings']['create']==1) {  ?>
                            <button type="button" id="btnAdd" class="btn btn-success"><i class="fa fa-plus"></i> Add</button> 
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <button id="btnOut" type="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Save</button>
                    </div>
                </div>
            </div>
            <hr>
            
        </div>
        <?php
    if( !empty( $_SESSION['is_superAdmin_login'] ) )
  {
    $this->load->view('super_admin/superAdmin_footer');
  }
  else
  {      
    $this->load->view('includes/footer');
  }
?>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

      <!--   <script src="<?php echo base_url()?>assets/menudropdown/fm.selectator.jquery.js"></script> -->

        <script src='<?php echo base_url(); ?>/js/bs-iconpicker/js/iconset/iconset-fontawesome-4.7.0.min.js'></script>
        <script src='<?php echo base_url(); ?>/js/bs-iconpicker/js/bootstrap-iconpicker.js'></script>
        <script src='<?php echo base_url(); ?>js/jquery-menu-editor.min.js'></script>



        <script>
      //  $('#btnReload').trigger('click');
            $(document).ready(function () {

                // menu items
                var strjson = <?php echo $menus['menu_details'] ?>;
                //icon picker options
                var iconPickerOptions = {searchText: 'Buscar...', labelHeader: '{0} de {1} Pags.'};
                //sortable list options
                var sortableListOptions = {
                    placeholderCss: {'background-color': 'cyan'}
                };

                var editor = new MenuEditor('myEditor', {listOptions: sortableListOptions, iconPicker: iconPickerOptions, labelEdit: 'Edit'});
                editor.setForm($('#frmEdit'));
                editor.setUpdateButton($('#btnUpdate'));
                
                // $('#btnReload').on('click', function () {
                    editor.setData(strjson);
              //  });

                $("#btnUpdate").click(function(){

                  $('.file1_preview').attr("style","display:none");
                     var reduce_image=$('#reduce-image').val();
                     //alert('dss');
                       var numItems = $('#img_error').html();
                       if(numItems.length==0){
                     if(reduce_image!='')
                    {
                    $.ajax({
                        type:"post",
                        url: "<?php echo base_url();?>Menu_management/menu_icon_upload",
                        data:{           
                          reduce_image:reduce_image,
                         // menu_id:menu_id,
                          
                             },
                        //dataType: 'json',
      
                      success: function(res)
                      {
                    $('#icon').val(res);
                     editor.update();
                     }
                   } );
                  }
                  else
                  {
                     editor.update();
                  }
                } 

                });

                 $('#btnAdd').click(function(){
                  $('.text_div').html('');
                  var numItems = $('#img_error').html();
                  if($('#text').val()!=''){
                    if( numItems.length == 0){
                    editor.add();
                    }
                  }
                  else
                  {
                    $('.text_div').html('Enter text first');
                  }
                });

                $('#btnOut').on('click', function () {

                  var numItems = $('#img_error').html();

                  if(numItems.length  == 0){
                 // return;

                    var str = editor.getString();
                    var menu_id = $('#menu_id').val();
                    // console.log(str);
                    // return;
                    if(menu_id!='')
                    {
                    $.ajax({
                        type:"post",
                        url: "<?php echo base_url();?>Menu_management/menu_update",
                        data:{           
                          menu_details:str,
                          menu_id:menu_id,
                          
                             },
                        //dataType: 'json',
      
                      success: function(res)
                      {
                         
                    $('.dashboard_success_message_staff').show();
                 setTimeout(function(){ $('.dashboard_success_message_staff').hide(); }, 2000);
                   
                        
                      }

                     });
                    }
                   } 
                });

              

            });

     //            function readURL( input , preview ) {
     //     if (input.files && input.files[0]) {
     //         var reader = new FileReader();
   
     //         reader.onload = function (e) {

     //             preview.attr('src', e.target.result);
     //             preview.attr("style","display:block");

     //                 }
     //         reader.readAsDataURL(input.files[0]);
     //    }
     // }
   
     // $(".menu_icon").change(function () {
     //     readURL(this , $('.file1_preview') );
     // });
        </script>
           <script type="text/javascript">
                // $(function () {
                //     var $activate_selectator = $('#activate_selectator4');
                //     $activate_selectator.click(function () {
                //         var $select = $('#icon1');
                //         if ($select.data('selectator') === undefined) {
                //             $select.selectator({
                //               //  showAllOptionsOnFocus: true,
                //                 //searchFields: 'value text subtitle right'
                //                  useSearch: false,
                //                  keepOpen: false,
                //             });
                //             $activate_selectator.val('destroy');
                //         } else {
                //             $select.selectator('destroy');
                //             $activate_selectator.val('activate');
                //         }
                //     });
                //     $activate_selectator.trigger('click');
                // });


$(document).ready(function() {


   <?php if(isset($_SESSION['permission']['Menu_Management_Settings']['edit']) && $_SESSION['permission']['Menu_Management_Settings']['edit']!=1) {  ?>
    $('.btnEdit').attr('style','display:none');

    <?php } ?>

       <?php if(isset($_SESSION['permission']['Menu_Management_Settings']['delete']) && $_SESSION['permission']['Menu_Management_Settings']['delete']!=1) {  ?>

        $('.btnRemove').attr('style','display:none');
    
    <?php } ?>

                  function readURL(input,  target) {
    // alert(target);

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                 $('.file1_preview').attr('src', e.target.result);
                 $('.file1_preview').attr("style","display:block;");
               //  $('.file1_preview').css({"width: auto !important;","height: auto  !important;"});
                

                var width =25;
                var height =25;

                var oldBase64 = e.target.result;
                resizeBase64Img(oldBase64, width, height, target);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

function resizeBase64Img(datas, wantedWidth, wantedHeight, target)
{
    // We create an image to receive the Data URI
    var img = document.createElement('img');

    // When the event "onload" is triggered we can resize the image.
    img.onload = function()
    {     
        // We create a canvas and get its context.
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');

        // We set the dimensions at the wanted size.
        canvas.width = wantedWidth;
        canvas.height = wantedHeight;

        // We resize the image with the canvas method drawImage();
        ctx.drawImage(this, 0, 0, wantedWidth, wantedHeight);

        var dataURI = canvas.toDataURL();

        $('#'+target).val(dataURI);
       
    };
    // We put the Data URI in the image's src attribute
     img.src = datas;
} 
         
      var a=0;

    $(".menu_icon").change(function () {
      //binds to onchange event of your input field
         $('#img_error').html('');
      var ext = $('.menu_icon').val().split('.').pop().toLowerCase();
      if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
        $('#img_error').html("Invalid Image Format! Image Format Must Be JPG, JPEG, or PNG .");
        //$('#error2').slideUp("slow");
        a=0;
        }else{
        var picsize = (this.files[0].size);
        if (picsize > 1000000){

        $('#img_error').html("Maximum File Size Limit is 1MB.");
        a=0;
        }else{
        a=1;
        //$('#error2').slideUp("slow");
        }
       // $('#error1').slideUp("slow");
        if (a==1){
         readURL(this,  'reduce-image');
          }
      }



       // readURL(this,  'reduce-image');


    }); 

     
}); 


            </script>