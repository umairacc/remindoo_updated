<?php
	 if( !empty( $_SESSION['is_superAdmin_login'] ) )
    {   
      $this->load->view('super_admin/superAdmin_header');
    }
    else
    {
      $this->load->view('includes/header');
    }
?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/hierarchy-select.min.css">
<style type="text/css">
	 .editfile1_preview, .file1_preview
	{
   	max-width:250px; 
  }
</style>

<!-- message box -->
<div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
   <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
     <div class="pop-realted1">
       <div class="position-alert1">
             Please! Select Record...
       </div>
     </div>
   </div>
</div>
<!-- message box -->

<div class="menumanagementtopcls">
	<div class="main-body">
		<div class="page-wrapper">
			<div class="page-body">
				<div class="menumanaicon">
				<form id="new_menu_form">
					<p class="menutittlecos">Menu Management</p>
          <div class="form-group">
  					<label class="col-form-label">All Menu</label>
  					<div class="dropdownallmenu">
    					 <div class="dropdown hierarchy-select" id="allMenus">
                    <button type="button" class="btn btn-secondary dropdown-toggle" id="example-one-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>

                    <div class="dropdown-menu" aria-labelledby="example-one-button">
                            <div class="hs-searchbox">
                                <input type="text" class="form-control" autocomplete="off">
                            </div>
                            <div class="hs-menu-inner">
                                <a class="dropdown-item" data-value="0" data-level="1" data-default-selected="" href="#">Add Main Menu</a>
                                <?php
                                foreach($menus as $menus_data){ ?>
                              <a class="dropdown-item remove_member" data-value="<?php echo $menus_data['menu_management_id']; ?>"  data-level="<?php echo $menus_data['data_level']; ?>" href="#">
                              <i class="fa fa-dot-circle-o"></i>
                              <?php echo $menus_data['label']; ?> 
                              </a>
                                <?php } ?>
                            </div>
                    </div>
                     <input class="d-none" name="parent_id" readonly="readonly"  aria-hidden="true" type="text"/> 
              </div>
              <div class="ActionButtons" style="display: none;">  
                <?php
                if( $_SESSION['permission']['Menu_Management_Settings']['edit'] == 1 )
                {
                  ?>
                  <a href="javascript:;" class="plusmenucionc showEdit">
                    <i class="fa fa-edit"></i>
                  </a>
                <?php
                }
                if( $_SESSION['permission']['Menu_Management_Settings']['delete'] == 1 )
                {
                ?>
                <a href="javascript:;" class="plusmenucionc" id="deleteMenu">
                  <i class="fa fa-trash"></i>
                </a>
                <?php
                }
                ?>  
              </div>
            </div>
          </div>
          <div class="form-group" id="icon_content">
              <label class="col-form-label">Upload Icon</label>
                <div class="custom_upload">
                  <input type="file" name="menu_icon" class="menu_icon">
                </div>
                <div class="position-preview"><img style="display: none;" src="" class="file1_preview"></div>
          </div>
          <div class="form-group">                               
  					<label class="col-form-label">Label</label>
  					<input type="text" name="menu_name"  placeholder="Name Of The Menu"  class="fieldstestcls">
          </div>					
          <div class="form-group form-radio">	
          <label class="col-form-label">Link Type</label>				
  					<div class="radio radio-inline">
  						<label><input type="radio" name="link_type" value="sub_menu" checked="checked"><i class="helper"></i>No Link</label>
  					</div>
  					<div class="radio radio-inline">
  						<label><input type="radio"  name="link_type" value="direct"><i class="helper"></i>Need Link</label>
  					</div>
  				</div>
  				<div class="form-group link_type_toggle" style="display: none;">
    				<label class="col-form-label">URL</label>
    				<div id="selectBox-pdiv">
    						<select name="menu_url">
    							<option value="">Select URL</option>
    							<?php
    							foreach ($modules as $datas) 
    							{ $optionVal = trim($datas['module_name']);
                    ?>
                  <option value="<?php echo $datas['module_mangement_id']; ?>"> <?php echo $optionVal;?> </option>      
                  <?php
                    }
    							?>
    						</select>
    				</div>
  				</div>
			
						<div class="form-group fileuploadmenyu">
							

							<div class="right-submits">
                <?php 
                if( $_SESSION['permission']['Menu_Management_Settings']['create'] == 1  )
                {
                  ?>
                <input  name="submit" type="submit" value="submit">

                <?php
                }
                ?>
              </div>

						</div>
						</form>
 				</div>
			</div>
		</div>
	</div>
</div>



<div class="modal fade menumangementfile" id="edit_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content_start-->
      	<div class="modal-content">

	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Edit Menu</h4>
	        </div>

	        <div class="modal-body">

		        <form id='edit_menu_form'>
		        	<input type="hidden" id="editId">
              <div class="form-group">
  		        	<label class="col-form-label">Label</label>
  		        	<input type="text" name="emenu_name" id="eMenuName" placeholder="" value="" class="modalpopupmenumange">
              </div>
			      <div class="form-group" id="eIcon_content" style="display: none;">
              <label class="col-form-label">Upload Icon</label>
              <div class="custom_upload">
              <input type="file" name="emenu_icon" id="eMenu_icon">         
              </div>
              <div class="position-preview"><img src="" style="display: none;" class="editfile1_preview"></div>
            </div>
			        <div class="form-group form-radio">		
                <label class="col-form-label">Link Type</label>
						<div class="radio radio-inline">
							<label><input type="radio" name="link_type" id="sub_menuType" value="sub_menu" checked="checked"><i class="helper"></i>No Link</label>
						</div>

						<div class="radio radio-inline">
							<label><input type="radio"  name="link_type" id="direct_menuType" value="direct"><i class="helper"></i>Need Link</label>
						</div>

							
					</div>
					<div class="form-group link_type_toggle" style="display: none;">
						<label class="col-form-label">URL</label>  
						<div id="selectBox-pdiv1">
							<select name="emenu_url" id="eMenu_url">
								<option value="">Select URL</option>
								<?php
								foreach ($modules as $datas) 
								{ $optionVal = trim($datas['module_name']);
									?>
								<option value="<?php echo $datas['module_mangement_id']; ?>"> <?php echo $optionVal;?> </option>			
								<?php
									}
								?>
							</select>
						</div>
					</div>

					<div class="fileuploadmenyu">
						
            <div class="right-submits">
						<input name="submit" type="submit" value="submit"></div>
					</div>

				</form>

	        </div>
        </div>
      <!-- Modal content_end-->
    </div>
</div>

<?php
	if( !empty( $_SESSION['is_superAdmin_login'] ) )
  {
    $this->load->view('super_admin/superAdmin_footer');
  }
  else
  {      
    $this->load->view('includes/footer');
  }
?>
<script src="<?php echo base_url();?>assets/js/hierarchy-select.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        
        $('#allMenus').hierarchySelect();
        $('#selectBox-pdiv').dropdown();
        $('#selectBox-pdiv1').dropdown();

      		$("input[name='link_type']").change(function()
      	{	
      		var parentDiv = $(this).closest("form");

      		if( $(this).val() == 'direct')
      		{
      			parentDiv.find(".link_type_toggle").show();
            
      		}
      		else
      		{
      			parentDiv.find(".link_type_toggle").hide();
            
      		}
      	});

      		$(".hierarchy-select .dropdown-item").click(function(){

      		if( $(this).attr("data-value") != 0 )
      		{
      			$(".ActionButtons").show();
      			$(".dropdownallmenu").addClass('incEdit');
            $("#icon_content").hide();
      		}
      		else
      		{
      			$(".ActionButtons").hide();
      			$(".dropdownallmenu").removeClass('incEdit');	
            $("#icon_content").show();
      		}
      	});

      	$('#new_menu_form').validate({
      		rules : {
      			menu_name : {required: true},
      			url : 
      			{
      				required : function(element)
      				{ 
		      			if( $("input[name='link_type']").val() ==  'direct' )
		      			{
		      				return true;
		      			}
		      			else
		      			{
		      				return false;
		      			}
      		 		} 
      			}
      			
      			},
      		submitHandler: function(form) {
      			var formData = new FormData($("#new_menu_form")[0]);      			
      			$.ajax({
                           url: '<?php echo base_url();?>Menu_management/createMenu',                           
                           type : 'POST',
                           data : formData,  
                           contentType : false,
                           processData : false,                        
                           beforeSend : function(){
                           	$(".LoadingImage").show();
                           },
                           success: function(data) {
                           	$(".LoadingImage").hide();
                           	
                           	$('.popup_info_msg').show();
    						$(".popup_info_msg .position-alert1").html("Saved Successfully..");    
    						setTimeout(function(){ location.reload(); },1000);

                           }
                        });           
      		}	
      	});

      	$('#edit_menu_form').validate({
      		rules : {
      			emenu_name : {required: true},
      			emenu_url : 
      			{
      				required : function(element)
      				{ 
		      			if( $("input[name='link_type']").val() ==  'direct' )
		      			{
		      				return true;
		      			}
		      			else
		      			{
		      				return false;
		      			}
      		 		} 
      			}
      			
      			},
      		submitHandler: function(form) {
      			
      			var formData = new FormData($("#edit_menu_form")[0]);
      			var editId = $("#editId").val();

      			$.ajax({      				
                           url: '<?php echo base_url();?>Menu_management/editMenu/'+editId,                           
                           type : 'POST',
                           data : formData,  
                           contentType : false,
                           processData : false,                        
                           beforeSend : function(){
                           	$(".LoadingImage").show();
                           },
                           success: function(data) {
                           	$(".LoadingImage").hide();
                           	
                           	$('.popup_info_msg').show();
    						$(".popup_info_msg .position-alert1").html("Edited Successfully..");    
    						setTimeout(function(){ location.reload(); },1000);

                           }
                        });           
      		}	
      	});
      	
      	
      	
      	function readURL( input , preview ) {
         if (input.files && input.files[0]) {
             var reader = new FileReader();
   
             reader.onload = function (e) {

                 preview.attr('src', e.target.result);
                 preview.attr("style","display:block");

                     }
             reader.readAsDataURL(input.files[0]);
        }
     }
   
     $(".menu_icon").change(function () {
         readURL(this , $('.file1_preview') );
     });

	$("#eMenu_icon").change(function () {
	         readURL(this , $('.editfile1_preview') );
	     });


	$(".showEdit").click(function()
	{
		var id = $("input[name='parent_id']").val();
		$.ajax({
			url:"<?php echo base_url()?>Menu_management/getEditData/"+id,
			dataType:'json',
			beforeSend : function(){
				$(".LoadingImage").show();
			},
			success:function(data){

				$("#editId").val(data.menu_management_id)

				if(data.url == 0 ) 
          {
            $("#sub_menuType").trigger('click');
          } 
				else
          {
            $("#direct_menuType").trigger('click');
          }

        if(data.parent_id == 0 )
          {
  				  $(".editfile1_preview").attr('src',data.icon_path);
  				  $(".editfile1_preview").show();
            $('#eIcon_content').show();
          }
          else
          {
            $('#eIcon_content').hide();
          }
          
          $("#eMenu_url").parent().find("li[data-value='"+data.url+"']").trigger('click');
        
        	$("#eMenuName").val(data.label);
        	$("#edit_modal").modal('show');			
				  $(".LoadingImage").hide();
			}
		});
	});
      $("#deleteMenu").click(function(){

        var id = $("input[name='parent_id']").val();
        var action = function()
        {          
          $.get( "<?php echo base_url()?>Menu_management/deleteMenu/"+id, function( data ) {
            if( parseInt(data) )
            {
              $('.popup_info_msg').show();
              $(".popup_info_msg .position-alert1").html("Deleted Successfully..");    
              setTimeout(function(){ location.reload(); },1000);
            }
          });
        };
        Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to delete this menu ?'});  
        $('#Confirmation_popup').modal('show');

      });

      });
</script>    