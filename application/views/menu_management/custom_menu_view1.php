<?php
$this->load->view('includes/header1');
?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.flexdatalist.css">
<div class="pcoded-content card-removes clientredesign">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     	<div class="card">
                        <!-- admin start-->

							<input type="text"
							       placeholder="Front-End Dev Skills"
							       class="flexdatalist"
							       data-min-length="0"
							       list="skills"
							       id="skill">
							<datalist id="skills">
							  <option value="Node.js">Node.js</option>
							  <option value="Accessibility">Accessibility</option>
							  <option value="Wireframing">Wireframing</option>
							  <option value="HTML5">HTML5</option>
							  <option value="CSS3">CSS3</option>
							  <option value="DOM Manipulation">DOM Manipulation</option>
							  <option value="MVC">MVC</option>
							  <option value="MVVM">MVVM</option>
							  <option value="JavaScript">JavaScript</option>
							  <option value="jQuery">jQuery</option>
							  <option value="ReactJS">ReactJS</option>
							</datalist>
					</div>
					</div>
					</div>
					</div>
				</div>
		</div>
	</div>
</div>


<?php
$this->load->view('includes/footer');
?>	
<script type="text/javascript" src="<?php echo base_url()?>/assets/js/jquery.flexdatalist.js"></script>
<script>
/*	$('SELECTOR').flexdatalist({
minLength:0
});*/
</script>