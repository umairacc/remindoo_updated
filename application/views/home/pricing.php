<?php $this->load->view('home/home_header'); ?>
<style type="text/css">
  .cont_active
  {
    display: block;
  }
  .cont_inactive
  {
    display: none;
  }
  .ScrollStyle
  {
      max-height: 750px;
      overflow-y: scroll;
      margin-bottom: 10px;
  }
</style>
<!--Header-->
<header class="header-home header-home--color header-home--center-content header-home--pricing">
  <div class="background background--wave">
    <div class="container">      
      <div class="row header-home__btns header-home__btns-pricing">
        <!-- <div class="col-6">
          <a href="10_get-app.html" class="site-btn site-btn--accent site-btn--right header-home__btn">
            Start Using for Free</a>
        </div> -->
        <div class="col-6">
          <a href="<?php echo base_url().'home/features'; ?>" class="site-btn site-btn--invert site-btn--left header-home__btn">
            Explore Features</a>
        </div>
      </div>
    </div>
  </div>
</header>
<!--Header-->

<div class="row text-center" style="margin-top: -146px;">
  <div class="col-12">
    <center><h2 class="header-home__title header-home__title--big"><?=page_content('price_page_title')?></h2></center>
    <p class="header-home__description header-home__description--big">
      </p>
  </div>
</div>

<!--Pricing Toggle-->
<div class="section section--first marno" style="margin-top: -1%;">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <p class="section__description">
         <?=page_content('price_page_description')?>
        </p>
      </div>
    </div>
    <div class="row pricing pricing--toggle plans">
      <div class="col-12">
        <div class="toggle-checkbox">
          <input class="toggle-checkbox__input" type="checkbox" id="pricing-toggle"/>
          <span class="toggle-checkbox__left">Monthly</span>
          <label class="toggle-checkbox__input-label" for="pricing-toggle">Toggle</label>
          <span class="toggle-checkbox__right">Yearly</span>
        </div>
      </div>
      <?php if(count($plans)>0){             
          
            foreach($plans as $key => $value) {

            if($value['type'] == '1'){ $class = 'monthplan'; }else{ $class = 'yearplan cont_inactive'; }
        
      ?>
      <div class="col-4 col-t-12 <?php echo $class; ?>">
        <div class="pricing__card card" style="height: 460px !important;">
          <h4 class="pricing__card-title"><?php echo ucwords($value['plan_name']); ?></h4>
          <div class="ScrollStyle" style="height: 290px !important;">
          <p><?php echo html_entity_decode($value['description']); ?></p>
          <p class=""><?php echo $value['plan_amount'].' '.$currency; ?></p> <br>
          <?php if($value['type'] == '2'){ ?>
          <p class="pricing__card-price pricing__card-price--month"><?php echo round(($value['plan_amount']/12),2).' '.$currency; ?></p>
          <p class=""><?php echo round(($value['plan_amount']/12),2).' '.$currency; ?> Billed monthly.</p> <!-- pricing__card-price--per-month -->
          <?php } ?>
          </div>
         <!--  <div class="pricing__card-price-save-wrap">
            <p class="pricing__card-price-save">Save 10%</p>
            <hr class="pricing__hr">
          </div> -->
        <!--   <div class="pricing__opportunities">
            <p>10 pages</p>
            <p>2 Gb storage</p>
            <p>Donations</p>
            <div class="pricing__opportunities--not-available">
              <p>Integrated e-commerce</p>
              <p>Custom domain free</p>
              <p>24/7 Customer support</p>
            </div>
          </div> -->
          <div style="height: 100px !important;">
          <a href="<?php echo base_url().'home/signup/'.$value['id']; ?>" class="site-btn site-btn--light">Select plan</a>
          </div>
        </div>
      </div>  
      <?php  } } ?> 
    </div>
    <div class="row text-center">
      <div class="col-12">
        <p class="section__description">
          <?= page_content('below_description'); ?>
        </p>
      </div>
    </div>
    <hr>
  </div>
</div>
<!--Pricing Toggle-->

<!--Integrate-->
<!-- <section class="section section--half section--bottom-space">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">Need a custom plan?</h3>
      </div>
    </div>
    <div class="row integrate integrate--calculator">
      <div class="col-12">
        <div class="integrate__card card">
          <div class="intagrate__logo">
            <div class="integrate__logo-svg">
              <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
                <path data-name="Sigma symbol" class="svg-element"
                      d="M237.418,8583.56a12.688,12.688,0,0,0,.419-3.37c-0.036-5.24-2.691-9.68-7.024-13.2h-3.878a20.819,20.819,0,0,1,4.478,13.01c0,4.56-2.456,10.2-6.413,11.4a16.779,16.779,0,0,1-2.236.51c-10.005,1.55-14.109-17.54-9.489-23.31,2.569-3.21,6.206-4.08,11.525-4.08h17.935A24.22,24.22,0,0,1,237.418,8583.56Zm-12.145-24.45c-8.571.02-12.338,0.98-16.061,4.84-6.267,6.49-6.462,20.69,4.754,27.72a24.092,24.092,0,1,1,27.3-32.57h-16v0.01Z"
                      transform="translate(-195 -8544)"/>
              </svg>
            </div>
            <p class="integrate__logo-title">Sigma Cost Calculator</p>
          </div>
          <p class="integrate__description">
            We’ve created this handy plan cost calculator just for you. Find out how much your custom plan will cost in
            under a minute!
          </p>
          <a href="11_calculator.html" class="site-btn site-btn--accent">Use Cost Calculator</a>
        </div>
      </div>
    </div>
  </div>
</section> -->
<!--Integrate-->

<!-- <div class="section section--light background background--clouds"></div> -->

<!--Pricing Common-->
<!-- <section class="section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <p class="section__description">Sigma’s monthly pricing is based on how many functions you need
          to start your work. If you ready to use Sigma for a long time you can choose yearly plan and
          save 10%.</p>
      </div>
    </div>
    <div class="row pricing pricing--common">
      <div class="col-10 col-m-12 col-offset-1">
        <div class="pricing__card card">
          <div class="pricing__card-price-wrap">
            <div class="pricing__card-price">
              <p id="month">$ 19.99</p>
              <p class="pricing__card-price-caption">per month</p>
            </div>
            <div class="pricing__card-price">
              <p id="year">$ 228.5</p>
              <p class="pricing__card-price-caption">per year</p>
            </div>
          </div>
          <select class="d-none" name="price-month" id="price">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select>
          <div class="pricing__plans">
            <div class="pricing__plan pricing__plan--hidden">
              <h4 class="pricing__card-title">Select plan</h4>
            </div>
            <div class="pricing__plan">
              <h4 class="pricing__card-title">Basic</h4>
              <div class="pricing__opportunities">
                <p>10 pages</p>
                <p>2 Gb storage</p>
                <p>Donations</p>
              </div>
            </div>
            <div class="pricing__plan active">
              <h4 class="pricing__card-title">Regular</h4>
              <div class="pricing__opportunities">
                <p>20 pages</p>
                <p>16 Gb storage</p>
                <p>Donations</p>
                <p>Integrated e-commerce</p>
                <p>Custom domain free</p>
              </div>
            </div>
            <div class="pricing__plan pricing__plan--disable">
              <h4 class="pricing__card-title">Premium</h4>
              <div class="pricing__opportunities">
                <p>30 pages</p>
                <p>100 Gb storage</p>
                <p>Donations</p>
                <p>Integrated e-commerce</p>
                <p>Custom domain free</p>
                <p>24/7 Customer support</p>
              </div>
            </div>
            <div class="pricing__plan pricing__plan--disable pricing__plan--last">
              <h4 class="pricing__card-title">Enterprise</h4>
              <div class="pricing__opportunities">
                <p>50 pages</p>
                <p>500 Gb storage</p>
                <p>Donations</p>
                <p>Integrated e-commerce</p>
                <p>Custom domain free</p>
                <p>24/7 Customer support</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row pricing pricing--text">
      <div class="col-offset-1 col-10 col-m-12">
        <h4>Ready to use Sigma?</h4>
      </div>
      <div class="col-5 col-t-12 col-offset-1">
        <p>Sigma has plans, from free to paid, that scale with your needs. Subscribe to a plan that fits
          the size of your business.</p>
      </div>
      <div class="col-5 col-t-12 site-btns--right btn-offset pricing--select-btns">
        <a class="site-btn site-btn--accent">Select plan</a>
        <a class="site-btn site-btn--light" href="07_about.html">Learn more</a>
      </div>
    </div>
    <hr>
  </div>
</section> -->
<!--Pricing Common-->

<!--Pricing simple-->
<!-- <section class="section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <p class="section__description">Sigma’s monthly pricing is based on how many functions you need to start
          your work. If you ready to use Sigma for a long time you can choose yearly plan and save some money.
        </p>
      </div>
    </div>
    <div class="row pricing pricing--simple">
      <div class="col-3 col-t-6">
        <div class="pricing__card card">
          <div class="pricing__card-head pricing__card-head--other-color-1">
            <h4 class="pricing__card-title">Basic</h4>
            <p class="pricing__card-price pricing__card-price--year">$ 108<span
                    class="pricing__card-price-caption">/year</span></p>
            <p class="pricing__card-price--per-month">$9 billed monthly</p>
          </div>
          <div class="pricing__opportunities">
            <p>10 pages</p>
            <p>2 Gb storage</p>
            <p>Donations</p>
            <div class="pricing__opportunities--not-available">
              <p>Integrated e-commerce</p>
              <p>Custom domain free</p>
              <p>24/7 Customer support</p>
            </div>
          </div>
          <a href="" class="site-btn site-btn--light">Select plan</a>
        </div>
      </div>
      <div class="col-3 col-t-6">
        <div class="pricing__card pricing__card--recommend card">
          <div class="pricing__card-head">
            <p class="pricing__card-recommend">Recommended</p>
            <h4 class="pricing__card-title">Business</h4>
            <p class="pricing__card-price pricing__card-price--year">$ 228<span
                    class="pricing__card-price-caption">/year</span></p>
            <p class="pricing__card-price--per-month">$19 billed monthly</p>
          </div>
          <div class="pricing__opportunities">
            <p>20 pages</p>
            <p>16 Gb storage</p>
            <p>Donations</p>
            <p>Integrated e-commerce</p>
            <p>Custom domain free</p>
            <div class="pricing__opportunities--not-available">
              <p>24/7 Customer support</p>
            </div>
          </div>
          <a href="" class="site-btn site-btn--accent">Select plan</a>
        </div>
      </div>
      <div class="col-3 col-t-6">
        <div class="pricing__card card">
          <div class="pricing__card-head pricing__card-head--other-color-2">
            <h4 class="pricing__card-title">Pro</h4>
            <p class="pricing__card-price pricing__card-price--year">$ 468<span
                    class="pricing__card-price-caption">/year</span></p>
            <p class="pricing__card-price--per-month">$39 billed monthly</p>
          </div>
          <div class="pricing__opportunities">
            <p>50 pages</p>
            <p>500 Gb storage</p>
            <p>Donations</p>
            <p>Integrated e-commerce</p>
            <p>Custom domain free</p>
            <p>24/7 Customer support</p>
          </div>
          <a href="" class="site-btn site-btn--light">Select plan</a>
        </div>
      </div>
      <div class="col-3 col-t-6">
        <div class="pricing__card card">
          <div class="pricing__card-head pricing__card-head--other-color-3">
            <h4 class="pricing__card-title">Enterprise</h4>
            <p class="pricing__card-price pricing__card-price--year">Custom</p>
            <p class="pricing__card-price--per-month">$9 billed monthly</p>
          </div>
          <div class="pricing__opportunities">
            <p>50 pages</p>
            <p>500 Gb storage</p>
            <p>Donations</p>
            <p>Integrated e-commerce</p>
            <p>Custom domain free</p>
            <p>24/7 Customer support</p>
          </div>
          <a href="" class="site-btn site-btn--light">Select plan</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <p class="section__description">Sigma has plans, from free to paid, that scale with your needs. Subscribe
          to a plan that fits the size of your business. All plans start with a free 15-day trial.
        </p>
      </div>
    </div>
  </div>
</section> -->
<!--Pricing simple-->


<!--Native-->
<section class="section section--light section--bottom-space">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title"><?=page_content('price_cloud_title')?></h3>
        <p class="section__description"><?= page_content('price_cloud_description'); ?></p>
      </div>
    </div>
    <div class="native">
    <!--   <div class="row native__btns">
        <div class="col-6">
          <a href="10_get-app.html" class="site-btn site-btn--accent site-btn--right native__btn-first">
            Start Using for Free</a>
        </div>
        <div class="col-6">
          <a href="05_features.html" class="site-btn site-btn--invert site-btn--left">
            Explore Features</a>
        </div>
      </div> -->
      <div class="row">
        <div class="col-12">
          <img alt="" class="native__img" src="<?php echo base_url();?>assets1/img/img_native.png">
        </div>
      </div>
    </div>
  </div>
</section>
<!--Native-->

<!--Plan details-->
<!-- <section class="section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">Plan details</h3>
        <p class="section__description">We believe that customer relationship should be a part of every business.
          All our plans have been designed to fit most business needs.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="site-table">
          <table class="tablesaw tablesaw-swipe" data-tablesaw-mode="swipe">
            <thead class="site-table__head">
            <tr>
              <th class="title" scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist"></th>
              <th class="site-table__head-th site-table__head-th--other-color-1">
                <p>Basic</p>
              </th>
              <th class="site-table__head-th site-table__head-th--accent">
                <p>Business</p>
              </th>
              <th class="site-table__head-th site-table__head-th--other-color-2">
                <p>Pro</p>
              </th>
              <th class="site-table__head-th site-table__head-th--other-color-3">
                <p>Enterprise</p>
              </th>
            </tr>
            </thead>
            <tbody class="site-table__body">
            <tr class="site-table__row">
              <th class="site-table__th">Users included</th>
              <td class="site-table__td"><p>5</p></td>
              <td class="site-table__td"><p>20</p></td>
              <td class="site-table__td"><p>50</p></td>
              <td class="site-table__td site-table__td--bold"><p>Unlimited</p></td>
            </tr>
            <tr class="site-table__row">
              <th class="site-table__th">Conversations</th>
              <td class="site-table__td site-table__td--bold"><p>Unlimited</p></td>
              <td class="site-table__td site-table__td--bold"><p>Unlimited</p></td>
              <td class="site-table__td site-table__td--bold"><p>Unlimited</p></td>
              <td class="site-table__td site-table__td--bold"><p>Unlimited</p></td>
            </tr>
            <tr class="site-table__row">
              <th class="site-table__th">Desktop and Mobile Apps</th>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
            </tr>
            <tr class="site-table__row">
              <th class="site-table__th">Notifications (Mobile + Email)</th>
              <td class="site-table__td"></td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
            </tr>
            <tr class="site-table__row">
              <th class="site-table__th site-table__th--new">Invisible mode</th>
              <td class="site-table__td">
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
            </tr>
            <tr class="site-table__row">
              <th class="site-table__th site-table__th--new">Search engine</th>
              <td class="site-table__td">
              </td>
              <td class="site-table__td">
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
            </tr>
            <tr class="site-table__row">
              <th class="site-table__th site-table__th--new">User Database</th>
              <td class="site-table__td">
              </td>
              <td class="site-table__td">
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
            </tr>
            <tr class="site-table__row">
              <th class="site-table__th">User Import/Export</th>
              <td class="site-table__td">
              </td>
              <td class="site-table__td">
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
            </tr>
            <tr class="site-table__row">
              <th class="site-table__th">Campaigns (Email + Chat)</th>
              <td class="site-table__td"><p>50 recipients</p></td>
              <td class="site-table__td"><p>1 000 recipients</p></td>
              <td class="site-table__td"><p>100 000 recipients</p></td>
              <td class="site-table__td"><p>200 000 recipients</p></td>
            </tr>
            <tr class="site-table__row">
              <th class="site-table__th">Customization</th>
              <td class="site-table__td"><p>Basic</td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
              <td class="site-table__td">
                <p><span class="site-table__icon"><i class="mdi mdi-check"></i></span></p>
              </td>
            </tr>
            <tr class="site-table__btn-row">
              <th class="site-table__th"></th>
              <td class="site-table__td">
                <p><a href="" class="site-btn site-btn--light site-btn--max-width">Select plan</a></p>
              </td>
              <td class="site-table__td">
                <p><a href="" class="site-btn site-btn--accent site-btn--max-width">Select plan</a></p>
              </td>
              <td class="site-table__td">
                <p><a href="" class="site-btn site-btn--light site-btn--max-width">Select plan</a></p>
              </td>
              <td class="site-table__td">
                <p><a href="" class="site-btn site-btn--light site-btn--max-width">Select plan</a></p>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section> -->
<!--Plan details-->

<!--Trusted by-->
<section class="section section--last">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title"><?=page_content('logo_price_title')?></h3>
      </div>
    </div>
    <div class="row logo logo--bottom-space">
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_blue_1.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_blue_2.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_blue_3.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_blue_4.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_blue_5.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_blue_6.png">
        </div>
      </div>
    </div>
  </div>
  <img alt="" class="section__img" src="<?php echo base_url();?>assets1/img/img_backgroud_footer.png">
</section>
<!--Trusted by-->

<?php $this->load->view('home/home_footer'); ?>
<script type="text/javascript">
  
  $('#pricing-toggle').on('click',function()
  {
      if($(this).prop('checked') == true)
      {
         $('.plans').find('.monthplan').each(function()
         {
             $(this).removeClass('cont_active');
             $(this).addClass('cont_inactive');
         });

         $('.plans').find('.yearplan').each(function()
         {
             $(this).removeClass('cont_inactive');
             $(this).addClass('cont_active');
         });
         
      }
      else
      {
         $('.plans').find('.monthplan').each(function()
         {
             $(this).removeClass('cont_inactive');
             $(this).addClass('cont_active');
         });

         $('.plans').find('.yearplan').each(function()
         {
             $(this).removeClass('cont_active');
             $(this).addClass('cont_inactive');
         });
      }
  });

</script>