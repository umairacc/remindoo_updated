<?php $this->load->view('home/home_header'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/css/stripe_payment.css" data-rel-css="" />

<section class="section section--last section--top-space">
<div class="container">
<div class="innerpage">
  <div class="innerpage_start">
    <div class="about_blog">

    <div class="payment_container">
      <div class="about_blog_inner">
          <h4><strong>Plan :</strong> <?php echo $plan['plan_name']; ?></h4>
          <h4><strong>Amount :</strong> <?php echo $plan['plan_amount'].' '.$currency; ?></h4>
      </div>     

      <label for="card-element">
            Pay by Card
      </label>

      <div class="cell pay_by_card card_pay" id="example-2">
  
        <form>
          <div data-locale-reversible>
          
          <div class="row">
            <div class="field">
              <div id="card_pay-card-number" class="input empty"></div>
              <label for="card_pay-card-number" data-tid="elements_examples.form.card_number_label">Card number</label>
              <div class="baseline"></div>
            </div>
          </div>
          <div class="row">
            <div class="field half-width">
              <div id="card_pay-card-expiry" class="input empty"></div>
              <label for="card_pay-card-expiry" data-tid="elements_examples.form.card_expiry_label">Expiration</label>
              <div class="baseline"></div>
            </div>
            <div class="field half-width">
              <div id="card_pay-card-cvc" class="input empty"></div>
              <label for="card_pay-card-cvc" data-tid="elements_examples.form.card_cvc_label">CVC</label>
              <div class="baseline"></div>
            </div>
          </div>
          <div class="error" role="alert">
            <span class="message"></span>
          </div>
          <button type="submit" data-tid="elements_examples.form.pay_button">Pay&nbsp; <?php echo $plan['plan_amount'].' '.$currency; ?></button>
        </form>

    </div>

    </div>
  </div>
</div>
</div>
</div>
<img alt="Background" class="section__img" src="<?php echo base_url();?>assets1/img/img_backgroud_footer.png"></section>

<script src="<?php echo base_url();?>assets1/js/vendor/jquery-2.2.4.min.js"></script>
<script src="https://js.stripe.com/v3/"></script>

<script type="text/javascript">

  var publish_key = '<?php echo $publish_key; ?>';
  var title = '<?php echo $plan['plan_name']; ?>';
  var amount = '<?php echo ($plan['plan_amount']*100); ?>';
  var client_secret = '<?php echo $intent->client_secret; ?>';
  var stripe_failure_url = "<?php echo $stripe_failure_url; ?>";
  var stripe_success_url = "<?php echo $stripe_success_url; ?>";
  var currency = '<?php echo strtolower($currency); ?>';
  
  
</script>

<script src="<?php echo base_url() ?>/assets/js/stripe_payment.js" data-rel-js></script>