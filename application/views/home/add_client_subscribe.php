<?php $this->load->view('home/home_header'); ?>
<style type="text/css">
  .form-error
  {
    color:red;
  } 
  .form-success
  {
    color:green;
  } 
</style>
<section class="section section--last section--top-space">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">Add Clients And Users</h3>
      </div>
    </div>
    <div class="row form">
      <div class="col-10 col-m-12 col-offset-1">
        <div class="form__card card form__card--background" style="background-image: url('<?php echo base_url(); ?>assets1/img/hero-imgnew1.png')">
          <div class="form__wrap">
           <!--  <h4>Get the sigma for free</h4> -->            
            <div class="form-error" role="alert" <?php if(empty($_SESSION['error_msg'])){ echo "style='display:none;'"; } ?>>
              <?php if(!empty($_SESSION['error_msg'])){ echo $_SESSION['error_msg']; } ?>
            </div>
            <div class="form-success" role="alert" <?php if(empty($_SESSION['success_msg'])){ echo "style='display:none;'"; } ?>>
              <?php if(!empty($_SESSION['success_msg'])){ echo html_entity_decode($_SESSION['success_msg']); } ?>
            </div>
            
            <?php if(!empty($firm['payment_details'])){ ?>
            <form class="form__form js-form" method="post" action="<?php echo base_url().'home/client_subscription/'.$firm['firm_id'].'/'.$plans[0]['id']; ?>" style="margin-right: 30px !important;">      
             <?php }else{ ?>
             <form class="form__form js-form" method="post" action="<?php echo base_url().'home/payment/'.$firm['firm_id'].'/'.$plans[0]['id']; ?>" style="margin-right: 30px !important;"> 
             <?php } ?>

             <?php if($plans[0]['unlimited'] != "1" && $plans[0]['limited'] != "1"){ ?>
              <div class="form__form-group form__form-group--small" style="width: 153px !important;">
              <label class="form__label">Amount Per Client</label>
              <input class="form__input js-field__company" type="text" name="camount" value="<?php echo $plans[0]['cost_per_client']; ?>" readonly> 
              </div> 

              <div class="form__form-group form__form-group--small form__form-group--right" style="width: 100px !important;"> 
              <label class="form__label">Currency</label>
              <input class="form__input js-field__company" type="text" value="<?php echo $currency; ?>" name="currency" readonly>
              </div> 

              <div class="form__form-group form__form-group--small form__form-group--right" style="width: 153px !important;">
                <label class="form__label">Client Count</label>
                <input class="form__input js-field__email" type="text" name="no_of_clients" placeholder="Client Count" data-validation="required,number">
              </div>  

             <?php }if($plans[0]['unlimited'] != "0" && $plans[0]['limited'] != "0"){ ?>

              <div class="form__form-group form__form-group--small" style="width: 153px !important;">
              <label class="form__label">Amount Per User</label>
              <input class="form__input js-field__company" type="text" name="uamount" value="<?php echo $plans[0]['cost_per_user']; ?>" readonly>
              </div>

              <div class="form__form-group form__form-group--small form__form-group--right" style="width: 100px !important;"> 
              <label class="form__label">Currency</label>
              <input class="form__input js-field__company" type="text" value="<?php echo $currency; ?>" name="currency" readonly>
              </div>      

              <div class="form__form-group form__form-group form__form-group--small form__form-group--right" style="width: 153px !important;">
                <label class="form__label">User Count</label>
                <input class="form__input js-field__email" type="text" name="no_of_users" placeholder="Client Count" data-validation="required,number">
              </div> 

              <?php } ?>     
              <div class="form__form-group">
                <label class="form__label">Total Cost</label>
                <input class="form__input js-field__email" type="text" id="cost" placeholder="Total Cost" readonly>
              </div>             
              <input type="hidden" name="total_cost">
              <button class="site-btn site-btn--accent form__submit" style="margin-top: 30px;" type="submit" value="Send">Pay</button>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<img alt="Background" class="section__img" src="<?php echo base_url();?>assets1/img/img_backgroud_footer.png"></section>
<!--Form-->


<?php $this->load->view('home/home_footer'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.79/jquery.form-validator.js"></script>
<script>
  $.validate({
    lang: 'en'
  }); 
</script>

<script type="text/javascript"> 

  var currency = "<?php echo $currency; ?>";

  $('input[name="no_of_clients"],input[name="no_of_users"]').keyup(function()
  { 
       var camount = "<?php echo $plans[0]['cost_per_client']; ?>";       
       var c_cost = 0;

       if($('input[name="no_of_clients"]').val()>0)
       {
         c_cost = camount*$('input[name="no_of_clients"]').val();
       }

       var uamount = "<?php echo $plans[0]['cost_per_user']; ?>";
       var u_cost = 0;

       if($('input[name="no_of_users"]').val()>0)
       {
         u_cost = uamount*$('input[name="no_of_users"]').val();
       } 
      
       total_cost = Number(c_cost) + Number(u_cost);      

       $('input[name="total_cost"]').val(total_cost);       
       $('#cost').val(total_cost+' '+currency);
      
  }); 

</script>  