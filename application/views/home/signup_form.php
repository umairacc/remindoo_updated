<?php $this->load->view('home/home_header'); ?>
<style type="text/css">
  .form-error
  {
    color:red;
  }  
</style>
<!--Form-->

<section class="section section--last section--top-space">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">Take the first step to Remindoo.</h3>
      </div>
    </div>
    <div class="row form">
      <div class="col-10 col-m-12 col-offset-1">
        <div class="form__card card form__card--background" style="background-image: url('<?php echo base_url(); ?>assets1/img/img_ui_3.png')">  
          <div class="form__wrap">
           <!--  <h4>Get the sigma for free</h4> -->
            <p>Learn how to sell smarter and faster.</p>
            <div class="form-error" role="alert" <?php if(empty($_SESSION['error_msg'])){ echo "style='display:none;'"; } ?>>
              <?php if(!empty($_SESSION['error_msg'])){ echo $_SESSION['error_msg']; } ?>
            </div>
            <div role="alert" <?php if(empty($_SESSION['success_msg'])){ echo "style='display:none;'"; } ?>>
              <?php if(!empty($_SESSION['success_msg'])){ echo html_entity_decode($_SESSION['success_msg']); } ?>
            </div>
            <form class="form__form js-form" method="post" action="<?php echo base_url().'home/checkout'; ?>" style="margin-right: 30px !important;" id="signup_form">
              <div class="form__form-group form__form-group--small">
                <label class="form__label">First Name</label>
                <input class="form__input js-field__first-name" type="text" name="first_name" placeholder="Your First Name" data-validation="required">
              </div>
              <div class="form__form-group form__form-group--small form__form-group--right">
                <label class="form__label">Last Name</label>
                <input class="form__input js-field__last-name" type="text" name="last_name" placeholder="Your Last Name" data-validation="required">
              </div>
              <div class="form__form-group">
                <label class="form__label">Email</label>
                <input class="form__input js-field__email" type="email" name="email" placeholder="Your Email" data-validation="required,email">
                <span id="email_er" style="color:red;"></span>
              </div>
              <div class="form__form-group">
                <label class="form__label">Phone Number</label>
              <!--   <select name="phonecode" class="form__input js-field__company" style="width:100px !important;" data-validation="required">
                  <option value=''>Select</option>
                  <?php foreach($countries as $key=> $lsource_value) { ?>
                  <option value='<?php echo $lsource_value['phonecode'];?>'><?php echo $lsource_value['sortname'].'-'.$lsource_value['phonecode'];?></option>
                  <?php } ?>
                </select> -->
                <input class="form__input js-field__phone" type="text" name="phone_numder" placeholder="Your Phone Number" data-validation="required,number,length" data-validation-length="10-16"> <!-- style="width:335px !important;"s -->
              </div>
              <div class="form__form-group">
                <label class="form__label">Company Name</label>
                <input class="form__input js-field__company" type="text" data-validation="required" name="company_name" placeholder="Your Company Name">
              </div>                         
              <div class="form__form-group">
                <label class="form__label">Plan</label>
                <select class="form__input js-field__company" name="plan" data-validation="required">
                  <option value="">Select</option>
                  <?php if(count($plans)>0){ 
                    foreach($plans as $key => $value) { ?>
                      <option value="<?php echo $value['id']; ?>"><?php echo $value['plan_name']; ?></option>
                  <?php } } ?>
                </select>
              </div>

              <div class="form__form-group">
                <label class="form__label">Company Size (Number of Emploees)</label>
                <label class="radio-btn">
                  <input class="radio-btn__radio" type="radio" value="1-5" name="company-size" checked>
                  <span class="radio-btn__radio-custom"></span>
                  <span class="radio-btn__label">1-5</span>
                </label>
                <label class="radio-btn">
                  <input class="radio-btn__radio" type="radio" value="6-50" name="company-size">
                  <span class="radio-btn__radio-custom"></span>
                  <span class="radio-btn__label">6-50</span>
                </label>
                <label class="radio-btn">
                  <input class="radio-btn__radio" type="radio" value="51-200" name="company-size">
                  <span class="radio-btn__radio-custom"></span>
                  <span class="radio-btn__label">51-200</span>
                </label>
                <label class="radio-btn">
                  <input class="radio-btn__radio" type="radio" value="201-1000" name="company-size">
                  <span class="radio-btn__radio-custom"></span>
                  <span class="radio-btn__label">201-1000</span>
                </label>
                <label class="radio-btn">
                  <input class="radio-btn__radio" type="radio" value="1001+" name="company-size">
                  <span class="radio-btn__radio-custom"></span>
                  <span class="radio-btn__label">1001+</span>
                </label>
              </div>
             
              <div class="form__form-group form__form-group--small">
              <label class="form__label">Amount</label>
              <input class="form__input js-field__company" type="text" name="amount" readonly> 
              </div> 
              <div class="form__form-group form__form-group--small form__form-group--right">           
              <label class="form__label">Currency</label>
              <input class="form__input js-field__company" type="text" name="currency" value="<?php echo $currency; ?>" readonly>
              </div>  

              <div class="form__form-group clientcount">
                <label class="form__label">Client Count</label>
                <input class="form__input js-field__email" type="text" name="no_of_clients" placeholder="Client Count" data-validation="required,number">
              </div>

              <div class="form__form-group usercount">
                <label class="form__label">User Count</label>
                <input class="form__input js-field__email" type="text" name="no_of_users" placeholder="User Count" data-validation="required,number">
              </div>
              
              <span id="benefits"></span>

              <div class="form__form-group form__form-group--read-and-agree">
                <label class="checkbox-btn">
                  <input class=" checkbox-btn__checkbox" type="checkbox" name="terms" data-validation="required">
                  <span class="checkbox-btn__checkbox-custom"><i class="mdi mdi-check"></i></span>
                  <span class="checkbox-btn__label">I read and agree to
										<a href="<?php echo base_url(); ?>home/terms" target="_blank" class="link link--accent link--accent-bold"> Terms & Conditions</a></span>
                </label>
                <span id="terms_er" style="color:red;"></span>
              </div>
              <button class="site-btn site-btn--accent form__submit" type="submit" value="Send">Submit </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<img alt="Background" class="section__img" src="<?php echo base_url();?>assets1/img/img_backgroud_footer.png"></section>
<!--Form-->

<?php $this->load->view('home/home_footer'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.79/jquery.form-validator.js"></script>
<script>
  $.validate({
    lang: 'en'
  }); 
</script>

<script type="text/javascript">

  $('select[name="plan"]').on('change',function()
  { 
     $.ajax(
     {
         url:'<?php echo base_url().'home/get_plan_details'; ?>',
         type: 'POST',
         data:{'id':$(this).val()},        

         success:function(data)
         {
            var res = JSON.parse(data);
            var benefits = "";

            $('input[name="amount"]').val(res.plan_amount);                  

            if(res.trial == '1')
            {
               $('.clientcount').hide();
               $('.usercount').hide();
            }
            else if(res.unlimited == '1')
            {
               $('.clientcount').hide();

               if(res.limited == '0')
               {
                  $('.usercount').hide();
               }
               else
               {
                  $('.usercount').show();
               }      

               benefits = benefits.concat('<p>Can Have Unlimited Clients.</p>');
            }
            else if(res.unlimited == '0')
            {
               $('.usercount').hide();

               if(res.limited == '1')
               {
                  $('.clientcount').hide();
               }
               else
               {
                  $('.clientcount').show();
               }  

               benefits = benefits.concat('<p>Can Have Unlimited Users.</p>');
            }
            else if(res.unlimited == '' || res.limited == '')
            {
               $('.clientcount').show();
               $('.usercount').show();
            }    

            if(res.limited == '1')
            {               
               benefits = benefits.concat('<p>Can Have '+res.client_limit+' Client(s).</p>');
            }
            else if(res.limited == '0')
            {
               benefits = benefits.concat('<p>Can Have '+res.user_limit+' User(s).</p>');
            }            
            
            if(benefits.length>0)
            {
              $('#benefits').html('<p style="margin-top: 17px !important;">Your Benefits</p>').css('color','#ec43b1');
              $('#benefits').append(benefits).css('color','#ec43b1');
            }
            else
            {
              $('#benefits').html('');
            }
         }
     });
  });

  $(document).ready(function()
  {
     var segment = '<?php echo $this->uri->segment(3); ?>';

     if(segment!="")
     {
        $('select[name="plan"]').val(segment).trigger('change');
     }
  });

  $('#signup_form').on('submit',function(event)
  {
     var email = $('input[name="email"]').val();

     if($('input[name="terms"]').prop('checked') == false)
     {
        $('#terms_er').html('This is a required feild.');
        event.preventDefault();
     }
     else
     {
        $('#terms_er').html('');
     }

     if(email != "")
     {
        $.ajax(
        {
           url:'<?php echo base_url().'home/check_email'; ?>',
           type:'POST',
           data:{email:email},
           async:false,

           success:function(result)
           { 
              if(result>0)
              {
                 $('#email_er').html('Already Exists.');
                 event.preventDefault();
              }
           }        
        });     
     }     
  });  

</script>