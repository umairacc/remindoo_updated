<?php $this->load->view('home/home_header'); ?>

<!--Header-->
<header class="header-home">
  <div class="container background background--right background--header background--mobile"
       style="background-image: url('<?php echo base_url();?>assets1/img/hero-imgnew1.png');">
    <div class="row">
      <div class="col-12">
        <h2 class="header-home__title">Great App that makes your life easier</h2>
        <p class="header-home__description">The simple, intuitive and powerful app to manage your work.
          Explore app of the next generation for free and become a part of community of like-minded members.</p>
        <div class="header-home__btns header-home__btns-mobile">
          <!-- <a href="10_get-app.html" class="site-btn site-btn--accent header-home__btn">Start Using for Free</a> -->
          <a href="<?php echo base_url().'home/features'; ?>" class="site-btn site-btn--light header-home__btn">Explore Features</a>
        </div>
      </div>
    </div>
  </div>
</header>
<!--Header-->

<!--Features-->
<section class="section featurescls">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <!-- <h3 class="section__title">The only app you’ll need to power your life</h3>
        <p class="section__description">Meet Sigma. The simple, intuitive and powerful app to manage your work.
          Explore app of the next generation for free and become a part of community of like-minded members.</p> -->
          <h3 class="section__title">The only app you’ll need to power your life</h3>
      </div>
    </div>
    <div class="row carousel">
      <div class="col-12">
        <div class="carousel__navigation">
          <ul id="carousel__navigation-feature-slider" class="carousel__navigation-items" role="tablist">
            <li class="carousel__navigation-item slick-active" role="presentation" tabindex="0">
                <img src="<?php echo base_url();?>assets1/img/taskicon.png">
              <p>Tasks</p>
            </li>
            <li class="carousel__navigation-item" role="presentation" tabindex="1">
              <img src="<?php echo base_url();?>assets1/img/deadline.png">
              <p>Deadline Manager</p>
            </li>
            <li class="carousel__navigation-item" role="presentation" tabindex="2">
              <img src="<?php echo base_url();?>assets1/img/timesheet.png">
              <p>Time Sheet</p>
            </li>
            <li class="carousel__navigation-item" role="presentation" tabindex="3">
               <img src="<?php echo base_url();?>assets1/img/prposal.png">
              <p>Proposal</p>
            </li>
            <li class="carousel__navigation-item" role="presentation" tabindex="4">
               <img src="<?php echo base_url();?>assets1/img/client.png">
              <p>client</p>
            </li>
            <!-- <li class="carousel__navigation-item" role="presentation" tabindex="5">
               <img src="<?php echo base_url();?>assets1/img/timesheet.png">
              <p>24h Support</p>
            </li> -->
          </ul>
        </div>
      </div>
      <div class="col-12">
        <div class="slider slider-img slider-img--big" id="feature-slider">
          <div class="carousel__slide">
            <div class="carousel__slide-content">
              <h4 class="carousel__slide-title">Tasks</h4>
              <p class="carousel__slide-description">Lorem ipsum dolor sit amet, consectetur adipiscing
                elit. Pellentesque placerat eros ac finibus congue. Integer consectetur, lorem nec
                accumsan commodo, sem mauris pharetra arcu, id viverra eros ipsum ac lorem.
                Suspendisse potenti.</p>
            </div>
            <div class="carousel__slide-img">
              <!-- <img alt="" src="assets/img/img_feature_screen_1.png"> -->
              <img alt="" src="<?php echo base_url();?>assets1/img/img_feature_screen_5.png">
            </div>
          </div>
          <div class="carousel__slide">
            <div class="carousel__slide-content">
              <h4 class="carousel__slide-title">Deadline Manager</h4>
              <p class="carousel__slide-description">Lorem ipsum dolor sit amet, consectetur adipiscing
                elit. Pellentesque placerat eros ac finibus congue. Integer consectetur, lorem nec
                accumsan commodo, sem mauris pharetra arcu, id viverra eros ipsum ac lorem.
                Suspendisse potenti.</p>
            </div>
            <div class="carousel__slide-img">
              <img alt="" src="assets1/img/img_feature_screen_5.png">
            </div>
          </div>
          <div class="carousel__slide">
            <div class="carousel__slide-content">
              <h4 class="carousel__slide-title">Time Sheet</h4>
              <p class="carousel__slide-description">Lorem ipsum dolor sit amet, consectetur adipiscing
                elit. Pellentesque placerat eros ac finibus congue. Integer consectetur, lorem nec
                accumsan commodo, sem mauris pharetra arcu, id viverra eros ipsum ac lorem.
                Suspendisse potenti.</p>
            </div>
            <div class="carousel__slide-img">
              <img alt="" src="assets1/img/img_feature_screen_5.png">
            </div>
          </div>
          <div class="carousel__slide">
            <div class="carousel__slide-content">
              <h4 class="carousel__slide-title">Proposal</h4>
              <p class="carousel__slide-description">Lorem ipsum dolor sit amet, consectetur adipiscing
                elit. Pellentesque placerat eros ac finibus congue. Integer consectetur, lorem nec
                accumsan commodo, sem mauris pharetra arcu, id viverra eros ipsum ac lorem.
                Suspendisse potenti.</p>
            </div>
            <div class="carousel__slide-img">
              <img alt="" src="assets1/img/img_feature_screen_5.png">
            </div>
          </div>
          <div class="carousel__slide">
            <div class="carousel__slide-content">
              <h4 class="carousel__slide-title">client</h4>
              <p class="carousel__slide-description">Lorem ipsum dolor sit amet, consectetur adipiscing
                elit. Pellentesque placerat eros ac finibus congue. Integer consectetur, lorem nec
                accumsan commodo, sem mauris pharetra arcu, id viverra eros ipsum ac lorem.
                Suspendisse potenti.</p>
            </div>
            <div class="carousel__slide-img">
              <img alt="" src="assets1/img/img_feature_screen_5.png">
            </div>
          </div>
          <!-- <div class="carousel__slide">
            <div class="carousel__slide-content">
              <h4 class="carousel__slide-title">Shape Your Ideas</h4>
              <p class="carousel__slide-description">Lorem ipsum dolor sit amet, consectetur adipiscing
                elit. Pellentesque placerat eros ac finibus congue. Integer consectetur, lorem nec
                accumsan commodo, sem mauris pharetra arcu, id viverra eros ipsum ac lorem.
                Suspendisse potenti.</p>
            </div>
            <div class="carousel__slide-img">
              <img alt="" src="assets1/img/img_feature_screen_6.png">
            </div>
          </div> -->
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="site-btn--center">
          <a class="site-btn site-btn--light" href="<?php echo base_url().'home/features'; ?>">Learn more</a>
        </div>
      </div>
    </div>
    <hr>
  </div>
</section>
<!--Features-->

<!--Video-->
<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">Explore app of the next generation.<br/>
          Watch the video.</h3>
      </div>
    </div>
    <div class="row video">
      <div class="col-offset-1 col-10 col-t-12 ">
        <div class="video__video" data-video="_DHPmGeXa-k">
          <div class="video__play"><img src="<?php echo base_url();?>assets1/img/img_play_btn.png" alt=""></div>
          <div class="embed-responsive">
            <div class="embed-responsive-item"></div>
          </div>
        </div>
        <!--<div class="video__wrap">-->
        <!--<video class="video__content" controls="controls">-->
        <!--<source src="assets/video/somethingmorev2mp4.mp4">-->
        <!--</video>-->
        <!--</div>-->
      </div>
    </div>
    <hr>
  </div>
</section>
<!--Video-->

<!--What is-->
<section class="section">
  <div class="container">
   <!--  <div class="row">
      <div class="col-12">
        <h3 class="section__title">What is Sigma?</h3>
        <p class="section__description">Meet Sigma. The simple, intuitive and powerful app to manage your work.
          Explore app of the next generation for free and become a part of community of like-minded members.</p>
      </div>
    </div> -->
    <div class="row about-app">
      <div class="col-6 about-app__description">
        <div class="about-app__description-content about-app__description-content--left">
          <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
            <path class="svg-element"
                  d="M496.649,3155.93a1.94,1.94,0,0,0-1.939,1.94v11.78a2.476,2.476,0,0,1-2.473,2.47H461.351a2.475,2.475,0,0,1-2.473-2.47v-30.89a2.475,2.475,0,0,1,2.473-2.47h11.781a1.94,1.94,0,0,0,0-3.88H461.351a6.359,6.359,0,0,0-6.351,6.35v30.89a6.359,6.359,0,0,0,6.351,6.35h30.886a6.359,6.359,0,0,0,6.351-6.35v-11.78A1.94,1.94,0,0,0,496.649,3155.93Zm5.783-18.54-8.824-8.82a1.935,1.935,0,0,0-2.743,0l-22.06,22.06a1.919,1.919,0,0,0-.568,1.37v8.82a1.94,1.94,0,0,0,1.939,1.94H479a1.948,1.948,0,0,0,1.371-.56l22.061-22.07A1.936,1.936,0,0,0,502.432,3137.39ZM478.2,3158.88h-6.082v-6.08l20.122-20.12,6.082,6.08Z"
                  transform="translate(-455 -3128)"/>
          </svg>
          <h4 class="about-app__description-title">Create</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque placerat eros ac finibus congue.
            Integer consectetur, lorem nec accumsan commodo, sem mauris pharetra arcu, id viverra eros ipsum ac lorem.
            Suspendisse potenti.</p>
        </div>
      </div>
      <div class="col-6 about-app__img">
        <div class="about-app__img-wrap">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_ui_1.png">
        </div>
      </div>
    </div>
    <div class="row about-app about-app--reverse">
      <div class="col-6 about-app__description">
        <div class="about-app__description-content">
          <svg xmlns="http://www.w3.org/2000/svg" width="42" height="48" viewBox="0 0 42 48">
            <path class="svg-element"
                  d="M1019.32,3629.62a4.6,4.6,0,0,0-1.57-1.56h0l-32.688-19.41h-0.01a4.7,4.7,0,0,0-6.4,1.61,4.616,4.616,0,0,0-.653,2.36h0v38.76a4.649,4.649,0,0,0,4.671,4.62h0a4.637,4.637,0,0,0,2.381-.65h0.009l32.689-19.38v-0.01A4.578,4.578,0,0,0,1019.32,3629.62Zm-3.6,3.02-32.67,19.38a0.758,0.758,0,0,1-.91-0.12,0.715,0.715,0,0,1-.219-0.52v-38.76h0a0.735,0.735,0,0,1,.1-0.38,0.75,0.75,0,0,1,.459-0.34,0.721,0.721,0,0,1,.565.08l32.67,19.4a0.667,0.667,0,0,1,.25.25A0.725,0.725,0,0,1,1015.72,3632.64Z"
                  transform="translate(-978 -3608)"/>
          </svg>
          <h4 class="about-app__description-title">Run</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque placerat eros ac finibus congue.</p>
          <!-- <a href="10_get-app.html" class="site-btn site-btn--accent about-app__btn">Start Using for Free</a> -->
        </div>
      </div>
      <div class="col-6 about-app__img about-app__img--left">
        <div class="about-app__img-wrap">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_ui_2.png">
        </div>
      </div>
    </div>
    <div class="row about-app">
      <div class="col-6 about-app__description">
        <div class="about-app__description-content about-app__description-content--left">
          <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
            <path class="svg-element"
                  d="M495.623,4102.78a1.367,1.367,0,0,1,1.246-.83h0.181a5.95,5.95,0,1,0,0-11.9h-0.333a1.379,1.379,0,0,1-1.136-.61,1.979,1.979,0,0,0-.126-0.4,1.372,1.372,0,0,1,.268-1.51l0.106-.1s0.006-.01.009-0.01a5.952,5.952,0,0,0,0-8.41s-0.006-.01-0.009-0.01a5.922,5.922,0,0,0-4.2-1.74h0a5.885,5.885,0,0,0-4.2,1.75l-0.111.11a1.354,1.354,0,0,1-1.506.26H485.78a1.376,1.376,0,0,1-.83-1.25v-0.18a5.95,5.95,0,0,0-11.9,0v0.33a1.368,1.368,0,0,1-.612,1.14,2.688,2.688,0,0,0-.4.12,1.354,1.354,0,0,1-1.506-.26l-0.115-.12a5.918,5.918,0,0,0-4.2-1.74h0a5.951,5.951,0,0,0-4.206,10.16l0.112,0.11a1.375,1.375,0,0,1,.268,1.51c-0.017.04-.032,0.08-0.046,0.11a1.36,1.36,0,0,1-1.236.9H460.95a5.95,5.95,0,0,0,0,11.9h0.333a1.368,1.368,0,0,1,1.254.83,0.167,0.167,0,0,1,.008.02,1.354,1.354,0,0,1-.268,1.5l-0.111.12h-0.008a5.95,5.95,0,0,0,.009,8.41c0,0.01.005,0.01,0.008,0.01a5.95,5.95,0,0,0,8.409-.01l0.111-.11a1.373,1.373,0,0,1,1.5-.27,0.542,0.542,0,0,0,.114.05,1.362,1.362,0,0,1,.9,1.24v0.15a5.95,5.95,0,1,0,11.9,0v-0.33a1.38,1.38,0,0,1,.831-1.26,0.019,0.019,0,0,0,.019-0.01,1.373,1.373,0,0,1,1.5.27l0.115,0.12a5.922,5.922,0,0,0,4.205,1.74h0a5.953,5.953,0,0,0,4.205-10.16l-0.111-.12a1.356,1.356,0,0,1-.268-1.5C495.618,4102.79,495.62,4102.79,495.623,4102.78Zm-2.506,4.24c0,0.01.01,0.01,0.015,0.02l0.121,0.12a2.076,2.076,0,0,1-1.467,3.54h0a2.077,2.077,0,0,1-1.467-.61l-0.12-.12s-0.01-.01-0.016-0.01a5.271,5.271,0,0,0-8.95,3.75v0.34a2.072,2.072,0,0,1-4.144,0v-0.18c0-.02,0-0.03,0-0.05a5.406,5.406,0,0,0-9.11-3.7s-0.01.01-.015,0.01l-0.121.12a2.071,2.071,0,0,1-2.931,0H464.9a2.076,2.076,0,0,1,0-2.93s0.005,0,.007-0.01l0.116-.11c0-.01.01-0.01,0.015-0.02a5.274,5.274,0,0,0-3.752-8.95H460.95a2.07,2.07,0,1,1,0-4.14h0.226a5.41,5.41,0,0,0,3.707-9.11c0-.01-0.01-0.01-0.015-0.02l-0.121-.12a2.076,2.076,0,0,1,1.466-3.54h0a2.032,2.032,0,0,1,1.466.61l0.121,0.12s0.01,0.01.015,0.01a5.2,5.2,0,0,0,5.413,1.2,1.87,1.87,0,0,0,.517-0.14,5.244,5.244,0,0,0,3.181-4.81v-0.34a2.072,2.072,0,0,1,4.144,0v0.19a5.271,5.271,0,0,0,8.95,3.74s0.01-.01.016-0.01l0.121-.12a2.024,2.024,0,0,1,1.465-.61h0a2.052,2.052,0,0,1,1.461.6s0,0.01.006,0.01a2.049,2.049,0,0,1,.608,1.46,2.079,2.079,0,0,1-.6,1.47h0l-0.121.12c0,0.01-.01.01-0.015,0.02a5.215,5.215,0,0,0-1.2,5.41,1.986,1.986,0,0,0,.141.52,5.257,5.257,0,0,0,4.811,3.18h0.341a2.07,2.07,0,1,1,0,4.14h-0.188A5.274,5.274,0,0,0,493.117,4107.02ZM479,4088.04a7.96,7.96,0,1,0,7.955,7.96A7.966,7.966,0,0,0,479,4088.04Zm0,12.04a4.08,4.08,0,1,1,4.077-4.08A4.083,4.083,0,0,1,479,4100.08Z"
                  transform="translate(-455 -4072)"/>
          </svg>
          <h4 class="about-app__description-title">Integrate</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque placerat eros ac finibus congue.
            Integer consectetur, lorem nec accumsan commodo, sem mauris pharetra arcu, id viverra eros ipsum ac lorem.
            Suspendisse potenti. Vestibulum aliquam blandit scelerisque. Aliquam hendrerit vestibulum lorem non
            placerat. Etiam at diam id erat tincidunt ullamcorper.</p>
        </div>
      </div>
      <div class="col-6 about-app__img">
        <div class="about-app__img-wrap">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_ui_3.png">
        </div>
      </div>
    </div>
    <div class="row about-app about-app--reverse">
      <div class="col-6 about-app__description">
        <div class="about-app__description-content">
          <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
            <path class="svg-element"
                  d="M1016.16,4592H981.841a6.849,6.849,0,0,0-6.841,6.84v39.22a1.937,1.937,0,0,0,3.31,1.37l9.237-9.24h28.613a6.843,6.843,0,0,0,6.84-6.84v-24.51A6.85,6.85,0,0,0,1016.16,4592Zm2.96,31.35a2.968,2.968,0,0,1-2.96,2.97H986.744a1.946,1.946,0,0,0-1.371.56l-6.5,6.5v-34.54a2.966,2.966,0,0,1,2.963-2.96h34.319a2.966,2.966,0,0,1,2.96,2.96v24.51Z"
                  transform="translate(-975 -4592)"/>
          </svg>
          <h4 class="about-app__description-title">Communicate</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque placerat eros ac finibus congue.
            Integer consectetur, lorem nec accumsan commodo, sem mauris pharetra arcu, id viverra eros ipsum ac
            lorem. Suspendisse potenti.</p>
        </div>
      </div>
      <div class="col-6 about-app__img about-app__img--left">
        <div class="about-app__img-wrap">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_ui_4.png">
        </div>
      </div>
    </div>
    <hr>
  </div>
</section>
<!--What is-->

<!--Pricing simple-->
<section class="section">
  <div class="container">
    <!-- <div class="row">
      <div class="col-12">
        <p class="section__description">Sigma’s monthly pricing is based on how many functions you need to start
          your work. If you ready to use Sigma for a long time you can choose yearly plan and save some money.
        </p>
      </div>
    </div> -->
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">Subscription</h3>
      </div>
    </div>
    <div class="row pricing pricing--simple">

      <?php if(count($plans)>0){ 
            
          $colors = array('color-1','color-2','color-3');
          $count = 0;
            foreach($plans as $key => $value) {

          if($count/3=='1'){ $count = 0; }
      ?>
      <div class="col-3 col-t-6">
        <div class="pricing__card card">
          <div class="pricing__card-head pricing__card-head--other-<?php echo $colors[$count]; ?>">
            <h4 class="pricing__card-title"><?php echo ucwords($value['plan_name']); ?></h4>
            <p class="pricing__card-price pricing__card-price--year"><span  class="pricing__card-price-caption"><?php echo $value['plan_amount'].' '.$currency; ?>/<?php if($value['type'] == '1'){ echo "Month"; }else{ echo "Year"; } ?></span></p>
               <?php if($value['type'] == '2'){ ?>
              <p class="pricing__card-price--per-month"><?php echo round(($value['plan_amount']/12),2).' '.$currency; ?> Billed monthly.</p>
               <?php } ?>
          </div>
          <div class="pricing__opportunities">           
            <div class="pricing__opportunities--not-available">
              <p><?php echo html_entity_decode($value['description']); ?></p>
            </div>
          </div>
          <a href="<?php echo base_url().'home/signup/'.$value['id']; ?>" class="site-btn site-btn--light">Select plan</a>
        </div>
      </div>
     <?php  $count++; } } ?>
    </div>
    <div class="row">
      <div class="col-12">
        <p class="section__description">Plans, from free to paid, that scale with your needs. Subscribe
          to a plan that fits the size of your business. All plans start with a free 15-day trial.
        </p>
      </div>
    </div>
    <hr>
  </div>
</section>
<!--Pricing simple-->


<!--Questions-->
<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">Have any questions?</h3>
      </div>
    </div>
<!--     <div class="row questions">
      <div class="col-4 col-m-12">
        <div class="questions__card card">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_question_1.png" class="questions__icon">
          <h4 class="questions__title">Who is Sigma for?</h4>
          <p class="questions__answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
            placerat eros ac finibus congue. Integer consectetur, lorem nec accumsan commodo, sem mauris pharetra
            arcu, id viverra eros ipsum ac lorem. Suspendisse potenti.</p>
        </div>
      </div>
      <div class="col-4 col-m-12">
        <div class="questions__card card">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_question_2.png" class="questions__icon">
          <h4 class="questions__title">How Sigma works?</h4>
          <p class="questions__answer">Sed tristique commodo bibendum. Orci varius natoque penatibus et magnis dis
            parturient montes, nascetur ridiculus mus. Cras molestie arcu in mauris euismod, quis faucibus urna
            aliquam.</p>
        </div>
      </div>
      <div class="col-4 col-m-12">
        <div class="questions__card card">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_question_3.png" class="questions__icon">
          <h4 class="questions__title">In which countries is Sigma available?</h4>
          <p class="questions__answer">Donec non vestibulum eros, auctor elementum felis. Mauris arcu lectus,
            vestibulum in aliquet a, interdum in enim. Orci varius natoque penatibus et magnis dis parturient montes,
            nascetur ridiculus mus. Aenean facilisis enim dui, ut hendrerit ligula lobortis sit amet. Nunc pulvinar
            risus id lectus posuere, at rutrum dui commodo.</p>
        </div>
      </div>
    </div> -->
    <div class="row">
      <div class="col-12">
        <div class="site-btn--center">
          <a class="site-btn site-btn--light" href="<?php echo base_url(); ?>home/faq">Read more</a>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Questions-->

<!--Testimonials-->
<section class="section section--light">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">What users think about us.</h3>
      </div>
    </div>
    <div class="row feedback">
      <div class="col-4 col-m-6 feedback__item">
        <div class="feedback__card card">
          <img alt="" class="feedback__avatar" src="<?php echo base_url();?>assets1/img/img_avatar_1.png">
          <a href="" class="feedback__social"><span><i class="mdi mdi-twitter" aria-hidden="true"></i></span></a>
          <div class="feedback__header">
            <p class="feedback__name">Sarah</p>
            <a href="" class="link link--gray feedback__nick">@sarah-dev</a>
          </div>
          <p class="feedback__main">I love <a href="" class="link link--accent"></a> - fantastic app, and
            providing <a href="" class="link link--accent">#nonprofits</a> and <a href="" class="link link--accent">
              #students</a> freememberships :) Thanks for making life easy! <a href="" class="link link--accent">#mobile
            </a> <a href="" class="link link--accent">#work</a> <a href="" class="link link--accent">#free</a></p>
        </div>
      </div>
      <div class="col-4 col-m-6 feedback__item">
        <div class="feedback__card card">
          <img alt="" class="feedback__avatar" src="<?php echo base_url();?>assets1/img/img_avatar_2.png">
          <a href="" class="feedback__social"><span><i class="mdi mdi-twitter" aria-hidden="true"></i></span></a>
          <div class="feedback__header">
            <p class="feedback__name">John</p>
            <a href="" class="link link--gray feedback__nick">@john_doe</a>
          </div>
          <p class="feedback__main">Best app ever!!! All free streaming <a href="" class="link link--accent"></a>
            for my fellow blues fans <a href="" class="link link--accent">@Fred</a>
            <a href="" class="link link--accent">@George etc</a></p>
        </div>
      </div>
      <div class="col-4 col-m-6 feedback__item">
        <div class="feedback__card card">
          <img alt="" class="feedback__avatar" src="<?php echo base_url();?>assets1/img/img_avatar_3.png">
          <a href="" class="feedback__social"><span><i class="mdi mdi-twitter" aria-hidden="true"></i></span></a>
          <div class="feedback__header">
            <p class="feedback__name">Pharah</p>
            <a href="" class="link link--gray feedback__nick">@pharah</a>
          </div>
          <p class="feedback__main">Success of <a href="" class="link link--accent"></a> - app crossed 16
            million downloads with more than 4 million active users.
            <a href="" class="link link--accent">#congratulations</a></p>
        </div>
      </div>
      <div class="col-4 col-m-6 feedback__item">
        <div class="feedback__card card">
          <img alt="" class="feedback__avatar" src="<?php echo base_url();?>assets1/img/img_avatar_4.png">
          <a href="" class="feedback__social"><span><i class="mdi mdi-twitter" aria-hidden="true"></i></span></a>
          <div class="feedback__header">
            <p class="feedback__name">Andrew</p>
            <a href="" class="link link--gray feedback__nick">@penandrew</a>
          </div>
          <p class="feedback__main">I can't stop telling people about how good <a href="" class="link link--accent">
            </a> app! Best purchase ever.</p>
        </div>
      </div>
      <div class="col-4 col-m-6 feedback__item">
        <div class="feedback__card card">
          <img alt="" class="feedback__avatar" src="<?php echo base_url();?>assets1/img/img_avatar_5.png">
          <a href="" class="feedback__social"><span><i class="mdi mdi-twitter" aria-hidden="true"></i></span></a>
          <div class="feedback__header">
            <p class="feedback__name">George</p>
            <a href="" class="link link--gray feedback__nick">@GeorgeW</a>
          </div>
          <p class="feedback__main"><a href="" class="link link--accent"></a> Would you please add vector
            icons to your app? Thanks in advance)</p>
        </div>
      </div>
      <div class="col-4 col-m-6 feedback__item">
        <div class="feedback__card card">
          <img alt="" class="feedback__avatar" src="<?php echo base_url();?>assets1/img/img_avatar_6.png">
          <a href="" class="feedback__social"><span><i class="mdi mdi-twitter" aria-hidden="true"></i></span></a>
          <div class="feedback__header">
            <p class="feedback__name">Jeff</p>
            <a href="" class="link link--gray feedback__nick">@JJfree</a>
          </div>
          <p class="feedback__main">Just buy a license for <a href="" class="link link--accent"></a> pro! Its
            been super great to manage my new <a href="" class="link link--accent">#website</a>! Thanks a lot!</p>
        </div>
      </div>
      <div class="col-4 col-m-6 feedback__item">
        <div class="feedback__card card">
          <img alt="" class="feedback__avatar" src="<?php echo base_url();?>assets1/img/img_avatar_7.png">
          <a href="" class="feedback__social"><span><i class="mdi mdi-twitter" aria-hidden="true"></i></span></a>
          <div class="feedback__header">
            <p class="feedback__name">Jay George</p>
            <a href="" class="link link--gray feedback__nick">@Jay_George</a>
          </div>
          <p class="feedback__main">The new version of <a href="" class="link link--accent feedback__nick"></a>
            is really fantastic. My favourite manage tool; especially with its excellent
            <a href="<?php echo base_url().'home/features'; ?>" class="link link--accent feedback__nick">#collaboration</a> features.</p>
        </div>
      </div>
      <div class="col-4 col-m-6 feedback__item">
        <div class="feedback__card card">
          <img alt="" class="feedback__avatar" src="<?php echo base_url();?>assets1/img/img_avatar_8.png">
          <a href="" class="feedback__social"><span><i class="mdi mdi-twitter" aria-hidden="true"></i></span></a>
          <div class="feedback__header">
            <p class="feedback__name">Robert</p>
            <a href="" class="link link--gray feedback__nick">@rob_Z</a>
          </div>
          <p class="feedback__main"><a href="" class="link link--accent feedback__nick"></a> thanks for the
            great tool. You are really the best. Amazing. I am 3 years with you already. Nobody compares. Don't
            stop!</p>
        </div>
      </div>
      <div class="col-4 col-m-6 feedback__item">
        <div class="feedback__card card">
          <img alt="" class="feedback__avatar" src="<?php echo base_url();?>assets1/img/img_avatar_9.png">
          <a href="" class="feedback__social"><span><i class="mdi mdi-twitter" aria-hidden="true"></i></span></a>
          <div class="feedback__header">
            <p class="feedback__name">Jess</p>
            <a href="" class="link link--gray feedback__nick">@a-jess</a>
          </div>
          <p class="feedback__main">You can now do some badass diagramming with
            <a href="" class="link link--accent feedback__nick"></a></p>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Testimonials-->

<!--Trusted by-->
<section class="section section--dark">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">Trusted by</h3>
      </div>
    </div>
    <div class="row logo">
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_1.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_2.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_3.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_4.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_5.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_6.png">
        </div>
      </div>
    </div>
  </div>
</section>
<!--Trusted by-->

<!--Download-->
<section class="section section--last">
  <div class="container download" style="display: none;">
    <div class="row">
      <div class="col-12">
        <h3 class="download__title">Download today</h3>
        <h3>and get started with a free trial for your business</h3>
      </div>
    </div>
    <div class="row download__btns">
      <div class="col-6">
        <a href="" class="site-btn site-btn--accent site-btn--download site-btn--right download__btn-first">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_appstore.png"></a>
      </div>
      <div class="col-6">
        <a href="" class="site-btn site-btn--dark site-btn--download site-btn--left">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_google.png"></a>
      </div>
    </div>
  </div>
  <img alt="" class="section__img" src="<?php echo base_url();?>assets1/img/img_backgroud_footer.png">
</section>
<!--Download-->

<?php $this->load->view('home/home_footer'); ?>