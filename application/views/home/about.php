<?php $this->load->view('home/home_header'); ?>

<!--Header-->
<header class="header-home header-home--color header-home--bottom-space">
  <div class="background background--wave">
    <div class="container">
      <div class="row header-home__about-img-wrap">
        <img class="header-home__about-img" src="<?php echo base_url();?>assets1/img/hero-img.png" alt="">
        <div class="col-12">
          <h2 class="header-home__title ">Meet <span
                  class="header-home__title--accent"></span></h2>
          <p class="header-home__description header-home__description--big header-home__description--about">
            </p>
        </div>
      </div>
    </div>
  </div>
</header>
<!--Header-->

<!--What is-->
<section class="section section--left-content">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title"></h3>
        <p class="section__description">
        </p>
      </div>
    </div>
    <hr>
  </div>
</section>
<!--What is-->

<!--With us-->
<section class="section section--left-content">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">Working with us</h3>
      </div>
    </div>
    <div class="row opportunities">
      <div class="col-4 col-l-6">
        <div class="opportunities__opportunity">
          <p class="opportunities__title">Creative Space</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec iaculis nibh, quis venenatis odio.
            Suspendisse cursus interdum sem, ultrices interdum.</p>
        </div>
      </div>
      <div class="col-4 col-l-6">
        <div class="opportunities__opportunity">
          <p class="opportunities__title">Collabaration</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec iaculis nibh, quis venenatis odio.
            Suspendisse cursus interdum sem, ultrices interdum.</p>
        </div>
      </div>
      <div class="col-4 col-l-6">
        <div class="opportunities__opportunity">
          <p class="opportunities__title">Growing Your Career</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec iaculis nibh, quis venenatis odio.
            Suspendisse cursus interdum sem, ultrices interdum.</p>
        </div>
      </div>
      <div class="col-4 col-l-6">
        <div class="opportunities__opportunity">
          <p class="opportunities__title">Benefits</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec iaculis nibh, quis venenatis odio.
            Suspendisse cursus interdum sem, ultrices interdum.</p>
        </div>
      </div>
      <div class="col-4 col-l-6">
        <div class="opportunities__opportunity">
          <p class="opportunities__title">Perks</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec iaculis nibh, quis venenatis odio.
            Suspendisse cursus interdum sem, ultrices interdum.</p>
        </div>
      </div>
      <div class="col-4 col-l-6">
        <div class="opportunities__opportunity">
          <p class="opportunities__title">Cookies</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec iaculis nibh, quis venenatis odio.
            Suspendisse cursus interdum sem, ultrices interdum.</p>
        </div>
      </div>
    </div>
    <div class="row gallery">
      <div class="col-6 c gallery__wrap">
        <img alt="" class="gallery__item gallery__item--big" src="<?php echo base_url();?>assets1/img/img_photo_1.png">
      </div>
      <div class="col-6 gallery__wrap">
        <img alt="" class="gallery__item gallery__item--small" src="<?php echo base_url();?>assets1/img/img_photo_2.png">
        <img alt="" class="gallery__item gallery__item--small" src="<?php echo base_url();?>assets1/img/img_photo_3.png">
        <div class="gallery__item gallery__item--medium">
          <h3 class="gallery__item-title">2014</h3>
          <p class="gallery__item-description">Founded</p>
        </div>
      </div>
      <div class="col-6 gallery__wrap">
        <img alt="" class="gallery__item gallery__item--medium" src="<?php echo base_url();?>assets1/img/img_photo_4.png">
        <div class="gallery__item gallery__item--small">
          <h3 class="gallery__item-title">5,000+</h3>
          <p class="gallery__item-description">Happy customers</p>
        </div>
        <img alt="" class="gallery__item gallery__item--small" src="<?php echo base_url();?>assets1/img/img_photo_5.png">
      </div>
      <div class="col-6 gallery__wrap">
        <img alt="" class="gallery__item gallery__item--big" src="<?php echo base_url();?>assets1/img/img_photo_6.png">
      </div>
      <div class="col-6 gallery__wrap">
        <img alt="" class="gallery__item gallery__item--medium" src="<?php echo base_url();?>assets1/img/img_photo_7.png">
      </div>
      <div class="col-6 gallery__wrap">
        <div class="gallery__item gallery__item--medium">
          <h3 class="gallery__item-title">$48 milion</h3>
          <p class="gallery__item-description">In funding</p>
        </div>
      </div>
    </div>
    <hr>
  </div>
</section>
<!--With us-->

<!--Leadership-->
<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">Leadership</h3>
      </div>
    </div>
    <div class="row leadership">
      <div class="col-3 col-t-6">
        <div class="leadership__item">
          <img alt="" class="leadership__avatar" src="<?php echo base_url();?>assets1/img/img_leadership_1.png">
          <p class="leadership__name">Michael Logan</p>
          <p class="leadership__work">Founder</p>
        </div>
      </div>
      <div class="col-3 col-t-6">
        <div class="leadership__item">
          <img alt="" class="leadership__avatar" src="<?php echo base_url();?>assets1/img/img_leadership_2.png">
          <p class="leadership__name">John Brown</p>
          <p class="leadership__work">CEO / Founder</p>
        </div>
      </div>
      <div class="col-3 col-t-6">
        <div class="leadership__item">
          <img alt="" class="leadership__avatar" src="<?php echo base_url();?>assets1/img/img_leadership_3.png">
          <p class="leadership__name">Mike Smith</p>
          <p class="leadership__work">CEO / Founder</p>
        </div>
      </div>
      <div class="col-3 col-t-6">
        <div class="leadership__item">
          <img alt="" class="leadership__avatar" src="<?php echo base_url();?>assets1/img/img_leadership_4.png">
          <p class="leadership__name">George Right</p>
          <p class="leadership__work">VP, Worldwide Sales</p>
        </div>
      </div>
      <div class="col-3 col-t-6">
        <div class="leadership__item">
          <img alt="" class="leadership__avatar" src="<?php echo base_url();?>assets1/img/img_leadership_5.png">
          <p class="leadership__name">Li Chang</p>
          <p class="leadership__work">Managing Director</p>
        </div>
      </div>
      <div class="col-3 col-t-6">
        <div class="leadership__item">
          <img alt="" class="leadership__avatar" src="<?php echo base_url();?>assets1/img/img_leadership_6.png">
          <p class="leadership__name">Jill Valentine</p>
          <p class="leadership__work">Art Director</p>
        </div>
      </div>
      <div class="col-3 col-t-6">
        <div class="leadership__item">
          <img alt="" class="leadership__avatar" src="<?php echo base_url();?>assets1/img/img_leadership_7.png">
          <p class="leadership__name">Sarah Jonson</p>
          <p class="leadership__work">Developer</p>
        </div>
      </div>
      <div class="col-3 col-t-6">
        <div class="leadership__item">
          <img alt="" class="leadership__avatar" src="<?php echo base_url();?>assets1/img/img_leadership_8.png">
          <p class="leadership__name">Stan Cloud</p>
          <p class="leadership__work">Designer</p>
        </div>
      </div>
    </div>
    <hr>
  </div>
</section>
<!--Leadership-->

<!--Long Way-->
<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">We Have Come A Long Way</h3>
      </div>
      <div class="row events">
        <div class="col-12">
          <div class="events__event events__event--right">
            <p class="events__event-title">Our Start</p>
            <p class="events__event-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec
              iaculis nibh, quis venenatis odio. Suspendisse cursus interdum sem, ultrices interdum.</p>
          </div>
        </div>
        <div class="col-12">
          <div class="events__event events__event--left">
            <p class="events__event-title">The early days</p>
            <p class="events__event-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
              pulvinar auctor libero, vel porttitor nulla laoreet quis. Nullam scelerisque libero sit amet blandit
              interdum. Fusce quis mauris varius, lobortis lacus eleifend, hendrerit dui. Curabitur aliquam metus
              nec tempus porttitor.</p>
          </div>
        </div>
        <div class="col-12">
          <div class="events__event events__event--right">
            <p class="events__event-title">We are growing</p>
            <p class="events__event-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              In nec iaculis nibh, quis venenatis odio.</p>
          </div>
        </div>
        <div class="col-12">
          <div class="events__event events__event--left">
            <p class="events__event-title">Now</p>
            <p class="events__event-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec
              iaculis nibh, quis venenatis odio.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Long Way-->

<!--We believe-->
<section class="section section--light section--bottom-space">
  <div class="container">
    <div class="row believe">
      <div class="col-12">
        <img alt="" src="<?php echo base_url();?>assets1/img/img_believe_avatar.png" class="believe__avatar">
        <p class="believe__name">Li Chang</p>
        <p class="believe__work">Managing Director</p>
        <h4 class="believe__quote"></h4>
      </div>
    </div>
  </div>
</section>
<!--We believe-->

<!--Trusted by-->
<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section__title">Investors</h3>
      </div>
    </div>
    <div class="row logo logo--bottom-space">
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_blue_1.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_blue_2.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_blue_3.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_blue_4.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_blue_5.png">
        </div>
      </div>
      <div class="col-2 logo__img-wrap">
        <div class="logo__img">
          <img alt="" src="<?php echo base_url();?>assets1/img/img_logo_blue_6.png">
        </div>
      </div>
    </div>
  </div>
  <img alt="" class="section__img" src="<?php echo base_url();?>assets1/img/img_backgroud_footer.png">
</section>
<!--Trusted by-->

<?php $this->load->view('home/home_footer'); ?>