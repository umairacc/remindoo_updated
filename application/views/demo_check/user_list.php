<?php 
 $this->load->view('includes/header');
?>  
<link href="<?php echo base_url();?>assets/css/timepicki.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree_select/style.css?ver=2">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.tagsinput.css">


<style>
#table-buttonsList
{
  display: none;
}
.tableToolBar_selectDiv
{
  width: 200px;
}
   .show{
    display: block !important;
   }
   tfoot {
    display: table-header-group;
} 
   .dropdown-content {
   display: none;
   position: absolute;
   background-color: #fff;
   min-width: 86px;
   overflow: auto;
   box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   z-index: 999;
   left: -92px;
   width: 150px;
   }
 
   .dropdown {
   position: relative;
   display: inline-block;
   }

   .show {display:block;}
   .hide{
   display: none;
   }
   .popup-inner.archivecls .body {
    text-align: right;
    padding: 20px;
  }
  .popup-inner.archivecls label {
      padding-left: 20px;
  }
.closes_mail_id{
background: transparent;
border: none;
}

p.action_01 i.fa.fa-sign-in {
    Color: #6639b8;
    font-size: 18PX;
}
.colvis_Content_popup{display: none;}

#reassign_button
{
  background-image: url("<?php echo base_url()?>assets/images/new_update5-w.png");
  background-color:  #00a2e8 !important;
}
#ch_sync_button
{
  background: #99d9ea !important;
  color: #fff !important;
  background-image: url("<?php echo base_url()?>assets/images/archive.png");
}

.filter-data.for-reportdash .box-item
{
    display: inline-block;
    width: initial !important;
}
.filter-data.for-reportdash #clear_container.f-right
{
  margin-top: 0px !important;
}
.filter-data.for-reportdash
{
  margin-bottom: 20px;
  display: none;  
}
</style>
<?php if($this->session->flashdata('message')) {?>
<div class="modal-alertsuccess alert alert-success">
 <!--  <div class="newupdate_alert"> -->
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <div class="pop-realted1">
      <div class="position-alert1">
         <strong>Success!</strong> Your Feedback has been sent successfully.
      </div>
    </div>
   <!-- </div> -->
</div>
<?php } ?>
<?php if($this->session->flashdata('flashmessage')) {?>
<div class="modal-alertsuccess alert alert-success">
  <div class="newupdate_alert">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <div class="pop-realted1">
      <div class="position-alert1">
         <strong>Success!</strong> Your Message has been sent successfully.
      </div>
   </div>
 </div>
</div>
<?php } ?>
<?php if($this->session->flashdata('flashmessage_error')) {?>
<div class="modal-alertsuccess alert alert-danger">
  <div class="newupdate_alert">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
   <div class="pop-realted1">
      <div class="position-alert1">
         <strong>Error!</strong> Some thing wrong ,Please try again.
      </div>
    </div>
   </div>
</div>
<?php } ?>
<?php if($this->session->flashdata('message_error')) {?>
<div class="modal-alertsuccess alert alert-danger">
  <div class="newupdate_alert">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
   <div class="pop-realted1">
      <div class="position-alert1">
         <strong>Error!</strong> Some thing wrong ,Please try again.
      </div>
   </div></div>
</div>
<?php } ?>
<div class="modal-alertsuccess alert alert-success-delete" style="display: none;">
 <!--  <div class="newupdate_alert"> -->
   <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
   <div class="pop-realted1">
      <div class="position-alert1">
         User Deleted Successfully
      </div>
   </div>
 <!--   </div> -->
</div>

<div class="modal-alertsuccess alert alert-success-sent" style="display: none;">
  <div class="newupdate_alert">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
   <div class="pop-realted1">
      <div class="position-alert1">
         Mail Sent Successfully
      </div>
   </div></div>
</div>


<div class="modal-alertsuccess alert info_popup" style="display:none;">
  <div class="newupdate_alert">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
   <div class="pop-realted1">
      <div class="position-alert1">
         
       </div>
      </div>
   </div>
</div>

<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
                <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                              
                              </div> <!-- all-clients -->

                           <!-- clients details content -->

                           <div class="all_user-section floating_set clientredesign select-check">
                              <div class="deadline-crm1 floating_set">
                                 <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" href="javascript:;"><img class="themicon" src="<?php echo base_url();?>assets/images/tabicon1.png" alt="themeicon" />All Clients</a>
                                       <div class="slide"></div>
                                    </li>
                                    <?php 
                                    if( $_SESSION['permission']['Client_Section']['create']!=0 )
                                    {
                                      ?>
                                      <li class="nav-item">
                                         <a class="nav-link" href="<?php echo base_url();?>client/addnewclient"><img class="themicon" src="<?php echo base_url();?>assets/images/tabicon2.png" alt="themeicon" />New client</a>
                                         <div class="slide"></div>
                                      </li>
                                     <?php
                                     } 
                                    ?>
                                    <!-- <li class="nav-item">
                                       <a class="nav-link" data-popup-open="popup-1" href="#"><img class="themicon" src="<?php echo base_url();?>assets/images/tabicon3.png" alt="themeicon" />Email all</a>
                                       <div class="slide"></div>
                                     </li>
                                     <li class="nav-item">
                                       <a class="nav-link" data-popup-open="popup-2" href="#"><img class="themicon" src="<?php echo base_url();?>assets/images/tabicon4.png" alt="themeicon" />Give feedback</a>
                                       <div class="slide"></div>
                                     </li>  -->                                 
                                 </ul>




                                        <?php

                                  $allclient = $actclient =$nolongercli = $frozenclient = $allstus = $archive = $draft =array();

                                   foreach ($getallUser as $key => $status_val) 
                                   {
                                           $status = $status_val['status'];
                                             array_push($allstus, $status);
                                           if($status == '1') 
                                           {
                                               array_push($allclient, $status); 
                                           }
                                           if($status == '2' || $status=='0' || $status=='')
                                           {
                                               array_push($nolongercli, $status); 
                                           }
                                       
                                           if($status == '3') 
                                           {
                                               array_push($frozenclient, $status); 
                                           }
                                           if($status == '4') 
                                           {
                                               array_push($draft, $status); 
                                           }
                                           
                                          if($status == '5') 
                                          {
                                             array_push($archive, $status); 
                                          }

                                   } 

                                   ?>
                                   <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard don1 renewdesign">
                                    <li class="nav-item">
                                       <a class="nav-link  color1 tap_click active" href="javascript:void(0);"  data-id="All">All
                                       <span><?php echo count($allstus); ?></span></a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link color2 tap_click " href="javascript:void(0);"  data-id="1" data-child-id="active_checkbox">Active<span><?php echo count($allclient); ?></span></a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link color3 tap_click"   href="javascript:void(0);" data-id="|0|2" data-child-id="inactive_checkbox">Non-Active<span><?php echo count($nolongercli); ?></span></a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link color4 tap_click" href="javascript:void(0);"   data-id="3"
                                        data-child-id="frozen_checkbox"
                                       >Frozen<span><?php echo count($frozenclient); ?></span></a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link color1 tap_click" href="javascript:void(0);"   data-id="4"
                                        data-child-id="frozen_checkbox"
                                       >Draft<span><?php echo count($draft); ?></span></a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link color5 tap_click"  data-id="5" href="javascript:void(0);"  data-child-id="archive_checkbox">Archived<span><?php echo count($archive); ?></span></a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
								 
								<!-- vishnu start -->


                                   <div class="count-value1 pull-right userlist-count01 ">
                                  
                                    <button type="button" id="send_mail_sms" data-toggle="modal" data-target="#popup_send_mail_sms" data-child-class="user_checkbox"  class="del-tsk12 f-right" style="display:none;" ><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Send</button>
                                    <?php 
                                    if( $_SESSION['permission']['Client_Section']['edit']!=0 )
                                    {
                                      ?>
                                      
                                      <button type="button" id="ch_sync_button" class="del-tsk12 f-right" style="display:none;" >  
                                        <i class="fa fa-refresh fa-6" aria-hidden="true"></i>
                                        CH Sync
                                      </button>

                                      <button type="button" id="reassign_button" data-toggle="modal" data-target="#reassign_popup" data-child-class="user_checkbox"  class="del-tsk12 f-right" style="display:none;" >  
                                        <i class="fa fa-archive fa-6" aria-hidden="true"></i>
                                        Reassign
                                      </button>

                                      <button type="button" id="archive_button" data-child-class="user_checkbox"  class="del-tsk12 f-right" style="display:none;" >  
                                        <i class="fa fa-archive fa-6" aria-hidden="true"></i>
                                        Archive
                                      </button>
                                    
                                      <button type="button" id="unarchive_button"  data-child-class="archive_checkbox"  class="del-tsk12 f-right" style="display:none;" >
                                        <i class="fa fa-archive fa-6" aria-hidden="true"></i>
                                        Un Archive
                                      </button>
                                    <?php 
                                    }                                          
                                    if( $_SESSION['permission']['Client_Section']['delete']!=0 )
                                    {

                                      ?>  
                                    <button type="button" id="deleteUser_rec" data-toggle="modal" data-target="#deleteconfirmation" data-child-class="user_checkbox" class="del-tsk12 f-right"
                                     style="display:none;">
                                       <i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete
                                    </button> 
                                    <?php 
                                    }
                                    ?> 


                                     <!-- <button type="button" id="block_service" data-toggle="modal" data-target="#popup_service_block" data-child-class="user_checkbox" class="del-tsk12 f-right" style="display:none"><i class="fa fa-ban" aria-hidden="true"></i>Service Block</button>
 -->
                                     <!-- <button type="button" id="block_reminder" data-toggle="modal" data-target="#popup_reminder_block" data-child-class="user_checkbox" class="del-tsk12 f-right" style="display:none"><i class="fa fa-ban" aria-hidden="true"></i>Reminder Block</button> -->



                                    <!-- <button id="deleteActive_records" class="deleteActive_records" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete</button> -->
                                 </div>
								 
								 <!-- vishnu close -->
                                 
                              </div>                             
                              
                              <!-- table content Open -->
                              <div class="all_user-section2 floating_set">
                              
                                 <div class="tab-content table_content data_resetfilter">
                                   <div id="allusers" class="tab-pane fade in active">
                                       <div id="task"></div>
                                       <div class="client_section3 table-responsive">
                                          <div class="status_succ"></div>
                                          <div class="alluser_al data-padding1 ">    
                                             <div class="insert_client_table">                                        
                                              <?php
                                                 $this->load->view('demo_check/Client_list_table');
                                              ?>
                                             </div>
                                          </div>
                                       </div>
                                    </div>


                                    </div>
                              </div>
                               <!-- END OF  table content Open -->
                           </div>
                            <!-- clients details content -->

                           <!-- Register your self card end -->
                        </div>
                     </div>
                  </div>
                  <!-- Page body end -->
               </div>
            </div>
            <!-- Main-body end -->
            <div id="styleSelector">
            </div>
         </div>
      </div>
   </div>


<!-- email all -->
<div class="popup" data-popup="popup-1">
   <div class="popup-inner">
      <div class="head">
         <h3>Email Form</h3>
         <p></p>
      </div>
      <form action="<?php echo base_url();?>User/sendMail" id="form" method="post" name="form">
         <label>Your content</label>
         <!-- <textarea name="msg" id="msg" placeholder="Type your text here..."></textarea> -->
         <textarea id="editor4" name="editor4" placeholder="Type your text here..." ></textarea>
         <div id="error_msg" style="color:red"></div>
         <input id="send" name="submit" type="submit" value="Send E-mail">
         <div class="feedback-submit  text-right">
            <div class="feed-submit close-feedback1">
               <a data-popup-close="popup-1" href="#">Close</a>
            </div>
         </div>
         <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
      </form>
   </div>
</div>
<!-- e nd email all
 <div class="popup" id="arc_con" data-popup="popup-2">
   <div class="popup-inner archivecls">
      <div class="head">
         <h3>Confirmation</h3>
                     <a class="popup-close" data-popup-close="popup-2" href="#">x</a>

      </div>
      <label>Do You Want Archive this?</label>
      <div class="body">
<input type="hidden" id="archive_id"/>
<button type="button" class="btn btn-primary" onClick="archive_update()">yes</button>
<button type="button" class="btn btn-danger" data-popup-close="popup-2">Close</button>
      </div>

   </div>
</div> -->



<!-- feedback all -->

<!-- end fedd-->
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
   <h1>Warning!!</h1>
   <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
   <div class="iew-container">
      <ul class="iew-download">
         <li>
            <a href="http://www.google.com/chrome/">
               <img src="assets/images/browser/chrome.png" alt="Chrome">
               <div>Chrome</div>
            </a>
         </li>
         <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
               <img src="assets/images/browser/firefox.png" alt="Firefox">
               <div>Firefox</div>
            </a>
         </li>
         <li>
            <a href="http://www.opera.com">
               <img src="assets/images/browser/opera.png" alt="Opera">
               <div>Opera</div>
            </a>
         </li>
         <li>
            <a href="https://www.apple.com/safari/">
               <img src="assets/images/browser/safari.png" alt="Safari">
               <div>Safari</div>
            </a>
         </li>
         <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
               <img src="assets/images/browser/ie.png" alt="">
               <div>IE (9 & above)</div>
            </a>
         </li>
      </ul>
   </div>
   <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<?php
   $data = $this->db->query("SELECT * FROM update_client")->result_array();
   foreach($data as $row) { ?>
<!-- Modal -->
<div id="myModal<?php echo $row['user_id'];?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">CRM-Updated fields</h4>
         </div>
         <div class="modal-body">
            <?php if(isset($row['company_name'])&&($row['company_name']!='0')){?>
            <p>Company Name: <span><?php echo $row['company_name'];?></span></p>
            <?php } ?> 
            <?php if(isset($row['company_url'])&&($row['company_url']!='0')){?>
            <p>Company URL: <span><?php echo $row['company_url'];?></span></p>
            <?php } ?>
            <?php if(isset($row['officers_url'])&&($row['officers_url']!='0')){?>
            <p>Officers URL: <span><?php echo $row['officers_url'];?></span></p>
            <?php } ?>
            <?php if(isset($row['incorporation_date'])&&($row['incorporation_date']!='0')){?>
            <p>Incorporation Date: <span><?php echo $row['incorporation_date'];?></span></p>
            <?php } ?>
            <?php if(isset($row['register_address'])&&($row['register_address']!='0')){?>
            <p>Registered Address: <span><?php echo $row['register_address'];?></span></p>
            <?php }  ?>
            <?php if(isset($row['company_status'])&&($row['company_status']!='0')){?>
            <p>Company Status: <span><?php echo $row['company_status'];?></span></p>
            <?php }  ?>
            <?php if(isset($row['company_type'])&&($row['company_type']!='0')){?>
            <p>Company Type: <span><?php echo $row['company_type'];?></span></p>
            <?php } ?>
            <?php if(isset($row['accounts_periodend'])&&($row['accounts_periodend']!='0')){?>
            <p>Accounts Period End: <span><?php echo $row['accounts_periodend'];?></span></p>
            <?php } ?>
            <?php if(isset($row['hmrc_yearend'])&&($row['hmrc_yearend']!='0')){?>
            <p>HMRC Year End: <span><?php echo $row['hmrc_yearend'];?></span></p>
            <?php } ?>
            <?php if(isset($row['ch_accounts_next_due'])&&($row['ch_accounts_next_due']!='0')){?>
            <p>CH Accounts Next Due: <span><?php echo $row['ch_accounts_next_due'];?></span></p>
            <?php } ?>
            <?php if(isset($row['confirmation_statement_date'])&&($row['confirmation_statement_date']!='0')){?>
            <p>Confirmation Statement Date: <span><?php echo $row['confirmation_statement_date'];?></span></p>
            <?php } ?>
            <?php if(isset($row['confirmation_statement_due_date'])&&($row['confirmation_statement_due_date']!='0')){?>
            <p>Confirmation Statement Due: <span><?php echo $row['confirmation_statement_due_date'];?></span></p>
            <?php } ?>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- /.modal -->
<?php } ?>
<input type="hidden" name="user_id" id="user_id" value="">
<!-- filter add -->
<!-- <div clas="filter-bar" style="display:none;">
   <nav class="navbar navbar-light" >
      <ul class="nav navbar-nav">
         <li class="nav-item active">
            <a class="nav-link" href="#!">Filter: <span class="sr-only">(current)</span></a>
         </li>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#!" id="bydate1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icofont icofont-clock-time"></i> From</a>
            <input type="text" id="datepicker" name="from_date_picker" placeholder="By Date">
            <a class="nav-link dropdown-toggle" href="#!" id="bydate1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icofont icofont-clock-time"></i> To</a>
            <input type="text" id="datepicker12" name="to_date_picker" placeholder="By Date">
         </li>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" name="stati" href="#!" id="statuswise_filter1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icofont icofont-chart-histogram-alt"></i> By Status</a>
            <div class="dropdown-menu" aria-labelledby="statuswise_filter1">
               <a class="dropdown-item" href="#!">All</a>
               <div class="dropdown-divider"></div>
               <a class="dropdown-item" href="#!">Active</a>
               <a class="dropdown-item" href="#!">In-Active</a>
               <a class="dropdown-item" href="#!">Fozen</a>
            </div>
         </li>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#!" id="priority"1 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icofont icofont-sub-listing"></i> By Priority</a>
            <div class="dropdown-menu" aria-labelledby="priority1">
               <a class="dropdown-item" href="#!">All</a>
               <div class="dropdown-divider"></div>
               <a class="dropdown-item" href="#!">Private Limited company</a>
               <a class="dropdown-item" href="#!">Public Limited company</a>
               <a class="dropdown-item" href="#!">Limited Liability Partnership</a>
               <a class="dropdown-item" href="#!">Partnership</a>
               <a class="dropdown-item" href="#!">Self Assessment</a>
               <a class="dropdown-item" href="#!">Trust</a>
               <a class="dropdown-item" href="#!">Charity</a>
               <a class="dropdown-item" href="#!">Charity</a>
            </div>
         </li>
         <li class="nav-item">
            <div class="dropdown-primary dropdown open">
               <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button>
               <div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url();?>User/user_exportCSV?stati=0&de=0&pe=0" class="dropdown-item waves-light waves-effect but">Csv</a><a href="<?php echo base_url();?>User/pdf?stati=0&de=0&pe=0" class="dropdown-item waves-light waves-effect but">Pdf</a><a href="<?php echo base_url();?>User/html?stati=0&de=0&pe=0" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>
            </div>
         </li>
      </ul>
   </nav>
</div> -->

<div class="modal fade" id="archiveconfirmation" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <inpu type="hidden" name="archive_user_id" id="archive_user_id" value="">
          <p>Do you want to archive users?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="confirm_archive">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
</div>

  <div class="modal fade" id="reassign_popup" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Client Reassign</h4>
        </div>
        <div class="modal-body">
          
          <input type="text" id="reassign_input">
          
          <label class="reassign_input_error" style="display: none;color: red">Please Enter Any Text</label>

          <div class="checkbox-color3" style="display: block;">
            <label class="custom_checkbox1">
              <input type="checkbox" id="also_reassign_task">
              <i></i>
            </label>
            <span>Also Reassign Task</span>
          </div>

        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="save_reassign">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>



<div class="modal fade" id="deleteconfirmation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <inpu type="hidden" name="delete_user_id" id="delete_user_id" value="">
          <p> Are you sure want to delete ?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="confirm_delete">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>

    </div>
  </div>












<!--    <div class="modal-alertsuccess alert alert-success" id="user_delete_<?php echo $getallUservalue['id'];?>" style="display:none;">
                                                         <a href="javascript:;" class="close" data-dismiss="modal">x</a>
                                                         <div class="pop-realted1">
                                                            <div class="position-alert1 addclss">
                                                               Are you sure want to delete 
                                                               <div class="yesnocls">
                                                               <a href="<?php echo base_url().'user/delete/'.$getallUservalue['id'];?>" class="delete_yes"> Yes </a>
                                                               <a href="#" id="close">No</a>
                                                             </div>
                                                            </div>
                                                         </div>
                                                      </div>  -->

  <div class="modal fade" id="popup_send_mail_sms" role="dialog" style="display: none;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Send Mail</h4>
        </div>
        <div class="modal-body">
        <div id="mail_text"></div>
        <div id="mail_text_error"></div>
        <hr>       
      <form action="<?php echo base_url();?>User/send_mail_sms" id="send_mail_form" method="post" name="send_mail_sms_form">
       <select id="temp_id" name="temp_id">
        <option value="">Select Email Template</option> 
        <?php foreach($email_temp as $v){echo "<option value=".$v['action_id'].">".$v['action']."</option>";}?>
        </select>
        <br>
         <style>
            input.error {
            border: 1px dotted red;
            }
            label.error{
            width: 100%;
            color: red;
            font-style: italic;
            margin-left: 120px;
            margin-bottom: 5px;
            }
         </style>
    
         <label><b>Your content</b></label>
         <input type="hidden" name="mail_ids" id="recivers_id">
         <input type="hidden" name="status" value="send">
         <textarea name="sub" id="notification_sub" placeholder="Type your text here..." rows="2" cols="60"></textarea>
         <textarea name="msg" id="notification_msg" placeholder="Type your text here..."></textarea>
         <br><br>
         <div class="feedback-submit">
            <div class="feed-submit text-left">
               <input name="submit" type="submit" value="submit">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         
         </div>
  
      </form>
    </div>
      
      </div>
      
    </div>
  </div>


  <div class="modal fade" id="missingInfo_composePopup" role="dialog" style="display: none;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Send Mail</h4>
        </div>
        <div class="modal-body">
            <form id="missingInfo_composeForm">
            <div class="choose_email_temp_div">
              <label>Choose Email Template</label>
              <select name="choose_email_temp_sel" id="choose_email_temp_sel" class="choose_email_temp_sel form-control">
                <option value="">--Select Email Template--</option>
                <?php                  
                  foreach($email_temp as $Ekey => $Eval) {
                    ?>
                    <option value="<?php echo $Eval['action_id']; ?>"><?php echo $Eval['action']; ?></option>
                    <?php
                  }
                ?>
              </select>
            </div>
            <div class="send-to-div"> 
              <label>To</label> <input type="text" class="sendto-mailId" name="mail"  id="missingInfo_Tomail">
              <input type="hidden" class="sendto-client_id" name="client_id"  id="missingInfo_ToClient_id">
            </div>
            <div class="subject-div">
              <label>subject</label>
              <textarea name="sub" id="missingInfo_sub" placeholder="Type your text here..." rows="2" cols="60"></textarea>
            </div>
            <div class="mail-content-div">
              <textarea name="msg" id="missingInfo_msg" placeholder="Type your text here..."></textarea>
            </div>
           <div class="feedback-submit">
              <div class="feed-submit text-left">
                 <input type="submit" class="btn btn-default" value="Send">               
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>    
          </div>
        </form>
    </div>
      
      </div>
      
    </div>
  </div>






  <div class="modal fade" id="popup_service_block" role="dialog" style="display: none;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Service Block</h4>
        </div>
        <div class="modal-body">
       
        <input type="hidden" name="blocked_user_id" id="blocked_user_id" value="">
        <p>Do You Want to Block the user services?     
    
    </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="block_section">Yes</button>
          <button type="button" class="btn btn-default close_popup" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>


    <div class="modal fade" id="popup_reminder_block" role="dialog" style="display: none;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reminder Block</h4>
        </div>
        <div class="modal-body">      
      
        <input type="hidden" name="blocked_reminder_id" id="blocked_reminder_id" value="">
        <p>Do You Want to Block the user Reminders?     
    
    </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="reminder_section">Yes</button>
          <button type="button" class="btn btn-default close_popup" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>

    <div id="colvis_Content_popup" class="colvis_icon" style="display: none;"> 
      <?php echo ClientColumn_ColVis_Content();?>
    </div>

  
<!-- filter end-->
<?php // $this->load->view('users/column_setting_view');?>
<?php $this->load->view('includes/footer');?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <!--  <script>var $j = jQuery.noConflict();</script>
<script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url()?>assets/js/user_page/materialize.js"></script> 

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>

<script src="<?php echo base_url()?>assets/js/custom/buttons.colVis.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom/datatable_extension.js"></script>

<!-- For Export Buttom -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<!-- For Export Buttom -->

<!-- For Date fields sorting  -->
<script src="<?php echo base_url()?>assets/js/custom/datatable_cus_sorting.js"></script>
<!-- For Date fields sorting  -->

<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/js/jquery.tagsinput.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/comboTreePlugin.js"></script>
<!-- <script type="text/javascript">
   CKEDITOR.replace('editor4');src="https://cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"
</script> -->

<script>


$(document).ready(function (){ 


var ClientTable_Instance;
var Assigned_client   =   "<?php echo implode(',' , array_column( $getallUser , 'id' ) ); ?>";
var Filter_Condition  =   { 'user__status':1 };
var Order             =   { 'client__crm_ch_accounts_next_due':'ASC','client__crm_confirmation_statement_due_date':'ASC' };
var Column_Filter     =   { 'client__crm_legal_form':['Private Limited company'] };
var Created_From  = ''
var Created_To    = '';
var last_filter_data_request = "";
//$('th.crm_legal_form_TH select').val('Private Limited company').trigger('change');


/*tiny editor*/
 tinymce_config['images_upload_url'] = "<?php echo base_url();?>Firm/image_upload_from_editor";
 tinymce_config['selector']          = '#notification_msg,#missingInfo_msg';
 tinymce.init( tinymce_config );


 var tree_select = <?php echo json_encode( GetAssignees_SelectBox() ); ?>;
     $('#reassign_input').comboTree({ 
        source : tree_select,
        isMultiple: true
     });
/*tiny editor*/


var debounce_draw_table = debounce(function(){ ClientTable_Instance.draw(); } , 500 );




function ColReorder_BackendUpdate( e, settings, details )
  {  
      Change_Sorting_Event ( ClientTable_Instance );
      var Reorder = [];
      var ReorderCnst = [];
      ClientTable_Instance.columns().every( function()
      {
        var OrderNo = $(this.header()).attr('data-orderNo');
        var Cnst = $(this.header()).attr('class').match(/\w+(?=_TH)/);
       
        if( OrderNo )  
          {
            Reorder.push( OrderNo );
            ReorderCnst.push( Cnst[0] )
          }  
      }
    
    );

      $.ajax(
      {
        url:"<?php echo base_url()?>user/ClientTable_ReorderUpdate",
        type : "post",
        data:{'reorder': Reorder,'column_cnst':ReorderCnst},
        beforeSend:function(){$('.LoadingImage').show();},
        success:function(res){ 
          ClientTable_Instance.draw();
          $('.LoadingImage').hide(); 
        }
      }
      );

  }
$('#colvis_Content_popup  div.colvis_column_list li').click(function(){
  var cons = $(this).attr('data-const');
  var column = ClientTable_Instance.column( '.'+cons+'_TH' );
  if( column )
  {
    column.visible( false );
  }
  else
  {
    column.visible( true );
  }
});

function ColVis_BackendUpdate(e)
{
  //$(e.target).closest('');
  $(this).toggleClass('active');
 
  var ShowHidden =[];

   $('#colvis_Content_popup  div.colvis_column_list li.active').each(function(){
      ShowHidden.push( $(this).attr('data-const') );
    });

  //if(!ShowHidden.length)return; 

  $.ajax(
      {
        url:"<?php echo base_url()?>user/ClientTable_ColVisUpdate",
        type : "post",
        data:{'Showhidden': ShowHidden},
        beforeSend:function(){$('.LoadingImage').show();},
        success:function(res)
        {
          $(".insert_client_table").html(res);
          initialize_ClientTable();
          $(document).find('th.crm_legal_form_TH img.themicond').trigger('mousedown');
          
          $("#colvis_Content_popup").modal('hide');
          $('.LoadingImage').hide(); 
        }
      }
      );

}

function Trigger_To_Reset_Filter()
{
    $('div.toolbar-table .tableToolBar_selectDiv li.dropdown-chose').each(function(){
      $(this).trigger('click');
    });

    $('div.toolbar-table input').val(' ');


    
    Created_From      ='';
    Created_To        = '';

    

    $('#select_all_client').prop( 'checked',false );

    $('#select_all_client').trigger( 'change' );

    Redraw_Table( ClientTable_Instance , 0 );

    debounce_draw_table();
}

function Add_TableToolBar()
{

      $(".first_box").find(".searching_box").attr('type','hidden');
      
      $("div.toolbar-table").prepend('<div class="filter-task1 sets"><h2>Filter:</h2><ul><li><span class="for-part"><label class="f2t">From</label><input type="text" id="clientCreate_FromDate" class="date_picker_dob" name="from_date_picker" placeholder="By Date" readonly></span><span class="for-part"><label class="f2t">To</label><input type="text" id="clientCreate_ToDate" name="to_date_picker" class="date_picker_dob" placeholder="By Date" readonly></span><a href="javascript:;" class="ToolBar_DateWiseFilter allusers-box"><i class="fa fa-search"></i></a></li><li><div class="tableToolBar_selectDiv"><select placeholder="By Priority" multiple="true" id="ToolBar_Priority" data-searchCol="crm_legal_form__primary_search_TH"><option value="Private Limited company">Private Limited company</option><option value="Public Limited company">Public Limited company</option><option value="Limited Liability Partnership" >Limited Liability Partnership</option><option value="Partnership">Partnership</option><option value="Self Assessment" >Self Assessment</option><option value="Trust">Trust</option><option value="Charity" >Charity</option><option value="Other" >Other</option></select></div></li></ul></div>');

       $("div#table-buttons").prepend('<button class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i></button>');

       $("#clientCreate_FromDate, #clientCreate_ToDate").datepicker({autoclose: true,dateFormat: 'dd-mm-yy'});
       
       $(".tableToolBar_selectDiv").dropdown();
}  

$(document).on('click',"div.sortMask",function(){
    
  var table  = $(this).closest('th').attr('data-table');
  var column = $(this).closest('th').attr('data-column');

  var sorting = "ASC";
  
  if( $(this).closest('th').hasClass('sorting_asc') )
  {
    sorting = "DESC";
  }
    var f = table+'__'+column;
    Order = {};
    Order[f] = sorting;

});



$(document).on('mousedown','th .themicond',function(){  

  var TH      = $(this).closest('th');
  var table   = TH.attr('data-table');
  var column  = TH.attr('data-column');
  
  if( !TH.find('select').length && last_filter_data_request != column)
  {
    last_filter_data_request = column;

    $.ajax({
      url :"<?php echo base_url()?>client/get_column_filter_data/"+table+"/"+column,
      type:'post',
      dataType:'json',
      data : {"client_user_id":Assigned_client},
      success : function( data ){
        console.log( data );

        var Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo( TH );

        $.each( data ,function( i , v ){ 

          var d_Sel = "";
          if( TH.hasClass('crm_legal_form_TH') && v[0] == "Private Limited company" )
            d_Sel = "selected='selected'";

          Filter_Select.append( '<option  value="'+v[0]+'" '+d_Sel+'>'+v[1]+'</option>' );
        });

        make_column_filter( Filter_Select );        
        TH.find('img.themicond').trigger('click');          

        if( TH.hasClass('crm_legal_form_TH') )        
          TH.find('ul').removeClass("Show_content");       
      }
    });
  }
});

$('th.crm_legal_form_TH').find('img.themicond').trigger('mousedown');

function make_column_filter( Filter_Select )
{
  Filter_Select.on('change',function(){

    //add or remove thi select box value from filter section
    config_filter_section( $(this) );

    var key = $(this).closest('th').data('table')+"__"+$(this).closest('th').data('column');

    var val = $(this).val();
    
    if( val.length )
      Column_Filter[ key ] = val;
    else
      delete Column_Filter[ key ];

    debounce_draw_table();
  });

  Filter_Select.formSelect();
}



function initialize_ClientTable()
{

var pageLength    = "<?php echo get_firm_page_length() ?>";
ClientTable_Instance = $('#client_table').DataTable({
        "pageLength": parseInt( pageLength ),          
        "dom": '<"toolbar-table"<"#table-buttons.dropdown.table-buttons"<"#table-buttonsList.dropdown-menu.table-buttonsList"B>> >lfrtip',
        buttons: [
          {
              text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
              action: function ( e, dt, node, config )
              {
                  Trigger_To_Reset_Filter();
              }
          },
          {
              text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
              action: function ( e, dt, node, config )
              {
                  $('#colvis_Content_popup').toggle();
              }
          },
          DataTable_Export_Buttons
        ],         
        order: [],//ITS FOR DISABLE SORTING
        columnDefs: 
        [
          {"orderable": false,"targets": ['select_row_TH','action_TH']},
        ],
        colReorder: 
        {
          realtime: false,
          fixedColumnsLeft : 1,
          fixedColumnsRight:1 
        },
      //
      //"search": {"regex": true,"smart": false},
      // responsive: true,
        "iDisplayLength": 10,
      /*  scrollX:        true,*/
     //  scrollCollapse: true,
     // // paging:         false,
     //  fixedColumns:   {
     //      leftColumns: 1,
     //      rightColumns: 1
     //  },
    "processing": true,
    "serverSide": true,
    "searchDelay": 700,
    'ajax' : {
      'url'   : '<?php echo base_url() ?>client/get_client_list_source',
      'type'  : 'POST', 
      beforeSend : Show_LoadingImg,     
        data  : function(data){

          data.filter           = Filter_Condition;
          data.order            = Order;
          data.column_filter    = Column_Filter;
          data.assigned_client  = Assigned_client;
          data.created_from     = Created_From;
          data.created_to       = Created_To;
          data.columns          = [];

        },
      complete: function(){
        $('th').find('ul').removeClass('Show_content');

        Hide_LoadingImg();
      }
    },
    initComplete: function(){ console.log('INI initComplete');}
  });
    
  Add_TableToolBar();
  
  Change_Sorting_Event( ClientTable_Instance ); 
  
  Filter_IconWrap();

  ClientTable_Instance.on( 'column-reorder.dt' , ColReorder_BackendUpdate );

  $('.filter-data.for-reportdash #clear_container').on('click' , function(){ Redraw_Table( ClientTable_Instance ); });  
}
function toggle_action_button(i,table='')
{
  if(i)
  {
            $("#deleteUser_rec").show();
            $("#send_mail_sms").show();
            $("#block_service").show();
            $("#block_reminder").show(); 
            $("#reassign_button").show(); 
            $("#ch_sync_button").show(); 
            if(table=="Archive")$("#unarchive_button").show();
            else $("#archive_button").show();
  }
  else
  {
            $("#deleteUser_rec").hide();
            $("#send_mail_sms").hide();
            $("#reassign_button").hide(); 
            $("#ch_sync_button").hide(); 
            $("#block_service").hide();
            $("#block_reminder").hide();
            if(table=="Archive")$("#unarchive_button").hide();
            else $("#archive_button").hide();
  }
}


function get_selected_rows()
{
    var ids =[];
    ClientTable_Instance.column('.select_row_TH',{search:'applied'}).nodes().to$().each(function(index) {
      if($(this).find(".select_client").is(":checked"))
      {
        ids.push($(this).find(".select_client").val());
      }
    });
    return ids;
}


function delete_action()
{

  $("#deleteconfirmation").modal('hide');
 
  var id=$("#delete_user_id").val();
    $.ajax({
      type: "post",
      url: "<?= base_url()?>client/delete_client",
      data: {id:id},
      dataType: "json",
      beforeSend: function() {
       $(".LoadingImage").show();
     },
      success:
        function(data){
            $(".LoadingImage").hide(); 
            if( data )
            {
              $('.info_popup').show();
              $('.info_popup .position-alert1').html('Client Deleted Successfully..');
              setTimeout(function(){ location.reload(); },1000);

            }
            

        }
    });

   // alert('fewds');
}


function SendClient_missingDetails(e)
{

  var mail_id = $("#missingInfo_Tomail").val();
  var subject = $("#missingInfo_sub").val();

  var body    = tinymce.get("missingInfo_msg").getContent();
         

  var client_id =  $("#missingInfo_ToClient_id").val();

  $.ajax({
      url: '<?php echo base_url(); ?>client/SendClient_missingDetails',
      type : 'POST',
      data : {'mail_id':mail_id,'subject':subject,'body':body,'client_id':client_id},
      dataType : 'json',
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) 
      {
        if( data.result == 1 )
        {
           $(".LoadingImage").hide(); 
           $('.info_popup').show();
           $('.info_popup .position-alert1').html('Mail Send Successfully..');
           $(this).parents('.missing_details').toggleClass('show'); 
           $("#missingInfo_composePopup").modal('hide');
           setTimeout(function(){$('.info_popup').hide();},1000);

        }          
      }
  });
}
function FilterFrom_OtherPage()
{
  <?php if(isset($_SESSION['firm_seen'])){ ?>

      var value='<?php echo $_SESSION['firm_seen'];  ?>';
      value = value.trim();

      // console.log(value);
      if(value=='private_limited')
      { 
         $("#alluser_filter input").val('private');
         $("#alluser_filter input").trigger('keyup');
        // $("#alluser_filter input").trigger('change');
      }
      if(value=='public_limited')
      {
           $("#alluser_filter input").val('public');
            //console.log($("#alluser_filter input").val());
           $("#alluser_filter input").trigger('keyup');
      }
      if(value=='limited_liablity')
      { 
          $("#alluser_filter input").val('limited liability');
          $("#alluser_filter input").trigger('keyup');
      }

      if(value=='partnership')
      { 
          $("#alluser_filter input").val('partnership');
         $("#alluser_filter input").trigger('keyup');
      }

      if(value=='self')
      {
          $("#alluser_filter input").val('self');
         $("#alluser_filter input").trigger('keyup');
      }

      if(value=='trust')
      { 
          $("#alluser_filter input").val('trust');
         $("#alluser_filter input").trigger('keyup');
       }
      if(value=='charity')
      { 
          $("#alluser_filter input").val('charity');
         $("#alluser_filter input").trigger('keyup');
      }
      if(value=='other')
      { 
          $("#alluser_filter input").val('other');
         $("#alluser_filter input").trigger('keyup');
      }   
      var Table_status_filter = '';

     if(value=='active')
     { 
          Table_status_filter ='Active'
     }
     else if(value=='alluser')
     { 
          Table_status_filter ='1'
     }
    else if(value=='inactive')
    { 
          Table_status_filter ='|0|2'     
     }
     else if(value=='frozen')
     { 
          Table_status_filter ='3'
     }
    
     if(Table_status_filter!='')
     {
      console.log( Table_status_filter +"session");

     $('.tap_click').each(function(){  
        if( $(this).attr('data-id').trim() == Table_status_filter)
        {
          console.log( "indide the siv" ) ; 
          $(this).trigger('click');          
        }
     });

     }
     <?php  } ?>
}

  initialize_ClientTable();

  //$('#confirm_archive').click(archive_action);
  $('#confirm_delete').click(delete_action); 
  


    $('.insert_client_table').on('click','.ToolBar_DateWiseFilter',function(){

      Created_From = $("#clientCreate_FromDate").val();
      Created_To   = $("#clientCreate_ToDate").val();

      debounce_draw_table();
  });

  $('.insert_client_table').on('change',' #ToolBar_Priority',function(){
    
    if( $(this).val().length )
      Column_Filter['client__crm_legal_form'] = $(this).val();
    else
      Column_Filter['client__crm_legal_form'] = {};  
    debounce_draw_table();

  });

  $('#colvis_Content_popup  div.colvis_column_list li').click(ColVis_BackendUpdate);
  
  $('#colvis_Content_popup  div.colvis_column_list .close').click(function(){
    $('#colvis_Content_popup').hide();
  });

  $('.insert_client_table').on('click','.DeleteClient',function()
  {
    $("#delete_user_id").val( JSON.stringify( [$(this).data('id')] ) );
  });

 

  $(document).on('click','.ArchiveClient, #archive_button',function()
  {
    if( $(this).hasClass('ArchiveClient') )
    {
      var id = JSON.stringify([$(this).data('id')]);      
    }
    else
    {
      var userRec = get_selected_rows();
      var id      = JSON.stringify(userRec);
    }
    
    var action = function()
    {
      $('#Confirmation_popup').modal('hide');
      $.ajax({
        type: "post",
        url: "<?= base_url()?>client/bulk_archive_unarchive",
        data: {id:id,"status":"archive"},
        dataType: "json",
        beforeSend: function() {
         $(".LoadingImage").show();
       },
        success:
          function(data){
            $(".LoadingImage").hide();
            $('.info_popup').show();
            $('.info_popup .position-alert1').html('Client status have been changed successfully... ');
            setTimeout(function(){
              $('.info_popup').hide();
              location.reload();
            }, 1500);
          }
      });
    };
    Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to archive Client...!'});  
    $('#Confirmation_popup').modal('show');

  });

  $(".tap_click").click(function(){

      $('.tap_click').removeClass('active');
      $(this).addClass('active');
      var status = $(this).attr('data-id').trim();

      Filter_Condition = {};
      Order = {};
      Column_Filter = {};

      Filter_Condition['user__status'] = status;


      if( status == 'All' )
      {
        delete Filter_Condition['user__status'];  
      }
      
      
      Trigger_To_Reset_Filter();
    });
  
  FilterFrom_OtherPage();

$(document).on("change", "#select_all_client", function(event) {


  var IsArchive=$('.tap_click.active').attr("data-id").trim();

  
    var checked = this.checked;
     $(".LoadingImage").show();
     var Selected_Num = 0;
    ClientTable_Instance.column('.select_row_TH').nodes().to$().each(function(index) {
        
          if (checked)
         {
           Selected_Num++;
         }
           
        $(this).find('.select_client').prop('checked',checked);

        toggle_action_button( checked , IsArchive );
        
    });
    $(".LoadingImage").hide();

    $("#select_client_count").val(Selected_Num + " Selected");

});

  
    $(document).on("change",".select_client",function(event)
    {  
      var IsArchive=$('.tap_click.active').attr("data-id").trim();
      
      var tr=0;
      var ck_tr=0;
      ClientTable_Instance.column('.select_row_TH').nodes().to$().each(function(index) {
      if( $(this).find(".select_client").is(":checked") )
        {
          ck_tr++;
        }
        tr++;
      });

      if(tr==ck_tr)
      {
        $("#select_all_client").prop("checked",true);
      } 
      else
      {
        $("#select_all_client").prop("checked",false);
      }

      if(ck_tr)
      {
        toggle_action_button(1,IsArchive);
      }
      else  
      {
        toggle_action_button(0,IsArchive);
      } 
   
      $("#select_client_count").val(ck_tr+ " Selected");
       
    });



  $(document).on('click', '#unarchive_button', function() {
  var userRec = get_selected_rows();
  
  if(userRec.length <=0)
   {
     $('.alert-danger-check').show();
   }
   else
   {
      var user_id=JSON.stringify(userRec);
      $.ajax({
              url:  "<?php echo base_url()?>client/bulk_archive_unarchive",
              type: "POST",
              data: {"id":user_id,"status":"unarchive"},
              dataType: "json",
              beforeSend: function() {$(".LoadingImage").show();},
              success:function(rel){location.reload();}
            });
   }
});

$(document).on('click','#ch_sync_button',function(e){ 
  var userRec = get_selected_rows();
  $.ajax({
              url:  "<?php echo base_url()?>client/client_CH_sync",
              type: "POST",
              data: {"ids":userRec},
              beforeSend: function() {$(".LoadingImage").show();},
              success:function(data){
                if( data )
                {
                  $('.info_popup').show();
                  $('.info_popup .position-alert1').html('Company House Data Update successfully...');
                  setTimeout(function(){ location.reload(); }, 1500);
                } 
              }
            });  

});


  $(document).on('click','#save_reassign',function(){

    var userRec = get_selected_rows();
    console.log( userRec );

    var Selected = [];
    $('#reassign_popup .comboTreeItemTitle').find('input[type="checkbox"]:checked').each(function(){

          var id1 = $(this).closest('.comboTreeItemTitle').attr('data-id');
          var id1 = id1.split('_');

          Selected.push( id1[0] );
    });
    
    console.log( Selected );
    console.log( Selected.length +"length");

    if( Selected.length == 0 )
    {    
      $(".reassign_input_error").show();
      return;
    }
    else
    {
      $(".reassign_input_error").hide();    
    }
    var task_also = ( $("#also_reassign_task").is(":checked") ? 1 : 0 );
    $.ajax({
      url         : "<?php echo base_url()?>client/Client_Reassign",
      data        : { 'user_ids' : userRec , 'assignees' : Selected , 'task_also':task_also },
      type        : "POST",
      dataType    : "JSON",
      beforeSend  : function() { $('.LoadingImage').show(); },
      success     : function(res) {
        if(res.result)
        {
          $('.LoadingImage').hide();
          $('#reassign_popup').modal('hide');

          $('.info_popup').show();
          $('.info_popup .position-alert1').html('Client Reassigned successfully... ');
          setTimeout(function(){ location.reload(); }, 1500);

        }
      }
    });




  });

$(document).on('click', '#deleteUser_rec', function() {
  var userRec = [];
  
  var userRec=get_selected_rows();

  if(userRec.length <=0) {
     $('.alert-danger-check').show();
  } else {
    //$('#delete_all_user'+userRec).show();
    var user_id=JSON.stringify(userRec);
    $("#delete_user_id").val(user_id);
  }
});

   $(document).on("change",".client_status",function(e){
    var rec_id = $(this).data('id');
    var stat = $(this).val();
    var that = $(this);
   
   $.ajax({
         url: '<?php echo base_url();?>client/statusChange',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         timeout: 3000,
          dataType: 'json',
         beforeSend:function(){
          $(".LoadingImage").show();
         },
         success: function( data ){
          

          //alert('ggg');
                $('.info_popup').show();
                $('.info_popup .position-alert1').html('Client status have been changed successfully... ');

                var row  = that.closest('tr');
                var cell = that.closest('td');
                var primary_cell = row.find('td.Status_primary_search_TD');
                var archiveIcon = row.find('.ArchiveClient');
                if(stat==5)
                {
                  archiveIcon.addClass('disabled');
                }
                else
                {
                  archiveIcon.removeClass('disabled');
                }
                cell.attr( 'data-search',that.find('option:selected').text().trim() );
                primary_cell.attr( 'data-search',that.find('option:selected').text().trim() );

                ClientTable_Instance.cell( cell ).invalidate().draw();
                ClientTable_Instance.cell( primary_cell ).invalidate().draw();
                

                console.log(data);
                $('.tap_click').each(function(){
                  
                  var status = $(this).attr('data-id').trim();

                    $(this).find('span').html(data[status]);

                });

              $(".LoadingImage").hide();
        
   
               setTimeout(function(){$('.info_popup').hide();}, 1500);
           
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
    });



$("#send_mail_sms").click(function(){

    var chd_id=get_selected_rows().join(",");
      $.ajax({
      url:"<?php echo base_url()?>user/send_mail_sms",
      type:"POST",
      data:{"ids":chd_id,"status":"get"},
         beforeSend: function() {
                        $(".LoadingImage").show();
                      },
      success:function(rld) 
      {
       // alert(rld)
        if(rld==1)
        {
              
              $(".LoadingImage").hide(); 
             $('.info_popup').show();
             $("#popup_send_mail_sms").hide();
             $('.info_popup .position-alert1').html('Mail Send Successfully..');
             setTimeout(function(){$('.info_popup').hide();},1000);
        }
        else
        {   
          var arr=JSON.parse(rld);
          var id="";
          var mail_ids="";
           for (i in arr)
           { 
            mail_ids+="<div class='cdsc' style='display: inline-block;'><span class='mail_id'>"+arr[i]+"<button type='button' class='closes_mail_id' data-id="+i+" style='background: transparentborder: 0;'>×</button></span></div>";
            id+=(id=="")?i:","+i;
           }
           /*no mailid are avilable*/
           if(id=="") mail_ids = "<label class='error'>Contact Mail Not Exsist</label>";

          $("#recivers_id").val(id);
          $("#mail_text").html(mail_ids);
          $(".LoadingImage").hide();
        }
      }
    });
    $("#submit_to_send").prop("disabled",false);
});





$(document).on('click', ".missing_info, .misclose" , function(){
 // console.log('ok');
 $(this).parents('tr').find('.missing_details').toggleClass('show');
});

$(document).mouseup(function(e) {
    var container = $(".missing_details.show");
    // if the target of the click isn't the container nor a descendant of the container
    if (container.length && !container.is(e.target) && container.has(e.target).length === 0) 
    {
        $('.missing_details').removeClass('show');
    }

    });

$(document).on('click', ".Open_MissingMailCompos",function(){

  var mail_id=$(this).parents('.missing_details').find(".missing_details_email").val();

  var mail_content=$(this).parents('.missing_details').find(".missing_details_content").val();
  var client_id =  $(this).parents('.missing_details').find(".missing_details_client_id").val();
  $("#missingInfo_composePopup").find('#missingInfo_Tomail').val( mail_id ) ;
  $("#missingInfo_composePopup").find('#missingInfo_ToClient_id').val( client_id ) ;
  $("#missingInfo_composePopup").find('#missingInfo_sub').val( "Missing Details" ) ;
  tinymce.get("missingInfo_msg").setContent( mail_content );


  $("#missingInfo_composePopup").modal('show');

 
// alert(missingInfo_sub);
// alert(missingInfo_msg);

$(document).on('change','#choose_email_temp_sel',function() {
  var temp_id = $(this).val();
  if(temp_id != '') {
    var data = {};
    data['id'] = temp_id;
    $.ajax({
      url : "<?php echo base_url('user/get_email_temp'); ?>",
      type : "POST",
      data : data,
      beforeSend : function() {
        $(".LoadingImage").show();
      },
      success : function(data) {
        // alert(data);
        var da = JSON.parse(data);
        console.log(da);
        var sub = da['subject'];
        var msg = mail_content+da['body'];
        $("#missingInfo_sub").val(sub);
        //$("#notification_msg").html(); 
        //CKEDITOR.instances.missingInfo_msg.setData(msg);
         tinymce.get("missingInfo_msg").setContent(msg);

        $(".LoadingImage").hide();
      }
    });
  } else {
    return false;
  }
});
  
});


$('#missingInfo_composeForm').validate(
  {
    ignore:false,
    
    rules:{     
      mail:{required:true},
      sub:{required:true},
    },
    messages:
    {
      mail:{required: "Mail Id Required"},
      sub:{required: "Subject Are Required"},      
    },
    submitHandler:SendClient_missingDetails
  }  
);





$("#block_service").click(function(){
  var child=get_selected_rows().join(',');

    $("#blocked_user_id").val(child);   
});



$("#block_reminder").click(function(){

  var child=get_selected_rows().join(',');
 
  $("#blocked_reminder_id").val(child);   
});

$("#block_section").click(function(){
    var block_users=$("#blocked_user_id").val();
       $.ajax({
                    url: '<?php echo base_url(); ?>Sample/block_users',
                    type : 'POST',
                    data : {'block_user_id':block_users},                    
                      beforeSend: function() {
                        $(".LoadingImage").show();
                      },
                      success: function(data) {
                        if(data=='1'){
                           $(".LoadingImage").hide();                         
                           $(".close_popup").trigger('click'); 
                        }                        
                      }
                });
});



$("#reminder_section").click(function(){
    var block_users=$("#blocked_reminder_id").val();
   $.ajax({
      url: '<?php echo base_url(); ?>Sample/block_reminders',
      type : 'POST',
      data : {'block_user_id':block_users},                    
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          if(data=='1'){
             $(".LoadingImage").hide();         
             $(".close_popup").trigger('click');       

          }          
        }
  });

});


/*var actives_table;
var inactives_table;
var frozens_table;
var archives_table;*/



  

 // $('th').on("click.DT", function (e) {
 //        //stop Propagation if clciked outsidemask
 //        //becasue we want to sort locally here
 //        if (!$(e.target).hasClass('sortMask')) {
 //            e.stopImmediatePropagation();
 //        }
 //    });




$("#temp_id").change(function(){
  var id=$(this).val();
  if(id=="0")return;
  $.ajax({
  url:"<?php echo base_url()?>user/get_email_temp",
  type:"POST",
  data:{'id':id},
  success:function(res){
        var da=JSON.parse(res);
        console.log(da);
        $("#notification_sub").html(da['subject']);
        //$("#notification_msg").html(); 
       //CKEDITOR.instances.notification_msg.setData(da['body']);
       tinymce.get("notification_msg").setContent(da['body']);

        }
      });
});





   
     
  



});

/*if(parent=="alluser")
{
var status_filter='<select id="'+parent+'statuswise_filter" name="stati"><option value="" hidden>By Status</option><option value="">All</option><option value="1">Active</option><option value="2">In-Active</option><option value="3">Fozen</option></select>';
}
else 
{
  var status_filter='';
}*/




   
   
   
       

   







 
   // $("#newactives").dataTable({
   //      "iDisplayLength": 10,
   //      // "scrollX": true,
   //   "dom": '<"top"fl>rt<"bottom"ip><"clear">'
   //   });
   // $("#newinactives").dataTable({
   //      "iDisplayLength": 10,
   //       //"scrollX": true,
   //   "dom": '<"top"fl>rt<"bottom"ip><"clear">'
   //   });
   // $("#frozens").dataTable({
   //      "iDisplayLength": 10,
   //       //"scrollX": true,
   //   "dom": '<"top"fl>rt<"bottom"ip><"clear">'
   //   });
   
     $('.search-input-text').tagsInput({   // initialization of tags input 
           'height':'100%',
           'width':'100%',
           'interactive':true,
           'defaultText':'Add a tag',
           'hide':true,
           'delimiter':',',
           'unique':true,
           'onAddTag':tagDraw,
           'onRemoveTag':tagDraw,
           'removeWithBackspace' : true,
           'minChars' : 0,
           'maxChars' : 0, //if not provided there is no limit,
           'placeholderColor' : '#AAA'
         });
         function tagDraw(){              //draw a request on add or remove tag
           var v= $(".search-input-text").val();
           dataTable.column(4).search(v).draw();
         }
   
   
   
  /* $("#bulkDelete").on('click',function() { // bulk checked
    alert('ok');
        var status = this.checked;
   
        $(".deleteRow").each( function() {
          $(this).prop("checked",status);
   
        });
         if(status==true)
         {
           $('#deleteTriger').show();
         } else {
           $('#deleteTriger').fadeOut('slow');
         }
      });
   
   $(".deleteRow").on('click',function() { // bulk checked
   
         var status = this.checked;
   
         if(status==true)
         {
           $('#deleteTriger').show();
         } else {
         //var chk_val = $(".deleteRow:checked").length;
   // if(chk_val==0)
         $('#deleteTriger').hide();
         }
       });
      
      $('#deleteTriger').on("click", function(event){ // triggering delete one by one
        
        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
          var ids = [];
          $('.deleteRow').each(function(){
            if($(this).is(':checked')) { 
              ids.push($(this).val());
            }
          });
          var ids_string = ids.toString();  // array to string conversion 
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>user/multiple_delete/",
            data: {data_ids:ids_string},
            success: function(response) {
              //dataTable.draw(); // redrawing datatable
              var emp_ids = response.split(",");
    for (var i=0; i < emp_ids.length; i++ ) {
   
      $("#"+emp_ids[i]).remove(); 
    }
    
            },
            //async:false,
          });
        }
      });*/
   

   
   
   // payment/non payment status
   $('.suspent').click(function() {
     if($(this).is(':checked'))
         var stat = '1';
     else
         var stat = '0';
     var rec_id = $(this).val();
     var $this = $(this);
      $.ajax({
         url: '<?php echo base_url();?>user/suspentChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         success: function( data ){
          //alert('ggg');
             $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Client status have been changed successfully...</span></div></div></div>');
             if(stat=='1'){
              
               $this.closest('td').next('td').html('Payment');
   
             } else {
               $this.closest('td').next('td').html('Non payment');
             }
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
   });
   //});*/


   
</script>
<script>
   /* var notes = {};
   
   <?php
      $data = $this->db->query("SELECT * FROM update_client")->result_array();
      foreach($data as $row) {
      
        echo 'notes["'.$row['user_id'].'"] = "<p>company status:'.$row['company_status'].'</p><br><p>account period end:'.$row['accounts_periodend'].'</p>";';
      
       //echo 'notes["'.$row['user_id'].'"] = "'.(isset($row['company_status'])&&($row['company_status']!=0)) ? .'<p>company status:'.$row['company_status'].'</p><br><p>account period end:'.$row['accounts_periodend'].'</p>";';
      
        //echo 'notes["'.$row['user_id'].'"]'= (isset($row['company_status'])&&($row['company_status']!=0)) ? $row['company_status']:'';
      }
      ?>
   
   $(document).on("click", ".noteslink", function() {
     var id = $(this).data("rowid");
     $("#myModalnote .modal-body").html(notes[id]);
     $("#myModalnote").modal("show");
   });
   */
   
   
   
   
</script>
<!-- <script type="text/javascript">
   $(document).ready(function(){
     $('#column_setting').on('submit', function (e) {
   
             e.preventDefault();
    $(".LoadingImage").show();
             $.ajax({
               type: 'post',
               url: '<?php echo base_url()."User/column_update";?>',
               data: $('form').serialize(),
               success: function (data) {
                   if(data == '1'){
                   // $('#new_user')[0].reset();
                   $('.alert-success').show();
                   $('.alert-danger').hide();
                   location.reload();
                  // $('.all-usera1').load('<?php echo base_url()?>user .all-usera1');
                   
                 
                   }
                   else if(data == '0'){
                   // alert('failed');
                   $('.alert-danger').show();
                   $('.alert-success').hide();
                   }
                   $(".LoadingImage").hide();
               }
             });
   
           });
   
   
   
   
   });
   
</script> -->
<script type="text/javascript">
   $(document).ready(function(){
     
   
    /* $( function() {
    $( "#alluserdatepicker, #alluserdatepicker12,#newactivesdatepicker, #newactivesdatepicker12,#newinactivesdatepicker, #newinactivesdatepicker12,#frozensdatepicker, #frozensdatepicker12,#archivesdatepicker, #archivesdatepicker12" ).datepicker({autoclose: true});
    });*/
   } );
   
   
   /*$("#sel").html('<form action="<?php echo base_url();?>User/user_exportCSV" method="POST"><div class="filter-task1"><h2>Filter:</h2><ul><li><img src="http://remindoo.org/CRMTool/assets/images/by-date1.png" alt="data"><input type="text" id="datepicker" name="de" placeholder="By Date"></li><li><img src="http://remindoo.org/CRMTool/assets/images/by-date2.png" alt="data"><select id="statuswise_filter" name="stati"><option value="0">By Status</option><option value="0">Active</option><option value="1">In-Active</option><option value="2">Frozen</option></select></li><li><button type="submit" class="btn but">Excel</button></li></form>
   
   <li><form action="<?php echo base_url();?>User/pdf" method="POST"><div style="display:none"><input type="text" id="picker" name="pickea"/><select id="hide_status" name="pickeb"><option value="0">By Status</option><option value="1">Active</option><option value="2">In-Active</option><option value="3">Fozen</option></select></div><a href="<?php echo base_url();?>User/pdf" class="btn but">Pdf</a></form></
   li></ul></div>');*/
   /*$("#sel").html('<form action="<?php echo base_url();?>User/user_exportCSV" method="POST"><div class="filter-task1"><h2>Filter:</h2><ul><li><img src="http://remindoo.org/CRMTool/assets/images/by-date1.png" alt="data"><input type="text" id="datepicker" name="" placeholder="By Date"></li><li><img src="<?php echo base_url(); ?>assets/images/by-date2.png" alt="data"><select id="statuswise_filter" name="stati"><option value="0">By Status</option><option value="1">Active</option><option value="2">In-Active</option><option value="3">Frozen</option></select></li><li><button type="submit" class="btn but">Excel</button></li></form><li><form action="<?php echo base_url();?>User/pdf" method="POST"><div style="display:none"><input type="text" id="picker" name="pickea"/><select id="hide_status" name="pickeb"><option value="0">By Status</option><option value="1">Active</option><option value="2">In-Active</option><option value="3">Frozen</option></select></div><button type="submit" class="btn but">Pdf</button></form></li></ul></div>');*/
   
   
   
  // $( document ).on('change', "#datepicker,#datepicker12", function(){ .allusers-box
    $(document).on('click','.newactivess-box, .newinactivess-box, .frozenss-box, .archivess-box',function(){/*
    // alert('ok');
         var row=$(this).parents('.dataTables_wrapper').attr('id');
          var parents = row.split('_');   
          var parent=parents[0];


       //var time=$(this).val();
       var time=$(this).parents('.dataTables_wrapper').find('#'+parent+'datepicker').val();
       var time1=$(this).parents('.dataTables_wrapper').find('#'+parent+'datepicker12').val();
      var status=$(this).parents('.dataTables_wrapper').find("#"+parent+"statuswise_filter").val();
      var priority=$(this).parents('.dataTables_wrapper').find("#"+parent+"priority").val();
      
     
      $.ajax({
                     type: "POST",
                     url: "<?php echo base_url();?>User/date_time_filter",
                     data: {selected_date:time,status:status,priority:priority,selected_end_date:time1,table:parent},
                      async: false,
                        beforeSend:function(){$(".LoadingImage").show();},
                      
                     success: function(data) {
                    $(".LoadingImage").hide();
                
             $("."+parent+"_al").html(data); 
        var check=0;
        var check1=0;
        var numCols = $('#'+parent+' thead th').length;   
        var table = $('#'+parent).DataTable({
           "dom": '<"toolbar-table">lfrtip',           
        order: [],
        columnDefs: [ { orderable: false, targets: [0]}],
         initComplete: function () { 
                    var q=1;
                       $('thead th').find('.filter_check').each( function(){
                         $(this).attr('id',q);
                         q++;
                       });
                    for(i=1;i<numCols;i++){      
                        if(i==1){
                            check=1;          
                            i=Number(i) + 1;
                            var select = $("#1"); 
                        }else if(i==7){
                            check=1;            
                            i=Number(i) + 1;
                            var select = $("#7"); 
                        }else{
                            var select = $("#"+i); 
                        }          
                        this.api().columns([i]).every( function () {
                          var column = this;
                          column.data().unique().sort().each( function ( d, j ) {    
               //             console.log(d);         
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                          });
                        });                   
                      if(check=='1'){                 
                         i=Number(i) - 1;
                         check=0;
                      } 
                       $("#"+i).formSelect();  
                    }
          }
      });
        for(j=1;j<numCols;j++){  
            $('#'+j).on('change', function(){  

              var c=$(this).attr('id');   

            //  alert(c);      
                var search = [];              
                $.each($('#'+c+ ' option:selected'), function(){                
                    search.push($(this).val());
                });      
                search = search.join('|');             
                if(c==7){               
                  c=Number(c) + 1;
                }             
                table.column(c).search(search, true, false).draw();  
            });
         }


         $('#'+parent+' thead th').on("click.DT", function (e) {  
          //stop Propagation if clciked outsidemask
          //becasue we want to sort locally here
           if (!$(e.target).hasClass('sortMask')) {
               e.stopImmediatePropagation();
          }
       });



         $('#'+parent+' thead th .themicond').on('click', function(e) {

  if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
    $(this).parent().find('.dropdown-content').removeClass('Show_content');
  }else{
     $('.dropdown-content').removeClass('Show_content');
     $(this).parent().find('.dropdown-content').addClass('Show_content');
  }
    $(this).parent().find('.select-wrapper').toggleClass('special');
 if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
   // alert('yes');

}else{
  $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
  $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
}
   
  });


if(parent=="alluser")
{
var status_filter='<select id="'+parent+'statuswise_filter" name="stati"><option value="" hidden>By Status</option><option value="">All</option><option value="1">Active</option><option value="2">In-Active</option><option value="3">Fozen</option></select>';
}
else 
{
  var status_filter='';
}





     $(document).ready(function(){
     
     $("#"+parent+"_wrapper").find(".toolbar-table").html('<div class="filter-task1 sets"><h2>Filter:</h2><ul><li><span class="for-part"><label class="f2t">From</label><input type="text" id="'+parent+'datepicker"  class="date_picker_dob" name="from_date_picker" placeholder="By Date" value="'+time+'"></span><span class="for-part"><label class="f2t">To</label><input type="text" id="'+parent+'datepicker12" class="date_picker_dob" name="to_date_picker" placeholder="By Date" value="'+time1+'"></span><a href="javascript:;" class="'+parent+'s-box"><i class="fa fa-search"></i></a></li><li><img src="<?php echo base_url(); ?>assets/images/by-date2.png" alt="data">'+status_filter+'</li><li><select id="'+parent+'priority"><option value=""hidden>Priority</option><option value="0">All</option><option value="Private Limited company">Private Limited company</option><option value="Public Limited company">Public Limited company</option><option value="Limited Liability Partnership" >Limited Liability Partnership</option><option value="Partnership">Partnership</option><option value="Self Assessment" >Self Assessment</option><option value="Trust">Trust</option><option value="Charity" >Charity</option><option value="Other" >Other</option></select></li><li><div class="dropdown"><button onclick="myFunction()" class="dropbtn">Export</button><div id="myDropdown" class="dropdown-content"><a href="<?php echo base_url();?>User/user_exportCSV?stati='+status+'&de='+time+'&de7='+time1+'&pe='+priority+'&table='+parent+'" class="but">Csv</a><a href="<?php echo base_url();?>User/pdf?stati='+status+'&de='+time+'&de7='+time1+'&pe='+priority+'&table='+parent+'" class="but">Pdf</a><a href="<?php echo base_url();?>User/html?stati='+status+'&de='+time+'&de7='+time1+'&pe='+priority+'&table='+parent+'" class="but" target="_blank">HTML</a></div></div></li></ul></div>');


                  $( function() {
                 $( "#"+parent+"datepicker, #"+parent+"datepicker12" ).datepicker({autoclose: true});
                  } );
     
                  $("#"+parent+"statuswise_filter").val(status);
                  $("#"+parent+"priority").val(priority);

                     });
     
         //    }
         // }); 
        <?php       
    $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $ex_url=explode('/', $actual_link);
    $ex_url_val='';
    if($_SESSION['role']==5 || $_SESSION['role']==6){
        if(isset($ex_url[4]) && $ex_url[4]=='user' && !isset($ex_url[5]))
        {
        $ex_url_val='had';
        }
  }
   if($ex_url_val!=''){ ?>
  $('p.action_01').css('pointer-events','none');
  $('td > .status').css('pointer-events','none');
  $('.checkbox-fade.fade-in-primary').css('pointer-events','none');

  <?php } ?>
                         
       
                     },
                     //async:false,
                  });
     
     */});
         // alert($(this).val());
   $( document ).on('change', "#alluserstatuswise_filter , #newactivesstatuswise_filter , #newinactivesstatuswise_filter , #frozensstatuswise_filter , #archivesstatuswise_filter", function(){  /*
 
      
        var row=$(this).parents('.dataTables_wrapper').attr('id');
        var parents = row.split('_');   
        var parent=parents[0];
         //alert(parent);
        var status=$(this).val();    
        var time=$(this).parents('.dataTables_wrapper').find("#"+parent+"datepicker").val();
        var time1=$(this).parents('.dataTables_wrapper').find('#'+parent+'datepicker12').val();
        var priority=$(this).parents('.dataTables_wrapper').find("#"+parent+"priority").val();
          $.ajax({
                   type: "POST",
                   url: "<?php echo base_url();?>User/user_status_filter",
                   data: {filterstatus:status,time:time,priority:priority,time1:time1,table:parent},
                    async: false,
                       beforeSend:function(){$(".LoadingImage").show();},
                    
                   success: function(data){
                    $(".LoadingImage").hide();
                    //console.log(data);
                   // alert('ok');
                    console.log("."+parent+"_al");
                     $("."+parent+"_al").html('');  
                      $("."+parent+"_al").html(data);  
                        var check=0;
                        var check1=0;
                        var numCols = $('#'+parent+' thead th').length;   
                        var table = $('#'+parent).DataTable({
                         "dom": '<"toolbar-table">lfrtip',
                          order: [],
                          columnDefs: [ { orderable: false, targets: [0]}],
                          initComplete: function () { 
                                var q=1;
                                   $('thead th').find('.filter_check').each( function(){
                                     $(this).attr('id',q);
                                     q++;
                                   });
                                for(i=1;i<numCols;i++){      
                                    if(i==1){
                                        check=1;          
                                        i=Number(i) + 1;
                                        var select = $("#1"); 
                                    }else if(i==7){
                                        check=1;            
                                        i=Number(i) + 1;
                                        var select = $("#7"); 
                                    }else{
                                        var select = $("#"+i); 
                                    }          
                                    this.api().columns([i]).every( function () {
                                      var column = this;
                                      column.data().unique().sort().each( function ( d, j ) {    
                //                        console.log(d);         
                                        select.append( '<option value="'+d+'">'+d+'</option>' )
                                      });
                                    });                   
                                  if(check=='1'){                 
                                     i=Number(i) - 1;
                                     check=0;
                                  } 
                                   $("#"+i).formSelect();  
                                }
                          }
                        });
                      for(j=1;j<numCols;j++){  
                        $('#'+j).on('change', function(){  
                          var c=$(this).attr('id');         
                            var search = [];              
                            $.each($('#'+c+ ' option:selected'), function(){                
                                search.push($(this).val());
                            });      
                            search = search.join('|');             
                            if(c==8){               
                              c=Number(c) + 1;
                            }             
                            table.column(c).search(search, true, false).draw();  
                        });
                      }

   
     $('#'+parent+' thead th').on("click.DT", function (e) {

     //   alert('ok');
        //stop Propagation if clciked outsidemask
        //becasue we want to sort locally here
         if (!$(e.target).hasClass('sortMask')) {
             e.stopImmediatePropagation();
        }
     });

            $('#'+parent+' thead th .themicond').on('click', function(e) {

if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
  $(this).parent().find('.dropdown-content').removeClass('Show_content');
}else{
   alert('hi');
   $('.dropdown-content').removeClass('Show_content');
   $(this).parent().find('.dropdown-content').addClass('Show_content');
}
  $(this).parent().find('.select-wrapper').toggleClass('special');
 if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
   // alert('yes');

}else{
$(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
$(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
}
 
});
   if(parent=="alluser")
{
var status_filter='<select id="'+parent+'statuswise_filter" name="stati"><option value="" hidden>By Status</option><option value="">All</option><option value="1">Active</option><option value="2">In-Active</option><option value="3">Fozen</option></select>';
}
else 
{
  var status_filter='';
}

   
    $("#"+parent+"_wrapper").find(".toolbar-table").html('<div class="filter-task1"><h2>Filter:</h2><ul><li><span class="for-part"><label class="f2t">From</label><input type="text" id="'+parent+'datepicker"  class="date_picker_dob" name="from_date_picker" placeholder="By Date" value="'+time+'"></span><span class="for-part"><label class="f2t">To</label><input type="text" id="'+parent+'datepicker12" name="to_date_picker" placeholder="By Date" class="date_picker_dob" value="'+time1+'"></span><a href="javascript:;" class="'+parent+'s-box"><i class="fa fa-search"></i></a></li><li><img src="<?php echo base_url(); ?>assets/images/by-date2.png" alt="data">'+status_filter+'</li><li><select id="'+parent+'priority" class="priority"><option value=""hidden>Priority</option><option value="0">All</option><option value="Private Limited company">Private Limited company</option><option value="Public Limited company">Public Limited company</option><option value="Limited Liability Partnership" >Limited Liability Partnership</option><option value="Partnership">Partnership</option><option value="Self Assessment" >Self Assessment</option><option value="Trust">Trust</option><option value="Charity" >Charity</option><option value="Other" >Other</option></select></li><li><div class="dropdown"><button onclick="myFunction()" class="dropbtn">Export</button><div id="myDropdown" class="dropdown-content"><a href="<?php echo base_url();?>User/user_exportCSV?stati='+status+'&de='+time+'&de7='+time1+'&pe='+priority+'&table='+parent+'" class="but">Csv</a><a href="<?php echo base_url();?>User/pdf?stati='+status+'&de='+time+'&de7='+time1+'&pe='+priority+'&table='+parent+'" class="but">Pdf</a><a href="<?php echo base_url();?>User/html?stati='+status+'&de='+time+'&de7='+time1+'&pe='+priority+'&table='+parent+'" class="but" target="_blank">HTML</a></div></div></li></ul></div>');
                
                $( function() {
                 $( "#"+parent+"datepicker, #"+parent+"datepicker12" ).datepicker({autoclose: true});
                } );
   
                $("#"+parent+"statuswise_filter").val(status);
                $("#"+parent+"priority").val(priority);

                
       //  },
   
   //   });        
   
           <?php       
  $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  $ex_url=explode('/', $actual_link);
  $ex_url_val='';
  if($_SESSION['role']==5 || $_SESSION['role']==6){
      if(isset($ex_url[4]) && $ex_url[4]=='user' && !isset($ex_url[5]))
      {
      $ex_url_val='had';
      }
}
 if($ex_url_val!=''){ ?>
$('p.action_01').css('pointer-events','none');
$('td > .status').css('pointer-events','none');
$('.checkbox-fade.fade-in-primary').css('pointer-events','none');

<?php } ?>
   
   
           
                       
     
                   },
                   //async:false,
                });
   
       */});
   
   
   $(  document ).on('change', "#alluserpriority , #newactivespriority , #newinactivespriority , #frozenspriority , #archivespriority ",function(){/*
               //alert("sdfg");
         

        var row=$(this).parents('.dataTables_wrapper').attr('id');
        var parents = row.split('_');   
        var parent=parents[0];
             
      var time=$(this).parents('.dataTables_wrapper').find("#"+parent+"datepicker").val();
      var time1=$(this).parents('.dataTables_wrapper').find("#"+parent+"datepicker12").val();
      var status=$(this).parents('.dataTables_wrapper').find("#"+parent+"statuswise_filter").val();
      var priority=$(this).val();

      $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>User/priority_status",
      data: {time:time,filterstatus:status,priority:priority,time1:time1,table:parent},
        beforeSend:function(){$(".LoadingImage").show();},
      success: function(data)
      {

        $(".LoadingImage").hide();
          // console.log(data);
      //$(".all-usera1").html('');  
      $("."+parent+"_al").html(data); 
      var check=0;
      var check1=0;
      var numCols = $('#'+parent+' thead th').length;   
      var table = $('#'+parent).DataTable({
         "dom": '<"toolbar-table">lfrtip',
        order: [],
        columnDefs: [ { orderable: false, targets: [0]}],
       initComplete: function () { 
                  var q=1;
                     $('thead th').find('.filter_check').each( function(){
                       $(this).attr('id',q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){      
                      if(i==1){
                          check=1;          
                          i=Number(i) + 1;
                          var select = $("#1"); 
                      }else if(i==7){
                          check=1;            
                          i=Number(i) + 1;
                          var select = $("#7"); 
                      }else{
                          var select = $("#"+i); 
                      }          
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                //          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      });                   
                    if(check=='1'){                 
                       i=Number(i) - 1;
                       check=0;
                    } 
                     $("#"+i).formSelect();  
                  }
        }
    });
      for(j=1;j<numCols;j++){  
          $('#'+j).on('change', function(){  
            var c=$(this).attr('id');         
              var search = [];              
              $.each($('#'+c+ ' option:selected'), function(){                
                  search.push($(this).val());
              });      
              search = search.join('|');             
              if(c==8){               
                c=Number(c) + 1;
              }             
              table.column(c).search(search, true, false).draw();  
          });
       }
                                      
     $('#'+parent+' thead th').on("click.DT", function (e) {

      //  alert('ok');
        //stop Propagation if clciked outsidemask
        //becasue we want to sort locally here
         if (!$(e.target).hasClass('sortMask')) {
             e.stopImmediatePropagation();
        }
     });


$('#'+parent+' thead th .themicond').on('click', function(e) {

if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
  $(this).parent().find('.dropdown-content').removeClass('Show_content');
}else{
  alert('2');
   $('.dropdown-content').removeClass('Show_content');
   $(this).parent().find('.dropdown-content').addClass('Show_content');
}
  $(this).parent().find('.select-wrapper').toggleClass('special');
 if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
   // alert('yes');

}else{
$(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
$(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
}
 
});

if(parent=="alluser")
{
var status_filter='<select id="'+parent+'statuswise_filter" name="stati"><option value="" hidden>By Status</option><option value="">All</option><option value="1">Active</option><option value="2">In-Active</option><option value="3">Fozen</option></select>';
}
else 
{
  var status_filter='';
}



    $("#"+parent+"_wrapper").find(".toolbar-table").html('<div class="filter-task1"><h2>Filter:</h2><ul><li><span class="for-part"><label class="f2t">From</label><input type="text" id="'+parent+'datepicker"  class="date_picker_dob" name="from_date_picker" placeholder="By Date" value="'+time+'"></span><span class="for-part"><label class="f2t">To</label><input type="text" id="'+parent+'datepicker12"  class="date_picker_dob" name="to_date_picker" placeholder="By Date" value="'+time1+'"></span><a href="javascript:;" class="'+parent+'s-box"><i class="fa fa-search"></i></a></li><li><img src="<?php echo base_url(); ?>assets/images/by-date2.png" alt="data">'+status_filter+'</li><li><select id="'+parent+'priority"><option value=""hidden>Priority</option><option value="0">All</option><option value="Private Limited company">Private Limited company</option><option value="Public Limited company">Public Limited company</option><option value="Limited Liability Partnership" >Limited Liability Partnership</option><option value="Partnership">Partnership</option><option value="Self Assessment" >Self Assessment</option><option value="Trust">Trust</option><option value="Charity" >Charity</option><option value="Other" >Other</option></select></li><li><div class="dropdown"><button onclick="myFunction()" class="dropbtn">Export</button><div id="myDropdown" class="dropdown-content"><a href="<?php echo base_url();?>User/user_exportCSV?stati='+status+'&de='+time+'&de7='+time1+'&pe='+priority+'&table='+parent+' " class="but">Csv</a><a href="<?php echo base_url();?>User/pdf?stati='+status+'&de='+time+'&de7='+time1+'&pe='+priority+'&table='+parent+'" class="but">Pdf</a><a href="<?php echo base_url();?>User/html?stati='+status+'&de='+time+'&de7='+time1+'&pe='+priority+'&table='+parent+'" class="but" target="_blank">HTML</a></div></div></li></ul></div>');
   
                $( function() {
                 $( "#"+parent+"datepicker, #"+parent+"datepicker12" ).datepicker({autoclose: true});
                } );
   
                $("#"+parent+"statuswise_filter").val(status);
                $("#"+parent+"priority").val(priority);
      //    },
   
      // });       
   
                 <?php       
  $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  $ex_url=explode('/', $actual_link);
  $ex_url_val='';
  if($_SESSION['role']==5 || $_SESSION['role']==6){
      if(isset($ex_url[4]) && $ex_url[4]=='user' && !isset($ex_url[5]))
      {
      $ex_url_val='had';
      }
}
 if($ex_url_val!=''){ ?>
$('p.action_01').css('pointer-events','none');
$('td > .status').css('pointer-events','none');
$('.checkbox-fade.fade-in-primary').css('pointer-events','none');

<?php } ?>     
   
   
   
                                     },
                           });
   
           */});
   
   




   </script>
<script type="text/javascript">
   $(function() {
   //----- OPEN
   $('[data-popup-open]').on('click', function(e)  {
   var targeted_popup_class = jQuery(this).attr('data-popup-open');
   $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
   e.preventDefault();
   });
   //----- CLOSE
   $('[data-popup-close]').on('click', function(e)  {
   var targeted_popup_class = jQuery(this).attr('data-popup-close');
   $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
   e.preventDefault();
   });
   });
</script>
<script type="text/javascript">
   $(document).ready(function() {
   
         $( "#control" ).change(function() {
   
   var sort= $("#control").val();
   
       $.ajax({
   
           type:"post",
           url: "<?php echo base_url();?>User/fetch_user",
           data:{           
             control:sort
                },
           dataType: 'json',
   
           success: function(res) {
            
          var jsonText = JSON.stringify(res); 
   
          var id = JSON.stringify(res.comments);
        
   
    $.each($.parseJSON(id), function(idx, obj) {
       $('input#feed_id').val(obj.id);
        $('input#feed_email').val(obj.crm_email_id);
         $('input#feed_user').val(obj.username);
   
   });
   
     }   
          });
         });
   
      });
     
</script> 
<!-- <script src="<?php //echo base_url()?>assets/js/jquery-1.11.1.min.js"></script> -->
<!-- <script src="<?php echo base_url()?>assets/js/additional-methods.min.js"></script> -->

<script type="text/javascript">

/*jQuery(document).ready(function( $ ) {
    $(document).on("click",".date_picker_dob",function(){$(this).datepicker({autoclose:true});});
});*/

$(document).ready(function(){

$(document).on("click",".closes_mail_id",function(){
var id=$(this).data("id");
  //alert(typeof(id.toString()));
var arr=$("#recivers_id").val().split(",");
/*console.log(arr);
alert(arr.indexOf(id));*/
var index = arr.indexOf(id.toString());
if (index > -1) {
  arr.splice(index, 1);
}
//console.log(arr);
$("#recivers_id").val(arr.join(","));
$(this).parent().parent().hide();

});

$('#send_mail_form').validate({
      ignore:false,
      errorPlacement: function(error, element) {
        if($(element).attr('name')=='mail_ids')
        {
          $("#mail_text_error").html(error.prop('outerHTML'));
        }else
        {
          error.insertAfter(element);
        }
      },
        rules:{mail_ids:{required: true},sub:{required: true}},
        messages: {mail_ids:{required: "Mail Id Required"}},
        submitHandler: function(form) {
         var msg =  tinyMCE.get('notification_msg').getContent();//CKEDITOR.instances.notification_msg.getData();


         var sub =  $("#notification_sub").val();
         var id  =  $("#recivers_id").val(); 
        $.ajax({
            url: "<?php echo base_url()?>user/send_mail_sms",
            type: "POST",
            data:{"sub":sub,"msg":msg,"mail_ids":id,"status":"send"},
            beforeSend:function(){$(".LoadingImage").show();},
            success: function(response) {
              $(".LoadingImage").hide();
              $('.info_popup').show();
              $('.info_popup .position-alert1').html('Mail Send Successfully..');
           }        
        });
    }
});

   $('#feed').click(function(){
   $("#feed_form").validate({
     rules: {
         control: {
             required: true,
         },
         feed_msg: {
             required: true,
         },      
     },
     messages: {
         control: "Please select username",
         feed_msg: {
             required: "Please Enter your message",
         },
              
          },
     });
  

 });
});
   
</script>
<script type="text/javascript">
   $('#send').click(function(){
   var value = $('#editor4').val(); 
   if(value=='')
   {
   document.getElementById("error_msg").innerHTML = "Enter Message";
   return false;
   }
   else
   {
   return true;
   }
   });
   
   
   // function myFunction() {
   // document.getElementById("myDropdown").classList.toggle("show");
   // }
   // window.onclick = function(event) {
   // if (!event.target.matches('.dropbtn')) {
   
   // var dropdowns = document.getElementsByClassName("dropdown-content");
   // var i;
   // for (i = 0; i < dropdowns.length; i++) {
   // var openDropdown = dropdowns[i];
   // if (openDropdown.classList.contains('show')) {
   //   openDropdown.classList.remove('show');
   // }
   // } 
   // }
   // }


              
/*
   function confirm(id)
   {
   $('#user_delete_'+id).show();
   return false;
   }
   function confirm_active(id)
   {
   $('#active_delete_'+id).show();
   return false;
   }
   function confirm_nolonger(id)
   {
   $('#inactive_delete_'+id).show();
   return false;
   }
   function confirm_frozen(id)
   {
   $('#delete_frozen'+id).show();
   return false;
   }
   
   $(document).on('click','#close, .close',function(e)
   {
    alert("vcbcncv");
   $('.alert-success').hide();
   $('.modal-backdrop').remove();
   $('body').attr("style","");
   
   });
   */
   
   $(document).ready(function(){/*
   
   
   $('#deleteActive_records').on('click', function() {
   var new_active = [];
   $(".active_checkbox:checked").each(function() {
   new_active.push($(this).data('active-id'));
   });
   if(new_active.length <=0) {
   $('.alert-danger-check').show();
   } else {
   $('#delete_all_active'+new_active).show();
   $('.delete_yes').click(function() {
    var selected_active_values = new_active.join(",");
   
   $.ajax({
     type: "POST",
     url: "<?php echo base_url().'user/multipleActiveDelete';?>",
     cache: false,
     data: 'data_id='+selected_active_values,
     beforeSend: function() {
       $(".LoadingImage").show();
     },
     success: function(data) {
       $(".LoadingImage").hide();
       var active_ids = data.split(",");
       for (var i=0; i < active_ids.length; i++ ) { 
       $("#"+active_ids[i]).remove(); } 
       $(".alert-success-delete").show();
       setTimeout(function() { 
       location.reload(); }, 500);     
     }
   });
   });
   }
   });
   
   
   
   $('#deleteActive_records').on('click', function() {
   var new_inactive = [];
   $(".inactive_checkbox:checked").each(function() {
   new_inactive.push($(this).data('inactive-id'));
   });
   if(new_inactive.length <=0) {
   $('.alert-danger-check').show();
   } else {
   $('#delete_all_inactive'+new_inactive).show();
   $('.delete_yes1').click(function() {
    var selected_inactive_values = new_inactive.join(",");
   
   $.ajax({
     type: "POST",
     url: "<?php echo base_url().'user/multipleActiveDelete';?>",
     cache: false,
     data: 'data_id='+selected_inactive_values,
     beforeSend: function() {
       $(".LoadingImage").show();
     },
     success: function(data) {
       $(".LoadingImage").hide();
       var inactive_ids = data.split(",");
       for (var i=0; i < inactive_ids.length; i++ ) { 
       $("#"+inactive_ids[i]).remove(); } 
       $(".alert-success-delete").show();
       setTimeout(function() { 
       location.reload(); }, 500);     
     }
   });
   });
   }
   });
   
   
   
   $('#deleteActive_records').on('click', function() {
   var frozen = [];
   $(".frozen_checkbox:checked").each(function() {
   frozen.push($(this).data('frozen-id'));
   });
   if(frozen.length <=0) {
   $('.alert-danger-check').show();
   } else {
   $('#delete_all_frozen'+frozen).show();
   $('.delete_yes2').click(function() {
    var selected_frozen_values = frozen.join(",");
   
   $.ajax({
     type: "POST",
     url: "<?php echo base_url().'user/multipleActiveDelete';?>",
     cache: false,
     data: 'data_id='+selected_frozen_values,
     beforeSend: function() {
       $(".LoadingImage").show();
     },
     success: function(data) {
       $(".LoadingImage").hide();
       var frozen_ids = data.split(",");
       for (var i=0; i < frozen_ids.length; i++ ) { 
       $("#"+frozen_ids[i]).remove(); } 
       $(".alert-success-delete").show();
       setTimeout(function() { 
       location.reload(); }, 500);     
     }
   });
   });
   }
   });
      
   */});
   
</script>

<script type="text/javascript">






 
$(document).ready(function() {
 
    
   /*   $(".tap_click").click(function(){
        toggle_action_button(0);
        var tap_id=$(this).attr("data-id");
        $(".table_content .tab-pane").each(function(){
          if(tap_id==$(this).attr("id"))
           $(this).addClass("active"); 
          else
           $(this).removeClass("active");
        }); 

        var cb_cls=$("#archive_button").attr('data-child-class');
        $("."+cb_cls).prop("checked", false);
        $("#select_all_"+cb_cls.replace("_checkbox",'')).prop("checked", false);

      var cb_cls=$(this).attr('data-child-id');
       //its for archive and unarchive 
       if(cb_cls=="archive_checkbox")$("#archive_button").hide();
       else $("#unarchive_button").hide();

       $("#send_mail_sms").attr('data-child-class',cb_cls);
       $("#archive_button").attr('data-child-class',cb_cls);
       $("#block_service").attr('data-child-class',cb_cls);
       $("#block_reminder").attr('data-child-class',cb_cls);
       $("#deleteUser_rec").attr('data-child-class',cb_cls);

      });*/













/*$(document).on('click', '#deleteUser_rec', function() {
  
  alert("inside");

  var userRec = [];
  $(".user_checkbox:checked").each(function() {
    userRec.push($(this).data('user-id'));
  });

alert(userRec.length);
  if(userRec.length <=0) {

     $('.alert-danger-check').show();

  } else {

    $('#delete_all_user'+userRec).show();
    $('.delete_yes').click(function() {
       var selected_user_values = userRec.join(",");

      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'user/multipleActiveDelete';?>",
        cache: false,
        data: 'data_id='+selected_user_values,
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          $(".LoadingImage").hide();
          var user_ids = data.split(",");
          for (var i=0; i < user_ids.length; i++ ) { 
          $("#"+user_ids[i]).remove(); } 
          $(".alert-success-delete").show();
          setTimeout(function() { 
          location.reload(); }, 500);     
        }
      });
    });
  }
});*/

  


});   

  



 


/*$('#alluser_filter input').on('change', function() {
        var oTable = $('#alluser').dataTable();
        oTable2.fnDraw();
    });*/
$(document).ready(function() {
          
//}catch(e){alert(e);}
        });

















/*$.noConflict();
jQuery(document).ready(function($) {

$(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true,changeYear: true});
   // Code that uses jQuery's $ can follow here.
});*/

</script>


 <script type="text/javascript">
  $(document).ready(function(){
   var $th = $('.dataTable').find('thead th')
$('.tableFixHead').on('scroll', function() {
  $th.css('transform', 'translateY('+ this.scrollTop +'px)');
});
});
 </script>