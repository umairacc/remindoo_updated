<?php

$this->load->view('includes/new_header'); ?>

<!-- <link rel="stylesheet" type="text/css" href="<?php //echo base_url()
                                                  ?>assets/css/email.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css"> -->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css?ver=2">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/tree_select/style.css?ver=2">


<style type="text/css">
  table.dataTable tbody th,
  table.dataTable tbody td {
    white-space: nowrap;
  }

  .modal-backdrop.fade.in {
    opacity: 0.5 !important;
  }

  .add_class {
    display: none;
  }

  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
  }

  .show {
    display: block !important;
  }

  tfoot {
    display: table-header-group;
  }
</style>
<div class="dummy_content" style="display: none;"></div>
<div class="modal fade" id="my_Modal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="archive_task_id" id="archive_task_id" value="">
        <input type="hidden" name="status_value" id="status_value" value="">
        <p>Do You Want Archive this Task?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="archive_action()">yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="deleteconfirmation" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="delete_task_id" id="delete_task_id" value="">
        <p> Are you sure you want to delete ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="confirm_delete" data-dismiss="modal" onclick="delete_action()">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="review_send" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="delete_task_id" id="delete_task_id" value="">
        <p> Are you sure want send review ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="confirm_delete" data-dismiss="modal" onclick="review_action()">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<?php
$service_display = '';
$role = $this->Common_mdl->getRole($_SESSION['id']);
function convertToHoursMins($time, $format = '%02d:%02d')
{
  if ($time < 1) {
    return;
  }
  $hours = floor($time / 60);
  $minutes = ($time % 60);
  return sprintf($format, $hours, $minutes);
}

function calculate_test($a, $b)
{
  $difference = $b - $a;

  $second = 1;
  $minute = 60 * $second;
  $hour   = 60 * $minute;

  $ans["hour"]   = floor(($difference) / $hour);
  $ans["minute"] = floor((($difference) % $hour) / $minute);
  $ans["second"] = floor(((($difference) % $hour) % $minute) / $second);
  //echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

  $test = $ans["hour"] . ":" . $ans["minute"] . ":" . $ans["second"];
  return $test;
}
function time_to_sec($time)
{
  list($h, $m, $s) = explode(":", $time);
  $seconds = 0;
  $seconds += (intval($h) * 3600);
  $seconds += (intval($m) * 60);
  $seconds += (intval($s));
  return $seconds;
}
function sec_to_time($sec)
{
  return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}

?>

<style>
  /** 28-06-2018 **/
  .time {
    padding: 8px;
    font-weight: bold;
  }


  select.close-test-04 {
    display: none;
  }

  /** 04-06-2018 **/
  span.demo {
    padding: 0 10px;
  }

  button.btn.btn-info.btn-lg.newonoff {
    padding: 3px 10px;
    height: initial;
    font-size: 15px;
    border-radius: 5px;
  }

  img.user_imgs {
    width: 38px;
    height: 38px;
    border-radius: 50%;
    display: inline-block;
  }


  #adduser label {
    text-transform: capitalize;
    font-size: 14px;
  }

  span.hours-left {
    background: #f76480;
    color: #fff;
    padding: 3px 10px;
    border-radius: 3px;
    margin-right: 15px;
  }

  .dropdown12 button.btn.btn-primary {
    background: #ccc;
    color: #555;
    font-size: 13px;
    padding: 4px 10px 4px;
    margin-top: -2px;
    border-color: transparent;
  }

  select+.dropdown:hover .dropdown-menu {
    display: none;
  }

  select+.dropdown12 .dropdown-menu {
    padding: 0 !important;
  }

  select+.dropdown12 .dropdown-menu a {
    color: #000 !important;
    padding: 7px 10px !important;
    border: none !important;
    font-size: 14px;
  }

  select+.dropdown12 .dropdown-menu li {
    border: none !important;
    padding: 0px !important;
  }

  select+.dropdown12 .dropdown-menu a:hover {
    background: #4c7ffe;
    color: #fff !important;
  }

  span.created-date {
    color: gray;
    padding-left: 10px;
    font-size: 13px;
    font-weight: 600;
  }

  span.created-date i.fa.fa-clock-o {
    padding: 0 5px;
  }

  .dropdown12 span.caret {
    right: -2px;
    z-index: 99999;
    border-top: 4px solid #555;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    border-bottom: 4px solid transparent;
    top: 12px;
    position: relative;
  }

  span.timer {
    padding: 0 10px;
    color: red;
  }

  body {
    font-family: "Arial", Helvetica, sans-serif;
    text-align: center;
  }

  #controls {
    font-size: 12px;
  }

  #time {
    font-size: 150%;
  }
</style>
<?php

// $for_main_task_timer=$result_status_array=array();
// $for_user_edit_per=array();     
// if(count($for_user_edit_permission)>0){
// foreach ($for_user_edit_permission as $edit_per_key => $edit_per_value) {
//  array_push($for_user_edit_per, $edit_per_value['id']);
// }
// }
?>
<!-- end of 29-06-2018 -->
<div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
  <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
    <div class="pop-realted1">
      <div class="position-alert1">
        Please! Select Record...
      </div>
    </div>
  </div>
</div>
<div id="action_result" class="modal-alertsuccess alert succ dashboard_success_message" style="display:none;">
  <div class="newupdate_alert"> <a href="#" class="close" id="close_action_result">×</a>
    <div class="pop-realted1">
      <div class="position-alert1">
        Success !!! Staff Assign have been changed successfully...
      </div>
    </div>
  </div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/progress-circle.css">

<div class="pcoded-content card-removes">
  <div class="pcoded-inner-content">
    <!-- Main-body start -->
    <div class="main-body">
      <div class="page-wrapper">
        <!-- Page body start -->
        <div class="page-body">
          <div class="row">
            <div class="col-sm-12">
              <!-- Register your self card start -->
              <div class="card">
                <!-- admin start-->
                <div class="client_section col-xs-12 floating_set newtaslcls">
                  <input type="hidden" name="assigneed_value" id="assigneed_value" value="">

                </div> <!-- all-clients -->
                <div class="all_user-section floating_set clientredesign">
                  <div class="deadline-crm1 floating_set tsk-crte">
                    <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                      
                      <li class="nav-item">
                        <a class="nav-link active" href="<?php echo base_url(); ?>user/task_list">
                          <img class="themicon" src="<?php echo base_url(); ?>assets/images/tabicon1.png" alt="themeicon" /><?php echo  strtoupper('All Tasks'); ?></a>
                        <div class="slide"></div>
                      </li>


                      <?php if ($_SESSION['permission']['Task']['create'] == 1) { ?>
                        <li class="nav-item">
                          <a class="nav-link " href="<?php echo base_url(); ?>user/new_task">
                            <img class="themicon" src="<?php echo base_url(); ?>assets/images/tabicon2.png" alt="themeicon" /><?php echo  strtoupper('Create  task') ?></a>
                          <div class="slide"></div>
                        </li>


                        <li class="nav-item">

                          <a class="nav-link" data-toggle="modal" data-target="#import-task">
                            <img class="themicon" src="<?php echo base_url(); ?>assets/images/tabicon3.png" alt="themeicon" /><?php echo  strtoupper('Import tasks') ?></a>
                          <div class="slide"></div>
                        </li>
                      <?php }
                      if ($_SESSION['user_type'] == 'FA') { ?>
                        <li class="nav-item">
                          <a class="nav-link " href="<?php echo base_url(); ?>Task_Status/task_progress_view">
                            <img class="themicon" src="<?php echo base_url(); ?>assets/images/tabicon4.png" alt="themeicon" /><?php echo  strtoupper('Task Progress Status') ?></a>
                          <div class="slide"></div>
                        </li>
                      <?php }

                      if ($_SESSION['permission']['Task']['edit'] == 1) { ?>

                        <li class="nav-item">
                          <a class="nav-link " href="<?php echo base_url(); ?>user/task_list_kanban">
                            <img class="themicon" src="<?php echo base_url(); ?>assets/images/tabicon4.png" alt="themeicon" /><?php echo  strtoupper('Switch To Kanban') ?></a>
                          <div class="slide"></div>
                        </li>

                      <?php } ?>

                    </ul>
                    <div class="taskpagedashboard new_layoutalign">

                      <div class="top-leads fr-task cfssc">
                        <div class="lead-data1 pull-right tsk-animation1">


                          <?php
                          $task_count = $all_count = 0;
                          $task_count = $task_list_count;

                          if ($status_condition == 0) {
                            $result_status_array = $task_status;


                          ?>


                            <div class="junk-lead tsk-color5 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val nots_task" data-id="" data-searchCol="status_TH">
                              <div class="lead-point1">
                                <strong><?php echo $task_count; ?></strong>
                                <span class="task_status_val_1 status_filter" data-id="">All</span>

                              </div>
                            </div>

                          <?php
                          } else {

                            $result_status_array = $this->Common_mdl->selectRecord('task_status', 'id', $status_condition);
                          }
                          //print_r($get_datalist);


                          foreach ($result_status_array as $key => $status_val) {

                            if (in_array($status_val['id'], array_keys($get_datalist))) {
                              $status_count = $get_datalist[$status_val['id']];
                            } else {
                              $status_count = 0;
                            }
                            //
                            //    echo $status_val['status_name'];

                          ?>

                            <div class="junk-lead tsk-color<?php echo $key ?>  color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val nots_task_<?php echo $status_val['id'] ?>" data-id="<?php echo $status_val['status_name'] ?>" data-searchCol="status_TH">
                              <div class="lead-point1">
                                <strong><?php

                                        echo $status_count; ?></strong>
                                <span class="task_status_val_1 status_filter" data-id="<?php echo $status_val['status_name'] ?>"><?php echo $status_val['status_name'] ?></span>

                              </div>
                            </div>

                          <?php } ?>



                        </div>
                      </div>
                    </div>

                    <div class="f-right">


                      <div class="assign_delete">
                        <?php if ($_SESSION['permission']['Task']['create'] == 1) { ?>
                          <button type="button" id="addSubtask-Btn" data-toggle="modal" data-target="#addSubTask-Popup" class="btn" style="display: none;">Add Sub Task </button>
                        <?php }
                        if ($_SESSION['permission']['Task']['edit'] == 1) { ?>
                          <button type="button" data-toggle="modal" data-target="#assignTask-Popup" id="task_assign_btn" class="btn   task_assign_btn" style="display: none;">Reassign</button>

                          <button type="button" id="archive_task" class="archive_task_button del-tsk12 " data-status="4" data-toggle="modal" data-target="#my_Modal" style="display: none;"><i class="fa fa-archive fa-6" aria-hidden="true"></i>Archive </button>
                        <?php } ?>
                        <!--   
                                      <button type="button"  data-toggle="modal" data-target="#review_send" id="btn_review_send" class="btn   for_user_review_send" style="display: none;">Send Review</button> -->

                        <?php if ($_SESSION['permission']['Task']['delete'] == '1') { ?>
                          <button type="button" data-toggle="modal" data-target="#deleteconfirmation" id="delete_task" class="del-tsk12 for_user_permission_delete" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete </button>
                        <?php } ?>



                        <button type="button" id="unarchive_task" class="archive_task_button del-tsk12 " data-status="unarchive" style="display: none;"><i class="fa fa-archive fa-6" aria-hidden="true"></i>Unarchive </button>
                      </div>
                    </div>
                  </div>
                  <div class="all_user-section2 floating_set <?php if ($_SESSION['permission']['Task']['view'] != '1') { ?> permission_deined<?php } ?>">
                    <div class="tab-content">
                      <div class="filter-data for-reportdash">
                        <div class="filter-head">
                          <h4>FILTERED DATA</h4>
                          <button class="btn btn-danger f-right" id="clear_container">clear</button>
                          <div id="container2" class="panel-body box-container">
                          </div>
                        </div>
                      </div>
                      <div id="alltasks" class="data_active_task tab-pane fade in active">
                        <div class="all_task_counts">
                        </div>
                        <input type="hidden" name="for_status_us" id="for_status_us">
                        <div class="client_section3  floating_set">
                          <div id="status_succ"></div>
                          <div class="all-usera1  user-dashboard-section1 for_task_status button_visibility">



                            <table class="table client_table1 all_task_table text-center display nowrap tasklistseccls" id="alltask" cellspacing="0" width="100%" data-status="notstarted">
                              <thead>
                                <tr class="text-uppercase">

                                  <th <?php if ($_SESSION['permission']['Task']['view'] != '1') {
                                        echo "style='display:none'";
                                      } ?> class="subtask_toggle_TH Exc-colvis">
                                    <div style="opacity:0">#</div>
                                  </th>

                                  <th class="select_row_TH Exc-colvis">
                                    <div class="checkbox-fade fade-in-primary">
                                      <label class="custom_checkbox1">
                                        <input type="checkbox" id="select_alltask">
                                        <i></i>
                                      </label>
                                    </div>
                                  </th>


                                  <th class="timer_TH hasFilter">Timer
                                    <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                    <div class="sortMask"></div>
                                  </th>





                                  <th class="subject_TH hasFilter">Subject
                                    <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                    <div class="sortMask"></div>

                                  </th>



                                  <th class="startdate_TH hasFilter">Start Date
                                    <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                    <div class="sortMask"></div>

                                  </th>


                                  <th class="duedate_TH hasFilter">Due Date
                                    <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                    <div class="sortMask"></div>

                                  </th>


                                  <th class="status_TH hasFilter">Status
                                    <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                    <div class="sortMask"></div>
                                    <select class="filter_check" searchable="true" multiple="" style="display: none;">

                                      <?php foreach ($task_status as $key => $status_val) { ?>

                                        <option value="<?php echo $status_val['status_name']; ?>"><?php echo $status_val['status_name']; ?></option>

                                      <?php } ?>

                                    </select>

                                  </th>


                                  <th class="progress_TH hasFilter">Progress Status
                                    <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                    <div class="sortMask"></div>
                                    <select class="filter_check" multiple="" searchable="true" style="display: none;">

                                      <?php foreach ($progress_task_status as $key => $status_val) { ?>

                                        <option value="<?php echo $status_val['status_name']; ?>"><?php echo $status_val['status_name']; ?></option>

                                      <?php } ?>

                                    </select>

                                  </th>





                                  <th class="priority_TH hasFilter">Priority
                                    <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                    <div class="sortMask"></div>
                                    <select class="filter_check" multiple="" searchable="true" style="display: none;">
                                      <option value="low">Low</option>
                                      <option value="medium">Medium</option>
                                      <option value="high">High</option>
                                      <option value="super_urgent">Super Urgent</option>
                                    </select>
                                  </th>



                                  <th class="tag_TH hasFilter">Tag
                                    <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                    <div class="sortMask"></div>

                                  </th>



                                  <th class="assignto_TH hasFilter">Assignto
                                    <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                    <div class="sortMask"></div>
                                    <select class="filter_check" multiple="" searchable="true" style="display: none;">
                                      <?php foreach ($user_list as $key => $value) { ?>
                                        <option value="<?php echo $value['crm_name']; ?>"><?php echo $value['crm_name']; ?></option>

                                      <?php   } ?>

                                    </select>
                                  </th>


                                  <th class="services_TH hasFilter">Related Services
                                    <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                    <div class="sortMask"></div>
                                  </th>

                                  <th class="company_TH hasFilter">Company Name
                                    <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                    <div class="sortMask"></div>
                                  </th>


                                  <th class="billable_TH hasFilter">Billable

                                    <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
                                    <div class="sortMask"></div>
                                    <select class="filter_check" multiple="" style="display: none;" searchable="true">
                                      <option value="Billable">Billable</option>
                                      <option value="Non Billable">Non Billable</option>
                                    </select>
                                  </th>
                                  <th class="due_TH">Due </th>


                                  <th class="action_TH Exc-colvis">Actions
                                  </th>
                                </tr>
                              </thead>

                            </table>

                            <!-- <table border="0" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td class="gutter">
                <div class="line number1 index0 alt2">1</div>
                <div class="line number2 index1 alt1">2</div>
                <div class="line number3 index2 alt2">3</div>
                <div class="line number4 index3 alt1">4</div>
                <div class="line number5 index4 alt2">5</div>
                <div class="line number6 index5 alt1">6</div>
                <div class="line number7 index6 alt2">7</div>
                <div class="line number8 index7 alt1">8</div>
                <div class="line number9 index8 alt2">9</div>
                <div class="line number10 index9 alt1">10</div>
                <div class="line number11 index10 alt2">11</div>
                <div class="line number12 index11 alt1">12</div>
                <div class="line number13 index12 alt2">13</div>
                <div class="line number14 index13 alt1">14</div>
                <div class="line number15 index14 alt2">15</div>
                <div class="line number16 index15 alt1">16</div>
                <div class="line number17 index16 alt1">17</div>
                <div class="line number18 index17 alt2">18</div>
                <div class="line number19 index18 alt1">19</div>
                <div class="line number20 index19 alt2">20</div>
                <div class="line number21 index20 alt1">21</div>
                <div class="line number22 index21 alt2">22</div>
                <div class="line number23 index22 alt1">23</div>
                <div class="line number24 index23 alt1">24</div>
                <div class="line number25 index24 alt1">25</div>
                <div class="line number26 index25 alt1">26</div>

            </td>
            <td class="code">
                <div class="container">
                    <div class="line number1 index0 alt2"><code class="js plain">$(document).ready(</code><code class="js keyword">function</code><code class="js plain">() {</code></div>
                    <div class="line number2 index1 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">$(</code><code class="js string">'#alltask'</code><code class="js plain">).dataTable( {</code></div>
                    <div class="line number3 index2 alt2"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">processing: </code><code class="js keyword">true</code><code class="js plain">,</code></div>
                    <div class="line number4 index3 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">serverSide: </code><code class="js keyword">true</code><code class="js plain">,</code></div>
                    <div class="line number5 index4 alt2"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">ajax: </code><code class="js string">"<?php echo base_url() ?>Test_slowness/test_ajx"</code><code class="js plain">,</code></div>


                    <div class="line number6 index5 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">columns: [</code></div>

                    <div class="line number7 index6 alt2"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string"></code> <code class="js plain">},</code></div>

                    <div class="line number8 index7 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string"><div class="checkbox-fade fade-in-primary">
                                        <label class="custom_checkbox1">
                                        <input type="checkbox" id="select_alltask">
                                         <i></i>
                                        </label></code> <code class="js plain">},</code></div>



                    <div class="line number9 index8 alt2"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Timer"</code> <code class="js plain">},</code></div>


                    <div class="line number10 index9 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Subject"</code> <code class="js plain">},</code></div>


                    <div class="line number11 index10 alt2"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Start Date"</code> <code class="js plain">},</code></div>

                    <div class="line number12 index11 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Due Date"</code> <code class="js plain">},</code></div>

                    <div class="line number13 index12 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Status"</code> <code class="js plain">},</code></div>

                    <div class="line number14 index13 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Progress Status"</code> <code class="js plain">},</code></div>

                    <div class="line number15 index14 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Priority"</code> <code class="js plain">},</code></div>

                    <div class="line number16 index15 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Tag"</code> <code class="js plain">},</code></div>

                    <div class="line number17 index16 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Assignto"</code> <code class="js plain">},</code></div>

                    <div class="line number18 index17 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Related Services"</code> <code class="js plain">},</code></div>


                    <div class="line number19 index18 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Company Name"</code> <code class="js plain">},</code></div>


                    <div class="line number20 index19 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Billable"</code> <code class="js plain">},</code></div>

                    <div class="line number21 index20 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Due"</code> <code class="js plain">},</code></div>


                    <div class="line number22 index21 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">{ data: </code><code class="js string">"Actions"</code> <code class="js plain">}</code></div>
 

                    <div class="line number23 index22 alt2"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">],</code></div>





                    <div class="line number24 index23 alt1"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">colReorder: </code><code class="js keyword">true</code></div>
                    <div class="line number25 index24 alt2"><code class="js spaces">&nbsp;&nbsp;&nbsp;&nbsp;</code><code class="js plain">} );</code></div>
                    <div class="line number26 index25 alt1"><code class="js plain">} );</code></div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td class="gutter">
                <div class="line number1 index0 alt2" style="display: none;">1</div>
            </td>
            <td class="code">
                <div class="container" style="display: none;">
                    <div class="line number1 index0 alt2" style="display: none;">&nbsp;</div>
                </div>
            </td>
        </tr>
    </tbody>
</table> -->

                            <input type="hidden" class="rows_selected" id="select_alltask_count">
                          </div>
                        </div>
                      </div>
                      <!-- home-->
                      <!-- new_user -->
                    </div>
                  </div>
                  <!-- admin close -->
                </div>
                <!-- Register your self card end -->
              </div>
            </div>
          </div>
          <!-- Page body end -->
        </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>


<div class="adduserpopup03 adduserpopup" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
      </div>
      <div class="modal-body">
        <input type="text" class="tree_select" name="assignees[]" placeholder="Select">
        <div class="formcon formconnew">
          <label>Reason</label>
          <select name="assign_task_status" class="form-control assign_task_status" id="assign_task_status_{0}">
            <option value="">select</option>
            <?php foreach ($assign_task_status as $key => $value1) {
              if ($value1['status_name'] != '') { ?>
                <option value="<?php echo $value1['id'] ?>"><?php echo $value1['status_name'] ?></option>
            <?php }
            } ?>
          </select>
          <div class="atagsection">

          </div>
        </div>
        <div class="formcon">
          <label>Description</label>
          <textarea rows="5" type="text" class="form-control" name="description" id="description_{0}" placeholder="Write something"></textarea>
        </div>
      </div>
      <div class="modal-footer profileEdit">
        <input type="hidden" name="hidden">
        <a href="javascript;" id="acompany_name" data-dismiss="modal" data-id="{0}" class="save_assign_staff">save</a>
      </div>
    </div>
  </div>
</div>
<div class="task_status_html"></div>




<div class="modal fade" id="import-task" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Import Task</h4>
      </div>
      <div class="modal-body">
        <!-- content update after page load source from tasks/import viewfile -->
      </div>
    </div>
  </div>
</div>






<div id="addSubTask-Popup" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align-last: center">Add Sub Task</h4>
      </div>
      <div class="modal-body">
        <input id="subTaskName" placeholder="Task Name" class="addSubtask">
      </div>
      <div class="modal-footer profileEdit">
        <a href="javascript:;" id="saveSubtaskbutton">save</a>
        <a href="javascript:;" data-dismiss="modal">Close</a>
      </div>
    </div>
  </div>
</div>

<div id="assignTask-Popup" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
      </div>
      <div class="modal-body">

        <input type="text" class="tree_select" name="assignees[]" placeholder="Select">

      </div>
      <div class="modal-footer profileEdit">
        <input type="hidden" name="hidden">
        <a href="javascript;" id="assign_tsk_btn" data-dismiss="modal" class="assign_tsk_btn">save</a>
      </div>
    </div>
  </div>
</div>



<?php $this->load->view('includes/session_timeout'); ?>
<?php $this->load->view('includes/new_footer'); ?>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/user_page/materialize.js"></script>

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/custom/buttons.colVis.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/custom/datatable_extension.js"></script>


<!-- <script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>  -->

<script src="<?php echo base_url(); ?>assets/js/jquery.dropdown.js"></script>

<!-- <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script> -->

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>


<!-- For Date fields sorting  -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/custom/datatable_cus_sorting.js"></script>
<!-- For Date fields sorting  -->

<script type="text/javascript">
  var vars = {};

  var reassign_status_poupup = '<div id="assignuser_{0}" class="adduserpopup03 adduserstatuspopup  modal fade" role="dialog">                              <div class="modal-dialog">           <div class="modal-content">                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title" style="text-align-last: center">Reason of Status</h4>                                    </div>                                <div class="modal-body delete_st_reason">                                  <input type="text" name="assignuser_staff" class="assignuser_staff_class" id="assignuser_staff_{0}"  >                             <div style="color: red" class="assign_staff_div"></div>                                    </div>                                <input type="hidden" name="assignuser_staffid" class="assignuser_staffid_class" id="assignuser_staffid_{0}"  >                                  <input type="hidden" name="update_staffid" class="update_staffid_class" id="update_staffid_{0}"  >          <div class="modal-footer profileEdit delete_st_reason">                                       <a href="javascript:;" id="assignuser_staff"  data-id="{0}" class="assignuser_staff">save</a>                                    </div>                                    <div class="modal-footer profileEdit delete_div_reason" style="display: none;">                                   <a href="javascript:;" id="assignuser_staff"  data-id="{0}" class="assignuser_staff">ok</a>                                         <a href="javascript;" data-dismiss="modal" data-id="{0}" >cancel</a>                                    </div>                                 </div>                              </div>                           </div>';


  $(document).ready(function() {

    $.fn.dataTable.moment('DD-MM-YYYY');

    $('.tags').tagsinput({
      allowDuplicates: true
    });




    $('.dropdown-sin-2').dropdown({
      limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });


    $('.dropdown-sin-7').dropdown({

      input: '<input type="text" maxLength="20" placeholder="Search">'
    });




    $('.dropdown-sin-25').dropdown({

      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
    $('.dropdown-sin-27').dropdown({

      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
    $('.dropdown-sin-29').dropdown({

      input: '<input type="text" maxLength="20" placeholder="Search">'
    });

  });









  $(document).ready(function() {

    $("#confirm-submit.confirm12").on("shown.bs.modal", function() {

      // alert('hi'); 

      $('div#pcoded + div + div + .modal-backdrop.fade.show').css('z-index', '9999');

    });

    $("#confirm-submit.confirm12").on("hidden.bs.modal", function() {

      // alert('hi'); 

      $('div#pcoded + div + div + .modal-backdrop.fade.show').css('z-index', '1040');

    })
  });
</script>

<script type="text/javascript">
  var TaskTable_Instance;
  var MainStatus_Tab = 'all_task';
  /*single and multiple row task archive*/
  $('.archive_task_button').on('click', function() {

    var alltask = getSelectedRow();

    if (alltask.length <= 0) {
      $('.popup_info_msg').show();
      $(".popup_info_msg .position-alert1").html("Please Selecte Any Records..");
    } else {
      // console.log(alltask);
      $("#archive_task_id").val(JSON.stringify(alltask));
      $("#status_value").val($(this).data('status'));
      //unarchive does not had conform box
      if ($(this).data('status') == "unarchive") archive_action();
    }
  });

  function archive_action() { //model yes button click
    var id = $("#archive_task_id").val();
    var status = $("#status_value").val();
    $.ajax({
      url: '<?php echo base_url(); ?>user/archive_update',
      type: 'POST',
      data: {
        'id': id,
        'status': status
      },
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {
        $(".LoadingImage").hide();
        $(".popup_info_msg").show();
        $(".popup_info_msg .position-alert1").html('Success !!! Task Archive successfully...');
        //    console.log(data);
        setTimeout(function() {
          $(".popup_info_msg").hide();
          location.reload();
        }, 1500);
      }

    });

  }

  function archieve_click(val) { //single button click
    //  alert('ok');
    //  alert($(val).attr('id'));
    $("#archive_task_id").val(JSON.stringify([$(val).attr('id')]));
    $("#status_value").val($(val).data('status'));
    //alert($("#archive_task_id").val());
    //alert($("#my_Modal").attr('class'));
  }
  /*single and multiple row task archive*/

  function humanise(diff) {

    // The string we're working with to create the representation 
    var str = '';
    // Map lengths of `diff` to different time periods 
    var values = [
      [' year', 365],
      [' month', 30],
      [' day', 1]
    ];
    // Iterate over the values... 
    for (var i = 0; i < values.length; i++) {
      var amount = Math.floor(diff / values[i][1]);
      // ... and find the largest time value that fits into the diff 
      if (amount >= 1) {
        // If we match, add to the string ('s' is for pluralization) 
        str += amount + values[i][0] + (amount > 1 ? 's' : '') + ' ';
        // and subtract from the diff 
        diff -= amount * values[i][1];
      } else {
        str += amount + values[i][0] + ' ';
      }

    }
    return str;
  }
</script>
<?php

?><script></script>
<script>
  // $(document).on('click','.sendtoreview',function(){
  //   var id=$(this).data("id");

  //   var timer=0;
  //   timer+=parseInt($(".timer_"+id+" .hours").text());
  //   timer+=parseInt($(".timer_"+id+" .minutes").text());
  //   timer+=parseInt($(".timer_"+id+" .seconds").text());
  // //alert(timer);
  // if(timer<=0){$(".popup_info_msg").show();$(".popup_info_msg .position-alert1").html("You Don't Have Worked Time..");return;}
  //   $.ajax({
  //             url: '<?php echo base_url(); ?>user/task_statusChange/',
  //             type: 'post',
  //             data: { 'rec_id':id,'status':'5'},
  //             timeout: 3000,
  //             success: function( data ){
  //                $(".popup_info_msg").show();
  //                $('.popup_info_msg .position-alert1').html('Success !!! Request Send successfully...');
  //                setTimeout(function () {
  //                 $(".popup_info_msg").hide();
  //                 }, 1500);

  //                 },
  //                 error: function( errorThrown ){
  //                     console.log( errorThrown );
  //                 }
  //             });
  // });
  function AddClass_SettingPopup() {

    $('button.Button_List').closest('.dt-button-collection').addClass('Settings_List');
    $('button.buttons-columnVisibility').closest('.dt-button-collection').addClass('columnVisibility_List');
  }

  $(document).ready(function() {

    // $.ajax({
    //               url: '<?php echo base_url(); ?>Test_slowness/ajax_task',
    //               type : 'POST',

    //                 beforeSend: function() {
    //                   $(".LoadingImage").show();
    //                 },
    //                 success: function(data) {
    //                   $('alltask tbody').html('').html(data.task_table);
    //                   $(".LoadingImage").hide();


    //                 }

    //               });
    /*for COLUMN oredering*/
    <?php
    $column_setting = Firm_column_settings('task_list');
    ?>
    var column_order = <?php echo $column_setting['order'] ?>;

    var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;


    var column_ordering = [];

    $.each(column_order, function(i, v) {

      var index = $('#alltask thead th').index($('.' + v));

      if (index !== -1) {
        // alert(index);
        column_ordering.push(index);
      }
    });

    console.log(column_ordering);
    /*for COLUMN oredering*/
    // table_sorting_trigger();
    //          "dom": '<"toolbar-table" <"#table-buttons.dropdown.table-buttons"<"#table-buttonsList.dropdown-menu.table-buttonsList"B>> >lfrtip',

    humanise();

    var pageLength = "<?php echo get_firm_page_length() ?>";

    TaskTable_Instance = $('#alltask').DataTable({
      //  "pageLength": parseInt( pageLength ),

      "processing": true,
      "ServerSide": true,

      'ajax': {
        'url': '<?php echo base_url() ?>Test_slowness/test_ajx',
        'type': 'POST',
        'data': function(data) {

          data.column_ordering = column_ordering;

        }
      },

      ' columns': [{
          data: "0"
        },
        {
          data: '1'
        },
        {
          data: "2"
        },
        {
          data: "3"
        },
        {
          data: "4"
        },
        {
          data: "5"
        },
        {
          data: "6"
        },
        {
          data: "7"
        },
        {
          data: "8"
        },
        {
          data: "9"
        },
        {
          data: "10"
        },
        {
          data: "11"
        },
        {
          data: "12"
        },
        {
          data: "13"
        },
        {
          data: "14"
        },
        {
          data: "15"
        }
      ],


      "language": {
        "emptyTable": "No Task is assigned to you"
      },


      "dom": '<"toolbar-table" B>lfrtip',

      buttons: [{
        extend: 'collection',
        background: false,
        text: '<i class="fa fa-cog" aria-hidden="true"></i>',
        className: 'Settings_Button',
        buttons: [{
            text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
            className: 'Button_List',
            action: function(e, dt, node, config) {
              Trigger_To_Reset_Filter();
            }
          },
          {
            extend: 'colvis',
            text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
            columns: ':not(.Exc-colvis)',
            className: 'Button_List',
            prefixButtons: [{
              text: 'X',
              className: 'close'
            }]
          }
        ]
      }],

      createdRow: function(row, data, dataIndex) {
        // Set the data-status attribute, and add a class

        //   console.log(data);
        var ci = 0;


        $.each(column_ordering, function(idx, val) {

          vars["col" + val + "_obj"] = $(row).find('td:eq(' + idx + ')');

        });

        // for(;ci<data.length;ci++)
        // {
        //  vars["col" + ci +"_obj"] =$( row ).find('td:eq('+ci+')');
        // }



        var tg_class_name = vars['col0_obj'].find('.first_td_class').val();
        var tg_tsk_id = vars['col0_obj'].find('.first_td_id').val();
        vars['col0_obj'].addClass(tg_class_name);
        vars['col0_obj'].attr('data-id', tg_tsk_id);

        vars['col0_obj'].parent('tr').attr({
          'class': 'count_section',
          "id": tg_tsk_id
        });


        $(row).find('td:eq(1)').addClass('select_row_TD');



        var col2_data_hour = vars['col2_obj'].find('.stopwatch').attr('data-hour');
        var col2_data_min = vars['col2_obj'].find('.stopwatch').attr('data-min');
        var col2_data_sec = vars['col2_obj'].find('.stopwatch').attr('data-sec');
        var col2_data_pauseon = vars['col2_obj'].find('.stopwatch').attr('data-pauseon');
        vars['col2_obj'].addClass('timer_TD timer_class');
        vars['col2_obj'].attr('data-search', (col2_data_hour).padStart(2, '0') + ' : ' + (col2_data_min).padStart(2, '0') + ' : ' + (col2_data_sec).padStart(2, '0'));


        vars['hours_' + tg_tsk_id] = col2_data_hour;
        vars['minutes_' + tg_tsk_id] = col2_data_min;
        vars['seconds_' + tg_tsk_id] = col2_data_sec;
        vars['milliseconds_' + tg_tsk_id] = 0;
        vars['data_pause_' + tg_tsk_id] = col2_data_pauseon;




        vars['col3_obj'].attr({
          "class": "subject_TD",

          "data-search": vars['col3_obj'].find('a').attr('data-val') // attributes which contain dash(-) should be covered in quotes.
        });

        vars['col4_obj'].attr({
          "class": "startdate_TD startdate_class",
          "data-sort": vars['col4_obj'].text(),
          "data-search": vars['col4_obj'].text() // attributes which contain dash(-) should be covered in quotes.
        });
        vars['col5_obj'].attr({
          "class": "duedate_TD duedate_class",
          "data-sort": vars['col5_obj'].find('div').text(),
          "data-search": vars['col5_obj'].find('div').text() // attributes which contain dash(-) should be covered in quotes.
        });




        var col6_selected_val = vars['col6_obj'].find(".task_status option:selected").text();
        //     console.log(col6_selected_val);


        vars['col6_obj'].attr({
          "class": "status_class",
          "id": "status_TD status_" + tg_tsk_id,
          "data-search": col6_selected_val // attributes which contain dash(-) should be covered in quotes.
        });


        vars['col8_obj'].attr({
          "class": "priority_TD priority_class",
          "data-search": vars['col8_obj'].find('.prio_class').text()

        });


        vars['col9_obj'].attr({
          "class": "tag_TD tag_class",
          "data-search": vars['col9_obj'].text() // attributes which contain dash(-) should be covered in quotes.
        });

        vars['col11_obj'].attr({
          "class": "services_TD services_class",
          "data-search": vars['col11_obj'].text() // attributes which contain dash(-) should be covered in quotes.
        });
        vars['col12_obj'].attr({
          "class": "s company_class",
          "data-search": vars['col12_obj'].text() // attributes which contain dash(-) should be covered in quotes.
        });



      },
      order: [], //ITS FOR DISABLE SORTING
      columnDefs: [{
          "visible": false,
          "targets": hidden_coulmns
        },
        {
          "orderable": false,
          "targets": ['subtask_toggle_TH', 'select_row_TH', 'timer_TH', 'action_TH']
        },
        {
          "orderable": true,
          "targets": '_all'
        }
      ],

      colReorder: {
        realtime: false,
        fixedColumnsLeft: 2,
        fixedColumnsRight: 1

      },

      //colReorder: true,

      colReorder: {
        realtime: false,
        order: column_ordering,
        fixedColumnsLeft: 2,
        fixedColumnsRight: 1

      },

      "iDisplayLength": 10,

      initComplete: function() {
        var api = this.api();

        api.columns('.hasFilter').every(function() {

          var column = this;
          var TH = $(column.header());
          var Filter_Select = TH.find('.filter_check');

          if (!Filter_Select.length) {

            Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo(TH);
            var unique_data = [];
            column.nodes().each(function(d, j) {
              var dataSearch = $(d).attr('data-search');

              if (jQuery.inArray(dataSearch, unique_data) === -1) {
                //console.log(d);
                Filter_Select.append('<option  value="' + dataSearch + '">' + dataSearch + '</option>');
                unique_data.push(dataSearch);
              }

            });
          }



          //$(document).on('change', Filter_Select, function(){
          Filter_Select.on('change', function(e) {

            var search = $(this).val();
            //  console.log( search );

            //add or remove thi select box value from filter section
            config_filter_section($(this));

            var class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);

            if (search.length)

              if (class_name == 'assignto') {
                search = ("\\b" + search + "\\b");
              }
            else {
              //  search= '^('+search.join('|') +')$'; 
              search = ("\\b" + search.join('|') + "\\b");
              // search=search.length > 0 ? '^(' + search + ')$' : '';
              //  search=     ("\\b" + search + "\\b"); 

            }


            var cur_column = api.column('.' + class_name + '_TH');

            //    console.log(class_name+'-'+search);



            cur_column.search(search, true, false).draw();
          });

          //console.log(select.attr('style'));
          if (this.visible()) {
            Filter_Select.formSelect();
          }

        });
        working_hours_timer();

      }
    });


    //   TaskTable_Instance.fnClearTable().fnDestroy();
    // //  $('#alltask').dataTable().fnDestroy();
    //  TaskTable_Instance= $('#alltask').dataTable();
    ColVis_Hide(TaskTable_Instance, hidden_coulmns);

    Filter_IconWrap();

    Change_Sorting_Event(TaskTable_Instance);

    ColReorder_Backend_Update(TaskTable_Instance, 'task_list');

    ColVis_Backend_Update(TaskTable_Instance, 'task_list');

    $('.filter-data.for-reportdash #clear_container').on('click', function() {
      Redraw_Table(TaskTable_Instance);
    });

    $(document).on('click', '.Settings_Button', AddClass_SettingPopup);
    $(document).on('click', '.Button_List', AddClass_SettingPopup);

    $(document).on('click', '.dt-button.close', function() {
      $('.dt-buttons').find('.dt-button-collection').detach();
      $('.dt-buttons').find('.dt-button-background').detach();
    });

    $(document).on('click', '.details-control', function() {

      //  alert("check");

      var tr = $(this).closest('tr');
      var row = TaskTable_Instance.row(tr);

      if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
      } else {
        var id = $(this).data('id');

        // alert(id);
        $.ajax({
          url: '<?php echo base_url(); ?>Sub_Task/sub_task_get',
          type: 'POST',
          data: {
            'id': id
          },
          beforeSend: function() {
            $(".LoadingImage").show();
          },
          success: function(data) {

            var parentTableThW = [];
            $('#alltask thead th').each(function() {
              parentTableThW.push($(this).outerWidth());
            });
            $(".dummy_content").html(data);
            $('.dummy_content').find('tr').each(function() {
              var i = 0;

              var sub_val = $(this).closest('.test_check').find('.subTaskcheckbox').data('alltask-id');

              vars['hours_' + sub_val] = $('#trhours_' + sub_val).val();
              vars['minutes_' + sub_val] = $('#trmin_' + sub_val).val();
              vars['seconds_' + sub_val] = $('#trsec_' + sub_val).val();
              vars['milliseconds_' + sub_val] = 0;
              vars['data_pause_' + sub_val] = $('#trpause_' + sub_val).val();

              $(this).find('td').each(function() {

                console.log(parentTableThW[i]);

                $(this).css({
                  width: parentTableThW[i]
                });
                i++;

              });
            });

            $(".LoadingImage").hide();
            row.child($(".dummy_content").html()).show();

            tr.addClass('shown');
            working_hours_timer(id);
          }

        });

        // Open this row

      }

    });



    var get_val = '';
    var Status_sel = '<li><div class="toolbar-search"><select id="statuswise_filter" class="statuswise_filter" multiple="true" data-searchCol="status_TH" placeholder="Select Status">';
    <?php foreach ($task_status as $key => $status_val) { ?>

      Status_sel += '<option value="<?php echo $status_val["status_name"] ?>">';
      get_val = "<?php echo $status_val["status_name"] ?>";
      Status_sel += get_val;
      Status_sel += '</option>';

    <?php } ?>


    Status_sel += '</select></div></li>';

    var Priority_sel = '<li><div class="toolbar-search"><select multiple="true" id="prioritywise_filter" class="prioritywise_filter" data-searchCol="priority_TH" placeholder="Select Priority"><option value="">By Priority</option><option value="low">Low</option><option value="medium">Medium</option><option value="high">High</option><option value="super_urgent">Super Urgent</option></select></div></li>';

    var Export_sel = '<li><div class="toolbar-search"><select id="export_report" class="export_report"><option value="">Export</option><option value="excel">Excel</option><option value="pdf">PDF</option><option value="html">HTML</option></select></div></li>';

    var Assignee_sel = '<li><div class="toolbar-search"><select multiple="true" data-searchCol="assignto_TH"  placeholder="Select Assignee" name="assignee" id="assignee" class="workers" ><?php if (count($staff_form)) { ?><option disabled>Staff</option><?php foreach ($staff_form as $s_key => $s_val) { ?><option value="<?php echo $s_val['crm_name']; ?>" ><?php echo $s_val['crm_name']; ?></option><?php }
                                                                                                                                                                                                                                                                                                                                                                                                    } ?></select></div></li>';


    var Billable_sel = '<li><div class="toolbar-search"><select multiple="true" data-searchCol="billable_TH" class="billable_filter" placeholder="Select Billable" > <option value="Billable">Billable</option><option value="Non Billable">Non Billable </option></select> </div> </li>';

    $("div.toolbar-table").prepend('<div class="filter-task1"><h2>Filter:</h2><ul>' + Status_sel + Priority_sel + Export_sel + Billable_sel + '</ul></div> ');

    /*this button for toggle settings*/
    /* $("div#table-buttons").prepend('<button class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cogs" aria-hidden="true"></i></button>');*/
    /*this button for toggle settings*/

    $('.toolbar-search').dropdown({
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });

    $(document).on('click', '.task_status_val', function() {

      var col = $.trim($(this).attr('data-searchCol'));
      var val = $.trim($(this).attr('data-id'));

      console.log(col + '-' + val);

      Trigger_To_Reset_Filter();


      MainStatus_Tab = val;

      TaskTable_Instance.column('.' + col).search(val, true, false).draw();
    });

    $('.toolbar-table select').on('change', function() {
      if ($(this).attr('id') != 'export_report') {
        var search = $(this).val();

        if (search.length) search = '^(' + search.join('|') + ')$';

        var col = $.trim($(this).attr('data-searchCol'));

        //console.log(col+"value"+val);

        TaskTable_Instance.column('.' + col).search(search, true, false).draw();

      }
    });

    function Trigger_To_Reset_Filter() {

      $('#select_alltask').prop('checked', false);
      $('#select_alltask').trigger('change');


      $('div.toolbar-table .toolbar-search li.dropdown-chose').each(function() {
        $(this).trigger('click');
      });


      MainStatus_Tab = "all_task";

      Redraw_Table(TaskTable_Instance);

      // Redraw_Table(TaskTable_Instance);

    }




  });
</script>
<script>
  $(document).ready(function() {

    $('#dropdown2').on('change', function() {
      // table.columns(5).search( this.value ).draw();
      var filterstatus = $(this).val();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>tasksummary/taskFilter",
        data: {
          filterstatus: filterstatus
        },
        success: function(response) {

          $(".all-usera1").html(response);

        },
        //async:false,
      });
    });



  });
</script>
<script>
  /*may be old required ment code*/

  $(document).on('change', '.test_status', function() {
    //$(".task_status").change(function(){
    var rec_id = $(this).data('id');
    var stat = $(this).val();
    $.ajax({
      url: '<?php echo base_url(); ?>user/test_statuschange/',
      type: 'post',
      data: {
        'rec_id': rec_id,
        'status': stat
      },
      timeout: 3000,

      success: function(data) {
        //alert('ggg');
        $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() {
          $('#status_succ');
        });

        //   setTimeout(resetAll,3000);
        /* if(stat=='3'){
                  
                   //$this.closest('td').next('td').html('Active');
                   $('#frozen'+rec_id).html('Frozen');
      
                } else {
                   $this.closest('td').next('td').html('Inactive');
                }*/
      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  });



  $(document).ready(function() {





    $(document).on('change', ".export_report", function() {
      //$("#prioritywise_filter").change(function(){
      var data = {};
      data['priority'] = $('.prioritywise_filter').val();
      data['status'] = $('.statuswise_filter').val();
      //data['status'] ='';
      // alert( data['status'] );
      data['d_type'] = $(this).val();
      file_download(data);
      return false;
    });




    function file_download(data) {
      var type = data.d_type;
      var status = data.status;
      var priority = data.priority;
      if (type == 'excel') {
        window.location.href = "<?php echo base_url() . 'user/task_excel?status="+status+"&priority="+priority+"' ?>";
      } else if (type == 'pdf') {
        window.location.href = "<?php echo base_url() . 'user/task_pdf?status="+status+"&priority="+priority+"' ?>";
      } else if (type == 'html') {
        window.open(
          "<?php echo base_url() . 'user/task_html?status="+status+"&priority="+priority+"' ?>",
          '_blank' // <- This is what makes it open in a new window.
        );
        // window.location.href="<?php echo base_url() . 'user/task_html?status="+status+"&priority="+priority+"' ?>";
      }
    }



  });


  $(document).on("click", ".assignuser_staff", function() {

    var id = $(this).attr("data-id");

    // $('.task_status_html').html('').html(reassign_status_poupup.format(id));
    // $('.adduserstatuspopup').modal('show');
    var data = {};
    $('.assign_staff_div').html('');

    data['task_id'] = id;


    data['assignuser_staff'] = $('#assignuser_staff_' + id).val();
    data['assignuser_staffid'] = $('#assignuser_staffid_' + id).val();
    data['type'] = $('#update_staffid_' + id).val();

    if ($('#assignuser_staff_' + id).val() != '' && data['type'] != '3') {




      $(".LoadingImage").show();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>user/assign_status_change/",
        data: data,
        success: function(response) {
          $('#assignuser_' + id + ' .close').trigger('click');

          $('#adduser_' + id + ' #assign_task_status_' + id).html('').html(response);
          $(".LoadingImage").hide();
        },
      });
    } else if (data['type'] == '3') {
      $(".LoadingImage").show();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>user/assign_status_change/",
        data: data,
        success: function(response) {
          $('#assignuser_' + id + ' .close').trigger('click');

          $('#adduser_' + id + ' #assign_task_status_' + id).html('').html(response);
          $(".LoadingImage").hide();
        },
      });
    } else {
      $('.assign_staff_div').html('Enter Status');
    }
  });

  $(document).on("click", ".assignuser_change", function(event) {
    $('.assign_staff_div').html('');

    var id = $(this).attr("data-id");


    $('.task_status_html').html('').html(reassign_status_poupup.format(id));
    $('.adduserstatuspopup').modal('show');


    $('#assignuser_staff_' + id).val('');
    $('#assignuser_staffid_' + id).val('');
    $('#update_staffid_' + id).val('');

    $('#assignuser_' + id + ' h4').text('Reason Status');
    $('.delete_st_reason').css('display', 'block');
    $('.delete_div_reason').css('display', 'none');
    $('#update_staffid_' + id).val('1');

  });

  $(document).on("click", ".edit_user_change", function(e) {
    $('.assign_staff_div').html('');

    var id = $(this).attr("data-id");
    $('.task_status_html').html('').html(reassign_status_poupup.format(id));
    $('.adduserstatuspopup').modal('show');
    var data = {};

    $('#assignuser_staff_' + id).val('');
    $('#assignuser_staffid_' + id).val('');
    $('#update_staffid_' + id).val('');

    var data_status = $('#assign_task_status_' + id).val();
    $('#assignuser_' + id + ' h4').text('Reason Status');
    $('.delete_st_reason').css('display', 'block');
    $('.delete_div_reason').css('display', 'none');
    var data_text = $('#assign_task_status_' + id + ' option:selected').text();



    if (data_status != '') {
      $('#assignuser_staff_' + id).val(data_text);
      $('#assignuser_staffid_' + id).val(data_status);
      $('#update_staffid_' + id).val('2');
    }



  });
  $(document).on("change", ".assign_task_status", function() {
    var id = $(this).attr('id');
    id = id.split('_');
    var val = $(this).val();
    if (val != "") {
      $('#edit_user_change_' + id[3]).css('display', 'block');
      $('#delete_user_change_' + id[3]).css('display', 'block');
      $(this).closest('.formconnew').addClass('add_edit_class');
    } else {
      $('#edit_user_change_' + id[3]).css('display', 'none');
      $('#delete_user_change_' + id[3]).css('display', 'none');
      $(this).closest('.formconnew').removeClass('add_edit_class');

    }


  });


  $(document).on("click", ".delete_user_change", function(e) {

    $('.assign_staff_div').html('');

    var id = $(this).attr("data-id");
    $('.task_status_html').html('').html(reassign_status_poupup.format(id));
    $('.adduserstatuspopup').modal('show');

    $('#assignuser_staffid_' + id).val('');
    $('#update_staffid_' + id).val('');

    var data = {};
    var data_status = $('#assign_task_status_' + id).val();
    // alert(data_status);
    $('.delete_st_reason').css('display', 'none');
    $('.delete_div_reason').css('display', 'block');
    $('#assignuser_' + id + ' h4').text('Do you Want Delete the status');
    var data_text = $('#assign_task_status_' + id + ' option:selected').text();


    if (data_status != '') {
      $('#assignuser_staffid_' + id).val(data_status);
      $('#update_staffid_' + id).val('3');
    }



  });

  $(document).on("click", ".save_assign_staff", function() {
    var id = $(this).attr("data-id");
    var data = {};
    var Assignees = [];
    // var countries =$("#workers"+id ).val();

    $('#adduser_' + id + ' .comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
      var id1 = $(this).closest('.comboTreeItemTitle').attr('data-id');
      var id2 = id1.split('_');
      Assignees.push(id2[0]);
    });
    //  data['assignee'] = Assignees

    var assign_role = Assignees.join(',');

    // alert(assign_role);
    data['task_id'] = id;
    data['assign_role'] = assign_role;
    data['assign_task_status'] = $('#assign_task_status_' + id).val();

    data['description'] = $('#description_' + id).val();

    $(".LoadingImage").show();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>user/update_assignees/",
      data: data,
      success: function(response) {
        // alert(response); die();
        $(".LoadingImage").hide();
        //$('#task_'+id).html(response);
        $('.task_' + id).html(response);
        $('.popup_info_msg').show();
        $(".popup_info_msg .position-alert1").html('Success !!! Staff Assign have been changed successfully...');
        setTimeout(function() {
          $('.popup_info_msg').hide();
        }, 1500);
      },
    });
  });




  $(function() {

    var hours = minutes = seconds = milliseconds = 0;
    var prev_hours = prev_minutes = prev_seconds = prev_milliseconds = undefined;
    var timeUpdate;

    // Start/Pause/Resume button onClick
    $("#start_pause_resume").button().click(function() {
      // Start button
      if ($(this).text() == "Start") { // check button label
        $(this).html("<span class='ui-button-text'>Pause</span>");
        updateTime(0, 0, 0, 0);
      }
      // Pause button
      else if ($(this).text() == "Pause") {
        clearInterval(timeUpdate);
        $(this).html("<span class='ui-button-text'>Resume</span>");
      }
      // Resume button    
      else if ($(this).text() == "Resume") {
        prev_hours = parseInt($("#hours").html());
        prev_minutes = parseInt($("#minutes").html());
        prev_seconds = parseInt($("#seconds").html());
        prev_milliseconds = parseInt($("#milliseconds").html());

        updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds);

        $(this).html("<span class='ui-button-text'>Pause</span>");
      }
    });

    // Reset button onClick subject
    $("#reset").button().click(function() {
      if (timeUpdate) clearInterval(timeUpdate);
      setStopwatch(0, 0, 0, 0);
      $("#start_pause_resume").html("<span class='ui-button-text'>Start</span>");
    });

    // Update time in stopwatch periodically - every 25ms
    function updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds) {
      var startTime = new Date(); // fetch current time

      timeUpdate = setInterval(function() {
        var timeElapsed = new Date().getTime() - startTime.getTime(); // calculate the time elapsed in milliseconds

        // calculate hours                
        hours = parseInt(timeElapsed / 1000 / 60 / 60) + prev_hours;

        // calculate minutes
        minutes = parseInt(timeElapsed / 1000 / 60) + prev_minutes;
        if (minutes > 60) minutes %= 60;

        // calculate seconds
        seconds = parseInt(timeElapsed / 1000) + prev_seconds;
        if (seconds > 60) seconds %= 60;

        // calculate milliseconds 
        milliseconds = timeElapsed + prev_milliseconds;
        if (milliseconds > 1000) milliseconds %= 1000;

        // set the stopwatch
        setStopwatch(hours, minutes, seconds, milliseconds);

      }, 25); // update time in stopwatch after every 25ms

    }

    // Set the time in stopwatch
    function setStopwatch(hours, minutes, seconds, milliseconds) {
      $("#hours").html(prependZero(hours, 2));
      $("#minutes").html(prependZero(minutes, 2));
      $("#seconds").html(prependZero(seconds, 2));
      $("#milliseconds").html(prependZero(milliseconds, 3));
    }

    // Prepend zeros to the digits in stopwatch
    function prependZero(time, length) {
      time = new String(time); // stringify time
      return new Array(Math.max(length - time.length + 1, 0)).join("0") + time;
    }
  });


  /*  function myFunction() {
     document.getElementById("myDropdown").classList.toggle("show");
    }
    */




  // Set the date we're counting down to
  var countDownDate = new Date("Sep 4, 2017 15:37:25").getTime();

  // Update the count down every 1 second
  var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    //alert(distance);
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    //document.getElementById("demo").innerHTML = days + "d " + hours + "h "
    var result = humanise(days) + hours + "h " +
      minutes + "m " + seconds + "s ";
    // console.log(result);
    $(".demos").html(result);



    // If the count down is over, write some text 
    if (distance < 0) {
      // alert('fff');
      //  clearInterval(x);
      $(".demos").html('EXPIRED');
      //document.getElementById("demo").innerHTML = "EXPIRED";
    }
  }, 1000);
</script>
<!--  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/> -->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.workout-timer.css" />
<!-- Example-page styles -->
<style>
  body {
    margin-bottom: 40px;
    background-color: #ECF0F1;
  }

  .event-log {
    width: 100%;
    margin: 20px 0;
  }

  .code-example {
    margin: 20px 0;
  }
</style>

<script>
  // var vars = {};
  //var time_count=0;
  function working_hours_timer(sub_task = null) {


    // test();
    $('.stopwatch').each(function() {
      //        $(document).on('each','.stopwatch',function(){

      // Cache very important elements, especially the ones used always
      //  var element = $(this);

      if (sub_task == null) {
        // console.log('s'); 
        var element = $(this);
      } else {
        // console.log('no'); 
        //  var element = $(this).closest('tr .test_check').find('.stopwatch');
        var subtask_id = $(this).closest('tr .test_check').find('.subTaskcheckbox').data('alltask-id');

        var element = $(this).closest('tr .subtask_check_' + sub_task + '_' + subtask_id).find('.stopwatch');
      }


      var running = element.data('autostart');
      var for_date = element.data('date');

      var task_id = element.data('id');

      var data_pause = element.data('pauseon');
      var data_pause = vars['data_pause_' + task_id];

      var hours = element.data('hour');
      var minutes = element.data('min');
      var seconds = element.data('sec');
      var milliseconds = element.data('mili');

      if (hours == '0') {
        var hours = vars['hours_' + task_id];
      }
      if (minutes == '0') {
        var minutes = vars['minutes_' + task_id];
      }
      if (seconds == '0') {
        var seconds = vars['seconds_' + task_id];

      }
      if (milliseconds == '0') { //alert("assig"+vars['milliseconds_'+task_id]);

        var milliseconds = vars['milliseconds_' + task_id];
      }

      element.closest('tr').find('td .hours-left').html('').html(parseInt(hours) + ':' + parseInt(minutes) + ' hours');

      // alert(milliseconds);
      var start = element.data('start');
      var end = element.data('end');

      var hoursElement = element.find('.hours');
      var minutesElement = element.find('.minutes');
      var secondsElement = element.find('.seconds');
      var millisecondsElement = element.find('.milliseconds');
      var toggleElement = element.find('.toggle');
      var resetElement = element.find('.reset');
      var pauseText = toggleElement.data('pausetext');
      var resumeText = toggleElement.data('resumetext');
      var startText = toggleElement.text();

      // And it's better to keep the state of time in variables 
      // than parsing them from the html.
      //var hours, minutes, seconds, milliseconds, timer;









      var timer;

      function prependZero(time, length) {

        //  alert('zzz');
        // Quick way to turn number to string is to prepend it with a string
        // Also, a quick way to turn floats to integers is to complement with 0
        time = '' + (time | 0);

        // And strings have length too. Prepend 0 until right.
        while (time.length < length) time = '0' + time;
        return time;

      }

      function setStopwatch(hours, minutes, seconds, milliseconds) {
        // Using text(). html() will construct HTML when it finds one, overhead.
        hoursElement.text(prependZero(hours, 2));
        minutesElement.text(prependZero(minutes, 2));
        secondsElement.text(prependZero(seconds, 2));
        millisecondsElement.text(prependZero(milliseconds, 3));
      }

      // Update time in stopwatch periodically - every 25ms
      function runTimer() {
        // Using ES5 Date.now() to get current timestamp            
        var startTime = Date.now();

        //  var startTime=1530000373832;

        //  //var startTime=1529926802;
        //  var startTime=new Date("2016/06/25 00:00:00");  
        // var startTime = new Date("June 23, 2018 15:37:25").getTime();
        if (hours != 0 && minutes != 0 && seconds != 0 && milliseconds != 0) {
          var startTime = Date.now();
        } else {
          // hours=$('#trhours_'+task_id).val();
          //  $minutes=$('#trmin_'+task_id).val();
          //  seconds=$('#trsec_'+task_id).val();
          //  milliseconds=$('#trmili_'+task_id).val();
          //var startTime = new Date("June 23, 2018 15:37:25").getTime();
          //            var startTime = new Date("2018/06/23 15:37:25").getTime();
          //var startTime = new Date(for_date).getTime();
          var startTime = Date.now();
        }





        // alert("before float"+milliseconds);

        var prevHours = parseFloat(hours);
        var prevMinutes = parseFloat(minutes);
        var prevSeconds = parseFloat(seconds);
        var prevMilliseconds = parseFloat(milliseconds);

        //alert("insidetimer"+prevMilliseconds);
        var tm = {};

        tm["hh_" + task_id] = tm["mm_" + task_id] = tm["ss_" + task_id] = tm["flag_" + task_id] = tm["flag1_" + task_id] = tm["flag2_" + task_id] = 00;
        //  tm["ss_" +task_id]= 60- prevSeconds;
        //console.log(tm["hh_" +task_id]);



        timer = setInterval(function() {

          var timeElapsed = Date.now() - startTime;



          if (tm["ss_" + task_id] == 60) {
            //  console.log('chk');
            tm["ss_" + task_id] = 00;

            if (tm["flag1_" + task_id] == 0) {
              tm["mm_" + task_id] = prevMinutes + 1;
              tm["flag1_" + task_id] = 1;
            } else {
              tm["mm_" + task_id] += 1;
            }

            // console.log(tm["mm" +task_id]);
            prevMinutes = 0;
            prevSeconds = 0;
          }

          if (tm["mm_" + task_id] == 60) {
            tm["mm_" + task_id] = 00;

            if (tm["flag2_" + task_id] == 0) {
              tm["hh_" + task_id] = prevHours + 1;
              tm["flag2_" + task_id] = 1;
            } else {
              tm["hh_" + task_id] += 1;
            }

            prevMinutes = 0;
            prevHours = 0;
          }

          hours = tm["hh_" + task_id] + prevHours;
          minutes = tm["mm_" + task_id] + prevMinutes;
          seconds = tm["ss_" + task_id] + prevSeconds;
          milliseconds = 0;

          // hours = (timeElapsed / 3600000) + prevHours;
          // minutes = ((timeElapsed / 60000) + prevMinutes) % 60;
          // seconds = ((timeElapsed / 1000) + prevSeconds) % 60;
          // milliseconds = (timeElapsed + prevMilliseconds) % 1000;


          //console.log(hours+"-"+minutes+'-'+seconds+'-'+milliseconds);
          // console.log(tm["mm_" +task_id]);



          element.attr('data-hour', hours);
          element.attr('data-min', minutes);
          element.attr('data-sec', seconds);
          element.attr('data-mili', milliseconds);
          element.attr('data-pauseon', '');

          vars['hours_' + task_id] = hours;
          vars['minutes_' + task_id] = minutes;
          vars['seconds_' + task_id] = seconds;
          vars['milliseconds_' + task_id] = milliseconds;
          vars['data_pause_' + task_id] = '';

          element.closest('tr').find('td .hours-left').html('').html(parseInt(hours) + ':' + parseInt(minutes) + ' hours');

          // $('#trpause_'+task_id).val('');

          // $('#trhours_'+task_id).val(hours);
          // $('#trmin_'+task_id).val(minutes);
          // $('#trsec_'+task_id).val(seconds);
          // $('#trmili_'+task_id).val(milliseconds);
          //  alert(hours+"h"+minutes+"m"+seconds+"s");
          setStopwatch(hours, minutes, seconds, milliseconds);

          if (tm["flag_" + task_id] == 0) {
            tm["ss_" + task_id] = prevSeconds;
            tm["flag_" + task_id] = 1;
            prevSeconds = 0;
          }

          tm["ss_" + task_id]++;
        }, 1000);
      }

      // Split out timer functions into functions.
      // Easier to read and write down responsibilities
      function run() {
        $(this).attr('pause', '');
        running = true;
        runTimer();
        if (start == '') {
          // $.ajax({
          //   url: '<?php echo base_url(); ?>user/task_countdown_update_start/',
          //   type: 'post',
          //   data: { 'task_id':task_id,'start':start },
          //   timeout: 3000,
          //   success: function( data ){
          //   //  alert('updated');
          //    toggleElement.attr('data-start','changed');
          //   }
          // });
        }
        toggleElement.text(pauseText);
        toggleElement.attr('data-current', pauseText);
      }

      function pause() {
        running = false;
        clearTimeout(timer);
        //   alert(toggleElement.data('pausetext'));
        toggleElement.text(resumeText);
        toggleElement.attr('data-current', resumeText);
        element.attr('data-pauseon', 'on');
        //  $('#trpause_'+task_id).val('on');
        vars['data_pause_' + task_id] = 'on';
        //  alert(parseInt(hours)+"--"+parseInt(minutes)+"--"+parseInt(seconds)+"--"+parseInt(milliseconds));
        $.ajax({
          url: '<?php echo base_url(); ?>user/task_countdown_update/',
          type: 'post',
          data: {
            'task_id': task_id,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds,
            'milliseconds': milliseconds,
            'pause': 'on'
          },
          timeout: 3000,
          success: function(data) {
            //  alert('updated');
          }
        });

      }

      function reset() {
        running = false;
        pause();
        hours = minutes = seconds = milliseconds = 0;
        setStopwatch(hours, minutes, seconds, milliseconds);
        toggleElement.text(startText);
        toggleElement.attr('data-current', startText);
      }
      //  And button handlers merely call out the responsibilities
      toggleElement.on('click', function() {
        if (running) {
          //  alert("true");
          pause();
        } else {
          // alert("false");
          run();
        }
      });

      resetElement.on('click', function() {
        reset();
      });

      demo();

      function demo() {
        running = false;

        //  hours =74; minutes=37;seconds =53; milliseconds = 764;

        element.attr('data-hour', hours);
        element.attr('data-min', minutes);
        element.attr('data-sec', seconds);
        element.attr('data-mili', milliseconds);

        vars['hours_' + task_id] = hours;
        vars['minutes_' + task_id] = minutes;
        vars['seconds_' + task_id] = seconds;
        vars['milliseconds_' + task_id] = milliseconds;


        //  $('#trhours_'+task_id).val(hours);
        // $('#trmin_'+task_id).val(minutes);
        // $('#trsec_'+task_id).val(seconds);
        // $('#trmili_'+task_id).val(milliseconds);

        // hours=$('#trhours_'+task_id).val();
        //           minutes=$('#trmin_'+task_id).val();
        //           seconds=$('#trsec_'+task_id).val();
        //           milliseconds=$('#trmili_'+task_id).val();
        if (hours != 0 || minutes != 0 || seconds != 0) {
          if (data_pause == '') { //alert("inside");

            run();
          }
        }
        //alert("after"+milliseconds);
        setStopwatch(hours, minutes, seconds, milliseconds);

      }




    });


  }
  // Init timers
  $(document).ready(function() {



    // <?php
        // foreach ($task_list as $tk_key => $tk_value) {
        //  
        ?>       
    //   vars['hours_'+<?php echo $tk_value['id']; ?> ]=$('#trhours_<?php echo $tk_value['id'] ?>').val();
    //   vars['minutes_'+<?php echo $tk_value['id']; ?>]=$('#trmin_<?php echo $tk_value['id'] ?>').val();
    //    vars['seconds_'+<?php echo $tk_value['id']; ?>]=$('#trsec_<?php echo $tk_value['id'] ?>').val();
    //     vars['milliseconds_'+<?php echo $tk_value['id']; ?>]=0;
    //      vars['data_pause_'+<?php echo $tk_value['id']; ?>]=$('#trpause_<?php echo $tk_value['id'] ?>').val();
    //  <?php
        // }
        // 
        ?> 

    // working_hours_timer();
    $(document).on('click', '.paginate_button', function() {
      working_hours_timer();
    });

  });
  /***************************************************/


  /*****************************************************/
  $(document).on('click', '.workout-timer__play-pause', function() {
    //$('.workout-timer').workoutTimer();
    var id = $(this).attr("data-id");
    var txt = $("#counter_" + id).html();
    var data = {};
    data['id'] = id;
    data['time'] = txt;
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>user/update_timer/",
      data: data,
      success: function(response) {
        // $('#task_'+id).html(response);
      },
    });
    /* alert(id);
     alert(txt);*/
  });



  /* var $eventLog = $('#event-log');
       var logEvent = function(eventType, eventTarget, timer) {
         var newLog = '"' + eventType + '" event triggered on #' + eventTarget.attr('id') + '\r\n';
         $eventLog.val( newLog.concat( $eventLog.val() ) );
       };
   
       $('.workout-timer-events').workoutTimer({
         onStart: logEvent,
         onRestart: logEvent,
         onPause: logEvent,
         onRoundComplete: logEvent,
         onComplete: logEvent
       });*/
</script>
<script>
  // Syntax highlighting
  $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });
</script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
  })();
</script>
<script type="text/javascript">
  $('#alltask thead').on('click', '#overall_tasks', function(e) {
    if ($(this).is(':checked')) {
      $(".assign_delete").show();
    } else {
      $(".assign_delete").hide();
    }
    $('#alltask .overall_tasks').not(this).prop('checked', this.checked);

  });
</script>
<script type="text/javascript">
  function delete_task(del) {
    $("#delete_task_id").val(JSON.stringify([$(del).data('id')]));
  }

  function delete_action() {

    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'user/alltasks_delete'; ?>",
      data: {
        'id': $("#delete_task_id").val()
      },
      beforeSend: function() {

        $(".LoadingImage").show();
      },
      success: function(data) {

        $(".LoadingImage").hide();

        $(".popup_info_msg").show();
        $('.popup_info_msg .position-alert1').html('Success !!! Task Successfully Deleted..');
        setTimeout(function() {
          $(".popup_info_msg").hide();
          location.reload();
        }, 1500);

        /*var task_ids = data.split(",");
        for (var i=0; i < task_ids.length; i++ ) { 
        $("#"+task_ids[i]).remove(); } 
        $(".alert-success-delete").show();
        setTimeout(function() { 
        }, 500);  */
      }
    });
  }







  $(document).ready(function() {
    //var table = $('#alltask').DataTable();           
    var tmp = [];
    $('#alltask tbody').on('click', '.overall_tasks', function(e) {
      $(".assign_delete").show();
      $(".overall_tasks").each(function() {
        if ($(this).is(':checked')) {

          var result = $(this).attr('id').split('_');
          var checked = result[2];
          tmp.push(checked);
        }
      });
      // alert(tmp);    

    });


    $(document).on('click', ".assign_tsk_btn", function() {
      //   alert('assign');
      var alltask = getSelectedRow();
      var Assignees = [];
      if (alltask.length <= 0) {
        $('.popup_info_msg').show();
        $(".popup_info_msg .position-alert1").html("Please Selecte Any Records..");
      } else {

        var selected_alltask_values = alltask.join(",");

        //alert('check');

        $('#assignTask-Popup .comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
          //  console.log('sss');
          var id1 = $(this).closest('.comboTreeItemTitle').attr('data-id');
          var id2 = id1.split('_');
          Assignees.push(id2[0]);
        });
        //  data['assignee'] = Assignees

        var assign_role = Assignees.join(',');
        // alert(assign_role);
        // return;

        var formData = {
          'task_id': selected_alltask_values,
          'assign_role': assign_role
        };
        // alert(selected_alltask_values);
        // return;

        $.ajax({
          type: "POST",
          url: "<?php echo base_url() . 'user/update_assignees1'; ?>",
          cache: false,
          data: formData,
          beforeSend: function() {
            $(".LoadingImage").show();
          },
          success: function(data) {
            $(".LoadingImage").hide();
            // var json = JSON.parse(data); 
            // status=json['status'];
            $(".popup_info_msg").show();
            $(".popup_info_msg .position-alert1").html(" Staff Assigned Successfully...");
            // return;
            setTimeout(function() {
              $(".popup_info_msg").hide();
              location.reload();
            }, 3000);


          }
        });


      }
    });


    $(document).on('click', "#assign_member", function() {
      //   alert('assign');
      var alltask = getSelectedRow();
      if (alltask.length <= 0) {
        $('.popup_info_msg').show();
        $(".popup_info_msg .position-alert1").html("Please Selecte Any Records..");
      } else {

        var selected_alltask_values = alltask.join(",");


        $(".assigned_staff").click(function() {
          var assign_to = $(".workers_assign").val();

          var formData = {
            'task_id': selected_alltask_values,
            'staff_id': assign_to
          };
          // alert(assign_to);
          $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'user/alltasks_assign'; ?>",
            cache: false,
            data: formData,
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data) {
              $(".LoadingImage").hide();
              var json = JSON.parse(data);
              status = json['status'];
              $(".popup_info_msg").show();
              $(".popup_info_msg .position-alert1").html(" Staff Assigned Successfully...");
              if (status == '1') {
                setTimeout(function() {
                  $(".popup_info_msg").hide();
                  location.reload();
                }, 3000);
              }
            }
          });

        });

      }
    });

    $("#saveSubtaskbutton").click(function() {

      var name = $("#addSubTask-Popup").find("#subTaskName").val();

      if (name == '') return;

      var selected = getSelectedRow();

      var ids = JSON.stringify(selected);

      $.ajax({
        url: "<?php echo base_url() ?>Task/addSubTask",
        type: "POST",
        data: {
          'name': name,
          'ids': ids
        },
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(res) {
          $(".LoadingImage").hide();

          console.log(selected);

          $.each(selected, function(i, v) {
            var data1 = {};

            var tr = $("#" + v);
            var row = TaskTable_Instance.row(tr);

            var toggleElement = tr.find('.stopwatch .toggle');
            data1['status'] = '2';
            data1['rec_id'] = v;
            tr.find(".task_status").val('2');

            var all_qCodes = $.ajax({
              type: "POST",
              data: data1,
              dataType: "json",
              url: "<?= base_url() ?>user/task_statusChange",
              async: false
            }).responseJSON;

            if (toggleElement.hasClass('time_pause1')) {
              toggleElement.trigger('click');
              // data1['change_id']='1';
            }



            if (row.child.isShown()) {
              row.child.hide();
              tr.removeClass('shown');
            } else {
              tr.find("td:nth-child(1)").addClass("details-control");
            }

          });
          $("#addSubTask-Popup").modal('hide');
        }
      });







    });

    $(document).on('change', ".subTaskcheckbox", function() {
      var con = $(this).is(":checked");
      var sel = $(this).closest('tr').find('select.task_status');
      var strikeout = $(this).closest('tr').find('td.subject_td');

      if (con) {
        sel.val('5');
        sel.trigger('change');
        strikeout.addClass('strikeout');
      } else {

        sel.val('2');

        sel.trigger('change');
        strikeout.removeClass('strikeout');

      }
    });

    //  $("#delete_task").click(function(){
    //    $('#delete_user'+tmp).show();
    //       $('.delete_yes').click(function() {
    //          var formData={'id':tmp};
    //       $.ajax({

    //         url: '<?php echo base_url(); ?>user/tasks_delete',
    //         type : 'POST',        
    //         data : formData,
    //         beforeSend: function() {
    //           $(".LoadingImage").show();
    //         },
    //         success: function(data) {
    //         // alert(data);
    //            $(".LoadingImage").hide();
    //           location.reload();
    //         }
    //    });
    //         }); 

    // }); 


  });

  $(".overall_tasks").each(function() {
    if ($(this).is(':checked')) {

      var result = $(this).attr('id').split('_');
      var checked = result[2];
      tmp.push(checked);
    }
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {


    $(document).on('click', '#delete_task', function() {

      var alltask = getSelectedRow();
      //alert(JSON.stringify(alltask));
      //$("#delete_task_id").attr("value",JSON.stringify(alltask));
      $("#delete_task_id").val(JSON.stringify(alltask));

    });

  });






  $(document).ready(function() {



    var date = $('.datepicker').datepicker({
      dateFormat: 'dd-mm-yy'
    }).val();

    // Multiple swithces
    var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));

    elem.forEach(function(html) {
      var switchery = new Switchery(html, {
        color: '#1abc9c',
        jackColor: '#fff',
        size: 'small'
      });
    });

    $('#accordion_close').on('click', function() {
      $('#accordion').slideToggle(300);
      $(this).toggleClass('accordion_down');
    });



  });
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->

<script type="text/javascript">
  $(document).ready(function() {
    $("#country").change(function() {
      var country_id = $(this).val();
      //alert(country_id);

      $.ajax({

        url: "<?php echo base_url() . 'Client/state'; ?>",
        data: {
          "country_id": country_id
        },
        type: "POST",
        success: function(data) {
          //alert('hi');
          $("#state").append(data);

        }

      });
    });
    $("#state").change(function() {
      var state_id = $(this).val();
      //alert(country_id);

      $.ajax({

        url: "<?php echo base_url() . 'Client/city'; ?>",
        data: {
          "state_id": state_id
        },
        type: "POST",
        success: function(data) {
          //alert('hi');
          $("#city").append(data);

        }

      });
    });







  });

  $(document).ready(function() {





    <?php
    if (isset($_SESSION['firm_seen'])) {
      if ($_SESSION['firm_seen'] == '2') { ?>
        $("div.nots_task_2").trigger('click');
      <?php } ?>

      <?php
      if ($_SESSION['firm_seen'] == '1') { ?>
        $("div.nots_task_1").trigger('click');
      <?php } ?>


      <?php
      if ($_SESSION['firm_seen'] == '5') { ?>
        $("div.nots_task_5").trigger('click');
      <?php } ?>

      <?php
      if ($_SESSION['firm_seen'] == 'low') { ?>
        $("#prioritywise_filter").val('low').trigger('change');
      <?php } ?>

      <?php
      if ($_SESSION['firm_seen'] == 'medium') { ?>
        $("#prioritywise_filter").val('medium').trigger('change');
      <?php } ?>

      <?php
      if ($_SESSION['firm_seen'] == 'high') { ?>
        $("#prioritywise_filter").val('high').trigger('change');
      <?php } ?>

      <?php
      if ($_SESSION['firm_seen'] == 'super_urgent') { ?>
        $("#prioritywise_filter").val('super_urgent').trigger('change');
      <?php } ?>


    <?php } ?>
  });
</script>
<!-- for new timer -->
<script type="text/javascript">
  $(document).on('click', '.for_timer_start_pause', function() {

    var toggleElement = $(this).closest('tr').find('.task_status');

    var task_id = $(this).attr('id');
    //var task_id=$(this).parent().attr("id");
    $.ajax({
      url: '<?php echo base_url(); ?>user/task_timer_start_pause/',
      type: 'post',
      data: {
        'task_id': task_id
      },
      timeout: 3000,
      success: function(data) {
        //  alert('updated');
      }
    });
    var data1 = {};
    if (toggleElement.val() == '5') {
      var user_type = "<?php echo $_SESSION['user_type'] ?>";
      if (user_type == 'FA') {
        data1['change_id'] = 1;
      }
      data1['status'] = toggleElement.val();
    }
    data1['task_id'] = task_id;

    $.ajax({
      url: '<?php echo base_url(); ?>user/task_timer_start_pause_individual/',
      type: 'post',
      data: data1,
      timeout: 3000,
      success: function(data) {
        //  alert('updated');
      }
    });
  });
</script>
<!-- end of new timer -->
<!-- for customize permission style -->
<script type="text/javascript">
  $(document).on('change', '.task_status', function(e) {

    var toggleElement1 = $(this).closest('tr').find('.stopwatch');

    var rec_id = $(this).data('id');
    var data1 = {};
    var stat = $.trim($(this).val());

    if (stat == '5') {
      var sub_check = toggleElement1.attr('data-subid');
      // if( $(this).closest('tr').hasClass('shown')){
      //       $(this).closest('tr').find('.details-control').trigger('click');
      //    }
      if (sub_check != 0) {
        var res = sub_check.split(",");
        $.each(res, function(key, val) {

          var toggleElement = $('tr .subtask_check_' + rec_id + '_' + val).find('.stopwatch .toggle');

          if (toggleElement.hasClass('time_pause1')) {
            var url1 = "<?php echo base_url(); ?>user/task_timer_start_pause_individual";
            toggleElement.trigger('click');

          } else {
            var url1 = "";
          }
          //else{
          data1['task_id'] = val;
          data1['status'] = '5';
          var user_type = "<?php echo $_SESSION['user_type'] ?>";
          if (user_type == 'FA') {
            data1['change_id'] = 1;
          }
          var all_qCodes = $.ajax({
            type: "POST",
            data: data1,
            dataType: "json",
            url: url1,
            async: false
          }).responseJSON;
          // }

        });

      }
    }

    var toggleElement = $(this).closest('tr').find('.stopwatch .toggle');
    if (stat == '5' && toggleElement.hasClass('time_pause1')) {

      toggleElement.trigger('click');

    } else {
      data1['rec_id'] = rec_id;
      data1['status'] = stat;

      var that = $(this);
      if (stat == '') return;
      $.ajax({
        url: '<?php echo base_url(); ?>user/task_statusChange/',
        type: 'post',
        data: data1,

        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {

          $(".LoadingImage").hide();
          if (data == 2) {
            $(".popup_info_msg").show();
            $('.popup_info_msg .position-alert1').html('You are has been remove from this task successfully...');
            setTimeout(function() {
              $(".popup_info_msg").hide();
            }, 1500);
            location.reload();
          } else {

            $(".popup_info_msg").show();
            $('.popup_info_msg .position-alert1').html('Success !!! Task status have been changed successfully...');

            var status_condition = '<?php echo $status_condition ?>';
            if (status_condition == 0) {
              $('.taskpagedashboard').load("<?php echo base_url(); ?>user/task_summary_data");

              var task_status_arr = '<?php echo json_encode($task_status) ?>';

              // $.each(JSON.parse(task_status_arr), function(item,i){
              //      console.log(i.id);
              //   });

              var idx = $.map(JSON.parse(task_status_arr), function(item, i) {
                if (item.id == stat)
                  return item.status_name;
              })[0];
              //console.log(idx);

              //console.log(task_status_arr);

              var cell = that.closest('td');
              cell.attr('data-search', idx);
            }

            TaskTable_Instance.cell(cell).invalidate().draw();



            setTimeout(function() {
              $(".popup_info_msg").hide();
            }, 1500);

            /* Add or Remove Archive / completed Link in menu */
            var recent_array = [];

            $(".count_section").find("td .task_status").each(function() {

              recent_array.push($(this).val());

            })

            var need_url = '<?php echo base_url() ?>user/task_list';
            var archive_count = 0;
            var complete_count = 0;
            $('.dropdown-menu1 li').each(function() {

              if ($(this).hasClass('archive_li'))
                archive_count = 1;


              if ($(this).hasClass('completed_li'))
                complete_count = 1;

            });

            $('.dropdown-menu1 li').each(function() {

              if (jQuery.inArray('4', recent_array) !== -1) {
                if (!archive_count && $(this).find('a').attr('href') == need_url) {

                  $(this).closest('ul').append('<li class="column-setting0 atleta archive_li"><a href="<?php echo base_url() ?>user/task_list/archive_task">Archive Task</a>   </li>');
                }
              } else {
                if (archive_count == 1) {
                  $('.archive_li').remove();
                }

              }


              if (jQuery.inArray('5', recent_array) !== -1) {
                if (!complete_count && $(this).find('a').attr('href') == need_url) {
                  $(this).closest('ul').append('<li class="column-setting0 atleta completed_li"><a href="<?php echo base_url() ?>user/task_list/complete_task">Completed Task</a>   </li>');
                }
              } else {
                if (complete_count == 1) {
                  $('.completed_li').remove();
                }

              }

            });




          }
        },
        error: function(errorThrown) {
          console.log(errorThrown);
        }
      });
    }
  });




  $(document).on('change', '.progress_status', function(e) {

    var rec_id = $(this).data('id');
    var data1 = {};
    var stat = $.trim($(this).val());

    data1['rec_id'] = rec_id;
    data1['status'] = stat;

    var that = $(this);
    if (stat == '') return;
    $.ajax({
      url: '<?php echo base_url(); ?>user/progress_statusChange/',
      type: 'post',
      data: data1,

      beforeSend: function() {

        $(".LoadingImage").show();
      },
      success: function(data) {

        $(".LoadingImage").hide();
        $(".popup_info_msg").show();
        $('.popup_info_msg .position-alert1').html('Success !!! Process Task status have been changed successfully...');
        // $('.taskpagedashboard').load("<?php echo base_url(); ?>user/task_summary_data");

        var cell = that.closest('td');
        cell.attr('data-search', stat);
        setTimeout(function() {
          $(".popup_info_msg").hide();
        }, 1500);


      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });

  });




  $(document).on('change', '.task_priority', function() {

    var rec_id = $(this).data('id');
    var priority = $.trim($(this).val());
    var that = $(this);
    if (priority == '') return;
    $.ajax({
      url: '<?php echo base_url(); ?>user/task_priorityChange/',
      type: 'post',
      data: {
        'rec_id': rec_id,
        'priority': priority
      },
      timeout: 3000,
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {

        $(".LoadingImage").hide();
        $(".popup_info_msg").show();
        $('.popup_info_msg .position-alert1').html('Success !!! Task Priority have been changed successfully...');
        setTimeout(function() {
          $(".popup_info_msg").hide();

        }, 1500);

        var cell = that.closest('td');
        cell.attr('data-search', priority);
        TaskTable_Instance.cell(cell).invalidate().draw();

      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });

  });



  $('.suspent').click(function() {
    if ($(this).is(':checked'))
      var stat = '1';
    else
      var stat = '0';
    var rec_id = $(this).val();
    var $this = $(this);
    $.ajax({
      url: '<?php echo base_url(); ?>user/suspentChange/',
      type: 'post',
      data: {
        'rec_id': rec_id,
        'status': stat
      },
      success: function(data) {
        //alert('ggg');
        $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
        if (stat == '1') {

          $this.closest('td').next('td').html('Payment');

        } else {
          $this.closest('td').next('td').html('Non payment');
        }
      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  });



  $('#alluser').on('change', '.status', function() {
    //e.preventDefault();  
    var rec_id = $(this).data('id');
    var stat = $(this).val();
    $.ajax({
      url: '<?php echo base_url(); ?>user/statusChange/',
      type: 'post',
      data: {
        'rec_id': rec_id,
        'status': stat
      },
      timeout: 3000,
      success: function(data) {
        //alert('ggg');
        $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() {
          $('#status_succ');
        });
        // setTimeout(resetAll,3000);
        if (stat == '3') {

          //$this.closest('td').next('td').html('Active');
          $('#frozen' + rec_id).html('Frozen');

        } else {
          $this.closest('td').next('td').html('Inactive');
        }
      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  });





  $(document).on("change", "#select_alltask", function(event) {

    var table = $(this).attr("id").replace("select_", "");
    var checked = this.checked;
    var count = 0;
    var task_id = 0;
    var user_type = "<?php echo $_SESSION['user_type'] ?>";

    $(".LoadingImage").show();

    TaskTable_Instance.column(1, {
        search: 'applied'
      })
      .nodes()
      .to$().each(function() {


        $(this).find('.alltask_checkbox').prop('checked', checked);

        console.log(MainStatus_Tab);

        if (checked) {
          $(this).find('.alltask_checkbox').prop('checked', true);


          if (MainStatus_Tab == "Archive") $("#unarchive_task").show();
          else $('#archive_task').show();
          $("#assign_member").show();
          // $("#btn_review_send").show();

          $("#delete_task").show();
          $("#addSubtask-Btn").show();
          $('.task_assign_btn').show();


        } else {

          $(this).find('.alltask_checkbox').prop('checked', false);

          if (MainStatus_Tab == "Archive") $('#unarchive_task').hide();
          else $("#archive_task").hide();
          $("#addSubtask-Btn").hide();
          $('.task_assign_btn').hide();
          $("#assign_member").hide();
          $("#delete_task").hide();
          // $("#btn_review_send").hide();       
        }



      });
    $(".LoadingImage").hide();
    //$("#select_"+table+"_count").val($("input."+table+"_checkbox:checked").length + " Selected");
  });

  $(document).on("change", ".alltask_checkbox", function(event) {
    var count = 0;
    var tr = 0;
    var ck_tr = 0;
    var task_id = 0;
    var user_type = "<?php echo $_SESSION['user_type'] ?>";
    TaskTable_Instance.column(1).nodes().to$().each(function(index) {

      if ($(this).find(".alltask_checkbox").is(":checked")) {
        ck_tr++;

        var status_val = '';
        var table_id = $(this).closest('tr');
        status_val = table_id.find(".task_status").val();

      }


      tr++;

    });
    //  alert("re"+$("#alltask").attr("data-status")+ck_tr);
    if (tr == ck_tr) {
      $("#select_alltask").prop("checked", true);
    } else {
      $("#select_alltask").prop("checked", false);
    }
    if (ck_tr) {
      // alert(user_type);
      if (MainStatus_Tab == "Archive") $("#unarchive_task").show();
      else $('#archive_task').show();
      $("#assign_member").show();
      $("#addSubtask-Btn").show();
      $('.task_assign_btn').show();
      $("#delete_task").show();
      // if(count==0 && user_type!='FA'){
      //   // alert('hai');
      //  $("#btn_review_send").show();
      // }
      // else
      // {
      //   $("#btn_review_send").hide();
      // }
    } else {
      if (MainStatus_Tab == "Archive") $('#unarchive_task').hide();
      else $("#archive_task").hide();
      $("#assign_member").hide();
      $("#addSubtask-Btn").hide();
      $('.task_assign_btn').hide();
      $("#delete_task").hide();
      //$("#btn_review_send").hide();
    }

    $("#select_alltask_count").val($("input.alltask_checkbox:checked").length + " Selected");

  });

  $("#close_action_result").click(function() {
    $(".popup_info_msg").hide();
  }); //for close mssage box
  $("#close_info_msg").click(function() {
    $(".popup_info_msg").hide();
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {

    var fruits = [];

    $('.data_active_task th').each(function() {
      fruits.push($(this).outerWidth());
      //console.log(fruits);
    });


    $(document).on("click", ".controls .for_timer_start_pause", function() {
      $(this).toggleClass('time_pause1')
    });

  });

  function getSelectedRow() {
    var alltask = [];

    TaskTable_Instance.column(1).nodes().to$().each(function(index) {

      if ($(this).find(".alltask_checkbox").is(":checked")) {
        alltask.push($(this).find(".alltask_checkbox").attr('data-alltask-id'));
      }

    });

    return alltask;

  }
</script>

<script src="<?php echo base_url(); ?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url(); ?>assets/tree_select/icontains.js"></script>
<script type="text/javascript">
  //string format function -- use 'hello {0}'.format('demo')  -> result : 'hello demo'
  String.prototype.format = String.prototype.f = function() {

    var s = this,
      i = arguments.length;

    console.log(arguments);

    while (i--) {
      s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
  };




  $(document).on("click", ".user_change_rejected", function(event) {

    var id = $(this).data('target');
    id = id.split('_');
    id = id[1];
    var main_id = id;
    var task_name = 'TASK';
    var Assignees = [];

    // $('.adduserpopup').modal('show');

    var user_popup = $('.adduserpopup');
    // user_popup.modal('hide');

    //  $('.modal-backdrop .fade .in').removeClass('modal-backdrop fade in');
    // $( /\{0}/g ).replaceAll( main_id );




    if (id != '') {
      //user_popup.modal('show');
      user_popup.attr('id', 'adduser_' + main_id);

      $('#adduser_' + main_id).modal('show');


      user_popup.find('.modal-body select').attr('id', 'assign_task_status_' + main_id);
      user_popup.find('.modal-footer .save_assign_staff').attr('data-id', main_id);


      var popup_anchor_tag = ' <a href="javascript:;" id="" data-id="{0}"  class="assignuser_change "><i class="fa fa-plus"></i></a><a style="display: none" href="javascript:;" id="edit_user_change_{0}" data-id="{0}"  class="edit_user_change "><i class="fa fa-pencil"></i></a><a style="display: none" href="javascript:;" id="delete_user_change_{0}" data-id="{0}"  class="delete_user_change "><i class="fa fa-trash"></i></a>';
      $('.atagsection').html('').html(popup_anchor_tag.format(main_id));

      $.ajax({
        url: "<?php echo base_url() ?>User/get_assigness",
        type: "POST",
        data: {
          "id": id,
          "task_name": task_name
        },
        dataType: "json",
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          if (data == 2) {
            $(".popup_info_msg").show();
            $('.popup_info_msg .position-alert1').html('You are has been remove from this task successfully...');
            setTimeout(function() {
              $(".popup_info_msg").hide();
            }, 1500);
            location.reload();
          } else {
            $(".LoadingImage").hide();


            $('#adduser_' + main_id + ' .comboTreeItemTitle').click(function() {

              Assignees = [];

              var id = $(this).attr('data-id');
              var id = id.split('_');

              $('#adduser_' + main_id + ' .comboTreeItemTitle').each(function() {
                var id1 = $(this).attr('data-id');
                var id1 = id1.split('_');
                //console.log(id[1]+"=="+id1[1]);
                if (id[1] == id1[1]) {
                  $(this).toggleClass('disabled');
                }


              });
              $(this).removeClass('disabled');

              $('#adduser_' + main_id + ' .comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
                var id = $(this).closest('#adduser_' + main_id + ' .comboTreeItemTitle').attr('data-id');
                var id = id.split('_');
                Assignees.push(id[0]);
              });

              var assign_role = Assignees.join(',');
              $('#adduser_txt_' + main_id).val(assign_role);

            });





            var arr1 = data.asssign_group;
            var assign_check = data.assign_check;
            //alert(assign_check);
            var unique = arr1.filter(function(itm, i, arr1) {
              return i == arr1.indexOf(itm);
            });

            $('#adduser_' + main_id + ' .comboTreeItemTitle').each(function() {

              $(this).find("input:checked").trigger('click');

              var id1 = $(this).attr('data-id');
              var id = id1.split('_');
              if (jQuery.inArray(id[0], unique) !== -1) {
                $(this).find("input").trigger('click');
                // if(assign_check==0)
                //  {
                //     $(this).toggleClass('disabled');
                //  }
                Assignees.push(id[0]);
              }

            });







            var assign_role = Assignees.join(',');
            $('#adduser_txt_' + main_id).val(assign_role);
            //alert(Assignees);

          }
        }
      });

    }
  });




  function prependZero(time, length) {
    time = new String(time); // stringify time
    return new Array(Math.max(length - time.length + 1, 0)).join("0") + time;
  }
  setInterval(function() {
    $.ajax({
      url: '<?php echo base_url(); ?>user/main_task_timer/',
      type: 'post',
      dataType: "json",
      success: function(data) {
        //  var result= $.parseJSON(data);
        //  alert('updated');
        let time_val = '';
        let element = '';
        let hoursElement = '';
        let secondsElement = '';
        let millisecondsElement = '';
        $.each(data, function(i, v) {

          time_val = v.split(',');
          // console.log(time_val[3]);

          element = $('#' + time_val[3]).find('.stopwatch');
          //  console.log(element.data('subid'));

          if (element.data('subid') != 0) {

            element.attr('data-hour', time_val[0]);
            element.attr('data-min', time_val[1]);
            element.attr('data-sec', time_val[2]);

            element.attr('data-pauseon', '');

            hoursElement = element.find('.hours');
            minutesElement = element.find('.minutes');
            secondsElement = element.find('.seconds');
            millisecondsElement = element.find('.milliseconds');


            hoursElement.text(prependZero(time_val[0], 2));
            minutesElement.text(prependZero(time_val[1], 2));
            secondsElement.text(prependZero(time_val[2], 2));
            millisecondsElement.text(prependZero(milliseconds, 3));

          }
        });
        //console.log(result);
      }
    });
  //}, 10000);
}, 60000);


  $(document).on('click', ".missing_info, .misclose", function() {
    console.log('ok');
    $(this).closest('tr').find('.missing_details').toggleClass('show');
  });
  $(document).mouseup(function(e) {
    var container = $(".missing_details.show");
    // if the target of the click isn't the container nor a descendant of the container
    if (container.length && !container.is(e.target) && container.has(e.target).length === 0) {
      $('.missing_details').removeClass('show');
    }

  });
</script>


<?php $this->load->view('tasks/import_task'); ?>