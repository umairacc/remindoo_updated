<!DOCTYPE html>
<html lang="en">

<head>
    <title>GMAIL API</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?=base_url()?>assets/gmail_assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="<?=base_url()?>assets/Gfont.css">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/gmail_assets/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/gmail_assets/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/gmail_assets/assets/icon/icofont/css/icofont.css">
    <!-- flag icon framework css -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/gmail_assets/assets/pages/flag-icon/flag-icon.min.css">
    <!-- Menu-Search css -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/gmail_assets/assets/pages/menu-search/css/component.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/gmail_assets/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/gmail_assets/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/custom/opt-profile.css?<?php echo time(); ?>">
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->

    <div id="pcoded" class="pcoded iscollapsed" vertical-nav-type="offcanvas">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">

                    <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                        <i class="ti-menu"></i>
                    </a>
                        <a class="mobile-search morphsearch-search" href="#">
                        <i class="ti-search"></i>
                    </a>
                        <a href="mail/index">
                        <img class="img-fluid" src="" alt="Theme-Logo" />
                    </a>
                        <a class="mobile-options">
                        <i class="ti-more"></i>
                    </a>
                    </div>

                    <div class="navbar-container container-fluid">
                   
                        <ul class="nav-right">
                         
                            <li class="header-notification">
                                <a href="#!">
                                <i class="ti-bell"></i>
                                <span class="badge bg-c-pink"></span>
                            </a>
                                <ul class="show-notification">
                                    <li>
                                        <h6>Notifications</h6>
                                        <label class="label label-danger">New</label>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center" src="<?=base_url()?>assets/gmail_assets/assets/images/user.png" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">John Doe</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center" src="<?=base_url()?>assets/gmail_assets/assets/images/user.png" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">Joseph William</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <img class="d-flex align-self-center" src="<?=base_url()?>assets/gmail_assets/assets/images/user.png" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">Sara Soudein</h5>
                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                <span class="notification-time">30 minutes ago</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="header-notification">
                                <a href="#!" class="displayChatbox">
                                <i class="ti-comments"></i>
                                <span class="badge bg-c-green"></span>
                            </a>
                            </li>
                            <li class="user-profile header-notification">
                                <a href="#!">
                                <img src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-4.jpg" class="img-radius" alt="User-Profile-Image">
                                <span>John Doe</span>
                                <i class="ti-angle-down"></i>
                            </a>
                                <ul class="tgl-opt">
                                    <li>
                                        <a href="#!">
                                        <i class="ti-settings"></i> Settings
                                    </a>
                                    </li>
                                    <li>
                                        <a href="user-profile.html">
                                        <i class="ti-user"></i> Profile
                                    </a>
                                    </li>
                                    <li>
                                        <a href="email-inbox.html">
                                        <i class="ti-email"></i> My Messages
                                    </a>
                                    </li>
                                    <li>
                                        <a href="auth-lock-screen.html">
                                        <i class="ti-lock"></i> Lock Screen
                                    </a>
                                    </li>
                                    <li>
                                        <a href="auth-normal-sign-in.html">
                                        <i class="ti-layout-sidebar-left"></i> Logout
                                    </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- search -->
                      <!--   <div id="morphsearch" class="morphsearch">
                            <form class="morphsearch-form">
                                <input class="morphsearch-input" type="search" placeholder="Search..." />
                                <button class="morphsearch-submit" type="submit">Search</button>
                            </form>
                            <div class="morphsearch-content">
                                <div class="dummy-column">
                                    <h2>People</h2>
                                    <a class="dummy-media-object" href="#!">
                                    <img src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-1.jpg" alt="Sara Soueidan" />
                                    <h3>Sara Soueidan</h3>
                                </a>
                                    <a class="dummy-media-object" href="#!">
                                    <img src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-2.jpg" alt="Shaun Dona" />
                                    <h3>Shaun Dona</h3>
                                </a>
                                </div>
                                <div class="dummy-column">
                                    <h2>Popular</h2>
                                    <a class="dummy-media-object" href="#!">
                                    <img src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-3.jpg" alt="PagePreloadingEffect" />
                                    <h3>Page Preloading Effect</h3>
                                </a>
                                    <a class="dummy-media-object" href="#!">
                                    <img src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-4.jpg" alt="DraggableDualViewSlideshow" />
                                    <h3>Draggable Dual-View Slideshow</h3>
                                </a>
                                </div>
                                <div class="dummy-column">
                                    <h2>Recent</h2>
                                    <a class="dummy-media-object" href="#!">
                                    <img src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-5.jpg" alt="TooltipStylesInspiration" />
                                    <h3>Tooltip Styles Inspiration</h3>
                                </a>
                                    <a class="dummy-media-object" href="#!">
                                    <img src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-6.jpg" alt="NotificationStyles" />
                                    <h3>Notification Styles Inspiration</h3>
                                </a>
                                </div>
                            </div> 
                            <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>
                        </div>
                       search end -->
                    </div>
                </div>
            </nav>

            <!-- Sidebar chat start -->
            <div id="sidebar" class="users p-chat-user showChat">
                <div class="had-container">
                    <div class="card card_main p-fixed users-main">
                        <div class="user-box">
                            <div class="card-block">
                                <div class="right-icon-control">
                                    <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends">
                                    <div class="form-icon">
                                        <i class="icofont icofont-search"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="main-friend-list">
                                <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-1.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Josephin Doe</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left" title="Lary Doe">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-2.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Lary Doe</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>gmail_assets/gmail_assets/images/avatar-3.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Alice</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left" title="Alia">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Alia</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left" title="Suzen">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-5.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Suzen</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="6" data-status="offline" data-username="Michael Scofield" data-toggle="tooltip" data-placement="left" title="Michael Scofield">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-6.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-danger"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Michael Scofield</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="7" data-status="online" data-username="Irina Shayk" data-toggle="tooltip" data-placement="left" title="Irina Shayk">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-7.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Irina Shayk</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="8" data-status="offline" data-username="Sara Tancredi" data-toggle="tooltip" data-placement="left" title="Sara Tancredi">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-1.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-danger"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Sara Tancredi</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="9" data-status="online" data-username="Samon" data-toggle="tooltip" data-placement="left" title="Samon">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-2.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Samon</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="10" data-status="online" data-username="Daizy Mendize" data-toggle="tooltip" data-placement="left" title="Daizy Mendize">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-3.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Daizy Mendize</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="11" data-status="offline" data-username="Loren Scofield" data-toggle="tooltip" data-placement="left" title="Loren Scofield">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-danger"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Loren Scofield</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="12" data-status="online" data-username="Shayk" data-toggle="tooltip" data-placement="left" title="Shayk">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-5.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Shayk</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="13" data-status="offline" data-username="Sara" data-toggle="tooltip" data-placement="left" title="Sara">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-6.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-danger"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Sara</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="14" data-status="online" data-username="Doe" data-toggle="tooltip" data-placement="left" title="Doe">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-7.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Doe</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="15" data-status="online" data-username="Lary" data-toggle="tooltip" data-placement="left" title="Lary">
                                    <a class="media-left" href="#!">
                                    <img class="media-object" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-1.jpg" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Lary</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Sidebar inner chat start-->
            <div class="showChat_inner">
                <div class="media chat-inner-header">
                    <a class="back_chatBox">
                    <i class="icofont icofont-rounded-left"></i> Josephin Doe
                </a>
                </div>
                <div class="media chat-messages">
                    <a class="media-left photo-table" href="#!">
                    <img class="media-object img-radius m-t-5" src="<?=base_url()?>assets/gmail_assets/assets/images/avatar-1.jpg" alt="Generic placeholder image">
                </a>
                    <div class="media-body chat-menu-content">
                        <div class="">
                            <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                            <p class="chat-time">8:20 a.m.</p>
                        </div>
                    </div>
                </div>
                <div class="media chat-messages">
                    <div class="media-body chat-menu-reply">
                        <div class="">
                            <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                            <p class="chat-time">8:20 a.m.</p>
                        </div>
                    </div>
                    <div class="media-right photo-table">
                        <a href="#!">
                        <img class="media-object img-radius m-t-5" src="<?=base_url()?>assets/gmail_assets/assets/images/user.png" alt="Generic placeholder image">
                    </a>
                    </div>
                </div>
                <div class="chat-reply-box p-b-20">
                    <div class="right-icon-control">
                        <input type="text" class="form-control search-text" placeholder="Share Your Thoughts">
                        <div class="form-icon">
                            <i class="icofont icofont-paper-plane"></i>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Sidebar inner chat end-->
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    
                    <div class="pcoded-content1">
                        <div class="pcoded-inner-content">

                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">

                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <div class="card">
                                            <!-- Email-card start -->
                                            <div class="card-block email-card">
                                                <div class="row">
                                                    <div class="col-lg-12 col-xl-3">
                                                        <div class="user-head row">
                                                            <div class="user-face">
                                                                <img class="img-fluid" src="<?=base_url()?>assets/gmail_assets/assets/images/logo.png" alt="Theme-Logo" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-xl-9">
                                                        <div class="mail-box-head row">
                                                            <div class="col-md-12">
                                                                <form class="f-right">
                                                                    <div class="right-icon-control">
                                                                        <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends-2">
                                                                        <div class="form-icon">
                                                                            <i class="icofont icofont-search"></i>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <!-- Left-side section start -->
                                                    <div class="col-lg-12 col-xl-3">
                                                        <div class="user-body">
                                                            <div class="p-20 text-center">
                                                                <a href="email-compose.html" class="btn btn-danger">Compose</a>
                                                            </div>
                                                            <ul class="page-list nav nav-tabs flex-column labels-list" id="pills-tab" role="tablist">
                                                                <li class="nav-item mail-section label" data-label="INBOX">
                                                                    <a class="nav-link active" data-toggle="pill" href="#e-inbox" role="tab">
                                                                        <i class="icofont icofont-inbox"></i> Inbox
                                                                        <!-- <span class="label label-primary f-right">6</span> -->
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item mail-section label" data-label="STARRED">
                                                                    <a class="nav-link" data-toggle="pill" href="#e-starred" role="tab">
                                                                        <i class="icofont icofont-star"></i> Starred
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item mail-section label" data-label="DRAFT">
                                                                    <a class="nav-link" data-toggle="pill" href="#e-drafts" role="tab">
                                                                        <i class="icofont icofont-file-text"></i> Drafts
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item mail-section label" data-label="SENT">
                                                                    <a class="nav-link" data-toggle="pill" href="#e-sent" role="tab">
                                                                        <i class="icofont icofont-paper-plane"></i> Sent Mail
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item mail-section label" data-label="TRASH">
                                                                    <a class="nav-link" data-toggle="pill" href="#e-trash" role="tab">
                                                                        <i class="icofont icofont-ui-delete"></i> Trash
<!--                                                                         <span class="label label-info f-right">30</span>
 -->                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <ul class="p-20 label-list">
                                                                <li>
                                                                    <h5>Labels</h5>
                                                                </li>
                                                                    <?php
                                                                    foreach ($lables as $id=>$val)
                                                                    {
                                                                        echo "<li><a data-id='$id'>$val</a></li>";
                                                                    }
                                                                    ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- Left-side section end -->
                                                    <!-- Right-side section start -->
                                                    <div class="col-lg-12 col-xl-9">
                                                        <div class="tab-content" id="pills-tabContent">
                                                            <div class="tab-pane fade show active" id="e-inbox" role="tabpanel">

                                                                <div class="mail-body">
                                                                    <div class="mail-body-header">
                                                                        <button type="button" class="btn btn-primary btn-xs waves-effect waves-light">
                                                                              <i class="icofont icofont-exclamation-circle"></i>
                                                                          </button>
                                                                        <button type="button" class="btn btn-success btn-xs waves-effect waves-light">
                                                                              <i class="icofont icofont-inbox"></i>
                                                                          </button>
                                                                        <button type="button" class="btn btn-danger btn-xs waves-effect waves-light">
                                                                              <i class="icofont icofont-ui-delete"></i>
                                                                          </button>
                                                                        <div class="btn-group dropdown-split-primary">
                                                                            <button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                  <i class="icofont icofont-ui-folder"></i>
                                                                              </button>
                                                                            <div class="dropdown-menu">
                                                                                <a class="dropdown-item waves-effect waves-light" href="#">Action</a>
                                                                                <a class="dropdown-item waves-effect waves-light" href="#">Another action</a>
                                                                                <a class="dropdown-item waves-effect waves-light" href="#">Something else here</a>
                                                                                <div class="dropdown-divider"></div>
                                                                                <a class="dropdown-item waves-effect waves-light" href="#">Separated link</a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="btn-group dropdown-split-primary">
                                                                            <button type="button" class="btn btn-warning dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                  More
                                                                              </button>
                                                                            <div class="dropdown-menu">
                                                                                <a class="dropdown-item waves-effect waves-light" href="#">Action</a>
                                                                                <a class="dropdown-item waves-effect waves-light" href="#">Another action</a>
                                                                                <a class="dropdown-item waves-effect waves-light" href="#">Something else here</a>
                                                                                <div class="dropdown-divider"></div>
                                                                                <a class="dropdown-item waves-effect waves-light" href="#">Separated link</a>
                                                                            </div>                                                                    
                                                                        </div>
                                                                            <button type="button" class="btn btn-primary btn-xs waves-effect waves-light" id="previousPage" style="float: right">
                                                                              Pre                                                                         </button>

                                                                          <button type="button" class="btn btn-primary btn-xs waves-effect waves-light" data-id="<?php echo $nextPage;?>" id="nextPage" style="float: right">
                                                                              Next
                                                                          </button>
                                                                    </div>

                                                                    <div class="mail-body-content threads">
                                                                        <div class="table-responsive">
<table class="table">

<?php $str="";
foreach ($threads as $info) {
    extract($info);
    if(in_array('UNREAD',$labels))$trcls="unread";
    else $trcls='read';
    if(in_array('STARRED',$labels))$is_star="warning";
    else $is_star="defult";

$str.="<tr class='$trcls'>
<td>
<div class='check-star'>
  <div class='checkbox-fade fade-in-primary checkbox'>
    <label><input type='checkbox' value='$id'><span class='cr'><i class='cr-icon icofont icofont-verification-check txt-primary'></i></span></label></div>
    <i class='icofont icofont-star text-$is_star'></i></div></td><td><a href='javascript:void(0);' data-id='$id' class='email-name'>$From</a></td><td><a href='javascript:void(0);' class='email-name' data-id='$id'>$Subject</a></td><td class='email-attch'><a href='#'><i class='icofont icofont-clip'></i></a></td><td class='email-time'>$Date
        </td></tr>";
}
echo $str;
?> 
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mail-body-content email-read">
                                                                    <div class="mail-body-header">
                                                                    <button type="button" class="btn btn-primary btn-xs waves-effect waves-light" id="show_thread">
                                                                              Back
                                                                          </button>
                                                                    </div>
                                                                     <div class="mail-body-content">
                                                                     </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Right-side section end -->
                                                </div>
                                            </div>
                                            <!-- Email-card end -->
                                        </div>
                                    </div>
                                    <!-- Page-body start -->
                                </div>
                            </div>
                            <!-- Main-body end -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="<?=base_url()?>assets/gmail_assets/assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="<?=base_url()?>assets/gmail_assets/assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="<?=base_url()?>assets/gmail_assets/assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="<?=base_url()?>assets/gmail_assets/assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="<?=base_url()?>assets/gmail_assets/assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?=base_url()?>assets/gmail_assets/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/gmail_assets/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/gmail_assets/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/gmail_assets/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?=base_url()?>assets/gmail_assets/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?=base_url()?>assets/gmail_assets/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/gmail_assets/bower_components/modernizr/js/css-scrollbars.js"></script>

    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?=base_url()?>assets/gmail_assets/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/gmail_assets/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/gmail_assets/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/gmail_assets/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->

    <script src="<?=base_url()?>assets/gmail_assets/assets/js/pcoded.min.js"></script>
    <script src="<?=base_url()?>assets/gmail_assets/assets/js/demo-12.js"></script>
    <script src="<?=base_url()?>assets/gmail_assets/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/gmail_assets/assets/js/script.js"></script>
    
    <script>
        var currentLabel="INBOX";
        var num_Rows=50;
        var nextPage=<?php echo $nextPage;?>;
        var currentPage=1;
        var previousPage=new Array();

     $(document).ready(function()
     {
        $("#nextPage").click(function()
        { 
            var nextPage=$(this).attr("data-id");
            alert(nextPage);

                var param= new Object();
                param.labelIds=[currentLabel];
                param.maxResults=num_Rows;
                param.pageToken=nextPage;

            if(nextPage!==undefined)
            {
                $.ajax(
                {
                    "url":"<?php echo base_url()?>mail/page_wise_threads",
                    "type":"POST",
                    "data":{"querys":JSON.stringify(param)},
                    success:function(rus)
                    {
                       previousPage.push(currentPage);
                       currentPage=nextPage;  
                       var data=JSON.parse(rus);
                       nextPage=data['nextPage'];
                       $("#nextPage").attr("data-id",data['nextPage']);
                        //console.log(data);
                      update_threads(data['threads']);
                      console.log(previousPage);
                    }
                }
                );
            }
        }
        );

        $("#previousPage").click(function()
        {
           var pageid=previousPage.pop();
           if(pageid!=null)
           {

            var param= new Object();
                param.labelIds=[currentLabel];
                param.maxResults=num_Rows;
                            
            if(pageid!=1)
                param.pageToken=pageid;
            

            $.ajax(
                {
                    "url":"<?php echo base_url()?>mail/page_wise_threads",
                    "type":"POST",
                    "data":{"querys":JSON.stringify(param)},
                    success:function(rus)
                    {
                       
                       currentPage=pageid;  
                       var data=JSON.parse(rus);
                       nextPage=data['nextPage'];
                       $("#nextPage").attr("data-id",data['nextPage']);
                        //console.log(data);
                      update_threads(data['threads']);
                      
                    }
                }
                );
           }
        }
        );
        $(".labels-list .label").click(function()
        { alert("i");
             currentLabel=$(this).attr("data-label");
            var param= new Object();
                param.labelIds=[currentLabel];
                param.maxResults=num_Rows;
            $.ajax(
                {
                    "url":"<?php echo base_url()?>mail/page_wise_threads",
                    "type":"POST",
                    "data":{"querys":JSON.stringify(param)},
                    success:function(rus)
                    {
                       
                       currentPage=1;  
                       var data=JSON.parse(rus);
                       nextPage=data['nextPage'];
                       $("#nextPage").attr("data-id",data['nextPage']);
                        //console.log(data);
                      update_threads(data['threads']);
                      previousPage=[];
                      
                    }
                }
                );
        }
        )
        $(document).on("click",".email-name",function(){
            var tid=$(this).attr("data-id");
            $.ajax({
                "url":"<?php echo base_url()?>mail/threads_messages/"+tid,
                "type":"POST",
                success:function(rus)
                    {
                       
                        
                       var data=JSON.parse(rus);
                        console.log(data);
                    for(i in data)
                    {
                       var header=data[i]['header'];
                        $(".threads").hide();
                        $(".mail-body-content.email-read").show();
                        $(".mail-body-content.email-read .mail-body-content").html("<div class='card'><div class='card-header'><h5>"+header['Subject']+"</h5><h6 class='f-right'>"+header['Date']+"</h6></div><div class='card-block'><div class='media m-b-20'><div class='media-left photo-table'><a href='#'><img class='media-object img-radius' src='assets/images/avatar-3.jpg' alt='E-mail User'></a></div><div class='media-body photo-contant'><a href='#'><h6 class='user-name txt-primary'>"+header['From']+"</h6></a><a class='user-mail txt-muted' href='#'><h6>example@gmail.com</h6></a>"+window.atob(data[i]['message'])+"</div> </div></div></div>");                      
                    }                   
                    }

            });
        });
        $("#show_thread").click(function(){
            $(".mail-body-content.email-read").hide();
            $(".threads").show();
        });
     }
     );   

 function update_threads(threads)
    {   
        var rows="";
        for(i in threads)
          {
            var td=threads[i];
            var trcls="";
            var is_star="";

            var cr=Object.values(td['labels']).indexOf("UNREAD");
            if(cr > -1)trcls="unread";
            else trcls="read";

            var cs=Object.values(td['labels']).indexOf("STARRED");
            if(cs > -1)is_star="warning";
            else is_star="defult";


            rows+="<tr class='"+trcls+"'><td><div class='check-star'><div class='checkbox-fade fade-in-primary checkbox'><label><input type='checkbox' value='"+td['id']+"'><span class='cr'><i class='cr-icon icofont icofont-verification-check txt-primary'></i></span></label></div><i class='icofont icofont-star text-"+is_star+"'></i></div></td><td><a href='#' data-id="+td['id']+" class='email-name'>"+td['From']+"</a></td><td><a href='javascript:void(0);' class='email-name' data-id="+td['id']+">"+td['Subject']+"</a></td><td class='email-attch'><a href='#'><i class='icofont icofont-clip'></i></a></td><td class='email-time'>"+td['Date']+"</td></tr>";
          }
          $(".table").html(rows);
    }

    </script>
</body>

</html>

