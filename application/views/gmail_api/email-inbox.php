<?php 
$this->load->model("Common_mdl");
$this->load->view('includes/header'); ?>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>  
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->

    <div id="pcoded" class="pcoded iscollapsed" vertical-nav-type="offcanvas">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper crmgmail">

            
            <!-- Sidebar inner chat end-->
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    
                    <div class="pcoded-content1">
                        <div class="pcoded-inner-content">

                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">

                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <div class="card">
                                            <!-- Email-card start -->
                                            <div class="card-block email-card">
                                               
                                                <div class="row">
                                                    <!-- Left-side section start -->
                                                    <div class="col-lg-12 col-xl-2">
                                                        <div class="user-body">
                                                            <div class="p-20 text-center">
                                                                <a href="javascript:;" class="btn btn-danger"><i class="icofont icofont-plus"></i>Compose</a>
                                                            </div>
                                                            <ul class="page-list nav nav-tabs flex-column labels-list" id="pills-tab" role="tablist">
                                                                <li class="nav-item mail-section label" data-label="INBOX">
                                                                    <a class="nav-link active" data-toggle="pill" href="#e-inbox" role="tab">
                                                                        <i class="icofont icofont-inbox"></i> Inbox
                                                                        <!-- <span class="label label-primary f-right">6</span> -->
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item mail-section label" data-label="STARRED">
                                                                    <a class="nav-link" data-toggle="pill" href="#e-starred" role="tab">
                                                                        <i class="icofont icofont-star"></i> Starred
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item mail-section label" data-label="DRAFT">
                                                                    <a class="nav-link" data-toggle="pill" href="#e-drafts" role="tab">
                                                                        <i class="icofont icofont-file-text"></i> Drafts
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item mail-section label" data-label="SENT">
                                                                    <a class="nav-link" data-toggle="pill" href="#e-sent" role="tab">
                                                                        <i class="icofont icofont-paper-plane"></i> Sent Mail
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item mail-section label" data-label="TRASH">
                                                                    <a class="nav-link" data-toggle="pill" href="#e-trash" role="tab">
                                                                        <i class="icofont icofont-ui-delete"></i> Trash
<!--                                                                         <span class="label label-info f-right">30</span>
 -->                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <ul class="labels-list labels-listbottom">
                                                                <li>
                                                                    <h5>User Labels</h5>
                                                                </li>
                                                                    <?php
                                                                    foreach ($lables as $id=>$val)
                                                                    {

                                                                        echo "<li  data-label='$id' class='label'><a class='nav-link' data-toggle='pill' href='javascript:void(0)' role='tab'>$val</a></li>";
                                                                    }
                                                                    ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- Left-side section end -->
                                                    <!-- Right-side section start -->
                                                    <div class="col-lg-12 col-xl-10">
                                                        <div class="tab-content" id="pills-tabContent">
                                                            <div class="tab-pane fade show active" id="e-inbox" role="tabpanel">

                                                                <div class="mail-body">
                                                                   

                                                                    <div class="mail-body-content threads crmmailcontent">

                                                                     <div class="mail-body-header">
                                                                      
                                                                            <button type="button" class="btn btn-primary btn-xs waves-effect waves-light" id="previousPage">
                                                                              Previous                                                                         </button>

                                                                          <button type="button" class="btn btn-primary btn-xs waves-effect waves-light" data-id="<?php echo $nextPage;?>" id="nextPage">
                                                                              Next
                                                                          </button>
                                                                    </div>
                                                                        <div class="table-responsive">
<table class="table">

<?php $str="";
foreach ($threads as $info) {
    extract($info);
    if(in_array('UNREAD',$labels))$trcls="unread";
    else $trcls='read';
    if(in_array('STARRED',$labels))$is_star="warning";
    else $is_star="defult";

$str.="<tr class='$trcls'>
<td>
<div class='check-star'>
  <div class='checkbox-fade fade-in-primary checkbox'>
    <label><input type='checkbox' value='$id'><span class='cr'><i class='cr-icon icofont icofont-verification-check txt-primary'></i></span></label></div>
    <i class='icofont icofont-star text-$is_star'></i></div></td><td><a href='javascript:void(0);' data-id='$id' class='email-name'>$From</a></td><td><a href='javascript:void(0);' class='email-name' data-id='$id'>$Subject</a></td><td class='email-attch'><a href='#'><i class='icofont icofont-clip'></i></a></td><td class='email-time'>$Date
        </td></tr>";
}
echo $str;
?> 
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mail-body-content email-read crmemail-read" style="display: none;">
                                                                    <div class="mail-body-header">
                                                                    <button type="button" class="btn btn-primary btn-xs waves-effect waves-light" id="show_thread">
                                                                              Back
                                                                          </button>
                                                                    </div>
                                                                     <div class="mail-body-content">
                                                                     </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Right-side section end -->
                                                </div>
                                            </div>
                                            <!-- Email-card end -->
                                        </div>
                                    </div>
                                    <!-- Page-body start -->
                                </div>
                            </div>
                            <!-- Main-body end -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <?php $this->load->view("includes/footer"); ?>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?=base_url()?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?=base_url()?>bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?=base_url()?>bower_components/modernizr/js/css-scrollbars.js"></script>

    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?=base_url()?>bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->

    <script src="<?=base_url()?>assets/js/pcoded.min.js"></script>
    <script src="<?=base_url()?>assets/js/demo-12.js"></script>
    <script src="<?=base_url()?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/script.js"></script>
    
    <script>
        var currentLabel="INBOX";
        var num_Rows=50;
        var nextPage=<?php echo $nextPage;?>;
        var currentPage=1;
        var previousPage=new Array();

     $(document).ready(function()
     {
        $("#nextPage").click(function()
        { 
            var nextPage=$(this).attr("data-id");
         //   alert(nextPage);

                var param= new Object();
                param.labelIds=[currentLabel];
                param.maxResults=num_Rows;
                param.pageToken=nextPage;

            if(nextPage!==undefined)
            {
                $.ajax(
                {
                    "url":"<?php echo base_url()?>mail/page_wise_threads",
                    "type":"POST",
                    "data":{"querys":JSON.stringify(param)},
                     beforeSend: function() {
                        $(".LoadingImage").show();
                      },
                    success:function(rus)
                    {
                       previousPage.push(currentPage);
                       currentPage=nextPage;  
                       var data=JSON.parse(rus);
                       nextPage=data['nextPage'];
                       $("#nextPage").attr("data-id",data['nextPage']);
                        //console.log(data);
                      update_threads(data['threads']);
                      $(".LoadingImage").hide();
                      console.log(previousPage);
                    }
                }
                );
            }
        }
        );

        $("#previousPage").click(function()
        {
           var pageid=previousPage.pop();
           if(pageid!=null)
           {

            var param= new Object();
                param.labelIds=[currentLabel];
                param.maxResults=num_Rows;
                            
            if(pageid!=1)
                param.pageToken=pageid;
            

            $.ajax(
                {
                    "url":"<?php echo base_url()?>mail/page_wise_threads",
                    "type":"POST",
                    "data":{"querys":JSON.stringify(param)},
                     beforeSend: function() {
                        $(".LoadingImage").show();
                      },
                    success:function(rus)
                    {                       
                       currentPage=pageid;  
                       var data=JSON.parse(rus);
                       nextPage=data['nextPage'];
                       $("#nextPage").attr("data-id",data['nextPage']);
                        //console.log(data);
                      update_threads(data['threads']);
                       $(".LoadingImage").hide();
                    }
                }
                );
           }
        }
        );
        $(".labels-list .label").click(function()
        { //alert("i");
             currentLabel=$(this).attr("data-label");
            var param= new Object();
                param.labelIds=[currentLabel];
                param.maxResults=num_Rows;
            $.ajax(
                {
                    "url":"<?php echo base_url()?>mail/page_wise_threads",
                    "type":"POST",
                    "data":{"querys":JSON.stringify(param)},
                     beforeSend: function() {
                        $(".LoadingImage").show();
                      }
                    ,success:function(rus)
                    {
                       
                       currentPage=1;  
                       var data=JSON.parse(rus);
                       nextPage=data['nextPage'];
                       $("#nextPage").attr("data-id",data['nextPage']);
                        //console.log(data);
                      update_threads(data['threads']);
                      previousPage=[];
                      $(".mail-body-content.email-read").hide();
                      $(".threads").show();
                      $(".LoadingImage").hide();
                      
                    }
                }
                );
        }
        )
        $(document).on("click",".email-name",function(){
            var tid=$(this).attr("data-id");
            $.ajax({
                "url":"<?php echo base_url()?>mail/threads_messages/"+tid,
                "type":"POST",
                beforeSend: function() {
                        $(".LoadingImage").show();
                      }
                ,success:function(rus)
                    {
                       
                        
                       var data=JSON.parse(rus);
                        console.log(data);
                    for(i in data)
                    {
                       var header=data[i]['header'];
                        $(".threads").hide();
                        $(".mail-body-content.email-read").show();
                        $(".mail-body-content.email-read .mail-body-content").html("<div class='card'><div class='card-header'><h5>"+header['Subject']+"</h5><h6 class='f-right'>"+header['Date']+"</h6></div><div class='card-block'><div class='media m-b-20'><div class='media-left photo-table'><a href='#'><img class='media-object img-radius' src='assets/images/avatar-3.jpg' alt='E-mail User'></a></div><div class='media-body photo-contant'><a href='#'><h6 class='user-name txt-primary'>"+header['From']+"</h6></a><a class='user-mail txt-muted' href='#'><h6>example@gmail.com</h6></a>"+window.atob(data[i]['message'])+"</div> </div></div></div>");                      
                    }  
                    $(".LoadingImage").hide();                 
                    }

            });
        });
        $("#show_thread").click(function(){
            $(".mail-body-content.email-read").hide();
            $(".threads").show();
        });
     }
     );   

 function update_threads(threads)
    {   
        var rows="";
        for(i in threads)
          {
            var td=threads[i];
            var trcls="";
            var is_star="";

            var cr=Object.values(td['labels']).indexOf("UNREAD");
            if(cr > -1)trcls="unread";
            else trcls="read";

            var cs=Object.values(td['labels']).indexOf("STARRED");
            if(cs > -1)is_star="warning";
            else is_star="defult";


            rows+="<tr class='"+trcls+"'><td><div class='check-star'><div class='checkbox-fade fade-in-primary checkbox'><label><input type='checkbox' value='"+td['id']+"'><span class='cr'><i class='cr-icon icofont icofont-verification-check txt-primary'></i></span></label></div><i class='icofont icofont-star text-"+is_star+"'></i></div></td><td><a href='#' data-id="+td['id']+" class='email-name'>"+td['From']+"</a></td><td><a href='javascript:void(0);' class='email-name' data-id="+td['id']+">"+td['Subject']+"</a></td><td class='email-attch'><a href='#'><i class='icofont icofont-clip'></i></a></td><td class='email-time'>"+td['Date']+"</td></tr>";
          }
          $(".table").html(rows);
    }

    </script>
</body>

</html>

