<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');
   ?>

    <!--   Datatable header by Ram -->
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css?ver=2"> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
 <!--   End by Ram -->

   <style type="text/css">
     
           .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}
   </style>
   <!-- hint 
just change the display name like due and over due.
in functions we use due as over due, over due as due.
   -->
<!-- management block -->
<div class="pcoded-content newalign-dead">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <div class="deadline-crm1 floating_set single-txt12 ddline">
               <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                  <li class="nav-item">
                     Deadline Manager
                     <div class="slide"></div>
                  </li>
               </ul>

               

            </div>


            <div class=" deadline-block">
               <!-- end of export option -->
               <div class="payrolltax floating rev-op">
                  <div class="company-search1">
                    <!-- <div class="vat-shipping">
                        <label>Company Name</label> 
                        <select name="companysearch" id="companysearch" class="search">
                           <option value="">All</option>
                           <option value="2">zts</option>
                           <option value="">abc company</option>
                        </select>
                        </div> -->
                        <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard don1 renewdesign">

                  <li class="search-block01">
                     <div class="search-vat1">
                     <label>Search:</label>
                     <input type="text" name="deadline_search" id="deadline_search" placeholder="Search Company">
                  </div>
                  </li>

                    <li>
                    <div class="dropdown-primary dropdown open for_ul_ajax">
                        <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button>
                        <div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url();?>Deadline_manager/deadline_exportCSV?stati=0&de=0&pe=0" class="dropdown-item waves-light waves-effect but">Csv</a><a href="<?php echo base_url();?>Deadline_manager/pdf?stati=0&de=0&pe=0" class="dropdown-item waves-light waves-effect but">Pdf</a><a href="<?php echo base_url();?>Deadline_manager/html?stati=0&de=0&pe=0" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>
                     </div>
                     </li>
                   
               </ul>
               <div class="vat-dead-01">
                     <div class="vat-shipping">
                        <label>Company Type</label>
                        <select class="  fields search" name="company_type" id="company_type">
                           <option value="" data-id="1">All</option>
                           <option value="Private Limited company" data-id="2">Private Limited company</option>
                           <option value="Public Limited company" data-id="3">Public Limited company</option>
                           <option value="Limited Liability Partnership" data-id="4">Limited Liability Partnership</option>
                           <option value="Partnership" data-id="5">Partnership</option>
                           <option value="Self Assessment" data-id="6">Self Assessment</option>
                           <option value="Trust" data-id="7">Trust</option>
                           <option value="Charity" data-id="8">Charity</option>
                           <option value="Other" data-id="9">Other</option>
                        </select>
                     </div>
                     <div class="vat-shipping">
                        <label>Deadline Type</label>
                        <select class="fields search12" name="deadline_type" id="deadline_type">
                           <option value="">ALL</option>
                           <option value="VAT">VAT</option>
                           <option value="Payroll">Payroll</option>
                           <option value="Accounts">Accounts</option>
                           <option value="Confirmation statement">Confirmation statement</option>
                           <option value="Company Tax Return">Company Tax Return</option>
                           <option value="Personal Tax Return">Personal Tax Return</option>
                           <option value="WorkPlace Pension - AE">WorkPlace Pension - AE</option>
                           <option value="CIS - Contractor">CIS - Contractor</option>
                           <option value="CIS - Sub Contractor">CIS - Sub Contractor</option>
                           <option value="P11D">P11D</option>
                           <option value="Bookkeeping">Bookkeeping</option>
                           <option value="Management Accounts">Management Accounts</option>
                           <option value="Investigation Insurance">Investigation Insurance</option>
                           <option value="Registered Address">Registered Address</option>
                           <option value="Tax Advice">Tax Advice</option>
                           <option value="Tax Investigation">Tax Investigation</option>
                        </select>
                     </div>
                     <div class="vat-shipping">
                      <div class="radio radio-inline">
                        <label> <input type="radio" name="for_due" id="for_due" value="due"> <i class="helper"></i>Over Due</label>
                      </div>
                      <div class="radio radio-inline">
                        <label> <input type="radio" name="for_due" id="for_due" value="over_due" checked="checked"> <i class="helper"></i>Due</label>
                      </div>
                        </div>
                  </div>
               </div>
               <!-- collapse panel -->
               <div class="day-list   accor-view09 floating_set">
                  <!-- <div class="f-days">
                     <div class="panel-head" ddata-parent="#accordion" data-toggle="collapse" data-target="#demo">
                        Today
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                     </div>
                     <div class="panel-body panel-collapse collapse" id="demo">
                        No Deadlines to view
                     </div>
                     </div>
                     <div class="f-week">
                     <div class="panel-head" data-parent="#accordion" data-toggle="collapse" data-target="#demo1">
                        This Week
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                     </div>
                     <div class="panel-body panel-collapse collapse" id="demo1">
                        No Deadlines to view
                     </div>
                     </div>
                     <div class="f-month">
                     <div class="panel-head" data-parent="#accordion" data-toggle="collapse" data-target="#demo2">
                        This Month
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                     </div>
                     <div class="panel-body panel-collapse collapse" id="demo2">
                        No Deadlines to view
                     </div>
                     </div>
                     <div class="n-month">
                     <div class="panel-head" data-parent="#accordion" data-toggle="collapse" data-target="#demo3">
                        Next Month
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                     </div>
                     <div class="panel-body panel-collapse collapse" id="demo3">
                        No Deadlines to view
                     </div>
                     </div> -->
                  <div class=" accordion-block color-accordion-block">

                  <!-- new tab -->
                  <input type="hidden" name="active_tabs_name" id="active_tabs_name" value="today">
                     <div class="taskpagedashboard1">
                              <div class="top-leads fr-task cfssc">
                              <ul class="nav nav-tabs lead-data1 pull-right">
                                    <li class="nav-item junk-lead tsk-color1 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val nots_task">
                                      <div class="lead-point1">
                                       <a class="for_link nav-link active" data-id="today" data-toggle="tab" href="#this-year">
                                        <strong class="for_count count_today"></strong>
                                       <span class="task_status_val_1 status_filter">Today</span>
                                     </a>
                                     </div>
                                    </li>
                                     <li class="nav-item junk-lead tsk-color1 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val nots_task">
                                      <div class="lead-point1">
                                       <a class="for_link nav-link" data-id="this-week" data-toggle="tab" href="#this-year">
                                        <strong class="for_count count_week"></strong>
                                        <span class="task_status_val_1 status_filter">This Week</span>
                                      </a>
                                       </div>
                                    </li>
                                    <li class="nav-item junk-lead tsk-color1 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val nots_task">
                                      <div class="lead-point1">
                                       <a class="for_link nav-link" data-id="this-month" data-toggle="tab" href="#this-year">
                                        <strong class="for_count count_month"></strong>
                                        <span class="task_status_val_1 status_filter">This Month</span></a>
                                       </div>
                                    </li>
                                    <li class="nav-item junk-lead tsk-color1 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val nots_task">
                                      <div class="lead-point1">
                                       <a class="for_link nav-link this_year" data-id="this-year" data-toggle="tab" href="#this-year">
                                        <strong class="for_count count_year"></strong>
                                        <span class="task_status_val_1 status_filter">This Year</span></a>
                                       </div>
                                    </li>
                                 </div>
                               </div>
                             <input type="text" id="clientCreate_FromDate" class="date_picker_dob" name="from_date_picker" placeholder="By Date" value="<?php echo date("d-m-Y") ?>" readonly>
                             <input type="text" id="clientCreate_ToDate" name="to_date_picker" class="date_picker_dob" value="<?php echo date("d-m-Y") ?>" placeholder="By Date" readonly>
                             <div class="tab-content <?php if($_SESSION['permission']['Deadline_manager']['view']!='1'){ ?> permission_deined <?php } ?>" id="accordion_table">
                             <div id="today" class="tab-pane fade in active">
                                 <table class="table client_table1 text-center " id="today_deadlineinfo">
                                    <thead>
                                       <th>Client Name</th>
                                       <th>Client Type</th>
                                       <th>Company Name</th>
                                       <th>Deadline Type</th>
                                       <th>Date</th>
                                    </thead>

                                      <tfoot>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                    </tfoot>
                                    <tbody>
                                      
                                       <?php 
                                       $current_date=date("Y-m-d");
                                          foreach ($getCompany_today as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                
                                          ?>
                                          <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))){ ?>
                                       <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Confirmation statement Due Date</td>
                                          <td><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
                                       </tr>
                                          <?php } ?>
                                          <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td>Accounts Due Date</td>
                                          
                                           <td><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
                                          <!-- account -->
                                           </tr>
                                           <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                           <tr class="for_table">
                                           <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Company Tax Return Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
                                           <!--company tax -->
                                         </tr>
                                         <?php } ?>
                                          <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return']))){ ?>
                                         <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td>Personal Tax Return Due Date</td>
                                          <td><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
                                          <!-- crm personal tax -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']))){ ?>
                                          <tr class="for_table">
                                            <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>VAT Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
                                          <!-- vat -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))){ ?>
                                          <tr class="for_table">
                                            <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td>Payroll Due Date</td>
                                             <td><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
                                          <!-- payrol -->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>WorkPlace Pension - AE Due Date</td>
                                            <td><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
                                          <!--workplace -->
                                          </tr>
                                          <?php } ?>
                                           
                                          <tr class="for_table">
                                           <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td>CIS - Contractor Due Date</td>
                                          <td></td>
                                          <!-- CIS - Contractor -->
                                          </tr>
                                         
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>CIS - Sub Contractor Due Date</td>
                                          <td></td>
                                          <!-- CIS - Sub Contractor-->
                                          </tr>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>P11D Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
                                          <!-- p11d -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Management Accounts Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
                                          <!-- manage account -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Bookkeeping Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
                                          <!-- booking-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Investigation Insurance Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
                                          <!-- insurance -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Registered Address Due Date</td>
                                         <td><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
                                          <!-- registed -->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Tax Advice Due Date</td>
                                        <td><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax advice-->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Tax Investigation Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax inve-->
                                          </tr>
                                      <?php } ?>
                                       <?php   }  ?>
                                    </tbody>
                                 </table>
                             
                        </div>
                        <div id="this-week" class="tab-pane fade">
                                 <table class="table client_table1 text-center " id="thisweek_deadlineinfo">
                                   <thead>
                                       <th>Client Name</th>
                                       <th>Client Type</th>
                                       <th>Company Name</th>
                                       <th>Deadline Type</th>
                                       <th>Date</th>
                                    </thead>

                                     <tfoot>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                    </tfoot>
                                       <tbody>
                                     
                                       <?php 
                                       $time=date('Y-m-d');
                                        $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
                                          foreach ($getCompany_oneweek as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                            
                                          ?>
                                      <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))){ ?>
                                       <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Confirmation statement Due Date</td>
                                          <td><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
                                       </tr>
                                          <?php } ?>
                                            <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td>Accounts Due Date</td>
                                          
                                           <td><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
                                          <!-- account -->
                                           </tr>
                                           <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                           <tr class="for_table">
                                           <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Company Tax Return Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
                                           <!--company tax -->
                                         </tr>
                                         <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                         <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td>Personal Tax Return Due Date</td>
                                          <td><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
                                          <!-- crm personal tax -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']))){ ?>
                                          <tr class="for_table">
                                            <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>VAT Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
                                          <!-- vat -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))){ ?>
                                          <tr class="for_table">
                                            <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td>Payroll Due Date</td>
                                             <td><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
                                          <!-- payrol -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>WorkPlace Pension - AE Due Date</td>
                                            <td><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
                                          <!--workplace -->
                                          </tr>
                                          <?php } ?>
                                           
                                          <tr class="for_table">
                                           <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td>CIS - Contractor Due Date</td>
                                          <td></td>
                                          <!-- CIS - Contractor -->
                                          </tr>
                                         
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>CIS - Sub Contractor Due Date</td>
                                          <td></td>
                                          <!-- CIS - Sub Contractor-->
                                          </tr>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>P11D Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
                                          <!-- p11d -->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Management Accounts Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
                                          <!-- manage account -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Bookkeeping Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
                                          <!-- booking-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Investigation Insurance Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
                                          <!-- insurance -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Registered Address Due Date</td>
                                         <td><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
                                          <!-- registed -->
                                          </tr>
                                          <?php } ?>
                                         <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Tax Advice Due Date</td>
                                        <td><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax advice-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Tax Investigation Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax inve-->
                                          </tr>
                                      <?php } ?>
                                       <?php   }  ?>
                                    </tbody>

                                 </table>
                              </div>
                           <div id="this-month" class="tab-pane fade">
                       
                                 <table class="table client_table1 text-center " id="thismonth_deadlineinfo">
                                    <thead>
                                       <th>Client Name</th>
                                       <th>Client Type</th>
                                       <th>Company Name</th>
                                       <th>Deadline Type</th>
                                       <th>Date</th>
                                    </thead>

                                     <tfoot>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                    </tfoot>
                                       <tbody>
                                      
                                       <?php 
                                       $time=date('Y-m-d');
                                        $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
                                          foreach ($getCompany_onemonth as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                              
                                          ?>
                                       <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))){ ?>
                                       <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Confirmation statement Due Date</td>
                                          <td><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
                                       </tr>
                                          <?php } ?>
                                            <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td>Accounts Due Date</td>
                                          
                                           <td><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
                                          <!-- account -->
                                           </tr>
                                           <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                           <tr class="for_table">
                                           <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Company Tax Return Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
                                           <!--company tax -->
                                         </tr>
                                         <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                         <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td>Personal Tax Return Due Date</td>
                                          <td><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
                                          <!-- crm personal tax -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']))){ ?>
                                          <tr class="for_table">
                                            <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>VAT Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
                                          <!-- vat -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))){ ?>
                                          <tr class="for_table">
                                            <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td>Payroll Due Date</td>
                                             <td><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
                                          <!-- payrol -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>WorkPlace Pension - AE Due Date</td>
                                            <td><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
                                          <!--workplace -->
                                          </tr>
                                          <?php } ?>
                                           
                                          <tr class="for_table">
                                           <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td>CIS - Contractor Due Date</td>
                                          <td></td>
                                          <!-- CIS - Contractor -->
                                          </tr>
                                         
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>CIS - Sub Contractor Due Date</td>
                                          <td></td>
                                          <!-- CIS - Sub Contractor-->
                                          </tr>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>P11D Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
                                          <!-- p11d -->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Management Accounts Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
                                          <!-- manage account -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Bookkeeping Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
                                          <!-- booking-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Investigation Insurance Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
                                          <!-- insurance -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Registered Address Due Date</td>
                                         <td><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
                                          <!-- registed -->
                                          </tr>
                                          <?php } ?>
                                         <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Tax Advice Due Date</td>
                                        <td><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax advice-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr class="for_table">
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td>Tax Investigation Due Date</td>
                                           <td><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax inve-->
                                          </tr>
                                      <?php } ?>
                                       <?php   }  ?>
                                    </tbody>

                                 </table>
                             
                        </div>
                       <div id="this-year" class="tab-pane fade">
                                 <table class="table client_table1 text-center " id="thisyear_deadlineinfo" style="width: 100%">
                               <thead>
                                            
                                       <th class="client_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Client Name</th>
                                       <th class="client_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Client Type</th>
                                       <th class="company_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Company Name</th>
                                       <th class="deadline_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Deadline Type</th>
                                       <th class="date_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Date</th>
                                        <th class="date_hidden_TH hasFilter"> Date</th>

                                    </thead>

                                  <!--      <tfoot>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                    </tfoot> -->
                                    <tbody>
                                      
                                       <?php 
                                       $time=date('Y-m-d');
                                        $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
                                          foreach ($getCompany_oneyear as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                    
                                          ?>
                                             <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))){ ?>
                                       <tr class="for_table">
                                             
 
                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td data-search="Confirmation statement Due Date">Confirmation statement Due Date</td>
                                          <td data-search="<?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td>

                                           <td data-search="<?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td>

                                          <!-- confi-->
                                       </tr>
                                          <?php } ?>
                                            <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))){ ?>
                                          <tr class="for_table">
                                       
                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td data-search="Accounts Due Date">Accounts Due Date</td>
                                          
                                           <td data-search="<?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>

                                           <td data-search="<?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>

                                          <!-- account -->
                                           </tr>
                                           <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                           <tr class="for_table">
                                         
                                           <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                             <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                              <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td data-search="Company Tax Return Due Date">Company Tax Return Due Date</td>
                                           <td data-search="php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>

                                            <td data-search="php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>

                                           <!--company tax -->
                                         </tr>
                                         <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                         <tr class="for_table">
                                     
                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td data-search="<?php echo $getusername['crm_name'];?>">Personal Tax Return Due Date</td>
                                          <td data-search="<?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>

                                           <td data-search="<?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>

                                          <!-- crm personal tax -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']))){ ?>
                                          <tr class="for_table">
                                       
                                            <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                           <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                           <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td data-search="VAT Due Date">VAT Due Date</td>
                                           <td data-search="<?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>

                                           <td data-search="<?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>

                                          <!-- vat -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))){ ?>
                                          <tr class="for_table">
                                       
                                            <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                           <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                           <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td data-search="Payroll Due Date">Payroll Due Date</td>
                                             <td data-search="<?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>

                                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
                                          <!-- payrol -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']))){ ?>
                                          <tr class="for_table">
                                         
                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                           <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td data-search="WorkPlace Pension - AE Due Date">WorkPlace Pension - AE Due Date</td>
                                            <td data-search="<?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>

                                             <td data-search="<?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>

                                          <!--workplace -->
                                          </tr>
                                          <?php } ?>
                                           
                                          <tr class="for_table">
                                        
                                           <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                           <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                           <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td data-search="CIS - Contractor Due Date">CIS - Contractor Due Date</td>
                                          <td></td>
                                            <td></td>
                                          <!-- CIS - Contractor -->
                                          </tr>
                                         
                                          <tr class="for_table">
                                        
                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td data-search="CIS - Sub Contractor Due Date">CIS - Sub Contractor Due Date</td>
                                          <td></td>
                                          <td></td>

                                          <!-- CIS - Sub Contractor-->
                                          </tr>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']))){ ?>
                                          <tr class="for_table">
                                   
                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                         <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td data-search="P11D Due Date">P11D Due Date</td>
                                           <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>

                                            <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>


                                          <!-- p11d -->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']))){ ?>
                                          <tr class="for_table">
                                        
                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                           <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td data-search="Management Accounts Due Date">Management Accounts Due Date</td>
                                           <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
                                             <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
                                          <!-- manage account -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']))){ ?>
                                          <tr class="for_table">
                                       
                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                         <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td data-search="Bookkeeping Due Date">Bookkeeping Due Date</td>
                                           <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
                                           <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>

                                          <!-- booking-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']))){ ?>
                                          <tr class="for_table">
                                         
                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                           <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                         <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td data-search="Investigation Insurance Due Date">Investigation Insurance Due Date</td>
                                           <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
                                          <!-- insurance -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']))){ ?>
                                          <tr class="for_table">
                                         
                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                           <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td data-search="Registered Address Due Date">Registered Address Due Date</td>
                                         <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>

                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>

                                          <!-- registed -->
                                          </tr>
                                          <?php } ?>
                                         <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr class="for_table">
                                        
                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                         <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                        <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td data-search="Tax Advice Due Date">Tax Advice Due Date</td>
                                        <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                           <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>

                                          <!-- tax advice-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr class="for_table">
                                       
                                          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                                           <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td data-search="Tax Investigation Due Date">Tax Investigation Due Date</td>
                                           <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>

                                           <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax inve-->
                                          </tr>
                                      <?php } ?>
                                       <?php   }  ?>
                                    </tbody>

                                 </table>
                                 <input type="hidden" class="rows_selected" id="select_client_count" > 
                              </div>
                          
                     </div>
                  </div>
               </div>
               <!-- collapse panel -->

               <!-- for staus board -->
   <!-- company details -->

    <div class="overall-quote01 floating_set">
   <div class="accident-toggle new">
        <h6>Status Board | Deadline Calender   </h6> 
             
              
              </div>


   

               <div class="deadlines-types1">

                  <div class="f-right floating_set ">
    <div class="search-vat1 cmn-inline-bg">
          <label>Company Name:</label>
          <input type="text" name="companysearch" id="companysearch" class="search" placeholder="Company Name">
    </div>  
         <div class="company-search1 cmn-inline-bg">
            <div class="vat-shipping">
               <label>Company Type</label>
               <select name="legal_form" class="  fields search" id="legal_form">
                  <option value="" data-id="1">All</option>
                  <option value="Private Limited company" data-id="2">Private Limited company</option>
                  <option value="Public Limited company" data-id="3">Public Limited company</option>
                  <option value="Limited Liability Partnership" data-id="4">Limited Liability Partnership</option>
                  <option value="Partnership" data-id="5">Partnership</option>
                  <option value="Self Assessment" data-id="6">Self Assessment</option>
                  <option value="Trust" data-id="7">Trust</option>
                  <option value="Charity" data-id="8">Charity</option>
                  <option value="Other" data-id="9">Other</option>
               </select>
            </div>
         </div>

          <div class="dropdown-primary dropdown open for_ul_ajax_status cmn-inline-bg">
                        <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button>
                        <div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url();?>Deadline_manager/deadline_exportCSV_status?stati=0&de=0&pe=0" class="dropdown-item waves-light waves-effect but">Csv</a><a href="<?php echo base_url();?>Deadline_manager/pdf_status?stati=0&de=0&pe=0" class="dropdown-item waves-light waves-effect but">Pdf</a><a href="<?php echo base_url();?>Deadline_manager/html_status?stati=0&de=0&pe=0" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>
                     </div>

               </div>

    <!--  <div class="accident-toggle"><h2>MR ACCIDENT SOLUTIONS LIMITED</h2></div> -->
     <div class="investigation-01">
            <div class="color-switches trading-swithch2">
               <button type="button" name="service_fil[]" id="vat" class="btn btn-sm btn-toggle" data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>VAT</label>
            </div>
            <div class="color-switches trading-swithch3">
               <button type="button"  name="service_fil[]" id="payroll" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Payroll</label>
            </div>
            <div class="color-switches trading-swithch4">
               <button type="button"  name="service_fil[]" id="accounts" class="btn btn-sm  btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Accounts</label>
            </div>
            <div class="color-switches trading-swithch5">
               <button type="button"  name="service_fil[]" id="conf_statement" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Confirmation Statement</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="company_tax_return"  class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Company Tax Return</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="personal_tax_return" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Personal Tax Return</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="workplace" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>WorkPlace Pension - AE</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="cis" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>CIS - Contractor</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="cissub" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>CIS - Sub Contractor</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="p11d" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>P11D</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="bookkeep" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Bookkeeping</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="management" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Management Accounts</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="investgate" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Investigation Insurance</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="registered" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Registered Address</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="taxadvice" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Tax Advice</label>
            </div>
            <div class="color-switches trading-swithch6">
               <button type="button"  name="service_fil[]" id="taxinvest" class="btn btn-sm btn-toggle " data-toggle="button" aria-pressed="true" autocomplete="off">
                  <div class="handle"></div>
               </button>
               <label>Tax Investigation</label>
            </div>
         </div>
         </div>

       </div>

         <!-- company details -->

         <!-- info table -->
 <div class="client_section3 table-responsive floating_set filterrec">
        <!--  <div class="service_succ"></div> -->
         <table class="table client_table1 text-center dataTable no-footer" id="servicedead14" role="grid" aria-describedby="newinactives_info">
              <thead>
               <tr class="text-uppercase">
                  <th>Client Name</th>
                  <th>Client Type</th>
                  <th>VAT</th>
                  <th>Payroll</th>
                  <th>Accounts</th>
                  <th>Confirmation statement</th>
                  <th>Company Tax Return</th>
                  <th>Personal Tax Return</th>
                  <th>WorkPlace Pension - AE</th>
                  <th>CIS - Contractor</th>
                  <th>CIS - Sub Contractor</th>
                  <th>P11D</th>
                  <th>Bookkeeping</th>
                  <th>Management Accounts</th>
                  <th>Investigation Insurance</th>
                  <th>Registered Address</th>
                  <th>Tax Advice</th>
                  <th>Tax Investigation</th>
               </tr>
            </thead>
            <tfoot class="ex_data1">
<tr>
<th>
</th>
</tr>
</tfoot>
            <tbody class="user-dashboard-section1">
               <?php 
                  foreach ($getCompany as $getCompanykey => $getCompanyvalue) {
                 
                      $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                  
                  (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                  ($jsnCst!='') ? $cst = $this->Common_mdl->get_price('proposal_service','service_scode','conf_statement','service_price')/*"checked='checked'" */ : $cst = "-";
                  
                  (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                  ($jsnAcc!='') ? $acc = $this->Common_mdl->get_price('proposal_service','service_scode','accounts','service_price')/*"checked='checked'"*/ : $acc = "-";
                  
                  (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                  ($jsntax!='') ? $tax = $this->Common_mdl->get_price('proposal_service','service_scode','company_tax_return','service_price') /*"checked='checked'"*/ : $tax = "-";
                  
                  (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                  ($jsnp_tax!='') ? $p_tax = $this->Common_mdl->get_price('proposal_service','service_scode','personal_tax_return','service_price')/*"checked='checked'"*/ : $p_tax = "-";
                  
                  (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                  ($jsnpay!='') ? $pay = $this->Common_mdl->get_price('proposal_service','service_scode','payroll','service_price')/*"checked='checked'"*/ : $pay = "-";
                  
                  (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                  ($jsnworkplace!='') ? $workplace = $this->Common_mdl->get_price('proposal_service','service_scode','workplace','service_price')/*"checked='checked'"*/ : $workplace = "-";
                  
                  (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                  ($jsnvat!='') ? $vat = $this->Common_mdl->get_price('proposal_service','service_scode','vat','service_price')/*"checked='checked'"*/ : $vat = "-";
                  
                  (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                  ($jsncis!='') ? $cis = $this->Common_mdl->get_price('proposal_service','service_scode','cis','service_price')/*"checked='checked'"*/ : $cis = "-";
                  
                  (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                  ($jsncis_sub!='') ? $cissub  = $this->Common_mdl->get_price('proposal_service','service_scode','cissub','service_price')/*"checked='checked'"*/ : $cissub     = "-";
                  
                  (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                  ($jsnp11d!='') ? $p11d   = $this->Common_mdl->get_price('proposal_service','service_scode','p11d','service_price')/*"checked='checked'"*/ : $p11d   = "-";
                  
                  (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                  ($jsnbk!='') ? $bookkeep     = $this->Common_mdl->get_price('proposal_service','service_scode','bookkeep','service_price')/*"checked='checked'"*/ : $bookkeep   = "-";
                  
                  (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                  ($jsnmgnt!='') ? $management     = $this->Common_mdl->get_price('proposal_service','service_scode','management','service_price')/*"checked='checked'"*/ : $management     = "-";
                  
                  (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                  ($jsninvest!='') ? $investgate   = $this->Common_mdl->get_price('proposal_service','service_scode','investgate','service_price')/*"checked='checked'"*/ : $investgate     = "-";
                  
                  (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                  ($jsnreg!='') ? $registered  = $this->Common_mdl->get_price('proposal_service','service_scode','registered','service_price')/*"checked='checked'"*/ : $registered     = "-";
                  
                  (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                  ($jsntaxad!='') ? $taxadvice     = $this->Common_mdl->get_price('proposal_service','service_scode','taxadvice','service_price')/*"checked='checked'"*/ : $taxadvice  = "-";
                  
                  (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                  ($jsntaxinvest!='') ? $taxinvest     = $this->Common_mdl->get_price('proposal_service','service_scode','taxadvice','service_price') /*"checked='checked'"*/ : $taxinvest  = "-";
                   ?>
               <tr class="for_table">
                  <td><a href="javascript:;" data-toggle="modal" data-target="#desk-popup_<?php echo $getCompanykey;?>"><?php echo $getusername['crm_name'];?></a></td>
                  <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                  <td>
                    <?php echo $vat;?> 

                     <div class="checkbox-color checkbox-primary" style="display: none;">
                         <input type="checkbox" disabled="disabled" name="vat" id="vat_<?php echo $getCompanyvalue['id'];?>" value="" 
                         <?php echo $vat;?> class="update_service" disabled="disabled" > <label for="vat_<?php echo $getCompanyvalue['id'];?>"> </label> 
                     </div>
                

                  </td>
                  <td>
                  <?php echo $pay;?>
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                        <input type="checkbox" name="payroll" id="payroll_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $pay;?> class="update_service" disabled="disabled" >
                  <label for="payroll_<?php echo $getCompanyvalue['id'];?>"> 
                  </label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $acc;?>
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                         <input type="checkbox" value="" name="accounts" id="accounts_<?php echo $getCompanyvalue['id'];?>"  <?php echo $acc;?> class="update_service" disabled="disabled" >
                   <label for="accounts_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $cst;?>
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                         <input type="checkbox" name="conf_statement" id="confstatement_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $cst;?> class="update_service" disabled="disabled" > 
                   <label for="confstatement_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $tax;?> 
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                        <input type="checkbox" name="company_tax" id="companytax_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $tax;?> class="update_service" disabled="disabled" > 
                  <label for="companytax_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $p_tax;?>
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                        <input type="checkbox" value="" name="personal_tax" id="personaltax_<?php echo $getCompanyvalue['id'];?>" <?php echo $p_tax;?> class="update_service" disabled="disabled" > 
                  <label for="personaltax_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $workplace;?>
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                        <input type="checkbox" name="workplace" id="workplace_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $workplace;?> class="update_service" disabled="disabled" > 
                  <label for="workplace_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $cis;?>
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                        <input type="checkbox" name="cis" id="cis_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $cis;?> class="update_service" disabled="disabled" > 
                  <label for="cis_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $cissub;?> 
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                         <input type="checkbox" name="cissub" id="cissub_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $cissub;?> class="update_service" disabled="disabled" >
                  <label for="cissub_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $p11d;?>
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                        <input type="checkbox" namne="p11d" id="p11d_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $p11d;?> class="update_service" disabled="disabled" > 
                  <label for="p11d_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $bookkeep;?>
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                         <input type="checkbox" name="bookkeep" id="bookkeep_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $bookkeep;?> class="update_service" disabled="disabled" >
                  <label for="bookkeep_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $management;?>
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                         <input type="checkbox" name="management" id="management_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $management;?> class="update_service" disabled="disabled" >  
                   <label for="management_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                   <?php echo $investgate;?> 
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                        <input type="checkbox" name="investgate" id="investgate_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $investgate;?> class="update_service" disabled="disabled" ><label for="investgate_<?php echo $getCompanyvalue['id'];?>">  </label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $registered;?>
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                        <input type="checkbox" name="registered" id="registered_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $registered;?> class="update_service" disabled="disabled" > 
                  <label for="registered_<?php echo $getCompanyvalue['id'];?>"> </label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $taxadvice;?>
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                        <input type="checkbox" name="taxadvice" id="taxadvice_<?php echo $getCompanyvalue['id'];?>" value="" <?php echo $taxadvice;?> class="update_service" disabled="disabled" > <label for="taxadvice_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
                  <td>
                  <?php echo $taxinvest;?>
                     <div class="checkbox-color checkbox-primary" style="display: none;">
                        <input type="checkbox" name="taxinvest" id="taxinvest_<?php echo $getCompanyvalue['id'];?>" value="taxinvest" <?php echo $taxinvest;?> class="update_service" disabled="disabled" >  
                  <label for="taxinvest_<?php echo $getCompanyvalue['id'];?>"></label> 
                     </div>
                  </td>
               </tr>
               <?php } ?>
            </tbody>
            </table>
</div>
         <!-- info table -->
               <!-- end of status board -->
            </div>
         </div>
      </div>
   </div>
</div>
<!-- management block -->
<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
       $('.edit-toggle1').click(function(){
         $('.proposal-down').slideToggle(300);
       });
   
        $("#legal_form12").change(function() {
          var val = $(this).val();
          if(val === "from2") {
            // alert('hi');
              $(".from2option").show();
          }
          else {
              $(".from2option").hide();
          }
          });
   
        // $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({ 
        //  //  "scrollX": true
        //  });
   })
</script>
<script type="text/javascript">
    $('#deadline_search').on('keyup',function(){
     // alert('zz');
      var due_val = $("input[name='for_due']:checked").val();
       var company=$(this).val();
       var company_type=$('#company_type').val();
       var deadline_type=$("#deadline_type").val();
     var data={};
                 data['company']= company;
                  data['company_type']= company_type;
                   data['deadline_type']= deadline_type;
                   data['due_val']=due_val;
                    $(".LoadingImage").show();
                 $.ajax({
                     url: '<?php echo base_url();?>Deadline_manager/company_search_deadline_info/',
                     type : 'POST',
                     data : data,
                     success: function(data) {
                         $(".LoadingImage").hide();
                      // alert(data);
                       console.log(data);
                       // $("#accordion").html('');
                       // $("#accordion").html(data);
                        $("#accordion_table").html('');
                       $("#accordion_table").html(data);
                        $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({ 
                         //    "scrollX": true
                           });

                        /** for count **/
                        var today = $('#today_deadlineinfo >tbody >tr.for_table').length;
    $('.count_today').html(today);
var thisweek = $('#thisweek_deadlineinfo >tbody >tr.for_table').length;
      $('.count_week').html(thisweek);
 var month = $('#thismonth_deadlineinfo >tbody >tr.for_table').length;
      $('.count_month').html(month);
 var year = $('#thisyear_deadlineinfo >tbody >tr.for_table').length;
      $('.count_year').html(year);
      var active_tabs_name=$('#active_tabs_name').val();
                $('.tab-pane').removeClass('active');
                    $('.tab-pane').each(function(){

                      if($(this).attr('id')==active_tabs_name)
                      {
                        var clsname=$(this).attr('class')+" active ";
                        $(this).attr('class',clsname);
                      }
                    });
                        /** for count **/
                         $('.for_ul_ajax').html('');
                        $('.for_ul_ajax').html(' <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button><div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url();?>Deadline_manager/deadline_exportCSV_new?company='+company+'&company_type='+company_type+'&deadline_type='+deadline_type+'&due_val='+due_val+'" class="dropdown-item waves-light waves-effect but">Csv</a><a href="<?php echo base_url();?>Deadline_manager/pdf_new?company='+company+'&company_type='+company_type+'&deadline_type='+deadline_type+'&due_val='+due_val+'" class="dropdown-item waves-light waves-effect but">Pdf</a><a href="<?php echo base_url();?>Deadline_manager/html_new?company='+company+'&company_type='+company_type+'&deadline_type='+deadline_type+'&due_val='+due_val+'" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>');
                     },
                 });

    });
      $('#company_type,#deadline_type').on('change',function(){
     // alert('zz');
      $(".LoadingImage").show();
      var due_val = $("input[name='for_due']:checked").val();
       var company=$('#deadline_search').val();
       var company_type=$('#company_type').val();
       var deadline_type=$("#deadline_type").val();
     var data={};
                 data['company']= company;
                  data['company_type']= company_type;
                   data['deadline_type']= deadline_type;
                    data['due_val']=due_val;
                 $.ajax({
                     url: '<?php echo base_url();?>Deadline_manager/company_search_deadline_info/',
                     type : 'POST',
                     data : data,
                     success: function(data) {
                      // alert(data);
                       $(".LoadingImage").hide();
                       console.log(data);
                       // $("#accordion").html('');
                       // $("#accordion").html(data);
                        $("#accordion_table").html('');
                       $("#accordion_table").html(data);
                        $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({ 
                           //  "scrollX": true
                           });

                        /** for count **/
var today = $('#today_deadlineinfo >tbody >tr.for_table').length;
    $('.count_today').html(today);
var thisweek = $('#thisweek_deadlineinfo >tbody >tr.for_table').length;
      $('.count_week').html(thisweek);
 var month = $('#thismonth_deadlineinfo >tbody >tr.for_table').length;
      $('.count_month').html(month);
 var year = $('#thisyear_deadlineinfo >tbody >tr.for_table').length;
      $('.count_year').html(year);
   var active_tabs_name=$('#active_tabs_name').val();
                $('.tab-pane').removeClass('active');
                    $('.tab-pane').each(function(){

                      if($(this).attr('id')==active_tabs_name)
                      {
                        var clsname=$(this).attr('class')+" active ";
                        $(this).attr('class',clsname);
                      }
                    });
/** end of count **/
                         $('.for_ul_ajax').html('');
                        $('.for_ul_ajax').html(' <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button><div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url();?>Deadline_manager/deadline_exportCSV_new?company='+company+'&company_type='+company_type+'&deadline_type='+deadline_type+'&due_val='+due_val+'" class="dropdown-item waves-light waves-effect but">Csv</a><a href="<?php echo base_url();?>Deadline_manager/pdf_new?company='+company+'&company_type='+company_type+'&deadline_type='+deadline_type+'&due_val='+due_val+'" class="dropdown-item waves-light waves-effect but">Pdf</a><a href="<?php echo base_url();?>Deadline_manager/html_new?company='+company+'&company_type='+company_type+'&deadline_type='+deadline_type+'&due_val='+due_val+'" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>');
                     },
                 });

    });
   $('input[type=radio][name=for_due]').change(function() {
      var due_val =this.value;
       var company=$('#deadline_search').val();
       var company_type=$('#company_type').val();
       var deadline_type=$("#deadline_type").val();
        $(".LoadingImage").show();
     var data={};
                 data['company']= company;
                  data['company_type']= company_type;
                   data['deadline_type']= deadline_type;
                    data['due_val']=due_val;
                 $.ajax({
                     url: '<?php echo base_url();?>Deadline_manager/company_search_deadline_info/',
                     type : 'POST',
                     data : data,
                     success: function(data) {
                      // alert(data);
                       $(".LoadingImage").hide();
                       console.log(data);
                       // $("#accordion").html('');
                       // $("#accordion").html(data);
                         $("#accordion_table").html('');
                       $("#accordion_table").html(data);
                        $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({ 
                           //  "scrollX": true
                           });
                        /** for count **/
var today = $('#today_deadlineinfo >tbody >tr.for_table').length;
    $('.count_today').html(today);
var thisweek = $('#thisweek_deadlineinfo >tbody >tr.for_table').length;
      $('.count_week').html(thisweek);
 var month = $('#thismonth_deadlineinfo >tbody >tr.for_table').length;
      $('.count_month').html(month);
 var year = $('#thisyear_deadlineinfo >tbody >tr.for_table').length;
      $('.count_year').html(year);
   var active_tabs_name=$('#active_tabs_name').val();
                $('.tab-pane').removeClass('active');
                    $('.tab-pane').each(function(){

                      if($(this).attr('id')==active_tabs_name)
                      {
                        var clsname=$(this).attr('class')+" active ";
                        $(this).attr('class',clsname);
                      }
                    });
/** end of count **/
                        $('.for_ul_ajax').html('');
                        $('.for_ul_ajax').html(' <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button><div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url();?>Deadline_manager/deadline_exportCSV_new?company='+company+'&company_type='+company_type+'&deadline_type='+deadline_type+'&due_val='+due_val+'" class="dropdown-item waves-light waves-effect but">Csv</a><a href="<?php echo base_url();?>Deadline_manager/pdf_new?company='+company+'&company_type='+company_type+'&deadline_type='+deadline_type+'&due_val='+due_val+'" class="dropdown-item waves-light waves-effect but">Pdf</a><a href="<?php echo base_url();?>Deadline_manager/html_new?company='+company+'&company_type='+company_type+'&deadline_type='+deadline_type+'&due_val='+due_val+'" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>');
                     },
                 });
    });

   function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

</script>


 <!-- Modal one-->
 <?php  foreach ($getCompany as $getCompanykey => $getCompanyvalue) { 
        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                  
              
                  
                  (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                  ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                  
                  (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                  ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                  
                  (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                  ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                  
                  (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                  ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                  
                  (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                  ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                  
                  (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                  ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                  
                  (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                  ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                  
                  (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                  ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                  
                  (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                  ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                  
                  (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                  ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                  
                  (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                  ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                  
                  (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                  ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                  
                  (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                  ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                  
                  (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                  ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                  
                  (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                  ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                  
                  (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                  ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                  /*****************************rspt*************************************/
                     (isset(json_decode($getCompanyvalue['conf_statement'])->invoice) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->invoice : $jsnCst = '';
                  ($jsnCst!='') ? $cst_mail = "checked='checked'" : $cst_mail = "";
                  
                  (isset(json_decode($getCompanyvalue['accounts'])->invoice) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->invoice : $jsnAcc = '';
                  ($jsnAcc!='') ? $acc_mail = "checked='checked'" : $acc_mail = "";
                  
                  (isset(json_decode($getCompanyvalue['company_tax_return'])->invoice) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->invoice : $jsntax = '';
                  ($jsntax!='') ? $tax_mail = "checked='checked'" : $tax_mail = "";
                  
                  (isset(json_decode($getCompanyvalue['personal_tax_return'])->invoice) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->invoice : $jsnp_tax = '';
                  ($jsnp_tax!='') ? $p_tax_mail = "checked='checked'" : $p_tax_mail = "";
                  
                  (isset(json_decode($getCompanyvalue['payroll'])->invoice) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->invoice : $jsnpay = '';
                  ($jsnpay!='') ? $pay_mail = "checked='checked'" : $pay_mail = "";
                  
                  (isset(json_decode($getCompanyvalue['workplace'])->invoice) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->invoice : $jsnworkplace = '';
                  ($jsnworkplace!='') ? $workplace_mail = "checked='checked'" : $workplace_mail = "";
                  
                  (isset(json_decode($getCompanyvalue['vat'])->invoice) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->invoice : $jsnvat = '';
                  ($jsnvat!='') ? $vat_mail = "checked='checked'" : $vat_mail = "";
                  
                  (isset(json_decode($getCompanyvalue['cis'])->invoice) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->invoice : $jsncis = '';
                  ($jsncis!='') ? $cis_mail = "checked='checked'" : $cis_mail = "";
                  
                  (isset(json_decode($getCompanyvalue['cissub'])->invoice) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->invoice : $jsncis_sub = '';
                  ($jsncis_sub!='') ? $cissub_mail  = "checked='checked'" : $cissub_mail     = "";
                  
                  (isset(json_decode($getCompanyvalue['p11d'])->invoice) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->invoice : $jsnp11d = '';
                  ($jsnp11d!='') ? $p11d_mail   = "checked='checked'" : $p11d_mail   = "";
                  
                  (isset(json_decode($getCompanyvalue['bookkeep'])->invoice) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->invoice : $jsnbk = '';
                  ($jsnbk!='') ? $bookkeep_mail     = "checked='checked'" : $bookkeep_mail   = "";
                  
                  (isset(json_decode($getCompanyvalue['management'])->invoice) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->invoice : $jsnmgnt = '';
                  ($jsnmgnt!='') ? $management_mail     = "checked='checked'" : $management_mail     = "";
                  
                  (isset(json_decode($getCompanyvalue['investgate'])->invoice) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->invoice : $jsninvest = '';
                  ($jsninvest!='') ? $investgate_mail   = "checked='checked'" : $investgate_mail     = "";
                  
                  (isset(json_decode($getCompanyvalue['registered'])->invoice) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->invoice : $jsnreg = '';
                  ($jsnreg!='') ? $registered_mail  = "checked='checked'" : $registered_mail     = "";
                  
                  (isset(json_decode($getCompanyvalue['taxadvice'])->invoice) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->invoice : $jsntaxad = '';
                  ($jsntaxad!='') ? $taxadvice_mail     = "checked='checked'" : $taxadvice_mail  = "";
                  
                  (isset(json_decode($getCompanyvalue['taxinvest'])->invoice) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->invoice : $jsntaxinvest = '';
                  ($jsntaxinvest!='') ? $taxinvest_mail     = "checked='checked'" : $taxinvest_mail  = "";
                  /*****************************************************************/
                  ?>
   <div class="modal fade common-schedule-msg1 remind-me-picker" id="desk-popup_<?php echo $getCompanykey;?>" role="dialog">
      <div class="modal-dialog">
          <!-- Modal content-->
           <div class="modal-content">
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Reminder</h4>
            </div>
            <div class="modal-body">
              <!-- reminders -->
             <div class="service_succ"></div>
              <div class="tax-details">
            <table class="table client_table1 text-center dataTable no-footer frclr" id="servicedead12" role="grid" aria-describedby="newinactives_info">
               <thead>
                  <tr class="text-uppercase" role="row">
                     <th class="sorting_asc" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Profile: activate to sort column descending" style="width: 0px;">#</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 0px;">deadline type</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending" style="width: 0px;">reminder (days before)</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 0px;">enabled</th>
                     <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="User Type: activate to sort column ascending" style="width: 0px;">email</th>
                  </tr>
               </thead>
               <tbody>
               <?php $current_date=date('Y-m-d'); ?>
                  <tr role="row" class="">
                     <td class="user_imgs sorting_1">1</td>
                     <td>VAT</td>
                     <td><?php 
                       $res_date=(strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): '');
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;
                    // echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); 
                       ?></td>

                     <td><select name="vat" id="vats_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                            <option value="enable" <?php if($vat!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($vat==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                     <td><select name="vat_mail" id="vatsmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                            <option value="yes" <?php if($vat_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($vat_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                  <tr role="row" class="">
                     <td class="user_imgs sorting_1">2</td>
                     <td>Payroll</td>
                     <td><?php 
                     //echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); 
                      $res_date=(strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); 
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;
                     ?></td>
                   <td><select name="payrolls" id="payrolls_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($pay!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($pay==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                      <td><select name="payrolls_mail" id="payrollsmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($pay_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($pay_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                  <tr role="row" class="">
                     <td class="user_imgs sorting_1">3</td>
                     <td>Accounts</td>
                     <td><?php 
                     //echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); 
                      $res_date=(strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); 
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;

                     ?></td>
                     <td><select name="accounts" id="accountss_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($acc!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($acc==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                     <td><select name="accounts_mail" id="accountsmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($acc_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($acc_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                  <tr role="row" class="">
                     <td class="user_imgs sorting_1">4</td>
                     <td>Confirmation statement</td>
                     <td><?php 
                     //echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): '');
                      $res_date=(strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); 
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;
                      ?></td>
                       <td><select name="confstatements" id="confstatements_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($cst!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($cst==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                      <td><select name="confstatements_mail" id="confstatementsmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($cst_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($cst_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                  <tr role="row" class="">
                     <td class="user_imgs sorting_1">5</td>
                     <td>Company Tax Return</td>
                     <td><?php 
                    // echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); 
                      $res_date=(strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): '');
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;

                     ?></td>
                    <td><select name="companytaxs" id="companytaxs_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($tax!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($tax==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                    <td><select name="companytaxs_mail" id="companytaxsmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($tax_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($tax_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                  <tr role="row" class="">
                     <td class="user_imgs sorting_1">6</td>
                     <td>Personal Tax Return</td>
                     <td><?php //echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); 
                     $res_date=(strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): '');
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;
                     ?></td>
                  <td><select name="personaltaxs" id="personaltaxs_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($p_tax!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($p_tax==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                    <td><select name="personaltaxs_mail" id="personaltaxsmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($p_tax_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($p_tax_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                    <tr role="row" class="">
                     <td class="user_imgs sorting_1">7</td>
                     <td>WorkPlace Pension - AE</td>
                     <td><?php //echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); 
                     $res_date=(strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); 
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;
                     ?></td>
                      <td><select name="workplaces" id="workplaces_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($workplace!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($workplace==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                     <td><select name="workplaces_mail" id="workplacesmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($workplace_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($workplace_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                    <tr role="row" class="">
                     <td class="user_imgs sorting_1">8</td>
                     <td>CIS - Contractor</td>
                     <td>0</td>
                     <td><select name="ciss" id="ciss_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($cis!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($cis==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                      <td><select name="ciss_mail" id="cissmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($cis_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($cis_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                    <tr role="row" class="">
                     <td class="user_imgs sorting_1">9</td>
                     <td>CIS - Sub Contractor</td>
                     <td>0</td>
                     <td><select name="cissubs" id="cissubs_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($cissub!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($cissub==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                      <td><select name="cissubs_mail" id="cissubsmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($cissub_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($cissub_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                    <tr role="row" class="">
                     <td class="user_imgs sorting_1">10</td>
                     <td>P11D</td>
                     <td><?php 
                      //echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): '');
                      $res_date=(strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): '');
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;
                     ?></td>
                     <td><select name="p11ds" id="p11ds_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($p11d!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($p11d==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                      <td><select name="p11ds_mail" id="p11dsmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($p11d_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($p11d_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                    <tr role="row" class="">
                     <td class="user_imgs sorting_1">11</td>
                     <td>Bookkeeping</td>
                     <td><?php //echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): '');

                      $res_date=(strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): '');
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;
                         ?></td>
                    <td><select name="bookkeeps" id="bookkeeps_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($bookkeep!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($bookkeep==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                       <td><select name="bookkeeps_mail" id="bookkeepsmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($bookkeep_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($bookkeep_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                    <tr role="row" class="">
                     <td class="user_imgs sorting_1">12</td>
                     <td>Management Accounts</td>
                     <td><?php 
                     //echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); 
                   $res_date=(strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); 
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;
                     ?></td>
                   <td><select name="managements" id="managements_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($management!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($management==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                       <td><select name="managements_mail" id="managementsmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($management_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($management_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                    <tr role="row" class="">
                     <td class="user_imgs sorting_1">13</td>
                     <td>Investigation Insurance</td>
                     <td><?php 
                     //echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): '');
                     $res_date=(strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); 
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;
                      ?></td>
                    <td><select name="investgates" id="investgates_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($investgate!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($investgate==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                    <td><select name="investgates_mail" id="investgatesmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($investgate_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($investgate_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
              
                    <tr role="row" class="">
                     <td class="user_imgs sorting_1">14</td>
                     <td>Registered Address</td>
                     <td><?php 
                     //echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): '');
                     $res_date=(strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): '');
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;
                      ?></td>
                     <td><select name="registereds" id="registereds_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($registered!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($registered==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                     <td><select name="registereds_mail" id="registeredsmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($registered_mail!=''){ echo "selected"; }?>>Yes</option>
                              <option value="no" <?php if($registered_mail==''){ echo "selected"; }?> >No</option>
                            
                           </select></td>
                  </tr>
                    <tr role="row" class="">
                     <td class="user_imgs sorting_1">15</td>
                     <td>Tax Advice</td>
                     <td><?php 
                     //echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); 
                     $res_date=(strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); 
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;
                     ?></td>
                     <td><select name="taxadvices" id="taxadvices_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($taxadvice!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($taxadvice==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                      <td><select name="taxadvices_mail" id="taxadvicesmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($taxadvice_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($taxadvice_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
                     <tr role="row" class="">
                     <td class="user_imgs sorting_1">16</td>
                     <td>Tax Investigation</td>
                     <td><?php 
                     //echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); 
                     $res_date=(strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); 
                       if($res_date!='')
                       {
                        $date1=date_create($current_date);
                        $date2=date_create($res_date);
                        $diff=date_diff($date1,$date2);
                        $withsym=$diff->format("%R%a");
                        if($withsym>0)
                        {
                            $diff_days=$diff->format("%a");
                        }
                        else
                        {
                            $diff_days=0;
                        }
                       }
                       else
                       {
                        $diff_days=0;
                       }
                        echo $diff_days;
                     ?></td>
                    <td><select name="taxinvests" id="taxinvests_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status" >
                              <option value="enable" <?php if($taxinvest!=''){ echo "selected"; }?>>Enable</option>
                              <option value="disable" <?php if($taxinvest==''){ echo "selected"; }?> >Disable</option>
                            
                           </select></td>
                     <td><select name="taxinvests_mail" id="taxinvestsmail_<?php echo $getCompanyvalue['id'];?>" class="form-control valid deadline_status_email" >
                              <option value="yes" <?php if($taxinvest_mail!=''){ echo "selected"; }?>>yes</option>
                              <option value="no" <?php if($taxinvest_mail==''){ echo "selected"; }?> >no</option>
                            
                           </select></td>
                  </tr>
               </tbody>
            </table>
            </div>
          <!-- reminders -->
            </div>
            <div class="modal-footer">
                 <!--  <button type="button" name="button"  class="btn btn-default submitBtn">Confirm</button> -->
                  <a  class="btn btn-danger" href="#" data-dismiss="modal">Cancel</a>
            </div>
          </div>
      </div>
    </div>
<?php } ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script type="text/javascript">
   $(document).ready(function(){
       $('.edit-toggle1').click(function(){
         $('.proposal-down').slideToggle(300);
       });

        $("#legal_form12").change(function() {
          var val = $(this).val();
          if(val === "from2") {
            // alert('hi');
              $(".from2option").show();
          }
          else {
              $(".from2option").hide();
          }
          });

        // $("#servicedead12").dataTable({
        //  // "iDisplayLength": 10 ,
        //  // "scrollX": true
         
        //  });

          $("#servicedead14").dataTable({
         // "iDisplayLength": 10 ,
         // "scrollX": true
         
         });   

         /******************************/
          $(".status").change(function(e){
     //$('#alluser').on('change','.status',function () {
     //e.preventDefault();
     
     
      var rec_id = $(this).data('id');
      var stat = $(this).val();
      $(".LoadingImage").show();
     $.ajax({
           url: '<?php echo base_url();?>user/statusChange/',
           type: 'post',
           data: { 'rec_id':rec_id,'status':stat },
           timeout: 3000,
           success: function( data ){
               //alert('ggg');
                $(".LoadingImage").hide();
               $(".status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('.status_succ'); });
              // setTimeout(resetAll,3000);
               //location.reload();
               setTimeout(function(){// wait for 5 secs(2)
              //location.reload(); // then reload the page.(3)
              $(".status_succ").hide();
         }, 3000);
               if(stat=='3'){
                   
                    //$this.closest('td').next('td').html('Active');
                    $('#frozen'+rec_id).html('Frozen');
     
               } else {
                    $this.closest('td').next('td').html('Inactive');
               }
               },
               error: function( errorThrown ){
                   console.log( errorThrown );
               }
           });
      });
   
   
   $(".search").change(function(){
  //   alert('zzz');
           $(".LoadingImage").show();
   
       var data = {};
   
      data['com_num'] = $("#companysearch").val();
      data['legal_form'] = $("#legal_form").val();
      //var values = $("button[name='service_fil[]']").map(function(){return $(this).attr("aria-pressed");}).get();
      var f_h = $("#h_f").val();
     // alert(f_h);
      if(f_h)
      {
       var fields = $("button[id='"+f_h+"'").attr('id');
       var values = $("button[id='"+f_h+"'").attr("aria-pressed");
      }else{
       var fields ='';
       var values ='';
      }
var for_fileds=[];
var for_values=[];
       $('.btn-sm').each(function(){
            if ($(this).hasClass("active")) {
                  var fields = $(this).attr('id');
                  for_fileds.push(fields);
                  var values = $(this).attr("aria-pressed");
                  for_values.push(values);
              }
        });
        // var fields = $(this).attr('id');
        // var values = $(this).attr("aria-pressed");
        if(for_fileds.length>0 && for_values.length>0){
        var fields = for_fileds.join(',');
        var values = for_values.join(',');
     }else{
        var fields = '';
        var values = '';
     }

     
      data['values'] = values;
      data['fields'] = fields;
     
      // data['values'] = '';
      // data['fields'] = '';

      $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>Deadline_manager/companysearch/",
               data: data,
               success: function(response) {
                       $(".LoadingImage").hide();
   
               $(".filterrec").html(response);
               $("#servicedead").dataTable({
               "iDisplayLength": 10,
               "scrollX": true,
                   "dom": '<"staffprofilelist01">lfrtip'
               });
                        $('.for_ul_ajax_status').html('');
                        $('.for_ul_ajax_status').html(' <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button><div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url();?>Deadline_manager/deadline_exportCSV_new_status?com_num='+data['com_num']+'&legal_form='+data['legal_form']+'&values='+data['values']+'&fields='+data['fields']+'" class="dropdown-item waves-light waves-effect but">Csv</a><a href="<?php echo base_url();?>Deadline_manager/pdf_new_status?com_num='+data['com_num']+'&legal_form='+data['legal_form']+'&values='+data['values']+'&fields='+data['fields']+'" class="dropdown-item waves-light waves-effect but">Pdf</a><a href="<?php echo base_url();?>Deadline_manager/html_new_status?com_num='+data['com_num']+'&legal_form='+data['legal_form']+'&values='+data['values']+'&fields='+data['fields']+'" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>');

               },
       });
   });

      $("#companysearch").on('keyup',function(){
           $(".LoadingImage").show();
   
       var data = {};
   
      data['com_num'] = $("#companysearch").val();
      data['legal_form'] = $("#legal_form").val();
      //var values = $("button[name='service_fil[]']").map(function(){return $(this).attr("aria-pressed");}).get();
      var f_h = $("#h_f").val();
     // alert(f_h);
      if(f_h)
      {
       var fields = $("button[id='"+f_h+"'").attr('id');
       var values = $("button[id='"+f_h+"'").attr("aria-pressed");
      }else{
       var fields ='';
       var values ='';
      }
     
      // data['values'] = '';
      // data['fields'] = '';
      var for_fileds=[];
var for_values=[];
          $('.btn-sm').each(function(){
            if ($(this).hasClass("active")) {
                  var fields = $(this).attr('id');
                  for_fileds.push(fields);
                  var values = $(this).attr("aria-pressed");
                  for_values.push(values);
              }
        });
        // var fields = $(this).attr('id');
        // var values = $(this).attr("aria-pressed");
        // var fields = for_fileds.join(',');
        // var values = for_values.join(',');
   if(for_fileds.length>0 && for_values.length>0){
        var fields = for_fileds.join(',');
        var values = for_values.join(',');
     }else{
        var fields = '';
        var values = '';
     }
      data['values'] = values;
      data['fields'] = fields;
      $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>Deadline_manager/companysearch/",
               data: data,
               success: function(response) {
                       $(".LoadingImage").hide();
   
               $(".filterrec").html(response);
               $("#servicedead").dataTable({
               "iDisplayLength": 10,
                   "dom": '<"staffprofilelist01">lfrtip'
               });

               $('.for_ul_ajax_status').html('');
                        $('.for_ul_ajax_status').html(' <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button><div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url();?>Deadline_manager/deadline_exportCSV_new_status?com_num='+data['com_num']+'&legal_form='+data['legal_form']+'&values='+data['values']+'&fields='+data['fields']+'" class="dropdown-item waves-light waves-effect but">Csv</a><a href="<?php echo base_url();?>Deadline_manager/pdf_new_status?com_num='+data['com_num']+'&legal_form='+data['legal_form']+'&values='+data['values']+'&fields='+data['fields']+'" class="dropdown-item waves-light waves-effect but">Pdf</a><a href="<?php echo base_url();?>Deadline_manager/html_new_status?com_num='+data['com_num']+'&legal_form='+data['legal_form']+'&values='+data['values']+'&fields='+data['fields']+'" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>');

               },
       });
   });
   
   
   $(document).on('click',".update_service",function(){
       var values = this.id;
   
       if($(this). prop("checked") == true){
       var stat = "on";
       }
       else if($(this). prop("checked") == false){
       var stat = "off";
       }
        $(".LoadingImage").show();
      $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>staff/service_update/",
               data: {'values':values,'stat':stat},
               success: function(response) {
                   $(".LoadingImage").hide();
                   $(".service_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('.service_succ'); });

              /* $(".filterrec").html(response);
               $("#servicedead").dataTable({
               "iDisplayLength": 10,
                   "dom": '<"staffprofilelist01">lfrtip'
               });*/
               },
       });
       
       /* var notChecked = [], checked = [];
       $(":checkbox").map(function() {
           this.checked ? checked.push(this.id) : notChecked.push(this.id);
       });
       alert("checked: " + checked);
       alert("not checked: " + notChecked);*/
   });
   
   //$(".btn-sm").click(function(){
       $(document).on('click',".btn-sm",function(){
      //$(this).attr("aria-pressed")
     $(".LoadingImage").show();
        //var values = $("button[name='service_fil[]']").map(function(){return $(this).attr("aria-pressed");}).get();
        var for_fileds = [];
        var for_values = [];
        $('.btn-sm').each(function(){
            if ($(this).hasClass("active")) {
                  var fields = $(this).attr('id');
                  for_fileds.push(fields);
                  var values = $(this).attr("aria-pressed");
                  for_values.push(values);
              }
        });
        // var fields = $(this).attr('id');
        // var values = $(this).attr("aria-pressed");
        // var fields = for_fileds.join(',');
        // var values = for_values.join(',');
   if(for_fileds.length>0 && for_values.length>0){
        var fields = for_fileds.join(',');
        var values = for_values.join(',');
     }else{
        var fields = '';
        var values = '';
     }
        $("#h_f").val(fields);
      // alert(values);
    var data = {};
   
      data['com_num'] = $("#companysearch").val();
      data['legal_form'] = $("#legal_form").val();
      /*data['com_num'] = '';
      data['legal_form'] = '';*/
      data['values'] = values;
      data['fields'] = fields;

      $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>Deadline_manager/companysearch/",
               data: data,
               success: function(response) {
                     $(".LoadingImage").hide();
               $(".filterrec").html(response);
               $("#servicedead").dataTable({
               "iDisplayLength": 10,
                   "dom": '<"staffprofilelist01">lfrtip'
               });

                $('.for_ul_ajax_status').html('');
                        $('.for_ul_ajax_status').html(' <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button><div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url();?>Deadline_manager/deadline_exportCSV_new_status?com_num='+data['com_num']+'&legal_form='+data['legal_form']+'&values='+data['values']+'&fields='+data['fields']+'" class="dropdown-item waves-light waves-effect but">Csv</a><a href="<?php echo base_url();?>Deadline_manager/pdf_new_status?com_num='+data['com_num']+'&legal_form='+data['legal_form']+'&values='+data['values']+'&fields='+data['fields']+'" class="dropdown-item waves-light waves-effect but">Pdf</a><a href="<?php echo base_url();?>Deadline_manager/html_new_status?com_num='+data['com_num']+'&legal_form='+data['legal_form']+'&values='+data['values']+'&fields='+data['fields']+'" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>');
                        
               },
       });
   
   });
         /********************************/     

         /************************/
            $(document).on('change',".deadline_status",function(){
       var values = this.id;
   
       //if($(this). prop("checked") == true){
         if(this.value=='enable'){
       var stat = "on";
       }
       else{
       var stat = "off";
       }
      // alert(stat);
 $(".LoadingImage").show();
      $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>Deadline_manager/service_update/",
               data: {'values':values,'stat':stat},
               success: function(response) {
                   $(".LoadingImage").hide();
                   // $(".service_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('.service_succ'); });
                    $(".service_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('.service_succ'); });
                   setTimeout(function(){ $(".service_succ").hide();  }, 2000);
                   $(".search").trigger('change');
              /* $(".filterrec").html(response);
               $("#servicedead").dataTable({
               "iDisplayLength": 10,
                   "dom": '<"staffprofilelist01">lfrtip'
               });*/
               },
       });
       
       /* var notChecked = [], checked = [];
       $(":checkbox").map(function() {
           this.checked ? checked.push(this.id) : notChecked.push(this.id);
       });
       alert("checked: " + checked);
       alert("not checked: " + notChecked);*/
   });

                     $(document).on('change',".deadline_status_email",function(){
       var values = this.id;
   //alert(value);
       //if($(this). prop("checked") == true){
         if(this.value=='yes'){
       var stat = "on";
       }
       else{
       var stat = "off";
       }
      // alert(stat);
 $(".LoadingImage").show();
      $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>Deadline_manager/service_update_mail/",
               data: {'values':values,'stat':stat},
               success: function(response) {
                   $(".LoadingImage").hide();
                   $(".service_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('.service_succ'); });
                     setTimeout(function(){ $(".service_succ").hide();  }, 2000);
                   $(".search").trigger('change');
              /* $(".filterrec").html(response);
               $("#servicedead").dataTable({
               "iDisplayLength": 10,
                   "dom": '<"staffprofilelist01">lfrtip'
               });*/
               },
       });
       
       /* var notChecked = [], checked = [];
       $(":checkbox").map(function() {
           this.checked ? checked.push(this.id) : notChecked.push(this.id);
       });
       alert("checked: " + checked);
       alert("not checked: " + notChecked);*/
   });
         /****************************/
   });
   </script>

<script type="text/javascript">
   /** find table row **/
   $(document).ready(function(){

var today = $('#today_deadlineinfo >tbody >tr.for_table').length;
    $('.count_today').html(today);
var thisweek = $('#thisweek_deadlineinfo >tbody >tr.for_table').length;
      $('.count_week').html(thisweek);
 var month = $('#thismonth_deadlineinfo >tbody >tr.for_table').length;
      $('.count_month').html(month);
 var year = $('#thisyear_deadlineinfo >tbody >tr.for_table').length;
      $('.count_year').html(year);

});

   $(document).on('click','.for_link',function(){
      //alert($(this).attr('data-id'));
      $('#active_tabs_name').val($(this).attr('data-id'));

   });


// VAT
// Payroll
// Accounts
// Confirmation statement
// Company Tax Return
// Personal Tax Return
// WorkPlace Pension - AE
// CIS - Contractor
// CIS - Sub Contractor
// P11D
// Bookkeeping
// Management Accounts
// Investigation Insurance
// Registered Address
// Tax Advice
// Tax Investigation

 $(document).ready(function(){        
        <?php
        if(isset($_SESSION['firm_seen'])){ ?>
           $(".this_year").trigger('click');
          <?php
        if($_SESSION['firm_seen']=='VAT'){ ?>
          $("#deadline_type").val('VAT').trigger('change');
          $
       <?php } ?>
       <?php
        if($_SESSION['firm_seen']=='Payroll'){ ?>
         $("#deadline_type").val('Payroll').trigger('change');
       <?php } ?>
       <?php
        if($_SESSION['firm_seen']=='Accounts'){ ?>
         $("#deadline_type").val('Accounts').trigger('change');
       <?php } ?>
       <?php
        if($_SESSION['firm_seen']=='Confirmation statement'){ ?>
         $("#deadline_type").val('Confirmation statement').trigger('change');
       <?php } ?>
       <?php
        if($_SESSION['firm_seen']=='Company Tax Return'){ ?>
         $("#deadline_type").val('Company Tax Return').trigger('change');
       <?php } ?>
       <?php
        if($_SESSION['firm_seen']=='Personal Tax Return'){ ?>
        $("#deadline_type").val('Personal Tax Return').trigger('change');
       <?php } ?>

       <?php
        if($_SESSION['firm_seen']=='WorkPlace Pension - AE'){ ?>
        $("#deadline_type").val('WorkPlace Pension - AE').trigger('change');
       <?php } ?>

       <?php
        if($_SESSION['firm_seen']=='CIS - Contractor'){ ?>
        $("#deadline_type").val('CIS - Contractor').trigger('change');
       <?php } ?>

       <?php
        if($_SESSION['firm_seen']=='CIS - Sub Contractor'){ ?>
        $("#deadline_type").val('CIS - Sub Contractor').trigger('change');
       <?php } ?>

       <?php
        if($_SESSION['firm_seen']=='P11D'){ ?>
        $("#deadline_type").val('P11D').trigger('change');
       <?php } ?>

       <?php
        if($_SESSION['firm_seen']=='Bookkeeping'){ ?>
        $("#deadline_type").val('Bookkeeping').trigger('change');
       <?php } ?>

       <?php
        if($_SESSION['firm_seen']=='Management Accounts'){ ?>
        $("#deadline_type").val('Management Accounts').trigger('change');
       <?php } ?>

       <?php
        if($_SESSION['firm_seen']=='Investigation Insurance'){ ?>
        $("#deadline_type").val('Investigation Insurance').trigger('change');
       <?php } ?>

       <?php
        if($_SESSION['firm_seen']=='Registered Address'){ ?>
        $("#deadline_type").val('Registered Address').trigger('change');
       <?php } ?>

       <?php
        if($_SESSION['firm_seen']=='Tax Advice'){ ?>
        $("#deadline_type").val('Tax Advice').trigger('change');
       <?php } ?>
       <?php
        if($_SESSION['firm_seen']=='Tax Investigation'){ ?>
        $("#deadline_type").val('Tax Investigation').trigger('change');
       <?php } ?>
         <?php } ?>

});

function initialize_yeardatatable()
{
    <?php 
          $column_setting = Firm_column_settings('deadline');

        ?>

       var column_order = <?php echo $column_setting['order'] ?>;
        
       var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;

        var column_ordering = [];

        $.each(column_order,function(i,v){

          var index = $('#thisyear_deadlineinfo thead  th.'+v).index();

          if(index!==-1)
          {
            column_ordering.push(index);
          }
        });

    var numCols = $('#thisyear_deadlineinfo thead th').length;   
    //alert(numCols);
        var table40 = $('#thisyear_deadlineinfo').DataTable({
         "dom": 'B<"toolbar-table">lfrtip',
           buttons: [
            {
                extend: 'colvis',
                columns: ':not(.Exc-colvis)'
            }
          ],
          order: [],//ITS FOR DISABLE SORTING
         
        
          fixedHeader: {
            header: true,
            footer: true
        },
         
         colReorder: 
          {
            realtime: false,
            order:column_ordering,
            fixedColumnsLeft : 1

          },
       initComplete: function () { 


                                 var api = this.api();

              api.columns('.hasFilter').every( function () {

                        var column = this;
                        var TH = $(column.header());
                        var Filter_Select = TH.find('.filter_check');
                        ///alert(Filter_Select.length);
                        // alert('check');
                    if( !Filter_Select.length )
                    {
                         // alert('check1');
                      Filter_Select = $('<select multiple="true" class="filter_check" style="display:none"></select>').appendTo( TH );
                      var unique_data = [];
                        column.nodes().each( function ( d, j ) { 
                          var dataSearch = $(d).attr('data-search');
                          
                            if( jQuery.inArray(dataSearch, unique_data) === -1 )
                            {
                              //console.log(d);
                              Filter_Select.append( '<option  value="'+dataSearch+'">'+dataSearch+'</option>' );
                              unique_data.push( dataSearch );
                            }

                        });
                    }
                    

                    Filter_Select.on( 'change', function () {

                        var search =  $(this).val(); 
                        //console.log( search );
                        if(search.length) search= '^('+search.join('|') +')$'; 

                       var  class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);  

                       var cur_column = api.column( '.'+class_name+'_TH' );
                       
                        cur_column.search( search, true, false ).draw();
                    } );
                      
                        //console.log(select.attr('style'));
                        Filter_Select.formSelect(); 

                   
                      }); 

                
        }
    });

                     ColVis_Hide( table40 , hidden_coulmns );

      Filter_IconWrap();

      Change_Sorting_Event( table40 ); 

      ColReorder_Backend_Update ( table40 , 'deadline' );

      ColVis_Backend_Update ( table40 , 'deadline' );

      
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
      
        //console.log("inside");

        var CreateTime =  data[data.length].trim() ;
        if(!CreateTime)
        {         
          return true;
        }
        var minDate = $("#clientCreate_FromDate");
       

        var maxDate = $("#clientCreate_ToDate");
        

         if ( minDate.length && minDate.val().length ) 
         {
            //console.log(minDate);

            minDate = minDate.val().split('-');
            minDate.reverse();
           
            
            var minDate = minDate[0]+minDate[1]+minDate[2];
           
            if (parseInt( CreateTime) < parseInt(minDate) )
            { 
              return false;
            }
         }

        if (maxDate.length && maxDate.val().length)
        {

          maxDate = maxDate.val().split('-');
          maxDate.reverse();

          maxDate =  maxDate[0]+maxDate[1]+maxDate[2];

         //console.log("inside maxDate"+new Date(maxDate[0]+"-"+maxDate[1]+"-"+maxDate[2]));

          if ( parseInt( CreateTime ) > parseInt( maxDate ) )
          {
            return false;
          }
        }


          //console.log("return true"+CreateTime);

    return true;
    }
);

     

        $(document).on("click",".task_status_val a",function(event){

          console.log($(this).data('id'));

          if($(this).data('id')=='today')
          {
            var today='<?php echo date("d-m-Y") ?>';

            $("#clientCreate_FromDate").val(today);
            $("#clientCreate_ToDate").val(today);

          }
           else if($(this).data('id')=='this-week')
          {
            var start_date='<?php echo date('d-m-Y', strtotime('-7 days'))?>';
            var today='<?php echo date("d-m-Y") ?>';

            $("#clientCreate_FromDate").val(start_date);
            $("#clientCreate_ToDate").val(today);

          }
          else if($(this).data('id')=='this-month')
          {
                var start_date=';<?php echo date('01-m-Y'); ?>';
            var today='<?php echo date("d-m-Y") ?>';

            $("#clientCreate_FromDate").val(start_date);
            $("#clientCreate_ToDate").val(today);
            
          }
          else
          {
            var start_date='<?php echo date('01-01-Y'); ?>';
            var today='<?php echo date("d-m-Y") ?>';

            $("#clientCreate_FromDate").val(start_date);
            $("#clientCreate_ToDate").val(today);
            
          }

        //   var minDate = $("#clientCreate_FromDate");
       

        // var maxDate = $("#clientCreate_ToDate");


          table40.draw();

          });



}

$(document).ready(function(){



    var check=0;
    var check1=0;
    var numCols = $('#today_deadlineinfo thead th').length;
        var table10 = $('#today_deadlineinfo').DataTable({
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=0;
                     $('#today_deadlineinfo thead th').find('.filter_check').each( function(){                    
                       $(this).attr('id',"today_"+q);
                       q++;
                     });
                  for(i=0;i<numCols;i++){               
                          var select = $("#today_"+i);                  
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      });                    
                     $("#today_"+i).formSelect();  
                  }
        }
    });
      for(j=0;j<numCols;j++){  
          $('#today_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#today_'+c+ ' option:selected'), function(){                
                  search.push($(this).val());
              });      
              search = search.join('|');           

              table10.column(c).search(search, true, false).draw();  
          });
       }


    var check=0;
    var check1=0;
    var numCols = $('#thisweek_deadlineinfo thead th').length;   
    //alert(numCols);
        var table20 = $('#thisweek_deadlineinfo').DataTable({
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=0;
                     $('#thisweek_deadlineinfo thead th').find('.filter_check').each( function(){
                     // alert('ok');
                       $(this).attr('id',"week_"+q);
                       q++;
                     });
                  for(i=0;i<numCols;i++){              
                          var select = $("#week_"+i); 


                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 
           
                     $("#week_"+i).formSelect();  
                  }
        }
    });
      for(j=0;j<numCols;j++){  
          $('#week_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#week_'+c+ ' option:selected'), function(){                
                  search.push($(this).val());
              });      
              search = search.join('|');    
              table20.column(c).search(search, true, false).draw();  
          });
       }


        var check=0;
    var check1=0;
    var numCols = $('#thismonth_deadlineinfo thead th').length;   
    //alert(numCols);
        var table30 = $('#thismonth_deadlineinfo').DataTable({
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=0;
                     $('#thismonth_deadlineinfo thead th').find('.filter_check').each( function(){
                     // alert('ok');
                       $(this).attr('id',"month_"+q);
                       q++;
                     });
                  for(i=0;i<numCols;i++){              
                          var select = $("#month_"+i); 


                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 
           
                     $("#month_"+i).formSelect();  
                  }
        }
    });
      for(j=0;j<numCols;j++){  
          $('#month_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#month_'+c+ ' option:selected'), function(){                
                  search.push($(this).val());
              });      
              search = search.join('|');            

              table30.column(c).search(search, true, false).draw();  
          });
       }


 initialize_yeardatatable();



});



 // $(document).on("change",".select_client",function(event)
 //    {
 //      var IsArchive=$('.tap_click.active').attr("data-id").trim();
      
 //      var tr=0;
 //      var ck_tr=0;
 //      table40.column('.select_row_TH').nodes().to$().each(function(index) {
 //      if( $(this).find(".select_client").is(":checked") )
 //        {
 //          ck_tr++;
 //        }
 //        tr++;
 //      });

 //        if(tr==ck_tr)
 //        {
 //          $("#select_all_client").prop("checked",true);
 //        } 
 //        else
 //        {
 //          $("#select_all_client").prop("checked",false);
 //        } 
 //      if(ck_tr)
 //      {
 //        toggle_action_button(1,IsArchive);
 //      }
 //      else  
 //      {
 //        toggle_action_button(0,IsArchive);
 //      } 
   
 //      $("#select_client_count").val(ck_tr+ " Selected");
       
 //    });





</script>
 <!--   Datatable JS by Ram -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url()?>assets/js/user_page/materialize.js"></script> 

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>

<script src="<?php echo base_url()?>assets/js/custom/buttons.colVis.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom/datatable_extension.js"></script>
 <!--   End JS by Ram -->