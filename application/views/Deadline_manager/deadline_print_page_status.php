<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://remindoo.org/CRMTool/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <style>
       table#alluser th, table#alluser td {
          padding: 10px;
          text-align: left;
          white-space: nowrap;
          font-family: 'Roboto', sans-serif;
          font-size: 14px;
          border: 1px solid #eee;
      }
      button.data-turn99.newonoff {
       background: transparent;
       border: none;
      }
      button.print-btn {
          background: #2ea7ec;
          color: #fff;
          border: none;
          padding: 8px 12px 7px;
          margin: 20px 30px;
          border-radius: 3px;
         }
         i.fa.fa-print {
          font-size: 22px;
      }
    </style>
  </head>
  <body>
    <center><h4><?php echo $title; ?></h4></center>
    <?php $th_css = 'style="border: 1px solid #eee;padding: 10px;font-size: 13px;white-space: nowrap;font-family: "Roboto", sans-serif;text-align: left;"';
    $tr_css = 'style="border: 1px solid #eee;padding: 10px;font-size: 13px;white-space: nowrap;"';
    ?>

<div class="dataTables_scroll">
   <button class="print-btn print_data_cls" ><i class="fa fa-print" aria-hidden="true"></i></button>

   <div class="dataTables_scrollBody" style="position: relative;width: 80%;
    margin: 50px auto;">
    <!-- for today -->
    <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%">
    <thead>
    <!-- <tr class="text-uppercase"><th colspan="5">Today</th></tr> -->
    <tr class="text-uppercase">
      <th <?php echo $th_css;?>>Client Name</th>
      <th <?php echo $th_css;?>>Client Type</th>
      <?php foreach($service_list as $key => $value){ ?>
      <th <?php echo $th_css;?>><?php echo $value['service_name']; ?></th>
      <?php } ?>
    </tr>
    </thead>
    <tbody>
    <?php        
      foreach($services as $key => $value) 
      {  ?>
      <tr class="for_table">
         <td><a><?php echo $value['crm_name']; ?></a></td>
         <td><?php echo $value['crm_legal_form']; ?></td>
         <?php foreach($service_list as $key1 => $value1){ ?>
         <td><?php  echo 'Due: '.$value[$value1['service_name']]['charge'].',Basic: '.$value[$value1['service_name']]['basic']; ?></td>  
         <?php } ?>            
      </tr>
    <?php } ?>
    </tbody>
    </table>
  <!-- end of this year-->

   </div>
</div>
</body>
</html>
<script>
  $(document).on('click',".print_data_cls",function(){
 

      var divContents = $(".dataTables_scrollBody").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title><?php echo $title; ?></title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();


  });
</script>