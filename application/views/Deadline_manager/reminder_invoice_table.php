<style type="text/css">
  .form-error {
    color: red !important;
  }

  .send-label {
    padding: 10px;
  }

  .send-label input {
    margin-top: 6px !important;
  }
</style>
<!-- company details -->

<!-- info table -->
<div class="client_section3 table-responsive floating_set filterrec">

  <div class="page-data-block">
    <div class="rem-card">
      <!--  <div class="service_succ"></div> -->
      <table class="table client_table1 text-center dataTable no-footer" id="servicedead14" role="grid" aria-describedby="newinactives_info">
        <thead>
          <tr class="text-uppercase">
            <th>Client Name</th>
            <th>Client Type</th>
            <?php foreach ($service_list as $key => $value) { ?>
              <th colspan="3"><?php echo $value['service_name']; ?></th>
            <?php } ?>
          </tr>
        </thead>
        <tfoot class="ex_data1">
          <tr>
            <th>
            </th>
          </tr>
        </tfoot>
        <tbody class="user-dashboard-section1">
          <tr class="for_table">
            <td></td>
            <td></td>
            <?php foreach ($service_list as $key => $value) { ?>
              <td colspan="1">Basic</td>
              <td colspan="1"></td>
              <td colspan="1">Due</td>
            <?php } ?>
          </tr>
          <?php

          foreach ($service_charges as $key => $value) {  ?>
            <tr class="for_table">
              <td><a href="javascript:;" data-toggle="modal" data-target="#desk-popup_<?php echo $value['id']; ?>"><?php echo $value['crm_name']; ?></a></td>
              <td><?php echo $value['crm_legal_form']; ?></td>
              <?php foreach ($service_list as $key1 => $value1) { ?>
                <td colspan="1"><?php echo $value[$value1['service_name']]['basic']; ?></td>
                <?php if ($value[$value1['service_name']]['enable'] == 'enable' && ($_SESSION['permission']['Deadline_manager']['edit'] == '1' || $_SESSION['permission']['Deadline_manager']['create'] == '1')) { ?>
                  <td colspan="1"><a href="javascript:void(0);" data-toggle="modal" data-target="<?php echo "#edit_basic_" . $value['id'] . $value1['id']; ?>" data-id="<?php echo $value1['id']; ?>"><i class="fa fa-edit"></i></a></td>
                <?php } else { ?><td colspan="1"></td><?php } ?>
                <td colspan="1"><?php echo $value[$value1['service_name']]['charge']; ?></td>
              <?php } ?>
            </tr>

          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php

foreach ($service_charges as $key => $value) {

  $client_subscribed_services = json_decode($value['client_subscribed_services']);
  $item_name = explode(',', $client_subscribed_services->item_name);
  $quantity = explode(',', $client_subscribed_services->quantity);
  $unit_price = explode(',', $client_subscribed_services->amount);
  $discount = explode(',', $client_subscribed_services->discount);
  $tax_amount = explode(',', $client_subscribed_services->tax_amount);

  foreach ($service_list as $key1 => $value1) {

    if ($value[$value1['service_name']]['enable'] == 'enable') {

?>

      <div class="modal fade" id="<?php echo "edit_basic_" . $value['id'] . $value1['id']; ?>" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo $value1['service_name'] . " - Subscription"; ?></h4>
            </div>
            <div class="modal-body">
              <?php

              $qty = "";
              $uprice = "";
              $dis = "";
              $taxamount = "";

              if (!empty($client_subscribed_services) && in_array($value1['service_name'], $item_name)) {
                $keys = array_search($value1['service_name'], $item_name);
                $qty = $quantity[$keys];
                $uprice = $unit_price[$keys];
                $dis = $discount[$keys];
                $taxamount = $tax_amount[$keys];
              }

              ?>
              <form id="<?php echo "basic_form_" . $value['id'] . $value1['id']; ?>" novalidate="novalidate" accept-charset="utf-8" method="POST" action="<?php echo base_url() . 'Deadline_manager/updateSubscription'; ?>">

                <input type="hidden" name="client_id" value="<?php echo $value['id']; ?>">
                <input type="hidden" name="REQUEST_URI" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
                <div class="send-label">
                  <span class="days1">Name</span>
                  <input type="text" name="item_name" value="<?php echo $value1['service_name']; ?>" readonly>
                </div>

                <div class="send-label">
                  <span class="days1">Quantity</span>
                  <input type="text" name="quantity" data-validation="required,number" value="<?php if (!empty($qty)) {
                                                                                                echo $qty;
                                                                                              } ?>">
                </div>

                <div class="send-label">
                  <span class="days1">Unit Price</span>
                  <input type="text" name="amount" data-validation="required,number" value="<?php if (!empty($uprice)) {
                                                                                              echo $uprice;
                                                                                            } ?>">
                </div>

                <div class="send-label">
                  <span class="days1">Discount Amount</span>
                  <input type="text" name="discount" data-validation="number" value="<?php if (!empty($dis)) {
                                                                                        echo $dis;
                                                                                      } ?>">
                </div>

                <div class="send-label">
                  <span class="days1">Tax Amount</span>
                  <input type="text" name="tax_amount" data-validation="number" value="<?php if (!empty($taxamount)) {
                                                                                          echo $taxamount;
                                                                                        } ?>">
                </div>

            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-default basic_save">Save</button>
              <a class="btn btn-danger" href="#" data-dismiss="modal">Cancel</a>
            </div>
            </form>
          </div>
        </div>
      </div>

<?php }
  }
} ?>


<script>
  $.validate({
    lang: 'en'
  });
</script>