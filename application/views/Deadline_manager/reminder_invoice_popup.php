<!-- Modal one-->
<?php  

$columns = array('1'=> 'conf_statement','2' =>'accounts','3'=>'company_tax_return','4'=>'personal_tax_return','5' => 'vat','6' => 'payroll','7' => 'workplace', '8' => 'cis', '9' => 'cissub','10'=>'bookkeep','11' => 'p11d','12' => 'management','13' => 'investgate','14'=>'registered','15'=>'taxinvest','16' => 'taxadvice');
$current_date = date('Y-m-d'); 
$columns_check = array('1'=> 'crm_confirmation_statement_due_date','2' =>'crm_ch_accounts_next_due','3'=>'crm_accounts_tax_date_hmrc','4'=>'crm_personal_due_date_return','5' => 'crm_vat_due_date','6' => 'crm_rti_deadline','7' => 'crm_pension_subm_due_date', '8' => '', '9' => '','10'=>'crm_next_booking_date','11' => 'crm_next_p11d_return_due','12' => 'crm_next_manage_acc_date','13' => 'crm_insurance_renew_date','14'=>'crm_registered_renew_date','15'=>'crm_investigation_end_date','16' => 'crm_investigation_end_date');   
$select = array('1'=> 'confstatements','2' =>'accountss','3'=>'companytaxs','4'=>'personaltaxs','5' => 'vats','6' => 'payrolls','7' => 'workplaces', '8' => 'ciss', '9' => 'cissubs','10'=>'bookkeeps','11' => 'p11ds','12' => 'managements','13' => 'investgate','14'=>'registereds','15'=>'taxinvests','16' => 'taxadvices');

$other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

foreach($other_services as $key => $value) 
{ 
   $columns[$value['id']] = $value['services_subnames'];
   $columns_check[$value['id']] = 'crm_'.$value['services_subnames'].'_statement_date';
   $select[$value['id']] = str_replace('_', '.', $value['services_subnames']);
}



foreach ($getCompany as $getCompanykey => $getCompanyvalue)
{ 
   $getusername = $this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
   $datas = array();

   $service_list = $this->Common_mdl->getClientServices($getCompanyvalue['user_id']); 

    foreach ($service_list as $key => $value) 
    { 
       (isset(json_decode($getCompanyvalue[$columns[$value['id']]])->reminder) && $getCompanyvalue[$columns[$value['id']]] != '') ? $datas['reminder'][$value['id']] = "checked" : $datas['reminder'][$value['id']] = "";

       (isset(json_decode($getCompanyvalue[$columns[$value['id']]])->invoice) && $getCompanyvalue[$columns[$value['id']]] != '') ? $datas['invoice'][$value['id']] = "checked" : $datas['invoice'][$value['id']] = "";
    } 

?>

  <div class="modal fade common-schedule-msg1 remind-me-picker" id="desk-popup_<?php echo $getCompanyvalue['id'];?>" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Reminder</h4>
  </div>
  <div class="modal-body">
  <!-- reminders -->
  <div class="service_succ" id="suc_<?php echo $getCompanyvalue['id'];?>"></div>
  <div class="tax-details">
  <table class="table client_table1 text-center dataTable no-footer frclr" id="servicedead12" role="grid" aria-describedby="newinactives_info">
  <thead>
  <tr class="text-uppercase" role="row">
  <th class="sorting_asc" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Profile: activate to sort column descending" style="width: 0px;">#</th>
  <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 0px;">deadline type</th>
  <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending" style="width: 0px;">Days For Due</th>
  <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 0px;">Reminder</th>
  <th class="sorting" tabindex="0" aria-controls="newinactives" rowspan="1" colspan="1" aria-label="User Type: activate to sort column ascending" style="width: 0px;">Invoice</th>
  </tr>
  </thead>
  <tbody>
      <?php 

      $j =1;
      foreach($service_list as $key => $value){
      ?>

      <tr role="row" class="">
      <td class="user_imgs sorting_1"><?php echo $j; ?></td>
      <td><?php echo $value['service_name']; ?></td>
      <td><?php 
      $res_date = (strtotime($getCompanyvalue[$columns_check[$value['id']]]) !='' ?  date('Y-m-d', strtotime($getCompanyvalue[$columns_check[$value['id']]])): '');

      if($res_date!='')
      {
          $date1=date_create($current_date);
          $date2=date_create($res_date);
          $diff=date_diff($date1,$date2);
          $withsym=$diff->format("%R%a");

          if($withsym>0)
          {
             $diff_days = $diff->format("%a");
          }
          else
          {
             $diff_days = 0;
          }
      }
      else
      {
         $diff_days = 0;
      }

      echo $diff_days." Days"; 
     
      ?></td>

      <td><select <?php if($_SESSION['permission']['Reminder_Settings']['edit'] != '1') { echo "disabled"; } ?>  name="<?php echo $select[$value['id']]; ?>" id="<?php echo $select[$value['id']].'_'.$getCompanyvalue['id']; ?>" class="form-control valid deadline_status" >
      <option value="enable" <?php if($datas['reminder'][$value['id']] != ''){ echo "selected"; }?>>Enable</option>
      <option value="disable" <?php if($datas['reminder'][$value['id']] == ''){ echo "selected"; }?>>Disable</option>
      </select></td>

      <td><select <?php if($_SESSION['permission']['Invoices']['edit'] != '1') { echo "disabled"; } ?> name="<?php echo $select[$value['id']]; ?>_mail" id="<?php echo $select[$value['id']].'mail_'.$getCompanyvalue['id']; ?>" class="form-control valid deadline_status_email" >
      <option value="yes" <?php if($datas['invoice'][$value['id']] != ''){ echo "selected"; }?>>yes</option>
      <option value="no" <?php if($datas['invoice'][$value['id']] == ''){ echo "selected"; }?> >no</option>
      </select></td>

      </tr>  
      <?php $j++; } ?>   
  </tbody>
  </table>
  </div>
  <!-- reminders -->
  </div>
  <div class="modal-footer">
  <!--  <button type="button" name="button"  class="btn btn-default submitBtn">Confirm</button> -->
  <a  class="btn btn-danger" href="#" data-dismiss="modal">Cancel</a>
  </div>
  </div>
  </div>
  </div>

<?php } ?>