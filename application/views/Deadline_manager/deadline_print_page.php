<!--  <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool//assets/css/dataTables.css">
 <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/common_style.css?ver=4"> -->
 <!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://remindoo.org/CRMTool/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>

          <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <style>
       table#alluser th, table#alluser td {
          padding: 10px;
          text-align: left;
          white-space: nowrap;
          font-family: 'Roboto', sans-serif;
          font-size: 14px;
          border: 1px solid #eee;
      }
      button.data-turn99.newonoff {
       background: transparent;
       border: none;
      }
      button.print-btn {
          background: #2ea7ec;
          color: #fff;
          border: none;
          padding: 8px 12px 7px;
          margin: 20px 30px;
          border-radius: 3px;
         }
         i.fa.fa-print {
          font-size: 22px;
      }
    </style>
  </head>
  <body>
    <?php $th_css = 'style="border: 1px solid #eee;padding: 10px;font-size: 13px;white-space: nowrap;font-family: "Roboto", sans-serif;text-align: left;"';
    $tr_css = 'style="border: 1px solid #eee;padding: 10px;font-size: 13px;white-space: nowrap;"';
    ?>

<div class="dataTables_scroll">
   <button class="print-btn print_data_cls" >Print<i class="fa fa-print" aria-hidden="true"></i></button>

   <div class="dataTables_scrollBody" style="position: relative;width: 80%;
    margin: 50px auto;">
    <!-- for today -->
    <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%">
                                    <thead>
                                    <tr class="text-uppercase"><th colspan="5">Today</th></tr>
                                  <tr class="text-uppercase">
                                       <th <?php echo $th_css;?> >Client Name</th>
                                       <th <?php echo $th_css;?> >Client Type</th>
                                       <th <?php echo $th_css;?> >Company Name</th>
                                       <th <?php echo $th_css;?> >Deadline Type</th>
                                       <th <?php echo $th_css;?> >Date</th>
                                  </tr>
                                    </thead>
                                    <tbody>
                                       <!-- <tr>
                                          <td  <?php echo $tr_css;?> >demo</td>
                                          <td  <?php echo $tr_css;?> >demo</td>
                                          <td  <?php echo $tr_css;?> >demo</td>
                                          <td  <?php echo $tr_css;?> >2018-05-02</td>
                                          </tr> -->
                                       <?php 
                                       $current_date=date("Y-m-d");
                                          foreach ($getCompany_today as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                                          ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                                          
                                          (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                                          ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                                          
                                          (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                                          ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                                          ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                                          ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                                          
                                          (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                          ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                                          
                                          (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                          ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                          ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                                          ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                          ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                                          ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                                          ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                                          ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                                          ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                                          ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                          ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                                          ?>
                                          <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))){ ?>
                                       <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Confirmation statement Due Date</td>
                                          <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
                                       </tr>
                                          <?php } ?>
                                          <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td  <?php echo $tr_css;?> >Accounts Due Date</td>
                                          
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
                                          <!-- account -->
                                           </tr>
                                           <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                           <tr>
                                           <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Company Tax Return Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
                                           <!--company tax -->
                                         </tr>
                                         <?php } ?>
                                          <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return']))){ ?>
                                         <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td  <?php echo $tr_css;?> >Personal Tax Return Due Date</td>
                                          <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
                                          <!-- crm personal tax -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']))){ ?>
                                          <tr>
                                            <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >VAT Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
                                          <!-- vat -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))){ ?>
                                          <tr>
                                            <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td  <?php echo $tr_css;?> >Payroll Due Date</td>
                                             <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
                                          <!-- payrol -->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >WorkPlace Pension - AE Due Date</td>
                                            <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
                                          <!--workplace -->
                                          </tr>
                                          <?php } ?>
                                           
                                          <tr>
                                           <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td  <?php echo $tr_css;?> >CIS - Contractor Due Date</td>
                                          <td  <?php echo $tr_css;?> ></td>
                                          <!-- CIS - Contractor -->
                                          </tr>
                                         
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >CIS - Sub Contractor Due Date</td>
                                          <td  <?php echo $tr_css;?> ></td>
                                          <!-- CIS - Sub Contractor-->
                                          </tr>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >P11D Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
                                          <!-- p11d -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Management Accounts Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
                                          <!-- manage account -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Bookkeeping Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
                                          <!-- booking-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Investigation Insurance Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
                                          <!-- insurance -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Registered Address Due Date</td>
                                         <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
                                          <!-- registed -->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Tax Advice Due Date</td>
                                        <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax advice-->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Tax Investigation Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax inve-->
                                          </tr>
                                      <?php } ?>
                                       <?php   }  ?>
                                    </tbody>
    </table>
<!-- end of today -->
<!-- this week -->
    <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%">
                                    <thead>
                                    <tr class="text-uppercase"><th colspan="5">This Week</th></tr>
                                  <tr class="text-uppercase">
                                       <th <?php echo $th_css;?> >Client Name</th>
                                       <th <?php echo $th_css;?> >Client Type</th>
                                       <th <?php echo $th_css;?> >Company Name</th>
                                       <th <?php echo $th_css;?> >Deadline Type</th>
                                       <th <?php echo $th_css;?> >Date</th>
                                  </tr>
                                    </thead>
                                       <tbody>
                                       <?php 
                                       $time=date('Y-m-d');
                                        $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
                                          foreach ($getCompany_oneweek as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                                          ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                                          
                                          (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                                          ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                                          
                                          (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                                          ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                                          ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                                          ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                                          
                                          (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                          ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                                          
                                          (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                          ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                          ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                                          ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                          ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                                          ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                                          ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                                          ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                                          ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                                          ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                          ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                                          ?>
                                      <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))){ ?>
                                       <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Confirmation statement Due Date</td>
                                          <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
                                       </tr>
                                          <?php } ?>
                                            <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td  <?php echo $tr_css;?> >Accounts Due Date</td>
                                          
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
                                          <!-- account -->
                                           </tr>
                                           <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                           <tr>
                                           <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Company Tax Return Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
                                           <!--company tax -->
                                         </tr>
                                         <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                         <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td  <?php echo $tr_css;?> >Personal Tax Return Due Date</td>
                                          <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
                                          <!-- crm personal tax -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']))){ ?>
                                          <tr>
                                            <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >VAT Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
                                          <!-- vat -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))){ ?>
                                          <tr>
                                            <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td  <?php echo $tr_css;?> >Payroll Due Date</td>
                                             <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
                                          <!-- payrol -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >WorkPlace Pension - AE Due Date</td>
                                            <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
                                          <!--workplace -->
                                          </tr>
                                          <?php } ?>
                                           
                                          <tr>
                                           <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td  <?php echo $tr_css;?> >CIS - Contractor Due Date</td>
                                          <td  <?php echo $tr_css;?> ></td>
                                          <!-- CIS - Contractor -->
                                          </tr>
                                         
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >CIS - Sub Contractor Due Date</td>
                                          <td  <?php echo $tr_css;?> ></td>
                                          <!-- CIS - Sub Contractor-->
                                          </tr>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >P11D Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
                                          <!-- p11d -->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Management Accounts Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
                                          <!-- manage account -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Bookkeeping Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
                                          <!-- booking-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Investigation Insurance Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
                                          <!-- insurance -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Registered Address Due Date</td>
                                         <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
                                          <!-- registed -->
                                          </tr>
                                          <?php } ?>
                                         <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Tax Advice Due Date</td>
                                        <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax advice-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Tax Investigation Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax inve-->
                                          </tr>
                                      <?php } ?>
                                       <?php   }  ?>
                                    </tbody>

                                 </table>
  <!-- end of this week -->
  <!--- this month -->
   <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%">
                                    <thead>
                                    <tr class="text-uppercase"><th colspan="5">This Month</th></tr>
                                  <tr class="text-uppercase">
                                       <th <?php echo $th_css;?> >Client Name</th>
                                       <th <?php echo $th_css;?> >Client Type</th>
                                       <th <?php echo $th_css;?> >Company Name</th>
                                       <th <?php echo $th_css;?> >Deadline Type</th>
                                       <th <?php echo $th_css;?> >Date</th>
                                    </tr>
                                    </thead>
                                       <tbody>
                                     
                                       <?php 
                                       $time=date('Y-m-d');
                                        $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
                                          foreach ($getCompany_onemonth as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                                          ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                                          
                                          (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                                          ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                                          
                                          (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                                          ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                                          ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                                          ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                                          
                                          (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                          ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                                          
                                          (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                          ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                          ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                                          ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                          ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                                          ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                                          ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                                          ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                                          ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                                          ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                          ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                                          ?>
                                       <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))){ ?>
                                       <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Confirmation statement Due Date</td>
                                          <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
                                       </tr>
                                          <?php } ?>
                                            <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td  <?php echo $tr_css;?> >Accounts Due Date</td>
                                          
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
                                          <!-- account -->
                                           </tr>
                                           <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                           <tr>
                                           <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Company Tax Return Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
                                           <!--company tax -->
                                         </tr>
                                         <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                         <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td  <?php echo $tr_css;?> >Personal Tax Return Due Date</td>
                                          <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
                                          <!-- crm personal tax -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']))){ ?>
                                          <tr>
                                            <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >VAT Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
                                          <!-- vat -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))){ ?>
                                          <tr>
                                            <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td  <?php echo $tr_css;?> >Payroll Due Date</td>
                                             <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
                                          <!-- payrol -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >WorkPlace Pension - AE Due Date</td>
                                            <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
                                          <!--workplace -->
                                          </tr>
                                          <?php } ?>
                                           
                                          <tr>
                                           <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td  <?php echo $tr_css;?> >CIS - Contractor Due Date</td>
                                          <td  <?php echo $tr_css;?> ></td>
                                          <!-- CIS - Contractor -->
                                          </tr>
                                         
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >CIS - Sub Contractor Due Date</td>
                                          <td  <?php echo $tr_css;?> ></td>
                                          <!-- CIS - Sub Contractor-->
                                          </tr>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >P11D Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
                                          <!-- p11d -->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Management Accounts Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
                                          <!-- manage account -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Bookkeeping Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
                                          <!-- booking-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Investigation Insurance Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
                                          <!-- insurance -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Registered Address Due Date</td>
                                         <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
                                          <!-- registed -->
                                          </tr>
                                          <?php } ?>
                                         <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Tax Advice Due Date</td>
                                        <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax advice-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Tax Investigation Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax inve-->
                                          </tr>
                                      <?php } ?>
                                       <?php   }  ?>
                                    </tbody>

                                 </table>
  <!-- end of this month -->
  <!-- this year -->
     <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%">
                                    <thead>
                                    <tr class="text-uppercase"><th colspan="5">This Year</th></tr>
                                  <tr class="text-uppercase">
                                       <th <?php echo $th_css;?> >Client Name</th>
                                       <th <?php echo $th_css;?> >Client Type</th>
                                       <th <?php echo $th_css;?> >Company Name</th>
                                       <th <?php echo $th_css;?> >Deadline Type</th>
                                       <th <?php echo $th_css;?> >Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      
                                       <?php 
                                       $time=date('Y-m-d');
                                        $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
                                          foreach ($getCompany_oneyear as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                                          ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                                          
                                          (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                                          ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                                          
                                          (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                                          ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                                          ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                                          ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                                          
                                          (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                          ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                                          
                                          (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                          ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                          ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                                          ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                          ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                                          ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                                          ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                                          ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                                          ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                                          ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                          ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                                          ?>
                                             <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))){ ?>
                                       <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Confirmation statement Due Date</td>
                                          <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
                                       </tr>
                                          <?php } ?>
                                            <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td  <?php echo $tr_css;?> >Accounts Due Date</td>
                                          
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
                                          <!-- account -->
                                           </tr>
                                           <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                           <tr>
                                           <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Company Tax Return Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
                                           <!--company tax -->
                                         </tr>
                                         <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))){ ?>
                                         <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                           <td  <?php echo $tr_css;?> >Personal Tax Return Due Date</td>
                                          <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
                                          <!-- crm personal tax -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']))){ ?>
                                          <tr>
                                            <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >VAT Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
                                          <!-- vat -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))){ ?>
                                          <tr>
                                            <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td  <?php echo $tr_css;?> >Payroll Due Date</td>
                                             <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
                                          <!-- payrol -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >WorkPlace Pension - AE Due Date</td>
                                            <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
                                          <!--workplace -->
                                          </tr>
                                          <?php } ?>
                                           
                                          <tr>
                                           <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                            <td  <?php echo $tr_css;?> >CIS - Contractor Due Date</td>
                                          <td  <?php echo $tr_css;?> ></td>
                                          <!-- CIS - Contractor -->
                                          </tr>
                                         
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >CIS - Sub Contractor Due Date</td>
                                          <td  <?php echo $tr_css;?> ></td>
                                          <!-- CIS - Sub Contractor-->
                                          </tr>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >P11D Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
                                          <!-- p11d -->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Management Accounts Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
                                          <!-- manage account -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Bookkeeping Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
                                          <!-- booking-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Investigation Insurance Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
                                          <!-- insurance -->
                                          </tr>
                                          <?php } ?>
                                           <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Registered Address Due Date</td>
                                         <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
                                          <!-- registed -->
                                          </tr>
                                          <?php } ?>
                                         <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Tax Advice Due Date</td>
                                        <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax advice-->
                                          </tr>
                                          <?php } ?>
                                          <?php if($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))){ ?>
                                          <tr>
                                          <td  <?php echo $tr_css;?> ><?php echo $getusername['crm_name'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td  <?php echo $tr_css;?> ><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td  <?php echo $tr_css;?> >Tax Investigation Due Date</td>
                                           <td  <?php echo $tr_css;?> ><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
                                          <!-- tax inve-->
                                          </tr>
                                      <?php } ?>
                                       <?php   }  ?>
                                    </tbody>

                                 </table>
  <!-- end of this year-->

   </div>
</div>
</body>
</html>
<script>
  $(document).on('click',".print_data_cls",function(){
 

      var divContents = $(".dataTables_scrollBody").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>Users List</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();


  });
</script>