<!DOCTYPE html>
<html>
   <head>
      <style>
         #pdf {
         font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
         border-collapse: collapse;
         width: 100%;
         }
         #pdf td, #pdf th {
         border: 1px solid #ddd;
         padding: 8px;
         }
         #pdf tr:nth-child(even){background-color: #f2f2f2;}
         #pdf td
         {
         font-size: 13px;
         }
         #pdf th {
         padding-top: 12px;
         padding-bottom: 12px;
         text-align: left;
         background-color: #9E9E9E;
         color: white;
         font-size: 14px !important;
         white-space: nowrap;
         }
         p{
         color: #D7CCC8;
         }
         caption{
         color: #555;
         font-size: 22px;
         margin-bottom: 20px;
         }
      </style>
   </head>
   <body>
      <!-- for today -->
      <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%" cellpadding="10" style="border-collapse:collapse;">
         <thead>
            <tr class="text-uppercase">
               <th colspan="5" style="text-align:left;padding:30px 0;font-size:18px;">Today</th>
            </tr>
            <tr class="text-uppercase">
               <th style="border: 1px solid #ddd;text-align:left;">Client Name</th>
               <th style="border: 1px solid #ddd;text-align:left;">Client Type</th>
               <th style="border: 1px solid #ddd;text-align:left;">Company Name</th>
               <th style="border: 1px solid #ddd;text-align:left;">Deadline Type</th>
               <th style="border: 1px solid #ddd;text-align:left;">Date</th>
            </tr>
         </thead>
         <tbody>
            <!-- <tr>
               <td>demo</td>
               <td>demo</td>
               <td>demo</td>
               <td>2018-05-02</td>
               </tr> -->
            <?php 

            $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();
            
               $current_date=date("Y-m-d");
                  foreach ($getCompany_today as $getCompanykey => $getCompanyvalue) {
                      $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                  ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                  
                  (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                  ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                  
                  (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                  ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                  
                  (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                  ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                  
                  (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                  ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                  
                  (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                  ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                  
                  (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                  ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                  
                  (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                  ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                  
                  (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                  ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                  
                  (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                  ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                  
                  (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                  ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                  
                  (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                  ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                  
                  (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                  ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                  
                  (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                  ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                  
                  (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                  ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                  
                  (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                  ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                  ?>
            <?php if(($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])))&& ($deadline_type=='Confirmation statement' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Confirmation statement Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td>
               <!-- confi-->
            </tr>
            <?php } ?>
            <?php if(($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))) && ($deadline_type=='Accounts' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Accounts Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
               <!-- account -->
            </tr>
            <?php } ?>
            <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && ($deadline_type=='Company Tax Return' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Company Tax Return Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
               <!--company tax -->
            </tr>
            <?php } ?>
            <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && ($deadline_type=='Personal Tax Return' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Personal Tax Return Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
               <!-- crm personal tax -->
            </tr>
            <?php } ?>
            <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && ($deadline_type=='VAT' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">VAT Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
               <!-- vat -->
            </tr>
            <?php } ?>
            <?php if(($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))) && ($deadline_type=='Payroll' || $deadline_type=='')){ ?>
            <tr class="<?php echo $current_date."---".date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']));?>">
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Payroll Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
               <!-- payrol -->
            </tr>
            <?php } ?>
            <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && ($deadline_type=='WorkPlace Pension - AE' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">WorkPlace Pension - AE Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
               <!--workplace -->
            </tr>
            <?php } ?>
            <?php if($deadline_type=='CIS - Contractor' || $deadline_type==''){ ?>       
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">CIS - Contractor Due Date</td>
               <td style="border: 1px solid #ddd;"></td>
               <!-- CIS - Contractor -->
            </tr>
            <?php } ?>
            <?php if($deadline_type=='CIS - Sub Contractor' || $deadline_type==''){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">CIS - Sub Contractor Due Date</td>
               <td style="border: 1px solid #ddd;"></td>
               <!-- CIS - Sub Contractor-->
            </tr>
            <?php } ?>
            <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && ($deadline_type=='P11D' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">P11D Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
               <!-- p11d -->
            </tr>
            <?php } ?>
            <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && ($deadline_type=='Management Accounts' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Management Accounts Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
               <!-- manage account -->
            </tr>
            <?php } ?>
            <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && ($deadline_type=='Bookkeeping' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Bookkeeping Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
               <!-- booking-->
            </tr>
            <?php } ?>
            <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && ($deadline_type=='Investigation Insurance' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Investigation Insurance Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
               <!-- insurance -->
            </tr>
            <?php } ?>
            <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && ($deadline_type=='Registered Address' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Registered Address Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
               <!-- registed -->
            </tr>
            <?php } ?>
            <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && ($deadline_type=='Tax Advice' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Tax Advice Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
               <!-- tax advice-->
            </tr>
            <?php } ?>
            <?php if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && ($deadline_type=='Tax Investigation' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Tax Investigation Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
               <!-- tax inve-->
            </tr>
            <?php } 
            
            foreach($other_services as $key => $value) 
            {
               if($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && ($deadline_type == $value['service_name'] || $deadline_type=='')){ ?>

               <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $value['service_name']; ?> Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])): ''); ?></td>  
               </tr>
                <?php }
            }

            ?>
            <?php   }  ?>
         </tbody>
      </table>
      <!-- end of today -->
      <!-- this week -->
      <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%" cellpadding="10" style="border-collapse:collapse;">
         <thead>
            <tr class="text-uppercase">
               <th colspan="5" style="text-align:left;padding:30px 0;font-size:18px;">This Week</th>
            </tr>
            <tr class="text-uppercase">
               <th style="border: 1px solid #ddd;text-align:left;">Client Name</th>
               <th style="border: 1px solid #ddd;text-align:left;">Client Type</th>
               <th style="border: 1px solid #ddd;text-align:left;">Company Name</th>
               <th style="border: 1px solid #ddd;text-align:left;">Deadline Type</th>
               <th style="border: 1px solid #ddd;text-align:left;">Date</th>
            </tr>
         </thead>
         <tbody>
            <!-- <tr>
               <td>demo</td>
               <td>demo</td>
               <td>demo</td>
               <td>2018-05-02</td>
               </tr> -->
            <?php 
               $time=date('Y-m-d');
               if($due_val=='over_due')
               {
                 $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
               }
               else
               {
                 $oneweek_end_date = date('Y-m-d', strtotime($time . ' -1 week'));
               }
               $current_date=date('Y-m-d');
               
                  foreach ($getCompany_oneweek as $getCompanykey => $getCompanyvalue) {
                      $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                  ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                  
                  (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                  ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                  
                  (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                  ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                  
                  (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                  ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                  
                  (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                  ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                  
                  (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                  ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                  
                  (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                  ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                  
                  (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                  ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                  
                  (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                  ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                  
                  (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                  ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                  
                  (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                  ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                  
                  (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                  ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                  
                  (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                  ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                  
                  (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                  ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                  
                  (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                  ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                  
                  (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                  ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                  ?>
            <?php
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
                  break;
               }
                                   if(($zz) && ($deadline_type=='Confirmation statement' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Confirmation statement Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td>
               <!-- confi-->
            </tr>
            <?php } 
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
                  break;
               }
                                    ?>
            <?php if(($zz) && ($deadline_type=='Accounts' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Accounts Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
               <!-- account -->
            </tr>
            <?php } 
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
                  break;
               }
                                  
                        ?>
            <?php if(($zz) && ($deadline_type=='Company Tax Return' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Company Tax Return Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
               <!--company tax -->
            </tr>
            <?php } 
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
                  break;
               }
                             ?>
            <?php if(($zz) && ($deadline_type=='Personal Tax Return' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Personal Tax Return Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
               <!-- crm personal tax -->
            </tr>
            <?php }
               switch ($due_val) {
                 case 'over_due':
                   $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
                   break;
                 
                 default:
                   $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
                   break;
               } ?>
            <?php if(($zz) && ($deadline_type=='VAT' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">VAT Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
               <!-- vat -->
            </tr>
            <?php } 
               switch ($due_val) {
                 case 'over_due':
                   $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
                   break;
                 
                 default:
                   $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
                   break;
               }
                            ?>
            <?php if(($zz) && $deadline_type=='Payroll' || $deadline_type==''){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Payroll Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
               <!-- payrol -->
            </tr>
            <?php } 
               switch ($due_val) {
               case 'over_due':
               $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
               break;
               
               default:
               $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
               break;
               } ?>
            <?php if(($zz) && ($deadline_type=='WorkPlace Pension - AE' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">WorkPlace Pension - AE Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
               <!--workplace -->
            </tr>
            <?php } ?>
            <?php if($deadline_type=='CIS - Contractor' || $deadline_type==''){ ?>       
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">CIS - Contractor Due Date</td>
               <td style="border: 1px solid #ddd;"></td>
               <!-- CIS - Contractor -->
            </tr>
            <?php } ?>
            <?php if($deadline_type=='CIS - Sub Contractor' || $deadline_type==''){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">CIS - Sub Contractor Due Date</td>
               <td style="border: 1px solid #ddd;"></td>
               <!-- CIS - Sub Contractor-->
            </tr>
            <?php } ?>
            <?php             
               switch ($due_val) {
               case 'over_due':
                $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
                break;
               
               default:
                $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
                break;
               }
               ?>
            <?php if(($zz) && ($deadline_type=='P11D' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">P11D Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
               <!-- p11d -->
            </tr>
            <?php }
               switch ($due_val) {
               case 'over_due':
                $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
                break;
               
               default:
                $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
                break;
               }
                            ?>
            <?php if(($zz) && ($deadline_type=='Management Accounts' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Management Accounts Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
               <!-- manage account -->
            </tr>
            <?php }
               switch ($due_val) {
               case 'over_due':
                 $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
                 break;
               
               default:
                 $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
                 break;
               } ?>
            <?php if(($zz) && ($deadline_type=='Bookkeeping' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Bookkeeping Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
               <!-- booking-->
            </tr>
            <?php } 
               switch ($due_val) {
               case 'over_due':
                 $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
                 break;
               
               default:
                 $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
                 break;
               }
                                     ?>
            <?php if(($zz) && ($deadline_type=='Investigation Insurance' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Investigation Insurance Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
               <!-- insurance -->
            </tr>
            <?php } 
               switch ($due_val) {
               case 'over_due':
                 $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
                 break;
               
               default:
                 $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
                 break;
               } ?>
            <?php if(($zz) && ($deadline_type=='Registered Address' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Registered Address Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
               <!-- registed -->
            </tr>
            <?php }
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                  break;
               }
                   ?>
            <?php if(($zz) && ($deadline_type=='Tax Advice' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Tax Advice Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
               <!-- tax advice-->
            </tr>
            <?php } 
               switch ($due_val) {
               case 'over_due':
               $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
               break;
               
               default:
               $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
               break;
               }
               ?>
            <?php if(($zz) && ($deadline_type=='Tax Investigation' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Tax Investigation Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
               <!-- tax inve-->
            </tr>
            <?php } 

            foreach ($other_services as $key => $value) 
            {
               switch ($due_val) 
               {
                 case 'over_due':
                 $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])));
                 break;

                 default:
                 $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])));
                 break;
               }

               if(($zz) && ($deadline_type == $value['service_name'] || $deadline_type==''))
               {  ?>

               <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $value['service_name']; ?> Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])): ''); ?></td>  
               </tr>
                <?php }
            }

            ?>
            <?php   }  ?>
         </tbody>
      </table>
      <!-- end of this week -->
      <!--- this month -->
      <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%" cellpadding="10" style="border-collapse:collapse;">
         <thead>
            <tr class="text-uppercase">
               <th colspan="5" style="text-align:left;padding:30px 0;font-size:18px;">This Month</th>
            </tr>
            <tr class="text-uppercase">
               <th style="border: 1px solid #ddd;text-align:left;">Client Name</th>
               <th style="border: 1px solid #ddd;text-align:left;">Client Type</th>
               <th style="border: 1px solid #ddd;text-align:left;">Company Name</th>
               <th style="border: 1px solid #ddd;text-align:left;">Deadline Type</th>
               <th style="border: 1px solid #ddd;text-align:left;">Date</th>
            </tr>
         </thead>
         <tbody>
            <!-- <tr>
               <td>demo</td>
               <td>demo</td>
               <td>demo</td>
               <td>2018-05-02</td>
               </tr> -->
            <?php 
               $time=date('Y-m-d');
               
               // $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
                 if($due_val=='over_due')
               {
                 $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
               }
               else
               {
                 $onemonth_end_date = date('Y-m-d', strtotime($time . ' -1 month'));
               }
               $current_date=date('Y-m-d');
                  foreach ($getCompany_onemonth as $getCompanykey => $getCompanyvalue) {
                      $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                  ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                  
                  (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                  ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                  
                  (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                  ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                  
                  (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                  ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                  
                  (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                  ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                  
                  (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                  ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                  
                  (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                  ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                  
                  (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                  ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                  
                  (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                  ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                  
                  (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                  ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                  
                  (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                  ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                  
                  (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                  ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                  
                  (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                  ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                  
                  (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                  ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                  
                  (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                  ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                  
                  (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                  ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                  ?>
            <?php
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
                  break;
               }
                                   if(($zz) && ($deadline_type=='Confirmation statement' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Confirmation statement Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td>
               <!-- confi-->
            </tr>
            <?php } 
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
                  break;
               }
                                    ?>
            <?php if(($zz) && ($deadline_type=='Accounts' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Accounts Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
               <!-- account -->
            </tr>
            <?php } 
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
                  break;
               }
                                  
                        ?>
            <?php if(($zz) && ($deadline_type=='Company Tax Return' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Company Tax Return Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
               <!--company tax -->
            </tr>
            <?php } 
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
                  break;
               }
                             ?>
            <?php if(($zz) && ($deadline_type=='Personal Tax Return' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Personal Tax Return Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
               <!-- crm personal tax -->
            </tr>
            <?php }
               switch ($due_val) {
                 case 'over_due':
                   $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
                   break;
                 
                 default:
                   $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
                   break;
               } ?>
            <?php if(($zz) && ($deadline_type=='VAT' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">VAT Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
               <!-- vat -->
            </tr>
            <?php } 
               switch ($due_val) {
                 case 'over_due':
                   $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
                   break;
                 
                 default:
                   $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
                   break;
               }
                            ?>
            <?php if(($zz) && $deadline_type=='Payroll' || $deadline_type=='' ){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Payroll Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
               <!-- payrol -->
            </tr>
            <?php } 
               switch ($due_val) {
               case 'over_due':
               $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
               break;
               
               default:
               $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
               break;
               } ?>
            <?php if(($zz) && ($deadline_type=='WorkPlace Pension - AE' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">WorkPlace Pension - AE Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
               <!--workplace -->
            </tr>
            <?php } ?>
            <?php if($deadline_type=='CIS - Contractor' || $deadline_type==''){ ?>       
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">CIS - Contractor Due Date</td>
               <td style="border: 1px solid #ddd;"></td>
               <!-- CIS - Contractor -->
            </tr>
            <?php } ?>
            <?php if($deadline_type=='CIS - Sub Contractor' || $deadline_type==''){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">CIS - Sub Contractor Due Date</td>
               <td style="border: 1px solid #ddd;"></td>
               <!-- CIS - Sub Contractor-->
            </tr>
            <?php } ?>
            <?php             
               switch ($due_val) {
               case 'over_due':
                $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
                break;
               
               default:
                $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
                break;
               }
               ?>
            <?php if(($zz) && ($deadline_type=='P11D' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">P11D Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
               <!-- p11d -->
            </tr>
            <?php }
               switch ($due_val) {
               case 'over_due':
                $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
                break;
               
               default:
                $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
                break;
               }
                            ?>
            <?php if(($zz) && ($deadline_type=='Management Accounts' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Management Accounts Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
               <!-- manage account -->
            </tr>
            <?php }
               switch ($due_val) {
               case 'over_due':
                 $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
                 break;
               
               default:
                 $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
                 break;
               } ?>
            <?php if(($zz) && ($deadline_type=='Bookkeeping' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Bookkeeping Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
               <!-- booking-->
            </tr>
            <?php } 
               switch ($due_val) {
               case 'over_due':
                 $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
                 break;
               
               default:
                 $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
                 break;
               }
                                     ?>
            <?php if(($zz) && ($deadline_type=='Investigation Insurance' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Investigation Insurance Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
               <!-- insurance -->
            </tr>
            <?php } 
               switch ($due_val) {
               case 'over_due':
                 $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
                 break;
               
               default:
                 $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
                 break;
               } ?>
            <?php if(($zz) && ($deadline_type=='Registered Address' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Registered Address Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
               <!-- registed -->
            </tr>
            <?php }
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                  break;
               }
                   ?>
            <?php if(($zz) && ($deadline_type=='Tax Advice' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Tax Advice Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
               <!-- tax advice-->
            </tr>
            <?php } 
               switch ($due_val) {
               case 'over_due':
               $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
               break;
               
               default:
               $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
               break;
               }
               ?>
            <?php if(($zz) && ($deadline_type=='Tax Investigation' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Tax Investigation Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
               <!-- tax inve-->
            </tr>
            <?php } 


            foreach ($other_services as $key => $value) 
            {
               switch ($due_val) 
               {
                 case 'over_due':
                 $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])));
                 break;

                 default:
                 $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])));
                 break;
               }

               if(($zz) && ($deadline_type == $value['service_name'] || $deadline_type=='')){  ?>

               <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $value['service_name']; ?> Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])): ''); ?></td>  
               </tr>
                <?php }
            }

            ?>
            <?php   }  ?>
         </tbody>
      </table>
      <!-- end of this month -->
      <!-- this year -->
      <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%" cellpadding="10" style="border-collapse:collapse;">
         <thead>
            <tr class="text-uppercase">
               <th colspan="5" style="text-align:left;padding:30px 0;font-size:18px;">This Year</th>
            </tr>
            <tr class="text-uppercase">
               <th style="border: 1px solid #ddd;text-align:left;">Client Name</th>
               <th style="border: 1px solid #ddd;text-align:left;">Client Type</th>
               <th style="border: 1px solid #ddd;text-align:left;">Company Name</th>
               <th style="border: 1px solid #ddd;text-align:left;">Deadline Type</th>
               <th style="border: 1px solid #ddd;text-align:left;">Date</th>
            </tr>
         </thead>
         <tbody>
            <!-- <tr>
               <td>demo</td>
               <td>demo</td>
               <td>demo</td>
               <td>2018-05-02</td>
               </tr> -->
            <?php 
               $time=date('Y-m-d');
                // $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
                if($due_val=='over_due')
               {
                 $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
               }
               else
               {
                 $oneyear_end_date = date('Y-m-d', strtotime($time . ' -1 year'));
               }
               $current_date=date('Y-m-d');
                  foreach ($getCompany_oneyear as $getCompanykey => $getCompanyvalue) {
                      $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                  ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                  
                  (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                  ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                  
                  (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                  ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                  
                  (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                  ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                  
                  (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                  ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                  
                  (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                  ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                  
                  (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                  ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                  
                  (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                  ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                  
                  (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                  ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                  
                  (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                  ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                  
                  (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                  ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                  
                  (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                  ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                  
                  (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                  ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                  
                  (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                  ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                  
                  (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                  ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                  
                  (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                  ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                  ?>
            <?php
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
                  break;
               }
                                   if(($zz) && ($deadline_type=='Confirmation statement' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Confirmation statement Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td>
               <!-- confi-->
            </tr>
            <?php } 
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
                  break;
               }
                                    ?>
            <?php if(($zz && ($deadline_type=='Accounts' || $deadline_type==''))){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Accounts Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
               <!-- account -->
            </tr>
            <?php } 
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
                  break;
               }
                                  
                        ?>
            <?php if(($zz) && ($deadline_type=='Company Tax Return' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Company Tax Return Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
               <!--company tax -->
            </tr>
            <?php } 
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
                  break;
               }
                             ?>
            <?php if(($zz) && ($deadline_type=='Personal Tax Return' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Personal Tax Return Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
               <!-- crm personal tax -->
            </tr>
            <?php }
               switch ($due_val) {
                 case 'over_due':
                   $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
                   break;
                 
                 default:
                   $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
                   break;
               } ?>
            <?php if(($zz) && ($deadline_type=='VAT' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">VAT Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
               <!-- vat -->
            </tr>
            <?php } 
               switch ($due_val) {
                 case 'over_due':
                   $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
                   break;
                 
                 default:
                   $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
                   break;
               }
                            ?>
            <?php if(($zz) && $deadline_type=='Payroll' || $deadline_type==''){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Payroll Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
               <!-- payrol -->
            </tr>
            <?php } 
               switch ($due_val) {
               case 'over_due':
               $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
               break;
               
               default:
               $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
               break;
               } ?>
            <?php if(($zz) && ($deadline_type=='WorkPlace Pension - AE' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">WorkPlace Pension - AE Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
               <!--workplace -->
            </tr>
            <?php } ?>
            <?php if($deadline_type=='CIS - Contractor' || $deadline_type==''){ ?>       
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">CIS - Contractor Due Date</td>
               <td style="border: 1px solid #ddd;"></td>
               <!-- CIS - Contractor -->
            </tr>
            <?php } ?>
            <?php if($deadline_type=='CIS - Sub Contractor' || $deadline_type==''){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">CIS - Sub Contractor Due Date</td>
               <td style="border: 1px solid #ddd;"></td>
               <!-- CIS - Sub Contractor-->
            </tr>
            <?php } ?>
            <?php             
               switch ($due_val) {
               case 'over_due':
                $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
                break;
               
               default:
                $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
                break;
               }
               ?>
            <?php if(($zz) && ($deadline_type=='P11D' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">P11D Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
               <!-- p11d -->
            </tr>
            <?php }
               switch ($due_val) {
               case 'over_due':
                $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
                break;
               
               default:
                $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
                break;
               }
                            ?>
            <?php if(($zz) && ($deadline_type=='Management Accounts' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Management Accounts Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
               <!-- manage account -->
            </tr>
            <?php }
               switch ($due_val) {
               case 'over_due':
                 $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
                 break;
               
               default:
                 $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
                 break;
               } ?>
            <?php if(($zz) && ($deadline_type=='Bookkeeping' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Bookkeeping Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
               <!-- booking-->
            </tr>
            <?php } 
               switch ($due_val) {
               case 'over_due':
                 $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
                 break;
               
               default:
                 $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
                 break;
               }
                                     ?>
            <?php if(($zz) && ($deadline_type=='Investigation Insurance' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Investigation Insurance Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
               <!-- insurance -->
            </tr>
            <?php } 
               switch ($due_val) {
               case 'over_due':
                 $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
                 break;
               
               default:
                 $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
                 break;
               } ?>
            <?php if(($zz) && ($deadline_type=='Registered Address' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Registered Address Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
               <!-- registed -->
            </tr>
            <?php }
               switch ($due_val) {
                case 'over_due':
                  $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                  break;
                
                default:
                  $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                  break;
               }
                   ?>
            <?php if(($zz) && ($deadline_type=='Tax Advice' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Tax Advice Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
               <!-- tax advice-->
            </tr>
            <?php } 
               switch ($due_val) {
               case 'over_due':
               $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
               break;
               
               default:
               $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
               break;
               }
               ?>
            <?php if(($zz) && ($deadline_type=='Tax Investigation' || $deadline_type=='')){ ?>
            <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;">Tax Investigation Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
               <!-- tax inve-->
            </tr>
            <?php } 

            foreach($other_services as $key => $value) 
            {
               switch ($due_val) 
               {
                 case 'over_due':
                 $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])));
                 break;

                 default:
                 $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])));
                 break;
               }

               if(($zz) && ($deadline_type == $value['service_name'] || $deadline_type=='')){  ?>

               <tr>
               <td style="border: 1px solid #ddd;"><?php echo $getusername['crm_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
               <td style="border: 1px solid #ddd;"><?php echo $value['service_name']; ?> Due Date</td>
               <td style="border: 1px solid #ddd;"><?php echo (strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])): ''); ?></td>  
               </tr>
                <?php }
            }

            ?>
            <?php   }  ?>
         </tbody>
         </tbody>
      </table>
      <!-- end of this year-->
   </body>
</html>