<?php $this->load->view('includes/header');
$succ = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

//die("Stope Here");

?>

<!--   Datatable header by Ram -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/email.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" /> -->
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css?ver=2"> -->



<!--   End by Ram -->

<style type="text/css">
  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
  }

  .card {
    width: 500px;
    margin: auto;
  }

  .breadcrumb-header span {
    color: green;
  }
</style>

<div class="modal-alertsuccess alert succ popup_info_msg" style="<?php if ($_SESSION['success_msg'] != "") {
                                                                    echo "display: block";
                                                                  } else {
                                                                    echo "display: none";
                                                                  } ?>">
  <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
    <div class="pop-realted1">
      <div class="position-alert1">
        <?php if ($_SESSION['success_msg'] != "") {
          echo $_SESSION['success_msg'];
        } ?>
      </div>
    </div>
  </div>
</div>

<!-- hint 
just change the display name like due and over due.
in functions we use due as over due, over due as due.
-->
<!-- management block -->
<div class="pcoded-content newalign-dead">
  <div class="pcoded-inner-content">
    <!-- Main-body start -->
    <div class="main-body">
      <div class="page-wrapper deadline-manager-wrapper">
        <div class="deadline-crm1  floating_set single-txt12 ddline">
          <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard page-tabs">
            <li class="nav-item">
              <a href="#">Deadline Manager</a>
              <div class="slide"></div>
            </li>
          </ul>



        </div>


        <div class=" deadline-block">
          <!-- end of export option -->
          <div class="payrolltax floating rev-op">
            <div class="company-search1">
              <!-- <div class="vat-shipping">
<label>Company Name</label> 
<select name="companysearch" id="companysearch" class="search">
<option value="">All</option>
<option value="2">zts</option>
<option value="">abc company</option>
</select>
</div> -->
              <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard don1 renewdesign">

                <li class="search-block01">
                  <div class="search-vat1">
                    <label>Search:</label>
                    <input type="text" name="deadline_search" id="deadline_search" placeholder="Search Company">
                  </div>
                </li>

                <li>
                  <div class="dropdown-primary dropdown open for_ul_ajax">
                    <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button>
                    <div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="" class="dropdown-item waves-light waves-effect but csv" target="_blank">Csv</a><a href="" class="dropdown-item waves-light waves-effect but pdf" target="_blank">Pdf</a><a href="" class="dropdown-item waves-light waves-effect but html" target="_blank">HTML</a></div>
                  </div>
                </li>

              </ul>
              <div class="vat-dead-01">
                <div class="vat-shipping">
                  <label>Company Type</label>
                  <select class="  fields search" name="company_type" id="company_type">
                    <option value="" data-id="1">All</option>
                    <option value="Private Limited company" data-id="2">Private Limited company</option>
                    <option value="Public Limited company" data-id="3">Public Limited company</option>
                    <option value="Limited Liability Partnership" data-id="4">Limited Liability Partnership</option>
                    <option value="Partnership" data-id="5">Partnership</option>
                    <option value="Self Assessment" data-id="6">Self Assessment</option>
                    <option value="Trust" data-id="7">Trust</option>
                    <option value="Charity" data-id="8">Charity</option>
                    <option value="Other" data-id="9">Other</option>
                  </select>
                </div>
                <?php

                $service_list = $this->Common_mdl->getServicelist();
                $service_charges = $this->Common_mdl->getClientsServiceCharge($getCompany);
                ?>
                <div class="vat-shipping">
                  <label>Deadline Type</label>
                  <select class="fields search12" name="deadline_type" id="deadline_type">
                    <option value="">ALL</option>
                    <?php foreach ($service_list as $key => $value) { ?>
                      <option value="<?php echo $value['service_name']; ?>"><?php echo $value['service_name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="vat-shipping">
                  <div class="radio radio-inline">
                    <label> <input type="radio" name="for_due" id="for_due" value="over_due"> <i class="helper"></i>Over Due</label>
                  </div>
                  <div class="radio radio-inline">
                    <label> <input type="radio" name="for_due" id="for_due" value="due" checked="checked"> <i class="helper"></i>Due</label>
                  </div>
                </div>
              </div>
            </div>
            <!-- collapse panel -->
            <div class="day-list   accor-view09 floating_set">
              <!-- <div class="f-days">
<div class="panel-head" ddata-parent="#accordion" data-toggle="collapse" data-target="#demo">
Today
<i class="fa fa-angle-double-down" aria-hidden="true"></i>
</div>
<div class="panel-body panel-collapse collapse" id="demo">
No Deadlines to view
</div>
</div>
<div class="f-week">
<div class="panel-head" data-parent="#accordion" data-toggle="collapse" data-target="#demo1">
This Week
<i class="fa fa-angle-double-down" aria-hidden="true"></i>
</div>
<div class="panel-body panel-collapse collapse" id="demo1">
No Deadlines to view
</div>
</div>
<div class="f-month">
<div class="panel-head" data-parent="#accordion" data-toggle="collapse" data-target="#demo2">
This Month
<i class="fa fa-angle-double-down" aria-hidden="true"></i>
</div>
<div class="panel-body panel-collapse collapse" id="demo2">
No Deadlines to view
</div>
</div>
<div class="n-month">
<div class="panel-head" data-parent="#accordion" data-toggle="collapse" data-target="#demo3">
Next Month
<i class="fa fa-angle-double-down" aria-hidden="true"></i>
</div>
<div class="panel-body panel-collapse collapse" id="demo3">
No Deadlines to view
</div>
</div> -->
              <div class=" accordion-block color-accordion-block">

                <!-- new tab -->
                <input type="hidden" name="active_tabs_name" id="active_tabs_name" value="today">
                <div class="taskpagedashboard1">
                  <div class="top-leads fr-task cfssc">
                    <ul class="nav nav-tabs lead-data1 pull-right">
                      <li class="nav-item junk-lead tsk-color1 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val nots_task">
                        <div class="lead-point1">
                          <a class="for_link nav-link active" data-id="today" data-toggle="tab" href="#today">
                            <strong class="for_count count_today"></strong>
                            <span class="task_status_val_1 status_filter">Today</span>
                          </a>
                        </div>
                      </li>
                      <li class="nav-item junk-lead tsk-color1 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val nots_task">
                        <div class="lead-point1">
                          <a class="for_link nav-link" data-id="this-week" data-toggle="tab" href="#this-week">
                            <strong class="for_count count_week"></strong>
                            <span class="task_status_val_1 status_filter">This Week</span>
                          </a>
                        </div>
                      </li>
                      <li class="nav-item junk-lead tsk-color1 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val nots_task">
                        <div class="lead-point1">
                          <a class="for_link nav-link" data-id="this-month" data-toggle="tab" href="#this-month">
                            <strong class="for_count count_month"></strong>
                            <span class="task_status_val_1 status_filter">This Month</span></a>
                        </div>
                      </li>
                      <li class="nav-item junk-lead tsk-color1 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val nots_task">
                        <div class="lead-point1">
                          <a class="for_link nav-link this_year" data-id="this-year" data-toggle="tab" href="#this-year">
                            <strong class="for_count count_year"></strong>
                            <span class="task_status_val_1 status_filter">This Year</span></a>
                        </div>
                      </li>
                  </div>
                </div>



                <div class="tab-content button_visibility <?php if ($_SESSION['permission']['Deadline_manager']['view'] != '1') { ?> permission_deined <?php } ?>" id="accordion_table">
                  <div id="today" class="tab-pane fade in active">

                    <!-- for filter section -->
                    <div class="filter-data for-reportdash">
                      <div class="filter-head">
                        <div class="filter-head-content">
                          <h4>FILTERED DATA:</h4>
                          <div id="container2" class="box-container">
                          </div>
                        </div>
                        <div>
                          <button class="btn btn-danger f-right" id="clear_container">clear</button>
                        </div>                      
                      </div>
                    </div>
                    <!-- for filter section -->

                    <table class="table client_table1 text-center " id="today_deadlineinfo">
                      <thead>
                        <th class="client_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Client Name</th>
                        <th class="client_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Client Type</th>
                        <th class="company_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Company Name</th>
                        <th class="deadline_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Deadline Type</th>
                        <th class="date_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Date</th>
                      </thead>

                      <!--  <tfoot>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
</tfoot> -->
                      <tbody>

                        <?php

                        $current_date = date("Y-m-d");

                        foreach ($getCompany_today as $getCompanykey => $getCompanyvalue) {
                          $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);

                          if (($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))) && ($deadline_type == 'Confirmation statement' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Confirmation statement Due Date">Confirmation statement Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : ''); ?></td><!-- confi-->
                            </tr>
                          <?php }

                          if (($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))) && ($deadline_type == 'Accounts' || $deadline_type == '')) { ?>

                            <tr class="for_table">

                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Accounts Due Date">Accounts Due Date</td>

                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : ''); ?></td>
                              <!-- account -->
                            </tr>

                          <?php }

                          if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && ($deadline_type == 'Company Tax Return' || $deadline_type == '')) { ?>

                            <tr class="for_table">

                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>">Personal Tax Return Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : ''); ?></td>
                              <!-- crm personal tax -->
                            </tr>
                          <?php }

                          if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && ($deadline_type == 'Personal Tax Return' || $deadline_type == '')) { ?>
                            <tr class="for_table">

                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>">Personal Tax Return Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : ''); ?></td>
                              <!-- crm personal tax -->
                            </tr>
                          <?php }

                          if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && ($deadline_type == 'VAT' || $deadline_type == '')) {

                          ?>
                            <tr class="for_table">

                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="VAT Due Date">VAT Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : ''); ?></td>
                              <!-- vat -->
                            </tr>
                          <?php }

                          if (($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))) && ($deadline_type == 'Payroll' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Payroll Due Date">Payroll Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : ''); ?></td>
                              <!-- payrol -->
                            </tr>
                          <?php }

                          if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && ($deadline_type == 'WorkPlace Pension - AE' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="WorkPlace Pension - AE Due Date">WorkPlace Pension - AE Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : ''); ?></td>
                              <!--workplace -->
                            </tr>
                          <?php }

                          if ($deadline_type == 'CIS - Contractor' || $deadline_type == '') { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="CIS - Contractor Due Date">CIS - Contractor Due Date</td>
                              <td></td>
                              <!-- CIS - Contractor -->
                            </tr>

                          <?php }

                          if ($deadline_type == 'CIS - Sub Contractor' || $deadline_type == '') { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="CIS - Sub Contractor Due Date">CIS - Sub Contractor Due Date</td>
                              <td></td>
                              <!-- CIS - Sub Contractor-->
                            </tr>
                          <?php }

                          if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && ($deadline_type == 'P11D' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="P11D Due Date">P11D Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) : ''); ?></td>
                              <!-- p11d -->
                            </tr>
                          <?php }

                          if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && ($deadline_type == 'Management Accounts' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Management Accounts Due Date">Management Accounts Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) : ''); ?></td>
                              <!-- manage account -->
                            </tr>
                          <?php }

                          if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && ($deadline_type == 'Bookkeeping' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Bookkeeping Due Date">Bookkeeping Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) : ''); ?></td>
                              <!-- booking-->
                            </tr>
                          <?php }

                          if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && ($deadline_type == 'Investigation Insurance' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Investigation Insurance Due Date">Investigation Insurance Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) : ''); ?></td>
                              <!-- insurance -->
                            </tr>
                          <?php }

                          if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && ($deadline_type == 'Registered Address' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Registered Address Due Date">Registered Address Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) : ''); ?></td>
                              <!-- registed -->
                            </tr>
                          <?php }

                          if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && ($deadline_type == 'Tax Advice' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Tax Advice Due Date">Tax Advice Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : ''); ?></td>
                              <!-- tax advice-->
                            </tr>
                          <?php }

                          if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && ($deadline_type == 'Tax Investigation' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Tax Investigation Due Date">Tax Investigation Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : ''); ?></td>
                              <!-- tax inve-->
                            </tr>
                            <?php }

                          foreach ($other_services as $key => $value) {
                            if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && ($deadline_type == $value['service_name'] || $deadline_type == '')) { ?>

                              <tr class="for_table">
                                <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                                <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                                <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                                <td data-search="<?php echo $value['service_name']; ?> Due Date"><?php echo $value['service_name']; ?> Due Date</td>
                                <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) : ''); ?></td>
                              </tr>
                        <?php }
                          }
                        }  ?>
                      </tbody>
                    </table>

                  </div>
                  <div id="this-week" class="tab-pane fade">
                    <!-- for filter section -->
                    <div class="filter-data for-reportdash">
                      <div class="filter-head">
                        <h4>FILTERED DATA</h4>
                        <button class="btn btn-danger f-right" id="clear_container">clear</button>
                        <div id="container2" class="box-container">
                        </div>
                      </div>
                    </div>
                    <!-- for filter section -->

                    <table class="table client_table1 text-center " id="thisweek_deadlineinfo">
                      <thead>

                        <th class="client_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Client Name</th>
                        <th class="client_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Client Type</th>
                        <th class="company_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Company Name</th>
                        <th class="deadline_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Deadline Type</th>
                        <th class="date_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Date</th>

                      </thead>

                      <!--             <tfoot>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
</tfoot> -->
                      <tbody>

                        <?php

                        $time = date('Y-m-d');
                        $current_date = date('Y-m-d');

                        if ($due_val == 'over_due') {
                          $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
                        } else {
                          $oneweek_end_date = date('Y-m-d', strtotime($time . ' -1 week'));
                        }

                        foreach ($getCompany_oneweek as $getCompanykey => $getCompanyvalue) {
                          $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Confirmation statement' || $deadline_type == '')) {
                        ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Confirmation statement Due Date">Confirmation statement Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : ''); ?></td><!-- confi-->
                            </tr>

                          <?php }
                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Accounts' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Accounts Due Date">Accounts Due Date</td>

                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : ''); ?></td>
                              <!-- account -->
                            </tr>
                          <?php }
                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Company Tax Return' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Company Tax Return Due Date">Company Tax Return Due Date</td>
                              <td data-search="php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) : ''); ?></td>
                              <!--company tax -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Personal Tax Return' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>">Personal Tax Return Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : ''); ?></td>
                              <!-- crm personal tax -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'VAT' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="VAT Due Date">VAT Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : ''); ?></td>
                              <!-- vat -->
                            </tr>

                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Payroll' || $deadline_type == '')) {

                          ?>
                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Payroll Due Date">Payroll Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : ''); ?></td>
                              <!-- payrol -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'WorkPlace Pension - AE' || $deadline_type == '')) {  ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="WorkPlace Pension - AE Due Date">WorkPlace Pension - AE Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : ''); ?></td>
                              <!--workplace -->
                            </tr>
                          <?php }
                          if ($deadline_type == 'CIS - Contractor' || $deadline_type == '') {  ?>

                            <tr class="for_table">

                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="CIS - Contractor Due Date">CIS - Contractor Due Date</td>
                              <td></td>
                              <!-- CIS - Contractor -->
                            </tr>

                          <?php }
                          if ($deadline_type == 'CIS - Sub Contractor' || $deadline_type == '') { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="CIS - Sub Contractor Due Date">CIS - Sub Contractor Due Date</td>
                              <td></td>
                              <!-- CIS - Sub Contractor-->
                            </tr>

                          <?php  }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'P11D' || $deadline_type == '')) {  ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="P11D Due Date">P11D Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) : ''); ?></td>
                              <!-- p11d -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Management Accounts' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Management Accounts Due Date">Management Accounts Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) : ''); ?></td>
                              <!-- manage account -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Bookkeeping' || $deadline_type == '')) {  ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Bookkeeping Due Date">Bookkeeping Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) : ''); ?></td>
                              <!-- booking-->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Investigation Insurance' || $deadline_type == '')) {

                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Investigation Insurance Due Date">Investigation Insurance Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) : ''); ?></td>
                              <!-- insurance -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
                              break;
                          }
                          if (($zz) && ($deadline_type == 'Registered Address' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Registered Address Due Date">Registered Address Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) : ''); ?></td>
                              <!-- registed -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Tax Advice' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Tax Advice Due Date">Tax Advice Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : ''); ?></td>
                              <!-- tax advice-->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Tax Investigation' || $deadline_type == '')) {

                          ?>

                            <tr class="for_table">

                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Tax Investigation Due Date">Tax Investigation Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : ''); ?></td>
                              <!-- tax inve-->
                            </tr>
                            <?php }

                          foreach ($other_services as $key => $value) {
                            switch ($due_val) {
                              case 'over_due':
                                $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])));
                                break;

                              default:
                                $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])));
                                break;
                            }

                            if (($zz) && ($deadline_type == $value['service_name'] || $deadline_type == '')) {  ?>

                              <tr class="for_table">
                                <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                                <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                                <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                                <td data-search="<?php echo $value['service_name']; ?> Due Date"><?php echo $value['service_name']; ?> Due Date</td>
                                <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) : ''); ?></td>
                              </tr>
                          <?php }
                          }

                          ?>
                        <?php   }  ?>
                      </tbody>

                    </table>
                  </div>
                  <div id="this-month" class="tab-pane fade">

                    <!-- for filter section -->
                    <div class="filter-data for-reportdash">
                      <div class="filter-head">
                        <h4>FILTERED DATA</h4>
                        <button class="btn btn-danger f-right" id="clear_container">clear</button>
                        <div id="container2" class="box-container">
                        </div>
                      </div>
                    </div>
                    <!-- for filter section -->

                    <table class="table client_table1 text-center " id="thismonth_deadlineinfo">
                      <thead>
                        <th class="client_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Client Name</th>
                        <th class="client_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Client Type</th>
                        <th class="company_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Company Name</th>
                        <th class="deadline_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Deadline Type</th>
                        <th class="date_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Date</th>

                      </thead>

                      <!--   <tfoot>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
</tfoot> -->
                      <tbody>

                        <?php

                        $time = date('Y-m-d');

                        if ($due_val == 'over_due') {
                          $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
                        } else {
                          $onemonth_end_date = date('Y-m-d', strtotime($time . ' -1 month'));
                        }

                        $current_date = date('Y-m-d');

                        foreach ($getCompany_onemonth as $getCompanykey => $getCompanyvalue) {
                          $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Confirmation statement' || $deadline_type == '')) {

                        ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Confirmation statement Due Date">Confirmation statement Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : ''); ?></td><!-- confi-->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Accounts' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Accounts Due Date">Accounts Due Date</td>

                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : ''); ?></td>
                              <!-- account -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Company Tax Return' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Company Tax Return Due Date">Company Tax Return Due Date</td>
                              <td data-search="php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) : ''); ?></td>
                              <!--company tax -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Personal Tax Return' || $deadline_type == '')) {

                          ?>

                            <tr class="for_table">

                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>">Personal Tax Return Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : ''); ?></td>
                              <!-- crm personal tax -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
                              break;
                          }
                          if (($zz) && ($deadline_type == 'VAT' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="VAT Due Date">VAT Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : ''); ?></td>
                              <!-- vat -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
                              break;
                          }

                          if (($zz) && $deadline_type == 'Payroll' || $deadline_type == '') {

                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Payroll Due Date">Payroll Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : ''); ?></td>
                              <!-- payrol -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
                              break;
                          }
                          if (($zz) && ($deadline_type == 'WorkPlace Pension - AE' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="WorkPlace Pension - AE Due Date">WorkPlace Pension - AE Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : ''); ?></td>
                              <!--workplace -->
                            </tr>
                          <?php }

                          if ($deadline_type == 'CIS - Contractor' || $deadline_type == '') {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="CIS - Contractor Due Date">CIS - Contractor Due Date</td>
                              <td></td>
                              <!-- CIS - Contractor -->
                            </tr>

                          <?php }
                          if ($deadline_type == 'CIS - Sub Contractor' || $deadline_type == '') { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="CIS - Sub Contractor Due Date">CIS - Sub Contractor Due Date</td>
                              <td></td>
                              <!-- CIS - Sub Contractor-->
                            </tr>

                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'P11D' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="P11D Due Date">P11D Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) : ''); ?></td>
                              <!-- p11d -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Management Accounts' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">

                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Management Accounts Due Date">Management Accounts Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) : ''); ?></td>
                              <!-- manage account -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Bookkeeping' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Bookkeeping Due Date">Bookkeeping Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) : ''); ?></td>
                              <!-- booking-->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Investigation Insurance' || $deadline_type == '')) {   ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Investigation Insurance Due Date">Investigation Insurance Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) : ''); ?></td>
                              <!-- insurance -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Registered Address' || $deadline_type == '')) {

                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Registered Address Due Date">Registered Address Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) : ''); ?></td>
                              <!-- registed -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Tax Advice' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Tax Advice Due Date">Tax Advice Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : ''); ?></td>
                              <!-- tax advice-->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Tax Investigation' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Tax Investigation Due Date">Tax Investigation Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : ''); ?></td>
                              <!-- tax inve-->
                            </tr>
                            <?php }

                          foreach ($other_services as $key => $value) {
                            switch ($due_val) {
                              case 'over_due':
                                $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])));
                                break;

                              default:
                                $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])));
                                break;
                            }

                            if (($zz) && ($deadline_type == $value['service_name'] || $deadline_type == '')) {  ?>

                              <tr class="for_table">
                                <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                                <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                                <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                                <td data-search="<?php echo $value['service_name']; ?> Due Date"><?php echo $value['service_name']; ?> Due Date</td>
                                <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) : ''); ?></td>
                              </tr>
                          <?php }
                          }

                          ?>
                        <?php   }  ?>
                      </tbody>

                    </table>

                  </div>
                  <div id="this-year" class="tab-pane fade">

                    <!-- for filter section -->
                    <div class="filter-data for-reportdash">
                      <div class="filter-head">
                        <h4>FILTERED DATA</h4>
                        <button class="btn btn-danger f-right" id="clear_container">clear</button>
                        <div id="container2" class="box-container">
                        </div>
                      </div>
                    </div>
                    <!-- for filter section -->

                    <table class="table client_table1 text-center " id="thisyear_deadlineinfo" style="width: 100%">
                      <thead>

                        <th class="client_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Client Name</th>
                        <th class="client_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Client Type</th>
                        <th class="company_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Company Name</th>
                        <th class="deadline_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Deadline Type</th>
                        <th class="date_TH hasFilter"> <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />Date</th>
                      </thead>

                      <!--      <tfoot>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
</tfoot> -->
                      <tbody>

                        <?php

                        $time = date('Y-m-d');

                        if ($due_val == 'over_due') {
                          $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
                        } else {
                          $oneyear_end_date = date('Y-m-d', strtotime($time . ' -1 year'));
                        }

                        $current_date = date('Y-m-d');

                        foreach ($getCompany_oneyear as $getCompanykey => $getCompanyvalue) {
                          $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Confirmation statement' || $deadline_type == '')) {

                        ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Confirmation statement Due Date">Confirmation statement Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : ''); ?></td><!-- confi-->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Accounts' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Accounts Due Date">Accounts Due Date</td>

                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : ''); ?></td>
                              <!-- account -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Company Tax Return' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Company Tax Return Due Date">Company Tax Return Due Date</td>
                              <td data-search="php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) : ''); ?></td>
                              <!--company tax -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Personal Tax Return' || $deadline_type == '')) {

                          ?>

                            <tr class="for_table">

                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>">Personal Tax Return Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : ''); ?></td>
                              <!-- crm personal tax -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
                              break;
                          }
                          if (($zz) && ($deadline_type == 'VAT' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="VAT Due Date">VAT Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : ''); ?></td>
                              <!-- vat -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
                              break;
                          }

                          if (($zz) && $deadline_type == 'Payroll' || $deadline_type == '') {

                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Payroll Due Date">Payroll Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : ''); ?></td>
                              <!-- payrol -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
                              break;
                          }
                          if (($zz) && ($deadline_type == 'WorkPlace Pension - AE' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="WorkPlace Pension - AE Due Date">WorkPlace Pension - AE Due Date</td>
                              <td data-search="<?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : ''); ?></td>
                              <!--workplace -->
                            </tr>
                          <?php }

                          if ($deadline_type == 'CIS - Contractor' || $deadline_type == '') {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="CIS - Contractor Due Date">CIS - Contractor Due Date</td>
                              <td></td>
                              <!-- CIS - Contractor -->
                            </tr>

                          <?php }
                          if ($deadline_type == 'CIS - Sub Contractor' || $deadline_type == '') { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="CIS - Sub Contractor Due Date">CIS - Sub Contractor Due Date</td>
                              <td></td>
                              <!-- CIS - Sub Contractor-->
                            </tr>

                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'P11D' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="P11D Due Date">P11D Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) : ''); ?></td>
                              <!-- p11d -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Management Accounts' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">

                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Management Accounts Due Date">Management Accounts Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) : ''); ?></td>
                              <!-- manage account -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Bookkeeping' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Bookkeeping Due Date">Bookkeeping Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) : ''); ?></td>
                              <!-- booking-->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Investigation Insurance' || $deadline_type == '')) {   ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Investigation Insurance Due Date">Investigation Insurance Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) : ''); ?></td>
                              <!-- insurance -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Registered Address' || $deadline_type == '')) {

                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Registered Address Due Date">Registered Address Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) : ''); ?></td>
                              <!-- registed -->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Tax Advice' || $deadline_type == '')) { ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Tax Advice Due Date">Tax Advice Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : ''); ?></td>
                              <!-- tax advice-->
                            </tr>
                          <?php }

                          switch ($due_val) {
                            case 'over_due':
                              $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                              break;

                            default:
                              $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
                              break;
                          }

                          if (($zz) && ($deadline_type == 'Tax Investigation' || $deadline_type == '')) {
                          ?>

                            <tr class="for_table">
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                              <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                              <td data-search="Tax Investigation Due Date">Tax Investigation Due Date</td>
                              <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : ''); ?></td>
                              <!-- tax inve-->
                            </tr>
                            <?php }

                          foreach ($other_services as $key => $value) {
                            switch ($due_val) {
                              case 'over_due':
                                $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])));
                                break;

                              default:
                                $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])));
                                break;
                            }

                            if (($zz) && ($deadline_type == $value['service_name'] || $deadline_type == '')) {  ?>

                              <tr class="for_table">
                                <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo $getusername['crm_name']; ?></td>
                                <td data-search="<?php echo $getCompanyvalue['crm_legal_form']; ?>"><?php echo $getCompanyvalue['crm_legal_form']; ?></td>
                                <td data-search="<?php echo $getCompanyvalue['crm_company_name']; ?>"><?php echo $getCompanyvalue['crm_company_name']; ?></td>
                                <td data-search="<?php echo $value['service_name']; ?> Due Date"><?php echo $value['service_name']; ?> Due Date</td>
                                <td data-search="<?php echo $getusername['crm_name']; ?>"><?php echo (strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) : ''); ?></td>
                              </tr>
                          <?php }
                          }

                          ?>
                        <?php } ?>
                      </tbody>

                    </table>
                    <input type="hidden" class="rows_selected" id="select_client_count">
                  </div>

                </div>
              </div>
            </div>
            <!-- collapse panel -->

            <!-- for staus board -->
            <!-- company details -->

            <div class="overall-quote01 floating_set" style="display: none;">
              <div class="accident-toggle new">
                <h6>Status Board | Deadline Calender </h6>


              </div>




              <div class="deadlines-types1">

                <div class="f-right floating_set ">
                  <div class="search-vat1 cmn-inline-bg">
                    <label>Company Name:</label>
                    <input type="text" name="companysearch" id="companysearch" class="search" placeholder="Company Name">
                  </div>
                  <div class="company-search1 cmn-inline-bg">
                    <div class="vat-shipping">
                      <label>Company Type</label>
                      <select name="legal_form" class="fields search" id="legal_form">
                        <option value="" data-id="1">All</option>
                        <option value="Private Limited company" data-id="2">Private Limited company</option>
                        <option value="Public Limited company" data-id="3">Public Limited company</option>
                        <option value="Limited Liability Partnership" data-id="4">Limited Liability Partnership</option>
                        <option value="Partnership" data-id="5">Partnership</option>
                        <option value="Self Assessment" data-id="6">Self Assessment</option>
                        <option value="Trust" data-id="7">Trust</option>
                        <option value="Charity" data-id="8">Charity</option>
                        <option value="Other" data-id="9">Other</option>
                      </select>
                    </div>
                  </div>

                  <div class="dropdown-primary dropdown open for_ul_ajax_status cmn-inline-bg">
                    <button onclick="myFunctionStatus()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button>
                    <div id="myDropdown1" class="dropdown-menu" aria-labelledby="myDropdown1" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="" class="dropdown-item waves-light waves-effect but csv">Csv</a><a href="" class="dropdown-item waves-light waves-effect but pdf">Pdf</a><a href="" class="dropdown-item waves-light waves-effect but html" target="_blank">HTML</a></div>
                  </div>

                </div>

                <!--  <div class="accident-toggle"><h2>MR ACCIDENT SOLUTIONS LIMITED</h2></div> -->
                <div class="investigation-01">
                  <?php

                  $count = 1;
                  $c = 2;

                  $columns = array('1' => 'conf_statement', '2' => 'accounts', '3' => 'company_tax_return', '4' => 'personal_tax_return', '5' => 'vat', '6' => 'payroll', '7' => 'workplace', '8' => 'cis', '9' => 'cissub', '10' => 'bookkeep', '11' => 'p11d', '12' => 'management', '13' => 'investgate', '14' => 'registered', '15' => 'taxinvest', '16' => 'taxadvice');

                  foreach ($other_services as $key => $value) {
                    $columns[$value['id']] = $value['services_subnames'];
                  }

                  foreach ($service_list as $key => $value) {
                    if ($count / 6 == 1) {
                      $c = 2;
                      $count = 1;
                    }

                  ?>
                    <div class="color-switches trading-swithch<?php echo $c; ?>">
                      <button type="button" name="service_fil[]" id="<?php echo $columns[$value['id']]; ?>" class="btn btn-sm btn-toggle" data-toggle="button" aria-pressed="true" autocomplete="off">
                        <div class="handle"></div>
                      </button>
                      <label><?php echo $value['service_name']; ?></label>
                    </div>
                  <?php $c++;
                    $count++;
                  } ?>
                </div>
              </div>

            </div>

            <?php $this->load->view('Deadline_manager/reminder_invoice_table', array('service_list' => $service_list, 'service_charges' => $service_charges)); ?>
            <?php $this->load->view('Deadline_manager/reminder_invoice_popup'); ?>
            <!-- info table -->
            <!-- end of status board -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- management block -->
  <?php $this->load->view('includes/footer'); ?>


  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
  <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
  <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
  <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
  <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/user_page/materialize.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/custom/buttons.colVis.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/custom/datatable_extension.js"></script>

  <script type="text/javascript">
    function config_filter_section(select) {
      var table_id = select.closest('table').attr('id');
      var class_name = select.closest('th').attr('class').match(/\w+(?=_TH)/);
      class_name = '#' + table_id + ' .' + class_name + '_TH';

      var filter_setion = select.closest('.tab-pane').find('.filter-data.for-reportdash');

      filter_setion.find('.box-container span.remove[data-targetth="' + class_name + '"]').each(function() {

        var is_last_box = $(this).closest('.box-container').find('.box-item').length;
        $(this).closest('.box-item').remove();

        if (is_last_box == 1) {
          filter_setion.hide();
        }
      });
      var select_val = select.val();
      if (select_val.length) {
        filter_setion.show();
        $.each(select_val, function(i, v) {
          filter_setion.find('.box-container').append('<div class="btn btn-default box-item">' + v + '<span class="remove" data-targetth="' + class_name + '" data-value="' + v + '">x</span></div>');
        });
      }
    }

    $(document).ready(function() {
      $('.edit-toggle1').click(function() {
        $('.proposal-down').slideToggle(300);
      });

      $("#legal_form12").change(function() {
        var val = $(this).val();
        if (val === "from2") {
          // alert('hi');
          $(".from2option").show();
        } else {
          $(".from2option").hide();
        }
      });

      $(document).on('change', ".deadline_status", function() {
        var values = this.id;
        var rid = values.split('_');

        if (this.value == 'enable') {
          var stat = "on";
        } else {
          var stat = "off";
        }

        $(".LoadingImage").show();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>Deadline_manager/service_update/",
          data: {
            'values': values,
            'stat': stat
          },
          success: function(response) {
            $(".LoadingImage").hide();
            if (response == 1) {
              $("#suc_" + rid[1]).html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>');
              setTimeout(function() {
                $("#suc_" + rid[1]).html('');
              }, 2000);
              $(".search").trigger('change');
            } else {
              $("#suc_" + rid[1]).html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span style="color:red;">' + response + '</span></div></div></div>');
              $(".search").trigger('change');
            }
          },
        });

      });

      $(document).on('change', ".deadline_status_email", function() {
        var values = this.id;
        var rid = values.split('_');

        if (this.value == 'yes') {
          var stat = "on";
        } else {
          var stat = "off";
        }

        $(".LoadingImage").show();

        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>Deadline_manager/service_update_mail/",
          data: {
            'values': values,
            'stat': stat
          },
          success: function(response) {
            $(".LoadingImage").hide();
            if (response == 1) {
              $("#suc_" + rid[1]).html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>');
              setTimeout(function() {
                $("#suc_" + rid[1]).html('');
              }, 2000);
              $(".search").trigger('change');
            } else {
              $("#suc_" + rid[1]).html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span style="color:red;">' + response + '</span></div></div></div>');
              $(".search").trigger('change');
            }
          },
        });
      });

      // $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({ 
      //  //  "scrollX": true
      //  });
    });
  </script>
  <script type="text/javascript">
    $('#deadline_search').on('keyup', function() {
      // alert('zz');
      var due_val = $("input[name='for_due']:checked").val();
      var table_name = 'deadline';
      var company = $(this).val();
      var company_type = $('#company_type').val();
      var deadline_type = $("#deadline_type").val();
      var data = {};
      data['company'] = company;
      data['company_type'] = company_type;
      data['deadline_type'] = deadline_type;
      data['due_val'] = due_val;
      $(".LoadingImage").show();
      $.ajax({
        url: '<?php echo base_url(); ?>Deadline_manager/company_search_deadline_info/',
        type: 'POST',
        data: data,
        success: function(data) {
          $(".LoadingImage").hide();


          var all_Codes = $.ajax({
            type: "POST",
            dataType: "json",
            data: {
              table_name: table_name
            },
            url: "<?= base_url() ?>leads/get_hidden_data",
            async: false
          }).responseJSON;

          // alert(data);

          // $("#accordion").html('');
          // $("#accordion").html(data);
          $("#accordion_table").html('');
          $("#accordion_table").html(data);
          // $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({ 
          //  //    "scrollX": true
          //    });

          initialize_todaydatatable(all_Codes);
          initialize_weekdatatable(all_Codes);
          initialize_monthdatatable(all_Codes);
          initialize_yeardatatable(all_Codes);

          /** for count **/

          var active_tabs_name = $('#active_tabs_name').val();
          $('.tab-pane').removeClass('active');
          $('.tab-pane').each(function() {

            if ($(this).attr('id') == active_tabs_name) {
              var clsname = $(this).attr('class') + " active ";
              $(this).attr('class', clsname);
            }
          });
          /** for count **/
          $('.for_ul_ajax').html('');
          $('.for_ul_ajax').html(' <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button><div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url(); ?>Deadline_manager/deadline_exportCSV_new?company=' + company + '&company_type=' + company_type + '&deadline_type=' + deadline_type + '&due_val=' + due_val + '" class="dropdown-item waves-light waves-effect but" target="_blank">Csv</a><a href="<?php echo base_url(); ?>Deadline_manager/pdf_new?company=' + company + '&company_type=' + company_type + '&deadline_type=' + deadline_type + '&due_val=' + due_val + '" class="dropdown-item waves-light waves-effect but" target="_blank">Pdf</a><a href="<?php echo base_url(); ?>Deadline_manager/html_new?company=' + company + '&company_type=' + company_type + '&deadline_type=' + deadline_type + '&due_val=' + due_val + '" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>');
        },
      });

    });
    $('#company_type,#deadline_type').on('change', function() {
      // alert('zz');
      $(".LoadingImage").show();
      var due_val = $("input[name='for_due']:checked").val();
      var company = $('#deadline_search').val();
      var company_type = $('#company_type').val();
      var deadline_type = $("#deadline_type").val();
      var table_name = 'deadline';
      var data = {};
      data['company'] = company;
      data['company_type'] = company_type;
      data['deadline_type'] = deadline_type;
      data['due_val'] = due_val;
      $.ajax({
        url: '<?php echo base_url(); ?>Deadline_manager/company_search_deadline_info/',
        type: 'POST',
        data: data,
        success: function(data) {
          // alert(data);
          $(".LoadingImage").hide();

          // $("#accordion").html('');
          // $("#accordion").html(data);
          $("#accordion_table").html('');
          $("#accordion_table").html(data);

          var all_Codes = $.ajax({
            type: "POST",
            dataType: "json",
            data: {
              table_name: table_name
            },
            url: "<?= base_url() ?>leads/get_hidden_data",
            async: false
          }).responseJSON;
          initialize_todaydatatable(all_Codes);
          initialize_weekdatatable(all_Codes);
          initialize_monthdatatable(all_Codes);
          initialize_yeardatatable(all_Codes);

          // $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({ 
          //    //  "scrollX": true
          //    });

          /** for count **/

          var active_tabs_name = $('#active_tabs_name').val();
          $('.tab-pane').removeClass('active');
          $('.tab-pane').each(function() {

            if ($(this).attr('id') == active_tabs_name) {
              var clsname = $(this).attr('class') + " active ";
              $(this).attr('class', clsname);
            }
          });
          /** end of count **/
          $('.for_ul_ajax').html('');
          $('.for_ul_ajax').html(' <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button><div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url(); ?>Deadline_manager/deadline_exportCSV_new?company=' + company + '&company_type=' + company_type + '&deadline_type=' + deadline_type + '&due_val=' + due_val + '" class="dropdown-item waves-light waves-effect but" target="_blank">Csv</a><a href="<?php echo base_url(); ?>Deadline_manager/pdf_new?company=' + company + '&company_type=' + company_type + '&deadline_type=' + deadline_type + '&due_val=' + due_val + '" class="dropdown-item waves-light waves-effect but" target="_blank">Pdf</a><a href="<?php echo base_url(); ?>Deadline_manager/html_new?company=' + company + '&company_type=' + company_type + '&deadline_type=' + deadline_type + '&due_val=' + due_val + '" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>'); //old code
        },
      });

    });
    $('input[type=radio][name=for_due]').change(function() {
      var due_val = this.value;
      var company = $('#deadline_search').val();
      var company_type = $('#company_type').val();
      var deadline_type = $("#deadline_type").val();
      var table_name = 'deadline';
      $(".LoadingImage").show();
      var data = {};
      data['company'] = company;
      data['company_type'] = company_type;
      data['deadline_type'] = deadline_type;
      data['due_val'] = due_val;
      $.ajax({
        url: '<?php echo base_url(); ?>Deadline_manager/company_search_deadline_info/',
        type: 'POST',
        data: data,
        success: function(data) {
          // alert(data);
          $(".LoadingImage").hide();
          console.log(data);
          // $("#accordion").html('');
          // $("#accordion").html(data);
          $("#accordion_table").html('');
          $("#accordion_table").html(data);

          var all_Codes = $.ajax({
            type: "POST",
            dataType: "json",
            data: {
              table_name: table_name
            },
            url: "<?= base_url() ?>leads/get_hidden_data",
            async: false
          }).responseJSON;
          config_filter_section($(this));
          (all_Codes);
          initialize_weekdatatable(all_Codes);
          initialize_monthdatatable(all_Codes);
          initialize_yeardatatable(all_Codes);

          // $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({ 
          //    //  "scrollX": true
          //    });
          /** for count **/

          var active_tabs_name = $('#active_tabs_name').val();
          $('.tab-pane').removeClass('active');
          $('.tab-pane').each(function() {

            if ($(this).attr('id') == active_tabs_name) {
              var clsname = $(this).attr('class') + " active ";
              $(this).attr('class', clsname);
            }
          });
          /** end of count **/
          $('.for_ul_ajax').html('');
          $('.for_ul_ajax').html(' <button onclick="myFunction()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button><div id="myDropdown" class="dropdown-menu" aria-labelledby="myDropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url(); ?>Deadline_manager/deadline_exportCSV_new?company=' + company + '&company_type=' + company_type + '&deadline_type=' + deadline_type + '&due_val=' + due_val + '" class="dropdown-item waves-light waves-effect but" target="_blank">Csv</a><a href="<?php echo base_url(); ?>Deadline_manager/pdf_new?company=' + company + '&company_type=' + company_type + '&deadline_type=' + deadline_type + '&due_val=' + due_val + '" class="dropdown-item waves-light waves-effect but" target="_blank">Pdf</a><a href="<?php echo base_url(); ?>Deadline_manager/html_new?company=' + company + '&company_type=' + company_type + '&deadline_type=' + deadline_type + '&due_val=' + due_val + '" class="dropdown-item waves-light waves-effect but" target="_blank">HTML</a></div>');
        },
      });
    });

    function myFunction() {
      var due_val = $('input[type=radio][name=for_due]:checked').val();
      var company = $('#deadline_search').val();
      var company_type = $('#company_type').val();
      var deadline_type = $("#deadline_type").val();

      $('#myDropdown a.csv').attr('href', '<?php echo base_url(); ?>Deadline_manager/deadline_exportCSV_new?company=' + company + '&company_type=' + company_type + '&deadline_type=' + deadline_type + '&due_val=' + due_val + '');
      $('#myDropdown a.pdf').attr('href', '<?php echo base_url(); ?>Deadline_manager/pdf_new?company=' + company + '&company_type=' + company_type + '&deadline_type=' + deadline_type + '&due_val=' + due_val + '');
      $('#myDropdown a.html').attr('href', '<?php echo base_url(); ?>Deadline_manager/html_new?company=' + company + '&company_type=' + company_type + '&deadline_type=' + deadline_type + '&due_val=' + due_val + '');
      document.getElementById("myDropdown").classList.toggle("show");
    }

    function myFunctionStatus() {
      var company = $('#companysearch').val();
      var company_type = $('#legal_form').val();

      var f_h = $("#h_f").val();

      if (f_h) {
        var fields = $("button[id='" + f_h + "'").attr('id');
        var values = $("button[id='" + f_h + "'").attr("aria-pressed");
      } else {
        var fields = '';
        var values = '';
      }

      var for_fileds = [];
      var for_values = [];

      $('.btn-sm').each(function() {
        if ($(this).hasClass("active")) {
          var fields = $(this).attr('id');
          for_fileds.push(fields);
          var values = $(this).attr("aria-pressed");
          for_values.push(values);
        }
      });

      if (for_fileds.length > 0 && for_values.length > 0) {
        var fields = for_fileds.join(',');
        var values = for_values.join(',');
      } else {
        var fields = '';
        var values = '';
      }


      $('#myDropdown1 a.csv').attr('href', '<?php echo base_url(); ?>Deadline_manager/deadline_exportCSV_status?com_num=' + company + '&legal_form=' + company_type + '&fields=' + fields + '&values=' + values + '');
      $('#myDropdown1 a.pdf').attr('href', '<?php echo base_url(); ?>Deadline_manager/pdf_status?com_num=' + company + '&legal_form=' + company_type + '&fields=' + fields + '&values=' + values + '');
      $('#myDropdown1 a.html').attr('href', '<?php echo base_url(); ?>Deadline_manager/html_status?com_num=' + company + '&legal_form=' + company_type + '&fields=' + fields + '&values=' + values + '');
      document.getElementById("myDropdown").classList.toggle("show");
    }
  </script>




  <script type="text/javascript">
    $(document).on('click', '.Settings_Button', AddClass_SettingPopup);
    $(document).on('click', '.Button_List', AddClass_SettingPopup);

    $(document).on('click', '.dt-button.close', function() {
      $('.dt-buttons').find('.dt-button-collection').detach();
      $('.dt-buttons').find('.dt-button-background').detach();
    });

    function AddClass_SettingPopup() {
      $('button.Button_List').closest('.dt-button-collection').addClass('Settings_List');
      $('button.buttons-columnVisibility').closest('.dt-button-collection').addClass('columnVisibility_List');
    }

    $(document).ready(function() {
      $(document).on('change', "#legal_form", function() {
        $(".LoadingImage").show();

        var data = {};

        data['com_num'] = $("#companysearch").val();
        data['legal_form'] = $("#legal_form").val();
        //var values = $("button[name='service_fil[]']").map(function(){return $(this).attr("aria-pressed");}).get();
        var f_h = $("#h_f").val();
        // alert(f_h);
        if (f_h) {
          var fields = $("button[id='" + f_h + "'").attr('id');
          var values = $("button[id='" + f_h + "'").attr("aria-pressed");
        } else {
          var fields = '';
          var values = '';
        }
        var for_fileds = [];
        var for_values = [];
        $('.btn-sm').each(function() {
          if ($(this).hasClass("active")) {
            var fields = $(this).attr('id');
            for_fileds.push(fields);
            var values = $(this).attr("aria-pressed");
            for_values.push(values);
          }
        });
        // var fields = $(this).attr('id');
        // var values = $(this).attr("aria-pressed");
        if (for_fileds.length > 0 && for_values.length > 0) {
          var fields = for_fileds.join(',');
          var values = for_values.join(',');
        } else {
          var fields = '';
          var values = '';
        }


        data['values'] = values;
        data['fields'] = fields;

        // data['values'] = '';
        // data['fields'] = '';

        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>Deadline_manager/companysearch/",
          data: data,
          success: function(response) {
            $(".LoadingImage").hide();

            $(".filterrec").html(response);
            $("#servicedead14").dataTable({
              "iDisplayLength": 10,
              "scrollX": true,
              "dom": '<"staffprofilelist01">lfrtip'
            });
            $('.for_ul_ajax_status').html('');
            $('.for_ul_ajax_status').html(' <button onclick="myFunctionStatus()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button><div id="myDropdown1" class="dropdown-menu" aria-labelledby="myDropdown1" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url(); ?>Deadline_manager/deadline_exportCSV_status?com_num=' + data['com_num'] + '&legal_form=' + data['legal_form'] + '&values=' + data['values'] + '&fields=' + data['fields'] + '" class="dropdown-item waves-light waves-effect but csv" target="_blank">Csv</a><a href="<?php echo base_url(); ?>Deadline_manager/pdf_status?com_num=' + data['com_num'] + '&legal_form=' + data['legal_form'] + '&values=' + data['values'] + '&fields=' + data['fields'] + '" class="dropdown-item waves-light waves-effect but pdf" target="_blank">Pdf</a><a href="<?php echo base_url(); ?>Deadline_manager/html_status?com_num=' + data['com_num'] + '&legal_form=' + data['legal_form'] + '&values=' + data['values'] + '&fields=' + data['fields'] + '" class="dropdown-item waves-light waves-effect but html" target="_blank">HTML</a></div>');

          },
        });

      });

      $("#companysearch").on('keyup', function() {

        $(".LoadingImage").show();

        var data = {};

        data['com_num'] = $("#companysearch").val();
        data['legal_form'] = $("#legal_form").val();
        //var values = $("button[name='service_fil[]']").map(function(){return $(this).attr("aria-pressed");}).get();
        var f_h = $("#h_f").val();
        // alert(f_h);
        if (f_h) {
          var fields = $("button[id='" + f_h + "'").attr('id');
          var values = $("button[id='" + f_h + "'").attr("aria-pressed");
        } else {
          var fields = '';
          var values = '';
        }

        // data['values'] = '';
        // data['fields'] = '';
        var for_fileds = [];
        var for_values = [];
        $('.btn-sm').each(function() {
          if ($(this).hasClass("active")) {
            var fields = $(this).attr('id');
            for_fileds.push(fields);
            var values = $(this).attr("aria-pressed");
            for_values.push(values);
          }
        });
        // var fields = $(this).attr('id');
        // var values = $(this).attr("aria-pressed");
        // var fields = for_fileds.join(',');
        // var values = for_values.join(',');
        if (for_fileds.length > 0 && for_values.length > 0) {
          var fields = for_fileds.join(',');
          var values = for_values.join(',');
        } else {
          var fields = '';
          var values = '';
        }
        data['values'] = values;
        data['fields'] = fields;
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>Deadline_manager/companysearch/",
          data: data,
          success: function(response) {
            $(".LoadingImage").hide();

            $(".filterrec").html(response);
            $("#servicedead14").dataTable({
              "iDisplayLength": 10,
              "dom": '<"staffprofilelist01">lfrtip'
            });

            $('.for_ul_ajax_status').html('');
            $('.for_ul_ajax_status').html(' <button onclick="myFunctionStatus()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button><div id="myDropdown1" class="dropdown-menu" aria-labelledby="myDropdown1" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url(); ?>Deadline_manager/deadline_exportCSV_status?com_num=' + data['com_num'] + '&legal_form=' + data['legal_form'] + '&values=' + data['values'] + '&fields=' + data['fields'] + '" class="dropdown-item waves-light waves-effect but csv" target="_blank">Csv</a><a href="<?php echo base_url(); ?>Deadline_manager/pdf_status?com_num=' + data['com_num'] + '&legal_form=' + data['legal_form'] + '&values=' + data['values'] + '&fields=' + data['fields'] + '" class="dropdown-item waves-light waves-effect but pdf" target="_blank">Pdf</a><a href="<?php echo base_url(); ?>Deadline_manager/html_status?com_num=' + data['com_num'] + '&legal_form=' + data['legal_form'] + '&values=' + data['values'] + '&fields=' + data['fields'] + '" class="dropdown-item waves-light waves-effect but html" target="_blank">HTML</a></div>');

          },
        });

      });


      $(document).on('click', ".update_service", function() {

        var values = this.id;

        if ($(this).prop("checked") == true) {
          var stat = "on";
        } else if ($(this).prop("checked") == false) {
          var stat = "off";
        }
        $(".LoadingImage").show();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>staff/service_update/",
          data: {
            'values': values,
            'stat': stat
          },
          success: function(response) {
            $(".LoadingImage").hide();
            $(".service_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Service have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() {
              $('.service_succ');
            });

            /* $(".filterrec").html(response);
            $("#servicedead").dataTable({
            "iDisplayLength": 10,
            "dom": '<"staffprofilelist01">lfrtip'
            });*/
          },
        });

        /* var notChecked = [], checked = [];
        $(":checkbox").map(function() {
        this.checked ? checked.push(this.id) : notChecked.push(this.id);
        });
        alert("checked: " + checked);
        alert("not checked: " + notChecked);*/

      });

      //$(".btn-sm").click(function(){
      $(document).on('click', ".btn-sm", function() {

        //$(this).attr("aria-pressed")
        $(".LoadingImage").show();
        //var values = $("button[name='service_fil[]']").map(function(){return $(this).attr("aria-pressed");}).get();
        var for_fileds = [];
        var for_values = [];
        $('.btn-sm').each(function() {
          if ($(this).hasClass("active")) {
            var fields = $(this).attr('id');
            for_fileds.push(fields);
            var values = $(this).attr("aria-pressed");
            for_values.push(values);
          }
        });
        // var fields = $(this).attr('id');
        // var values = $(this).attr("aria-pressed");
        // var fields = for_fileds.join(',');
        // var values = for_values.join(',');
        if (for_fileds.length > 0 && for_values.length > 0) {
          var fields = for_fileds.join(',');
          var values = for_values.join(',');
        } else {
          var fields = '';
          var values = '';
        }
        $("#h_f").val(fields);
        // alert(values);
        var data = {};

        data['com_num'] = $("#companysearch").val();
        data['legal_form'] = $("#legal_form").val();
        /*data['com_num'] = '';
        data['legal_form'] = '';*/
        data['values'] = values;
        data['fields'] = fields;

        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>Deadline_manager/companysearch/",
          data: data,
          success: function(response) {
            console.log(response);
            $(".LoadingImage").hide();
            $(".filterrec").html(response);
            $("#servicedead14").dataTable({
              "iDisplayLength": 10,
              "dom": '<"staffprofilelist01">lfrtip'
            });

            $('.for_ul_ajax_status').html('');
            $('.for_ul_ajax_status').html(' <button onclick="myFunctionStatus()" class="btn btn-primary dropdown-toggle waves-effect waves-light dropbtn" id="myDropdown1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Export</button><div id="myDropdown1" class="dropdown-menu" aria-labelledby="myDropdown1" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a href="<?php echo base_url(); ?>Deadline_manager/deadline_exportCSV_status?com_num=' + data['com_num'] + '&legal_form=' + data['legal_form'] + '&values=' + data['values'] + '&fields=' + data['fields'] + '" class="dropdown-item waves-light waves-effect but csv" target="_blank">Csv</a><a href="<?php echo base_url(); ?>Deadline_manager/pdf_status?com_num=' + data['com_num'] + '&legal_form=' + data['legal_form'] + '&values=' + data['values'] + '&fields=' + data['fields'] + '" class="dropdown-item waves-light waves-effect but pdf" target="_blank">Pdf</a><a href="<?php echo base_url(); ?>Deadline_manager/html_status?com_num=' + data['com_num'] + '&legal_form=' + data['legal_form'] + '&values=' + data['values'] + '&fields=' + data['fields'] + '" class="dropdown-item waves-light waves-effect but html" target="_blank">HTML</a></div>');

          },
        });

      });

      $(".status").change(function(e) {
        //$('#alluser').on('change','.status',function () {
        //e.preventDefault();


        var rec_id = $(this).data('id');
        var stat = $(this).val();
        $(".LoadingImage").show();
        $.ajax({
          url: '<?php echo base_url(); ?>user/statusChange/',
          type: 'post',
          data: {
            'rec_id': rec_id,
            'status': stat
          },
          timeout: 3000,
          success: function(data) {
            //alert('ggg');
            $(".LoadingImage").hide();
            $(".status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() {
              $('.status_succ');
            });
            // setTimeout(resetAll,3000);
            //location.reload();
            setTimeout(function() { // wait for 5 secs(2)
              //location.reload(); // then reload the page.(3)
              $(".status_succ").hide();
            }, 3000);
            if (stat == '3') {

              //$this.closest('td').next('td').html('Active');
              $('#frozen' + rec_id).html('Frozen');

            } else {
              $this.closest('td').next('td').html('Inactive');
            }
          },
          error: function(errorThrown) {
            console.log(errorThrown);
          }
        });
      });


      $('.edit-toggle1').click(function() {
        $('.proposal-down').slideToggle(300);
      });

      $("#legal_form12").change(function() {
        var val = $(this).val();
        if (val === "from2") {
          // alert('hi');
          $(".from2option").show();
        } else {
          $(".from2option").hide();
        }
      });

      // $("#servicedead12").dataTable({
      //  // "iDisplayLength": 10 ,
      //  // "scrollX": true

      //  });

      $("#servicedead14").dataTable({
        // "iDisplayLength": 10 ,
        // "scrollX": true

      });


    });
  </script>

  <script type="text/javascript">
    /** find table row **/


    $(document).on('click', '.for_link', function() {
      //alert($(this).attr('data-id'));
      $('#active_tabs_name').val($(this).attr('data-id'));

    });

    $(document).on('click', '#close_info_msg', function() {
      $('.popup_info_msg').hide();

    });


    // VAT
    // Payroll
    // Accounts
    // Confirmation statement
    // Company Tax Return
    // Personal Tax Return
    // WorkPlace Pension - AE
    // CIS - Contractor
    // CIS - Sub Contractor
    // P11D
    // Bookkeeping
    // Management Accounts
    // Investigation Insurance
    // Registered Address
    // Tax Advice
    // Tax Investigation

    $(document).ready(function() {
      <?php
      if (isset($_SESSION['firm_seen'])) { ?>
        $(".this_year").trigger('click');
        <?php
        if ($_SESSION['firm_seen'] == 'VAT') { ?>
          $("#deadline_type").val('VAT').trigger('change');
          $
        <?php } ?>
        <?php
        if ($_SESSION['firm_seen'] == 'Payroll') { ?>
          $("#deadline_type").val('Payroll').trigger('change');
        <?php } ?>
        <?php
        if ($_SESSION['firm_seen'] == 'Accounts') { ?>
          $("#deadline_type").val('Accounts').trigger('change');
        <?php } ?>
        <?php
        if ($_SESSION['firm_seen'] == 'Confirmation Statement') { ?>
          $("#deadline_type").val('Confirmation Statement').trigger('change');
        <?php } ?>
        <?php
        if ($_SESSION['firm_seen'] == 'Company Tax Return') { ?>
          $("#deadline_type").val('Company Tax Return').trigger('change');
        <?php } ?>
        <?php
        if ($_SESSION['firm_seen'] == 'Personal Tax Return') { ?>
          $("#deadline_type").val('Personal Tax Return').trigger('change');
        <?php } ?>

        <?php
        if ($_SESSION['firm_seen'] == 'WorkPlace Pension - AE') { ?>
          $("#deadline_type").val('WorkPlace Pension - AE').trigger('change');
        <?php } ?>

        <?php
        if ($_SESSION['firm_seen'] == 'CIS - Contractor') { ?>
          $("#deadline_type").val('CIS - Contractor').trigger('change');
        <?php } ?>

        <?php
        if ($_SESSION['firm_seen'] == 'CIS - Sub Contractor') { ?>
          $("#deadline_type").val('CIS - Sub Contractor').trigger('change');
        <?php } ?>

        <?php
        if ($_SESSION['firm_seen'] == 'P11D') { ?>
          $("#deadline_type").val('P11D').trigger('change');
        <?php } ?>

        <?php
        if ($_SESSION['firm_seen'] == 'Bookkeeping') { ?>
          $("#deadline_type").val('Bookkeeping').trigger('change');
        <?php } ?>

        <?php
        if ($_SESSION['firm_seen'] == 'Management Accounts') { ?>
          $("#deadline_type").val('Management Accounts').trigger('change');
        <?php } ?>

        <?php
        if ($_SESSION['firm_seen'] == 'Investigation Insurance') { ?>
          $("#deadline_type").val('Investigation Insurance').trigger('change');
        <?php } ?>

        <?php
        if ($_SESSION['firm_seen'] == 'Registered Address') { ?>
          $("#deadline_type").val('Registered Address').trigger('change');
        <?php } ?>

        <?php
        if ($_SESSION['firm_seen'] == 'Tax Investigation/Tax Advice') { ?>
          $("#deadline_type").val('Tax Investigation/Tax Advice').trigger('change');
        <?php } ?>
        <?php
        if ($_SESSION['firm_seen'] == 'Tax Investigation') { ?>
          $("#deadline_type").val('Tax Investigation').trigger('change');
        <?php } ?>
        <?php
        if ($_SESSION['firm_seen'] != '') { ?>
          $("#deadline_type").val('<?php echo $_SESSION['firm_seen']; ?>').trigger('change');
        <?php } ?>
      <?php } ?>

    });

    function initialize_todaydatatable(response = null) {
      <?php
      $column_setting = Firm_column_settings('deadline');

      ?>

      if (response == null) {
        var column_order = <?php echo $column_setting['order'] ?>;

        var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;
      } else {
        var order = [];
        var hidden = [];

        JSON.parse(response.order, (key, value) => {

          order.push(value);

        });
        JSON.parse(response.hidden, (key, value) => {
          hidden.push(value);

        });

        //var newArr = 

        var column_order = order.slice(0, -1);

        var hidden_coulmns = hidden.slice(0, -1);

      }

      console.log(column_order);
      var column_ordering = [];

      $.each(column_order, function(i, v) {

        var index = $('#today_deadlineinfo thead  th.' + v).index();

        if (index !== -1) {
          column_ordering.push(index);
        }
      });

      var numCols = $('#today_deadlineinfo thead th').length;
      //alert(numCols);
      var table10 = $('#today_deadlineinfo').DataTable({
        "dom": '<"toolbar-table" B>lfrtip',
        buttons: [{
          extend: 'collection',
          background: false,
          text: '<i class="fa fa-cog" aria-hidden="true"></i>',
          className: 'Settings_Button',
          buttons: [{
              text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
              className: 'Button_List',
              action: function(e, dt, node, config) {
                $('div.toolbar-table .toolbar-search li.dropdown-chose').each(function() {
                  $(this).trigger('click');
                });


                MainStatus_Tab = "all_task";

                Redraw_Table(table10);
              }
            },
            {
              extend: 'colvis',
              text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
              columns: ':not(.Exc-colvis)',
              className: 'Button_List',
              prefixButtons: [{
                text: 'X',
                className: 'close'
              }]
            }
          ]
        }],
        order: [], //ITS FOR DISABLE SORTING


        fixedHeader: {
          header: true,
          footer: true
        },

        colReorder: {
          realtime: false,
          order: column_ordering,
          fixedColumnsLeft: 1

        },
        initComplete: function() {


          var api = this.api();

          api.columns('.hasFilter').every(function() {

            var column = this;
            var TH = $(column.header());
            var Filter_Select = TH.find('.filter_check');
            ///alert(Filter_Select.length);
            // alert('check');
            if (!Filter_Select.length) {
              // alert('check1');
              Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo(TH);
              var unique_data = [];
              column.nodes().each(function(d, j) {
                var dataSearch = $(d).attr('data-search');

                if (jQuery.inArray(dataSearch, unique_data) === -1) {
                  //console.log(d);
                  Filter_Select.append('<option  value="' + dataSearch + '">' + dataSearch + '</option>');
                  unique_data.push(dataSearch);
                }

              });
            }


            Filter_Select.on('change', function() {
              config_filter_section($(this));
              var search = $(this).val();
              //console.log( search );
              if (search.length) search = '^(' + search.join('|') + ')$';

              var class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);

              var cur_column = api.column('.' + class_name + '_TH');

              cur_column.search(search, true, false).draw();
            });

            //console.log(select.attr('style'));
            Filter_Select.formSelect();


          });


        }
      });

      var today = table10.page.info().recordsTotal;
      $('.count_today').html(today);

      ColVis_Hide(table10, hidden_coulmns);

      Filter_IconWrap();

      Change_Sorting_Event(table10);

      ColReorder_Backend_Update(table10, 'deadline');

      ColVis_Backend_Update(table10, 'deadline');

      $(document).on('click', '#today .filter-data.for-reportdash #clear_container', function() {
        Redraw_Table(table10);
      });


      /*var Reset_filter = "<li><button id='reset_filter_button1' >Reset Filter</button></li>";

      $("#today_deadlineinfo_wrapper .toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

      $('#reset_filter_button1').click();*/

    }

    function initialize_weekdatatable(response = null) {
      <?php
      $column_setting = Firm_column_settings('deadline');

      ?>

      if (response == null) {
        var column_order = <?php echo $column_setting['order'] ?>;

        var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;
      } else {
        var order = [];
        var hidden = [];

        JSON.parse(response.order, (key, value) => {

          order.push(value);

        });
        JSON.parse(response.hidden, (key, value) => {
          hidden.push(value);

        });

        //var newArr = 

        var column_order = order.slice(0, -1);

        var hidden_coulmns = hidden.slice(0, -1);

      }


      var column_ordering = [];

      $.each(column_order, function(i, v) {

        var index = $('#thisweek_deadlineinfo thead  th.' + v).index();

        if (index !== -1) {
          column_ordering.push(index);
        }
      });

      var numCols = $('#thisweek_deadlineinfo thead th').length;
      //alert(numCols);
      var table20 = $('#thisweek_deadlineinfo').DataTable({
        "dom": '<"toolbar-table" B>lfrtip',
        buttons: [{
          extend: 'collection',
          background: false,
          text: '<i class="fa fa-cog" aria-hidden="true"></i>',
          className: 'Settings_Button',
          buttons: [{
              text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
              className: 'Button_List',
              action: function(e, dt, node, config) {
                $('div.toolbar-table .toolbar-search li.dropdown-chose').each(function() {
                  $(this).trigger('click');
                });


                MainStatus_Tab = "all_task";

                Redraw_Table(table20);


              }
            },
            {
              extend: 'colvis',
              text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
              columns: ':not(.Exc-colvis)',
              className: 'Button_List',
              prefixButtons: [{
                text: 'X',
                className: 'close'
              }]
            }
          ]
        }],
        order: [], //ITS FOR DISABLE SORTING


        fixedHeader: {
          header: true,
          footer: true
        },

        colReorder: {
          realtime: false,
          order: column_ordering,
          fixedColumnsLeft: 1

        },
        initComplete: function() {


          var api = this.api();

          api.columns('.hasFilter').every(function() {

            var column = this;
            var TH = $(column.header());
            var Filter_Select = TH.find('.filter_check');
            ///alert(Filter_Select.length);
            // alert('check');
            if (!Filter_Select.length) {
              // alert('check1');
              Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo(TH);
              var unique_data = [];
              column.nodes().each(function(d, j) {
                var dataSearch = $(d).attr('data-search');

                if (jQuery.inArray(dataSearch, unique_data) === -1) {
                  //console.log(d);
                  Filter_Select.append('<option  value="' + dataSearch + '">' + dataSearch + '</option>');
                  unique_data.push(dataSearch);
                }

              });
            }


            Filter_Select.on('change', function() {

              config_filter_section($(this));

              var search = $(this).val();
              //console.log( search );
              if (search.length) search = '^(' + search.join('|') + ')$';

              var class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);

              var cur_column = api.column('.' + class_name + '_TH');

              cur_column.search(search, true, false).draw();
            });

            //console.log(select.attr('style'));
            Filter_Select.formSelect();


          });


        }
      });

      var thisweek = table20.page.info().recordsTotal;
      $('.count_week').html(thisweek);

      ColVis_Hide(table20, hidden_coulmns);

      //Filter_IconWrap();

      Change_Sorting_Event(table20);

      ColReorder_Backend_Update(table20, 'deadline');

      ColVis_Backend_Update(table20, 'deadline');

      /*var Reset_filter = "<li><button id='reset_filter_button2' >Reset Filter</button></li>";

      $("#thisweek_deadlineinfo_wrapper .toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

      $('#reset_filter_button2').click(function(){



      });*/

      $(document).on('click', '#this-week .filter-data.for-reportdash #clear_container', function() {
        Redraw_Table(table20);
      });


    }

    function initialize_monthdatatable(response = null) {
      <?php
      $column_setting = Firm_column_settings('deadline');

      ?>

      if (response == null) {
        var column_order = <?php echo $column_setting['order'] ?>;

        var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;
      } else {
        var order = [];
        var hidden = [];

        JSON.parse(response.order, (key, value) => {

          order.push(value);

        });
        JSON.parse(response.hidden, (key, value) => {
          hidden.push(value);

        });

        //var newArr = 

        var column_order = order.slice(0, -1);

        var hidden_coulmns = hidden.slice(0, -1);

      }

      var column_ordering = [];

      $.each(column_order, function(i, v) {

        var index = $('#thismonth_deadlineinfo thead  th.' + v).index();

        if (index !== -1) {
          column_ordering.push(index);
        }
      });

      var numCols = $('#thismonth_deadlineinfo thead th').length;
      //alert(numCols);
      var table30 = $('#thismonth_deadlineinfo').DataTable({
        "dom": '<"toolbar-table" B>lfrtip',
        buttons: [{
          extend: 'collection',
          background: false,
          text: '<i class="fa fa-cog" aria-hidden="true"></i>',
          className: 'Settings_Button',
          buttons: [{
              text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
              className: 'Button_List',
              action: function(e, dt, node, config) {
                $('div.toolbar-table .toolbar-search li.dropdown-chose').each(function() {
                  $(this).trigger('click');
                });


                MainStatus_Tab = "all_task";

                Redraw_Table(table30);
              }
            },
            {
              extend: 'colvis',
              text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
              columns: ':not(.Exc-colvis)',
              className: 'Button_List',
              prefixButtons: [{
                text: 'X',
                className: 'close'
              }]
            }
          ]
        }],
        order: [], //ITS FOR DISABLE SORTING


        fixedHeader: {
          header: true,
          footer: true
        },

        colReorder: {
          realtime: false,
          order: column_ordering,
          fixedColumnsLeft: 1

        },
        initComplete: function() {


          var api = this.api();

          api.columns('.hasFilter').every(function() {

            var column = this;
            var TH = $(column.header());
            var Filter_Select = TH.find('.filter_check');
            ///alert(Filter_Select.length);
            // alert('check');
            if (!Filter_Select.length) {

              // alert('check1');
              Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo(TH);
              var unique_data = [];
              column.nodes().each(function(d, j) {
                var dataSearch = $(d).attr('data-search');

                if (jQuery.inArray(dataSearch, unique_data) === -1) {
                  //console.log(d);
                  Filter_Select.append('<option  value="' + dataSearch + '">' + dataSearch + '</option>');
                  unique_data.push(dataSearch);
                }

              });
            }


            Filter_Select.on('change', function() {
              config_filter_section($(this));

              var search = $(this).val();
              //console.log( search );
              if (search.length) search = '^(' + search.join('|') + ')$';

              var class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);

              var cur_column = api.column('.' + class_name + '_TH');

              cur_column.search(search, true, false).draw();
            });

            //console.log(select.attr('style'));
            Filter_Select.formSelect();


          });


        }
      });

      var month = table30.page.info().recordsTotal;
      $('.count_month').html(month);

      ColVis_Hide(table30, hidden_coulmns);

      // Filter_IconWrap();

      Change_Sorting_Event(table30);

      ColReorder_Backend_Update(table30, 'deadline');

      ColVis_Backend_Update(table30, 'deadline');
      $(document).on('click', '#this-month .filter-data.for-reportdash #clear_container', function() {
        Redraw_Table(table30);
      });

      /*var Reset_filter = "<li><button id='reset_filter_button3' >Reset Filter</button></li>";

      $("#thismonth_deadlineinfo_wrapper .toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

      $('#reset_filter_button3').click(function(){




      });*/


    }


    function initialize_yeardatatable(response = null) {
      <?php
      $column_setting = Firm_column_settings('deadline');

      ?>

      if (response == null) {
        var column_order = <?php echo $column_setting['order'] ?>;

        var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;
      } else {
        var order = [];
        var hidden = [];

        JSON.parse(response.order, (key, value) => {

          order.push(value);

        });
        JSON.parse(response.hidden, (key, value) => {
          hidden.push(value);

        });

        //var newArr = 

        var column_order = order.slice(0, -1);

        var hidden_coulmns = hidden.slice(0, -1);

      }


      var column_ordering = [];

      $.each(column_order, function(i, v) {

        var index = $('#thisyear_deadlineinfo thead  th.' + v).index();

        if (index !== -1) {
          column_ordering.push(index);
        }
      });

      var numCols = $('#thisyear_deadlineinfo thead th').length;
      //alert(numCols);
      var table40 = $('#thisyear_deadlineinfo').DataTable({
        "dom": '<"toolbar-table" B>lfrtip',
        buttons: [{
          extend: 'collection',
          background: false,
          text: '<i class="fa fa-cog" aria-hidden="true"></i>',
          className: 'Settings_Button',
          buttons: [{
              text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
              className: 'Button_List',
              action: function(e, dt, node, config) {

                $('div.toolbar-table .toolbar-search li.dropdown-chose').each(function() {
                  $(this).trigger('click');
                });


                MainStatus_Tab = "all_task";

                Redraw_Table(table40);


              }
            },
            {
              extend: 'colvis',
              text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
              columns: ':not(.Exc-colvis)',
              className: 'Button_List',
              prefixButtons: [{
                text: 'X',
                className: 'close'
              }]
            }
          ]
        }],
        order: [], //ITS FOR DISABLE SORTING


        fixedHeader: {
          header: true,
          footer: true
        },

        colReorder: {
          realtime: false,
          order: column_ordering,
          fixedColumnsLeft: 1

        },
        initComplete: function() {


          var api = this.api();

          api.columns('.hasFilter').every(function() {

            var column = this;
            var TH = $(column.header());
            var Filter_Select = TH.find('.filter_check');
            ///alert(Filter_Select.length);
            // alert('check');
            if (!Filter_Select.length) {
              // alert('check1');
              Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo(TH);
              var unique_data = [];
              column.nodes().each(function(d, j) {
                var dataSearch = $(d).attr('data-search');

                if (jQuery.inArray(dataSearch, unique_data) === -1) {
                  //console.log(d);
                  Filter_Select.append('<option  value="' + dataSearch + '">' + dataSearch + '</option>');
                  unique_data.push(dataSearch);
                }

              });
            }


            Filter_Select.on('change', function() {
              config_filter_section($(this));

              var search = $(this).val();
              //console.log( search );
              if (search.length) search = '^(' + search.join('|') + ')$';

              var class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);

              var cur_column = api.column('.' + class_name + '_TH');

              cur_column.search(search, true, false).draw();
            });

            //console.log(select.attr('style'));
            Filter_Select.formSelect();


          });


        }
      });

      var year = table40.page.info().recordsTotal;
      $('.count_year').html(year);

      ColVis_Hide(table40, hidden_coulmns);

      // Filter_IconWrap();

      Change_Sorting_Event(table40);

      ColReorder_Backend_Update(table40, 'deadline');

      ColVis_Backend_Update(table40, 'deadline');

      $(document).on('click', '#this-year .filter-data.for-reportdash #clear_container', function() {
        Redraw_Table(table40);
      });
      // Reset js by Ram//

      /*var Reset_filter = "<li><button id='reset_filter_button' >Reset Filter</button></li>";

      $("#thisyear_deadlineinfo_wrapper .toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

      $('#reset_filter_button').click(function(){


      });*/


    }


    $(document).ready(function() {

      initialize_todaydatatable();
      initialize_weekdatatable();
      initialize_monthdatatable();
      initialize_yeardatatable();

    });



    // $(document).on("change",".select_client",function(event)
    //    {
    //      var IsArchive=$('.tap_click.active').attr("data-id").trim();

    //      var tr=0;
    //      var ck_tr=0;
    //      table40.column('.select_row_TH').nodes().to$().each(function(index) {
    //      if( $(this).find(".select_client").is(":checked") )
    //        {
    //          ck_tr++;
    //        }
    //        tr++;
    //      });

    //        if(tr==ck_tr)
    //        {
    //          $("#select_all_client").prop("checked",true);
    //        } 
    //        else
    //        {
    //          $("#select_all_client").prop("checked",false);
    //        } 
    //      if(ck_tr)
    //      {
    //        toggle_action_button(1,IsArchive);
    //      }
    //      else  
    //      {
    //        toggle_action_button(0,IsArchive);
    //      } 

    //      $("#select_client_count").val(ck_tr+ " Selected");

    //    });
  </script>