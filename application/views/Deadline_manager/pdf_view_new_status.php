<!DOCTYPE html>
<html>
   <head>
      <style>
         #pdf {
         font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
         border-collapse: collapse;
         width: 100%;
         }
         #pdf td, #pdf th {
         border: 1px solid #ddd;
         padding: 8px;
         }
         #pdf tr:nth-child(even){background-color: #f2f2f2;}
         #pdf td
         {
         font-size: 13px;
         }
         #pdf th {
         padding-top: 12px;
         padding-bottom: 12px;
         text-align: left;
         background-color: #9E9E9E;
         color: white;
         font-size: 14px !important;
         white-space: nowrap;
         }
         p{
         color: #D7CCC8;
         }
         caption{
         color: #555;
         font-size: 22px;
         margin-bottom: 20px;
         }
         @page
         {
          margin: 10px;
         }
      </style>
   </head>
   <body>
      <center><h4><?php echo $title; ?></h4></center>
      <!-- for today -->
      <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%" cellpadding="5" style="border:1px solid #ccc;border-collapse:collapse;">
         <thead>
            <!-- <tr class="text-uppercase"><th colspan="5">Today</th></tr> -->
            <tr class="text-uppercase">
               <th style="border: 1px solid #ddd;text-align:left;font-size:13px;">Client Name</th>
               <th style="border: 1px solid #ddd;text-align:left;font-size:13px;">Client Type</th>
               <?php foreach($service_list as $key => $value){ ?>
               <th style="border: 1px solid #ddd;text-align:left;font-size:13px;"><?php echo $value['service_name']; ?></th>
               <?php } ?>
            </tr>
         </thead>
         <tbody>
            <?php foreach($services as $key => $value) { ?>            
            <tr class="for_table">
               <td><a><?php echo $value['crm_name']; ?></a></td>
               <td><?php echo $value['crm_legal_form']; ?></td>
               <?php foreach($service_list as $key1 => $value1){ ?>
               <td><?php echo 'Due: '.$value[$value1['service_name']]['charge'].',Basic: '.$value[$value1['service_name']]['basic']; ?></td>  
               <?php } ?>            
            </tr>
            <?php } ?>
         </tbody>
      </table>
      <!-- end of this year-->
   </body>
</html>