
                 
                     
                        <div class="accordion-panel">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse"
                                    data-parent="#accordion" href="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                                 Today
                                 </a>
                              </h3>
                           </div>
                           <div id="collapseOne" class="panel-collapse in collapse show" role="tabpanel" aria-labelledby="headingOne">
                              <div class="accordion-content accordion-desc">
                                 <table class="table client_table1 text-center " id="today_deadlineinfo">
                                    <thead>
                                       <th>Client Name</th>
                                       <th>Client Type</th>
                                       <th>Company Name</th>
                                       <th>VAT Due Date</th>
                                       <th>Payroll Due Date</th>
                                       <th>Accounts Due Date</th>
                                       <th>Confirmation statement Due Date</th>
                                       <th>Company Tax Return Due Date</th>
                                       <th>Personal Tax Return Due Date</th>
                                       <th>WorkPlace Pension - AE Due Date</th>
                                       <th>CIS - Contractor Due Date</th>
                                       <th>CIS - Sub Contractor Due Date</th>
                                       <th>P11D Due Date</th>
                                       <th>Bookkeeping Due Date</th>
                                       <th>Management Accounts Due Date</th>
                                       <th>Investigation Insurance Due Date</th>
                                       <th>Registered Address Due Date</th>
                                       <th>Tax Advice Due Date</th>
                                       <th>Tax Investigation Due Date</th>
                                    </thead>

                                    <tfoot class="ex_data1">
                                    <tr>
                                    <th>
                                    </th>
                                    </tr>
                                    </tfoot>

                                    <tbody>
                                       <!-- <tr>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>2018-05-02</td>
                                          </tr> -->
                                       <?php 
                                          foreach ($getCompany_today as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                                          ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                                          
                                          (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                                          ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                                          
                                          (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                                          ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                                          ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                                          ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                                          
                                          (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                          ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                                          
                                          (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                          ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                          ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                                          ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                          ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                                          ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                                          ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                                          ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                                          ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                                          ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                          ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                                          ?>
                                       <tr>
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_confirmation_statement_due_date']; ?></td>
                                          <!-- confi-->
                                          <td><?php echo $getCompanyvalue['crm_ch_accounts_next_due']; ?></td>
                                          <!-- account -->
                                          <td><?php echo $getCompanyvalue['crm_accounts_tax_date_hmrc']; ?></td>
                                          <!--company tax -->
                                          <td><?php echo $getCompanyvalue['crm_personal_due_date_return']; ?></td>
                                          <!-- crm personal tax -->
                                          <td><?php echo $getCompanyvalue['crm_vat_due_date']; ?></td>
                                          <!-- vat -->
                                          <td><?php echo $getCompanyvalue['crm_rti_deadline']; ?></td>
                                          <!-- payrol -->
                                          <td><?php echo $getCompanyvalue['crm_pension_subm_due_date']; ?></td>
                                          <!--workplace -->
                                          <td></td>
                                          <!-- CIS - Contractor -->
                                          <td></td>
                                          <!-- CIS - Sub Contractor-->
                                          <td><?php echo $getCompanyvalue['crm_next_p11d_return_due']; ?></td>
                                          <!-- p11d -->
                                          <td><?php echo $getCompanyvalue['crm_next_manage_acc_date'];  ?></td>
                                          <!-- manage account -->
                                          <td><?php echo $getCompanyvalue['crm_next_booking_date'] ?></td>
                                          <!-- booking-->
                                          <td><?php echo $getCompanyvalue['crm_insurance_renew_date']; ?></td>
                                          <!-- insurance -->
                                          <td><?php echo $getCompanyvalue['crm_registered_renew_date']; ?></td>
                                          <!-- registed -->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax advice-->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax inve-->
                                       </tr>
                                       <?php   }  ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                        <div class="accordion-panel">
                           <div class="accordion-heading" role="tab" id="headingTwo">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse"
                                    data-parent="#accordion" href="#collapseTwo"
                                    aria-expanded="false"
                                    aria-controls="collapseTwo">
                                 This week
                                 </a>
                              </h3>
                           </div>
                           <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                              <div class="accordion-content accordion-desc">
                                 <table class="table client_table1 text-center " id="thisweek_deadlineinfo">
                                    <thead>
                                       <th>Client Name</th>
                                       <th>Client Type</th>
                                       <th>Company Name</th>
                                       <th>VAT Due Date</th>
                                       <th>Payroll Due Date</th>
                                       <th>Accounts Due Date</th>
                                       <th>Confirmation statement Due Date</th>
                                       <th>Company Tax Return Due Date</th>
                                       <th>Personal Tax Return Due Date</th>
                                       <th>WorkPlace Pension - AE Due Date</th>
                                       <th>CIS - Contractor Due Date</th>
                                       <th>CIS - Sub Contractor Due Date</th>
                                       <th>P11D Due Date</th>
                                       <th>Bookkeeping Due Date</th>
                                       <th>Management Accounts Due Date</th>
                                       <th>Investigation Insurance Due Date</th>
                                       <th>Registered Address Due Date</th>
                                       <th>Tax Advice Due Date</th>
                                       <th>Tax Investigation Due Date</th>
                                    </thead>
                                    <tbody>
                                       <!-- <tr>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>2018-05-02</td>
                                          </tr> -->
                                       <?php 
                                          foreach ($getCompany_oneweek as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                                          ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                                          
                                          (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                                          ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                                          
                                          (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                                          ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                                          ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                                          ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                                          
                                          (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                          ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                                          
                                          (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                          ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                          ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                                          ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                          ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                                          ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                                          ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                                          ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                                          ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                                          ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                          ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                                          ?>
                                       <tr>
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_confirmation_statement_due_date']; ?></td>
                                          <!-- confi-->
                                          <td><?php echo $getCompanyvalue['crm_ch_accounts_next_due']; ?></td>
                                          <!-- account -->
                                          <td><?php echo $getCompanyvalue['crm_accounts_tax_date_hmrc']; ?></td>
                                          <!--company tax -->
                                          <td><?php echo $getCompanyvalue['crm_personal_due_date_return']; ?></td>
                                          <!-- crm personal tax -->
                                          <td><?php echo $getCompanyvalue['crm_vat_due_date']; ?></td>
                                          <!-- vat -->
                                          <td><?php echo $getCompanyvalue['crm_rti_deadline']; ?></td>
                                          <!-- payrol -->
                                          <td><?php echo $getCompanyvalue['crm_pension_subm_due_date']; ?></td>
                                          <!--workplace -->
                                          <td></td>
                                          <!-- CIS - Contractor -->
                                          <td></td>
                                          <!-- CIS - Sub Contractor-->
                                          <td><?php echo $getCompanyvalue['crm_next_p11d_return_due']; ?></td>
                                          <!-- p11d -->
                                          <td><?php echo $getCompanyvalue['crm_next_manage_acc_date'];  ?></td>
                                          <!-- manage account -->
                                          <td><?php echo $getCompanyvalue['crm_next_booking_date'] ?></td>
                                          <!-- booking-->
                                          <td><?php echo $getCompanyvalue['crm_insurance_renew_date']; ?></td>
                                          <!-- insurance -->
                                          <td><?php echo $getCompanyvalue['crm_registered_renew_date']; ?></td>
                                          <!-- registed -->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax advice-->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax inve-->
                                       </tr>
                                       <?php   }  ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                        <div class="accordion-panel">
                           <div class=" accordion-heading" role="tab" id="headingThree">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse"
                                    data-parent="#accordion" href="#collapseThree"
                                    aria-expanded="false"
                                    aria-controls="collapseThree">
                                 This Month
                                 </a>
                              </h3>
                           </div>
                           <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                              <div class="accordion-content accordion-desc">
                                 <table class="table client_table1 text-center " id="thismonth_deadlineinfo">
                                    <thead>
                                       <th>Client Name</th>
                                       <th>Client Type</th>
                                       <th>Company Name</th>
                                       <th>VAT Due Date</th>
                                       <th>Payroll Due Date</th>
                                       <th>Accounts Due Date</th>
                                       <th>Confirmation statement Due Date</th>
                                       <th>Company Tax Return Due Date</th>
                                       <th>Personal Tax Return Due Date</th>
                                       <th>WorkPlace Pension - AE Due Date</th>
                                       <th>CIS - Contractor Due Date</th>
                                       <th>CIS - Sub Contractor Due Date</th>
                                       <th>P11D Due Date</th>
                                       <th>Bookkeeping Due Date</th>
                                       <th>Management Accounts Due Date</th>
                                       <th>Investigation Insurance Due Date</th>
                                       <th>Registered Address Due Date</th>
                                       <th>Tax Advice Due Date</th>
                                       <th>Tax Investigation Due Date</th>
                                    </thead>
                                    <tbody>
                                       <!-- <tr>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>2018-05-02</td>
                                          </tr> -->
                                       <?php 
                                          foreach ($getCompany_onemonth as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                                          ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                                          
                                          (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                                          ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                                          
                                          (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                                          ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                                          ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                                          ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                                          
                                          (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                          ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                                          
                                          (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                          ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                          ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                                          ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                          ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                                          ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                                          ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                                          ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                                          ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                                          ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                          ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                                          ?>
                                       <tr>
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_confirmation_statement_due_date']; ?></td>
                                          <!-- confi-->
                                          <td><?php echo $getCompanyvalue['crm_ch_accounts_next_due']; ?></td>
                                          <!-- account -->
                                          <td><?php echo $getCompanyvalue['crm_accounts_tax_date_hmrc']; ?></td>
                                          <!--company tax -->
                                          <td><?php echo $getCompanyvalue['crm_personal_due_date_return']; ?></td>
                                          <!-- crm personal tax -->
                                          <td><?php echo $getCompanyvalue['crm_vat_due_date']; ?></td>
                                          <!-- vat -->
                                          <td><?php echo $getCompanyvalue['crm_rti_deadline']; ?></td>
                                          <!-- payrol -->
                                          <td><?php echo $getCompanyvalue['crm_pension_subm_due_date']; ?></td>
                                          <!--workplace -->
                                          <td></td>
                                          <!-- CIS - Contractor -->
                                          <td></td>
                                          <!-- CIS - Sub Contractor-->
                                          <td><?php echo $getCompanyvalue['crm_next_p11d_return_due']; ?></td>
                                          <!-- p11d -->
                                          <td><?php echo $getCompanyvalue['crm_next_manage_acc_date'];  ?></td>
                                          <!-- manage account -->
                                          <td><?php echo $getCompanyvalue['crm_next_booking_date'] ?></td>
                                          <!-- booking-->
                                          <td><?php echo $getCompanyvalue['crm_insurance_renew_date']; ?></td>
                                          <!-- insurance -->
                                          <td><?php echo $getCompanyvalue['crm_registered_renew_date']; ?></td>
                                          <!-- registed -->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax advice-->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax inve-->
                                       </tr>
                                       <?php   }  ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                        <div class="accordion-panel">
                           <div class=" accordion-heading" role="tab" id="headingFour">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse"
                                    data-parent="#accordion" href="#collapseFour"
                                    aria-expanded="false"
                                    aria-controls="collapseThree">
                                 This Year
                                 </a>
                              </h3>
                           </div>
                           <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                              <div class="accordion-content accordion-desc">
                                 <table class="table client_table1 text-center " id="thisyear_deadlineinfo">
                                    <thead>
                                       <th>Client Name</th>
                                       <th>Client Type</th>
                                       <th>Company Name</th>
                                       <th>VAT Due Date</th>
                                       <th>Payroll Due Date</th>
                                       <th>Accounts Due Date</th>
                                       <th>Confirmation statement Due Date</th>
                                       <th>Company Tax Return Due Date</th>
                                       <th>Personal Tax Return Due Date</th>
                                       <th>WorkPlace Pension - AE Due Date</th>
                                       <th>CIS - Contractor Due Date</th>
                                       <th>CIS - Sub Contractor Due Date</th>
                                       <th>P11D Due Date</th>
                                       <th>Bookkeeping Due Date</th>
                                       <th>Management Accounts Due Date</th>
                                       <th>Investigation Insurance Due Date</th>
                                       <th>Registered Address Due Date</th>
                                       <th>Tax Advice Due Date</th>
                                       <th>Tax Investigation Due Date</th>
                                    </thead>
                                    <tbody>
                                       <!-- <tr>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>2018-05-02</td>
                                          </tr> -->
                                       <?php 
                                          foreach ($getCompany_oneyear as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                                          ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                                          
                                          (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                                          ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                                          
                                          (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                                          ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                                          ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                                          ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                                          
                                          (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                          ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                                          
                                          (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                          ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                          ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                                          ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                          ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                                          ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                                          ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                                          ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                                          ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                                          ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                          ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                                          ?>
                                       <tr>
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_confirmation_statement_due_date']; ?></td>
                                          <!-- confi-->
                                          <td><?php echo $getCompanyvalue['crm_ch_accounts_next_due']; ?></td>
                                          <!-- account -->
                                          <td><?php echo $getCompanyvalue['crm_accounts_tax_date_hmrc']; ?></td>
                                          <!--company tax -->
                                          <td><?php echo $getCompanyvalue['crm_personal_due_date_return']; ?></td>
                                          <!-- crm personal tax -->
                                          <td><?php echo $getCompanyvalue['crm_vat_due_date']; ?></td>
                                          <!-- vat -->
                                          <td><?php echo $getCompanyvalue['crm_rti_deadline']; ?></td>
                                          <!-- payrol -->
                                          <td><?php echo $getCompanyvalue['crm_pension_subm_due_date']; ?></td>
                                          <!--workplace -->
                                          <td></td>
                                          <!-- CIS - Contractor -->
                                          <td></td>
                                          <!-- CIS - Sub Contractor-->
                                          <td><?php echo $getCompanyvalue['crm_next_p11d_return_due']; ?></td>
                                          <!-- p11d -->
                                          <td><?php echo $getCompanyvalue['crm_next_manage_acc_date'];  ?></td>
                                          <!-- manage account -->
                                          <td><?php echo $getCompanyvalue['crm_next_booking_date'] ?></td>
                                          <!-- booking-->
                                          <td><?php echo $getCompanyvalue['crm_insurance_renew_date']; ?></td>
                                          <!-- insurance -->
                                          <td><?php echo $getCompanyvalue['crm_registered_renew_date']; ?></td>
                                          <!-- registed -->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax advice-->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax inve-->
                                       </tr>
                                       <?php   }  ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     
      
