<div class="tab-content <?php if($_SESSION['permission']['Deadline_manager']['view']!='1'){ ?> permission_deined <?php } ?>" id="accordion_table">

    <div id="today" class="tab-pane fade in active">

      <!-- For filter section -->
      <div class="filter-data for-reportdash">
        <div class="filter-head">
          <h4>FILTERED DATA</h4>
          <button class="btn btn-danger f-right" id="clear_container">clear</button>
          <div id="container2" class=" box-container">
          </div>
        </div>                                   
      </div>
      <!-- For filter section -->

    <table class="table client_table1 text-center " id="today_deadlineinfo">
    <thead>
    <th class="client_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Client Name</th>
    <th class="client_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Client Type</th>
    <th class="company_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Company Name</th>
    <th class="deadline_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Deadline Type</th>
    <th class="date_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Date</th>
    </thead>

    <!--  <tfoot>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    </tfoot> -->
    <tbody>

    <?php 

        $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

        $current_date=date("Y-m-d");

        foreach ($getCompany_today as $getCompanykey => $getCompanyvalue) 
        {
            $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
         
             if(($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])))&& ($deadline_type=='Confirmation statement' || $deadline_type==''))
             { ?>
              
                   <tr class="for_table">
                   <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
                   <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                   <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
                   <td data-search="Confirmation statement Due Date">Confirmation statement Due Date</td>
                   <td data-search="<?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
                   </tr>
       <?php } 

          if(($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))) && ($deadline_type=='Accounts' || $deadline_type==''))
          { ?>

          <tr class="for_table">

          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
          <td data-search="Accounts Due Date">Accounts Due Date</td>

          <td data-search="<?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
          <!-- account -->
          </tr>

          <?php }    

          if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && ($deadline_type=='Company Tax Return' || $deadline_type==''))
          { ?>

          <tr class="for_table">

          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
          <td data-search="<?php echo $getusername['crm_name'];?>">Personal Tax Return Due Date</td>
          <td data-search="<?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
          <!-- crm personal tax -->
          </tr>
          <?php }

          if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && ($deadline_type=='Personal Tax Return' || $deadline_type==''))
          { ?>
          <tr class="for_table">

          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
          <td data-search="<?php echo $getusername['crm_name'];?>">Personal Tax Return Due Date</td>
          <td data-search="<?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
          <!-- crm personal tax -->
          </tr>
          <?php }

          if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && ($deadline_type=='VAT' || $deadline_type==''))
          { 

          ?>
          <tr class="for_table">

          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
          <td data-search="VAT Due Date">VAT Due Date</td>
          <td data-search="<?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
          <!-- vat -->
          </tr>
          <?php }

          if(($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))) && ($deadline_type=='Payroll' || $deadline_type==''))
          { ?>

          <tr class="for_table">
          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
          <td data-search="Payroll Due Date">Payroll Due Date</td>
          <td data-search="<?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
          <!-- payrol -->
          </tr>
          <?php }

          if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && ($deadline_type=='WorkPlace Pension - AE' || $deadline_type==''))
          { ?>

          <tr class="for_table">
          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
          <td data-search="WorkPlace Pension - AE Due Date">WorkPlace Pension - AE Due Date</td>
          <td data-search="<?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
          <!--workplace -->
          </tr>
          <?php }

          if($deadline_type=='CIS - Contractor' || $deadline_type==''){ ?> 

          <tr class="for_table">
          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
          <td data-search="CIS - Contractor Due Date">CIS - Contractor Due Date</td>
          <td></td>
          <!-- CIS - Contractor -->
          </tr>

          <?php } 

          if($deadline_type=='CIS - Sub Contractor' || $deadline_type==''){ ?>

          <tr class="for_table">
          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
          <td data-search="CIS - Sub Contractor Due Date">CIS - Sub Contractor Due Date</td>
          <td></td>
          <!-- CIS - Sub Contractor-->
          </tr>
          <?php }

          if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && ($deadline_type=='P11D' || $deadline_type==''))
            { ?>

          <tr class="for_table">
          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
          <td data-search="P11D Due Date">P11D Due Date</td>
          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
          <!-- p11d -->
          </tr>
          <?php } 

          if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && ($deadline_type=='Management Accounts' || $deadline_type==''))
          { ?>

         <tr class="for_table">
         <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
         <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
         <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
         <td data-search="Management Accounts Due Date">Management Accounts Due Date</td>
         <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
         <!-- manage account -->
         </tr>
        <?php }

          if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && ($deadline_type=='Bookkeeping' || $deadline_type==''))
          { ?>

          <tr class="for_table">
          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
          <td data-search="Bookkeeping Due Date">Bookkeeping Due Date</td>
          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
          <!-- booking-->
          </tr>
          <?php }

          if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && ($deadline_type=='Investigation Insurance' || $deadline_type==''))
          { ?>

         <tr class="for_table">
         <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
         <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
         <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
         <td data-search="Investigation Insurance Due Date">Investigation Insurance Due Date</td>
         <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
          <!-- insurance -->
        </tr>
        <?php }

          if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && ($deadline_type=='Registered Address' || $deadline_type==''))
            { ?>

         <tr class="for_table">
         <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
         <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
         <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
         <td data-search="Registered Address Due Date">Registered Address Due Date</td>
         <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
        <!-- registed -->
          </tr>
          <?php } 

          if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && ($deadline_type=='Tax Advice' || $deadline_type=='')){ ?>

           <tr class="for_table">
           <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
           <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
           <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
           <td data-search="Tax Advice Due Date">Tax Advice Due Date</td>
           <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
           <!-- tax advice-->
           </tr>
          <?php } 

          if($current_date==date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && ($deadline_type=='Tax Investigation' || $deadline_type=='')){ ?>

          <tr class="for_table">
          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
          <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
          <td data-search="Tax Investigation Due Date">Tax Investigation Due Date</td>
          <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
          <!-- tax inve-->
          </tr>
           <?php }
           
           foreach ($other_services as $key => $value) 
           {
              if($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && ($deadline_type == $value['service_name'] || $deadline_type=='')){ ?>

              <tr class="for_table">
              <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
              <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
              <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
              <td data-search="<?php echo $value['service_name']; ?> Due Date"><?php echo $value['service_name']; ?> Due Date</td>
              <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])): ''); ?></td>  
              </tr>
               <?php }
           }

        }  ?>  

    </tbody>
    </table>

    </div>
    <div id="this-week" class="tab-pane fade">
      
      <!-- For filter section -->
      <div class="filter-data for-reportdash">
        <div class="filter-head">
          <h4>FILTERED DATA</h4>
          <button class="btn btn-danger f-right" id="clear_container">clear</button>
          <div id="container2" class=" box-container">
          </div>
        </div>                                   
      </div>
      <!-- For filter section -->

    <table class="table client_table1 text-center " id="thisweek_deadlineinfo">
    <thead>

    <th class="client_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Client Name</th>
    <th class="client_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Client Type</th>
    <th class="company_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Company Name</th>
    <th class="deadline_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Deadline Type</th>
    <th class="date_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Date</th>

    </thead>

    <!--             <tfoot>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
    </tfoot> -->
    <tbody>

    <?php 
   
    $time=date('Y-m-d');
    $current_date=date('Y-m-d');     
    
    if($due_val=='over_due')
    {
       $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
    }
    else
    {
       $oneweek_end_date = date('Y-m-d', strtotime($time . ' -1 week'));
    }

    foreach ($getCompany_oneweek as $getCompanykey => $getCompanyvalue) 
    {
       $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
       
       switch ($due_val) 
       {
       case 'over_due':
       $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
       break;

       default:
       $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
       break;
       }

    if(($zz) && ($deadline_type=='Confirmation statement' || $deadline_type==''))
    { 
    ?> 

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Confirmation statement Due Date">Confirmation statement Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
    </tr>

    <?php } 
      switch ($due_val) 
      {
      case 'over_due':
      $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
      break;

      default:
      $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
      break;
      }

      if(($zz) && ($deadline_type=='Accounts' || $deadline_type==''))
      { ?>
   
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Accounts Due Date">Accounts Due Date</td>

    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
    <!-- account -->
    </tr>
    <?php } 
    switch ($due_val) 
    {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
    break;
    }

    if(($zz) && ($deadline_type=='Company Tax Return' || $deadline_type==''))
    { ?>
    
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Company Tax Return Due Date">Company Tax Return Due Date</td>
    <td data-search="php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
    <!--company tax -->
    </tr>
    <?php } 

    switch ($due_val) 
    {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
    break;
    }

    if(($zz) && ($deadline_type=='Personal Tax Return' || $deadline_type==''))
    { ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="<?php echo $getusername['crm_name'];?>">Personal Tax Return Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
    <!-- crm personal tax -->
    </tr>
    <?php }

    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
    break;
    } 

    if(($zz) && ($deadline_type=='VAT' || $deadline_type==''))
    { ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="VAT Due Date">VAT Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
    <!-- vat -->
    </tr>

    <?php } 

    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
    break;
    }

    if(($zz) && $deadline_type=='Payroll' || $deadline_type=='')
    {
   
    ?>
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Payroll Due Date">Payroll Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
    <!-- payrol -->
    </tr>
    <?php } 

      switch ($due_val) {
      case 'over_due':
      $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
      break;

      default:
      $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
      break;
      } 

      if(($zz) && ($deadline_type=='WorkPlace Pension - AE' || $deadline_type==''))
      {  ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="WorkPlace Pension - AE Due Date">WorkPlace Pension - AE Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
    <!--workplace -->
    </tr>
    <?php }  if($deadline_type=='CIS - Contractor' || $deadline_type==''){  ?>

    <tr class="for_table">

    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="CIS - Contractor Due Date">CIS - Contractor Due Date</td>
    <td></td>
    <!-- CIS - Contractor -->
    </tr>
    
    <?php } if($deadline_type=='CIS - Sub Contractor' || $deadline_type==''){ ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="CIS - Sub Contractor Due Date">CIS - Sub Contractor Due Date</td>
    <td></td>
    <!-- CIS - Sub Contractor-->
    </tr>

    <?php  }

    switch ($due_val) {
      case 'over_due':
      $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
      break;

      default:
      $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
      break;
      }

    if(($zz) && ($deadline_type=='P11D' || $deadline_type=='')){  ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="P11D Due Date">P11D Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
    <!-- p11d -->
    </tr>
    <?php } 

       switch ($due_val) {
      case 'over_due':
      $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
      break;

      default:
      $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
      break;
      }

    if(($zz) && ($deadline_type=='Management Accounts' || $deadline_type=='')){ ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Management Accounts Due Date">Management Accounts Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
    <!-- manage account -->
    </tr>
    <?php } 

      switch ($due_val) {
      case 'over_due':
      $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
      break;

      default:
      $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
      break;
      } 

    if(($zz) && ($deadline_type=='Bookkeeping' || $deadline_type=='')){  ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Bookkeeping Due Date">Bookkeeping Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
    <!-- booking-->
    </tr>
    <?php } 

    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
    break;
    }

    if(($zz) && ($deadline_type=='Investigation Insurance' || $deadline_type=='')){ 

    ?>
   
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Investigation Insurance Due Date">Investigation Insurance Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
    <!-- insurance -->
    </tr>
    <?php }
     
     switch ($due_val) {
     case 'over_due':
     $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
     break;

     default:
     $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
     break;
     } 
     if(($zz) && ($deadline_type=='Registered Address' || $deadline_type=='')){ 
     ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Registered Address Due Date">Registered Address Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
    <!-- registed -->
    </tr>
    <?php } 

    switch ($due_val) {
      case 'over_due':
      $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
      break;

      default:
      $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
      break;
      }

    if(($zz) && ($deadline_type=='Tax Advice' || $deadline_type=='')){ ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Tax Advice Due Date">Tax Advice Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
    <!-- tax advice-->
    </tr>
    <?php }

    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
    break;
    }

    if(($zz) && ($deadline_type=='Tax Investigation' || $deadline_type=='')){  

    ?>
   
    <tr class="for_table">

    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Tax Investigation Due Date">Tax Investigation Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
    <!-- tax inve-->
    </tr>
    <?php }

    foreach ($other_services as $key => $value) 
    {
       switch ($due_val) 
       {
         case 'over_due':
         $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && $oneweek_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])));
         break;

         default:
         $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && $oneweek_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])));
         break;
       }

       if(($zz) && ($deadline_type == $value['service_name'] || $deadline_type=='')){  ?>

       <tr class="for_table">
       <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
       <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
       <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
       <td data-search="<?php echo $value['service_name']; ?> Due Date"><?php echo $value['service_name']; ?> Due Date</td>
       <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])): ''); ?></td>  
       </tr>
        <?php }
    }


     ?>
    <?php   }  ?>
    </tbody>

    </table>
    </div>
    <div id="this-month" class="tab-pane fade">

      <!-- For filter section -->
      <div class="filter-data for-reportdash">
        <div class="filter-head">
          <h4>FILTERED DATA</h4>
          <button class="btn btn-danger f-right" id="clear_container">clear</button>
          <div id="container2" class=" box-container">
          </div>
        </div>                                   
      </div>
      <!-- For filter section -->

    <table class="table client_table1 text-center " id="thismonth_deadlineinfo">
    <thead>
    <th class="client_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Client Name</th>
    <th class="client_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Client Type</th>
    <th class="company_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Company Name</th>
    <th class="deadline_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Deadline Type</th>
    <th class="date_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Date</th>

    </thead>

    <!--   <tfoot>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    </tfoot> -->
    <tbody>

    <?php 

    $time=date('Y-m-d');
    
    if($due_val=='over_due')
    {
      $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
    }
    else
    {
      $onemonth_end_date = date('Y-m-d', strtotime($time . ' -1 month'));
    }

    $current_date=date('Y-m-d');

    foreach ($getCompany_onemonth as $getCompanykey => $getCompanyvalue) {
    $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);

    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
    break;
    }

    if(($zz) && ($deadline_type=='Confirmation statement' || $deadline_type=='')){

    ?>  

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Confirmation statement Due Date">Confirmation statement Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
    </tr>
    <?php }  

    switch ($due_val) {
      case 'over_due':
      $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
      break;

      default:
      $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
      break;
      }

     if(($zz) && ($deadline_type=='Accounts' || $deadline_type=='')){ 
     ?>
    
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Accounts Due Date">Accounts Due Date</td>

    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
    <!-- account -->
    </tr>
    <?php }
     
     switch ($due_val) {
     case 'over_due':
     $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
     break;

     default:
     $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
     break;
     }

     if(($zz) && ($deadline_type=='Company Tax Return' || $deadline_type=='')){
     ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Company Tax Return Due Date">Company Tax Return Due Date</td>
    <td data-search="php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
    <!--company tax -->
    </tr>
    <?php } 
    
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
    break;
    }

    if(($zz) && ($deadline_type=='Personal Tax Return' || $deadline_type=='')){ 

    ?>
    
    <tr class="for_table">

    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="<?php echo $getusername['crm_name'];?>">Personal Tax Return Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
    <!-- crm personal tax -->
    </tr>
    <?php } 

    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
    break;
    } 
    if(($zz) && ($deadline_type=='VAT' || $deadline_type=='')){     
    ?>
   
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="VAT Due Date">VAT Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
    <!-- vat -->
    </tr>
    <?php } 
    
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
    break;
    }

    if(($zz) && $deadline_type=='Payroll' || $deadline_type==''){

    ?>
 
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Payroll Due Date">Payroll Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
    <!-- payrol -->
    </tr>
    <?php } 

    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
    break;
    } 
    if(($zz) && ($deadline_type=='WorkPlace Pension - AE' || $deadline_type=='')){  
    ?>
   
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="WorkPlace Pension - AE Due Date">WorkPlace Pension - AE Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
    <!--workplace -->
    </tr>
    <?php } 
   
    if($deadline_type=='CIS - Contractor' || $deadline_type==''){ 
    ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="CIS - Contractor Due Date">CIS - Contractor Due Date</td>
    <td></td>
    <!-- CIS - Contractor -->
    </tr>
    
    <?php } if($deadline_type=='CIS - Sub Contractor' || $deadline_type==''){ ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="CIS - Sub Contractor Due Date">CIS - Sub Contractor Due Date</td>
    <td></td>
    <!-- CIS - Sub Contractor-->
    </tr>

    <?php } 
    
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
    break;
    }

    if(($zz) && ($deadline_type=='P11D' || $deadline_type=='')){
    ?>
    
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="P11D Due Date">P11D Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
    <!-- p11d -->
    </tr>
    <?php } 
    
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
    break;
    }

    if(($zz) && ($deadline_type=='Management Accounts' || $deadline_type=='')){
    ?>
   
    <tr class="for_table">

    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Management Accounts Due Date">Management Accounts Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
    <!-- manage account -->
    </tr>
    <?php } 
     
     switch ($due_val) {
     case 'over_due':
     $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
     break;

     default:
     $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
     break;
     }

    if(($zz) && ($deadline_type=='Bookkeeping' || $deadline_type=='')){ 
    ?>
    
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Bookkeeping Due Date">Bookkeeping Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
    <!-- booking-->
    </tr>
    <?php } 

    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
    break;
    }

    if(($zz) && ($deadline_type=='Investigation Insurance' || $deadline_type=='')){   ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Investigation Insurance Due Date">Investigation Insurance Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
    <!-- insurance -->
    </tr>
    <?php } 
 
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
    break;
    } 

    if(($zz) && ($deadline_type=='Registered Address' || $deadline_type=='')){

    ?>
   
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Registered Address Due Date">Registered Address Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
    <!-- registed -->
    </tr>
    <?php } 
 
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
    break;
    }

    if(($zz) && ($deadline_type=='Tax Advice' || $deadline_type=='')){ ?>
    
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Tax Advice Due Date">Tax Advice Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
    <!-- tax advice-->
    </tr>
    <?php } 
    
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
    break;
    }

    if(($zz) && ($deadline_type=='Tax Investigation' || $deadline_type=='')){     
    ?>
    
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Tax Investigation Due Date">Tax Investigation Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
    <!-- tax inve-->
    </tr>
    <?php } 

    foreach ($other_services as $key => $value) 
    {
       switch ($due_val) 
       {
         case 'over_due':
         $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && $onemonth_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])));
         break;

         default:
         $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && $onemonth_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])));
         break;
       }

       if(($zz) && ($deadline_type == $value['service_name'] || $deadline_type=='')){  ?>

       <tr class="for_table">
       <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
       <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
       <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
       <td data-search="<?php echo $value['service_name']; ?> Due Date"><?php echo $value['service_name']; ?> Due Date</td>
       <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])): ''); ?></td>  
       </tr>
        <?php }
    }

    ?>
    <?php   }  ?>
    </tbody>
    </table>
    </div>

    <div id="this-year" class="tab-pane fade">
      
      <!-- For filter section -->
      <div class="filter-data for-reportdash">
        <div class="filter-head">
          <h4>FILTERED DATA</h4>
          <button class="btn btn-danger f-right" id="clear_container">clear</button>
          <div id="container2" class=" box-container">
          </div>
        </div>                                   
      </div>
      <!-- For filter section -->

    <table class="table client_table1 text-center " id="thisyear_deadlineinfo" style="width: 100%">
    <thead>

    <th class="client_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Client Name</th>
    <th class="client_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Client Type</th>
    <th class="company_name_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Company Name</th>
    <th class="deadline_type_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Deadline Type</th>
    <th class="date_TH hasFilter"> <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />Date</th>
    </thead>

    <!--      <tfoot>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
    </tfoot> -->
    <tbody>

    <?php 

    $time=date('Y-m-d');
    
    if($due_val=='over_due')
    {
      $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
    }
    else
    {
      $oneyear_end_date = date('Y-m-d', strtotime($time . ' -1 year'));
    }

    $current_date=date('Y-m-d');

    foreach ($getCompany_oneyear as $getCompanykey => $getCompanyvalue) 
    {
      $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']); 

     switch ($due_val) { 
      case 'over_due':
      $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
      break;

      default:
      $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
      break; 
      } 

    if(($zz) && ($deadline_type=='Confirmation statement' || $deadline_type=='')){

    ?>  

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Confirmation statement Due Date">Confirmation statement Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
    </tr>
    <?php }  

    switch ($due_val) {
      case 'over_due':
      $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
      break;

      default:
      $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
      break;
      }

     if(($zz) && ($deadline_type=='Accounts' || $deadline_type=='')){ 
     ?>
    
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Accounts Due Date">Accounts Due Date</td>

    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
    <!-- account -->
    </tr>
    <?php }
     
     switch ($due_val) {
     case 'over_due':
     $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
     break;

     default:
     $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
     break;
     }

     if(($zz) && ($deadline_type=='Company Tax Return' || $deadline_type=='')){
     ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Company Tax Return Due Date">Company Tax Return Due Date</td>
    <td data-search="php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
    <!--company tax -->
    </tr>
    <?php } 
    
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
    break;
    }

    if(($zz) && ($deadline_type=='Personal Tax Return' || $deadline_type=='')){ 

    ?>
    
    <tr class="for_table">

    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="<?php echo $getusername['crm_name'];?>">Personal Tax Return Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
    <!-- crm personal tax -->
    </tr>
    <?php } 

    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
    break;
    } 
    if(($zz) && ($deadline_type=='VAT' || $deadline_type=='')){     
    ?>
   
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="VAT Due Date">VAT Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
    <!-- vat -->
    </tr>
    <?php } 
    
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
    break;
    }

    if(($zz) && $deadline_type=='Payroll' || $deadline_type==''){

    ?>
 
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Payroll Due Date">Payroll Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
    <!-- payrol -->
    </tr>
    <?php } 

    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
    break;
    } 
    if(($zz) && ($deadline_type=='WorkPlace Pension - AE' || $deadline_type=='')){  
    ?>
   
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="WorkPlace Pension - AE Due Date">WorkPlace Pension - AE Due Date</td>
    <td data-search="<?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?>"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
    <!--workplace -->
    </tr>
    <?php } 
   
    if($deadline_type=='CIS - Contractor' || $deadline_type==''){ 
    ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="CIS - Contractor Due Date">CIS - Contractor Due Date</td>
    <td></td>
    <!-- CIS - Contractor -->
    </tr>
    
    <?php } if($deadline_type=='CIS - Sub Contractor' || $deadline_type==''){ ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="CIS - Sub Contractor Due Date">CIS - Sub Contractor Due Date</td>
    <td></td>
    <!-- CIS - Sub Contractor-->
    </tr>

    <?php } 
    
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
    break;
    }

    if(($zz) && ($deadline_type=='P11D' || $deadline_type=='')){
    ?>
    
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="P11D Due Date">P11D Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
    <!-- p11d -->
    </tr>
    <?php } 
    
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
    break;
    }

    if(($zz) && ($deadline_type=='Management Accounts' || $deadline_type=='')){
    ?>
   
    <tr class="for_table">

    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Management Accounts Due Date">Management Accounts Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
    <!-- manage account -->
    </tr>
    <?php } 
     
     switch ($due_val) {
     case 'over_due':
     $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
     break;

     default:
     $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
     break;
     }

    if(($zz) && ($deadline_type=='Bookkeeping' || $deadline_type=='')){ 
    ?>
    
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Bookkeeping Due Date">Bookkeeping Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
    <!-- booking-->
    </tr>
    <?php } 

    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
    break;
    }

    if(($zz) && ($deadline_type=='Investigation Insurance' || $deadline_type=='')){   ?>

    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Investigation Insurance Due Date">Investigation Insurance Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
    <!-- insurance -->
    </tr>
    <?php } 
 
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
    break;
    } 

    if(($zz) && ($deadline_type=='Registered Address' || $deadline_type=='')){

    ?>
   
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Registered Address Due Date">Registered Address Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
    <!-- registed -->
    </tr>
    <?php } 
 
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
    break;
    }

    if(($zz) && ($deadline_type=='Tax Advice' || $deadline_type=='')){ ?>
    
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Tax Advice Due Date">Tax Advice Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
    <!-- tax advice-->
    </tr>
    <?php } 
    
    switch ($due_val) {
    case 'over_due':
    $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
    break;

    default:
    $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
    break;
    }

    if(($zz) && ($deadline_type=='Tax Investigation' || $deadline_type=='')){     
    ?>
    
    <tr class="for_table">
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
    <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
    <td data-search="Tax Investigation Due Date">Tax Investigation Due Date</td>
    <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
    <!-- tax inve-->
    </tr>
    <?php } 
    
    foreach($other_services as $key => $value) 
    {
       switch ($due_val) 
       {
         case 'over_due':
         $zz=($current_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && $oneyear_end_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])));
         break;

         default:
         $zz=($current_date>=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])) && $oneyear_end_date<=date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])));
         break;
       }

       if(($zz) && ($deadline_type == $value['service_name'] || $deadline_type=='')){  ?>

       <tr class="for_table">
       <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo $getusername['crm_name'];?></td>
       <td data-search="<?php echo $getCompanyvalue['crm_legal_form'];?>"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
       <td data-search="<?php echo $getCompanyvalue['crm_company_name'];?>"><?php echo $getCompanyvalue['crm_company_name'];?></td>
       <td data-search="<?php echo $value['service_name']; ?> Due Date"><?php echo $value['service_name']; ?> Due Date</td>
       <td data-search="<?php echo $getusername['crm_name'];?>"><?php echo (strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_'.$value['services_subnames'].'_statement_date'])): ''); ?></td>  
       </tr>
        <?php }
    }

    ?>
    <?php } ?>
    </tbody>

    </table>
    <input type="hidden" class="rows_selected" id="select_client_count" > 
    </div>
