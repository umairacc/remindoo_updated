<div class="client_section3 table-responsive floating_set filterrec">
<!--  <div class="service_succ"></div> -->
<table class="table client_table1 text-center dataTable no-footer" id="servicedead14" role="grid" aria-describedby="newinactives_info">
<thead>
<tr class="text-uppercase">
<th>Client Name</th>
<th>Client Type</th>
<?php foreach($service_list as $key => $value){ ?>
<th colspan="3"><?php echo $value['service_name']; ?></th>
<?php } ?>
</tr>
</thead>
<tfoot class="ex_data1">
<tr>
<th>
</th>
</tr>
</tfoot>
<tbody class="user-dashboard-section1">
  <tr class="for_table">
    <td></td><td></td>
    <?php foreach($service_list as $key => $value){ ?>
    <td colspan="1">Basic</td><td colspan="1"></td><td colspan="1">Due</td>
  <?php } ?>
  </tr>
<?php 
   
  foreach($service_charges as $key => $value) 
  {  ?>
  <tr class="for_table">
     <td><a href="javascript:;" data-toggle="modal" data-target="#desk-popup_<?php echo $value['id'];?>"><?php echo $value['crm_name']; ?></a></td>
     <td><?php echo $value['crm_legal_form']; ?></td>
     <?php foreach($service_list as $key1 => $value1){ ?>      
     <td colspan="1"><?php echo $value[$value1['service_name']]['basic']; ?></td>
     <?php if($value[$value1['service_name']]['enable'] == 'enable' && ($_SESSION['permission']['Deadline_manager']['edit'] == '1' || $_SESSION['permission']['Deadline_manager']['create'] == '1')){ ?>
     <td colspan="1"><a href="javascript:void(0);" data-toggle="modal" data-target="<?php echo "#edit_basic_".$value['id'].$value1['id']; ?>"  data-id="<?php echo $value1['id']; ?>"><i class="fa fa-edit"></i></a></td>
     <?php }else{ ?><td colspan="1"></td><?php } ?>
     <td colspan="1"><?php echo $value[$value1['service_name']]['charge']; ?></td>      
     <?php } ?>            
  </tr>
   
<?php } ?>
</tbody>
</table>
</div>