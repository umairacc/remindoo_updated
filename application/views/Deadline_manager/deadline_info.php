<?php 
   $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');
?>
<!-- management block -->
<div class="pcoded-content newalign-dead">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <div class="deadline-crm1 floating_set single-txt12">
               <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard">
                  <li class="nav-item">
                     Deadline Manager
                     <div class="slide"></div>
                  </li>
               </ul>
            </div>
            <div class="card deadline-block">
               <div class="payrolltax floating rev-op">
                  <div class="search-vat1">
                     <label>Search:</label>
                     <input type="text" name="deadline_search" id="deadline_search" placeholder="Search Company">
                  </div>
                  <div class="company-search1">
                     <!--   <div class="vat-shipping">
                        <label>Company Name</label> 
                        <select name="companysearch" id="companysearch" class="search">
                           <option value="">All</option>
                           <option value="2">zts</option>
                           <option value="">abc company</option>
                        </select>
                        </div> -->
                     <div class="vat-shipping">
                        <label>Company Type</label>
                        <select class=" fields search" name="company_type" id="company_type" onchange="test()">
                           <option value="" data-id="1">All</option>
                           <option value="Private Limited company" data-id="2">Private Limited company</option>
                           <option value="Public Limited company" data-id="3">Public Limited company</option>
                           <option value="Limited Liability Partnership" data-id="4">Limited Liability Partnership</option>
                           <option value="Partnership" data-id="5">Partnership</option>
                           <option value="Self Assessment" data-id="6">Self Assessment</option>
                           <option value="Trust" data-id="7">Trust</option>
                           <option value="Charity" data-id="8">Charity</option>
                           <option value="Other" data-id="9">Other</option>
                        </select>
                     </div>
                     <div class="vat-shipping">
                        <label>Deadline Type</label>
                        <select class="fields search12" name="deadline_type" id="deadline_type">
                           <option value="">ALL</option>
                           <option value="VAT">VAT</option>
                           <option value="Payroll">Payroll</option>
                           <option value="Accounts">Accounts</option>
                           <option value="Confirmation statement">Confirmation statement</option>
                           <option value="Company Tax Return">Company Tax Return</option>
                           <option value="Personal Tax Return">Personal Tax Return</option>
                           <option value="WorkPlace Pension - AE">WorkPlace Pension - AE</option>
                           <option value="CIS - Contractor">CIS - Contractor</option>
                           <option value="CIS - Sub Contractor">CIS - Sub Contractor</option>
                           <option value="P11D">P11D</option>
                           <option value="Bookkeeping">Bookkeeping</option>
                           <option value="Management Accounts">Management Accounts</option>
                           <option value="Investigation Insurance">Investigation Insurance</option>
                           <option value="Registered Address">Registered Address</option>
                           <option value="Tax Advice">Tax Advice</option>
                           <option value="Tax Investigation">Tax Investigation</option>
                        </select>
                     </div>
                     <div class="vat-shipping">
                      <div class="radio radio-inline">
                        <label> <input type="radio" name="for_due" id="for_due" value="due"> <i class="helper"></i>Due</label>
                      </div>
                      <div class="radio radio-inline">
                        <label> <input type="radio" name="for_due" id="for_due" value="over_due" checked="checked"> <i class="helper"></i>Over Due</label>
                      </div>
                        
                  </div>
               </div>
               <!-- collapse panel -->
               <div class="day-list  accor-view09 floating_set">
                  <!-- <div class="f-days">
                     <div class="panel-head" ddata-parent="#accordion" data-toggle="collapse" data-target="#demo">
                        Today
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                     </div>
                     <div class="panel-body panel-collapse collapse" id="demo">
                        No Deadlines to view
                     </div>
                     </div>
                     <div class="f-week">
                     <div class="panel-head" data-parent="#accordion" data-toggle="collapse" data-target="#demo1">
                        This Week
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                     </div>
                     <div class="panel-body panel-collapse collapse" id="demo1">
                        No Deadlines to view
                     </div>
                     </div>
                     <div class="f-month">
                     <div class="panel-head" data-parent="#accordion" data-toggle="collapse" data-target="#demo2">
                        This Month
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                     </div>
                     <div class="panel-body panel-collapse collapse" id="demo2">
                        No Deadlines to view
                     </div>
                     </div>
                     <div class="n-month">
                     <div class="panel-head" data-parent="#accordion" data-toggle="collapse" data-target="#demo3">
                        Next Month
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                     </div>
                     <div class="panel-body panel-collapse collapse" id="demo3">
                        No Deadlines to view
                     </div>
                     </div> -->
                  <div class="card-block accordion-block color-accordion-block">
                     <div id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="accordion-panel">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse"
                                    data-parent="#accordion" href="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                                 Today
                                 </a>
                              </h3>
                           </div>
                           <div id="collapseOne" class="panel-collapse in collapse show" role="tabpanel" aria-labelledby="headingOne">
                              <div class="accordion-content accordion-desc">
                                 <table class="table client_table1 text-center " id="today_deadlineinfo">
                                    <thead>
                                       <th>Client Name</th>
                                       <th>Client Type</th>
                                       <th>Company Name</th>
                                       <th>VAT Due Date</th>
                                       <th>Payroll Due Date</th>
                                       <th>Accounts Due Date</th>
                                       <th>Confirmation statement Due Date</th>
                                       <th>Company Tax Return Due Date</th>
                                       <th>Personal Tax Return Due Date</th>
                                       <th>WorkPlace Pension - AE Due Date</th>
                                       <th>CIS - Contractor Due Date</th>
                                       <th>CIS - Sub Contractor Due Date</th>
                                       <th>P11D Due Date</th>
                                       <th>Bookkeeping Due Date</th>
                                       <th>Management Accounts Due Date</th>
                                       <th>Investigation Insurance Due Date</th>
                                       <th>Registered Address Due Date</th>
                                       <th>Tax Advice Due Date</th>
                                       <th>Tax Investigation Due Date</th>
                                    </thead>
                                    <tbody>
                                       <!-- <tr>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>2018-05-02</td>
                                          </tr> -->
                                       <?php 
                                          foreach ($getCompany_today as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                                          ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                                          
                                          (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                                          ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                                          
                                          (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                                          ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                                          ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                                          ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                                          
                                          (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                          ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                                          
                                          (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                          ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                          ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                                          ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                          ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                                          ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                                          ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                                          ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                                          ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                                          ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                          ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                                          ?>
                                       <tr>
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_confirmation_statement_due_date']; ?></td>
                                          <!-- confi-->
                                          <td><?php echo $getCompanyvalue['crm_ch_accounts_next_due']; ?></td>
                                          <!-- account -->
                                          <td><?php echo $getCompanyvalue['crm_accounts_tax_date_hmrc']; ?></td>
                                          <!--company tax -->
                                          <td><?php echo $getCompanyvalue['crm_personal_due_date_return']; ?></td>
                                          <!-- crm personal tax -->
                                          <td><?php echo $getCompanyvalue['crm_vat_due_date']; ?></td>
                                          <!-- vat -->
                                          <td><?php echo $getCompanyvalue['crm_rti_deadline']; ?></td>
                                          <!-- payrol -->
                                          <td><?php echo $getCompanyvalue['crm_pension_subm_due_date']; ?></td>
                                          <!--workplace -->
                                          <td></td>
                                          <!-- CIS - Contractor -->
                                          <td></td>
                                          <!-- CIS - Sub Contractor-->
                                          <td><?php echo $getCompanyvalue['crm_next_p11d_return_due']; ?></td>
                                          <!-- p11d -->
                                          <td><?php echo $getCompanyvalue['crm_next_manage_acc_date'];  ?></td>
                                          <!-- manage account -->
                                          <td><?php echo $getCompanyvalue['crm_next_booking_date'] ?></td>
                                          <!-- booking-->
                                          <td><?php echo $getCompanyvalue['crm_insurance_renew_date']; ?></td>
                                          <!-- insurance -->
                                          <td><?php echo $getCompanyvalue['crm_registered_renew_date']; ?></td>
                                          <!-- registed -->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax advice-->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax inve-->
                                       </tr>
                                       <?php   }  ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                        <div class="accordion-panel">
                           <div class="accordion-heading" role="tab" id="headingTwo">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse"
                                    data-parent="#accordion" href="#collapseTwo"
                                    aria-expanded="false"
                                    aria-controls="collapseTwo">
                                 This week
                                 </a>
                              </h3>
                           </div>
                           <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                              <div class="accordion-content accordion-desc">
                                 <table class="table client_table1 text-center " id="thisweek_deadlineinfo">
                                    <thead>
                                       <th>Client Name</th>
                                       <th>Client Type</th>
                                       <th>Company Name</th>
                                       <th>VAT Due Date</th>
                                       <th>Payroll Due Date</th>
                                       <th>Accounts Due Date</th>
                                       <th>Confirmation statement Due Date</th>
                                       <th>Company Tax Return Due Date</th>
                                       <th>Personal Tax Return Due Date</th>
                                       <th>WorkPlace Pension - AE Due Date</th>
                                       <th>CIS - Contractor Due Date</th>
                                       <th>CIS - Sub Contractor Due Date</th>
                                       <th>P11D Due Date</th>
                                       <th>Bookkeeping Due Date</th>
                                       <th>Management Accounts Due Date</th>
                                       <th>Investigation Insurance Due Date</th>
                                       <th>Registered Address Due Date</th>
                                       <th>Tax Advice Due Date</th>
                                       <th>Tax Investigation Due Date</th>
                                    </thead>
                                    <tbody>
                                       <!-- <tr>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>2018-05-02</td>
                                          </tr> -->
                                       <?php 
                                          foreach ($getCompany_oneweek as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                                          ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                                          
                                          (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                                          ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                                          
                                          (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                                          ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                                          ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                                          ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                                          
                                          (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                          ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                                          
                                          (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                          ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                          ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                                          ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                          ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                                          ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                                          ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                                          ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                                          ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                                          ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                          ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                                          ?>
                                       <tr>
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_confirmation_statement_due_date']; ?></td>
                                          <!-- confi-->
                                          <td><?php echo $getCompanyvalue['crm_ch_accounts_next_due']; ?></td>
                                          <!-- account -->
                                          <td><?php echo $getCompanyvalue['crm_accounts_tax_date_hmrc']; ?></td>
                                          <!--company tax -->
                                          <td><?php echo $getCompanyvalue['crm_personal_due_date_return']; ?></td>
                                          <!-- crm personal tax -->
                                          <td><?php echo $getCompanyvalue['crm_vat_due_date']; ?></td>
                                          <!-- vat -->
                                          <td><?php echo $getCompanyvalue['crm_rti_deadline']; ?></td>
                                          <!-- payrol -->
                                          <td><?php echo $getCompanyvalue['crm_pension_subm_due_date']; ?></td>
                                          <!--workplace -->
                                          <td></td>
                                          <!-- CIS - Contractor -->
                                          <td></td>
                                          <!-- CIS - Sub Contractor-->
                                          <td><?php echo $getCompanyvalue['crm_next_p11d_return_due']; ?></td>
                                          <!-- p11d -->
                                          <td><?php echo $getCompanyvalue['crm_next_manage_acc_date'];  ?></td>
                                          <!-- manage account -->
                                          <td><?php echo $getCompanyvalue['crm_next_booking_date'] ?></td>
                                          <!-- booking-->
                                          <td><?php echo $getCompanyvalue['crm_insurance_renew_date']; ?></td>
                                          <!-- insurance -->
                                          <td><?php echo $getCompanyvalue['crm_registered_renew_date']; ?></td>
                                          <!-- registed -->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax advice-->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax inve-->
                                       </tr>
                                       <?php   }  ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                        <div class="accordion-panel">
                           <div class=" accordion-heading" role="tab" id="headingThree">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse"
                                    data-parent="#accordion" href="#collapseThree"
                                    aria-expanded="false"
                                    aria-controls="collapseThree">
                                 This Month
                                 </a>
                              </h3>
                           </div>
                           <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                              <div class="accordion-content accordion-desc">
                                 <table class="table client_table1 text-center " id="thismonth_deadlineinfo">
                                    <thead>
                                       <th>Client Name</th>
                                       <th>Client Type</th>
                                       <th>Company Name</th>
                                       <th>VAT Due Date</th>
                                       <th>Payroll Due Date</th>
                                       <th>Accounts Due Date</th>
                                       <th>Confirmation statement Due Date</th>
                                       <th>Company Tax Return Due Date</th>
                                       <th>Personal Tax Return Due Date</th>
                                       <th>WorkPlace Pension - AE Due Date</th>
                                       <th>CIS - Contractor Due Date</th>
                                       <th>CIS - Sub Contractor Due Date</th>
                                       <th>P11D Due Date</th>
                                       <th>Bookkeeping Due Date</th>
                                       <th>Management Accounts Due Date</th>
                                       <th>Investigation Insurance Due Date</th>
                                       <th>Registered Address Due Date</th>
                                       <th>Tax Advice Due Date</th>
                                       <th>Tax Investigation Due Date</th>
                                    </thead>
                                    <tbody>
                                       <!-- <tr>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>2018-05-02</td>
                                          </tr> -->
                                       <?php 
                                          foreach ($getCompany_onemonth as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                                          ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                                          
                                          (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                                          ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                                          
                                          (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                                          ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                                          ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                                          ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                                          
                                          (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                          ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                                          
                                          (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                          ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                          ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                                          ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                          ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                                          ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                                          ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                                          ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                                          ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                                          ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                          ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                                          ?>
                                       <tr>
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_confirmation_statement_due_date']; ?></td>
                                          <!-- confi-->
                                          <td><?php echo $getCompanyvalue['crm_ch_accounts_next_due']; ?></td>
                                          <!-- account -->
                                          <td><?php echo $getCompanyvalue['crm_accounts_tax_date_hmrc']; ?></td>
                                          <!--company tax -->
                                          <td><?php echo $getCompanyvalue['crm_personal_due_date_return']; ?></td>
                                          <!-- crm personal tax -->
                                          <td><?php echo $getCompanyvalue['crm_vat_due_date']; ?></td>
                                          <!-- vat -->
                                          <td><?php echo $getCompanyvalue['crm_rti_deadline']; ?></td>
                                          <!-- payrol -->
                                          <td><?php echo $getCompanyvalue['crm_pension_subm_due_date']; ?></td>
                                          <!--workplace -->
                                          <td></td>
                                          <!-- CIS - Contractor -->
                                          <td></td>
                                          <!-- CIS - Sub Contractor-->
                                          <td><?php echo $getCompanyvalue['crm_next_p11d_return_due']; ?></td>
                                          <!-- p11d -->
                                          <td><?php echo $getCompanyvalue['crm_next_manage_acc_date'];  ?></td>
                                          <!-- manage account -->
                                          <td><?php echo $getCompanyvalue['crm_next_booking_date'] ?></td>
                                          <!-- booking-->
                                          <td><?php echo $getCompanyvalue['crm_insurance_renew_date']; ?></td>
                                          <!-- insurance -->
                                          <td><?php echo $getCompanyvalue['crm_registered_renew_date']; ?></td>
                                          <!-- registed -->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax advice-->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax inve-->
                                       </tr>
                                       <?php   }  ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                        <div class="accordion-panel">
                           <div class=" accordion-heading" role="tab" id="headingFour">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse"
                                    data-parent="#accordion" href="#collapseFour"
                                    aria-expanded="false"
                                    aria-controls="collapseThree">
                                 This Year
                                 </a>
                              </h3>
                           </div>
                           <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                              <div class="accordion-content accordion-desc">
                                 <table class="table client_table1 text-center " id="thisyear_deadlineinfo">
                                    <thead>
                                       <th>Client Name</th>
                                       <th>Client Type</th>
                                       <th>Company Name</th>
                                       <th>VAT Due Date</th>
                                       <th>Payroll Due Date</th>
                                       <th>Accounts Due Date</th>
                                       <th>Confirmation statement Due Date</th>
                                       <th>Company Tax Return Due Date</th>
                                       <th>Personal Tax Return Due Date</th>
                                       <th>WorkPlace Pension - AE Due Date</th>
                                       <th>CIS - Contractor Due Date</th>
                                       <th>CIS - Sub Contractor Due Date</th>
                                       <th>P11D Due Date</th>
                                       <th>Bookkeeping Due Date</th>
                                       <th>Management Accounts Due Date</th>
                                       <th>Investigation Insurance Due Date</th>
                                       <th>Registered Address Due Date</th>
                                       <th>Tax Advice Due Date</th>
                                       <th>Tax Investigation Due Date</th>
                                    </thead>
                                    <tbody>
                                       <!-- <tr>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>demo</td>
                                          <td>2018-05-02</td>
                                          </tr> -->
                                       <?php 
                                          foreach ($getCompany_oneyear as $getCompanykey => $getCompanyvalue) {
                                              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                                                (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
                                          ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                                          
                                          (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
                                          ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                                          
                                          (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
                                          ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
                                          ($jsnp_tax!='') ? $p_tax = "checked='checked'" : $p_tax = "";
                                          
                                          (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
                                          ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                                          
                                          (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                          ($jsnworkplace!='') ? $workplace = "checked='checked'" : $workplace = "";
                                          
                                          (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                          ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                          ($jsncis!='') ? $cis = "checked='checked'" : $cis = "";
                                          
                                          (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
                                          ($jsncis_sub!='') ? $cissub  = "checked='checked'" : $cissub     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                          ($jsnp11d!='') ? $p11d   = "checked='checked'" : $p11d   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
                                          ($jsnbk!='') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";
                                          
                                          (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
                                          ($jsnmgnt!='') ? $management     = "checked='checked'" : $management     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
                                          ($jsninvest!='') ? $investgate   = "checked='checked'" : $investgate     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
                                          ($jsnreg!='') ? $registered  = "checked='checked'" : $registered     = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
                                          ($jsntaxad!='') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";
                                          
                                          (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                          ($jsntaxinvest!='') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";
                                          ?>
                                       <tr>
                                          <td><?php echo $getusername['crm_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_company_name'];?></td>
                                          <td><?php echo $getCompanyvalue['crm_confirmation_statement_due_date']; ?></td>
                                          <!-- confi-->
                                          <td><?php echo $getCompanyvalue['crm_ch_accounts_next_due']; ?></td>
                                          <!-- account -->
                                          <td><?php echo $getCompanyvalue['crm_accounts_tax_date_hmrc']; ?></td>
                                          <!--company tax -->
                                          <td><?php echo $getCompanyvalue['crm_personal_due_date_return']; ?></td>
                                          <!-- crm personal tax -->
                                          <td><?php echo $getCompanyvalue['crm_vat_due_date']; ?></td>
                                          <!-- vat -->
                                          <td><?php echo $getCompanyvalue['crm_rti_deadline']; ?></td>
                                          <!-- payrol -->
                                          <td><?php echo $getCompanyvalue['crm_pension_subm_due_date']; ?></td>
                                          <!--workplace -->
                                          <td></td>
                                          <!-- CIS - Contractor -->
                                          <td></td>
                                          <!-- CIS - Sub Contractor-->
                                          <td><?php echo $getCompanyvalue['crm_next_p11d_return_due']; ?></td>
                                          <!-- p11d -->
                                          <td><?php echo $getCompanyvalue['crm_next_manage_acc_date'];  ?></td>
                                          <!-- manage account -->
                                          <td><?php echo $getCompanyvalue['crm_next_booking_date'] ?></td>
                                          <!-- booking-->
                                          <td><?php echo $getCompanyvalue['crm_insurance_renew_date']; ?></td>
                                          <!-- insurance -->
                                          <td><?php echo $getCompanyvalue['crm_registered_renew_date']; ?></td>
                                          <!-- registed -->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax advice-->
                                          <td><?php echo $getCompanyvalue['crm_investigation_end_date']; ?></td>
                                          <!-- tax inve-->
                                       </tr>
                                       <?php   }  ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- collapse panel -->
            </div>
         </div>
      </div>
   </div>
</div>
<!-- management block -->
<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
       $('.edit-toggle1').click(function(){
         $('.proposal-down').slideToggle(300);
       });
   
        $("#legal_form12").change(function() {
          var val = $(this).val();
          if(val === "from2") {
            // alert('hi');
              $(".from2option").show();
          }
          else {
              $(".from2option").hide();
          }
          });
   
        $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({ 
          // "scrollX": true
         });
   })
</script>
<script type="text/javascript">
function test(){
     alert('zz');
}
    $('#deadline_search').on('keyup',function(){
     // alert('zz');
     var due_val = $("input[name='for_due']:checked").val();
       var company=$(this).val();
       var company_type=$('#company_type').val();
       var deadline_type=$("#deadline_type").val();

     var data={};
                 data['company']= company;
                  data['company_type']= company_type;
                   data['deadline_type']= deadline_type;
                   data['due_val']=due_val;
                 $.ajax({
                     url: '<?php echo base_url();?>Deadline_manager/company_search_deadline_info/',
                     type : 'POST',
                     data : data,
                     success: function(data) {
                      // alert(data);
                       console.log(data);
                       $("#accordion").html('');
                       $("#accordion").html(data);
                        $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({ 
                            // "scrollX": true
                           });
                     },
                 });

    });
  // $('#company_type,#deadline_type,#for_due').on('change',function(){

         $(document).ready(function(){
    $('#company_types').on('change',function(){
      alert('zz');
      var due_val = $("input[name='for_due']:checked").val();
       var company=$('#deadline_search').val();
       var company_type=$('#company_type').val();
       var deadline_type=$("#deadline_type").val();
     var data={};
                 data['company']= company;
                  data['company_type']= company_type;
                   data['deadline_type']= deadline_type;
                    data['due_val']=due_val;
                 $.ajax({
                     url: '<?php echo base_url();?>Deadline_manager/company_search_deadline_info/',
                     type : 'POST',
                     data : data,
                     success: function(data) {
                      // alert(data);
                       console.log(data);
                       $("#accordion").html('');
                       $("#accordion").html(data);
                        $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({ 
                            // "scrollX": true
                           });
                     },
                 });

    });
    })
</script>