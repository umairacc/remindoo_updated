<?php $this->load->view('includes/header');?>
<style>
/*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   li.ui-state-default.ui-sortable-handle.ui-sortable-helper
   {
      /*top: 0px !important;*/
   }
   .card.firm-field span.pro-head {
    font-weight: 600;
    font-size: 15px;
    text-transform: uppercase;
    }
   .card.firm-field {
    padding: 30px;
    background: #fff;
    box-shadow: 0 1px 11px 0 rgba(0, 0, 0, 0.12);
    }  
   body
   {
      overflow-x:hidden; 
   }
   .card.firm-field {
       padding: 30px;
   }
   .invoice-details {
       margin-top: 30px;
   }
   span.invoice-topinner {
    float: left;
    width: 15%;
   }
   span.invoice-topinner label {
    display: block;
    text-transform: capitalize;
    font-size: 14px;
   }
   span.invoice-topinner input {
    padding-left: 10px;
    height: 30px;
   }
   span.invoice-topinner {
    float: left;
    width: 17%;
   }
   i.fa.fa-eye {
    color: #303549;
    font-size: 14px;
   }
   span.preview-pdf {
    float: right;
    display: inline-block;
    height: 60px;
    vertical-align: middle;
    line-height: 60px;
    color: #303549;
    font-size: 14px;
   }
   .invoice-top {
    float: left;
    width: 100%;
   }
   .amounts-detail {
    float: right;
    width: auto;
    padding-top: 30px;
   }
   .amounts-detail select {
    background: transparent;
    border: 1px solid #dbdbdb;
    height: 30px;
    padding: 0 10px;
    text-transform: capitalize;
   }
   .amounts-detail label {
    text-transform: capitalize;
    font-size: 14px;
    padding-right: 5px;
   }
   .invoice-table table {
    width: 100%;
    margin-top: 30px;
   }
   .invoice-table table th {
    text-transform: capitalize;
    font-size: 14px;
    padding: 5px 10px;

   }
   .invoice-table table tr:first-child {
    border: 1px solid #ccc;
   }
   .invoice-table table td {
    border: 1px solid #ccc;
    padding: 10px;
    height: 30px;
   }
   .invoice-table table td:last-child {
    text-align: center;
   }
   td i.fa.fa-times:hover {
    color: red;
    transition: color 0.5s ease;
    cursor: pointer;
   }
   .add-newline input {
    background: #0956ff !important;
    color: #fff;
    border: none;
    text-transform: capitalize;
    font-size: 14px;
    margin-top: 20px;
    padding: 5px 10px;
   }
   .add-newline {
    display: inline-block;
   }
   .sub-tot_table {
    float: right;
    display: inline-block;
    width:35%;
   }
   .sub-tot_table th, .sub-tot_table td, .sub-tot_table tr {
    border: none !important;
   }
   .sub-tot_table td {
    font-size: 14px;
   }
   tr.grand-total {
    border-top: 2px solid #ccc !important;
    border-bottom: 2px solid #ccc !important;
   }
   .table-savebtn input {
    background: #0956ff;
    color: #fff;
    border: none;
    font-size: 15px;
    padding: 3px 10px;
    text-transform: capitalize;
   }
   .approve-btn input.approve {
    background: #0956ff;
    border: none;
    color: #fff;
    text-transform: capitalize;
    font-size: 15px;
    padding: 2px 10px 4px;
    margin-right: 10px;
   }
   .approve-btn a.cancel-btn {
    background: gray;
    color: #fff;
    padding: 5px 10px;
    font-size: 15px;
    text-transform: capitalize;
   }
   .table-savebtn {
    display: inline-block;
    padding-top: 20px;
   }
   .approve-btn {
    display: inline-block;
    float: right;
    padding-top: 20px;
   }
   .invoice-table
   {
      padding-left: 0;
   }
   .save-approve {
    float: left;
    width: 100%;
   }
   @media(max-width: 1024px) {

      span.invoice-topinner {
       float: left;
       width: auto;
       padding-right: 15px;
       padding-top: 10px;
      }

   }
   @media(max-width: 991px) {

      span.preview-pdf
      {
         float: right;
         width: 100%;
         height: auto;
         line-height: normal;
         text-align: right;
         padding: 15px 0 0 15px;
      }
      .sub-tot_table
      {
         width:auto;
         padding-top: 10px;

      }
   }
   @media(min-width: 640px) and (max-width: 767px) {

      span.invoice-topinner input
      {
         width: 100%;
      }
      span.invoice-topinner
      {
         width:50%;
      }
      .sub-tot_table
      {
         width: auto;
      }

   }
   @media(max-width: 639px) {

      span.invoice-topinner, span.invoice-topinner input
      {
         width:100%;
      }
      .sub-tot_table
      {
         width: 100%;
      }

   }
   @media(max-width: 424px) {
      .approve-btn
      {
         width:100%;
         text-align: right;
      }
      .sub-tot_table
      {
         overflow-x:auto;
      }
   }

</style>

<style>
.dropbtn {
    background-color: #3498DB;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
    height: 22px;
padding-top: 0px;
}

.dropbtn:hover, .dropbtn:focus {
    background-color: #2980B9;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown a:hover {background-color: #ddd}

.show {display:block;}
</style>
 


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>




<!-- management block -->
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <div class="card firm-field">
               <span class="pro-head">invoice details</span>
               <!-- <div class="j-info"><i class="fa fa-circle" aria-hidden="true"></i> please not that only selected ( <i class="fa fa-check" aria-hidden="true"></i> ) fields are available in important template.</div> -->
  <form action="<?php echo base_url(); ?>Minvoice/insert" name="form1" id="form1" method="post">  
               <div class="invoice-details">
                     
                     <div class="amounts-detail">
                        <a href="<?php echo base_url(); ?>Minvoice/invoice"><input type="button" class="btn btn-info" value="Add Invoice"></a>
                     </div>
                     <div class="invoice-table">
                        <div class="table-responsive">
                          
                                    <table id="example1"  style="max-width:100%">
                                        <thead >
                                            <tr>
                                                <th>ID</th>
                                                <th>Date</th>
                                                <th>Invoice Number</th>
                                                <th>Reference Number</th>
                                                <th>Customer Name</th>
                                                <th>Due Date</th>
                                                <th>Tax Type</th>                                              
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php
                                                $no=1;
                                                
                                                foreach($records as $row):
                                            ?>
                                                <tr>
                                                    <td><?php echo $no ?></td>
                                                    <td><?php echo $row->invoicedate ?></td>
                                                    <td><?php echo $row->invoiceNo ?></td>
                                                    <td><?php echo $row->refer ?></td>
                                                    <td><?php echo $row->clientname ?></td>
                                                    <td><?php echo $row->duedate ?></td>
                                                    <td><?php echo $row->amountstype ?></td>
                                                    <td>
                                                        <!-- <div class="btn-group">
                                                            <button type="button" class="btn btn-primary">Action</button>
                                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                                <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu" style="min-width:100px; margin-left:-5px">
                                                                <li><a href="<?php echo base_url() ?>index.php/Invoice/info/<?php echo $row->invoiceNo ?>">View Details</a></li>
                                                                <li><a href="<?php echo base_url() ?>index.php/Invoice/edit/<?php echo $row->invoiceNo ?>">Edit</a></li>
                                                                <li><a href="<?php echo base_url() ?>index.php/Invoice/delete/<?php echo $row->invoiceNo ?>">Delete</a></li>
                                                            </ul>
                                                        </div> -->
<!-- <div class="dropdown">
  <button type="button" onclick="myFunction()" class="dropbtn">Action</button>
      <div id="myDropdown" class="dropdown-content">
        <a href="<?php echo base_url() ?>Minvoice/edit/<?php echo $row->invoiceNo ?>">Edit</a>
        <a href="<?php echo base_url() ?>Minvoice/delete/<?php echo $row->invoiceNo ?>">Delete</a>
        <a href="#contact">Details</a>
      </div>
</div> -->

<p class="action_01">
    <a href="<?php echo base_url() ?>Minvoice/delete/<?php echo $row->invoiceNo ?>"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
    <a href="<?php echo base_url() ?>Minvoice/edit/<?php echo $row->invoiceNo ?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
    <a href="<?php echo base_url() ?>Minvoice/info/<?php echo $row->invoiceNo ?>"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a>
</p>
                                                    </td>
                                                    
                                                    
                                                    
                                                </tr>
                                            <?php
                                                $no++;
                                                endforeach;                                               
                                            ?> 
                                        </tbody>
                                        
                                    </table>
                                <!-- /.box-body -->
                            
                          
                        </div>
                        <div class="field_wrapper">
                              <div>
                                  <!-- <input type="text" name="field_name[]" value=""/> -->
                                  <!-- <a href="javascript:void(0);" class="add_button" title="Add field"><img src="add-icon.png"/></a> -->
                                  
                              </div>
                          </div>
                        
                        

                           
                     </div><!--invoice table -->
                     <div class="save-approve">
                        <div class="table-savebtn">
                           <!-- <input type="submit" value="save" id="save"> -->
                        </div>
                        
                     </div>
               </div>
 </form>              
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ajax loader -->
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style>
<!-- ajax loader end-->
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
   <h1>Warning!!</h1>
   <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
   <div class="iew-container">
      <ul class="iew-download">
         <li>
            <a href="http://www.google.com/chrome/">
               <img src="assets/images/browser/chrome.png" alt="Chrome">
               <div>Chrome</div>
            </a>
         </li>
         <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
               <img src="assets/images/browser/firefox.png" alt="Firefox">
               <div>Firefox</div>
            </a>
         </li>
         <li>
            <a href="http://www.opera.com">
               <img src="assets/images/browser/opera.png" alt="Opera">
               <div>Opera</div>
            </a>
         </li>
         <li>
            <a href="https://www.apple.com/safari/">
               <img src="assets/images/browser/safari.png" alt="Safari">
               <div>Safari</div>
            </a>
         </li>
         <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
               <img src="assets/images/browser/ie.png" alt="">
               <div>IE (9 & above)</div>
            </a>
         </li>
      </ul>
   </div>
   <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- j-pro js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Bootstrap date-time-picker js -->
<!--     <script type="text/javascript" src="assets/pages/advance-elements/moment-with-locales.min.js"></script>
   <script type="text/javascript" src="bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
   <script type="text/javascript" src="assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
   <script type="text/javascript" src="bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
   <script type="text/javascript" src="bower_components/datedropper/js/datedropper.min.js"></script> -->
<!-- Custom js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>

<!-- invoice script -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->

<script src="<?php echo base_url() ?>js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<script src="<?php echo base_url() ?>js/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
 <script src="<?php echo base_url() ?>js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
 <script src="<?php echo base_url() ?>js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>js/AdminLTE/app.js" type="text/javascript"></script>

   <script src="<?php echo base_url() ?>js/AdminLTE/dashboard.js" type="text/javascript"></script>

   <!-- ----datatables--- -->
   
   <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
   


   <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">


        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="/resources/demos/style.css">   

 <!-- end invoice -->
<script>
   $( document ).ready(function() {
   
       // var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();

       // $( "#datepicker" ).datepicker();
       // $( "#picker" ).datepicker();
       $("#example1").dataTable();
   
       // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#1abc9c',
               jackColor: '#fff',
               size: 'small'
           });
       });
   
       $('#accordion_close').on('click', function(){
               $('#accordion').slideToggle(300);
               $(this).toggleClass('accordion_down');
       });
   });
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>


<!-- invoice -->
<script>
        //adds extra table rows
        

        var i=$('table tr').length;
        $("#added").on('click',function(){
            count=$('table tr').length;
            html = '<tr>';
            html += '<td><input class="case" type="checkbox"/></td>';
            html += '<td><input type="text" data-type="item" name="item[]" id="isbn_'+i+'" class="form-control autocomplete_txt" autocomplete="off"></td>';
            html += '<td><input type="text" data-type="descr" name="descr[]" id="title_'+i+'" class="form-control autocomplete_txt" autocomplete="off"></td>';
            html += '<td><input type="text" name="price[]" id="price_'+i+'" class="form-control changesNo " autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
            html += '<td><input type="text" name="quantity[]" id="quantity_'+i+'" class="form-control changesNo quantity" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
            // html += '<td><input type="text" name="disct[]" id="disct_'+i+'" class="form-control changesNo quantity" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
            // html += '<td><input type="text" name="taxr[]" id="taxr_'+i+'" class="form-control changesNo quantity" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
            // html += '<td><input type="text" name="taxam[]" id="taxam_'+i+'" class="form-control totalLinePrice" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" readonly/></td>';
            html += '<td><input type="text" name="total[]" id="total_'+i+'" class="form-control totalLinePrice" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" readonly/></td>';
            html += '</tr>';
            $('#table').append(html);
            i++;
            autocomplete();
            return false;
        });

 /*$(document).ready(function(){

    var i = $('#table table tr').length;

        $(".addmore").on('click',function(){
            html = '<tr>';
            html += '<td><input class="case" type="checkbox"/></td>';
            html += '<td><input type="text" data-type="isbn" name="isbn[]" id="isbn_'+i+'" class="form-control autocomplete_txt" autocomplete="off"></td>';
            html += '<td><input type="text" data-type="title" name="title[]" id="title_'+i+'" class="form-control autocomplete_txt" autocomplete="off"></td>';
            html += '<td><input type="text" name="price[]" id="price_'+i+'" class="form-control changesNo " autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
            html += '<td><input type="text" name="quantity[]" id="quantity_'+i+'" class="form-control changesNo quantity" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
            html += '<td><input type="text" name="total[]" id="total_'+i+'" class="form-control totalLinePrice" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" readonly/></td>';
            html += '</tr>';
            $('#table').append(html);
            $i++;
            autocomplete();

        });
        return false;

        }); */
                      
        //to check all checkboxes
            $(document).on('change','#check_all',function(){
                $('input[class=case]:checkbox').prop("checked", $(this).is(':checked'));
            });

        //deletes the selected table rows
            $(".delete").on('click', function() {
                $('.case:checkbox:checked').parents("tr").remove();
                $('#check_all').prop("checked", false); 
                check();
                calculateTotal();
            });

        function check(){
            obj=$('table tr').find('span');
            $.each( obj, function( key, value ) {
                id=value.id;
                $('#'+id).html(key+1);
            });
        }

            //autocomplete script
                $(".autocomplete_txt").on('click', function() {  
                        var id= this.id;
                        var ids = id.split("_");
                        type = $(this).data('type'); 
                        if(type =='isbn' )autoTypeNo=0;
                        if(type =='title' )autoTypeNo=1;                         
                        $(this).autocomplete({
                            source: function( request, response ) {
                                $.ajax({
                                    url : '<?php echo base_url();?>/index.php/Invoice/getdata',
                                    //dataType: "json",
                                     method: 'post',
                                     data: {
                                        name_startsWith: request.term,
                                        datatype:type,
                                         //type: type
                                     },
                                     success: function(data) {
                                        var dispn = $.parseJSON(data);   
                                        response( $.map(dispn, function( item ) {
                                                var code = item.split("|");            
                                                return {
                                                    label: code[autoTypeNo],
                                                    value: code[autoTypeNo],
                                                    data : item
                                                }
                                            }));                                         
                                    }
                                });
                            },
                            autoFocus: true,            
                            minLength: 0,                            

                            select: function( event, ui ) {
                                var names = ui.item.data.split("|");   
                                $('#isbn_'+ids[1]).val(names[0]);
                                $('#title_'+ids[1]).val(names[1]);
                                $('#price_'+ids[1]).val(names[2]);
                                $('#quantity_'+ids[1]).val(1);
                                $('#total_'+ids[1]).val(1*names[2]);
                                calculateTotal();
                            }          
                                    
                        });
                    });
                function autocomplete()
                {
                    $(".autocomplete_txt").on('click', function() {  
                        var id= this.id;
                        var ids = id.split("_");
                        type = $(this).data('type'); 
                        if(type =='isbn' )autoTypeNo=0;
                        if(type =='title' )autoTypeNo=1;                         
                        $(this).autocomplete({
                            source: function( request, response ) {
                                $.ajax({
                                    url : '<?php echo base_url();?>/index.php/Invoice/getdata',
                                    //dataType: "json",
                                     method: 'post',
                                     data: {
                                        name_startsWith: request.term,
                                        datatype:type,
                                         //type: type
                                     },
                                     success: function(data) {
                                        var dispn = $.parseJSON(data);   
                                        response( $.map(dispn, function( item ) {
                                                var code = item.split("|");            
                                                return {
                                                    label: code[autoTypeNo],
                                                    value: code[autoTypeNo],
                                                    data : item
                                                }
                                            }));                                         
                                    }
                                });
                            },
                            autoFocus: true,            
                            minLength: 0,                            

                            select: function( event, ui ) {
                                var names = ui.item.data.split("|");   
                                $('#isbn_'+ids[1]).val(names[0]);
                                $('#title_'+ids[1]).val(names[1]);
                                $('#price_'+ids[1]).val(names[2]);
                                $('#quantity_'+ids[1]).val(1);
                                $('#total_'+ids[1]).val(1*names[2]);
                                calculateTotal();
                            }          
                                    
                        });
                    });
                }
            //price quantity change
            $(document).on('change keyup blur','.changesNo',function(){
                var id= this.id;                
                var ids = id.split("_");                 
                quantity = $('#quantity_'+ids[1]).val();                  
                price = $('#price_'+ids[1]).val();
                total = parseFloat(price)*parseFloat(quantity).toFixed(2);
                if( quantity!='' && price !='' ) $('#total_'+ids[1]).val(total); 
                calculateTotal();
            });

            $(document).on('change keyup blur','#tax',function(){
                calculateTotal();
            });

            //total price calculation 
            function calculateTotal(){
                subTotal = 0 ; total = 0;  quantity = 0;
                $('.quantity').each(function(){
                     if($(this).val() != '' )quantity += parseFloat( $(this).val() );
                })
                $('#totalQuantity').val(quantity);

                $('.totalLinePrice').each(function(){
                    if($(this).val() != '' )subTotal += parseFloat( $(this).val() );
                })
                $('#subTotal').val( subTotal.toFixed(2) );
                tax = $('#tax').val();
                if(tax != '' && typeof(tax) != "undefined" ){
                    taxAmount = subTotal * ( parseFloat(tax) /100 );
                    $('#taxAmount').val(taxAmount.toFixed(2));
                    total = subTotal + taxAmount;
                }else{
                    $('#taxAmount').val(0);
                    total = subTotal;
                }
                $('#totalAftertax').val( total.toFixed(2) );
                calculateAmountDue();
            }

            $(document).on('change keyup blur','#amountPaid',function(){
                calculateAmountDue();
            });

            //due amount calculation
            function calculateAmountDue(){
                amountPaid = $('#amountPaid').val();
                total = $('#totalAftertax').val();
                if(amountPaid != '' && typeof(amountPaid) != "undefined" ){
                    amountDue = parseFloat(total) - parseFloat( amountPaid );
                    $('.amountDue').val( amountDue.toFixed(2) );
                }else{
                    total = parseFloat(total).toFixed(2);
                    $('.amountDue').val( total );
                }
            }


            //It restrict the non-numbers
            var specialKeys = new Array();
            specialKeys.push(8,46); //Backspace
            function IsNumeric(e) {
                var keyCode = e.which ? e.which : e.keyCode;
                console.log( keyCode );
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                return ret;
            }            
            //autocomplete client    
             $(".autocomplete_client").on('click focus', function() {                         
                        type = $(this).data('type');   
                        
                        if(type =='cnumber' )autoTypeNo=0;
                        if(type =='name' )autoTypeNo=1;                         
                        $(this).autocomplete({
                            source: function( request, response ) {
                                $.ajax({
                                    url : '<?php echo base_url();?>/index.php/Invoice/customer',
                                    //dataType: "json",
                                     method: 'post',
                                     data: {
                                        name_startsWith: request.term,
                                        datatype:type,
                                         //type: type
                                     },
                                     success: function(data) {
                                       var dispn = $.parseJSON(data);   
                                        response( $.map(dispn, function( item ) {
                                                var code = item.split("|");            
                                                return {
                                                    label: code[autoTypeNo],
                                                    value: code[autoTypeNo],
                                                    data : item
                                                }
                                            })); 
                                        }
                                });
                            },
                            autoFocus: true,            
                            minLength: 0,                            

                            select: function( event, ui ) {
                                var names = ui.item.data.split("|");   
                                $('#customerid').val(names[0]);
                                $('#clientCompanyName').val(names[1]);
                                $('#clientAddress').val(names[2]);          
                                $('#clientdetails').val(names[3]);
                            }                                             
                        });
                    });
        </script>
  <!-- end invoice -->    

  <!-- ---- invoice details---- -->  
  <script>

function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}


window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>


</body>
</html>