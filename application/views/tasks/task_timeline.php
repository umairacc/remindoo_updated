<?php $this->load->view('includes/header'); ?>

<div class="pcoded-content tasktimetop <?php if ($_SESSION['permission']['Task']['view'] != '1') { ?> permission_deined <?php } ?>">
  <div class="pcoded-inner-content">
    <div class="card propose-template" style="display: block;">
      <!-- admin start-->
      <div class="client_section col-xs-12 floating_set">
      </div> <!-- all-clients -->
      <div class="client_section user-dashboard-section1 floating_set">
        <div class="all_user-section floating_set proposal-commontab tsk-timeframe">
          <div class="card-header">
            <h5>Timeline</h5>
            <div class="page-tabs">
              <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                <li class="nav-item">
                  <a class="nav-link active" href="<?php echo base_url(); ?>user/task_list">All Tasks</a>
                  <div class="slide"></div>
                </li>
                <?php if ($_SESSION['permission']['Task']['create'] == 1) { ?>
                  <li class="nav-item">
                    <a class="nav-link " href="<?php echo base_url(); ?>user/new_task">Create task</a>
                    <div class="slide"></div>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link " href="<?php echo base_url(); ?>task">Task Timeline</a>
                    <div class="slide"></div>
                  </li>
                <?php }
                if ($_SESSION['user_type'] == 'FA') { ?>
                  <li class="nav-item">
                    <a class="nav-link " href="<?php echo base_url(); ?>Task_Status/task_progress_view">Task Progress Status</a>
                    <div class="slide"></div>
                  </li>
                <?php }
                if ($_SESSION['permission']['Task']['edit'] == 1) { ?>
                  <li class="nav-item">
                    <a class="nav-link " href="<?php echo base_url(); ?>user/task_list_kanban">Switch To Kanban</a>
                    <div class="slide"></div>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link " href="<?php echo base_url(); ?>user/task_list/archive_task">Archive Task</a>
                    <div class="slide"></div>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link " href="<?php echo base_url(); ?>user/task_list/complete_task">Completed Task</a>
                    <div class="slide"></div>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#import-task">Import tasks</a>
                    <div class="slide"></div>
                  </li>
                <?php } ?>
              </ul>
            </div>
            <div class="f-right">
              <!--   <div class="form-group check-options">
                                  <div class="checkbox-color checkbox-primary" style="display: none;">
                                  <input type="checkbox"  name="public" value="Public" id="p-check">
                                  <label for="p-check">Public </label>
                                  </div>

                                  <div class="checkbox-color checkbox-primary">
                                  <input type="checkbox"  name="billable"  id="billable"  id="p-check1">
                                  <label for="p-check1"> Billable</label>
                                  </div>
                                  </div> -->
              <input class="form-control form-control-lg width_set" type="text" id="search-criteria" placeholder="Search notes">
              <!--   <select name="related_to" id="related_to" class="width_set">
                                  <option value="">Select</option>
                                  <option value="tasks">Main Task</option>
                                  <option value="Auto created task">Auto Created Task </option>
                                  <option value="magic task">Magic Task</option>
                                  </select> -->

              <select name="related_to" id="related_to" class="width_set">
                <option value="">Select</option>
                <option value="tasks">Main Task</option>
                <!--  <option value="service_reminder">Auto Created Task </option> -->
                <option value="sub_task">Sub Task</option>
              </select>
              <select name="task_status" id="task_status" class="width_set">
                <option value="">Select</option>
                <?php foreach ($task_status as $key => $status_value) { ?>
                  <option value="<?php echo $status_value['id'] ?>"><?php echo $status_value['status_name'] ?></option>
                  <!-- <option value="notstarted">Not started</option>
                                  <option value="inprogress">In Progress</option>
                                  <option value="awaiting">Awaiting Feedback</option>
                                  <option value="complete">Complete</option>
                                  <option value="archive">Archive</option> -->
                <?php } ?>
              </select>
              </select>
              <select name="company_name" id="company_name" class="width_set">
                <?php
                /*$query1 = $this->db->query(
                            "SELECT * 
                            FROM `user` 
                            where user_type='FC' 
                            AND autosave_status!='1' 
                            and crm_name!='' 
                            and firm_id=" . $_SESSION['firm_id'] . " 
                            order by id DESC"
                          );
                $results1 = $query1->result_array();
                //print_r($results1);
                $res = array();
                foreach ($results1 as $key => $value) {
                  array_push($res, $value['id']);
                }*/
                
                /*$res  = $this->db->query(
                          "SELECT id 
                          FROM user 
                          WHERE user_type='FC' 
                          AND autosave_status!='1' 
                          AND crm_name!='' 
                          AND firm_id=" . $_SESSION['firm_id'] . " 
                          ORDER BY id DESC"
                        )->result_array();
                
                if (!empty($res)) {
                  foreach($res as $r){
                    $query = $this->db->query(
                      "SELECT * 
                      FROM client 
                      WHERE autosave_status=0 
                      AND user_id IN (" . $res[0]['ids'] . ") 
                      ORDER BY id DESC "
                    );
                    $results = $query->result_array();
                  }
                } else {
                  $results = 0;

                  $query = $this->db->query(
                            "SELECT * 
                            FROM client 
                            WHERE autosave_status=0 
                            and user_id=" . $_SESSION['id'] . " 
                            order by id desc "
                          );
                  $results = $query->result_array();
                }*/

                /*echo '<pre>';
                print_r($results);
                die;*/
                $results = [];
                //die("Stop Here");
                if (count($results) > 0) {  ?>
                  <option value="">Select</option>
                  <?php foreach ($results as $key => $val) {  ?>
                    <option value="<?php echo $val['id']; ?>"><?php echo $val["crm_company_name"]; ?></option>
                <?php
                  }
                }  ?>
              </select>

              <select name="worker" id="worker" class="width_set">
                <option value="">Select</option>
                <?php
                if (isset($staff_form) && $staff_form != '') { ?>
                  <option disabled>Staff</option>
                  <?php
                  $i = 0;
                  foreach ($staff_form as $value) {  ?>
                    <option value="<?php echo $value['id']; ?>"><?php echo $value["crm_name"]; ?></option>
                <?php $i++;
                  }
                } ?>
              </select>
            </div>


          </div>

          <div class="row history tasktime">


            <div class="card">

              <div class="post" style="display: none">
                <?php
                foreach ($task_list as $task) {
                ?>
                  <div class="cd-timeline-block <?php echo $task['id']; ?>">
                    <div class="cd-timeline-icon bg-primary">
                      <i class="icofont icofont-ui-file"></i>
                    </div>
                    <div class="cd-timeline-content card_main">
                      <div class="p-0">
                        <div class="btn-group dropdown-split-primary">
                          <button type="button" class="btn btn-primary search_word">
                            <?php
                            $status_val = $this->Common_mdl->select_record('task_status', 'id', $task['task_status']);
                            //print_r($status_val);

                            echo $status_val['status_name']; ?>
                          </button>
                        </div>
                      </div>
                      <div class="p-20 search_word">
                        <h6 style="width: 78%;"><?php echo $task['subject']; ?> </h6>
                        <div class="timeline-details search_word">
                          <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Start Date : <?php echo (strtotime($task['start_date']) != '' ?  date('d-m-Y', strtotime($task['start_date'])) : ''); ?></span> </a>
                          <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($task['end_date']) != '' ?  date('d-m-Y', strtotime($task['end_date'])) : ''); ?></span> </a>
                          <p class="m-t-0 search_word">Company Name :

                            <?php
                            if (isset($task['company_name']) && is_numeric($task['company_name']) &&  $task['company_name'] != '' && $task['related_to'] != 'leads') {
                              //echo $this->Common_mdl->getcompany_name($task['company_name']);
                            } else {
                              echo '-';
                            }
                            ?></p>
                          <p class="m-t-0 search_word"> Staffs:
                            <?php
                            $explode_worker = explode(',', $task['worker']);
                            $username = array();
                            foreach ($explode_worker as $key => $val) {
                              $getUserProfilename = "";//$this->Common_mdl->getUserProfileName($val);
                              array_push($username, $getUserProfilename);
                            }
                            $variable = array_filter($username);
                            if (!empty($variable)) {
                              echo implode(',', $username);
                            } else {
                              echo "Not Assigned";
                            }
                            ?>
                          </p>
                        </div>
                      </div>

                    </div>
                  </div>

                <?php } ?>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<input type="hidden" id="row" value="0">
<input type="hidden" id="all" value="<?php echo $row_count; ?>">



<link rel="stylesheet" href="<?php echo base_url(); ?>assets/cdns/bootstrap-select.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/cdns/app.css">
<script src="<?php echo base_url(); ?>assets/cdns/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/cdns/bootstrap-tagsinput.css">
<link href="<?php echo base_url(); ?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/timeline-style.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />
<?php $this->load->view('includes/footer'); ?>
<script src='<?php echo base_url(); ?>/assets/js/jquery-3.3.1.js' type='text/javascript'></script>
<script>
  // $(document).ready(function(){

  //     $(window).scroll(function(){

  //         var position = $(window).scrollTop();
  //         var bottom = $(document).height() - $(window).height();

  //         if( position == bottom ){

  //             var row = Number($('#row').val());
  //             var allcount = Number($('#all').val());
  //             var rowperpage = 15;
  //             row = row + rowperpage;

  //             if(row <= allcount){
  //                 $('#row').val(row);
  //                 $.ajax({
  //                     url: '<?php echo base_url(); ?>/Task/fetch_data',
  //                     type: 'post',
  //                     data: {row:row},
  //                     beforeSend: function() {
  //                          $(".LoadingImage").show();
  //                     },
  //                     success: function(response){
  //                         $(".post:last").after(response).show().fadeIn("slow");
  //                         $(".LoadingImage").hide();
  //                     }
  //                 });
  //             }
  //         }

  //     });

  // });


  $(document).on('keyup', '#search-criteria', function() {
    var txt = $('#search-criteria').val();
    $(".cd-timeline-block").hide();
    $('.search_word').each(function() {

      // //console.log($(this).text());
      if ($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1) {
        $(this).show();
        //console.log($(this).text());         

        $(this).parents('.cd-timeline-block').show();

      }
    });

  });


  $("#billable,#related_to,#task_status,#company_name,#worker").change(function() {
    var billable = $("#billable").val();
    var related_to = $("#related_to").val();
    var task_status = $("#task_status").val();
    var company_name = $("#company_name").val();
    var worker = $("#worker").val();
    console.log(billable);
    console.log(related_to);
    console.log(task_status);
    console.log(company_name);
    console.log(worker);

    var formData = {
      'billable': billable,
      'related_to': related_to,
      'task_status': task_status,
      'company_name': company_name,
      'worker': worker
    };

    $.ajax({
      url: '<?php echo base_url(); ?>/Task/filter_data',
      type: 'post',
      data: formData,
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(response) {
        //alert('ok');
        // $(".post").css('background-color',);
        $('.post').css('display', 'block');
        $(".post").html('');
        $(".post").html(response);
        $(".LoadingImage").hide();
      }
    });







  })
</script>