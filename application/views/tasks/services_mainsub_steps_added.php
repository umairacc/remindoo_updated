                 <!--start-->
                <!--   <div class="col-sm-12 common_form_section2 forservice">       -->                    
                   <!--   <div class="deadline-crm1 floating_set">
                        <ul class="nav nav-tabs all_user1 md-tabs pull-left">
                           <li class="nav-item">
                              <a class="nav-link" href="javascript:;">Edit Services</a>
                              <div class="slide"></div>
                           </li>
                        </ul> -->
                        <?php
                          if($table=='work_flow')
                            {
                              
                              $column='work_flow_id';
                              $step_val="wor_".$service['id'];

                            }
                            else
                            {
                               $column='service_id';
                               $step_val="ser_".$service['id'];
                       
                            } ?>
                        <div class="form-group f-right">
                            <button type="button" name="add_task" class="btn btn-primary add_step add-task-step" id="<?php echo  $step_val ?>">New Step</button>
                        </div>                       
                        <?php
                       
                
                        //$records = $this->db->query("select * from service_steps where ".$column."=".$service_id." ")->result_array();
                        $records =$this->Service_model->get_steps( $service_id , $column );
                        $i=1;
                        foreach($records as $rec){ ?>

                          <div class="service-steps">
                            <h4>step <?php echo $i; ?></h4>
                            <div class="inside-step">
                              <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                              <div class="checkbox-fade fade-in-primary">                        
                                <label class="check-task">
                                <span class="task-title-sp"><?php echo ucfirst($rec['title']); ?></span>
                                <span class="f-right">
                                <p class="action_01">
                                  <a href="javascript:;" class="title_delete" id="<?php echo $rec['id']; ?>" data-type="<?php echo ($table=='work_flow') ? 1: 0 ?>" ><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a>
                                  <a href="javascript:;" class="title_edit" id="<?php echo $rec['id']; ?>" data-type="<?php echo ($table=='work_flow') ? 1: 0 ?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                </p>
                                </span>
                                </label>
                              </div>

                              <div class="dropdown_con">
                                <span class="f-right action_01"><a href="javascript:;" class="steps_adds" id="<?php echo $rec['id']; ?>" data-val="<?php echo ($rec['work_flow_id']!=0)?1:0 ?>" ><i class="fa fa-plus fa-6" aria-hidden="true"></i></a></span>
                                <ul>
                                <?php
                                //$steps_details = $this->db->query("select * from service_step_details where ".$column."=".$service_id." and step_id = ".$rec['id']."  ")->result_array();

                                  $steps_details = $this->Service_model->get_sub_steps( $rec['id'] , 'step_id' );

                               if(count($steps_details)>0){

                                foreach($steps_details as $steps)
                                { 
                                  ?>
                                  <li>
                                    <div class="checkbox-fade fade-in-primary">                        
                                      <label class="check-task">
                                      <span class="task-title-sp"><?php echo $steps['step_content']; ?></span>
                                      <span class="f-right">
                                      <p class="action_01">
                                        <a href="javascript:;" class="step_section_delete" id="<?php echo $steps['id']; ?>" data-type="<?php echo ($table=='work_flow') ? 1: 0 ?>"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a>
                                        <a href="javascript:;" class="step_section_edit" id="<?php echo $steps['id']; ?>" data-type="<?php echo ($table=='work_flow') ? 1: 0 ?>" ><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                      </p>
                                      </span>
                                      </label>
                                    </div>
                                  </li>
                                  <?php 
                                }
                                   }
                                   else
                                   { ?>
                                    <li>
                                    <div class="checkbox-fade fade-in-primary">                        
                                      <label class="check-task">
                                      <span class="task-title-sp">No Sub Details</span>

                                      </label>
                                      </div>
                                      </li>


                                    
                                  <?php  } ?>
                                  
                                
                                </ul>
                              </div>

                            </div>
                          </div>
                      <?php $i++; } ?>
                <!--          </div> -->
                      <!-- close -->
                 <!--   </div> -->
               <!-- </div> -->
               <!-- Page body end -->
        