<?php 
   $this->load->view('includes/header');
?>
<link href="<?php echo base_url();?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/timepicki.css" rel="stylesheet">
<style type="text/css">
	.checkbox-error label::before {
	border: 2px solid red !important;
	}
	.position-alert1 h4 {
	border-bottom: 1px solid #ddd;
	padding-bottom: 10px;
	margin-bottom: 15px;
	}
	#summary_new p {
	border-bottom: 1px solid #ddd;
	padding: 10px 0px;
	margin: 0;
	color: green;
	}
	#summary_val p
	{
	color: green;
	}
	label.remider_option_error.error {
	text-align: center;
	padding-left: 0 !important;
	}
	.picker-category.form-group.border-checkbox-section.form-radio {
	padding: 0px 30px 0px 30px;
	}
	/*.required_check{
	border: 1px solid red !important;
	}*/
</style>
<!-- <link href="<?php echo base_url();?>assets/css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" /> -->
<?php 
	$team = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();
	$department = $this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();
	
	$timeline_task_section=isset($timeline_task_section)?$timeline_task_section:'';
	$task_timeline_description='';
	$task_timeline_comapny='';
	if($timeline_task_section!='')
	{
	 $get_data_timeline=$this->db->query('select * from timeline_services_notes_added where id='.$timeline_task_section.' ')->row_array();
	 $service_type=$get_data_timeline['services_type'];
	 $task_timeline_description=$get_data_timeline['notes'];
	 $timeline_user=$get_data_timeline['user_id'];
	 $records['company_id']=$this->Common_mdl->get_field_value('client','id','user_id',$timeline_user);
	}
	?>
<div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
	<div class="newupdate_alert">
		<a href="#" class="close" id="close_info_msg">×</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				Please! Select Record...
			</div>
		</div>
	</div>
</div>
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<!-- Main-body start -->
		<div class="main-body">
			<div class="page-wrapper">
				<!-- Page body start -->
				<div class="page-body">
					<div class="row">
						<!--start-->
						<div class="col-sm-12 common_form_section2">
							<div class="modal-alertsuccess alert alert-success" style="display:none;">
								<div class="newupdate_alert">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<div class="pop-realted1">
										<div class="position-alert1">
											Your task was successfully added.
										</div>
									</div>
								</div>
							</div>
							<div class="modal-alertsuccess  alert alert-danger" style="display:none;">
								<div class="newupdate_alert">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<div class="pop-realted1">
										<div class="position-alert1">
											Please fill the required fields.
										</div>
									</div>
								</div>
							</div>
							<!-- model success msg contect -->
							<div class="modal-alertsuccess alert alert-danger-check succ dashboard_success_message1" style="display:none;">
								<div class="newupdate_alert">
									<a href="#" class="close" data-dismiss="modal">×</a>
									<div class="pop-realted1">
										<div class="position-alert1">
											<h4>Summary</h4>
											<div id="summary_new"></div>
										</div>
									</div>
								</div>
							</div>
							<!-- end of success msg content -->
							<form id="add_new_task" method="post" action="" enctype="multipart/form-data">
								<div class="deadline-crm1 floating_set">
									<ul class="nav nav-tabs all_user1 md-tabs pull-left">
										<li class="nav-item">
											<a class="nav-link" href="<?php echo base_url();?>user/task_list"><img class="themicon" src="http://remindoo.uk/assets/images/tabicon1.png" alt="themeicon">All Tasks</a>
											<div class="slide"></div>
										</li>
										<li class="nav-item ">
											<a class="nav-link active"  href="<?php echo base_url(); ?>user/new_task"><img class="themicon" src="http://remindoo.uk/assets/images/tabicon2.png" alt="themeicon">Create New task</a>
											<div class="slide"></div>
										</li>
										<li class="nav-item">
											<!--  <a class="nav-link "  href="<?php echo base_url();?>tasksummary/import_task">Import tasks</a> -->
											<a class="nav-link" data-toggle="modal" data-target="#import-task"><img class="themicon" src="http://remindoo.uk/assets/images/tabicon3.png" alt="themeicon">Import tasks</a> 
											<div class="slide"></div>
										</li>
									</ul>
									<div class="form-group create-btn new_task-bts1 nw-tsk f-right">
										<input type="submit" name="add_task" class="btn btn-succuess" value="create"/>
										<!--  <input type="button" name="button" value="Archive"> -->
										<!--   <input type="button" name="button" value="delete" onclick="clear_form_elements('#add_new_task')" > -->
										<input type="button" name="button" value="reset" onclick="clear_form_elements('#add_new_task')" >
									</div>
								</div>
								<div class="col-xs-12 public-bill01 adnewtsk">
									<div class="add_new_post04">
										<div class="row">
											<div class="col-sm-12 attached-popup1">
												<div class="form-group check-options">
													<div class="checkbox-color checkbox-primary" style="display: none;">
														<input type="checkbox"  name="public" value="Public" id="p-check">
														<label for="p-check">Public </label>
													</div>
													<div class="checkbox-color checkbox-primary">
														<input type="checkbox"  name="billable" value="Billable" checked id="p-check1">
														<label for="p-check1"> Billable</label>
													</div>
												</div>
												<!-- 16-06-2018 we remove multiple="multiple" -->
												<div class="form-group attach-files f-right">
													<div id="input_container" style="width: 0px; height: 0px; overflow: hidden">
														<input type="file" id="inputfile" name="attach_file[]" multiple="multiple" onchange="readURL(this);"/>
													</div>
													<div class="button" onclick="uploadss();">attach file</div>
													<span id="preview_image_name"></span>
													<!--  <img src="<?php echo base_url().'uploads/profile_image/avatar'?>" id="preview_image" > -->
												</div>
												<div class="form-group attach-files f-right">
													<button class="button" id='exc_service_button' style="display: none;">Exclude Services</button>
												</div>
												<!-- end of 16-06-2018 -->
											</div>
											<div class="space-equal-data col-sm-6 spaceleftcls attach-task-new">
												<!--   <div class=" form-group">
													<label>Task Name</label>
													<input type="text" name="task_name" class="form-control" placeholder="Enter Name" /> -->
												<input type="hidden" name="timeline_task_id" id="timeline_task_id" value="<?php if(isset($timeline_task_section)){  echo $timeline_task_section; } ?>">
												<input type="hidden" name="timeline_task_id" id="timeline_task_id" value="<?php if(isset($timeline_task_section)){  echo $timeline_task_section; } ?>">
												<!--</div> -->
												<div class="form-group">
													<label>Subject</label>
													<input type="text" class="form-control" name="subject" placeholder="Enter Subject" value="">
												</div>
												<div class="row new-tsk01">
													<div class="col-sm-6">
														<div class="form-group">
															<label>Start Date</label>
															<div class="input-group date">
																<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
																<input type='text' class="" id='txtFromDate' name="startdate" placeholder="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>" />
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group ">
															<label>End Date</label>
															<div class="input-group date">
																<span class="input-group-addon "><span class="icofont icofont-ui-calendar"></span></span>
																<input type='text' class="" id='txtToDate' name="enddate" placeholder="dd-mm-yyyy" />
															</div>
														</div>
													</div>
													<div class="form-group widthfifty">
														<label>Priority</label>
														<select name="priority" class="form-control" id="priority">
															<option value="low">Low</option>
															<option value="medium">Medium</option>
															<option value="high">High</option>
															<option value="super_urgent">Super Urgent</option>
														</select>
													</div>
													<!-- <div class="form-group widthfifty">
														<label>Related to do</label>
														<select class="form-control" name="related_to" id='related_to' placeholder="Select">
														   <option value='tasks'>tasks</option>
														</select>
														</div> -->
													<div class="form-group widthfifty">
														<label>Task status*</label>
														<select name="task_status" class="form-control" id="task_status">
															<option value="notstarted">Not started</option>
															<option value="inprogress">In Progress</option>
															<option value="awaiting">Awaiting Feedback</option>
															<!-- <option value="testing">Testing</option> -->
															<option value="complete">Complete</option>
															<option value="archive" <?php if(isset($_SESSION['sta_tus']) &&  $_SESSION['sta_tus']=='archive'){ ?> selected="selected" <?php } ?>>Archive</option>
														</select>
													</div>
													<div class="form-group widthfifty even tag-design">
														<h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
														<!-- <div class="dropdown-sin-255">
															<select style="display:none" name="tag[]" id="tag" multiple placeholder="Select">
															   <?php foreach($tag as $tags) {
																?>
															   <option value="<?php echo $tags['id']?>"><?php echo $tags['tag'];?></option>
															   <?php } ?>
															</select>
															</div> -->
														<input type="text" value="" name="tags" id="tags" class="tags" />
													</div>
													<!-- for realated to services 16-10-2018 -->
													<div class="form-group widthfifty even">
														<label>Related to Services<?php //echo strtolower($service_type); ?></label>
														<div class="dropdown-sin-5 lead-form-st ">
															<select class="form-control" name="related_to_services[]" id='related_to_services' placeholder="Select any Services" multiple="">
																<?php 
																	$settings_val=$this->db->query('select * from service_lists where id in(1,2,3,4,5,6,7,8,9,10,11,12)')->result_array();
																	
																	foreach($settings_val as $set_key => $set_value)
																	 {
																	  ?>
																<option value="<?php echo $set_value['services_subnames']; ?>" <?php if(isset($service_type) && $service_type!=''){ if(strtolower($service_type) == $set_value['services_subnames']){?> selected="selected" <?php } } ?> ><?php echo $set_value['service_name']; ?></option>
																<?php
																	}
																	  ?>
															</select>
														</div>
													</div>

													<div class="form-group">
														<label>Choose Client/Firm</label>
														<div class="dropdown-sin-4">
															<select name="company_name" id="company_name" onchange="alert('inline');" placeholder="select Company">
																<!-- <input type="text" name="company" id="company"> -->
																<option value="">Select Company</option>
																<?php 
																	$query1=$this->db->query("SELECT * FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$_SESSION['id']." order by id DESC");
																	$results1 = $query1->result_array();
                                                   $res=array();
                                                   
																	foreach ($results1 as $key => $value) {
																	   array_push($res, $value['id']);
                                                   }
                                                   
																	if(!empty($res)) {
																	   $im_val=implode(',',$res);
																	   // echo $im_val;
																	   $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (".$im_val.") order by id desc ");    
																	   $results = $query->result_array();  
																	}
																	else {
                                                      $results = 0;
                                                      $query = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id=".$_SESSION['id']." order by id desc ");    
                                                      $results = $query->result_array();  
																	}  
																	if (count($results) > 0)  
																	{  
                                                      foreach($results as $key => $val) {  
                                                         $is_client_selected = '';
                                                         if ( (isset($records['company_id']) && $records['company_id'] == $val['id']) ) {
                                                            $is_client_selected = 'selected';
                                                         } 
                                                         ?>
                                                      <option value="<?php echo $val['id'];?>" <?php echo $is_client_selected ?>>
                                                         <?php echo $val["crm_company_name"];?>
                                                      </option>
                                                      <?php
                                                         }  
																	}  ?>
															</select>
														</div>
														<!--  <input type="text" name="company_name" id="search_company" class="form-control" placeholder="Search Client" />  
															<input type="hidden" value="" name="user_id" id="user_id">
															<div id="searchresult"></div> -->
														<input type="hidden" name="user_id" id="user_id" value="<?php if(isset($timeline_user)){ echo $timeline_user; } ?>">
													</div>
													<!-- <div class="form-group cus-permission radio_button01 exclude_step" style="display: none">
														<div class="radio radio-inline">
														   <label><input type="checkbox" class="cus_per" name="exclude_step" value="1" id="exclude-step">
														   <i class="helper"></i>Exclude Steps</label>
														</div>
														</div> -->
													<!-- end of related services 16-10-2018 -->
													<div id="base_projects"></div>
													<div class="form-group widthfifty">
														<label>Assignfg to(worker)</label>
														<div class="dropdown-sin-2 lead-form-st">
															<select style="display:none" name="worker[]" id="worker" multiple placeholder="Select">
																<?php
																	if(isset($staff_form) && $staff_form!='') {
																	
																	foreach($staff_form as $value) {
																	?>
																<!-- <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option> -->
																<?php
																	}
																	}
																	?>
																<?php
																	if(isset($staff_form) && $staff_form!='') 
																	{ ?>
																<option disabled>Staff</option>
																<?php 
																	$i=0;
																	foreach($staff_form as $value) 
																	{
																	?>
																<option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
																<?php
																	$i++;
																	   }
																	   }
																	if(isset($team) && $team!='') 
																	   { ?>
																<option disabled>Team</option>
																<?php 
																	$i=0;
																	foreach($team as $value) 
																	{
																	?>
																<option value="tm_<?php echo $value['id']; ?>" ><?php echo $value["team"]; ?></option>
																<?php
																	$i++;
																	   }
																	   }
																	 if(isset($department) && $department!='') 
																	   { ?>
																<option disabled>Department</option>
																<?php 
																	$i=0;
																	foreach($department as $value) 
																	{
																	?>
																<option value="de_<?php echo $value['id']; ?>"><?php echo $value["new_dept"]; ?></option>
																<?php
																	$i++;
																	   }
																	   }
																	   ?>
															</select>
														</div>
													</div>
													<div class="form-group widthfifty even">
														<label>Notify to(Manager)</label>
														<div class="dropdown-sin-3">
															<select style="display:none" name="manager[]" id="manager" multiple placeholder="Select">
																<?php foreach($manager as $managers) {
																	?>
																<option value="<?php echo $managers['id']?>"><?php echo $managers['crm_name'];?></option>
																<?php } ?>
															</select>
														</div>
													</div>
												</div>
												<div class="form-group cus-permission radio_button01 ">
													<div class="radio radio-inline">
														<label><input type="checkbox" class="cus_per" name="customize" value="1" id="cus-per">
														<i class="helper"></i>Customize Permissions</label>
													</div>
												</div>
												<div class="form-group   select-user">
													<label class="select-head">Select Users*</label>
													<div class="sel-user-options">
														<?php foreach ($roles as $roles_key => $roles_value) {
															# code...
															?>
														<div class="checkbox-color checkbox-primary">
															<input type="checkbox"  class="selectuser" name="selectuser[]" value="<?php echo $roles_value['id'];?>" id="sel-user<?php echo $roles_value['id'];?>"><label for="sel-user<?php echo $roles_value['id'];?>"><?php echo $roles_value['role'];?></label>
														</div>
														<?php } ?>
													</div>
												</div>
											</div>
											<!-- col-sm-6--> 
											<div class="space-equal-data col-sm-6">
												<!--         <div class="form-group attach-files file-attached1">
													<div id="input_container" style="width: 0px; height: 0px; overflow: hidden">
													   <input type="file" id="inputfile" name="attach_file" multiple="multiple" onchange="readURL(this);"/>
													</div>
													 <div class="button" onclick="uploadss();">attach file</div>
													<!--  <img src="<?php echo base_url().'uploads/profile_image/avatar'?>" id="preview_image" > -->
												<!--</div> -->
												<div class="form-group">
													<label>Description</label>
													<textarea rows="5" type="text" class="form-control" name="description" id="description" placeholder="Write something"><?php echo $task_timeline_description; ?></textarea>
												</div>
												<div class="form-group">
													<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Make this recuring task</button>
													<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalone">Remind me about the task</button>
												</div>
											</div>
											<!-- col-sm-6--> 
											<!-- Modal -->
											<div class="modal fade recurring-msg common-schedule-msg1" id="myModal" role="dialog">
												<div class="modal-dialog">
													<!-- Modal content-->
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" data-dismiss="modal" class="close">&times;</button>
															<h4 class="modal-title">Schedule Recurring Message</h4>
														</div>
														<div class="modal-body">
															<div class="row">
																<div class="col-sm-6 form-group common-repeats1 ">
																	<label class="label-column1 col-form-label">Starts:</label>
																	<div class="label-column2 input-group date">
																		<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
																		<input type='text' class="required1" id='modalFromDate' name="sche_startdate" placeholder="dd-mm-yyyy" />
																	</div>
																</div>
																<div class="col-sm-6 form-group common-repeats1 ">
																	<label class="label-column1 col-form-label">Send time:</label>
																	<div class="label-column2  input-group bootstrap-timepicker timepicker">
																		<input id="timepicker1" type="text" class="form-control input-small required1" name="sche_send_time">
																		<!-- <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span> -->
																	</div>
																</div>
																<div class="form-group col-sm-6 every-week01 floating_set">
																	<label class="label-column1  col-form-label">Every:</label>
																	<div class="label-column2">
																		<!-- <input type="text" class="form-control week-text" name="every" placeholder="" value=""> -->
																		<input type="number" class="form-control week-text required1"  name="every" min='1' id="every" placeholder="" value="">
																		<!-- <p>weeks</p> -->
																	</div>
																</div>
																<div class="form-group col-sm-6">
																	<label class="label-column1 col-form-label">Repeats:</label>
																	<div class="label-column2">
																		<select name="repeats" class="form-control repeats" id="repeats" data-date="" data-day="" data-month="">
																			<option value="week">Week</option>
																			<option value="day">Day</option>
																			<option value="month">Month</option>
																			<option value="year">Year</option>
																		</select>
																	</div>
																</div>
																<div id="weekly" class="   every_popup1">
																	<div class="form-group days-list floating_set ">
																		<label class="label-column1 col-form-label">On:</label>
																		<div class="label-column2">
																			<div class="days-left">
																				<div class="checkbox-color checkbox-primary">
																					<input type="checkbox" class="border-checkbox dayss"  name="days[]" value="Sunday" id="day-check1">
																					<label for="day-check1"  class="border-checkbox-label">Sun</label>
																				</div>
																				<div class="checkbox-color checkbox-primary">
																					<input type="checkbox"  class="border-checkbox dayss" name="days[]" value="Monday" id="day-check2"><label  class="border-checkbox-label" for="day-check2">Mon</label>
																				</div>
																				<div class="checkbox-color checkbox-primary">
																					<input type="checkbox"  class="border-checkbox dayss" name="days[]" value="Tuesday" id="day-check3"><label  class="border-checkbox-label" for="day-check3">Tue</label> 
																				</div>
																				<div class="checkbox-color checkbox-primary">
																					<input type="checkbox"  class="border-checkbox dayss" name="days[]" value="Wednesday" id="day-check4"><label  class="border-checkbox-label" for="day-check4">Wed</label>
																				</div>
																				<div class="checkbox-color checkbox-primary">
																					<input type="checkbox"  class="border-checkbox dayss" name="days[]" value="Thursday" id="day-check5"><label  class="border-checkbox-label" for="day-check5">Thur</label> 
																				</div>
																				<div class="checkbox-color checkbox-primary">
																					<input type="checkbox"  class="border-checkbox dayss" name="days[]" value="Friday" id="day-check6"><label  class="border-checkbox-label" for="day-check6">Fri</label>
																				</div>
																				<div class="checkbox-color checkbox-primary">
																					<input type="checkbox"  class="border-checkbox dayss" name="days[]" value="Saturday" id="day-check7"><label  class="border-checkbox-label" for="day-check7">Sat</label>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-12  form-group left-align radio_button01">
																	<label class="label-column1 left-label">Ends:</label>
																	<div class="forms-option label-column2">
																		<div class="radio-width01">
																			<input type="radio"  name="end" value="after" id="end-date">
																			<label for="end-date">After</label>
																			<!-- <input type="text" class="form-control message-count" name="message" placeholder="" value="" > -->
																			<input type="number" class="form-control message-count"  id="message" name="message" placeholder="" value="" >
																			<span>messages</span> 
																		</div>
																		<div class="radio-width01 on-optin">
																			<input type="radio"  name="end" value="on" id="end-date3">
																			<label for="end-date3">On </label>
																			<div class="input-group date">
																				<!-- <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
																				<input type='text' class="" id='modalendDate' name="sche_on_date" data-year="" data-day="" data-month=""/>
																			</div>
																		</div>
																		<div class="no-end radio-width01">
																			<input type="radio"  name="end" value="noend" id="end-date1" checked="checked">
																			<label for="end-date1">No end date</label>
																		</div>
																	</div>
																</div>
																<div class=" left-align col-sm-12">
																	<label class="label-column1 left-label">Summary:</label>
																	<div class="label-column2">
																		<p id="summary"></p>
																		<input type="hidden" name="recuring_sus_msg" id="recuring_sus_msg">
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" name="button"  class="btn btn-default submitBtn_new" id="submitBtn">Schedule</button>
															<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> -->
															<button type="button" data-dismiss="modal" >Cancel</button>
														</div>
													</div>
												</div>
											</div>
											<!-- modal close-->
											<!-- Modal one-->
											<div class="modal fade common-schedule-msg1 remind-me-picker" id="myModalone" role="dialog">
												<div class="modal-dialog">
													<!-- Modal content-->
													<div class="modal-content">
														<div class="modal-header">
															<button type="button"  data-dismiss="modal" class="close">&times;</button>
															<h4 class="modal-title">Remind Me</h4>
														</div>
														<div class="modal-body">
															<div class="picker-category form-group border-checkbox-section form-radio ">
																<label class="remider_option_error error"></label>
																<!-- display: none; for no functionlity to this -->
																<div class="hour-rate1" style="display: none;">
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" class="remind_poup_data" name="radio" id="repeat1" value="nobody"><label for="repeat1">Only if nobody responds</label>
																	</div>
																</div>
																<div class="hour-rate1">
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" class="remind_poup_data" name="radio"  id="repeat2" value="1hour"><label for="repeat2">In 1 hour</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" class="remind_poup_data" name="radio"   id="repeat3" value="2hour"><label for="repeat3">In 2 hour</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" class="remind_poup_data" name="radio"  id="repeat4" value="4hour"><label for="repeat4">In 4 hour</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" class="remind_poup_data" name="radio"  id="repeat5" value="5hour"><label for="repeat5">In 6 hour</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" class="remind_poup_data" name="radio"  id="repeat6" value="8hour"><label for="repeat6">In 8 hour</label>
																	</div>
																</div>
																<div class="hour-rate1">
																	<div class="checkbox-color checkbox-primary marricls">
																		<input type="checkbox" name="radio"   id="repeat7" class="remind_poup_data" value="tomorrow_morning"><label for="repeat7">Tomorrow Morning </label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" class="remind_poup_data" value="tomorrow_afternoon" name="radio"  id="repeat8"><label for="repeat8">Tomorrow Afternoon </label>
																	</div>
																</div>
																<div class="hour-rate1">
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"  id="repeat9" class="remind_poup_data" value="1day"><label for="repeat9">In 1 day</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"   id="repeat10" class="remind_poup_data" value="2day" ><label for="repeat10"  >In 2 days</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"   id="repeat11" class="remind_poup_data" value="4day" ><label for="repeat11" >In 4 days</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"  id="repeat12" class="remind_poup_data" value="6day"><label for="repeat12">In 6 days</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"  id="repeat13" class="remind_poup_data" value="8day"><label for="repeat13">In 8 days</label>
																	</div>
																</div>
																<div class="hour-rate1">
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"  id="repeat14" class="remind_poup_data" value="1week"><label for="repeat14">In 1 week</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"   id="repeat15" class="remind_poup_data" value="2week"><label for="repeat15">In 2 weeks</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"   id="repeat16" class="remind_poup_data" value="4week"><label for="repeat16">In 4 weeks</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"  id="repeat17" class="remind_poup_data" value="6week"><label for="repeat17">In 6 weeks</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"   id="repeat18" class="remind_poup_data" value="8week"><label for="repeat18">In 8 weeks</label>
																	</div>
																</div>
																<div class="hour-rate1">
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"  id="repeat19" class="remind_poup_data" value="1month"><label for="repeat19">In 1 month</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio" id="repeat20" class="remind_poup_data" value="2month"><label for="repeat20">In 2 months</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio" id="repeat21" class="remind_poup_data" value="4month"><label for="repeat21">In 4 months</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"   id="repeat22" class="remind_poup_data" value="6month"><label for="repeat22">In 6 months</label>
																	</div>
																	<div class="checkbox-color checkbox-primary">
																		<input type="checkbox" name="radio"   id="repeat23" class="remind_poup_data" value="8month"><label for="repeat23">In 8 months</label>
																	</div>
																</div>
															</div>
															<input type="hidden" name="for_reminder_chk_box" id="for_reminder_chk_box">
															<!-- form-group -->
															<div class="picker-category exam-picker">
																<h3>
																At a specific time <span>(Examples: "Wednesday", "3 days from now")</span></h2>
																<div class="specific-picker">
																	<div class="form-group">
																		<label class="col-form-label">Starts:</label>
																		<div class="control-start1">
																			<div class="input-group date">
																				<span class="input-group-addon "><span class="icofont icofont-ui-calendar"></span></span>
																				<input type='text' class="form-control required_check" id='specific_time' name="specific_time" placeholder="dd-mm-yyyy" />
																			</div>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-form-label">Send Time:</label>
																		<div class="input-group control-start1" >
																			<input type="text" name="specific_timepic" id="timepicker2" class="form-control  required_check">
																		</div>
																	</div>
																</div>
																<div class="col-summar">
																	<label>Summary:</label>
																	<!-- <strong>12 weeks, onWednesday at 7:26: PM</strong> -->
																	<p id="summary_val"></p>
																	<input type="hidden" name="reminder_msg" id="reminder_msg">
																</div>
															</div>
														</div>
														<!-- modal-body -->
														<div class="modal-footer">
															<button type="button" name="button"  class="btn btn-default submitBtn_newremind">Confirm</button>
															<a href="javascript:;" data-dismiss="modal">Cancel</a>
														</div>
													</div>
												</div>
											</div>
											<!-- modal close-->
										</div>
										<!-- service exclude popup-->
										<div class="modal fade step_accordion-popup" id="exc_service_popup">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">×</button>
													</div>
													<div class="card-block accordion-block">  
													</div>
													<!-- accordion -->
												</div>
											</div>
										</div>
										<!-- service exclude popup-->
							</form>
							</div>
							</div>
							<!-- close -->
						</div>
					</div>
					<!-- Page body end -->
				</div>
			</div>
			<!-- Main-body end -->
			<div id="styleSelector">
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="import-task" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Import Task</h4>
			</div>
			<div class="modal-body">
				<?php $this->load->view('tasks/import_task'); ?>
			</div>
			<!-- <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div> -->
		</div>
	</div>
</div>
<!-- ajax loader -->
<!-- <style>
	.LoadingImage {
	display : none;
	position : fixed;
	z-index: 100;
	/* background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');*/
	background-color:#666;
	opacity : 0.4;
	background-repeat : no-repeat;
	background-position : center;
	left : 0;
	bottom : 0;
	right : 0;
	top : 0;
	}
	</style> -->
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
	<h1>Warning!!</h1>
	<p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
	<div class="iew-container">
		<ul class="iew-download">
			<li>
				<a href="http://www.google.com/chrome/">
					<img src="assets/images/browser/chrome.png" alt="Chrome">
					<div>Chrome</div>
				</a>
			</li>
			<li>
				<a href="https://www.mozilla.org/en-US/firefox/new/">
					<img src="assets/images/browser/firefox.png" alt="Firefox">
					<div>Firefox</div>
				</a>
			</li>
			<li>
				<a href="http://www.opera.com">
					<img src="assets/images/browser/opera.png" alt="Opera">
					<div>Opera</div>
				</a>
			</li>
			<li>
				<a href="https://www.apple.com/safari/">
					<img src="assets/images/browser/safari.png" alt="Safari">
					<div>Safari</div>
				</a>
			</li>
			<li>
				<a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
					<img src="assets/images/browser/ie.png" alt="">
					<div>IE (9 & above)</div>
				</a>
			</li>
		</ul>
	</div>
	<p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<div class="modal fade recurring-msg common-schedule-msg1" id="import-task12" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Import CSV File Data</h4>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button type="button" name="button"  class="btn btn-default submitBtn">Confirm</button>
				<a href="#" data-dismiss="modal">Cancel</a>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/cdns/bootstrap-tagsinput.css">
<script src="<?php echo base_url(); ?>assets/cdns/bootstrap-tagsinput.min.js"></script>
<!-- 17-09-2018 -->
<!-- end of 17-09-2018 -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/cdns/ckeditor.js"></script>
<script type="text/javascript">
	$('.tags').tagsinput({
	        allowDuplicates: true
	      });
	      
	
	$( document ).ready(function() {
	          CKEDITOR.editorConfig = function (config) {
	          config.language = 'es';
	          config.uiColor = '#fff';
	          config.height = 300;
	          config.toolbarCanCollapse = true;
	          config.toolbarLocation = 'bottom';
	          };
	           CKEDITOR.replace('description'); 
	         
	        } );
</script>
<script src="<?php echo base_url(); ?>assets/js/timepicki.js"></script>
<script>
	$('#timepicker1').timepicki();
	 $('#timepicker2').timepicki();
	 
</script>
<script src="<?php echo base_url(); ?>assets/cdns/jquery-ui.js"></script>
<script type="text/javascript">
	$('.dropdown-sin-4').dropdown({
	   //limitCount: 5,
	   input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
	 });
	  $('.dropdown-sin-5').dropdown({
	   //limitCount: 5,
	   input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
	 });
</script>
<script>
	var Random = Mock.Random;
	var json1 = Mock.mock({
	  "data|10-50": [{
	    name: function () {
	      return Random.name(true)
	    },
	    "id|+1": 1,
	    "disabled|1-2": true,
	    groupName: 'Group Name',
	    "groupId|1-4": 1,
	    "selected": false
	  }]
	});
	
	$('.dropdown-mul-1').dropdown({
	  data: json1.data,
	  limitCount: 40,
	  multipleMode: 'label',
	  choice: function () {
	    // console.log(arguments,this);
	  }
	});
	
	var json2 = Mock.mock({
	  "data|10000-10000": [{
	    name: function () {
	      return Random.name(true)
	    },
	    "id|+1": 1,
	    "disabled": false,
	    groupName: 'Group Name',
	    "groupId|1-4": 1,
	    "selected": false
	  }]
	});
	
	$('.dropdown-mul-2').dropdown({
	  limitCount: 5,
	  searchable: false
	});
	
	$('.dropdown-sin-1').dropdown({
	  readOnly: true,
	  input: '<input type="text" maxLength="20" placeholder="Search">'
	});
	
	$('.dropdown-sin-2').dropdown({
	  //limitCount: 5,
	  input: '<input type="text" maxLength="20" placeholder="Search">'
	});
	
	  $('.dropdown-sin-255').dropdown({
	  //limitCount: 5,
	  input: '<input type="text" maxLength="20" placeholder="Search">'
	});
	
	   $('.dropdown-sin-3').dropdown({
	  //limitCount: 5,
	  input: '<input type="text" maxLength="20" placeholder="Search">'
	});
</script>
<script type="text/javascript">
	$('#timepicker1').timepicker();
	
</script> 
<script src="<?php echo base_url(); ?>assets/cdns/jquery.inputmask.bundle.js"></script>
<script>
	$(document).ready(function(){
	
	
	 $("#related_to_services").change(function(){
	   var service = $(this).val();
	   if(service.length)
	   {   
		   $.ajax({
	       url : "<?php echo base_url()?>Tasksummary/get_services_options",
	       type : "post",
	       data : {"services":service.join()},
	       beforeSend : function(){$(".LoadingImage").show();},
	       success : function (res){
	         $(".LoadingImage").hide();
	         $('.card-block.accordion-block').html(res);
	         $("#exc_service_button").show();
	       }
			}); 			
	   }else
	   {
	     $("#exc_service_button").hide();
	   }
	 });
	
	$('input[id$="timepicker1"]').inputmask({
	 mask: "h:s t\\m",
	 placeholder: "hh:mm am",
	 alias: "datetime",
	 hourFormat: "12"
	});
	$('input[name$="text"]').inputmask({
	 mask: "h:s t\\m",
	 placeholder: "hh:mm am",
	 alias: "datetime",
	 hourFormat: "12"
	});
	   // $('#timepicker1').timepicker();
	  $("#txtFromDate").datepicker({
	      dateFormat: 'dd-mm-yy',
	      changeMonth: true,
	     changeYear: true,
	      numberOfMonths: 1,
	      minDate:0,
	      onSelect: function(selected) {
	        $("#txtToDate").datepicker("option","minDate", selected)
	      }
	  });
	  $("#txtToDate").datepicker({ 
	      dateFormat: 'dd-mm-yy',
	      changeMonth: true,
	     changeYear: true,
	      numberOfMonths: 1,
	      minDate:0,
	      onSelect: function(selected) {
	         $("#txtFromDate").datepicker("option","maxDate", selected)
	      }
	  });  
	
	});
	
	
	
	
	  $( document ).ready(function() {
	  
	      //var date = $('#modalFromDate').datepicker({ dateFormat: 'dd-mm-yy' }).val();
	      
	      $('#specific_time').datepicker({ dateFormat: 'dd-mm-yy',minDate:0, changeMonth: true,
	     changeYear: true, }).val();
	     
	      // Multiple swithces
	      var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
	
	      elem.forEach(function(html) {
	          var switchery = new Switchery(html, {
	              color: '#1abc9c',
	              jackColor: '#fff',
	              size: 'small'
	          });
	      });
	
	      $('#accordion_close').on('click', function(){
	              $('#accordion').slideToggle(300);
	              $(this).toggleClass('accordion_down');
	      });
	
	
	$("#repeats").change(function(){
	
	var vals=$(this).val();
	if(vals=='week'){
	
	  $('#weekly').html('<div class="form-group  days-list floating_set"><label class="label-column1 col-form-label">On</label><div class="label-column2"><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox dayss"  name="days[]" value="Sunday" id="day-check01"><label for="day-check01" class="border-checkbox-label">Sun</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Monday" id="day-check02"><label for="day-check02" class="border-checkbox-label">Mon</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Tuesday" id="day-check03"><label for="day-check03" class="border-checkbox-label">Tue</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Wednesday" id="day-check04"><label for="day-check04" class="border-checkbox-label">Wed</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Thursday" id="day-check05"><label for="day-check05" class="border-checkbox-label">Thur</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Friday" id="day-check06"><label for="day-check06" class="border-checkbox-label">Fri</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox dayss" name="days[]" value="Saturday" id="day-check07"><label for="day-check07" class="border-checkbox-label">Sat</label></div></div></div>');
	
	}/*else if(vals=='month'){
	  
	  $('#weekly').html('<div class="form-group  every-week01 floating_set"><label class="label-column1  col-form-label">Every Date</label><input type="text" class="dob_picker"  name="every_date" id="testtest" placeholder="dd-mm-yyyy"></div>');
	   $("#testtest").datepicker();
	 $('.dob_picker').datepicker({ dateFormat: 'dd-mm-yy',minDate:0, changeMonth: true,
	     changeYear: true, });
	}*/
	else
	{
	 $('#weekly').html('');
	}
	
	/* else if(vals=='day'){
	  $('#weekly').html('<div class="form-group  every-week01 floating_set"><label class="label-column1  col-form-label">Every</label><div class="label-column2"><input type="number" class="form-control" name="every" placeholder="" value=""><p>day</p></div></div>');
	  
	} else if(vals=='month'){
	  
	  $('#weekly').html('<div class="form-group  every-week01 floating_set"><label class="label-column1  col-form-label">Every</label><div class="label-column2"><input type="number" class="form-control" name="every" placeholder="" value=""><p>month</p></div><div class="col-sm-6 form-group common-repeats1 test-test"><div class="label-column2 input-group date"> <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> </div> </div></div></div>');
	  // $("#testtest").datepicker();<input type="text" class="dob_picker"  name="every_date" id="testtest" placeholder="dd-mm-yyyy"> 
	 $('.dob_picker').datepicker({ dateFormat: 'dd-mm-yy',minDate:0, changeMonth: true,
	     changeYear: true, });
	} else if(vals=='year'){
	  $('#weekly').html('<div class="form-group  every-week01 floating_set"><label class="label-column1  col-form-label">Every</label><div class="label-column2"><input type="number" class="form-control" name="every" placeholder="" value=""><p>year</p></div></div>');
	}*/
	
	});
	
	
	
	
	$( document ).ready(function() {
	 $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy',minDate:0, changeMonth: true,
	     changeYear: true,  }).val();
	//$(document).on('change','.othercus',function(e){
	});
	
	
	
	    $('.submitBtn_new').on('click', function(event){
	
	     event.preventDefault();
	       var empty_flds1 = 0;
	       //alert($(".required1").length);
	       $(".required1").each(
	       function() 
	       {       
	         if(!$.trim($(this).val())) 
	         {    
	           empty_flds1++;
	           //var input_id=$(this).attr('id');
	           // console.log(input_id);
	           $(this).css('border', '1px solid red');
	         }
	         else 
	         {
	           $(this).css('border', '1px solid rgba(0,0,0,.15)');
	         } 
	       }
	       );
	
	       //console.log(empty_flds1);
	
	
	
	       if(empty_flds1==0){
	      
	      var every=$('#repeats').val(); //select box
	      var day_word=$('.repeats').attr('data-date'); //
	      var date=$('.repeats').attr('data-day');
	      var month=$('.repeats').attr('data-month');
	      var time=$('#timepicker1').val();
	      var word=$("#every").val(); //every
	
	     var  week_repeat = [];
	     $(".dayss").each(
	       function()
	       {
	         if($(this).is(":checked"))
	         {
	           week_repeat.push($(this).val());
	         }
	       }
	     );
	
	     var end_on_summary = '<p>No End Date</p>';
	     var end_type = $("input[name=end]:checked").val();
	     if(end_type == "after")
	     {
	       end_on_summary="<p>End On After Recurring "+ $("#message").val() + " Tasks</p>";
	     }
	     else if(end_type == "on")
	     {
	       var elm = $("#modalendDate");
	       var date_text = elm.attr("data-day")+"-" +elm.attr("data-month")+"-" +elm.attr("data-year");
	       end_on_summary="<p>End On "+date_text +" </p>";
	     }
	
	     if(every=='week')
	     {
	        $('#summary').html('Every'+' '+ word +' '+every+','+'on '+ week_repeat.join() +' at '+time+end_on_summary);
	        $('#summary_new').html('<p>Every'+' '+every+','+'on '+ week_repeat.join() +' at '+time+"</p>"+end_on_summary);
	        $('#recuring_sus_msg').val('Every'+' '+every+','+'on '+ week_repeat.join() +' at '+time+end_on_summary);
	     } 
	     else if(every=='day')
	     {
	       $('#summary').html('Every'+' '+ word +' '+every+','+' at '+time+end_on_summary);
	       $('#summary_new').html('<p>Every'+' '+ word +' '+every+','+' at '+time+"</p>"+end_on_summary);
	       $('#recuring_sus_msg').val('Every'+' '+ word +' '+every+','+' at '+time+end_on_summary);
	     }
	     else if(every=='month')
	     {
	       $('#summary').html('Every'+' '+ word +' '+every+','+'on day '+date+' at '+time+end_on_summary);
	       $('#summary_new').html('<p>Every'+' '+ word +' '+every+','+'on day '+date+' at '+time+"</p>"+end_on_summary);
	       $('#recuring_sus_msg').val('Every'+' '+ word +' '+every+','+'on day '+date+' at '+time+end_on_summary);
	     }
	     else if(every=='year')
	     {
	       $('#summary').html('Every'+' '+ word +' '+every+','+'on '+month+' '+date +' at '+time+end_on_summary);
	       $('#summary_new').html('<p>Every'+' '+ word +' '+every+','+'on '+month+' '+date +' at '+time+"</p>"+end_on_summary);
	       $('#recuring_sus_msg').val('Every'+' '+ word +' '+every+','+'on '+month+' '+date +' at '+time+end_on_summary);
	     }
	
	  $('.dashboard_success_message1').show();
	   setTimeout(function(){ $('.dashboard_success_message1').hide();  
	     $('#myModal').modal('hide');
	      $('.modal-backdrop.show').hide(); }, 1000);
	     // var modalFromDate=$('#modalFromDate').val($.datepicker.formatDate('dd M yy', new Date()));
	
	 }
	          
	      });
	
	
	
	     $('.submitBtn_newremind').on('click', function(event){
	       var empty_flds = 0;
	
	       $(".required_check").each(function() {  
	
	     //  alert($(this).attr('type'));     
	       if(!$.trim($(this).val())) {
	         empty_flds++;
	       //var input_id=$(this).attr('id');
	       // console.log(input_id);
	         $(this).css('border', '1px solid red');
	       }
	       else
	       {
	         $(this).css('border', '1px solid rgba(0,0,0,.15)');
	       }
	
	     });
	       //console.log(empty_flds);
	     if(empty_flds==0)
	     {
	       
	     event.preventDefault();
	
	      var every=$('#specific_time').val();
	      var time=$('#timepicker2').val();
	
	                       var new_array=[];                            
	                         $('input[class="remind_poup_data"]:checked').each(function() {
	                              new_array.push(this.value);
	                           });
	                         if(new_array.length)
	                         {
	                            $('.remider_option_error').html('');
	
	                         var for_reminder_chk = new_array.join(','); 
	                         $.ajax({
	                           url:"<?php echo base_url()?>Tasksummary/remider_summary_task",
	                           type : 'POST',
	                           dataType : 'json',
	                           data : {'date':every,'time':time,'remainder_data':for_reminder_chk},
	                           beforeSend :function()
	                           {
	                             $(".LoadingImage").show();
	                           },
	                           success : function(res)
	                           { $(".LoadingImage").hide();
	                             var text='';
	                             for (x in res) 
	                             {
	                               var data = res[x].split("//");
	                               text += "<p> "+ data[0] + " At " + data[1] +"</p>";
	                             }
	
	                             $('#summary_val').html(text);
	                             $('#summary_new').html(text);
	                             $('#reminder_msg').val(text);
	                           $('.dashboard_success_message1').show();
	                           
	                           setTimeout(function(){ $('.dashboard_success_message1').hide();  
	                             $('#myModalone').modal('hide');
	                              $('.modal-backdrop.show').hide(); }, 1500);
	
	                           }
	                         });
	
	                         
	                         }
	                         else
	                         {
	                           $('.remider_option_error').html("Please Choose at leats one option");
	                         }
	       }         
	      });
	
	 //for recurring Model popup validation
	
	
	  
	  $("#every ,#message").on("keyup",function(){
	   
	   $(this).val( Math.ceil( $(this).val() ) );
	   if($(this).val() < 1)
	   {
	     $(this).addClass("required1");
	   }
	
	  });
	
	$("input[name=end]").change(function()
	{
	
	   if($(this).val()=="after")
	   {
	     $("input[name=message]").addClass("required1");
	   }
	   else if($(this).val()=="on")
	   {
	     $("input[name=sche_on_date]").addClass("required1");
	   }
	   else 
	   {
	     $("input[name=sche_on_date]").removeClass("required1");
	     $("input[name=message]").removeClass("required1");
	     $("input[name=sche_on_date]").attr("style","border:1px solid #ccc !important");
	     $("input[name=message]").attr("style","border:1px solid #ccc !important");
	   }
	});
	
	//for recurring Model popup validation
	/*$('#modalendDate').datepicker({ dateFormat: 'dd-mm-yy',minDate:0, changeMonth: true,
	     changeYear: true, }).val();*/
	
	  $( "#modalFromDate, #modalendDate" ).datepicker({   
	   dateFormat:'dd-mm-yy',
	  	minDate:0,
	 	changeMonth: true,
	     changeYear: true,
	  	onSelect: function(dateText){
	
	      var seldate = $(this).datepicker('getDate');
	      var day = $(this).datepicker('getDate').getDate();
	      seldate = seldate.toDateString();
	      //alert(seldate);
	      seldate = seldate.split(' ');
	      var weekday=new Array();
	          weekday['Mon']="Monday";
	          weekday['Tue']="Tuesday";
	          weekday['Wed']="Wednesday";
	          weekday['Thu']="Thursday";
	          weekday['Fri']="Friday";
	          weekday['Sat']="Saturday";
	          weekday['Sun']="Sunday";
	      var dayOfWeek = weekday[seldate[0]];
	      
	      var elm_id=$(this).attr("id");
	
	   if(elm_id=="modalFromDate")
	   {
	     $('#repeats').attr('data-date',dayOfWeek);
	     $('#repeats').attr('data-day',day);
	     $('#repeats').attr('data-month',seldate[1]);
	     $('.dayss').prop('checked', false);
	     $('.dayss[value='+dayOfWeek+']').prop('checked', true);
	   } 
	   else if(elm_id=="modalendDate")
	   {
	     $(this).attr('data-year',seldate[3]);
	     $(this).attr('data-day',day);
	     $(this).attr('data-month',seldate[1]);
	   }  
	      
	       
	      //$('#summary').html(dayOfWeek);
	  }
	});
	
	
	
	});
	  
</script>
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-36251023-1']);
	_gaq.push(['_setDomainName', 'jqueryscript.net']);
	_gaq.push(['_trackPageview']);
	
	(function() {
	  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="<?php echo base_url(); ?>assets/cdns/jquery.validate.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){    
	  $.validator.setDefaults({
	      ignore: []
	  });
	});
	 $(document).ready(function(){
	  
	    
	
	
	
	
	
	
	
	
	
	
	 $("#add_new_task").validate({
	   
	        ignore: false,
	 
	        /*onfocusout: function(element) {
	         if ( !this.checkable(element)) {
	             this.element(element);
	         }
	     },*/
	    // errorClass: "error text-warning",
	     //validClass: "success text-success",
	     /*highlight: function (element, errorClass) {
	         //alert('em');
	        // $(element).fadeOut(function () {
	            // $(element).fadeIn();
	         //});
	     },*/
	       errorPlacement: function(error, element) {
	              if (element.attr("name") == "company_name" )
	                  error.insertAfter(".dropdown-sin-4");
	                   else if (element.attr("name") == "worker[]" )
	                  error.insertAfter(".dropdown-sin-2");
	                 else if (element.attr("name") == "manager[]" )
	                  error.insertAfter(".dropdown-sin-3"); 
	              
	              else
	                  error.insertAfter(element);
	          },
	                         rules: {
	                        // task_name:{required:true},
	                         subject: {required: true},
	                         company_name: {required: true},
	                         task_status:{required:true},
	                         startdate:{required:true},
	                         enddate:{required:true},
	                         "worker[]": "required",  
	                          "manager[]":"required", 
	                         },
	                         errorElement: "span" , 
	                         errorClass: "field-error",                             
	                          messages: {
	                         task_name:"Required Field",
	                         subject: "Required field",
	                       company_name: "Required field",
	                         task_status:"Required Field",
	                         startdate:"Required Field",
	                         enddate:"Required Field",
	                         "worker[]": "Required Field",
	                          "manager[]": "Required Field",
	                          },
	                          
	 
	                         
	                         submitHandler: function(form) {
	                         	
	
	                            var new_array=[];
	                          $('input[class="remind_poup_data"]:checked').each(function() {
	                               new_array.push(this.value);
	                            });
	
	                          var for_reminder_chk = new_array.join(',');
	                          $('#for_reminder_chk_box').val(for_reminder_chk);
	
	                             var formData = new FormData($("#add_new_task")[0]);
	
	                         
	                        //console.log(formData);return;
	 
	                       //  $(".LoadingImage").show();
	 
	                             $.ajax({
	                                 url: '<?php echo base_url();?>user/add_new_task/',
	                                 type : 'POST',
	                                 data : formData,
	                                 contentType : false,
	                                 processData : false,
	                                   beforeSend: function() {
	                      $(".LoadingImage").show();
	                    },    
	                                 success: function(data) {
	                                   //  aldateert(data);
	                                     if(parseInt(data)){
	                                        //  location.reload(true);
	                                        $(".LoadingImage").hide();
	                                        $(".popup_info_msg").show();
	                                         $(".popup_info_msg .position-alert1").html("Task Added successfully..");
	                                        setTimeout(function(){window.location.replace("<?php echo base_url(); ?>user/task_details/"+data);},1000);                                        
	                                         
	                                     }
	                                 
	                                 },
	                                 error: function() { $('.alert-danger').show();
	                                         $('.alert-success').hide();}
	                             });
	 
	                             return false;
	                         } 
	                          /*invalidHandler: function(e, validator) {
	            if(validator.errorList.length)
	         $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show');
	 
	         }*/
	                          
	                     });
	 
	 
	 });
</script>
<script type="text/javascript">
	function uploadss(){
	$('#inputfile').click();
	}
</script>
<script>  
	$(document).ready(function(){  
	     $('#search_company').keyup(function(){  
	      // alert('text');
	          var query = $(this).val();  
	          if(query != '')  
	          {  
	               $.ajax({  
	                    url:"<?php echo base_url();?>user/search_companies/",  
	                    method:"POST",  
	                    data:{query:query},  
	                    success:function(data)  
	                    {  
	                         $('#searchresult').fadeIn();  
	                         $('#searchresult').html(data);  
	                    }
	               });  
	          }
	          if(query=='')
	          {
	            $('#searchresult').fadeOut();
	          }  
	     });  
	     $(document).on('click', 'li', function(){  
	          $('#search_company').val($(this).text()); 
	          $('#user_id').val($(this).data('id')); 
	          $('#searchresult').fadeOut();  
	     });  
	}); 
	
	
	
	$("#related_to").change(function () {
	     var val = this.value;
	
	     if(val=='projects'){
	
	          $.ajax( {
	
	     type:"post",
	     dataType : "json",
	     url:"<?php echo base_url();?>user/get_projects",
	     processData: false,
	     contentType: false,
	      data:{ 
	       'get':'get'
	     }
	     ,
	     success:function(response)
	     {
	
	
	     var ret='<div id="frame_projects" class="form-group"><label>Projects</label><select name="r_project" class="form-control" id="r_projects" placeholder="select">';
	
	         $.each(response, function (index, value){
	         ret+='<option value='+value.id+'>'+value.project_name+'</option>';
	         });
	
	       ret+='</select><div>';
	
	      $("#base_projects").before(ret);
	     }
	 });
	
	
	     }else{
	     
	     $('#frame_projects').remove();
	     }
	     
	 });
	
</script> 
<script type="text/javascript">
	function readURL(input) {
	 // if (input.files && input.files[0]) {
	 //     var reader = new FileReader();
	
	 //     reader.onload = function (e) {
	 //    //   alert(input.files[0].name);
	 //    $('#preview_image_name').html(input.files[0].name);
	 //         $('#preview_image')
	 //             .attr('src', e.target.result)
	 //             .width(225)
	 //             .height(225);
	 //     };
	
	 //     reader.readAsDataURL(input.files[0]);
	 // }
	   var zz=input.files.length;
	   var fruits = [];
	   var fruits_new = [];
	var i=0
	if(zz>0){
	   for(i=0;i<zz;i++){
	  // alert(i);
	 if (input.files && input.files[i]) {
	   // $('#sample').append('<input type="file" name="attach_file[]" id="sample" value="'+input+'">');
	     var reader = new FileReader();
	 fruits.push(input.files[i].name);
	     fruits_new.push(input.files[i]);
	
	 }
	 }
	}
	var join=fruits.join(',');
	 $('#preview_image_name').html(join);
	}
	
	
	$( document ).ready(function() {
	
	//  $.ajax( {
	
	//     type:"post",
	//     dataType : "json",
	//     url:"<?php //echo base_url();?>user/get_projects",
	//     processData: false,
	//     contentType: false,
	//      data:{ 
	//       'get':'get'
	//     }
	//     ,
	//     success:function(response)
	//     {
	
	
	//     var ret='<div id="frame_projects" class="form-group"><label>Projects</label><select name="r_project" class="form-control" id="r_projects" placeholder="select">';
	
	//         $.each(response, function (index, value){
	//         ret+='<option value='+value.id+'>'+value.project_name+'</option>';
	//         });
	
	//       ret+='</select><div>';
	
	//      $("#base_projects").before(ret);
	//     }
	// });
	
	
	   var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy',minDate:0, changeMonth: true,
	     changeYear: true, }).val();
	
	   // Multiple swithces
	   var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
	
	   elem.forEach(function(html) {
	       var switchery = new Switchery(html, {
	           color: '#1abc9c',
	           jackColor: '#fff',
	           size: 'small'
	       });
	   });
	
	   $('#accordion_close').on('click', function(){
	           $('#accordion').slideToggle(300);
	           $(this).toggleClass('accordion_down');
	   });
	
	   $('.dropdown-sin-25').dropdown({
	  //limitCount: 5,
	  input: '<input type="text" maxLength="20" placeholder="Search">'
	   });
	    $('.dropdown-sin-27').dropdown({
	  //limitCount: 5,
	  input: '<input type="text" maxLength="20" placeholder="Search">'
	   });
	     $('.dropdown-sin-29').dropdown({
	  //limitCount: 5,
	  input: '<input type="text" maxLength="20" placeholder="Search">'
	   });
	   $('.dropdown-sin-31').dropdown({
	  //limitCount: 5,
	  input: '<input type="text" maxLength="20" placeholder="Search">'
	   });
	});
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script type="text/javascript">
	$(document).ready(function(){
	$("#importFrm").validate({    
	       ignore: false,  
	                        rules: {                         
	                        file: {required: true, accept: "csv"},
	                        priority : { required : true },
	                        "company_name" : { required : true },
	                        "worker[]" : { required : true },
	                        "manager[]" : { required : true },
	                        "task_status":{ required : true },                          
	                        },
	                        errorElement: "span" , 
	                        errorClass: "field-error",                             
	                         messages: {                            
	                          file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},
	                          priority: {required: 'Required'},                         
	                          "worker[]": {required: 'Required'},
	                          "manager[]": {required: 'Required'},
	                          'company_name': {required: 'Required'},
	                           'task_status': {required: 'Required'}                           
	                         },
	                        submitHandler: function(form) {
	                            var formData = new FormData($("#importFrm")[0]);                              
	                            $(".LoadingImage").show();  
	                            $.ajax({
	                                url: '<?php echo base_url();?>tasksummary/import_tasks',                                 
	                                type : 'POST',
	                                data : formData,
	                                contentType : false,
	                                processData : false,
	                                success: function(data) 
	                                {               
	                                         $('.alert-success').show();
	                                         $("..alert-success .position-alert1").html("Task Imported Successfully..");                                       
	                                        setTimeout(function(){
	                                          $('.alert-success').hide();
	                                          window.location.replace("<?php echo base_url(); ?>user/task_list/");},1500);  
	                                },
	                                error: function() { $('.alert-danger').show();
	                                        $('.alert-success').hide();}
	                            });
	
	                            return false;
	                        }
	                         
	                    });
	
	
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
	  $("#country").change(function(){
	  var country_id = $(this).val();
	  //alert(country_id);
	
	    $.ajax({
	
	      url:"<?php echo base_url().'Client/state';?>",
	      data:{"country_id":country_id},
	      type:"POST",
	      success:function(data){
	        //alert('hi');
	        $("#state").append(data);
	        
	      }
	
	    });
	  });
	   $("#state").change(function(){
	  var state_id = $(this).val();
	  //alert(country_id);
	
	    $.ajax({
	
	      url:"<?php echo base_url().'Client/city';?>",
	      data:{"state_id":state_id},
	      type:"POST",
	      success:function(data){
	        //alert('hi');
	        $("#city").append(data);
	        
	      }
	
	    });
	  });
	
	
	
	$("#related_to").change(function () {
	     var val = this.value;
	
	     if(val=='projects'){
	
	          $.ajax( {
	
	     type:"post",
	     dataType : "json",
	     url:"<?php echo base_url();?>user/get_projects",
	     processData: false,
	     contentType: false,
	      data:{ 
	       'get':'get'
	     }
	     ,
	     success:function(response)
	     {
	
	
	     var ret='<div id="frame_projects" class="form-group"><label>Projects</label><select name="r_project" class="form-control" id="r_projects" placeholder="select">';
	
	         $.each(response, function (index, value){
	         ret+='<option value='+value.id+'>'+value.project_name+'</option>';
	         });
	
	       ret+='</select><div>';
	
	      $("#base_projects").before(ret);
	     }
	 });
	
	
	     }else{
	     
	     $('#frame_projects').remove();
	     }
	     
	 });
	
	
	
	});
	
	function clear_close(obj)
	{
	var obj=$(obj).closest(".modal-dialog");
	obj.find(':input').each(function() {
	 
	  switch(this.type) {
	      case 'password':
	      case 'select-multiple':
	      case 'select-one':
	      case 'text':
	      case 'textarea':
	          $(this).val('');
	          break;
	      case 'checkbox':
	      case 'radio':
	          this.checked = false;
	  }
	 
	 });
	
	$(obj).find('select').each(function(){$(this).prop('selectedIndex', 0);});
	obj.parent().modal('hide');
	$('.modal-backdrop.show').hide();
	
	}
	
	/** for reset button **/
	 function clear_form_elements(ele) {
	 
	 $(ele).not("#myModal").find(':input').each(function() {
	
	
	console.log($(this).attr('id'));
	 
	  switch(this.type) {
	      case 'password':
	      case 'select-multiple':
	      case 'select-one':
	      case 'text':
	      case 'file':
	        $(this).val("");
	        break;
	      case 'textarea':
	          $(this).val('');
	          break;
	      case 'checkbox':
	      case 'radio':
	          this.checked = false;
	  }
	 
	 });
	
	
	 //alert($(ele).find("file").attr('class'));
	 $("#preview_image_name").html('');
	
	
	 $(ele).not("#myModal").find('select').each(function(){$(this).val([]);});
	
	 
	 $('.dropdown-main li').each(function(){$(this).removeClass('dropdown-chose');});
	 $('.dropdown-chose-list').each(function(){$(this).html('<span class="placeholder">select</span>');});
	
	 //console.log($( "form" ).serialize());
	
	/* $("select option").each(function(){$(this).prop("selected",false);});
	 $('.dropdown-chose-list').each(function(){
	  // alert('zzz');
	   $(this).html('<span class="placeholder">select</span>'); // this is correct but with validation
	 
	  });*/
	 //$(".selectDate").css('display','block');
	 
	 }
	/** end of reset button **/
	
	$(".dropdown-sin-4").find('.dropdown-main > ul > li').click(function(){
	
	     var custom=$(this).attr('data-value');
	     var customclass=$(this).attr('class');
	
	    $('#user_id').val(custom);
	    //for un select
	    $(".dropdown-sin-3 .dropdown-main ul li.dropdown-chose").trigger("click");
	    $(".dropdown-sin-2 .dropdown-main ul li.dropdown-chose").trigger("click");
	    $("#worker option").prop("selected",false);
	    $("#manager option").prop("selected",false);
	    //for un select
	
	    var id=custom;
	    if(id!='')
	    {
	     // alert(id);
	
	      $.ajax({
	        url : "<?php echo base_url()?>user/get_company_assign",
	        type : "POST",
	        data : {"id":id},
	        dataType : "json",        
	        beforeSend : function(){
	          $(".LoadingImage").show();
	        },
	        success: function(data){
	          $(".LoadingImage").hide();  
	
	          var manager = data.manager;
	
	          for(i=0;i < manager.length;i++)
	          {
	            $("#manager option[value="+manager[i]+"]").attr("selected",true);
	            //$(".dropdown-sin-3 .dropdown-main ul li[data-value="+manager[i]+"]").addClass("dropdown-chose");
	            $(".dropdown-sin-3 .dropdown-main ul li[data-value="+manager[i]+"]").trigger("click");
	            //alert("manager-sin"+$(".dropdown-sin-3 .dropdown-main ul li[data-value="+manager[i]+"]").attr("class"));
	          }
	          
	          var assign = data.assignee;
	
	          for(i=0;i < assign.length;i++)
	          {
	            $("#worker option[value="+assign[i]+"]").attr("selected",true);
	            //$(".dropdown-sin-2 .dropdown-main ul li[data-value="+assign[i]+"]").addClass("dropdown-chose");
	            $(".dropdown-sin-2 .dropdown-main ul li[data-value="+assign[i]+"]").trigger("click");
	            //alert("assign-sin"+$(".dropdown-sin-2 .dropdown-main ul li[data-value="+assign[i]+"]").attr("class"));
	          }
	
	
	        }
	    });
	    
	    }   
	});
	$(document).on("click",".save_assign_staff",function(){
	
	//alert('ok');
	var id = $(this).attr("data-id");
	var data = {};
	var countries =$("#workers"+id ).val();
	/* alert($("#workers"+id ).val());
	$.each($(".workers option:selected"), function(){            
	countries.push($(this).val());
	});*/
	data['task_id'] = id;
	data['worker'] = countries;
	$(".LoadingImage").show();
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>user/update_assignees/",
	   data: data,
	   success: function(response) {
	      // alert(response); die();
	      $(".LoadingImage").hide();
	   $('#task_'+id).html(response);
	   $('.task_'+id).html(response);
	      $('.dashboard_success_message1').show();
	     setTimeout(function(){ $('.dashboard_success_message1').hide(); }, 2000);
	   },
	});
	});
	
	
	 $(".save_assign_staff1").click(function(){
	
	  alert('ok');
	    var id = $(this).attr("data-id");
	    var data = {};
	     var countries =$("#workers1"+id ).val();
	    
	    data['task1_id'] = id;
	    data['worker'] = countries;
	      $.ajax({
	             type: "POST",
	             url: "<?php echo base_url();?>user/update_assignees1/",
	             data: data,
	             success: function(response) {
	              // alert(response); die();
	             $('#task1_'+id).html(response);
	             },
	          });
	 });
	
	
	
	
	
	// $(document).on('click','.remind_poup_data',function(){
	
	//   //alert('ok');
	//   $('.spl_date,.spl_time').addClass('required_check');
	// });
	
	 function staff_click(staff){
	  alert($(staff).attr("data-id"));
	 }
	$("#close_info_msg").click(function(){$(".popup_info_msg").hide(); });
	
	$(document).on('click', '.modal .hasDatepicker', function(){
	$('.ui-datepicker').addClass('uidate_visible');
	});
	
	$(document).on('click', '.modal button[data-dismiss="modal"]', function(){
	$('.ui-datepicker').removeClass('uidate_visible');
	});
	
	
	
	$(document).on("change",".services_checkbox",function()
	{ 
	var state = ($(this).is(":checked")) ? true : false ;
	
	 var parent_div = $(this).closest(".service-parent");
	 parent_div.find(".steps_checkbox").prop("checked",state);
	 parent_div.find(".steps_details_checkbox").prop("checked",state); 
	
	}
	);
	
	
	$(document).on("change",".steps_checkbox",function()
	{  
	var state = ($(this).is(":checked")) ? true : false ;
	
	 var parent_div = $(this).closest(".steps-parent");
	 parent_div.find(".steps_details_checkbox").prop("checked",state);
	
	}
	);
	$(document).ready(function(){
	  $( document ).on('change', ".js-small", function(){
	    $(this).parents('.steps-parent').find('.steps-details-parent').hide();
	  });
	})
	$("#exc_service_button").click(function(){
	$('#exc_service_popup').modal("show");
	});
	
	$(document).on("click",".steps_toggle, .steps_details_toggle",function()
	{
	  var child_id = $(this).attr('data-id');
	  
	//   alert($(this).attr('class')+"---"+child_id);
	
	  if($(this).hasClass('steps_toggle'))
	  {
	    $("#steps_gparent"+child_id).toggle();
	  }
	  else 
	  {
	   $("#steps_details_gparent"+child_id).toggle(); 
	  }
	
	});
</script>
</body>
</html></html>