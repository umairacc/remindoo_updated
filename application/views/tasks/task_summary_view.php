<?php $this->load->view('includes/header');
//$role = $this->Common_mdl->getRole($_SESSION['id']);
$role = $this->session->userdata('roleName');
$roleid = $this->session->userdata('roleId');
?>



<div class="leads-section floating_set research-section01">


<div class="management_section floating_set ">
<div class="card-block">



<!-- tab start -->
<div class="tab-content">
<div id="dashboard" class="tab-pane fade in active">

<div class="switch-list01 floating_set switch-border1">
	<div class="pull-left">
		<ul class="news-leads">
		<li><a href="#">NEW TASK</a></li>
		
		<li class="edit-op"><a href="#">SWITCH TO LIST</a></li>
	</ul>
	</div>
	
	<div class="pull-right leads-search1">
		<form>
			<input type="search" name="search" placeholder="Search Tasks">
		</form>	
	</div>
</div> <!-- switch-list01 -->


<div class="lead-summary2 floating_set">		

<div class="lead-summary3 testimonial-group floating_set">
  <div class="row">

	<div class="new-row-data1 col-xs-12 col-sm-4 complet-task-color1">
		<div class="trial-stated-title">
			<h4><i class="fa fa-bars fa-6" aria-hidden="true"></i> Not Started</h4>
		</div>	
			<div class="back-set02">
				<?php
				$n = 0;
				foreach ($notstartedTasks as $task_list_key => $task_list_value) { 
					$n++;
						$staffs = $task_list_value['worker'].','.$task_list_value['manager'];
						$explode_worker=explode(',',$staffs);
					if($role=='Super Admin') {
					 ?>
				<div class="user-leads1">
					<h3> <?php echo ucfirst($task_list_value['subject']);?> </h3>
					<div class="source-google">
						<div class="source-set1">
							<?php foreach ($explode_worker as $userIds => $user_ids) {
							$profilePic = $this->Common_mdl->getUserProfilepic($user_ids);	
							?>
						<img src="<?php echo $profilePic;?>" alt="img">
						<?php } ?>
						</div>
						<div class="source-set1 source-set3 ">
						
							<strong> <i class="fa fa-check-square-o fa-6" aria-hidden="true"></i> 2/3 </strong>
							<strong><i class="fa fa-comments fa-6" aria-hidden="true"></i> 1</strong>
							<strong><i class="fa fa-paperclip fa-6" aria-hidden="true"></i> 0</strong>
						</div>
						<div class="follow-up">
							<a href="#">Wordpress</a>
							<a href="#">todo</a>
						</div>
					</div>	
				</div>
				<?php }else if(($role=='Staff' || $role=='Manager' ) && in_array($_SESSION['id'], $explode_worker)){
					 ?>
				<div class="user-leads1">
					<h3> <?php echo ucfirst($task_list_value['subject']);?> </h3>
					<div class="source-google">
						<div class="source-set1">
							<?php foreach ($explode_worker as $userIds => $user_ids) {
							$profilePic = $this->Common_mdl->getUserProfilepic($user_ids);	
							?>
						<img src="<?php echo $profilePic;?>" alt="img">
						<?php } ?>
						</div>
						<div class="source-set1 source-set3 ">
						
							<strong> <i class="fa fa-check-square-o fa-6" aria-hidden="true"></i> 2/3 </strong>
							<strong><i class="fa fa-comments fa-6" aria-hidden="true"></i> 1</strong>
							<strong><i class="fa fa-paperclip fa-6" aria-hidden="true"></i> 0</strong>
						</div>
						<div class="follow-up">
							<a href="#">Wordpress</a>
							<a href="#">todo</a>
						</div>
					</div>	
				</div>
				<?php 
				}
				else{
					echo "No Records Found";
					break;
				} }if($n=='0'){
					echo 'No Records Found';
				} ?>
			</div>
		</div> <!-- 1tab -->
		
		<div class="new-row-data1 col-xs-12 col-sm-4  complet-task-color2">
		<div class="trial-stated-title">
			<h4><i class="fa fa-bars fa-6" aria-hidden="true"></i> In Progress</h4>
		</div>	
			<div class="back-set02">
				
				<?php 
				$i = 0;
				foreach ($inprogressTasks as $task_list_key => $task_list_value) { 
					$i++;
						$staffs = $task_list_value['worker'].','.$task_list_value['manager'];
						$explode_worker=explode(',',$staffs);
					if($role=='Super Admin') {

					 ?>
				<div class="user-leads1">
					<h3> <?php echo ucfirst($task_list_value['subject']);?> </h3>
					<div class="source-google">
						<div class="source-set1">
							<?php foreach ($explode_worker as $userIds => $user_ids) {
							$profilePic = $this->Common_mdl->getUserProfilepic($user_ids);	
							?>
						<img src="<?php echo $profilePic;?>" alt="img">
						<?php } ?>
						</div>
						<div class="source-set1 source-set3 ">
						
							<strong> <i class="fa fa-check-square-o fa-6" aria-hidden="true"></i> 2/3 </strong>
							<strong><i class="fa fa-comments fa-6" aria-hidden="true"></i> 1</strong>
							<strong><i class="fa fa-paperclip fa-6" aria-hidden="true"></i> 0</strong>
						</div>
						<div class="follow-up">
							<a href="#">Wordpress</a>
							<a href="#">todo</a>
						</div>
					</div>	
				</div>
				<?php }else if(($role=='Staff' || $role=='Manager' ) && in_array($_SESSION['id'], $explode_worker)){
					 ?>
				<div class="user-leads1">
					<h3> <?php echo ucfirst($task_list_value['subject']);?> </h3>
					<div class="source-google">
						<div class="source-set1">
							<?php foreach ($explode_worker as $userIds => $user_ids) {
							$profilePic = $this->Common_mdl->getUserProfilepic($user_ids);	
							?>
						<img src="<?php echo $profilePic;?>" alt="img">
						<?php } ?>
						</div>
						<div class="source-set1 source-set3 ">
						
							<strong> <i class="fa fa-check-square-o fa-6" aria-hidden="true"></i> 2/3 </strong>
							<strong><i class="fa fa-comments fa-6" aria-hidden="true"></i> 1</strong>
							<strong><i class="fa fa-paperclip fa-6" aria-hidden="true"></i> 0</strong>
						</div>
						<div class="follow-up">
							<a href="#">Wordpress</a>
							<a href="#">todo</a>
						</div>
					</div>	
				</div>
				<?php 
				}else{
					echo "No Records Found";
					break;
				} }if($i=='0'){
					echo 'No Records Found';
				} ?>
			</div>
		</div> <!-- 1tab -->
		
		<div class="new-row-data1 col-xs-12 col-sm-4  complet-task-color3">
		<div class="trial-stated-title">
			<h4><i class="fa fa-bars fa-6" aria-hidden="true"></i> Awaiting Feedback</h4>
		</div>	
			<div class="back-set02">
				
				<?php 
				$a = 0;
				foreach ($awaitingTasks as $task_list_key => $task_list_value) { 
					$a++;
						$staffs = $task_list_value['worker'].','.$task_list_value['manager'];
						$explode_worker=explode(',',$staffs);
					if($role=='Super Admin') {
						
					 ?>
				<div class="user-leads1">
					<h3> <?php echo ucfirst($task_list_value['subject']);?> </h3>
					<div class="source-google">
						<div class="source-set1">
							<?php foreach ($explode_worker as $userIds => $user_ids) {
							$profilePic = $this->Common_mdl->getUserProfilepic($user_ids);	
							?>
						<img src="<?php echo $profilePic;?>" alt="img">
						<?php } ?>
						</div>
						<div class="source-set1 source-set3 ">
						
							<strong> <i class="fa fa-check-square-o fa-6" aria-hidden="true"></i> 2/3 </strong>
							<strong><i class="fa fa-comments fa-6" aria-hidden="true"></i> 1</strong>
							<strong><i class="fa fa-paperclip fa-6" aria-hidden="true"></i> 0</strong>
						</div>
						<div class="follow-up">
							<a href="#">Wordpress</a>
							<a href="#">todo</a>
						</div>
					</div>	
				</div>
				<?php }else if(($role=='Staff' || $role=='Manager' ) && in_array($_SESSION['id'], $explode_worker)){
					 ?>
				<div class="user-leads1">
					<h3> <?php echo ucfirst($task_list_value['subject']);?> </h3>
					<div class="source-google">
						<div class="source-set1">
							<?php foreach ($explode_worker as $userIds => $user_ids) {
							$profilePic = $this->Common_mdl->getUserProfilepic($user_ids);	
							?>
						<img src="<?php echo $profilePic;?>" alt="img">
						<?php } ?>
						</div>
						<div class="source-set1 source-set3 ">
						
							<strong> <i class="fa fa-check-square-o fa-6" aria-hidden="true"></i> 2/3 </strong>
							<strong><i class="fa fa-comments fa-6" aria-hidden="true"></i> 1</strong>
							<strong><i class="fa fa-paperclip fa-6" aria-hidden="true"></i> 0</strong>
						</div>
						<div class="follow-up">
							<a href="#">Wordpress</a>
							<a href="#">todo</a>
						</div>
					</div>	
				</div>
				<?php 
				}else{
					echo "No Records Found";
					break;
				} }if($a==0){
					echo "No Records Found";
				} ?>
			</div>
		</div> <!-- 1tab -->
		
		<div class="new-row-data1 col-xs-12 col-sm-4  complet-task-color4">
		<div class="trial-stated-title">
			<h4><i class="fa fa-bars fa-6" aria-hidden="true"></i>Testing </h4>
		</div>	
			<div class="back-set02">
				
				<?php 
				$t = 0;
				foreach ($testingTasks as $task_list_key => $task_list_value) {
				$t++; 
						$staffs = $task_list_value['worker'].','.$task_list_value['manager'];
						$explode_worker=explode(',',$staffs);
					if($role=='Super Admin') {
						
					 ?>
				<div class="user-leads1">
					<h3> <?php echo ucfirst($task_list_value['subject']);?> </h3>
					<div class="source-google">
						<div class="source-set1">
							<?php foreach ($explode_worker as $userIds => $user_ids) {
							$profilePic = $this->Common_mdl->getUserProfilepic($user_ids);	
							?>
						<img src="<?php echo $profilePic;?>" alt="img">
						<?php } ?>
						</div>
						<div class="source-set1 source-set3 ">
						
							<strong> <i class="fa fa-check-square-o fa-6" aria-hidden="true"></i> 2/3 </strong>
							<strong><i class="fa fa-comments fa-6" aria-hidden="true"></i> 1</strong>
							<strong><i class="fa fa-paperclip fa-6" aria-hidden="true"></i> 0</strong>
						</div>
						<div class="follow-up">
							<a href="#">Wordpress</a>
							<a href="#">todo</a>
						</div>
					</div>	
				</div>
				<?php }else if( ($role=='Staff' || $role=='Manager' ) && in_array($_SESSION['id'], $explode_worker)){
					 ?>
				<div class="user-leads1">
					<h3> <?php echo ucfirst($task_list_value['subject']);?> </h3>
					<div class="source-google">
						<div class="source-set1">
							<?php foreach ($explode_worker as $userIds => $user_ids) {
							$profilePic = $this->Common_mdl->getUserProfilepic($user_ids);	
							?>
						<img src="<?php echo $profilePic;?>" alt="img">
						<?php } ?>
						</div>
						<div class="source-set1 source-set3 ">
						
							<strong> <i class="fa fa-check-square-o fa-6" aria-hidden="true"></i> 2/3 </strong>
							<strong><i class="fa fa-comments fa-6" aria-hidden="true"></i> 1</strong>
							<strong><i class="fa fa-paperclip fa-6" aria-hidden="true"></i> 0</strong>
						</div>
						<div class="follow-up">
							<a href="#">Wordpress</a>
							<a href="#">todo</a>
						</div>
					</div>	
				</div>
				<?php 
				}else{
					echo "No Records Found";
					break;
				} }if($t=='0'){
					echo "No Records Found";
				} ?>
			</div>
		</div> <!-- 1tab -->
		
		<div class="new-row-data1 col-xs-12 col-sm-4  complet-task-color5">
		<div class="trial-stated-title">
			<h4><i class="fa fa-bars fa-6" aria-hidden="true"></i> Complete</h4>
		</div>	
			<div class="back-set02">
				
				<?php 
				$c = 0;
				foreach ($completeTasks as $task_list_key => $task_list_value) { 
						$staffs = $task_list_value['worker'].','.$task_list_value['manager'];
						$explode_worker=explode(',',$staffs);
					if($role=='Super Admin') {
						
					 ?>
				<div class="user-leads1">
					<h3> <?php echo ucfirst($task_list_value['subject']);?> </h3>
					<div class="source-google">
						<div class="source-set1">
							<?php foreach ($explode_worker as $userIds => $user_ids) {
							$profilePic = $this->Common_mdl->getUserProfilepic($user_ids);	
							?>
						<img src="<?php echo $profilePic;?>" alt="img">
						<?php } ?>
						</div>
						<div class="source-set1 source-set3 ">
						
							<strong> <i class="fa fa-check-square-o fa-6" aria-hidden="true"></i> 2/3 </strong>
							<strong><i class="fa fa-comments fa-6" aria-hidden="true"></i> 1</strong>
							<strong><i class="fa fa-paperclip fa-6" aria-hidden="true"></i> 0</strong>
						</div>
						<div class="follow-up">
							<a href="#">Wordpress</a>
							<a href="#">todo</a>
						</div>
					</div>	
				</div>
				<?php }else if(($role=='Staff' || $role=='Manager' ) && in_array($_SESSION['id'], $explode_worker)){
					 ?>
				<div class="user-leads1">
					<h3> <?php echo ucfirst($task_list_value['subject']);?> </h3>
					<div class="source-google">
						<div class="source-set1">
							<?php foreach ($explode_worker as $userIds => $user_ids) {
							$profilePic = $this->Common_mdl->getUserProfilepic($user_ids);	
							?>
						<img src="<?php echo $profilePic;?>" alt="img">
						<?php } ?>
						</div>
						<div class="source-set1 source-set3 ">
						
							<strong> <i class="fa fa-check-square-o fa-6" aria-hidden="true"></i> 2/3 </strong>
							<strong><i class="fa fa-comments fa-6" aria-hidden="true"></i> 1</strong>
							<strong><i class="fa fa-paperclip fa-6" aria-hidden="true"></i> 0</strong>
						</div>
						<div class="follow-up">
							<a href="#">Wordpress</a>
							<a href="#">todo</a>
						</div>
					</div>	
				</div>
				<?php 
				}else{
					echo "No Records Found";
					break;
				} } if($c=='0'){
					echo "No Records Found";
				}?>
			</div>
		</div> <!-- 1tab -->
		</div>
		
		
		
		
		
	</div>

						
			


</div> <!-- lead-summary2 -->
					


</div> <!-- dashboard -->


</div> <!-- tab-content -->
</div>


</div> 
</div> <!-- leads-section -->


<?php $this->load->view('includes/footer');?>