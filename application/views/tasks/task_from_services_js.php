<script>
// Task From service js
$(document).ready(function(){

$('.dropdown-sin-services').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
    });
$('.dropdown-sin-company_name').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
    });
$('.dropdown-sin-worker').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
    });
$('.dropdown-sin-manager').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
    });

$(document).on("change",".services_checkbox",function()
{ 
  var state = ($(this).is(":checked")) ? true : false ;
  
   var parent_div = $(this).closest(".service-parent");
   parent_div.find(".steps_checkbox").prop("checked",state);
   parent_div.find(".steps_details_checkbox").prop("checked",state); 
  
}
);


$(document).on("change",".steps_checkbox",function()
{  
  var state = ($(this).is(":checked")) ? true : false ;  
   var parent_div = $(this).closest(".steps-parent");
   parent_div.find(".steps_details_checkbox").prop("checked",state);
  
}
);

$(document).on("change",".steps_details_checkbox",function()
{  
  var state = ($(this).is(":checked")) ? true : false ;  
  var parent_div = $(this).closest(".steps-parent");
  var i=0;
  parent_div.find('.steps_details_checkbox').each(function()
  {
    if($(this).is(":checked"))i++;
  });
  if(i)
  {
    parent_div.find(".steps_checkbox").prop("checked",true);
  }
  else
  {
    parent_div.find(".steps_checkbox").prop("checked",false);
  }

}
);

$("#exc_service_popup").find('.close').click(function(){
  $("#exc_service_popup").modal("hide");
})



$("#exc_service_button").click(function(e){
  e.preventDefault();
 $('#exc_service_popup').modal("show");
});

$(document).on("click",".steps_toggle, .steps_details_toggle",function()
  { 
    var child_id = $(this).attr('data-id');
    
 //   alert($(this).attr('class')+"---"+child_id);

    if($(this).hasClass('steps_toggle'))
    {
      $("#steps_gparent"+child_id).toggle();
    }
    else 
    {
     $("#steps_details_gparent"+child_id).toggle(); 
    }

  });



 $("select[id='services']").change(function(){
      var service = $(this).val();
      service = service.join();
      if(service.length)
      {  
          $.ajax({
          url : "<?php echo base_url()?>Tasksummary/get_services_options",
          type : "post",
          data : {"services":service,'action':'import_service'},
          //beforeSend : function(){$(".LoadingImage").show();},
          success : function (res){
            //$(".LoadingImage").hide();
            $('.card-block.accordion-block').html(res);
            $("#exc_service_button").show();
          }
           });          
      }else
      {
        $("#exc_service_button").hide();
      }
    });




$("#servicesTaskForm").validate({
  ignore: false, 
    rules :  {    "worker[]" : {required:true},  
                  "manager[]" :{required:true},
                  "company_name" : {required:true},
                  "services[]" : {required:true},                  
                  "task_status" : {required:true}
              },
               errorPlacement: function(error, elm) 
               { 
                var elm_name = elm.attr('name');
                if( elm_name == "inc_exc_steps_details[]")
                {
                   $('#exc_service_popup').modal("show");

                  // alert($('.card-block.accordion-block').find('.inc_exc_error').attr('class') +"--"+ $('.card-block.accordion-block').find('.inc_exc_error').length);

                   $('.card-block.accordion-block').find('.inc_exc_error').show();
                  // alert($('.card-block.accordion-block').find('.inc_exc_error').attr("style"));
                }
                else if( elm_name == "services[]" || elm_name == "company_name" || elm_name == "worker[]" || elm_name == "manager[]")
                {
                  error.insertAfter(elm.parent());
                }
             
                
              },
              
    submitHandler : function(form)
    {

     // alert($("#servicesTaskForm").find("name='manager[]'").val());return;
      var formData = new FormData($("#servicesTaskForm")[0]);
      $.ajax({
                                   url: '<?php echo base_url();?>user/taskFromServices/',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                     beforeSend: function() {
                        $(".LoadingImage").show();
                      },    
                                   success: function(data) {
                                     //  aldateert(data);
                                       if(parseInt(data)){
                                          //  location.reload(true);
                                          $(".LoadingImage").hide();
                                          $(".popup_info_msg").show();
                                           $(".popup_info_msg .position-alert1").html("Service Tasks Imported successfully..");
                                          setTimeout(function(){window.location.href ="<?php echo base_url(); ?>user/task_list";},1000);                                        
                                           
                                       }
                                   
                                   },
                                   error: function() { $('.alert-danger').show();
                                           $('.alert-success').hide();}
                               });
    }              
   });


});
// Task From service js

</script>