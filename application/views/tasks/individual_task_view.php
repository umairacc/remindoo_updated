<?php $this->load->view('includes/header');?>

<link href="<?php echo  base_url()?>assets/css/jquery.lineProgressbar.css" rel="stylesheet" type="text/css">
<link href="<?php echo  base_url()?>assets/css/c3.css" rel="stylesheet" type="text/css">

<div class="leads-section floating_set">

<div class="col-sm-8 project-task-left">
<div class="individual-user1 floating_set">

<h4>Project Owner: Manager Govan | Start: 01/Feb/2016 | Due on:01/Oct/2016 | <i class="fa fa-file-pdf-o fa-6" aria-hidden="true"></i></h4>

<div class="project-user1 floating_set">

	<div class="project-task1 col-sm-4">
		<div class="view-status-data">
			<div class="project-bugs">
				<span>12</span>
			</div>
		<div class="project-employee"> <i class="fa fa-user-o fa-6" aria-hidden="true"></i></div>
		
		</div>
		<p>Project Users</p>		
	</div>
	
	<div class="project-task1 col-sm-4">
		<div class="view-status-data">
			<div class="project-bugs">
				<span>12</span>
			</div>
		<div class="project-employee"> <i class="fa fa-calendar-check-o fa-6" aria-hidden="true"></i></div>
		
		</div>
		<p>Project Task</p>		
	</div>
	
	<div class="project-task1 col-sm-4">
		<div class="view-status-data">
			<div class="project-bugs">
				<span>12</span>
			</div>
		<div class="project-employee"> <i class="fa fa-bug fa-6" aria-hidden="true"></i></div>
		
		</div>
		<p>Project Bug</p>		
	</div>
	
	
	<div class="project-task1 col-sm-4">
		<div class="view-status-data">
			<div class="project-bugs">
				<span>12</span>
			</div>
		<div class="project-employee"> <i class="fa fa-bug fa-6" aria-hidden="true"></i></div>
		
		</div>
		<p>Project Bug</p>		
	</div>
	<div class="project-task1 col-sm-4">
		<div class="view-status-data">
			<div class="project-bugs">
				<span>12</span>
			</div>
		<div class="project-employee"> <i class="fa fa-bug fa-6" aria-hidden="true"></i></div>
		
		</div>
		<p>Project Bug</p>		
	</div>
	</div>
	
	<div class="project-status-tasks floating_set">
		<div class="status-title floating_set">
			<h4><span>Egal's Eye View Of Project Status</span></h4>
		</div>
		
		<div class="progress-project floating_set">
			 <div class="col-sm-6 progress-percent color-progress1">
			 <div id="task1"></div>
			 <div class="tasks-percent">Tasks</div>
			 </div>
			 
			 <div class="col-sm-6 progress-percent color-progress2">
			 <div id="task2"></div>
			 <div class="tasks-percent">TaskLists</div>
			 </div>
			 
			 <div class="col-sm-6 progress-percent color-progress3">
			 <div id="task3"></div>
			 <div class="tasks-percent">Millestones</div>
			 </div>
			 
			 <div class="col-sm-6 progress-percent color-progress4">
			 <div id="task4"></div>
			 <div class="tasks-percent">Bugs</div>
			 </div>
		</div>	
	</div>	
</div>

	<div class="chat-setting01 floating_set">
		<div class="task-progress1 col-sm-6">	
			<div class="bug-progress">
			<div class="progress-title1 floating_set">
				<span class="pull-left">TASK PROGRESS</span>
				<a href="#" class="pull-right"><i class="fa fa-refresh fa-6" aria-hidden="true"></i></a>
			</div>
			
			<div class="open-closed1 floating_set">
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> open</span>
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> closed</span>
			</div>
			
			<div class="progress-section1 floating_set">
						<div class="card-block">
						<!-- <div id="chart3"></div> -->
						<div id="chartContainer1" style="width: 100%; height: 300px"></div> 
						</div>
			</div>
			</div>
		</div>
		
		<div class="task-progress1 col-sm-6">	
		<div class="bug-progress">
			<div class="progress-title1 floating_set">
				<span class="pull-left">BUG PROGRESS</span>
				<a href="#" class="pull-right"><i class="fa fa-refresh fa-6" aria-hidden="true"></i></a>
			</div>
			<div class="open-closed1 floating_set">
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i>Critical</span>
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> High</span>
				
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Medium</span>
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Low</span>
				
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Resolved</span>
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> closed</span>
				
			</div>
			
			<div class="progress-section1 floating_set"> 
					<div class="card-block">
						<!-- <div id="chart3"></div> -->
						<div id="chartContainer" style="width: 100%; height: 300px"></div> 
						</div>
			</div></div>
		</div>
		
	</div>	
				
	


</div> <!-- col-sm-8 -->

			<div class="col-sm-4 project-task-right">

			<div class="project-details">
				<span class="pro-head">project details</span>
				<div class="search-boxcon">
					<input type="text" class="s-box" placeholder="search project type, bugs, user">
					<i class="fa fa-search" aria-hidden="true"></i>
				</div>
				<div class="overall-progress">
					<span class="pro-head">project overall progress</span>
					<div id="task5"></div>
				</div>

				<div class="project-overview">
					<span class="pro-head">project overview</span>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
					<div class="sub-tips">
						<span class="pro-head">loreum ipsum dolor sit</span>
						<ul>
							<li>1. Lorem Ipsum</li>
							<li>2. Lorem Ipsum</li>
							<li>3. Lorem Ipsum</li>
							<li>4. Lorem Ipsum</li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="project-chat1">
				<h4>PROJECT USERS</h4>
				<div class="project-chat2">
				
					<div class="user-chat1">
						<img src="http://remindoo.org/CRMTool/uploads/1514958997.jpg" class="img-radius" alt="User-Profile-Image">
						<div class="user-name-chat online-user">
							<span>Margaret Govan</span>
							<strong><i class="fa fa-circle fa-6" aria-hidden="true"></i> Project Owner</strong>
						</div>
						</div>
						
						<div class="user-chat1">
						<img src="http://remindoo.org/CRMTool/uploads/1514958997.jpg" class="img-radius" alt="User-Profile-Image">
						<div class="user-name-chat offline-user">
							<span>Margaret Govan</span>
							<strong> <i class="fa fa-circle fa-6" aria-hidden="true"></i> Project Owner</strong>
						</div>
						</div>
						
						<div class="user-chat1">
						<img src="http://remindoo.org/CRMTool/uploads/1514958997.jpg" class="img-radius" alt="User-Profile-Image">
						<div class="user-name-chat online-user">
							<span>Margaret Govan</span>
							<strong><i class="fa fa-circle fa-6" aria-hidden="true"></i> Project Owner</strong>
						</div>
						</div>
						
						<div class="user-chat1">
						<img src="http://remindoo.org/CRMTool/uploads/1514958997.jpg" class="img-radius" alt="User-Profile-Image">
						<div class="user-name-chat offline-user">
							<span>Margaret Govan</span>
							<strong> <i class="fa fa-circle fa-6" aria-hidden="true"></i> Project Owner</strong>
						</div>
						</div>
						
					</div>	
				</div>		
					</div>

			</div><!-- col-sm-4 -->


</div> <!-- leads-section -->



<?php $this->load->view('includes/footer');?>

<script src="<?php echo  base_url()?>assets/js/jquery.lineProgressbar.js"></script>
<script src="<?php echo  base_url()?>assets/js/c3-custom-chart.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/cdns/jquery.canvasjs.min.js"></script> 

<script>
$('#task1').LineProgressbar({
percentage:82,
radius: '3px',
height: '20px',
fillBackgroundColor:'#2dcee3'
});
$('#task2').LineProgressbar({
percentage:78,
radius: '3px',
height: '20px',
fillBackgroundColor: '#16d39a'
});
$('#task3').LineProgressbar({
percentage:68,
radius: '3px',
height: '20px',
fillBackgroundColor: '#ffa87d'
});
$('#task4').LineProgressbar({
percentage:62,
radius: '3px',
height: '20px',
fillBackgroundColor: '#ff7588'
});
$('#task5').LineProgressbar({
percentage:72,
radius: '3px',
height: '5px',
fillBackgroundColor: '#ff7588'
});
</script>
<script type="text/javascript"> 
window.onload = function() { 
	$("#chartContainer").CanvasJSChart({ 
		title: { 
			fontSize: 24
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		data: [ 
		{ 
			type: "pie", 
			toolTipContent: "{label} <br/> {y} %", 
			indexLabel: "{y} %", 
			dataPoints: [ 
				{ label: "Critical",  y: 30.3, legendText: "Critical"}, 
				{ label: "High",    y: 19.1, legendText: "High"  }, 
				{ label: "Medium",   y: 4.0,  legendText: "Medium" }, 
				{ label: "Low",       y: 3.8,  legendText: "Low"}, 
				{ label: "Resolved",   y: 3.2,  legendText: "Resolved" }, 
				{ label: "Closed",   y: 39.6, legendText: "Closed" } 
			] 
		} 
		] 
	}); 
	
	$("#chartContainer1").CanvasJSChart({ 
		title: { 
			fontSize: 24
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		data: [ 
		{ 
			type: "pie", 
			toolTipContent: "{label} <br/> {y} %", 
			indexLabel: "{y} %", 
			dataPoints: [ 
				{ label: "Open",  y: 30.3, legendText: "Open"}, 
				{ label: "Closed",    y: 19.1, legendText: "Closed"  }, 
				
			] 
		} 
		] 
	}); 
} 
</script> 