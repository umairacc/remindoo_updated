<?php

$team = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();
$department=$this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();

?>
<div class="pcoded-content">
  <div class="pcoded-inner-content">
    <!-- Main-body start -->
    <div class="main-body">
      <div class="page-wrapper">
        <!-- Page body start -->
        <div class="page-body">
          <div class="row">
            <!--start-->
            <div class="title_page01 fileupload_01 floating tskl">
            
              <div class="panel panel-default card import-tsk1">
           
        <!--         <div class="download-sample">
                <a href='<?= base_url() ?>uploads/csv/Task_import.csv'>Download Sample CSV File</a></div> -->
                <div class="panel-body">
                  <form action="" method="post" enctype="multipart/form-data" id="servicesTaskForm">


                   
                      <div class="form-group">
                                       <label>Services</label>
                                        <div class="dropdown-sin-services">
                                       <select class="form-control" name="services[]" id='services' placeholder="Select any Services" multiple="">     
                                          <?php 
                                  $settings_val=$this->db->query('select * from service_lists where id in(1,2,3,4,5,6,7,8,9,10,11,12)')->result_array();
                                 
                                  foreach($settings_val as $set_key => $set_value)
                                   {
                                    ?>
                                    <option value="<?php echo $set_value['services_subnames'];?>" ><?php echo trim($set_value['services_subnames']); ?></option>
                                                                          
                                    
                                    <?php
                                    }
                                      ?>
                                       </select>
                                       </div>
                                    </div>
                    <div class="form-group">
                      <label>Choose Client/Firm</label>
                        <div class="dropdown-sin-company_name">
                        <select name="company_name" id="company_name"  placeholder="select Company">
                                             <!-- <input type="text" name="company" id="company"> -->
                                             <option value="">Select Company</option> 
                                            
                                             <?php 
                                                $user_ids  = $this->db->query("SELECT GROUP_CONCAT(id) as ids FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$_SESSION['id']." order by id DESC")->row_array();
                                
                                  if(!empty($user_ids['ids']))
                                  {
                                 
                                  $results = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (".$user_ids['ids'].") order by id desc ")->result_array();
                                }
                               else
                               {

                                  $results=0;
                                  $results = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id=".$_SESSION['id']." order by id desc ")->result_array();                                          
                                }  
                                                if(count($results) > 0)  
                                                {  
                                                   foreach($results as $key => $val)
                                                   {  ?>
                                             <option value="<?php echo $val['id'];?>"  <?php if(isset($records['company_id'])){     
                                                if($records['company_id']==$val['id']){ ?>
                                                selected="selected"
                                                <?php } } ?>
                                                ><?php echo $val["crm_company_name"];?></option>
                                             <?php
                                                }  
                                                }  ?>
                                          </select>

                        </div>
                      </div>
                        <div class="form-group">
                      <label>Assign to(worker)</label>
                      <div class="dropdown-sin-worker lead-form-st">
                        <select  name="worker[]" id="worker" multiple placeholder="Select">
                          <?php
                          /*  if(isset($staff_form) && $staff_form!='') {
                            
                            foreach($staff_form as $value) {
                            ?>
                          <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
                          <?php
                            }
                            } */

                                    if(isset($staff_form) && $staff_form!='') 
                                    { ?>
                                      <option disabled>Staff</option> 
                                      <?php 
                                       $i=0;
                                     foreach($staff_form as $value) 
                                     {
                                    ?>
                                 <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
                                 <?php
                                 $i++;
                                    }
                                    }
                               if(isset($team) && $team!='') 
                                    { ?>
                                      <option disabled>Team</option> 
                                      <?php 
                                       $i=0;
                                     foreach($team as $value) 
                                     {
                                    ?>
                                 <option value="tm_<?php echo $value['id']; ?>" ><?php echo $value["team"]; ?></option>
                                 <?php
                                 $i++;
                                    }
                                    }
                                  if(isset($department) && $department!='') 
                                    { ?>
                                      <option disabled>Department</option> 
                                      <?php 
                                       $i=0;
                                     foreach($department as $value) 
                                     {
                                    ?>
                                 <option value="de_<?php echo $value['id']; ?>"><?php echo $value["new_dept"]; ?></option>
                                 <?php
                                 $i++;
                                    }
                                    }
                                 
                            ?>
                        </select>
                      </div>
                    </div>
                      <div class="form-group">
                        <label>Notify to(Manager)</label>
                        <div class="dropdown-sin-manager">
                          <select style="display:none" name="manager[]" id="manager" multiple placeholder="Select">
                            <?php foreach($manager as $managers) {
                              ?>
                            <option value="<?php echo $managers['id']?>"><?php echo $managers['crm_name'];?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                                       <label>Task status*</label>
                                       <select name="task_status" class="form-control" id="task_status">
                                          <option value="notstarted">Not started</option>
                                          <option value="inprogress">In Progress</option>
                                          <option value="awaiting">Awaiting Feedback</option>
                                          <!-- <option value="testing">Testing</option> -->
                                          <option value="complete">Complete</option>
         <option value="archive" <?php if(isset($_SESSION['sta_tus']) &&  $_SESSION['sta_tus']=='archive'){ ?> selected="selected" <?php } ?> >Archive</option>
                                       </select>
                                    </div>
                    <div class="form-group">
                      <label>Prioriety</label>
                      <select name="priority" class="form-control" id="priority">
                        <option value="low">Low</option>
                        <option value="medium">Medium</option>
                        <option value="high">High</option>
                        <option value="super urgent">Super Urgent</option>
                      </select>
                    </div>



                     <div class="form-group widthfifty even tag-design">
                                    <h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
                                       <input type="text" value="" name="tags" id="tag" class="tags" />
                                    </div>

 <div class="upload_input09 custom_uploadtop">
                    <label>Attach File</label>

                       <div class="custom_upload">
                        <label for="file">Attachment</label>
                      <input type="file" name="attach_file[]" id="servicesTaskFile" multiple="" />
                    </div>
                    </div>


                    
                    <!-- <div id="base_projects"></div> -->
                    <div class="bothcls">
                      <button id='exc_service_button' style="display: none;" class="btn btn-primary exc_service_button">Exclude Steps</button>
                      <input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT">
                    </div>
                      
                    
                    <!-- service exclude popup-->
                    <div class="modal fade step_accordion-popup" id="exc_service_popup">
                      <div class="modal-dialog">
                          <div class="modal-content">
                             <div class="modal-header">
                                <h4 style="display: inline-block;"> Exclude Services Steps</h4>
                                <button type="button" class="close" data-dismiss="modal">×</button>
                             </div>
                            <div class="card-block accordion-block">  
                            </div>
                          </div>
                       </div>
                    </div>    
                    <!-- service exclude popup-->

                  </form>
                  </div>
                </div>
              </div>
              <!-- close -->
              
            </div>
          </div>
          <!-- Page body end -->
        </div>
      </div>
  
    </div>
  </div>
