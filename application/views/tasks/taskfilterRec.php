<?php $role = $this->Common_mdl->getRole($_SESSION['id']);?>
<table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
					<thead>
						<tr class="text-uppercase">
							<th><input type="checkbox"  id="bulkDelete"  /></th>
							<th>Timer</th>
							<th>Task Name</th>
							<th>Start Date</th>
							<th>Due Date</th>
							<th>Status</th>
							<th>Priority</th>
							<th>Tag</th>
							<th>Assignto</th>
							<th>Actions</th>
						</tr>	
					</thead>

					<tbody>
                        <?php foreach ($task_list as $key => $value) {
                       $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
                        if($value['task_status']=='notstarted')
                      {
                      	$stat = 'Not Started';
                      }else if($value['task_status']=='inprogress')
                      {
                      	$stat = 'In Progress';
                      }else if($value['task_status']=='awaiting')
                      {
                      	$stat = 'Awaiting for a feedback';
                      }else if($value['task_status']=='testing')
                      {
                      	$stat = 'Testing';
                      }else if($value['task_status']='complete')
                      {
                      	$stat = 'Complete';
                      }
                      $exp_tag = explode(',', $value['tag']);
                    
                      ?>
						<tr id="<?php echo $value['id']; ?>">
							 <td>
                                      <input type='checkbox'  class='deleteRow' value="<?php echo $value['id'];?>"  />
                                    </td>
							<td><?php echo date('Y-m-d H:i:s', $value['created_date']);?></td>
							<td><?php echo ucfirst($value['subject']);?></td>
							<td><?php echo $value['start_date'];?></td>
							<td><?php echo $value['end_date'];?></td>
                            <td><?php echo $stat;?></td>
							<td>
							<?php echo $value['priority'];?>
							</td>
							<td>
								<?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                      	    echo $tagName = $this->Common_mdl->getTag($exp_tag_value).' ';
                      }?>
							</td>
							<td class="user_imgs">
							<?php
                             foreach($staff as $key => $val){     
							?>
							<img src="<?php echo base_url().'uploads/'.$val['profile'];?>" alt="img">
							<?php } ?>
							</td>
							
							<td>
							<p class="action_01">
							<?php if($role!='Staff'){?>	
								<a href="<?php echo base_url().'user/delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
								<?php } ?>
								<a href="<?php echo base_url().'user/update_task/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
								<!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
							</p>
							</td>
						</tr>
					<?php } ?>
					</tbody>		
							</table>