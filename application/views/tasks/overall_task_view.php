<?php $this->load->view('includes/header');
$notstarted = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','notstarted');
        $inprogress = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','inprogress');
        $awaiting = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','awaiting');
        $testing = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','testing');
        $complete = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','complete');

?>

<link href="<?php echo  base_url()?>assets/css/jquery.lineProgressbar.css" rel="stylesheet" type="text/css">
<link href="<?php echo  base_url()?>assets/css/c3.css" rel="stylesheet" type="text/css">

<div class="leads-section floating_set">

<div class="col-sm-8 project-task-left">
<div class="individual-user1 floating_set">

<h4><!-- Project Owner: Manager Govan | Start: 01/Feb/2016 | Due on:01/Oct/2016 |  --><i class="fa fa-file-pdf-o fa-6" aria-hidden="true"></i></h4>

<div class="project-user1 floating_set">

	<div class="project-task1 col-sm-4">
		<div class="view-status-data">
			<div class="project-bugs">
				<span><?php echo count($notstarted);?></span>
			</div>
		<div class="project-employee"> <i class="fa fa-user-o fa-6" aria-hidden="true"></i></div>
		
		</div>
		<p>Not started Tasks</p>		
	</div>
	
	<div class="project-task1 col-sm-4">
		<div class="view-status-data">
			<div class="project-bugs">
				<span><?php echo count($inprogress);?></span>
			</div>
		<div class="project-employee"> <i class="fa fa-calendar-check-o fa-6" aria-hidden="true"></i></div>
		
		</div>
		<p>Inprogress</p>		
	</div>
		<div class="project-task1 col-sm-4">
		<div class="view-status-data">
			<div class="project-bugs">
				<span><?php echo count($awaiting);?></span>
			</div>
		<div class="project-employee"> <i class="fa fa-bug fa-6" aria-hidden="true"></i></div>
		
		</div>
		<p>Awaiting for feedback</p>		
	</div>
	</div>
	<div class="project-task1 col-sm-4">
		<div class="view-status-data">
			<div class="project-bugs">
				<span><?php echo count($testing);?></span>
			</div>
		<div class="project-employee"> <i class="fa fa-bug fa-6" aria-hidden="true"></i></div>
		
		</div>
		<p>Testing</p>		
	</div>
	
	
	<div class="project-task1 col-sm-4">
		<div class="view-status-data">
			<div class="project-bugs">
				<span><?php echo count($complete);?></span>
			</div>
		<div class="project-employee"> <i class="fa fa-bug fa-6" aria-hidden="true"></i></div>
		
		</div>
		<p>Complete Tasks</p>		
	</div>

	
	<div class="project-status-tasks floating_set">
		<div class="status-title floating_set">
			<h4><span>Egal's Eye View Of Project Status</span></h4>
		</div>
		
		<div class="progress-project floating_set">
			 <div class="col-sm-6 progress-percent color-progress1">
			 <div id="task1"></div>
			 <div class="tasks-percent">Not Started</div>
			 </div>
			 
			 <div class="col-sm-6 progress-percent color-progress2">
			 <div id="task2"></div>
			 <div class="tasks-percent">Inprogress</div>
			 </div>
			 
			 <div class="col-sm-6 progress-percent color-progress3">
			 <div id="task3"></div>
			 <div class="tasks-percent">Awaiting For feedback</div>
			 </div>
			 
			 <div class="col-sm-6 progress-percent color-progress4">
			 <div id="task4"></div>
			 <div class="tasks-percent">Testing</div>
			 </div>
			 <div class="col-sm-6 progress-percent color-progress6">
			 <div id="task6"></div>
			 <div class="tasks-percent">Completed</div>
			 </div>
		</div>	
	</div>	
</div>

	<div class="chat-setting01 floating_set">
		<!-- <div class="task-progress1 col-sm-6">	
			<div class="bug-progress">
			<div class="progress-title1 floating_set">
				<span class="pull-left">TASK PROGRESS</span>
				<a href="#" class="pull-right"><i class="fa fa-refresh fa-6" aria-hidden="true"></i></a>
			</div>
			
			<div class="open-closed1 floating_set">
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> open</span>
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> closed</span>
			</div>
			
			<div class="progress-section1 floating_set">
						<div class="card-block">
						<div id="chart3"></div>
						<div id="chartContainer1" style="width: 100%; height: 300px"></div> 
						</div>
			</div>
			</div>
		</div> -->
		
		<div class="task-progress1 col-sm-6">	
		<div class="bug-progress">
			<div class="progress-title1 floating_set">
				<span class="pull-left">TASk PROGRESS</span>
				<a href="#" class="pull-right"><i class="fa fa-refresh fa-6" aria-hidden="true"></i></a>
			</div>
			<div class="open-closed1 floating_set">
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i>Not Started</span>
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Inprogress</span>
				
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Awaiting For Feedback</span>
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Testing</span>
				
				<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Completed</span>
				
			</div>
			
			<div class="progress-section1 floating_set"> 
					<div class="card-block">
						<!-- <div id="chart3"></div> -->
						<div id="chartContainer" style="width: 100%; height: 300px"></div> 
						</div>
			</div></div>
		</div>
		
	</div>	
				
	


</div> <!-- col-sm-8 -->

			

			</div><!-- col-sm-4 -->


</div> <!-- leads-section -->



<?php $this->load->view('includes/footer');?>

<script src="<?php echo  base_url()?>assets/js/jquery.lineProgressbar.js"></script>
<script src="<?php echo  base_url()?>assets/js/c3-custom-chart.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/cdns/jquery.canvasjs.min.js"></script> 

<script>
$('#task1').LineProgressbar({
percentage:<?php echo count($notstarted);?>,
radius: '3px',
height: '20px',
fillBackgroundColor:'#2dcee3'
});
$('#task2').LineProgressbar({
percentage:<?php echo count($inprogress);?>,
radius: '3px',
height: '20px',
fillBackgroundColor: '#16d39a'
});
$('#task3').LineProgressbar({
percentage:<?php echo count($awaiting);?>,
radius: '3px',
height: '20px',
fillBackgroundColor: '#ffa87d'
});
$('#task4').LineProgressbar({
percentage:<?php echo count($testing);?>,
radius: '3px',
height: '20px',
fillBackgroundColor: '#ff7588'
});
$('#task6').LineProgressbar({
percentage:<?php echo count($complete);?>,
radius: '3px',
height: '5px',
fillBackgroundColor: '#ff7588'
});


</script>
<script type="text/javascript"> 
window.onload = function() { 
	$("#chartContainer").CanvasJSChart({ 
		title: { 
			fontSize: 24
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		data: [ 
		{ 
			type: "pie", 
			toolTipContent: "{label} <br/> {y} %", 
			indexLabel: "{y} %", 
			dataPoints: [ 
				{ label: "Notstarted",  y: <?php echo count($notstarted);?>, legendText: "Critical"}, 
				{ label: "Inprogress",    y: <?php echo count($inprogress);?>, legendText: "High"  }, 
				{ label: "Awaiting For feedback",   y: <?php echo count($awaiting);?>,  legendText: "Medium" }, 
				{ label: "Testing",       y: <?php echo count($testing);?>,  legendText: "Low"}, 
				{ label: "Complete",   y: <?php echo count($complete);?>,  legendText: "Resolved" }
			] 
		} 
		] 
	}); 
	
	$("#chartContainer1").CanvasJSChart({ 
		title: { 
			fontSize: 24
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		data: [ 
		{ 
			type: "pie", 
			toolTipContent: "{label} <br/> {y} %", 
			indexLabel: "{y} %", 
			dataPoints: [ 
				{ label: "Open",  y: 30.3, legendText: "Open"}, 
				{ label: "Closed",    y: 19.1, legendText: "Closed"  }, 
				
			] 
		} 
		] 
	}); 
} 
</script> 