<?php 
  $uri_clientid = $this->uri->segment(3);
  $task_status=$this->db->query('SELECT * from task_status where is_superadmin_owned=1')->result_array();
?>
<div class="import-tab-content">
            <div class="title_page01 fileupload_01 floating tskl">
    
              <div class="panel panel-default card import-tsk1">
              
                <div class="download-sample">
                <a href='<?= base_url() ?>uploads/csv/Task_import.csv'>Download Sample CSV File</a>
                </div>
                <div class="panel-body">
                  <form action="" method="post" enctype="multipart/form-data" id="importFrm">

                    <div class="upload_input09 custom_uploadtop">
                    <label>Choose csv File</label>

                       <div class="custom_upload">
                        <label for="file"></label>
                      <input type="file" name="file" id="file" accept=".csv"/>
                    </div>

                    </div>
                      <div class="new_task_div" id="tree_div2">
                    <div class="form-group">
                      <label>Choose Client/Firm</label>
                        <div class="import-tab-dropdown">
                        <select name="company_name" id="company_name"  placeholder="select Company">
                                             <!-- <input type="text" name="company" id="company"> -->
                                             <option value="">Select Company</option> 
                                             <?php 
                                                if(isset($records['company_id'])){ ?>
                                             <?php }else{ ?>
                                            <!--  <option value="">Select Company</option> -->
                                             <?php  } ?>
                                             <?php 
                                                  $query1=$this->db->query("SELECT * FROM `user` where autosave_status!='1' and user_type='FC' and crm_name!='' and firm_id=".$_SESSION['firm_id']." order by id DESC");
                                $results1 = $query1->result_array();
                                $res=array();
                                foreach ($results1 as $key => $value) {
                                     array_push($res, $value['id']);
                                  }
                                  if(!empty($res)){
                                  $im_val=implode(',',$res);
                                 // echo $im_val;
                              $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (".$im_val.") order by id desc ");    
                               $results = $query->result_array();  
                               }
                               else{
                                $results=0;
                               
                                    $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id=".$_SESSION['id']." order by id desc ");    
                                     $results = $query->result_array();  
                               }  
                                                if(count($results) > 0)  
                                                {  
                                                   foreach($results as $key => $val)
                                                   {  ?>
                                             <option value="<?php echo $val['id'];?>"  <?php if(isset($records['company_id'])){     
                                                if($records['company_id']==$val['id']){ ?>
                                                selected="selected"
                                                <?php } } ?>
                                                ><?php echo $val["crm_company_name"];?></option>
                                             <?php
                                                }  
                                                }  ?>
                                          </select>

                        </div>
                      </div>

                      <div class="clearfix"></div>
                      <div class="form-group">
                                       <label>Task status*</label>
                                       <select name="task_status" class="form-control" id="task_status">
                                       <?php foreach($task_status as $key =>$status_val)  { ?>
                                          <option value="<?php echo $status_val['id'] ?>"><?php echo $status_val['status_name'] ?></option>
                                         
                                          <?php } ?>
         <option value="archive" <?php if(isset($_SESSION['sta_tus']) &&  $_SESSION['sta_tus']=='archive'){ ?> selected="selected" <?php } ?>>Archive</option>
                                       </select>
                                    </div>
                    

                 
                    <!-- <div id="base_projects"></div> -->
                    <div class="form-group">
                      <label>Assign to(worker)</label>

                          <input type="text" id="tree_select2" class="tree_select" name="assignees[]" placeholder="Select">

                          <input type="hidden" id="assign_role1" name="assign_role" >

                    </div>
                    <div class="form-group">
                      <label>Priority</label>
                      <select name="priority" class="form-control" id="priority">
                        <option value="low">Low</option>
                        <option value="medium">Medium</option>
                        <option value="high">High</option>
                        <option value="super urgent">Super Urgent</option>
                      </select>
                    </div>
                    </div>
                  

                      <input type="hidden" name="client_id" id="client_id" value="<?php echo $uri_clientid;?>">
                      <div class="newimports"><input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT"></div>
                  </form>
                  </div>
                </div>
              </div>
              </div>
<script>
    
    $(document).ready(function(){

      $('#import-task .modal-body').html( $('.import-tab-content').html() );

        var tree_select = <?php echo json_encode( GetAssignees_SelectBox() ); ?>;
         $('.tree_select').comboTree({
            source : tree_select,
            isMultiple: true
         });


           $('#tree_div2 .comboTreeItemTitle').click(function(){
            var id = $(this).attr('data-id');
            var id = id.split('_');
               $('#tree_div2 .comboTreeItemTitle').each(function(){
               var id1 = $(this).attr('data-id');
               var id1 = id1.split('_');
               //console.log(id[1]+"=="+id1[1]);
                  if(id[1]==id1[1])
                  {
                     $(this).toggleClass('disabled');
                  }
               });
                $(this).removeClass('disabled');
         });


$('.import-tab-content').empty();

/*import tap  select company assignee */

$("select[name='company_name']").change(function(){


       var id = $(this).val();
      // alert(id);
    
        var task_name='CLIENT';
         $('#user_id').val(id);
       // var manager_select = $(this).closest('form').find("select[name='manager[]']");
       // var workers_select = $(this).closest('form').find("select[name='worker[]']");
      var new_div=$(this).closest('form').find('.new_task_div').attr('id');
      
    //  alert(new_div);
      if(id!='')
      {

        $.ajax({
          url : "<?php echo base_url()?>user/get_company_assign",
          type : "POST",
          data : {"id":id,"task_name":task_name},
          dataType : "json",        
          beforeSend : function(){
            $(".LoadingImage").show();
          },
          success: function(data){
            $(".LoadingImage").hide();  

        
             var arr1 = data.asssign_group;
         
                 var unique = arr1.filter(function(itm, i, arr1) {
              return i == arr1.indexOf(itm);
          });


       $('#'+new_div+' .comboTreeItemTitle').each(function(){
      $(this).find("input:checked").trigger('click');
               var id1 = $(this).attr('data-id');
                var id = id1.split('_');
             if(jQuery.inArray(id[0], unique) !== -1)
               {
                  $(this).find("input").trigger('click');
                    
              }
                    
               });
         
         
  
          }
      });
      
      }   
});


$("#importFrm").validate({
    
         ignore: false,
  
                          rules: {
                               
                                file: {required: true, accept: "csv"},
                                'company_name' : { required : true },
                                "assignees[]" : { required : true },
                                // "manager[]" : { required : true },                          
                          },
                          errorElement: "span" , 
                          errorClass: "field-error",                             
                           messages: {
                            
                            file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},
                            'company_name': {required: 'Required'},
                            "assignees[]": {required: 'Required'},
                            // "manager[]": {required: 'Required'},
                           
                           },
                           
                          submitHandler: function(form) {
                              
                              var Assignees=[];
                                $('#tree_div2 .comboTreeItemTitle input[type="checkbox"]:checked').each(function(){
                               var id = $(this).closest('.comboTreeItemTitle').attr('data-id');
                               var id = id.split('_');
                              Assignees.push( id[0] );
                           });
                         //  data['assignee'] = Assignees


                            var assign_role = Assignees.join(',');
                            $('#assign_role1').val(assign_role);
                            var formData = new FormData($("#importFrm")[0]);
    
                             // $(".LoadingImage").show();
  
                              $.ajax({
                                  url: '<?php echo base_url();?>tasksummary/import_tasks',
                                  dataType : 'json',
                                  type : 'POST',
                                  data : formData,
                                  contentType : false,
                                  processData : false,
                                  beforeSend : function()
                                  {$('.LoadingImage').show();},
                                  success: function(data) {

                                      $('.LoadingImage').hide();
                                       if(data[0]){
                                        $(".popup_info_msg").show();
               $('.popup_info_msg .position-alert1').html('Success !!! New Task Imported successfully...');

               setTimeout(function () {
                $(".popup_info_msg").hide();
                 window.location.replace("<?php echo base_url(); ?>user/task_list/");
                }, 1500);
                                             }
                                             else 
                                             {
                                    $(".popup_info_msg").show();
                                    $('.popup_info_msg .position-alert1').html('<p style="color:red">Error!!!  '+data[1]+'</p>');
                                     setTimeout(function () {
                                  $(".popup_info_msg").hide();
                                  }, 1500);
                                             }
                                  },
                                  error: function() { $('.alert-danger').show();
                                          $('.popup_info_msg').hide();}
                              });
  
                              return false;
                          } 
                           
                      });
  
  
$('.import-tab-dropdown').dropdown();

});
</script>



<script src="<?php echo base_url();?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/icontains.js"></script>

