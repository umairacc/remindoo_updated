<script type="text/javascript">
  'use strict';
function boxMinimizedCount() {

    var _count = $('#main-chat .chat-single-box.minimized .chat-dropdown li').length;

    $('#main-chat .chat-single-box.minimized .count span').html($('#main-chat .chat-single-box.minimized .chat-dropdown li').length);

    if (_count == 0) {
        $('#main-chat .chat-single-box.minimized').remove();
    }
}


function boxMinimizedUserAdd() {

    var _boxHidden = $('#main-chat .chat-single-box:not(".minimized"):not(".hidden")').eq(0);
    _boxHidden.addClass('hidden');
    var dataId = _boxHidden.data('id');

    var hasItem = false;
    $('#main-chat .chat-single-box.minimized .chat-dropdown li').each(function () {
        if ($(this).data('id') == dataId) {
            hasItem = true;
        }
    });

    if (!hasItem) {

        var dataUserName = _boxHidden.find('.user-info a').text();
        $('#main-chat .chat-single-box.minimized .chat-dropdown').append(box_minimized_dropdownLi.format(dataId, dataUserName));
      //   $('.our_message_{0}').parents(".chat-body").animate({ scrollTop: $('.our_message_'+chat_from).height() }, 0);
    }
}

var box_minimized_dropdownLi = '<li data-id="{0}"><div class="username">{1}</div> <div class="remove">X</div></li>'
function boxMinimized() {

    var _boxDefaultWidth = parseInt($('#main-chat .chat-single-box:not(".minimized")').css('width'));
    var _boxCommonWidth = parseInt($('.chat-box').css('width').replace('px', ''), 10)  + parseInt($('#sidebar').css('width').replace('px', ''), 10);
    var _windowWidth = $(window).width();
    var _hasMinimized = false;

    $('#main-chat .boxs .chat-single-box').each(function (index) {

        if ($(this).hasClass('minimized')) {
                _hasMinimized = true;
        }
    });

    if ((_windowWidth) > (_boxCommonWidth)) {

        if (!_hasMinimized) {
           if((_windowWidth)< 768 ){

                    $(".chat-box").css('margin-right','70px');
                    return;
           }
           else{
                 return;
           }

        }

        var dataId = $('#main-chat .boxs .minimized .chat-dropdown li').last().data('id');
        $('#main-chat .boxs .minimized .chat-dropdown li').last().remove();
        $('#main-chat .boxs .chat-single-box').each(function (index) {

            if ($(this).data('id') == dataId) {
                $(this).removeClass('hidden');
                return false;
            }
        });
    } else {
        if (!_hasMinimized) {

            $('#main-chat .boxs').prepend('<li class="chat-single-box minimized"><div class="count"><span>0</span></div><ul class="chat-dropdown"></ul></li>');
        }

        boxMinimizedUserAdd();

    }

    boxMinimizedCount();
}



$(window).on('resize',function () {

    boxMinimized();
    sidebarClosed();
});
$(function () {

    var waveEffect = $('.user-box').attr('wave-effect');
    var waveColor = $('.user-box').attr('wave-color');
    if (waveEffect == 'true') {

        $('#sidebar .user-box .userlist-box').each(function (index) {
            $(this).addClass('waves-effect ' + waveColor);
        });
    }

    initialTooltip();
    messageScroll();
    generatePlaceholder();

    boxMinimized();
});



$(document).on('click', '#main-chat .chat-single-box', function () {

    if ($(this).hasClass('new-message')) {

        $(this).removeClass('new-message');
    }
    ActiveChatBox(this);
});

$(document).on('click', '#main-chat .chat-header .user-info', function () {

    removeBoxCollapseClass($(this).parents('.chat-single-box'));

    messageScroll();
});

$(document).on('click', '#main-chat .chat-single-box .mini', function () {

    parent = $(this).parents('.chat-single-box');

    if ($(parent.children()[0].children[0]).hasClass('custom-collapsed')) {

        $(parent.children()[0].children[0]).removeClass('custom-collapsed');
        $(parent.children()[0].children[1]).css('display','block');
         $(parent.children()[0].children[2]).css('display','block');
       parent.addClass('bg-white');
       parent.addClass('card-shadow');
        messageScroll();
    } else {
       parent.removeClass('bg-white');
       parent.removeClass('card-shadow');
       $(parent.children()[0].children[0]).addClass('custom-collapsed');
        $(parent.children()[0].children[1]).css('display','none');
         $(parent.children()[0].children[2]).css('display','none');
    }
<?php if($_SESSION['roleId']==4){ ?>
     //$(".chat-body").animate({ scrollTop: $('.chat-body').height() }, 0);
          var class_name=$(parent.children()[0].children[1]).attr('class').split(' ')[0];
     // console.log(class_name);
       $("."+class_name).animate({ scrollTop: $('.'+class_name).height() }, 0);

      <?php } ?>
      // console.log(JSON.stringify(parent.children()[0].children[0])+"---"+JSON.stringify(parent.children()[0].children[1])+"--"+JSON.stringify(parent.children()[0].children[0]));
      //$(parent.children()[0].children[1]).animate({ scrollTop: $(parent.children()[0].children[1]).height() }, 0);
});

$(document).on('click', '#main-chat .chat-single-box .close', function () {

    parent = $(this).parents('.chat-single-box');
    if (parent.hasClass('active')) {

        parent.remove();
        setTimeout(function () { $('#main-chat .boxs .chat-single-box:last-child').addClass('active'); }, 1);
    }
    parent.remove();
    parent.find('.close_tooltip').tooltip('dispose');

    boxMinimized();
});

/*Click on username*/
$(document).on('click', '#main-chat #sidebar .user-box .userlist-box', function () {

    var dataId = $(this).attr('data-id');
    var dataStatus = $(this).data('status');
    var dataUserName = $(this).attr('data-username');

    var _return = false;

/** for group **/
var group=$(this).attr('data-group');
var members=$(this).attr('data-member');
var creater=$(this).attr('data-creater');
var session=$(this).attr('data-session');

if(typeof group === "undefined"){
    group='single';
}

/** end of group **/

    $('#main-chat .chat-box .boxs .chat-single-box').each(function (index) {

        if ($(this).attr('data-id') == dataId) {

            removeBoxCollapseClass(this);
            ActiveChatBox(this);
            _return = true;
        }
    });


    if (_return) {

        return;
    }
    if(group=='single'){ 
    if(dataStatus == "online"){

    var newBox = '<li class="chat-single-box card-shadow bg-white active chatbox_{0}" data-id="{0}"><div class="had-container"><div class="chat-header p-10 bg-gray"><div class="user-info d-inline-block f-left"><div class="box-live-status bg-success  d-inline-block m-r-10"></div><a href="javascript:void(0)" class="mini" >{1}</a></div><div class="box-tools d-inline-block"><a href="#" class="mini"><i class="icofont icofont-minus f-20 m-r-10"></i></a><a class="close" href="#"><i class="icofont icofont-close f-20"></i></a></div></div><div class="chat-body p-10"><input type="hidden" id="ajax_value_{0}" name="ajax_value"><div class="messages our_message_{0}"><div class="our_info_right" data-id="0" data-dbid="{0}"></div><div class="message-scrooler"></div></div></div><div class="chat-footer b-t-muted"><div class="input-group write-msg"><input type="text"  class="form-control input-value" name="text_message" data-id="{0}" id="text_message_{0}" placeholder="Type a Message"><span class="input-group-btn"><button  id="paper-btn" class="btn btn-primary message_send" data-id="{0}"  type="button"><i class="icofont icofont-paper-plane"></i></button></span></div></div></div></li>';
    }
    else{

        var newBox = '<li class="chat-single-box card-shadow bg-white active chatbox_{0}" data-id="{0}"><div class="had-container"><div class="chat-header p-10 bg-gray"><div class="user-info d-inline-block f-left"><div class="box-live-status bg-danger  d-inline-block m-r-10"></div><a href="javascript:void(0)" class="mini" >{1}</a></div><div class="box-tools d-inline-block"><a href="#" class="mini"><i class="icofont icofont-minus f-20 m-r-10"></i></a><a class="close" href="#"><i class="icofont icofont-close f-20"></i></a></div></div><div class="chat-body p-10"><input type="hidden" id="ajax_value_{0}" name="ajax_value"><div class="message-scrooler"><div class="messages our_message_{0}"><div class="our_info_right" data-id="0" data-dbid="{0}"></div></div></div></div><div class="chat-footer b-t-muted"><div class="input-group write-msg"><input type="text" class="form-control input-value" name="text_message" data-id="{0}" id="text_message_{0}" placeholder="Type a Message"><span class="input-group-btn"><button  id="paper-btn" class="btn btn-primary message_send" data-id="{0}" type="button"><i class="icofont icofont-paper-plane"></i></button></span></div></div></div></li>';
    }
      }
    else{ 
if(creater==session){
    var newBox = '<li class="chat-single-box card-shadow bg-white active chatbox_{0}" data-id="{0}"><div class="had-container"><div class="chat-header p-10 bg-gray"><div class="user-info d-inline-block f-left"><a href="javascript:void(0)"  class="mini members_chat_{0}" data-toggle="tooltip" title="'+members+'" >{1}</a></div><div class="box-tools d-inline-block"><a href="#" class="group-info" data-toggle="modal" data-target="#popup_group_edit_{0}" ><i class="icofont icofont-plus f-20 m-r-10"></i></a><a href="#" class="mini"><i class="icofont icofont-minus f-20 m-r-10"></i></a><a class="close" href="#"><i class="icofont icofont-close f-20"></i></a></div></div><div class="chat-body p-10"><input type="hidden" id="ajax_value_{0}" name="ajax_value"><div class="messages our_message_{0}"><div class="group_our_info_right_{0}" data-id="0" data-dbid="{0}"></div><div class="message-scrooler"></div></div></div><div class="chat-footer b-t-muted"><div class="input-group write-msg"><input type="text" data-group="group"  class="form-control input-value" name="text_message" data-id="{0}" id="text_message_{0}" placeholder="Type a Message"><span class="input-group-btn"><button  id="paper-btn" data-group="group" class="btn btn-primary message_send" data-id="{0}"  type="button"><i class="icofont icofont-paper-plane"></i></button></span></div></div></div></li>';
  }
  else
  {
        var newBox = '<li class="chat-single-box card-shadow bg-white active chatbox_{0}" data-id="{0}"><div class="had-container"><div class="chat-header p-10 bg-gray"><div class="user-info d-inline-block f-left"><a href="javascript:void(0)" class="mini members_chat_{0}" data-toggle="tooltip" title="'+members+'" >{1}</a></div><div class="box-tools d-inline-block"><a href="#" class="mini"><i class="icofont icofont-minus f-20 m-r-10"></i></a><a class="close" href="#"><i class="icofont icofont-close f-20"></i></a></div></div><div class="chat-body p-10"><input type="hidden" id="ajax_value_{0}" name="ajax_value"><div class="messages our_message_{0}"><div class="group_our_info_right_{0}" data-id="0" data-dbid="{0}"></div><div class="message-scrooler"></div></div></div><div class="chat-footer b-t-muted"><div class="input-group write-msg"><input type="text" data-group="group"  class="form-control input-value" name="text_message" data-id="{0}" id="text_message_{0}" placeholder="Type a Message"><span class="input-group-btn"><button  id="paper-btn" data-group="group" class="btn btn-primary message_send" data-id="{0}"  type="button"><i class="icofont icofont-paper-plane"></i></button></span></div></div></div></li>';
  }
 

    }  

    $('#main-chat .chat-single-box').removeClass('active');
    $('#main-chat .chat-box .boxs').append(newBox.format(dataId, dataUserName, dataStatus));
   //  $('.our_message_{0}').parents(".chat-body").animate({ scrollTop: $('.our_message_'+chat_from).height() }, 0);
if(group=='single'){
       var data = {};
       data['chat_id'] = dataId;
         data['condition']='no';
             $.ajax({
           url: '<?php echo base_url();?>User_chat/get_chat/',
           type : 'GET',
           data : data,
           success: function(data) {
           if(data!=''){
            $('.our_message_'+dataId).html(data);
            }
             $('.our_message_'+dataId).parents(".chat-body").animate({ scrollTop: $('.our_message_'+dataId).height() }, 0);

           },
       });
}
else
{
       var data = {};
       data['chat_id'] = dataId;
         data['condition']='no';
             $.ajax({
           url: '<?php echo base_url();?>User_chat/get_group_chat/',
           type : 'GET',
           data : data,
           success: function(data) {
           if(data!='null'){
            $('.our_message_'+dataId).html(data);
            }
             $('.our_message_'+dataId).parents(".chat-body").animate({ scrollTop: $('.our_message_'+dataId).height() }, 0);

           },
       });
}  



    generatePlaceholder();
    messageScroll();
    boxMinimized();
    initialTooltip();




});

$(document).on('focus', '#main-chat .textarea', function () {

    if ($(this).html() == '<span class="placeholder">{0}</span>'.format($(this).data('placeholder'))) {

       $(this).html('');
    }
});

$(document).on('blur', ' #main-chat .textarea', function () {

    if ($(this).html() == '') {

        $(this).html('<span class="placeholder">{0}</span>'.format($(this).data('placeholder')));
    }
});

$(document).on('click', '#main-chat .sidebar-collapse', function () {

    if ($('#main-chat').hasClass('sidebar-closed')) {

        $('#main-chat').removeClass('sidebar-closed');

        $('#main-chat .search input').attr('placeholder', '');
        $('#main-chat .search').css('display', 'block');


        deinitialTooltipSiderbarUserList();



    } else {

        $('#main-chat').addClass('sidebar-closed');

        $('#main-chat .search input').attr('placeholder', $('.search input').data('placeholder'));
        $('#main-chat .search').css('display', 'none');
        $('#main-chat .search').removeAttr('style');
        $('#main-chat .searchbar-closed').removeAttr('style');


        initialTooltipSiderbarUserList();
    }
});

$(document).on('click', '#main-chat .searchbar-closed', function () {

    $('#main-chat .sidebar-collapse').click();
    setTimeout(function () { $('#main-chat .searchbar input').focus(); }, 50);
    return false;
});

$(document).on('click', '#main-chat .chat-single-box .maximize', function () {

   /* window.open('inbox.html', 'window name', "width=300,height=400,scrollbars=yes");
    $(this).parents('.chat-single-box').remove();
    $('.maximize').tooltip('dispose');*/
     parent = $(this).parents('.chat-single-box');
      $(parent.children()[0].children[0]).removeClass('custom-collapsed');
        $(parent.children()[0].children[1]).css('display','block');
         $(parent.children()[0].children[2]).css('display','block');
       parent.addClass('bg-white');
       parent.addClass('card-shadow');
        messageScroll();
    return false;
});



$(document).on('click', '#main-chat .boxs .minimized .count', function (e) {

    e.stopPropagation();
    hideStickerBox();
    var _parent = $(this).parents('.minimized');

    if (_parent.hasClass('show')) {

        hideMinimizedBox();
    } else {

        _parent.addClass('show');
        var _bottom = parseInt(_parent.css('height').replace('px', ''),0) + 10;
        _parent.find('.chat-dropdown').css({
            'display': 'block',
            'bottom': _bottom
        });
    }
});

$(document).on('click', '#main-chat .boxs .minimized .chat-dropdown .username', function (e) {

    e.stopPropagation();
    var selectedDataId = $(this).parent().data('id');

    $(this).parent().remove();

    boxMinimizedUserAdd();

    $('#main-chat .boxs .chat-single-box').each(function (index) {

        if ($(this).data('id') == selectedDataId) {

            $(this).removeClass('hidden').removeClass('custom-collapsed');
            ActiveChatBox($(this));
        }
    });
});

$(document).on('click', '#main-chat .boxs .minimized .chat-dropdown .remove', function (e) {

    e.stopPropagation();
    var _parent = $(this).parents('.chat-dropdown li');
    dataId = _parent.data('id');

    $('#main-chat .chat-single-box').each(function () {

        if ($(this).data('id') == dataId) {
            $(this).remove();
        }
    });
    _parent.remove();

    boxMinimizedCount();
});
</script>
<script type="text/javascript">
$(window).on('load', function () {
  console.log('fsdfsdfsd dsddfsa');
    var currentXhr1 = null;var currentXhr2 = null;var currentXhr3 = null;var currentXhr4 = null;
    var currentXhr5 = null;var currentXhr6 = null;var currentXhr7 = null;var currentXhr8 = null;
    var currentXhr9 = null;var currentXhr10 = null;
    var current_arr1=[];var current_arr2=[];var current_arr3=[];var current_arr4=[];var current_arr5=[];var current_arr6=[];
    var rspt_oneline_ajax=null;var rspt_oneline_ajax_one=null;var rspt_oneline_ajax_two=null;var rspt_oneline_ajax_three=null;
   window.setInterval(function(){


     var listed_chatlist = [];
<?php 
$common_id_val=array();
 if(isset($getallUser) && !empty($getallUser)){
 foreach ($getallUser as $getallUser_key => $getallUser_value) {
    array_push($common_id_val, $getallUser_value['id']);
 }
}
 if(isset($getalladmin) && !empty($getalladmin)){
 foreach ($getalladmin as $getallUser_key => $getallUser_value) {
    array_push($common_id_val, $getallUser_value['id']);
 }
}
 if(isset($getallstaff) && !empty($getallstaff)){
 foreach ($getallstaff as $getallUser_key => $getallUser_value) {
    array_push($common_id_val, $getallUser_value['id']);
 }
}
$rspt_new_online=array();
foreach ($common_id_val as $getallUser_key => $getallUser_value) {
  ?>
  listed_chatlist.push('<?php echo $getallUser_value;?>');
  <?php
  array_push($rspt_new_online, $getallUser_value);
}
$ajax_data_online_ids=implode(',',$rspt_new_online);
   ?>

/** rspt new online shown **/
              var data = {};
              data['id']='<?php echo $ajax_data_online_ids; ?>'
rspt_oneline_ajax=$.ajax({

    url: '<?php echo base_url();?>User_chat/get_status_online_rspt/',
                           type : 'GET',
                           data : data,
                            beforeSend : function(){        
                   
                             if(rspt_oneline_ajax != undefined ) {
                                 rspt_oneline_ajax.abort();
                                   }
                            },
                           success: function(data) {

                            // if(data=='online')
                            // {
                            //    var status_color="bg-success";
                            // }
                            // else
                            // {
                            //    var status_color="bg-danger";
                            // }
                            if(data!=0)
                            { 
                               var data = $.parseJSON(data);
                               $.each(data, function(index, element) {
                                  
                            if(element=='online')
                            {
                               var status_color="bg-success";
                            }
                            else
                            {
                               var status_color="bg-danger";
                            }
                           /** for left side box **/
                            $('.box_'+index).find('.media-left').children('.live-status').removeClass('bg-danger');
                            $('.box_'+index).find('.media-left').children('.live-status').removeClass('bg-success');
                            $('.box_'+index).find('.media-left').children('.live-status').removeClass('bg-success');
                            $('.box_'+index).find('.media-left').children('.live-status').addClass(status_color);
                             /** for open box chat online/offline **/
                            $('.chatbox_'+index).find('.user-info').children('.box-live-status').removeClass('bg-danger');
                            $('.chatbox_'+index).find('.user-info').children('.box-live-status').removeClass('bg-success');
                            
                           $('.chatbox_'+index).find('.user-info').children('.box-live-status').addClass(status_color);
                           /** end of online/offline **/
                               })
                            } // if

                          },

});


/** end of new online shown **/

  
var lst_id;
var for_admin;
var rightarray=[];
  $('.our_info_right').each(function(){
lst_id=$(this).attr('data-id');
for_admin=$(this).attr('data-dbid');
 rightarray.push(lst_id);

  });
 var leftarray = [];
 $('.our_info').each(function(){
lst_left_id=$(this).attr('data-id');
 leftarray.push(lst_left_id);
 });
<?php //if($_SESSION['roleId']=='4'){ // crct
//if($_SESSION['roleId']=='12'){ // dummy  ?>
    //console.log(lst_id+"--------"+for_admin);
if(typeof lst_id !== "undefined" && typeof for_admin !== "undefined"){}
<?php // } ?>
/** for our **/


/** end of our **/

      var data = {};
   data['new'] = '';
   var rspt_get_chat_ajax=[];
    var leftarray = [];
     $('.our_info').each(function(){
        lst_left_id=$(this).attr('data-id');
         leftarray.push(lst_left_id);

         });
   $( "li.chat-single-box" ).each(function() {
    var it_ids=$(this).attr('data-id');
    rspt_get_chat_ajax.push(it_ids);
   });
   data['shown_box_ids']=rspt_get_chat_ajax.join(",");
   var currentXhr3=$.ajax({
       url: '<?php echo base_url();?>User_chat/get_chat_ajax_rsptnew/',
       type : 'GET',
       data : data,
         beforeSend : function(){           
                             if(currentXhr3 != null) {
                                 currentXhr3.abort();
                                   }
                          },
       success: function(data) {
          
          var data = $.parseJSON(data);var newarray = [];
              var ourarray = {};
                $.each(data, function(index, element) {
                  newarray.push(element.chat_from);
                  if(jQuery.inArray(element.id, leftarray) == -1){
                  if(typeof(element.message_content) != "undefined" && element.message_content !== null) {
                    console.log(data);
                                var val=$('#ajax_value_'+element.chat_from).val();
                                             //  console.log(leftarray.length+"length of array ");
                                                if (rspt_get_chat_ajax.length > 0) {
                                                   $('#ajax_value_'+element.chat_from).val(val+','+element.id);
                                                    $('.our_message_'+element.chat_from).append(element.message_content);
                                                /** for scroll **/
 $('.our_message_'+element.chat_from).parents(".chat-body").animate({ scrollTop: $('.our_message_'+element.chat_from).height() }, 0);

                                                /** end of scroll **/
                                                                                  
                                                    }  
                                                  }
                                                  

                  }
                });
                 counts = {};
  jQuery.each(newarray, function(key,value) {
                if (!counts.hasOwnProperty(value)) {
                  counts[value] = 1;
                } else {
                  counts[value]++;
                }
              });
  var unread_count=[];
                jQuery.each(counts, function(key,value) {
                 $('.new_message_count_'+key).html(value);
                 // console.log('unread');
                 // console.log(value);
                 // console.log('.new_message_count_'+key);
                 /** open unread msg chat box **/
                 $('#main-chat #sidebar .user-box .box_'+key).trigger('click');
                 /** end of chatbox **/
                 unread_count.push(key);
                 /** for unread msg count **/
                // alert($('.chatbox_2308').attr('class'));
                  if(jQuery.inArray(key, listed_chatlist) == -1)
                      {
                        $('.new_message_count_'+key).html('');             
                      }   
                 /** end of msg count **/

                });

                /** for msg count **/
                jQuery.each( listed_chatlist, function( i, val ) {
                

                   if(jQuery.inArray(val, unread_count) == -1)
                                      {
                           $('.new_message_count_'+val).html('');             
                                      }                   
                });
                /** end of msg count **/

       },
   
   });
 


}, 5000);
/** for group **/
window.setInterval(function(){

  /*** for group chat data **/
  <?php $group_data_my=$this->db->query("SELECT id FROM chat_group where create_by=".$_SESSION['id']." ")->result_array(); 
    $group_data_member=$this->db->query("SELECT id FROM chat_group where find_in_set('".$_SESSION['id']."',members) ")->result_array();
    $one_array=array();
    $two_array=array();
    foreach ($group_data_my as $key => $value) {
      array_push($one_array, $value['id']);
    }
    foreach ($group_data_member as $key => $value) {
      array_push($two_array, $value['id']);
    }
    //var currentXhr = null;
    $group_common=array_unique(array_merge($one_array,$two_array));
/** 28-12-2018 rst new **/
 ?>
 var data_group={};
 data_group['chat_ids']='<?php echo implode(',',$group_common); ?>';
 if(data_group['chat_ids']!=''){
 rspt_oneline_ajax_one=$.ajax({
       url: '<?php echo base_url();?>User_chat/get_group_chat_unread_rsptnew/',
                         type : 'GET',
                         data : data_group,
             beforeSend : function(){
                             if(rspt_oneline_ajax_one != undefined ) {
                                 rspt_oneline_ajax_one.abort();
                                   }
                            },
                         success: function(data) {
                            var data = $.parseJSON(data);
                            $.each(data, function(index, element) {
                              if(element>0)
                              {
                                $('.new_message_count_'+index).html(element);
                              }
                            });
                         },

 });
}
/** fopr get group chat data **/
var group_leftarray=[];
var group_rightarray=[];
<?php 
foreach ($group_common as $group_key => $group_value) { ?>
    var group_leftarray_one=[];
            $('.group_our_info_'+<?php echo $group_value;?>).each(function(){
            lst_left_id=$(this).attr('data-id');
             group_leftarray_one.push(lst_left_id);
             });
    
       var group_rightarray_one=[];
             $('.group_our_info_right_'+<?php echo $group_value;?>).each(function(){
            var lst_right_id=$(this).attr('data-id');
        //group_leftarray[][<?php echo $group_value;?>]=lst_left_id;
         group_rightarray_one.push(lst_right_id);
             //group_rightarray.push($(this).attr('data-id'));
             });

      group_leftarray[<?php echo $group_value; ?>]=group_leftarray_one;
      group_rightarray[<?php echo $group_value; ?>]=group_rightarray_one;

  <?php }
?>

/** end of group chat data **/
// var data={};
// data['group_full_data']=JSON.stringify(group_leftarray);
// data['chat_ids']='<?php echo implode(',',$group_common); ?>';
//  var currentXhr4_respt=$.ajax({
//        url: '<?php echo base_url();?>User_chat_chart/get_group_chat_rsptnew/',
//                          type : 'GET',
//                          data : data,
//              beforeSend : function(){
//                              if(currentXhr4_respt != undefined ) {
//                                  currentXhr4_respt.abort();
//                                    }
//                             },
//                          success: function(data) {
//                           console.log('fsdfsdf');
                 
//                          },

//  });
 <?php
/** end of 28-12-2018 **/

    foreach ($group_common as $group_key => $group_value) { ?>
      var chat_id=<?php echo $group_value;?>;
        var data = {};
        data['chat_id'] = <?php echo $group_value;?>;
        data['condition']='yes';
             //    current_arr3['<?php echo $group_value; ?>']=$.ajax({
             //             url: '<?php echo base_url();?>User_chat_chart/get_group_chat_unread/',
             //             type : 'GET',
             //             data : data,

             // beforeSend : function(){        
                   
             //                 if(current_arr3['<?php echo $group_value;?>'] != undefined ) {
             //                     current_arr3['<?php echo $group_value;?>'].abort();
             //                       }
             //                },
             //             success: function(data) {
             //             // console.log('.new_message_count_'+<?php echo $group_value;?>+"-------"+data);
             //              if(data>0){
             //                $('.new_message_count_'+<?php echo $group_value;?>).html(data);
             //              }

             //              },

             //            });
                var group_leftarray=[];
            $('.group_our_info_'+<?php echo $group_value;?>).each(function(){
            lst_left_id=$(this).attr('data-id');
             group_leftarray.push(lst_left_id);
             });
          
            if(group_leftarray.length>0){
               var leftdata=group_leftarray.join(',');
                 var data = {};
                data['chat_id'] = <?php echo $group_value;?>;
                data['ids']=leftdata;
                data['for']='left';
                data['condition']='yes';
                 current_arr6[<?php echo $group_value;?>]=$.ajax({
                     url: '<?php echo base_url();?>User_chat/get_group_chat/',
                     type : 'GET',
                     data : data,
                       beforeSend : function(){           
                             if(current_arr6[<?php echo $group_value;?>] != null) {
                                 current_arr6[<?php echo $group_value;?>].abort();
                                   }
                          },
                     success: function(data) {
                        // console.log('data');
                        // console.log(leftdata);
                        // console.log(data);
                        // console.log('data');
                     if(data!='null'){
                    //  $('.our_message_'+<?php echo $group_value;?>).html(data);
                    $('.our_message_'+<?php echo $group_value;?>).append(data);
                      }
                       $('.our_message_'+<?php echo $group_value;?>).parents(".chat-body").animate({ scrollTop: $('.our_message_'+<?php echo $group_value;?>).height() }, 0);

                     },
                 });
            }
            var group_rightarray=[];
       
             $('.group_our_info_right_'+<?php echo $group_value;?>).each(function(){
            var lst_right_id=$(this).attr('data-id');
        
             group_rightarray.push($(this).attr('data-id'));
             });
         //   console.log('test returm');
                    //  console.log(group_rightarray);
               
                    //  console.log('test end returm');
        if(group_rightarray.length>0){
            if($.inArray("0",group_rightarray)==-1)
            {
                     var leftdata=group_rightarray.join(',');
                     var data = {};
                    data['chat_id'] = <?php echo $group_value;?>;
                    data['ids']=leftdata;
                    data['for']='right';
                    data['condition']='yes';
                     currentXhr5=$.ajax({
                         url: '<?php echo base_url();?>User_chat/get_group_chat/',
                         type : 'GET',
                         data : data,
                        beforeSend : function(){           
                             if(currentXhr5 != null) {
                                 currentXhr5.abort();
                                   }
                          },
                         success: function(data) {
                         if(data!='null'){
                        //  console.log('returm');
                          //console.log(group_rightarray);
                          //console.log(data);
                         // console.log('end returm');
                        $('.our_message_'+<?php echo $group_value;?>).append(data);
                       //  $('#main-chat #sidebar .user-box .box_'+<?php echo $group_value;?>).trigger('click');
                         $('.our_message_'+<?php echo $group_value;?>).parents(".chat-body").animate({ scrollTop: $('.our_message_'+<?php echo $group_value;?>).height() }, 0);
                          }
                          

                         },
                     });
                 }
             }
      
  <?php  }
  ?>
  /** for group chat ajax load and it's actions **/
   var currentXhr = null;
   var chat_list=[];
   var chat_data_mem=[];
$('.group_chat_list').each(function(){
       chat_list.push($(this).attr('data-id'));
       chat_data_mem[$(this).attr('data-id')]=$(this).attr('data-member');
     });  
if(chat_list.length>0){
  var chart_list_ids=chat_list.join(",");
var data={};
data['group_ids']=chart_list_ids;
rspt_oneline_ajax_two=$.ajax({
                         url: '<?php echo base_url();?>User_chat/check_group_delete_rsptnew/',
                         type : 'GET',
                         data : data,
                         beforeSend : function(){
                            if(rspt_oneline_ajax_two != undefined ) {
                                 rspt_oneline_ajax_two.abort();
                                   }
                            },
                         success: function(data) {
                           var data = $.parseJSON(data);
                            $.each(data, function(index, element) {
                              if(element>0)
                              {
                                    $('.box_'+index).hide();
                                    $('#popup_group_edit_'+index).modal('hide');
                                    $('.chatbox_'+index).hide();
                              }
                              if(element < 1)
                              {
                                    $('.box_'+index).show();
                              }
                            });                             
                               
                            }                        
                     });
data['members']=JSON.stringify(chat_data_mem);
rspt_oneline_ajax_three=$.ajax({                      
                         url: '<?php echo base_url();?>User_chat/check_group_update_members_rsptnew/',
                         type : 'GET',
                         data : data,
                         beforeSend : function(){
                            if(rspt_oneline_ajax_three != undefined ) {
                                 rspt_oneline_ajax_three.abort();
                                   }
                            },
                         success: function(data) {
                            var data = $.parseJSON(data);
                            $.each(data, function(index, element) {
                              if(element!='wrong')
                              {
                                  $('.members_chat_'+index).attr('data-original-title',element);
                                  $('.box_'+index).attr('data-member',element);
                              }
                            });
                          //  console.log('check_group_update_members---'+data);
                             //    if(data!='wrong')
                             //    {
                             // $('.members_chat_'+ids).attr('data-original-title',data);
                             // $('.box_'+ids).attr('data-member',data);
                             //    }
                               
                            }                        
                     });
} // new
$('.group_chat_list').each(function(){
     chat_list.push($(this).attr('data-id'));
// console.log('each each');
// console.log($(this).attr('data-id'));
// console.log('end of each');
           var data = {};
           var ids=$(this).attr('data-id');
                     data['group_id']=$(this).attr('data-id');
                     // current_arr4[ids]=$.ajax({
                     //     url: '<?php echo base_url();?>User_chat_chart/check_group_delete/',
                     //     type : 'GET',
                     //     data : data,
                     //     beforeSend : function(){
                     //        if(current_arr4[ids] != undefined ) {
                     //             current_arr4[ids].abort();
                     //               }
                     //        },
                     //     success: function(data) {
                     //            if(data > 0)
                     //            {
                                  
                     //                $('.box_'+ids).hide();
                     //                $('#popup_group_edit_'+ids).modal('hide');
                     //                $('.chatbox_'+ids).hide();
                     //            }
                     //            if(data < 1)
                     //            {
                     //                $('.box_'+ids).show();
                                   
                     //            }
                               
                     //        }                        
                     // });
                     /** for group edit options **/
           // var data = {};
           // var ids=$(this).attr('data-id');
           //           data['group_id']=$(this).attr('data-id');
           //           data['group_members']=$(this).attr('data-member');
           //           current_arr5[ids]=$.ajax({                      
           //               url: '<?php echo base_url();?>User_chat_chart/check_group_update_members/',
           //               type : 'GET',
           //               data : data,
           //               beforeSend : function(){
           //                  if(current_arr5[ids] != undefined ) {
           //                       current_arr5[ids].abort();
           //                         }
           //                  },
           //               success: function(data) {
           //                //  console.log('check_group_update_members---'+data);
           //                      if(data!='wrong')
           //                      {
           //                   $('.members_chat_'+ids).attr('data-original-title',data);
           //                   $('.box_'+ids).attr('data-member',data);
           //                      }
                               
           //                  }                        
           //           });
                     /** end of edit option **/
});
/** for add new group **/
var currentXhr=null;
if(chat_list.length>0)
{
 var data = {};
           var chat_lists=chat_list.join(',');
                     data['group_ids']=chat_lists;
                     currentXhr7=$.ajax({
                         url: '<?php echo base_url();?>User_chat/check_group_added/',
                         type : 'GET',
                         data : data,
                          beforeSend : function()    {           
                            if(currentXhr7 != null) {
                                currentXhr7.abort();
                                  }
                              },
                         success: function(data) {
                             // console.log('---rspt'+data+'----rspt');
                                if(data!='wrong')
                                {
                                     $('.for_group_chat').html('');
          $('.for_group_chat').load("<?php echo base_url(); ?>User_chat/group_chat_lists");
          $('.edit_user_group_popup').html('');
          $('.edit_user_group_popup').load("<?php echo base_url(); ?>User_chat/edit_chatgroup_popup");
                                 // console.log('---2ndrspt'+data+'----rspt');
                                }
                               
                               
                            }
                        });
}

/** end of new group added **/
/** end of group chat and ajax load **/

/*** end of group chat data **/

}, 5000);
/** end group **/


});
   $(document).on('click','.input-value',function(){
       // alert($(this).attr('data-id'));
         var data = {};
         var it_id=$(this).attr('data-id');
         var data_group=$(this).attr('data-group');
         if(typeof data_group=="undefined")
         {
            data_group='single';
         }
         if(data_group=='single'){
               data['chat_id'] = $(this).attr('data-id');
             
                     $.ajax({
                   url: '<?php echo base_url();?>User_chat/chat_status_change/',
                   type : 'GET',
                   data : data,
                    beforeSend : function(){          
                   
                          },
                   success: function(data) {
                    $('.new_message_count_'+it_id).html('');
                   
                   },
                 });
              }
         if(data_group=='group'){
             data['chat_id'] = $(this).attr('data-id');
           
                  $.ajax({
                 url: '<?php echo base_url();?>User_chat/chat_status_change_forgroup/',
                 type : 'GET',
                 data : data,
                beforeSend : function(){          
                            
                          },
                 success: function(data) {
                  $('.new_message_count_'+it_id).html('');
                 
                 },
               });
            }
      });
      

   $(document).on('keypress','.input-value',function(e){
     if ( e.keyCode == 13 ) {  // detect the enter key
      // alert('zzz');
       //alert($(this).val());

        if($(this).val()!='')
          {
                
             //    $(this).closest('.input-group-btn').find("#paper-btn").trigger('click');
         $(this).parents('.write-msg').children('.input-group-btn').find('#paper-btn').trigger('click');
           // console.log($(this).parents('.write-msg').children('.input-group-btn').find('#paper-btn').trigger('click'));
           
          }
        }
   });

</script>