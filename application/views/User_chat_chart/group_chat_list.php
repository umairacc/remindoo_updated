
                                      <h6>Groups</h6>
                                      <a href="javascript:void(0)" class="create_group">+Create Group</a>
                                      <div class="chat_user_list" style="display:none;">
                                      <input type="text" name="group_name" id="group_name" placeholder="Group Name">
                                      <button class="btn btn-info add_create_group" type="button">Create</button>
                                      <?php if($_SESSION['roleId']!=6 && !empty($getallstaff)){ ?>
                                        <div class="user_list">
                                        <h5>Staff</h5>
                                          <?php  foreach ($getallstaff as $getallUser_key => $getallUser_value) { ?>
                                            <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                                              <a class="media-left" href="#!">
                                                <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                
                                              </a>

                                              <div class="media-body">
                                                <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                                 <input type="checkbox" class="for_group" name="our_group_data" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>">
                                              </div>

                                           
                                            </div>
                                          <?php } ?>
                                        </div>

                                        <?php
                                        } ?>
                                            <?php if(!empty($getallUser)){ ?>
                                        <div class="user_list">
                                        <h5>Client</h5>
                                          <?php  foreach ($getallUser as $getallUser_key => $getallUser_value) { ?>
                                            <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                                              <a class="media-left" href="#!">
                                                <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                
                                              </a>
                                            
                                              <div class="media-body">
                                                <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                                  <input type="checkbox" class="for_group" name="our_group_data" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>">
                                              </div>
                                           
                                            </div>
                                          <?php } ?>
                                        </div>

                                        <?php
                                        } ?>

                                        
                                      </div>
<!-- for group -->
<!-- for creaated users -->
                                      <div class="created_group_list">
                                      <?php $group_data=$this->db->query("SELECT * FROM chat_group where create_by=".$_SESSION['id']."  ")->result_array(); ?>
                                      <?php foreach ($group_data as $key => $value) {

                                        $group_member_data=$this->db->query("SELECT * FROM user where id in (".$value['members'].",".$value['create_by'].") ")->result_array();
                                        $member_names=array();
                                        foreach ($group_member_data as $men_key => $mem_value)
                                         {
                                            array_push($member_names, $mem_value['crm_name']);
                                         }                                    
                                       ?>
                                      <div <?php if($value['status']!=''){ ?> style="display: none;" <?php } ?> class="media group_chat_list userlist-box box_<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>" data-username="<?php echo $value['groupname']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $value['groupname']; ?>" data-group="group" data-member="<?php echo implode(',',$member_names);?>" data-creater="<?php echo $value['create_by']; ?>" data-session="<?php echo $_SESSION['id'];?>" >
                                     
                                      <div class="media-body">
                                        <div class="f-13 chat-header"><?php echo $value['groupname']; ?></div>
                                      </div>
                                      <div class="badge badge-primary new_message_count_<?php echo $value['id'];?>"></div>
                                    </div>
                                       <?php
                                      } ?>

                                      </div>
  <!-- for member of the group -->
                                        <div class="created_group_list">
                                      <?php $group_data=$this->db->query("SELECT * FROM chat_group where find_in_set('".$_SESSION['id']."',members)   ")->result_array(); ?>
                                      <?php foreach ($group_data as $key => $value) {

                                        $group_member_data=$this->db->query("SELECT * FROM user where id in (".$value['members'].",".$value['create_by'].") ")->result_array();
                                        $member_names=array();
                                        foreach ($group_member_data as $men_key => $mem_value)
                                         {
                                            array_push($member_names, $mem_value['crm_name']);
                                         }                                    
                                       ?>
                                      <div <?php if($value['status']!=''){ ?> style="display: none;" <?php } ?> class="media group_chat_list userlist-box box_<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>" data-username="<?php echo $value['groupname']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $value['groupname']; ?>" data-group="group" data-member="<?php echo implode(',',$member_names);?>" data-creater="<?php echo $value['create_by']; ?>" data-session="<?php echo $_SESSION['id'];?>" >
                                     
                                      <div class="media-body">
                                        <div class="f-13 chat-header"><?php echo $value['groupname']; ?></div>
                                      </div>
                                      <div class="badge badge-primary new_message_count_<?php echo $value['id'];?>"></div>
                                    </div>
                                       <?php
                                      } ?>

                                      </div>
<!-- end of chat group -->
           
                                    <!-- for admin and support -->
          <!-- end of group chat -->