<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');
   ?>
<!-- management block -->
<div class="pcoded-content">
   <div class="pcoded-inner-content chat-page">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!--  <div class="deadline-crm1 floating_set single-txt12">
               <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                  <li class="nav-item">
                     Deadline Manager
                     <div class="slide"></div>
                  </li>
               </ul>              
               </div> -->
            <div class="deadline-block">
               <div id="main-chat" class="container-fluid">
                  <!-- <div class="page-header card">
                     <div class="row align-items-end">
                       <div class="col-lg-8">
                         <div class="page-header-title">
                           <i class="icofont icofont-ui-text-chat bg-c-blue"></i>
                           <div class="d-inline">
                             <h4>Chat API</h4>
                             <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                           </div>
                         </div>
                       </div>
                       <div class="col-lg-4">
                         <div class="page-header-breadcrumb">
                           <ul class="breadcrumb-title">
                             <li class="breadcrumb-item">
                               <a href="#!">
                               <i class="icofont icofont-home"></i>
                               </a>
                             </li>
                             <li class="breadcrumb-item"><a href="#!">Pages</a>
                             </li>
                             <li class="breadcrumb-item"><a href="#!">Sample page</a>
                             </li>
                           </ul>
                         </div>
                       </div>
                     </div>
                     </div> -->
                  <div class="page-body">
                     <div class="row">
                        <div class="chat-box">
                           <ul class="text-right boxs">
                              <!--           <li class="chat-single-box card-shadow bg-white active" data-id="1">
                                 <div class="had-container">
                                   <div class="chat-header p-10 bg-gray">
                                     <div class="user-info d-inline-block f-left">
                                       <div class="box-live-status bg-danger  d-inline-block m-r-10"></div>
                                       <a href="#">Josephin Doe</a>
                                     </div>
                                     <div class="box-tools d-inline-block">
                                       <a href="#" class="mini">
                                       <i class="icofont icofont-minus f-20 m-r-10"></i>
                                       </a>
                                       <a class="close" href="#">
                                       <i class="icofont icofont-close f-20"></i>
                                       </a>
                                     </div>
                                   </div>
                                   <div class="chat-body p-10">
                                     <div class="message-scrooler">
                                       <div class="messages">
                                         <div class="message out no-avatar media">
                                           <div class="body media-body text-right p-l-50">
                                             <div class="content msg-reply f-12 bg-primary d-inline-block">Good morning..</div>
                                             <div class="seen">
                                               <i class="icon-clock f-12 m-r-5 txt-muted d-inline-block"></i><span>a few seconds ago </span>
                                               <div class="clear"></div>
                                             </div>
                                           </div>
                                           <div class="sender media-right friend-box">
                                             <a href="javascript:void(0);" title="Me"><img src="<?php echo base_url();?>assets/images/avatar-3.jpg" class="  img-chat-profile" alt="Me"></a>
                                           </div>
                                         </div>
                                       </div>
                                     </div>
                                   </div>
                                   <div class="chat-footer b-t-muted">
                                     <div class="input-group write-msg">
                                       <input type="text" class="form-control input-value" placeholder="Type a Message">
                                       <span class="input-group-btn">
                                       <button id="paper-btn" class="btn btn-primary" type="button">
                                       <i class="icofont icofont-paper-plane"></i>
                                       </button>
                                       </span>
                                     </div>
                                   </div>
                                 </div>
                                 </li> -->
                           </ul>
                           <div id="sidebar" class="users p-chat-user chat_particular">
                              <div class="had-container">
                                 <div class="card card_main p-fixed users-main ">
                                    <div class="user-box">
                                       <div class="card-block">
                                          <div class="right-icon-control">
                                             <input type="text" class="form-control  search-text" placeholder="Search Friend">
                                             <div class="form-icon">
                                                <i class="icofont icofont-search"></i>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="user-groups">
                                          <h6>Groups</h6>
                                          <a href="javascript:void(0)" class="create_group"> Create Group <i class="fa fa-plus" aria-hidden="true"></i></a>
                                          <div class="chat_user_list" style="display:none;">
                                            <div class="group-search">
                                             <input type="text" name="group_name" id="group_name" placeholder="Group Name">
                                             <button class="btn btn-danger add_create_group" type="button">Create</button>
                                            </div>
                                             <?php if($_SESSION['roleId']!=6 && !empty($getallstaff)){ ?>
                                             <div class="user_list">
                                                <h5>Staff</h5>
                                                <?php  foreach ($getallstaff as $getallUser_key => $getallUser_value) { ?>
                                                <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                                                   <a class="media-left" href="#!">
                                                   <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                   </a>
                                                   <div class="media-body">
                                                      <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                                      <div class="checkforgroup checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="for_group" name="our_group_data" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>">
                                                      <label for="our_group_data_<?php echo $getallUser_value['id'];?>"></label>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                             </div>
                                             <?php
                                                } ?>
                                             <?php if(!empty($getallUser)){ ?>
                                             <div class="user_list">
                                                <h5>Client</h5>
                                                <?php  foreach ($getallUser as $getallUser_key => $getallUser_value) { ?>
                                                <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                                                   <a class="media-left" href="#!">
                                                   <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                   </a>
                                                   <div class="media-body">
                                                      <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                                      <div class="checkforgroup checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="for_group" name="our_group_data" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>">
                                                      <label for="our_group_data_<?php echo $getallUser_value['id'];?>"></label>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                             </div>
                                             <?php
                                                } ?>
                                          </div>
                                          <!-- for group -->
                                          <!-- for creaated users -->
                                          <div class="created_group_list">
                                             <?php $group_data=$this->db->query("SELECT * FROM chat_group where create_by=".$_SESSION['id']." and status='' ")->result_array(); ?>
                                             <?php foreach ($group_data as $key => $value) {
                                                $group_member_data=$this->db->query("SELECT * FROM user where id in (".$value['members'].",".$value['create_by'].") ")->result_array();
                                                $member_names=array();
                                                foreach ($group_member_data as $men_key => $mem_value)
                                                 {
                                                    array_push($member_names, $mem_value['crm_name']);
                                                 }                                    
                                                ?>
                                             <div class="media userlist-box box_<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>" data-username="<?php echo $value['groupname']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $value['groupname']; ?>" data-group="group" data-member="<?php echo implode(',',$member_names);?>" data-creater="<?php echo $value['create_by']; ?>" data-session="<?php echo $_SESSION['id'];?>" >
                                                <div class="media-body">
                                                   <div class="f-13 chat-header"><?php echo $value['groupname']; ?></div>
                                                </div>
                                                <div class="badge badge-primary new_message_count_<?php echo $value['id'];?>"></div>
                                             </div>
                                             <?php
                                                } ?>
                                          </div>
                                          <!-- for member of the group -->
                                          <div class="created_group_list">
                                             <?php $group_data=$this->db->query("SELECT * FROM chat_group where find_in_set('".$_SESSION['id']."',members)  and status='' ")->result_array(); ?>
                                             <?php foreach ($group_data as $key => $value) {
                                                $group_member_data=$this->db->query("SELECT * FROM user where id in (".$value['members'].",".$value['create_by'].") ")->result_array();
                                                $member_names=array();
                                                foreach ($group_member_data as $men_key => $mem_value)
                                                 {
                                                    array_push($member_names, $mem_value['crm_name']);
                                                 }                                    
                                                ?>
                                             <div class="media userlist-box box_<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>" data-username="<?php echo $value['groupname']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $value['groupname']; ?>" data-group="group" data-member="<?php echo implode(',',$member_names);?>" data-creater="<?php echo $value['create_by']; ?>" data-session="<?php echo $_SESSION['id'];?>" >
                                                <div class="media-body">
                                                   <div class="f-13 chat-header"><?php echo $value['groupname']; ?></div>
                                                </div>
                                                <div class="badge badge-primary new_message_count_<?php echo $value['id'];?>"></div>
                                             </div>
                                             <?php
                                                } ?>
                                          </div>
                                          <!-- end of chat group -->
                                          <!-- <ul>
                                             <li class="frnds">Friends</li>
                                             <li class="work">Work</li>
                                             </ul> -->
                                       </div>
                                       <!-- for admin and support -->
                                       <?php
                                          // $_SESSION['roleId']=1;
                                            if($_SESSION['roleId']==4 || $_SESSION['roleId']==6){ ?>
                                       <div class="user-groups">
                                          <h6>For Support</h6>
                                          <!-- for staff -->
                                          <?php
                                             $i=1;
                                             
                                              foreach ($getalladmin as $getallUser_key => $getallUser_value) {
                                              
                                               $user_login=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Login'  order by id DESC")->result_array();
                                             if(!empty($user_login))
                                             {
                                             $login_time=$user_login[0]['CreatedTime'];
                                                $user_logout=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Logout'  order by id DESC")->result_array();
                                             if(!empty($user_logout))
                                             {
                                                $logout_time=$user_logout[0]['CreatedTime'];
                                             }
                                             else
                                             {
                                               $logout_time=0;
                                             }
                                                $status="";
                                                if($login_time>$logout_time){
                                                  if(date('Y-m-d', $login_time)==date('Y-m-d'))
                                                  {
                                                   $status="online";
                                                    $status_color="bg-success";
                                                  }
                                                  else
                                                  {
                                                   $status="offline";
                                                    $status_color="bg-danger";
                                                  }
                                                }
                                                else
                                                {
                                                 $status="offline";
                                                  $status_color="bg-danger";
                                                }
                                             }
                                             else
                                             {
                                             $status="offline";
                                             $status_color="bg-danger";
                                             }
                                             //echo $status;
                                               ?>
                                          <div class="media userlist-box box_<?php echo $getallUser_value['id'];?>" data-id="<?php echo $getallUser_value['id'];?>" data-status="<?php echo $status;?>" data-username="<?php echo $getallUser_value['crm_name']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $getallUser_value['crm_name']; ?>">
                                             <a class="media-left" href="#!">
                                                <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                <div class="live-status <?php echo $status_color;?>"></div>
                                             </a>
                                             <div class="media-body">
                                                <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                             </div>
                                             <div class="badge badge-primary new_message_count_<?php echo $getallUser_value['id'];?>"></div>
                                          </div>
                                          <?php 
                                             $i++;
                                             } ?>
                                          <!-- end of staff -->
                                       </div>
                                       <!-- for client -->
                                       <?php } ?>
                                       <?php if($_SESSION['roleId']!=6 && !empty($getallstaff)){ ?>
                                       <!-- end of admin support -->
                                       <div class="user-groups">
                                          <h6>Staff</h6>
                                          <!-- for staff -->
                                          <?php
                                             $i=1;
                                             
                                              foreach ($getallstaff as $getallUser_key => $getallUser_value) {
                                              
                                               $user_login=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Login'  order by id DESC")->result_array();
                                             if(!empty($user_login))
                                             {
                                             $login_time=$user_login[0]['CreatedTime'];
                                                $user_logout=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Logout'  order by id DESC")->result_array();
                                             if(!empty($user_logout))
                                             {
                                                $logout_time=$user_logout[0]['CreatedTime'];
                                             }
                                             else
                                             {
                                               $logout_time=0;
                                             }
                                                $status="";
                                                if($login_time>$logout_time){
                                                  if(date('Y-m-d', $login_time)==date('Y-m-d'))
                                                  {
                                                   $status="online";
                                                    $status_color="bg-success";
                                                  }
                                                  else
                                                  {
                                                   $status="offline";
                                                    $status_color="bg-danger";
                                                  }
                                                }
                                                else
                                                {
                                                 $status="offline";
                                                  $status_color="bg-danger";
                                                }
                                             }
                                             else
                                             {
                                             $status="offline";
                                             $status_color="bg-danger";
                                             }
                                             //echo $status;
                                               ?>
                                          <div class="media userlist-box box_<?php echo $getallUser_value['id'];?>" data-id="<?php echo $getallUser_value['id'];?>" data-status="<?php echo $status;?>" data-username="<?php echo $getallUser_value['crm_name']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $getallUser_value['crm_name']; ?>">
                                             <a class="media-left" href="#!">
                                                <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                <div class="live-status <?php echo $status_color;?>"></div>
                                             </a>
                                             <div class="media-body">
                                                <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                             </div>
                                             <div class="badge badge-primary new_message_count_<?php echo $getallUser_value['id'];?>"></div>
                                          </div>
                                          <?php 
                                             $i++;
                                             } ?>
                                          <!-- end of staff -->
                                       </div>
                                       <?php } ?>
                                       <!-- for client -->
                                       <?php if(!empty($getallUser)){ ?>
                                       <div class="user-groups">
                                          <h6>Client</h6>
                                          <?php
                                             $i=1;
                                             
                                              foreach ($getallUser as $getallUser_key => $getallUser_value) {
                                              
                                               $user_login=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Login'  order by id DESC")->result_array();
                                             if(!empty($user_login))
                                             {
                                             $login_time=$user_login[0]['CreatedTime'];
                                                $user_logout=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Logout'  order by id DESC")->result_array();
                                                $logout_time=$user_logout[0]['CreatedTime'];
                                                $status="";
                                                if($login_time>$logout_time){
                                                  if(date('Y-m-d', $login_time)==date('Y-m-d'))
                                                  {
                                                   $status="online";
                                                    $status_color="bg-success";
                                                  }
                                                  else
                                                  {
                                                   $status="offline";
                                                    $status_color="bg-danger";
                                                  }
                                                }
                                                else
                                                {
                                                 $status="offline";
                                                  $status_color="bg-danger";
                                                }
                                             }
                                             else
                                             {
                                             $status="offline";
                                             $status_color="bg-danger";
                                             }
                                             //echo $status;
                                               ?>
                                          <div class="media userlist-box box_<?php echo $getallUser_value['id'];?>" data-id="<?php echo $getallUser_value['id'];?>" data-status="<?php echo $status;?>" data-username="<?php echo $getallUser_value['crm_name']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $getallUser_value['crm_name']; ?>">
                                             <a class="media-left" href="#!">
                                                <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                <div class="live-status <?php echo $status_color;?>"></div>
                                             </a>
                                             <div class="media-body">
                                                <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                             </div>
                                             <div class="badge badge-primary new_message_count_<?php echo $getallUser_value['id'];?>"></div>
                                          </div>
                                          <?php 
                                             $i++;
                                             } ?>
                                       </div>
                                       <!-- for client -->
                                       <?php } ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="page-error">
                        <div class="card text-center">
                           <div class="card-block">
                              <div class="m-t-10">
                                 <i class="icofont icofont-warning text-white bg-c-yellow"></i>
                                 <h4 class="f-w-600 m-t-25">Not supported</h4>
                                 <p class="text-muted m-b-0">Chat not supported in this device</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<!-- Modal -->
<?php 
   $group_data=$this->db->query("SELECT * FROM chat_group where create_by=".$_SESSION['id']."  and status='' ")->result_array(); ?>
<?php foreach ($group_data as $key => $value) {
   $ex_members=explode(',',$value['members']);
   
                                         $group_member_data=$this->db->query("SELECT * FROM user where id in (".$value['members'].",".$value['create_by'].") ")->result_array();
                                         $member_names=array();
                                         foreach ($group_member_data as $men_key => $mem_value)
                                          {
                                             array_push($member_names, $mem_value['crm_name']);
                                          }                                    
                                        ?>
<div class="modal fade recurring-msg common-schedule-msg1" id="popup_group_edit_<?php echo $value['id']; ?>" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Change Group Option</h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-sm-6 form-group common-repeats1 ">
                  <label class="label-column1 col-form-label">Group Name:</label>
                  <div class="label-column2 input-group date">
                     <input type='text' class="" id='group_name_<?php echo $value['id'];?>' name="group_name" value="<?php echo $value['groupname'];?>" />
                  </div>
                  <!-- members list -->
                  <div class="chat_user_list">
                     <?php if($_SESSION['roleId']!=6 && !empty($getallstaff)){ ?>
                     <div class="user_list">
                        <h5>Staff</h5>
                        <?php  foreach ($getallstaff as $getallUser_key => $getallUser_value) { ?>
                        <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                           <a class="media-left" href="#!">
                           <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                           </a>
                           <div class="media-body">
                              <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                              <input type="checkbox" class="for_group" name="our_group_data_<?php echo $value['id'];?>" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>" <?php if(in_array($getallUser_value['id'],$ex_members)){ echo "checked"; } ?>  >
                           </div>
                        </div>
                        <?php } ?>
                     </div>
                     <?php
                        } ?>
                     <?php if(!empty($getallUser)){ ?>
                     <div class="user_list">
                        <h5>Client</h5>
                        <?php  foreach ($getallUser as $getallUser_key => $getallUser_value) { ?>
                        <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                           <a class="media-left" href="#!">
                           <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                           </a>
                           <div class="media-body">
                              <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                              <input type="checkbox" class="for_group" name="our_group_data_<?php echo $value['id'];?>" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>" <?php if(in_array($getallUser_value['id'],$ex_members)){ echo "checked"; } ?> >
                           </div>
                        </div>
                        <?php }  ?>
                     </div>
                     <?php
                        } ?>
                  </div>
                  <!-- end -->
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button class="btn btn-info edit_create_group" data-id="<?php echo $value['id'];?>" type="button">Update</button>
            <button type="button" class="btn btn-default delete_group" data-id="<?php echo $value['id'];?>" >Delete Group</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<!-- modal close-->
<!-- management block -->
<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/js/mmc-common.js"></script> -->
<!--   <script src="<?php echo base_url();?>assets/js/mmc-chat.js"></script> -->
<?php $this->load->view('User_chat/mmc-common_js');?>
<?php $this->load->view('User_chat/mmc-chat_js');?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/chat.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
       $('.edit-toggle1').click(function(){
         $('.proposal-down').slideToggle(300);
       });
   
        $("#legal_form12").change(function() {
          var val = $(this).val();
          if(val === "from2") {
            // alert('hi');
              $(".from2option").show();
          }
          else {
              $(".from2option").hide();
          }
          });
   
        $("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({ 
         //  "scrollX": true
         });
   })
</script>
<script type="text/javascript">
   $('.create_group').on('click',function(){
       //$('.chat_user_list').show();
        $('.chat_user_list').toggle();
   });
   $('.add_create_group').on('click',function(){
     var group_name=$('#group_name').val();
     if(group_name=='')
     {
       alert('Enter Group Name');
     }
     else
     {
         var checkboxValues = [];
     $('input[name="our_group_data"]:checked').each(function(index, elem) {
         checkboxValues.push($(elem).val());
     });
       if(checkboxValues.length>0){
         //alert(checkboxValues.join(', '));
          var data = {};
          data['name']=group_name;
    data['member'] =checkboxValues.join(',');
   $('.LoadingImage').show();
          $.ajax({
        url: '<?php echo base_url();?>User_chat/create_group/',
        type : 'POST',
        data : data,
        success: function(data) {
         //alert(data);
         $('.LoadingImage').hide();
         console.log(data);
         if(data!='wrong'){
           $('.created_group_list').append(data);
           $('.chat_user_list').hide();
         }
   
        }
      });
       }
       else
       {
         alert('Choose Any checkbox ');
       }
     
     }
   
   });
   
   $('.group-info').on('click',function(){
   // alert('zzzz');
   //   $('.rsptrspt').html('zzzz');
   });
   
   $('.edit_create_group').on('click',function(){
   
     var id=$(this).attr('data-id');
        var group_name=$('#group_name_'+id).val();
     if(group_name=='')
     {
       alert('Enter Group Name');
     }
     else
     {
         var checkboxValues = [];
     $('input[name="our_group_data_'+id+'"]:checked').each(function(index, elem) {
         checkboxValues.push($(elem).val());
     });
       if(checkboxValues.length>0){
         //alert(checkboxValues.join(', '));
          var data = {};
          data['name']=group_name;
    data['member'] =checkboxValues.join(',');
   $('.LoadingImage').show();
          $.ajax({
        url: '<?php echo base_url();?>User_chat/update_group/'+id,
        type : 'POST',
        data : data,
        success: function(data) {
         //alert(data);
         $('.LoadingImage').hide();
         //alert(data);
         location.reload();
         // console.log(data);
         // if(data!='wrong'){
         //   $('.created_group_list').append(data);
         //   $('.chat_user_list').hide();
         // }
   
        }
      });
       }
       else
       {
         alert('Choose Any checkbox ');
       }
     
     }
   
   });
   
   $(document).on('click','.delete_group',function(){
     var group_id=$(this).attr('data-id');
           var data = {};
          data['group_id']=group_id;
   $('.LoadingImage').show();
          $.ajax({
        url: '<?php echo base_url();?>User_chat/delete_group_chat/'+group_id,
        type : 'POST',
        data : data,
        success: function(data) {
        
         $('.LoadingImage').hide();     
         $('.box_'+group_id).hide();
         $('#popup_group_edit_'+group_id).modal('hide');
         $('.chatbox_'+group_id).hide();
         $('.modal-backdrop.show').hide();
        }
      });
   
   });
   
</script>