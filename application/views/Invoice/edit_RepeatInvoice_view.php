<?php 

$this->load->view('includes/header');

$cur_symbols = $this->Common_mdl->getallrecords('crm_currency');

foreach ($cur_symbols as $key => $value) 
{
   $currency_symbols[$value['country']] = $value['currency'];
}

?>

<style type="text/css">
  .euro-position span { font-size: 12px; }
</style>
 <div class="modal-alertsuccess alert  succ popup_info_box" style="display:none;">
  <div class="newupdate_alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              </div>
         </div>
         </div>
   </div>


<!-- management block -->
<div id="print_preview">
<div class="pcoded-content  new-upload-03">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper new-invoice">
         <div class="deadline-crm1 floating_set invoice-align1">
            <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard page-tabs">
                    <li class="nav-item"> <a href="#">Repeat Invoice</a> </li>
        </ul>
        </div>
        <?php 
          echo $this->session->flashdata('invoice_success');
        ?>  

            <div class="firm-field floating_set">
              <form id="invoice_form" name="invoice_form" action="<?php echo base_url()?>invoice/UpdateRepeatInvoice" method="post">    
              <input type="hidden" name="repeat_clientID" value="<?php echo $repeatinvoice_info['client_id']; ?>">
               <div class="invoice-details repeat-invoice">
                     <div class="invoice-top  repeat-quote1">
                         <span class="invoice-topinner side-inputbox01">
                            <label>repeat this transaction every</label>
                            <input type="text" name="repeat_trans" class="date-col" value="<?php echo $repeatdate_info['repeats']; ?>">
                            <select name="select_transaction">
                            <?php $date_info = $repeatdate_info['transaction_every']; ?>
                               <option value="0" <?php if($date_info == 'Week'){ echo "selected='selected'";} ?> >week(s)</option>
                               <option value="1" <?php if($date_info == 'Month'){ echo "selected='selected'";} ?> >month(s)</option>
                               <option value="2" <?php if($date_info == 'Year'){ echo "selected='selected'";} ?> >year(s)</option>
                            </select>
                         </span>
                         <span class="invoice-topinner">
                            <label>invoice date</label>
                            <input type="text" name="invoice_startdate" class="datepicker" data-parsley-required="true" value="<?php echo date('Y-m-d', strtotime($repeatdate_info['invoice_startdate'])) ?>">
                            <!-- <select>
                               <option>19 Feb 2018</option>
                               <option>19 Feb 2018</option>
                            </select> -->
                         </span>
                         <span class="invoice-topinner side-inputbox01">
                            <label>due date <strong>due</strong></label>
                            <input type="text" name="duedate_no" class="date-col" value="<?php echo $repeatdate_info['due_days']; ?>">
                            <select name="repeat_days">
                              <?php $payment = $repeatdate_info['payment_due']; ?>
                               <option value="1" <?php if($payment == 'day(s) after the invoice date'){ echo "selected='selected'";} ?> >day(s) after the invoice date</option>
                               <option value="2">19 Feb 2018</option>
                            </select>
                         </span>

                         <span class="invoice-topinner">
                            <label>end date (Optional)</label>
                            <input type="text" name="invoice_enddate" class="datepicker" value="<?php if($repeatdate_info['end_date'] != '0000-00-00 00:00:00' && date('Y', strtotime($repeatdate_info['end_date'])) != '1970'){ echo date('Y-m-d', strtotime($repeatdate_info['end_date'])); } ?>">
                            <!-- <select>
                               <option>19 Mar 2018</option>
                               <option>19 Mar 2018</option>
                            </select>  -->
                         </span>
                         <div class="reinvoice-date">
                            <div class="radio-option">
                               <span class="radio invoice-option">
                               <div class="form-group cus-permission radio_button01 ">
                                  <div class="radio radio-inline">
                                  <?php $save_draft = $repeatinvoice_info['save_as_draft']; ?>
                                    <label><input type="radio"  name="save_as_draft" value="0" <?php if($save_draft == '0'){ echo 'checked'; } ?> id="cus-per"  ><i class="helper"></i>save as draft</label>
                                  </div>
                                </div>
                               </span>
                               <span class="invoice-option">
                               <div class="form-group cus-permission radio_button01 ">
                                  <div class="radio radio-inline">
                                  <?php $approve = $repeatinvoice_info['approve']; ?>
                                    <label><input type="radio"  name="payment_status" value="approve" <?php if($approve == 'approve'){ echo 'checked'; } ?> id="cus-per"><i class="helper"></i>Approve</label>                                
                                  </div>
                                </div>
                               </span>
                               <span class="invoice-option">
                               <div class="form-group cus-permission radio_button01 ">
                                  <div class="radio radio-inline">
                                     <label><input type="radio"  name="payment_status" value="decline" <?php if($approve == 'decline'){ echo 'checked'; } ?> id="cus-per"><i class="helper"></i>Decline</label>
                                   </div>
                              </div>
                               </span>
                               <span class="invoice-option">
                               <div class="form-group cus-permission radio_button01 ">
                                  <div class="radio radio-inline">                                      
                                     <label><input type="radio"  name="payment_status" value="pending" <?php if($approve == 'pending'){ echo 'checked'; } ?> id="cus-per"><i class="helper"></i>Pending</label>
                                  </div>
                                </div>
                               </span>
                               <span class="invoice-option">
                               <div class="form-group cus-permission radio_button01 ">
                                  <div class="radio radio-inline">
                                  <?php $app_send = $repeatinvoice_info['approve_send']; ?>
                                    <label><input type="radio"  name="approve_send" value="2" <?php if($app_send == '2') { echo 'checked'; } ?> id="cus-per" ><i class="helper"></i>Approve For Sending</label>
                                  </div>
                                </div>
                               </span>
                            </div>
                            <span class="invoice-no">Invoice no. will be set on Invoice creation date.</span>
                         </div>
                         <!-- reinvoice-date -->
                      </div>
                      <div class="add-plus12 repeat-refer">
                      <div class="invoice-ref">
                         <span class="invoice-topinner">
                         <label>invoice to</label>
                         <select  name="invoice_to" placeholder="Select User" data-parsley-required="true">
                               
                           <?php 

                           $client = $this->db->query("SELECT user.*,client.crm_email FROM user LEFT JOIN client ON user.id = client.user_id where user.autosave_status!='1' and user.crm_name!='' AND user.id = '".$repeatinvoice_info['client_email']."'")->row_array();
                           ?>
                                <option value="">Select client </option>
                                 <?php                                

                                $contact_mail=$this->db->query("select main_email from client_contacts where client_id=".$client['id']." and make_primary=1 limit 1")->row_array();
                                
                                if($contact_mail['main_email'] != '')
                                {
                            ?>   
                            <option value=<?php echo $client['id'];?> selected><?php echo $contact_mail['main_email']; ?></option>
                          <?php }
                                else if($client['crm_email_id'] != '')
                                { ?>
                            <option value=<?php echo $client['id'];?> selected><?php echo $client['crm_email_id']; ?></option>

                     <?php      } 
                                else if($client['crm_email'] != '')
                                { ?>
                            <option value=<?php echo $client['id'];?> selected><?php echo $client['crm_email']; ?></option>

                     <?php      } ?>
                            </select>
                       </span>
                         <input type="hidden" name="to_email">
                         <span class="invoice-topinner">
                         <label>reference</label>
                         <input type="text" name="reference" value="<?php echo $repeatinvoice_info['reference']; ?>">
                         </span>
                      </div>

                     <div class="amounts-detail">
                        <label>amounts are</label>
                        <select name="amounts_details" class="select_tax" id="select_tax">
                          <?php $amt_type1 = $repeatinvoice_info['amounts_type']; if($amt_type1 == 'Tax Exclusive'){ $amt_type_val = '1'; }else if($amt_type1 == 'Tax Inclusive'){ $amt_type_val = '2'; }?>
                           <option value="1" <?php if($amt_type1 == 'Tax Exclusive') { echo "selected='selected'"; } ?>>Tax Exclusive</option>
                           <option value="2" <?php if($amt_type1 == 'Tax Inclusive') { echo "selected='selected'"; } ?>>Tax Inclusive</option>
                        </select>
                     </div>
                     </div>

                     <div class="invoice-table floating_set">
                      <div class="invoice-realign1 floating_set">
                        <div class="table-responsive">
                           <table class="invoice_table" id="invoice_table">
                              <thead>
                                 <th></th>
                                 <th>item</th>
                                 <th>description</th>
                                 <th>qty</th>
                                 <th>unit price</th>
                                 <th>disc%</th>
                                 <th>account</th>
                              <?php //if($amt_type1 == 'Tax Exclusive') { ?>  
                                 <th class="th_tax_rate" id="th_tax_rate">tax rate</th>
                                 <th class="th_tax_amount" id="th_tax_amount">tax amount</th>
                              <?php //} ?>
                                 <th>amount <?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></th>
                                 <th></th>
                                 <th></th>
                              </thead>

                              <tfoot class="ex_data1">
<tr>
<th>
</th>
</tr>
</tfoot>

                              <tbody>

                    <?php  $sno = 0;  $m = 0; $send_status = $repeatinvoice_info['send_statements'];
                  foreach ($repeatproduct_info as $key => $value) {  $m = $m + 1; ?>
                      
                    <tr class="default_row" id="default_row<?php echo $value['id']?>">
                      <td><span id="sr_no" class="sr_no"><?php echo ++$sno; ?></span></td>
                       <td><input <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="item_name[]" data-parsley-required="true" data-parsley-error-message="Item is required"'; } ?> id="item_name<?php echo $m; ?>" class="form-control input-sm item_name" value="<?php echo $value['item'];?>" type="text">
                       </td>                     
                      <td><textarea <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="description[]" data-parsley-required="true" data-parsley-error-message="Description is required"'; } ?> id="description<?php echo $m; ?>" data-srno="1" class="form-control input-sm description"><?php echo $value['description'];?></textarea> </td>

                      <td><input type="number" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="quantity[]" data-parsley-required="true" data-parsley-type="number"'; } ?> id="quantity<?php echo $m; ?>" data-srno="1" class="form-control input-sm  quantity decimal_qty" maxlength="4" value="<?php echo $value['quantity'];?>"></td>

                      <td><input type="text" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="unit_price[]" data-parsley-required="true" data-parsley-type="number"'; } ?> id="unit_price<?php echo $m; ?>" data-srno="1" class="form-control input-sm unit_price decimal" maxlength="6" value="<?php echo $value['unit_price'];?>" /></td>

                      <td><input type="text" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="discount[]" data-parsley-required="true" data-parsley-type="number"'; } ?> id="discount<?php echo $m; ?>" data-srno="1" class="form-control input-sm discount decimal" maxlength="4" value="<?php echo $value['discount'];?>"></td>

                      <td><input type="text" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="account[]" data-parsley-required="true" data-parsley-error-message="Account is required"'; } ?> id="account<?php echo $m; ?>" data-srno="1" class="form-control input-sm account decimal" value="<?php echo $value['account'];?>"></td>

                  <?php  //if($amt_type == 'Tax Exclusive') { ?>
                      <td class="td_tax_rate" id="td_tax_rate"><input type="text" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="tax_rate[]" data-parsley-required="true" data-parsley-type="number"'; } ?> id="tax_rate<?php echo $m; ?>" data-srno="1" maxlength="4" class="form-control input-sm tax_rate decimal" value="<?php echo $value['tax_rate'];?>" ></td>

                      <td class="td_tax_amount" id="td_tax_amount<?php echo $m; ?>"><input type="text" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="tax_amount[]"'; } ?>  id="tax_amount1" data-srno="1" class="form-control input-sm tax_amount hidden_tax_amount<?php echo $value['id']?>"  value="<?php echo $value['tax_amount'];?>"/></td>
                    
                  <?php //} ?>  
                      
                      <td><input type="text" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="amount_gbp[]"'; } ?> id="amount_gbp<?php echo $m; ?>" data-srno="1" class="form-control input-sm amount_gbp hidden_amount_gbp<?php echo $value['id']?>"  value="<?php echo $value['amount_gbp'];?>"/ ></td>

                      <!-- <td></td> -->
                      <td><button type="button" name="default_remove_row" id="<?php echo $value['id'];?>" class="btn btn-danger btn-xs default_remove_row" aria-hidden="true" <?php if($send_status == 'send'){ echo "disabled"; } ?>>x</button></td>              
                     
                        
                <!-- <td><input type="hidden" name="act_amt" id="actual_amount1" data-srno="1" class="data-srno="1" class="form-control input-sm actual_amount"> </td>   -->  

                      <input type="hidden" name="primaryID" value="<?php echo $value['id']; ?>"> 
                      <input type="hidden" name="productID[]" value="<?php echo $value['client_id']; ?>"> 

                      <td>
                        
                         <div class="add-newline addmore">
                              <input type="button" class="btn btn-card btn-primary" id="addmore" value="add new line">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                           </div>

                      </td>
                    </tr>
                  <?php } ?>
                            </tbody>   
                    </table> 
                          
                        </div>
                      </div>
                        

                          <div class="cmn-invoice-approve floating_set"> 

                        <div class="right-invoice-new1">

                           <div class="sub-tot_table" id="sub-tot_table">
                              <table id="sub_tot_table" class="sub_tot_table">
                                 <tbody>
                                    <tr>
                                       <th>sub total</th>
                                       <td>

                                         <div class="euro-position">
                                        <strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong><input type="text" name="sub_tot" id="sub_total" class="form-control input-sm txtcal sub_total" value="<?php echo $repeatamount_info['sub_total']; ?>" readonly ></div></td>
                                    </tr>
                                  <?php  if($amt_type1 == 'Tax Exclusive') { ?>    
                                    <tr id="vat_row" class="vat_row">
                                       <th>VAT</th>
                                       <td><input type="text" name="value_at_tax" id="value_at_tax" class="form-control input-sm txtcal value_at_tax" value="<?php echo $repeatamount_info['VAT']; ?>" readonly></td>
                                    </tr>
                                  <?php } ?> 
                                    <tr>
                                       <th>includes adjustments to Tax</th>
                                       <td><input type="text" name="adjust_tax" id="adjust_tax" class="form-control input-sm adjust_tax" value="<?php echo $repeatamount_info['adjust_to_tax']; ?>" ></td>
                                    </tr>
                                    <tr class="grand-total">
                                       <th>total</th>
                                       <td>

                                         <div class="euro-position">
                                        <strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong>
                                        <input type="text" name="grand_total" id="grand_total" class="form-control input-sm grand_total" value="<?php echo $repeatamount_info['grand_total']; ?>" readonly>
                                      </div>

                                      </td>


                                    <input type="hidden" name="grandtotal_hidden" id="grandtotal_hidden" class="form-control input-sm grandtotal_hidden" value="0.00">   
                                       
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                         </div>

                      <div class="attach-invoice1">
                     <div class="save-approve" id="save-approve">
                       
                          
                        <div class="approve-btn">
                            <div class="table-savebtn">
                            <input type="hidden" name="total_item" class="total_item" id="total_item" value="1">
                            <a href="#"  class="preview-re">Preview placeholders</a>
                            <input type="submit" class="btn btn-card btn-primary" id="update_repeat_form"  value="save">
                            </div>
                           <!-- <input type="button" class="approve" value="approve"> -->
                           <a href="#" onclick="return reset_form()" class="cancel-btn">cancel</a> 
                        </div>
                     </div>
                   </div>
                     </div><!--invoice table -->

               </div>
            </form>   
            </div>
         </div>
      </div>
   </div>
</div>
</div>

<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
   <h1>Warning!!</h1>
   <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
   <div class="iew-container">
      <ul class="iew-download">
         <li>
            <a href="http://www.google.com/chrome/">
               <img src="assets/images/browser/chrome.png" alt="Chrome">
               <div>Chrome</div>
            </a>
         </li>
         <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
               <img src="assets/images/browser/firefox.png" alt="Firefox">
               <div>Firefox</div>
            </a>
         </li>
         <li>
            <a href="http://www.opera.com">
               <img src="assets/images/browser/opera.png" alt="Opera">
               <div>Opera</div>
            </a>
         </li>
         <li>
            <a href="https://www.apple.com/safari/">
               <img src="assets/images/browser/safari.png" alt="Safari">
               <div>Safari</div>
            </a>
         </li>
         <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
               <img src="assets/images/browser/ie.png" alt="">
               <div>IE (9 & above)</div>
            </a>
         </li>
      </ul>
   </div>
   <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->


<!-- Modal -->

<!-- Modal -->

  <div class="modal fade modal-invoice-quote1" id="temp_invoice" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Invoice</h4>
        </div>
        <div class="modal-body">
        <form action="#" method="post">
          <div id="generate_invoice" class="invoice-details generate-invoice">
               <!-- <div class="update-logo">
                    <input type="file">
                    <div class="up-logo">
                    </div>
                  </div> -->
                  <div class="online-payments">
                    <div class="online-qut">
                    <div class="temp-edit">
                      <h3>Remindoo.org</h3>
                    </div>
                    <div class="payment-table newlayout">                        
                            <h3><?php   if(isset($admin_settings['crm_currency'])){ echo $currency_symbols[$admin_settings['crm_currency']]; }  ?><span class="grand_totel"></span></h3>
                            <div class="divok">
                              <div class="uprecs">
                                <h3 class="upon-res">Upon Reciept</h3>
                                <h6>Due</h6>   
                              </div>
                              <div class="duenoin">                      
                                <span class="due-date12" id="in_number"></span>
                                <h5 class="invoice-no">invoic no</h5>
                              </div>
                            </div>
                    </div>
                  </div>
                  </div>
                  <div class="generate-top">
                    <span class="project-name">
                      Invoice
                     <!--  <span>project name</span> -->
                    </span>
                    <span class="date-sec">
                      <strong><div class="g-date" id="date_of_invoice"></div></strong>
                      <!-- <input type"text" class="g-date" id="date_of_invoice" value="12/2/2018">  -->
                    </span>
                  </div>
                  <div class="des-table invoice-table">
                    <div class="description-inv1">
                  <div class="table-responsive">
                      <table class="invoice_bill_table" id="invoice_bill_table">
                            <thead>    
                              <tr>
                                 <th>description</th>
                                 <th>qty</th>
                                 <th>price</th>
                                 <th style="width:5%;">amount</th>
                              </tr>
                            </thead> 
                            <tfoot class="ex_data1">
<tr>
<th>
</th>
</tr>
</tfoot>  
                            <tbody>
                              <tr class="fetch_table" id="fetch_table">
                                <td></td>

                                <td></td>


                                <td><i class="fa fa-gbp"></i>
                                 </td>

                                <td><i class="fa fa-gbp"></i> </td>

                              </tr>
                             
                            </tbody>    
                          </table>
                       </div>
                  </div>

                    <div class="sub-totaltab sub-tot_table">
                      <table class="fetch_sub_total" id="fetch_sub_total">
                          <tr>
                            <th>sub total</th>
                            <td class="subTot">
                              <!-- <i class="fa fa-gbp"> </i> -->
                              <div class="euro-position">
       <strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong>
       <div class="sub-quote01">
       </div>
     </div>
                            </td>
                          </tr>
                          <tr>
                            <th>total</th>
                            <td class="gndTot">
                              <!-- <i class="fa fa-gbp"> </i> -->
                              <div class="euro-position">
       <strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong>
       <div class="sub-quote01">

       </div>
     </div>
                            </td>
                          </tr>
                      </table>                     
                    </div>
                  </div>
                  <div class="payment-ins">
                 <!--    <span>payment instrctions</span> -->
                    </div>
                <!--   <div class="payment-ins">
                    <span class="place">united kingdom</span>
                  </div> -->
                <input type="hidden" name="total_item" class="total_item" id="total_item" value="1">
                  
               </div>
            </form>   
        </div>
       <!--  <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>


<!-- Modal -->

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script> -->
<script>
   $( document ).ready(function() {
   
       var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
       // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#1abc9c',
               jackColor: '#fff',
               size: 'small'
           });
       });
   
       $('#accordion_close').on('click', function(){
               $('#accordion').slideToggle(300);
               $(this).toggleClass('accordion_down');
       });
   });
</script>

<script type="text/javascript">
  $(document).on('click', '.default_remove_row', function() {
    var id = $(this).attr("id");
    var final_amount1 = $('.hidden_amount_gbp'+id).val();
    var sub_total1 = $('#sub_total').val();
    var result_amt1 = parseFloat(sub_total1) - parseFloat(final_amount1);
    $('#sub_total').val(result_amt1);

    
    var tax_amt1 = $('.hidden_tax_amount'+id).val();
    var vat_amount1 = $('#value_at_tax').val();
    var vat_result1 = parseFloat(vat_amount1) - parseFloat(tax_amt1);
    $('#value_at_tax').val(vat_result1);

    var gndtot1 = parseFloat(result_amt1) + parseFloat(vat_result1);
    // var adjusttax = $('.adjust_tax').val();
    // var totalgrand = parseFloat(gndtot1) + parseFloat(adjusttax);
    var selTax = $('.select_tax').val();
    if(selTax == 1)
    {
      $('#grand_total').val(gndtot1);  
    } else {
      $('#grand_total').val(result_amt1);  
    }
    
    $('#default_row'+id).remove();
        
  });

</script>

<script type="text/javascript">

function reset_form()
{
   $("#invoice_form")[0].reset();
   return false;
}

  $(document).ready(function() {
    var sub_total_amount = $('#sub_total').val();
    var count = "<?php echo $m; ?>";
    setTimeout(function() { $('#select_tax').val('<?php echo $amt_type_val; ?>').trigger('change'); },300);
    $('#select_tax').on('change', function() {
    var select_val = $(this).val();
    if(select_val == 1)
    {
      $('table tr > th:nth-child(8), table tr > td:nth-child(8)').show();
      $('table tr > th:nth-child(9), table tr > td:nth-child(9)').show();
      $('#sub_tot_table tr.vat_row').show();
      // $('#sub-tot_table').show();
      // $('.approve-btn').show();


    } else {
      $('table tr > th:nth-child(8), table tr > td:nth-child(8)').hide();
      $('table tr > th:nth-child(9), table tr > td:nth-child(9)').hide();
      $('#sub_tot_table tr.vat_row').hide();
      // $('#sub-tot_table').hide();
      // $('.approve-btn').hide();
    }
    $(".tax_rate").val(0);
    $(".tax_amount").val(0);
    $(".tax_rate").eq( 0 ).trigger('change');

  });


    $(document).on('keyup','.decimal',function(){  
    //  alert('gdfgfg');
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
   });

    $(document).on('click','.addmore', function() { 
      var a = $('.select_tax').val();
     
      count++;
      $('.total_item').val(count);
      var html_code = '';
      var html_code1 = '';

      if(a == 1) 
      {
      html_code += '<tr id="row_id_'+count+'">';
      html_code += '<td><span id="sr_no">'+count+'</span></td>';
      html_code += '<td><input type="text" name="item_name[]" id="item_name'+count+'" class="form-control input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required"></td>';
      html_code += '<td><textarea name="description[]" id="description'+count+'" data-srno="'+count+'" class="form-control input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"></textarea></td>';
      html_code += '<td><input type="text" name="quantity[]" id="quantity'+count+'" data-srno="'+count+'" class="form-control input-sm quantity" data-parsley-required="true" data-parsley-type="number"></td>';
      html_code += '<td><input type="text" name="unit_price[]" id="unit_price'+count+'" data-srno="'+count+'" class="form-control input-sm unit_price" data-parsley-required="true" data-parsley-type="number"></td>';
      html_code += '<td><input type="text" name="discount[]" id="discount'+count+'" data-srno="'+count+'" class="form-control input-sm number_only discount" data-parsley-required="true" data-parsley-type="number"></td>';
      html_code += '<td><input type="text" name="account[]" id="account'+count+'" data-srno="'+count+'" class="form-control input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>';
      html_code += '<td class="td_tax_rate" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate'+count+'" data-srno="'+count+'" class="form-control input-sm tax_rate" data-parsley-required="true" data-parsley-type="number" value="0"></td>';
      html_code += '<td class="td_tax_amount" id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount'+count+'" data-srno="'+count+'" class="form-control input-sm tax_amount" readonly></td>';
      html_code += '<td><input type="text" name="amount_gbp[]" id="amount_gbp'+count+'" data-srno="'+count+'" class="form-control input-sm amount_gbp" readonly /></td>';
      html_code += '<td><button type="button" name="remove_row" id="'+count+'" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td><td></td>';  

      // html_code += '<td><input type="hidden" name="act_amt" id="actual_amount'+count+'" data-srno="'+count+'" class="data-srno="1" class="form-control input-sm actual_amount"> </td>';
      html_code += '</tr>';
       } else {
 
      html_code += '<tr id="row_id_'+count+'">';
      html_code += '<td><span id="sr_no">'+count+'</span></td>';
      html_code += '<td><input type="text" name="item_name[]" id="item_name'+count+'" class="form-control input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required"></td>';
      html_code += '<td><textarea name="description[]" id="description'+count+'" data-srno="'+count+'" class="form-control input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"></textarea></td>';
      html_code += '<td><input type="text" name="quantity[]" id="quantity'+count+'" data-srno="'+count+'" class="form-control input-sm quantity" data-parsley-required="true" data-parsley-type="number"></td>';
      html_code += '<td><input type="text" name="unit_price[]" id="unit_price'+count+'" data-srno="'+count+'" class="form-control input-sm unit_price" data-parsley-required="true" data-parsley-type="number"></td>';
      html_code += '<td><input type="text" name="discount[]" id="discount'+count+'" data-srno="'+count+'" class="form-control input-sm number_only discount" data-parsley-required="true" data-parsley-type="number"></td>';
      html_code += '<td><input type="text" name="account[]" id="account'+count+'" data-srno="'+count+'" class="form-control input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>';

        html_code += '<td class="td_tax_rate" style="display:none" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate'+count+'" data-srno="'+count+'" class="form-control input-sm tax_rate" data-parsley-required="true" data-parsley-type="number" value="0"></td>';
      html_code += '<td class="td_tax_amount" style="display:none" id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount'+count+'" data-srno="'+count+'" class="form-control input-sm tax_amount" readonly></td>';

      html_code += '<td><input type="text" name="amount_gbp[]" id="amount_gbp'+count+'" data-srno="'+count+'" class="form-control input-sm amount_gbp" readonly /></td>';
      html_code += '<td><button type="button" name="remove_row" id="'+count+'" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td><td></td>';  

      // html_code += '<td><input type="hidden" name="act_amt" id="actual_amount'+count+'" data-srno="'+count+'" class="data-srno="1" class="form-control input-sm actual_amount"> </td>';

      html_code += '</tr>';
      }
     
      $('#invoice_table').append(html_code);
      // $('#invoice_bill_table').append(html_code1);

      // var data=$("#description").html();

      // // $(".body-popup-content").html('');
      // $("#invoice_bill_table").append(data);

      
     
    });


  $(document).on('click', '.remove_row', function() { 
    var row_id = $(this).attr("id");
    // alert(row_id);
    var final_amount = $('#amount_gbp'+row_id).val();
    var sub_total = $('#sub_total').val();
    var result_amt = parseFloat(sub_total) - parseFloat(final_amount);
    $('#sub_total').val(result_amt);

    var tax_amt = $('#tax_amount'+row_id).val();
    var vat_amount = $('#value_at_tax').val();
    var vat_result = parseFloat(vat_amount) - parseFloat(tax_amt);
    $('#value_at_tax').val(vat_result);

    var gndtot = parseFloat(result_amt) + parseFloat(vat_result);
    $('#grand_total').val(gndtot);


    $('#row_id_'+row_id).remove();
    count--;
    $('.total_item').val(count);  

  });


  function cal_final_total(count)
  { 
    var a = $('.quantity').val();
    
    var final_total_amount = 0;
    for(j=1; j<=count; j++)
    { 
      var item = 0;
      var descre = 0;
      var qty = 0;
      var price = 0;
      var discount = 0;
      var account = 0;
      var taxRate = 0;
      var taxAmount = 0;
      var amount = 0;
      
      
    qty = $('#quantity'+j).val();
    if(qty >= 0)
    {  
      price = $('#unit_price'+j).val();
      if(price > 0)
      { 
        amount = parseFloat(qty) * parseFloat(price);
        $('#amount_gbp'+j).val(amount);

        // $('#amount_gbp'+j).each(function() {
        //     $(this).keyup(function() {

        //       calculateSum();
        //     });
        //   });
        calculateSum();
        

        discount = parseFloat($('#discount'+j).val());
        if(discount >= 0)
        { 
          var dis = (discount/100).toFixed(2);
          var mult = amount*dis;
          var result_amt = amount-mult;
          $('#amount_gbp'+j).val(result_amt);
          
          calculateSum();
          
        }

        taxRate = parseFloat($('#tax_rate'+j).val());
        if(taxRate >= 0)
        { 
          taxAmount = (parseFloat(amount) * taxRate)/100;
          $('#tax_amount'+j).val(taxAmount);

          cal_tax_amount();
          
        }

          
       

      }

        
    }  

     
    }
  
    

  
  }

   



function calculateSum()
{
  var sum = 0;
  $('.amount_gbp').each(function() {
    if(!isNaN(this.value) && this.value.length!=0) {
      sum += parseFloat(this.value);
      // alert(sum);
    }
  });
  $('#sub_total').val(sum.toFixed(2));
  // $('#grand_total').val(sum.toFixed(2));
  update_gndtotal();
}


function cal_tax_amount()
{
  var tot = 0;
  $('.tax_amount').each(function() {
    if(!isNaN(this.value) && this.value.length!=0) {
      tot += parseFloat(this.value);
    }
  });
  $('#value_at_tax').val(tot.toFixed(2));
  update_gndtotal();
}



function update_gndtotal()
{

var sub = 0;
var vat = 0;
var gnd_tot = 0;
var ad = 0; 
 
  sub = parseFloat($('#sub_total').val());
  vat = parseFloat($('#value_at_tax').val());
  gnd_tot = sub + vat;

  var selTax = $('.select_tax').val();
  if(selTax == 1)
  {
    $('#grand_total').val(gnd_tot.toFixed(2));
    $('#grandtotal_hidden').val(gnd_tot.toFixed(2));    
  } else {
    $('#grand_total').val(sub.toFixed(2));
    $('#grandtotal_hidden').val(sub.toFixed(2));    
  }

   //alert(selTax);
}

  
  


$(document).on('change', '.quantity', function(){
    cal_final_total(count);
  });

  $(document).on('change', '.unit_price', function(){
    cal_final_total(count);
  });

  $(document).on('change', '.discount', function(){
    cal_final_total(count);
  });   

  $(document).on('change', '.tax_rate', function(){
    cal_final_total(count);
  });

  $(document).on('change', '.amount_gbp', function(){
    cal_final_total(count);
  });


$('#adjust_tax').change(function () {
 
    // initialize the sum (total price) to zero
    var sum = 0;
    // var b = $('.grandtotal_hidden').val();
    // we use jQuery each() to loop through all the textbox with 'price' class
    // and compute the sum for each loop
    // alert(sum);
    $('.adjust_tax').each(function() {
        a = parseFloat($(this).val());
        if(!isNaN(a) && a > 0) {
          sum = parseFloat($('.grandtotal_hidden').val()) + a;  
        } else {
          sum = parseFloat($('.grandtotal_hidden').val());
          // alert(sum);
        }
        
    });
    
     // console.log(sum);
    // set the computed value to 'totalPrice' textbox
    $('#grand_total').val(sum.toFixed(2));
     
});


// $.event.special.inputchange = {
//     setup: function() {
//         var self = this, val;
//         $.data(this, 'timer', window.setInterval(function() {
//             val = self.value;
//             if ( $.data( self, 'cache') != val ) {
//                 $.data( self, 'cache', val );
//                 $( self ).trigger( 'inputchange' );
//             }
//         }, 20));
//     },
//     teardown: function() {
//         window.clearInterval( $.data(this, 'timer') );
//     },
//     add: function() {
//         $.data(this, 'cache', this.value);
//     }
// };

// $('.description').on('inputchange', function() {
//   // alert(this.value);
//     // $('.invoice_bill_table td description').val(this.value);
//     var a = $(this).val();
//     // $(this).closest('.invoice_bill_table td').find('#description').val(a);
//     alert(a);
// });


//   $(document).ready(function() {
    
//     sum();
//     $("#adjust_tax").on("keydown keyup", function() {
//         sum();
//     });
// });

//   function sum() {
//             // var num = document.getElementById('sub_total').value;
//             // var num1 = document.getElementById('adjust_tax').value;

            
//             var num1 = $('#adjust_tax').val();
            
//           if(!isNaN(num1) && num1 > 0)
//           {
//             var c = parseFloat(num1)/100;
//             var num = parseFloat($('#sub_total').val());

//             // var ad = parseFloat(num1/100).toFixed(2);
//             // var num2 = document.getElementById('value_at_tax').value;
//             var num2 = parseFloat($('#value_at_tax').val());
//       // var result = parseInt(num1) + parseInt(num2);
//       // var result1 = parseFloat(num2) * parseFloat(ad);
//       var result1 = num2 * c;
//       var result2 = num + result1;
//       // var result2 = parseFloat(num) + parseFloat(result1);
//             if (!isNaN(result2)) {
//                 // document.getElementById('sum').value = result;
//         // document.getElementById('grand_total').value = result2;
//           $('#grand_total').val(result2.toFixed(2)); 
//             }
//           }  
            
//         }


});
</script>





<script>
$(document).ready(function(){
$('.number_only').keypress(function(e){
return isNumbers(e, this);      
});
function isNumbers(evt, element) 
{
var charCode = (evt.which) ? evt.which : event.keyCode;
if (
(charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
(charCode < 48 || charCode > 57))
return false;
return true;
}
});
</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/parsley.css') ?>">
<script src="<?php echo base_url('assets/js/parsley.min.js') ?>"></script>
<script type="text/javascript">
 var form_valid=$('#invoice_form').parsley();
 $("#update_repeat_form").click(function(envent){
  if(form_valid.isValid()){  
  event.preventDefault();
  var to_id = $('select[name=invoice_to]').val(); 
  var to_email = $('select[name=invoice_to] option[value='+to_id+']').html();
  $('input[name=to_email]').val(to_email.trim());
  var form_data = new FormData($("#invoice_form")[0]);
      $.ajax({
        url : "<?php echo base_url()?>Invoice/UpdateRepeatInvoice",
        type : 'POST',
        data : form_data,
        processData : false,
        contentType : false,
        beforeSend:function(){$('.LoadingImage').show();},
        success:function(response)
        {
          $('.LoadingImage').hide(); 

          if(response == 1) 
          {
            $(".popup_info_box .position-alert1").html("Invoice Updated Successfully..");
            $(".popup_info_box").show();
          }
          else
          {
            $(".popup_info_box .position-alert1").html("Process Failed.");
            $(".popup_info_box").show();
          }

        setTimeout(function(){ window.location.href="<?php echo base_url()?>Invoice";},1000);
        }
   });

  }
 });

</script>

<script type="text/javascript">
  $(document).ready(function() 
  {

  $('.preview-re').on('click', function(event) 
  { 
     event.preventDefault();     
     datas = $('#invoice_form').serializeArray(); 
     
     $('.default_row').find('input,textarea').each(function()
     { 
        if($(this).hasClass('description'))
        {
          datas.push({name:'description[]',value:$(this).html()});
        }
        else if($(this).hasClass('quantity'))
        {
          datas.push({name:'quantity[]',value:$(this).val()});
        }
        else if($(this).hasClass('unit_price'))
        {
          datas.push({name:'unit_price[]',value:$(this).val()});
        }
        else if($(this).hasClass('amount_gbp'))
        {
          datas.push({name:'amount_gbp[]',value:$(this).val()});
        }
     }); 
     
      $.ajax(
      {
        type: "POST",
        // dataType: 'JSON',
        url: "<?php echo base_url().'Invoice/previewInvoice';?>",
        data: datas, 
        beforeSend:function()
        {          
           $(".LoadingImage").show();
        },
        success:function(data) 
        {
          // alert(data);
          //console.log( data );
          var con = $(data).find('tbody').html();
          //console.log( con );
          $('#invoice_bill_table tbody').html( con );

          var subValue = $(data).find('tfoot #sTot').html();
          $('#fetch_sub_total td.subTot').html( subValue ); 

          var gndValue = $(data).find('tfoot #gTot').html();
          $('#fetch_sub_total td.gndTot').html( gndValue );   

          var in_date = $(data).find('tfoot #invoiceDate').html();
          $('#date_of_invoice').html(in_date);

          var in_no = $(data).find('tfoot #invoiceNo').html();
          $('#in_number').html(in_no);
          
          $(".LoadingImage").hide();
          $("#temp_invoice").modal("show");

          // $('#des_pre').text(data.description);
        //   jQuery.each(data, function(index, item) {
            
        //     //alert(item);
        // });
            

            
        }

      });

    });


  });
</script> 









    
</body>
</html>