<?php $this->load->view('includes/header');?>
<!-- management block -->
<?php
if(isset($_GET['preview']))
{
  ?>
  <style type="text/css">
    .card.firm-field {
        pointer-events: none;
      }
    a#preview_btn {
        display: none;
      }
  </style>
  <?php
}


$cur_symbols = $this->Common_mdl->getallrecords('crm_currency');

foreach ($cur_symbols as $key => $value) 
{
   $currency_symbols[$value['country']] = $value['currency'];
}

 ?>

<?php
  $color = '';
  if(isset($invoice_general_settings['accent_color']))
  {
    $color = $invoice_general_settings['accent_color'];
  }

  $path = '#';
  if(isset($invoice_general_settings['logo_path']) && ($invoice_general_settings['logo_path']))
  {
    $path = base_url().$invoice_general_settings['logo_path'];
  }

  $h_text = '#';
  if(isset($invoice_general_settings['header_text']))
  {
    $h_text = trim($invoice_general_settings['header_text']);
  }

  $f_text = '#';
  if(isset($invoice_general_settings['footer_text']))
  {
    $f_text = trim($invoice_general_settings['footer_text']);
  }

  $x_days = 0;
  if(isset($invoice_general_settings['due_date_x_days']))
  {
    $x_days = $invoice_general_settings['due_date_x_days'];
  }

?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/template.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/classic-template.css">
<style type="text/css">
  .invoice-top-1,.invoice-header>th
  {
    background: <?php echo $color;?> !important;
  }
  .hide_first
  {
    display: none;
  }
  /* #email_invoice_form {
   background: #00a2e8 !important;
  }  */
</style>

 <style type="text/css">
   .euro-position span { font-size: 12px; }
 </style>
 <div class="modal-alertsuccess alert  succ popup_info_box" style="display:none;">
  <div class="newupdate_alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              </div>
         </div>
  </div>
 </div>

<div id="print_preview">
<div class="pcoded-content new-upload-03 new-invoice-wrapper">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper new-invoice">

         <div class="deadline-crm1 floating_set invoice-align1">
            <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                    <li class="nav-item">
            <h4>Edit Invoice</h4>
          </li></ul>
          </div>

        <?php 
          echo $this->session->flashdata('invoice_success');
        ?>  

            <div class="firm-field floating_set">
              <form id="invoice_form" name="invoice_form" action="<?php echo base_url()?>invoice/UpdateInvoice" method="post">     
            <input type="hidden" name="invoiceID" value="<?php echo $invoice_details['client_id']; ?>">    
               <div class="invoice-details">
                     <div class="invoice-top">
                        <span class="invoice-topinner">
                          <?php 

                          $client = $this->db->query("SELECT user.*,client.crm_email FROM user LEFT JOIN client ON user.id = client.user_id where user.autosave_status!='1' and user.crm_name!='' AND user.id = '".$invoice_details['client_email']."'")->row_array();   
                          $client_email = '';                     
                          ?>   
                      
                          <label>to</label>
                       
                          <select id="clint_email" class="invoice_client_email clr-check-select" name="clint_email" placeholder="Select User" data-parsley-required="true">   

                              <option value="">Select client </option>
                                 <?php                                

                                $contact_mail=$this->db->query("select main_email from client_contacts where client_id=".$client['id']." and make_primary=1 limit 1")->row_array();
                                
                                if($contact_mail['main_email'] != '')
                                {
                            ?>   
                            <option value=<?php echo $client['id'];?> selected><?php echo $contact_mail['main_email']; ?></option>
                          <?php 
                            $client_email = $contact_mail['main_email'];
                        }
                                else if($client['crm_email_id'] != '')
                                { ?>
                            <option value=<?php echo $client['id'];?> selected><?php echo $client['crm_email_id']; ?></option>

                     <?php      
                            $client_email = $client['crm_email_id'];
                   } 
                                else if($client['crm_email'] != '')
                                { ?>
                            <option value=<?php echo $client['id'];?> selected><?php echo $client['crm_email']; ?></option>

                     <?php      

                            $client_email = $client['crm_email'];
                   } ?>
                              
                        </select> 
                        </span>
                        <span class="invoice-topinner">
                           <label>date</label>
                          <!--  <div class='input-group date' id='datetimepicker7'>
                            <span class="input-group-addon ">
                                             <span class="icofont icofont-ui-calendar"></span>
                                                                                </span> -->
                            <input type="text" name="invoice_date" class="clr-check-select datepicker1" data-parsley-required="true" 
                            value='<?php echo date("d-m-Y", $invoice_details['invoice_date']); ?>'>
                         <!--  </div>-->
                        </span> 
                        <span class="invoice-topinner">
                           <label>due date</label>
<!-- <div class='input-group date'>
                           <span class="input-group-addon ">
                                             <span class="icofont icofont-ui-calendar"></span>
                                                                                </span> -->
                          <?php
                            if($invoice_details['invoice_duedate'] != 'Expired')
                            {
                              $invoice_details['invoice_duedate'] = strtotime('+'.$x_days.' days', $invoice_details['invoice_duedate']);
                            }

                          ?>
                           <input type="text" name="due_date" class="clr-check-select datepicker1" data-parsley-required="true" value='<?php if($invoice_details['invoice_duedate'] == 'Expired') {
                                    echo $invoice_details['invoice_duedate']; } else { echo date("d-m-Y", $invoice_details['invoice_duedate']); } ?>'><!-- </div>-->

                        </span> 
                        <span class="invoice-topinner">
                           <label>invoice</label>
                           <input class="clr-check-client" type="text" name="invoice_no" value="<?php echo $invoice_details['invoice_no'];?>" data-parsley-required="true" readonly>
                        </span>

                        <span class="invoice-topinner side-inputbox01">
                           <label>repeat this transaction For</label>
                           <input type="text" name="repeat_trans" class="date-col clr-check-client" style="min-width: 80px;" value="<?php echo $invoice_details['repeats']; ?>">
                           <select name="select_transaction" class="clr-check-select" style="width: 120px !important;">
                              <option value="0" <?php if($invoice_details['repeat_period'] == '0'){ echo "selected"; } ?>>week(s)</option>
                              <option value="1" <?php if($invoice_details['repeat_period'] == '1'){ echo "selected"; } ?>>month(s)</option>
                              <option value="2" <?php if($invoice_details['repeat_period'] == '2'){ echo "selected"; } ?>>year(s)</option>
                           </select>
                           <p id="repeat_trans_er" style="color:red;"></p>
                        </span> 
                      </div>

                      <div class="invoice-top">
                           <span class="invoice-topinner hide_first">
                           <label>end date (Optional)</label>
                           <div class='input-group date' id='datetimepicker6'>
                           <span class="input-group-addon ">
                           <span class="icofont icofont-ui-calendar"></span>
                           </span>
                           <input type="text" name="invoice_enddate" class="clr-check-select datepicker1" placeholder="dd-mm-yyyy">
                           </div>
                           </span>

                          <span class="invoice-topinner side-inputbox01 hide_first">
                          <label>Day(s)&nbsp;&nbsp;&nbsp;<strong>due</strong></label>
                          <input type="text" name="duedate_no" class="date-col clr-check-client" style="min-width: 218px;" value="<?php echo $invoice_details['days_after_invoice'];?>">
                          <p>Day(s) After The Invoice Date</p>
                          <select name="repeat_days" style="display: none;">
                          <option value="1">day(s) after the invoice date</option>
                          </select>
                          <p id="duedate_no_er" style="color:red;"></p>
                          </span>
                      <!--   <span class="invoice-topinner">
                           <label>reference</label>


                            <select name="reference" id="refered_by" class="fields" data-parsley-required="true" data-parsley-error-message="Reference is required">
                             <option value="">select</option>
                             <?php foreach ($referby as $referbykey => $referbyvalue) {
                                # code...
                                 ?>
                             <option value="<?php echo $referbyvalue['id'];?>" <?php if(isset($invoice_details['reference']) && $invoice_details['reference']==$referbyvalue['user_id']) {?> selected="selected"<?php } ?>><?php echo $referbyvalue['crm_first_name'];?></option>
                             <?php } ?>
                          </select>

                           <!- <input type="text" name="reference" data-parsley-required="true" data-parsley-error-message="Reference is required" value="<?php //echo $invoice_details['reference']; ?>"> -->
                      <!-- </span> -->
                        <span class="preview-pdf preview-pdf-edit" id="preview-pdf">
                              <!-- <i class="fa fa-eye" aria-hidden="true"></i>
                              preview -->
                              <!-- <button type="button" data-toggle="modal" data-target="#temp_invoice" class="btn btn-card btn-primary" aria-hidden="true" id="preview_btn">
                              <i class="fa fa-eye fa-5"></i>
                              preview</button> -->
                              <?php  //if(count($product_details) == '0')
                              //{
                                ?>
                               <div class="add-newline addmore inadd01">
                                    <input type="button" class="btn btn-card btn-primary " id="addmore" value="add new line">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                 </div>
                              <?php //} ?>
                              <?php 
                                  $model_data_target = '#';
                                  if(isset($invoice_general_settings['template']) && ($invoice_general_settings['template'] == 'classic'))
                                    {
                                      $model_data_target = '#temp_invoice_classic';
                                    }
                                    elseif (isset($invoice_general_settings['template']) && ($invoice_general_settings['template'] == 'modern')) {
                                      $model_data_target = '#temp_invoice_modern';
                                    }

                              ?>
                              <a href="#" data-toggle="modal" data-target="<?php echo $model_data_target;?>" class="btn btn-card btn-primary" aria-hidden="true" id="preview_btn"><i class="fa fa-eye fa-5"></i>preview</a>
                              <a href="<?php echo base_url().'invoice/generate_invoice_pdf/'.$invoice_details['client_id'];?>" class="btn btn-card btn-primary" aria-hidden="true" id="download_btn"><i class="fa fa-download fa-5"></i>download</a>
                        </span>
                     </div>
                     <input type="hidden" name="to_email">
                     <div class="amounts-detail pull-left">
                        <label>Service Category</label>
                        <select name="category_id" class="select_tax clr-check-select">
                          <option value="">Select Category</option>
                          <?php foreach ($category as $key => $value) { ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo ucwords($value['category_name']); ?></option>
                          <?php } ?>
                        </select>
                        <p id="category_id_er" style="color:red;"></p>
                     </div>
                     <div class="amounts-detail">
                        <label>amounts are</label>
                        <select name="amounts_details" class="select_tax clr-check-select" id="select_tax">
                          <option value="">Select</option>
                          <?php $amt_type = $invoice_details['amounts_type']; if($amt_type == 'Tax Exclusive'){ $amt_type_val = '1'; }else if($amt_type == 'Tax Inclusive'){ $amt_type_val = '2'; } ?>
                           <option value="1" <?php //if($amt_type == 'Tax Exclusive') { echo "selected='selected'"; } ?>>Tax Exclusive</option>
                           <option value="2" <?php //if($amt_type == 'Tax Inclusive') { echo "selected='selected'"; } ?>>Tax Inclusive</option>
                        </select>
                     </div>

                     <div class="invoice-table floating_set">

                      <div class="invoice-realign1 floating_set">
                        <div class="table-responsive">

                           <table class="invoice_table" id="invoice_table">
                            <thead>
                                 <th></th>
                                 <th width="17%">Service</th>
                                 <th width="28%">description</th>
                                 <th>qty</th>
                                 <th>unit price</th>
                                 <th>disc%</th>
                                 <th>account</th>
                              <?php //if($amt_type == 'Tax Exclusive') { ?>  
                               <th class="th_tax_rate tax td_tax_rate" id="th_tax_rate">tax rate</th>
                               <th class="th_tax_amount" id="th_tax_amount">tax amount</th>
                              <?php //} ?>
                                 
                                 <th>amount <?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></th>
                                 <th></th>
                                 <!-- <th></th> -->
                              </thead>

                                 <tfoot class="ex_data1">
<tr>
<th>
</th>
</tr>
</tfoot>

                              <tbody>
                        
                <?php  $sno = 0;  $m = 0; $send_status = $invoice_details['send_statements'];
                  foreach ($product_details as $key => $value) {  $m = $m + 1;  ?>
                      
                    <tr class="default_row" id="default_row<?php echo $value['id']?>">
                      <td><span id="sr_no" class="sr_no"><?php echo ++$sno; ?></span></td>                 
                      <td><input <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="item_name[]" data-parsley-required="true" data-parsley-error-message="Item is required"'; } ?> id="item_name<?php echo $m; ?>" class="form-control clr-check-client input-sm item_name" value="<?php echo $value['item'];?>">
                       </td>                     
                      <td><textarea <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="description[]" data-parsley-required="true" data-parsley-error-message="Description is required"'; } ?> id="description<?php echo $m; ?>" data-srno="1" class="form-control clr-check-client input-sm description"><?php echo $value['description'];?></textarea> </td>

                      <td><input type="number" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="quantity[]" data-parsley-required="true" data-parsley-type="number"'; } ?> id="quantity<?php echo $m; ?>" data-srno="1" class="form-control clr-check-select input-sm  quantity decimal_qty" maxlength="4" value="<?php echo $value['quantity'];?>"></td>

                      <td><input type="text" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="unit_price[]" data-parsley-required="true" data-parsley-type="number"'; } ?> id="unit_price<?php echo $m; ?>" data-srno="1" class="form-control clr-check-select input-sm unit_price decimal" maxlength="6" value="<?php echo $value['unit_price'];?>" /></td>

                      <td><input type="text" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="discount[]" data-parsley-required="true" data-parsley-type="number"'; } ?> id="discount<?php echo $m; ?>" data-srno="1" class="form-control clr-check-select input-sm discount decimal" maxlength="4" value="<?php echo $value['discount'];?>"></td>

                      <td><input type="text" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="account[]" data-parsley-required="true" data-parsley-error-message="Account is required"'; } ?> id="account<?php echo $m; ?>" data-srno="1" class="form-control clr-check-client input-sm account decimal" value="<?php echo $value['account'];?>"></td>

                  <?php  //if($amt_type == 'Tax Exclusive') { ?>
                      <td class="td_tax_rate" id="td_tax_rate"><input type="text" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="tax_rate[]" data-parsley-required="true" data-parsley-type="number"'; } ?> id="tax_rate<?php echo $m; ?>" data-srno="1" maxlength="4" class="form-control input-sm tax_rate decimal" value="<?php echo $value['tax_rate'];?>" ></td>

                      <td class="td_tax_amount" id="td_tax_amount<?php echo $m; ?>"><input type="text" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="tax_amount[]"'; } ?>  id="tax_amount1" data-srno="1" class="form-control input-sm tax_amount hidden_tax_amount<?php echo $value['id']?>"  value="<?php echo $value['tax_amount'];?>"/></td>
                    
                  <?php //} ?>  
                      
                      <td><input type="text" <?php if($send_status == 'send'){ echo "disabled"; }else{ echo 'name="amount_gbp[]"'; } ?> id="amount_gbp<?php echo $m; ?>" data-srno="1" class="form-control input-sm amount_gbp hidden_amount_gbp<?php echo $value['id']?>"  value="<?php echo $value['amount_gbp'];?>"/ ></td>

                      <!-- <td></td> -->
                      <td><button type="button" name="default_remove_row" id="<?php echo $value['id'];?>" class="btn btn-danger btn-xs default_remove_row" aria-hidden="true" <?php if($send_status == 'send'){ echo "disabled"; } ?>>x</button></td>
                        
                <!-- <td><input type="hidden" name="act_amt" id="actual_amount1" data-srno="1" class="data-srno="1" class="form-control input-sm actual_amount"> </td>   -->  


                      <input type="hidden" name="primaryID" value="<?php echo $value['id']; ?>"> 
                      <input type="hidden" name="productID[]" value="<?php echo $value['client_id']; ?>"> 


                    <!--   <td>
                            <div class="add-newline  addmore">
                              <input type="button" class="btn btn-card btn-primary " id="addmore" value="add new line">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                           </div>
                      </td> -->
                    </tr>


                  <?php } ?>   
                  </tbody>              
                    </table> 
                          
                        </div>
                       
                         </div>


                         <div class="cmn-invoice-approve floating_set">
                          <div class="right-invoice-new1">
                           <div class="sub-tot_table" id="sub-tot_table">
                              <table id="sub_tot_table" class="sub_tot_table">
                                 <tbody>
                                    <tr>
                                       <th>sub total</th>
                                       <td>
                                         <div class="euro-position">
                                        <strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong>
                                        <input type="text" name="sub_tot" id="sub_total" class="form-control input-sm txtcal sub_total" value="<?php echo $amount_details['sub_total']; ?>" readonly></div></td>
                                    </tr>
                                <?php  if($amt_type == 'Tax Exclusive' || !empty($invoice_details['proposal_id'])) { ?>    
                                    <tr id="vat_row" class="vat_row">
                                       <th>VAT</th>
                                       <td><input type="text" name="value_at_tax" id="value_at_tax" class="form-control input-sm txtcal value_at_tax" value="<?php echo $amount_details['VAT']; ?>" readonly></td>
                                    </tr>
                                <?php } ?>      
                                    <tr>
                                       <th>includes adjustments to Tax</th>
                                       <td><input type="text" name="adjust_tax" id="adjust_tax" class="form-control input-sm adjust_tax" value="<?php echo $amount_details['adjust_to_tax']; ?>"></td>
                                    </tr>
                                    <tr class="grand-total">
                                       <th>total</th>
                                       <td>

                                         <div class="euro-position">
                                        <strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong>
                                        <input type="text" name="grand_total" id="grand_total" class="form-control input-sm grand_total" value="<?php echo $amount_details['grand_total']; ?>" readonly>
                                      </div>

                                      </td>

                                    <input type="hidden" name="grandtotal_hidden" id="grandtotal_hidden" class="form-control input-sm grandtotal_hidden" value="0.00">   
                                       
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                         </div>


                     

                     <div class="attach-invoice1">
                     <div class="save-approve" id="save-approve">
                       <select name="transaction_payment" class="select_payment" id="select_payment">
                          <?php $payment_status = $amount_details['payment_status']; ?>  
                             <option value="approve" <?php if($payment_status == 'approve'){ echo "selected='selected'"; }?> > Approve </option> 
                             <option value="decline" <?php if($payment_status == 'decline'){ echo "selected='selected'"; }?> > Decline </option> 
                             <option value="pending" <?php if($payment_status == 'pending'){ echo "selected='selected'"; }?> > Pending </option> 

                           </select> 
                        
                        <div class="approve-btn"> 
                           <div class="table-savebtn">
                            <input type="hidden" name="total_item" class="total_item" id="total_item" value="1">
                            <input type="submit" class="btn btn-card btn-primary" id="update_invoice_form" value="Update">
                            <input type="hidden" name="clientID" value="<?php echo $invoice_details['client_id'];?>">

                        </div>
                        <div class="table-savebtn">
                          <input type="submit" class="btn btn-card btn-success" id="email_invoice_form" value="Email">
                        </div>
                           <!-- <input type="button" class="approve" value="approve"> -->
                           <a href="#" onclick="return reset_form()" class="cancel-btn">cancel</a> 
                        </div> 
                     </div>
                   </div>

                   </div><!--invoice table -->

               </div>
            </form>   
            </div>
         </div>
      </div>
   </div>
</div>
</div>

<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
   <h1>Warning!!</h1>
   <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
   <div class="iew-container">
      <ul class="iew-download">
         <li>
            <a href="http://www.google.com/chrome/">
               <img src="assets/images/browser/chrome.png" alt="Chrome">
               <div>Chrome</div>
            </a>
         </li>
         <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
               <img src="assets/images/browser/firefox.png" alt="Firefox">
               <div>Firefox</div>
            </a>
         </li>
         <li>
            <a href="http://www.opera.com">
               <img src="assets/images/browser/opera.png" alt="Opera">
               <div>Opera</div>
            </a>
         </li>
         <li>
            <a href="https://www.apple.com/safari/">
               <img src="assets/images/browser/safari.png" alt="Safari">
               <div>Safari</div>
            </a>
         </li>
         <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
               <img src="assets/images/browser/ie.png" alt="">
               <div>IE (9 & above)</div>
            </a>
         </li>
      </ul>
   </div>
   <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->


<!-- Modal -->

<!-- Modal -->
  <div class="modal fade modal-invoice-quote1" id="temp_invoice" role="dialog">
    <div class="modal-dialog modal-classic-invoice">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Invoice</h4>
        </div>
        <div class="modal-body">
        <form action="#" method="post">
          <div id="generate_invoice" class="invoice-details generate-invoice">
               <!-- <div class="update-logo">
                    <input type="file">
                    <div class="up-logo">
                    </div>
                  </div> -->
                  <div class="online-payments">
                     <div class="online-qut">
                    <div class="temp-edit">
                    <h3> Remindoo.org</h3>
                    </div>
                    <div class="payment-table newlayout">
                          

                            <h3><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></i><span class="grand_totel"></span>
                             <strong> Amount due <span></span>  </strong>  </h3>
                          
                            <div class="divok">
                              <div class="uprecs">
                          <h3 class="upon-res">Upon Reciept</h3>
                          <h6>Due</h6> 
                        </div>

                            <div class="duenoin">                      
                            <span class="due-date12" id="in_number"></span>
                            <h5 class="invoice-no">invoic no</h5>
                          </div>
                        </div>
                        </div>
                    </div>
                  </div>

                  <div class="generate-top">
                    <span class="project-name">
                      Invoice
                    </span>
                    <span class="date-sec">
                      <strong><div class="g-date" id="date_of_invoice"></div></strong>
                      <!-- <input type"text" class="g-date" id="date_of_invoice" value="12/2/2018">  -->
                    </span>
                  </div>
                  <div class="des-table invoice-table">
                    <div class="description-inv1">
                  <div class="table-responsive">
                           <table class="invoice_bill_table" id="invoice_bill_table">
                            <thead>    
                              <tr>
                                 <th>description</th>
                                 <th>qty</th>
                                 <th>price</th>
                                 <th style="width:5%;">amount</th>
                                 <!-- <th></th> -->
                              </tr>
                            </thead>   

                            <tfoot class="ex_data1">
<tr>
<th>
</th>
</tr>
</tfoot>  

                            <tbody>

                              <tr class="fetch_table" id="fetch_table">
                                <td></td>

                                <td></td>

                                <td><i class="fa fa-gbp"></i> </td>

                                <td><i class="fa fa-gbp"></i> </td>

                                <!-- <td></td> -->
                               
                              </tr>
                              <!-- <tr>
                                <td>accounts</td>
                                <td>1</td>
                                <td><i class="fa fa-gbp"></i> 350.00</td>
                                <td><i class="fa fa-gbp"></i> 350.00</td>
                              </tr> -->
                            </tbody>    
                          </table>
                  </div>
                </div>
                    <div class="sub-totaltab sub-tot_table">
                      <table class="fetch_sub_total" id="fetch_sub_total">
                          <tr>
                            <th>sub total</th>
                            <td class="subTot">

                                  <div class="euro-position">
       <strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong>
       <div class="sub-quote01">
       </div>
     </div>

                              <!-- <i class="fa fa-gbp"> </i> -->

                            </td>
                          </tr>
                          <tr>
                            <th>total</th>
                            <td class="gndTot">

                              <!-- <i class="fa fa-gbp"> </i> -->

                                <div class="euro-position">
       <strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong>
       <div class="sub-quote01">
       </div>
     </div>

                            </td>
                          </tr>
                      </table>
                    </div>

                  </div>
                  <div class="payment-ins">
                  <!--   <span>payment instrctions</span> -->
                    </div>
                 <!--  <div class="payment-ins">
                    <span class="place">united kingdom</span>
                  </div> -->
                <input type="hidden" name="total_item" class="total_item" id="total_item" value="1">
                  
               </div>
            </form>   
        </div>
       <!--  <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>

  <div class="modal fade modal-invoice-quote1" id="temp_invoice_classic" role="dialog">
    <div class="modal-dialog modal-classic-invoice">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Invoice</h4>
        </div>
        <div class="modal-body">
         <div id="container">
          <div class="invoice-top-1">
            <section id="memo">
              <div class="logo">
                <img src="<?php echo $path;?>" />
              </div>
              
              <div class="company-info">
                <span class="company-name">Remindoo.org</span>

                <span class="spacer"></span>
                  <div><?php echo $h_text;?></div>
                <span class="clearfix"></span>
              </div>

            </section>
            
            <section id="invoice-info">
              <div>
                <span>Issue Date</span>
                <span>Due Date</span>
                <span>End Date</span>
                <span>DAY(S) Due</span>
              </div>
              
              <div>
                <span><?php echo date("d-m-Y", $invoice_details['invoice_date']); ?></span>
                <span><?php if($invoice_details['invoice_duedate'] == 'Expired') {
                                    echo $invoice_details['invoice_duedate']; } else { echo date("d-m-Y", $invoice_details['invoice_duedate']); } ?></span>
                <span></span>
                <span></span>
              </div>

              <span class="clearfix"></span>

              <section id="invoice-title-number">
            
                <span id="title">#Invoice Number:</span>
                <span id="number"><?php echo $invoice_details['invoice_no'];?></span>
                
              </section>
            </section>
            
            <section id="client-info">
              <span>Bill To:</span>
              
              <div>
                <span><?php echo $client_email;?></span>
              </div>
            </section>

            <div class="clearfix"></div>
          </div>

          <div class="clearfix"></div>

          <div class="invoice-body">
            <section id="items">
              
              <table cellpadding="0" cellspacing="0">
              
                <tr class="invoice-header">
                 <th style="width:5%">S.No</th>
                 <th style="width:20%">Service</th>
                 <th style="width:25%">description</th>
                 <th>qty</th>
                 <th>unit price</th>
                 <th>disc%</th>
                 <th>account</th>
              <?php //if($amt_type == 'Tax Exclusive') { ?>  
               <th class="th_tax_rate tax td_tax_rate" id="th_tax_rate">tax rate</th>
               <th class="th_tax_amount" id="th_tax_amount">tax amount</th>
              <?php //} ?>
                 
                 <th style="width:15%">amount <?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></th>
                </tr>
               <?php  $sno = 0;  $m = 0; $send_status = $invoice_details['send_statements'];
                  foreach ($product_details as $key => $value) {  $m = $m + 1;  ?> 
                <tr data-iterate="item">
                  <td><?php echo ++$sno; ?></td> <!-- Don't remove this column as it's needed for the row commands -->
                  <td><?php echo $value['item'];?></td>
                  <td><?php echo $value['description'];?></td>
                  <td><?php echo $value['quantity'];?></td>
                  <td><?php echo $value['unit_price'];?></td>
                  <td><?php echo $value['discount'];?></td>
                  <td><?php echo $value['account'];?></td>
                  <td><?php echo $value['tax_rate'];?></td>
                  <td><?php echo $value['tax_amount'];?></td>
                  <td><?php echo $value['amount_gbp'];?></td>
                </tr>
                <?php } ?>  
              </table>
              
            </section>
            
            <section id="sums">
            
              <table cellpadding="0" cellspacing="0">
                <tr>
                  <th>Sub Total</th>
                  <td><strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong><?php echo $amount_details['sub_total']; ?></td>
                  <td></td>
                </tr>
                
                <!-- <tr data-iterate="tax">
                  <th>{tax_name}</th>
                  <td>{tax_value}</td>
                  <td></td>
                </tr> -->
                
                <tr class="amount-total">
                  <th>Total</th>
                  <td><strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong><?php echo $amount_details['grand_total']; ?></td>
                  <!-- <td>
                    <div class="currency">
                      <span>{currency_label}</span> <span>{currency}</span>
                    </div>
                  </td> -->
                </tr>
                
                <!-- You can use attribute data-hide-on-quote="true" to hide specific information on quotes.
                     For example Invoicebus doesn't need amount paid and amount due on quotes  -->
              <!--   <tr data-hide-on-quote="true">
                  <th>{amount_paid_label}</th>
                  <td>{amount_paid}</td>
                  <td></td>
                </tr>
                
                <tr data-hide-on-quote="true">
                  <th>{amount_due_label}</th>
                  <td>{amount_due}</td>
                  <td></td>
                </tr> -->
                
              </table>
              
            </section>

            <div class="clearfix"></div>
            
            <section id="terms">
            
              <span class="hidden">{terms_label}</span>
              <div><?php echo $f_text;?></div>
              
            </section>

            <!-- <div class="payment-info">
              <div>{payment_info1}</div>
              <div>{payment_info2}</div>
              <div>{payment_info3}</div>
              <div>{payment_info4}</div>
              <div>{payment_info5}</div>
            </div> -->
          </div>
            
        </div>
        </div>
       <!--  <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>

  <div class="modal fade modal-invoice-quote1" id="temp_invoice_modern" role="dialog">
    <div class="modal-dialog modal-classic-invoice">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Invoice</h4>
        </div>
        <div class="modal-body">
          <div id="container">
            <section id="memo">
              <div class="logo">
                <img src="<?php echo $path;?>" />
              </div>
              
              <div class="company-info">
                <div>Remindoo.org</div>

                <br />
                <span><?php echo $h_text;?></span>
              </div>

            </section>

            <section id="invoice-title-number">
            
              <span id="title">Invoice</span>
              <span id="number"><?php echo $invoice_details['invoice_no'];?></span>
              
            </section>
            
            <div class="clearfix"></div>
            
            <section id="client-info">
              <span>{bill_to_label}</span>
              <div>
                <span class="bold">{client_name}</span>
              </div>
              
              <div>
                <span>{client_address}</span>
              </div>
              
              <div>
                <span>{client_city_zip_state}</span>
              </div>
              
              <div>
                <span>{client_phone_fax}</span>
              </div>
              
              <div>
                <span>{client_email}</span>
              </div>
              
              <div>
                <span>{client_other}</span>
              </div>
            </section>
            
            <div class="clearfix"></div>
            
            <section id="items">
              
              <table cellpadding="0" cellspacing="0">
              
                <tr  class="invoice-header">
                  <th>{item_row_number_label}</th> <!-- Dummy cell for the row number and row commands -->
                  <th>{item_description_label}</th>
                  <th>{item_quantity_label}</th>
                  <th>{item_price_label}</th>
                  <th>{item_discount_label}</th>
                  <th>{item_tax_label}</th>
                  <th>{item_line_total_label}</th>
                </tr>
                
                <tr data-iterate="item">
                  <td>{item_row_number}</td> <!-- Don't remove this column as it's needed for the row commands -->
                  <td>{item_description}</td>
                  <td>{item_quantity}</td>
                  <td>{item_price}</td>
                  <td>{item_discount}</td>
                  <td>{item_tax}</td>
                  <td>{item_line_total}</td>
                </tr>
                
              </table>
              
            </section>
            
            <section id="sums">
            
              <table cellpadding="0" cellspacing="0">
                <tr>
                  <th>{amount_subtotal_label}</th>
                  <td>{amount_subtotal}</td>
                </tr>
                
                <tr data-iterate="tax">
                  <th>{tax_name}</th>
                  <td>{tax_value}</td>
                </tr>
                
                <tr class="amount-total">
                  <th>{amount_total_label}</th>
                  <td>{amount_total}</td>
                </tr>
                
                <!-- You can use attribute data-hide-on-quote="true" to hide specific information on quotes.
                     For example Invoicebus doesn't need amount paid and amount due on quotes  -->
                <tr data-hide-on-quote="true">
                  <th>{amount_paid_label}</th>
                  <td>{amount_paid}</td>
                </tr>
                
                <tr data-hide-on-quote="true">
                  <th>{amount_due_label}</th>
                  <td>{amount_due}</td>
                </tr>
                
              </table>

              <div class="clearfix"></div>
              
            </section>
            
            <div class="clearfix"></div>

            <section id="invoice-info">
              <div>
                <span>{issue_date_label}</span> <span>{issue_date}</span>
              </div>
              <div>
                <span>{due_date_label}</span> <span>{due_date}</span>
              </div>

              <br />

              <div>
                <span>{currency_label}</span> <span>{currency}</span>
              </div>
              <div>
                <span>{po_number_label}</span> <span>{po_number}</span>
              </div>
              <div>
                <span>{net_term_label}</span> <span>{net_term}</span>
              </div>
            </section>
            
            <section id="terms">

              <div class="notes">{terms}</div>

              <br />

              <div class="payment-info">
                <div>{payment_info1}</div>
                <div>{payment_info2}</div>
                <div>{payment_info3}</div>
                <div>{payment_info4}</div>
                <div>{payment_info5}</div>
              </div>
              
            </section>

            <div class="clearfix"></div>

            <div class="thank-you">{terms_label}</div>

            <div class="clearfix"></div>
          </div>
        </div>
       <!--  <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>

<!-- Modal -->
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script> -->

<script>
   $( document ).ready(function() 
   {   
       var date = $('.datepicker1').datepicker({ dateFormat: 'dd-mm-yy', minDate:0, changeMonth: true,
        changeYear: true, }).val();
   
       // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#1abc9c',
               jackColor: '#fff',
               size: 'small'
           });
       });
   
       $('#accordion_close').on('click', function(){
               $('#accordion').slideToggle(300);
               $(this).toggleClass('accordion_down');
       });
   });
</script>


<script type="text/javascript">

function reset_form()
{
  $("#invoice_form").reset();
  return false;
}
  $(document).on('click', '.default_remove_row', function() {
    var id = $(this).attr("id");
    var final_amount1 = $('.hidden_amount_gbp'+id).val();
    var sub_total1 = $('#sub_total').val();
    var result_amt1 = parseFloat(sub_total1) - parseFloat(final_amount1);
    $('#sub_total').val(result_amt1);

    
    var tax_amt1 = $('.hidden_tax_amount'+id).val();
    var vat_amount1 = $('#value_at_tax').val();
    var vat_result1 = parseFloat(vat_amount1) - parseFloat(tax_amt1);   
    $('#value_at_tax').val(vat_result1);
    

    var gndtot1 = parseFloat(result_amt1) + parseFloat(vat_result1);
    // var adjusttax = $('.adjust_tax').val();
    // var totalgrand = parseFloat(gndtot1) + parseFloat(adjusttax);
    var selTax = $('.select_tax').val();
    if(selTax == 1)
    {
      $('#grand_total').val(gndtot1);  
    } else {
      $('#grand_total').val(result_amt1);  
    }
    
    $('#default_row'+id).remove();
    
  });

</script>



<script type="text/javascript">
  $(document).ready(function() {
    var sub_total_amount = $('#sub_total').val();
    var count = "<?php echo $m; ?>"; 
    setTimeout(function() { $('#select_tax').val('<?php echo $amt_type_val; ?>').trigger('change'); },300);
    $(document).on('change','#select_tax',function() 
    { 
       var select_val = $(this).val();
       if(select_val == 1)
       {
         $('table tr > th:nth-child(8), table tr > td:nth-child(8)').show();
         $('table tr > th:nth-child(9), table tr > td:nth-child(9)').show();
         $('#sub_tot_table tr.vat_row').show();
         // $('#sub-tot_table').show();
         // $('.approve-btn').show();


       } 
       else 
       {
         $('table tr > th:nth-child(8), table tr > td:nth-child(8)').hide();
         $('table tr > th:nth-child(9), table tr > td:nth-child(9)').hide();

         <?php if(!empty($invoice_details['proposal_id'])){ ?>

         if(Number($('.value_at_tax').val()) <= '0')
         {
           $('#sub_tot_table tr.vat_row').hide();
         }
         else
         {
           $('#sub_tot_table tr.vat_row').show();
         }

         <?php }else{ ?>

          $('#sub_tot_table tr.vat_row').hide();

         <?php } ?>
         
         // $('#sub-tot_table').hide();
         // $('.approve-btn').hide();
       }
       $(".tax_rate").val(0);
       $(".tax_amount").val(0);
       $(".tax_rate").eq( 0 ).trigger('change');
    });

    $(document).on('click','.addmore', function() { 
      var a = $('.select_tax').val();
     
      count++;
      $('.total_item').val(count);
      var html_code = '';
      var html_code1 = '';

      if(a == 1) 
      {
      html_code += '<tr id="row_id_'+count+'">';
      html_code += '<td><span id="sr_no">'+count+'</span></td>';
      html_code += '<td><select name="item_name[]" id="item_name'+count+'" class="form-control input-sm item_name clr-check-select" data-parsley-required="true" data-parsley-error-message="Item is required"><option value="">Select</option><?php if(count($services)>0){ foreach($services as $value){ ?><option value="<?php echo $value['service_name']; ?>"><?php echo $value['service_name']; ?></option><?php } } ?></select></td>';
      html_code += '<td><textarea name="description[]" id="description'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"></textarea></td>';
      html_code += '<td><input type="number" name="quantity[]" id="quantity'+count+'" data-srno="'+count+'" class="form-control clr-check-select input-sm quantity decimal_qty" data-parsley-required="true"  maxlength="4" data-parsley-type="number"></td>';
      html_code += '<td><input type="text" name="unit_price[]" id="unit_price'+count+'" data-srno="'+count+'" class="form-control clr-check-select input-sm unit_price decimal" data-parsley-required="true" maxlength="6" data-parsley-type="number"></td>';
      html_code += '<td><input type="text" name="discount[]" id="discount'+count+'" data-srno="'+count+'" class="form-control clr-check-select input-sm number_only discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number"></td>';
      html_code += '<td><input type="text" name="account[]" id="account'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>';
      html_code += '<td class="td_tax_rate" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate'+count+'" data-srno="'+count+'" class="form-control input-sm number_only tax_rate decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number"></td>';
      html_code += '<td class="td_tax_amount" id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount'+count+'" data-srno="'+count+'" class="form-control input-sm tax_amount" readonly></td>';
      html_code += '<td><input type="text" name="amount_gbp[]" id="amount_gbp'+count+'" data-srno="'+count+'" class="form-control input-sm amount_gbp" readonly /></td>';
      html_code += '<td><button type="button" name="remove_row" id="'+count+'" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td><td></td>';  

      // html_code += '<td><input type="hidden" name="act_amt" id="actual_amount'+count+'" data-srno="'+count+'" class="data-srno="1" class="form-control input-sm actual_amount"> </td>';
      html_code += '</tr>';
       } else {
 
      html_code += '<tr id="row_id_'+count+'">';
      html_code += '<td><span id="sr_no">'+count+'</span></td>';
      html_code += '<td><select name="item_name[]" id="item_name'+count+'" class="form-control input-sm item_name clr-check-select" data-parsley-required="true" data-parsley-error-message="Item is required"><option value="">Select</option><?php if(count($services)>0){ foreach($services as $value){ ?><option value="<?php echo $value['service_name']; ?>"><?php echo $value['service_name']; ?></option><?php } } ?></select></td>';
      html_code += '<td><textarea name="description[]" id="description'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"></textarea></td>';
      html_code += '<td><input type="number" name="quantity[]" id="quantity'+count+'" data-srno="'+count+'" class="form-control clr-check-select input-sm quantity decimal_qty" maxlength="4" data-parsley-required="true" data-parsley-type="number"></td>';
      html_code += '<td><input type="number" name="unit_price[]" id="unit_price'+count+'" data-srno="'+count+'" class="form-control clr-check-select input-sm unit_price decimal" maxlength="6" data-parsley-required="true" data-parsley-type="number"></td>';
      html_code += '<td><input type="number" name="discount[]" id="discount'+count+'" data-srno="'+count+'" class="form-control clr-check-select input-sm number_only discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number"></td>';
      html_code += '<td><input type="text" name="account[]" id="account'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>';
      
       html_code += '<td class="td_tax_rate" style="display:none" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate'+count+'" data-srno="'+count+'" class="form-control input-sm number_only tax_rate"></td>';
      html_code += '<td class="td_tax_amount"  style="display:none"  id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount'+count+'" data-srno="'+count+'" class="form-control input-sm tax_amount" readonly></td>';

      html_code += '<td><input type="text" name="amount_gbp[]" id="amount_gbp'+count+'" data-srno="'+count+'" class="form-control input-sm amount_gbp" readonly /></td>';
      html_code += '<td><button type="button" name="remove_row" id="'+count+'" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td><td></td>';  

      // html_code += '<td><input type="hidden" name="act_amt" id="actual_amount'+count+'" data-srno="'+count+'" class="data-srno="1" class="form-control input-sm actual_amount"> </td>';

      html_code += '</tr>';
      }
  
      $('#invoice_table').append(html_code);
      // $('#invoice_bill_table').append(html_code1);

      // var data=$("#description").html();

      // // $(".body-popup-content").html('');
      // $("#invoice_bill_table").append(data);

      
     
    });



  $(document).on('click', '.remove_row', function() 
  {
    var row_id = $(this).attr("id");
     //alert(row_id);
    var final_amount = $('#amount_gbp'+row_id).val();

    var sub_total = $('#sub_total').val();
    var result_amt = parseFloat(sub_total) - parseFloat(final_amount);
    $('#sub_total').val(result_amt);

    var tax_amt = $('#tax_amount'+row_id).val();
    if(tax_amt!='')
    {
      var vat_amount = $('#value_at_tax').val();
      var vat_result = parseFloat(vat_amount) - parseFloat(tax_amt);
      $('#value_at_tax').val(vat_result);
    }
   

    // var gndtot = parseFloat(result_amt) + parseFloat(vat_result);
    // $('#grand_total').val(gndtot);


    $('#row_id_'+row_id).remove();
    count--;

 calculateSum();
    $('.total_item').val(count);  

  });


  function cal_final_total(count)
  { 
    var a = $('.quantity').val();
    
    var final_total_amount = 0;
    for(j=1; j<=count; j++)
    { 
      var item = 0;
      var descre = 0;
      var qty = 0;
      var price = 0;
      var discount = 0;
      var account = 0;
      var taxRate = 0;
      var taxAmount = 0;
      var amount = 0;
      
      
    qty = $('#quantity'+j).val();
    if(qty > 0)
    {  
      price = $('#unit_price'+j).val();
      if(price > 0)
      {
        amount = parseFloat(qty) * parseFloat(price);
        $('#amount_gbp'+j).val(amount);
        // console.log(amount);
        // $('#amount_gbp'+j).each(function() {
        //     $(this).keyup(function() {

        //       calculateSum();
        //     });
        //   });
        calculateSum();
        
        discount = parseFloat($('#discount'+j).val());
        if(discount >= 0)
        { 
          var dis = (discount/100).toFixed(2);
          var mult = amount*dis;
          var result_amt = amount-mult;
          $('#amount_gbp'+j).val(result_amt);
          
          calculateSum();
          
        }

        taxRate = parseFloat($('#tax_rate'+j).val());
        if(taxRate >= 0)
        { 
          taxAmount = (parseFloat(amount) * taxRate)/100;
          $('#tax_amount'+j).val(taxAmount);

          cal_tax_amount();
          
        }

          
       

      }

        
    }  

     
    }
  
    

  
  }

   



function calculateSum()
{
  var sum = 0;
  $('.amount_gbp').each(function() {
    if(!isNaN(this.value) && this.value.length!=0) {
      sum += parseFloat(this.value);
      // alert(sum);
    }
  });
  $('#sub_total').val(sum.toFixed(2));
  // $('#grand_total').val(sum.toFixed(2));
  update_gndtotal();
}


function cal_tax_amount()
{ 
  var tot = 0;
  $('.tax_amount').each(function() 
  {
    if(!isNaN(this.value) && this.value.length!=0) 
    {
      tot += parseFloat(this.value);
    }
  }); 

  <?php if(!empty($invoice_details['proposal_id'])){ ?>

  var value_at_tax = '<?php echo $amount_details['VAT']; ?>';
  $('#value_at_tax').val(Number(Number(value_at_tax)+Number(tot)).toFixed(2));

  <?php }else{ ?>
  $('#value_at_tax').val(Number(tot).toFixed(2));
  <?php } ?>
  update_gndtotal();
}



function update_gndtotal()
{
  var sub = 0;
  var vat = 0;
  var gnd_tot = 0;
  var ad = 0; 
 
  sub = parseFloat($('#sub_total').val());
  vat = parseFloat($('#value_at_tax').val());
  gnd_tot = sub + vat;

  var selTax = $('.select_tax').val();

  if(selTax == 1)
  {
    $('#grand_total').val(gnd_tot.toFixed(2));
    $('#grandtotal_hidden').val(gnd_tot.toFixed(2));    
  } 
  else 
  {
    <?php if(!empty($invoice_details['proposal_id'])){ ?>
    $('#grand_total').val(Number((Number('<?php echo $amount_details['VAT']; ?>')+Number(sub)) + Number($('#adjust_tax').val())).toFixed(2));
    $('#grandtotal_hidden').val(Number((Number('<?php echo $amount_details['VAT']; ?>')+Number(sub)) + Number($('#adjust_tax').val())).toFixed(2)); 
    <?php }else{ ?>
    $('#grand_total').val(sub.toFixed(2));
    $('#grandtotal_hidden').val(sub.toFixed(2)); 
    <?php } ?>   
  }
}

  
  


$(document).on('change', '.quantity', function(){
    cal_final_total(count);
  });

  $(document).on('change', '.unit_price', function(){
    cal_final_total(count);
  });

  $(document).on('change', '.discount', function(){
    cal_final_total(count);
  });   

  $(document).on('change', '.tax_rate', function(){
    cal_final_total(count);
  });

  $(document).on('change', '.amount_gbp', function(){
    cal_final_total(count);
  });

  $(document).on('keyup', 'input[name="repeat_trans"]', function(){
    var value = $(this).val();
    if(value != '')
    {
      $('.hide_first').css('display','block');
    }
    else
    {
      $('.hide_first').css('display','none');
    }
  });


$('#adjust_tax').change(function () {
 
    // initialize the sum (total price) to zero
    var sum = 0;
    // var b = $('.grandtotal_hidden').val();
    // we use jQuery each() to loop through all the textbox with 'price' class
    // and compute the sum for each loop
    // alert(sum);
    $('.adjust_tax').each(function() {
        a = parseFloat($(this).val());
        if(!isNaN(a) && a > 0) {
          sum = parseFloat($('.grandtotal_hidden').val()) + a;  
        } else {
          sum = parseFloat($('.grandtotal_hidden').val());
          // alert(sum);
        }
        
    });
    
     // console.log(sum);
    // set the computed value to 'totalPrice' textbox
    $('#grand_total').val(sum.toFixed(2));
     
});


// $.event.special.inputchange = {
//     setup: function() {
//         var self = this, val;
//         $.data(this, 'timer', window.setInterval(function() {
//             val = self.value;
//             if ( $.data( self, 'cache') != val ) {
//                 $.data( self, 'cache', val );
//                 $( self ).trigger( 'inputchange' );
//             }
//         }, 20));
//     },
//     teardown: function() {
//         window.clearInterval( $.data(this, 'timer') );
//     },
//     add: function() {
//         $.data(this, 'cache', this.value);
//     }
// };

// $('.description').on('inputchange', function() {
//   // alert(this.value);
//     // $('.invoice_bill_table td description').val(this.value);
//     var a = $(this).val();
//     // $(this).closest('.invoice_bill_table td').find('#description').val(a);
//     alert(a);
// });


//   $(document).ready(function() {
    
//     sum();
//     $("#adjust_tax").on("keydown keyup", function() {
//         sum();
//     });
// });

//   function sum() {
//             // var num = document.getElementById('sub_total').value;
//             // var num1 = document.getElementById('adjust_tax').value;

            
//             var num1 = $('#adjust_tax').val();
            
//           if(!isNaN(num1) && num1 > 0)
//           {
//             var c = parseFloat(num1)/100;
//             var num = parseFloat($('#sub_total').val());

//             // var ad = parseFloat(num1/100).toFixed(2);
//             // var num2 = document.getElementById('value_at_tax').value;
//             var num2 = parseFloat($('#value_at_tax').val());
//       // var result = parseInt(num1) + parseInt(num2);
//       // var result1 = parseFloat(num2) * parseFloat(ad);
//       var result1 = num2 * c;
//       var result2 = num + result1;
//       // var result2 = parseFloat(num) + parseFloat(result1);
//             if (!isNaN(result2)) {
//                 // document.getElementById('sum').value = result;
//         // document.getElementById('grand_total').value = result2;
//           $('#grand_total').val(result2.toFixed(2)); 
//             }
//           }  
            
//         }


});
</script>





<script>
$(document).ready(function(){
$('.number_only').keypress(function(e){
return isNumbers(e, this);      
});
function isNumbers(evt, element) 
{
var charCode = (evt.which) ? evt.which : event.keyCode;
if (
(charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
(charCode < 48 || charCode > 57))
return false;
return true;
}
});
</script>

<script>
  $(document).ready(function(){
    function onInvoiceInput(){
      console.log("hello click");
      $(".clr-check-select").change(function(){
        console.log("inside select change");
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		  });

      $(".clr-check-select").each(function(i){
        if($(this).val() !== ""){
            $(this).addClass("clr-check-select-client-outline");
        } 
      });

      $(".clr-check-client").keyup(function(){
        var clr = $(this).val();
        if(clr.length >= 3){
          $(this).addClass("clr-check-client-outline");
        }else{
          $(this).removeClass("clr-check-client-outline");
        }
      });

      $(".clr-check-client").each(function(i){
        if($(this).val() !== ""){
            $(this).addClass("clr-check-client-outline");
        } 
      });
    }
    onInvoiceInput();

    $(".add-newline").click(function(){
      onInvoiceInput();
    })
  })
    
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#preview-pdf a').on('click', function() 
    {
      datas = $('#invoice_form').serializeArray(); 
      
      $('.default_row').find('input,textarea').each(function()
      { 
         if($(this).hasClass('description'))
         {
           datas.push({name:'description[]',value:$(this).html()});
         }
         else if($(this).hasClass('quantity'))
         {
           datas.push({name:'quantity[]',value:$(this).val()});
         }
         else if($(this).hasClass('unit_price'))
         {
           datas.push({name:'unit_price[]',value:$(this).val()});
         }
         else if($(this).hasClass('amount_gbp'))
         {
           datas.push({name:'amount_gbp[]',value:$(this).val()});
         }
      }); 
      
      $.ajax({
        type: "POST",
        // dataType: 'JSON',
        url: "<?php echo base_url().'Invoice/previewInvoice';?>",
        data: datas,
        success:function(data) {
          // alert(data);
          //console.log( data );
          var con = $(data).find('tbody').html();
          //console.log( con );
          $('#invoice_bill_table tbody').html( con );

          var subValue = $(data).find('tfoot #sTot').html();
          $('#fetch_sub_total td.subTot .sub-quote01').html( subValue ); 
         

          var gndValue = $(data).find('tfoot #gTot').html();
          $('#fetch_sub_total td.gndTot .sub-quote01').html( gndValue ); 
         
          $(".grand_totel").html( gndValue);  

          var in_date = $(data).find('tfoot #invoiceDate').html();
          $('#date_of_invoice').html(in_date);

          var in_no = $(data).find('tfoot #invoiceNo').html();
          $('#in_number').html(in_no);




          
          

          // $('#des_pre').text(data.description);
        //   jQuery.each(data, function(index, item) {
            
        //     //alert(item);
        // });
            

            
        }

      });

    });


  });


      $('.decimal').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});

        $('.decimal_qty').keyup(function(evt){

     var keycode = evt.charCode || evt.keyCode;

      if (keycode  == 46) {
        return false;
      }
});
   var inputQuantity = [];
    $(function() {
      $(".decimal_qty").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".decimal_qty").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the index
//        window.console && console.log($field.is(":invalid"));
          //  $field.is(":invalid") is for Safari, it must be the last to not error in IE8
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 4);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    });

</script>
<?php $this->load->view('Invoice/package_scripts'); ?>
</body>
</html>