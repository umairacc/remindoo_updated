<?php 

$this->load->view('includes/header');

$cur_symbols = $this->Common_mdl->getallrecords('crm_currency');

foreach ($cur_symbols as $key => $value) 
{
   $currency_symbols[$value['country']] = $value['currency'];
}

?>
  <?php
    if(isset($proposal_details)){ ?>
<style type="text/css">
  
  .dis{
    display: none;
  }

    .tax{
    display: none;
  }


  .inner-addon { 
    position: relative; 
}

/* style icon */
.inner-addon .glyphicon {
  position: absolute;
  padding: 10px;
  pointer-events: none;
}

/* align icon */
.left-addon .glyphicon  { left:  0px;}
.right-addon .glyphicon { right: 0px;}

/* add padding  */
.left-addon input  { padding-left:  30px; }
.right-addon input { padding-right: 30px; }

</style>
<?php } ?>

<style type="text/css">
  .euro-position span { font-size: 12px; }
</style>

<div class="modal-alertsuccess alert  succ popup_info_box" style="display:none;">
  <div class="newupdate_alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              </div>
         </div>
         </div>
   </div>

<!-- management block -->
<div id="print_preview">
<div class="pcoded-content new-upload-03 new-invoice-wrapper">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper new-invoice">

         <div class="deadline-crm1 floating_set invoice-align1">
            <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                    <li class="nav-item">
                      <a href="#">New Invoice</a>
                    </li>
                  </ul>
          </div>

        <?php 
          echo $this->session->flashdata('invoice_success');
        ?>  

            <div class="firm-field floating_set">
              <form id="invoice_form" name="invoice_form" action="<?php echo base_url()?>invoice/CreateInvoice" method="post" enctype="multipart/form-data">
              <?php
                if(isset($proposal_details)){ ?>
                    <input type="hidden" name="draft_id" id="draft_id" value="<?php echo $proposal_details['id']; ?>">
                    <input type="hidden" name="proposal_id" id="proposal_id" value="<?php echo $proposal_details['proposal_id']; ?>">
                <?php } ?>  
               <div class="invoice-details">               
                     
                     <div class="invoice-top">
                        <span class="invoice-topinner dropdown-sin-5">
                           <label>to</label>
                        <select id="clint_email" class="clr-check-select-cus" name="clint_email" placeholder="Select User" data-parsley-required="true">  
                          <?php 
                          if(isset($proposal_details))
                          { 

                           $proposal = $this->Common_mdl->select_record('proposals','id',$proposal_details['proposal_id']);

                          ?>                        
                                     <option value="<?php echo $proposal['company_id']; ?>" <?php if($proposal_details['to_id']==$proposal['company_id']){ echo "selected"; } ?> ><?php if($proposal['receiver_mail_id'] != ''){ echo $proposal['receiver_mail_id']; } ?></option>
                              <?php 
                          }
                          else
                          {                               
                             $getallclients = $this->db->query("SELECT user.*,client.crm_email FROM user LEFT JOIN client ON user.id = client.user_id where user.autosave_status!='1' and user.crm_name!='' AND FIND_IN_SET(user.id,'".$clients_user_ids."') order by id DESC")->result_array(); 
                          ?>
                                <option value="">Select client </option>
                                 <?php 
                               foreach ($getallclients as $key => $value)
                               { 

                                $contact_mail=$this->db->query("select main_email from client_contacts where client_id=".$value['id']." and make_primary=1 limit 1")->row_array();
                                
                                if($contact_mail['main_email'] != '')
                                {
                            ?>   
                            <option value=<?php echo $value['id'];?>><?php echo $contact_mail['main_email']; ?></option>
                          <?php }
                                else if($value['crm_email_id'] != '')
                                { ?>
                            <option value=<?php echo $value['id'];?>><?php echo $value['crm_email_id']; ?></option>

                     <?php      } 
                                else if($value['crm_email'] != '')
                                { ?>
                            <option value=<?php echo $value['id'];?>><?php echo $value['crm_email']; ?></option>

                     <?php      }
                              }
                          } ?> 
                        </select> 
                        <label class="error client_email_erorr" ></label>
                        </span>
                        <span class="invoice-topinner">
                           <label>date</label>
                           <input type="text" name="invoice_date" class="clr-check-select datepicker1" id="currentDate" placeholder="dd-mm-yyyy" value="<?php if(isset($proposal_details)){  echo date('d-m-Y'); } ?>">
                        </span>
                        <span class="invoice-topinner form-group  date_birth">
                           <label>due date</label>
                           <!-- <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
                           <input type="text" name="due_date" class="form-control clr-check-select datepicker1 fields" placeholder="dd-mm-yyyy" required="required" value="<?php echo date('d-m-Y');  ?>">
                        </span>
                        <span class="invoice-topinner">
                           <label>invoice</label>
                           <input type="text" name="invoice_no" class="clr-check-client" readonly value="<?php if(isset($proposal_details)){ echo $proposal_details['invoice_no']; }else{ echo(mt_rand(100000,999999)); }?>" data-parsley-required="true">
                        </span>           

                         <span class="invoice-topinner side-inputbox01">
                            <label>repeat this transaction For</label>
                            <input type="text" name="repeat_trans" class="date-col date-col-wrap clr-check-client" style="min-width: 80px;">
                            <select name="select_transaction" style="min-width: 120px !important;" class="clr-check-select">
                               <option value="0">week(s)</option>
                               <option value="1">month(s)</option>
                               <option value="2">year(s)</option>
                            </select>
                            <p id="repeat_trans_er" style="color:red;"></p>
                         </span>                    
                     </div>

                     <div class="invoice-top">
                          <span class="invoice-topinner">
                          <label>end date (Optional)</label>
                          <div class='input-group date' id='datetimepicker6'>
                          <span class="input-group-addon ">
                          <span class="icofont icofont-ui-calendar"></span>
                          </span>
                          <input type="text" name="invoice_enddate" class="clr-check-select datepicker1" placeholder="dd-mm-yyyy">
                          </div>
                          </span>

                         <span class="invoice-topinner side-inputbox01">
                         <label>Day(s)&nbsp;&nbsp;&nbsp;<strong>due</strong></label>
                         <input type="text" name="duedate_no" class="date-col clr-check-client" style="min-width: 218px;">
                         <p>Day(s) After The Invoice Date</p>
                         <select name="repeat_days" style="display: none;">
                         <option value="1">day(s) after the invoice date</option>
                         </select>
                         <p id="duedate_no_er" style="color:red;"></p>
                         </span>
                      
                        <!--  <span class="invoice-topinner">
                            <label>reference</label>
                            <select name="reference" id="refered_by" class="fields" data-parsley-required="true" data-parsley-error-message="Reference is required" required="required">
                              <option value="">select</option>
                              <?php //foreach ($referby as $referbykey => $referbyvalue) {
                                 # code...
                                  ?>
                              <option value="<?php //echo $referbyvalue['id'];?>" <?php //if(isset($client[0]['crm_refered_by']) && $client[0]['crm_refered_by']==$referbyvalue['user_id']) {?> selected="selected"<?php //} ?>><?php //echo $referbyvalue['crm_first_name'];?></option>
                              <?php //} ?>
                           </select>
                            
                         </span> -->
                         
                         <span class="preview-pdf" id="preview-pdf">
                               <!-- <i class="fa fa-eye" aria-hidden="true"></i>
                               preview -->
                               <!-- <button type="button" data-toggle="modal" data-target="#temp_invoice" class="btn btn-card btn-primary" aria-hidden="true" id="preview_btn">
                               <i class="fa fa-eye fa-5"></i>
                               preview</button> -->
                               <?php  if(isset($proposal_details))
                         {
                           ?>
                          <div class="add-newline addmore inadd01">
                               <input type="button" class="btn btn-card btn-primary " id="addmore" value="add new line">
                               <i class="fa fa-plus" aria-hidden="true"></i>
                            </div>
                         <?php } ?>

                               <a href="#"  data-toggle="modal" data-target="#temp_invoice" class="btn btn-card btn-primary" aria-hidden="true" id="preview_btn"><i class="fa fa-eye fa-5"></i>preview</a>
                         </span>
                     </div> 
                     <input type="hidden" name="to_email">
                     <div class="amounts-detail pull-left">
                        <label>Service Category</label>
                        <select name="category_id" class="select_tax clr-check-select" <?php if(!isset($proposal_details)){ echo 'data-parsley-required="true"'; } ?>>
                          <option value="">Select Category</option>
                          <?php foreach ($category as $key => $value) { ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo ucwords($value['category_name']); ?></option>
                          <?php } ?>
                        </select>
                        <p id="category_id_er" style="color:red;"></p>
                     </div>
                     <div class="amounts-detail">
                        <label>amounts are</label>
                        <select name="amounts_details" class="select_tax clr-check-select" id="select_tax" data-parsley-required="true">
                           <option value="">Select</option>
                           <option value="1">Tax Exclusive</option>
                           <option value="2">Tax Inclusive</option>
                        </select>
                     </div>
                     <div class="invoice-table floating_set">

                      <div class="invoice-realign1 floating_set">
                       
                        <div class="table-responsive">
                           <table class="invoice_table" id="invoice_table">
                            <thead>
                               
                                 <th></th>
                                 <th>Service</th>
                                 <th>description</th>
                                 <th>qty</th>
                                 <th>unit price</th>
                                 <th class="discount_tr dis">disc%</th>
                                 <th>account</th>
                                 <th class="th_tax_rate tax td_tax_rate" id="th_tax_rate">tax rate</th>
                                 <th class="th_tax_amount" id="th_tax_amount">tax amount</th>
                                 <th>amount  <?php if(isset($proposal_details['currency'])){  echo $currency_symbols[$proposal_details['currency']];  }else{  if(isset($admin_settings['crm_currency'])){ echo $currency_symbols[$admin_settings['crm_currency']]; } }?></th>
                                 <th></th>
                              </thead>
                              <tfoot class="ex_data1">
                                <tr>
                                    <th>
                                    </th>
                                </tr>
                                </tfoot>
                                <tbody>
                                  <?php
                                      if(isset($proposal_details))
                                      {
                                          $item_name=explode(',',$proposal_details['item_name']);
                                          $price=explode(',',$proposal_details['price']);
                                          $qty=explode(',',$proposal_details['qty']);
                                          $tax=explode(',',$proposal_details['tax']);
                                          $discount=explode(',',$proposal_details['discount']);
                                          $description=explode(',',$proposal_details['description']);
                                          $product_name=explode(',',$proposal_details['product_name']);
                                          $product_price=explode(',',$proposal_details['product_price']);
                                          $product_qty=explode(',',$proposal_details['product_qty']);
                                          $product_description=explode(',',$proposal_details['product_description']);
                                          $product_tax=explode(',',$proposal_details['product_tax']);
                                          $product_discount=explode(',',$proposal_details['product_discount']);
                                          $subscription_name=explode(',',$proposal_details['subscription_name']);
                                          $subscription_price=explode(',',$proposal_details['subscription_price']);
                                          $subscription_unit=explode(',',$proposal_details['subscription_unit']);
                                          $subscription_description=explode(',',$proposal_details['subscription_description']);
                                          $subscription_tax=explode(',',$proposal_details['subscription_tax']);
                                          $subscription_discount=explode(',',$proposal_details['subscription_discount']);
                                          $k=1;  

                                          if($proposal['discount_amount'] <= '0' || $proposal['discount_amount'] == '')
                                          {
                                              $tax_amount = 0;         

                                              if(!empty($proposal['tax_amount']) && !empty($proposal['total_amount']))
                                              {
                                                  $tax_amount = $proposal['total_amount'] * ($proposal['tax_amount']/100);
                                              }       

                                              if((isset($proposal['total_amount']) && !empty($proposal['total_amount'])) && (isset($proposal['grand_total'])) && !empty($proposal['grand_total']) && $proposal['total_amount'] > (str_replace(',', '', $proposal['grand_total']) - $tax_amount)) 
                                              { 
                                                $proposal_details['discount_amount'] = number_format(($proposal['total_amount'] + $tax_amount) - str_replace(',', '', $proposal['grand_total']),2);
                                              }
                                          } 
                                          
                                    ?>
                                    <input type="hidden"  name="" id="tax_option" value="<?php echo $proposal_details['tax_option']; ?>">
                                    <input type="hidden"  name="" id="discount_option" value="<?php echo $proposal_details['discount_option']; ?>">
                                     <input type="hidden"  name="" id="taxes_amount" value="<?php echo $proposal_details['tax_amount']; ?>">
                                    <input type="hidden"  name="" id="discountes_amount" value="<?php echo $proposal_details['discount_amount']; ?>">                           


                                <?php  for($i=0;$i<count($item_name);$i++){ 

                                    if($item_name[0]!=''){
                                    ?>                                

                                   <tr  id="row_id_<?php echo $k; ?>" class="top-fields">

                      <td><span id="sr_no"><?php echo $k; ?></span></td>
                      <?php if(in_array($item_name[$i],array_column($services, 'service_name'))){ ?>
                      <td><select name="item_name[]" id="item_name<?php echo $k; ?>" class="form-control clr-check-select input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required"><option value="">Select</option><?php if(count($services)>0){ foreach($services as $value){ ?><option value="<?php echo $value['service_name']; ?>" <?php if($item_name[$i]==$value['service_name']) { echo "selected"; } ?>><?php echo $value['service_name']; ?></option><?php } } ?></select></td>
                      <?php }else{ ?>
                      <td><input type="text" name="item_name[]" id="item_name<?php echo $k; ?>" class="form-control clr-check-client input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required" value="<?php echo $item_name[$i]; ?>"></td>
                      <?php } ?>
                      <td><textarea name="description[]" id="description<?php echo $k; ?>" data-srno="1" class="form-control clr-check-client input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"><?php echo $description[$i]; ?></textarea> </td>

                      <td><input type="number" name="quantity[]" id="quantity<?php echo $k; ?>" data-srno="1" class="form-control clr-check-client input-sm  quantity decimal_qty" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php echo $qty[$i]; ?>" ></td>

                      <td><input type="text" name="unit_price[]" id="unit_price<?php echo $k; ?>" data-srno="1" class="form-control clr-check-client input-sm unit_price decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php echo $price[$i]; ?>"  ></td>

                      <td class="discount_tr dis"><input type="text" name="discount[]" id="discount<?php echo $k; ?>" data-srno="1" class="form-control clr-check-client input-sm discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php if($proposal_details['discount_option']!='total_discount'){if($discount[$i]!=''){ echo $discount[$i]; }else{ echo '0'; }}else{ echo '0'; } ?>"  ></td>

                      <td><input type="text" name="account[]" id="account<?php echo $k; ?>" data-srno="1" class="form-control clr-check-client input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>

                      <td class="td_tax_rate tax" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate<?php echo $k; ?>" data-srno="1" maxlength="4" class="form-control clr-check-client input-sm tax_rate decimal" data-parsley-required="true" data-parsley-type="number" value="<?php if($proposal_details['tax_option']!='total_tax'){ if($tax[$i]!=''){ echo $tax[$i]; }else{ echo '0';} }else{ echo '0'; } ?>" ></td>

                      <td class="td_tax_amount " id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount<?php echo $k; ?>" data-srno="1" class="form-control input-sm tax_amount" readonly /></td>

                      <td><input type="text" name="amount_gbp[]" id="amount_gbp<?php echo $k; ?>" data-srno="1" class="form-control clr-check-client input-sm amount_gbp" readonly /></td>
                        <td><button type="button" name="remove_row" id="<?php echo $k; ?>" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td>
                      </tr>

                      <?php $k++; }}
                                $m=$k;
                                  for($i=0;$i<count($product_name);$i++){
                                    if($product_name[0]!=''){
                                  ?>
                                   <tr  id="row_id_<?php echo $m; ?>" class="top-fields">
                                   <td><span id="sr_no"><?php echo $m; ?></span></td>

                      <td><input type="text" name="item_name[]" id="item_name<?php echo $m; ?>" class="form-control clr-check-client input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required" value="<?php echo $product_name[$i]; ?>"></td>

                      <td><textarea name="description[]" id="description<?php echo $m; ?>" data-srno="1" class="form-control clr-check-client input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"><?php echo $product_description[$i]; ?></textarea> </td>

                      <td><input type="number" name="quantity[]" id="quantity<?php echo $m; ?>" data-srno="1" class="form-control clr-check-select input-sm  quantity decimal_qty" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php echo $product_qty[$i]; ?>" ></td>

                      <td><input type="text" name="unit_price[]" id="unit_price<?php echo $m; ?>" data-srno="1" class="form-control clr-check-client input-sm unit_price decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php echo $product_price[$i]; ?>" /></td>

                      <td class="discount_tr dis"><input type="text" name="discount[]" id="discount<?php echo $m; ?>" data-srno="1" class="form-control clr-check-client input-sm discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php if($proposal_details['discount_option']!='total_discount'){ if($product_discount[$i]!=''){ echo $product_discount[$i]; }else{ echo '0'; } }else{ echo '0'; } ?>" ></td>

                      <td><input type="text" name="account[]" id="account<?php echo $m; ?>" data-srno="1" class="form-control clr-check-client input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>

                      <td class="td_tax_rate tax" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate<?php echo $m; ?>" data-srno="1" maxlength="4" class="form-control clr-check-client input-sm tax_rate decimal" data-parsley-required="true" data-parsley-type="number" value="<?php if($proposal_details['tax_option']!='total_tax'){ if($product_tax[$i]!=''){ echo $product_tax[$i]; }else{ echo '0'; }}else{ echo '0';} ?>" ></td>

                      <td class="td_tax_amount" id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount<?php echo $m; ?>" data-srno="1" class="form-control clr-check-client input-sm tax_amount" readonly /></td>

                      <td><input type="text" name="amount_gbp[]" id="amount_gbp<?php echo $m; ?>" data-srno="1" class="form-control clr-check-client input-sm amount_gbp" readonly /></td>
                        <td><button type="button" name="remove_row" id="<?php echo $m; ?>" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td>
                      </tr>

                      <?php $m++; }}

                          $n=$m;
                          for($i=0;$i<count($subscription_name);$i++){ 
                            if($subscription_name[0]!=''){
                            ?>

                            <tr  id="row_id_<?php echo $n; ?>" class="top-fields">
                            <td><span id="sr_no"><?php echo $n; ?></span></td>

                      <td><input type="text" name="item_name[]" id="item_name<?php echo $n; ?>" class="form-control clr-check-client input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required" value="<?php echo $subscription_name[$i]; ?>"></td>

                      <td><textarea name="description[]" id="description<?php echo $n; ?>" data-srno="1" class="form-control clr-check-client input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"><?php echo $subscription_description[$i]; ?></textarea> </td>

                      <td><input type="number" name="quantity[]" id="quantity<?php echo $n; ?>" data-srno="1" class="form-control clr-check-select input-sm  quantity decimal_qty" maxlength="4" data-parsley-required="true" data-parsley-type="number" data-parsley-min="1" value="1" ></td>

                      <td><input type="text" name="unit_price[]" id="unit_price<?php echo $n; ?>" data-srno="1" class="form-control clr-check-client input-sm unit_price decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" data-parsley-min="1" value="<?php echo $subscription_price[$i]; ?>" /></td>

                      <td class="discount_tr dis"><input type="text" name="discount[]" id="discount<?php echo $n; ?>" data-srno="1" class="form-control clr-check-client input-sm discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="<?php  if($proposal_details['discount_option']!='total_discount'){ if($subscription_discount[$i]!=''){ echo $subscription_discount[$i]; }else{ echo'0'; } }else{ echo '0'; }?>"  ></td>

                      <td><input type="text" name="account[]" id="account<?php echo $n; ?>" data-srno="1" class="form-control clr-check-client input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>

                      <td class="td_tax_rate tax" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate<?php echo $n; ?>" data-srno="1" maxlength="4" class="form-control clr-check-client input-sm tax_rate decimal" data-parsley-required="true" data-parsley-type="number" value="<?php  if($proposal_details['tax_option']!='total_tax'){ if($subscription_tax[$i]!=''){ echo $subscription_tax[$i]; }else{ echo '0'; } }else{ echo '0'; }?>" ></td>

                      <td class="td_tax_amount" id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount<?php echo $n; ?>" data-srno="1" class="form-control input-sm tax_amount" readonly /></td>

                      <td><input type="text" name="amount_gbp[]" id="amount_gbp<?php echo $n; ?>" data-srno="1" class="form-control clr-check-client input-sm amount_gbp" readonly /></td>
                      <td><button type="button" name="remove_row" id="<?php echo $n; ?>" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td>
                      </tr>
                                    
                              <?php   $n++;  }} ?>

                              <input type="hidden" name="counts_section" id="counts_section" value="<?php echo $n; ?>">

                  <?php  }else{ ?>
                     <tr class="default_row" id="default_row">
                       <td><span id="sr_no">1</span></td>
                      <td>
                        <select name="item_name[]" id="item_name1" class="form-control clr-check-select input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required">
                        <option value="">Select</option>
                        <?php if(count($services)>0){ foreach($services as $value){ ?>
                        <option value="<?php echo $value['service_name']; ?>"><?php echo $value['service_name']; ?></option>
                        <?php } } ?>
                        </select>
                      </td>  
                      <td><textarea name="description[]" id="description1" data-srno="1" class="form-control clr-check-client input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"></textarea> </td>

                      <td><input type="number" name="quantity[]" id="quantity1" data-srno="1" class="form-control clr-check-select input-sm  quantity decimal_qty" maxlength="4" data-parsley-required="true" data-parsley-type="number" data-parsley-min="1" value="0"></td>

                      <td><input type="text" name="unit_price[]" id="unit_price1" data-srno="1" class="form-control clr-check-client input-sm unit_price decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" data-parsley-min="1" value="0" /></td>

                      <td><input type="text" name="discount[]" id="discount1" data-srno="1" class="form-control clr-check-client input-sm discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0" ></td>

                      <td><input type="text" name="account[]" id="account1" data-srno="1" class="form-control clr-check-client input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>

                      <td class="td_tax_rate" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate1" data-srno="1" maxlength="4" class="form-control clr-check-client input-sm tax_rate decimal" data-parsley-required="true" data-parsley-type="number"  value="0"></td>

                      <td class="td_tax_amount" id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount1" data-srno="1" class="form-control clr-check-client input-sm tax_amount" readonly /></td>

                      <td><input type="text" name="amount_gbp[]" id="amount_gbp1" data-srno="1" class="form-control clr-check-client input-sm amount_gbp" readonly /></td>

                      <td>
                        <div class="add-newline addmore">
                              <input type="button" class="btn btn-card btn-primary " id="addmore" value="add new line">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                           </div>

                      </td>
                      <!-- <td><button type="button" name="remove_row" id="1" class="btn btn-danger btn-xs remove_row" aria-hidden="true">x</button></td></td> -->

                <!-- <td><input type="hidden" name="act_amt" id="actual_amount1" data-srno="1" class="data-srno="1" class="form-control input-sm actual_amount"> </td>   -->   
                    </tr>
                      <?php } ?>        
                  </tbody>
                  </table>         
                  </div>       
                </div>
                      <div class="cmn-invoice-approve floating_set">
                        <div class="right-invoice-new1">
                           <div class="sub-tot_table" id="sub-tot_table">
                              <table id="sub_tot_table" class="sub_tot_table">
                                 <tbody>
                                    <tr>
                                       <th>sub total</th>
                                       <td> 
                                        <div class="input-group">
                                              <div class="input-group-addon">
                                             <?php  if(isset($proposal_details['currency'])){  echo $currency_symbols[$proposal_details['currency']];  }else{  if(isset($admin_settings['crm_currency'])){ echo $currency_symbols[$admin_settings['crm_currency']]; } }?>
                                              </div>

                                              <div class="euro-position">
                                       <strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong>
                                       <input type="text" name="sub_tot" id="sub_total" class="form-control input-sm txtcal sub_total" value="0.00" readonly>
                                     </div>

                                       </div>
                                     </td>
                                    </tr>
                                    <tr id="vat_row" class="vat_row">
                                       <th>VAT</th>
                                       <td><input type="text" name="value_at_tax" id="value_at_tax" class="form-control input-sm txtcal value_at_tax" value="0.00" readonly></td>
                                    </tr>
                                    <tr>
                                       <th>includes adjustments to Tax</th>
                                       <td><input type="text" name="adjust_tax" id="adjust_tax" class="form-control input-sm adjust_tax" value="0.00"></td>
                                    </tr>
                                    <tr class="grand-total">
                                       <th>total</th>
                                       <td>
                                          <div class="input-group">
                                            <div class="input-group-addon">
                                             <?php  if(isset($proposal_details['currency'])){  echo $currency_symbols[$proposal_details['currency']];  }else{  if(isset($admin_settings['crm_currency'])){ echo $currency_symbols[$admin_settings['crm_currency']]; } }?>
                                            </div>

                                            <div class="euro-position">
                                        <strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong>
                                            <input type="text" name="grand_total" id="grand_total" class="form-control input-sm grand_total" value="0.00" readonly>
                                           </div>

                                        </div></td>

                                    <input type="hidden" name="grandtotal_hidden" id="grandtotal_hidden" class="form-control input-sm grandtotal_hidden" value="0.00">   
                                       
                                    </tr>
                                 </tbody>
                              </table>
                           
                         </div>
                    </div>

                     <div class="attach-invoice1">
                      <div class="custom_upload upload-data06">
                      <label>Attachment</label>
                           <input type="file" id="files" name="userFiles[]" multiple="multiple">
                           </div>
                        <div id="image_preview">
                          
                        </div>                         
                     
                    <input type="hidden" name="attch_image" id="attch_image" value="<?php if(isset($records)){ if($records['attachment']!=''){  echo $records['attachment']; }} ?>">

                    <div class="decline-quote1 floating_set">
                    <div class="save-approve" id="save-approve">
                      <select name="transaction_payment" class="select_payment" id="select_payment">
                       <option value="approve"> Approve </option> 
                       <option value="decline"> Decline </option> 
                       <option value="pending"> Pending </option> 
                      </select> 
                      <div class="approve-btn">
                          <div class="table-savebtn">
                          <input type="hidden" name="total_item" class="total_item" id="total_item" value="1">
                          <input type="submit" class="btn btn-card btn-primary" id="invoice_save"  value="Send">
                          <input type="reset" style="display:none">

                          </div>

                         <a href="#" onclick="return reset_form()" class="cancel-btn">cancel</a> 
                      </div>
                    </div>                              
                  </div>
               </div>
              </div>
            </div><!--invoice table -->
           </div>
          </form>   
        </div>
      </div>
    </div>
   </div>
</div>
</div>
 <?php ?>
<!-- Modal -->
<!-- Modal -->
  <div class="modal fade modal-invoice-quote1" id="temp_invoice" role="dialog">
    <div class="modal-dialog modal-quote-invoice">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Invoice</h4>
        </div>
        <div class="modal-body">
        <form action="#" method="post">
          <div id="generate_invoice" class="invoice-details generate-invoice">
               <!-- <div class="update-logo">
                    <input type="file">
                    <div class="up-logo">
                    </div>
                  </div> -->
                  <div class="online-payments">
                    <div class="online-qut">
                    <div class="temp-edit">
                      <h3>Remindoo.org</h3>
                    </div>
                    <div class="payment-table newlayout">                        
                            <h3><?php  if(isset($proposal_details['currency'])){  echo $currency_symbols[$proposal_details['currency']];  }else{  if(isset($admin_settings['crm_currency'])){ echo $currency_symbols[$admin_settings['crm_currency']]; } } ?><span class="grand_totel"></span></h3>
                            <div class="divok">
                              <div class="uprecs">
                                <h3 class="upon-res">Upon Reciept</h3>
                                <h6>Due</h6>   
                              </div>
                              <div class="duenoin">                      
                                <span class="due-date12" id="in_number"></span>
                                <h5 class="invoice-no">invoic no</h5>
                              </div>
                            </div>
                    </div>
                  </div>
                  </div>
                  <div class="generate-top">
                    <span class="project-name">
                      Invoice
                     <!--  <span>project name</span> -->
                    </span>
                    <span class="date-sec">
                      <strong><div class="g-date" id="date_of_invoice"></div></strong>
                      <!-- <input type"text" class="g-date" id="date_of_invoice" value="12/2/2018">  -->
                    </span>
                  </div>
                  <div class="des-table invoice-table">
                    <div class="description-inv1">
                  <div class="table-responsive">
                      <table class="invoice_bill_table" id="invoice_bill_table">
                            <thead>    
                              <tr>
                                 <th>description</th>
                                 <th>qty</th>
                                 <th>price</th>
                                 <th style="width:5%;">amount</th>
                              </tr>
                            </thead> 
                            <tfoot class="ex_data1">
                              <tr>
                                <th>
                                </th>
                              </tr>
                            </tfoot>  
                            <tbody>
                              <tr class="fetch_table" id="fetch_table">
                                <td></td>

                                <td></td>

                                <td><i class="fa fa-gbp"></i>
                                 </td>

                                <td><i class="fa fa-gbp"></i> </td>

                              </tr>                             
                            </tbody>    
                          </table>
                       </div>
                  </div>

                    <div class="sub-totaltab sub-tot_table">
                      <table class="fetch_sub_total" id="fetch_sub_total">
                          <tr>
                            <th>sub total</th>
                            <td class="subTot">
                              <!-- <i class="fa fa-gbp"> </i> -->
                              <div class="euro-position">
       <strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong>
       <div class="sub-quote01">
       </div>
     </div>
                            </td>
                          </tr>
                          <tr>
                            <th>total</th>
                            <td class="gndTot">
                              <!-- <i class="fa fa-gbp"> </i> -->
                              <div class="euro-position">
       <strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong>
       <div class="sub-quote01">

       </div>
     </div>
                            </td>
                          </tr>
                      </table>                     
                    </div>
                  </div>
                  <div class="payment-ins">
                 <!--    <span>payment instrctions</span> -->
                    </div>
                <!--   <div class="payment-ins">
                    <span class="place">united kingdom</span>
                  </div> -->
                <input type="hidden" name="total_item" class="total_item" id="total_item" value="1">
                  
               </div>
            </form>   
        </div>
       <!--  <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>


<!-- Modal -->

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<script>

function reset_form()
{
  $("#invoice_form").reset();
  return false;
}
   $( document ).ready(function() {
   
       var date = $('.datepicker1').datepicker({ dateFormat: 'dd-mm-yy',  minDate:0, changeMonth: true,
        changeYear: true, }).val();

      $('#currentDate').datepicker({ dateFormat: 'dd-mm-yy'});
      $('#currentDate').datepicker('setDate', 'today');
   
       // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#1abc9c',
               jackColor: '#fff',
               size: 'small'
           });
       });
   
       $('#accordion_close').on('click', function(){
               $('#accordion').slideToggle(300);
               $(this).toggleClass('accordion_down');
       });
   });
</script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script type="text/javascript">
 $('.dropdown-sin-5').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });
  
</script>

<!-- <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
 -->

<!-- <script type="text/javascript">$(document).ready(function(){
var number = 1 + Math.floor(Math.random() * 999999999);
//$("#item1").hide();
$("input[name='refnumber']").val(number);

});
</script> -->

<script type="text/javascript">
  $(document).ready(function() {
    var sub_total_amount = $('#sub_total').val();

<?php if(isset($proposal_details)){ ?>
var count = $('#counts_section').val();

/*console.log('Counts');
console.log(count);*/
<?php }else{ ?>
var count = 1;
<?php } ?>

    $('#select_tax').on('change', function() 
    {       
       var select_val = $(this).val();
       if(select_val == 1)
       {
         $('table tr > th:nth-child(8), table tr > td:nth-child(8)').show();
         $('table tr > th:nth-child(9), table tr > td:nth-child(9)').show();
         $('#sub_tot_table tr.vat_row').show();
         // $('#sub-tot_table').show();
         // $('.approve-btn').show();
       } 
       else 
       {
         $('table tr > th:nth-child(8), table tr > td:nth-child(8)').hide();
         $('table tr > th:nth-child(9), table tr > td:nth-child(9)').hide();

         if(Number($('.value_at_tax').val()) <= '0')
         {
           $('#sub_tot_table tr.vat_row').hide();
         }
         else
         {
           $('#sub_tot_table tr.vat_row').show();
         }
         // $('#sub-tot_table').hide();
         // $('.approve-btn').hide();
       }

       $(".tax_rate").val(0);
       $(".tax_amount").val(0);
       $(".tax_rate").eq( 0 ).trigger('change');       
     });

    $(document).on('click','.addmore', function() { 
      var a = $('.select_tax').val();
     
      count++;
      $('.total_item').val(count);
      var html_code = '';
      var html_code1 = '';

      if(a == 1) 
      {
      html_code += '<tr id="row_id_'+count+'">';
      html_code += '<td><span id="sr_no">'+count+'</span></td>';
      html_code += '<td><select name="item_name[]" id="item_name'+count+'" class="form-control clr-check-select input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required"><option value="">Select</option><?php if(count($services)>0){ foreach($services as $value){ ?><option value="<?php echo $value['service_name']; ?>"><?php echo $value['service_name']; ?></option><?php } } ?></select></td>';
      html_code += '<td><textarea name="description[]" id="description'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"></textarea></td>';
      html_code += '<td><input type="number" name="quantity[]" id="quantity'+count+'" data-srno="'+count+'" class="form-control clr-check-select input-sm quantity decimal_qty" maxlength="4" data-parsley-required="true" data-parsley-type="number" data-parsley-min="1"  value="0" ></td>';
      html_code += '<td><input type="text" name="unit_price[]" id="unit_price'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm unit_price decimal" maxlength="6" data-parsley-required="true" data-parsley-type="number" data-parsley-min="1" value="0" ></td>';
      html_code += '<td class="discount_tr dis"><input type="text" name="discount[]" id="discount'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm number_only discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0" ></td>';
      html_code += '<td><input type="text" name="account[]" id="account'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>';
      html_code += '<td class="td_tax_rate tax" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm number_only tax_rate decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0"></td>';
      html_code += '<td class="td_tax_amount" id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm tax_amount" readonly></td>';
      html_code += '<td><input type="text" name="amount_gbp[]" id="amount_gbp'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm amount_gbp" readonly /></td>';
      html_code += '<td><button type="button" name="remove_row" id="'+count+'" class="btn btn-danger btn-xs remove_row" aria-hidden="true"></button></td>';  

        <?php if(isset($proposal_details)){ ?>

        var section_count=$("#counts_section").val();

        var sec=Number(section_count) + 1;

        $("#counts_section").val(sec);
        <?php } ?>

      // html_code += '<td><input type="hidden" name="act_amt" id="actual_amount'+count+'" data-srno="'+count+'" class="data-srno="1" class="form-control input-sm actual_amount"> </td>';
      html_code += '</tr>';
       } else {
 
      html_code += '<tr id="row_id_'+count+'">';
      html_code += '<td><span id="sr_no">'+count+'</span></td>';
      html_code += '<td><select name="item_name[]" id="item_name'+count+'" class="form-control clr-check-select input-sm item_name" data-parsley-required="true" data-parsley-error-message="Item is required"><option value="">Select</option><?php if(count($services)>0){ foreach($services as $value){ ?><option value="<?php echo $value['service_name']; ?>"><?php echo $value['service_name']; ?></option><?php } } ?></select></td>';
      html_code += '<td><textarea name="description[]" id="description'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm description" data-parsley-required="true" data-parsley-error-message="Description is required"></textarea></td>';
      html_code += '<td><input type="number" name="quantity[]" id="quantity'+count+'" data-srno="'+count+'" class="form-control clr-check-select input-sm quantity decimal_qty"  maxlength="4" data-parsley-required="true" data-parsley-type="number" data-parsley-min="1" value="0"></td>';
      html_code += '<td><input type="text" name="unit_price[]" id="unit_price'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm unit_price decimal" maxlength="6" data-parsley-required="true" data-parsley-type="number" data-parsley-min="1" value="0"></td>';
      html_code += '<td class="discount"><input type="text" name="discount[]" id="discount'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm number_only discount decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0"></td>';
      html_code += '<td><input type="text" name="account[]" id="account'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm account decimal" data-parsley-required="true" data-parsley-error-message="Account is required"></td>';
      html_code += '<td class="td_tax_rate tax" style="display:none" id="td_tax_rate"><input type="text" name="tax_rate[]" id="tax_rate'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm number_only tax_rate decimal" maxlength="4" data-parsley-required="true" data-parsley-type="number" value="0" <?php if(isset($proposal_details)){ ?> onchange="qty_check()" <?php } ?>></td>';
      html_code += '<td class="td_tax_amount" style="display:none" id="td_tax_amount"><input type="text" name="tax_amount[]" id="tax_amount'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm tax_amount" readonly></td>';
      html_code += '<td><input type="text" name="amount_gbp[]" id="amount_gbp'+count+'" data-srno="'+count+'" class="form-control clr-check-client input-sm amount_gbp" readonly /></td>';
      html_code += '<td><button type="button" name="remove_row" id="'+count+'" class="btn btn-danger btn-xs remove_row" aria-hidden="true"></button></td>';  

      // html_code += '<td><input type="hidden" name="act_amt" id="actual_amount'+count+'" data-srno="'+count+'" class="data-srno="1" class="form-control input-sm actual_amount"> </td>';

      html_code += '</tr>';

      }
     
      $('#invoice_table').append(html_code);

       <?php 
       if(isset($proposal_details)){

        if($proposal_details['tax_option']!='total_tax'){ ?>
        console.log('line tax');
        var counting_section="#row_id_"+count;
        console.log(counting_section);
        $(counting_section).find(".td_tax_rate").removeClass('tax');
         console.log($(counting_section).find(".td_tax_rate").attr('class'));

      <?php } ?>

      <?php   if($proposal_details['discount_option']!='total_discount'){ ?>
         console.log('line discount');
           var counting_section="#row_id_"+count;
            console.log(counting_section);
            $(counting_section).find(".discount_tr").removeClass('dis');
            console.log( $(counting_section).find(".discount_tr").attr('class'));
      <?php   } } ?>
      // $('#invoice_bill_table').append(html_code1);

      // var data=$("#description").html();

      // // $(".body-popup-content").html('');
      // $("#invoice_bill_table").append(data);

    });

  $(document).on('click', '.remove_row', function() { 
    var row_id = $(this).attr("id");
    // alert(row_id);
    var final_amount = $('#amount_gbp'+row_id).val();

    var sub_total = $('#sub_total').val();
    var result_amt = parseFloat(sub_total) - parseFloat(final_amount);
    $('#sub_total').val(result_amt);

    var tax_amt = $('#tax_amount'+row_id).val();
    if(tax_amt!=''){
    var vat_amount = $('#value_at_tax').val();
    var vat_result = parseFloat(vat_amount) - parseFloat(tax_amt);
    $('#value_at_tax').val(vat_result);
   // calculateSum();

  }
   
    // var gndtot = parseFloat(result_amt) + parseFloat(vat_result);
    // $('#grand_total').val(gndtot);

    $('#row_id_'+row_id).remove();
    count--;

 calculateSum();
    $('.total_item').val(count);  

  });

  function cal_final_total(count)
  {  //alert("test");
    //console.log(count + 1);
    var count=Number(count) + 1;
    //console.log(count);
    //console.log(count);
    var a = $('.quantity').val();

    console.log(a);
    
    var final_total_amount = 0;
    for(j=1; j<=count; j++)
    { 
      var item = 0;
      var descre = 0;
      var qty = 0;
      var price = 0;
      var discount = 0;
      var account = 0;
      var taxRate = 0;
      var taxAmount = 0;
      var amount = 0;
      
    qty = $('#quantity'+j).val();
    if(qty > 0)
    {  
      price = $('#unit_price'+j).val();
      if(price > 0)
      {
        amount = parseFloat(qty) * parseFloat(price);
        $('#amount_gbp'+j).val(amount);

        // $('#amount_gbp'+j).each(function() {
        //     $(this).keyup(function() {

        //       calculateSum();
        //     });
        //   });
        calculateSum();
        
        discount =parseFloat( $('#discount'+j).val() );
        console.log('Discount');
        console.log(discount);

        <?php 
       // if(isset($proposal_details)){ ?>        

       <?php   //}else{ ?>
        if(discount >= 0)
        { 
          var dis = (discount/100).toFixed(2);
          var mult = amount*dis;
          var result_amt = amount-mult;
          $('#amount_gbp'+j).val(result_amt);
          
          calculateSum();
          
        }

        <?php //} ?>

        taxRate = parseFloat( $('#tax_rate'+j).val() );

        <?php 
 ?>
        if(taxRate >= 0)
        { 
          taxAmount = (parseFloat(amount) * taxRate)/100;
          $('#tax_amount'+j).val(taxAmount);

          cal_tax_amount();
          
        }

        <?php //} ?>
      }        
    }     
    }  
  }

function calculateSum()
{
  var sum = 0;
  $('.amount_gbp').each(function() {
    if(!isNaN(this.value) && this.value.length!=0) {
      sum += parseFloat(this.value);
      // alert(sum);
    }
  });
  $('#sub_total').val(sum.toFixed(2));

  // $('#grand_total').val(sum.toFixed(2));
  update_gndtotal();
}


function cal_tax_amount()
{
      var tot = 0;
      $('.tax_amount').each(function() {
        if(!isNaN(this.value) && this.value.length!=0) {
          tot += parseFloat(this.value);
        }
      });
      $('#value_at_tax').val(tot.toFixed(2));
      update_gndtotal();

 <?php //} ?>
}

function update_gndtotal()
{

var sub = 0;
var vat = 0;
var gnd_tot = 0;
var ad = 0; 
 
  sub = parseFloat($('#sub_total').val());
  vat = parseFloat($('#value_at_tax').val());

  gnd_tot = sub + vat;

  //console.log(gnd_tot);
  $('#grand_total').val(gnd_tot.toFixed(2));
  $('#grandtotal_hidden').val(gnd_tot.toFixed(2));

}

  $(document).on('change', '.quantity', function(){
      <?php
    if(isset($proposal_details)){ ?>
      qty_check();
 //   cal_final_total($('#counts_section').val());
    <?php }else{ ?>
    cal_final_total(count);
    <?php } ?>
  });

  $(document).on('change', '.unit_price', function(){ 
      <?php
    if(isset($proposal_details)){ ?>
      qty_check();
 //   cal_final_total($('#counts_section').val());
    <?php }else{ ?>
    cal_final_total(count);
    <?php } ?>
  });

  $(document).on('change', '.discount', function(){
       <?php
    if(isset($proposal_details)){ ?>
       qty_check();
  //  cal_final_total($('#counts_section').val());
    <?php }else{ ?>
    cal_final_total(count);
    <?php } ?>
  });   

  $(document).on('change', '.tax_rate', function(){
       <?php
    if(isset($proposal_details)){ ?>
       qty_check();
   // cal_final_total($('#counts_section').val());
    <?php }else{ ?>
    cal_final_total(count);
    <?php } ?>
  });

  $(document).on('change', '.amount_gbp', function(){
       <?php
    if(isset($proposal_details)){ ?>
       qty_check();
   // cal_final_total($('#counts_section').val());
    <?php }else{ ?>
    cal_final_total(count);
    <?php } ?>
  });


$('#adjust_tax').change(function () {
 
    // initialize the sum (total price) to zero
    var sum = 0;
    // var b = $('.grandtotal_hidden').val();
    // we use jQuery each() to loop through all the textbox with 'price' class
    // and compute the sum for each loop
    // alert(sum);
    $('.adjust_tax').each(function() {
        a = parseFloat($(this).val());
        if(!isNaN(a) && a > 0) {
          sum = parseFloat($('.grandtotal_hidden').val()) + a;  
        } else {
          sum = parseFloat($('.grandtotal_hidden').val());
          // alert(sum);
        }
        
    });
    
     // console.log(sum);
    // set the computed value to 'totalPrice' textbox
    $('#grand_total').val(sum.toFixed(2));
     
});
});
</script>
<script>
  $(document).ready(function(){
    $('.number_only').keypress(function(e){
      return isNumbers(e, this);      
    });

    function isNumbers(evt, element) 
      {
        var charCode = (evt.which) ? evt.which : event.keyCode;
          if (
          (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
          (charCode < 48 || charCode > 57))
          return false;
          return true;
      }
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy' }).val();
  });

</script>

<script>
  $(document).ready(function(){
    function onInvoiceInput(){
      $(".clr-check-select").change(function(){
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		  });

      $(".clr-check-select").each(function(i){
        if($(this).val() !== ""){
            $(this).addClass("clr-check-select-client-outline");
        } 
      });

      $(".clr-check-client").keyup(function(){
        var clr = $(this).val();
        if(clr.length >= 3){
          $(this).addClass("clr-check-client-outline");
        }else{
          $(this).removeClass("clr-check-client-outline");
        }
      });

      $(".clr-check-client").each(function(i){
        if($(this).val() !== ""){
            $(this).addClass("clr-check-client-outline");
        } 
      });
    }
    onInvoiceInput();

    $(".add-newline").click(function(){
      onInvoiceInput();
    })

    $(".clr-check-select-cus").change(function(){
      $(this).next().next().addClass("clr-check-select-cus-wrap");
    })

    $(".clr-check-select-cus").each(function(){
      if($(this).val() !== ""){
        $(this).next().next().addClass("clr-check-select-cus-wrap");
      }  
    })
})
    
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#preview-pdf a').on('click', function() {
      
      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'Invoice/previewInvoice';?>",
        data: $('#invoice_form').serialize(),
        success:function(data) {
          var con = $(data).find('tbody').html();
          $('#invoice_bill_table tbody').html( con );
          var subValue = $(data).find('tfoot #sTot').html();
          $('#fetch_sub_total td.subTot .sub-quote01').html( subValue ); 
          var gndValue = $(data).find('tfoot #gTot').html();
          $('#fetch_sub_total td.gndTot .sub-quote01').html( gndValue );  
           $(".grand_totel").html( gndValue);  
          var in_date = $(data).find('tfoot #invoiceDate').html();
          $('#date_of_invoice').html(in_date);
          var in_no = $(data).find('tfoot #invoiceNo').html();
          $('#in_number').html(in_no);
        }

      });

    });


  });

/*
  $("#invoice_save").click(function(event) {
  //event.preventDefault();
var client_email=$("#clint_email").val();
if ($.trim(client_email).length == 0) {
alert('All fields are mandatory');
$(".client_email_erorr").("Select Any Client");
event.preventDefault();

}
if (client_email!='') {
alert('email valide');
if($("input[name=due_date]").val()!=''){
alert('INSIDE -SUMMIT');
 $("#invoice_form").submit();

}
}else {
  $("#clint_email").css('border','1px solid red');
//alert('Invalid Email Address');
//event.preventDefault();
}

});


  function validateEmail(sEmail) {
var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
if (filter.test(sEmail)) {
return true;
}
else {
return false;
}
}*/

    $('.decimal').keyup(function(){
    //  alert('gdfgfg');
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});

  $('.decimal_qty').keyup(function(evt){

     var keycode = evt.charCode || evt.keyCode;
      if (keycode  == 46) {
        return false;
      }
});


   var inputQuantity = [];
    $(function() {
      $(".decimal_qty").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".decimal_qty").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the index
//        window.console && console.log($field.is(":invalid"));
          //  $field.is(":invalid") is for Safari, it must be the last to not error in IE8
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 4);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    });
    <?php
    if(isset($proposal_details)){ ?>
      $( document ).ready(function() {
      <?php  if($proposal_details['tax_option']!='total_tax'){ ?>

          $(".td_tax_rate").removeClass('tax');

      <?php } ?>

      <?php   if($proposal_details['discount_option']!='total_discount'){ ?>
             $(".discount_tr").removeClass('dis');
      <?php   } ?>
      //  console.log('work');
           // $(".discount").trigger('blur');
           // $(".unit_price").trigger('blur');
           // $(".amount_gbp").trigger('blur');
           // $(".tax_rate").trigger('blur');
          // console.log('trigger');
          qty_check();

      });

    <?php }else{ ?>

 $(".td_tax_rate").removeClass('tax');
 $(".discount_tr").removeClass('dis');

   <?php } ?>

    function cal_taxes_count()
      {
        var tot=$('#sub_total').val();
        console.log(tot);
        var taxamountes=$("#taxes_amount").val();

        tot = parseFloat(tot) * parseFloat(taxamountes)/100;
        console.log(tot);
        console.log('Working');

        alert('ok');
        $('#value_at_tax').val(tot);
        //update_gndtotal();  
        
          sub = parseFloat($('#sub_total').val());
          vat = parseFloat($('#value_at_tax').val());

          gnd_tot = sub + vat;

          console.log(gnd_tot);
          $('#grand_total').val(gnd_tot.toFixed(2));
          $('#grandtotal_hidden').val(gnd_tot.toFixed(2));

      }
    function cal_discount_count()
    {
      var tot=$('#grand_total').val();
      console.log(tot);
      var taxamountes=$("#discountes_amount").val();
      console.log('Discount');
      console.log(taxamountes);
      tot = parseFloat(tot) * parseFloat(taxamountes)/100; 
        sub = parseFloat($('#grand_total').val());
        gnd_tot = sub - tot;

        console.log(gnd_tot);
        $('#grand_total').val(gnd_tot.toFixed(2));
        $('#grandtotal_hidden').val(gnd_tot.toFixed(2));

      // cal_discount_count();
    }

  function qty_check()
  {   
    var sum=0;
    var vat_tot=0;    
    
    $('.unit_price').each(function() 
    {         
        var qty_1 = Number($(this).parents('.top-fields').find('.quantity').val());
        //  console.log(qty_1);
        var tax = Number($(this).parents('.top-fields').find('.tax_rate').val());
        //   console.log(tax);
        var discount = Number($(this).parents('.top-fields').find('.discount').val()); 
        //  console.log(discount);
        var price = Number($(this).val());
        //console.log(price);
        var amount = Number(price * qty_1).toFixed(2);

        var dec = Number(tax/100).toFixed(2);  

        //var discount1 = (discount/100).toFixed(2);    
        if(dec > '0')
        {  
           var mult = Number(amount* dec).toFixed(2); 
           $(this).parents('.top-fields').find('.tax_amount').val(mult);
        }
        else
        {
           $(this).parents('.top-fields').find('.tax_amount').val(amount);
        }
        //      console.log(mult); 
        //var dis_count = (tax_amount* discount1).toFixed(2); 
        //var price_cal=(Number($(this).val()) * Number(qty_1)).toFixed(2);     
        
        if(discount != "" && discount != '0' && discount != 'null') 
        {
           var dis_count = Number(discount).toFixed(2);
           var calculation = (Number(amount) - Number(dis_count)).toFixed(2); 
        }
        else
        {
           var calculation = amount;
        }

        $(this).parents('.top-fields').find('.amount_gbp').val(calculation);
        var vat_tot = 0;
        $('.tax_amount').each(function() 
        {  
          vat_tot += Number($(this).val());    
        });     

        $(".value_at_tax").val(Number(vat_tot).toFixed(2));

        var sum=0;

        $('.amount_gbp').each(function() 
        {
          sum += Number($(this).val());      
        });
        //console.log(sum);
        $(".sub_total").val(Number(sum).toFixed(2));

        var sub = $(".sub_total").val();

        var vat = $(".value_at_tax").val();

        var total = Number(sub) + Number(vat);

        $(".grand_total").val(Number(total).toFixed(2));         

    });

    if($("#tax_option").val()=="line_tax")
    {
      var tax_amount=0;
    }
    else
    {
      var tax_amount = Number($("#taxes_amount").val());    
      var calcPrice  = Number(Number(Number($(".sub_total").val()) * Number(tax_amount / 100)) + Number($(".value_at_tax").val())).toFixed(2);     

      $(".value_at_tax").val(calcPrice);
      //  $('.tax_total').html( '' );
      // $('.tax_total').html( calcPrice );
      var grandPrice  = Number(Number($(".sub_total").val()) + Number(calcPrice)).toFixed(2);
      //$('.grand_total').html( '' );
      $('.grand_total').val(grandPrice);
    }

    if($("#discount_option").val()=="line_discount")
    {
       var discount_amount=0;
    } 
    else
    {
       var discount_amount = Number($("#discountes_amount").val());
       var grand_totals = Number($(".grand_total").val());       

       if(discount_amount != '' && discount_amount > '0')
       { 
          var grandPrice  = Number(Number(grand_totals) - Number(discount_amount)).toFixed(2);
          $('.grand_total').val(grandPrice);
          $('#adjust_tax').val('-'+discount_amount);
       }
       //var disPrice  = Number(grand_totals * discount_amount / 100).toFixed(2);
      //  console.log(calcPrice);

      //  $(".value_at_tax").val(Math.round(calcPrice));
      //  $('.tax_total').html( '' );
      // $('.tax_total').html( calcPrice );
       
      //$('.grand_total').html( '' );
       
    }
}
     $(document).ready(function() {
        if (window.File && window.FileList && window.FileReader) {
          $("#files").on("change", function(e) {    
             $("#image_preview").html('');   
            var files = e.target.files,
            filesLength = files.length;
              for (var i = 0; i < filesLength; i++) {

                  var filenames = this.files[i].name;                     
                  var fileType=files[i].type;
                  var fileSize=files[i].size / 1024 / 1024;     
                  var fileName= files[i].name;  
                  console.log(fileName);
                  if(fileSize <= 2)
                  {
                  var ValidImageTypes = ["application/pdf", "image/jpg", "image/png","image/jpeg","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel","text/csv","application/doc","application/docx","application/msword"];
                  if ($.inArray(fileType, ValidImageTypes) < 0) {
                     $("#upload").attr('disabled','disabled');
                     $("#error_content").html('Document Must be Png,JPG,JPEG,PDF,xlsx,csv Format');
                   //  $("#Error_show").modal('show');
   
                   $(".Error-alert").show();
                    // alert('Document Must be Png,JPG,JPEG,PDF Format');
                  }else{ 
                     $("#upload").removeAttr("disabled");
                   if(fileType =='application/pdf'){

                      $('#image_preview').append("<div class='col-md-12 pip original' id="+files[i].name+"><div class='bg-filei'>"+files[i].name+"<span class='removed' id='remove_"+i+"'>X</span><input type='hidden' name='image_name[]' value='" +files[i].name+ "'></div></div>");  
   
                       $('#image_preview1').append("<div class='col-md-12 pip' id="+files[i].name+"><div class='bg-filei'>"+files[i].name+"<span class='removed' id='remove_"+i+"'>X</span><input type='hidden' name='image_name[]' value='" +files[i].name+ "'></div></div>");  
                   }else{
                       $('#image_preview').append("<div class='col-md-12 pip original' id="+files[i].name+"><div class='bg-filei'>"+files[i].name+"<span class='removed' id='remove_"+i+"'>X</span><input type='hidden' name='image_name[]' value='" +files[i].name+ "'></div></div>");  
                        $('#image_preview1').append("<div class='col-md-12 pip' id="+files[i].name+"><div class='bg-filei'>"+files[i].name+"<span class='removed' id='remove_"+i+"'>X</span><input type='hidden' name='image_name[]' value='" +files[i].name+ "'></div></div>");  
                   }
                }
               }else{
                $("#upload").attr('disabled','disabled');
               // alert('FileSize Is Too Large');
   
               $(".File-alert").show();
               }
              } 
              //
                   var accept = [];
                   $("#image_preview").find(".original .bg-filei").each(function() {    
                     var value=$(this).html().split('<span');
                     var removeItem =value[0];
                     accept.push(removeItem);   
                   });
                   console.log("selected file-"+accept.join());
                  $("#attch_image").val(accept.join());


          });
        }else {
          alert("Your browser doesn't support to File API")
        }

         $(document).on("click",".removed",function(){

                      console.log($(this).parents(".original").html());
                       var value=$(this).parent().html().split('<span');
                        console.log(value[0]);
                      //  console.log(value[1]);
                        var removeItem =value[0]; 

                         $(this).parents(".original").remove();
   
                         console.log($("#attch_image").val());
                          var result=$("#attch_image").val().split(',');

                          //console.log(result);
                          var result1 = result.filter(function(elem){

                          return elem != removeItem; 
                        });                       
   
                          $("#attch_image").val(result1.join());
                          console.log($("#attch_image").val());
                    }); 
      });    

</script> 
<?php $this->load->view('Invoice/package_scripts'); ?>
</body>
</html>