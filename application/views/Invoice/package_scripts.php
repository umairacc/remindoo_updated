<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/parsley.css') ?>">
<script src="<?php echo base_url('assets/js/parsley.min.js') ?>"></script>
<script type="text/javascript">

	$(document).ready(function()
	{ 
	    $(document).on('click','#invoice_save,#update_invoice_form,#email_invoice_form',function(event)
	    { 
	        var form_valid = $('#invoice_form').parsley();
	        var this_id = $(this).attr('id');

		    if(form_valid.isValid()) 
		    { 
		        event.preventDefault();
		        var to_id = $('select[name=clint_email]').val(); 
		        var to_email = $('select[name=clint_email] option[value='+to_id+']').html();
		        $('input[name=to_email]').val(to_email.trim());
		        var form_data = new FormData($("#invoice_form")[0]);    
		        var error = 0;

		        if(form_data.get('repeat_trans')!="" || form_data.get('duedate_no')!="")
		        {
		           if(form_data.get('duedate_no')=="")
		           {
		               $('#duedate_no_er').html('Required');
		               error = 1;
		           }
		           else
		           {
		               $('#duedate_no_er').html('');
		           }

		           if(form_data.get('repeat_trans')=="")
		           {
		               $('#repeat_trans_er').html('Required');
		               error = 1;
		           }
		           else
		           {
		               $('#repeat_trans_er').html('');
		           }
		        }
		        
		        if(error == 0)
		        { 
		            if(this_id == 'invoice_save')
		            {
		            	createInvoice(form_data);
		            }
		            else if(this_id == 'update_invoice_form')
	                { 
	                    updateInvoice(form_data, null);
	                }
	                else if(this_id == 'email_invoice_form')
	                { 
	                	form_data.append('email', 'email');
	                    updateInvoice(form_data, 'email');
	                }
		        }    
		   }	     
	  });
	
    });

	$(document).on('change','select[name="item_name[]"]',function()
	{
	    var package_id = $(this).val();
	    var id_val = $(this).attr('id');
	    var id = id_val.substr(9);
	    var category_id = $('select[name="category_id"]').val();
	     
	    getPackage(package_id,category_id,id_val,id);
	});

	$(document).on('change','select[name="category_id"]',function()
	{
        var category_id = $(this).val();

        $('select[name="item_name[]"]').each(function()
        {
        	var package_id = $(this).val();
        	var id_val = $(this).attr('id');
        	var id = id_val.substr(9);
            
            if($('#description'+id).val() == "")
            {          
        	   getPackage(package_id,category_id,id_val,id);
        	}   
        });

	});

	function getPackage(package_id,category_id,id_val,id)
	{
		if(package_id!="" && category_id!="")
		{              
		    $('#'+id_val+'_er').html(''); 
		    $('#category_id_er').html('');
		    $.ajax(
		    {
		       url:'<?php echo base_url().'invoice/getServicePackage'; ?>',
		       type:'POST',
		       data:{'service_name':package_id,'category_id':category_id},

		       beforeSend:function()
		       {
		         $('.LoadingImage').show();
		       },
		       success:function(response)
		       { 
		           if(response!=0)
		           {
		             var rec = JSON.parse(response);
		             $('#description'+id).val(rec.service_description);
		             $('#quantity'+id).val(rec.service_qty);
		             $('#unit_price'+id).val((rec.service_price/rec.service_qty));		 
		             $('.quantity').trigger('change');
		             $('.unit_price').trigger('change');            
		           }
		           else
		           {
		              $('#item_name'+id).after('<span id="'+id_val+'_er" style="color:red;">Selected Service Is Not Defined..</span>');
		              $('#description'+id).val('');
		              $('#quantity'+id).val('');
		              $('#unit_price'+id).val('');		
		              $('#amount_gbp'+id).val('');            
		           }		             
		           $('.LoadingImage').hide();
		       }
		    }) 
		}
		else
		{             
		   if(category_id == "" && !($('select[name="category_id"]').hasClass('parsley-error')))
		   {
		      <?php if(!isset($proposal_details)){ ?>
		      $('#category_id_er').html('Required');
		      <?php } ?>
		   }

		   if(package_id == "" && !($('#item_name'+id).hasClass('parsley-error')))
		   {
		      $('#item_name'+id).after('<span id="'+id_val+'_er" style="color:red;">Item'+id+' Is Required.</span>');
		   }   
		}       
	}

	function updateInvoice(form_data,email)
	{
		  $.ajax({
		    url : "<?php echo base_url(); ?>Invoice/UpdateInvoice",
		    type : 'POST',
		    data : form_data,
		    processData : false,
		    contentType : false,
		    beforeSend:function(){$('.LoadingImage').show();},
		    success:function(response)
		    {
		        $('.LoadingImage').hide(); 

		        if(response == 1) 
		        {
		          if(email)
		          {
		          $(".popup_info_box .position-alert1").html("Email Sent Successfully..");
		          }	
		          else
		          {
		          	$(".popup_info_box .position-alert1").html("Invoice Updated Successfully..");
		          }

		          $(".popup_info_box").show();
		        }
		        else
		        {
		          $(".popup_info_box .position-alert1").html("Process Failed.");
		          $(".popup_info_box").show();
		        }

		      setTimeout(function(){ window.location.href="<?php echo base_url()?>Invoice";},1000);
		    }
		});
	}

	function createInvoice(form_data)
	{
		$.ajax({
		  url : "<?php echo base_url()?>Invoice/CreateInvoice",
		  type : 'POST',
		  data : form_data,
		  processData : false,
		  contentType : false,
		  beforeSend:function(){
		    $('.LoadingImage').show();
		  },
		  success:function(response)
		  { 
		    $('.LoadingImage').hide();

		    if(response == 1) 
		    {
		      $(".popup_info_box .position-alert1").html("Invoice Created successfully..");
		      $(".popup_info_box").show();
		    }
		    else
		    {
		      $(".popup_info_box .position-alert1").html("Process Failed.");
		      $(".popup_info_box").show();
		    }
		    setTimeout(function(){ 
		      window.location.href="<?php echo base_url()?>Invoice";
		    },1000);
		  }
		});
	}

</script>