<style>
 /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   .payment-table td {
      padding: 10px;
  }
  .payment-table tr {
    border: 1px solid #ccc;
}
</style>
<?php echo html_entity_decode(firm_mail_header()); ?>
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <div style="width: 700px;margin: 0 auto;background: #fff;padding: 30px;box-shadow:0 1px 11px 0 rgba(0, 0, 0, 0.12);box-shadow:0 1px 11px 0 rgba(0, 0, 0, 0.12);">
               <table class="invoice-details generate-invoice" style="margin-top:30px;border: none;width:100%;">
                  <tr>
                    <td colspan="2">
                    <div class="online-payments" style="float: left;width: 100%;padding-bottom: 30px">
                      <div class="temp-edit" style="float: left;width: 50%;">
                        <input type="text" value="test" style="width: 100px;text-transform: capitalize;padding: 8px 15px;border: 1px solid #ccc;">
                      </div>
                      <div class="payment-table" style="float: right;width: 40%;">
                        <table style="border: 1px solid #e9ecef;width:100%;border-collapse: collapse;">
                          <tr>
                            <td colspan="2" style="padding: 10px;border-bottom: 1px solid #eee;">
                              <h3 style="font-size: 1.75rem;margin-bottom: 5px;margin-top: 0;"><i class="fa fa-gbp" aria-hidden="true"></i>150.00</h3>
                              <h5 style="font-size: 14px;margin: 0;">Amount due <span>(GBP)</span></h5>
                            </td>
                          </tr>
                          <tr>
                            <td style="padding: 10px;border-right: 1px solid #eee;    border-bottom: 1px solid #ddd;"><h3 class="upon-res" style="font-size: 18px;margin: 0;">Upon Reciept</h3>
                            <h6 style="font-size: 14px;margin: 10px 0;">Due</h6></td>
                            <td style="padding: 10px;    border-bottom: 1px solid #ddd;">
                              <span class="due-date12" style="font-size: 17px;padding-bottom: 5px;display: block;"><!-- <?php echo $invoice_no; ?> --></span>
                              <h5 style="font-size: 14px;    margin: 5px 0;" class="invoice-no">invoice no</h5>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" style="padding: 10px;">
                              <a href="javascript:;" style="display: block;background: #38b87c;color: #fff;padding: 10px;text-align: center;text-transform: uppercase;font-size: 15px;font-weight: 600;" class="onlinepayments">accept online payments</a>
                            </td>
                          <tr>
                        </table>
                      </div>
                    </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="generate-top">
                        <span class="project-name" style="float: left;width: 50%;font-size: 29px;text-transform: capitalize;">
                          invoice
                          <span style="display: block;">project name</span>
                        </span>
                        <span class="date-sec" style="float: left;width: 50%;text-align: right;line-height: 74px;">
                          <input style="width: 100px;padding-left: 10px;height: 30px;font-size: 14px;" type="text" class="g-date" value="<?php echo $invoice_generate; ?>"> 
                        </span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <td>
                    <div class="des-table" style="float: left;width: 100%;margin-top: 30px;">
                    <div class="table-responsive" style="isplay: inline-block;width: 100%;overflow-x: auto;">
                             <table style="width:100%;border-collapse: collapse;">
                                <tr>
                                   <th style="background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">description</th>
                                   <th style="background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">qty</th>
                                   <th style="background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">price</th>
                                   <th style="width:5%;background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">amount</th>
                                </tr>
                              <?php foreach ($products as $key => $value) { ?>    
                                <tr>
                              
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"><?php echo $value['description']; ?></td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"><?php echo $value['quantity']; ?></td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"><i class="fa fa-gbp"></i> <?php echo $value['unit_price']; ?></td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"> <i class="fa fa-gbp"></i> <?php echo $value['amount_gbp']; ?></td>
                                </tr>
                              <?php } ?>  
                                <!-- <tr>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;">accounts</td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;">1</td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;"><i class="fa fa-gbp"></i> 350.00</td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;"><i class="fa fa-gbp"></i> 350.00</td>
                                </tr> -->
                              </table>
                    </div>

                      <div class="sub-totaltab" style="width: 350px;float: right;margin-top: 20px;">
                        <table style="border: 1px solid #e9ecef;width:100%;">
                          <tr>
                          <th style="background: transparent;padding: 10px;border: none;text-align: right;font-size: 15px;">sub total</th>
                          <td style="border-right: none;background: transparent;border: none;text-align: right;padding: 10px;"><i class="fa fa-gbp">  <?php echo $amounts['sub_total']; ?></i></td>
                          </tr>
                          <tr>
                          <th tyle="background: transparent;padding: 10px;border: none;text-align: right;font-size: 15px;">total</th>
                          <td style="border-right: none;background: transparent;border: none;text-align: right;padding: 10px;"><i class="fa fa-gbp"> <?php echo $amounts['grand_total']; ?></i></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </td>
                  </tr>
                  <tr>
                    <td>
                    <div class="payment-ins" style="float: left;width: 100%;border-top: 1px solid #ccc;padding-top: 10px;margin-top: 30px;">
                      <span style="font-size: 15px;text-transform: capitalize;">payment instrctions</span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                    <div class="payment-ins" style="float: left;width: 100%;border-top: 1px solid #ccc;padding-top: 10px;margin-top: 30px;">
                      <span class="place" style="border: 1px solid #ccc;padding: 6px 10px;margin-top: 10px;display: inline-block;font-size: 15px;text-transform: capitalize;">united kingdom</span>
                    </div>
                    </td>
                  </tr>
              </table>
            </div>
         </div>
      </div>
   </div>
</div>
<?php echo html_entity_decode(firm_mail_footer()); ?>
<!-- ajax loader -->
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style>
<!-- ajax loader end-->
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
   <h1>Warning!!</h1>
   <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
   <div class="iew-container">
      <ul class="iew-download">
         <li>
            <a href="http://www.google.com/chrome/">
               <img src="assets/images/browser/chrome.png" alt="Chrome">
               <div>Chrome</div>
            </a>
         </li>
         <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
               <img src="assets/images/browser/firefox.png" alt="Firefox">
               <div>Firefox</div>
            </a>
         </li>
         <li>
            <a href="http://www.opera.com">
               <img src="assets/images/browser/opera.png" alt="Opera">
               <div>Opera</div>
            </a>
         </li>
         <li>
            <a href="https://www.apple.com/safari/">
               <img src="assets/images/browser/safari.png" alt="Safari">
               <div>Safari</div>
            </a>
         </li>
         <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
               <img src="assets/images/browser/ie.png" alt="">
               <div>IE (9 & above)</div>
            </a>
         </li>
      </ul>
   </div>
   <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- j-pro js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Bootstrap date-time-picker js -->
<!--     <script type="text/javascript" src="assets/pages/advance-elements/moment-with-locales.min.js"></script>
   <script type="text/javascript" src="bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
   <script type="text/javascript" src="assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
   <script type="text/javascript" src="bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
   <script type="text/javascript" src="bower_components/datedropper/js/datedropper.min.js"></script> -->
<!-- Custom js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<script>
   $( document ).ready(function() {
   
       var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
       // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#1abc9c',
               jackColor: '#fff',
               size: 'small'
           });
       });
   
       $('#accordion_close').on('click', function(){
               $('#accordion').slideToggle(300);
               $(this).toggleClass('accordion_down');
       });
   });
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   $("#staff_form").validate({
     
          ignore: false,
   
          /*onfocusout: function(element) {
           if ( !this.checkable(element)) {
               this.element(element);
           }
       },*/
      // errorClass: "error text-warning",
       //validClass: "success text-success",
       /*highlight: function (element, errorClass) {
           //alert('em');
          // $(element).fadeOut(function () {
              // $(element).fadeIn();
           //});
       },*/
                           rules: {
                           first_name: {required: true},
                           last_name: {required: true},
                           email_id:{required: true,email:true},
                           telephone_number : {required:true,minlength:10,
     maxlength:10,
     number: true},
     
                           password:{required: true},
                           /*image_upload: {required: false, accept: "jpg|jpeg|png|gif"},*/
                          image_upload: {
       required: function(element) {
   
       if ($("#image_name").val().length > 0) {
          return false;
       }
       else {
       return true;
       }
     },
     accept: "jpg|jpeg|png|gif"
   },
                           hourly_rate: {required: true,number:true},
                           roles:{required: true},
                           
                           },
                           errorElement: "span" , 
                           errorClass: "field-error",                             
                            messages: {
                             first_name: "Enter a first name",
                             last_name: "Enter a last name",
                             roles:"Select a role",
                             email_id: {
       email:"Please enter a valid email address", 
       required:"Please enter a email address",
   
   },
                            telephone_number:{  yourtel:"Enter a valid phone number.",
     required: "Enter a phone number."},
                             password: {required: "Please enter your password "},
                             image_upload: {required: 'Required!', accept: 'Not an image!'},
                             
                             hourly_rate: {required: "Please Enter Your rate",number:"Please enter numbers Only"},
                            },
                            
   
                           
                           submitHandler: function(form) {
                               var formData = new FormData($("#staff_form")[0]);
                              
   
                               $(".LoadingImage").show();
   
                               $.ajax({
                                   url: '<?php echo base_url();?>staff/add_staff/',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) {
                                       
                                       if(data == 1){
                                          
                                          // $('#new_user')[0].reset();
                                               location.reload(true);
                                           $('.alert-success').show();
                                           $('.alert-danger').hide();
   
                                       }
                                       else{
                                          // alert('failed');
                                           $('.alert-danger').show();
                                           $('.alert-success').hide();
                                       }
                                   $(".LoadingImage").hide();
                                   },
                                   error: function() { $('.alert-danger').show();
                                           $('.alert-success').hide();}
                               });
   
                               return false;
                           } ,
                            invalidHandler: function(e, validator) {
              if(validator.errorList.length)
           $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
   
           }
                            
                       });
   
   
   });
</script>
</body>
</html>