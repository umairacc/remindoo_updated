<?php
error_reporting(E_ERROR);
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
	public function __construct() {
		parent::__construct();

		$this->load->model(array('Common_mdl','Security_model','Invoice_model'));
		$this->load->helper(['comman']);
	}

	public function index()
	{ 
		$this->Security_model->chk_login();		
		
		if($_SESSION['permission']['Invoices']['view'] == '1')
		{
			$assigned_client = Get_Assigned_Datas('CLIENT');

			foreach ($assigned_client as $key => $value) 
			{
			    $rec = $this->Invoice_model->selectRecord('client','id', $value);
			    
			    if(!empty($rec['user_id']))
			    {
			      $arr[] = $rec['user_id'];
			    }
			}
			
			$clients_user_ids = implode(',', $arr);  

			$this->Invoice_model->UpdateExpiredInvoice($clients_user_ids);	  
			 
            $data['paid'] = $this->Invoice_model->selectInvoiceByType('amount_details','payment_status','approve','client_email', $clients_user_ids); //type 1
		    $data['unpaid'] = $this->Invoice_model->selectInvoiceByType('amount_details','payment_status','pending', 'client_email', $clients_user_ids); // type 2
		    $data['cancelled'] = $this->Invoice_model->selectInvoiceByType('amount_details','payment_status','decline', 'client_email', $clients_user_ids); //type 3
		    $data['invoice_all'] = $this->Invoice_model->selectInvoiceByType('','','','client_email', $clients_user_ids);			
            $data['repeatinvoice_details'] = $this->Invoice_model->getRepeatInvoice('client_email',$clients_user_ids);	

			$Assigned_Leads = Get_Assigned_Datas('LEADS');
			$assigned_leads = implode(',', $Assigned_Leads);

		    $data['draft_details'] = $this->db->query('SELECT * from invoices where status = "draft" and FIND_IN_SET(lead_id,"'.$assigned_leads.'")')->result_array();
		    $data['firm_currency'] = $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'crm_currency');
			$data['clients_user_ids']  = $clients_user_ids;
			
			$this->load->view('Invoice/HomeInvoice_view', $data);	
			$this->session->unset_userdata('invoice_type');		   
		}
		else
		{
			$this->load->view('users/blank_page');
		}		
	}

	public function ProposalInvoice($prpopsal_id)
	{
		$this->Security_model->chk_login();			

		if($_SESSION['permission']['Invoices']['edit'] == '1')
		{
			if($prpopsal_id!='')
			{
				$value = explode( "---", $prpopsal_id );            
				$values = end($value); 			
				$data['proposal_details'] = $this->Common_mdl->select_record('invoices','proposal_id',$values);
			}
			
			$assigned_client = Get_Assigned_Datas('CLIENT');

			foreach ($assigned_client as $key => $value) 
			{
			    $rec = $this->Invoice_model->selectRecord('client','id', $value);
			    
			    if(!empty($rec['user_id']))
			    {
			      $arr[] = $rec['user_id'];
			    }
			}
			
			$clients_user_ids = implode(',', $arr);

			$data['clients_user_ids'] = $clients_user_ids;
			$data['services'] = $this->Common_mdl->getServicelist();
			$data['category'] = $this->Common_mdl->GetAllWithWheretwo('proposal_category','firm_id',$_SESSION['firm_id'],'category_type','service');
			$data['admin_settings']=$this->Common_mdl->select_record('admin_setting','firm_id',$_SESSION['firm_id']);
			$data['referby'] = $this->db->query("SELECT * FROM `client` where crm_first_name!='' and crm_first_name!='0' GROUP BY `crm_first_name`")->result_array();

			$this->load->view('Invoice/NewInvoice_view',$data);	
		}
		else
		{
			$this->load->view('users/blank_page');
		}		
	}


	public function NewInvoice()
	{	
		$this->Security_model->chk_login();	

		if($_SESSION['permission']['Invoices']['create'] == '1')
		{
			$assigned_client = Get_Assigned_Datas('CLIENT');

			foreach ($assigned_client as $key => $value) 
			{
			    $rec = $this->Invoice_model->selectRecord('client','id', $value);
			    
			    if(!empty($rec['user_id']))
			    {
			      $arr[] = $rec['user_id'];
			    }
			}
			
			$clients_user_ids = implode(',', $arr);
			$data['clients_user_ids'] = $clients_user_ids;
            $data['services'] = $this->Common_mdl->getServicelist();	
            $data['category'] = $this->Common_mdl->GetAllWithWheretwo('proposal_category','firm_id',$_SESSION['firm_id'],'category_type','service');			
			$data['admin_settings']=$this->Common_mdl->select_record('admin_setting','firm_id',$_SESSION['firm_id']);

			$this->load->view('Invoice/NewInvoice_view',$data);	
		}
		else
		{
			$this->load->view('users/blank_page');
		}
	}

	public function CreateInvoice()
	{
		if(count($_POST)>0)
		{
			$cli_id = $_POST['clint_email'];
			$cli_rec = $this->Invoice_model->selectRecord('user', 'id', $cli_id);

			if(isset($_POST['proposal_id']))
			{
				$data=array('proposal_id'=>$_POST['proposal_id'],'invoice_no'=>$_POST['invoice_no'],'created_at'=>date('Y-m-d h:i:s'));
	            $insert=$this->Common_mdl->insert('invoice_proposals',$data);

				$data=array('proposal_id'=>$_POST['proposal_id'],'tag'=>'invoice','created_at'=>date('Y-m-d h:i:s'));
				$inse=$this->Common_mdl->insert('proposal_history',$data); 

				$data=array('status'=>'sent');
				$inse=$this->Common_mdl->update('invoices',$data,'id',$_POST['draft_id']); 

				$data=array('invoice_id'=>$_POST['invoice_no']);
				$insert=$this->Common_mdl->update('proposals',$data,'id',$_POST['proposal_id']);
			}

			if(!empty($_FILES['userFiles']['name']))
			{

			  $filesCount = count($_FILES['userFiles']['name']);

	            for($i = 0; $i < $filesCount; $i++){
	                $_FILES['uplode_userFile']['name'] = $_FILES['userFiles']['name'][$i];
	                $_FILES['uplode_userFile']['type'] = $_FILES['userFiles']['type'][$i];
	                $_FILES['uplode_userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
	                $_FILES['uplode_userFile']['error'] = $_FILES['userFiles']['error'][$i];
	                $_FILES['uplode_userFile']['size'] = $_FILES['userFiles']['size'][$i];
	                $uploadPath = 'attachment/';
	                $config['upload_path'] = $uploadPath;
	                $config['allowed_types'] = 'gif|jpg|png|pdf';                
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);
	                if($this->upload->do_upload('uplode_userFile')){
	                    $fileData = $this->upload->data();
	                    $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
	                }
	            }
	        	if(count($uploadData)){
					$images=implode(',',array_column($uploadData,'file_name'));
				}else
				{
					$images='';
				}
		    }

			$ima_ges=explode(',',$_POST['attch_image']);
			$attachhh=array();
			$repeat_trans = $_POST['repeat_trans'];
			

			for($i=0;$i<count($ima_ges);$i++)
			{
				$imgExts = array("gif", "jpg", "jpeg", "png", "tiff", "tif");
				$urls = $ima_ges[$i];
				$urlExt = pathinfo($urls, PATHINFO_EXTENSION);

				$url1=pathinfo($urls)['filename'];
				$strlower = preg_replace("~[\\\\/:*?'&,<>|]~", '', $url1);
				$lesg = str_replace(' ', '_', $strlower);			
				$lesgs = str_replace('.', '_', $lesg);

				//  echo pathinfo($url1)['extension']; // "ext"

				$ext = pathinfo($urls, PATHINFO_EXTENSION);

				// echo   $lesgs=pathinfo($url1)['filename']; // "file"
				$att_image=$lesgs.'.'.$ext;     

				if($att_image!='.')
				{
				   array_push($attachhh,$att_image);
				}			
			}

			$attaching_image=implode(',',$attachhh);

			// echo "<pre>";
			// print_r($cli_rec['crm_email_id']); exit();
			$invoiceDate = strtotime($_POST['invoice_date']);
			$due_date = strtotime($_POST['due_date']);
			$invoice_no = $_POST['invoice_no'];
			//$reference = $_POST['reference'];
			$amt_det = $_POST['amounts_details'];
			if($amt_det == 1)
			{
				$amounts_type = 'Tax Exclusive';
				$tax_rate = $_POST['tax_rate'];
				$tax_amount = $_POST['tax_amount'];
			} else {
				$amounts_type = 'Tax Inclusive';
				$tax_rate = '';
				$tax_amount = '';
			}

			$data = array(
				//'client_email' => $cli_rec['crm_email_id'],
							'client_email' => $cli_id,
							'invoice_date' => $invoiceDate,
							'invoice_duedate' => $due_date,
							'invoice_no' => $invoice_no,
							//'reference' => $reference,
							'amounts_type' => $amounts_type,
							'status' => 'Active', 
							'created_at' => time(),
							'repeats' => $repeat_trans,
							'repeat_period' => $_POST['select_transaction'],
							'days_after_invoice' => $_POST['duedate_no'],
						);

			if(isset($_POST['proposal_id'])){

				$data['proposal_id']=$_POST['proposal_id'];
			}

			// print_r($data); exit();
			$ins_id = $this->Invoice_model->insert('Invoice_details', $data);
			//print_r($ins_id); exit();
			if(isset($ins_id))
			{	
				$clientId = $ins_id;
			}

			$item_name = $_POST['item_name'];
			$description = $_POST['description'];
			$quantity = $_POST['quantity'];
			$unit_price = $_POST['unit_price'];
			$discount = $_POST['discount'];
			$account = $_POST['account'];
			$amount_gbp = $_POST['amount_gbp'];
			
			foreach ($item_name as $key => $value) {
				// echo "<pre>";
				$itmname = $item_name[$key];  
				$descr = $description[$key];
				$qty = $quantity[$key];
				$uprice = $unit_price[$key];
				$dis = $discount[$key];
				$acc = $account[$key];
				$taxrate = $tax_rate[$key];
				$taxamount = $tax_amount[$key];
				$amt_gbp = $amount_gbp[$key];

			$data1 = array('client_id' => $clientId,
							'item' => $itmname,
							'description' => $descr,
							'quantity' => $qty,
							'unit_price' => $uprice,
							'discount' => $dis,
							'account' => $acc,
							'tax_rate' => $taxrate,
							'tax_amount' => $taxamount,
							'amount_gbp' => $amt_gbp
							
				);
			
			$sql = $this->Invoice_model->insert('products_details', $data1);
			
			}

			$sub_total = $_POST['sub_tot'];
			$vat = $_POST['value_at_tax'];
			$adjust_tax = $_POST['adjust_tax'];
			$grand_total = $_POST['grand_total'];
			$transaction_payment = $_POST['transaction_payment'];
			/*if($transaction_payment == 'approve')
			{
				$payment_status = 'approve';
			} else {
				$payment_status = 'decline';
			}*/

			$data2 = array('client_id' => $clientId,
							'sub_total' => $sub_total,
							'VAT' => $vat,
							'adjust_to_tax' => $adjust_tax,
							'grand_total' => $grand_total, 
							'payment_status' => $transaction_payment
						);


			$result = $this->Invoice_model->insert('amount_details', $data2);

			if(!empty($repeat_trans))
			{     
               $result = $this->Create_Repeat_Invoice($_POST,$tax_rate,$tax_amount,$clientId);    
			}			
			
			if(isset($result))
			{
	            $from = $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'company_email');

	            if(empty($from))
	            {
	            	$from = 'admin@crm.com';
	            }

	            $content['body'] = $this->load->view('Invoice/invoice_remainder_email.php', $_POST, TRUE);			

				if(!empty($attaching_image)){	
					
					$attaching_image=explode(',',$attaching_image);		

					for($i=0;$i<count($attaching_image);$i++) {
						//echo $attaching_image[$i];
						$content['attachment'][] = FCPATH.'attachment/'.$attaching_image[$i];												
					}
				}

				$Issend = firm_settings_send_mail( $_SESSION['firm_id'] , trim($_POST['to_email']) ,'Invoice' , $content );

				if( $Issend['result'] )
				{
                    $send_data = array('send_statements' => 'send','updated_at' => date('Y-m-d H:i:s'));

                    if($this->Invoice_model->update('Invoice_details', 'client_id', $clientId, $send_data))
                    {
                      echo 1;
                    }
				}           
			}				
		}
		else
		{
			$this->load->view('users/blank_page');
		}
	}
    
    public function Create_Repeat_Invoice($post,$tax_rate,$tax_amount,$invoice_det_id,$repeat_invoice_id=false)
    {  
        $repeat_set = $post['select_transaction'];
        $invoice_enddate = $post['invoice_enddate'];

        if(!empty($invoice_enddate))
        {
            $end_date = strtotime($invoice_enddate);
            $endDate = date('Y-m-d', $end_date);
        }
        else
        {
        	$endDate = "";
        }

        $repeat_days = $post['repeat_days'];
        $duedate_no = $post['duedate_no'];
        $repeat_trans = $post['repeat_trans'];
        $invoiceDate = $post['invoice_date'];

        if($post['amounts_details'] == 1)
        {
        	$amounts_type = 'Tax Exclusive';        	
        } 
        else 
        {
        	$amounts_type = 'Tax Inclusive';        	
        }

        $data = array('save_as_draft' => '',
					'approve' => $post['transaction_payment'],
					'approve_send' => '2',
					'client_email' => $post['clint_email'],
					'reference' => $post['invoice_no'],
					'amounts_type' => $amounts_type,	
					'status' => 'Active',
					'created_at' => time(),
					'send_statements' =>''
    			); 

        if(empty($repeat_invoice_id))
        {	        
	        $ins_id = $this->Invoice_model->insert('repeatInvoice_details', $data);
	       
			if(isset($ins_id))
			{	
				$clientId = $ins_id;
				$update = array('repeat_invoice_id' => $clientId);
				$this->Invoice_model->update('Invoice_details', 'client_id', $invoice_det_id, $update);
			} 	
        }
        else
        {
        	$clientId = $repeat_invoice_id;
        	$this->Invoice_model->update('repeatInvoice_details','client_id',$repeat_invoice_id,$data);	
        }

        if(!empty($repeat_invoice_id))
        {
            $this->Invoice_model->delete('repeatInvoiceDate_details', 'client_id', $clientId);
        }
 
        if($repeat_set == '0')
        { 
        	$trans_every = 'Week';
        	$week = $repeat_trans;
        	$start_date = strtotime($invoiceDate);
        	
        	for ($i=0; $i < $week; $i++) 
        	{
        		$strtime = strtotime('+'.$i.' weeks', $start_date);				
        		$newDate = date('Y-m-d', $strtime);
        		$last_date = date('Y-m-t', $strtime);
        		$newDate1 = strtotime($newDate);
        		
        		if($repeat_days == 1)
        		{	
        			$countdate = $duedate_no;
        			$repeat_every = 'day(s) after the invoice date';
        			$in_date = strtotime($invoiceDate);						
        			$strtotime = strtotime('+'.$countdate.' day', strtotime($newDate));
        			$repeat_due = date('Y-m-d', $strtotime);
        		}  

        		$data1 = array('client_id' => $clientId,
        					'repeats' => $repeat_trans,
        					'transaction_every' => $trans_every,
        					'invoice_startdate' => $newDate,
        					'invoice_enddate' => $last_date,
        					'due_days' => $countdate,
        					'count_duedays' => $repeat_due,	
        					'payment_due' => $repeat_every,
        					'end_date' => $endDate);   
 
        	    $this->Invoice_model->insert('repeatInvoiceDate_details', $data1);     			
        	}		
        } 		

        if($repeat_set == '1')
        {
        	$trans_every = 'Month';
        	$month = $repeat_trans;
        	$start_date = strtotime($invoiceDate);

        	for($i=0; $i < $month; $i++)
        	{ 
        		$strtime = strtotime('+'.$i.' month', $start_date );				
        		$newDate = date('Y-m-d', $strtime);
        		$last_date = date('Y-m-t', $strtime);
        		$newDate1 = strtotime($newDate);
        	
        		if($repeat_days == 1)
        		{	
        			$countdate = $duedate_no;
        			$repeat_every = 'day(s) after the invoice date';
        			$in_date = strtotime($invoiceDate);						
        			$strtotime = strtotime('+'.$countdate.' day', strtotime( $newDate ));
        			$repeat_due = date('Y-m-d', $strtotime);
        		}  

        		$data1 = array('client_id' => $clientId,
        					'repeats' => $repeat_trans,
        					'transaction_every' => $trans_every,
        					'invoice_startdate' => $newDate,
        					'invoice_enddate' => $last_date,
        					'due_days' => $countdate,
        					'count_duedays' => $repeat_due,	
        					'payment_due' => $repeat_every,
        					'end_date' => $endDate,
        					
        			);  

        	   if(empty($repeat_invoice_id))
        	   {
        	      $this->Invoice_model->insert('repeatInvoiceDate_details', $data1);	
        	   }
        	   else
        	   {
        	   	  $this->Invoice_model->insertRecords('repeatInvoiceDate_details', 'client_id', $repeat_invoice_id, $data1);
        	   }		     		
           }		
        }  
                    
        if($repeat_set == '2')
        {
        	$trans_every = 'Year';
        	$year = $repeat_trans;
        	$start_date = strtotime($invoiceDate);
        	
        	for ($i=0; $i < $year; $i++) 
        	{ 
        		$strtime = strtotime('+'.$i.' year', $start_date);				
        		$newDate = date('Y-m-d', $strtime);
        		$last_date = date('Y-m-t', $strtime);
        		$newDate1 = strtotime($newDate);
        		
        		if($repeat_days == 1)
        		{	
        			$countdate = $duedate_no;
        			$repeat_every = 'day(s) after the invoice date';
        			$in_date = strtotime($invoiceDate);						
        			$strtotime = strtotime('+'.$countdate.' day', strtotime( $newDate ));
        			$repeat_due = date('Y-m-d', $strtotime);
        		}  
        			
        		$data1 = array('client_id' => $clientId,
        					'repeats' => $repeat_trans,
        					'transaction_every' => $trans_every,
        					'invoice_startdate' => $newDate,
        					'invoice_enddate' => $last_date,
        					'due_days' => $countdate,
        					'count_duedays' => $repeat_due,	
        					'payment_due' => $repeat_every,
        					'end_date' => $endDate	
        			);

        		if(empty($repeat_invoice_id))
        		{
        		   $this->Invoice_model->insert('repeatInvoiceDate_details', $data1);	
        		}
        		else
        		{
        		   $this->Invoice_model->insertRecords('repeatInvoiceDate_details', 'client_id', $repeat_invoice_id, $data1);
        		}	
        	}
        }               
        
        
		$this->Invoice_model->delete('repeatproducts_details', 'client_id', $repeat_invoice_id);       
      
        $products_details = $this->Common_mdl->GetAllWithWhere('products_details','client_id', $invoice_det_id);             
        
        foreach ($products_details as $key => $value) 
        {	        
        	$item_name[] = $value['item'];
        	$description[] = $value['description'];
        	$quantity[] = $value['quantity'];
        	$unit_price[] = $value['unit_price'];
        	$discount[] = $value['discount'];
        	$account[] = $value['account'];
        	$amount_gbp[] = $value['amount_gbp'];
        }    


        foreach ($item_name as $key => $value) 
        {	
        	$itmname = $item_name[$key];  
        	$descr = $description[$key];
        	$qty = $quantity[$key];
        	$uprice = $unit_price[$key];
        	$dis = $discount[$key];
        	$acc = $account[$key];
        	$taxrate = $tax_rate[$key];
        	$taxamount = $tax_amount[$key];
        	$amt_gbp = $amount_gbp[$key];

            $data2 = array('client_id' => $clientId,
        				'item' => $itmname,
        				'description' => $descr,
        				'quantity' => $qty,
        				'unit_price' => $uprice,
        				'discount' => $dis,
        				'account' => $acc,
        				'tax_rate' => $taxrate,
        				'tax_amount' => $taxamount,
        				'amount_gbp' => $amt_gbp				
        	);
        	
            $sql = $this->Invoice_model->insert('repeatproducts_details', $data2);
        } 

        $sub_total = $post['sub_tot'];
        $vat = $post['value_at_tax'];
        $adjust_tax = $post['adjust_tax'];
        $grand_total = $post['grand_total'];

        $data3 = array('client_id' => $clientId,
        				'sub_total' => $sub_total,
        				'VAT' => $vat,
        				'adjust_to_tax' => $adjust_tax,
        				'grand_total' => $grand_total
        			);

        if(empty($repeat_invoice_id))
        {
           $result = $this->Invoice_model->insert('repeatamount_details', $data3);
        }
        else
        {
           $result = $this->Invoice_model->update('repeatamount_details','client_id', $repeat_invoice_id,$data3);
        }

        return $result;
    }

	public function previewInvoice()
	{
		$invoice_date=isset($_POST['invoice_date'])?$_POST['invoice_date']:date('d-m-Y');
		$invoice_no=isset($_POST['invoice_no'])?$_POST['invoice_no']:"";
		$data = array('description' => $_POST['description'],
						'quantity' => $_POST['quantity'],
						'unit_price' => $_POST['unit_price'],
						'amount_gbp' => $_POST['amount_gbp'],
						'sub_total' => $_POST['sub_tot'],
						'grand_total' => $_POST['grand_total'],
						'invoice_date' => strtotime($invoice_date),
						'invoice_no' => $invoice_no,
				);
     
	    $this->load->view('Invoice/invoice_foreach_view', $data);

		  // print_r($data);
		// echo json_encode($_POST);
	}

	public function importFile()
	{
		$csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
		if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes))
		{
			if(is_uploaded_file($_FILES['file']['tmp_name']))
			{
				$csvFile = fopen($_FILES['file']['tmp_name'], 'r');
	            fgetcsv($csvFile);
	            
	            while(($line = fgetcsv($csvFile)) !== FALSE) {
	            	
	        $res = $this->db->get_where("Invoice_details", array("client_id"=>$line[0]))->result();

			if(count($res) > 0) 
			{
				$this->db->update("Invoice_details", array("client_email"=>$line[1], "invoice_date"=>$line[2], "invoice_duedate"=>$line[3], "invoice_no"=>$line[4], "reference"=>$line[5], "amounts_type"=>$line[6] , "status"=>$line[7], "created_at"=>$line[8]), array("client_id"=>$line[0]));

	        } else {
	        	$this->db->insert("Invoice_details", array("client_id"=>$line[0], "client_email"=>$line[1], "invoice_date"=>$line[2], "invoice_duedate"=>$line[3], "invoice_no"=>$line[4], "reference"=>$line[5], "amounts_type"=>$line[6] , "status"=>$line[7], "created_at"=>$line[8]));
	        }

			
			$result = $this->db->get_where("products_details", array("client_id"=>$line[0]))->result();

			if(count($result) > 0)
			{
				$this->db->update("products_details", array("item"=>$line[9], "description"=>$line[10], "quantity"=>$line[11], "unit_price"=>$line[12], "discount"=>$line[13], "account"=>$line[14], "tax_rate"=>$line[15], "tax_amount"=>$line[16], "amount_gbp"=>$line[17]), array("client_id"=>$line[0]));
			} else {
				$this->db->insert("products_details", array("client_id"=>$line[0], "item"=>$line[9], "description"=>$line[10], "quantity"=>$line[11], "unit_price"=>$line[12], "discount"=>$line[13], "account"=>$line[14], "tax_rate"=>$line[15], "tax_amount"=>$line[16], "amount_gbp"=>$line[17]));
				}

			$result1 = $this->db->get_where("amount_details", array("client_id"=>$line[0]))->result();
			if(count($result1) > 0)
			{
				$this->db->update("amount_details", array("sub_total"=>$line[18], "VAT"=>$line[19], "adjust_to_tax"=>$line[20], "grand_total"=>$line[21], "payment_status"=>$line[22]), array("client_id"=>$line[0]));
			} else {
				$this->db->insert("amount_details", array("client_id"=>$line[0], "sub_total"=>$line[18], "VAT"=>$line[19], "adjust_to_tax"=>$line[20], "grand_total"=>$line[21], "payment_status"=>$line[22]));
			}	


			}
				fclose($csvFile);

				$this->session->set_flashdata('success_import', '<div class="modal-alertsuccess alert alert-success">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <div class="pop-realted1">
                        <div class="position-alert1">
                        <strong>Success!</strong> File Imported Successfully.</div>
                        </div></div>');	
			} else  {
				$this->session->set_flashdata('error_import', '<div class="modal-alertsuccess alert alert-success">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <div class="pop-realted1">
                        <div class="position-alert1">
                        <strong></strong> Pls! try again.</div>
                        </div></div>');	
			}
		}	

		else {
			$this->session->set_flashdata('invalid_import', '<div class="modal-alertsuccess alert alert-success">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <div class="pop-realted1">
                        <div class="position-alert1">
                        <strong>Error!</strong> Invalid File.</div>
                        </div></div>');			
		}

		redirect('Invoice');
	}

	public function EditInvoice($id)
	{	
		if($_SESSION['permission']['Invoices']['edit'] == '1')
		{
			$data['invoice_details'] = $this->Invoice_model->selectRecord('Invoice_details', 'client_id', $id);
			$data['product_details'] = $this->Invoice_model->selectAllRecord('products_details', 'client_id', $id);
			$data['amount_details'] = $this->Invoice_model->selectRecord('amount_details', 'client_id', $id);
			$data['admin_settings']=$this->Common_mdl->select_record('admin_setting','firm_id',$_SESSION['firm_id']);
		    $data['services'] = $this->Common_mdl->getServicelist();
		    $data['category'] = $this->Common_mdl->GetAllWithWheretwo('proposal_category','firm_id',$_SESSION['firm_id'],'category_type','service');
        
		    $this->load->view('Invoice/edit_NewInvoice_view', $data);
		}
		else
		{
			$this->load->view('users/blank_page');
		}
	}

	public function UpdateInvoice()
	{ 
		if(count($_POST)>0)
        {			
			$invoice_ID = $_POST['invoiceID'];
			//$product_id = $_POST['productID']; 	 
			$client_id = $_POST['clientID'];
			$c_name = $_POST['clint_email'];
			$invoiceDate = strtotime($_POST['invoice_date']);
			$due_date = strtotime($_POST['due_date']);
			$invoice_no = $_POST['invoice_no'];
			$reference = $_POST['reference'];
			$amt_det = $_POST['amounts_details'];
			
			if($amt_det == 1)
	        {
	            $amounts_type = 'Tax Exclusive';
	            $tax_rate = $_POST['tax_rate'];
	            $tax_amount = $_POST['tax_amount'];
	        } else {
	            $amounts_type = 'Tax Inclusive';
	            $tax_rate = ' ';
	            $tax_amount = ' ';
	        }
			$data = array('client_email' => $c_name,
	                        'invoice_date' => $invoiceDate,
	                        'invoice_duedate' => $due_date,
	                        'invoice_no' => $invoice_no,
	                        'reference' => $reference,
	                        'amounts_type' => $amounts_type,
	                        'status' => 'Active', 
	                        'updated_at' => time()
	                    );

			$update_id = $this->Invoice_model->update('Invoice_details', 'client_id', $client_id, $data);
	      

	        //$primary_id = $_POST['primaryID'];

	        // echo "<pre>";
	        // print_r($cnt_primary_id);
	        // print_r($invoice_ID); exit();
	        // print_r($client_id); exit();
	        // for ($i=0; $i <$primary_id ; $i++) { 

	        // }

	        $Invoice_details_table_data = $this->Common_mdl->select_record('Invoice_details','client_id',$client_id);

	        if(empty($Invoice_details_table_data['send_statements']))
	        {
	           $delete_id = $this->Invoice_model->delete('products_details', 'client_id', $invoice_ID);	
	        }
	        //$primary_id = $_POST['primaryID'];
	        $item_name = $_POST['item_name'];
	        $description = $_POST['description'];
	        $quantity = $_POST['quantity'];
	        $unit_price = $_POST['unit_price'];
	        $discount = $_POST['discount'];
	        $account = $_POST['account'];
	        $amount_gbp = $_POST['amount_gbp'];

	        foreach ($item_name as $key => $value) {
	            // echo "<pre>";
	            // $id = $primary_id[$key];
	            $itmname = $item_name[$key];  
	            $descr = $description[$key];
	            $qty = $quantity[$key];
	            $uprice = $unit_price[$key];
	            $dis = $discount[$key];
	            $acc = $account[$key];
	           if($amt_det == 1)
	           { 
	            $taxrate = $tax_rate[$key];
	            $taxamount = $tax_amount[$key];
	           } else {
	           	$taxrate = '';
	            $taxamount = '';
	           }
	            $amt_gbp = $amount_gbp[$key];

	        $data1 = array(
	        				'client_id' => $client_id,
	                        'item' => $itmname,
	                        'description' => $descr,
	                        'quantity' => $qty,
	                        'unit_price' => $uprice,
	                        'discount' => $dis,
	                        'account' => $acc,
	                        'tax_rate' => $taxrate,
	                        'tax_amount' => $taxamount,
	                        'amount_gbp' => $amt_gbp
	                        
	            );
	        //  echo "<pre>";
	        // print_r($data1); 
	        // $sql = $this->Invoice_model->updateRecords('products_details', 'id', $id, 'client_id', $client_id, $data1);
	        $sql = $this->Invoice_model->insertRecords('products_details', 'client_id', $invoice_ID, $data1);
	        
	        }
	        
	        $sub_total = $_POST['sub_tot'];
	        $vat = $_POST['value_at_tax'];
	        $adjust_tax = $_POST['adjust_tax'];
	        $grand_total = $_POST['grand_total'];
	        $transaction_payment = $_POST['transaction_payment'];
	        $repeat_trans = $_POST['repeat_trans'];
	      /*  if($transaction_payment == 'approve')
	        {
	            $payment_status = 'approve';
	        } else {
	            $payment_status = 'decline';
	        }*/

	        $data2 = array('client_id' => $client_id,
	                        'sub_total' => $sub_total,
	                        'VAT' => $vat,
	                        'adjust_to_tax' => $adjust_tax,
	                        'grand_total' => $grand_total, 
	                        'payment_status' => $transaction_payment
	                    );

	        
	        $result = $this->Invoice_model->update('amount_details', 'client_id', $client_id, $data2);
	        
	        if(!empty($repeat_trans))
			{    
               $result = $this->Create_Repeat_Invoice($_POST,$tax_rate,$tax_amount,$client_id,$Invoice_details_table_data['repeat_invoice_id']);    
			}

	        if(isset($result))
	        {          
                $from = $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'company_email');

                if(empty($from))
                {
                	$from = 'admin@crm.com';
                }
	            
	            $products_details = $this->Common_mdl->GetAllWithWhere('products_details','client_id', $client_id);
                unset($_POST['description']);
                unset($_POST['quantity']);
                unset($_POST['amount_gbp']);                
	            
	            foreach ($products_details as $key => $value) 
	            {
	            	$_POST['description'][] = $value['description'];
	            	$_POST['quantity'][] = $value['quantity'];
	            	$_POST['amount_gbp'][] = $value['amount_gbp'];
	            }

	      	    $body = $this->load->view('Invoice/invoice_remainder_email.php', $_POST, TRUE);

	      	    $Issend = firm_settings_send_mail( $_SESSION['firm_id'] , trim($_POST['to_email']) ,'Invoice' , $body );
				
				if( $Issend['result'] )
				{
                    $send_data = array('send_statements' => 'send','updated_at' => date('Y-m-d H:i:s'), 'views'  => '0');
                   
                    if($this->Invoice_model->update('Invoice_details', 'client_id', $client_id, $send_data))
                    {    
                      echo 1;
                    }
				}	
	            
	        }				
		}
		else
		{
			$this->load->view('users/blank_page');
		}
    }
	public function DeleteInvoice($id)
	{ 
		if($_SESSION['permission']['Invoices']['delete'] == '1')
		{
			$this->Invoice_model->delete('Invoice_details', 'client_id', $id);
			$this->Invoice_model->delete('products_details', 'client_id', $id);
			$sql = $this->Invoice_model->delete('amount_details', 'client_id', $id);
			if(isset($sql))
			{
				redirect("Invoice");
			}
	    }
	    else
	    {
	    	$this->load->view('users/blank_page');
	    }		
	}


	public function DeleteDraft()
	{	
	    if($_SESSION['permission']['Invoices']['delete'] == '1')
	    {
			$sql = $this->Invoice_model->delete('invoices', 'id', $id);
			if(isset($sql))
			{
				redirect("Invoice");
			}
	    }
	    else
	    {
	    	$this->load->view('users/blank_page');
	    }		
	}	



	public function SendInfo()
	{
		$id = $_POST['clientid'];
		$data['c_email'] = $_POST['clientemail'];
		
		$clientID_repeat = $_POST['repeatclientid'];
		$clientEmail_repeat = $_POST['repeatclientemail'];
		if($id)
		{
			$data['in_no'] = $_POST['invoiceno'];
			$data['in_date'] = $_POST['invoicedate'];
			$data['in_details'] = $this->db->query('UPDATE Invoice_details set send_statements="send" WHERE client_id='.$id.'');
			$data['pro_details'] = $this->db->query('select * from products_details where client_id='.$id.'')->result_array();
        	$data['amt_details'] = $this->db->query('select * from amount_details where client_id='.$id.'')->row_array(); 

			//$this->load->view('Invoice/send_statements_remainder_email', $data);
			$body = $this->load->view('Invoice/send_statements_remainder_email.php', $data, TRUE);

			firm_settings_send_mail( $_SESSION['firm_id'] , $_POST['clientemail'] ,'Invoice' , $body );
			
	        $this->session->set_flashdata('invoice_send_success', '<div class="modal-alertsuccess alert alert-success">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <div class="pop-realted1">
                        <div class="position-alert1">
                        <strong>Success!</strong> Your Order Successfully Updated.</div>
                        </div></div>');
            redirect('Invoice');		
		} else {
			$this->session->set_flashdata('invoice_error_msg', '<div class="modal-alertsuccess alert alert-success">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <div class="pop-realted1">
                        <div class="position-alert1">
                        <strong>Please!...</strong> Select Record.</div>
                        </div></div>');
            redirect('Invoice');
		}
		if($clientID_repeat)
		{
			$data['rep_invoiceDetails'] = $this->db->query('UPDATE repeatInvoice_details set send_statements="send" WHERE client_id='.$clientID_repeat.'');
			$data['rep_invoicedateDetails'] = $this->db->query('select * from repeatInvoiceDate_details where client_id='.$clientID_repeat.'')->row_array();
	        $data['rep_productDetails'] = $this->db->query('select * from repeatproducts_details where client_id='.$clientID_repeat.'')->result_array();
	        $data['rep_amountDetails'] = $this->db->query('select * from repeatamount_details where client_id='.$clientID_repeat.'')->row_array();

	        $body = $this->load->view('Invoice/send_statements_remainder_email.php', $data, TRUE);

	        firm_settings_send_mail( $_SESSION['firm_id'] , $_POST['repeatclientemail'] ,'Invoice' , $body );

			$this->session->set_flashdata('invoice_send_success', '<div class="modal-alertsuccess alert alert-success">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <div class="pop-realted1">
                        <div class="position-alert1">
                        <strong>Success!</strong> Your Order Successfully Updated.</div>
                        </div></div>');
            redirect('Invoice');
		} else {
			$this->session->set_flashdata('repinvoice_error_msg', '<div class="modal-alertsuccess alert alert-success">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <div class="pop-realted1">
                        <div class="position-alert1">
                        <strong>Please!...</strong> Select Record.</div>
                        </div></div>');
            redirect('Invoice');
		}

	}

	public function sendInvoiceBill($id)
	{
		$data['invoice_info'] = $this->db->query('select * from Invoice_details where client_id='.$id.'')->row_array();
        
        $data['product_info'] = $this->db->query('select * from products_details where client_id='.$id.'')->result_array();
        $data['amount_info'] = $this->db->query('select * from amount_details where client_id='.$id.'')->row_array(); 

        $data['repeatinvoicedate_info'] = $this->db->query('select * from repeatInvoiceDate_details where client_id='.$id.'')->row_array();
        $data['repeatproduct_info'] = $this->db->query('select * from repeatproducts_details where client_id='.$id.'')->result_array();

        $data['repeatamount_info'] = $this->db->query('select * from repeatamount_details where client_id='.$id.'')->row_array();
        $data['repeatinvoice_info'] = $this->db->query('select * from repeatInvoice_details where client_id='.$id.'')->row_array();
     
        $this->load->view('Invoice/send_statements_remainder_email', $data);
	}


	public function RepeatInvoice()
	{	
		$this->Security_model->chk_login();		

		if($_SESSION['permission']['Invoices']['create'] == '1')
		{
		    $assigned_client = Get_Assigned_Datas('CLIENT');

		    foreach ($assigned_client as $key => $value) 
		    {
		        $rec = $this->Invoice_model->selectRecord('client','id', $value);
		        
		        if(!empty($rec['user_id']))
		        {
		          $arr[] = $rec['user_id'];
		        }
		    }
		    
		    $clients_user_ids = implode(',', $arr);

		    $data['clients_user_ids'] = $clients_user_ids;
		    $data['admin_settings']=$this->Common_mdl->select_record('admin_setting','firm_id',$_SESSION['firm_id']);

		    $this->load->view('Invoice/repeat_invoice',$data);
		}
		else
		{
			$this->load->view('users/blank_page');
		}			
	}

	public function AddRepeatInvoice()
	{		
		if(count($_POST)>0)
	    {
			$invoice_startdate = $_POST['invoice_startdate'];
			$invoice_enddate = $_POST['invoice_enddate'];
			$repeat_trans = $_POST['repeat_trans'];
			$repeat_set = $_POST['select_transaction'];
			$duedate_no = $_POST['duedate_no'];
			$repeat_days = $_POST['repeat_days'];
			$invoice_to = $_POST['invoice_to'];
			$reference = $_POST['reference'];
			$amt_det = $_POST['amounts_details'];

			$end_date = strtotime($invoice_enddate);
			$endDate = date('Y-m-d', $end_date);
		

			if(isset($_POST['save_as_draft']) != '') {
				$save_draft = $_POST['save_as_draft'];
			} else {
				$save_draft ='';
			}


			if(isset($_POST['payment_status']) != '') {
				$approve1 = $_POST['payment_status'];
			} else {
				$approve1 = 'pending';
			}

			if(isset($_POST['approve_send']) != '') {
				$approvesend = $_POST['approve_send'];
			} else {
				$approvesend = '';
			}

			
			if($amt_det == 1)
			{
				$amounts_type = 'Tax Exclusive';
				$tax_rate = $_POST['tax_rate'];
				$tax_amount = $_POST['tax_amount'];
			} else {
				$amounts_type = 'Tax Inclusive';
				$tax_rate = ' ';
				$tax_amount = ' ';
			}
	 
			$data = array('save_as_draft' => $save_draft,
								'approve' => $approve1,
								'approve_send' => $approvesend,
								'client_email' => $invoice_to,
								'reference' => $reference,
								'amounts_type' => $amounts_type,	
								'status' => 'Active',
								'created_at' => time()
						); 
				$ins_id = $this->Invoice_model->insert('repeatInvoice_details', $data);
					//print_r($ins_id); 
					if(isset($ins_id))
					{	
						$clientId = $ins_id;
					} 	
				
			if($repeat_set == 0)
			{
				$trans_every = 'Week';
				$week = $repeat_trans;
				$start_date = strtotime($invoice_startdate);
				
				for ($i=0; $i < $week; $i++) {

					$strtime = strtotime('+'.$i.' weeks', $start_date );				
					$newDate = date('Y-m-d', $strtime);
					$last_date = date('Y-m-t', $strtime);
					$newDate1 = strtotime($newDate);
					// $a = date('d', $strtime);
					// echo $newDate."<br>";
					
					if($repeat_days == 1)
					{	
						// $countdate = $duedate_no;
						$countdate = $_POST['duedate_no'];
						$repeat_every = 'day(s) after the invoice date';
						$in_date = strtotime($invoice_startdate);
						
						$strtotime = strtotime('+'.$countdate.' day', strtotime( $newDate ));
						// $strtotime = strtotime('+'.$countdate.' day', $c);
						$repeat_due = date('Y-m-d', $strtotime);
					}  
					$data1 = array('client_id' => $clientId,
								'repeats' => $repeat_trans,
								'transaction_every' => $trans_every,
								'invoice_startdate' => $newDate,
								'invoice_enddate' => $last_date,
								'due_days' => $duedate_no,
								'count_duedays' => $repeat_due,	
								'payment_due' => $repeat_every,
								'end_date' => $endDate,
								
						);
					$insert_id = $this->Invoice_model->insert('repeatInvoiceDate_details', $data1);	
				}		
			} 		

			if($repeat_set == 1)
			{
				$trans_every = 'Month';
				$month = $repeat_trans;
				$start_date = strtotime($invoice_startdate);
				
				// $strtime = strtotime('+'.$month.' month', $start_date );
				// $newDate = date('d/m/Y', $strtime);

				for ($i=0; $i < $month; $i++) { 
					$strtime = strtotime('+'.$i.' month', $start_date );				
					$newDate = date('Y-m-d', $strtime);
					$last_date = date('Y-m-t', $strtime);
					$newDate1 = strtotime($newDate);
					// $a = date('d', $strtime);
					// echo $newDate."<br>";
					
					if($repeat_days == 1)
					{	
						// $countdate = $duedate_no;
						$countdate = $_POST['duedate_no'];
						$repeat_every = 'day(s) after the invoice date';
						$in_date = strtotime($invoice_startdate);
						
						$strtotime = strtotime('+'.$countdate.' day', strtotime( $newDate ));
						// $strtotime = strtotime('+'.$countdate.' day', $c);
						$repeat_due = date('Y-m-d', $strtotime);
						// for ($i=0; $i < $countdate; $i++) { 
						// 	// $a = date('d', $newDate);
						// 	$strtotime = strtotime('+'.$i.' day', strtotime($newDate));
						// 	$repeat_due = date('d/m/Y', $strtotime);	
						 // echo $newDate; 
						// } 
						//echo $repeat_due;	
					}  
					$data1 = array('client_id' => $clientId,
								'repeats' => $repeat_trans,
								'transaction_every' => $trans_every,
								'invoice_startdate' => $newDate,
								'invoice_enddate' => $last_date,
								'due_days' => $duedate_no,
								'count_duedays' => $repeat_due,	
								'payment_due' => $repeat_every,
								'end_date' => $endDate,
								
						);
					$insert_id = $this->Invoice_model->insert('repeatInvoiceDate_details', $data1);
					
				}		
			}  
	              
			if($repeat_set == 2)
			{
				$trans_every = 'Year';
				$year = $repeat_trans;
				$start_date = strtotime($invoice_startdate);
				
				for ($i=0; $i < $year; $i++) { 
					$strtime = strtotime('+'.$i.' year', $start_date );				
					$newDate = date('Y-m-d', $strtime);
					$last_date = date('Y-m-t', $strtime);
					$newDate1 = strtotime($newDate);
					// $a = date('d', $strtime);
					// echo $newDate."<br>";
					
					if($repeat_days == 1)
					{	
						// $countdate = $duedate_no;
						$countdate = $_POST['duedate_no'];
						$repeat_every = 'day(s) after the invoice date';
						$in_date = strtotime($invoice_startdate);
						
						$strtotime = strtotime('+'.$countdate.' day', strtotime( $newDate ));
						// $strtotime = strtotime('+'.$countdate.' day', $c);
						$repeat_due = date('Y-m-d', $strtotime);
						
					}  
						
					$data1 = array('client_id' => $clientId,
								'repeats' => $repeat_trans,
								'transaction_every' => $trans_every,
								'invoice_startdate' => $newDate,
								'invoice_enddate' => $last_date,
								'due_days' => $duedate_no,
								'count_duedays' => $repeat_due,	
								'payment_due' => $repeat_every,
								'end_date' => $endDate	
						);
					$insert_id = $this->Invoice_model->insert('repeatInvoiceDate_details', $data1);
					//print_r($clientId);  
				}
			} 
			$item_name = $_POST['item_name'];
			$description = $_POST['description'];
			$quantity = $_POST['quantity'];
			$unit_price = $_POST['unit_price'];
			$discount = $_POST['discount'];
			$account = $_POST['account'];
			$amount_gbp = $_POST['amount_gbp'];
			
			foreach ($item_name as $key => $value) {
				// echo "<pre>";
				$itmname = $item_name[$key];  
				$descr = $description[$key];
				$qty = $quantity[$key];
				$uprice = $unit_price[$key];
				$dis = $discount[$key];
				$acc = $account[$key];
				$taxrate = $tax_rate[$key];
				$taxamount = $tax_amount[$key];
				$amt_gbp = $amount_gbp[$key];

			$data2 = array('client_id' => $clientId,
							'item' => $itmname,
							'description' => $descr,
							'quantity' => $qty,
							'unit_price' => $uprice,
							'discount' => $dis,
							'account' => $acc,
							'tax_rate' => $taxrate,
							'tax_amount' => $taxamount,
							'amount_gbp' => $amt_gbp
							
				);
				//print_r($data1);	
			$sql = $this->Invoice_model->insert('repeatproducts_details', $data2);
			} 

			$sub_total = $_POST['sub_tot'];
			$vat = $_POST['value_at_tax'];
			$adjust_tax = $_POST['adjust_tax'];
			$grand_total = $_POST['grand_total'];
			
			$data3 = array('client_id' => $clientId,
							'sub_total' => $sub_total,
							'VAT' => $vat,
							'adjust_to_tax' => $adjust_tax,
							'grand_total' => $grand_total, 
							// 'payment_status' => $payment_status
						);
			$result = $this->Invoice_model->insert('repeatamount_details', $data3);
			
			if(isset($result) && $approvesend!="")
			{		
				$from = $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'company_email');

				if(empty($from))
				{
					$from = 'admin@crm.com';
				}

				$body = $this->load->view('Invoice/invoice_remainder_email.php', $_POST, TRUE);
				$Issend = firm_settings_send_mail( $_SESSION['firm_id'] , trim($_POST['to_email']) ,'Invoice' , $body );
				
				if( $Issend['result'] )
				{
                    $send_data = array('send_statements' => 'send','updated_at' => date('Y-m-d H:i:s'));

                    if($this->Invoice_model->update('repeatInvoice_details', 'client_id', $clientId, $send_data))
                    {
	                  echo 1;		
	                }
				}
			}
			else if(isset($result))
			{
				echo 1;
			}
        }
        else
        {
        	$this->load->view('users/blank_page');
        }	
		
	}


	public function fetchData()
	{
		//$invoice_details = $this->db->query('select * from repeatInvoice_details')->result_array();
		$date_details = $this->db->query('select * from repeatInvoiceDate_details')->result_array();
		//$product_details = $this->db->query('select * from repeatproducts_details')->result_array();
		//$amount_details = $this->db->query('select * from repeatamount_details')->result_array();

		foreach ($date_details as $key => $invoice) {

			$data['email'] = $this->db->query('select * from repeatInvoice_details where client_id = '.$invoice['client_id'].'')->row_array();

			$data['products'] = $this->db->query('select * from repeatproducts_details where client_id = '.$invoice['client_id'].'')->result_array();

			$data['amounts'] = $this->db->query('select * from repeatamount_details where client_id = '.$invoice['client_id'].'')->row_array();

			
			$data['invoice_generate'] = date('Y-m-d', strtotime($invoice['invoice_startdate']));
			$data['invoice_due'] = date('Y-m-d', strtotime($invoice['count_duedays']));

			// $invoice_generate = date('Y-m-d', strtotime($invoice['invoice_startdate']));
			// $invoice_due = date('Y-m-d', strtotime($invoice['count_duedays']));
			//print_r($invoice_generate);
			$cur_date = date('Y-m-d');
			$cur_date1 = date('Y-m-d');
		
			if(($data['invoice_generate'] == $cur_date) || ($data['invoice_due'] == $cur_date1))
			{
				$body = $this->load->view('Invoice/invoiceGenerate_remainder_email.php', $data, TRUE);

				firm_settings_send_mail( $_SESSION['firm_id'] , $data['email']['client_email'] ,'Invoice' , $body );
			} 
		}

	}

	public function EditRepeatInvoice($id)
	{
		if($_SESSION['permission']['Invoices']['edit'] == '1')
		{
	   		$data['repeatinvoice_info'] = $this->Invoice_model->selectRecord('repeatInvoice_details', 'client_id', $id);
	   		$data['repeatdate_info'] = $this->Invoice_model->selectRecord('repeatInvoiceDate_details', 'client_id', $id);
	   		$data['repeatproduct_info'] = $this->Invoice_model->selectAllRecord('repeatproducts_details', 'client_id', $id);
	   		$data['repeatamount_info'] = $this->Invoice_model->selectRecord('repeatamount_details', 'client_id', $id);
	       	$data['admin_settings']=$this->Common_mdl->select_record('admin_setting','firm_id',$_SESSION['firm_id']);

	   		$this->load->view('Invoice/edit_RepeatInvoice_view', $data);	   		//print_r($id);
		}
		else
		{
			$this->load->view('users/blank_page');
		}
	}

	public function UpdateRepeatInvoice()
	{
		if(count($_POST)>0)
        {
			$re_clientID = $_POST['repeat_clientID'];
			$del_id = $this->Invoice_model->delete('repeatInvoiceDate_details', 'client_id', $re_clientID);

			$invoice_startdate = $_POST['invoice_startdate'];
			$invoice_enddate = $_POST['invoice_enddate'];
			$repeat_trans = $_POST['repeat_trans'];
			$repeat_set = $_POST['select_transaction'];
			$duedate_no = $_POST['duedate_no'];
			$repeat_days = $_POST['repeat_days'];
			$invoice_to = $_POST['invoice_to'];
			$reference = $_POST['reference'];
			$amt_det = $_POST['amounts_details'];

			$end_date = strtotime($invoice_enddate);
			$endDate = date('Y-m-d', $end_date);

			if(isset($_POST['save_as_draft']) != '') {
				$save_draft = $_POST['save_as_draft'];
			} else {
				$save_draft ='';
			}

			if(isset($_POST['payment_status']) != '') {
				$approve1 = $_POST['payment_status'];
			} else {
				$approve1 = 'pending';
			}

			if(isset($_POST['approve_send']) != '') {
				$approvesend = $_POST['approve_send'];
			} else {
				$approvesend = '';
			}

			if($amt_det == 1)
			{
				$amounts_type = 'Tax Exclusive';
				$tax_rate = $_POST['tax_rate'];
				$tax_amount = $_POST['tax_amount'];
			} else {
				$amounts_type = 'Tax Inclusive';
				$tax_rate = ' ';
				$tax_amount = ' ';
			}

			$data = array('save_as_draft' => $save_draft,
								'approve' => $approve1,
								'approve_send' => $approvesend,
								'client_email' => $invoice_to,
								'reference' => $reference,
								'amounts_type' => $amounts_type,	
								'status' => 'Active',
								'updated_at' => time()
							);	

				$qry = $this->Invoice_model->update('repeatInvoice_details', 'client_id', $re_clientID, $data);

			if($repeat_set == 0)
			{
				$trans_every = 'Week';
				$week = $repeat_trans;
				$start_date = strtotime($invoice_startdate);
				
				for ($i=0; $i < $week; $i++) { 
					$strtime = strtotime('+'.$i.' weeks', $start_date );				
					$newDate = date('Y-m-d', $strtime);
					$last_date = date('Y-m-t', $strtime);
					$newDate1 = strtotime($newDate);
					// $a = date('d', $strtime);
					// echo $newDate."<br>";
					
					if($repeat_days == 1)
					{	
						// $countdate = $duedate_no;
						$countdate = $_POST['duedate_no'];
						$repeat_every = 'day(s) after the invoice date';
						$in_date = strtotime($invoice_startdate);
						
						$strtotime = strtotime('+'.$countdate.' day', strtotime( $newDate ));
						// $strtotime = strtotime('+'.$countdate.' day', $c);
						$repeat_due = date('Y-m-d', $strtotime);
					}  
					$data1 = array('client_id' => $re_clientID,
								'repeats' => $repeat_trans,
								'transaction_every' => $trans_every,
								'invoice_startdate' => $newDate,
								'invoice_enddate' => $last_date,
								'due_days' => $duedate_no,
								'count_duedays' => $repeat_due,	
								'payment_due' => $repeat_every,
								'end_date' => $endDate,
								
						);
					$insert_id = $this->Invoice_model->insert('repeatInvoiceDate_details', $data1);	
				}		
			} 	
				
			if($repeat_set == 1)
			{
				$trans_every = 'Month';
				$month = $repeat_trans;
				$start_date = strtotime($invoice_startdate);
				
				// $strtime = strtotime('+'.$month.' month', $start_date );
				// $newDate = date('d/m/Y', $strtime);
				
				for ($i=0; $i < $month; $i++) { 
					$strtime = strtotime('+'.$i.' month', $start_date );				
					$newDate = date('Y-m-d', $strtime);
					$last_date = date('Y-m-t', $strtime);
					$newDate1 = strtotime($newDate);
					// $a = date('d', $strtime);
					// echo $newDate."<br>";
					
					if($repeat_days == 1)
					{	
						// $countdate = $duedate_no;
						$countdate = $_POST['duedate_no'];
						$repeat_every = 'day(s) after the invoice date';
						$in_date = strtotime($invoice_startdate);
						
						$strtotime = strtotime('+'.$countdate.' day', strtotime( $newDate ));
						// $strtotime = strtotime('+'.$countdate.' day', $c);
						$repeat_due = date('Y-m-d', $strtotime);
					}	
					$data1 = array('client_id' => $re_clientID,
								'repeats' => $repeat_trans,
								'transaction_every' => $trans_every,
								'invoice_startdate' => $newDate,
								'invoice_enddate' => $last_date,
								'due_days' => $duedate_no,
								'count_duedays' => $repeat_due,	
								'payment_due' => $repeat_every,
								'end_date' => $endDate,
								
						);
					$sql_id = $this->Invoice_model->insertRecords('repeatInvoiceDate_details', 'client_id', $re_clientID, $data1);
					
				}						
			}

			if($repeat_set == 2)
			{
				$trans_every = 'Year';
				$year = $repeat_trans;
				$start_date = strtotime($invoice_startdate);

				
				for ($i=0; $i < $year; $i++) { 
					$strtime = strtotime('+'.$i.' year', $start_date );				
					$newDate = date('Y-m-d', $strtime);
					$last_date = date('Y-m-t', $strtime);
					$newDate1 = strtotime($newDate);
					// $a = date('d', $strtime);
					// echo $newDate."<br>";
					
					if($repeat_days == 1)
					{	
						// $countdate = $duedate_no;
						$countdate = $_POST['duedate_no'];
						$repeat_every = 'day(s) after the invoice date';
						$in_date = strtotime($invoice_startdate);
						
						$strtotime = strtotime('+'.$countdate.' day', strtotime( $newDate ));
						// $strtotime = strtotime('+'.$countdate.' day', $c);
						$repeat_due = date('Y-m-d', $strtotime);
						
					}  
						
					$data1 = array('client_id' => $re_clientID,
								'repeats' => $repeat_trans,
								'transaction_every' => $trans_every,
								'invoice_startdate' => $newDate,
								'invoice_enddate' => $last_date,
								'due_days' => $duedate_no,
								'count_duedays' => $repeat_due,	
								'payment_due' => $repeat_every,
								'end_date' => $endDate	
						);
					$sql1_id = $this->Invoice_model->insertRecords('repeatInvoiceDate_details', 'client_id', $re_clientID, $data1); 
				}
			}
            
            $repeatInvoice_details = $this->Common_mdl->select_record('repeatInvoice_details','client_id',$re_clientID);
			
			if(empty($repeatInvoice_details['send_statements']))
			{
			   $this->Invoice_model->delete('repeatproducts_details', 'client_id', $re_clientID);	
	        }

	        $item_name = $_POST['item_name'];
	        $description = $_POST['description'];
	        $quantity = $_POST['quantity'];
	        $unit_price = $_POST['unit_price'];
	        $discount = $_POST['discount'];
	        $account = $_POST['account'];
	        $amount_gbp = $_POST['amount_gbp'];

	        foreach ($item_name as $key => $value) {
	            $itmname = $item_name[$key];  
	            $descr = $description[$key];
	            $qty = $quantity[$key];
	            $uprice = $unit_price[$key];
	            $dis = $discount[$key];
	            $acc = $account[$key];
	           if($amt_det == 1)
	           { 
	            $taxrate = $tax_rate[$key];
	            $taxamount = $tax_amount[$key];
	           } else {
	           	$taxrate = '';
	            $taxamount = '';
	           }
	            $amt_gbp = $amount_gbp[$key];

	        $data1 = array(
	        				'client_id' => $re_clientID,
	                        'item' => $itmname,
	                        'description' => $descr,
	                        'quantity' => $qty,
	                        'unit_price' => $uprice,
	                        'discount' => $dis,
	                        'account' => $acc,
	                        'tax_rate' => $taxrate,
	                        'tax_amount' => $taxamount,
	                        'amount_gbp' => $amt_gbp
	                        
	            );
	        //  echo "<pre>";
	        // print_r($data1); 
	        // $sql = $this->Invoice_model->updateRecords('products_details', 'id', $id, 'client_id', $client_id, $data1);
	        $sql = $this->Invoice_model->insertRecords('repeatproducts_details', 'client_id', $re_clientID, $data1);
	        
	        }
	        
	        $sub_total = $_POST['sub_tot'];
	        $vat = $_POST['value_at_tax'];
	        $adjust_tax = $_POST['adjust_tax'];
	        $grand_total = $_POST['grand_total'];
	        

	        $data2 = array('client_id' => $re_clientID,
	                        'sub_total' => $sub_total,
	                        'VAT' => $vat,
	                        'adjust_to_tax' => $adjust_tax,
	                        'grand_total' => $grand_total
	                    );

	        
	        $result = $this->Invoice_model->update('repeatamount_details', 'client_id', $re_clientID, $data2);

	        if(isset($result) && $approvesend!='')
	        {
	            $from = $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'company_email');

	            if(empty($from))
	            {
	            	$from = 'admin@crm.com';
	            }

	            $products_details = $this->Common_mdl->GetAllWithWhere('repeatproducts_details','client_id', $re_clientID);
                unset($_POST['description']);
                unset($_POST['quantity']);
                unset($_POST['amount_gbp']);                
	            
	            foreach ($products_details as $key => $value) 
	            {
	            	$_POST['description'][] = $value['description'];
	            	$_POST['quantity'][] = $value['quantity'];
	            	$_POST['amount_gbp'][] = $value['amount_gbp'];
	            }

	            $body = $this->load->view('Invoice/invoice_remainder_email.php', $_POST, TRUE);

	            $IsSend = firm_settings_send_mail( $_SESSION['firm_id'] , trim($_POST['to_email']) ,'Invoice' , $body );
	                  
				if( $IsSend['result'] )
				{
                    $send_data = array('send_statements' => 'send','updated_at' => date('Y-m-d H:i:s'), 'views'  => '0');

                    if($this->Invoice_model->update('repeatInvoice_details', 'client_id', $re_clientID, $send_data))
                    {
	                   echo 1;	
	                }	
				}
	            
	        }
	        else if(isset($result))
	        {
	        	echo 1;
	        }
	        
		}
        else
        {
        	$this->load->view('users/blank_page');
        }

	}

	public function DeleteRepeatInvoice($id)
	{ 
		if($_SESSION['permission']['Invoices']['delete'] == '1')
		{
			$this->Invoice_model->delete('repeatInvoice_details', 'client_id', $id);
			$this->Invoice_model->delete('repeatInvoiceDate_details', 'client_id', $id);
			$this->Invoice_model->delete('repeatproducts_details', 'client_id', $id);
			$qry = $this->Invoice_model->delete('repeatamount_details', 'client_id', $id);
			if(isset($qry))
			{
				redirect("Invoice");
			}
		}
        else
        {
        	$this->load->view('users/blank_page');
        }	
	}

	public function invoiceDelete_all()
    {
    	if($_SESSION['permission']['Invoices']['delete'] == '1')
    	{
	    	if($_POST['type']=='invoice'){
				if(isset($_POST['data_ids']))
				{
					$id = trim($_POST['data_ids']);
					$this->db->query("DELETE FROM Invoice_details WHERE client_id in ($id)");
					$this->db->query("DELETE FROM products_details WHERE client_id in ($id)");
					$this->db->query("DELETE FROM amount_details WHERE client_id in ($id)");
					echo $this->db->affected_rows();
				}
	    	}else if($_POST['type']=='repinvoice'){
					$id = trim($_POST['data_ids']);
					$this->db->query("DELETE FROM repeatInvoice_details WHERE client_id in ($id)");
					$this->db->query("DELETE FROM repeatInvoiceDate_details WHERE client_id in ($id)");
					$this->db->query("DELETE FROM repeatproducts_details WHERE client_id in ($id)");
					$this->db->query("DELETE FROM repeatamount_details WHERE client_id in ($id)");
					echo $this->db->affected_rows();
	    	}else{
	    			$id = trim($_POST['data_ids']);
					$this->db->query("DELETE FROM invoices WHERE id in ($id)");	
					echo $this->db->affected_rows();		
	    	}      		    	
    	}
        else
        {
        	$this->load->view('users/blank_page');
        }  
    }

    /*Archive records */
    public function invoiceArchive_all()
    {
   	
      	if($_POST['type']=='invoice'){
			if(isset($_POST['data_ids']))
			{
				$id = explode(',',$_POST['data_ids']);

				for($i=0;$i<count($id);$i++){
				$this->db->query("UPDATE `Invoice_details` SET `status` = 'archive' WHERE `client_id` = ".$id[$i]."");
				}
				echo $_POST['data_ids'];
			}
    	}else if($_POST['type']=='repinvoice'){
				$id = explode(',',$_POST['data_ids']);
				for($i=0;$i<count($id);$i++){
				$this->db->query("UPDATE `repeatInvoice_details` SET `status` = 'archive' WHERE `client_id` = ".$id[$i]."");
				}
				echo $_POST['data_ids'];	
    	}else{
    			$id = explode(',',$_POST['data_ids']);
				for($i=0;$i<count($id);$i++){
				$this->db->query("UPDATE `invoices` SET `status` = 'archive' WHERE `client_id` = ".$id[$i]."");
				}
				echo $_POST['data_ids'];		
    	} 
    }

    public function invoiceunArchive_all()
    {
   	
      	if($_POST['type']=='invoice'){
			if(isset($_POST['data_ids']))
			{
				$id = explode(',',$_POST['data_ids']);

				for($i=0;$i<count($id);$i++){
				$this->db->query("UPDATE `Invoice_details` SET `status` = 'active' WHERE `client_id` = ".$id[$i]."");
				}
				echo $_POST['data_ids'];
			}
    	}else if($_POST['type']=='repinvoice'){
				$id = explode(',',$_POST['data_ids']);
				for($i=0;$i<count($id);$i++){
				$this->db->query("UPDATE `repeatInvoice_details` SET `status` = 'active' WHERE `client_id` = ".$id[$i]."");
				}
				echo $_POST['data_ids'];	
    	}else{
    			$id = explode(',',$_POST['data_ids']);
				for($i=0;$i<count($id);$i++){
				$this->db->query("UPDATE `invoices` SET `status` = 'active' WHERE `client_id` = ".$id[$i]."");
				}
				echo $_POST['data_ids'];		
    	} 
    }

    public function RepinvoiceDelete_all()
    {
      if($_SESSION['permission']['Invoices']['delete'] == '1')
      {
	      if(isset($_POST['data_ids']))
	      {
	          $id = trim($_POST['data_ids']);
	          $this->db->query("DELETE FROM repeatInvoice_details WHERE client_id in ($id)");
	          $this->db->query("DELETE FROM repeatInvoiceDate_details WHERE client_id in ($id)");
	          $this->db->query("DELETE FROM repeatproducts_details WHERE client_id in ($id)");
	          $this->db->query("DELETE FROM repeatamount_details WHERE client_id in ($id)");
	          echo $id;
	      }
	   }
	   else
	   {
	   	$this->load->view('users/blank_page');
	   }     
    }

    public function getServicePackage()
    { 
    	$package = $this->db->query("SELECT * from proposal_service where firm_id = '".$_SESSION['firm_id']."' and ( service_category!='' and service_category!='0') AND service_name = '".trim($_POST['service_name'])."' AND service_category_id = '".$_POST['category_id']."'")->row_array();

    	if(count($package)>0)
    	{
    	   echo json_encode($package);
    	}
    	else
    	{
    	   echo 0;
    	}   
    }

    public function SendRepeatInvoice()
    { 	
        $today = date('Y-m-d'); 
        $invoices = $this->Common_mdl->GetAllWithWhere('repeatInvoiceDate_details','invoice_startdate',$today);

	    foreach($invoices as $key => $value) 
	    {	    	
	        $invoice_details = $this->Common_mdl->select_record('repeatInvoice_details','client_id', $value['client_id']);
	        
	        if($invoice_details['approve'] == 'pending' && $invoice_details['approve_send'] == '2')
            {
		        $to_email = $this->getEmail($invoice_details['client_email']);

		        if(!empty($to_email))
		        {
			        $data = array();
			        $products_details = $this->Common_mdl->GetAllWithWhere('repeatproducts_details','client_id', $invoice_details['client_id']);	    
			        $amount_details = $this->Common_mdl->select_record('repeatamount_details','client_id', $invoice_details['client_id']);                  
			        
			        foreach($products_details as $value1) 
			        {
			        	$data['description'][] = $value1['description'];
			        	$data['quantity'][] = $value1['quantity'];
			        	$data['amount_gbp'][] = $value1['amount_gbp'];
			        	$data['unit_price'][] = $value1['unit_price'];
			        }	      

		        	$data['grand_total'] = $amount_details['grand_total'];
		        	$data['sub_tot'] = $amount_details['sub_total'];     
			        $data['invoice_startdate'] = $value['invoice_startdate'];
			        $data['reference'] = $invoice_details['reference'];
			        
			        $body = $this->load->view('Invoice/invoice_remainder_email.php', $data, TRUE);

			        $from = $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'company_email');

			        if(empty($from))
			        {
			        	$from = 'admin@crm.com';
			        }

			        $this->load->library('email');
			        $this->email->set_mailtype('html');
			        $this->email->from($from);
			        $this->email->to();
			        $this->email->subject();	
			        $this->email->message();	           

			        $Issent = firm_settings_send_mail( $_SESSION['firm_id'] , trim($to_email) ,'Invoice' , $body );

					if( $Issent['result'] )
					{
			            $send_data = array('send_statements' => 'send','updated_at' => date('Y-m-d H:i:s'));
			            $this->Invoice_model->update('repeatInvoice_details', 'client_id', $invoice_details['client_id'], $send_data);
			            $datas['log'] = "Sent ".$data['reference']." Invoice To ".$to_email;
			            $datas['createdTime'] = time();
			            $datas['module'] = 'Invoice';
			            $datas['user_id'] = $invoice_details['client_email'];
			            $this->Common_mdl->insert('activity_log',$datas);
					}
			   }
			}   		
	    }

	    unset($_SESSION['firm_id']);		
    }

    public function getEmail($client_email)
    {
    	$email = "";
    	$client = $this->db->query("SELECT user.*,client.crm_email FROM user LEFT JOIN client ON user.id = client.user_id where user.autosave_status!='1' and user.crm_name!='' AND user.id = '".$client_email."'")->row_array();

    	$contact_mail = $this->db->query("SELECT main_email from client_contacts where client_id='".$client['id']."' and make_primary='1' limit 1")->row_array();
    	           
    	if($contact_mail['main_email']!= '')
    	{    	          
    	   $email =  $contact_mail['main_email'];
    	}
    	else if($client['crm_email_id']!= '')
    	{ 
    	   $email = $client['crm_email_id']; 
        } 
    	else if($client['crm_email']!= '')
    	{
    	   $email = $client['crm_email']; 
        }

        $_SESSION['firm_id'] = $client['firm_id'];

        return $email;
    }

    public function CreateSubscriptionInvoice()
    {
        $clients = $this->db->query('SELECT * FROM client ORDER BY id ASC')->result_array();
    	
    	foreach($clients as $key => $value) 
    	{ 
    	   if(date('m-d',$value['created_date']) == date('m-d') && date('Y',$value['created_date']) < date('Y'))
    	   { 
    	   	  $this->Invoice_model->CreateInvoice($value); 
    	   }
    	}
        
        return true;
    }
   
}
