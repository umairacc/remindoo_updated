
               <table class="invoice-details generate-invoice" style="margin-top:30px;border: none;width:100%;">
                  <tr>
                    <td colspan="2">
                    <div class="online-payments" style="float: left;width: 100%;padding-bottom: 30px">
                      <div class="temp-edit" style="float: left;width: 50%;">
                       <h3><?php echo ucwords($this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'crm_name')); ?></h3>
                      </div>
                      <div class="payment-table" style="float: right;width: 40%;">
                        <table style="border: 1px solid #e9ecef;width:100%;border-collapse: collapse;">
                          <tr>
                            <td colspan="2" style="padding: 10px;border-bottom: 1px solid #eee;">
                              <h3 style="font-size: 1.75rem;margin-bottom: 5px;margin-top: 0;"><i class="fa fa-gbp" aria-hidden="true"></i><?php echo $grand_total; ?></h3>
                              <h5 style="font-size: 14px;margin: 0;">Amount due <span>(<?php echo $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'crm_currency'); ?>)</span></h5>
                            </td>
                          </tr>
                          <tr>
                            <td style="padding: 10px;border-right: 1px solid #eee;    border-bottom: 1px solid #ddd;"><h3 class="upon-res" style="font-size: 18px;margin: 0;">Upon Reciept</h3>
                            <h6 style="font-size: 14px;margin: 10px 0;">Due</h6></td>
                            <td style="padding: 10px;    border-bottom: 1px solid #ddd;">
                            <?php if(isset($invoice_no)) { ?> 
                               <span class="due-date12" style="font-size: 17px;padding-bottom: 5px;display: block;"><?php echo $invoice_no; ?></span>
                              <h5 style="font-size: 14px;    margin: 5px 0;" class="invoice-no">invoice no</h5>
                            <?php } ?>
                            <?php if(isset($reference)) { ?> 
                               <span class="due-date12" style="font-size: 17px;padding-bottom: 5px;display: block;"><?php echo $reference; ?></span>
                              <h5 style="font-size: 14px;    margin: 5px 0;" class="invoice-no">invoice no</h5>
                            <?php } ?>
                             
                            </td>
                          </tr>
                          <tr>
                            <!-- <td colspan="2" style="padding: 10px;">
                              <a href="javascript:;" style="display: block;background: #38b87c;color: #fff;padding: 10px;text-align: center;text-transform: uppercase;font-size: 15px;font-weight: 600;" class="onlinepayments">accept online payments</a>
                            </td> -->
                          <tr>
                        </table>
                      </div>
                    </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="generate-top">
                        <span class="project-name" style="float: left;width: 50%;font-size: 29px;text-transform: capitalize;">
                          invoice
                          <!-- <span style="display: block;">project name</span> -->
                        </span>
                        <span class="date-sec" style="float: left;width: 50%;text-align: right;line-height: 74px;">
                        <?php if(isset($invoice_date)) { ?> 
                           <input style="width: 100px;padding-left: 10px;height: 30px;font-size: 14px;" type="text" class="g-date" value="<?php echo $invoice_date; ?>"> 
                        <?php } ?>

                        <?php if(isset($invoice_startdate)) { ?> 
                           <input style="width: 100px;padding-left: 10px;height: 30px;font-size: 14px;" type="text" class="g-date" value="<?php echo date('Y-m-d',strtotime($invoice_startdate)); ?>"> 
                        <?php } ?>
                         
                        </span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <td>
                    <div class="des-table" style="float: left;width: 100%;margin-top: 30px;">
                    <div class="table-responsive" style="display: inline-block;width: 100%;overflow-x: auto;">
                             <table style="width:100%;border-collapse: collapse;">
                                <tr>
                                   <th style="background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">description</th>
                                   <th style="background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">qty</th>
                                   <th style="background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">price</th>
                                   <th style="width:5%;background: #f6f6f6;font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;text-align: left;border-right: 1px solid #ccc;">amount</th>
                                </tr>
                              <?php foreach ($description as $key => $value) { ?>    
                                <tr>
                              
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"><?php echo $description[$key]; ?></td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"><?php echo $quantity[$key]; ?></td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"><i class="fa fa-gbp"></i> <?php echo $unit_price[$key]; ?></td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;"> <i class="fa fa-gbp"></i> <?php echo $amount_gbp[$key]; ?></td>
                                </tr>
                              <?php } ?>  
                                <!-- <tr>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;">accounts</td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;">1</td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;"><i class="fa fa-gbp"></i> 350.00</td>
                                  <td style="font-size: 15px;text-transform: capitalize;padding: 10px;border-bottom: 1px solid #ccc;"><i class="fa fa-gbp"></i> 350.00</td>
                                </tr> -->
                              </table>
                    </div>

                      <div class="sub-totaltab" style="width: 350px;float: right;margin-top: 20px;">
                        <table style="border: 1px solid #e9ecef;width:100%;">
                          <tr>
                          <th style="background: transparent;padding: 10px;border: none;text-align: right;font-size: 15px;">sub total</th>
                          <td style="border-right: none;background: transparent;border: none;text-align: right;padding: 10px;"><i class="fa fa-gbp">  <?php echo $sub_tot; ?></i></td>
                          </tr>
                          <tr>
                          <th tyle="background: transparent;padding: 10px;border: none;text-align: right;font-size: 15px;">total</th>
                          <td style="border-right: none;background: transparent;border: none;text-align: right;padding: 10px;"><i class="fa fa-gbp"> <?php echo $grand_total; ?></i></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </td>
                  </tr>
                  <tr>
                    <td>
                    <div class="payment-ins" style="float: left;width: 100%;border-top: 1px solid #ccc;padding-top: 10px;margin-top: 30px;">
                      <!-- <span style="font-size: 15px;text-transform: capitalize;">payment instrctions</span> -->
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                  <!--   <div class="payment-ins" style="float: left;width: 100%;border-top: 1px solid #ccc;padding-top: 10px;margin-top: 30px;">
                      <span class="place" style="border: 1px solid #ccc;padding: 6px 10px;margin-top: 10px;display: inline-block;font-size: 15px;text-transform: capitalize;">united kingdom</span>
                    </div> -->
                    </td>
                  </tr>
              </table>
           