<?php 

$this->load->view('includes/header');

?>

<style type="text/css">
  .euro-position span { font-size: 12px; }
  .radio-inline input {
	    width: auto !important;
	    opacity: 1 !important;
	    position: relative !important;
	    left: 0;
	    margin-left: 60px;
	}
   #invoice_general_settings
   {
   	float: left !important;
   	margin-left: 30% !important;
   }
   .cmn-invoice-approve {
   	width: 40%;
   	float: left !important;
   }
   .invoice-templates{
   	width: 100px;
    height: 150px;
    margin-right: 0px !important;
    margin-left: 30px;
   }
   input[type="color"] {
    height: 34px;
    width: 80px;
   }

   #hexcolor:invalid{
	  outline: 2px solid red;
	}
	textarea{
		width: 50% !important;
	}

	.textarea-label
	{
		margin-left: 0px !important;
	}

	#img_preview
	{
		margin: 10px 0 15px;
		width: 200px;
	}

	.general_settings_header
	{
		margin-left: 5%;
	}

	.invoice-templates
	{
		border: 1px solid black;
	}

	.error
	{
		margin-left: 5%;
		color: red;
	}

	.success
	{
		margin-left: 5%;
		color: green;
	}

  #due_date_x_days
  {
    width: 50%;
  }
</style>
<?php
	$color = '';
	if(isset($accent_color))
	{
		$color = $accent_color;
	}

	$path = '#';
	if(isset($logo_path) && ($logo_path))
	{
		$path = base_url().$logo_path;
	}

	$h_text = '#';
	if(isset($header_text))
	{
		$h_text = trim($header_text);
	}

	$f_text = '#';
	if(isset($footer_text))
	{
		$f_text = trim($footer_text);
	}

  $f_id = '';
  if(isset($firm_id))
  {
    $f_id = trim($firm_id);
  }

  $x_days = '';
  if(isset($due_date_x_days))
  {
    $x_days = trim($due_date_x_days);
  }

?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/template.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/classic-template.css">
<style type="text/css">
  .invoice-top-1,th
  {
    background: <?php echo $color;?> !important;
  }
</style>
<div class="modal-alertsuccess alert  succ popup_info_box" style="display:none;">
  <div class="newupdate_alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              </div>
         </div>
         </div>
   </div>

<!-- management block -->
<div id="print_preview">
<div class="pcoded-content new-upload-03 invoice-setting-wrapper">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper new-invoice">

         <div class="deadline-crm1 floating_set invoice-align1">
            <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                    <li class="nav-item">
                      <a href="#">Invoice General Settings</a>
                    </li>
                  </ul>
          </div>

        	<h4 class="general_settings_header">Invoice General Settings</h4>
        	<b class="error"><?php if(isset($response)) echo $response; ?></b>
        	<b class="success"><?php if(isset($success)) echo $success; ?></b>
            <div class="firm-field floating_set">
              <form id="invoice_general_settings" name="invoice_general_settings" action="<?php echo base_url()?>invoice/PostGeneralSettings" method="post" enctype="multipart/form-data">
                <?php 
                    $model_data_target = '#';
                    if(isset($template) && ($template == 'classic'))
                      {
                        $model_data_target = '#temp_invoice_classic';
                      }
                      elseif (isset($template) && ($template == 'modern')) {
                        $model_data_target = '#temp_invoice_modern';
                      }

                ?>
               <div class="invoice-details">               
                     <div class="invoice-top">   
                         <span class="preview-pdf" id="preview-pdf">
                               <a href="#"  data-toggle="modal" data-target="<?php echo $model_data_target;?>" class="btn btn-card btn-primary" aria-hidden="true" id="preview_btn"><i class="fa fa-eye fa-5"></i>preview</a>
                         </span>
                     </div> 


	                <div class="invoice-table floating_set">
	                    <label>Template</label>
	                     <label class="radio-inline">
	                     	<div class="invoice-templates">
	                     		<img class="sample_invoice" src="<?php echo base_url() ?>assets/images/invoice-templates/classic.png" />
	                     	</div>
					      <input type="radio" name="template" value="classic" <?php if(isset($template) && ($template == 'classic')){ echo 'checked';}?> >Classic
					     </label>
					     <label class="radio-inline">
					     	<div class="invoice-templates">
					     		<img class="sample_invoice" src="<?php echo base_url() ?>assets/images/invoice-templates/modern.png" />
					     	</div>
					      <input type="radio" name="template" value="modern" <?php if(isset($template) && ($template == 'modern')){ echo 'checked';}?>>Modern
					     </label>
				     </div>

                     <div class="invoice-table floating_set">
                        <label>Accent Color</label>
                        <input type="color" id="colorpicker" name="accent_color" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" value="<?php echo $color;?>"> 
 
						<input type="text" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" value="<?php echo $color;?>" id="hexcolor" name="hexcolor">
                     </div>

                     <div class="invoice-table floating_set">
                     	<div class="row textarea-label">
                        	<label>Header Text</label>
                    	</div>
                        <textarea name="header_text" id="header_text">
                        	<?php echo $h_text;?>
                        </textarea>
                     </div>

                     <div class="invoice-table floating_set">
                        <div class="row textarea-label">
                        	<label>Footer Text</label>
                    	</div>
                        <textarea name="footer_text" id="footer_text">
                        	<?php echo $f_text;?>
                        </textarea>
                     </div>

                     <div class="invoice-table floating_set">
                        <div class="row textarea-label">
                          <label>No. of Days After Due Date</label>
                      </div>
                        <input type="number" name="due_date_x_days" id="due_date_x_days" value="<?php echo $x_days;?>">
                     </div>

                     <div class="invoice-table floating_set">
                       
                      
                    <div class="cmn-invoice-approve floating_set">
                     <div class="attach-invoice1">
                     	<label>Logo</label>
                      <div class="custom_upload upload-data06">
                           <input type="file" id="invoice_logo" name="invoice_logo">
                           </div>
                        <div id="image_preview">
                          
                        </div>                         
                        <div id="img_preview">
                          <img id="blah" src="<?php echo $path;?>" alt="" />
                        </div> 
                    <div class="decline-quote1 floating_set">
                    <div class="save-approve" id="save-approve">
                      <div class="approve-btn">
                          <div class="table-savebtn">
                          	<input type="submit" name="submit" class="btn btn-card btn-primary" id="invoice_save"  value="Submit">
                          </div>
                      </div>
                    </div>

                              
                           </div>
                         </div>


                  </div>
                      </div><!--invoice table -->
               </div>
            </form>   
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<div class="modal fade modal-invoice-quote1" id="temp_invoice_classic" role="dialog">
    <div class="modal-dialog modal-classic-invoice">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Invoice</h4>
        </div>
        <div class="modal-body">
         <div id="container">
          <div class="invoice-top-1">
            <section id="memo">
              <div class="logo">
                <img src="<?php echo $path;?>" />
              </div>
              
              <div class="company-info">
                <span class="company-name">Remindoo.org</span>

                <span class="spacer"></span>
                  <div><?php echo $h_text;?></div>
                <span class="clearfix"></span>
              </div>

            </section>
            
            <section id="invoice-info">
              <div>
                <span>{issue_date_label}</span>
                <span>{net_term_label}</span>
                <span>{due_date_label}</span>
                <span>{po_number_label}</span>
              </div>
              
              <div>
                <span>{issue_date}</span>
                <span>{net_term}</span>
                <span>{due_date}</span>
                <span>{po_number}</span>
              </div>

              <span class="clearfix"></span>

              <section id="invoice-title-number">
            
                <span id="title">{invoice_title}</span>
                <span id="number">{invoice_number}</span>
                
              </section>
            </section>
            
            <section id="client-info">
              <span>{bill_to_label}</span>
              <div>
                <span class="bold">{client_name}</span>
              </div>
              
              <div>
                <span>{client_address}</span>
              </div>
              
              <div>
                <span>{client_city_zip_state}</span>
              </div>
              
              <div>
                <span>{client_phone_fax}</span>
              </div>
              
              <div>
                <span>{client_email}</span>
              </div>
              
              <div>
                <span>{client_other}</span>
              </div>
            </section>

            <div class="clearfix"></div>
          </div>

          <div class="clearfix"></div>

          <div class="invoice-body">
            <section id="items">
              
              <table cellpadding="0" cellspacing="0">
              
                <tr>
                  <th>{item_row_number_label}</th> <!-- Dummy cell for the row number and row commands -->
                  <th>{item_description_label}</th>
                  <th>{item_quantity_label}</th>
                  <th>{item_price_label}</th>
                  <th>{item_discount_label}</th>
                  <th>{item_tax_label}</th>
                  <th>{item_line_total_label}</th>
                </tr>
                
                <tr data-iterate="item">
                  <td>{item_row_number}</td> <!-- Don't remove this column as it's needed for the row commands -->
                  <td>{item_description}</td>
                  <td>{item_quantity}</td>
                  <td>{item_price}</td>
                  <td>{item_discount}</td>
                  <td>{item_tax}</td>
                  <td>{item_line_total}</td>
                </tr>
                
              </table>
              
            </section>
            
            <section id="sums">
            
              <table cellpadding="0" cellspacing="0">
                <tr>
                  <th>{amount_subtotal_label}</th>
                  <td>{amount_subtotal}</td>
                  <td></td>
                </tr>
                
                <tr data-iterate="tax">
                  <th>{tax_name}</th>
                  <td>{tax_value}</td>
                  <td></td>
                </tr>
                
                <tr class="amount-total">
                  <th>{amount_total_label}</th>
                  <td>{amount_total}</td>
                  <td>
                    <div class="currency">
                      <span>{currency_label}</span> <span>{currency}</span>
                    </div>
                  </td>
                </tr>
                
                <!-- You can use attribute data-hide-on-quote="true" to hide specific information on quotes.
                     For example Invoicebus doesn't need amount paid and amount due on quotes  -->
                <tr data-hide-on-quote="true">
                  <th>{amount_paid_label}</th>
                  <td>{amount_paid}</td>
                  <td></td>
                </tr>
                
                <tr data-hide-on-quote="true">
                  <th>{amount_due_label}</th>
                  <td>{amount_due}</td>
                  <td></td>
                </tr>
                
              </table>
              
            </section>

            <div class="clearfix"></div>
            
            <section id="terms">
            
              <span class="hidden">{terms_label}</span>
              <div><?php echo $f_text;?></div>
              
            </section>

            <div class="payment-info">
              <div>{payment_info1}</div>
              <div>{payment_info2}</div>
              <div>{payment_info3}</div>
              <div>{payment_info4}</div>
              <div>{payment_info5}</div>
            </div>
          </div>
            
        </div>
        </div>
       <!--  <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>

  <div class="modal fade modal-invoice-quote1" id="temp_invoice_modern" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Invoice</h4>
        </div>
        <div class="modal-body">
          <div id="container">
            <section id="memo">
              <div class="logo">
                <img src="<?php echo $path;?>" />
              </div>
              
              <div class="company-info">
                <div>Remindoo.org</div>

                <br />
              </div>

            </section>

            <section id="invoice-title-number">
            
              <span id="title">Invoice</span>
              <span id="number">{invoice_number}</span>
              
            </section>
            
            <div class="clearfix"></div>
            
            <section id="client-info">
              <span>{bill_to_label}</span>
              <div>
                <span class="bold">{client_name}</span>
              </div>
              
              <div>
                <span>{client_address}</span>
              </div>
              
              <div>
                <span>{client_city_zip_state}</span>
              </div>
              
              <div>
                <span>{client_phone_fax}</span>
              </div>
              
              <div>
                <span>{client_email}</span>
              </div>
              
              <div>
                <span>{client_other}</span>
              </div>
            </section>
            
            <div class="clearfix"></div>
            
            <section id="items">
              
              <table cellpadding="0" cellspacing="0">
              
                <tr>
                  <th>{item_row_number_label}</th> <!-- Dummy cell for the row number and row commands -->
                  <th>{item_description_label}</th>
                  <th>{item_quantity_label}</th>
                  <th>{item_price_label}</th>
                  <th>{item_discount_label}</th>
                  <th>{item_tax_label}</th>
                  <th>{item_line_total_label}</th>
                </tr>
                
                <tr data-iterate="item">
                  <td>{item_row_number}</td> <!-- Don't remove this column as it's needed for the row commands -->
                  <td>{item_description}</td>
                  <td>{item_quantity}</td>
                  <td>{item_price}</td>
                  <td>{item_discount}</td>
                  <td>{item_tax}</td>
                  <td>{item_line_total}</td>
                </tr>
                
              </table>
              
            </section>
            
            <section id="sums">
            
              <table cellpadding="0" cellspacing="0">
                <tr>
                  <th>{amount_subtotal_label}</th>
                  <td>{amount_subtotal}</td>
                </tr>
                
                <tr data-iterate="tax">
                  <th>{tax_name}</th>
                  <td>{tax_value}</td>
                </tr>
                
                <tr class="amount-total">
                  <th>{amount_total_label}</th>
                  <td>{amount_total}</td>
                </tr>
                
                <!-- You can use attribute data-hide-on-quote="true" to hide specific information on quotes.
                     For example Invoicebus doesn't need amount paid and amount due on quotes  -->
                <tr data-hide-on-quote="true">
                  <th>{amount_paid_label}</th>
                  <td>{amount_paid}</td>
                </tr>
                
                <tr data-hide-on-quote="true">
                  <th>{amount_due_label}</th>
                  <td>{amount_due}</td>
                </tr>
                
              </table>

              <div class="clearfix"></div>
              
            </section>
            
            <div class="clearfix"></div>

            <section id="invoice-info">
              <div>
                <span>{issue_date_label}</span> <span>{issue_date}</span>
              </div>
              <div>
                <span>{due_date_label}</span> <span>{due_date}</span>
              </div>

              <br />

              <div>
                <span>{currency_label}</span> <span>{currency}</span>
              </div>
              <div>
                <span>{po_number_label}</span> <span>{po_number}</span>
              </div>
              <div>
                <span>{net_term_label}</span> <span>{net_term}</span>
              </div>
            </section>
            
            <section id="terms">

              <div class="notes">{terms}</div>

              <br />

              <div class="payment-info">
                <div>{payment_info1}</div>
                <div>{payment_info2}</div>
                <div>{payment_info3}</div>
                <div>{payment_info4}</div>
                <div>{payment_info5}</div>
              </div>
              
            </section>

            <div class="clearfix"></div>

            <div class="thank-you">{terms_label}</div>

            <div class="clearfix"></div>
          </div>
        </div>
       <!--  <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<script type="text/javascript">


     $(document).ready(function() {
        if (window.File && window.FileList && window.FileReader) {
          $("#invoice_logo").on("change", function(e) {    
             $("#image_preview").html('');   
            var files = e.target.files,
            filesLength = files.length;
            readURL(this);
              for (var i = 0; i < filesLength; i++) {

                  var filenames = this.files[i].name;                     
                  var fileType=files[i].type;
                  var fileSize=files[i].size / 1024 / 1024;     
                  var fileName= files[i].name;  
                  console.log(fileName);
                  if(fileSize <= 2)
                  {
                  var ValidImageTypes = ["application/pdf", "image/jpg", "image/png","image/jpeg","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel","text/csv","application/doc","application/docx","application/msword"];
                  if ($.inArray(fileType, ValidImageTypes) < 0) {
                     $("#upload").attr('disabled','disabled');
                     $("#error_content").html('Document Must be Png,JPG,JPEG,PDF,xlsx,csv Format');
                   //  $("#Error_show").modal('show');
   
                   $(".Error-alert").show();
                    // alert('Document Must be Png,JPG,JPEG,PDF Format');
                  }else{ 
                     $("#upload").removeAttr("disabled");
                   if(fileType =='application/pdf'){

                      $('#image_preview').append("<div class='col-md-12 pip original' id="+files[i].name+"><div class='bg-filei'>"+files[i].name+"<span class='removed' id='remove_"+i+"'>X</span><input type='hidden' name='image_name[]' value='" +files[i].name+ "'></div></div>");  
   
                       $('#image_preview1').append("<div class='col-md-12 pip' id="+files[i].name+"><div class='bg-filei'>"+files[i].name+"<span class='removed' id='remove_"+i+"'>X</span><input type='hidden' name='image_name[]' value='" +files[i].name+ "'></div></div>");  
                   }else{
                       $('#image_preview').append("<div class='col-md-12 pip original' id="+files[i].name+"><div class='bg-filei'>"+files[i].name+"<span class='removed' id='remove_"+i+"'>X</span><input type='hidden' name='image_name[]' value='" +files[i].name+ "'></div></div>");  
                        $('#image_preview1').append("<div class='col-md-12 pip' id="+files[i].name+"><div class='bg-filei'>"+files[i].name+"<span class='removed' id='remove_"+i+"'>X</span><input type='hidden' name='image_name[]' value='" +files[i].name+ "'></div></div>");  
                   }
                   
                   

                 

                }
               }else{
                $("#upload").attr('disabled','disabled');
               // alert('FileSize Is Too Large');
   
               $(".File-alert").show();
               }
              } 
              //
                   var accept = [];
                   $("#image_preview").find(".original .bg-filei").each(function() {    
                     var value=$(this).html().split('<span');
                     var removeItem =value[0];
                     accept.push(removeItem);   
                   });
                   console.log("selected file-"+accept.join());
                  $("#attch_image").val(accept.join());


          });
        }else {
          alert("Your browser doesn't support to File API")
        }

        $('#colorpicker').on('input', function() {
			$('#hexcolor').val(this.value);
		});
		$('#hexcolor').on('input', function() {
		  $('#colorpicker').val(this.value);
		});
        


      });  
  $(document).on("click",".removed",function(){

      $('#blah').attr('src', '#');
	  $(this).parents(".original").remove();
	  $("#invoice_logo").val();
    });  

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
} 

</script> 
<?php $this->load->view('Invoice/package_scripts'); ?>
</body>
</html>