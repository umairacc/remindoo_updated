<!DOCTYPE html>
<!--
  Invoice template by invoicebus.com
  To customize this template consider following this guide https://invoicebus.com/how-to-create-invoice-template/
  This template is under Invoicebus Template License, see https://invoicebus.com/templates/license/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Aldona (black)</title>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="Invoicebus Invoice Template">
    <meta name="author" content="Invoicebus">

    <meta name="template-hash" content="ff0b4f896b757160074edefba8cfab3b">

    <style type="text/css">
      /*! Invoice Templates @author: Invoicebus @email: info@invoicebus.com @web: https://invoicebus.com @version: 1.0.0 @updated: 2015-02-27 16:02:57 @license: Invoicebus */
/* Reset styles */
@import url("https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700&subset=cyrillic,cyrillic-ext,latin,greek-ext,greek,latin-ext,vietnamese");
#temp_invoice_classic div,#temp_invoice_classic span,#temp_invoice_classic applet,#temp_invoice_classic object,#temp_invoice_classic iframe,
#temp_invoice_classic h1,#temp_invoice_classic h2,#temp_invoice_classic h3,#temp_invoice_classic h4,#temp_invoice_classic h5,#temp_invoice_classic h6,#temp_invoice_classic p,#temp_invoice_classic blockquote,#temp_invoice_classic pre,
#temp_invoice_classic a,#temp_invoice_classic abbr,#temp_invoice_classic acronym,#temp_invoice_classic address,#temp_invoice_classic big,#temp_invoice_classic cite,#temp_invoice_classic code,
#temp_invoice_classic del,#temp_invoice_classic dfn,#temp_invoice_classic em,#temp_invoice_classic img,#temp_invoice_classic ins,#temp_invoice_classic kbd,#temp_invoice_classic q,#temp_invoice_classic s,#temp_invoice_classic samp,
#temp_invoice_classic small,#temp_invoice_classic strike,#temp_invoice_classic strong,#temp_invoice_classic sub,#temp_invoice_classic sup,#temp_invoice_classic tt,#temp_invoice_classic var,
#temp_invoice_classic b,#temp_invoice_classic u,#temp_invoice_classic i,#temp_invoice_classic center,
#temp_invoice_classic dl,#temp_invoice_classic dt,#temp_invoice_classic dd,#temp_invoice_classic ol,#temp_invoice_classic ul,#temp_invoice_classic li,
#temp_invoice_classic fieldset,#temp_invoice_classic form,#temp_invoice_classic label,#temp_invoice_classic legend,
#temp_invoice_classic table,#temp_invoice_classic caption,#temp_invoice_classic tbody,#temp_invoice_classic tfoot,#temp_invoice_classic thead,#temp_invoice_classic tr,#temp_invoice_classic th,#temp_invoice_classic td,
#temp_invoice_classic article,#temp_invoice_classic aside,#temp_invoice_classic canvas,#temp_invoice_classic details,#temp_invoice_classic embed,
#temp_invoice_classic figure,#temp_invoice_classic figcaption,#temp_invoice_classic footer,#temp_invoice_classic header,#temp_invoice_classic hgroup,
#temp_invoice_classic menu,#temp_invoice_classic nav,#temp_invoice_classic output,#temp_invoice_classic ruby,#temp_invoice_classic section,#temp_invoice_classic summary,
#temp_invoice_classic time,#temp_invoice_classic mark,#temp_invoice_classic audio,#temp_invoice_classic video {
  margin: 0;
  padding: 0;
  border: 0;
  font: inherit;
  font-size: 100%;
  vertical-align: baseline;
}

html #temp_invoice_classic{
  line-height: 1;
}

#temp_invoice_classic ol, #temp_invoice_classic ul {
  list-style: none;
}

#temp_invoice_classic table {
  border-collapse: collapse;
  border-spacing: 0;
}

#temp_invoice_classic caption, #temp_invoice_classic th, #temp_invoice_classic td {
  text-align: left;
  font-weight: normal;
  vertical-align: middle;
}

#temp_invoice_classic q, #temp_invoice_classic blockquote {
  quotes: none;
}
#temp_invoice_classic q:before, #temp_invoice_classic q:after, #temp_invoice_classic blockquote:before, #temp_invoice_classic blockquote:after {
  content: "";
  content: none;
}

#temp_invoice_classic a img {
  border: none;
}

#temp_invoice_classic article,#temp_invoice_classic aside,#temp_invoice_classic details,#temp_invoice_classic figcaption,#temp_invoice_classic figure,#temp_invoice_classic footer,#temp_invoice_classic header,#temp_invoice_classic hgroup,#temp_invoice_classic main,#temp_invoice_classic menu,#temp_invoice_classic nav,#temp_invoice_classic section, #temp_invoice_classic summary {
  display: block;
}

/* Invoice styles */
/**
 * DON'T override any styles for the <html> and <body> tags, as this may break the layout.
 * Instead wrap everything in one main <div id="container"> element where you may change
 * something like the font or the background of the invoice
 */
html, body {
  /* MOVE ALONG, NOTHING TO CHANGE HERE! */
}

/** 
 * IMPORTANT NOTICE: DON'T USE '!important' otherwise this may lead to broken print layout.
 * Some browsers may require '!important' in oder to work properly but be careful with it.
 */
#temp_invoice_classic .clearfix {
  display: block;
  clear: both;
}

#temp_invoice_classic .hidden {
  display: none;
}

#temp_invoice_classic b,#temp_invoice_classic strong,#temp_invoice_classic .bold {
  font-weight: bold;
}

#temp_invoice_classic #container {
  font: normal 13px/1.4em 'Open Sans', Sans-serif;
  margin: 0 auto;
  min-height: 1078px;
}

#temp_invoice_classic .invoice-top-1 {
  background: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PHJhZGlhbEdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgY3g9IjUwJSIgY3k9IjUwJSIgcj0iMTAwJSI+PHN0b3Agb2Zmc2V0PSIxMCUiIHN0b3AtY29sb3I9IiMzMzMzMzMiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMDAwMDAiLz48L3JhZGlhbEdyYWRpZW50PjwvZGVmcz48cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDAlIiBmaWxsPSJ1cmwoI2dyYWQpIiAvPjwvc3ZnPiA=');
  background: -moz-radial-gradient(center center, circle farthest-corner, #333333 10%, #000000);
  background: -webkit-radial-gradient(center center, circle farthest-corner, #333333 10%, #000000);
  background: radial-gradient(circle farthest-corner at center center, #333333 10%, #000000);
  color: #fff;
  padding: 40px 40px 30px 40px;
}

#temp_invoice_classic .invoice-body {
  padding: 50px 40px 40px 40px;
}

#temp_invoice_classic #memo .logo {
  float: left;
  margin-right: 20px;
}
#temp_invoice_classic #memo .logo img {
  width: 150px;
  height: 150px;
}
#temp_invoice_classic #memo .company-info {
  float: right;
  text-align: right;
}
#temp_invoice_classic #memo .company-info .company-name {
  color: #F8ED31;
  font-weight: bold;
  font-size: 32px;
}
#temp_invoice_classic #memo .company-info .spacer {
  height: 15px;
  display: block;
}
#temp_invoice_classic #memo .company-info div {
  font-size: 12px;
  float: right;
  margin: 0 3px 3px 0;
}
#temp_invoice_classic #memo:after {
  content: '';
  display: block;
  clear: both;
}

#temp_invoice_classic #invoice-info {
  float: left;
  margin-top: 50px;
}
#temp_invoice_classic #invoice-info > div {
  float: left;
}
#temp_invoice_classic #invoice-info > div > span {
  display: block;
  min-width: 100px;
  min-height: 18px;
  margin-bottom: 3px;
}
#temp_invoice_classic #invoice-info > div:last-of-type {
  margin-left: 10px;
  text-align: right;
}
#temp_invoice_classic #invoice-info:after {
  content: '';
  display: block;
  clear: both;
}

#temp_invoice_classic #client-info {
  float: right;
  margin-top: 50px;
  margin-right: 30px;
  min-width: 220px;
}
#temp_invoice_classic #client-info > div {
  margin-bottom: 3px;
}
#temp_invoice_classic #client-info span {
  display: block;
}
#temp_invoice_classic #client-info > span {
  margin-bottom: 3px;
}

#temp_invoice_classic #invoice-title-number {
  margin-top: 30px;
}
#temp_invoice_classic #invoice-title-number #title {
  margin-right: 5px;
  text-align: right;
  font-size: 35px;
}
#temp_invoice_classic #invoice-title-number #number {
  margin-left: 5px;
  text-align: left;
  font-size: 20px;
}

#temp_invoice_classic table {
  table-layout: fixed;
}
#temp_invoice_classic table th,#temp_invoice_classic table td {
  vertical-align: top;
  word-break: keep-all;
  word-wrap: break-word;
}

#temp_invoice_classic #items .first-cell,#temp_invoice_classic #items table th:first-child,#temp_invoice_classic #items table td:first-child {
  width: 18px;
  text-align: right;
}
#temp_invoice_classic #items table {
  border-collapse: separate;
  width: 100%;
}
#temp_invoice_classic #items table th {
  font-weight: bold;
  padding: 12px 10px;
  text-align: right;
  border-bottom: 1px solid #444;
  text-transform: uppercase;
}
#temp_invoice_classic #items table th:nth-child(2) {
  width: 30%;
  text-align: left;
}
#temp_invoice_classic #items table th:last-child {
  text-align: right;
}
#temp_invoice_classic #items table td {
  border-right: 1px solid #b6b6b6;
  padding: 15px 10px;
  text-align: right;
}
#temp_invoice_classic #items table td:first-child {
  text-align: left;
  border-right: none !important;
}
#temp_invoice_classic #items table td:nth-child(2) {
  text-align: left;
}
#temp_invoice_classic #items table td:last-child {
  border-right: none !important;
}

#temp_invoice_classic #sums {
  float: right;
  margin-top: 30px;
}
#temp_invoice_classic #sums table tr th,#temp_invoice_classic #sums table tr td {
  min-width: 100px;
  padding: 10px;
  text-align: right;
  font-weight: bold;
  font-size: 14px;
}
#temp_invoice_classic #sums table tr th {
  text-align: left;
  padding-right: 25px;
  color: #707070;
}
#temp_invoice_classic #sums table tr td:last-child {
  min-width: 0 !important;
  max-width: 0 !important;
  width: 0 !important;
  padding: 0 !important;
  overflow: visible;
}
#temp_invoice_classic #sums table tr.amount-total th {
  color: black;
}
#temp_invoice_classic #sums table tr.amount-total th,#temp_invoice_classic #sums table tr.amount-total td {
  font-weight: bold;
}
#temp_invoice_classic #sums table tr.amount-total td:last-child {
  position: relative;
}
#temp_invoice_classic #sums table tr.amount-total td:last-child .currency {
  position: absolute;
  top: 3px;
  left: -740px;
  font-weight: normal;
  font-style: italic;
  font-size: 12px;
  color: #707070;
}
#temp_invoice_classic #sums table tr.amount-total td:last-child:before {
  display: block;
  content: '';
  border-top: 1px solid #444;
  position: absolute;
  top: 0;
  left: -740px;
  right: 0;
}
#temp_invoice_classic #sums table tr:last-child th,#temp_invoice_classic #sums table tr:last-child td {
  color: black;
}

#temp_invoice_classic #terms {
  margin: 100px 0 15px 0;
}
#temp_invoice_classic #terms > div {
  min-height: 70px;
}

#temp_invoice_classic .payment-info {
  color: #707070;
  font-size: 12px;
}
#temp_invoice_classic .payment-info div {
  display: inline-block;
  min-width: 10px;
}

#temp_invoice_classic .ib_drop_zone {
  color: #F8ED31 !important;
  border-color: #F8ED31 !important;
}

/**
 * If the printed invoice is not looking as expected you may tune up
 * the print styles (you can use !important to override styles)
 */
@media print {
  /* Here goes your print styles */
}

    </style>
  </head>
  <body id="temp_invoice_classic">
    <?php
  $color = '';
  if(isset($invoice_general_settings['accent_color']))
  {
    $color = $invoice_general_settings['accent_color'];
  }

  $path = '#';
  if(isset($invoice_general_settings['logo_path']) && ($invoice_general_settings['logo_path']))
  {
    $path = base_url().$invoice_general_settings['logo_path'];
  }

  $h_text = '#';
  if(isset($invoice_general_settings['header_text']))
  {
    $h_text = trim($invoice_general_settings['header_text']);
  }

  $f_text = '#';
  if(isset($invoice_general_settings['footer_text']))
  {
    $f_text = trim($invoice_general_settings['footer_text']);
  }

  $x_days = 0;
  if(isset($invoice_general_settings['due_date_x_days']))
  {
    $x_days = $invoice_general_settings['due_date_x_days'];
  }

?>
    <div id="container">
          <div class="invoice-top-1">
            <section id="memo">
              <div class="logo">
                <img src="<?php echo $path;?>" />
              </div>
              
              <div class="company-info">
                <span class="company-name">Remindoo.org</span>

                <span class="spacer"></span>
                  <div><?php echo $h_text;?></div>
                <span class="clearfix"></span>
              </div>

            </section>
            
            <section id="invoice-info">
              <div>
                <span>Issue Date</span>
                <span>Due Date</span>
                <span>End Date</span>
                <span>DAY(S) Due</span>
              </div>
              
              <div>
                <span><?php echo date("d-m-Y", $invoice_details['invoice_date']); ?></span>
                <span><?php if($invoice_details['invoice_duedate'] == 'Expired') {
                                    echo $invoice_details['invoice_duedate']; } else { echo date("d-m-Y", $invoice_details['invoice_duedate']); } ?></span>
                <span></span>
                <span></span>
              </div>

              <span class="clearfix"></span>

              <section id="invoice-title-number">
            
                <span id="title">#Invoice Number:</span>
                <span id="number"><?php echo $invoice_details['invoice_no'];?></span>
                
              </section>
            </section>
            
            <section id="client-info">
              <span>Bill To:</span>
              
              <div>
                <span><?php echo $client_email;?></span>
              </div>
            </section>

            <div class="clearfix"></div>
          </div>

          <div class="clearfix"></div>

          <div class="invoice-body">
            <section id="items">
              
              <table cellpadding="0" cellspacing="0">
              
                <tr class="invoice-header">
                 <th style="width:5%">S.No</th>
                 <th style="width:20%">Service</th>
                 <th style="width:25%">description</th>
                 <th>qty</th>
                 <th>unit price</th>
                 <th>disc%</th>
                 <th>account</th>
              <?php //if($amt_type == 'Tax Exclusive') { ?>  
               <th class="th_tax_rate tax td_tax_rate" id="th_tax_rate">tax rate</th>
               <th class="th_tax_amount" id="th_tax_amount">tax amount</th>
              <?php //} ?>
                 
                 <th style="width:15%">amount <?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></th>
                </tr>
               <?php  $sno = 0;  $m = 0; $send_status = $invoice_details['send_statements'];
                  foreach ($product_details as $key => $value) {  $m = $m + 1;  ?> 
                <tr data-iterate="item">
                  <td><?php echo ++$sno; ?></td> <!-- Don't remove this column as it's needed for the row commands -->
                  <td><?php echo $value['item'];?></td>
                  <td><?php echo $value['description'];?></td>
                  <td><?php echo $value['quantity'];?></td>
                  <td><?php echo $value['unit_price'];?></td>
                  <td><?php echo $value['discount'];?></td>
                  <td><?php echo $value['account'];?></td>
                  <td><?php echo $value['tax_rate'];?></td>
                  <td><?php echo $value['tax_amount'];?></td>
                  <td><?php echo $value['amount_gbp'];?></td>
                </tr>
                <?php } ?>  
              </table>
              
            </section>
            
            <section id="sums">
            
              <table cellpadding="0" cellspacing="0">
                <tr>
                  <th>Sub Total</th>
                  <td><strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong><?php echo $amount_details['sub_total']; ?></td>
                  <td></td>
                </tr>
                
                <!-- <tr data-iterate="tax">
                  <th>{tax_name}</th>
                  <td>{tax_value}</td>
                  <td></td>
                </tr> -->
                
                <tr class="amount-total">
                  <th>Total</th>
                  <td><strong class="euro-icon1"> <span><?php echo $currency_symbols[$admin_settings['crm_currency']]; ?></span></strong><?php echo $amount_details['grand_total']; ?></td>
                  <!-- <td>
                    <div class="currency">
                      <span>{currency_label}</span> <span>{currency}</span>
                    </div>
                  </td> -->
                </tr>
                
                <!-- You can use attribute data-hide-on-quote="true" to hide specific information on quotes.
                     For example Invoicebus doesn't need amount paid and amount due on quotes  -->
              <!--   <tr data-hide-on-quote="true">
                  <th>{amount_paid_label}</th>
                  <td>{amount_paid}</td>
                  <td></td>
                </tr>
                
                <tr data-hide-on-quote="true">
                  <th>{amount_due_label}</th>
                  <td>{amount_due}</td>
                  <td></td>
                </tr> -->
                
              </table>
              
            </section>

            <div class="clearfix"></div>
            
            <section id="terms">
            
              <span class="hidden">{terms_label}</span>
              <div><?php echo $f_text;?></div>
              
            </section>

            <!-- <div class="payment-info">
              <div>{payment_info1}</div>
              <div>{payment_info2}</div>
              <div>{payment_info3}</div>
              <div>{payment_info4}</div>
              <div>{payment_info5}</div>
            </div> -->
          </div>
            
        </div>

    <!-- <script src="http://cdn.invoicebus.com/generator/generator.min.js?data=data.js"></script> -->
  </body>
</html>
