<?php $this->load->view('includes/header');?>
<?php 

/*$invoice_ids = $this->db->query("SELECT invoice_id FROM add_new_task WHERE invoice_id!='' AND related_to = 'tasks' AND billable = 'Billable' AND FIND_IN_SET(user_id,'".$clients_user_ids."') ORDER BY id DESC")->result_array();
$task_invoice_details = array();
$subscription_invoice = array();

foreach($invoice_details as $key => $value) 
{
   foreach($invoice_ids as $key1 => $value1) 
   {
     if($value['client_id'] == $value1['invoice_id'])
     {
        $task_invoice_details[] = $invoice_details[$key];
        unset($invoice_details[$key]);
     }
   } 

   if($value['invoice_type'] == 'subscription')
   {
      $subscription_invoice[] = $invoice_details[$key];
      unset($invoice_details[$key]);
   } 
}*/

foreach($unpaid as $key => $value) 
{ 
  if($value['invoice_duedate'] == 'Expired')
  {
     unset($unpaid[$key]);
  }
}  

?>

<style>
.dropdown-content {
display: none;
position: absolute;
background-color: #fff;
min-width: 86px;
overflow: auto;
box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
z-index: 1;
left: -92px;
width: 150px;
}
tfoot {
    display: table-header-group;
}
.dt-button-collection.columnVisibility_List > button.dt-button.buttons-columnVisibility:nth-of-type(2) {
    display: none;
}

.client_name:hover {
     cursor:pointer;
     color:#007bff;
     text-decoration:none;
}

.invoice_edit:hover {
   cursor:pointer;
   color:#007bff;
   text-decoration:none;
}

</style>

<div class="modal-alertsuccess alert  succ popup_info_box" style="display:none;">
  <div class="newupdate_alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              </div>
         </div>
         </div>
   </div>

    <div class="modal-alertsuccess alert alert-danger-check succ popup_success_msg" id="alert-danger-check" style="display:none;">
      <div class="newupdate_alert">
        <a href="#" class="close">×</a>
          <div class="pop-realted1">
              <div class="position-alert1">
                   Please! Select Record...
              </div>
          </div>
      </div>
    </div>
    <div class="modal-alertsuccess alert alert-danger-check1 succ popup_success_msg " id="alert-danger-check1" style="display:none;">
      <div class="newupdate_alert">
        <a href="#" class="close">×</a>
          <div class="pop-realted1">
            <div class="position-alert1">
               Please! Select Record...
            </div>
          </div>
      </div>
    </div>

     <div class="modal-alertsuccess alert alert-danger-check1 succ popup_success_msg alert-success-delete" id="alert-danger-check1" style="display:none;">
      <div class="newupdate_alert">
        <a href="#" class="close">×</a>
          <div class="pop-realted1">
            <div class="position-alert1" id="success_message">
               Please! Select Record...
            </div>
          </div>
      </div>
    </div>

 <!--  <div class="modal-alertsuccess alert alert-success" id="delete_all_invoice" style="display:none;">
      <div class="newupdate_alert">
          <a href="#" class="close">X</a>
            <input type="hidden" name="delete_ids" id="delete_ids" value="">
              <input type="hidden" name="delete_type" id="delete_type" value="">
            <div class="pop-realted1">
              <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_yes"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
              </div>
            </div>
      </div>
  </div>   -->


   <div class="modal-alertsuccess alert alert-success" id="archive_all_invoice" style="display:none;">
      <div class="newupdate_alert">
          <a href="#" class="close">X</a>
            <input type="hidden" name="archive_ids" id="archive_ids" value="">
            <input type="hidden" name="archive_type" id="archive_type" value="">
            <div class="pop-realted1">
              <div class="position-alert1">
                  Are you sure want to archive <b><a href="#" class="archive_yes"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
              </div>
            </div>
      </div>
  </div>  

   <div class="modal-alertsuccess alert alert-success" id="unarchive_all_invoice" style="display:none;">
      <div class="newupdate_alert">
          <a href="#" class="close">X</a>
            <input type="hidden" name="archive_ids" id="unarchive_ids" value="">
            <input type="hidden" name="archive_type" id="unarchive_type" value="">
            <div class="pop-realted1">
              <div class="position-alert1">
                  Are you sure want to archive <b><a href="#" class="unarchive_yes"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
              </div>
            </div>
      </div>
  </div>  
  <!-- Import Service Popup -->
<div id="import-service" class="modal fade new-adds-categories" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <form class="form-horizontal" action="<?php echo base_url()?>invoice/importFile" method="post" name="upload_excel" enctype="multipart/form-data" id="import_form">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Import Invoice Excel</h4>
            </div>
            <div class="download-sample pdf-sample01">
               <a href="<?php echo base_url()?>uploads/csv/sample_importInvoice.csv">DOWNLOAD SAMPLE</a>
            </div>
            <div class="modal-body">           
               <div class="add-input-service">
                  <label>Invoice Excel</label>
                  <div class="custom_upload">
                 <input type="file" name="file" id="file" class="input-large" required="required">
               </div>
               </div>
            </div>
            <!-- moadl=body -->
            <div class="modal-footer">
               <a href="#" data-dismiss="modal">Cancel</a>
               <button type="submit" id="submit" name="Import" class="btn btn-primary button-loading">Upload</button>        
            </div>
         </div>
          </form>
      </div>
   </div>

<!-- management block -->
<div class="pcoded-content new-upload-03">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
         <div class="import-container-1 margin-les invoice-home-wrapper">
              <div class="deadline-crm1 floating_set invoice-align1 invoice-crm2">
                 
                  <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                    <li class="nav-item">
                      <h4> invoice 
                      </h4>
                    </li>
                  </ul>

                  
                      <div class="selectnew-invoice">
                         <div class="newinvoice-btn" >
                          <?php if($_SESSION['permission']['Invoices']['create'] == '1') { ?>
                          <div class="dropdown-primary dropdown">
                           <!-- <button class="add-newinvoice dropdown-toggle btn btn-card btn-primary" data-toggle="dropdown">+ New</span> -->
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">New</button>
                            <ul class="dropdown-menu">
                              <a href="<?php echo base_url().'invoice/NewInvoice';?>" class="dropdown-item"><i class="fa fa-sticky-note-o" aria-hidden="true"></i>Invoice</a>
                              <a href="<?php echo base_url().'invoice/RepeatInvoice';?>" class="dropdown-item"><i class="fa fa-repeat " aria-hidden="true"></i>Repeating invoice</a>
                            </ul>
                          </div>
                           <?php } ?>
                        <!-- for functionlity incompleted -->

                          <!-- <a href="javascript:;" class="statements" id="statements">Send Statements</a> -->
                          <!--<form action="<?php echo base_url()?>invoice/SendInfo" class="send_form fr-stflist" id="send_form" method="post">
                        <input type="hidden" name="clientid" class="c_id" id="c_id">  
                        <input type="hidden" name="clientemail" class="c_email" id="c_email">  
                        <input type="hidden" name="invoicedate" class="c_date" id="c_date">
                        <input type="hidden" name="invoiceno" class="c_no" id="c_no"> 
                        <input type="hidden" name="repeatclientid" class="re_cID" id="re_cID">
                        <input type="hidden" name="repeatclientemail" class="re_cEmail" id="re_cEmail"> 
                        <input type="submit" class="statements btn btn-card btn-primary" id="statements" value="Send Statements">
                  </form>

                         
                        <a href="javascript:;" class="import btn btn-card btn-primary" data-toggle="modal" data-target="#import-service">Import</a> -->
                        
                        <!-- for functionlity incompleted -->
                        <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                        <div class="csv-sample01 fr">
                        <button type="button" id="deleteInvoice_rec" class="deleteInvoice_rec del-tsk12 f-right btn btn-primary" data-id="delete" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button> 

                       <!-- <button type="button" id="archiveInvoice_rec" class="archiveInvoice_rec del-tsk12 f-right btn btn-primary" data-id="archive" style="display:none;"><i class="fa fa-archive" aria-hidden="true"></i>Archive</button> 


                        <button type="button" id="unarchiveInvoice_rec" class="unarchiveInvoice_rec del-tsk12 f-right btn btn-primary" data-id="unarchive" style="display:none;"><i class="fa fa-archive" aria-hidden="true"></i>Unarchive</button>  -->

                      <!--   <button type="button" id="deleterepInvoice_rec" class="deleterepInvoice_rec del-tsk12 f-right" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>    -->
                        </div>
                      <?php } ?>
                      </div>
                 
              </div>
            </div>
            <div class=" home_invoice-12">
            <?php  
             // echo $this->session->flashdata('invoice_success');
             // echo $this->session->flashdata('invoice_update_success');
              echo $this->session->flashdata('invoice_send_success');
              echo $this->session->flashdata('success_import');
              echo $this->session->flashdata('error_import');
              echo $this->session->flashdata('invalid_import');
             // echo $this->session->flashdata('Repeatinvoice_success');
              echo $this->session->flashdata('invoice_error_msg');
              echo $this->session->flashdata('repinvoice_error_msg');
            ?> 


              
                
               <div class="homein-12">
                <div class="right-invoice1 floating_set invoice-crm1">
             <!--  <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">  
                <li class="nav-item">
                <a class="nav-link active all-user-invoice" data-toggle="tab" href="#allusers">Invoice</a>
                </li>
                <li class="nav-item">
                <a class="nav-link all-user-repinvoice" data-toggle="tab" href="#newactive">Repeating Invoice</a>
                </li>
                <li class="nav-item">
                <a class="nav-link all-user-repinvoice" data-toggle="tab" href="#draftactive">Draft Invoice</a>
                </li>
               </ul> -->
				
				
				<div class="top-leads fr-task cfssc redesign-lead-wrapper">
        <div class="nav nav-tabs  lead-data1 pull-right leadsG leads_status_count">                     
          <div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1 archive1 all">
          <div class="lead-point1">
          <a class="nav-link active all-user-invoice" data-toggle="tab" href="#allusers">
          <strong id="archive1"><?php echo count($invoice_all);?></strong>
          <span id="23" class="status_filter">All</span>
          </a>
          </div>
          </div>    

          <div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1 archive1 task">
          <div class="lead-point1">
          <a class="nav-link active all-user-invoice" data-toggle="tab" href="#alltask">
          <strong id="archive1"><?php echo count($paid);?></strong>
          <span id="23" class="status_filter">Paid</span>
          </a>
          </div>
          </div>

          <div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1 archive1 sub">
          <div class="lead-point1">
          <a class="nav-link active all-user-invoice" data-toggle="tab" href="#allsubs">
          <strong id="archive1"><?php echo count($unpaid);?></strong>
          <span id="23" class="status_filter">Awaiting Payment</span>
          </a>
          </div>
          </div>     

          <div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1 archive1 cancel">
          <div class="lead-point1">
          <a class="nav-link active all-user-invoice" data-toggle="tab" href="#allcancel">
          <strong id="archive1"><?php echo count($cancelled);?></strong>
          <span id="23" class="status_filter">Cancelled</span>
          </a>
          </div>
          </div>  
                     
          <div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1 archive1 repeat">
          <div class="lead-point1">
          <a class="nav-link all-user-repinvoice" data-toggle="tab" href="#newactive">
          <strong id="archive1"><?php echo count($repeatinvoice_details);?></strong>
          <span id="23" class="status_filter">Repeating</span>
          </a>
          </div>
          </div>

          <div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors1 archive1 draft">
          <div class="lead-point1">
          <a class="nav-link all-user-repinvoice" data-toggle="tab" href="#draftactive">
          <strong id="archive1"><?php echo count($draft_details);?></strong>
          <span id="23" class="status_filter">Draft</span>
          </a>
          </div>
          </div>
          
        

					
                       </div>
                  </div>
			   

				
			   
              <!--  <div class="top-leads fr-task cfssc redesign-lead-wrapper pull-right">
                <div class="lead-data1 pull-right leadsG leads_status_count">
                  <div class="invoice-tabs card widget-card-1 junk-lead color-junk2 quote-colors1">
                    <div class="lead-point1">
                <?php 
                $clients_array=array();
                $im_val=0;
                if($_SESSION['role']==4)
                {
                  $get_client_ids=$this->Invoice_model->selectAllRecord('Invoice_details', 'client_email', $_SESSION['id']);
                  if(count($get_client_ids)>0)
                  {
                    foreach ($get_client_ids as $cli_key => $cli_value) {
                      array_push($clients_array, $cli_value['client_id']);
                    }
                  }
                }

                if(!empty($clients_array))
                {
                  $im_val=implode(',', $clients_array);
                } 
                 
                 ?>
                <?php $approve = $this->db->query('select count(*) as total from amount_details where payment_status="approve" and client_id in ('.$im_val.')')->row_array();
                      $decline = $this->db->query('select count(*) as total from amount_details where payment_status="decline" and client_id in ('.$im_val.') ')->row_array();
                      $decline_amount_count = $this->db->query('select sum(grand_total) as total from amount_details where payment_status="decline" and client_id in ('.$im_val.') ')->row_array();
                 ?>
                      <strong  class="b-part"><?php echo $approve['total']; ?></strong>
                      <span class="tiny-heading status_filter">approval</span>
                    </div>
                  </div>

                  <div class="invoice-tabs card widget-card-1 junk-lead color-junk2 quote-colors1">
                    <div class="lead-point1">
                      <strong  class="b-part"><?php echo $decline['total']; ?></strong>
                       <span class="tiny-heading status_filter">awaiting approval</span>
                    </div>
                  </div>
                  <div class="invoice-tabs card widget-card-1 junk-lead color-junk2 quote-colors1">
                    <div class="lead-point1">
                      <strong  class="b-part"><?php echo ($decline_amount_count['total']!='')? $decline_amount_count['total']:0; ?></strong>
                       <span class="tiny-heading status_filter">awaiting payment (<?php echo $decline['total']; ?>)</span>
                  </div>
                </div>

                  <div class="invoice-tabs card widget-card-1 junk-lead color-junk2 quote-colors1">
                    <div class="lead-point1">
                  <?php $duedate = $this->db->query('select * from Invoice_details where invoice_duedate="Expired" and client_id in ('.$im_val.')')->result_array(); ?>
            
                      <strong  class="b-part"><?php echo count($duedate); ?></strong>
                       <span class="tiny-heading status_filter">overdue</span>
                    </div>
                  </div>


                   <div class="invoice-tabs card widget-card-1 junk-lead color-junk2 quote-colors1">               
                     <div class="lead-point1">
                      <strong  class="b-part"><?php echo count($draft_details); ?></strong>
                       <span class="tiny-heading status_filter">Draft</span>
                    </div>
                  </div>
                </div>
            </div>   -->
          </div>

               <div class="tab-content invoice-tab-cont">
                  <div id="allusers" class="all-usera1  user-dashboard-section1 for_task_status button_visibility">
                 <table class="table client_table1 text-center display nowrap printableArea" id="home_table" cellspacing="0" width="100%">
                   <thead>
                     <tr class="text-uppercase">
                      <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                       <th> 
                        <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all_invoice">
                        <i></i>
                          </label> 
                     </th>
                      <?php } ?>
                     <th>Invoice Number 
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                      </th>
                       <th>To
                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                           <div class="sortMask"></div>
                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> 
                       <th>Services
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>Progress Status
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>Task Status
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>Assigned To
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                       <th>Date 
                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                           <div class="sortMask"></div>
                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                       </th>
                       <th>Due Date 
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                       </th>
                       
                        <th>Status
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th> 
                        <th>Amount</th>  
                         <th>Mail Status</th>                        
                        <th>Payment Status</th>  
                       <?php if($_SESSION['permission']['Invoices']['delete'] == '1' || $_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                       <th>Action</th>                       
                        <?php } ?>
                     </tr>
                   </thead>
                   <tfoot>
                     <tr class="text-uppercase">
                       <th></th>
                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> 
                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>                    
                       <th></th>
                     </tr>
                   </tfoot>
                   <tbody>                    
                  <?php 
                    if($invoice_all)
                    {  
                      foreach ($invoice_all as $value) { ?>
                      <tr class="client_info" id="client_info">  
                       <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                        <td>
                          <label class="custom_checkbox1">
                              
                             <input type="checkbox" class="invoice_checkbox home_table_checkbox" data-invoice-id="<?php echo $value['client_id'];?>">
                            <i></i>
                             </label>
                          
                       </td>
                       <?php } ?>
                         <td class="invoice_edit" data-href="<?php echo base_url().'/invoice/EditInvoice/'.$value['client_id'];?>"><?php echo $value['invoice_no'];?></td> 
                    <td class="client_name" data-href="<?php echo base_url().'/client/client_infomation/'.$value['client_email'];?>"> <?php 

                    // if($value['proposal_id']==0){
                     // echo $value['client_email'];
                        //  // echo  $this->Common_mdl->get_field_value('user','crm_email_id','id',$value['client_email']);
                        // $sql_cmd=$this->db->query('SELECT `crm_name`from user where id='.$value["client_email"].'')->row_array();
                        // print_r($sql_cmd);

                       $result= $this->Common_mdl->get_field_value('user','crm_name','id',$value['client_email']);

                       echo $result;
                    // }else{
                    //      echo $value['client_email'];
                    // }
                        ?> </td>  
                        <td><?php echo $value['services'];?></td>
                        <td><?php echo $value['progress_status'];?></td>
                        <td><?php echo $value['task_status'];?></td>
                        <td><?php echo $value['assigned_to'];?></td>
                        <td data-sort="<?php echo date("Ymd", $value['invoice_date']);?>"><?php echo date("M, d, Y", $value['invoice_date']);?></td> 
                        <td data-sort="<?php if($value['invoice_duedate'] == 'Expired') {
                          echo '100000000';
                        } else {
                          echo date("Ymd", $value['invoice_duedate']);  
                        } ?>"><?php if($value['invoice_duedate'] == 'Expired') {
                          echo $value['invoice_duedate'];
                        } else {
                          echo date("M, d, Y", $value['invoice_duedate']);  
                        } ?></td> 
                      
                         <td><?php echo $value['status'];?></td> 
                         <td><?php echo $value['grand_total'].' '.$firm_currency;?></td> 
                         <td><?php if($value['send_statements'] == 'send') { echo "Sent"; }else{ echo "Not Sent"; }  ?></td>
                         <!-- <td><?php //echo ucwords($value['payment_status']);?></td>  -->
                         <td>
                           <select class="payment_status_select" data-amount_details_id="<?php echo $value['amount_details_id'];?>">
                            <option value="" <?php if($value['payment_status'] == ''){ echo 'selected';}?>>Select Status</option>
                            <option value="approve" <?php if($value['payment_status'] == 'approve'){ echo 'selected';}?> >Approved</option>
                            <option value="decline" <?php if($value['payment_status'] == 'decline'){ echo 'selected';}?>>Declined</option>
                            <option value="pending" <?php if($value['payment_status'] == 'pending'){ echo 'selected';}?>>Pending</option>
                          </select>
                         </td>
                       <?php if($_SESSION['permission']['Invoices']['delete'] == '1' || $_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                        <td>
                          <p class="action_01 ticket_list"> 
                            <div class="dropdown">
                              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownLeadsMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="dropdwn-action"></span><span class="dropdwn-action"></span><span class="dropdwn-action"></span></button>
                              <div class="dropdown-menu" aria-labelledby="dropdownLeadsMenuButton">                         
                                <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                                <a href="#" onclick="return confirm_delete('<?php echo $value['client_id'];?>');"><i class="fa fa-trash fa-6 delete_invoice" id="delete_invoice" aria-hidden="true" ></i></a>
                                <?php } if($_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                                <a href="<?php echo base_url().'invoice/EditInvoice/'.$value['client_id'];?>" target="_blank" class="edit_invoice">
                                  <i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                  <a href="<?php echo base_url().'invoice/generate_invoice_pdf/'.$value['client_id'];?>" class="edit_invoice">
                                  <i class="fa fa-download fa-6" aria-hidden="true"></i></a>
                                <?php } ?>
                              </div>
                            </div>
                          </p>
                         </td> 
                         <?php } ?>
                          <input type="hidden" class="clientID" value="<?php echo $value['client_id'];?>">
                          <input type="hidden" class="clientEmail" value="<?php echo $value['client_email'];?>"> 
                          <input type="hidden" class="invoiceDate" value="<?php echo $value['invoice_date'];?>">
                          <input type="hidden" class="invoiceNo" value="<?php echo $value['invoice_no'];?>">
                      </tr>   
                     <?php  } } //else { ?> 
                      <!-- <tr class="for-norecords">
                        <td colspan="5"><h5>No records to display</h5></td>
                      </tr> -->
                    <?php //} ?>  
                   </tbody>
                 </table>
                <input type="hidden" class="rows_selected" id="select_invoice_count" > 
                 </div>

          <div id="alltask" class="tab-pane fade">
         <table class="table client_table1 text-center display nowrap printableArea" id="home_table3" cellspacing="0" width="100%">
           <thead>
             <tr class="text-uppercase">
              <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
               <th> 
                <label class="custom_checkbox1">
                  <input type="checkbox" id="select_task_invoice">
                <i></i>
                  </label> 
             </th>
              <?php } ?>
             <th>Invoice Number 
                  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                  <div class="sortMask"></div>
                  <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
              </th>
               <th>To
                   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                   <div class="sortMask"></div>
                   <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> 
               <th>Date 
                   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                   <div class="sortMask"></div>
                   <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
               </th>
               <th>Due Date 
                  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                  <div class="sortMask"></div>
                  <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
               </th>
               
                <th>Status
                 <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                  <div class="sortMask"></div>
                  <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                  </th> 
                <th>Amount</th>  
                 <th>Mail Status</th>
                <th>Payment Status</th>  
               <?php if($_SESSION['permission']['Invoices']['delete'] == '1' || $_SESSION['permission']['Invoices']['edit'] == '1') { ?>
               <th>Action</th>
                <?php } ?>
             </tr>
           </thead>
           <tfoot>
             <tr class="text-uppercase">
               <th></th>
               <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> 
               <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
               <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
               <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>                    
               <th></th>
             </tr>
           </tfoot>
           <tbody>                    
          <?php 
            if($paid)
            {  
              foreach ($paid as $value) { ?>
              <tr class="client_info" id="client_info">  
               <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                <td>
                  <label class="custom_checkbox1">
                      
                     <input type="checkbox" class="invoice_checkbox1 home_table3_checkbox" data-invoice-id="<?php echo $value['client_id'];?>">
                    <i></i>
                     </label>
                  
               </td>
               <?php } ?>
                 <td><?php echo $value['invoice_no'];?></td> 
            <td> <?php 

            // if($value['proposal_id']==0){
             // echo $value['client_email'];
                //  // echo  $this->Common_mdl->get_field_value('user','crm_email_id','id',$value['client_email']);
                // $sql_cmd=$this->db->query('SELECT `crm_name`from user where id='.$value["client_email"].'')->row_array();
                // print_r($sql_cmd);

               $result= $this->Common_mdl->get_field_value('user','crm_name','id',$value['client_email']);

               echo $result;
            // }else{
            //      echo $value['client_email'];
            // }
                ?> </td>  
                <td><?php echo date("M, d, Y", $value['invoice_date']);?></td> 
                <td><?php if($value['invoice_duedate'] == 'Expired') {
                  echo $value['invoice_duedate'];
                } else {
                  echo date("M, d, Y", $value['invoice_duedate']);  
                } ?></td> 
              
                 <td><?php echo $value['status'];?></td> 
                 <td><?php echo $value['grand_total'].' '.$firm_currency;?></td> 
                 <td><?php if($value['send_statements'] == 'send') { echo "Sent"; }else{ echo "Not Sent"; }  ?></td>
                 <!-- <td><?php //echo ucwords($value['payment_status']);?></td>  -->
                 <td>
                   <select class="payment_status_select" data-amount_details_id="<?php echo $value['amount_details_id'];?>">
                    <option value="" <?php if($value['payment_status'] == ''){ echo 'selected';}?>>Select Status</option>
                    <option value="approve" <?php if($value['payment_status'] == 'approve'){ echo 'selected';}?> >Approved</option>
                    <option value="decline" <?php if($value['payment_status'] == 'decline'){ echo 'selected';}?>>Declined</option>
                    <option value="pending" <?php if($value['payment_status'] == 'pending'){ echo 'selected';}?>>Pending</option>
                  </select>
                 </td>
               <?php if($_SESSION['permission']['Invoices']['delete'] == '1' || $_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                <td>
                  <p class="action_01 ticket_list">
                    <div class="dropdown">
                      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownLeadsMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="dropdwn-action"></span><span class="dropdwn-action"></span><span class="dropdwn-action"></span></button>
                      <div class="dropdown-menu" aria-labelledby="dropdownLeadsMenuButton">                          
                        <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                        <a href="#" onclick="return confirm_delete('<?php echo $value['client_id'];?>');"><i class="fa fa-trash fa-6 delete_invoice" id="delete_invoice" aria-hidden="true" ></i></a>
                        <?php } if($_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                        <a href="<?php echo base_url().'invoice/EditInvoice/'.$value['client_id'];?>" target="_blank" class="edit_invoice">
                          <i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                        <?php } ?>
                      </div>
                    </div>
                  </p>
                 </td> 
                 <?php } ?>
                  <input type="hidden" class="clientID" value="<?php echo $value['client_id'];?>">
                  <input type="hidden" class="clientEmail" value="<?php echo $value['client_email'];?>"> 
                  <input type="hidden" class="invoiceDate" value="<?php echo $value['invoice_date'];?>">
                  <input type="hidden" class="invoiceNo" value="<?php echo $value['invoice_no'];?>">
              </tr>   
             <?php  } } //else { ?> 
              <!-- <tr class="for-norecords">
                <td colspan="5"><h5>No records to display</h5></td>
              </tr> -->
            <?php //} ?>  
           </tbody>
         </table>
        <input type="hidden" class="rows_selected" id="select_task_count" > 
         </div>

           <div id="allsubs" class="tab-pane fade">
          <table class="table client_table1 text-center display nowrap printableArea" id="home_table4" cellspacing="0" width="100%">
            <thead>
              <tr class="text-uppercase">
               <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                <th> 
                 <label class="custom_checkbox1">
                   <input type="checkbox" id="select_sub_invoice">
                 <i></i>
                   </label> 
                </th>
               <?php } ?>
              <th>Invoice Number 
                   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                   <div class="sortMask"></div>
                   <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
               </th>
                <th>To
                    <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                    <div class="sortMask"></div>
                    <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> 
                <th>Date 
                    <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                    <div class="sortMask"></div>
                    <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                </th>
                <th>Due Date 
                   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                   <div class="sortMask"></div>
                   <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                </th>
                
                 <th>Status
                  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                   <div class="sortMask"></div>
                   <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                   </th> 
                 <th>Amount</th>  
                  <th>Mail Status</th>
                 <th>Payment Status</th>  
                <?php if($_SESSION['permission']['Invoices']['delete'] == '1' || $_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                <th>Action</th>
                 <?php } ?>
              </tr>
            </thead>
            <tfoot>
              <tr class="text-uppercase">
                <th></th>
                <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> 
                <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>                    
                <th></th>
              </tr>
            </tfoot>
            <tbody>                    
           <?php 
             if($unpaid)
             {  
               foreach ($unpaid as $value) { if($value['invoice_duedate'] != 'Expired'){ ?>
               <tr class="client_info" id="client_info">  
                <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                 <td>
                   <label class="custom_checkbox1">
                       
                      <input type="checkbox" class="invoice_checkbox2 home_table4_checkbox" data-invoice-id="<?php echo $value['client_id'];?>">
                     <i></i>
                      </label>
                   
                </td>
                <?php } ?>
                  <td><?php echo $value['invoice_no'];?></td> 
             <td> <?php 

             // if($value['proposal_id']==0){
              // echo $value['client_email'];
                 //  // echo  $this->Common_mdl->get_field_value('user','crm_email_id','id',$value['client_email']);
                 // $sql_cmd=$this->db->query('SELECT `crm_name`from user where id='.$value["client_email"].'')->row_array();
                 // print_r($sql_cmd);

                $result= $this->Common_mdl->get_field_value('user','crm_name','id',$value['client_email']);

                echo $result;
             // }else{
             //      echo $value['client_email'];
             // }
                 ?> </td>  
                 <td><?php echo date("M, d, Y", $value['invoice_date']);?></td> 
                 <td><?php //if($value['invoice_duedate'] == 'Expired') {
                  // echo $value['invoice_duedate'];
               //  } else {
                   echo date("M, d, Y", $value['invoice_duedate']);  
                // } ?></td> 
               
                  <td><?php echo $value['status'];?></td> 
                  <td><?php echo $value['grand_total'].' '.$firm_currency;?></td> 
                  <td><?php if($value['send_statements'] == 'send') { echo "Sent"; }else{ echo "Not Sent"; }   ?></td>
                  <!-- <td><?php //echo ucwords($value['payment_status']);?></td>  -->
                  <td>
                     <select class="payment_status_select" data-amount_details_id="<?php echo $value['amount_details_id'];?>">
                      <option value="" <?php if($value['payment_status'] == ''){ echo 'selected';}?>>Select Status</option>
                      <option value="approve" <?php if($value['payment_status'] == 'approve'){ echo 'selected';}?> >Approved</option>
                      <option value="decline" <?php if($value['payment_status'] == 'decline'){ echo 'selected';}?>>Declined</option>
                      <option value="pending" <?php if($value['payment_status'] == 'pending'){ echo 'selected';}?>>Pending</option>
                    </select>
                   </td>
                <?php if($_SESSION['permission']['Invoices']['delete'] == '1' || $_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                 <td>
                   <p class="action_01 ticket_list"> 
                    <div class="dropdown">
                      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownLeadsMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="dropdwn-action"></span><span class="dropdwn-action"></span><span class="dropdwn-action"></span></button>
                      <div class="dropdown-menu" aria-labelledby="dropdownLeadsMenuButton">                         
                        <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                        <a href="#" onclick="return confirm_delete('<?php echo $value['client_id'];?>');"><i class="fa fa-trash fa-6 delete_invoice" id="delete_invoice" aria-hidden="true" ></i></a>
                        <?php } if($_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                        <a href="<?php echo base_url().'invoice/EditInvoice/'.$value['client_id'];?>" target="_blank" class="edit_invoice">
                          <i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                        <?php } ?>
                      </div>
                    </div>
                   </p>
                  </td> 
                  <?php } ?>
                   <input type="hidden" class="clientID" value="<?php echo $value['client_id'];?>">
                   <input type="hidden" class="clientEmail" value="<?php echo $value['client_email'];?>"> 
                   <input type="hidden" class="invoiceDate" value="<?php echo $value['invoice_date'];?>">
                   <input type="hidden" class="invoiceNo" value="<?php echo $value['invoice_no'];?>">
               </tr>   
              <?php  } } } //else { ?> 
             <!--   <tr class="for-norecords">
                 <td colspan="5"><h5>No records to display</h5></td>
               </tr> -->
             <?php //} ?>  
            </tbody>
          </table>
         <input type="hidden" class="rows_selected" id="select_sub_count" > 
          </div>

        <div id="allcancel" class="tab-pane fade">
           <table class="table client_table1 text-center display nowrap printableArea" id="home_table5" cellspacing="0" width="100%">
             <thead>
               <tr class="text-uppercase">
                <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                 <th> 
                  <label class="custom_checkbox1">
                    <input type="checkbox" id="select_cancel_invoice">
                  <i></i>
                    </label> 
               </th>
                <?php } ?>
               <th>Invoice Number 
                    <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                    <div class="sortMask"></div>
                    <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                </th>
                 <th>To
                     <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                     <div class="sortMask"></div>
                     <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> 
                 <th>Date 
                     <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                     <div class="sortMask"></div>
                     <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                 </th>
                 <th>Due Date 
                    <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                    <div class="sortMask"></div>
                    <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                 </th>
                 
                  <th>Status
                   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                    <div class="sortMask"></div>
                    <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                    </th> 
                  <th>Amount</th>  
                   <th>Mail Status</th>
                  <th>Payment Status</th>  
                 <?php if($_SESSION['permission']['Invoices']['delete'] == '1' || $_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                 <th>Action</th>
                  <?php } ?>
               </tr>
             </thead>
             <tfoot>
               <tr class="text-uppercase">
                 <th></th>
                 <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> 
                 <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                 <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                 <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>                    
                 <th></th>
               </tr>
             </tfoot>
             <tbody>                    
            <?php 
              if($cancelled)
              {  
                foreach ($cancelled as $value) { ?>
                <tr class="client_info" id="client_info">  
                 <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                  <td>
                    <label class="custom_checkbox1">
                        
                       <input type="checkbox" class="invoice_checkbox3 home_table5_checkbox" data-invoice-id="<?php echo $value['client_id'];?>">
                      <i></i>
                       </label>
                    
                 </td>
                 <?php } ?>
                   <td><?php echo $value['invoice_no'];?></td> 
              <td> <?php 

              // if($value['proposal_id']==0){
               // echo $value['client_email'];
                  //  // echo  $this->Common_mdl->get_field_value('user','crm_email_id','id',$value['client_email']);
                  // $sql_cmd=$this->db->query('SELECT `crm_name`from user where id='.$value["client_email"].'')->row_array();
                  // print_r($sql_cmd);

                 $result= $this->Common_mdl->get_field_value('user','crm_name','id',$value['client_email']);

                 echo $result;
              // }else{
              //      echo $value['client_email'];
              // }
                  ?> </td>  
                  <td><?php echo date("M, d, Y", $value['invoice_date']);?></td> 
                  <td><?php if($value['invoice_duedate'] == 'Expired') {
                    echo $value['invoice_duedate'];
                  } else {
                    echo date("M, d, Y", $value['invoice_duedate']);  
                  } ?></td> 
                
                   <td><?php echo $value['status'];?></td> 
                   <td><?php echo $value['grand_total'].' '.$firm_currency;?></td> 
                   <td><?php if($value['send_statements'] == 'send') { echo "Sent"; }else{ echo "Not Sent"; }  ?></td>
                   <!-- <td><?php //echo ucwords($value['payment_status']);?></td>  -->
                   <td>
                     <select class="payment_status_select" data-amount_details_id="<?php echo $value['amount_details_id'];?>">
                      <option value="" <?php if($value['payment_status'] == ''){ echo 'selected';}?>>Select Status</option>
                      <option value="approve" <?php if($value['payment_status'] == 'approve'){ echo 'selected';}?> >Approved</option>
                      <option value="decline" <?php if($value['payment_status'] == 'decline'){ echo 'selected';}?>>Declined</option>
                      <option value="pending" <?php if($value['payment_status'] == 'pending'){ echo 'selected';}?>>Pending</option>
                    </select>
                   </td>
                 <?php if($_SESSION['permission']['Invoices']['delete'] == '1' || $_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                  <td>
                    <p class="action_01 ticket_list">  
                      <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownLeadsMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="dropdwn-action"></span><span class="dropdwn-action"></span><span class="dropdwn-action"></span></button>
                        <div class="dropdown-menu" aria-labelledby="dropdownLeadsMenuButton">                        
                          <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                          <a href="#" onclick="return confirm_delete('<?php echo $value['client_id'];?>');"><i class="fa fa-trash fa-6 delete_invoice" id="delete_invoice" aria-hidden="true" ></i></a>
                          <?php } if($_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                          <a href="<?php echo base_url().'invoice/EditInvoice/'.$value['client_id'];?>" target="_blank" class="edit_invoice">
                            <i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                          <?php } ?>
                        </div>
                      </div>
                    </p>
                   </td> 
                   <?php } ?>
                    <input type="hidden" class="clientID" value="<?php echo $value['client_id'];?>">
                    <input type="hidden" class="clientEmail" value="<?php echo $value['client_email'];?>"> 
                    <input type="hidden" class="invoiceDate" value="<?php echo $value['invoice_date'];?>">
                    <input type="hidden" class="invoiceNo" value="<?php echo $value['invoice_no'];?>">
                </tr>   
               <?php  } } //else { ?> 
                <!-- <tr class="for-norecords">
                  <td colspan="5"><h5>No records to display</h5></td>
                </tr> -->
              <?php //} ?>  
             </tbody>
           </table>
          <input type="hidden" class="rows_selected" id="select_cancel_count" > 
           </div>

        <div id="newactive" class="tab-pane fade ">
            <table class="table client_table1 text-center display nowrap printableArea" id="home_table1" cellspacing="0" width="100%">
                   <thead>
                     <tr class="text-uppercase">
                       <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                       <th>
                        <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all_repinvoice">
                          <i></i>
                          </label>
                       </th>
                       <?php } ?>
                    <th>Invoice Number 
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                         <div class="sortMask"></div>
                         <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                       </th>

                       <th>To
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>

                        <th>Services
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>Progress Status
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>Task Status
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>Assigned To
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                     <!--   <th>Reference 
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                         <div class="sortMask"></div>
                         <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                       </th> -->
                      
                       <th>Status 
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                         <div class="sortMask"></div>
                         <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                       </th>
                       <th>Payment Type 
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                         <div class="sortMask"></div>
                         <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                       </th> 
                       <th>Amount</th>       
                       <th>Mail Status</th>            
                       <th>Payment Status</th>
                       <?php if($_SESSION['permission']['Invoices']['delete'] == '1' || $_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                       <th>Action</th>
                        <?php } ?>
                     </tr>
                   </thead>

                    <tfoot>
                     <tr class="text-uppercase">
                       <th></th>
                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                      <!--  <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> -->
                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                       <th></th>
                     </tr>
                   </tfoot>
                   <tbody>
                    
                  <?php 
                //  print_r($repeatinvoice_details);
                    if($repeatinvoice_details)
                    {  
                      foreach ($repeatinvoice_details as $key => $repeatVal) { ?>
                      <tr class="client_info1" id="client_info1">  
                        <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                        <td>
                          <label class="custom_checkbox1">
                             <input type="checkbox" class="repinvoice_checkbox home_table1_checkbox" id="repinvoice_checkbox" data-invoice-id="<?php echo $repeatVal['client_id'];?>">
                             <i></i>
                             </label>
                       </td>
                        <?php } ?>
                         <td><?php echo $repeatVal['reference'];?></td>
                        <td>

                        <?php // echo 
                        $result= $this->Common_mdl->get_field_value('user','crm_name','id',$repeatVal['client_email']);

                       echo $result;
                        ?></td>                      
                        
                        <td><?php echo $repeatVal['status'];?></td> 
                        <td><?php echo $repeatVal['amounts_type'];?></td>    
                        <td><?php echo $repeatVal['grand_total'].' '.$firm_currency; ?></td>
                        <td><?php if($value['send_statements'] == 'send') { echo "Sent"; }else{ echo "Not Sent"; } ?></td>
                        <td><?php echo ucwords($repeatVal['approve']);?></td>
                        <?php if($_SESSION['permission']['Invoices']['delete'] == '1' || $_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                        <td>
                          <p class="action_01 ticket_list">
                            <div class="dropdown">
                              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownLeadsMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="dropdwn-action"></span><span class="dropdwn-action"></span><span class="dropdwn-action"></span></button>
                              <div class="dropdown-menu" aria-labelledby="dropdownLeadsMenuButton">
                                <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                                  <a href="#" onclick="return confirm1_delete('<?php echo $repeatVal['client_id'];?>');"><i class="fa fa-trash fa-6 delete_invoice" id="delete_invoice" aria-hidden="true" ></i></a>
                                  <?php } if($_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                                  <a href="<?php echo base_url().'invoice/EditRepeatInvoice/'.$repeatVal['client_id'];?>" target="_blank" class="edit_invoice"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                  <?php } ?>
                              </div>
                            </div>
                          </p>
                         </td> 
                         <?php } ?>
                        <input type="hidden" class="repeat_clientID" value="<?php echo $repeatVal['client_id'];?>">
                        <input type="hidden" class="repeat_clientEmail" value="<?php echo $repeatVal['client_email'];?>">
                      </tr>   
                  <?php  } } //else { ?> 
                     <!--  <tr class="for-norecords">
                        <td colspan="5"><h5>No records to display</h5></td>
                      </tr> -->
                  <?php //} ?> 
                   </tbody>
                 </table>
                 <input type="hidden" class="rows_selected" id="select_repinvoice_count" > 
        </div>
        <div id="draftactive" class="tab-pane fade">

                 <table class="table client_table1 text-center display nowrap printableArea" id="home_table2" cellspacing="0" width="100%">
                   <thead>
                     <tr class="text-uppercase">
                        <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                        <th>
                          <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all_draft">
                          <i></i>
                          </label>
                         </th> 
                       <?php } ?>
                       <th>To
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                         <div class="sortMask"></div>
                         <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                       </th>
                       <th>Date 
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                         <div class="sortMask"></div>
                         <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                       </th>
                       <th>Proposal Name 
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                          <div class="sortMask"></div>
                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                       </th>
                       <th>Amount</th>
                       <th>Mail Status</th>
                       <th>Status 
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                         <div class="sortMask"></div>
                         <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                       </th>                     
                       <?php if($_SESSION['permission']['Invoices']['delete'] == '1' || $_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                       <th>Action</th>
                        <?php } ?>
                     </tr>
                   </thead>

                    <tfoot>
                     <tr class="text-uppercase">
                      <th>                      
                     </th>
                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                       <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>                     
                       <th></th>
                     </tr>
                   </tfoot>
                   <tbody>
                    
                  <?php 
                    if($draft_details)
                    {  
                      foreach ($draft_details as $value) { 
                        $randomstring=generateRandomString('100');

                        ?>
                      <tr class="client_info" id="client_info">  
                     <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                      <td>
                        <label class="custom_checkbox1">
                        <input type="checkbox" class="draft_checkbox home_table2_checkbox" data-invoice-id="<?php echo $value['id'];?>">
                        <i></i>
                        </label>
                      </td>
                      <?php } ?> 
                      <td><?php echo $this->Common_mdl->get_price('proposals','id',$value['proposal_id'],'receiver_mail_id'); ?></td> 
                      <td><?php echo date("M, d, Y", strtotime($value['date']));?></td> 
                      <td><?php  $proposals=$this->Common_mdl->select_record('proposals','id',$value['proposal_id']);
                      echo $proposals['proposal_name']; ?></td> 
                      <td><?php echo $this->Common_mdl->get_price('proposals','id',$value['proposal_id'],'grand_total').' '.$firm_currency; ?></td>
                      <td>Draft</td>
                      <td><?php echo ucfirst($value['status']);?></td>  
                      <?php if($_SESSION['permission']['Invoices']['delete'] == '1' || $_SESSION['permission']['Invoices']['edit'] == '1') { ?>                     
                      <td>
                      <p class="action_01 ticket_list">
                        <div class="dropdown">
                          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownLeadsMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="dropdwn-action"></span><span class="dropdwn-action"></span><span class="dropdwn-action"></span></button>
                          <div class="dropdown-menu" aria-labelledby="dropdownLeadsMenuButton">
                            <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                            <a href="#" onclick="return confirm_draftdelete('<?php echo $value['id'];?>');"><i class="fa fa-trash fa-6 delete_invoice" id="delete_invoice" aria-hidden="true" ></i></a>
                            <?php } if($_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                            <a href="<?php echo base_url().'invoice/ProposalInvoice/'.$randomstring.'---'.$value['proposal_id'];?>" target="_blank" class="edit_invoice"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                            <?php } ?>
                          </div>
                        </div>
                      </p>
                      </td> 
                      <?php } ?>
                      </tr>   
                    <?php  } } //else { ?> 
                      <!-- <tr class="for-norecords">
                        <td colspan="5"><h5>No records to display</h5></td>
                      </tr> -->
                  <?php //} ?>  
                   </tbody>
                 </table>
                <input type="hidden" class="rows_selected" id="select_draft_count" > 
                 </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="deleteconfirmation" role="dialog">
    <div class="modal-dialog modal-delete-sub-task">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input type="hidden"  id="delete_ids" value="">
        <input type="hidden"  id="delete_type" value="">

          <p> Are you sure you want to delete ?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" onclick="delete_action()" >Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>

    </div>
  </div>

  <div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
    <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
      <div class="pop-realted1">
        <div class="position-alert1">
          Please! Select Record...
        </div>
      </div>
    </div>
  </div>

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<script src="<?php echo base_url() ?>assets/js/task/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/user_page/materialize.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/task/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/task/dataTables.buttons.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/custom/buttons.colVis.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">

<script>
var home_table;

   $( document ).ready(function() {   
       var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();   
       // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#1abc9c',
               jackColor: '#fff',
               size: 'small'
           });
       });
   
       $('#accordion_close').on('click', function(){
               $('#accordion').slideToggle(300);
               $(this).toggleClass('accordion_down');
       });
   });
</script>
<script type="text/javascript">


function confirm_delete(id)
{
$("#delete_ids").val(id);
$("#delete_type").val('invoice');
$("#deleteconfirmation").modal('show');
return false;
}


function confirm_draftdelete(id)
{
  $("#delete_ids").val(id);
  $("#delete_type").val('draft');
$("#deleteconfirmation").modal('show');
  return false;
}

function confirm1_delete(id)
{
  $("#delete_ids").val(id);
  $("#delete_type").val('repinvoice');
$("#deleteconfirmation").modal('show');
  return false;
}

function delete_action()
{

     var selected_invoice_values = $("#delete_ids").val();
     var type=$("#delete_type").val();
     var formData={ 'data_ids':selected_invoice_values,'type':type};

     $.ajax({
         type: "POST",
         url: "<?php echo base_url().'invoice/invoiceDelete_all';?>",
         cache: false,
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
           $("#deleteconfirmation").modal("hide");
         },
         success: function(data) {
           $(".LoadingImage").hide();
           /*var invoice_ids = data.split(",");
           for (var i=0; i < invoice_ids.length; i++ ) { 
           $("#"+invoice_ids[i]).remove(); } */
           
           if(parseInt(data))
           {
            $(".popup_info_box .position-alert1").html("Invoice Deleted Successfully !!");
            $(".popup_info_box").show();
           }

           setTimeout(function() { 
           location.reload(); }, 500);     
         }
       });    
}



$(document).on('click','.close',function(e)
   {
   $(this).parents('.alert-success').hide();
   return false;
   });

  function AddClass_SettingPopup() {
    $('button.Button_List').closest('.dt-button-collection').addClass('Settings_List');
    $('button.buttons-columnVisibility').closest('.dt-button-collection').addClass('columnVisibility_List');
  }

  $(document).on('click', '.Settings_Button', AddClass_SettingPopup);
    $(document).on('click', '.Button_List', AddClass_SettingPopup);

  $(document).on('click', '.dt-button.close', function() {
    $('.dt-buttons').find('.dt-button-collection').detach();
    $('.dt-buttons').find('.dt-button-background').detach();
  });

</script>  
<style type="text/css">
tr.highlighted td {
    background: #6699ff;    
}
tr.highlighted1 td {
    background: #6699ff;
}
div .dt-button-collection {
    left: -117px !important;
    max-height: 1000px;
}
button .active {
  color:#989898;
}
.buttons-collection:hover
{
  border-radius: 50% !important;
  color: black !important;
  border: 1px solid black !important;
}
</style>

<script type="text/javascript">

var invoice=[]; 
  $(document).on('change','#select_all_invoice',function(event) {  //on click 
  var checked = this.checked;
  var table_rows = $('#home_table tr');
  $(table_rows).each(function(index, value) {
    if (checked) {
        $(this).find('.invoice_checkbox').prop('checked', true);
        invoice.push($(this).find('.invoice_checkbox').data('invoice-id'));   
        $("#deleteInvoice_rec").show();
        $("#archiveInvoice_rec").show();
        $("#unarchiveInvoice_rec").show();
     }else{
        $(this).find('.invoice_checkbox').prop('checked', false);
         $("#deleteInvoice_rec").hide();
         $("#archiveInvoice_rec").hide();
         $("#unarchiveInvoice_rec").hide();
     } 
  });
    // home_table.column(0).nodes().to$().each(function(index) {    
    //  if (checked) {
    //     $(this).find('.invoice_checkbox').prop('checked', true);
    //     invoice.push($(this).find('.invoice_checkbox').data('invoice-id'));   
    //     $("#deleteInvoice_rec").show();
    //     $("#archiveInvoice_rec").show();
    //     $("#unarchiveInvoice_rec").show();
    //  }else{
    //     $(this).find('.invoice_checkbox').prop('checked', false);
    //      $("#deleteInvoice_rec").hide();
    //      $("#archiveInvoice_rec").hide();
    //      $("#unarchiveInvoice_rec").hide();
    //  } 
    // });  
     $("#select_invoice_count").val($("input.invoice_checkbox:checked").length+" Selected");
  });

  var task_invoice=[]; 
    $(document).on('change','#select_task_invoice',function(event) {  //on click 
    var checked = this.checked;
      home_table3.column(0).nodes().to$().each(function(index) {    
       if (checked) {
          $(this).find('.invoice_checkbox1').prop('checked', 'checked');
          task_invoice.push($(this).find('.invoice_checkbox1').data('invoice-id'));   
          $("#deleteInvoice_rec").show();
          $("#archiveInvoice_rec").show();
          $("#unarchiveInvoice_rec").show();
       }else{
          $(this).find('.invoice_checkbox1').prop('checked', false);
           $("#deleteInvoice_rec").hide();
           $("#archiveInvoice_rec").hide();
           $("#unarchiveInvoice_rec").hide();
       } 
      });  
       $("#select_task_count").val($("input.invoice_checkbox1:checked").length+" Selected");
    });

    $(document).on('click','.client_name',function(event) {  
      var link = $(this).attr('data-href');
      var win = window.open(link, "_blank");
      win.focus();
    });

    $(document).on('click','.invoice_edit',function(event) {  
      var link = $(this).attr('data-href');
      var win = window.open(link, "_blank");
      win.focus();
    });

  var subinvoice=[]; 
    $(document).on('change','#select_sub_invoice',function(event) {  //on click 
    var checked = this.checked;
      home_table4.column(0).nodes().to$().each(function(index) {    
       if (checked) {
          $(this).find('.invoice_checkbox2').prop('checked', 'checked');
          subinvoice.push($(this).find('.invoice_checkbox2').data('invoice-id'));   
          $("#deleteInvoice_rec").show();
          $("#archiveInvoice_rec").show();
          $("#unarchiveInvoice_rec").show();
       }else{
          $(this).find('.invoice_checkbox2').prop('checked', false);
           $("#deleteInvoice_rec").hide();
           $("#archiveInvoice_rec").hide();
           $("#unarchiveInvoice_rec").hide();
       } 
      });  
       $("#select_sub_count").val($("input.invoice_checkbox2:checked").length+" Selected");
    });

    var cancelinvoice=[]; 
      $(document).on('change','#select_cancel_invoice',function(event) {  //on click 
      var checked = this.checked;
        home_table5.column(0).nodes().to$().each(function(index) {    
         if (checked) {
            $(this).find('.invoice_checkbox3').prop('checked', 'checked');
            cancelinvoice.push($(this).find('.invoice_checkbox3').data('invoice-id'));   
            $("#deleteInvoice_rec").show();
            $("#archiveInvoice_rec").show();
            $("#unarchiveInvoice_rec").show();
         }else{
            $(this).find('.invoice_checkbox3').prop('checked', false);
             $("#deleteInvoice_rec").hide();
             $("#archiveInvoice_rec").hide();
             $("#unarchiveInvoice_rec").hide();
         } 
        });  
         $("#select_cancel_count").val($("input.invoice_checkbox3:checked").length+" Selected");
      });


  var repinvoice=[];
  $(document).on('change','#select_all_repinvoice',function(event) {  //on click 
  var checked = this.checked;
    home_table1.column(0).nodes().to$().each(function(index) {    
     if (checked) {  
      console.log($(this).find('.repinvoice_checkbox').attr('class'));
        $(this).find('.repinvoice_checkbox').prop('checked', 'checked');
        repinvoice.push($(this).find('.repinvoice_checkbox').data('invoice-id'));   
        $("#deleteInvoice_rec").show();
        $("#archiveInvoice_rec").show();
        $("#unarchiveInvoice_rec").show();
     }else{
         $(this).find('.repinvoice_checkbox').prop('checked', false);
         $("#deleteInvoice_rec").hide();
         $("#archiveInvoice_rec").hide();
         $("#unarchiveInvoice_rec").hide();
     } 
    });  
      $("#select_repinvoice_count").val($("input.repinvoice_checkbox:checked").length+" Selected");
  });

  var draftinvoice=[];
  $(document).on('change','#select_all_draft',function(event) {  //on click 
  var checked = this.checked;
    home_table2.column(0).nodes().to$().each(function(index) {    
     if (checked) {
        $(this).find('.draft_checkbox').prop('checked', 'checked');
        draftinvoice.push($(this).find('.draft_checkbox').data('invoice-id'));   
        $("#deleteInvoice_rec").show();
        $("#archiveInvoice_rec").show();
        $("#unarchiveInvoice_rec").show();
     }else{
        $(this).find('.draft_checkbox').prop('checked', false);
         $("#deleteInvoice_rec").hide();
         $("#archiveInvoice_rec").hide();
         $("#unarchiveInvoice_rec").hide();
     } 
    });  
    // $("#select_invoice_count").val($("input.draft_checkbox:checked").length+" Selected");
  });

  $(document).on('click','.invoice_checkbox', function() {
      if($(this).is(':checked', true)) {        
         $("#deleteInvoice_rec").show();    
         $("#archiveInvoice_rec").show(); 
         $("#unarchiveInvoice_rec").show(); 
      }else{
          $("#select_all_invoice").prop('checked',false);
          if($("input.invoice_checkbox:checked").length == 0){
                 $("#deleteInvoice_rec").hide();   
                 $("#archiveInvoice_rec").hide();   
                 $("#unarchiveInvoice_rec").hide();         
          }
      }
      
      $("#select_invoice_count").val($("input.invoice_checkbox:checked").length+" Selected");

    });  

   $(document).on('click','.invoice_checkbox1', function() {
      if($(this).is(':checked', true)) {        
         $("#deleteInvoice_rec").show();    
         $("#archiveInvoice_rec").show(); 
         $("#unarchiveInvoice_rec").show(); 
      }else{
          $("#select_task_invoice").prop('checked',false);
          if($("input.invoice_checkbox1:checked").length == 0){
                 $("#deleteInvoice_rec").hide();   
                 $("#archiveInvoice_rec").hide();   
                 $("#unarchiveInvoice_rec").hide();         
          }
      }
       $("#select_task_count").val($("input.invoice_checkbox1:checked").length+" Selected");
    }); 

   $(document).on('click','.invoice_checkbox2', function() {
      if($(this).is(':checked', true)) {        
         $("#deleteInvoice_rec").show();    
         $("#archiveInvoice_rec").show(); 
         $("#unarchiveInvoice_rec").show(); 
      }else{
          $("#select_sub_invoice").prop('checked',false);
          if($("input.invoice_checkbox2:checked").length == 0){
                 $("#deleteInvoice_rec").hide();   
                 $("#archiveInvoice_rec").hide();   
                 $("#unarchiveInvoice_rec").hide();         
          }
      }
       $("#select_sub_count").val($("input.invoice_checkbox2:checked").length+" Selected");
    }); 

   $(document).on('click','.invoice_checkbox3', function() {
       if($(this).is(':checked', true)) {        
          $("#deleteInvoice_rec").show();    
          $("#archiveInvoice_rec").show(); 
          $("#unarchiveInvoice_rec").show(); 
       }else{
           $("#select_cancel_invoice").prop('checked',false);
           if($("input.invoice_checkbox3:checked").length == 0){
                  $("#deleteInvoice_rec").hide();   
                  $("#archiveInvoice_rec").hide();   
                  $("#unarchiveInvoice_rec").hide();         
           }
       }
       
       $("#select_cancel_count").val($("input.invoice_checkbox3:checked").length+" Selected");

     }); 

    $(document).on('click','.repinvoice_checkbox', function() {
      if($(this).is(':checked', true)) {        
         $("#deleteInvoice_rec").show();   
         $("#archiveInvoice_rec").show();
         $("#unarchiveInvoice_rec").show();
      }else{
          $("#select_all_repinvoice").prop('checked',false);
          if($("input.repinvoice_checkbox:checked").length == 0){
                 $("#deleteInvoice_rec").hide();   
                 $("#archiveInvoice_rec").hide();  
                 $("#unarchiveInvoice_rec").hide();             
          }
      }
       $("#select_repinvoice_count").val($("input.proposal_checkbox:checked").length+" Selected");
    });

    $(document).on('click','.draft_checkbox', function() {
      if($(this).is(':checked', true)) {        
         $("#deleteInvoice_rec").show();   
         $("#archiveInvoice_rec").show();   
         $("#unarchiveInvoice_rec").show();     
      }else{
          $("#select_all_draft").prop('checked',false);
          if($("input.draft_checkbox:checked").length == 0){
                 $("#deleteInvoice_rec").hide();   
                 $("#archiveInvoice_rec").hide();       
                 $("#unarchiveInvoice_rec").hide();             
          }
      }
    });  

$(document).on('change','.payment_status_select', function(e) {
    e.preventDefault();
    var obj = $(this);
    var amount_details_id = obj.attr('data-amount_details_id');
    var payment_status = obj.val();
    var data={ 'amount_details_id':amount_details_id,'payment_status':payment_status};
    $.ajax({
        type: "POST",
        url: "<?php echo base_url().'invoice/change_payment_status';?>",
        cache: false,
        data: data,
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          $(".LoadingImage").hide();
          if(data == 1)
          {
            var msg = 'Success !!! Payment Status Changed successfully...';
          }
          else
          {
            var msg = 'Server error! Please Try Again...';
            obj.val(payment_status);
          }
          $(".popup_info_msg").show();
          $(".popup_info_msg .position-alert1").html(msg);
          //    console.log(data);
          setTimeout(function() {
            $(".popup_info_msg").hide();
          }, 2000);     
        }
      });

});


$(document).ready(function() {

$(".status_filter_new").click(function()
{
  var id = $(this).find('a').attr('href');
  getTab(id);

  $("#select_all_repinvoice").prop("checked",false);
  $("#select_all_draft").prop("checked",false);
  $("#select_all_invoice").prop("checked",false);
  $("#select_task_invoice").prop("checked",false);
  $("#select_sub_invoice").prop("checked",false);
  $("#select_cancel_invoice").prop("checked",false);

  home_table.column(0).nodes().to$().each(function(index) {
    $(this).find("input[type=checkbox]").prop("checked",false);
  });

  home_table1.column(0).nodes().to$().each(function(index) {
    $(this).find("input[type=checkbox]").prop("checked",false);
  });

  home_table2.column(0).nodes().to$().each(function(index) {
    $(this).find("input[type=checkbox]").prop("checked",false);
  });

  home_table3.column(0).nodes().to$().each(function(index) {
    $(this).find("input[type=checkbox]").prop("checked",false);
  });

  home_table4.column(0).nodes().to$().each(function(index) {
    $(this).find("input[type=checkbox]").prop("checked",false);
  });

  home_table5.column(0).nodes().to$().each(function(index) {
    $(this).find("input[type=checkbox]").prop("checked",false);
  });

  $("#deleteInvoice_rec").hide();
});

});


$(document).on('click', '#deleteInvoice_rec, #archiveInvoice_rec, #unarchiveInvoice_rec', function() { 
 // alert("iside");
   var option=$(this).data('id');
   var ref = $(".lead-point1").find(".nav-link.active").attr('href');
   //alert(ref);
   //var href = ref.substring(ref.indexOf("#") + 1); 
   var table_id=$(ref).find('table').attr('id'); 
   var table_obj;

  if(table_id=="home_table")
  {
    table_obj=home_table;
    type='invoice';
  }
  else if (table_id=="home_table1")
  {
    table_obj=home_table1;
    type='repinvoice';
  }
  else if(table_id=="home_table2")
  {
    table_obj=home_table2;
    type='draft';
  }
  else if(table_id=="home_table3")
  {
      table_obj=home_table3;
      type='invoice';
  }
  else if(table_id=="home_table4")
  {
      table_obj=home_table4;
      type='invoice';
  }
  else if(table_id=="home_table5")
  {
      table_obj=home_table5;
      type='invoice';
  }


 // alert(type);
  var invoice = [];
   table_obj.column(0).nodes().to$().each(function(index) {
      if($(this).find("."+table_id+"_checkbox").is(":checked"))
      {
        //console.log($(this).find("."+table_id+"_checkbox").attr('class'));
        invoice.push($(this).find("."+table_id+"_checkbox").data('invoice-id'));
      }
    });
   invoice=invoice.join();  
   //alert(invoice);
    //console.log(invoice);
  /*if(invoice.length <=0) {
     $('.alert-danger-check').show();
  } else { }*/
    if(option=='delete'){
        $('#deleteconfirmation').modal('show');
        $("#delete_ids").val(invoice);
        $("#delete_type").val(type);
    }else if(option=='archive'){
         $('#archive_all_invoice').show();
        $("#archive_ids").val(invoice);
        $("#archive_type").val(type);
    }else{
         $('#unarchive_all_invoice').show();
        $("#unarchive_ids").val(invoice);
        $("#unarchive_type").val(type);
    }    
 
});

    


      $(document).on('click','.archive_yes',function() {
       var selected_invoice_values = $("#archive_ids").val();
       var type=$("#archive_type").val();
       var formData={ 'data_ids':selected_invoice_values,'type':type};
      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'invoice/invoiceArchive_all';?>",
        cache: false,
        data: formData,
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          $(".LoadingImage").hide();
          var invoice_ids = data.split(",");
          for (var i=0; i < invoice_ids.length; i++ ) { 
          $("#"+invoice_ids[i]).remove(); } 
          $("#success_message").html("Invoice Archived Successfully !!");
          $(".alert-success-delete").show();
          setTimeout(function() { 
          location.reload(); }, 500);     
        }
      });
    });


     $(document).on('click','.unarchive_yes',function() {
       var selected_invoice_values = $("#unarchive_ids").val();
       var type=$("#unarchive_type").val();
       var formData={ 'data_ids':selected_invoice_values,'type':type};
      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'invoice/invoiceunArchive_all';?>",
        cache: false,
        data: formData,
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          $(".LoadingImage").hide();
          var invoice_ids = data.split(",");
          for (var i=0; i < invoice_ids.length; i++ ) { 
          $("#"+invoice_ids[i]).remove(); } 
          $("#success_message").html("Invoice Archived Successfully !!");
          $(".alert-success-delete").show();
          setTimeout(function() { 
          location.reload(); }, 500);     
        }
      });
    });


/* $(document).ready(function() {
  $('a.all-user-invoice').on('click', function() {
    $(".deleterepInvoice_rec").hide();
  })
  $('a.all-user-repinvoice').on('click', function() {
   $(".deleteInvoice_rec").hide();
  })
});*/


  $(document).ready(function(){
    var pageLength = "<?php echo get_firm_page_length() ?>";
    var check=0;
    var check1=0;
    var numCols = $('#home_table thead th').length;   
    //alert(numCols);
       home_table = $('#home_table').DataTable({
        // "pageLength": pageLength,
        "pageLength": 25,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
        // "dom": '<"top"fl>rt<"bottom"ip><"clear">',
          fixedColumns:   {
         //   leftColumns: 1,
            rightColumns: 1
        },
        "dom": '<"toolbar-table" B>lfrtip',
          stateSave: true,
          "stateDuration": 60 * 60 * 3600,
      buttons: [{
        extend: 'collection',
        background: false,
        text: '<i class="fa fa-cog" aria-hidden="true"></i>',
        className: 'Settings_Button',
        buttons: [
          {
            extend: 'colvis',
            text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',
            columns: ':not(.Exc-colvis)',
            className: 'Button_List',
            prefixButtons: [{
              text: 'X',
              className: 'close'
            }]
          }
        ]
      }],
       initComplete: function () { 
                  var q=1;
                     $('#home_table thead th').find('.filter_check').each( function(){
                     // alert('ok');
                      $(this).attr( 'searchable' , true );
                       $(this).attr('id',"home_"+q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){ 
                          var select = $("#home_"+i);    
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 
                     $("#home_"+i).formSelect();  
                  }

                      

        }
    });
      for(j=1;j<numCols;j++){  
          $('#home_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#home_'+c+ ' option:selected'), function(){                
                  search.push($(this).val());
              });      
              search = search.join('|'); 
              home_table.column(c).search(search, true, false).draw();  
          });
       }

   });

$(document).on('click','.DT', function (e) {
        if (!$(e.target).hasClass('sortMask')) {      
            e.stopImmediatePropagation();
        }
    });




//  $(document).on('click', 'th .themicond', function(){
//       if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
//          $(this).parent().find('.dropdown-content').removeClass('Show_content');
//       }else{
//          $('.dropdown-content').removeClass('Show_content');
//          $(this).parent().find('.dropdown-content').addClass('Show_content');
//       }
//           $(this).parent().find('.select-wrapper').toggleClass('special');
//       if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){ 
//       }else{
//           $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
//           $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
//       }
// });
  

  $(document).ready(function(){

  var id  = '<?php echo $_GET['type']; ?>';
   
   if(id == "1")
   {
      $('.task').trigger('click');      
   }
   else if(id == "2")
   {
      $('.sub').trigger('click');      
   }
   else if(id == "3")
   {
      $('.cancel').trigger('click');      
   }

   $('th .themicond').on('click', function(e) {
      if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
      $(this).parent().find('.dropdown-content').removeClass('Show_content');
      }else{
       $('.dropdown-content').removeClass('Show_content');
       $(this).parent().find('.dropdown-content').addClass('Show_content');
      }
      $(this).parent().find('.select-wrapper').toggleClass('special');
          if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
      }else{
        $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
        $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );

      }
  });

   $("th").on("click.DT", function (e) {      
        if (!$(e.target).hasClass('sortMask')) {       
            e.stopImmediatePropagation();
        }
    });

  });

  function getTab(id)
  { 
      var active_tab = $('div .invoice-tab-cont .active');
      $(active_tab).removeClass('active');
      $(active_tab).removeClass('show');

      $('div '+id).addClass('active show');
      $('div '+id).attr('aria-expanded',true);      
  }



</script>

</body>
</html>