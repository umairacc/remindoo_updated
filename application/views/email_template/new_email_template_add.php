
<style>	
.cke_chrome
{
    z-index: 999 !important;
}
.columns {
background: #fff;
padding: 20px;
}
.columns:after {
content: "";
clear: both;
display: block;
}
/*.columns > .editor {
float: left;
width: 100%;
position: relative;
z-index: 1;
}
.columns > .contacts {
float: right;
width: 100%;
box-sizing: border-box;
padding: 0 0 0 20px;
}
#contactList2 {
list-style-type: none;
margin: 0 !important;
padding: 0;
} */

#contactList2{
   list-style-type: none;
   margin: 0 !important;
   padding: 0;
   }
   #contactList2 li {
   background: #F3F3F3;
   margin-bottom: 5px;
   border-radius: 5px;
   line-height: 30px;
   cursor: pointer;
   }
   #contactList2 li:nth-child(2n) {
   background: #F3F3F3;
   }
    
   #contactList2 li:hover {
       background: #99d9ea;
       color: #fff;
   }


.contact2 {
    padding: 8px 10px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    line-height: 19px;
}
#editor3 .h-card2 {
background: #FFFDE3;
padding: 3px 6px;
border-bottom: 1px dashed #ccc;
}
#editor4 .h-card2 {
background: #FFFDE3;
padding: 3px 6px;
border-bottom: 1px dashed #ccc;
}
#editor3 {
border: 1px solid #ccc;
padding: 0 20px;
background: #fff;
position: relative;
}
#editor4 {
border: 1px solid #ccc;
padding: 0 20px;
background: #fff;
position: relative;
}
#editor3 .h-card2 .p-tel {
font-style: italic;
}
#editor3 .h-card2 .p-tel::before,
#editor4 .h-card2 .p-tel::after {
font-style: normal;
}
div#editor4 {
min-height: 400px;
max-height: 600px;
padding: 15px;
overflow: auto;
}
div#editor3 {
min-width: 100%;
max-width: 700px;
padding: 15px;
overflow: auto;
}
</style>
<div class=" suballemailclsnew">
    
                  <!--start-->
			<div class="col-sm-12">
              <!-- Register your self card start -->
              <div class="col-12 card">
                <!-- admin start-->
                <div class="client_section user-dashboard-section1 floating_set newmailclsnew">
                     
                                       
          
          <div class="columns">
          	 <div class="edit-tempsect col-xs-12 editor">
          	 	<div class="newemailset firstnewemailset">
			          <div class="col-md-12">          
			          <label>Titles</label>
			          </div>
			   		 <div class="col-md-12 select">          
			         <select  name="title" id="title" class="form-control" data-parsley-required="true">
			         <option value="">Select </option>
			         <option value="welcome_mail">Wecome mail</option>
			        <!--  <option value="discuss_proposal">Discuss Proposal</option> -->
			 		 <option value="task_assigne_member_mail">Task Assigne Members Mail</option>
			 		 <option value="task_reminder_mail">Task Reminder Mail</option>
			 		 <option value="leads_assigne_members_mail">Leads Assigne Members Mail</option>
			 		 <option value="services_mail">Client Services Notification</option>
			 		  <option value="task_delete_mail">Task Delete Notification</option> 
			 		 
			 		  <?php 
			 		  foreach($template_list as $template){
			 		  ?>
			 		   <option value="<?php echo $template['title']; ?>"><?php echo $template['title']; ?></option>
			 		  <?php } ?>

			 		   <option value="add_custom_reminder">Add Custom Reminder Template</option>
			 		   <option value="add_email_template">Add Email Template</option>
			         </select>
			          </div>   
			          <span style="color: red;" id="title_er"></span>
      		</div>
          <div class="col-md-12 reminder_heading_div" style="display: none;">
          	 <label>Reminder Heading</label>
          </div>
           <div class="col-md-12 select reminder_heading_input" style="display: none;">   
           <input type="text" name="reminder_heading" class="form-control" id="reminder_heading" value="">
           <span style="color: red;" id="reminder_heading_er"></span>
           </div>
           <div class="newemailset">
	          <div class="col-md-12">          
	          	<label>Subject</label>
	          </div>
	       
	          <div cols="10" id="editor3" name="editor3" rows="10"  contenteditable="true">			
			  </div>
			  <span style="color: red;" id="subject1_er"></span>
		   </div>
		   <div class="newemailset newemailsetsub">
	          <div class="col-md-12">          
	          <label>Body</label>
	          </div>		
			  <div cols="10" id="editor4" name="editor4" rows="10"  contenteditable="true">			 				
			  </div>
			   <span style="color: red;" id="body_er"></span>
			</div>
		</div>
		<div class="contacts">
			<h5>Adjustable Tokens</h5>
			<ul id="contactList2">
				<li class="welcome_mail a add_email_template">
					<div class="contact2 h-card2 " data-contact="0" draggable="true" tabindex="0">
						::Client Username::</div>
				</li>
				<li class="welcome_mail a add_email_template">
					<div class="contact2 h-card2" data-contact="0" draggable="true" tabindex="0">
						::Client Password::</div>
				</li>
				<li class="welcome_mail a add_email_template">
					<div class="contact2 h-card2" data-contact="0" draggable="true" tabindex="0">
						::Client Name::</div>
				</li>
				<li class="welcome_mail a add_email_template">
					<div class="contact2 h-card2" data-contact="1" draggable="true" tabindex="0">
						::Client Company::</div>
				</li>
			 
				<li class="welcome_mail a add_email_template">
					<div class="contact2 h-card2" data-contact="7" draggable="true" tabindex="0">
						:: Client Name::</div>
				</li>

				<li class="a add_email_template">
					<div class="contact2 h-card2" data-contact="7" draggable="true" tabindex="0">
						:: Client Missing Datas::</div>
				</li>

				<li class="welcome_mail a add_email_template">
					<div class="contact2 h-card2" data-contact="8" draggable="true" tabindex="0">
						:: ClientPhoneno::</div>
				</li>
				<li class="welcome_mail a add_email_template">
					<div class="contact2 h-card2" data-contact="9" draggable="true" tabindex="0">
						:: ClientWebsite::</div>
				</li>

				<li class="welcome_mail a add_email_template">
					<div class="contact2 h-card2" data-contact="10" draggable="true" tabindex="0">
						:: Sender Company::</div>
				</li>

				<li class="welcome_mail a add_email_template">
					<div class="contact2 h-card2" data-contact="11" draggable="true" tabindex="0">
						:: Sender Name::</div>
				</li>


				<li class="task_assigne_member_mail a">
					<div class="contact2 h-card2" data-contact="12" draggable="true" tabindex="0">
						:: Staff Name::</div>
				</li>

				<li class="task_assigne_member_mail a">
					<div class="contact2 h-card2" data-contact="13" draggable="true" tabindex="0">
						:: Task Link::</div>
				</li>

				<li class="task_assigne_member_mail a">
					<div class="contact2 h-card2" data-contact="14" draggable="true" tabindex="0">
						:: Task Name::</div>
				</li>

				<li class="task_assigne_member_mail a">
					<div class="contact2 h-card2" data-contact="15" draggable="true" tabindex="0">
						:: Assign via::</div>
				</li>


				<li class="leads_assigne_members_mail a">
					<div class="contact2 h-card2" data-contact="16" draggable="true" tabindex="0">
						:: Staff Name::</div>
				</li>

				<li class="leads_assigne_members_mail a">
					<div class="contact2 h-card2" data-contact="17" draggable="true" tabindex="0">
						:: Lead Link::</div>
				</li>

				<li class="leads_assigne_members_mail a">
					<div class="contact2 h-card2" data-contact="18" draggable="true" tabindex="0">
						:: Lead Name::</div>
				</li>

				<li class="leads_assigne_members_mail a">
					<div class="contact2 h-card2" data-contact="19" draggable="true" tabindex="0">
						:: Assign via::</div>
				</li>

				<li class="task_reminder_mail a">
					<div class="contact2 h-card2" data-contact="20" draggable="true" tabindex="0">
						:: Staff Name::</div>
				</li>

				<li class="task_reminder_mail a">
					<div class="contact2 h-card2" data-contact="21" draggable="true" tabindex="0">
						:: Task Link::</div>
				</li>

				<li class="task_reminder_mail a">
					<div class="contact2 h-card2" data-contact="22" draggable="true" tabindex="0">
						:: Task Name::</div>
				</li>

				<li class="task_reminder_mail a">
					<div class="contact2 h-card2" data-contact="23" draggable="true" tabindex="0">
						:: Assign via::</div>
				</li>

				<li class="services_mail a">
					<div class="contact2 h-card2" data-contact="24" draggable="true" tabindex="0">
						:: Client Name::</div>
				</li>

				<li class="services_mail a add_email_template">
					<div class="contact2 h-card2" data-contact="23" draggable="true" tabindex="0">
						:: Service Name::</div>
				</li>

				<li class="services_mail a add_email_template">
					<div class="contact2 h-card2" data-contact="23" draggable="true" tabindex="0">
						:: service Deadlines::</div>
				</li>

				<!-- <li class="services_mail a">
					<div class="contact2 h-card2" data-contact="23" draggable="true" tabindex="0">
						:: Service End date::</div>
				</li> -->
				<li class="services_mail a">
					<div class="contact2 h-card2" data-contact="24" draggable="true" tabindex="0">
						:: Service Link::</div>
				</li>

				<li class="services_mail a">
					<div class="contact2 h-card2" data-contact="25" draggable="true" tabindex="0">
						:: Sender Company::</div>
				</li>		


					<li class="task_delete_mail a">
					<div class="contact2 h-card2" data-contact="20" draggable="true" tabindex="0">
						:: Staff Name::</div>
				</li>

				<li class="task_delete_mail a">
					<div class="contact2 h-card2" data-contact="21" draggable="true" tabindex="0">
						:: Task Link::</div>
				</li>

				<li class="task_delete_mail a">
					<div class="contact2 h-card2" data-contact="22" draggable="true" tabindex="0">
						:: Task Name::</div>
				</li>

				<li class="task_delete_mail a">
					<div class="contact2 h-card2" data-contact="23" draggable="true" tabindex="0">
						:: Assign via::</div>
				</li>
								<li class="add_custom_reminder a">
					<div class="contact2 h-card2" data-contact="7" draggable="true" tabindex="0">
					:: Client Name::</div>
				</li>

				<li class="add_custom_reminder a">
				<div class="contact2 h-card2" data-contact="0" draggable="true" tabindex="0">
					::Client Username::</div>
				</li>

				<li class="add_custom_reminder a">
					<div class="contact2 h-card2" data-contact="1" draggable="true" tabindex="0">
						::Client Company::</div>
				</li>
<!-- 

				<li class="add_custom_reminder a">
					<div class="contact2 h-card2" data-contact="10" draggable="true" tabindex="0">
						:: Sender Company::</div>
				</li>

				<li class="add_custom_reminder a">
					<div class="contact2 h-card2" data-contact="11" draggable="true" tabindex="0">
						:: Sender Name::</div>
				</li> -->


					<li class="add_custom_reminder a">
					<div class="contact2 h-card2" data-contact="23" draggable="true" tabindex="0">
						:: Service Name::</div>
				</li>

				<li class="add_custom_reminder a">
					<div class="contact2 h-card2" data-contact="23" draggable="true" tabindex="0">
						:: service Date::</div>
				</li>
 
				<li class="add_custom_reminder a add_email_template">
					<div class="contact2 h-card2" data-contact="23" draggable="true" tabindex="0">
						:: service Due Date::</div>
				</li>

				<!-- <li class="add_custom_reminder a">
					<div class="contact2 h-card2" data-contact="23" draggable="true" tabindex="0">
						:: Reminder Days ::</div>
				</li>

				<li class="add_custom_reminder a">
					<div class="contact2 h-card2" data-contact="23" draggable="true" tabindex="0">
						:: Add Custom Reminder ::</div>
				</li>
 -->
			    <li class="add_custom_reminder a add_email_template">
					<div class="contact2 h-card2" data-contact="23" draggable="true" tabindex="0">
						:: Date ::</div>
				</li>


				
			</ul>
		</div>
		<!-- <div class="modal-footer">
            
         </div> -->
	</div> 
	 </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
 

	
  