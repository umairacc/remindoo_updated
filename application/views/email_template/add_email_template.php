
<style>	

.cke_chrome
{
    z-index: 999 !important;
}
.columns {
background: #fff;
padding: 20px;
}
.columns:after {
content: "";
clear: both;
display: block;
}
#contactList2 
{
	list-style-type: none;
	margin: 0 !important;
	padding: 0;
}
#contactList2 li 
{
	background: #F3F3F3;
	margin-bottom: 5px;
	border-radius: 5px;
	line-height: 30px;
	cursor: pointer;
}
#contactList2 li:nth-child(2n)
{
	background: #F3F3F3;
}

#contactList2 li:hover 
{
   background: #99d9ea;
   color: #fff;
}

.contact2 {
padding: 8px 10px;
white-space: nowrap;
overflow: hidden;
text-overflow: ellipsis;
line-height: 19px;
}
#editor3 .h-card2 
{
	background: #FFFDE3;
	padding: 3px 6px;
	border-bottom: 1px dashed #ccc;
}
#editor4 .h-card2 
{
	background: #FFFDE3;
	padding: 3px 6px;
	border-bottom: 1px dashed #ccc;
}
/*#editor3 {
border: 1px solid #ccc;
padding: 0 20px;
background: #fff;
position: relative;
}*/
#editor4 
{
	border: 1px solid #ccc;
	padding: 0 20px;
	background: #fff;
	position: relative;
}
#editor3 .h-card2 .p-tel 
{
	font-style: italic;
}
#editor3 .h-card2 .p-tel::before,
#editor4 .h-card2 .p-tel::after 
{
	font-style: normal;
}
div#editor4 
{
	min-height: 400px;
	max-height: 600px;
	padding: 15px;
	overflow: auto;
}
div#editor3 
{
	min-width: 100%;
	max-width: 700px;
	padding: 15px;
	overflow: auto;
}

</style>

<?php  

 $type = $this->uri->segment(2); 

?>

<div class=" suballemailclsnew">    
	  <!--start-->
	<div class="col-sm-12">
	<!-- Register your self card start -->
	<div class="col-12 card">
	<!-- admin start-->
	<div class="client_section user-dashboard-section1 floating_set newmailclsnew">     
          
          <div class="columns">
          	 <div class="edit-tempsect col-md-9">
				
                <?php if($type == 'add_template' || $type == 'edit_template'){ ?>
				<div class="newemailset firstnewemailset">
					<div class="">          
					<label>Email Type</label>
					</div>
					<div class="select">          
					<select  name="type" id="type" class="form-control clr-check-select" data-parsley-required="true">
					<option value="">Select</option>
                   <?php   foreach($email_type as $key => $value){  ?>
                         <optgroup label="<?php echo ucfirst($key); ?>">	
                         <?php foreach($value as $key1 => $value1) { ?>
                         	                  
                         <option value="<?php echo $key1; ?>" <?php if($key1 == $template['action_id']){ echo "selected";} ?> ><?php echo ucwords(str_replace('_',' ', $value1)); ?></option>
                         <?php } ?>
                     </optgroup>
                    <?php } ?>                    
					</select>
					</div>   
					<span style="color: red;" id="type_er"></span>
				</div>
			    <?php } if($type == 'add_reminder' || $type == 'edit_reminder'){ ?>
		    	 <div class="newemailset">
		    	 	<div class="">          
		    	 	<label>Reminder Heading</label>
		    	 	</div>
		    	 	<input type="text" name="reminder_heading" id="reminder_heading" class="form-control clr-check-client" value="<?php echo $template['reminder_heading']; ?>">
		    	 	<span style="color: red;" id="reminder_heading_er"></span>
		    	 </div>
			    <?php } ?>
				<div class="newemailset email_title" style="display: none;">
					<div class="">          
					<label>Title</label>
					</div>
					<input type="text" name="title" id="title" class="form-control" value="<?php echo $template['title']; ?>">
					<span style="color: red;" id="title_er"></span>
				</div>
				<div class="newemailset">
					<div class="">          
					<label>Subject</label>
					</div>
					<input id="editor3" name="editor3" class="txtDropTarget form-control clr-check-client" value="<?php echo html_entity_decode($template['subject']); ?>">
					<span style="color: red;" id="subject1_er"></span>
				</div>
				<div class="newemailset from" style="display: none;">
					<div class="">          
					<label>From (Email)</label>
					</div>
					<input type="text" name="from_address" id="from_address" class="form-control" value="<?php echo $template['from_address']; ?>">
					<span style="color: red;" id="from_address_er"></span>
				</div>
				<div class="newemailset newemailsetsub">
					<div class="">          
					<label>Body</label>
					</div>		
					<textarea cols="10" id="editor4" name="editor4" rows="10">		
					 	    <?php echo html_entity_decode($template['body']); ?>		
					</textarea>
				    <span style="color: red;" id="body_er"></span>
				</div>
				<div class="newemailset">
					<div class="">          
					<label>Status</label>
					</div>		
					<select name="status" id="temp_status" class="form-control clr-check-select">
						<option value="">Select</option>
						<?php 
						$select1 = "";
						$select0 = "";
						if($template['status'] == '1'){ $select1 = "selected"; }
                        else { $select0 = "selected"; }
						 ?>
                        }
						<option value="1" <?php echo $select1; ?>>Active</option>
						<option value="0" <?php echo $select0; ?>>Inactive</option>
					</select>
				    <span style="color: red;" id="status_er"></span>
				</div>
		   </div>
		 <div class="contacts col-md-3">
			<h5>Adjustable Tokens</h5>
			<div class="row mar_bottls">			  
			  <div class="col-12">
			    <input class="form-control form-control-lg" type="text" id="search_placeholders" placeholder="Search">
			  </div>
			</div>
			<ul id="contactList2">
				<?php if($type != 'add_reminder' && $type != 'edit_reminder') { ?>
			      
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2 " data-contact="0" draggable="true" tabindex="0">::Client Username::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2" data-contact="1" draggable="true" tabindex="0">::Client Password::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2" data-contact="2" draggable="true" tabindex="0">::Client Name::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2" data-contact="3" draggable="true" tabindex="0">::Client Company::</div>
			      </li>				
			      <li class="a add_email_template">
			      <div class="contact2 h-card2" data-contact="4" draggable="true" tabindex="0">:: Client Missing Datas::</div>
			      </li>
			      <li class="a add_email_template">
			      <div class="contact2 h-card2" data-contact="42" draggable="true" tabindex="0">:: Client Invitation Link::</div>
			      </li>

			      <li class="a add_email_template">
			      <div class="contact2 h-card2" data-contact="43" draggable="true" tabindex="0">::CHASE_INFO_NOTES::</div>
			      </li>
			      
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2" data-contact="5" draggable="true" tabindex="0">:: ClientPhoneno::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2" data-contact="6" draggable="true" tabindex="0">:: ClientWebsite::</div>
			      </li>				
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2" data-contact="7" draggable="true" tabindex="0">:: Sender Name::</div>
			      </li>
			      <li class="task_assigne_member_mail a">
			      <div class="contact2 h-card2" data-contact="8" draggable="true" tabindex="0">:: Staff Name::</div>
			      </li>
			      <li class="task_assigne_member_mail a">
			      <div class="contact2 h-card2" data-contact="9" draggable="true" tabindex="0">:: Task Link::</div>
			      </li>
			      <li class="task_assigne_member_mail a">
			      <div class="contact2 h-card2" data-contact="10" draggable="true" tabindex="0">:: Task Name::</div>
			      </li>
			      <li class="task_assigne_member_mail a">
			      <div class="contact2 h-card2" data-contact="11" draggable="true" tabindex="0">:: Assign via::</div>
			      </li>				
			      <li class="leads_assigne_members_mail a">
			      <div class="contact2 h-card2" data-contact="12" draggable="true" tabindex="0">:: Lead Link::</div>
			      </li>
			      <li class="leads_assigne_members_mail a">
			      <div class="contact2 h-card2" data-contact="13" draggable="true" tabindex="0">:: Lead Name::</div>
			      </li>	
			      <li class="services_mail a add_email_template">
			      <div class="contact2 h-card2" data-contact="14" draggable="true" tabindex="0">:: Service Name::</div>
			      </li>
			      <li class="services_mail a add_email_template">
			      <div class="contact2 h-card2" data-contact="15" draggable="true" tabindex="0">:: service Deadlines::</div>
			      </li>				
			      <li class="services_mail a">
			      <div class="contact2 h-card2" data-contact="16" draggable="true" tabindex="0">:: Service Link::</div>
			      </li>
			      <li class="add_custom_reminder a">
			      <div class="contact2 h-card2" data-contact="17" draggable="true" tabindex="0">:: service Date::</div>
			      </li>
			      <li class="add_custom_reminder a add_email_template">
			      <div class="contact2 h-card2" data-contact="18" draggable="true" tabindex="0">:: service Due Date::</div>
			      </li>
			      <li class="add_custom_reminder a add_email_template">
			      <div class="contact2 h-card2" data-contact="19" draggable="true" tabindex="0">:: Date ::</div>
			      </li>
			      <li class="add_custom_reminder a add_email_template">
			      <div class="contact2 h-card2" data-contact="20" draggable="true" tabindex="0">:: Proposal Link ::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2 " data-contact="21" draggable="true" tabindex="0">::Username::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2 " data-contact="22" draggable="true" tabindex="0">::Client Company City::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2 " data-contact="23" draggable="true" tabindex="0">::Client Company Country::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2 " data-contact="24" draggable="true" tabindex="0">::Client Company Religion::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2 " data-contact="25" draggable="true" tabindex="0">::Client First Name::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2 " data-contact="26" draggable="true" tabindex="0">::Client Last Name::</div>
			      </li>

			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2 " data-contact="27" draggable="true" tabindex="0">:: ProposalCurrency::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2 " data-contact="28" draggable="true" tabindex="0">:: ProposalExpirationDate::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2 " data-contact="29" draggable="true" tabindex="0">:: ProposalName::</div>
			      </li>
			      <li class="welcome_mail a add_email_template">
			      <div class="contact2 h-card2 " data-contact="30" draggable="true" tabindex="0">:: ProposalCodeNumber::</div>
			      </li>
                  <li class="welcome_mail a add_email_template">
                  <div class="contact2 h-card2 " data-contact="31" draggable="true" tabindex="0">::SenderCompany::</div>
                  </li>
                  <li class="welcome_mail a add_email_template">
                  <div class="contact2 h-card2 " data-contact="32" draggable="true" tabindex="0">::SenderCompanyAddress::</div>
                  </li>
                  <li class="welcome_mail a add_email_template">
                  <div class="contact2 h-card2 " data-contact="33" draggable="true" tabindex="0">::SenderCompanyCity::</div>
                  </li>
                  <li class="welcome_mail a add_email_template">
                  <div class="contact2 h-card2 " data-contact="34" draggable="true" tabindex="0">::SenderCompanyCountry::</div>
                  </li>
                  <li class="welcome_mail a add_email_template">
                  <div class="contact2 h-card2 " data-contact="35" draggable="true" tabindex="0">::SenderCompanyReligion::</div>
                  </li>
                  <li class="welcome_mail a add_email_template">
                  <div class="contact2 h-card2 " data-contact="36" draggable="true" tabindex="0">::SenderEmail::</div>
                  </li>
                  <li class="welcome_mail a add_email_template">
                  <div class="contact2 h-card2 " data-contact="37" draggable="true" tabindex="0">::SenderEmailSignature::</div>
                  </li>
                  <li class="welcome_mail a add_email_template">
                  <div class="contact2 h-card2 " data-contact="38" draggable="true" tabindex="0">::senderPhoneno::</div>
                  </li>
                  <li class="welcome_mail a add_email_template">
                  <div class="contact2 h-card2 " data-contact="39" draggable="true" tabindex="0">::OnlineRegistrationLink::</div>
                  </li>  
                  <li class="welcome_mail a add_email_template">
                  <div class="contact2 h-card2 " data-contact="40" draggable="true" tabindex="0">::URL::</div>
                  </li>
                  <li class="welcome_mail a add_email_template">
                  <div class="contact2 h-card2 " data-contact="41" draggable="true" tabindex="0">::Message::</div>
                  </li>

                   <li class="a add_email_template">
			      <div class="contact2 h-card2" data-contact="44" draggable="true" tabindex="0">::DirectoryName::</div>
			      </li>

			       <li class="a add_email_template">
			      <div class="contact2 h-card2" data-contact="45" draggable="true" tabindex="0">::FileName::</div>
			      </li>


			       <li class="a add_email_template">
			      <div class="contact2 h-card2" data-contact="46" draggable="true" tabindex="0">::TaskStatus::</div>
			      </li>


			       <li class="a add_email_template">
			      <div class="contact2 h-card2" data-contact="47" draggable="true" tabindex="0">::TaskProgress::</div>
			      </li>


			         <li class="a add_email_template">
			      <div class="contact2 h-card2" data-contact="48" draggable="true" tabindex="0">::Assignee::</div>
			      </li>


			         <li class="a add_email_template">
			      <div class="contact2 h-card2" data-contact="49" draggable="true" tabindex="0">::TaskDueDate::</div>
			      </li>





				<?php }
			else
			{
				$client_data = $this->db->list_fields('client');

				$un_wanted = ['id','user_id','firm_id','firm_admin_id'];

				$client_data = array_diff($client_data, $un_wanted);
				$i=1;
				foreach ($client_data as $key => $value)
				{
					$value = trim(str_replace('crm_','', $value));
					echo '<li class="add_custom_reminder a add_email_template">
						<div class="contact2 h-card2" data-contact="'.$i.'" draggable="true" tabindex="0">::'.$value.'::</div>
					</li>';
					$i++;
				}

			} ?>					
			</ul>
		</div>		
	  </div> 
	 </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector"></div>
</div>
