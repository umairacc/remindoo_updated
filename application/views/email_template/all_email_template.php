<?php 

if($_SESSION['firm_id'] == '0')
{
   $this->load->view('super_admin/superAdmin_header');
}
else
{
   $this->load->view('includes/header');
}

?>
<style> 

   .columns > .editor {
   float: left;
   width: 70%;
   position: relative;
   z-index: 1;
   }
   .columns > .contacts {
   float: right;
   width: 30%;
   box-sizing: border-box;
   padding: 0 0 0 20px;
   }

   #contactList li {
   background: #F3F3F3;
   margin-bottom: 5px;
   border-radius: 5px;
   line-height: 30px;
   cursor: pointer;
   }
   #contactList li:nth-child(2n) {
   background: #F3F3F3;
   }
    
   #contactList li:hover {
       background: #99d9ea;
       color: #fff;
   } 
   
</style>
<div class="pcoded-content pcodedteam-content allemailclsnew" style="margin-top: -60px !important;">
<div class="pcoded-inner-content">
<!-- Main-body start -->
<div class="main-body">
<div class="page-wrapper">
<!-- Page body start -->
<div class="page-body">
<div class="row email-rem-temp-wrapper">              
    <!--start-->
    <div class="col-sm-12">
       <!-- Register your self card start -->
       <div class="card propose-template">     


        <div class="deadline-crm1 floating_set">                 
          <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard client-top-nav">
            <li class="nav-item borbottomcls">
              <a class="nav-link"  href="javascript:void(0)">Email Template</a>
              <div class="slide"></div>
            </li>
          </ul>      
        </div>                     

                      
        <div class="all_user-section floating_set pros-editemp">
         
           <div class="all_user-section2 floating_set">

              <div class="tab-content">
                 <div id="allusers" class="tab-pane fade in active">
                    <div id="task"></div>
                    <div class="client_section3 table-responsive">
                       <div class="status_succ"></div>
                       <div class="all-usera1 data-padding1 ">
                       
                          <div class="">
                        <form id="new_proposal" name="new_proposal" class="require-validation" method="post">

                        <?php $this->load->view('email_template/add_email_template'); ?>
                        <button type="button" id="add_template" class="btn btn-primary saveclsset">Save</button>
                  
                        </form>
                        <input type="hidden" class="rows_selected" id="select_template_count" >     
                          </div>
                       </div>
                       <!-- List -->           
                    </div>
                 </div>
              </div>
           </div>
        </div>
            
          <!-- Page body end -->
    </div>
    </div>
    <!-- Main-body end -->
    <div id="styleSelector">
    </div>
 </div> 
 </div>
 </div>
 </div>
 </div>
 </div>      

 <?php 

 $this->load->view('includes/session_timeout');

 if($_SESSION['firm_id'] == '0')
 {
    $this->load->view('super_admin/superAdmin_footer');
 }
 else
 {
    $this->load->view('includes/footer');
 }

 ?>     
<?php include('new_email_template_script.php'); ?> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/parsley.css') ?>">
<script src="<?php echo base_url('assets/js/parsley.min.js') ?>"></script>

<script type="text/javascript">

$('#new_proposal').parsley();

$(document).ready(function()
{ 
   $('.header-navbar').css('display','none');
});
 
</script> 

<script>
	$(document).ready(function(){
		$(".clr-check-client").keyup(function(){
			var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-client-outline");
			}else{
				$(this).removeClass("clr-check-client-outline");
			}
		});
    $(".clr-check-client").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-client-outline");
      }  
    })

		$(".clr-check-select").change(function(){
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		});
    $(".clr-check-select").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-select-client-outline");
      }  
    })
	});

</script>