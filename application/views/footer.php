<?php $fun = $this->uri->segment(2);?>

<?php $isBasicTheme = ( isset( $theme ) && $theme == 'basic' ) ? true : false; ?>

<?php if( $isBasicTheme ) { ?>
  
  <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script> 
  <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>  
  <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function (){
      $(".theme-loader").remove();
    });
  </script>

<?php return; } ?>

<div class="modal fade" id="manager_service_accept" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input type="hidden" name="services_name" id="services_name" value="">
         <input type="hidden" name="service_user_id" id="service_user_id" value="">
         <input type="hidden" name="notifications_id" id="notifications_id" value="">
          <p>Do you want to accept Service Request?.</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="accept_service">Yes</button>
         <button type="button" class="btn btn-primary" id="send_proposal">Send Proposal</button>
          <button type="button" class="btn btn-default close_popup" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>
  <div class="modal fade" id="notification_modal" role="dialog">
    <div class="modal-dialog modal-notify">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">  
          <input type="hidden" name="notification_id" id="notification_id" value="">
          <input type="hidden" name="notification_type" id="notification_type" value="">
          <input type="hidden" name="notification_section" id="notification_section" value="">
          <input type="hidden" name="notification_users" id="notification_users" value="">
          <p id="show_message">Do you want to accept Service Request?.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="confirm_button">Yes</button>
          <button type="button" class="btn btn-default close_popup" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>



  <div id="forward_task" class="modal fade" role="dialog">
<div class="modal-dialog">
 <!-- Modal content-->
 <div class="modal-content">
    <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
    </div>
    <div class="modal-body">

        <input type="hidden" name="notification_id" id="fwd_notification_id" value="">
          <input type="hidden" name="notification_type" id="fwd_notification_type" value="">
          <input type="hidden" name="notification_section" id="fwd_notification_section" value="">
          <input type="hidden" name="notification_users" id="fwd_notification_users" value="">

           <?php 
      if($_SESSION['role']==5){
         $staff_list=$this->db->query("select * from  assign_manager where manager='".$_SESSION['id']."'")->row_array();
         $staff=$staff_list['staff'];
         if($staff==''){
          $staff=0;
         }
        $staff_form = $this->db->query('select * from user where id in ('.$staff.')')->result_array();

      }else{

             $staff_form = $this->Common_mdl->GetAllWithWheretwo('user','role','6','firm_admin_id',$_SESSION['id']);
      }

       $team = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();
       $department=$this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();

       ?>
       <!--  <label>assign to </label> -->
       <div class="dropdown-sin-21234">
          <select multiple placeholder="select" name="workers[]" id="workers" class="workers">
          
  <?php    


   if(count($staff_form)){ ?>
          <option disabled>Staff</option>
             <?php foreach($staff_form as $s_key => $s_val){ ?>
              <option value="<?php echo $s_val['id'];?>"   ><?php echo $s_val['crm_name'];?></option>
             <?php } 
             } ?>
      <?php if(count($team)){ ?>
          <option disabled>Team</option>
             <?php foreach($team as $s_key => $s_val){ ?>
              <option value="tm_<?php echo $s_val['id'];?>"  ><?php echo $s_val['team'];?></option>
             <?php } 
             } ?>
          <?php if(count($department)){ ?>
          <option disabled>Department</option>
             <?php foreach($department as $s_key => $s_val){ ?>
              <option value="de_<?php echo $s_val['id'];?>" ><?php echo $s_val['new_dept'];?></option>
             <?php } 
             } ?>
          </select>
       </div>
    </div>
    <div class="modal-footer profileEdit">
        <button type="button" class="btn btn-primary" id="forward">Save</button>
          <button type="button" class="btn btn-default close_popup" data-dismiss="modal">No</button>
    </div>
 </div>
</div>
</div>




<div class="modal fade" id="manager_task_review" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>

        <div class="modal-body">
        <input type="hidden" id="review_notification_id">

         <p>Do you want to accept Service Request?.</p>
         <br>
         <b>Notes</b>
         <textarea id="review_response_notes"></textarea>
        </div>

        <div class="modal-footer" id="model_footer_button">
          <button type="button" class="btn btn-primary"  onclick="review_response('1')">Yes</button>
          <button type="button" class="btn btn-default" onclick="review_response('0')">No</button>
          <!-- <button type="button" class="btn btn-default close_popup" data-dismiss="modal" onclick="review_response(1)">No</button> -->
        </div>

      </div>
      
    </div>
  </div>


<div class="modal fade" id="popup-2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Feedback Form</h4>
        </div>
        <div class="modal-body">
      <form action="<?php echo base_url();?>User/feedMail" id="feed_form" method="post" name="feed_form">
         <style>
            input.error {
            border: 1px dotted red;
            }
            label.error{
            width: 100%;
            color: red;
            font-style: italic;
            margin-left: 120px;
            margin-bottom: 5px;
            }
         </style>
  
         <label><b>Your content</b></label>
         <textarea name="feed_msg" id="feed_msg" placeholder="Type your text here..."></textarea>
         <!-- <br><br> -->        
  
      </form>
    </div>
        <div class="modal-footer">
          <div class="feedback-submit">
            <div class="feed-submit text-left">
               <input id="feed" name="submit" type="submit" value="submit">
            </div>
         </div>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>







      <!-- Required Jquery -->
      <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields">
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script> 
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script> 
  <!--     <script src="<?php echo base_url();?>assets/js/timezones.full.js"></script> -->
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>  
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script> 
      <!-- j-pro js -->
      <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script> -->
      <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
      <!-- jquery slimscroll js -->
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
      <!-- modernizr js -->
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
      <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
   <!--    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.min.js"></script> -->
    <!--   <script type="text/javascript" src="<?php echo base_url();?>assets/js/daterangepicker.js"></script> -->
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/datedropper.min.js"></script>
      <!-- <script type="text/javascript" src="https://remindoo.uk/assets/js/masonry.pkgd.min.js"></script> -->
      <!-- Custom js -->
   <!--    <script src="<?php echo base_url();?>assets/pages/wysiwyg-editor/wysiwyg-editor.js"></script>  -->
      <script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script> 
      <script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
      <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/masonry.pkgd.min.js"></script> -->
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>	
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
    <!--   <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-picker.js"></script>  -->
      <script src="<?php echo base_url();?>assets/js/mock.js"></script>
      <script src="<?php echo base_url();?>assets/js/hierarchy-select.min.js"></script>
<!-- <script src="https://use.fontawesome.com/86c8941095.js"></script> -->
  
    <?php $url = $this->uri->segment('2'); ?>
    <?php if ($url == 'step_proposal' || $url == 'copy_proposal' || $url == 'template_design' || $url=='templates') { ?>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <?php } ?>
    <script src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/debounce.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script><!-- 
    <script src="<?php echo base_url();?>assets/js/bootstrap-slider.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
    <script src="https://cdn.jsdelivr.net/medium-editor/latest/js/medium-editor.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/creative.tools.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/html2canvas.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/image-edit.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/editor.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
    <script src="https://cdn.ckeditor.com/4.11.1/standard-all/ckeditor.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
  <script type="text/javascript">
$(document).ready(function (){



     $('.dropdown-sin-21234').dropdown({
         limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });




      $('th').on("click.DT", function (e) {
      //stop Propagation if clciked outsidemask
      //becasue we want to sort locally here
        if (!$(e.target).hasClass('sortMask')) {
          e.stopImmediatePropagation();
        }
      });
 $(document).on("click",".close_notification",function(){})
});

function review_response(res)
{
  var id=$("#review_notification_id").val();
  var notes=$("#review_response_notes").val();
  $.ajax({
    url:"<?=base_url()?>user/manager_review_response",
    data:{"mn_id":id,"res":res,"notes":notes},
    type:"POST",
    success:function(res){location.reload();}
    });
}

$(document).ready(function() {

  $('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
$('.custom_checkbox1 input').after( "<i></i>" )

  
          CKEDITOR.editorConfig = function (config) {
          config.language = 'es';
          config.uiColor = '#fff';
          config.height = 300;
          config.toolbarCanCollapse = true;
          config.toolbarLocation = 'bottom';
          };
           CKEDITOR.replace('feed_msg'); 
//}catch(e){alert(e);}

        });
        </script>


    <script>
    $( document ).ready(function() {   

        var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy',minDate:0 }).val();

        // Multiple swithces
        var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));

        elem.forEach(function(html) {
            var switchery = new Switchery(html, {
                color: '#22b14c',
                jackColor: '#fff',
                size: 'small'
            });
        });

        $('#accordion_close').on('click', function(){
                $('#accordion').slideToggle(300);
                $(this).toggleClass('accordion_down');
        });


        $('.chat-single-box .chat-header .close').on('click', function(){
          $('.chat-single-box').hide();
        });

		
		var resrow = $('#responsive-reorder').DataTable({
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    });
});
$(".rejected_tsk").click(function(){
  $.ajax({
    url:"<?= base_url()?>user/rejected_tsk_seen_status/"+$(this).attr("data-id"),
    type:'get',
    success:function(){location.reload();}
  })
});
</script>
</body>
<!-- chat box -->
  <!-- modal 1-->
      <div class="modal fade all_layout_modal" id="default-Modal" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Search Companines House</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="input-group input-group-button input-group-primary modal-search">
                     <input type="text" class="form-control" placeholder="Search here..." id="searchCompany">
                     <button class="btn btn-primary input-group-addon" id="basic-addon1">
                     <i class="ti-search" aria-hidden="true"></i>
                     </button>
                  </div>
                  <div class="british_gas1 trading_limited" id="searchresult">
                  </div>
                  <div class="british_view" style="display:none" id="companyprofile">                     
                  </div>
                  <div class="main_contacts" id="selectcompany" style="display:none">
                     <h3>british gas trading limited</h3>
                     <div class="trading_tables">
                        <div class="trading_rate">
                           <p>
                              <span><strong>BARBARO,Gab</strong></span>
                              <span>Born 1971</span>
                              <span><a href="#">Use as Main Contact</a></span>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- modalbody -->
            </div>
         </div>
      </div>
        <!-- modal-close -->
        <!--modal 2-->
      <div class="modal fade all_layout_modals" id="exist_person" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Select Existing Person</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="main_contacts" id="main_contacts" style="display:block">     
                  </div>
               </div>
               <!-- modalbody -->
            </div>
         </div>
      </div>
          <!-- modal-close -->
          <!-- modal 2-->

</html>
<script>
//class="modal fade all_layout_modal show"
function company_model_close(){
  //console.log('aaa');
    $('.all_layout_modal').modal('hide');
 //   $('button.close').trigger('click');
    $('body').removeClass('modal-open');
	$('.modal-backdrop').remove();

  $("#default-Modal").hide();
    // $('.modal-backdrop.show').hide();
	// $('.modal-backdrop.show').each(function(){
	// 	$(this).remove();
	// });
}


 $("#searchCompany").keyup(function(){


  $(".form_heading").html("<h2>Adding Client via Companies House</h2>");
       var currentRequest = null;    
       var term = $(this).val();
       //var term = $(this).val().replace(/ +?/g, '');
       //var term = $(this).val($(this).val().replace(/ +?/g, ''));
       $("#searchresult").show();
       $('#selectcompany').hide();
        $(".LoadingImage").show();
       currentRequest = $.ajax({
          url: '<?php echo base_url();?>client/SearchCompany/',
          type: 'post',
          data: { 'term':term },
          beforeSend : function()    {           
          if(currentRequest != null) {
              currentRequest.abort();
          }
      },
          success: function( data ){
              $("#searchresult").html(data);
               $(".LoadingImage").hide();
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });
     });

function addmorecontact(id)
{
$(".LoadingImage").hide();
$(".modal-backdrop").css("display","none");
    var url = $(location).attr('href').split("/").splice(0, 8).join("/");

    var segments = url.split( '/' );
    var fun = segments[5];

if(fun!='' && fun !='addnewclient'  && fun !='addnewclient#')
{
    $(location).attr('href', '<?php echo base_url();?>client/addnewclient/'+id);
}  else{
   $(location).attr('href', '<?php echo base_url();?>client/addnewclient/'+id);
   $("#companyss").attr("href","#");

$('.contact_form').show();
 $('.all_layout_modal').modal('hide');
 
     $('.nav-link').removeClass('active');
  //   $('.main_contacttab').addClass('active');
//$('.contact_form').show();

    $("#company").removeClass("show");
    $("#company").removeClass("active");
    $("#required_information").removeClass("active");
    $(".main_contacttab").addClass("active");
    $("#main_Contact").addClass("active");
    $("#main_Contact").addClass("show"); 
}      
                
  /* $("#companyss").attr("href","#");

$('.contact_form').show();
 $('.all_layout_modal').modal('hide');
 
     $('.nav-link').removeClass('active');
  //   $('.main_contacttab').addClass('active');
//$('.contact_form').show();

$("#company").removeClass("show");
$("#company").removeClass("active");
$("#required_information").removeClass("active");
 $(".main_contacttab").addClass("active");
$("#main_Contact").addClass("active");
$("#main_Contact").addClass("show");
*/
//$( "." ).tabs( { disabled: [1, 2] } );
  // $(".it3").attr("style", "display:none");
//$(".it3").tabs({disabled: true });
}

var xhr = null;
   function getCompanyRec(companyNo)
   {
      $(".LoadingImage").show();
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
       
      xhr = $.ajax({
          url: '<?php echo base_url();?>client/CompanyDetails/',
          type: 'post',
          data: { 'companyNo':companyNo },
          success: function( data ){
              //alert(data);
              $("#searchresult").hide();
              $('#companyprofile').show();
              $("#companyprofile").html(data);
              $(".LoadingImage").hide();
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });}


      function getCompanyView(companyNo)
   {
//alert(companyNo);
      //$('.modal-search').hide();
      $(".LoadingImage").show();
     var user_id= $("#user_id").val();
     
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
   
       
      xhr = $.ajax({
          url: '<?php echo base_url();?>client/selectcompany/',
          type: 'post',
          dataType: 'JSON',
          data: { 'companyNo':companyNo,'user_id': user_id},
          success: function( data ){
              //alert(data.html);
              $("#searchresult").hide();
              $('#companyprofile').hide();
              $('#selectcompany').show();
              $("#selectcompany").html(data.html);
              $('.main_contacts').html(data.html);
   // append form field value
   $("#company_house").val('1');
   //alert($("#company_house").val());
   $("#company_name").val(data.company_name);
   $("#company_name1").val(data.company_name);
   $("#company_number").val(data.company_number);
   $("#companynumber").val(data.company_number);
   $("#company_url").val(data.company_url);
   $("#company_url_anchor").attr("href",data.company_url);
   $("#officers_url").val(data.officers_url);
   $("#officers_url_anchor").attr("href",data.officers_url);
   $("#company_status").val(data.company_status);
   $("#company_type").val(data.company_type);

   // console.log('comapny type ');
   // console.log(data.company_type);
   // console.log('comapny type test ');
   //$("#company_sic").append(data.company_sic);
   $("#company_sic").val(data.company_sic);
   $("#sic_codes").val(data.sic_codes);
   
   $("#register_address").val(data.address1+"\n"+data.address2+"\n"+data.locality+"\n"+data.postal);
   $("#allocation_holder").val(data.allocation_holder);
   $("#date_of_creation").val(data.date_of_creation);
   $("#period_end_on").val(data.period_end_on);
   $("#next_made_up_to").val(data.next_made_up_to);
   $("#next_due").val(data.next_due);
   $("#accounts_due_date_hmrc").val(data.accounts_due_date_hmrc);
   $("#accounts_tax_date_hmrc").val(data.accounts_tax_date_hmrc);
   $("#confirm_next_made_up_to").val(data.confirm_next_made_up_to);
   $("#confirm_next_due").val(data.confirm_next_due);
   
   $("#tradingas").val(data.company_name);
   $("#business_details_nature_of_business").val(data.nature_business);
   $("#user_id").val(data.user_id);

 //  $(".edit-button-confim").show(); //for hide edit option button 
   $(".LoadingImage").hide();
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });}
   
   
      function getmaincontact(companyNo,appointments,i,birth)
   {
      $(".LoadingImage").show();
      var user_id= $("#user_id").val();
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
//alert(birth);
          var cnt = $("#append_cnt").val();

            if(cnt==''){
            cnt = 1;
           }else{
            cnt = parseInt(cnt)+1;
           }
        $("#append_cnt").val(cnt);

        var incre=$("#incre").val();
        if(incre==''){
            incre = 1;
           }else{
            incre = parseInt(incre)+1;
           }
        $("#incre").val(incre);
      xhr = $.ajax({
          url: '<?php echo base_url();?>client/maincontact/',
          type: 'post',
          
          data: { 'companyNo':companyNo,'appointments': appointments,'user_id': user_id,'cnt':cnt,'birth':birth,'incre':incre,'count':i},
          success: function( data ){
        //   console.log('#usecontact'+i);
        console.log($('#selectcompany').find('#usecontact'+i).html('<span class="succ_contact"><a href="#">Added</a></span>'));
          
           // $("#usecontact"+i).html('<span class="succ_contact"><a href="#">Added</a></span>');
              //$("#default-Modal,.modal-backdrop.show").hide();
                // $('#default-Modal').modal('hide');
                 // $('#exist_person').modal('hide');
       $('.contact_form').append(data);
       /** 05-07-2018 rs **/
           $('.make_a_primary').each(function(){
                     var countofdiv=$('.make_a_primary').length;
                     if(countofdiv>1){
                     var id=$(this).attr('id').split('-')[1];
                   //   $('.for_remove-'+id).remove();
                   // $('.for_row_count-'+id).append('<div class="remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
                      $('.for_remove-'+id).remove();
                   $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
                  }

                  });
       /** end of 05-07-2018 **/
        var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear());
      $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true,
        changeYear: true,yearRange: start.getFullYear() + ':' + end.getFullYear() }).val();
   $(".LoadingImage").hide();
   
   // append form field value
   /*$("#title").val(data.title);
   $("#first_name").val(data.first_name);
   $("#middle_name").val(data.middle_name);
   $("#last_name").val(data.last_name);
   $("#postal_address").val(data.premises+"\n"+data.address_line_1+"\n"+data.address_line_2+"\n"+data.region+"\n"+data.locality+"\n"+data.country+"\n"+data.postal_code);
   $(".LoadingImage").hide();
   */
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });}
function backto_company()
{
   $('#companyprofile').show();
   $('.main_contacts').hide();
}
 
</script>

<script type="text/javascript">
	 
		$(function(){
			
			var headheight = $('.header-navbar').outerHeight();
			var navbar = $('.remin-admin');
	});
	 
</script>

<script type="text/javascript">
  $(document).ready(function(){    
      $("a#mobile-collapse").click(function () {
          $('#pcoded.pcoded.iscollapsed').toggleClass('addlay');
    });
     
      $(".nav-left .data1 li.comclass a.com12").click(function () {
      $(this).parent('.nav-left .data1 li.comclass').find('ul.dropdown-menu1').slideToggle();
      $(this).parent('.nav-left .data1 li.comclass').siblings().find('ul.dropdown-menu1:visible').slideUp();      

    });
    
    // $(document).on( 'click', 'li.column-setting0', function() {
    //   $(this).children('ul.dropdown-menu.hide-dropdown01').slideToggle();
    // });

    // var $grid = $('.masonry-container').masonry({
    //   itemSelector: '.accordion-panel',
    //   percentPosition: true,
    //   columnWidth: '.grid-sizer' 
    // });

    // $(document).on( 'click', 'td.splwitch .switchery', function() {
    //   setTimeout(function(){ $grid.masonry('layout'); }, 600);
    // });

    // $(".basic-border1 input").keypress(function(){
    //     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    // });

    //   $(document).on( 'click', 'ol .nav-item a,.nav-item a', function() {
    //       setTimeout(function(){ $grid.masonry('layout'); }, 600);
    //   });

    //   $(document).on( 'change', '.main-pane-border1', function() {
    //     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    //   });

    //   $(document).on( 'click', '.btn', function() {
    //     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    //    });

    //   $(document).on( 'click', '.switching', function() {
    //     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    //    });

    //   $(document).on( 'click', '.service-table-client .checkall', function() {
    //     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    //    });

    //   $(document).on( 'click', '.service-table-client .switchery', function() {
    //     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    //    });

    //   setTimeout(function(){ $grid.masonry('layout'); }, 600);
    
      var test = $('body').height();
      $('test').scrollTop(test);

   });

  $(".client_request").click(function(){
    //alert('ok');
    $("#services_name").val($(this).data('service'));
    $("#service_user_id").val($(this).attr('id'));
    $("#notifications_id").val($(this).data('id'));

    console.log($(this).data('service'));
     console.log($(this).attr('id'));
      console.log($(this).data('id'));
    $("#manager_service_accept").modal('show');
  });
$("#enable_reassign").change(function(){
  if($(this).prop("checked"))
  {   $(".review_notes").hide();
     $("#reassign_selectbox").show();
  }else{
     $("#reassign_selectbox").hide();
     $(".review_notes").show();
  } 
  });

  $(".staff_request").click(function(){
    //alert('ok');
    $("#review_notification_id").val($(this).data('id'));
  });

  $("#accept_service").click(function(){
    //alert('ok');
    var services_name=$("#services_name").val();
   var user_id=$("#service_user_id").val();
   var id=$("#notifications_id").val();
   var data={'user_id':user_id,'service_name':services_name,'id':id}

   $.ajax({
       url: '<?php echo base_url();?>Sample/service_update/',
    type: "POST",
    data: data,
    success: function(data)  
    {
      if(data=='1'){
        $(".close_popup").trigger('click');
      }

    }
   });
  });


  $("#send_proposal").click(function(){
        var services_name=$("#services_name").val();
   var user_id=$("#service_user_id").val();
   var id=$("#notifications_id").val();
   var data={'user_id':user_id,'service_name':services_name,'id':id}

   $.ajax({
       url: '<?php echo base_url();?>Sample/proposal_add/',
    type: "POST",
    data: data,
    success: function(data)  
    {
      if(data!=' '){
        $(".close_popup").trigger('click');
        $(".proposal_success_message").show();
         $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");
         $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");
        setTimeout(function(){ $(".proposal_success_message").hide(); }, 3000);
      }
    }
   });
  });


$(".companyhouse_update").click(function(){

var id=$(this).attr('id');
//alert(id);
var companyname=$(this).data('companyname');
//alert(companyname);
var companynumber=$(this).data('companynumber');
//alert(companynumber);
var incorporationdate=$(this).data('incorporationdate');
//alert(incorporationdate);
var registeraddress=$(this).data('registeraddress');
//alert(registeraddress);
var companytype=$(this).data('companytype');
//alert(companytype);

var accounts_next_made_up_to=$(this).data('accounts_next_made_up_to');
//alert(companynumber);
var accounts_next_due=$(this).data('accounts_next_due');
//alert(incorporationdate);
var confirmation_next_made_up_to=$(this).data('confirmation_next_made_up_to');
//alert(registeraddress);
var confirmation_next_due=$(this).data('confirmation_next_due');
//alert(companytype);

var formData={'id':id,'companyname':companyname,'companynumber':companynumber,'incorporationdate':incorporationdate,'registeraddress':registeraddress,'companytype':companytype,'accounts_next_made_up_to':accounts_next_made_up_to,'accounts_next_due':accounts_next_due,'confirmation_next_made_up_to':confirmation_next_made_up_to,'confirmation_next_due':confirmation_next_due }

$.ajax({
    url: '<?php echo base_url(); ?>/client/companyhouse_update',
    type : 'POST',
    data : formData,                    
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {
        if(data=='1'){
          location.reload();
        }
      }
    });

});


$(document).on("click","#remove-notification,#archive-notification,#forward-notification",function(){
  var type=$(this).data('type');
  var id=$(this).data('id');
  var section=$(this).data('section');
  var users=$(this).data('user');

  console.log(id);
  console.log(section);
  console.log(users);
  if(type=="remove"){
    $("#confirm_button").removeAttr('class'); 
    $("#show_message").html('Do you want to remove notification');
    $("#confirm_button").addClass('btn btn-primary remove_notify');
    $("#notification_id").val(id);
    $("#notification_modal").modal('show');   
    $("#notification_type").val(type);
    $("#notification_section").val(section);
    $("#notification_users").val(users);
  }else if(type=="archive"){
    $("#confirm_button").removeAttr('class');
    $("#show_message").html('Do you want to archive notification');
    $("#confirm_button").addClass('btn btn-primary archive_notify');
    $("#notification_id").val(id);
    $("#notification_modal").modal('show');
    $("#notification_type").val(type);
    $("#notification_section").val(section);
    $("#notification_users").val(users);
  }else{
    $("#forward").removeAttr('class');
    $("#show_message").html('Do you want to forward notification');
    $("#forward").addClass('btn btn-primary forward_notify');
    $("#fwd_notification_id").val(id);
    $("#forward_task").modal('show');
    $("#fwd_notification_type").val(type);
    $("#fwd_notification_section").val(section);
    $("#fwd_notification_users").val(users);
  }
});




$(document).on('click','.remove_notify,.archive_notify',function(){
 //  alert('ok');
    var type   =$("#notification_type").val();
    var section=$("#notification_section").val();
    var users  =$("#notification_users").val();
    var id     =$("#notification_id").val();

  console.log(type);
    console.log(section);
    console.log(users);
    console.log(id);

    if(section=='company-house' && type=="remove"){

      console.log($("#remove-notification").attr('class'));
      console.log($("#remove-notification").parents('.company_notification').attr('class'));
      console.log( $("#remove-notification").parents('.company_notification').find('.companyhouse_update').attr('class'));
      $("#remove-notification").parents('.company_notification').find('.companyhouse_update').trigger('click');
    }else{
  
    var formData={'type':type,'section':section,'users':users,'id':id};
    $.ajax({
    url: '<?php echo base_url(); ?>Sample/notification_section',
    type : 'POST',
    data : formData,                    
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {
        if(data=='1'){
         /*****/    $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");
             $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");
           $(".LoadingImage").hide();
          //location.reload();
        }
      }
    });
  }
});



$(document).on('click','.forward_notify',function(){
    var worker=$("#workers").val();
    var task_id=$("#fwd_notification_id").val();
      var data = {};
      data['task_id'] = task_id;
      data['worker'] = worker;
      $(".LoadingImage").show();
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/update_assignees/",
               data: data,
               success: function(response) {
                  // alert(response); die();
                  $(".LoadingImage").hide();
               //$('#task_'+id).html(response);
             //  $('.task_'+id).html(response);
                    $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>sample/update_task_view/",
               data: {'id':task_id},
               success: function(response) {

                /*****/    $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");

                $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");
  $(".LoadingImage").hide();
                 //     $('#action_result').show();
                 //  $("#action_result .position-alert1").html('Success !!! Staff Assign have been changed successfully...');
                 // setTimeout(function(){ $('#action_result').hide();location.reload(); }, 1500);

                  }

                });



               
               },
            });
});
</script>

<?php $uri_seg_menu = $this->uri->segment('1'); 


//echo $this->uri->segment(2);
if($this->uri->segment(2)!='proposal' || $this->uri->segment(2)!='proposal_feedback'){
//if($_SESSION['roleId']=='4'){ // only for client login users.
//if($_SESSION['roleId']=='1' || $_SESSION['roleId']=='4' ){ //remove 12 and put 4 
if( strtolower($uri_seg_menu)=='tickets'){ // REMOVED BY AJITH  $uri_seg_menu=='task_list' || 
  ?>  
<?php

         if($_SESSION['roleId']==1)
        {
            $login_user_id=$_SESSION['userId'];
     

        $data['getallUser']=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();
        }
        if($_SESSION['roleId']==4)
        {
            $login_user_id=$_SESSION['userId'];
            $client_data=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and id=".$login_user_id." order by id DESC")->result_array();
           
             $firm_id=$client_data[0]['firm_admin_id'];
         
            $data['getallUser']=$this->db->query("SELECT * FROM user where crm_name!='' and id=".$firm_id." order by id DESC")->result_array();

        }
        $data['column_setting']=$this->Common_mdl->getallrecords('column_setting_new');
        $data['client'] =$this->db->query('select * from column_setting_new where id=1')->row_array();
        $data['feedback'] =$this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' "  )->result();



         $this->load->view('User_chat/chat_page_new',$data); 
      
       ?>

 <script type="text/javascript">
$(document).ready(function(){
  //alert('zzz');
//$('#main-chat #sidebar .user-box .userlist-box').trigger('click');
//$('#main-chat .chat-box').find('.chat-single-box .mini i').trigger('click');
//alert($('#main-chat .chat-box').find('.chat-single-box .mini').length+".trigger('click')");
//BY AJITH FOr show all chat box 
});
</script>
<?php } 
//} 
} ?>

<?php 


if($this->uri->segment(2)!='task_list'){
/** for admin login users **/
//if($_SESSION['roleId']=='1'){ 
if(strtolower($uri_seg_menu)!='user_chat'){

  if($_SESSION['roleId']=='1' || $_SESSION['roleId']=='4' || $_SESSION['roleId']=='6'){ 
	// if($uri_seg_menu!='user_chat' && $uri_seg_menu!='Firm_dashboard' && $uri_seg_menu!='leads' && $uri_seg_menu!='proposal') {
  //  if($uri_seg_menu=='Deadline_manager'){
		

	 if($_SESSION['roleId']==1)
        {
            $login_user_id=$_SESSION['userId'];
     

        $data['getallUser']=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();

         $user_staff_data=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();
        
        $leads_data=$this->db->query("SELECT * FROM leads where user_id=".$login_user_id." order by id DESC")->result_array();
        $taskdata=$this->db->query("SELECT * FROM add_new_task where create_by=".$login_user_id." order by id DESC")->result_array();
$assign='';$worker='';
$user_staff=array();
                foreach ($leads_data as $leads_key => $leads_value) {
                    if($leads_value['assigned']!=''){
                        $assign.=$leads_value['assigned'].",";
                    }

                }
                foreach ($taskdata as $task_key => $task_value) {
                    if($task_value['worker']!=''){
                        $worker.=$task_value['worker'].",";
                    }
                }
                foreach ($user_staff_data as $user_staff_key => $user_staff_value) {
                    //user_staff
                    array_push($user_staff,$user_staff_value['id']);
                   
                }
                $leadstask=array_values(array_unique(array_merge(array_filter(explode(',',$assign)),array_filter(explode(',', $worker)))));
                $newarray=array_values(array_unique(array_merge(array_filter($leadstask),array_filter($user_staff))));

                if(!empty($newarray)){
                $data['getallstaff']=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (".implode(',',$newarray).") order by id DESC")->result_array();
              }
              else
              {
                $data['getallstaff']='';
              }

               // echo "SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (".implode(',',$newarray).") order by id DESC";

        }
        if($_SESSION['roleId']==4)
        {
            $login_user_id=$_SESSION['userId'];
            $client_data=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and id=".$login_user_id." order by id DESC")->result_array();
           
             $firm_id=$client_data[0]['firm_admin_id'];
         
            $data['getalladmin']=$this->db->query("SELECT * FROM user where crm_name!='' and id=".$firm_id." order by id DESC")->result_array();

            /** extra added for staff shown **/
        $data['getallUser']=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();

         $user_staff_data=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();
        
        $leads_data=$this->db->query("SELECT * FROM leads where user_id=".$login_user_id." order by id DESC")->result_array();
        $taskdata=$this->db->query("SELECT * FROM add_new_task where create_by=".$login_user_id." order by id DESC")->result_array();
$assign='';$worker='';
$user_staff=array();
                foreach ($leads_data as $leads_key => $leads_value) {
                    if($leads_value['assigned']!=''){
                        $assign.=$leads_value['assigned'].",";
                    }

                }
                foreach ($taskdata as $task_key => $task_value) {
                    if($task_value['worker']!=''){
                        $worker.=$task_value['worker'].",";
                    }
                }
                foreach ($user_staff_data as $user_staff_key => $user_staff_value) {
                    //user_staff
                    array_push($user_staff,$user_staff_value['id']);
                   
                }
                $leadstask=array_values(array_unique(array_merge(array_filter(explode(',',$assign)),array_filter(explode(',', $worker)))));
                $newarray=array_values(array_unique(array_merge(array_filter($leadstask),array_filter($user_staff))));

                if(!empty($newarray)){
                $data['getallstaff']=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (".implode(',',$newarray).") order by id DESC")->result_array();
                }
                else
                {
                   $data['getallstaff']='';
                }
                /** for staff end **/

        }

        if($_SESSION['roleId']==6)
        {
            $login_user_id=$_SESSION['userId'];
            $client_data=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id=".$login_user_id." order by id DESC")->result_array();
           
             $firm_id=$client_data[0]['firm_admin_id'];
         
            $data['getalladmin']=$this->db->query("SELECT * FROM user where crm_name!='' and id=".$firm_id." order by id DESC")->result_array();

            $leads_data=$this->db->query("SELECT * FROM leads where find_in_set('".$login_user_id."',assigned)  order by id DESC")->result_array();
            $taskdata=$this->db->query("SELECT * FROM add_new_task where find_in_set('".$login_user_id."',worker) order by id DESC")->result_array();
            $for_assigned=array();
            foreach ($leads_data as $leads_key => $leads_value) {
               array_push($for_assigned, $leads_value['user_id']);
            }
            foreach ($taskdata as $task_key => $task_value) {
               array_push($for_assigned, $task_value['create_by']);
            }
            $newarray=array_values(array_unique($for_assigned));
            if(!empty($newarray)){
                $data['getallUser']=$this->db->query("SELECT * FROM user where id in (".implode(',',$newarray).")  order by id DESC")->result_array();
            }
            else
            {
                $data['getallUser']='';
            }

        }

        // if($this->uri->segment(2)=='chat_page'){
        //     $this->load->view('User_chat/chat_page_admin',$data); 
        // }

        /** for newly added 18-05-2018 **/
    //  }
	}

}}

/** 09-07-2018 for permission **/
$this->load->view('includes/footer_permission_script');

/** end of permission **/

?>

<script type="text/javascript">
  $(document).ready(function(){
      $('.dataTables_wrapper').each(function(){
var datatable_id = $(this).attr('id');
var datatable_id_cur = datatable_id.replace('_wrapper','');
if($(this).find('.dataTables_scrollBody').prop("scrollHeight") > $(this).find('.dataTables_scrollBody').height()){
    $(this).find('.dataTables_scrollHeadInner').css({"padding-right": "21px", "min-width": "calc(100% - 21px)"}); 
}
$('#'+ datatable_id_cur).DataTable().columns.adjust();
setTimeout(function(){$('#'+ datatable_id_cur).DataTable().columns.adjust();}, 1000);
});


$('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
var target_id = $(this).attr('href');
var href = target_id.substring(target_id.indexOf("#") + 1); 
var datatable_id = $("#"+href).find('table').attr('id');
var datatable_id_cur = datatable_id.replace('_wrapper','');
if($(this).find('.dataTables_scrollBody').prop("scrollHeight") > $(this).find('.dataTables_scrollBody').height()){
    $(this).find('.dataTables_scrollHeadInner').css({"padding-right": "21px", "min-width": "calc(100% - 21px)"}); 
}
$('#'+ datatable_id_cur).DataTable().columns.adjust();
setTimeout(function(){$('#'+ datatable_id_cur).DataTable().columns.adjust();}, 1000);

});

});
</script>

<script type="text/javascript">
  $(document).ready(function(){
      $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });

      // $('.modal').modal({backdrop: 'static', keyboard: false,show: false});

    $(document).on('click', '.modal .hasDatepicker', function(){
    $('.ui-datepicker').addClass('uidate_visible');
    });

    $(document).on('click', '.modal button[data-dismiss="modal"]', function(){
    $('.ui-datepicker').removeClass('uidate_visible');
    });
  });

  $(document).ready(function(){
      $(".modal-alertsuccess").each(function(){ 
          if($(this).find('.newupdate_alert').length < 1){
            $(this).wrapInner( "<div class='newupdate_alert'></div>");
          } 
      });
  });
</script>