<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1.0"/>
<meta name="format-detection" content="telephone=no">
  <title></title>
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
</head>
<body>

<div id="m_5349442688376831455m_-3036873862170083609wrapper" dir="ltr" style="background-color:#f7f7f7;margin:0;padding:70px 0 70px 0;width:100%">
  <span class="HOEnZb"><font color="#888888">
  </font></span>
  <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tbody>
      <tr>
        <td align="center" valign="top">
          <div id="m_5349442688376831455m_-3036873862170083609template_header_image">
          </div>
          <span class="HOEnZb"><font color="#888888">
          </font></span><span class="HOEnZb"><font color="#888888">
          </font></span>
          <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_5349442688376831455m_-3036873862170083609template_container" style="background-color:#ffffff;border:1px solid transparent;border-radius:3px!important">
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_5349442688376831455m_-3036873862170083609template_header" style="background-color:transparent;border-radius:3px 3px 0 0!important;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;background-image:none;background-color: rgba(0,0,0,0.9);border-bottom: 2px solid orange;;background-repeat: no-repeat;">
                    <tbody>
                      <tr>
                        <td id="m_5349442688376831455m_-3036873862170083609header_wrapper" style="padding:20px 25px;display:block;text-align:center;">
                        <img width="200" src="http://remindoo.org/CRMTool/assets/images/logo.png" alt="school-logo" style="background: transparent;border-radius: 5px;/* float: left; */display: inline-block;vertical-align: middle;">
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_5349442688376831455m_-3036873862170083609template_body">
                    <tbody>
                      <tr>
                        <td valign="top" id="m_5349442688376831455m_-3036873862170083609body_content" style="background-color:#ffffff">
                          <table border="0" cellpadding="20" cellspacing="0" width="100%">
                            <tbody>
                              <tr>
                                <td valign="top" style="padding:48px">
                                  <div id="m_5349442688376831455m_-3036873862170083609body_content_inner" style="color:#636363;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:14px;line-height:150%;text-align:left">                                  
                                  <p style="padding: 10px 0;font-family:'Lato';font-size:17px;font-weight: normal;">Hi<span class="blue-name"><a href="javascript:;" style="    color: #00aeef;text-decoration: none; padding-left: 3px;text-transform: capitalize;"><?php echo $username?></a></span></p>
                                    <h2 style="color:#555;display:block;font-family:lato;font-size:16px;line-height:130%;margin:16px 0 8px;text-align:left;text-transform:capitalize">we got a request to reset your password.
                                    </h2> 
                                    <div class="re-btn btn btn-primary" style="padding: 15px;text-align: center;">
                                      <?php echo $email_content;?>
                                    </div> 
                                    <h2 style="color:#555;display:block;font-family:lato;font-size:16px;line-height:130%;margin:16px 0 8px;text-align:left;text-transform:capitalize">if you ignore this message, your password won't be changed.
                                    </h2>  
                                        
                                    <!-- <h2 style="color:#198428;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:16px 0 8px;text-align:left">Customer
                                      details
                                    </h2>
                                    <ul>
                                      <li>
                                        <strong>Email address:</strong> <span class="m_5349442688376831455m_-3036873862170083609text" style="color:#3c3c3c;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif"><a href="mailto:school1@gmail.com" target="_blank">school1@gmail.com</a></span>
                                      </li>
                                    </ul> -->
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <span class="HOEnZb"><font color="#888888">
                  </font></span>
                  <table border="0" cellpadding="10" cellspacing="0" width="600" id="m_5349442688376831455m_-3036873862170083609template_footer">
                    <tbody>
                      <tr>
                        <td valign="top" style="padding:0">
                          <span class="HOEnZb"><font color="#888888">
                          </font></span>
                          <table border="0" cellpadding="10" cellspacing="0" width="100%">
                            <tbody>
                              <tr> 
                                <td colspan="2" valign="middle" id="m_5349442688376831455m_-3036873862170083609credit" style="padding:10px 15px 10px;border:0;color:#fff;background: #353535;font-family:Arial;font-size:15px;line-height:125%;text-align:center;text-transform: captialize;font-family: 'Lato', sans-serif !important">
                                  <p style="margin-top: 10px;"><span style="color: #999999">Copyright © 2018 Remindoo.</span></p>
                                  <span class="HOEnZb"><font color="#888888">
                                  </font></span>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <span class="HOEnZb"><font color="#888888">
                          </font></span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <span class="HOEnZb"><font color="#888888">
                  </font></span>
                </td>
              </tr>
            </tbody>
          </table>
          <span class="HOEnZb"><font color="#888888">
          </font></span>
        </td>
      </tr>
    </tbody>
  </table>
</div>
</body>
</html>
