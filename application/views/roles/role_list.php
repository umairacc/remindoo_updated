<?php $this->load->view('includes/header');?>
<style>
  button.btn.btn-info.btn-lg.newonoff
  {
    padding: 3px 10px;
    height: initial;
    font-size: 15px;
    border-radius: 5px;
  }
  .dropdown-content
  {
      display: none;
      position: absolute;
      background-color: #fff;
      min-width: 86px;
      overflow: auto;
      box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
      z-index: 1;
      left: -92px;
      width: 150px;
  }
  tfoot
  {
      display: table-header-group;
  }

  /* 03122020 */
  
div#role_list_wrapper {
  display: flex;
  flex-wrap: wrap;
  align-items: center;
}

div#role_list_filter {
    margin-left: auto;
    margin-bottom: 0;
    width: 26%;
    padding-right: 10px;
}

div#add_new_role {
    margin-left: auto;
    margin-right: 30px;
    width: 11%;
}

div#add_new_role a{
    display: block;
    background: #eee;    
    border-radius: 0px;
    transition: 0.3s ease all;
    font-weight: 600;
    font-size: 13px;
    font-family: 'Roboto', sans-serif;    
    padding: 8px 20px;    
    cursor: pointer;
}

div#add_new_role a:hover{
  background: #00a2e8;
  color: #fff !important;
}

div#role_list_length {
    width: 60%;
    margin-bottom: 0;
    padding-left: 10px;
}
</style>
  <!-- show info  -->
  <div class="modal-alertsuccess alert info_popup" style="display:none;">
    <div class="newupdate_alert">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
     <div class="pop-realted1">
        <div class="position-alert1 info-text">
         </div>
        </div>
     </div>
  </div>
<!-- end show info  -->
<div class="pcoded-content card-removes">
  <div class="pcoded-inner-content">
    <!-- Main-body start -->
    <div class="main-body">
      <div class="page-wrapper">
          <!-- Page body start -->
          <div class="page-body">
            <div class="row">
              <div class="col-sm-12">
                <!-- Register your self card start -->
                <div class="card">
                  <!-- all-clients -->
                  <div class="addarchieves floating_set fr-stflist add-role-assign">
                    <div class="floating_set">
                      <div class="deadline-crm1 floating_set">
                        <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard client-top-nav">
                          <li class="nav-item ">
                            <a href="<?php echo base_url()?>Role_Assign" class="nav-link active" data-tag="home">Roles</a>
                            <div class="slide"></div>
                          </li>
                           <?php if( $_SESSION['permission']['User_and_Management']['create']==1 ) { ?>
                          <li class="nav-item ">
                            <a href="<?php echo base_url()?>Role_Assign/add_role" class="nav-link" data-tag="home">New Role</a>
                            <div class="slide"></div>
                          </li>
                          <?php } ?>
                          <li class="nav-item ">
                              <a class="nav-link"  data-tag="home" href="<?php echo base_url()?>user/staff_list">Users</a>
                              <div class="slide"></div>
                          </li>
                          <li class="nav-item ">
                            <a href="<?php echo base_url()?>team/organisation_tree" class="nav-link " data-tag="home">Groups</a>
                            <div class="slide"></div>
                          </li>
                        </ul>
                      </div>
                      <div class="tab-content">
                        <div id="allusers" class="tab-pane fade in active">
                           <div class="client_section3 table-responsive floating_set ">
                              <div id="status_succ"></div>
                              <div class="all-usera1">
                                 <table class="table role_table client_table1 text-center display nowrap" id="role_list" cellspacing="0" width="100%">
                                    <thead>
                                    <tr class="text-uppercase">
                                      <th>S.NO</th>
                                      <th>Roles</th>
                                      <th>Action</th>
                                    </tr>
                                    </thead>                                                  
                                    <tbody>
                                      <?php
                                       $k=1;
                                      
                                      foreach($roles as $role){

                                        $Isdeleteable = $this->db->query("select id from user where firm_id =".$_SESSION['firm_id']." and role=".$role['id']." ")->result_array();
                                        $disable = "";
                                        
                                        if(! empty( $Isdeleteable ) )
                                        {
                                          $disable = "disabled";
                                        }
                                        ?>
                                      <tr>
                                        <td><?php echo $k ?></td>
                                        <td><?php echo $role['role']; ?></td>
                                        <td class="role-table-wrap">
                                        <?php if( $_SESSION['permission']['User_and_Management']['edit']==1 ) {?> 
                                          <a href="<?php echo base_url(); ?>Role_Assign/edit_role/<?php echo $role['id'] ?>">
                                            <i class="icofont icofont-edit close_role" aria-hidden="true" title="Edit"></i>
                                          </a>
                                        <?php } ?>
                                        <?php if( $_SESSION['permission']['User_and_Management']['delete']==1 ) {?>
                                          <a href="javascript:void(0)" data-id="<?php echo $role['id']?>" class="DeleteRole <?php echo $disable; ?>" >
                                            <i class="icofont icofont-ui-delete" aria-hidden="true"></i>
                                          </a>
                                        <?php } ?>
                                         </td>
                                      </tr>
                                      <?php $k++; } ?>
                                    </tbody>
                                 </table>
                                 <input type="hidden" class="rows_selected" id="select_staff_count" >     
                              </div>
                           </div>
                        </div>
                        <!-- home-->
                      </div>
                    </div>
                    <!-- admin close -->
                  </div>
                  <!-- Register your self card end -->
                </div>
              </div>
            </div>
            <!-- Page body end -->
          </div>
        </div>
      </div>
    </div>
</div>


<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.tagsinput.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>   
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/js/jquery.tagsinput.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script>
$(document).ready(function(){

  $(document).on('click','#close',function(e){
    $('.alert-success1').hide();
    return false;
  });
  
  $(document).on('click','#close',function(e)
   {
     $('.alert-success').hide();
     return false;
   });

  $('.DeleteRole').click(function(){    
    var id = $(this).attr('data-id');
    var action = function(){    
      $.ajax({
        url:"<?php echo base_url()?>Role_Assign/DeleteRole",
        data:{'id':id},
        type:'post',
        beforeSend:function(){Show_LoadingImg();$('#Confirmation_popup').modal('hide');},
        success:function(data){
          Hide_LoadingImg();
          $('.info_popup .info-text').html("Role deleted successfully..!");
          $('.info_popup').show();
          setTimeout(function(){
            location.reload();
          },1500);
        }
      });    
    };
    Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to delete this role ?'});  
    $('#Confirmation_popup').modal('show');
  });
  $('#role_list').DataTable(
    {
      "pageLength": "<?php echo get_firm_page_length() ?>",
      order: [],//ITS FOR DISABLE SORTING
      columnDefs: 
      [
        {"orderable": false,"targets": '_all'}
      ]
    });

  $('<div id="add_new_role" class="add-new-role"><a href="/remindoo/Role_Assign/add_role" data-tag="home">Add new role</a></div>').insertAfter('#role_list_filter');  
});
  
</script>