<?php
   $role = $this->Common_mdl->getRole($_SESSION['id']);
   function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
   }

   function calculate_test($a,$b){
$difference = $b-$a;

$second = 1;
$minute = 60*$second;
$hour   = 60*$minute;

$ans["hour"]   = floor(($difference)/$hour);
$ans["minute"] = floor((($difference)%$hour)/$minute);
$ans["second"] = floor(((($difference)%$hour)%$minute)/$second);
//echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

$test=$ans["hour"].":".$ans["minute"].":".$ans["second"];
return $test;
}
function time_to_sec($time) {
list($h, $m, $s) = explode (":", $time);
$seconds = 0;
$seconds += (intval($h) * 3600);
$seconds += (intval($m) * 60);
$seconds += (intval($s));
return $seconds;
}
function sec_to_time($sec) {
return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}    
?>

<?php 
if($_SESSION['role']==6 || $_SESSION['role']==5)
{
  ?>
  <style type="text/css">
    /** 04-07-2018 rs **/
.for_user_permission_assign{
  display: none;
}
.for_user_permission_delete
{
  display: none;
}
/** end of 04-07-2018 **/
  </style>
  <?php
}
?>
<style>
/** 28-06-2018 **/
 .add_class{
    display: none;
  }

.time{
      padding: 8px;
    font-weight: bold;
}
.new_play_stop{
      width: 20px;
    height: 20px;
    background: #000;
    color: #fff;
    line-height: initial;
    text-align: center;
    font-size: 15px;
    vertical-align: top;
    border-radius: 50%;
    margin: 7px 0 0 0;
}

   /** 04-06-2018 **/
   select.close-test-04{
    display: none;
   }
   /** 04-06-2018 **/
   span.demo {
   padding: 0 10px;
   }
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   img.user_imgs {
   width: 38px;
   height: 38px;
   border-radius: 50%;
   display: inline-block;
   }


a.adduser1 {
    background: #38b87c;
    color: #fff;
    padding: 3px 7px 3px;
    border-radius: 6px;
    vertical-align: middle;
    display: inline-block;
}
   #adduser label {
   text-transform: capitalize;
   font-size: 14px;
   }
  span.hours-left {
    background: #f76480;
    color: #fff;
    padding: 3px 10px;
    border-radius: 3px;
    margin-right: 15px;
}
   .dropdown12 button.btn.btn-primary {
   background: #ccc;
   color: #555;
   font-size: 13px;
   padding: 4px 10px 4px;
   margin-top: -2px;
   border-color:transparent;
   }
   select + .dropdown:hover .dropdown-menu
   {
   display: none;
   }
   select + .dropdown12 .dropdown-menu
   {
   padding: 0 !important;
   }
   select + .dropdown12 .dropdown-menu a {
   color: #000 !important;
   padding: 7px 10px !important;
   border: none !important;
   font-size: 14px;
   }
   select + .dropdown12 .dropdown-menu li {
   border: none !important;
   padding: 0px !important;
   }
   select + .dropdown12 .dropdown-menu a:hover {
   background:#4c7ffe ;
   color: #fff !important;
   }
   span.created-date {
   color: gray;
   padding-left: 10px;
   font-size: 13px;
   font-weight: 600;
   }
   span.created-date i.fa.fa-clock-o {
   padding: 0 5px;
   }
   .dropdown12 span.caret {
   right: -2px;
   z-index: 99999;
   border-top: 4px solid #555;
   border-left: 4px solid transparent;
   border-right: 4px solid transparent;
   border-bottom: 4px solid transparent;
   top: 12px;
   position: relative;
   }
   span.timer {
   padding: 0 10px;
   }
   body {
   font-family:"Arial", Helvetica, sans-serif;
   text-align: center;
   }
   #controls{
   font-size: 12px;
   }
   #time {
   font-size: 150%;
   }
  
</style>
<!-- dynamic values passed in css -->

<style type="text/css">
  <?php 

 ?>
</style>
<?php 

     $for_user_edit_per=array();
     if(count($for_user_edit_permission)>0){
     foreach ($for_user_edit_permission as $edit_per_key => $edit_per_value) {
      array_push($for_user_edit_per, $edit_per_value['id']);
     }
     }
?>
<style>

 td.details-control {
    background: url('http://www.datatables.net/examples/resources/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('http://www.datatables.net/examples/resources/details_close.png') no-repeat center center;
}

</style>


<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.min.js"></script>
<script src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
      <?php
                                            foreach ($task_list as $tre_key => $tre_value) {
                                            $time=json_decode($tre_value['counttimer']);
                                            $time_start_date =$tre_value['time_start_date'];
                                            $time_end_date=$tre_value['time_end_date'];
                                            $hours=0;
                                            $mins=0;
                                            $sec=0;
                                            $pause='';
                                            if($_SESSION['role']==6)
                                            {
                                                $individual_timer=$this->db->query("select * from individual_task_timer where user_id=".$_SESSION['id']." and task_id=".$tre_value['id']." ")->row_array();
                                            if(count($individual_timer)>0)
                                            {
                                              if($individual_timer['time_start_pause']!=''){
                                                $res=explode(',',$individual_timer['time_start_pause']);
                                                //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
                                                $res1=array_chunk($res,2);
                                                $result_value=array();
                                                $pause='on';
                                                foreach($res1 as $rre_key => $rre_value)
                                                {
                                                   $abc=$rre_value;
                                                   if(count($abc)>1){
                                                   if($abc[1]!='')
                                                   {
                                                      $ret_val=calculate_test($abc[0],$abc[1]);
                                                      array_push($result_value, $ret_val) ;
                                                   }
                                                   else
                                                   {
                                                    $pause='';
                                                      $ret_val=calculate_test($abc[0],time());
                                                       array_push($result_value, $ret_val) ;
                                                   }
                                                  }
                                                  else
                                                  {
                                                    $pause='';
                                                      $ret_val=calculate_test($abc[0],time());
                                                       array_push($result_value, $ret_val) ;
                                                  }
                                                }

                                                $time_tot=0;
                                                 foreach ($result_value as $re_key => $re_value) {
                                                    $time_tot+=time_to_sec($re_value) ;
                                                 }
                                                 $hr_min_sec=sec_to_time($time_tot);
                                                 $hr_explode=explode(':',$hr_min_sec);
                                                 $hours=(int)$hr_explode[0];
                                                 $min=(int)$hr_explode[1];
                                                 $sec=(int)$hr_explode[2]; 
                                                }
                                                else
                                                {
                                                  $hours=0;
                                                  $min=0;
                                                  $sec=0;
                                                  $pause='on';
                                                }
                                            }
                                            else
                                            {
                                                $hours=0;
                                                $min=0;
                                                $sec=0;
                                                $pause='on';
                                            }
                                            }
                                            else
                                            {
                                            /** non staff members shown timer **/
                                            $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$tre_value['id']." ")->result_array();
                                            $pause_val='on';
                                            $pause='on';
                                            $for_total_time=0;
                                            if(count($individual_timer)>0){
                                            foreach ($individual_timer as $intime_key => $intime_value) {
                                            $its_time=$intime_value['time_start_pause'];
                                            $res=explode(',', $its_time);
                                              $res1=array_chunk($res,2);
                                            $result_value=array();
                                            //  $pause='on';
                                            foreach($res1 as $rre_key => $rre_value)
                                            {
                                               $abc=$rre_value;
                                               if(count($abc)>1){
                                               if($abc[1]!='')
                                               {
                                                  $ret_val=calculate_test($abc[0],$abc[1]);
                                                  array_push($result_value, $ret_val) ;
                                               }
                                               else
                                               {
                                                $pause='';
                                                $pause_val='';
                                                  $ret_val=calculate_test($abc[0],time());
                                                   array_push($result_value, $ret_val) ;
                                               }
                                              }
                                              else
                                              {
                                                $pause='';
                                                $pause_val='';
                                                  $ret_val=calculate_test($abc[0],time());
                                                   array_push($result_value, $ret_val) ;
                                              }
                                            }
                                            // $time_tot=0;
                                             foreach ($result_value as $re_key => $re_value) {
                                                //$time_tot+=time_to_sec($re_value) ;
                                                $for_total_time+=time_to_sec($re_value) ;
                                             }

                                            }
                                            //echo $for_total_time."val";
                                            $hr_min_sec=sec_to_time($for_total_time);
                                             $hr_explode=explode(':',$hr_min_sec);
                                              $hours=(int)$hr_explode[0];
                                              $min=(int)$hr_explode[1];
                                             $sec=(int)$hr_explode[2];

                                            }
                                            else
                                            {
                                              $hours=0;
                                              $min=0;
                                              $sec=0;
                                              $pause='on';
                                            }
                                        }
                                    ?>
                                  <input type="hidden" name="trhours_<?php echo $tre_value['id'];?>" id="trhours_<?php echo $tre_value['id'];?>" value='<?php echo $hours; ?>' >
                                  <input type="hidden" name="trmin_<?php echo $tre_value['id'];?>" id="trmin_<?php echo $tre_value['id'];?>" value='<?php echo $min;?>' >
                                  <input type="hidden" name="trsec_<?php echo $tre_value['id'];?>" id="trsec_<?php echo $tre_value['id'];?>" value='<?php echo $sec; ?>' >
                                  <input type="hidden" name="trmili_<?php echo $tre_value['id'];?>" id="trmili_<?php echo $tre_value['id'];?>" value='0' >
                                  <input type="hidden" name="trpause_<?php echo $tre_value['id'];?>" id="trpause_<?php echo $tre_value['id'];?>" value="<?php echo $pause; ?>" >
                                  <?php                             }                                  ?>
<table id="alltask" class="display nowrap" cellspacing="0" width="100%">
    <thead>
        <tr>
           <th>

</th>
<th> <div class="checkbox-fade fade-in-primary"><label><input type="checkbox" id="select_alltask">
<span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  </label>
</div> 
</th>
<th>Timer
<img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
<div class="sortMask"></div> 
<select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
</th>                                     
<th>Subject
<img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
<div class="sortMask"></div> 
<select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
</th>
<th class="add_class" style="display: none;">Subject
<img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
<div class="sortMask"></div> 
<select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
</th>                                    
<th>Start Date 
<img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
<div class="sortMask"></div> 
<select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
</th>
<th>Due Date
<img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
<div class="sortMask"></div> 
<select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
</th>
<th>Status
<img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
<div class="sortMask"></div> 
<select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
</th>
<th>Priority
<img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
<div class="sortMask"></div> 
<select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
</th>
<th>Tag 
<img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
<div class="sortMask"></div> 
<select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
</th>
<th>Assignto 
<img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
<div class="sortMask"></div> 
<select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
</th>
<th style="display: none;">Assignto 
<img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
<div class="sortMask"></div> 
<select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
</th>
<th>Actions  
</th>
<th>Due                                      
</th>
        </tr>
    </thead>
    <tbody>
<?php 
foreach ($task_list as $key => $value) {

  if($value['related_to']!='magic task'){

error_reporting(0);                                   
  if($value['start_date']!='' && $value['end_date']!=''){    
    $start  = date_create(implode('-', array_reverse(explode('-', $value['start_date']))));
    $end  = date_create(implode('-', array_reverse(explode('-', $value['end_date']))));
    $diff    = date_diff ( $start, $end );
    $y =  $diff->y;
    $m =  $diff->m;
    $d =  $diff->d;
    $h =  $diff->h;
    $min =  $diff->i;
    $sec =  $diff->s;
  }else{
    $y =  0;
    $m =  0;
    $d =  0;
    $h =  0;
    $min = 0;
    $sec =  0;
  }

$date_now = date("Y-m-d"); // this format is string comparable                                   
$d_rec = date('Y-m-d',strtotime($value['end_date']));
if ($date_now > $d_rec) {                                     
  $d_val = "EXPIRED";
}else{
  $d_val = $y .' years '. $m .' months '. $d .' days '. $h .' Hours '. $min .' min '. $sec .' sec ';
}
if($value['worker']=='')
{
$value['worker'] = 0;
}
$staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
if($value['task_status']=='notstarted')
{
$percent = 0;
$stat = 'Not Started';
} if($value['task_status']=='inprogress')
{
$percent = 25;
$stat = 'In Progress';
} if($value['task_status']=='awaiting')
{
$percent = 50;
$stat = 'Awaiting for a feedback';
} if($value['task_status']=='testing')
{
$percent = 75;
$stat = 'Testing';
} if($value['task_status']=='complete')
{
$percent = 100;
$stat = 'Complete';
}
$exp_tag = explode(',', $value['tag']);
$explode_worker=explode(',',$value['worker']);
/** new 12-06-2018 **/
$explode_team=explode(',',$value['team']);
$explode_department=explode(',',$value['department']);
/** end of 12-06-2018 **/
?>


        <tr data-key-1="Value 1" data-key-2="Value 2" id="<?php echo $value['id']; ?>" class="count_section">
            <td class="details-control" id="<?php echo $value['id']; ?>"></td>
            <td> <div class="checkbox-fade fade-in-primary">
            <label>
            <input type="checkbox" class="alltask_checkbox" data-alltask-id="<?php echo $value['id'];?>">
            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
            </label>
            </div> </td>
            <td>  <?php echo date('Y-m-d H:i:s', $value['created_date']);?></td>
            <td><a href="<?php echo base_url().'user/task_details/'.$value['id'];?>" target="_blank"><?php echo ucfirst($value['subject']);?></a></td>
            <td class="add_class"> <?php echo ucfirst($value['subject']);?></td>
            <td><?php echo $value['start_date'];?></td>
            <td><?php echo $value['end_date'];?></td>
            <td id="status_<?php echo $value['id'];?>"><?php echo $stat;?></td>
            <td> <?php echo $value['priority'];?></td>
            <td> <?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                                      echo $tagName = $this->Common_mdl->getTag($exp_tag_value).' ';
                                      }?></td>
            <td class="user_imgs" id="task_<?php echo $value['id'];?>">
            <span class="task_<?php echo $value['id'];?>">
            <?php
            foreach($explode_worker as $key => $val){     
            $getUserProfilepic = $this->Common_mdl->getUserProfilepic($val);
            ?>
            <img src="<?php echo $getUserProfilepic;?>" alt="img">
            <?php } ?>
            <a href="javascript:;" data-toggle="modal" data-target="#adduser_<?php echo $value['id'];?>" class="adduser1 per_assigne_<?php echo $value['id']; ?>"><i class="fa fa-plus"></i></a>
            </span></td>
            <td class="user_imgs" style="display: none;">                                     
              <?php
              $username=array();
              foreach($explode_worker as $key => $val){     
              $getUserProfilename = $this->Common_mdl->getUserProfileName($val);
              array_push($username,$getUserProfilename);
              } 
              echo implode(',', $username);
              ?>       
           </td>
            <td>  <?php //echo $value['worker'];?>
                                      <p class="action_01 per_action_<?php echo $value['id']; ?>">  
                                      <?php if($role!='Staff'){?>   

                                      <a href="#" onclick="return alltask_delete('<?php echo $value['id'];?>');"><i class="fa fa-trash fa-6 deleteAllTask" aria-hidden="true" ></i></a>
                                      <?php } ?>
                                      <a href="<?php echo base_url().'user/update_task/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>

                                      <?php 
                                      if($value['task_status']=='archive'){ ?> 
                                        <a href="javascript:;" id="<?php echo $value['id'];?>" class="archieve_click" onclick="archieve_click(this)" data-status="unarchive" data-toggle="modal" data-target="#my_Modal">
                                      <i class="fa fa-archive archieve_click"  aria-hidden="true" title="unarchive"></i></a>
                                      <?php }else{ ?>
                                        <a href="javascript:;" id="<?php echo $value['id'];?>" class="archieve_click" onclick="archieve_click(this)" data-status="archive" data-toggle="modal" data-target="#my_Modal">
                                      <i class="fa fa-archive archieve_click"  aria-hidden="true" title="archive"></i></a>
                                       

                                      <?php } ?>
                                      </p>
                                      </td>
             <td> <span class="hours-left"><?php
                                      //echo $value['timer_status'];
                                      if($value['timer_status']!=''){ echo convertToHoursMins($value['timer_status']); }else{ echo '0'; }?> hours<?php //echo $value['task_status'];?></span>
                                      <!--  <select>  <option>Normal</option>
                                      <option>Normal</option> </select> -->
                                    
                                    <?php if($_SESSION['role']==6 && $value['review_status']!=1){?> 
                                     <button data-id="<?=$value[id]?>" type="button" class="btn btn-primary sendtoreview">Request To Review</button>
                                     <?php } ?>

                                      <select name="task_status" id="task_status" class="task_status per_taskstatus_<?php echo $value['id']; ?>" data-id="<?php echo $value['id'];?>">
                                      <option value="">Select Status</option>
                                      <option value="notstarted" <?php if($value['task_status']=='notstarted'){ echo 'selected="selected"'; }?>>Not started</option>
                                      <option value="inprogress" <?php if($value['task_status']=='inprogress'){ echo 'selected="selected"'; }?>>In Progress</option>
                                      <option value="awaiting" <?php if($value['task_status']=='awaiting'){ echo 'selected="selected"'; }?>>Awaiting Feedback</option>
                                      <option value="testing" <?php if($value['task_status']=='testing'){ echo 'selected="selected"'; }?>>Testing</option>
                                      <option value="complete" <?php if($value['task_status']=='complete'){ echo 'selected="selected"'; }?>>Complete</option>
                                      </select>

                                      <!-- priority -->

                                      <select name="task_priority" id="task_priority" class="task_priority per_taskpriority_<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>">
                                      <option value="">By Priority</option>
                                      <option value="low" <?php if(isset($value['priority']) && $value['priority']=='low') {?> selected="selected"<?php } ?>>Low</option>
                                      <option value="medium" <?php if(isset($value['priority']) && $value['priority']=='medium') {?> selected="selected"<?php } ?>>Medium</option>
                                      <option value="high" <?php if(isset($value['priority']) && $value['priority']=='high') {?> selected="selected"<?php } ?>>High</option>
                                      <option value="super_urgent" <?php if(isset($value['priority']) && $value['priority']=='super_urgent') {?> selected="selected"<?php } ?>>Super Urgent</option>
                                      </select>

                                      <!-- priority -->

                                      <select name="test_status" id="test_status" class="close-test-04 test_status" data-id="<?php echo $value['id'];?>">
                                      <option value="open" <?php if($value['test_status']=='open'){ echo 'selected="selected"'; }?>>Open</option>
                                      <option value="onhold" <?php if($value['test_status']=='onhold'){ echo 'selected="selected"'; }?>>On Hold</option>
                                      <option value="resolved" <?php if($value['test_status']=='resolved'){ echo 'selected="selected"'; }?>>Resolved</option>
                                      <option value="closed" <?php if($value['test_status']=='closed'){ echo 'selected="selected"'; }?>>Closed</option>
                                      <option value="duplicate" <?php if($value['test_status']=='duplicate'){ echo 'selected="selected"'; }?>>Duplicate</option>
                                      <option value="invalid" <?php if($value['test_status']=='invalid'){ echo 'selected="selected"'; }?>>Invalid</option>
                                      <option value="wontfix" <?php if($value['test_status']=='wontfix'){ echo 'selected="selected"'; }?>>Wontfix</option>
                                      </select>
               
                 <span class="created-date">
                 <i class="fa fa-clock-o"></i>Created <?php echo $value['start_date'];?></span>
                 <span class="timer"><?php echo $d_val;?> </span>
                 <span class="demo" id="demo_<?php echo $value['id'];?>"></span>
                                                        
                                                         
                    <!-- for new timer -->
                  <?php
                  $time=json_decode($value['counttimer']);
                  $time_start_date =$value['time_start_date'];
                  $time_end_date=$value['time_end_date'];
                  $hours=0;
                  $mins=0;
                  $sec=0;
                  $pause='';

                  /** for task timer **/
                  if($_SESSION['role']==6)
                  {

                  $individual_timer=$this->db->query("select * from individual_task_timer where user_id=".$_SESSION['id']." and task_id=".$value['id']." ")->row_array();
                  if(count($individual_timer)>0)
                  {
                       if($individual_timer['time_start_pause']!=''){
                    $res=explode(',',$individual_timer['time_start_pause']);
                    //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
                    $res1=array_chunk($res,2);
                    $result_value=array();
                    $pause='on';
                    foreach($res1 as $rre_key => $rre_value)
                    {
                       $abc=$rre_value;
                       if(count($abc)>1){
                       if($abc[1]!='')
                       {
                          $ret_val=calculate_test($abc[0],$abc[1]);
                          array_push($result_value, $ret_val) ;
                       }
                       else
                       {
                        $pause='';
                          $ret_val=calculate_test($abc[0],time());
                           array_push($result_value, $ret_val) ;
                       }
                      }
                      else
                      {
                        $pause='';
                          $ret_val=calculate_test($abc[0],time());
                           array_push($result_value, $ret_val) ;
                      }


                    }


                    $time_tot=0;
                     foreach ($result_value as $re_key => $re_value) {
                        $time_tot+=time_to_sec($re_value) ;
                     }
                     $hr_min_sec=sec_to_time($time_tot);
                     $hr_explode=explode(':',$hr_min_sec);
                     $hours=(int)$hr_explode[0];
                     $min=(int)$hr_explode[1];
                     $sec=(int)$hr_explode[2];
                     


                    }
                    else
                    {
                      $hours=0;
                      $min=0;
                      $sec=0;
                    }
                  }
                  //echo $hours."--".$min."--".$sec;
                  }  
                  /** end of task timer **/
                  else {

                  $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$tre_value['id']." ")->result_array();
                  $pause_val='on';
                  $pause='on';
                  $for_total_time=0;
                  if(count($individual_timer)>0){
                  foreach ($individual_timer as $intime_key => $intime_value) {
                  $its_time=$intime_value['time_start_pause'];
                  $res=explode(',', $its_time);
                    $res1=array_chunk($res,2);
                  $result_value=array();
                  //  $pause='on';
                  foreach($res1 as $rre_key => $rre_value)
                  {
                     $abc=$rre_value;
                     if(count($abc)>1){
                     if($abc[1]!='')
                     {
                        $ret_val=calculate_test($abc[0],$abc[1]);
                        array_push($result_value, $ret_val) ;
                     }
                     else
                     {
                      $pause='';
                      $pause_val='';
                        $ret_val=calculate_test($abc[0],time());
                         array_push($result_value, $ret_val) ;
                     }
                    }
                    else
                    {
                      $pause='';
                      $pause_val='';
                        $ret_val=calculate_test($abc[0],time());
                         array_push($result_value, $ret_val) ;
                    }
                  }
                  // $time_tot=0;
                   foreach ($result_value as $re_key => $re_value) {
                      //$time_tot+=time_to_sec($re_value) ;
                      $for_total_time+=time_to_sec($re_value) ;
                   }

                  }
                  //echo $for_total_time."val";
                  $hr_min_sec=sec_to_time($for_total_time);
                   $hr_explode=explode(':',$hr_min_sec);
                    $hours=(int)$hr_explode[0];
                    $min=(int)$hr_explode[1];
                   $sec=(int)$hr_explode[2];

                  }
                  else
                  {
                    $hours=0;
                    $min=0;
                    $sec=0;
                    $pause='on';
                  }


                  }

                  ?>

                <div class="stopwatch" data-autostart="false" data-date="<?php echo '2018/06/23 15:37:25'; ?>" data-id="<?php echo $value['id']; ?>"  data-hour="<?php echo $hours;?>" data-min="<?php echo $min; ?>" data-sec="<?php echo $sec;?>" data-mili="0" data-start="<?php if($time_start_date!=''){ echo date('Y/m/d H:i:s',strtotime($time_start_date)); } else { echo ""; } ?>" data-end="<?php if($time_end_date!=''){ echo date('Y/m/d H:i:s',strtotime($time_end_date)); } else { echo ""; } ?>" data-current="Start" data-pauseon="<?php echo $pause;?>" >
                <div class="time timer_<?php echo $value['id'];?>">
                <span class="hours"></span> : 
                <span class="minutes"></span> : 
                <span class="seconds"></span> :: 
                <span class="milliseconds"></span>
                </div>
                <div class="controls per_timerplay_<?php echo $value['id']; ?>" id="<?php echo $value['id'];?>">
                <!-- Some configurability -->
                <?php //09-08-2018 
                if($_SESSION['role']==6 && $value['review_status']!=1){
                ?>
                <button class="toggle for_timer_start_pause new_play_stop per_timerplay_<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" data-pausetext="||" data-resumetext=">"> > </button>
                <?php } ?>
                <!--  <button class="reset">Reset</button> -->
                </div>
                </div>
                <!-- end of timer -->

                <div class="progress-circle progress-<?php echo $percent;?>"><span><?php echo $percent;?></span></div>
                </td>

                 <div class="modal-alertsuccess alert alert-success" id="delete_user<?php echo $value['id'];?>" style="display:none;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                <div class="pop-realted1">
                <div class="position-alert1">
                Are you sure want to delete <b><a href="#" class="delete_yes"> Yes </a></b> OR <b><a href="#" id="close" class="close_div">No</a></b></div>
                </div></div>  
        </tr>
        <?php }
        } ?>


    <!--     <tr data-key-1="Value 1" data-key-2="Value 2">
            <td class="details-control"></td>
            <td>data 2a</td>
            <td>data 2b</td>
            <td>data 2c</td>
            <td>data 2d</td>
        </tr> -->
    </tbody>
</table>



<script  type="text/javascript">
function format ( d ) {


   var json = JSON.parse(d); 
   console.log(json);
          //status=json['status'];
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Full name:</td>'+
            '<td>'+json['row1']+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extension number:</td>'+
            '<td>'+d.row2+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extra info:</td>'+
            '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
    '</table>';
}

$(function () {

      var table = $('#alltask').DataTable({});

      // Add event listener for opening and closing details
      $('#alltask').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table.row(tr);

          if (row.child.isShown()) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          } else {



            var id=$(this).attr('id');
              $.ajax({
                    url: '<?php echo base_url(); ?>Sub_Task/sub_task_get',
                    type : 'POST',
                    data : { 'id':id },                    
                      beforeSend: function() {
                        $(".LoadingImage").show();
                      },
                      success: function(data) {
                                  var json = JSON.parse(data); 
                                  var myJSON = JSON.stringify(json);
                                  var finalData = myJSON.replace(/\\/g, "");  
                                  var result = finalData.replace(/\,{"table":"/g, ' ');
                                  var result1= result.replace(/\"}/g, ' ');
                                  var result2= result1.replace(/\[{"table":"/g, ' ');
                                  var result3= result2.replace(/\]/g, ' ');

                                  //var result= finalData.replaceAll(',{"table":"', "");

                                   //var result1= result.replaceAll('"},{"table":"', "");
                                  // console.log(result1);
                                  // $(".LoadingImage").hide();

                                  row.child(  result3  ).show();
                                  tr.addClass('shown');                      

                      }

                    });


            // Open this row
           
         }

      });
  });


  </script>