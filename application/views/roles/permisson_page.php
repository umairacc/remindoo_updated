<?php 
    $all_pages=  array(
                      "Dashboard" =>
                      [
                        "Dashboard"=>["view"=>"view","add"=>"","edit"=>"","delete"=>""]
                      ],
                      "Task Section" => 
                      [
                        "Task"=>["data_visibility"=>'viewall_Task',"view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"]
                      ],
                      "CRM" =>  
                      [
                        "Leads" => ["data_visibility"=>'viewall_Leads',"view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],
                        "Webtolead" => ["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"]
                      ],
                      "Proposal"=>
                      [
                        "Proposal Dashboad"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"]
                      ],
                      "Client"=>
                      [
                        "Client Section"=>["data_visibility"=>'viewall_Client_Section',"view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"]
                      ],
                      "Deadline"=>
                      [
                        "Deadline manager"=>["view"=>"view","add"=>"","edit"=>"","delete"=>""]
                      ],
                      "Report"=>
                      [
                        "Reports"=>["view"=>"view","add"=>"","edit"=>"","delete"=>""]
                      ],
                      "Settings"=>
                      [
                        "User and Management"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],
                        "Admin Settings"=>["view"=>"view","add"=>"","edit"=>"edit","delete"=>""],
                        "Services"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],
                        "Firm Fields Settings"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],
                        "Menu Management Settings"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],  
                        "Reminder Settings"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],
                        //"Section Settings"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],
                        "Pricelist Settings"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],
                        "Notification Settings"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],
                        //"Staff Custom Firmlist"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],
                        "Email Template"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],
                        "Term and Condition"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],
                        "Tickets"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"],
                        "Chat"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"]
                      ],
                      "Document"=>
                      [
                        "Documents"=>["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"]
                      ],
                      "Invoices" =>
                      [
                        "Invoices" => ["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"]
                      ],  
                      /*'My Profile'=>
                      [
                        "My Profile" => ["view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"]
                      ],*/
                      'Give Feedback'=>
                      [
                        "Give Feedback" => ["add"=>"add"]
                      ]
                    );
?>

<div class="department-table 123456">
  <div class="dept-checkbox-danger">
    <table class="permission-table" id="permission-table">
      <thead>
        <tr>
           <th>
              <label class="custom_checkbox1">
                 <input type="checkbox" id="selectall" value="permission" level="parent">
                <i></i> 
                 </label>
              <label>permissions</label>
           </th>
          <th>
            <label>Data Visibility</label>
          </th>
           <th>
              <label class="custom_checkbox1">
                 <input type="checkbox" id="view_all" value="permission_view">
                  <i></i> 
                 </label>
              <label>view</label>
           </th>
           <th>
              <label class="custom_checkbox1">
                 <input type="checkbox" id="create_all" value="permission_create">
                 <i></i>
                 </label>
              <label>create</label>
           </th>
           <th>
              <label class="custom_checkbox1">
                 <input type="checkbox" id="edit_all" value="permission_edit">
                 <i></i>
                 </label>
              
              <label>edit</label>
           </th>
           <th>
              <label class="custom_checkbox1">
                 <input type="checkbox" id="delete_all" value="permission_delete">
                 <i></i>
                 </label>
              <label>delete</label>
           </th>
        </tr>
      </thead>
      <tbody>
        <?php 
        $i=0;
        foreach ($all_pages as $page_key => $page_value) { ?>
        <tr>
           <td colspan="6">
              <label class="permission_heading"><h6><?php echo $page_key;?></h6></label>
           </td>                                                     
        </tr>
         <?php
        foreach($page_value as $key=>$value) 
        {
          $its_view_checked   = 0;
          $its_add_checked    = 0;
          $its_edit_checked   = 0;
          $its_delete_checked = 0;

          $table_field =  str_replace(" ","_",$key);


          if(isset( $edit_role ) )
          {
            $its_view_checked=$this->User_management->Is_Permision_Given( $edit_role['id'] , $table_field , 0 );
            $its_add_checked=$this->User_management->Is_Permision_Given( $edit_role['id'] , $table_field , 1 );
            $its_edit_checked=$this->User_management->Is_Permision_Given( $edit_role['id'] , $table_field , 2 );
            $its_delete_checked=$this->User_management->Is_Permision_Given( $edit_role['id'] , $table_field , 3 );
          }
        ?>
        <tr  <?php if(isset( $edit_role ) && $edit_role['parent_id']==0 && $page_key=='Settings' ){ echo  "class=''"; }?> >
          
          <td>
            <label class="custom_checkbox1">
              <input type="checkbox" id="bulkall" class="dashboard_check checkboxall bulkall" value="dashboard" level="parent">
              <i></i>
            </label>
            <label><?php echo $key; ?></label>                               
            <input type="hidden" name="settings_section[]" class="sections" value="<?php echo str_replace(" ","_",$key); ?>"> 
          </td>
          <td class="radio_button01">
            <?php if( !empty( $value['data_visibility'] ) ) { 

              $viewall = $this->Common_mdl->get_field_value( 'role_permission1' , $value['data_visibility'] , 'role_id' , $edit_role['id'] );

              $check_view_all[0] = $check_view_all[1] = '';
              $index = ( $viewall == 1 ? 0 : 1 );
              $check_view_all[ $index ] = "checked='checked'";
              ?>
              <input id="<?php echo 'all_'.$table_field;?>" value='1' type="radio" name="<?php echo 'viewall_'.$table_field;?>" <?php echo $check_view_all[0];?> >
              <label for="<?php echo 'all_'.$table_field;?>">
                All
              </label>

              <input id="<?php echo 'allocated_'.$table_field;?>" value='0'  type="radio" name="<?php echo 'viewall_'.$table_field;?>" <?php echo $check_view_all[1];?> >
              <label for="<?php echo 'allocated_'.$table_field;?>">
                Allocated
              </label>
            <?php } ?>
          </td>

          <td>
            <div class="new_checkviews">
              <?php if($value['view']!=''){ ?>
              <label class="custom_checkbox1">
                <input type="checkbox" id="bulkviews" class="dashboard_check viewall bulkview"  name="bulkview[<?php echo str_replace(" ","_",$key); ?>]" value="<?php echo str_replace(" ","_",$key); ?>" <?php if($its_view_checked=='1'){ echo "checked"; } ?> >
                <i></i>
              </label>  
               <?php } ?>
            </div>
          </td>
          <td>
            <div class="new_checkviews">
              <?php if( $value['add'] != '' ){ ?>
                <label class="custom_checkbox1">
                  <input type="checkbox" id="bulkcreates" class="dashboard_check viewall bulkcreate" name="bulkcreates[<?php echo str_replace(" ","_",$key); ?>]" value="<?php echo str_replace(" ","_",$key); ?>" <?php if($its_add_checked=='1'){ echo "checked"; } ?> >
                  <i></i> 
                </label>                                
              <?php }?>
            </div>
          </td>
          <td>
            <div class="new_checkviews">
              <?php if( $value['edit'] != '' ){ ?>
              <label class="custom_checkbox1">
                <input type="checkbox" id="bulkedits" class="dashboard_check viewall bulkedit" name="bulkedit[<?php echo str_replace(" ","_",$key); ?>]" value="<?php echo str_replace(" ","_",$key); ?>" <?php if($its_edit_checked=='1'){ echo "checked"; } ?> >
                <i></i>
              </label> 
            <?php } ?>
            </div>
          </td>
          <td>
            <div class="new_checkviews">
              <?php if( $value['delete'] != '' ){ ?>
              <label class="custom_checkbox1">
                <input type="checkbox" id="bulkdeletes" class="dashboard_check viewall bulkdelete" name="bulkdelete[<?php echo str_replace(" ","_",$key); ?>]" value="<?php echo str_replace(" ","_",$key); ?>" <?php  if($its_delete_checked=='1'){ echo "checked"; } ?> >
                <i></i>
               </label>
              <?php } ?>     
            </div>
          </td>
        </tr>
        <?php
        $i++; 
        }                     
        }
      ?>
      </tbody>
    </table>
  </div>
</div>