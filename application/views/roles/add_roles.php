<?php 
//error_reporting('0');
$this->load->view('includes/header');
?>
<style type="text/css">
 .usr_role_tab {
opacity: 0;
display: none;
}
.usr_role_tab.active {
opacity: 1;
display: block;
}
</style>
<link rel="stylesheet" href="assets/css/highlight.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/pygments.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/hierarchy-select.min.css">

<!-- show info  -->
<div class="modal-alertsuccess alert info_popup" style="display:none;">
  <div class="newupdate_alert">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
   <div class="pop-realted1">
      <div class="position-alert1 info-text">
       </div>
      </div>
   </div>
</div>
<!-- end show info  -->
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="page-body">
      <div class="client-details-view hidden-user01 floating_set card-removes priceli addrolecls hidden-proposal_pay">
        <div class="right-side-proposal">
          <div class="card desk-dashboard">
            <div class="inner-tab123">
              
              <div class="deadline-crm1 floating_set add_rolecls">
                <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard client-top-nav">
                  <li class="nav-item">
                    <a href="<?php echo base_url()?>Role_Assign" class="nav-link " data-tag="home">Roles</a>
                    <div class="slide"></div>
                  </li>
                  <li class="nav-item ">
                    <a href="#" class="nav-link active" data-tag="home">New Role</a>
                    <div class="slide"></div>
                  </li>
                  <li class="nav-item ">
                    <a class="nav-link"  data-tag="home" href="<?php echo base_url()?>user/staff_list">Users</a>
                    <div class="slide"></div>
                  </li>
                  <li class="nav-item ">
                    <a href="<?php echo base_url()?>team/organisation_tree" class="nav-link" data-tag="home">Groups</a>
                    <div class="slide"></div>
                  </li>
                </ul>
              </div>

              <div class="tab-content">
                <form action="<?php echo base_url(); ?>Role_Assign/roles_add" method="post" id="role_add_form">
                  
                  <div class="role-title-wrapper">
                    <div class="tittle_headingcls floating_set">
                        <h3>New Role</h3>
                    </div>

                    <div class="form_iputsubmit">
                      <input type="submit" value="Save" class="btn-warning">
                    </div>
                  </div>
                    <div class="document-center client-infom-1 floating_set pricelist parent_role_id">
                      <div class="space-required show_permission_error" style="display: none;">
                        <div class="main-pane-border1 cmn-errors1" id="required_information_error">
                          <div class="alert-ss"></div>
                            <span for="company_name" generated="true" class="field-error required-errors">Permission Required.</span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-12 parent-role-wrapper">
                        <div class="price-tb-setting role_select_float pric_clsaddrole" >      
                          <input type="hidden" name="role_id" id="role_id" value="<?php echo $role['id']?>">
                            <!-- hierarchy select -->
                          <div class="dropdown hierarchy-select" id="roles_select">
                            <h3>Parent Role</h3>
                            <button type="button" class="btn btn-secondary dropdown-toggle" id="example-one-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                            <div class="dropdown-menu" aria-labelledby="example-one-button">
                              <div class="hs-searchbox">
                                  <input type="text" class="form-control" autocomplete="off">
                              </div>
                              <div class="hs-menu-inner">
                                  <?php echo $roles_tags;?>
                              </div>
                            </div>
                            <input class="d-none" name="parent_id" readonly="readonly"  aria-hidden="true" type="text"/> 
                          </div>
                          <div class="line-items1">
                                <h3>Role Name</h3>
                                <input type="text" name="role" id="role" required="required" class="clr-check-client">
                          </div>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-12 martop_infi">
                        <?php $this->load->view('roles/permisson_page'); ?>
                      </div>
                  </div>
                </form>
              </div>

            </div>
          </div>
        </div><!-- right-side -->
      </div>
		</div>
  </div>
</div>

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<script src="<?php echo base_url();?>assets/js/hierarchy-select.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script type="text/javascript">
function Save_Role()
{
    
  var view = $("input[type='checkbox']:checked.bulkview").length;
  var create = $("input[type='checkbox']:checked.bulkcreate").length;
  var edit  =  $("input[type='checkbox']:checked.bulkedit").length;
  var Delete = $("input[type='checkbox']:checked.bulkdelete").length;
  //console.log(view+"view count"+create+"c count"+edit+"edit c"+Delete);
  if(view==0 && create == 0  && edit == 0 && Delete == 0 )
  {
    $(".show_permission_error").show();  
    return;
  }
  else
  {
    $(".show_permission_error").hide();  
  }
  var DATA = new FormData( $('#role_add_form')[0] );
  $.ajax({
               
            url: '<?php echo base_url()."Role_Assign/save_role";?>',
            type: 'post',
            data:DATA,
            contentType : false,
            processData : false,
            beforeSend:function(){ $(".LoadingImage").show(); },
            success: function (data) {               
               if(data == '1')
               {
                 $('.info_popup .info-text').html("Role Save Successfully..!");
                 $('.info_popup').show();
               }
               $(".LoadingImage").hide();
               setTimeout(function(){
                window.location.href = "<?php echo base_url();?>Role_Assign";
               },1500);
           }
        });
};
  

     $('#role_add_form').validate(
  {
    ignore:false,
    rules:{
      parent_id:{required:true},
      role:{required:true},
    },    
    submitHandler:Save_Role
  }  
);


/*$(".close_role").click(function(){ 
 var id=$(this).attr('id');
 var par_id=$(this).data('parent');
 $('#role').prop('readonly',true);
   $.ajax({
                    url: '<?php echo base_url(); ?>Role_Assign/reassign_member',
                    type : 'POST',
                    data : { 'id':id},                    
                      beforeSend: function() {
                        $(".LoadingImage").show();
                      },
                      success: function(data) {                       
                             var json = JSON.parse(data); 
                             role=json['role'];
                             id=json['id'];        
                             $("#role").val(role);  
                             $('#role_id').val(id);
                             $("#par_id").val(par_id);  
                             $("#parent_id").val('');
                             $(".LoadingImage").hide(); 
                      }

                    });

});*/

 $(document).ready(function(){

  function RowCheck(Jobj)
{
  Jobj.each(function(){

  var tds = $(this).find('td:not(:nth-child(1))');
  //console.log(tds.length+"tds")
  var all= tds.find('input[type="checkbox"]').length;
  //console.log(all+"tds checkbox")

  var check = tds.find('input[type="checkbox"]:checked').length;
  //console.log(check+"tds checkbox:checked")

  if(all==check)
  {
    $(this).find('td:nth-child(1) input[type="checkbox"]').prop('checked',true);
  }
  else{
    $(this).find('td:nth-child(1) input[type="checkbox"]').prop('checked',false);

  }
});
}


function Column_check(Jobj)
{
  Jobj.each(function(){

  var ind = $(this).index()+1;
  //console.log(ind);
  var tds = $(this).closest('table').find('tbody tr td:nth-child('+ind+')');
  var all= tds.find('input[type="checkbox"]').length;
  var check = tds.find('input[type="checkbox"]:checked').length;

  if(all==check)
  {
    $(this).find('input[type="checkbox"]').prop('checked',true);
  }
  else
  {
    $(this).find('input[type="checkbox"]').prop('checked',false);
  }

});
}

        /*$('.nav-item a').click(function(){
            $('.nav-item a').removeClass('active');
            $(this).addClass('active');
            var tagid = $(this).data('tag');
            $('.tab-pane').removeClass('active').addClass('hide');
            $('#'+tagid).addClass('active').removeClass('hide');
        });*/

        $('#roles_select').hierarchySelect({
            width: 'auto'
        });

         $('.tab_toggle').click(function(){
          
          $('.tab_toggle').removeClass('disabled');
          
          $(this).addClass('disabled');
          var target = $(this).attr('data-target');

          $('.tab-pane').removeClass('active');
          $('.tab-pane.'+target).addClass('active');


         });


$('#selectall').on('click',function () {
  
    if ($(this).is(":checked")){
      console.log('inside');
      $('#view_all').prop('checked',false).trigger('click');
      $('#create_all').prop('checked',false).trigger('click');
      $('#edit_all').prop('checked',false).trigger('click');
      $('#delete_all').prop('checked',false).trigger('click');

      $('.bulkall').each(function(){
         $(this).prop('checked', true); 
      });

    }
      else
    {     
      $('#view_all').prop('checked',true).trigger('click');
      $('#create_all').prop('checked',true).trigger('click');
      $('#edit_all').prop('checked',true).trigger('click');
      $('#delete_all').prop('checked',true).trigger('click');
      $('.bulkall').each(function(){
        $(this).prop('checked', false);
      });
    }
 });

  

$("#view_all").on("click",function () {
    if ($(this).is(":checked")){

      $('.bulkview').each(function(){
         $(this).prop('checked', true);
        
         //var value=$(this).attr('value');
         //$(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value);
         //$(this).next('.for_bulkview').attr('value',value);
      });
    }
    else
    {
      $('.bulkview').each(function(){
        if( !$(this).closest('tr').hasClass('disabled'))
        {
          $(this).prop('checked', false); 
        }
        //var value=$(this).attr('value')+"//unchecked";
        // $(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value);

       //  $(this).next('.for_bulkview').attr('value',value);
      });
    }
 });

$("#create_all").on("click",function () {
    if ($(this).is(":checked")){
      $('.bulkcreate').each(function(){
         $(this).prop('checked', true); 
             /** for extra fields **/
     var value=$(this).attr('value');
     $(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value);
     /** for extra fields **/
      });
    }
   else
    {
      $('.bulkcreate').each(function(){
         $(this).prop('checked', false); 
             /** for extra fields **/
     var value=$(this).attr('value')+"//unchecked";
     $(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value);
     /** for extra fields **/
      });
    }

 });
$("#edit_all").on("click",function () {
    if ($(this).is(":checked")){
      $('.bulkedit').each(function(){
         $(this).prop('checked', true); 
             /** for extra fields **/
     var value=$(this).attr('value');
     $(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value);
     /** for extra fields **/
      });
    }
      else
    {
      $('.bulkedit').each(function(){
         $(this).prop('checked', false); 
             /** for extra fields **/
     var value=$(this).attr('value')+"//unchecked";
     $(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value);
     /** for extra fields **/
      });
    }
 });
$('#delete_all').on("click",function () {
    if ($(this).is(":checked")){
      $('.bulkdelete').each(function(){
         $(this).prop('checked', true); 
             /** for extra fields **/
     var value=$(this).attr('value');
     $(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value);
     /** for extra fields **/
      });
    }
      else
    {
      $('.bulkdelete').each(function(){
         $(this).prop('checked', false); 
             /** for extra fields **/
     var value=$(this).attr('value')+"//unchecked";
     $(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value);
     /** for extra fields **/
      });
    }
 });


$('.bulkall').on('click',function(){
 if($(this).is(":checked")){
    $(this).closest('tr').find('td').each(function(){
   //alert('sdsd');
   $(this).find('input[type="checkbox"]').prop('checked',true);
  // alert($(this).attr('class'));
      var value=$(this).find('input[type="checkbox"]').attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
   var itid=$(this).find('input[type="checkbox"]').attr('id');
  //alert(itid);
   if(itid=='bulkviews'){
$(this).find('.for_bulkview').attr('value',value);
}
  if(itid=='bulkcreates'){
$(this).find('.for_bulkcreate').attr('value',value);
}  if(itid=='bulkedits'){
$(this).find('.for_bulkedit').attr('value',value);
}  if(itid=='bulkdeletes'){
$(this).find('.for_bulkdelete').attr('value',value);
}

  });
 }
 else
 {
    $(this).closest('tr').find('td').each(function(){
   //alert('sdsd');
   $(this).find('input[type="checkbox"]').prop('checked',false);
      var value=$(this).find('input[type="checkbox"]').attr('value')+"//unchecked";
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
   var itid=$(this).find('input[type="checkbox"]').attr('id');
   if(itid=='bulkviews'){
$(this).find('.for_bulkview').attr('value',value);
}
  if(itid=='bulkcreates'){
$(this).find('.for_bulkcreate').attr('value',value);
}  if(itid=='bulkedits'){
$(this).find('.for_bulkedit').attr('value',value);
}  if(itid=='bulkdeletes'){
$(this).find('.for_bulkdelete').attr('value',value);
}
  });
 }


  });


    
  $(document).on('click','.bulkview',function(){
    RowCheck($(this).closest('tr'));
    var nth = $(this).closest('td').index()+1;
    Column_check($('#permission-table thead th:nth-child('+nth+')'));
  });

  $(document).on('click','.bulkcreate',function(){
    RowCheck($(this).closest('tr'));
    var nth = $(this).closest('td').index()+1;
    Column_check($('#permission-table thead th:nth-child('+nth+')'));
  });

  $(document).on('click','.bulkedit',function(){
    RowCheck($(this).closest('tr'));
    var nth = $(this).closest('td').index()+1;
    Column_check($('#permission-table thead th:nth-child('+nth+')'));
  });

  $(document).on('click','.bulkdelete',function(){
    RowCheck($(this).closest('tr'));
    var nth = $(this).closest('td').index()+1;
    Column_check($('#permission-table thead th:nth-child('+nth+')'));
  });
  });
  //  dropdown-sin-2
</script>

<script>
  $(document).ready(function(){
    $(".clr-check-client").keyup(function(){
			var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-client-outline");
			}else{
				$(this).removeClass("clr-check-client-outline");
			}
		});
    $(".clr-check-client").each(function(i){
      if($(this).val() !== ""){
          $(this).addClass("clr-check-client-outline");
      } 
    });
  })
</script>
