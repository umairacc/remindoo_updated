<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1.0"/>
<meta name="format-detection" content="telephone=no">
  <title>Remainder</title>
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
</head>
<body>

<div id="m_5349442688376831455m_-3036873862170083609wrapper" dir="ltr" style="background-color:#f7f7f7;margin:0;padding:70px 0 70px 0;width:100%">
  <span class="HOEnZb"><font color="#888888">
  </font></span>
  <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tbody>
      <tr>
        <td align="center" valign="top">
          <div id="m_5349442688376831455m_-3036873862170083609template_header_image">
          </div>
          <span class="HOEnZb"><font color="#888888">
          </font></span><span class="HOEnZb"><font color="#888888">
          </font></span>
          <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_5349442688376831455m_-3036873862170083609template_container" style="background-color:#ffffff;border:1px solid transparent;border-radius:3px!important">
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_5349442688376831455m_-3036873862170083609template_header" style="background-color:transparent;border-radius:3px 3px 0 0!important;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;background-image:none;background-color:#303549;background-repeat: no-repeat;">
                    <tbody>
                      <tr>
                        <td id="m_5349442688376831455m_-3036873862170083609header_wrapper" style="padding:10px 25px;display:block;text-align:center;">
                        <img width="200" src="http://techleafsolutions.com/projects/CRM/assets/images/logo.png" alt="Site-logo" style="background: transparent;border-radius: 5px;/* float: left; */display: inline-block;vertical-align: middle;">
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_5349442688376831455m_-3036873862170083609template_body">
                    <tbody>
                      <tr>
                        <td valign="top" id="m_5349442688376831455m_-3036873862170083609body_content" style="background-color:#ffffff">
                          <table border="0" cellpadding="20" cellspacing="0" width="100%">
                            <tbody>
                              <tr>
                                <td valign="top" style="padding:48px">
                                  <div id="m_5349442688376831455m_-3036873862170083609body_content_inner" style="color:#636363;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:14px;line-height:150%;text-align:left">                                  
                                  <p style="padding: 10px 0;font-family:'Lato';font-size:17px;font-weight: normal;">Hi<span class="blue-name"><a href="javascript:;" style="    color: #00aeef;text-decoration: none; padding-left: 3px;text-transform: capitalize;">User</a></span></p>
                               

                                    <p style="color:#555;display:block;font-family:lato;font-size:14px;;margin:10px 0 8px;text-align:left;">New lead has been created.

                                      <p><a href = "<?php echo base_url()?>leads/">Click Here</a> to refer this</p>

                                    </p> 

                                   <h5 style="color:#555;display:block;font-family:lato;font-size:16px;;margin:10px 0 8px;text-align:center;">Thankyou</h5> 
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <span class="HOEnZb"><font color="#888888">
                  </font></span>
                  <table border="0" cellpadding="10" cellspacing="0" width="600" id="m_5349442688376831455m_-3036873862170083609template_footer">
                    <tbody>
                      <tr>
                        <td valign="top" style="padding:0">
                          <span class="HOEnZb"><font color="#888888">
                          </font></span>
                          <table border="0" cellpadding="10" cellspacing="0" width="100%">
                            <tbody>
                              <tr> 
                                <td colspan="2" valign="middle" id="m_5349442688376831455m_-3036873862170083609credit" style="padding:10px 15px 10px;border:0;color:#fff;background: #303549;font-family:Arial;font-size:15px;line-height:125%;text-align:center;text-transform: captialize;font-family: 'Lato', sans-serif !important">
                                  <p style="margin-top: 10px;"><span style="color: #999999">Copyright © 2017 GURU able.</span></p>
                                  <span class="HOEnZb"><font color="#888888">
                                  </font></span>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <span class="HOEnZb"><font color="#888888">
                          </font></span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <span class="HOEnZb"><font color="#888888">
                  </font></span>
                </td>
              </tr>
            </tbody>
          </table>
          <span class="HOEnZb"><font color="#888888">
          </font></span>
        </td>
      </tr>
    </tbody>
  </table>
</div>
</body>
</html>
