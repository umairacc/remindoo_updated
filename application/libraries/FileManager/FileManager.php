<?php

if($_SESSION['user_type']=='FC')
{
$_SESSION['permission']['Documents']=array('1','1','1','0');
}

class FileManager extends CI_Controller {
	
	// The top level directory where this script is located, or alternatively one of it's sub-directories
	public $startDirectory = 'documents';

	// An optional title to show in the address bar and at the top of your page (set to null to leave blank)
	public $pageTitle = 'Document Drive Manager';

	// The URL of this script. Optionally set if your server is unable to detect the paths of files
	public $includeUrl = true;

	/*vicky*/
	// The URL of this script. Optionally set if your server is unable to detect the paths of files
	public $controller = 'Documents';

	// If you've enabled the includeUrl parameter above, enter the full url to the directory the index.php file
	// is located in here, followed by a forward slash.
	public $directoryUrl = '';

	// Set to true to list all sub-directories and allow them to be browsed
	public $showSubDirectories = true;

	// Set to true to open all file links in a new browser tab
	public $openLinksInNewTab = true;

	// Set to true to show thumbnail previews of any images
	public $showThumbnails = true;

	// Set to true to allow new directories to be created.
	public $enableDirectoryCreation = true;

	// Set to true to allow file uploads (NOTE: you should set a password if you enable this!)
	public $enableUploads = true;

	// Enable multi-file uploads (NOTE: This makes use of javascript libraries hosted by Google so an internet connection is required.)
	public $enableMultiFileUploads = true;

	// Set to true to overwrite files on the server if they have the same name as a file being uploaded
	public $overwriteOnUpload = false;

	// Set to true to enable file deletion options
	public $enableFileDeletion = true;

	// Set to true to enable directory deletion options (only available when the directory is empty)
	public $enableDirectoryDeletion = true;

	// List of all mime types that can be uploaded. Full list of mime types: http://www.iana.org/assignments/media-types/media-types.xhtml
	public $allowedUploadMimeTypes = array(
		'image/jpeg',
		'image/gif',
		'image/png',
		'image/bmp',
		'audio/mpeg',
		'audio/mp3',
		'audio/mp4',
		'audio/x-aac',
		'audio/x-aiff',
		'audio/x-ms-wma',
		'audio/midi',
		'audio/ogg',
		'video/ogg',
		'video/webm',
		'video/quicktime',
		'video/x-msvideo',
		'video/x-flv',
		'video/h261',
		'video/h263',
		'video/h264',
		'video/jpeg',
		'text/plain',
		'text/html',
		'text/css',
		'text/csv',
		'text/calendar',
		'application/pdf',
		'application/x-pdf',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document', // MS Word (modern)
		'application/msword',
		'application/vnd.ms-excel',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', // MS Excel (modern)
		'application/zip',
		'application/x-tar'
	);

	// Set to true to unzip any zip files that are uploaded (note - will overwrite files of the same name!)
	public $enableUnzipping = true;

	// If you've enabled unzipping, you can optionally delete the original zip file after its uploaded by setting this to true.
	public $deleteZipAfterUploading = false;

	// The Evoluted Directory Listing Script uses Bootstrap. By setting this value to true, a nicer theme will be loaded remotely.
	// Setting this to false will make the directory listing script use the default bootstrap style, loaded locally.
	public $enableTheme = true;

	// Set to true to require a password be entered before being able to use the script
	public $passwordProtect = false;

	// The password to require to use this script (only used if $passwordProtect is set to true)
	public $password = 'password';

	// Optional. Allow restricted access only to whitelisted IP addresses
	public $enableIpWhitelist = false;

	// List of IP's to allow access to the script (only used if $enableIpWhitelist is true)
	public $ipWhitelist = array(
		'127.0.0.1'
	);

	// File extensions to block from showing in the directory listing
	public $ignoredFileExtensions = array(
		'php',
		'ini',
	);

	// File names to block from showing in the directory listing
	public $ignoredFileNames = array(
		'.htaccess',
		'.DS_Store',
		'Thumbs.db',
	);

	// Directories to block from showing in the directory listing
	public $ignoredDirectories = array(

	);

	// Files that begin with a dot are usually hidden files. Set this to false if you wish to show these hiden files.
	public $ignoreDotFiles = true;

	// Works the same way as $ignoreDotFiles but with directories.
	public $ignoreDotDirectories = true;

	/*
	====================================================================================================
	You shouldn't need to edit anything below this line unless you wish to add functionality to the
	script. You should only edit this area if you know what you are doing!
	====================================================================================================
	*/
	private $__previewMimeTypes = array(
		'image/gif',
		'image/jpeg',
		'image/png',
		'image/bmp'
	);

	private $__currentDirectory = null;

	private $__fileList = array();

	private $__directoryList = array();

	private $__debug = true;

	public $sortBy = 'name';

	public $sortableFields = array(
		'name',
		'size',
		'modified'
	);

	private $__sortOrder = 'asc';

	public function __construct() {

		if (!defined('DS')) {
			define('DS', '/');
		}

		if (!defined('DOCUMENT_ROOT')) {
			define("DOCUMENT_ROOT", $_SERVER['DOCUMENT_ROOT'].'/');
		}

		$this->CI =& get_instance();

		$this->createparentDirectory($this->startDirectory.'/'.$_SESSION['firm_id']);
		$this->startDirectory = $this->startDirectory.'/'.$_SESSION['firm_id'].$this->CI->config->item('startDirectory');
		$this->directoryUrl = $this->CI->config->item('directoryUrl');
		$this->innerDirectory = ( isset( $_GET['dir'] ) ) ? str_replace('./', '', $_GET['dir']) : '';


	}

	public function run() {
		if ($this->enableIpWhitelist) {
			$this->__ipWhitelistCheck();
		}

		$this->__currentDirectory = $this->startDirectory.'/'.$this->innerDirectory;

		// Sorting
		if (isset($_GET['order']) && in_array($_GET['order'], $this->sortableFields)) {
			$this->sortBy = $_GET['order'];
		}

		if (isset($_GET['sort']) && ($_GET['sort'] == 'asc' || $_GET['sort'] == 'desc')) {
			$this->__sortOrder = $_GET['sort'];
		}

		if (isset($_GET['deleteDir'])) {
			if (isset($_GET['deleteDir']) && $this->enableDirectoryDeletion) {
				$this->deleteDirectory();
			}

			/*vicky*/
			//$this->__currentDirectory = $this->startDirectory;
			return $this->__display();
		} elseif (isset($_GET['preview'])) {
			$this->__generatePreview($_GET['preview']);
		} elseif (isset($_GET['download'])) {
			$this->__downloadFile($_GET['download']);
		} else {
			return $this->__display();
		}
	}

	public function createparentDirectory($dir_id) {

			$filePath1 = $dir_id;
			$_SESSION['start_path']=$dir_id;
			$filePath2 = $filePath1.'/'.$_SESSION['userId']; 
			

			if (file_exists($filePath1)) {

				if ($_SESSION['user_type']=='FC'  && !file_exists($filePath2)) {

					return mkdir($filePath2, 0755);

				}
				else
				{
					return false;
				}

			}
			else
			{		
					   mkdir($filePath1, 0755);

				if ($_SESSION['user_type']=='FC'  && !file_exists($filePath2)) {

					return mkdir($filePath2, 0755);

				}


				//return 1;


			}

	}

	public function upload() {
		$files = $this->__formatUploadArray($_FILES['upload']);

		if ($this->enableUploads) {
			if ($this->enableMultiFileUploads) {
				foreach ($files as $file) {
					$status = $this->__processUpload($file);
				}
			} else {
				$file = $files[0];
				$status = $this->__processUpload($file);
			}

			return $status;
		}
		return false;
	}

	private function __formatUploadArray($files) {
		$fileAry = array();
		$fileCount = count($files['name']);
		$fileKeys = array_keys($files);

		for ($i = 0; $i < $fileCount; $i++) {
			foreach ($fileKeys as $key) {
				$fileAry[$i][$key] = $files[$key][$i];
			}
		}

		return $fileAry;
	}

	private function __processUpload($file) {

		$CI =& get_instance();
 		$CI->load->model('Common_mdl');

		if (isset($_GET['dir'])) {
			$this->__currentDirectory = $this->startDirectory.'/'.$_GET['dir'];
		}

		if (! $this->__currentDirectory) {
			$filePath = realpath($this->startDirectory);
		} else {
			$this->__currentDirectory = str_replace('..', '', $this->__currentDirectory);
			$this->__currentDirectory = ltrim($this->__currentDirectory, "/");
			$filePath = realpath($this->__currentDirectory);
		}

		$filePath = $filePath . DS . $file['name'];
		
		if (! empty($file)) {

			if (! $this->overwriteOnUpload) {
				if (file_exists($filePath)) {
					return 2;
				}
			}

			if (! in_array(mime_content_type($file['tmp_name']), $this->allowedUploadMimeTypes)) {
				return 3;
			}

			move_uploaded_file($file['tmp_name'], $filePath);
			$CI->Common_mdl->document_function($_SESSION['doc_client_id'],'file_added',$file['name'],$filePath);
			if (mime_content_type($filePath) == 'application/zip' && $this->enableUnzipping && class_exists('ZipArchive')) {

				$zip = new ZipArchive;
				$result = $zip->open($filePath);
				$zip->extractTo(realpath($this->__currentDirectory));
				$zip->close();

				if ($this->deleteZipAfterUploading) {
					// Delete the zip file
					unlink($filePath);
				}


			}

			return true;
		}
	}

	public function deleteFile() {
		 $CI =& get_instance();
 		$CI->load->model('Common_mdl');
		if (isset($_GET['deleteFile'])) {
			$file = $_GET['deleteFile'];
			//echo"<pre>";
			$tmp=explode("/",$file);
		    $tmp=end($tmp);
			//exit;
			// Clean file path
			$file = str_replace('..', '', $file);
			$file = ltrim($file, "/");

			// Work out full file path
			$filePath = DOCUMENT_ROOT . $this->__currentDirectory . '/' . $file;

			$CI->Common_mdl->document_function($_SESSION['doc_client_id'],'delete_file',$tmp,$filePath);

			if (file_exists($filePath) && is_file($filePath)) {
				return unlink($filePath);
			}
			return false;
		}
	}

	public function deleteDirectory() {
			$CI =& get_instance();
 			$CI->load->model('Common_mdl');

		if (isset($_GET['deleteDir'])) {

			$dir = $this->startDirectory.'/'.$this->innerDirectory.'/'.$_GET['deleteDir'];
			// Clean dir path
			$dir = str_replace('..', '', $dir);
			$dir = ltrim($dir, "/");

			$tmp=explode("/",$dir);
		    $tmp=end($tmp);

			// echo "<pre>";
			// echo $dir;
			// exit;

			// Work out full directory path
			$dirPath = DOCUMENT_ROOT . '/' . $dir;

			if (file_exists($dirPath) && is_dir($dirPath)) {

				$iterator = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
				$files = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::CHILD_FIRST);

				foreach ($files as $file) {
					if ($file->isDir()) {
						rmdir($file->getRealPath());
					} else {
						unlink($file->getRealPath());
					}
				}

				$CI->Common_mdl->document_function($_SESSION['doc_client_id'],'delete_directory',$tmp,$dir);
				
				return rmdir($dir);
			}
		
		}
		return false;
	}

	public function createDirectory() {
			$CI =& get_instance();
 			$CI->load->model('Common_mdl');
		if ($this->enableDirectoryCreation) {
			$directoryName = $_POST['directory'];

			// Convert spaces
			$directoryName = str_replace(' ', '_', $directoryName);

			// Clean up formatting
			$directoryName = preg_replace('/[^\w-_]/', '', $directoryName);

			if (isset($_GET['dir'])) {
				$this->__currentDirectory = $this->startDirectory.'/'.$_GET['dir'];
			}

			if (! $this->__currentDirectory) {
				$filePath = realpath($this->startDirectory);
			} else {
				$this->__currentDirectory = str_replace('..', '', $this->__currentDirectory);
				$filePath = realpath($this->__currentDirectory);
			}

			$filePath = $filePath . DS . strtolower($directoryName);

			if (file_exists($filePath)) {
				return false;
			}
		
			$CI->Common_mdl->document_function($_SESSION['doc_client_id'],'directory_added',strtolower($directoryName), $filePath);

			return mkdir($filePath, 0755);

		}
		return false;
	}

	public function sortUrl($sort) {

		// Get current URL parts
		$urlParts = parse_url($_SERVER['REQUEST_URI']);

		$url = '';

		if (isset($urlParts['scheme'])) {
			$url = $urlParts['scheme'] . '://';
		}

		if (isset($urlParts['host'])) {
			$url .= $urlParts['host'];
		}

		if (isset($urlParts['path'])) {
			$url .= $urlParts['path'];
		}


		// Extract query string
		if (isset($urlParts['query'])) {
			$queryString = $urlParts['query'];

			parse_str($queryString, $queryParts);

			// work out if we're already sorting by the current heading
			if (isset($queryParts['order']) && $queryParts['order'] == $sort) {
				// Yes we are, just switch the sort option!
				if (isset($queryParts['sort'])) {
					if ($queryParts['sort'] == 'asc') {
						$queryParts['sort'] = 'desc';
					} else {
						$queryParts['sort'] = 'asc';
					}
				}
			} else {
				$queryParts['order'] = $sort;
				$queryParts['sort'] = 'asc';
			}

			// Now convert back to a string
			$queryString = http_build_query($queryParts);

			$url .= '?' . $queryString;
		} else {
			$order = 'asc';
			if ($sort == $this->sortBy) {
				$order = 'desc';
			}
			$queryString = 'order=' . $sort . '&sort=' . $order;
			$url .= '?' . $queryString;
		}

		return $url;
	}

	public function sortClass($sort) {
		$class = $sort . '_';

		if ($this->sortBy == $sort) {
			if ($this->__sortOrder == 'desc') {
				$class .= 'desc sort_desc';
			} else {
				$class .= 'asc sort_asc';
			}
		} else {
			$class = '';
		}
		return $class;
	}

	private function __ipWhitelistCheck() {
		// Get the users ip
		$userIp = $_SERVER['REMOTE_ADDR'];

		if (! in_array($userIp, $this->ipWhitelist)) {
			header('HTTP/1.0 403 Forbidden');
			die('Your IP address (' . $userIp . ') is not authorized to access this file.');
		}
	}

	private function __display() {
		if ($this->__currentDirectory != '.' && !$this->__endsWith($this->__currentDirectory, DS)) {
			$this->__currentDirectory = $this->__currentDirectory . DS;
		}

		return $this->__loadDirectory($this->__currentDirectory);
	}

	private function __loadDirectory($path) {
		$files = $this->__scanDir($path);

		if (! empty($files)) {
			// Strip excludes files, directories and filetypes
			$files = $this->__cleanFileList($files);
			foreach ($files as $file) {
				$filePath = realpath($this->__currentDirectory . DS . $file);
				
				if ($this->__isDirectory($filePath)) {

					if (! $this->includeUrl) {
						$urlParts = parse_url($_SERVER['REQUEST_URI']);

						$dirUrl = '';
						$dirCUrl = '';

						if (isset($urlParts['scheme'])) {
							$dirUrl = $urlParts['scheme'] . '://';
						}

						if (isset($urlParts['host'])) {
							$dirUrl .= $urlParts['host'];
						}

						if (isset($urlParts['path'])) {
							$dirUrl .= $urlParts['path'];
						}
					} else {
						$dirUrl = $this->directoryUrl;
						$dirCUrl = $this->directoryUrl;
					}

					/*vicky*/
					$dirUrl .= $this->controller;
					$dirCUrl.= $this->controller;
					/*vicky hide all curr direc*/

					if ($this->__currentDirectory != '' && $this->__currentDirectory != '.') {
						$dirUrl .= '?dir=' . rawurlencode( $this->innerDirectory ) .'/'. rawurlencode($file);
						$dirCUrl .= '?dir=' . '/'. $this->innerDirectory;
					} else {
						$dirUrl .= '?dir=' . rawurlencode($file);
						$dirCUrl .= '?dir=' . rawurlencode($file);
					}

					$this->__directoryList[$file] = array(
						'name' => rawurldecode($file),
						'path' => $filePath,
						'type' => 'dir',
						'url' => $dirUrl,
						'inner' => $dirCUrl
					);
				} else {
					$this->__fileList[$file] = $this->__getFileType($filePath, $this->__currentDirectory .'/' . DS . $file);
				}
			}
		}

		if (! $this->showSubDirectories) {
			$this->__directoryList = null;
		}

		$data = array(
			'currentPath' => $this->__currentDirectory,
			'directoryTree' => $this->__getDirectoryTree(),
			'files' => $this->__setSorting($this->__fileList),
			'directories' => $this->__directoryList,
			'requirePassword' => $this->passwordProtect,
			'enableUploads' => $this->enableUploads
		);

		return $data;
	}

	private function __setSorting($data) {
		$sortOrder = '';
		$sortBy = '';

		// Sort the files
		if ($this->sortBy == 'name') {
			function compareByName($a, $b) {
				return strnatcasecmp($a['name'], $b['name']);
			}

			usort($data, 'compareByName');
			$this->soryBy = 'name';
		} elseif ($this->sortBy == 'size') {
			function compareBySize($a, $b) {
				return strnatcasecmp($a['size_bytes'], $b['size_bytes']);
			}

			usort($data, 'compareBySize');
			$this->soryBy = 'size';
		} elseif ($this->sortBy == 'modified') {
			function compareByModified($a, $b) {
				return strnatcasecmp($a['modified'], $b['modified']);
			}

			usort($data, 'compareByModified');
			$this->soryBy = 'modified';
		}

		if ($this->__sortOrder == 'desc') {
			$data = array_reverse($data);
		}
		return $data;
	}

	private function __scanDir($dir) {
		// Prevent browsing up the directory path.
		if (strstr($dir, '../')) {
			return false;
		}

		if ($dir == '/') {
			$dir = $this->startDirectory;
			$this->__currentDirectory = $dir;
		}

		$strippedDir = str_replace('/', '', $dir);

		$dir = ltrim($dir, "/");

		// Prevent listing blacklisted directories
		if (in_array($strippedDir, $this->ignoredDirectories)) {
			return false;
		}

		if (! file_exists($dir) || !is_dir($dir)) {
			return false;
		}

		return scandir($dir);
	}

	private function __cleanFileList($files) {
		$this->ignoredDirectories[] = '.';
		$this->ignoredDirectories[] = '..';
		foreach ($files as $key => $file) {

			// Remove unwanted directories
			if ($this->__isDirectory(realpath($file)) && in_array($file, $this->ignoredDirectories)) {
				unset($files[$key]);
			}

			// Remove dot directories (if enables)
			if ($this->ignoreDotDirectories && substr($file, 0, 1) === '.') {
				unset($files[$key]);
			}

			// Remove unwanted files
			if (! $this->__isDirectory(realpath($file)) && in_array($file, $this->ignoredFileNames)) {
				unset($files[$key]);
			}
			// Remove unwanted file extensions
			if (! $this->__isDirectory(realpath($file))) {

				$info = pathinfo(mb_convert_encoding($file, 'UTF-8', 'UTF-8'));

				if (isset($info['extension'])) {
					$extension = $info['extension'];

					if (in_array($extension, $this->ignoredFileExtensions)) {
						unset($files[$key]);
					}
				}

				// If dot files want ignoring, do that next
				if ($this->ignoreDotFiles) {

					if (substr($file, 0, 1) == '.') {
						unset($files[$key]);
					}
				}
			}
		}
		return $files;
	}

	private function __isDirectory($file) {
		if ($file == $this->__currentDirectory . DS . '.' || $file == $this->__currentDirectory . DS . '..') {
			return true;
		}
		$file = mb_convert_encoding($file, 'UTF-8', 'UTF-8');

		if (filetype($file) == 'dir') {
			return true;
		}

		return false;
	}

	/**
	 * __getFileType
	 *
	 * Returns the formatted array of file data used for thre directory listing.
	 *
	 * @param  string $filePath Full path to the file
	 * @return array   Array of data for the file
	 */
	private function __getFileType($filePath, $relativePath = null) {
		$fi = new finfo(FILEINFO_MIME_TYPE);

		if (! file_exists($filePath)) {
			return false;
		}

		$type = $fi->file($filePath);

		$filePathInfo = pathinfo($filePath);

		$fileSize = filesize($filePath);

		$fileModified = filemtime($filePath);

		$filePreview = false;

		// Check if the file type supports previews
		if ($this->__supportsPreviews($type) && $this->showThumbnails) {
			$filePreview = true;
		}

		return array(
			'name' => $filePathInfo['basename'],
			'extension' => (isset($filePathInfo['extension']) ? $filePathInfo['extension'] : null),
			'dir' => $filePathInfo['dirname'],
			'path' => $filePath,
			'relativePath' => $relativePath,
			'size' => $this->__formatSize($fileSize),
			'size_bytes' => $fileSize,
			'modified' => $fileModified,
			'type' => 'file',
			'mime' => $type,
			'url' => $this->__getUrl($filePathInfo['basename']),
			'base_url' => $this->directoryUrl . $this->controller,
			'inner' => $this->directoryUrl . $this->controller.'?dir=' . $this->innerDirectory,
			'preview' => $filePreview,
			'target' => ($this->openLinksInNewTab ? '_blank' : '_parent')
		);
	}

	private function __supportsPreviews($type) {
		if (in_array($type, $this->__previewMimeTypes)) {
			return true;
		}
		return false;
	}

	/**
	 * __getUrl
	 *
	 * Returns the url to the file.
	 *
	 * @param  string $file filename
	 * @return string   url of the file
	 */
	private function __getUrl($file) {
		if (! $this->includeUrl) {
			$dirUrl = $_SERVER['REQUEST_URI'];

			$urlParts = parse_url($_SERVER['REQUEST_URI']);

			$dirUrl = '';

			if (isset($urlParts['scheme'])) {
				$dirUrl = $urlParts['scheme'] . '://';
			}

			if (isset($urlParts['host'])) {
				$dirUrl .= $urlParts['host'];
			}

			if (isset($urlParts['path'])) {
				$dirUrl .= $urlParts['path'];
			}
		} else {
			$dirUrl = $this->directoryUrl;
		}

		if ($this->__currentDirectory != '.') {
			$dirUrl = $dirUrl . $this->__currentDirectory;
		}
		return $dirUrl . rawurlencode($file);
	}

	private function __getDirectoryTree() {
		$dirString = $this->__currentDirectory;

		$directoryTree = array();

		$directoryTree['./'] = 'Index';

		if (substr_count($dirString, '/') >= 0) {
			$items = explode("/", $dirString);
			$items = array_filter($items);
			$path = '';
			
			foreach ($items as $item) {
				if ($item == '.' || $item == '..' || strtolower( $item ) == 'documents' || strtolower($item) == strtolower( $this->CI->config->item('startDirectory') ) ) {
					continue;
				}
				$path .= rawurlencode($item) . '/';
				$directoryTree[$path] = $item;
			}
		}

		$directoryTree = array_filter($directoryTree);

		return $directoryTree;
	}

	private function __endsWith($haystack, $needle) {
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
	}

	private function __generatePreview($filePath) {
		$file = $this->__getFileType($filePath);

		if ($file['mime'] == 'image/jpeg') {
			$image = imagecreatefromjpeg($file['path']);
		} elseif ($file['mime'] == 'image/png') {
			$image = imagecreatefrompng($file['path']);
		} elseif ($file['mime'] == 'image/gif') {
			$image = imagecreatefromgif($file['path']);
		} else {
			die();
		}

		$oldX = imageSX($image);
		$oldY = imageSY($image);

		$newW = 250;
		$newH = 250;

		if ($oldX > $oldY) {
			$thumbW = $newW;
			$thumbH = $oldY * ($newH / $oldX);
		}
		if ($oldX < $oldY) {
			$thumbW = $oldX * ($newW / $oldY);
			$thumbH = $newH;
		}
		if ($oldX == $oldY) {
			$thumbW = $newW;
			$thumbH = $newW;
		}

		ob_clean();
		header('Content-Type: ' . $file['mime']);

		$newImg = ImageCreateTrueColor($thumbW, $thumbH);

		imagecopyresampled($newImg, $image, 0, 0, 0, 0, $thumbW, $thumbH, $oldX, $oldY);

		if ($file['mime'] == 'image/jpeg') {
			imagejpeg($newImg);
		} elseif ($file['mime'] == 'image/png') {
			imagepng($newImg);
		} elseif ($file['mime'] == 'image/gif') {
			imagegif($newImg);
		}
		imagedestroy($newImg);
		die();
	}

	private function __formatSize($bytes) {
		$units = array('B', 'KB', 'MB', 'GB', 'TB');

		$bytes = max($bytes, 0);
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1);

		$bytes /= pow(1024, $pow);

		return round($bytes, 2) . ' ' . $units[$pow];
	}

	private function __downloadFile( $filePath ){
		
		$this->CI->load->helper('download');
		force_download( $filePath, NULL );
		
	}

}

$listing = new FileManager();

$successMsg = null;
$errorMsg = null;

if (isset($_FILES['upload'])) {
	$uploadStatus = $listing->upload();
	if ($uploadStatus == 1) {
		$successMsg = 'Your file was successfully uploaded!';
	} elseif ($uploadStatus == 2) {
		$errorMsg = 'Your file could not be uploaded. A file with that name already exists.';
	} elseif ($uploadStatus == 3) {
		$errorMsg = 'Your file could not be uploaded as the file type is blocked.';
	}
} elseif (isset($_POST['directory'])) {
	if ($listing->createDirectory()) {
		$successMsg = 'Directory Created!';
	} else {
		$errorMsg = 'There was a problem creating your directory.';
	}
} elseif (isset($_GET['deleteFile']) && $listing->enableFileDeletion) {
	if ($listing->deleteFile()) {
		$successMsg = 'The file was successfully deleted!';
	} else {
		$errorMsg = 'The selected file could not be deleted. Please check your file permissions and try again.';
	}
} elseif (/*isset($_GET['dir']) &&*/ isset($_GET['deleteDir']) && $listing->enableDirectoryDeletion) {
	if ($listing->deleteDirectory()) {
		$successMsg = 'The directory was successfully deleted!';
		unset($_GET['deleteDir']);
	} else {
		$errorMsg = 'The selected directory could not be deleted. Please check your file permissions and try again.';
	}
}

$data = $listing->run();

?>
<html>
<head>
	<title>Document management <?php echo $data['currentPath'] . (!empty($listing->pageTitle) ? ' (' . $listing->pageTitle . ')' : null); ?></title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; minimum-scale=1.0; user-scalable=no; target-densityDpi=device-dpi" />
	<meta charset="UTF-8">
	<style>
		
	</style>
	<?php if($listing->enableTheme): ?>
		<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/yeti/bootstrap.min.css" rel="stylesheet" integrity="sha256-gJ9rCvTS5xodBImuaUYf1WfbdDKq54HCPz9wk8spvGs= sha512-weqt+X3kGDDAW9V32W7bWc6aSNCMGNQsdOpfJJz/qD/Yhp+kNeR+YyvvWojJ+afETB31L0C4eO0pcygxfTgjgw==" crossorigin="anonymous">
	<?php endif; ?>
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"> -->
</head>
<body>
<div class="empty_div"></div>
	<div class="container-fluid filmanagercls document-fluid-wrapper">
		<?php if (! empty($listing->pageTitle)): ?>
			<div class="row">
				<div class="col-xs-12">
					<h1 class="filtittle"><?php echo $listing->pageTitle; ?></h1>
				</div>
			</div>
		<?php endif; ?>

		<?php if (! empty($successMsg)): ?>
			<div class="modal-alertsuccess alert alert-success">
				<div class="newupdate_alert">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
				<div class="pop-realted1">
				<div class="position-alert1">
				<?php echo $successMsg; ?>
			</div>
		</div>
	</div>
					


				</div>
		<?php endif; ?>

		<?php if (!empty($errorMsg)): ?>
			<div class="modal-alertsuccess alert alert-danger">
					<div class="newupdate_alert">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
				<div class="pop-realted1">
					<div class="position-alert1">
						<?php echo $errorMsg; ?>
					</div>
				</div>
			</div>
				</div>
		<?php endif; ?>

		<?php 
		$var_count=0;
		if(!empty($data['directoryTree'])): ?>
			<div class="row">
				<div class="col-xs-12 file-block1">
					<ul class="breadcrumb breadcrumbfile">
					<?php foreach ($data['directoryTree'] as $url => $name): ?>
						<li style="pointer-events: none;">
							<?php
								$name1='';
							if($var_count == 1 ||$var_count == 2){
								$name1=$name;
								if($var_count==2)
								{
									 	$_SESSION['doc_client_id']=$name1;
								}
							   if($var_count==1){
							  
								         $query=$this->db->query("SELECT * from firm where firm_id=$name1" );  
								            //echo $this->db->last_query(); 
								            $res=$query->row_array();
								             
							   	  $name1=$res['user_id'];
							   	}
							   	// $username=$this->Common_mdl->getUserProfileName($name);
							   	 $query = $this->db->query("select * from user where id='$name1'");
							    $row = $query->row_array();
							    $name1 = $row['crm_name'];

							// echo $name1;
									}

					

							$lastItem = end($data['directoryTree']);
							if($name === $lastItem):
									if($name1!=''){
										echo $name1;
									}
								else{
									echo $name;
									}
							else:
							?>
									
								<a href="?dir=<?php echo $url; ?>">
									<?php 		
									if($name1!=''){
										echo $name1;
									}
									else{
								echo $name;
							} ?>
								</a>
							<?php
							endif;
							?>
						</li>
					<?php
					$var_count++;
					 endforeach; ?>
					</ul>

				<div class="uploadcls">

				<?php 
				$root_link=base_url()."Documents?dir=./";
				$link="javascript:;";
				$uri_val=explode("/",$data['currentPath']);
			 $per=array_values($_SESSION['permission']['Documents']);
				if(count($uri_val) > 4 && $per[1]==1) { 
					// $temp=$data['directoryTree'];`
					$last = key(array_slice($data['directoryTree'], -1, 1, true));
					 $second_last = explode("/",$last);
					  //print_r($last);
					// print_r($data['directoryTree'][$second_last]);
					 array_shift($second_last);
					 array_pop($second_last);
					 array_pop($second_last);

					// unset($second_last[count($second_last)]);
					if(!empty($second_last))
					{
						$last= implode("/",$second_last);
						$link=base_url()."Documents?dir=/".$last;
					}
					else
					{
						$link=base_url()."Documents?dir=./";
					}
					
					//echo $last;

					?>

		

				<div class="docu-btns">
					<a title="New Folder" class="createFolder btn btn-link" data-toggle="modal" data-target="#createFolderModal">
					<span class="glyphicon glyphicon-plus"></span> Folder
					</a>

					<a title="Upload" class="uploadFiles btn btn-link" data-toggle="modal" data-target="#uploadFilesModal">
					<i class="glyphicon glyphicon-upload"></i> Upload
					</a>
					<?php } if(count($uri_val) > 4 ){  ?>
							<a title="Previous Folder" class="previous btn btn-link"  href="<?php echo $link ?>">
					<span class="glyphicon glyphicon-level-up"></span> Previous Folder
					</a>

					<a title="Root Folder" class="root btn btn-link"  href="<?php echo $root_link ?>">
					<span class="glyphicon glyphicon-level-up"></span> Root Folder
					</a>
				</div>
				<?php } ?>
				<div class="docu-input">
					<input type="text" name="for_kan_search" id="for_kan_search" class="for_kanban_search" placeholder="Search Directories">
				</div>
				</div>
				</div>
			</div>
		<?php endif; ?>

		

        
        <div id="previewImages" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-preview-files">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"></button>
		        <h4 class="modal-title">Preview</h4>
		      </div>
		      <div class="modal-body">
		        		
		        	<div class="preview-align-pdf">
		        		<div class="preview-align-pdf1">	</div>
		        	</div>

		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default close-ico" data-dismiss="modal">Close</button>
		      </div>
		    </div>

		  </div>
		</div>


		<div id="createFolderModal" class="modal fade modalcreatefolder" role="dialog">
		  <div class="modal-dialog modal-direct">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"></button>
		        <h4 class="modal-title">Create Directory</h4>
		      </div>
		      <div class="modal-body">
		        
		        <?php if($listing->enableDirectoryCreation): ?>
						
					<form action="" method="post" class="text-center form-inline">
						<div class="form-group">
							<label for="directory">Directory Name:</label>
							<input type="text" name="directory" id="directory" class="form-control directoryinput">
							<label class="errorMsg pull-left"></label>
							<button type="submit" class="btn btn-primary create_directory" name="submit"> 
							<span class="glyphicon glyphicon-plus"></span>
							 Create Directory</button>
						</div>
					</form>
											

				<?php endif; ?>

		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default close-ico" data-dismiss="modal">Close</button>
		      </div>
		    </div>

		  </div>
		</div>

		<div id="uploadFilesModal" class="modal fade createFolderModal" role="dialog">
		  <div class="modal-dialog modal-upload">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"></button>
		        <h4 class="modal-title">Upload A File</h4>
		      </div>
		      <div class="modal-body">
		        
		      	<?php if ($data['enableUploads']): ?>

					<form action="" method="post" enctype="multipart/form-data" class="text-center upload-form form-vertical">
						<div class=" upload-field">
							 
								<div class="form-group">
							<div class="file-input-up">
										 	<div class="custom_upload">
											<input type="file" name="upload[]" id="upload" class="form-control fileinput">
											<label class="errorMsg pull-left"></label>
										</div>
										 
											<a href="javascript:void(0)" class="btn btn-danger remove_file"><i class="glyphicon glyphicon-remove"></i></a>
											<a href="javascript:void(0)" class="btn btn-success add_file"><i class="glyphicon glyphicon-plus"></i></a>
										</div>
									</div>
								 
							</div>
						
						 
						<?php if ($listing->enableMultiFileUploads): ?>
							 
						 
									<button type="submit" class="btn btn-primary btn-block upload_files" name="submit">Upload File(s)</button>
							 
						<?php else: ?>
							 
									<button type="submit" class="btn btn-primary btn-block upload_files" name="submit">Upload File</button>
							 
						<?php endif; ?>
						</div>
					</form>

				<?php endif; ?>

		     
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default close-ico" data-dismiss="modal">Close</button>
		      </div>
			   </div>
		    </div>

		  </div>

		  <?php
			$fileIcons = array(
				'pdf' => '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>',
				'xls' => '<i class="fa fa-file-excel-o" aria-hidden="true"></i>',
				'xlsx' => '<i class="fa fa-file-excel-o" aria-hidden="true"></i>',
				'jpg' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>',
				'jpeg' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>',
				'png' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>',
				'gif' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>',
				'doc' => '<i class="fa fa-file-word-o" aria-hidden="true"></i>',
				'docx' => '<i class="fa fa-file-word-o" aria-hidden="true"></i>'
			  );

		  ?>
	
		<div class="col-xs-12 new-redesignfoler">
		<?php if (! empty($data['directories'])): ?>
			<div class="left-folder">
					<div class="table-container">
						<h3 class="tittleclsnew">FOLDERS</h3>
						<div class="director-folder_01">
						<ul class="directories">
							<?php 
							$uri_val=explode("/",$data['currentPath']);

							// print_r($data['directories']);

							foreach ($data['directories'] as $directory):

								

							 if(in_array($directory['name'],$_SESSION['document_userid']) && count($uri_val) < 4 )
							 {

							 	 $cli_name=$this->db->query("SELECT `crm_name` FROM user WHERE id='".$directory['name']."'")->row_array();

							?>
								<li class="search_val">
										<a href="<?php echo $directory['url']; ?>" class="item dir">


											<?php echo  $cli_name['crm_name']; ?>
										</a>

										<?php if ($listing->enableDirectoryDeletion): ?>
										<!-- 	<span class="pull-right deletecls">
												<a href="<?php echo $directory['inner']; ?>&deleteDir=<?php echo $directory['name'] ?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure?')"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a>
											</span> -->
										<?php endif; ?>
								</li>
							<?php 
						}
				 if(count($uri_val) >=4)

						{ ?>
								<li class="search_val">
										<a href="<?php echo $directory['url']; ?>" class="item dir">
											<?php echo $directory['name']; ?>
										</a>

										<?php 
												 $per=array_values($_SESSION['permission']['Documents']);
										if ($listing->enableDirectoryDeletion): 
												if($per[3]==1)
												{

										?>
											<span class="pull-right deletecls">
												<a href="<?php echo $directory['inner']; ?>&deleteDir=<?php echo $directory['name'] ?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure?')"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a>
											</span>
										<?php } endif; ?>
								</li>


						<?php }

							endforeach; ?>
						</ul>
					</div>
				</div>
				</div>
				
		<?php endif; ?>

		<?php if (! empty($data['files'])): ?>
			 <div class="left-folder">	
					<div class="table-container">
						<h3 class="tittleclsnew">FILES</h3>
						<div class="director-folder_01">
						<ul class="files">

						<?php foreach ($data['files'] as $file): ?>
							<li class="search_val1">
								<div class="preview-align1">
								
								<h3> <a href="<?php //echo $file['url']; ?>" target="<?php //echo $file['target']; ?>" class="item _blank <?php echo $file['extension']; ?> preview" data-toggle="modal" data-target="#previewImages" data-path="<?php echo $file['relativePath']; ?>"> <i class="fa fa-eye" aria-hidden="true"></i>
								</a>
								<span class="file_icon"><?php $arr_indx = end(explode(".", $file['relativePath']));
									echo $fileIcons[$arr_indx]; ?>
								</span> <span><?php echo $icon  ?></span> <?php echo $file['name']; ?></h3>
							</div>
								<div class="toprightcorner">
								<?php if (isset($file['preview']) && $file['preview']): ?>
									<span class="preview" data-toggle="modal" data-target="#previewImages" data-path="<?php echo $file['relativePath']; ?>"><img src="?preview=<?php echo $file['relativePath']; ?>"><i class="fa fa-search" aria-hidden="true"></i></span>
								
									<a href="<?php echo $file['base_url']; ?>?download=<?php echo urlencode($file['relativePath']); ?>" class="down-icon1"><i class="fa fa-download" aria-hidden="true"></i></a>
								<?php endif; ?>

								<?php
									 $per=array_values($_SESSION['permission']['Documents']);
									if($per[3]==1)
									{
								 if ($listing->enableFileDeletion == true): ?>
									<a href="<?php echo $file['inner']; ?>&deleteFile=<?php echo urlencode($file['relativePath']); ?>" class="pull-right btn btn-danger btn-xs" onclick="return confirm('Are you sure?')"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a>
								<?php 
								endif;
								} ?>
								</div>
							</li>
						<?php endforeach; ?>
						</ul>
					</div>
					</div>
				</div>
		<?php else: ?>
		</div>

		<!-- 	<div class="row">
				<div class="col-xs-12">
					<div class="modal-alertsuccess alert alert-info text-center">
						<div class="newupdate_alert">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
   <div class="pop-realted1">
      <div class="position-alert1">
					This directory does not contain any files.
				</div>
			</div>
		</div>
		
				</div>
				</div>
			</div> 
			 -->


		<?php  endif; ?>
	</div>
	<style>
		
	</style>
	
	<?php require_once( 'styles.php' ); ?>

	<?php if ($listing->enableMultiFileUploads): ?>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		<script>
			$(document).ready(function() {
				if($('.uploadcls').has('.docu-btns').length == 0){
					$('.docu-input input').addClass('docu-input-folder');
				}
			});
		</script>
		<script>
			$( document ).ready(function(){

				        $('.for_kanban_search').on('keyup',function(){
            //alert('121');
            var txt=$(this).val();
            $('.search_val').each(function(){
                   if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
                        $(this).show();
                 
                       //$(this).unwrap();

                   }
                   else
                   {
                     $(this).hide();
                
                   }
                });

        });

				chekRemoveOpt();

				$( document ).on('click', '.create_directory', function(e) {

					if( $('.directoryinput').val() == '' ){
						$(this).closest('.form-group').find('.errorMsg').text("Please enter folder name.");
						return false;
					} else {
						$(this).closest('.form-group').find('.errorMsg').text("");
					}

				});
				

				$( document ).on('click', '.upload_files', function(e) {
					//e.preventDefault();
					var invalid = 0;

					$.each( $('.fileinput'), function(){
						if( $(this).val() == '' ){
							$(this).closest('.row').find('.errorMsg').text("This field is Required.");
							invalid++;
						} else {
							$(this).closest('.row').find('.errorMsg').text("");
						}
					})

					if( invalid != 0 ){
						return false;
					}

				});

			});
			
			$( document ).on('click', '.add_file', function(e) {
				e.preventDefault();
				$('.upload-field:last').clone().insertAfter('.upload-field:last').find('input').val('');
				chekRemoveOpt();
			});

			$( document ).on('click', '.remove_file', function(e) {
				e.preventDefault();
				$( this ).closest('div.upload-field').remove();
				chekRemoveOpt();
			});

			$( document ).on('click', '.preview', function(e) {
				var path = $( this ).data('path');
				var preview = $("#previewImages .modal-body .preview-align-pdf1");
				preview.html('');
				preview.append( "<img src='"+path+"' />" );
			})

			function chekRemoveOpt(){
				
				if( $('.remove_file').length <= 1 ){
					$('.remove_file').hide();
				} else {
					$('.remove_file').show();
				}

			}

				  $(document).ready(function (){
				  	//$('.empty_div').html('');
					$(".permission_deined").each(function(){
					// alert('ok');
					$(this).html('');
					$(this).html('<div class="pcoded-content card-removes blank_page">   <div class="pcoded-inner-content">      <!-- Main-body start -->      <div class="main-body">         <div class="page-wrapper">            <!-- Page body start -->            <div class="page-body">               <div class="row">                  <div class="col-sm-12">                     <!-- Register your self card start -->                     <div class="card"> You Dont have a Permission to access this page</div> </div> </div>  </div>  </div></div></div></div>');
					});

				});
		</script>
	<?php endif; ?>
</body>
</html>