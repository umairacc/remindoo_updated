<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class PHPMailer_Lib
{
	public function __construct(){
	log_message('Debug', 'PHPMailer class is loaded.');
	}

	public function load()
	{
		// Include PHPMailer library files
		require_once APPPATH.'third_party/PHPMailer/src/Exception.php';
		require_once APPPATH.'third_party/PHPMailer/src/PHPMailer.php';
		require_once APPPATH.'third_party/PHPMailer/src/SMTP.php';

		$mail = new PHPMailer();
		return $mail;
	}
	public function Setup_config( $firm_id , $debug_level = 0 , $keep_alive = FALSE )
	{	
		$mail 			=	$this->load();
		$CI 		= & get_instance();
		$smtp_details   = $CI->db->get_where( "mail_protocol_settings", "firm_id=".$firm_id )->row_array();

	    // SMTP configuration
	    $_SESSION['debug_smtp'] = '';
	    $mail->isSMTP();

	    if( $keep_alive ) $mail->SMTPKeepAlive = true;

	    $mail->Hostname     	= 'remindoo.co';
	    $mail->SMTPDebug    	= $debug_level;
	    $mail->Debugoutput  	= function( $str , $level ){ echo  "</br><b>".$str."</b>"; $_SESSION['debug_smtp'] .= "</br><b>".$str."</b>"; };
	    $mail->Host         	= trim( $smtp_details['smtp_server'] );
	    $mail->SMTPAuth     	= true;
	    $mail->Username     	= trim( $smtp_details['smtp_username'] );
	    $mail->Password     	= $smtp_details['smtp_password'];
	    $mail->SMTPSecure   	= $smtp_details['smtp_security'];
	    $mail->Port         	= $smtp_details['smtp_port'];
	    //Set email format to HTML
	    $mail->isHTML( true );
	    $mail->CharSet = 'UTF-8';	  
	    $mail->setFrom( $smtp_details['smtp_email'] , $smtp_details['smtp_from_name'] );

	    return $mail;
	}
	public function AddRecipient( $mail , $recipient )
	{
		$mail->ClearAddresses();

		//check if multiple recipient
	    if( is_array( $recipient ) )
	    {
	        if( !empty( $recipient['to']) )
	        {
	            if( is_array( $recipient['to'] ) )
	            {
	                foreach ( $recipient['to'] as $to_address ) $mail->addAddress( $to_address );
	            }
	            else
	            {
	                $mail->addAddress( $recipient['to'] );
	            }
	        }
	        if( !empty( $recipient['cc']) )
	        {
	            if( is_array( $recipient['cc'] ) )
	            {
	                foreach ( $recipient['cc'] as $cc_address ) $mail->addCC( $cc_address );
	            }
	            else
	            {
	                $mail->addCC( $recipient['cc'] );
	            }
	        }
	    }
	    else
	    {    
	        $mail->addAddress( $recipient );
	    }

	    return $mail;
	}
	public function AddSubjectBody( $mail , $subject , $body )
	{
		// Email subject
	    $mail->Subject = strip_tags( html_entity_decode( $subject ) );

	    // Email body content        
	    $mail->Body = html_entity_decode( $body );

	    return $mail;
	}

	public function AddAttachment( $mail , $paths )
	{	
        $attachments 	= 	$paths;
		if( !is_array( $paths ) )
        {
            $attachments[] = $paths;
        }
        
		foreach ( $attachments as $path )
		{
	        $mail->addAttachment( $path );
	    }
	    return $mail;
	}

	public function Send( $mail )
	{	
    	$response       = ['result'=>1,'message'=>''];
	    try
	    {
	        if( !$mail->send() )
	        {               
	            $response['result'] = 0;
	        }
	        $response['message'] = !empty( $_SESSION['debug_smtp'] ) ? $_SESSION['debug_smtp'] : '';
	        unset( $_SESSION['debug_smtp'] );
	    }
	    catch (Exception $e)
	    {
	        $response['Exception'] = $e->errorMessage(); //Pretty error messages from PHPMailer
	    }
	    catch (\Exception $e)
	    {   
	        //The leading slash means the Global PHP Exception class will be caught
	        $response['Exception'] = $e->getMessage(); //Boring error messages from anything else!
	    }
	   	return $response;
	}

}