<?php
Class Task_recuring_model extends CI_Model {

       public function __construct()
    {
        parent::__construct();
       $this->load->model(array('Common_mdl','Security_model','Invoice_model','Task_invoice_model'));
    }

public function for_reminder_sample($task_id)
{
   // $task_data=$this->db->query("select * from add_new_task where id=".$task_id)->row_array();

   //    $for_reminder_chk_box=$task_data['for_reminder_chk_box'];
   //    $res=json_decode($for_reminder_chk_box, true);
   //   foreach ($res as $re_key => $re_value) {
      
   //       $ex_reminder=explode("//", $re_value);
   //       $re_date=$ex_reminder[0];
   //       $re_time=$ex_reminder[1];
   //      echo $re_date."--".$re_time."<br>";
   //       $current_date=date('Y-m-d');
   //      $dateTime = new DateTime('now', new DateTimeZone(date_default_timezone_get()));   
   //      $zz= $dateTime->format("h:i A"); 

   //       if(($current_date==$re_date))
   //       {
           // echo $task_id;
            echo "send";
            $this->Task_invoice_model->task_new_mail($task_id,'for_reminder');
         // }
     //}
}

public function Recurring_Task_WeekDaysWise( $Task_Data )
  {
    $start_of_the_week = strtotime("Last Monday");

    if ( date('l') === 'Monday' )
    {
        $start_of_the_week = strtotime('today');
    }
    $dates = array();
    $mon = date('Y-m-d',$start_of_the_week);
    $tue = date('Y-m-d',strtotime(date("Y-m-d",$start_of_the_week)." +1 days"));
    $wed = date('Y-m-d',strtotime(date("Y-m-d",$start_of_the_week)." +2 days"));
    $thu = date('Y-m-d',strtotime(date("Y-m-d",$start_of_the_week)." +3 days"));
    $fri = date('Y-m-d',strtotime(date("Y-m-d",$start_of_the_week)." +4 days"));
    $sat = date('Y-m-d',strtotime(date("Y-m-d",$start_of_the_week)." +5 days"));
    $sun = date('Y-m-d',strtotime(date("Y-m-d",$start_of_the_week)." +6 days"));
    $dates=array( 'Monday' =>$mon,
            'Tuesday' => $tue,
            'Wednesday' => $wed,
            'Thursday' => $thu,
            'Friday' => $fri,
            'Saturday' => $sat,
            'Sunday' => $sun);
    /******************************/
    //Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday

    $task_days=explode(',',$Task_Data['sche_on']);

    if ( array_search( date('l') , $task_days) !== FALSE )
    {
      //for new recuriing task start and end date
      $this->recuring_sample_task( $Task_Data['id'] );
    }
    if( $current_date == $sun )
    {
      $data['recuring_start_time']=date("Y-m-d", strtotime("+".$recuring_week." week"));
      $this->Common_mdl->update('add_new_task',$data,'id',$task_id);          
    }
  }

 

  public function recuring_sample_task( $task_id )
  {
    $task_data=$this->db->query("select * from add_new_task where id=".$task_id)->row_array();
    $recuring_task_ids=$task_data['recuring_task_ids'];
    $get_data=$this->db->query('SELECT * FROM firm_assignees where module_name="TASK" and module_id='.$task_id.'' )->result_array();

    $date1 = date_create($task_data['start_date']);
    $date2 = date_create($task_data['end_date']);
    $diff  = date_diff($date1,$date2);
    $diffence = $diff->format("%a");
    $data['start_date'] = date('Y-m-d');
    $data['end_date']   = date('Y-m-d', strtotime( date("Y-m-d")." +".$diffence." days" ) );    

    $data['user_id']=$task_data['user_id'];
    $data['public']=$task_data['public'];
    $data['billable']=$task_data['billable'];
    $data['subject']=$task_data['subject'];        
    
    $data['service_due_date']=date('Y-m-d');  
    $data['priority']=$task_data['priority'];
   
    $data['related_to']=$task_data['related_to'];
    $data['project_id']=$task_data['project_id'];
    $data['company_name']=$task_data['company_name'];
    $data['firm_id']=$task_data['firm_id'];

    // $data['worker']=$task_data['worker'];
    // $data['team']=$task_data['team'];
    // $data['department']=$task_data['department'];
    // $data['for_old_assign']=$task_data['for_old_assign'];

    // $data['manager']=$task_data['manager'];
    $data['tag']=$task_data['tag'];

    $data['sche_start_date']='';
    $data['sche_send_time']='';
    $data['sche_repeats']='';
    $data['sche_every_week']='';
    $data['sche_on']='';
    $data['sche_ends']='';
    $data['sche_after_msg']='';
    $data['sche_on_date']='';
    $data['specific_time']='';

    $data['description']=$task_data['description'];
    $data['customize']=$task_data['customize'];
    $data['selectuser']=$task_data['selectuser'];
     $data['task_status']='1';
    $data['created_date']=time();

    $data['attach_file']=$task_data['attach_file'];
   
    /** newly added rs **/
    $data['create_by']=$task_data['create_by'];
    /* print_r($data);
    exit;*/

    /** for reminder popup data 18-06-2018 **/
    $data['recuring_sus_msg']='';
    $data['reminder_specific_time']='';
    $data['reminder_specific_timepic']='';
    $data['reminder_chk_box']='';
    $data['reminder_msg']='';

    /** end of reminder popup data **/

    $insert_data=$this->db->insert( 'add_new_task', $data );
    $in=$this->db->insert_id();

    $log=$data['subject']." task was created by Chron";
    $this->Common_mdl->Create_Log( 'TASK' , $in , $log );
    $this->Common_mdl->task_setting($in);
    foreach ($get_data as $key => $value) 
    {        
      $data = ['firm_id'=>$task_data['firm_id'],'module_name'=>'TASK','module_id'=>$in ,'assignees'=>$value['assignees'] ];         
      $this->db->insert('firm_assignees',$data);        
    }
    /** for insert recuring task ids **/
      
    $recuring_task_ids.=($recuring_task_ids!='' ? ','.$in : $in );    

    $dataz['recuring_task_ids'] = $recuring_task_ids;
    $dataz['recuring_task_count'] = $task_data['recuring_task_count'] + 1;
    

    $this->Common_mdl->update('add_new_task',$dataz,'id',$task_id);

    $this->Task_invoice_model->task_new_mail($in,'for_add');

    if( $insert_data )
    {
      $in=$this->db->insert_id();
      echo '1'; 
    }
    else
    {
      echo '0';
    }

  }
  }