<?php
Class Task_section_model extends CI_Model {

       public function __construct()
    {
        parent::__construct();
       $this->load->model(array('Common_mdl','Security_model','Invoice_model','Task_invoice_model'));
    }

    public function get_services_options_insert( $task_id )
    {

     $get_task_data = $this->Common_mdl->select_record('add_new_task','id',$task_id);
      $services=explode("_",$get_task_data['related_to_services']);

      if(isset($services[1]) && $services[0]=='wor')
      {
        $table='work_flow';
        $service_id=$services[1];
        $column='work_flow_id';
      }
      else
      {
         $table='service_lists';
        $service_id=(isset($services[1]))?$services[1]:$services[0];
        $column='service_id';
      }

      


    $services_lists = $this->db->query("select * from ".$table." where id='".$service_id."' ")->row_array();
      
      //$records = $this->db->query("select * from service_steps where ".$column."=".$services_lists['id']." and firm_id=".$get_task_data['firm_id']." ")->result_array();  
      $records=$this->Service_model->get_steps( $services_lists['id'] , $column );

      $Subtask = [];

      foreach($records as $rec)
      {

          $main_task_title=$rec['title'];

          $main_task_id = $rec['id'];

          if( empty($_POST['inc_exc_steps']) || in_array( $main_task_id , $_POST['inc_exc_steps'] ) )
          {

            $Subtask[] =$this->task_creation($task_id,$main_task_title,$main_task_id,'');

          }
                  
      }
      
          $datas['sub_task_id'] = implode( ',' , $Subtask ); 

          $this->Common_mdl->update('add_new_task',$datas,'id',$task_id);

        if(count($Subtask) && isset($_SESSION['id']) )
        {
          $username=$this->Common_mdl->getUserProfileName($_SESSION['id']); 
          $activity_datas['log'] = "Subtask Was included  by " .$username;
          $activity_datas['createdTime'] = time();
          $activity_datas['module'] = 'Task';
          $activity_datas['sub_module']='';
          $activity_datas['user_id'] = $_SESSION['id'];
          $activity_datas['module_id'] = $task_id;
          $this->Common_mdl->insert('activity_log',$activity_datas);
        }    
 
  
  }
  /*
    @Don't add any session variable to this function,it is call from cron function
  */
  public function task_creation($task_id,$subject,$main_task_id,$sub_task_id)
  {
   // echo "another";
          if(isset($_SESSION['id']))
          {
              $task_val=$_SESSION['id'];
          }
          else 
          {
              $task_val="Chron";
          }

      $task_data=$this->db->query("select * from add_new_task where id=".$task_id)->row_array();
      $task_status = 'main';
      if($sub_task_id!='')
      {
        $task_status='sub';
      }
           // $task_name=$task_data['subject']."-".$subject."-".$task_status;
            $task_name=$subject;

            $data['services_main_id']=$main_task_id;
            $data['services_sub_id']=$sub_task_id;
            $data['user_id']=$task_data['user_id'];
            $data['public']=$task_data['public'];
            $data['billable']=0;
           // $data['subject']=$subject;
            $data['subject']=$task_name;
            $data['service_due_date']=$task_data['start_date'];
            $data['start_date']=$task_data['start_date'];
            $data['end_date']=$task_data['end_date'];

            $data['priority']=$task_data['priority'];
           
            //$data['related_to']='magic task';
            $data['firm_id']=$task_data['firm_id'];
            $data['related_to']='sub_task';
            $data['related_to_services']=$task_data['related_to_services'];
            $data['project_id']=$task_data['project_id'];
            $data['company_name']=$task_data['company_name'];
      
            $data['worker']=$task_data['worker'];
            $data['team']=$task_data['team'];
            $data['department']=$task_data['department'];
            $data['for_old_assign']=$task_data['for_old_assign'];
       
            $data['manager']=$task_data['manager'];
            $data['tag']=$task_data['tag'];

            $data['sche_start_date']='0';
            $data['sche_send_time']='0';
            $data['sche_repeats']='0';
            $data['sche_every_week']='0';
            $data['sche_on']='0';
            $data['sche_ends']='0';
            $data['sche_after_msg']='0';
            $data['sche_on_date']='0';
            $data['specific_time']='0';

          //  $data['description']=$task_data['description'];
            $data['customize']=$task_data['customize'];
             $data['selectuser']=$task_data['selectuser'];
             $data['task_status']=$task_data['task_status'];
             $data['created_date']=time();

             $data['attach_file']=$task_data['attach_file'];
           
            /** newly added rs **/
            $data['create_by']=$task_data['create_by'];
           /* print_r($data);
            exit;*/

/** for reminder popup data 18-06-2018 **/
    $data['recuring_sus_msg']='';
    $data['reminder_specific_time']='';
    $data['reminder_specific_timepic']='';
    $data['reminder_chk_box']='';
    $data['reminder_msg']='';

/** end of reminder popup data **/

            $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();

             $this->Common_mdl->task_setting($in);
             $log=$task_data['subject']." task was created by ".$task_val;
             $this->Common_mdl->Create_Log( 'TASK' , $in , $log );


            //$this->Common_mdl->add_assignto( $_POST['assign_role'] , $in , 'TASK' );
            $this->db->where( 'module_name' , 'TASK' );
            $this->db->where( 'module_id' , $task_data['id']);
            $assignee = $this->db->get('firm_assignees')->result_array();

             foreach ($assignee as $key => $value)
            {

              $data = ['firm_id'=>$task_data['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$value['assignees'] ];
             
              $this->db->insert('firm_assignees',$data);
            }

            return $in;

  }
    /** 16-10-2018 for services sub tasks  in creation section **/

 public function get_services_options_insertbk( $service , $task_id )
 {

  $s_end=array(
    "confirmation" => ['','crm_confirmation_statement_due_date'],
    "accounts" => ['crm_ch_yearend','crm_ch_accounts_next_due'],
    "tax_return" => ['crm_accounts_tax_date_hmrc','crm_accounts_tax_date_hmrc'],
    "personal_tax" => ['crm_personal_tax_return_date','crm_personal_due_date_return'],
    "vat" => ['','crm_vat_due_date'],
    "payroll" => ['','crm_rti_deadline'],
    "pension" => ['','crm_pension_subm_due_date'],
    "cis" => ['','crm_cis_subcontractor_start_date'],
    "cis_sub" => ['','crm_cis_subcontractor_start_date'],
    "bookkeep" => ['','crm_next_booking_date'],
    "p11d" => ['','crm_next_p11d_return_due'],
    "management" => ['','crm_next_manage_acc_date'],
    "insurence" => ['','crm_insurance_renew_date'],
    "investigation" => ['','crm_investigation_end_date'],
    "register" => ['','crm_registered_renew_date']
    );

    $assignee = array('team'=> [], 'department' => [] , 'assign' => [] );

    if(isset($_POST['worker']))
    {
        foreach ($_POST['worker'] as $key => $value) 
        {
          $ex_val=explode('_', $value);
          if(count($ex_val)>1)
          {
            if($ex_val[0]=='tm')
            {
               array_push($assignee['team'] , $ex_val[1]);
            }
            if($ex_val[0]=='de')
            {
                array_push($assignee['department'] , $ex_val[1]);
            }
          }
          else
          {
             array_push($assignee['assign'] , $ex_val[0]);
          }
        }
    }

    $assignee['manager'] = $_POST['manager'];


    $for_old_assign = json_encode( $assignee );


    $assignee = array_map( function(array $a){ return implode(',',$a); }, $assignee );






    $client_id = $_POST['company_name'];
    $client_data = $this->db->query("select * from client where id =".$client_id." ")->row_array();




      if(!empty($_POST['services']))
      { 
        $services = "'".implode("','",$_POST['services'])."'";         
        $services_lists=$this->db->query("select * from service_lists where id IN (".$services.")  ")->result_array();

        $exc_steps = (!empty($_POST['inc_exc_steps']))?$_POST['inc_exc_steps']:[];
        $exc_steps_details = (!empty($_POST['inc_exc_steps_details']))?$_POST['inc_exc_steps_details']:[];
      
        
        foreach ($services_lists as $datas) 
        {

          /*end Date*/
          $Edate_feild = $s_end[$datas['services_subnames']][1];
          $service_Edate = $client_data[$Edate_feild];
          $service_Edate = (!empty($service_Edate) ? strtotime($service_Edate."+1 year") : strtotime('+ 10 day') );
          $e_date = date( 'd-m-Y' , $service_Edate );
          /*end Date*/

          $records=$this->db->query("select * from service_steps where service_id=".$datas['id']." and firm_id=".$_SESSION['firm_id']." ")->result_array();
           
            foreach($records as $rec)
            {
              if(in_array($rec['id'] , $exc_steps))
              {
                  $main_task_title=$rec['title'];
                  $main_task_id=$rec['id'];

                  $task_detail= [
                  'related_to_services'=>$datas['services_subnames'],
                  's_date'=>date('d-m-Y'),
                  'e_date'=>$e_date,
                  'subject'=>$datas['services_subnames']."-".$main_task_title,
                  'assignee'=>$assignee,
                  'old_assign' => $for_old_assign
                  ];

                  $main_id = $this->task_creation($task_detail,$main_task_title,$main_task_id,'');                            
                
                //  $steps_details = $this->db->query("select * from service_step_details where service_id=".$datas['id']." and step_id=".$rec['id']." and created_user=".$_SESSION['id']." ")->result_array();
                  $steps_details = $this->Service_model->get_sub_steps($rec['id'], 'step_id' );

                   $sub_ids=array();
                 foreach($steps_details as $steps)
                  { 
                    $sub_task_content=$steps['step_content'];
                    $sub_task_id=$steps['id'];

                    if(in_array($steps['id'] , $exc_steps_details))
                    {
                      $res1=$this->task_creation($task_detail,$sub_task_content,$main_task_id,$sub_task_id);
                      array_push($sub_ids, $res1);
                    }
                  }

                  if(!count($steps_details))
                  {
                      $res1=$this->task_creation($task_detail,$main_task_title,$main_task_id,'0');
                      array_push($sub_ids, $res1);                
                  }

                $data['sub_task_id']=implode(',', $sub_ids);                
                $this->Common_mdl->update('add_new_task',$data,'id',$main_id);




              /** new task send a mail to a assignee **/
               $this->Task_invoice_model->task_new_mail($main_id,'for_add');

                $data12 = array('task_id' => $main_id,
                        'comment' => '1',
                        'task_timer' => '1',
                        'attach_file' => '1',
                        'user_edit_task'=>'1',
                );
                $this->Common_mdl->insert('task_setting',$data12);

                  if(count($sub_ids))
                {              
                  $username=$this->Common_mdl->getUserProfileName($_SESSION['id']); 
                  $activity_datas['log'] = "Subtask Was included  by " .$username;
                  $activity_datas['createdTime'] = time();
                  $activity_datas['module'] = 'Task';
                  $activity_datas['sub_module']='';
                  $activity_datas['user_id'] = $_SESSION['id'];
                  $activity_datas['module_id'] = $main_id;
                  $this->Common_mdl->insert('activity_log',$activity_datas);
                } 
            }

          }

        }
            
             
      }
  
  }
  public function task_creationbk($task_detail,$subject,$main_task_id,$sub_task_id)
  {
    
      $task_status='main';
      if($sub_task_id!='')
      {
        $task_status='sub';
      }

            $task_name=$task_detail['subject'];
            
            if(!empty($sub_task_id)) $task_name .="-".$subject;

            $task_name .= "-".$task_status; 

            $data['services_main_id']=$main_task_id;
            $data['services_sub_id']=$sub_task_id;
            $data['user_id']=$_SESSION['id'];
            $data['billable']=0;
           // $data['subject']=$subject;
            $data['subject']=$task_name;
            $data['start_date']=$task_detail['s_date'];
            $data['end_date']=$task_detail['e_date'];

            $data['priority']=(!empty($_POST['priority']) ? $_POST['priority'] : '' );
           if($sub_task_id!='')
            {
              $data['related_to']='magic task';
            }
            else
            {
             $data['related_to']='tasks'; 
            }   

            $data['related_to_services']=$task_detail['related_to_services'];
            
            $data['company_name']=(!empty( $_POST['company_name'] ) ? $_POST['company_name'] : '' );
      
            $data['worker']=$task_detail['assignee']['assign'];
            $data['team']=$task_detail['assignee']['team'];
            $data['department']=$task_detail['assignee']['department'];

            $data['for_old_assign']=$task_detail['old_assign'];
       
            $data['manager']=$task_detail['assignee']['manager'];
            $data['tag']= (!empty($_POST['tags']) ? $_POST['tags'] : '' );
          
             $data['task_status']= (!empty( $_POST['task_status'] ) ? $_POST['task_status'] : '' );

             $data['created_date']=time();

             $data['attach_file']= (!empty($_POST['attach_file_urls']) ? $_POST['attach_file_urls'] : '' );
           
            /** newly added rs **/
            $data['create_by']= $_SESSION['id'];
           /* print_r($data);
            exit;*/

            $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();


        unset($task_detail['assignee']['manager']);

        foreach ($task_detail['assignee'] as $a_type => $a_str) 
        {
          $a_type = ($a_type=="assign") ? 'staff' : $a_type ;

          $add_remove_list['task_id']=$in;         
          $add_remove_list['status']='active';
          $add_remove_list['removed_workers_timing']='';
          $add_remove_list['added_removed_date']=time();
          $add_remove_list['created_by']=$_SESSION['id'];
          $add_remove_list['created_time']=time();
          $add_remove_list['staff_team_department']=$a_type;
          $a_arr = explode(',', $a_str);
          foreach ($a_arr as $id)
          {
          $add_remove_list['assignee_ids']=$id;
          $this->Common_mdl->insert('task_assignee_add_remove_list',$add_remove_list);
          }
          
        }



return $in;

  }


  //update main task data to its subtask if main task change
  public function update_subtasks($main_task_id)
  {

  $task_data=$this->db->query("select * from add_new_task where id=".$main_task_id)->row_array();

  if(!empty($task_data['sub_task_id']))
  {  $affected=0; 
    $sub_tasks_ids = explode(',', $task_data['sub_task_id']);
    foreach ($sub_tasks_ids as $id) 
    {
        $sub_task_data=$this->db->query("select * from add_new_task where id=".$id)->row_array();
        
        $subject = explode('-',$sub_task_data['subject']);
        
            $subject = $task_data['subject'].'-'.$subject[1].'-'.$subject[2];
            $data['subject'] = $subject;
            $data['description'] = $task_data['description'];
            $data['customize'] = $task_data['customize'];
            $data['selectuser'] = $task_data['selectuser'];
            $data['task_status'] = $task_data['task_status'];
            $data['attach_file'] = $task_data['attach_file'];     
            $data['related_to_services'] = $task_data['related_to_services'];
            $data['project_id'] = $task_data['project_id'];
            $data['company_name'] = $task_data['company_name'];      
            $data['worker'] = $task_data['worker'];
            $data['team'] = $task_data['team'];
            $data['department'] = $task_data['department'];
            $data['for_old_assign'] = $task_data['for_old_assign'];       
            $data['manager'] = $task_data['manager'];
            $data['tag'] = $task_data['tag'];
            $data['start_date'] = $task_data['start_date'];
            $data['end_date'] = $task_data['end_date'];
            $data['priority'] = $task_data['priority'];
            $data['user_id'] = $task_data['user_id'];
            $data['public'] = $task_data['public'];
            $this->db->update("add_new_task",$data,"id=".$id." ");
            $affected = $this->db->affected_rows();
    }
    return $affected;
  }
  
  }













    /** end of 16-10-2018 **/
    /** 22-10-2018 **/
 function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
   }

   /** for timer 02-07-2018 **/
function calculate_test($a,$b){
$difference = $b-$a;

$second = 1;
$minute = 60*$second;
$hour   = 60*$minute;

$ans["hour"]   = floor(($difference)/$hour);
$ans["minute"] = floor((($difference)%$hour)/$minute);
$ans["second"] = floor(((($difference)%$hour)%$minute)/$second);
//echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

$test=$ans["hour"].":".$ans["minute"].":".$ans["second"];
return $test;
}
function time_to_sec($time) {
list($h, $m, $s) = explode (":", $time);
$seconds = 0;
$seconds += (intval($h) * 3600);
$seconds += (intval($m) * 60);
$seconds += (intval($s));
return $seconds;
}
function sec_to_time($sec) {
return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}
 public function calculate_timing_status_removed($time_start_pause){

    if($time_start_pause!=''){
      $res=explode(',',$time_start_pause);
      //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
      $res1=array_chunk($res,2);
      $result_value=array();
      $pause='on';
      foreach($res1 as $rre_key => $rre_value)
      {
         $abc=$rre_value;
         if(count($abc)>1){
         if($abc[1]!='')
         {
            $ret_val=$this->calculate_test($abc[0],$abc[1]);
            array_push($result_value, $ret_val) ;
         }
         else
         {
          $pause='';
            $ret_val=$this->calculate_test($abc[0],time());
             array_push($result_value, $ret_val) ;
         }
        }
        else
        {
          $pause='';
            $ret_val=$this->calculate_test($abc[0],time());
             array_push($result_value, $ret_val) ;
        }


      }

      $time_tot=0;
       foreach ($result_value as $re_key => $re_value) {
          $time_tot+=$this->time_to_sec($re_value) ;
       }
       $hr_min_sec=$this->sec_to_time($time_tot);
       $hr_explode=explode(':',$hr_min_sec);
       $hours=(int)$hr_explode[0];
       $min=(int)$hr_explode[1];
       $sec=(int)$hr_explode[2];
       


      }
      else
      {
        $hours=0;
        $min=0;
        $sec=0;
      }
  return $hours."-".$min."-".$sec;

 }
    /** end of 22-10-2018 **/

}
