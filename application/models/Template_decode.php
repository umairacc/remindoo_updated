<?php
class Template_decode extends CI_Model
{
	public function email_decode($client_id)
	{ 	//echo $client_id;
		$data=$this->db->query("select * from client where id=$client_id")->result_array();
		
		//printf($data);		
		$service_array=array();
		$servicedue_date_array=array();
		foreach($data as $service)
		{ 
			// echo '<pre>';
			// print_r($service_array);
			// print_r($servicedue_date_array);
			// echo '</pre>';
			(isset(json_decode($service['conf_statement'])->tab) && $service['conf_statement'] != '') ? $jsnvat =  json_decode($service['conf_statement'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $conf_statement = "On" : $conf_statement = "Off";
			//accounts
			 (isset(json_decode($service['accounts'])->tab) && $service['accounts'] != '') ? $jsnvat =  json_decode($service['accounts'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $accounts = "On" : $accounts = "Off";
			//company_tax_return
			 (isset(json_decode($service['company_tax_return'])->tab) && $service['company_tax_return'] != '') ? $jsnvat =  json_decode($service['company_tax_return'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $company_tax_return = "On" : $company_tax_return = "Off";
			//personal_tax_return
			 (isset(json_decode($service['personal_tax_return'])->tab) && $service['personal_tax_return'] != '') ? $jsnvat =  json_decode($service['personal_tax_return'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off";
			//payroll
			 (isset(json_decode($service['payroll'])->tab) && $service['payroll'] != '') ? $jsnvat =  json_decode($service['payroll'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $payroll = "On" : $payroll = "Off";
			//workplace
			 (isset(json_decode($service['workplace'])->tab) && $service['workplace'] != '') ? $jsnvat =  json_decode($service['workplace'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $workplace = "On" : $workplace = "Off";
			//vat
			 (isset(json_decode($service['vat'])->tab) && $service['vat'] != '') ? $jsnvat =  json_decode($service['vat'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $vat = "On" : $vat = "Off";
			//cis
			 (isset(json_decode($service['cis'])->tab) && $service['cis'] != '') ? $jsnvat =  json_decode($service['cis'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $cis = "On" : $cis = "Off";
			//cissub
			 (isset(json_decode($service['cissub'])->tab) && $service['cissub'] != '') ? $jsnvat =  json_decode($service['cissub'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $cissub = "On" : $cissub = "Off";
			//p11d
			 (isset(json_decode($service['p11d'])->tab) && $service['p11d'] != '') ? $jsnvat =  json_decode($service['p11d'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $p11d = "On" : $p11d = "Off";
			//bookkeep
			 (isset(json_decode($service['bookkeep'])->tab) && $service['bookkeep'] != '') ? $jsnvat =  json_decode($service['bookkeep'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $bookkeep = "On" : $bookkeep = "Off";
			//management
			 (isset(json_decode($service['management'])->tab) && $service['management'] != '') ? $jsnvat =  json_decode($service['management'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $management = "On" : $management = "Off";
			//investgate
			 (isset(json_decode($service['investgate'])->tab) && $service['investgate'] != '') ? $jsnvat =  json_decode($service['investgate'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $investgate = "On" : $investgate = "Off";
			//registered
			 (isset(json_decode($service['registered'])->tab) && $service['registered'] != '') ? $jsnvat =  json_decode($service['registered'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $registered = "On" : $registered = "Off";
			//taxadvice
			 (isset(json_decode($service['taxadvice'])->tab) && $service['taxadvice'] != '') ? $jsnvat =  json_decode($service['taxadvice'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $taxadvice = "On" : $taxadvice = "Off";
			//taxinvest
			  (isset(json_decode($service['taxinvest'])->tab) && $service['taxinvest'] != '') ? $jsnvat =  json_decode($service['taxinvest'])->tab : $jsnvat = '';
			  ($jsnvat!='') ? $taxinvest = "On" : $taxinvest = "Off"; ?>


			  <?php 
			  // $status=explode(',',$_SESSION['service']);
			  // for($i=0;$i<count($status);$i++){
			  //   if($status[$i]=='conf_statement'){
			    if($conf_statement=='On'){ 
			      array_push($service_array,'Confirmation Statements');
			      $confirmation_start_date=$service['crm_confirmation_statement_date'];
			      $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
			        if($confirmation_due_date!=''){
			          $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
			        }else{
			          $confirmation_due_date='-';
			        }
			      array_push($servicedue_date_array,$confirmation_due_date);
			    } 
			  //}
			  //
			    if($accounts=='On'){ 
			      array_push($service_array,'Accounts');
			      $accounts_due_date=$service['crm_ch_accounts_next_due'];
			        if($accounts_due_date!=''){
			          $accounts_due_date=$service['crm_ch_accounts_next_due'];
			        }else{
			          $accounts_due_date='-';
			        }
			      array_push($servicedue_date_array,$accounts_due_date);
			    } 
			 // }
			  //if($status[$i]=='company_tax_return'){
			    if($company_tax_return=='On'){ 
			      array_push($service_array,'Company Tax Return');
			      $company_tax_due_date=$service['crm_accounts_tax_date_hmrc']; 
			        if($company_tax_due_date!=''){
			          $company_tax_due_date=$service['crm_accounts_tax_date_hmrc']; 
			        }else{
			          $company_tax_due_date='-'; 
			        }
			      array_push($servicedue_date_array,$company_tax_due_date);
			    } 
			 // }
			  //if($status[$i]=='personal_tax_return'){
			    if($personal_tax_return=='On'){ 
			    array_push($service_array,'Personal Tax Return');
			    $personal_tax_due_date=$service['crm_personal_due_date_return']; 
			      if($personal_tax_due_date!=''){
			        $personal_tax_due_date=$service['crm_personal_due_date_return']; 
			      }else{
			        $personal_tax_due_date='-'; 
			      }
			    array_push($servicedue_date_array,$personal_tax_due_date);
			    } 
			 // }
			 // if($status[$i]=='vat'){
			    if($vat=='On'){ 
			    array_push($service_array,'VAT Returns');
			    $vat_due_date=$service['crm_vat_due_date']; 
			      if($vat_due_date!=''){
			        $vat_due_date=$service['crm_vat_due_date']; 
			      }else{
			        $vat_due_date='-';  
			      }
			      array_push($servicedue_date_array,$vat_due_date);
			    } 
			 // }
			 // if($status[$i]=='payroll'){
			    if($payroll=='On'){ 
			    array_push($service_array,'Payroll');
			    $payroll_due_date=$service['crm_rti_deadline']; 
			      if($payroll_due_date!=''){
			        $payroll_due_date=$service['crm_rti_deadline']; 
			      }else{
			        $payroll_due_date='-';  
			      }
			      array_push($servicedue_date_array,$payroll_due_date);
			    }
			 // }  
			 // if($status[$i]=='workplace'){
			    if($workplace=='On'){ 
			    array_push($service_array,'WorkPlace Pension - AE');
			    $work_place_due_date=$service['crm_pension_subm_due_date']; 
			      if($work_place_due_date!=''){
			        $work_place_due_date=$service['crm_pension_subm_due_date']; 
			      }else{
			        $work_place_due_date='-';   
			      }
			      array_push($servicedue_date_array,$work_place_due_date);
			    } 
			//  }
			 // if($status[$i]=='cis'){
			    if($cis=='On'){ 
			    array_push($service_array,'CIS - Contractor');
			    $cis_due_date='-'; 
			    array_push($servicedue_date_array,$cis_due_date);
			    } 
			 // }
			 // if($status=='cissub'){
			    if($cissub=='On'){ 
			    array_push($service_array,'CIS - Sub Contractor');
			    $cis_sub_due_date='-'; 
			    array_push($servicedue_date_array,$cis_sub_due_date);
			    } 
			 // }
			//  if($status[$i]=='p11d'){
			    if($p11d=='On'){ 
			    array_push($service_array,'P11D');
			    $p11d_due_date=$service['crm_next_p11d_return_due']; 
			      if($p11d_due_date!=''){
			        $p11d_due_date=$service['crm_next_p11d_return_due']; 
			      }else{
			        $p11d_due_date='-';   
			      }
			      array_push($servicedue_date_array,$p11d_due_date);
			    } 
			 // }
			  //if($status[$i]=='management'){
			    if($management=='On'){ 
			    array_push($service_array,'Management Accounts');
			    $management_due_date=$service['crm_next_manage_acc_date']; 
			      if($management_due_date!=''){
			        $management_due_date=$service['crm_next_manage_acc_date']; 
			      }else{
			        $management_due_date='-';   
			      }
			      array_push($servicedue_date_array,$management_due_date);
			    } 
			 // }
			 // if($status[$i]=='bookkeep'){
			    if($bookkeep=='On'){ 
			    array_push($service_array,'Bookkeeping');
			    $bookkeep_due_date=$service['crm_next_booking_date']; 
			      if($bookkeep_due_date!=''){
			        $bookkeep_due_date=$service['crm_next_booking_date']; 
			      }else{
			        $bookkeep_due_date='-';   
			      }
			      array_push($servicedue_date_array,$bookkeep_due_date);
			    }
			//  }
			  //if($status[$i]=='investgate'){ 
			    if($investgate=='On'){ 
			    array_push($service_array,'Investigation Insurance');
			    $investigate_due_date=$service['crm_insurance_renew_date']; 
			      if($investigate_due_date!=''){
			        $investigate_due_date=$service['crm_insurance_renew_date']; 
			      }else{
			        $investigate_due_date='-'; 
			      }
			      array_push($servicedue_date_array,$investigate_due_date);
			    } 
			 // }
			 // if($status[$i]=='registered'){
			    if($registered=='On'){ 
			    array_push($service_array,'Registered Address');
			    $registered_due_date=$service['crm_registered_renew_date']; 
			      if($registered_due_date!=''){
			        $registered_due_date=$service['crm_registered_renew_date'];
			      }else{
			        $registered_due_date='-';
			      }
			      array_push($servicedue_date_array,$registered_due_date);
			    } 
			  //}
			 // if($status[$i]=='taxadvice'){
			    if($taxadvice=='On'){ 
			    array_push($service_array,'Tax Advice');
			    $taxadvice_due_date=$service['crm_investigation_end_date']; 
			      if($taxadvice_due_date!=''){
			        $taxadvice_due_date=$service['crm_investigation_end_date']; 
			      }else{
			        $taxadvice_due_date='-';  
			      }
			      array_push($servicedue_date_array,$taxadvice_due_date);
			    } 
			 // }
			 // if($status[$i]=='taxinvest'){
			    if($taxinvest=='On'){ 
			    array_push($service_array,'Tax Investigation');
			    $taxinvest_due_date=$service['crm_investigation_end_date']; 
			      if($taxinvest_due_date!=''){
			        $taxinvest_due_date=$service['crm_investigation_end_date']; 
			      }else{
			        $taxinvest_due_date='-'; 
			      }
			      array_push($servicedue_date_array,$taxinvest_due_date);
			    }
			}
return 	array($service_array, $servicedue_date_array);
	}
	public function decode_temp_send($id,$sub,$body)
	{
		$tot_rec=$this->db->query("select cc.client_id,cc.main_email,c.id,c.crm_company_name,c.crm_mobile_number,c.crm_company_url,u.crm_name,u.username,u.password from client_contacts as cc left join client c on c.user_id=cc.client_id left join user u on u.id=cc.client_id where cc.id=$id")->result_array();
		$tot_rec=$tot_rec[0];	
		$service=$this->email_decode($tot_rec['id']);
		$sender_details=$this->db->query('select company_name,crm_name from admin_setting where user_id='.$_SESSION['id'].'')->row_array();
          $sender_company=$sender_details['company_name'];
          $sender_name=$sender_details['crm_name'];
          $a1  =   array(
          	'::Client Name::'=>$tot_rec['crm_name'],
            '::Client Company::'=>$tot_rec['crm_company_name'],
            ':: ClientPhoneno::'=> $tot_rec['crm_mobile_number'],
            '::ClientWebsite::'=>$tot_rec['crm_company_url'],
            '::Client Username::'=>$tot_rec['username'],
                   
            ':: Sender Name::'=>$sender_company,
            ':: Sender Company::'=>$sender_name,       
            ':: Date ::'=>date("Y-m-d h:i:s"),
            ':: Service Name::'=>implode(",",$service[0]),
            ':: service Due Date::'=>implode(",",$service[1])
            );
          $sub=strtr($sub,$a1);
          $body=strtr($body,$a1);
         // echo $sub."<br>".$body; '::Client Password::'=> $tot_rec['password'], 

           $mail_data['body']  =   strtr($body,$a1); 
           $subject  =   strtr($sub,$a1);
        //  $random_string=$this->generateRandomString();
          $body = $this->load->view('email/email-sub-body', $mail_data, TRUE);
      /* 18.09.2018 */
    	 // $body = $this->load->view('email/welcome-email-mail.php', $mail_data, TRUE);
      $this->load->library('email');
      $this->email->set_mailtype('html');
      $this->email->from('info@remindoo.org');
      $this->email->to($tot_rec["main_email"]);
   		 //  $this->email->to('shanmugapriya.techleaf@gmail.com');
      $this->email->subject(preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($subject))));
      $this->email->message($body);
      $send = $this->email->send();
	return 1;
	}



}









?>