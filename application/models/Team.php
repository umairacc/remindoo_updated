<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct(){
        parent::__construct();
        $this->load->model(array('Common_mdl','Security_model'));
    }
   /* public function insert_group()
    { 
      if( isset( $_POST['group_name'] ) )
      {
        $id = $_POST['group_leader_id'];

        $parent_g = $this->db->query("select * from firm_groups where firm_id=".$_SESSION['firm_id']." and parent_id=0")->row_array();
        
        $this->db->query("insert into firm_groups values ('',".$_SESSION['firm_id'].",'".$_POST['group_name']."','".$_POST['description']."',".$parent_g['id'].")");
        
        $GID = $this->db->insert_id();

        $this->db->insert('firm_group_members',['user_id'=>$id,'parent_id'=>"G:".$GID."","group_id"=>$GID]);

        echo $this->db->affected_rows();
      }
    }

    public function Get_Edit_GroupDetails()
    {
      if( isset($_POST['group_id']) )
      {
        $id = explode(':', $_POST['group_id']);
        $g_details = $this->db->query("select * from firm_groups where id =".$id[1])->row_array();

        $lead_id = $this->db->query("select user_id from firm_group_members where parent_id='G:".$id[1]."'")->row_array();

        $g_leaders = $this->db->query("select id,crm_name as name from user where firm_id =".$_SESSION['firm_id']." and user_type='FU'")->result_array();

        $Set_select_lead =  array_keys( array_column( $g_leaders ,'id' ) , $lead_id['user_id'] );
       
        if(isset( $Set_select_lead[0] ) &&   isset( $g_leaders[ $Set_select_lead[0] ] ) )
        {
          $g_leaders[ $Set_select_lead[0] ]['selected']=1;
        }

        array_unshift($g_leaders,['id'=>'','name'=>'Select Any lead']);
        
        echo json_encode(['details'=>$g_details,'leaders'=>$g_leaders]);
      }
    }
    public function Save_Edited_GroupDetails()
    {
      if( isset( $_POST['group_id'] ) )
      {

        $data['group_name'] = $_POST['group_name'];
        $data['description'] = $_POST['description'];
        $this->db->update('firm_groups',$data,"id=".$_POST['group_id']);

        $data['user_id'] = $_POST['group_leader_id'];
        $this->db->update('firm_group_members',$data,"parent_id='G:".$_POST['group_id']."'");

        echo $this->db->affected_rows();
      }

    } 
    */

    public function Firm_Group_Members( $id = 0 )
    {
      $CHILDS = [];

      $DB_DATA = $this->db->query("select * from organisation_tree where firm_id = ".$_SESSION['firm_id']." and  parent_id=".$id." ")->result_array();  
      
      foreach( $DB_DATA as $val )
      {
        $CHILD_DATA = [];

        $user_info = $this->db->query("select crm_name from user where id=".$val['user_id'])->row_array();
        $CHILD_DATA['id'] = $val['id'];
        $CHILD_DATA['name'] = $val['title'];
        $CHILD_DATA['title'] = $user_info['crm_name'];
        if($id==0)
        {
          $CHILD_DATA['className'] = 'ROOT_NODE';
        }
        $hasChild = $this->Firm_Group_Members( $val['id'] );

        if( !empty( $hasChild ) )
        {
          $CHILD_DATA['children'] = $hasChild;
        }
        $CHILDS[] = $CHILD_DATA;
      }
      return $CHILDS;
    }


    public function organisation_tree()
    {
      $this->Security_model->chk_login();
      
      $Chart_DataSource =  $this->Firm_Group_Members();

      $data['Chart_DataSource_json'] = json_encode( $Chart_DataSource[0] );

      $data['assignee'] = $this->db->query("select u.id,u.crm_name,r.role as role_name,u.role from user as u left join roles_section r on r.firm_id = u.firm_id and r.id = u.role where u.firm_id=".$_SESSION['firm_id']." and u.user_type='FU'")->result_array();

      $this->load->view('team/orgChart', $data );
    }

    public function org_chart_Add_newAssignee()
    {
      $this->Security_model->chk_login();

      if( isset( $_POST['parent_id'] )  && isset( $_POST['assigne'] ) )
      {
        $this->db->insert('organisation_tree',['firm_id'=>$_SESSION['firm_id'],'user_id'=>$_POST['assigne'],'parent_id'=>$_POST['parent_id'],'title'=>$_POST['title']]);

        $data['result'] = $this->db->affected_rows();
        $Chart_DataSource =  $this->Firm_Group_Members();
        $data['DataSource_json'] = $Chart_DataSource[0];

        echo json_encode( $data );

      }
    }
    public function orgChart_ChangeParent()
    {
      $this->Security_model->chk_login();

      if( isset($_POST['dropN']) && isset($_POST['dragN']) )
      {

        $this->db->update('organisation_tree',['parent_id'=>$_POST['dropN']],"id=".$_POST['dragN']);
        echo $this->db->affected_rows(); 
          
      }
          $activity_log['user_id'] = $_SESSION['userId'];
          $activity_log['module'] = "Organisation Chart";
          $activity_log['log'] = "change Position of organisation chart";
          $activity_log['CreatedTime'] =time();

        $this->db->insert('activity_log', $activity_log);    
    } 
    public function Get_Edit_orgChart()
    { 
      $this->Security_model->chk_login();

      if( isset($_POST['id']) )
      {

        $org = $this->db->query("select * from organisation_tree where id =".$_POST['id'])->row_array();

        $Members = $this->db->query("select u.id as id ,CONCAT(u.crm_name,'-',r.role ) as name  from user as u left join roles_section r on r.firm_id = u.firm_id and r.id = u.role where u.firm_id=".$_SESSION['firm_id']." and u.user_type='FU'")->result_array();

        array_unshift($Members,['id'=>'','name'=>'Select Any Member']);
        
        echo json_encode(['details'=>$org,'assignee'=>$Members]);
      }
    }
    public function org_chart_edit_Assignee()
    {
      if( isset($_POST['node_id']) && isset($_POST['title']) )
      {
        $data= ['title'=>$_POST['title'],'user_id'=>$_POST['assigne']];
        $this->db->update('organisation_tree',$data,"id=".$_POST['node_id']);
        $data['result'] = $this->db->affected_rows();
        $Chart_DataSource =  $this->Firm_Group_Members();
        $data['DataSource_json'] = $Chart_DataSource[0];
        echo json_encode( $data );

      }
    }
    /*public function Firm_Groups( $GID = 0 )
    { 
      $GROUPS=[];
      
      $LG = $this->db->query("SELECT * FROM firm_groups where firm_id=".$_SESSION['firm_id']." and parent_id=".$GID." ")->result_array();
      
      foreach($LG as $val)
      {
        $GROUP_DATA = [];
        
        $lead_id = $this->db->query("select user_id,id from firm_group_members where parent_id='G:".$val['id']."'")->row_array();

         $lead_info = $this->db->query("select u.crm_name,r.role as role_name,u.role from user as u left join roles_section r on r.firm_id = u.firm_id and r.id = u.role where u.id=".$lead_id['user_id'])->row_array();

        $GROUP_DATA['id'] = 'G:'.$val['id'];
        $GROUP_DATA['name'] = $val['group_name'];
        $GROUP_DATA['title'] = $lead_info['crm_name'].'-'.$lead_info['role_name'];
        $GROUP_DATA['className'] = 'GROUP';
        if( $GID == 0 )
        {
          $GROUP_DATA['className'] .= ' ROOT_NODE';
        }

        $Child_GData = $this->Firm_Groups( $val['id'] );
        $Child_GMData = $this->Firm_Group_Members( $lead_id['id'] );
        
        if( !empty( $Child_GData ) || !empty( $Child_GMData ) )
        {
          $GROUP_DATA['children'] = array_merge( $Child_GData , $Child_GMData );
        }
        
        $GROUPS[] = $GROUP_DATA;
      }   
      return $GROUPS;
    }*/
     //  $T means parent type G:group_id first child of the group, GM:g_m_id under the group_member in chart
    /*public function Firm_Group_Members( $id )
    {
      $CHILDS = [];

      $DB_DATA = $this->db->query("select * from firm_group_members where  parent_id='GM:".$id."' ")->result_array();  
      
      foreach( $DB_DATA as $val )
      {
        $CHILD_DATA = [];

        $user_info = $this->db->query("select u.crm_name,r.role as role_name,u.role from user as u left join roles_section r on r.firm_id = u.firm_id and r.id = u.role where u.id=".$val['user_id'])->row_array();
        $CHILD_DATA['id'] = 'GM:'.$val['id'];
        $CHILD_DATA['name'] = $user_info['role_name'];
        $CHILD_DATA['title'] = $user_info['crm_name'];
        $CHILD_DATA['className'] = $user_info['role'].'_ROLE';
        
        $hasChild = $this->Firm_Group_Members( $val['id'] );

        if( !empty( $hasChild ) )
        {
          $CHILD_DATA['children'] = $hasChild;
        }
        $CHILDS[] = $CHILD_DATA;
      }
      return $CHILDS;
    }

    public  function orgChart_ChangeChilds_GroupID($M_Id ,$GID )
    {
      $childs = $this->db->query("select id from firm_group_members where parent_id='GM:".$M_Id."'")->result_array();
      foreach ($childs as $key => $value)
      {
        $this->db->update('firm_group_members',['group_id'=>$GID],"id=".$value['id']);
        $this->orgChart_ChangeChilds_GroupID($value['id'],$GID);
      }  
    }

    public  function orgChart_ChangeParent()
    {
      $this->Security_model->chk_login();

      if( isset($_POST['dropN']) && isset($_POST['dragN']) )
      {
        $dragN = explode( ':', $_POST['dragN'] );
        $dropN= explode( ':', $_POST['dropN'] );

        if( $dragN[0] == 'G' )
        {
          $this->db->update('firm_groups',['parent_id'=>$dropN[1]],"id=".$dragN[1]);
          echo $this->db->affected_rows(); 
        }
        else
        { 
          
          //find droped group id         
          if( $dropN[0]=="G")
          {
            $group_id = $dropN[1];
            $lead_id = $this->db->query("select id from firm_group_members where parent_id='G:".$dropN[1]."'")->row_array();
            $_POST['dropN']='G:'.$lead_id['id'];
          }
          else
          {
            $dropN_Data = $this->db->query("select group_id from firm_group_members where id=".$dropN[1])->row_array();
            $group_id = $dropN_Data['group_id'];
          }

          //check drag node if group_id deffer with droped node
          $check_IfParent_Change = $this->db->query("select id from firm_group_members where id=".$dragN[1]." and group_id!=".$group_id)->row_array();

          if( !empty( $check_IfParent_Change ) )
          {
            $this->orgChart_ChangeChilds_GroupID( $dragN[1] , $group_id );
          }

          //echo "GROUPS--".$group_id;
          $this->db->update('firm_group_members',['parent_id'=>$_POST['dropN']],"id=".$_POST['dragN']);
          echo $this->db->affected_rows(); 
          
        }
          $activity_log['user_id'] = $_SESSION['userId'];
          $activity_log['module'] = "Organisation Chart";
          $activity_log['log'] = "change Position of organisation chart";
          $activity_log['CreatedTime'] =time();

        $this->db->insert('activity_log', $activity_log);  
      }
    }
     public  function orgChart_Get_assignee()
    {
      $this->Security_model->chk_login();
      $con=' ';
      if( ! isset($_POST['is_group']) && isset($_POST['parent_roles']) )
      {
        $id = explode(':',$_POST['id']);

        $assign_to = $this->db->query("select user_id from firm_group_members where id=".$id[1])->row_array(); 

        $roles = implode(',', $_POST['parent_roles'] );
        $con = " and role NOT IN (".$roles.") and id!=".$assign_to['user_id'];
      }

      $data = $this->db->query("select id,crm_name as name from user where firm_id =".$_SESSION['firm_id']." and user_type='FU' ".$con)->result_array();
            array_unshift($data, ['id'=>'','name'=>'select Any Members','selected'=>1]);
      echo json_encode( $data );
    }    
    public function org_chart_Add_newAssignee()
    {
      if( isset( $_POST['parent_id'] )  && isset( $_POST['assigne'] ) )
      {
        $this->db->insert('organisation_tree',['firm_id'=>$_SESSION['firm_id'],'user_id'=>$_POST['assigne'],'parent_id'=>$_POST['parent_id'],'title'=>$_POST['title']]);
        echo $this->db->affected_rows();
      }
    }*/

    public function index()
    {
        $this->Security_model->chk_login();
        // $data['team']=$this->Common_mdl->getallrecords('team');
          $data['team']=$this->Common_mdl->GetAllWithWhere('team','create_by',$_SESSION['id']);
        $this->load->view('team/team',$data);
    }
    public function archive_team($id)
    {
      $this->db->query("update team set status=1 where id=$id");
       echo $this->db->affected_rows();
    } 

    public function add_team()
    {
        $this->Security_model->chk_login();
        $this->load->view('team/add_team');
    }

    public function add_new()
    {
        $this->Security_model->chk_login();
        $data['team'] = $this->input->post('new_team');
       // $data['role'] = $_POST['roles'];//11-07-2018 
        $data['createdTime'] = time();
        $data['create_by']=$_SESSION['id'];
        $res=$this->Common_mdl->insert('team',$data);
        
        //redirect('department/add_department');
        if($_POST['for_create']=='create')
        {
          $this->session->set_flashdata('success', 'Team Added Successfully');
          redirect('team');
        }
        else if($_POST['for_create']=='assign')
        {
          $this->session->set_flashdata('from_team', $res);
          redirect('team/assign_staff/');
        }
        else
        {
          $this->session->set_flashdata('success', 'Team Added Successfully');
          redirect('team');
        }
        

    }

    public function delete($id)
    {
        $this->Security_model->chk_login();
        $this->Common_mdl->delete('team','id',$id);
        /** delete permision page **/
        $this->Common_mdl->delete('team_access_permission','team_id',$id);
        /** end of permission page **/
        $this->session->set_flashdata('success', 'Team Deleted Successfully');
        redirect('team');
    }

    public function teamDelete()
    {
      if(isset($_POST['data_ids']))
      {
          $id = trim($_POST['data_ids']);
          $sql = $this->db->query("DELETE FROM team WHERE id in ($id)");
          echo $id;
      }  
    }
      public function update_team($id)
    {
        $this->Security_model->chk_login();
        $data['team']=$this->Common_mdl->select_record('team','id',$id);
        $data['id'] = $id;
        $this->load->view('team/update_team',$data);
    }

    public function update($id)
    {
        $this->Security_model->chk_login();
        $data['team'] = $this->input->post('new_team');
     //   $data['role'] = $_POST['roles'];
        $this->Common_mdl->update('team',$data,'id',$id);
        //$this->session->set_flashdata('success', 'Team Updated Successfully');
        //redirect('department/update_department/'.$id);
       // redirect('team');
        if($_POST['for_create']=='create')
        {
          $this->session->set_flashdata('success', 'Team Updated Successfully');
          redirect('team');
        }
        else if($_POST['for_create']=='assign')
        {
          $this->session->set_flashdata('from_team', $id);
          redirect('team/assign_staff/');
        }
        else if($_POST['for_create']=='assignupdate')
        {
          $res=$_POST['add_assign_update_id'];
          //$this->session->set_flashdata('from_team', $res);

          redirect('team/update_assignedTeam/'.$res);
        }
        else
        {
          $this->session->set_flashdata('success', 'Team Updated Successfully');
          redirect('team');
        }

    }
    public function assign_staff()
    {
        $this->Security_model->chk_login();
        $data['team_assign_staff']= $this->Common_mdl->getallrecords('team_assign_staff');
/*
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `team_id` SEPARATOR  ',' ) AS  `team` , GROUP_CONCAT(  `staff_id` SEPARATOR  ',' ) AS  `staff` FROM team_assign_staff")->row_array();

         $team =  explode(',', $sql['team']);
         $staff = explode(',',$sql['staff']);*/
        
        // $data['team'] = $this->db->query( "SELECT * FROM team WHERE id NOT IN ( '" . implode( "','", $team ) . "' )")->result_array();
        
        // $data['staff'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and (role='6' or role='5')  ")->result_array();
         $from_team=$this->session->flashdata('from_team');
      //   echo $this->session->flashdata('from_team')."zzzz";
        if($from_team!='')
        {
          $data['from_team']=$from_team;
        }

        $data['team'] = $this->db->query( "SELECT * FROM team WHERE  create_by=".$_SESSION['id']." ")->result_array();
        
        $data['staff'] = $this->db->query( "SELECT * FROM user WHERE (role='6' or role='5' or role='3') and firm_admin_id=".$_SESSION['id']."  ")->result_array(); // for old

         // $data['staff'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and (role='6') and firm_admin_id=".$_SESSION['id']."  ")->result_array();
       
        $this->load->view('team/assign_staff',$data);
    }

    public function assign_to_staff()
    { 
        $this->Security_model->chk_login();
        $data['team_id'] = $this->input->post('team');
        $staff = $this->input->post('staff');
        if(!empty($staff))
        {
          $staffs = implode(',', $staff);
        }
        $data['staff_id'] = $staffs;
        $data['createdTime'] = time();
        // create by
        $data['create_by']=$_SESSION['id'];
        $this->Common_mdl->insert('team_assign_staff',$data);
        $this->session->set_flashdata('success', 'Team Assigned Members Successfully');
        redirect('team/assigned_team');
    }

    public function assigned_team()
    { 
        $this->Security_model->chk_login();
      //  $data['assigned_team']=$this->Common_mdl->getallrecords('team_assign_staff');
         $data['assigned_team'] = $this->Common_mdl->GetAllWithWhere('team_assign_staff','create_by',$_SESSION['id']);
        $this->load->view('team/assigned_team',$data);
    }

    public function AssignTeam_Delete()
    {
      if(isset($_POST['data_ids']))
      {
          $id = trim($_POST['data_ids']);
          $sql = $this->db->query("DELETE FROM team_assign_staff WHERE id in ($id)");
          echo $id;
      }  
    }

    public function delete_assignedTeam($id)
    {
        $this->Security_model->chk_login();
        $this->Common_mdl->delete('team_assign_staff','id',$id);
        $this->session->set_flashdata('success', 'Team Deleted Successfully');
        redirect('team/assigned_team');
    }

    public function update_assignedTeam($id)
    {
        $data['teams']=$this->Common_mdl->select_record('team_assign_staff','id',$id);
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `team_id` SEPARATOR  ',' ) AS  `team` , GROUP_CONCAT(  `staff_id` SEPARATOR  ',' ) AS  `staff` FROM team_assign_staff where id NOT IN( '" .$id."' )")->row_array();
  
        $team =  explode(',', $sql['team']);
        $staff = explode(',',$sql['staff']);
        
       // $data['team'] = $this->db->query( "SELECT * FROM team WHERE id NOT IN ( '" . implode( "','", $team ) . "' ) and create_by=".$_SESSION['id']." ")->result_array();

          $data['team'] = $this->db->query( "SELECT * FROM team WHERE  create_by=".$_SESSION['id']." ")->result_array();
      
         $data['staff'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and (role='6' or role='5' or role='3') and firm_admin_id=".$_SESSION['id']." ")->result_array(); // for old
        //  $data['staff'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and (role='6') and firm_admin_id=".$_SESSION['id']." ")->result_array(); // only dispaly staff
       //print_r($data['team']);
       //print_r($data['staff']);
       //die;
       $this->load->view('team/update_assigned_team',$data);
    }

    public function update_assigned_team($id)
    {
      /*echo '<pre>';
      echo $id;die;
      print_r($_POST);die;*/
        $this->Security_model->chk_login();
        $data['team_id'] = $this->input->post('team');
        $staff = $this->input->post('staff');
       // print_r($_POST);
          if(!empty($staff))
        {
          $staffs = implode(',', $staff);
        }
        $data['staff_id'] = $staffs;
        /** 10-08-2018 **/
        $db_teams=$this->Common_mdl->select_record('team_assign_staff','team_id',$this->input->post('team'));
        if(count($db_teams) > 0)
        {
        //  echo "update";
          $this->Common_mdl->update('team_assign_staff',$data,'id',$id);
        }
        else
        {
       //   echo "insertr";
          $data['CreatedTime']=time();
          $data['create_by']=$_SESSION['id'];
         $this->Common_mdl->insert('team_assign_staff',$data);
        }
     
        /** end of 10-08-2018 **/
          
        
       // $this->Common_mdl->update('team_assign_staff',$data,'id',$id);

        $this->session->set_flashdata('success', 'Team Assigned Members Successfully');
        redirect('team/assigned_team');
    }

    public function team_management($team_id=false)
    {
        $this->Security_model->chk_login();
        $data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user','role','1');
        $data['teams']=$this->Common_mdl->select_record('team_assign_staff','id',$team_id);
        $data['staff_list']=$this->Common_mdl->GetAllWithWhere('staff_form','status','1');
        $data['department_list']=$this->Common_mdl->getallrecords('department');
        $data['new_department'] = $this->db->query( "SELECT * FROM department_permission WHERE new_dept!='' and create_by=".$_SESSION['id']." ")->result_array();
       $this->load->view('team/team_and_management',$data); 
    }

    public function team_staff_delete($team_id=false)
    {
     
        $this->Security_model->chk_login();
        
        $teams=$this->Common_mdl->select_record('team_assign_staff','id',$_POST['team_id']);
        $exp_team=explode(',', $teams['staff_id']);
        $del_array=array_diff($exp_team,array($_POST['staff_id']));
        $imp_team=implode(',',$del_array);
         
        $data['staff_id'] = $imp_team;
        $this->Common_mdl->update('team_assign_staff',$data,'id',$_POST['team_id']);
       
       redirect('team/assigned_team'); 
    }
    public function delete_department($dept_id=false)
    {
     
        $this->Security_model->chk_login();
        
        
       //$this->Common_mdl->delete('department','id',$dept_id);
        
        $this->Common_mdl->delete('department_permission','id',$dept_id);
       redirect('team/team_management');
       //redirect('department'); 
    }

    public function get_teamwise_users(){
      $team=$_POST['team'];
       //$teams=$this->Common_mdl->select_record('team','id',$_POST['team']);
      $teams=$this->Common_mdl->select_record('team_assign_staff','team_id',$team);
    
       $sql = $this->db->query("SELECT GROUP_CONCAT(  `team_id` SEPARATOR  ',' ) AS  `team` , GROUP_CONCAT(  `staff_id` SEPARATOR  ',' ) AS  `staff` FROM team_assign_staff where id NOT IN( '" .$teams['id']."' )")->row_array();
  
        $team =  explode(',', $sql['team']);
        $staff = explode(',',$sql['staff']);
        
     //   $data['team'] = $this->db->query( "SELECT * FROM team WHERE id NOT IN ( '" . implode( "','", $team ) . "' ) and create_by=".$_SESSION['id']." ")->result_array();
      
       $user_detailes = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and (role='6' or role='5' or role='3') and firm_admin_id=".$_SESSION['id']." ")->result_array(); 
        $staffid = $teams['staff_id'];
        $staff_exp = explode(',',$staffid); 
      $res='';
      $res.='<label>Select Staff</label><div class="dropdown-sin-2"><select name="staff[]" id="staff" multiple placeholder="Select">';
      foreach ($user_detailes as $key => $value) {
        $sel='';
        if(isset($value['id']) && in_array( $value['id'] ,$staff_exp )) { $sel="selected"; }
       $res.='<option value='.$value['id'].' '.$sel.' >'.$value['crm_name'].'</option>';
      }
      $res.='</select></div>';
      //echo $res;
      $data['staff']=$staff;
      $data['teams']=$teams;
      $data['user_detailes']=$user_detailes;
      $data['staffid']=$staffid;
       $this->load->view('team/team_assign_staff_onchange',$data);

    }
/** 21-07-2018 for team name check **/
  public function check_teamname($id=false)
    {
        $teamname = $_POST['new_team'];
        if(isset($id) && $id!='')
        {
          //$sql = $this->Common_mdl->GetAllWithWhere('user','userName',$username);
            $sql=$this->db->query("select * from team where team='".$teamname."' and id!=".$id." and create_by=".$_SESSION['id']."")->result_array();
        }
        else
        {
           $sql=$this->db->query("select * from team where team='".$teamname."' and create_by=".$_SESSION['id']." ")->result_array();
        }
        //$sql = $this->Common_mdl->GetAllWithWhere('user','userName',$username);
        if(!empty($sql))
        {
            echo 'false';
        }else{
            echo 'true';
        }
    }

/** end of check **/
/** team permission page **/

  public function after_team_permission($id){
    $this->Security_model->chk_login();
      $team=$this->Common_mdl->select_record('team','id',$id);
      $data['team']=$team;
      $this->load->view('team/team_access_permission',$data);
  }
  public function insert_team_access_permission($id){
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";

    $for_bulkview=$_POST['for_bulkview'];
    $for_bulkcreates=$_POST['for_bulkcreates'];
    $for_bulkedit=$_POST['for_bulkedit'];
    $for_bulkdelete=$_POST['for_bulkdelete'];

    $team_id=$_POST['team_id'];
    $data['team_id']=$team_id;
    $sql=$this->db->query('select * from team_access_permission where team_id='.$team_id.' ')->result_array();
    if(count($sql)>0)
    {
 $this->Common_mdl->delete('team_access_permission','team_id',$team_id);
    }
      
    foreach ($for_bulkview as $for_key => $for_value) {

      if($for_value!='')
      {
      $un_check=explode('//', $for_value);
      if(count($un_check)>1){
        $data['view']=0;
      }
      else
      {
        $data['view']=1;
      }
      $it_view=explode('-', $for_value);
      $main='';
      $sub='';
      if($it_view[0]!='')
      {
        $main=$it_view[0];
      }
      if($it_view[1]!='')
      {
        $sub=$it_view[1];
      }
      
      $data['main']=$main;
      if(count($it_view)>2)
      {
         $data['sub']=$sub;
      }
      else
      {
        $data['sub']='';
      }
     
      //$data['view']=1;
     }     
     if($for_bulkcreates[$for_key]!='')
      {
      $un_check=explode('//', $for_bulkcreates[$for_key]);
      if(count($un_check)>1){
        $data['create']=0;
      }
      else
      {
        $data['create']=1;
      }
      $it_view=explode('-', $for_bulkcreates[$for_key]);
      $main='';
      $sub='';
      if($it_view[0]!='')
      {
        $main=$it_view[0];
      }
      if($it_view[1]!='')
      {
        $sub=$it_view[1];
      }
      
      $data['main']=$main;
   //   $data['sub']=$sub;
      if(count($it_view)>2)
      {
         $data['sub']=$sub;
      }
      else
      {
        $data['sub']='';
      }
    //  $data['create']=1;
     }
      if($for_bulkedit[$for_key]!='')
      {
      $un_check=explode('//', $for_bulkedit[$for_key]);
      if(count($un_check)>1){
        $data['edit']=0;
      }
      else
      {
        $data['edit']=1;
      }
      $it_view=explode('-', $for_bulkedit[$for_key]);
      $main='';
      $sub='';
      if($it_view[0]!='')
      {
        $main=$it_view[0];
      }
      if($it_view[1]!='')
      {
        $sub=$it_view[1];
      }
      
      $data['main']=$main;
      //$data['sub']=$sub;
      if(count($it_view)>2)
      {
         $data['sub']=$sub;
      }
      else
      {
        $data['sub']='';
      }
    //  $data['edit']=1;
     }
      if($for_bulkdelete[$for_key]!='')
      {
      $un_check=explode('//', $for_bulkdelete[$for_key]);
      if(count($un_check)>1){
        $data['delete']=0;
      }
      else
      {
        $data['delete']=1;
      }
      $it_view=explode('-', $for_bulkdelete[$for_key]);
      $main='';
      $sub='';
      if($it_view[0]!='')
      {
        $main=$it_view[0];
      }
      if($it_view[1]!='')
      {
        $sub=$it_view[1];
      }
      
      $data['main']=$main;
      //$data['sub']=$sub;
      if(count($it_view)>2)
      {
         $data['sub']=$sub;
      }
      else
      {
        $data['sub']='';
      }
     // $data['delete']=1;
     }
// echo "<pre>";
// print_r($data);

$res=$this->Common_mdl->insert('team_access_permission',$data);
 
//echo $res;
      }
    $this->session->set_flashdata('success', 'Successfully Added permission');
       redirect('team'); 
  }


  public function bulkArchive(){  
    $id_value=explode(',',$_POST['data_ids']);
    for($i=0;$i<count($id_value);$i++){
      $data=array('status'=>1);
        $this->Common_mdl->update('team',$data,'id',$id_value[$i]);
    }
    echo 1;
  }


   public function bulkUnArchive(){  
    $id_value=explode(',',$_POST['data_ids']);
    for($i=0;$i<count($id_value);$i++){
      $data=array('status'=>0);
        $this->Common_mdl->update('team',$data,'id',$id_value[$i]);
    }
    echo 1;
  }

  public function team(){
    $this->load->view('team/team_page');
  }


/** end of team permission page **/
 
   
}
?>
