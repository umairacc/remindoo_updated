<?php
//Session validation with Admin for DSM-Live
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Security_model extends CI_Model
{

    public function chk_login()
	{
        if($this->session->userdata('admin_id'))
        {
            return true;
        }
        else
        {
            redirect(base_url('login'));
			//echo '<script type="text/javascript">window.top.location.href="'.base_url().'login/'.'";</script>';
            return false;
        }

	}

    public function check_login()
    {
        if(!empty($this->session->userdata('client_uid')))
        {
            return true;
        }
        else
        {
            redirect(base_url('login'));            
        }
    }
    
  
}
/* End of file security_model.php */
/* Location: ./application/models/security_model.php */