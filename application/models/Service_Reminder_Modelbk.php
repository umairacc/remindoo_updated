	<?php
class Service_Reminder_Model extends CI_Model
{

	public function Get_Sending_Data($firm_id)
	{	
		$data = $this->db->query("SELECT * FROM service_reminder_cron_data WHERE `status`=0 AND `date` ='".date('d-m-Y')."' AND firm_id=".$firm_id)->result_array();
		$RETURN = [];
		foreach ($data as $key => $value)
		{
			$query = "SELECT cc.main_email,c.id,u.status 
						FROM client as c 
							INNER JOIN client_contacts as cc 
								ON cc.client_id=c.user_id and cc.make_primary =1 
							INNER JOIN user as u 
								ON u.id=c.user_id
							WHERE c.id=".$value['client_id'];

			$client_data = $this->db->query($query)->row_array();

		

			if( $value['template_type']!=0 )
			{
				$Template = $this->db->query("SELECT subject,body,headline_color FROM custom_service_reminder WHERE id=".$value['template_id'])->row_array();
			}
			else
			{
				$Template = $this->db->query("SELECT subject,message,headline_color as body FROM reminder_setting WHERE id=".$value['template_id'])->row_array();
			}
			
			print_r($Template);die;
			$Color_code = 'color:#111010';

			if( $Template['headline_color'] )
			{
				$Color_code = "color:".$Template['headline_color'];
			}
			
			$heading = "<b style='".$Color_code."'>Service Reminder</b></br>";

			if(!empty( $client_data['main_email'] ) )
			{				
				$Content['subject'] = $Template['subject'];
				$Content['body'] = $heading.$Template['body'];
				$DATA = Decode_ClientTable_Datas( $value['client_id'] , $Content );
				$DATA['to'] = $client_data['main_email'];
				$DATA['client_id'] = $value['client_id'];
				$DATA['client_status'] = $client_data['status'];
				$DATA['cron_id'] = $value['id'];
				$RETURN[] = $DATA;
			}		
		}
			return $RETURN;
	}	
	public Function Get_Service_ReminderFields()
	{
		$feilds = "
		crm_confirmation_email_remainder,crm_confirmation_add_custom_reminder,crm_confirmation_statement_date,'Annually',
		crm_accounts_next_reminder_date,crm_accounts_custom_reminder,crm_hmrc_yearend,'Annually',
		crm_company_next_reminder_date,crm_company_custom_reminder,crm_accounts_due_date_hmrc,'Annually',
		crm_personal_next_reminder_date,crm_personal_custom_reminder,crm_personal_tax_return_date,'Annually',
		crm_vat_next_reminder_date,crm_vat_add_custom_reminder,crm_vat_quarters,crm_vat_quarters,
		crm_payroll_next_reminder_date,crm_payroll_add_custom_reminder,crm_payroll_run_date,crm_payroll_run,
		crm_pension_next_reminder_date,crm_pension_add_custom_reminder,crm_pension_subm_due_date,'Annually',
		crm_cis_next_reminder_date,crm_cis_add_custom_reminder,crm_cis_contractor_start_date,crm_cis_frequency,
		crm_cissub_next_reminder_date,crm_cissub_add_custom_reminder,crm_cis_subcontractor_start_date,'Annually',
		crm_bookkeep_next_reminder_date,crm_bookkeep_add_custom_reminder,crm_next_booking_date,crm_bookkeeping,
		crm_p11d_next_reminder_date,crm_p11d_add_custom_reminder,crm_next_p11d_return_due,'Annually',
		crm_manage_next_reminder_date,crm_manage_add_custom_reminder,crm_next_manage_acc_date,crm_manage_acc_fre,
		crm_insurance_next_reminder_date,crm_insurance_add_custom_reminder,crm_insurance_renew_date,'Annually',
		crm_registered_next_reminder_date,crm_registered_add_custom_reminder,crm_registered_renew_date,'Annually',
		crm_investigation_next_reminder_date,crm_investigation_add_custom_reminder,crm_investigation_end_date,'Annually'";
		
		return $feilds;
	}
	public function Create_ServiceReminder( $client_id )
	{	
		$Services = range(1, 15);
		$Fields = $this->Get_Service_ReminderFields();
		$query = "SELECT ".$Fields." FROM client where id =".$client_id;		
		$C_Services = $this->db->query( $query )->row_array();
		$C_Services = array_combine( $Services, array_chunk( $C_Services, 4 ) );
		
		foreach ($C_Services as $key => $value)
		{
			if($key==5)//vat
			{
				$this->QuartersBase_ServiceReminder($client_id ,$key,$value);
				continue;
			}
			$this->Insert_ServiceReminder_CronData( $client_id ,$key,$value);
		}
	}
	public function Update_ServiceReminder( $client_id , array $OLD_SERVICE_DATA ,array $NEW_SERVICE_DATA )
	{
		$OLD_SERVICE_DATA = array_combine( range(1,15), array_chunk($OLD_SERVICE_DATA,3) );	
		$NEW_SERVICE_DATA = array_combine( range(1,15), array_chunk($NEW_SERVICE_DATA,3) );
		
		foreach ($NEW_SERVICE_DATA as $key => $New_Value)
		{	
			$Old_Value = $OLD_SERVICE_DATA[ $key ];

			if( $New_Value[0]!= $Old_Value[0] || $New_Value[1]!= $Old_Value[1] || $New_Value[2]!= $Old_Value[2] )	
			{	
				$this->db->query("DELETE FROM service_reminder_cron_data where client_id = ".$client_id." and service_id =".$key);
				if($key==5)//vat
				{
					$this->QuartersBase_ServiceReminder($client_id ,$key,$New_Value);
					continue;
				}
				$this->Insert_ServiceReminder_CronData( $client_id ,$key,$New_Value);		
			}
		}
	}

	public function Insert_ServiceReminder_CronData( $client_id , $service_id ,array $service_Data )
	{
		
			if( $service_Data[0] != '' && $service_Data[2]!='')
			{	


				$query ="SELECT * FROM `reminder_setting` where custom_reminder=0 and firm_id IN (0,".$_SESSION['firm_id'].") and service_id=".$service_id." and id NOT IN (SELECT super_admin_owned from reminder_setting WHERE firm_id=".$_SESSION['firm_id']." )";
				
				$Reminders = $this->db->query( $query )->result_array();

				

				foreach($Reminders as $value)
				{
					$Date = $this->Calculate_Due( $service_Data[2], $value['due'] , $value['days'] );
					
					$data =[
								'firm_id' => $_SESSION['firm_id'],
								'client_id' => $client_id,
								'service_id' => $service_id,
								'template_id' => $value['id'],
								'date' => $Date
							];
					$this->db->insert('service_reminder_cron_data',$data);			 

				}

			}
			else if( $service_Data[1] != '' && $service_Data[2]!='' )
			{
				

				$Reminder = $this->db->query("SELECT * FROM custom_service_reminder where client_id=".$client_id." and service_id =".$service_id)->row_array();

				$Date = $this->Calculate_Due( $service_Data[2], $Reminder['due_type'] , $Reminder['due_days'] );

				$data =[
							'firm_id' => $_SESSION['firm_id'],
							'client_id' => $client_id,
							'service_id' => $service_id,
							'template_type'=>1,
							'template_id' => $Reminder['id'],
							'date' => $Date
						];
				$this->db->insert('service_reminder_cron_data',$data);	
			}
		
	}

	//public function Create_ServiceReminderTask( $client_id , $service_id , $end_date)
	public function Create_ServiceReminderTask( $Record_id )
	{
		$Cron_data = $this->db->query("SELECT * FROM service_reminder_cron_data WHERE id=".$Record_id)->row_array();

		$Check_Already_Send = $this->db->query("SELECT * FROM service_reminder_cron_data WHERE client_id=".$Cron_data['client_id']." AND service_id=".$Cron_data['service_id']." AND status=1")->num_rows();

		if( !$Check_Already_Send )
		{
			$client_data = $this->db->query("SELECT id,user_id,crm_company_name,firm_id FROM client where id = ".$Cron_data['client_id'])->row_array();

				$service = $this->db->query("SELECT service_name FROM service_lists where id = ".$Cron_data['service_id'])->row_array(); 

				$task_name = $client_data['crm_company_name']." ".$service['service_name']." Reminder Task";

				$insert_task =[	
								'user_id'	=>		$client_data['user_id'],
								'company_name' => $client_data['id'],
								'subject'	=>		$task_name,
								'related_to'=>		'service_reminder',
								'firm_id'	=>    	$client_data['firm_id'],
								'start_date'=> 		date( 'Y-m-d' ),
								'end_date'	=>   	date( 'Y-m-d', strtotime('+ 10 days') ),
								'related_to_services'=>$Cron_data['service_id']
							  ];
				
				$this->db->insert('add_new_task',$insert_task);

				$Task_id =  $this->db->insert_id();

				$Assiness = $this->db->query("SELECT * FROM firm_assignees where module_name='CLIENT' and module_id=".$Cron_data['client_id']." and firm_id = ".$client_data['firm_id'])->result_array();
				foreach ($Assiness as $key => $value)
				{
					$data= [
							'firm_id' => $client_data['firm_id'],
							'module_name' => 'TASK',
							'module_id' =>  $Task_id,
							'assignees' => $value['assignees']
						   ];
					$this->db->insert('firm_assignees',$data);
				}
				
				/*CREATE SUB TASK*/
				$this->Task_section_model->get_services_options_insert( $Task_id );
				/*CREATE SUB TASK*/

				return $Task_id;						  

		}

	}
	public function Calculate_Due($date,$due_type,$days)
	{
		$Date = date_create_from_format('Y-m-d',$date);
					
		if( $due_type =='due_by' )
		{
			date_sub($Date,date_interval_create_from_date_string($days." days"));
		}
		else if(  $due_type == 'overdue_by' )
		{
			date_add($Date,date_interval_create_from_date_string($days." days"));
		}
		return date_format($Date,'d-m-Y');
	}

	public function QuartersBase_ServiceReminder( $client_id, $service_id ,array $service_Data)	
	{
		if( !empty( $service_Data[2] ) )
		{			
			$array = $this->Get_Months( $service_Data[2] );
			foreach ($array as $key => $value)
			{
				$Due_Date =  strtotime( date('Y').'-'.$value.'-01' );
				$service_Data[2] =  date( 'Y-m-t' , $Due_Date );
				
				$this->Insert_ServiceReminder_CronData( $client_id , $service_id , $service_Data );
			}
		}

	}
	public function Get_Months( $Month )
	{
		switch ( $Month )
			{
				case 'Annual End Jan':
					$month = '01';
					break;
				case 'Annual End Feb':
					$month = '02';
					break;
				case 'Annual End Mar':
					$month = '03';
					break;
				case 'Annual End Apr':
					$month = '04';
					break;
				case 'Annual End May':
					$month = '05';
					break;
				case 'Annual End Jun':
					$month = '06';
					break;
				case 'Annual End Jul':
					$month = '07';
					break;
				case 'Annual End Aug':
					$month = '08';
					break;
				case 'Annual End Sep':
					$month = '09';
					break;
				case 'Annual End Oct':
					$month = '10';
					break;
				case 'Annual End Nov':
					$month = '11';
					break;
				case 'Annual End Dec':
					$month = '12';
					break;										
				case 'Jan/Apr/Jul/Oct':
					$month = '01_04_07_10';
					break;	
				case 'Feb/May/Aug/Nov':
					$month = '02_05_08_11';
					break;	
				case 'Mar/Jun/Sep/Dec':
					$month = '03_06_09_12';
					break;	
				case 'Monthly':
					$month = '01_02_03_04_05_06_07_08_09_10_11_12';
					break;	
			}
			return explode('_', $month );
	}
	public function Get_Week( $Type , $Related_Date )
	{	
		$Related_Date 	= date_create_from_format( 'Y-m-d' , $Related_Date );
		$DAY 			= date_format( $Related_Date , "N");
		$Date           = date_format( $Related_Date , "l");
		
		if($Type == 'weekly')
		{
			if(date('N') < $DAY)
			{
				return date('Y-m-d' , strtotime("next ".$Date) );
			}
			else if(date('N') == $DAY)
			{
				return date('Y-m-d');
			}	
		}
		else if($Type == 'Forthnightly')
		{

			$List 		=  $this->getListOfdays( date('Y') , date('m') , $Date );
			
			if( date('d') < $List[1]->format("d") )
			{
				return $List[1]->format("Y-m-d");
			}
			else if( date('d') < $List[3]->format("d") )
			{
				return $List[3]->format("Y-m-d");
			}
			else
			{
				$List 		=  $this->getListOfdays( date('Y' ,strtotime('next month'))  , date( 'm' ,strtotime('next month') ) , $Date );

				return $List[1]->format("Y-m-d");
			}
		}
	}
	public function Add_NewServiceReminder_Cron($Tem_id)
	{
		$TEM_DATA = $this->db->query("SELECT * FROM reminder_setting WHERE id=".$Tem_id)->row_array();
		$FIRM_CON = ( $TEM_DATA['firm_id'] != 0 ? " and firm_id = ".$TEM_DATA['firm_id'] : " " );

		$Fields = $this->Get_Service_ReminderFields();		
		$Fields = array_chunk( explode( ',' , $Fields ) , 3 );
		$Fields = $Fields[ $TEM_DATA['service_id']-1 ];
		
		$query = "SELECT id,firm_id,user_id,".$Fields[2]." as E_date FROM client WHERE ".$Fields[0]."!='' and ".$Fields[2]." !='' ".$FIRM_CON;

		$clients = $this->db->query($query)->result_array();
		foreach ($clients as $key => $value)
		{
			/*$service_task = $this->db->query("SELECT task_status FROM add_new_task where user_id=".$value['user_id']." and related_to_services=".$TEM_DATA['service_id']." AND related_to='service_reminder'")->row_array();
			if(empty($service_task))
			{
				$this->Create_ServiceReminderTask( $value['id'] , $TEM_DATA['service_id'] , $value['E_date'] );	
			}*/
			$Cron_Dates = [];
			if( $TEM_DATA['service_id']==5 )
			{
				$Months = $this->Get_Months($value['E_date']);
				foreach ($Months as $mon)
				{
				 	$date = date('Y').'-'.$mon.'-01';
				 	$Cron_Dates[] = $this->Calculate_Due( $date , $TEM_DATA['due'] , $TEM_DATA['days'] );
				}
			}
			else
			{
				$Cron_Dates[] = $this->Calculate_Due( $value['E_date'] , $TEM_DATA['due'] , $TEM_DATA['days'] );
			}


			foreach ($Cron_Dates as  $Cron_D)
			{
				$data = [
							'firm_id'   	=> $value['firm_id'],
							'client_id' 	=> $value['id'],
							'service_id'	=> $TEM_DATA['service_id'],
							'template_id'	=> $TEM_DATA['id'],
							'date'       	=> $Cron_D,
							/*'status' 		=> (!empty($service_task['task_status'])?$service_task['task_status']:0)	*/
							'status' 		=> 0
						];
				$this->db->insert('service_reminder_cron_data',$data);		
			}  
		}
		
	}

	public function Edit_ExistServiceReminder_Cron($Tem_id)
	{
		$TEM_DATA = $this->db->query("SELECT * FROM reminder_setting WHERE id=".$Tem_id)->row_array();
		//$FIRM_CON = ( $TEM_DATA['firm_id'] != 0 ? " and firm_id = ".$TEM_DATA['firm_id'] : " " );

		$Fields = $this->Get_Service_ReminderFields();		
		$Fields = array_chunk( explode( ',' , $Fields ) , 3 );
		$Fields = end( $Fields[ $TEM_DATA['service_id']-1 ] );

		$query = "SELECT sc.id,sc.client_id,c.".$Fields." as E_date FROM service_reminder_cron_data AS sc 
					INNER JOIN client AS c on c.id = sc.client_id 
						WHERE sc.template_id=".$TEM_DATA['id']." and sc.template_type=0 
							GROUP BY sc.client_id";

		$CLIENTS =$this->db->query($query)->result_array();

		foreach ($CLIENTS as $key => $value)
		{
			if( $TEM_DATA['service_id'] == 5 )
			{	
				$query = "SELECT id FROM service_reminder_cron_data WHERE client_id=".$value['client_id']." and template_id=".$TEM_DATA['id']." and template_type=0 ORDER BY id ASC";
				$CRON_DATA = $this->db->query( $query )->result_array();
				$Months = $this->Get_Months( $value['E_date'] );
				foreach ($Months as $key => $mon)
				{
					$date = date('Y').'-'.$mon.'-01';
					$date = $this->Calculate_Due( $date , $TEM_DATA['due'] , $TEM_DATA['days']);
					$this->db->update('service_reminder_cron_data',['date'=>$date],"id=".$CRON_DATA[$key]['id']);
				}
			}
			else
			{
				$date = $this->Calculate_Due( $value['E_date'] , $TEM_DATA['due'] , $TEM_DATA['days']);
				$this->db->update('service_reminder_cron_data',['date'=>$date],"id=".$value['id']);
			}
		}

	}
	/*
	@From_id is super_admin_owned id
	@To_id  is firm Newly created one.
	*/
	public function Shift_ServiceReminders_Cron( $TEM_id)
	{
		$New_Temp = $this->db->query("SELECT * FROM reminder_setting WHERE id=".$TEM_id)->row_array();

		$this->db->query("UPDATE service_reminder_cron_data SET  template_id=".$New_Temp['id']." WHERE template_type=0 and template_id=".$New_Temp['super_admin_owned']." and firm_id=".$New_Temp['firm_id']);
	}

	Public function Delete_ServiceReminder_Cron($temp_id)
	{
		$this->db->query("DELETE FROM service_reminder_cron_data WHERE template_type=0 and template_id=".$temp_id);
		return $this->db->affected_rows();
	}

	Public function Reminder_Status_Change																																( $User_id , $Service_id , $status )
	{
		$data = $this->db->query("SELECT id from client where user_id = ".$User_id)->row_array();
		$this->db->update('service_reminder_cron_data',["status"=>$status],'client_id='.$data['id'].' and service_id='.$Service_id);
		return  $this->db->affected_rows();
	}
	Public function getListOfdays($y, $m,$D)
	{
	    return new DatePeriod(
	        new DateTime("first ".$D." of $y-$m"),
	        DateInterval::createFromDateString('next '.$D),
	        new DateTime("last day of $y-$m")
	    );
	}

}