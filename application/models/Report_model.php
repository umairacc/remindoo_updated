<?php

class Report_model extends CI_Model 
{
     /* Shashmethah */
	public function index()
	{
	   echo "Model Called";
	}

	public function selectRecord($table, $con, $field)
	{
	    $query = $this->db->query('SELECT * from '.$table.' where '.$con.'='.$field.''); 
	    
	    return $query->row_array();
	}

	public function getClientsUserIds()
	{
   		$this->load->helper('comman');

   		$Assigned_Client = Get_Assigned_Datas('CLIENT'); 		

   		foreach ($Assigned_Client as $key => $value) 
        {
           $rec = $this->selectRecord('client','id', $value);
           
           if(!empty($rec['user_id']))
           {
             $arr[] = $rec['user_id'];
           }  
        }

        if(isset($arr) && count($arr)>0)
        {	           
	       $clients_user_ids = implode(',', $arr);
        }
        else
        {
           $clients_user_ids = "";
        }

	    return $clients_user_ids;
        
	}

	public function client_list()
	{		
		$clients_user_ids = $this->getClientsUserIds();
	    $query = $this->db->query("SELECT * FROM client WHERE autosave_status=0 AND crm_ch_status = 'active' AND FIND_IN_SET(user_id,'".$clients_user_ids."') AND firm_id = '".$_SESSION['firm_id']."' AND crm_company_name != '' ORDER BY user_id desc");  
		$results = $query->result_array();
	    
		return $results;
	}

	public function details_list($legal_form,$start=false,$end=false,$client=false)
	{		
		if(!empty($client))
		{
           $clients_user_ids = $client;
		}
		else
		{
		   $clients_user_ids = $this->getClientsUserIds();
		}

		if(!empty($start) && !empty($end))
		{
			$between = 'AND from_unixtime(created_date) BETWEEN "'.$start.'" AND "'.$end.'"';
		}
		else
		{
			$between = '';
		}	
		   
		$query = $this->db->query("SELECT count(*) as limit_count FROM client WHERE autosave_status=0 AND FIND_IN_SET(user_id,'".$clients_user_ids."') AND firm_id = '".$_SESSION['firm_id']."' AND crm_company_name != '' and crm_legal_form = '".$legal_form."'  ".$between."")->row_array();
	   
		return $query;		
	}

	public function task_details($status)
	{
		$this->load->helper('comman');

		$Assigned_Task = Get_Assigned_Datas('TASK');
		$assigned_task = implode(',',$Assigned_Task);

		$query = $this->db->query('SELECT count(*) as task_count from add_new_task WHERE FIND_IN_SET(id,"'.$assigned_task.'") AND firm_id = "'.$_SESSION['firm_id'].'" AND task_status = "'.$status.'"')->row_array();
        return $query;
	}

	public function task_count($start=false,$end=false,$client=false)
	{
		$this->load->helper('comman');

		$Assigned_Task = Get_Assigned_Datas('TASK'); 
		$assigned_task = implode(',',$Assigned_Task);

		if(!empty($client))
		{
           $client_cond = 'AND user_id = "'.$client.'"';
		}
		else
		{
		   $client_cond = '';
		}

		if(!empty($start) && !empty($end))
		{
			$between = 'AND (start_date BETWEEN "'.$start.'" AND "'.$end.'" OR end_date BETWEEN "'.$start.'" AND "'.$end.'")';
		}
		else
		{
			$between = '';
		}

		$query = $this->db->query('SELECT count(*) as task_count from add_new_task WHERE FIND_IN_SET(id,"'.$assigned_task.'") AND firm_id = "'.$_SESSION['firm_id'].'" AND task_status !="" AND task_status IN("1","2","3","4","5") '.$client_cond.'  '.$between.'')->row_array();
        return $query;
	}

	public function getTasks()
	{
        $this->load->helper('comman');

        $Assigned_Task = Get_Assigned_Datas('TASK');
        $assigned_task = implode(',',$Assigned_Task);

		$tasks = $this->db->query('SELECT * from add_new_task WHERE FIND_IN_SET(id,"'.$assigned_task.'") AND firm_id = "'.$_SESSION['firm_id'].'" AND task_status IN("1","2","3","4","5")')->result_array();
        return $tasks;
	}

	public function getCompletedTasks()
	{
        $this->load->helper('comman');

        $Assigned_Task = Get_Assigned_Datas('TASK');
        $assigned_task = implode(',',$Assigned_Task);

		$tasks = $this->db->query('SELECT * from add_new_task WHERE FIND_IN_SET(id,"'.$assigned_task.'") AND firm_id = "'.$_SESSION['firm_id'].'" AND task_status !="" AND task_status = "5"')->result_array();
        return $tasks;
	}

	public function task_pridetails($status,$start=false,$end=false,$client=false)
	{
		$this->load->helper('comman');

		$Assigned_Task = Get_Assigned_Datas('TASK');
		$assigned_task = implode(',',$Assigned_Task);

		if(!empty($client))
		{
           $client_cond = 'AND user_id = "'.$client.'"';
		}
		else
		{
		   $client_cond = '';
		}

		if(!empty($start) && !empty($end))
		{
			$between = 'AND (start_date BETWEEN "'.$start.'" AND "'.$end.'" OR end_date BETWEEN "'.$start.'" AND "'.$end.'")';
		}
		else
		{
			$between = '';
		}

		$query = $this->db->query('SELECT count(*) as task_count from add_new_task WHERE FIND_IN_SET(id,"'.$assigned_task.'") AND firm_id = "'.$_SESSION['firm_id'].'" and priority = "'.$status.'" '.$client_cond.'  '.$between.'')->row_array();
        return $query;
	}

	public function lead_details($status,$start=false,$end=false,$client=false)
	{
		$this->load->helper('comman');

		$Assigned_Leads = Get_Assigned_Datas('LEADS');
		$assigned_leads = implode(',',$Assigned_Leads);

		if(!empty($start) && !empty($end))
		{
			$between = 'AND from_unixtime(createdTime) BETWEEN "'.$start.'" AND "'.$end.'"';
		}
		else
		{
			$between = '';
		}

		if(!empty($client))
		{
			$client_cond = 'AND convert_client_user_id = "'.$client.'"';
		}
		else
		{
			$client_cond = '';
		}

		$query = $this->db->query('SELECT count(*) as lead_count from leads where FIND_IN_SET(id,"'.$assigned_leads.'") AND firm_id = "'.$_SESSION['firm_id'].'" AND lead_status = "'.$status.'" '.$between.' '.$client_cond.'')->row_array();
        return $query;
	}

	public function getLeads()
	{ 
        $this->load->helper('comman');

        $Assigned_Leads = Get_Assigned_Datas('LEADS');
        $assigned_leads = implode(',',$Assigned_Leads);

        $leads = $this->db->query('SELECT * from leads where FIND_IN_SET(id,"'.$assigned_leads.'") AND firm_id = "'.$_SESSION['firm_id'].'"')->result_array();
        return $leads;
	}

	public function leads_history_section($start=false,$end=false,$client=false)
	{
		if(empty($start) && empty($end))
		{
			$month_end = date('Y-m-t');
			$month_end = date('Y-m-d',strtotime($month_end. '+1 day'));
			$month_start = date('Y-m-01');
        }
        else
        {
            $month_start = $start;
            $month_end = $end;
        }

        if(!empty($client))
        {
        	$client_cond = 'AND convert_client_user_id = "'.$client.'"';
        }
        else
        {
        	$client_cond = '';
        }

		$this->load->helper('comman');

		$Assigned_Leads = Get_Assigned_Datas('LEADS');
		$assigned_leads = implode(',',$Assigned_Leads);
	     
		$query=$this->db->query('SELECT name,createdTime,id,lead_status from leads where FIND_IN_SET(id,"'.$assigned_leads.'") AND firm_id = "'.$_SESSION['firm_id'].'" and from_unixtime(createdTime) BETWEEN "'.$month_start.'" AND "'.$month_end.'"  '.$client_cond.'')->result_array();
		return $query;
	}

	public function service_count($service)
	{
		$clients_user_ids = $this->getClientsUserIds();
        $client_details =$this->db->query('SELECT count(*) as service_count FROM client WHERE FIND_IN_SET(user_id,"'.$clients_user_ids.'") AND firm_id = "'.$_SESSION['firm_id'].'" and crm_company_name != "" and ('.$service.' != "") ORDER BY id DESC')->row_array();
        return $client_details;
	}

	public function dead_count($search_value,$service)
	{
	    $clients_user_ids = $this->getClientsUserIds();

	    if($search_value!="")
	    {
	    	$search = 'and ('.$search_value.'!= "")';
	    }
	    else
	    {
	        $search = "";
	    }

        $client_details = $this->db->query('SELECT count(*) as dead_count FROM client WHERE FIND_IN_SET(user_id,"'.$clients_user_ids.'") AND firm_id = "'.$_SESSION['firm_id'].'" and crm_company_name != "" and ('.$service.'!="") '.$search.' order by id desc')->row_array();
         
        return $client_details;
	}

	public function task_this_month($table,$start,$end,$client=false)
	{
		$this->load->helper('comman');

		$Assigned_Task = Get_Assigned_Datas('TASK');
		$assigned_task = implode(',',$Assigned_Task);

		if(!empty($client))
		{
           $client_cond = 'AND user_id = "'.$client.'"';
		}
		else
		{
		   $client_cond = '';
		}

		$query = $this->db->query('SELECT start_date,end_date,subject from add_new_task where FIND_IN_SET(id,"'.$assigned_task.'") AND firm_id = "'.$_SESSION['firm_id'].'" and (start_date BETWEEN "'.$start.'" AND "'.$end.'" OR end_date BETWEEN "'.$start.'" AND "'.$end.'")  '.$client_cond.' ORDER BY start_date ASC')->result_array();
		return $query;
	}

	public function users_count($start=false,$end=false)
	{
		if(!empty($start) && !empty($end))
		{
			$between = 'AND from_unixtime(CreatedTime) BETWEEN "'.$start.'" AND "'.$end.'"';
		}
		else
		{
			$between = '';
		}

		$query1 = $this->db->query("SELECT count(*) as user_count FROM `user` where autosave_status!='1' and crm_name!='' and firm_id = '".$_SESSION['firm_id']."' AND user_type IN ('FU')  ".$between." order by id DESC");
		$results1 = $query1->row_array();
		return $results1;
	}

	public function getFirmUsers()
	{
		$fu = $this->db->query("SELECT * FROM `user` where autosave_status!='1' and crm_name!='' and firm_id = '".$_SESSION['firm_id']."' AND user_type IN ('FU') ORDER BY id DESC")->result_array();
		
		return $fu;
	}

	public function activeusers_count($start=false,$end=false)
	{		
		if(!empty($start) && !empty($end))
		{
			$between = 'AND from_unixtime(CreatedTime) BETWEEN "'.$start.'" AND "'.$end.'"';
		}
		else
		{
			$between = '';
		}

		$query1 = $this->db->query("SELECT count(*) as activeuser_count FROM `user` where  autosave_status!='1' and crm_name!='' and firm_id = '".$_SESSION['firm_id']."' AND user_type IN ('FU')  ".$between." and status='1' order by id DESC");
		$results1 = $query1->row_array();
		return $results1;
	}

	public function inactiveusers_count($start=false,$end=false)
	{
		if(!empty($start) && !empty($end))
		{
			$between = 'AND from_unixtime(CreatedTime) BETWEEN "'.$start.'" AND "'.$end.'"';
		}
		else
		{
			$between = '';
		}

		$query1 = $this->db->query("SELECT count(*) as inactiveuser_count FROM `user` where autosave_status!='1' and crm_name!='' and firm_id = '".$_SESSION['firm_id']."' AND user_type IN ('FU')  ".$between." and (status='0' or status='2') order by id DESC");
		$results1 = $query1->row_array();
		return $results1;
	}

	public function frozenusers_count($start=false,$end=false)
	{
		if(!empty($start) && !empty($end))
		{
			$between = 'AND from_unixtime(CreatedTime) BETWEEN "'.$start.'" AND "'.$end.'"';
		}
		else
		{
			$between = '';
		}

		$query1 = $this->db->query("SELECT count(*) as frozenuser_count FROM `user` where autosave_status!='1' and crm_name!='' and firm_id = '".$_SESSION['firm_id']."' AND user_type IN ('FU')  ".$between." and (status='3') order by id DESC");
		$results1 = $query1->row_array();
		return $results1;
	}

	public function completed_task_count($start=false,$end=false,$client=false)
	{		
		$this->load->helper('comman');

		$Assigned_Task = Get_Assigned_Datas('TASK');
		$assigned_task = implode(',', $Assigned_Task);

		if(!empty($client))
		{
           $client_cond = 'AND user_id = "'.$client.'"';
		}
		else
		{
		   $client_cond = '';
		}

		if(!empty($start) && !empty($end))
		{
			$between = 'AND (start_date BETWEEN "'.$start.'" AND "'.$end.'" OR end_date BETWEEN "'.$start.'" AND "'.$end.'")';
		}
		else
		{
			$between = '';
		}

		$query = $this->db->query("SELECT count(*) as task_count from add_new_task where firm_id = '".$_SESSION['firm_id']."' AND FIND_IN_SET(id,'".$assigned_task."') AND task_status = '5'  ".$client_cond."  ".$between."")->row_array();
        return $query;
	}

	public function overallleads_count($start=false,$end=false)
	{
		$this->load->helper('comman');

		$Assigned_Leads = Get_Assigned_Datas('LEADS');
		$assigned_leads = implode(',',$Assigned_Leads);

		if(!empty($start) && !empty($end))
		{
			$between = 'AND from_unixtime(createdTime) BETWEEN "'.$start.'" AND "'.$end.'"';
		}
		else
		{
			$between = '';
		}

		$query = $this->db->query('SELECT count(*) as leads_count from leads where FIND_IN_SET(id,"'.$assigned_leads.'") '.$between.'')->row_array();
        return $query;
	}

	public function task_detailsWithdate($status,$start,$end,$client=false)
	{		
		$this->load->helper('comman');

		$Assigned_Task = Get_Assigned_Datas('TASK');
		$assigned_task = implode(',',$Assigned_Task);

		if(!empty($client))
		{
           $client_cond = 'AND user_id = "'.$client.'"';
		}
		else
		{
		   $client_cond = '';
		}

		if(!empty($start) && !empty($end))
		{
			$between = 'AND (start_date BETWEEN "'.$start.'" AND "'.$end.'" OR end_date BETWEEN "'.$start.'" AND "'.$end.'")';
		}
		else
		{
			$between = '';
		}

		$query = $this->db->query('SELECT count(*) as task_count from add_new_task WHERE FIND_IN_SET(id,"'.$assigned_task.'") AND firm_id = "'.$_SESSION['firm_id'].'"  '.$between.' and task_status = "'.$status.'"  '.$client_cond.'')->row_array();
        return $query;
	}

	public function leads_detailsWithDate($status,$start,$end)
	{
		$this->load->helper('comman');

		$Assigned_Leads = Get_Assigned_Datas('LEADS');
		$assigned_leads = implode(',',$Assigned_Leads);

		$query = $this->db->query('SELECT count(*) as lead_count from leads WHERE FIND_IN_SET(id,"'.$assigned_leads.'") AND firm_id = "'.$_SESSION['firm_id'].'" and lead_status = "'.$status.'" and from_unixtime(createdTime) BETWEEN "'.$start.'" AND "'.$end.'"')->row_array();
		return $query;
	}

	public function detailsWithDate($service,$search_value,$start,$end,$client=false)
	{
		if(!empty($client))
		{
           $clients_user_ids = $client;
		}
		else
		{
		   $clients_user_ids = $this->getClientsUserIds();
		}

		if($search_value!="")
	    {
	    	$search = 'and ('.$search_value.'!= "")';
	    }
	    else
	    {
	        $search = '';
	    }

	    if(!empty($service))
	    {
	    	$service_cond = 'and ('.$service.' != "")';
	    }
	    else
	    {
	    	$service_cond = '';
	    }

	    if(!empty($start) && !empty($end))
	    {
	    	$between = 'AND from_unixtime(created_date) BETWEEN "'.$start.'" AND "'.$end.'"';
	    }
	    else
	    {
	    	$between = '';
	    }

        $client_details =$this->db->query('SELECT count(*) as service_count FROM client WHERE FIND_IN_SET(user_id,"'.$clients_user_ids.'") AND firm_id = "'.$_SESSION['firm_id'].'" and crm_company_name != ""  '.$service_cond.'  '.$search.' '.$between.'')->row_array();
         
        return $client_details;
	}
    
    public function selectInvoiceByType($table,$field,$status,$field1,$start,$end,$client=false)
    {
        if(empty($client))
        {
            $client_ids = $this->getClientsUserIds();
        }
        else
        {
        	$client_ids = $client;
        }

        if(!empty($table) && !empty($field) && !empty($status))
        {
            $condition = ''.$table.'.'.$field.' = "'.$status.'" AND ';

            if($status == 'pending')
            {
            	$condition .= 'Invoice_details.invoice_duedate != "Expired" AND ';
            }
        }
        else
        {
            $condition = "";
        } 

        if(!empty($start) && !empty($end))
        {
        	$between = 'AND from_unixtime(Invoice_details.invoice_date) BETWEEN "'.$start.'" AND "'.$end.'"';
        }
        else
        {
        	$between = '';
        }
       
        $records = $this->db->query('SELECT count(*) as invoice_count FROM Invoice_details LEFT JOIN amount_details ON Invoice_details.client_id = amount_details.client_id where '.$condition.' FIND_IN_SET(Invoice_details.'.$field1.',"'.$client_ids.'") '.$between.'')->row_array();
        
        return $records;
    }

    public function getServices()
    {	
    	$clients_user_ids = $this->getClientsUserIds();
        $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();     
         
        $checks = "";

        foreach($other_services as $key => $value) 
        {
           $checks .= "or crm_".$value['services_subnames']."_statement_date != ''";
        }  

    	$records = $this->db->query("SELECT * FROM client WHERE FIND_IN_SET(user_id,'".$clients_user_ids."') and crm_company_name !='' and ( crm_investigation_end_date!='' or  crm_registered_renew_date!='' or crm_insurance_renew_date!='' or  crm_next_booking_date!='' or crm_next_manage_acc_date!='' or crm_next_p11d_return_due!='' and crm_pension_subm_due_date!='' or crm_rti_deadline!='' or crm_vat_due_date!='' or crm_personal_due_date_return!='' or  crm_accounts_tax_date_hmrc!='' or crm_ch_accounts_next_due!='' or crm_confirmation_statement_due_date!='' ".$checks.") ")->result_array();

    	return $records;
    }

    public function getClients()
    {
    	$clients_user_ids = $this->getClientsUserIds();

    	$records = $this->db->query("SELECT * FROM user where autosave_status!='1' and crm_name!='' and firm_id = '".$_SESSION['firm_id']."' AND FIND_IN_SET(id,'".$clients_user_ids."') ORDER BY id DESC")->result_array(); 

    	return $records;
    }

	public function deadline_detailsWithDate($search_value,$service,$start,$end,$client=false)
	{
		if(!empty($client))
		{
           $clients_user_ids = $client;
		}
		else
		{
		   $clients_user_ids = $this->getClientsUserIds();
		}

		if($search_value!="" && $start == "" && $end == "")
	    {
	    	$search = 'and ('.$search_value.'!= "")';
	    }
	    else if($search_value!="" && $start != "" && $end != "")
	    {
            $search = 'and ('.$search_value.' BETWEEN "'.$start.'" AND "'.$end.'")';
	    }
	    else
	    {
	        $search = '';
	    }

	    if(!empty($service))
	    {
	    	$service_cond = 'and ('.$service.' != "")';
	    }
	    else
	    {
	    	$service_cond = '';
	    }

	    $client_details = $this->db->query('SELECT count(*) as service_count FROM client WHERE FIND_IN_SET(user_id,"'.$clients_user_ids.'") AND firm_id = "'.$_SESSION['firm_id'].'" and crm_company_name != ""  '.$service_cond.'  '.$search.'')->row_array();
	     
	    return $client_details;
	}
    
    /* Shashmethah */
    public function proposal_detailsWithDate($status,$last_Year_start,$last_month_end)
	{
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select count(*) as proposal_count from proposals
        where user_id="'.$user_id.'" and status="'.$status.'" and created_at BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
        return $query;
	}

	public function details_list1($legal_form,$last_Year_start,$last_month_end,$con,$status){
		$user_id=$_SESSION['id'];
		//and status='".$status."'
		$query1=$this->db->query("SELECT * FROM `user` where autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' ".$con." status='".$status."'  order by id DESC");
		echo $this->db->last_query();
		$results1 = $query1->result_array();	
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);		
		$count=explode(',',$im_val);		
		if($count['0']!=''){
		$query=$this->db->query('SELECT count(*) as limit_count FROM client WHERE autosave_status=0 and user_id in ('.$im_val.') and crm_legal_form="'.$legal_form.'" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();		
		}else{
			$query=0;
		}
		return $query;		
	}


	public function details_list2($legal_form,$last_Year_start,$last_month_end,$con,$status){
		$user_id=$_SESSION['id'];
		//and status='".$status."'
		$query1=$this->db->query("SELECT * FROM `user` where autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' ".$con." status='".$status."'  order by id DESC");
		echo $this->db->last_query();
		$results1 = $query1->result_array();	
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);		
		$count=explode(',',$im_val);		
		if($count['0']!=''){
		$query=$this->db->query('SELECT count(*) as limit_count FROM client WHERE autosave_status=0 and user_id in ('.$im_val.') and crm_legal_form!="'.$legal_form.'" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();		
		}else{
			$query=0;
		}
		return $query;		
	}

	public function client_list1($legal_form,$con1,$last_Year_start,$last_month_end,$con2,$status){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4  and firm_admin_id='".$user_id."' ".$con2." status='".$status."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
	if($count['0']!=''){
	   $query=$this->db->query('SELECT user_id FROM client WHERE autosave_status=0 and user_id in ('.$im_val.') and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'" '.$con1.' crm_legal_form="'.$legal_form.'"  order by id desc ');
	  // echo $query;  
		$results = $query->result_array();
	}else{
		$results=0;
	}
		return $results;	
	}

	

	public function task_details_date($status,$con1,$last_Year_start,$last_month_end,$con,$priority){	
	   $user_id=$_SESSION['id'];	
		$query=$this->db->query('select count(*) as task_count from add_new_task where create_by="'.$user_id.'" and  from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'" AND task_status="'.$status.'" '.$con.' priority="'.$priority.'"')->row_array();
		//echo $this->db->last_query();
		return $query;
	}


	public function task_details_date1($status,$con1,$last_Year_start,$last_month_end,$con,$priority){	
	   $user_id=$_SESSION['id'];	
		$query=$this->db->query('select count(*) as task_count from add_new_task where create_by="'.$user_id.'" and  from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'" AND task_status!="'.$status.'" '.$con.' priority="'.$priority.'"')->row_array();
		//echo $this->db->last_query();
		return $query;
	}

	public function run_task_list(){
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select * from add_new_task where create_by="'.$user_id.'"')->result_array();
		//echo $query;
        return $query;
	}



	public function task_list(){
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select id,subject,start_date,end_date,task_status,worker,team,department from add_new_task where create_by="'.$user_id.'"')->result_array();
		//echo $query;
        return $query;
	}

	public function task_list1($status,$con1,$last_Year_start,$last_month_end,$con2,$priority){
		$user_id=$_SESSION['id'];
		if($con1=='and'){
		$query=$this->db->query('select id,subject,start_date,end_date,task_status,worker,team,department  from add_new_task where create_by="'.$user_id.'" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'" '.$con1.' task_status="'.$status.'" '.$con2.' priority="'.$priority.'"')->result_array();
	}else{
		$query=$this->db->query('select id,subject,start_date,end_date,task_status,worker,team,department  from add_new_task where create_by="'.$user_id.'" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"  '.$con2.' priority="'.$priority.'" '.$con1.' task_status="'.$status.'"')->result_array();
	}
		//echo $query;
       return $query;
	}

	public function proposal_details($status){
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select count(*) as proposal_count from proposals
        where user_id="'.$user_id.'" and status="'.$status.'"')->row_array();
        return $query;
	}

	public function proposal_details_date(){
		$query=$this->db->query('select count(*) as proposal_count from proposals
	        where status="accepted" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"');
	}

	public function proposal_list(){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT id,proposal_no,proposal_name,grand_total,status,created_at FROM `proposals` where user_id='".$user_id."' order by id Asc")->result_array();
		return $query1;
	}

	public function proposal_searching($status,$con,$last_month_start,$last_mon_end){
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select count(*) as proposal_count from proposals
	        where user_id='.$user_id.' and status="'.$status.'" '.$con.' created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
		//echo $this->db->last_query();
		return $query;
	}

	public function proposal_searching_new($status,$con,$last_month_start,$last_mon_end){
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select count(*) as proposal_count from proposals
	        where user_id='.$user_id.' and status!="'.$status.'" '.$con.' created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
		//echo $this->db->last_query();
		return $query;
	}

	public function proposal_searching1($status,$con,$last_month_start,$last_mon_end){
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select id,proposal_no,proposal_name,grand_total,status,created_at from proposals
	        where user_id='.$user_id.' and status="'.$status.'" '.$con.' created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->result_array();
		//echo $this->db->last_query();
		return $query;
	}

	

	public function lead_list(){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT id,name,company,email_address,assigned,lead_status FROM `leads` where user_id='".$user_id."' order by id Asc")->result_array();
		return $query1;
	}

	public function lead_status($id){
		$user_id=$_SESSION['id'];
		$query=$this->db->query("select status_name from leads_status where id='".$id."'")->row_array();
		$status_name=$query['status_name'];
		return $status_name;
	}

	public function leads_details($status,$last_Year_start,$last_month_end)
	{
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select count(*) as lead_count from leads where user_id='.$user_id.' and lead_status="'.$status.'"  and from_unixtime(createdTime) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
		return $query;
	}

	public function leads_details_new($status,$last_Year_start,$last_month_end)
	{
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select count(*) as lead_count from leads where user_id='.$user_id.' and lead_status!="'.$status.'"  and from_unixtime(createdTime) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
		return $query;
	}

	public function leads_details1($status,$con,$last_Year_start,$last_month_end)
	{
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select id,name,company,email_address,assigned,lead_status from leads where user_id='.$user_id.' and lead_status="'.$status.'" '.$con.' from_unixtime(createdTime) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->result_array();
		return $query;
	}

	public function service_details(){
		 $id=$_SESSION['id'];
         $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
           $us_id =  explode(',', $sql['us_id']);
          $client_details =$this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') order by id desc")->result_array();
          return $client_details;
	}

	

	public function service_count_1($status,$last_Year_start, $last_month_end){
		if($status=='conf_statement'){ 
			$search_value='crm_confirmation_statement_due_date';
		}
		if($status=='accounts'){ 
			$search_value='crm_ch_accounts_next_due';
		}
		if($status=='company_tax_return'){ 
			$search_value='crm_accounts_tax_date_hmrc';
		}
		if($status=='personal_tax_return'){ 
			$search_value='crm_personal_due_date_return';
		}
		if($status=='vat'){ 
			$search_value='crm_vat_due_date';
		}
		if($status=='payroll'){ 
			$search_value='crm_rti_deadline';
		}
		if($status=='workplace'){ 
			$search_value='crm_pension_subm_due_date';
		}
		if($status=='cis'){ 
			$search_value='';
		}
		if($status=='cissub'){ 
			$search_value='';
		}
		if($status=='p11d'){ 
			$search_value='crm_next_p11d_return_due';
		}
		if($status=='management'){ 
			$search_value='crm_next_manage_acc_date';
		}
		if($status=='bookkeep'){ 
			$search_value='crm_next_booking_date';
		}
		if($status=='investgate'){ 
			$search_value='crm_insurance_renew_date';
		}
		if($status=='registered'){ 
			$search_value='crm_registered_renew_date';
		}
		if($status=='taxadvice'){ 
			$search_value='crm_investigation_end_date';
		}
		if($status=='taxinvest'){ 
			$search_value='crm_investigation_end_date';
		}
		if($search_value!=''){
		$id=$_SESSION['id'];
         $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
           $us_id =  explode(',', $sql['us_id']);
          $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (".$status."!='') and (".$search_value." BETWEEN '".$last_Year_start."' AND '".$last_month_end."') order by id desc")->row_array();  

          }else{
          	$client_details=0;
          }        
          return $client_details;
	}

	public function service_count_2($status,$last_Year_start, $last_month_end){
		$id=$_SESSION['id'];
         $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
           $us_id =  explode(',', $sql['us_id']);

		if($status=='conf_statement'){ 
			$search_value='crm_confirmation_statement_due_date';
			$client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or payroll!='' or taxinvest!='' ) and ((crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array(); 
		}
		if($status=='accounts'){ 
			$search_value='crm_ch_accounts_next_due';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or payroll!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  

		}
		if($status=='company_tax_return'){ 
			$search_value='crm_accounts_tax_date_hmrc';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or  personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!=''  or payroll!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='personal_tax_return'){ 
			$search_value='crm_personal_due_date_return';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or  workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!=''  or payroll!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='vat'){ 
			$search_value='crm_vat_due_date';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or  cis!='' or cissub!='' or p11d!='' or payroll!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='payroll'){ 
			$search_value='crm_rti_deadline';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or  bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='workplace'){ 
			$search_value='crm_pension_subm_due_date';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or  vat!='' or payroll!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='cis'){ 
			$search_value='';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or payroll!='' or vat!='' or  cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='cissub'){ 
			$search_value='';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or payroll!='' or vat!='' or cis!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='p11d'){ 
			$search_value='crm_next_p11d_return_due';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or payroll!='' or vat!='' or cis!='' or cissub!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='management'){ 
			$search_value='crm_next_manage_acc_date';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or payroll!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or  investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )   or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='bookkeep'){ 
			$search_value='crm_next_booking_date';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or payroll!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or  management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='investgate'){ 
			$search_value='crm_insurance_renew_date';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or payroll!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or  registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )   or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='registered'){ 
			$search_value='crm_registered_renew_date';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or payroll!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or  taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='taxadvice'){ 
			$search_value='crm_investigation_end_date';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or payroll!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or  taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}
		if($status=='taxinvest'){ 
			$search_value='crm_investigation_end_date';
			 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !=''  and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or payroll!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->row_array();  
		}        
           return $client_details;
	}

	public function service_details_list($last_Year_start,$last_month_end){
		$id=$_SESSION['id'];
         $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
           $us_id =  explode(',', $sql['us_id']);

		$client_details =$this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !=''  and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->result_array();
		 return $client_details;
	}

	public function service_details_list1($status,$last_Year_start,$last_month_end){
		if($status=='conf_statement'){ 
			$search_value='crm_confirmation_statement_due_date';
		}
		if($status=='accounts'){ 
			$search_value='crm_ch_accounts_next_due';
		}
		if($status=='company_tax_return'){ 
			$search_value='crm_accounts_tax_date_hmrc';
		}
		if($status=='personal_tax_return'){ 
			$search_value='crm_personal_due_date_return';
		}
		if($status=='vat'){ 
			$search_value='crm_vat_due_date';
		}
		if($status=='payroll'){ 
			$search_value='crm_rti_deadline';
		}
		if($status=='workplace'){ 
			$search_value='crm_pension_subm_due_date';
		}
		if($status=='cis'){ 
			$search_value='';
		}
		if($status=='cissub'){ 
			$search_value='';
		}
		if($status=='p11d'){ 
			$search_value='crm_next_p11d_return_due';
		}
		if($status=='management'){ 
			$search_value='crm_next_manage_acc_date';
		}
		if($status=='bookkeep'){ 
			$search_value='crm_next_booking_da';
		}
		if($status=='investgate'){ 
			$search_value='crm_insurance_renew_date';
		}
		if($status=='registered'){ 
			$search_value='crm_registered_renew_date';
		}
		if($status=='taxadvice'){ 
			$search_value='crm_investigation_end_date';
		}
		if($status=='taxinvest'){ 
			$search_value='crm_investigation_end_date';
		}
		if($search_value!=''){
		$id=$_SESSION['id'];
         $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
           $us_id =  explode(',', $sql['us_id']);
          $client_details =$this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (".$status."!='') and (".$search_value." BETWEEN '".$last_Year_start."' AND '".$last_month_end."') order by id desc")->result_array();  

          }else{
          	$client_details=0;
          }        
          return $client_details;
	}

	public function deadline_records(){
		$id=$_SESSION['id'];
         $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
           $us_id =  explode(',', $sql['us_id']);
		$deadline_records=$this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date!= '') or (crm_ch_accounts_next_due != '') or (crm_accounts_tax_date_hmrc != '' ) or (crm_personal_due_date_return != '' ) or (crm_vat_due_date != '' ) or (crm_rti_deadline != '')  or (crm_pension_subm_due_date != '') or (crm_next_p11d_return_due != '')  or (crm_next_manage_acc_date != '') or (crm_next_booking_date != '' ) or (crm_insurance_renew_date != '')  or (crm_registered_renew_date != '' ) or (crm_investigation_end_date != '' )) ")->result_array();
		return $deadline_records;
	}

	

	public function dead_counts($search_value,$service,$last_Year_start,$last_month_end,$con,$company_type){
		if($search_value!=''){
		$id=$_SESSION['id'];
         $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
           $us_id =  explode(',', $sql['us_id']);
          $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (".$service."!='') and (".$search_value." BETWEEN '".$last_Year_start."' AND '".$last_month_end."') ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();
         }else{
         	$client_details['dead_count']=0;
         }
          return $client_details;
	}

	public function dead_count_new($search_value,$status,$last_Year_start,$last_month_end,$con,$company_type){
		$id=$_SESSION['id'];
         $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
           $us_id =  explode(',', $sql['us_id']);

		if($status=='conf_statement'){ 
			$search_value='crm_confirmation_statement_due_date';
			$client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or payroll!='' or taxinvest!='' ) and ((crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array(); 
		}
		if($status=='accounts'){ 
			$search_value='crm_ch_accounts_next_due';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or payroll!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  

		}
		if($status=='company_tax_return'){ 
			$search_value='crm_accounts_tax_date_hmrc';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or  personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!=''  or payroll!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='personal_tax_return'){ 
			$search_value='crm_personal_due_date_return';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or  workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!=''  or payroll!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='vat'){ 
			$search_value='crm_vat_due_date';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or  cis!='' or cissub!='' or p11d!='' or payroll!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='payroll'){ 
			$search_value='crm_rti_deadline';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or  bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='workplace'){ 
			$search_value='crm_pension_subm_due_date';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or  vat!='' or payroll!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='cis'){ 
			$search_value='';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or payroll!='' or vat!='' or  cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='cissub'){ 
			$search_value='';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or payroll!='' or vat!='' or cis!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='p11d'){ 
			$search_value='crm_next_p11d_return_due';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or payroll!='' or vat!='' or cis!='' or cissub!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='management'){ 
			$search_value='crm_next_manage_acc_date';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or payroll!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or  investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )   or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='bookkeep'){ 
			$search_value='crm_next_booking_date';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or payroll!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or  management!='' or investgate!='' or registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='investgate'){ 
			$search_value='crm_insurance_renew_date';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or payroll!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or  registered!='' or taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )   or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='registered'){ 
			$search_value='crm_registered_renew_date';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or payroll!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or  taxadvice!='' or taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='taxadvice'){ 
			$search_value='crm_investigation_end_date';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or cis!='' or payroll!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or  taxinvest!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}
		if($status=='taxinvest'){ 
			$search_value='crm_investigation_end_date';
			 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !=''  and (conf_statement!='' or accounts!='' or company_tax_return!='' or personal_tax_return!='' or workplace!='' or vat!='' or payroll!='' or cis!='' or cissub!='' or p11d!='' or bookkeep!='' or management!='' or investgate!='' or registered!='' or taxadvice!='') and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con." crm_legal_form='".$company_type."' order by id desc")->row_array();  
		}  
		return $client_details;  
	}

	public function deadline_records_new($search_value,$status,$con1,$last_Year_start,$last_month_end,$company_type,$con2){
		$id=$_SESSION['id'];
         $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
           $us_id =  explode(',', $sql['us_id']);

           if($con1=='and'){
		$client_details=$this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !=''  and (".$status."!='') and ((".$search_value." BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) ".$con2." crm_legal_form='".$company_type."' order by id desc")->result_array();
		}else{
			if($con2=='and'){
				
				$client_details=$this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and `crm_company_name`!='' and (crm_legal_form='".$company_type."' and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."') or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."') or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."')  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."') or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."')  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."') or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."')  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ))) order by id desc")->result_array();
			}else{
				//echo "second";
				//exit;
			$client_details= $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and `crm_company_name`!='' and ((crm_confirmation_statement_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."') or (crm_ch_accounts_next_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."') or (crm_accounts_tax_date_hmrc BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_personal_due_date_return BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_vat_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_rti_deadline BETWEEN '".$last_Year_start."' AND '".$last_month_end."')  or (crm_pension_subm_due_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."') or (crm_next_p11d_return_due BETWEEN '".$last_Year_start."' AND '".$last_month_end."')  or (crm_next_manage_acc_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."') or (crm_next_booking_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_insurance_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."')  or (crm_registered_renew_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' ) or (crm_investigation_end_date BETWEEN '".$last_Year_start."' AND '".$last_month_end."' )) order by id desc")->result_array();
			}
	}
	return $client_details;
	}

	

	public function task_this_month_demo($table,$start,$end){
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select start_date,end_date,subject from add_new_task where create_by="'.$user_id.'" and  start_date BETWEEN "'.$start.'" AND "'.$end.'" or end_date BETWEEN "'.$start.'" AND "'.$end.'" order by start_date asc')->result_array();
		return $query;
	}

	

	public function summary_client_details(){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT id FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		return $results1;
	}

	public function summary_task_details(){
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select id,subject,start_date,end_date,task_status,worker,team,department from add_new_task
        where create_by="'.$user_id.'" and task_status="5"')->result_array();
        return $query;
	}

	public function summary_lead_details(){
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select id,name,company,email_address,assigned,lead_status from leads
        where user_id="'.$user_id.'"')->result_array();
        return $query;
	}

	public function summary_user_detailsWithDate($start,$end,$filter=false)
	{
		if(!empty($filter) && $filter != 'all')
		{					
			if($filter=='week')
			{
				$current_date = date('Y-m-d');
				$week = date('W', strtotime($current_date));
				$year = date('Y', strtotime($current_date));

				$dto = new DateTime();
				$start_date = $dto->setISODate($year, $week, 0)->format('Y-m-d');
				$end_date = $dto->setISODate($year, $week, 6)->format('Y-m-d');
				$end_date = date('Y-m-d', strtotime($end_date . ' +1 day')); 
			}
            else if($filter=='three_week')
            {
				$current_date = date('Y-m-d');
				$weeks = strtotime('- 3 week', strtotime( $current_date ));
				$start_date = date('Y-m-d', $weeks); 
				$addweeks = strtotime('+ 3 week', strtotime( $start_date ));
				$end_Week = date('Y-m-d', $addweeks); 
				$end_date = date('Y-m-d', strtotime($end_Week . ' +1 day'));
			}

			else if($filter=='month')
			{
				$start_date = date("Y-m-01");
				$month_end = date("Y-m-t"); 
				$end_date = date('Y-m-d', strtotime($month_end . ' +1 day'));
			}

			if($filter=='Three_month')
			{
				$end_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 day'));
				$start_date = date('Y-m-d',strtotime('- 3 month', strtotime($end_date)));
			}	

			$filter = 'AND from_unixtime(CreatedTime) BETWEEN "'.$start_date.'" AND "'.$end_date.'"';	
	    }
	    else if(!empty($start) && !empty($end))
		{
			$filter = 'AND from_unixtime(CreatedTime) BETWEEN "'.$start.'" AND "'.$end.'"';	
		}
	    else
	    {
	    	$filter = '';
	    }	
			
		$records = $this->db->query("SELECT * FROM `user` where autosave_status!='1' and crm_name!='' and firm_id = '".$_SESSION['firm_id']."' AND user_type IN ('FU')  ".$filter." order by id ASC")->result_array();

		return $records;
	}

	public function summary_task_detailsWithDate($start,$end,$filter,$client)
	{
		if(!empty($filter) && $filter != 'all')
		{					
			if($filter=='week')
			{
				$current_date = date('Y-m-d');
				$week = date('W', strtotime($current_date));
				$year = date('Y', strtotime($current_date));

				$dto = new DateTime();
				$start_date = $dto->setISODate($year, $week, 0)->format('Y-m-d');
				$end_date = $dto->setISODate($year, $week, 6)->format('Y-m-d');
				$end_date = date('Y-m-d', strtotime($end_date . ' +1 day')); 
			}
            else if($filter=='three_week')
            {
				$current_date = date('Y-m-d');
				$weeks = strtotime('- 3 week', strtotime( $current_date ));
				$start_date = date('Y-m-d', $weeks); 
				$addweeks = strtotime('+ 3 week', strtotime( $start_date ));
				$end_Week = date('Y-m-d', $addweeks); 
				$end_date = date('Y-m-d', strtotime($end_Week . ' +1 day'));
			}

			else if($filter=='month')
			{
				$start_date = date("Y-m-01");
				$month_end = date("Y-m-t"); 
				$end_date = date('Y-m-d', strtotime($month_end . ' +1 day'));
			}

			if($filter=='Three_month')
			{
				$end_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 day'));
				$start_date = date('Y-m-d',strtotime('- 3 month', strtotime($end_date)));
			}	

			$filter = 'AND (start_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" OR end_date BETWEEN "'.$start_date.'" AND "'.$end_date.'")';	
	    }
	    else if(!empty($start) && !empty($end))
		{
			$filter = 'AND (start_date BETWEEN "'.$start.'" AND "'.$end.'" OR end_date BETWEEN "'.$start.'" AND "'.$end.'")';	
		}
	    else
	    {
	    	$filter = '';
	    }	

	    if(!empty($client))
		{
           $rec = $this->selectRecord('client','id', $client);
           $client_cond = 'AND user_id = "'.$rec['user_id'].'"';
		}
		else
		{
		   $client_cond = '';
		}
	    
		$this->load->helper('comman');

		$Assigned_Task = Get_Assigned_Datas('TASK');
		$assigned_task = implode(',',$Assigned_Task);	 

        $records = $this->db->query("SELECT * from add_new_task where firm_id = '".$_SESSION['firm_id']."' AND FIND_IN_SET(id,'".$assigned_task."') AND task_status = '5'  ".$client_cond."  ".$filter." ORDER BY id ASC")->result_array();

        return $records;
	}

	public function summary_lead_detailsWithDate($start,$end,$filter,$client)
	{
		if(!empty($filter) && $filter != 'all')
		{					
			if($filter=='week')
			{
				$current_date = date('Y-m-d');
				$week = date('W', strtotime($current_date));
				$year = date('Y', strtotime($current_date));

				$dto = new DateTime();
				$start_date = $dto->setISODate($year, $week, 0)->format('Y-m-d');
				$end_date = $dto->setISODate($year, $week, 6)->format('Y-m-d');
				$end_date = date('Y-m-d', strtotime($end_date . ' +1 day')); 
			}
            else if($filter=='three_week')
            {
				$current_date = date('Y-m-d');
				$weeks = strtotime('- 3 week', strtotime( $current_date ));
				$start_date = date('Y-m-d', $weeks); 
				$addweeks = strtotime('+ 3 week', strtotime( $start_date ));
				$end_Week = date('Y-m-d', $addweeks); 
				$end_date = date('Y-m-d', strtotime($end_Week . ' +1 day'));
			}

			else if($filter=='month')
			{
				$start_date = date("Y-m-01");
				$month_end = date("Y-m-t"); 
				$end_date = date('Y-m-d', strtotime($month_end . ' +1 day'));
			}

			if($filter=='Three_month')
			{
				$end_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 day'));
				$start_date = date('Y-m-d',strtotime('- 3 month', strtotime($end_date)));
			}	

			$filter = 'AND from_unixtime(createdTime) BETWEEN "'.$start_date.'" AND "'.$end_date.'"';	
	    }
	    else if(!empty($start) && !empty($end))
		{
			$filter = 'AND from_unixtime(createdTime) BETWEEN "'.$start.'" AND "'.$end.'"';	
		}
	    else
	    {
	    	$filter = '';
	    }	

	    if(!empty($client))
		{
           $rec = $this->selectRecord('client','id', $client);
           $client_cond = 'AND convert_client_user_id = "'.$rec['user_id'].'"';
		}
		else
		{
		   $client_cond = '';
		}

		$this->load->helper('comman');

		$Assigned_Leads = Get_Assigned_Datas('LEADS');
		$assigned_leads = implode(',',$Assigned_Leads);

		$records = $this->db->query('SELECT * from leads WHERE FIND_IN_SET(id,"'.$assigned_leads.'") AND firm_id = "'.$_SESSION['firm_id'].'" '.$filter.' '.$client_cond.' ORDER BY id ASC')->result_array();

		return $records;
	}

	public function invoice_details(){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		if($count['0']!=''){
	    $query=$this->db->query("SELECT client_email,invoice_no,client_id,amounts_type,created_at FROM Invoice_details WHERE client_email in (".$im_val.") order by client_id desc ");  
		$results = $query->result_array();
	     }else{
	     	$results=0;
	     }
		return $results;
	}

	public function expired_count($last_Year_start,$last_month_end){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		if($count['0']!=''){
			$results=$this->db->query("select count(*) as invoice_count from Invoice_details where client_email in (".$im_val.") and invoice_duedate='Expired' and from_unixtime(created_at) BETWEEN '".$last_Year_start."' AND '".$last_month_end."'")->row_array();
		}else{
	     	$results=0;
	     }
		return $results;
	}

	public function others_count($last_Year_start,$last_month_end,$status){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		if($count['0']!=''){
			$records=$this->db->query("select client_id from Invoice_details where client_email in (".$im_val.")  and from_unixtime(created_at) BETWEEN '".$last_Year_start."' AND '".$last_month_end."'")->result_array();
			$val=array();
			foreach($records as $rec){
				array_push($val, $rec['client_id']);
			}
			$clients_id=implode(',',$val);
			//print_r($clients_id);
			$results=$this->db->query("select count(*) as invoice_count from amount_details where client_id in (".$clients_id.") and payment_status='".$status."'")->row_array();
			}else{
	     	$results=0;
	     }
		return $results;
	}


	public function invoice_details_check(){
		 $last_Year_start=$_SESSION['fromdate'];
         $last_month_end=$_SESSION['todate'];
         $status=$_SESSION['status'];
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		if($count['0']!=''){
	    $query=$this->db->query("SELECT client_email,invoice_no,client_id,amounts_type,created_at FROM Invoice_details WHERE client_email in (".$im_val.") and from_unixtime(created_at) BETWEEN '".$last_Year_start."' AND '".$last_month_end."' and invoice_duedate='Expired' order by client_id desc ");  
		$results = $query->result_array();
	     }else{
	     	$results=0;
	     }
		return $results;
	}

		public function others_details($last_Year_start,$last_month_end,$status){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		if($count['0']!=''){
			$results=$this->db->query("select client_email,invoice_no,client_id,amounts_type,created_at from Invoice_details where client_email in (".$im_val.")  and from_unixtime(created_at) BETWEEN '".$last_Year_start."' AND '".$last_month_end."'")->result_array();
			//$val=array();
			// foreach($records as $rec){
			// 	array_push($val, $rec['client_id']);
			// }
			//$clients_id=implode(',',$val);
			// $results=$this->db->query("select count(*) as invoice_count from amount_details where client_id in (".$clients_id.") and payment_status='".$status."'")->result_array();
			}else{
	     	$results=0;
	     }
		return $results;
	}



	public function exp_count(){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		if($count['0']!=''){
			$results=$this->db->query("select count(*) as invoice_count from Invoice_details where client_email in (".$im_val.") and invoice_duedate='Expired'")->row_array();
		}else{
	     	$results=0;
	     }
		return $results;
	}

	public function oth_count($status){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		if($count['0']!=''){
			$records=$this->db->query("select client_id from Invoice_details where client_email in (".$im_val.")")->result_array();
			$val=array();
			foreach($records as $rec){
				array_push($val, $rec['client_id']);
			}
			$clients_id=implode(',',$val);
			//print_r($clients_id);
			/** 03-08-2018 rs**/
			if($clients_id=='')
			{
				$clients_id=0;
			}
			/** end 03-08-2018 **/
			$results=$this->db->query("select count(*) as invoice_count from amount_details where client_id in (".$clients_id.") and payment_status='".$status."'")->row_array();
			}else{
	     	$results=0;
	     }
		return $results;
	}

	public function proposal_details_section()
	{
		$last_month_end=date('Y-m-t');
		$last_Year_start=date('Y-m-01');
		$query=$this->db->query("select proposal_name,created_at from proposals where user_id='".$_SESSION['id']."' and created_at BETWEEN '".$last_Year_start."' AND '".$last_month_end."'")->result_array();
		return $query;
	}

	

	public function leads_status($id){
		$query=$this->db->query("select status_name from leads_status where id='".$id."'")->row_array();
		return $query['status_name'];
	}


	public function email_find($email){
		$result=$this->db->query("select crm_company_name,crm_email from client where user_id='".$email."'")->row_array();
		return $result;
	}


	public function client_custom_list(){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		if($count['0']!=''){
	    $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (".$im_val.") order by id desc ");  
		$results = $query->result_array();
	     }else{
	     	$results=0;
	     }
		return $results;
	}


	 public function Client_reports($id){
//echo $id;

	    $task_data=$this->db->query("select * from reports_schedule where id=".$id."")->row_array(); 
	    // echo '<pre>';
	    // print_r($task_data);
	    $task_aft_msg=$task_data['message'];
	    $weekly_on_date=date('Y-m-d',strtotime($task_data['schedule_on_date']));
	    $weekly_ends=$task_data['ends'];
	    $repeats=$task_data['repeats'];
	    $monthly_date=$task_data['monthly_date'];
	    $every_week=$task_data['every_week'];
	     $recuring_week=$task_data['every_week'];
	    $recuring_times=$task_data['schedule_times'];
	    $days=$task_data['days'];
	    $ends=$task_data['ends'];
	    $email=$task_data['emails'];
	    $users=$task_data['users'];
	    $groups=$task_data['groups'];  
	    $schedule_start_date=$task_data['schedule_start_date'];
	    $task_after_msg=$task_data['schedule_send_count'];
	    $schedule_send_count=$task_data['schedule_send_count'];
	    $recuring_on_off=$task_data['schedule_reports'];
	    $recuring_task_count=$task_data['schedule_send_count'];
	    $task_id=$task_data['id'];
	    $every_date=$monthly_date.'-'.date('m-Y');
	    $reports_type=$task_data['reports_type'];
	    $new_recuring_start_time=$task_data['schedule_start_date'];
    	$user_id=$task_data['user_id'];
  		//$new_recuring_start_time;

	   	 /*** time round off **/

	     $send_time='10:00 am';
	     $time = strtotime($send_time);
	     $round = 5*60;
	     $rounded = round($time / $round) * $round;
	     $send_time=date("h:i A", $rounded);
	    /** end of time round off **/
	     $status=$task_data['repeats'];
	     $task_days=explode(',',$task_data['days']);

		    if($status!='month'){
		       if($new_recuring_start_time!='')
		       {    
		          $timestamp=strtotime($new_recuring_start_time);
		       }
		       else
		       {
		        $timestamp=strtotime($schedule_start_date);
		       }
		    }
		    else
		    {
		      if($new_recuring_start_time!='')
		      {
		        $timestamp=strtotime($new_recuring_start_time);
		      }
		      else
		      {       
		        $timestamp=strtotime($every_date);
		      }

		    }
			/** for get current date time **/
			$dateTime = new DateTime('now', new DateTimeZone(date_default_timezone_get()));   
			$zz= $dateTime->format("h:i A"); 
			/** end of current date time **/
			$current_date=date('Y-m-d');
			$current_datetimestamp=strtotime(date('Y-m-d'));
			$current_date_week = date('W', $current_datetimestamp);
			$day_of_the_week = date('W', $timestamp);	 


			echo  $day_of_the_week.'---'.$current_date_week;
   			 if(($current_date_week==$day_of_the_week))
			{
				if($status=="week"){		
					$first_day_of_the_week = 'Monday';
					$start_of_the_week     = strtotime("Last $first_day_of_the_week");
					if ( strtolower(date('l')) === strtolower($first_day_of_the_week) )
					{
						$start_of_the_week = strtotime('today');
					}
						$dates = array();
						$mon = date('Y-m-d',$start_of_the_week);
						$tue = date('Y-m-d',strtotime(date("Y-m-d",$start_of_the_week)." +1 days"));
						$wed = date('Y-m-d',strtotime(date("Y-m-d",$start_of_the_week)." +2 days"));
						$thu = date('Y-m-d',strtotime(date("Y-m-d",$start_of_the_week)." +3 days"));
						$fri = date('Y-m-d',strtotime(date("Y-m-d",$start_of_the_week)." +4 days"));
						$sat = date('Y-m-d',strtotime(date("Y-m-d",$start_of_the_week)." +5 days"));
						$sun = date('Y-m-d',strtotime(date("Y-m-d",$start_of_the_week)." +6 days"));
						$dates=array( 'Monday' =>$mon,
						'Tuesday' => $tue,
						'Wednesday' => $wed,
						'Thursday' => $thu,
						'Friday' => $fri,
						'Saturday' => $sat,
						'Sunday' => $sun);					
					
						if($weekly_ends=='after')
						{
						
							if((int)$task_aft_msg==(int)$recuring_task_count)
							{

								$data_on['schedule_reports']='off';
								
								$this->Common_mdl->update('reports_schedule',$data_on,'id',$task_id);
								$recuring_on_off='off';
							}

						}
						else if($weekly_ends=='on')
						{
						/** for weekly ends update **/

						/** end of weekly ends **/
							if($current_date==$weekly_on_date)
							{
								if($zz='11:50 PM'){ // WRK at the end of the time
									$data_on['schedule_reports']='off';							
								    $this->Common_mdl->update('reports_schedule',$data_on,'id',$task_id);
									$recuring_on_off='off';
								}
							}

						}
					/** end of recuring **/
					//  $this->Common_mdl->update('add_new_task',$data,'id',$task_id);
					if(($current_date==$sun) && ($recuring_on_off!="off")){
					//echo date("Y-m-d", strtotime("+ week"));
					if($zz='11:50 PM'){ // WRK at the end of the time

					//$data['schedule_send_count']=(int)$schedule_send_count+1;
						
					}
					}
					/**end of recuring time **/
					$for_dates=array_values($dates);
					$for_days=array_keys($dates);
					$res=array_keys($dates);
					$res1=array_values($dates);	
					echo '<pre>';
					print_r($for_dates);
							print_r($for_days);

		print_r($res);

		print_r($res1);



					$tst_date = array_search($current_date, $res1);
					if ($tst_date !== false) {	

					echo "had";		

					echo '<pre>';
					print_r($res[$tst_date]);
					print_r($task_days);			
						$tst_day = array_search($res[$tst_date], $task_days);

						echo "123456";
					
							if ($tst_day !== false) {   
echo "789456";

							echo  $recuring_week .'---' . $schedule_send_count; 
							echo '<br>';           
							
								if(($recuring_on_off!="off") && ($recuring_times>$schedule_send_count))
								{										
									
									echo "had1";
									/* Reports Send Function */

									//if($reports_type=='client'){
								//	$this->report_pdf($users,$groups,$email,$user_id);
									if($reports_type=='client'){
										$this->report_pdf($users,$groups,$email,$user_id);
								}
								if($reports_type=='task'){
										$this->task_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='proposal'){

										$this->proposal_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='leads'){

										$this->leads_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='service'){

										$this->service_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='deadline'){

										$this->deadline_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='invoice'){

										$this->invoice_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='summary'){

										$this->summary_pdf($users,$groups,$email,$user_id);
									}

									if($reports_type=='timesheet'){

										$this->timesheet_pdf($users,$groups,$email,$user_id);
									}

								//	}


									$data['schedule_start_date']=date("Y-m-d", strtotime("+".$recuring_week." week"));							
									$this->Common_mdl->update('reports_schedule',$data,'id',$task_id);
									/* Reports Send Function End */
									$datas['schedule_send_count']=(int)$schedule_send_count+1;									
									$this->Common_mdl->update('reports_schedule',$datas,'id',$task_id);
								}

							}
					}else{
						echo "not had ";
					}
				}else if($status=='day'){

					$for_dates=array();
					array_push($for_dates,date('Y-m-d',$timestamp));
					for($i=1;$i<(int)$recuring_week;$i++)
					{
					$for_date_my=date('Y-m-d',strtotime(date('Y-m-d',$timestamp)." +".$i." days"));
					array_push($for_dates, $for_date_my);
					}
					echo end($for_dates);
					if($weekly_ends=='after')
					{
					//$task_aft_msg
						if((int)$task_aft_msg==(int)$recuring_task_count)
						{
						$data_on['schedule_reports']='off';
						
						  $this->Common_mdl->update('reports_schedule',$data_on,'id',$task_id);
						}

					}
					else if($weekly_ends=='on')
					{
					/** for weekly ends update **/
					/** end of weekly ends **/
						if($current_date==$weekly_on_date)
						{
							if($zz='11:50 PM'){ // WRK at the end of the time
							$data_on['schedule_reports']='off';
							$this->Common_mdl->update('reports_schedule',$data_on,'id',$task_id);
							$recuring_on_off='off';
							}
						}

					}

					if($recuring_on_off!='off'){ // for rec off
						if(in_array($current_date, $for_dates)){
							/* Reports Send Function */
							//	if($reports_type=='client'){
									if($reports_type=='client'){
										$this->report_pdf($users,$groups,$email,$user_id);
								}
								if($reports_type=='task'){
										$this->task_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='proposal'){

										$this->proposal_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='leads'){

										$this->leads_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='service'){

										$this->service_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='deadline'){

										$this->deadline_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='invoice'){

										$this->invoice_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='summary'){

										$this->summary_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='timesheet'){

										$this->timesheet_pdf($users,$groups,$email,$user_id);
									}



								// if($reports_type=='task'){
								// $this->task_pdf($users,$groups,$email,$user_id);
								// }

							$data['schedule_start_date']=date("Y-m-d", strtotime("+".$recuring_week." days"));
							$this->Common_mdl->update('reports_schedule',$data,'id',$task_id);

							/* Reports Send Function End */
							$datas['schedule_send_count']=(int)$schedule_send_count+1;						
							$this->Common_mdl->update('reports_schedule',$datas,'id',$task_id);
						}

					if($current_date==end($for_dates))
					{
						// if($zz='11:50 PM'){ // WRK at the end of the time
						
							
						// }
					}
					} // rec off
				}else if($status=='month') // for day
				{
					if($weekly_ends=='after')
					{
					//$task_aft_msg
						if((int)$task_aft_msg==(int)$recuring_task_count)
						{
							$data_on['recuring_on_off']='off';
							$this->Common_mdl->update('add_new_task',$data_on,'id',$task_id);
							$recuring_on_off='off';
						}

					}
					else if($weekly_ends=='on')
					{
					/** for weekly ends update **/
					/** end of weekly ends **/
						if($current_date==$weekly_on_date)
						{
							if($zz='11:50 PM'){ // WRK at the end of the time
								$data_on['schedule_reports']='off';				
							    $this->Common_mdl->update('reports_schedule',$data_on,'id',$task_id);
							}
						}

					}

				if($recuring_on_off!='off'){ // for rec off
					//if(in_array($current_date, $for_dates)){
					$for_every_date=$every_date;
					$timestamp_new='';
					if($new_recuring_start_time!='')
					{
					//echo "sd";
					$timestamp_new=$new_recuring_start_time;
					}
					else
					{
					//echo "sdas";
					$timestamp_new=date('Y-m-d',strtotime($every_date));
					}				
					if(date('Y-m-d')==$timestamp_new){

						/* Reports Send Function */
							//if($reports_type=='client'){
							 	if($reports_type=='client'){
										$this->report_pdf($users,$groups,$email,$user_id);
								}
								if($reports_type=='task'){
										$this->task_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='proposal'){

										$this->proposal_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='leads'){

										$this->leads_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='service'){

										$this->service_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='deadline'){

										$this->deadline_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='invoice'){

										$this->invoice_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='summary'){

										$this->summary_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='timesheet'){

										$this->timesheet_pdf($users,$groups,$email,$user_id);
									}



							// if($reports_type=='task'){
							//    $this->task_pdf($users,$groups,$email,$user_id);
							// }

						$data['schedule_start_date']=date("Y-m-d", strtotime("+".$recuring_week." months"));

						$this->Common_mdl->update('reports_schedule',$data,'id',$task_id);
						/* Reports Send Function End */
						$datas['schedule_send_count']=(int)$schedule_send_count+1;

						$this->Common_mdl->update('reports_schedule',$datas,'id',$task_id);

					}				
				}

						/** end of monthly **/
						/** for year **/
				}else if($status=='year'){
						if($weekly_ends=='after')
						{
						//$task_aft_msg
							if((int)$task_aft_msg==(int)$recuring_task_count)
							{
								$data_on['schedule_reports']='off';
								
								$this->Common_mdl->update('reports_schedule',$data_on,'id',$task_id);
							}

						}
						else if($weekly_ends=='on')
						{
						/** for weekly ends update **/
						/** end of weekly ends **/
							if($current_date==$weekly_on_date)
							{
								if($zz='11:50 PM'){ // WRK at the end of the time
									$data_on['schedule_reports']='off';							
								    $this->Common_mdl->update('reports_schedule',$data_on,'id',$task_id);
									$recuring_on_off='off';
								}
							}

						}

						if($recuring_on_off!='off'){ // for rec off

							/* Reports Send Function End */
								if($reports_type=='client'){
										$this->report_pdf($users,$groups,$email,$user_id);
								}
								if($reports_type=='task'){
										$this->task_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='proposal'){

										$this->proposal_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='leads'){

										$this->leads_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='service'){

										$this->service_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='deadline'){

										$this->deadline_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='invoice'){

										$this->invoice_pdf($users,$groups,$email,$user_id);
									}
									if($reports_type=='summary'){

										$this->summary_pdf($users,$groups,$email,$user_id);
									}


								// if($reports_type=='task'){
								// 		$this->task_pdf($users,$groups,$email,$user_id);
								// }

							$data['schedule_start_date']=date("Y-m-d", strtotime("+".$recuring_week." years"));

							$this->Common_mdl->update('reports_schedule',$data,'id',$task_id);

							$datas['schedule_send_count']=(int)$schedule_send_count+1;	

							$this->Common_mdl->update('reports_schedule',$datas,'id',$task_id); 					
						}

				}   

    }

/** for monthly **/
} 




 function report_pdf($users,$groups,$email,$user_id){


$emails=array();
$email=explode(',',$email);
$emails=$email;

$users=explode(',',$users);
	for($i=0;$i<count($users);$i++){

		$user_email=$this->db->query("select crm_email_id from user where id='".$users[$i]."'")->row_array();
		array_push($emails,$user_email['crm_email_id']);
	}

$group_email=$this->db->query("select * from Groups where id='".$groups."'")->row_array();
$groups=explode(',',$group_email['users']);

	for($i=0;$i<count($groups);$i++){
			$groups_email=$this->db->query("select crm_email_id from user where id='".$groups[$i]."'")->row_array();
			array_push($emails,$groups_email['crm_email_id']);
	}
$emails=implode(',',$emails);
$email=explode(',',$emails);


    $data['records']=$this->client_custom_list2($user_id);  
	$this->load->library('Tc');
	$pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
	$pdf->AddPage('P','A4');
	$html = $this->load->view('reports/Client_report_pdf',$data,true);      
	$pdf->SetTitle('Client list');
	$pdf->SetHeaderMargin(30);
	$pdf->SetTopMargin(20);
	$pdf->setFooterMargin(20);
	$pdf->SetAutoPageBreak(true, 20);
	$pdf->SetAuthor('Author');
	$pdf->SetDisplayMode('real', 'default');
	$pdf->WriteHTML($html);          
	$dir = 'uploads/reports_pdf/';
	$timestamp = date("Y-m-d_H:i:s");
	$filename='Client'.'_'.$timestamp.'.pdf';       
	$pdf->Output(FCPATH . $dir . $filename, 'F');
	  $company=$this->db->query("SELECT * FROM `admin_setting` where user_id='".$user_id."'")->row_array();
		for($i=0;$i<count($email);$i++){ 
			
		    $body['body']			= '<p>Hai Users</p>';
		    $body['attachment'][]   = FCPATH.'uploads/reports_pdf/'.$filename;
			$email_subject          = 'Reports from '.$company['company_name'];

			$send = firm_settings_send_mail( $company['firm_id'] , $email[$i] , strip_tags($email_subject) , $body );
            $send = $send['result'];
		}

	}


	 function task_pdf($users,$groups,$email,$user_id){

$emails=array();
$email=explode(',',$email);
$emails=$email;

$users=explode(',',$users);
	for($i=0;$i<count($users);$i++){

		$user_email=$this->db->query("select crm_email_id from user where id='".$users[$i]."'")->row_array();
		array_push($emails,$user_email['crm_email_id']);
	}

$group_email=$this->db->query("select * from Groups where id='".$groups."'")->row_array();
$groups=explode(',',$group_email['users']);

	for($i=0;$i<count($groups);$i++){
			$groups_email=$this->db->query("select crm_email_id from user where id='".$groups[$i]."'")->row_array();
			array_push($emails,$groups_email['crm_email_id']);
	}
$emails=implode(',',$emails);
$email=explode(',',$emails);
	//$email=explode(',',$email);

    $data['records']=$this->run_task_list2($user_id);  
	$this->load->library('Tc');
	$pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
	$pdf->AddPage('P','A4');
	$html = $this->load->view('reports/task_report_pdf',$data,true);      
	$pdf->SetTitle('Task list');
	$pdf->SetHeaderMargin(30);
	$pdf->SetTopMargin(20);
	$pdf->setFooterMargin(20);
	$pdf->SetAutoPageBreak(true, 20);
	$pdf->SetAuthor('Author');
	$pdf->SetDisplayMode('real', 'default');
	$pdf->WriteHTML($html);          
	$dir = 'uploads/reports_pdf/';
	$timestamp = date("Y-m-d_H:i:s");
	$filename='Task'.'_'.$timestamp.'.pdf';       
	$pdf->Output(FCPATH . $dir . $filename, 'F');
		
		$company=$this->db->query("SELECT * FROM `admin_setting` where user_id='".$user_id."'")->row_array();
		for($i=0;$i<count($email);$i++){ 
			//$body[];
		    $body['body']			= '<p>Hai Users</p>';
		    $body['attachment'][]   = FCPATH.'uploads/reports_pdf/'.$filename;
			$email_subject          = 'Reports from '.$company['company_name'];

			$send = firm_settings_send_mail( $company['firm_id'] , $email[$i] , strip_tags($email_subject) , $body );
            $send = $send['result'];
		}

	}



	function proposal_pdf($users,$groups,$email,$user_id){

		$emails=array();
$email=explode(',',$email);
$emails=$email;

$users=explode(',',$users);
	for($i=0;$i<count($users);$i++){

		$user_email=$this->db->query("select crm_email_id from user where id='".$users[$i]."'")->row_array();
		array_push($emails,$user_email['crm_email_id']);
	}

$group_email=$this->db->query("select * from Groups where id='".$groups."'")->row_array();
$groups=explode(',',$group_email['users']);

	for($i=0;$i<count($groups);$i++){
			$groups_email=$this->db->query("select crm_email_id from user where id='".$groups[$i]."'")->row_array();
			array_push($emails,$groups_email['crm_email_id']);
	}
$emails=implode(',',$emails);
$email=explode(',',$emails);

	//$email=explode(',',$email);
    $data['records']= $data['records']=$this->db->query("select * from proposals where user_id='".$user_id."'")->result_array();
	$this->load->library('Tc');
	$pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
	$pdf->AddPage('P','A4');
	$html = $this->load->view('reports/proposal_report_pdf',$data,true);      
	$pdf->SetTitle('Proposal list');
	$pdf->SetHeaderMargin(30);
	$pdf->SetTopMargin(20);
	$pdf->setFooterMargin(20);
	$pdf->SetAutoPageBreak(true, 20);
	$pdf->SetAuthor('Author');
	$pdf->SetDisplayMode('real', 'default');
	$pdf->WriteHTML($html);          
	$dir = 'uploads/reports_pdf/';
	$timestamp = date("Y-m-d_H:i:s");
	$filename='Proposal'.'_'.$timestamp.'.pdf';       
	$pdf->Output(FCPATH . $dir . $filename, 'F');
		$company=$this->db->query("SELECT * FROM `admin_setting` where user_id='".$user_id."'")->row_array();
		for($i=0;$i<count($email);$i++){ 
			//$body[];
		    $body['body']			= '<p>Hai Users</p>';
		    $body['attachment'][]   = FCPATH.'uploads/reports_pdf/'.$filename;
			$email_subject          = 'Reports from '.$company['company_name'];

			$send = firm_settings_send_mail( $company['firm_id'] , $email[$i] , strip_tags($email_subject) , $body );
            $send = $send['result'];
		}

	}




	function leads_pdf($users,$groups,$email,$user_id){

		$emails=array();
$email=explode(',',$email);
$emails=$email;

$users=explode(',',$users);
	for($i=0;$i<count($users);$i++){

		$user_email=$this->db->query("select crm_email_id from user where id='".$users[$i]."'")->row_array();
		array_push($emails,$user_email['crm_email_id']);
	}

$group_email=$this->db->query("select * from Groups where id='".$groups."'")->row_array();
$groups=explode(',',$group_email['users']);

	for($i=0;$i<count($groups);$i++){
			$groups_email=$this->db->query("select crm_email_id from user where id='".$groups[$i]."'")->row_array();
			array_push($emails,$groups_email['crm_email_id']);
	}
$emails=implode(',',$emails);
$email=explode(',',$emails);

	//$email=explode(',',$email);
    $data['records']= $this->db->query("select * from leads where user_id='".$user_id."'")->result_array();
	$this->load->library('Tc');
	$pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
	$pdf->AddPage('P','A4');
	$html = $this->load->view('reports/leads_report_pdf',$data,true);      
	$pdf->SetTitle('Leads list');
	$pdf->SetHeaderMargin(30);
	$pdf->SetTopMargin(20);
	$pdf->setFooterMargin(20);
	$pdf->SetAutoPageBreak(true, 20);
	$pdf->SetAuthor('Author');
	$pdf->SetDisplayMode('real', 'default');
	$pdf->WriteHTML($html);          
	$dir = 'uploads/reports_pdf/';
	$timestamp = date("Y-m-d_H:i:s");
	$filename='Leads'.'_'.$timestamp.'.pdf';       
	$pdf->Output(FCPATH . $dir . $filename, 'F');
		$company=$this->db->query("SELECT * FROM `admin_setting` where user_id='".$user_id."'")->row_array();
		for($i=0;$i<count($email);$i++){ 
			//$body[];
		    $body['body']			= '<p>Hai Users</p>';
		    $body['attachment'][]   = FCPATH.'uploads/reports_pdf/'.$filename;
			$email_subject          = 'Reports from '.$company['company_name'];

			$send = firm_settings_send_mail( $company['firm_id'] , $email[$i] , strip_tags($email_subject) , $body );
            $send = $send['result'];
		}

	}


	function service_pdf($users,$groups,$email,$user_id){

		$emails=array();
$email=explode(',',$email);
$emails=$email;

$users=explode(',',$users);
	for($i=0;$i<count($users);$i++){

		$user_email=$this->db->query("select crm_email_id from user where id='".$users[$i]."'")->row_array();
		array_push($emails,$user_email['crm_email_id']);
	}

$group_email=$this->db->query("select * from Groups where id='".$groups."'")->row_array();
$groups=explode(',',$group_email['users']);

	for($i=0;$i<count($groups);$i++){
			$groups_email=$this->db->query("select crm_email_id from user where id='".$groups[$i]."'")->row_array();
			array_push($emails,$groups_email['crm_email_id']);
	}
$emails=implode(',',$emails);
$email=explode(',',$emails);

	//$email=explode(',',$email);
   $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$user_id." and role = 4 and crm_name !=''")->row_array();
             $us_id =  explode(',', $sql['us_id']);
    
   $data['records']=$this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and ( crm_investigation_end_date!='' or  crm_registered_renew_date!='' or crm_insurance_renew_date!='' or  crm_next_booking_date!='' or crm_next_manage_acc_date!='' or crm_next_p11d_return_due!='' and crm_pension_subm_due_date!='' or crm_rti_deadline!='' or crm_vat_due_date!='' or crm_personal_due_date_return!='' or  crm_accounts_tax_date_hmrc!='' or crm_ch_accounts_next_due!='' or crm_confirmation_statement_due_date!='') ")->result_array();

	$this->load->library('Tc');
	$pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
	$pdf->AddPage('P','A4');
	$html = $this->load->view('reports/service_report_pdf',$data,true);      
	$pdf->SetTitle('Service list');
	$pdf->SetHeaderMargin(30);
	$pdf->SetTopMargin(20);
	$pdf->setFooterMargin(20);
	$pdf->SetAutoPageBreak(true, 20);
	$pdf->SetAuthor('Author');
	$pdf->SetDisplayMode('real', 'default');
	$pdf->WriteHTML($html);          
	$dir = 'uploads/reports_pdf/';
	$timestamp = date("Y-m-d_H:i:s");
	$filename='Service'.'_'.$timestamp.'.pdf';       
	$pdf->Output(FCPATH . $dir . $filename, 'F');
		$company=$this->db->query("SELECT * FROM `admin_setting` where user_id='".$user_id."'")->row_array();
		for($i=0;$i<count($email);$i++){ 
			//$body[];
		    $body['body']			= '<p>Hai Users</p>';
		    $body['attachment'][]   = FCPATH.'uploads/reports_pdf/'.$filename;
			$email_subject          = 'Reports from '.$company['company_name'];

			$send = firm_settings_send_mail( $company['firm_id'] , $email[$i] , strip_tags($email_subject) , $body );
            $send = $send['result'];
		}

	}

		function deadline_pdf($users,$groups,$email,$user_id){

		$emails=array();
		$email=explode(',',$email);
		$emails=$email;

		$users=explode(',',$users);
		for($i=0;$i<count($users);$i++){

		$user_email=$this->db->query("select crm_email_id from user where id='".$users[$i]."'")->row_array();
		array_push($emails,$user_email['crm_email_id']);
		}

		$group_email=$this->db->query("select * from Groups where id='".$groups."'")->row_array();
		$groups=explode(',',$group_email['users']);

		for($i=0;$i<count($groups);$i++){
		$groups_email=$this->db->query("select crm_email_id from user where id='".$groups[$i]."'")->row_array();
		array_push($emails,$groups_email['crm_email_id']);
		}
		$emails=implode(',',$emails);
		$email=explode(',',$emails);
    	//$email=explode(',',$email);
		$sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$user_id." and role = 4 and crm_name !=''")->row_array();
		$us_id =  explode(',', $sql['us_id']);

		$data['records']=$this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and ( crm_investigation_end_date!='' or  crm_registered_renew_date!='' or crm_insurance_renew_date!='' or  crm_next_booking_date!='' or crm_next_manage_acc_date!='' or crm_next_p11d_return_due!='' and crm_pension_subm_due_date!='' or crm_rti_deadline!='' or crm_vat_due_date!='' or crm_personal_due_date_return!='' or  crm_accounts_tax_date_hmrc!='' or crm_ch_accounts_next_due!='' or crm_confirmation_statement_due_date!='') ")->result_array();

		$this->load->library('Tc');
		$pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
		$pdf->AddPage('P','A4');
		$html = $this->load->view('reports/deadline_report_pdf',$data,true);      
		$pdf->SetTitle('Deadline list');
		$pdf->SetHeaderMargin(30);
		$pdf->SetTopMargin(20);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true, 20);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->WriteHTML($html);          
		$dir = 'uploads/reports_pdf/';
		$timestamp = date("Y-m-d_H:i:s");
		$filename='Deadline'.'_'.$timestamp.'.pdf';       
		$pdf->Output(FCPATH . $dir . $filename, 'F');
		$company=$this->db->query("SELECT * FROM `admin_setting` where user_id='".$user_id."'")->row_array();
		for($i=0;$i<count($email);$i++){ 
			//$body[];
		    $body['body']			= '<p>Hai Users</p>';
		    $body['attachment'][]   = FCPATH.'uploads/reports_pdf/'.$filename;
			$email_subject          = 'Reports from '.$company['company_name'];

			$send = firm_settings_send_mail( $company['firm_id'] , $email[$i] , strip_tags($email_subject) , $body );
            $send = $send['result'];
		}

	}



	function invoice_pdf($users,$groups,$email,$user_id){

	$emails=array();
	$email=explode(',',$email);
	$emails=$email;

	$users=explode(',',$users);
	for($i=0;$i<count($users);$i++){

	$user_email=$this->db->query("select crm_email_id from user where id='".$users[$i]."'")->row_array();
	array_push($emails,$user_email['crm_email_id']);
	}

	$group_email=$this->db->query("select * from Groups where id='".$groups."'")->row_array();
	$groups=explode(',',$group_email['users']);

	for($i=0;$i<count($groups);$i++){
	$groups_email=$this->db->query("select crm_email_id from user where id='".$groups[$i]."'")->row_array();
	array_push($emails,$groups_email['crm_email_id']);
	}
	$emails=implode(',',$emails);
	$email=explode(',',$emails);

	 // $email=explode(',',$email);
      $res=array();  
     // $user_id=$_SESSION['id'];
      $query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");       
      $results1 = $query1->result_array();      
      foreach ($results1 as $key => $value) {
      array_push($res, $value['id']);
      }

      if(isset($_POST['client']) && $_POST['client']!=0){

        $im_val=$_POST['client'];
      }else{
        $im_val=implode(',',$res);  
      }
      //$sql=$this->db->query("select client_id from Invoice_details where client_email in (".$im_val.")")->result_array();
      $sql=$this->db->query("select client_id from Invoice_details where client_email in (".$im_val.")")->result_array();
      $client_id=array();
      foreach ($sql as $key => $value) {
      array_push($client_id, $value['client_id']);
      }
      $client_id=implode(',',$client_id);

      $data['records']=$this->db->query("SELECT * FROM Invoice_details INNER JOIN amount_details ON Invoice_details.client_id = amount_details.client_id WHERE Invoice_details.client_id in (".$client_id.")")->result_array();

      $data['draft']=$this->db->query("select * from invoices where status='draft'")->result_array();



	$this->load->library('Tc');
	$pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
	$pdf->AddPage('P','A4');
	$html = $this->load->view('reports/invoice_report_pdf',$data,true);      
	$pdf->SetTitle('Leads list');
	$pdf->SetHeaderMargin(30);
	$pdf->SetTopMargin(20);
	$pdf->setFooterMargin(20);
	$pdf->SetAutoPageBreak(true, 20);
	$pdf->SetAuthor('Author');
	$pdf->SetDisplayMode('real', 'default');
	$pdf->WriteHTML($html);          
	$dir = 'uploads/reports_pdf/';
	$timestamp = date("Y-m-d_H:i:s");
	$filename='Invoice'.'_'.$timestamp.'.pdf';       
	$pdf->Output(FCPATH . $dir . $filename, 'F');
		$company=$this->db->query("SELECT * FROM `admin_setting` where user_id='".$user_id."'")->row_array();
		for($i=0;$i<count($email);$i++){ 
		//	$body[];
		    $body['body']			= '<p>Hai Users</p>';
		    $body['attachment'][]   = FCPATH.'uploads/reports_pdf/'.$filename;
			$email_subject          = 'Reports from '.$company['company_name'];

			$send = firm_settings_send_mail( $company['firm_id'] , $email[$i] , strip_tags($email_subject) , $body );
            $send = $send['result'];
		}



	}



	function summary_pdf($users,$groups,$email,$user_id){

			$emails=array();
			$email=explode(',',$email);
			$emails=$email;

			$users=explode(',',$users);
			for($i=0;$i<count($users);$i++){

			$user_email=$this->db->query("select crm_email_id from user where id='".$users[$i]."'")->row_array();
			array_push($emails,$user_email['crm_email_id']);
			}

			$group_email=$this->db->query("select * from Groups where id='".$groups."'")->row_array();
			$groups=explode(',',$group_email['users']);

			for($i=0;$i<count($groups);$i++){
			$groups_email=$this->db->query("select crm_email_id from user where id='".$groups[$i]."'")->row_array();
			array_push($emails,$groups_email['crm_email_id']);
			}
			$emails=implode(',',$emails);
			$email=explode(',',$emails);

	//$email=explode(',',$email);
    $data['client_details']=$query1=$this->db->query("SELECT id FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC")->result_array();     
      $data['task_details']=$this->db->query('select id,subject,start_date,end_date,task_status,worker,team,department from add_new_task  where create_by="'.$user_id.'" and task_status="5"')->result_array();  
      $data['lead_details']=$query=$this->db->query('select id,name,company,email_address,assigned,lead_status from leads
        where user_id="'.$user_id.'"')->result_array(); 
      $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');   
	$this->load->library('Tc');
	$pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
	$pdf->AddPage('P','A4');
	$html = $this->load->view('reports/summary_report_pdf',$data,true);      
	$pdf->SetTitle('Leads list');
	$pdf->SetHeaderMargin(30);
	$pdf->SetTopMargin(20);
	$pdf->setFooterMargin(20);
	$pdf->SetAutoPageBreak(true, 20);
	$pdf->SetAuthor('Author');
	$pdf->SetDisplayMode('real', 'default');
	$pdf->WriteHTML($html);          
	$dir = 'uploads/reports_pdf/';
	$timestamp = date("Y-m-d_H:i:s");
	$filename='Summary'.'_'.$timestamp.'.pdf';       
	$pdf->Output(FCPATH . $dir . $filename, 'F');
		$company=$this->db->query("SELECT * FROM `admin_setting` where user_id='".$user_id."'")->row_array();
		for($i=0;$i<count($email);$i++){ 
			//$body[];
		    $body['body']			= '<p>Hai Users</p>';
		    $body['attachment'][]   = FCPATH.'uploads/reports_pdf/'.$filename;
			$email_subject          = 'Reports from '.$company['company_name'];

			$send = firm_settings_send_mail( $company['firm_id'] , $email[$i] , strip_tags($email_subject) , $body );
            $send = $send['result'];
		}

	}


	function timesheet_pdf($users,$groups,$email,$user_id){
			$emails=array();
			$email=explode(',',$email);
			$emails=$email;

			$users=explode(',',$users);
			for($i=0;$i<count($users);$i++){

			$user_email=$this->db->query("select crm_email_id from user where id='".$users[$i]."'")->row_array();
			array_push($emails,$user_email['crm_email_id']);
			}

			$group_email=$this->db->query("select * from Groups where id='".$groups."'")->row_array();
			$groups=explode(',',$group_email['users']);

			for($i=0;$i<count($groups);$i++){
			$groups_email=$this->db->query("select crm_email_id from user where id='".$groups[$i]."'")->row_array();
			array_push($emails,$groups_email['crm_email_id']);
			}
			$emails=implode(',',$emails);
			$email=explode(',',$emails);

	//$email=explode(',',$email);
    $data['task_list']=$this->db->query(" select start_date,end_date,subject from add_new_task where create_by='".$user_id."'")->result_array();
// echo '<pre>';
// print_r($data['task_list']);
// echo '</pre>';
 

	$this->load->library('Tc');
	$pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
	$pdf->AddPage('P','A4');
	$html = $this->load->view('reports/timesheet_report_pdf',$data,true);      
	$pdf->SetTitle('TimeSheet list');
	$pdf->SetHeaderMargin(30);
	$pdf->SetTopMargin(20);
	$pdf->setFooterMargin(20);
	$pdf->SetAutoPageBreak(true, 20);
	$pdf->SetAuthor('Author');
	$pdf->SetDisplayMode('real', 'default');
	$pdf->WriteHTML($html);          
	$dir = 'uploads/reports_pdf/';
	$timestamp = date("Y-m-d_H:i:s");
	$filename='Timesheet'.'_'.$timestamp.'.pdf';       
	$pdf->Output(FCPATH . $dir . $filename, 'F');
		$company=$this->db->query("SELECT * FROM `admin_setting` where user_id='".$user_id."'")->row_array();
		for($i=0;$i<count($email);$i++){ 
			//$body[];
		    $body['body']			= '<p>Hai Users</p>';
		    $body['attachment'][]   = FCPATH.'uploads/reports_pdf/'.$filename;
			$email_subject          = 'Reports from '.$company['company_name'];

			$send = firm_settings_send_mail( $company['firm_id'] , $email[$i] , strip_tags($email_subject) , $body );
            $send = $send['result'];
		}
	}
	function client_custom_list2($user_id){
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		if($count['0']!=''){
	    $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (".$im_val.") order by id desc ");  
		$results = $query->result_array();
	     }else{
	     	$results=0;
	     }
		return $results;
	}

	public function run_task_list2($user_id){
		//$user_id=$_SESSION['id'];
		$query=$this->db->query('select * from add_new_task where create_by="'.$user_id.'"')->result_array();
		//echo $query;
        return $query;
	}

	public function Staff_custom_list(){
		$this->db->query("select * from client")->result_array();
		
	}


}
