<?php
Class Task_creating_model extends CI_Model {

       public function __construct()
    {
        parent::__construct();
       $this->load->model(array('Common_mdl','Security_model','Invoice_model'));
    }
/** end of 30-08-2018 **/
public function client_create_task($user_id)
{
  $company_data=$this->db->query('select * from client where user_id='.$user_id.' ')->row_array();
  $task_ids=$company_data['task_ids'];

  $assignees = $this->db->query("SELECT assignees FROM firm_assignees WHERE module_name = 'CLIENT' AND module_id = '".$user_id."' AND firm_id = '".$company_data['firm_id']."'")->row_array();
  $assignees = $assignees['assignees'];

  if($company_data['crm_company_name']!='')
  {
    $company_name=$company_data['crm_company_name'];
  }
  else
  {
    $company_name=$this->Common_mdl->get_field_value('user','crm_name','id',$user_id);
  }
  $name="Chase for information -".$company_name;
  $task_data['user_id']=$company_data['id'];
  $task_data['lead_id']='';
  $task_data['public']=0;
  $task_data['subject']=$name;
  $data['service_due_date']=date('Y-m-d');
  $task_data['start_date'] = date('Y-m-d');
  $task_data['end_date'] = date('Y-m-d' , strtotime('+10 days') );
  $end_date='';
  $task_data['priority']='low';
  $task_data['related_to']='tasks';
  $task_data['company_name']=$company_data['id'];
  $task_data['worker']='';
  $task_data['team']='';
  $task_data['department']='';
  $task_data['task_status']='notstarted';

  $for_old_values['assign']='';
  $for_old_values['team']='';
  $for_old_values['department']='';
  $for_old_values['manager']='';
  $task_data['for_old_assign']='';
  $task_data['created_date']=time();
  $task_data['create_by']=$_SESSION['id'];
  $task_data['firm_id'] = $company_data['firm_id'];
  
  $this->db->insert('add_new_task',$task_data);
  $in=$this->db->insert_id();
  $data = ['firm_id'=>$company_data['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
  $this->db->insert('firm_assignees',$data);
  $res_task='';
  if($task_ids!=''){ 
    $res_task=$task_ids.",".$in;
     }
  else{ $res_task=$in; }
  $convert_task['task_ids']=$res_task;
  $this->Common_mdl->update('client',$convert_task,'user_id',$user_id);
  
}
    /** end of 30-08-2018 **/


public function maintask_timer($task_list, $user_id=0, $details_page=0, $list_page=0){
  $for_main_task_timer = $data = $hr_explode = $hr_explodes = array();

  foreach ($task_list as $tre_key => $tre_value) {
    // If $user_id value true get the timer value for user when FA or FU  both are Login  //
    // If $user_id value false get the timer value for user when FA  Login get induvidual timer / remaining gets over all task timer  //
    $timer_condition1 = ($user_id==0) ? "AND user_id=".$_SESSION['id']."" : "and  user_id=".$user_id." ";
    $timer_condition2 = ($user_id==0) ? ""                                : "and  user_id=".$user_id." ";
    // If $details_page value true Its function call from task details page  //
    $timer_condition  = ($_SESSION['user_type']=='FU' && $details_page==0) ? $timer_condition1 : $timer_condition2;
    $individual_timer = $this->db->query("select * from individual_task_timer where task_id=".$tre_value['id']." ".$timer_condition." ")->result_array();
    $for_total_time   = 0;

    if(count($individual_timer)>0){
      foreach ($individual_timer as $intime_key => $intime_value) {
        $its_time     = $intime_value['time_start_pause'];
        $res          = explode(',', $its_time);
        $res1         = array_chunk($res,2);
        $result_value = array();
        
        foreach($res1 as $rre_key => $rre_value){
          $abc = $rre_value;
          
          if(count($abc)>1){
            if($abc[1]!=''){
              $ret_val = $this->Common_mdl->calculate_test($abc[0],$abc[1]);
              array_push($result_value, $ret_val) ;
            }else{
              $ret_val   = $this->Common_mdl->calculate_test($abc[0],time());
              array_push($result_value, $ret_val) ;
            }
          }else{
            $ret_val   = $this->Common_mdl->calculate_test($abc[0],time());
            array_push($result_value, $ret_val) ;
          }
        }                                   
        foreach ($result_value as $re_key => $re_value) {          
          $for_total_time += $this->Common_mdl->time_to_sec($re_value);
        }
      }          
      $for_main_task_timer[$tre_value['id']] = $for_total_time;
      $hr_min_sec                            = $this->Common_mdl->sec_to_time($for_total_time);
      $hr_explode                            = explode(':',$hr_min_sec);
      $hr_explodes[$tre_value['id']]         = explode(':',$hr_min_sec);      
    }
  }
  $hr_explode1 = [];

  foreach ($task_list as $key => $value) {    
    $sub_task_id = array_filter(explode(",", $value['sub_task_id']));
    
    if(count($sub_task_id)>0){
      $main_task_timer  = 0;
      $main_task_timer += $for_main_task_timer[$value['id']];
      
      foreach ($sub_task_id as $sk => $sv){
        if(array_key_exists($sv, $for_main_task_timer)){
          $main_task_timer += $for_main_task_timer[$sv];          
        }
      }
      $hr_min_sec = $this->Common_mdl->sec_to_time($main_task_timer);      
      $hr_explode = explode(':', $hr_min_sec);        
      array_push($hr_explode, $value['id']);
    }
    if(isset($hr_explode[3]) && !in_array($hr_explode[3], $hr_explode1)){
      array_push($hr_explode1, $value['id']);
      $data[] = implode(",", $hr_explode);
    }     
  }        
  return $data;    
}

public function array2readable($array, $separator, $last_separator) {                              
  $result = preg_replace(strrev("/$separator/"),strrev($last_separator),strrev(implode($separator, $array)), 1);
  return strrev($result);
}

public function unixTime2text($time) {
  $value['y'] = floor($time/31536000);
  $value['w'] = floor(($time-($value['y']*31536000))/604800);
  $value['d'] = floor(($time-($value['y']*31536000+$value['w']*604800))/86400);
  $value['h'] = floor(($time-($value['y']*31536000+$value['w']*604800+$value['d']*86400))/3600);
  $value['m'] = floor(($time-($value['y']*31536000+$value['w']*604800+$value['d']*86400+$value['h']*3600))/60);
  $value['s'] = $time-($value['y']*31536000+$value['w']*604800+$value['d']*86400+$value['h']*3600+$value['m']*60);
  $unit['y']  = 'year' . ($value['y'] > 1 ? 's' : '');
  $unit['w']  = 'week' . ($value['w'] > 1 ? 's' : '');
  $unit['d']  = 'day' . ($value['d'] > 1 ? 's' : '');
  $unit['h']  = 'hour' . ($value['h'] > 1 ? 's' : '');
  $unit['m']  = 'minute' . ($value['m'] > 1 ? 's' : '');
  $unit['s']  = 'second' . ($value['s'] > 1 ? 's' : '');

  $not_null_values = [];  
  foreach($value as $key => $val) {
      if (!empty($val)) {
          $not_null_values[] .= $val . ' ' . $unit[$key];
      }
  }
  //return $this->array2readable($not_null_values, ', ', ' and ');
  return $this->array2readable($not_null_values, ', ', ', ');
}

public function sec_to_time($sec){
  return sprintf('%02d:%02d:%02d', floor($sec / 3600) , floor($sec / 60 % 60) , floor($sec % 60));
}

public function calculate_diff($a, $b){
  $difference    = $b - $a;
  $second        = 1;
  $minute        = 60 * $second;
  $hour          = 60 * $minute;
  $ans["hour"]   = floor(($difference) / $hour);
  $ans["minute"] = floor((($difference) % $hour) / $minute);
  $ans["second"] = floor(((($difference) % $hour) % $minute) / $second);
  $test          = $ans["hour"] . ":" . $ans["minute"] . ":" . $ans["second"];
  
  return $test;
}

public function time_to_sec($time){
  list($h, $m, $s)  = explode(":", $time);
  $seconds          = 0;
  $seconds         += (intval($h) * 3600);
  $seconds         += (intval($m) * 60);
  $seconds         += (intval($s));
  
  return $seconds;
}

public function getSubtaskTimers($subtask_ids=""){
  $sql              = "SELECT ITT.*,
                              ANT.subject 
                      FROM individual_task_timer ITT
                      INNER JOIN add_new_task ANT
                              ON ANT.id = ITT.task_id
                      WHERE ITT.task_id IN(".$subtask_ids.")";                 
  $individual_timer = $this->db->query($sql)->result_array();                    
  $userwiseTimers   = [];
  
  if(!empty($individual_timer)){
      foreach($individual_timer as $it){
          $timeArr = explode(',', $it['time_start_pause']);                    
          
          if(!empty($timeArr)){
              $i = 0;
              foreach($timeArr as $tm){
                  $start_time = date('Y-m-d H:i:s', (int)$timeArr[$i]);

                  if(isset($timeArr[$i+1])){
                      $end_time     = date('Y-m-d H:i:s', isset($timeArr[$i+1]) ? (int)$timeArr[$i+1] : time());                        
                      $time_seconds = $this->calculate_diff((int)$timeArr[$i], (int)$timeArr[$i+1]);
                      $time_seconds = $this->time_to_sec($time_seconds);

                      if($time_seconds > 0){
                        $userwiseTimers[$it['user_id']][] = [
                            'subject'    => $it['subject'],
                            'start_time' => $start_time,
                            'end_time'   => $end_time,
                            'time_spent' => $time_seconds
                        ];
                        $userwiseTimers[$it['user_id']]['total_time_spent'] += $time_seconds;                        
                      }
                  }
                  $i = $i+2;
              }
          }
      }                   
  }
  return $userwiseTimers;
}

public function get_module_user_data($s_v){
  foreach ($s_v as $key => $sv_value) {
    $tmp          = explode(":",$sv_value);
    $result_val[] = end($tmp);
  }
  $get_user = implode(',',$result_val);
  
  if(!empty($get_user)){
    $sql              = "SELECT ot.user_id,
                                us.crm_name 
                        FROM organisation_tree AS ot  
                        LEFT JOIN user AS us 
                               ON us.id=ot.user_id
                        WHERE ot.id IN ($get_user) 
                        AND ot.firm_id = ". $_SESSION['firm_id']." ";
    $Returns          = $this->db->query($sql)->result_array();
    $userid_Return    = array_unique( array_column( $Returns,'user_id') );
    $user_name_Return = array_column( $Returns,'crm_name');
    $Return           = implode(",",array_intersect_key($user_name_Return, $userid_Return));
  }
  return $Return;
}

      public function timer_validate($get_tk_id)
    {
       $data=[];
       $timer_condition=($_SESSION['user_type']=='FU')?"AND user_id=".$_SESSION['id']."":"";
      $individual_timer=$this->db->query("SELECT `time_start_pause`,`task_id`,`user_id`,(CHAR_LENGTH(time_start_pause) - CHAR_LENGTH(REPLACE(time_start_pause, ',', '')) + 1) as total from individual_task_timer where task_id in ($get_tk_id) ".$timer_condition." ORDER BY id ")->result_array();


          $overall_individual_timer=$individual_timer;

          $us_id=$_SESSION['id'];

          $user_count_result=array_filter($overall_individual_timer, function ($var) use ($us_id) {
            return $var['user_id'] ==$us_id && $var['total'] % 2 !== 0;
              });
          $user_count_result=(count($user_count_result)>0)?current($user_count_result):[];
           $user_count_result=(!empty($user_count_result))?$user_count_result['task_id']:0;
           $timer_flag=0;


           $task_list=$this->Common_mdl->getall_wherein_records('add_new_task','id',$get_tk_id);


          foreach ($task_list as $key => $value) {
            $modulo_value=count(array_filter(explode(",",$value["time_start_pause"]))) % 2;

                 if($user_count_result!=0 && $timer_flag!==1)
                {
                    if($user_count_result==$value['id'])
                    {
                      $_SESSION['header_task_timer']=$value['id'];
                        $timer_style=1;
                        $timer_flag=1;

                    }
                    else
                    {
                         $timer_style=0;
                    }

                }

                // else if( $modulo_value == 0 && $user_count_result == 0 )
                // {
                //     $timer_style=1;
                //     if(isset($_SESSION['header_task_timer']))unset($_SESSION['header_task_timer']);
                // }
                // else if($modulo_value!=0 && $user_count_result==0)
                // {
                //     $timer_style=0;
                //     if(isset($_SESSION['header_task_timer']))unset($_SESSION['header_task_timer']);
                // }
                else if( $user_count_result == 0 )
                {
                    $timer_style=1;
                    if(isset($_SESSION['header_task_timer']))unset($_SESSION['header_task_timer']);
                }
                $data[$key]['task_id']=$value["id"];
                $data[$key]['timer_style']=$timer_style;
              }
              return $data;
        }
    

       public function Firm_column_settings($table) 
       {
        
        $firm_id = "firm" . $_SESSION['firm_id'];
        $column_setting = $this->db->query("select sub_id,name,visible_" . $firm_id . ",order_" . $firm_id . ",cusname_" . $firm_id . " from firm_column_settings where table_name='" . $table . "' order by order_" . $firm_id . " ASC")->result_array();
        
        $hidden_coulmns = array_keys(array_column($column_setting, 'visible_' . $firm_id, 'name'), 0);
        $column_order = array_column($column_setting, "sub_id");
        $sort_order = array_column($column_setting, "name");        
        $hidden_coulmns = array_map(function ($a) {
            return trim($a) . '_TH';
        }, $hidden_coulmns);
        //fixed the first column
        if(in_array("subtask_toggle_TH",$hidden_coulmns)){
          array_splice($hidden_coulmns,array_search("subtask_toggle_TH",$hidden_coulmns),1);
        }
        //fixed the second column
        if(in_array("select_row_TH",$hidden_coulmns)){
          array_splice($hidden_coulmns,array_search("select_row_TH",$hidden_coulmns),1);
        }

        if(count($hidden_coulmns)==13){
          array_splice($hidden_coulmns,array_search("subject_TH",$hidden_coulmns),1);
          array_splice($hidden_coulmns,array_search("timer_TH",$hidden_coulmns),1);
          array_splice($hidden_coulmns,array_search("progress_TH",$hidden_coulmns),1);
          array_splice($hidden_coulmns,array_search("company_TH",$hidden_coulmns),1);
          array_splice($hidden_coulmns,array_search("action_TH",$hidden_coulmns),1);
        }
        //$column_order = array_map(function ($a){return trim( $a ).'_TH';}, $column_order );
        $return['sort_order'] = json_encode($sort_order);
        $return['hidden'] = json_encode($hidden_coulmns);
        $return['order'] = json_encode($column_order);
        return $return;
    }

}
