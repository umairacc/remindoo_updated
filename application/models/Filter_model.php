<?php
class Filter_model extends CI_Model {

	public function index(){
		echo "Model Called";
	}

	public function details_list($legal_form,$date,$client,$user){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT * FROM `user` where autosave_status!='1' and crm_name!='' and role=4 and  firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		//echo $date;
		$dates=date('d M Y',strtotime($date));
		if($count['0']!=''){
			if($client!='0'){
				$query=$this->db->query("SELECT count(*) as limit_count FROM client WHERE autosave_status=0 and user_id in (".$im_val.") and crm_legal_form='".$legal_form."' and from_unixtime(created_date) BETWEEN ".$date." AND ".$date." and user_id=".$client."")->row_array();
			}else{
				$query=$this->db->query("SELECT count(*) as limit_count FROM client WHERE autosave_status=0 and user_id in (".$im_val.") and crm_legal_form='".$legal_form."' and DATE_FORMAT(FROM_UNIXTIME(`created_date`), '%e %b %Y')= '".$dates."'")->row_array();
			}
	    }else{
	    	$query=0;
	    }
		return $query;
	}

	public function task_details($status,$date,$client,$user,$team){
		$user_id=$_SESSION['id'];	
		$dates=date('d M Y',strtotime($date));

		if($user!='0'){
			if($team!='0'){
				if($client!='0'){
					$query=$this->db->query('select count(*) as task_count from add_new_task where create_by="'.$client.'" and   DATE_FORMAT(FROM_UNIXTIME(created_date), "%e %b %Y") = "'.$dates.'" AND task_status="'.$status.'" and find_in_set("'.$user.'",worker) and find_in_set("'.$team.'",team)')->row_array();	
				}else{
					$query=$this->db->query('select count(*) as task_count from add_new_task where create_by="'.$user_id.'" and   DATE_FORMAT(FROM_UNIXTIME(created_date), "%e %b %Y") = "'.$dates.'" AND task_status="'.$status.'" and find_in_set("'.$user.'",worker) and find_in_set("'.$team.'",team)')->row_array();			
				}
			}else{
				if($client!='0'){
					$query=$this->db->query('select count(*) as task_count from add_new_task where create_by="'.$client.'" and   DATE_FORMAT(FROM_UNIXTIME(created_date), "%e %b %Y") = "'.$dates.'" AND task_status="'.$status.'" and find_in_set("'.$user.'",worker) ')->row_array();	
				}else{
					$query=$this->db->query('select count(*) as task_count from add_new_task where create_by="'.$user_id.'" and   DATE_FORMAT(FROM_UNIXTIME(created_date), "%e %b %Y") = "'.$dates.'" AND task_status="'.$status.'" and find_in_set("'.$user.'",worker)')->row_array();			
				}
			}

		}else{
			if($client!='0'){
				$query=$this->db->query('select count(*) as task_count from add_new_task where create_by="'.$client.'" and   DATE_FORMAT(FROM_UNIXTIME(created_date), "%e %b %Y") = "'.$dates.'" AND task_status="'.$status.'"')->row_array();	
			}else{
				$query=$this->db->query('select count(*) as task_count from add_new_task where create_by="'.$user_id.'" and   DATE_FORMAT(FROM_UNIXTIME(created_date), "%e %b %Y") = "'.$dates.'" AND task_status="'.$status.'"')->row_array();	
			}
		}			
		return $query;
	}

	public function proposal_details($status,$date,$client,$user){
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select count(*) as proposal_count from proposals
        where user_id="'.$user_id.'" and status="'.$status.'" and date(created_at)="'.$date.'" and company_id="'.$client.'"')->row_array();
        return $query;
	}

	public function service_count($status,$date,$client,$user){
		if($status=='conf_statement'){ 
			$search_value='crm_confirmation_statement_due_date';
		}
		if($status=='accounts'){ 
			$search_value='crm_ch_accounts_next_due';
		}
		if($status=='company_tax_return'){ 
			$search_value='crm_accounts_tax_date_hmrc';
		}
		if($status=='personal_tax_return'){ 
			$search_value='crm_personal_due_date_return';
		}
		if($status=='vat'){ 
			$search_value='crm_vat_due_date';
		}
		if($status=='payroll'){ 
			$search_value='crm_rti_deadline';
		}
		if($status=='workplace'){ 
			$search_value='crm_pension_subm_due_date';
		}
		if($status=='cis'){ 
			$search_value='';
		}
		if($status=='cissub'){ 
			$search_value='';
		}
		if($status=='p11d'){ 
			$search_value='crm_next_p11d_return_due';
		}
		if($status=='management'){ 
			$search_value='crm_next_manage_acc_date';
		}
		if($status=='bookkeep'){ 
			$search_value='crm_next_booking_date';
		}
		if($status=='investgate'){ 
			$search_value='crm_insurance_renew_date';
		}
		if($status=='registered'){ 
			$search_value='crm_registered_renew_date';
		}
		if($status=='taxadvice'){ 
			$search_value='crm_investigation_end_date';
		}
		if($status=='taxinvest'){ 
			$search_value='crm_investigation_end_date';
		}
		if($search_value!=''){
		$id=$_SESSION['id'];
         $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
           $us_id =  explode(',', $sql['us_id']);

           if($client!='0'){
          $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (".$status."!='') and (".$search_value."='".$date."') and user_id='".$client."' order by id desc")->row_array();  
          }else{
          	 $client_details =$this->db->query("SELECT count(*) as service_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (".$status."!='') and (".$search_value."='".$date."') order by id desc")->row_array();  
          }
          }else{
          	$client_details=0;
          }        
          return $client_details;
	}


	public function dead_count($search_value,$service,$date,$client,$user){
		if($search_value!=''){
		$id=$_SESSION['id'];
         $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
           $us_id =  explode(',', $sql['us_id']);
           if($client!='0'){
          $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (".$service."!='') and (".$search_value." ='".$date."')  and user_id='".$user."' order by id desc")->row_array();
	      }else{
	      	 $client_details =$this->db->query("SELECT count(*) as dead_count FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and (".$service."!='') and (".$search_value." ='".$date."') order by id desc")->row_array();
	      }
         }else{
         	$client_details['dead_count']=0;
         }
          return $client_details;
	}

	public function leads_details($status,$date,$client,$user,$team)
	{
		$user_id=$_SESSION['id'];
		$dates=date('d M Y',strtotime($date));
		if($user!='0'){
			if($client!='0'){
				if($team!='0'){
					$query=$this->db->query('select count(*) as lead_count from leads where user_id='.$client.' and lead_status="'.$status.'"  and DATE_FORMAT(FROM_UNIXTIME(createdTime), "%e %b %Y") = "'.$dates.'" AND find_in_set("'.$user.'",assigned) and find_in_set("'.$team.'",team)')->row_array();
				}else{
					$query=$this->db->query('select count(*) as lead_count from leads where user_id='.$client.' and lead_status="'.$status.'"  and DATE_FORMAT(FROM_UNIXTIME(createdTime), "%e %b %Y") = "'.$dates.'" AND find_in_set("'.$user.'",assigned)')->row_array();	
				}
			}else{
			 if($team!='0'){
			 	$query=$this->db->query('select count(*) as lead_count from leads where user_id='.$user_id.' and lead_status="'.$status.'"  and DATE_FORMAT(FROM_UNIXTIME(createdTime), "%e %b %Y") = "'.$dates.'" AND find_in_set("'.$user.'",assigned) and find_in_set("'.$team.'",team) ')->row_array();
			 }else{
			 	$query=$this->db->query('select count(*) as lead_count from leads where user_id='.$user_id.' and lead_status="'.$status.'"  and DATE_FORMAT(FROM_UNIXTIME(createdTime), "%e %b %Y") = "'.$dates.'" AND find_in_set("'.$user.'",assigned) ')->row_array();
			 }				
			}
		}else{
			if($client!='0'){
				$query=$this->db->query('select count(*) as lead_count from leads where user_id='.$client.' and lead_status="'.$status.'"  and DATE_FORMAT(FROM_UNIXTIME(createdTime), "%e %b %Y") = "'.$dates.'"')->row_array();
			}else{
				$query=$this->db->query('select count(*) as lead_count from leads where user_id='.$user_id.' and lead_status="'.$status.'"  and DATE_FORMAT(FROM_UNIXTIME(createdTime), "%e %b %Y") = "'.$dates.'"')->row_array();
			}
		}
		//$query=$this->db->query('select count(*) as lead_count from leads where user_id='.$user_id.' and lead_status="'.$status.'"  and DATE_FORMAT(FROM_UNIXTIME(createdTime), "%e %b %Y") = "'.$dates.'" AND (find_in_set("'.$user.'",assigned) or find_in_set("'.$user.'",team) or find_in_set("'.$user.'",dept))')->row_array();
		return $query;
	}


	public function activeusers_count($date,$client,$user){
		$user_id=$_SESSION['id'];
		$dates=date('d M Y',strtotime($date));
		if($client!='0'){
		$query1=$this->db->query("SELECT count(*) as activeuser_count FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' and status='1' and FROM_UNIXTIME(CreatedTime) = '".$dates."' and id='".$client."' order by id DESC");
	}else{
		$query1=$this->db->query("SELECT count(*) as activeuser_count FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' and status='1' and FROM_UNIXTIME(CreatedTime) = '".$dates."' order by id DESC");
	}
		$results1 = $query1->row_array();
		return $results1;
	}

	public function inactiveusers_count($date,$client,$user){
		$user_id=$_SESSION['id'];
		$dates=date('d M Y',strtotime($date));
		if($client!='0'){
		$query1=$this->db->query("SELECT count(*) as inactiveuser_count FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' and (status='0' or status='2' ) and FROM_UNIXTIME(CreatedTime)='".$dates."' and id='".$client."' order by id DESC");
		$results1 = $query1->row_array();
	}else{
		$query1=$this->db->query("SELECT count(*) as inactiveuser_count FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' and (status='0' or status='2' ) and FROM_UNIXTIME(CreatedTime) = '".$dates."' order by id DESC");
		$results1 = $query1->row_array();
	}
		return $results1;	
	}

	public function frozenusers_count($date,$client,$user){
		$user_id=$_SESSION['id'];
		$dates=date('d M Y',strtotime($date));
		if($client!='0'){
		$query1=$this->db->query("SELECT count(*) as frozenuser_count FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' and (status='0' or status='2' ) and FROM_UNIXTIME(CreatedTime)='".$dates."' and id='".$client."' order by id DESC");
		$results1 = $query1->row_array();
	}else{
		$query1=$this->db->query("SELECT count(*) as frozenuser_count FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' and (status='0' or status='2' ) and FROM_UNIXTIME(CreatedTime) = '".$dates."' order by id DESC");
		$results1 = $query1->row_array();
	}
		return $results1;	
	}

	 public function completed_task_count($date,$client,$user){
		$user_id=$_SESSION['id'];
		$dates=date('d M Y',strtotime($date));
		$query=$this->db->query('select count(*) as task_count from add_new_task
        where create_by="'.$user_id.'" and task_status="complete"  and   DATE_FORMAT(FROM_UNIXTIME(created_date), "%e %b %Y") = "'.$dates.'" and (find_in_set("'.$user.'",worker) or find_in_set("'.$user.'",team) or find_in_set("'.$user.'",department))')->row_array();
        return $query;
	 }

	public function overallleads_count($date,$client,$user){
		$user_id=$_SESSION['id'];
		$dates=date('d M Y',strtotime($date));
		$query=$this->db->query('select count(*) as leads_count from leads  where user_id="'.$user_id.'" and DATE_FORMAT(FROM_UNIXTIME(createdTime), "%e %b %Y") = "'.$dates.'" AND (find_in_set("'.$user.'",assigned) or find_in_set("'.$user.'",team) or find_in_set("'.$user.'",dept))')->row_array();
        return $query;
	}

	public function oth_count($status,$date,$client,$user){
		$user_id=$_SESSION['id'];
		$dates=date('d M Y',strtotime($date));
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		if($count['0']!=''){
			$records=$this->db->query("select client_id from Invoice_details where client_email in (".$im_val.")")->result_array();
			$val=array();
			foreach($records as $rec){
				array_push($val, $rec['client_id']);
			}
			$clients_id=implode(',',$val);
			//print_r($clients_id);
			$results=$this->db->query("select count(*) as invoice_count from amount_details where client_id in (".$clients_id.") and payment_status='".$status."' and date(created_at) = '".$date."'")->row_array();
			}else{
	     	$results=0;
	     }
		return $results;
	}


	public function exp_count($date,$client,$user){
		$user_id=$_SESSION['id'];
		$dates=date('d M Y',strtotime($date));
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		if($count['0']!=''){
			$results=$this->db->query("select count(*) as invoice_count from Invoice_details where client_email in (".$im_val.") and invoice_duedate='Expired' and DATE_FORMAT(FROM_UNIXTIME(created_at), '%e %b %Y') = '".$dates."'")->row_array();
		}else{
	     	$results=0;
	     }
		return $results;
	}


	public function proposal_details_section($date){
		$dates=date('Y-m-d',strtotime($date));
		//$last_month_end=date('Y-m-t');
		//$last_Year_start=date('Y-m-01');
		$query=$this->db->query("select proposal_name,created_at from proposals where user_id='".$_SESSION['id']."' and date(created_at)='".$date."'")->result_array();
		return $query;
	}

	public function leads_history_section($date){
		$dates=date('d M Y',strtotime($date));
		$last_month_end=date('Y-m-t');
		$last_Year_start=date('Y-m-01');
	    $user_id=$_SESSION['id'];
		$query=$this->db->query('select name,createdTime from leads where user_id='.$user_id.' and DATE_FORMAT(FROM_UNIXTIME(createdTime), "%e %b %Y") = "'.$dates.'"')->result_array();
		return $query;
	}

	public function task_pridetails($status,$date,$client,$user,$team){
	  $dates=date('d M Y',strtotime($date));
		$user_id=$_SESSION['id'];
		if($user!='0'){
			$query=$this->db->query('select count(*) as task_count from add_new_task
        where create_by="'.$user_id.'" and priority="'.$status.'" and  DATE_FORMAT(FROM_UNIXTIME(created_date), "%e %b %Y") = "'.$dates.'"')->row_array();
		}else{
			$query=$this->db->query('select count(*) as task_count from add_new_task
        where create_by="'.$user_id.'" and priority="'.$status.'" and  DATE_FORMAT(FROM_UNIXTIME(created_date), "%e %b %Y") = "'.$dates.'" and find_in_set("'.$user.'",worker) and find_in_set("'.$team.'",team)')->row_array();
		}
		
        return $query;
	}

	public function task_this_month($table,$date,$client,$user,$team){
		$dates=date('d M Y',strtotime($date));
		$user_id=$_SESSION['id'];
		$query=$this->db->query('select start_date,end_date,subject from add_new_task where create_by="'.$user_id.'" and  DATE_FORMAT(FROM_UNIXTIME(start_date), "%e %b %Y") = "'.$dates.'"  and DATE_FORMAT(FROM_UNIXTIME(end_date), "%e %b %Y") = "'.$dates.'" order by start_date asc')->result_array();
		return $query;
	}


} ?>