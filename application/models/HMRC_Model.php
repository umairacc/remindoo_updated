<?php
class HMRC_Model extends CI_Model
{
	/*Coded BY Ajith*/
  /*===============================================*/
	// public $key = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
  public $arn = "sample_arn";
  public $sandbox_url = "https://test-api.service.hmrc.gov.uk/";
  public $prod_url = "https://api.service.hmrc.gov.uk/";
  public function Execute_Curl($URL,$method,$auth_token)
  {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $URL); 
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method); 
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $headers = array(
        "Content-Type: application/vnd.hmrc.1.0+json",
        "Bearer: bb7fed3fe10dd235a2ccda3d50fb",
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      // curl_setopt($curl, CURLOPT_USERPWD,$this->key);
      $response = curl_exec($curl);
      if($errno = curl_errno($curl)) 
      {
        $error_message = curl_strerror($errno);
        $RETURN['error'] = "cURL error ({$errno}):\n {$error_message}";
      }
      else
      {
        curl_close($curl);
        $RETURN['response'] = json_decode($response,true);
      }
      return $RETURN;
  }

  public function Execute_Curl_Post($URL,$payload,$auth_token)
  {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $URL); 
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
      $headers = array(
        "Content-Type: application/vnd.hmrc.1.0+json",
        "Bearer: ".$auth_token,
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $response = curl_exec($curl);
      if($errno = curl_errno($curl)) 
      {
        $error_message = curl_strerror($errno);
        $RETURN['error'] = "cURL error ({$errno}):\n {$error_message}";
      }
      else
      {
        curl_close($curl);
        $RETURN['response'] = json_decode($response,true);
      }
      return $RETURN;
  }

  public function Get_OAuth_Token($client_id,$client_secret,$scope)
  {
    $url = "https://test-www.tax.service.gov.uk/oauth/authorize?response_type=code&client_id=".$client_id."&scope=".$scope."&redirect_uri=".get_instance()->config->item('base_url').'/auth-redirect';
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url); 
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    // curl_setopt($curl, CURLOPT_USERPWD,$this->key);
    $response = curl_exec($curl);
    curl_close($curl);


    $payload = json_encode(array("client_secret"=>$client_secret, "client_id"=> $client_id, "grant_type"=>"authorization_code", "redirect_uri" => get_instance()->config->item('base_url').'/auth-redirect', "code" => $response_code));
    $URL = "https://test-api.service.hmrc.gov.uk/oauth/token";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $URL); 
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
    $headers = array(
      "Content-Type: application/json"
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    if($errno = curl_errno($curl)) 
    {
      $error_message = curl_strerror($errno);
      $RETURN['error'] = "cURL error ({$errno}):\n {$error_message}";
    }
    else
    {
      curl_close($curl);
      $RETURN['response'] = json_decode($response,true);
    }

    return $RETURN;

  }

	public function Get_All_Auths($clientId,$client_secret)
	{
    $URL = $this->sandbox_url.'agents/'.$this->arn.'/invitations';
    $auth_token = $this->Get_OAuth_Token($clientId,$client_secret,"read:sent-invitations");
    return $this->Execute_Curl($URL,'GET',$auth_token);
  }

  public function Create_New_Auth($service,$clientType,$clientIdType,$clientId,$knownFact,$client_secret)
  {
    $data = array("service" => $service,"clientType" => $clientType,"clientIdType"=>$clientIdType,"clientId" => $clientId, "knownFact" => $knownFact);
    $postdata = json_encode($data);
    $URL = $this->sandbox_url.'agents/'.$this->arn.'/invitations';
    $auth_token = $this->Get_OAuth_Token($clientId,$client_secret,"write:sent-invitations");
    return $this->Execute_Curl_Post($URL,$postdata,$auth_token);
  }

  public function Get_Invitation_By_Id($clientId,$client_secret, $invitationId)
  {
    $URL = $this->sandbox_url.'agents/'.$this->arn.'/invitations/'.$invitationId;
    $auth_token = $this->Get_OAuth_Token($clientId,$client_secret,"read:sent-invitations");
    return $this->Execute_Curl($URL,'GET',$auth_token);
  }
  
  public function Cancel_Invitation_By_Id($clientId,$client_secret, $invitationId)
  {
    $URL = $this->sandbox_url.'agents/'.$this->arn.'/invitations/'.$invitationId;
    $auth_token = $this->Get_OAuth_Token($clientId,$client_secret,"write:cancel-invitations");
    return $this->Execute_Curl($URL,'DELETE',$auth_token);
  }

  public function Get_Relationship_Status($clientId,$client_secret)
	{
    $URL = $this->sandbox_url.'agents/'.$this->arn.'/relationships';
    $auth_token = $this->Get_OAuth_Token($clientId,$client_secret,"read:check-relationship");
    return $this->Execute_Curl($URL,'GET',$auth_token);
  }
  /*===================================*/

}
?>