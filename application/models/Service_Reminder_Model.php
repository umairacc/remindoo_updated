<?php
class Service_Reminder_Model extends CI_Model
{	
	public $no_recurring_service 			= [1,2];
	public $Get_Service_ReminderFields 		= [];
	public $firm_exc_sr_log					= '';
	public $f_indexs						= ['default_r','custom_r','due_date','duration','service_id'];
	public function __construct()
	{
    	parent::__construct();
		require_once(APPPATH.'controllers/AwsMailer.php');
    	$this->load->helper('date');

    	$f = $this->Get_Service_ReminderFields();
    	$f = array_chunk( explode( ',' , $f ) , 5 );
    	$this->Get_Service_ReminderFields = array_combine( range(1, 15), $f );
	}

	/*CRON TIME Executing Scripts*/
	public function Get_Sending_Data( $Firm_Data )
	{		
		echo "Inside Get_Sending_Data()";

		$this->firm_exc_sr_log	.= "Inside Get_Sending_Data()\n";

		$current_date = date('d-m-Y');

		$current_task_date = date('Y-m-d');

		$reminders = $this->db->where( "UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) = UNIX_TIMESTAMP(STR_TO_DATE(  '".$current_date."' ,'%d-%m-%Y' )) AND firm_id=".$Firm_Data['firm_id'] )
		                      ->get( "service_reminder_cron_data" )
		                      ->result_array();
		
		$this->firm_exc_sr_log .= "Today service reminders\n".json_encode( $reminders )."\n";

		foreach ( $reminders as $key => $reminder_data )
		{
			$check_days = 5;

			if($reminder_data['frequency'] == '1 month')
			{
				$check_days = 15;
			}
			elseif($reminder_data['frequency'] == '3 Months')
			{
				$check_days = 75;
			}
			elseif($reminder_data['frequency'] == '1 Year')
			{
				$check_days = 350;
			}
			elseif($reminder_data['frequency'] == '7 days')
			{
				$check_days = 5;
			}

			$starting_task_date = date('Y-m-d', strtotime($current_task_date. ' - '.$check_days.' days'));

			$check_task_query_status=0;

			$this->firm_exc_sr_log 		.= $key."=Reminder Excution Start=".json_encode( $reminder_data )."\n";

			$service_id_plain = $reminder_data['service_id'];

			$service_id_with_ser = "ser_" . $reminder_data['service_id'];

			$task_query = "SELECT id,task_status,progress_status FROM add_new_task WHERE firm_id=".$Firm_Data['firm_id']." AND company_name = '".$reminder_data['client_id']."' AND client_contacts_id = ".$reminder_data['client_contacts_id']." AND related_to = 'service_reminder' AND (service_due_date BETWEEN '".$starting_task_date."' AND '".$current_task_date."') AND (related_to_services = '".$service_id_plain."' OR related_to_services = '".$service_id_with_ser."')";

			$task_data = $this->db->query($task_query)->result_array();
			$td_statuses=[];
			if (is_array($task_data) ){

				if ( count($task_data) > 0 ){
					foreach($task_data as $td){
						$td_statuses[] = $td['task_status'];
						if(($td['task_status']=="1" || $td['task_status']=="2" || $td['task_status']=="3") && $td['progress_status']==11){
							$check_task_query_status=1;
						}
					}
				} else {
					$check_task_query_status=1;
				}
			} else {
				$check_task_query_status=1;
			}

			if( $reminder_data['status']==0 && $check_task_query_status==1)
			{
				$this->firm_exc_sr_log .= "inside reminder progress condtion\n";

				$client_service_contact_cond  = [
												'client_id' 		=> 	$reminder_data['client_id'] ,
												'service_id'		=>	$reminder_data['service_id'],
												'client_contacts_id'=>	$reminder_data['client_contacts_id']
											];

				$frequency_data = $this->db->where( $client_service_contact_cond )
										   ->get( "client_service_frequency_due" )
										   ->row_array();
			
				if( $frequency_data['is_create_task'] == 0  )
				{	
					$this->firm_exc_sr_log .= "TASK not created yet\n";
					$this->Create_ServiceTask( $reminder_data );
					$this->firm_exc_sr_log .= "Task created\n";
				}

				$query = "SELECT c.user_id,c.id,u.status 
							FROM client as c  
								INNER JOIN user as u 
									ON u.id=c.user_id 
								WHERE c.id=".$reminder_data['client_id'];

				$client_data = $this->db->query( $query )->row_array();

				$this->firm_exc_sr_log .= "client data".json_encode( $client_data )."\n";

				$Reminder_SendAble_Status		=	json_decode( $Firm_Data['reminder_send_status'] , true );

				if( $reminder_data['template_id'] !='' && in_array( $client_data['status'] , $Reminder_SendAble_Status ) )
				{
					echo "status".$client_data['status']."allowed status";
					//print_r( $client_data );
					
					$this->firm_exc_sr_log .= "Inside reminder creatable client status\n";
					
					$this->db->select("main_email,work_email");

					if( $reminder_data['client_contacts_id'] != 0 )
						$this->db->where( "id=".$reminder_data['client_contacts_id'] );
					else 
						$this->db->where( "client_id=".$client_data['user_id']." AND make_primary=1" );

					$contact_details = $this->db->get("client_contacts")->row_array();

					$contact_details['mails'][] 	=	$contact_details['main_email']; 

					if( !empty( $contact_details['work_email'] ) )
					{
						$work_email            	   = json_decode( $contact_details['work_email'] , true );

						$contact_details['mails']  = array_merge( $contact_details['mails'] , $work_email );
					}

					if( !empty( $contact_details['mails'] ) && $Firm_Data['service_reminder'] == 1 )
					{				
						$client 				= [ 
													'client_id' 			=>	$reminder_data['client_id'],
													'client_contacts_id'	=>	$reminder_data['client_contacts_id']
												];

						$DATA 					=	$this->get_reminder_template_data( $client , $reminder_data['template_id'] , $reminder_data['template_type'] );					

						$send_to['to']	= 	$contact_details['mails'];
						$send_to['cc']	=	$Firm_Data['company_email'];	 
						$body 			=   setup_body_content( $Firm_Data['firm_id'] , $DATA['body'] );

                      
	                    $data = [
	                        'send_from'             =>  $Firm_Data['firm_id'],
	                        'send_to'               =>  json_encode( $send_to ),
	                        'subject'               =>  strip_tags( $DATA['subject'] ),
	                        'content'               =>  $body,
	                        'status'                =>  0,
	                        'relational_module'     =>  'service_reminder',
	                        'relational_module_id'  =>  $reminder_data['id'],
	                        'created_at'            =>  date('Y-m-d H:i:s')
	                    ];

                      	$this->db->insert( "email" , $data );

						$this->firm_exc_sr_log .= $this->db->insert_id()."-->reminder insert in email table\n".json_encode($data)."\n";
						$task_query = "SELECT t.id,t.user_id,t.task_status,t.progress_status,t.sent_email_count,t.email_count FROM add_new_task as t WHERE firm_id=".$Firm_Data['firm_id']." AND company_name = '".$reminder_data['client_id']."' AND client_contacts_id = ".$reminder_data['client_contacts_id']." AND related_to = 'service_reminder' AND (service_due_date BETWEEN '".$starting_task_date."' AND '".$current_task_date."') AND (related_to_services = '".$service_id_plain."' OR related_to_services = '".$service_id_with_ser."')";

						$task_data = $this->db->query($task_query)->result_array();
						$td_statuses=[];
						if (is_array($task_data) ){

							if ( count($task_data) > 0 ){
								foreach($task_data as $td){
									$td_statuses[] = $td['task_status'];
									if(($td['task_status']=="1" || $td['task_status']=="2" || $td['task_status']=="3") && $td['progress_status']==11){
										if($td['sent_email_count'] == null)
										{
											$this->db->update( 'add_new_task' , [ 'sent_email_count'=> 1 ] , "id=".$td['id'] );
										}
										else
										{
											$sent_email_count = $td['sent_email_count'] + 1;
											$this->db->update( 'add_new_task' , [ 'sent_email_count'=> $sent_email_count ] , "id=".$td['id'] );
											$client_non_responding_reminder_no = [];
											$admin_setting_row = $this->db->query( "Select * from admin_setting where firm_id = ".$Firm_Data['firm_id'] )->row_array();
											if(isset($admin_setting_row['client_non_responding_reminder_no'])){
												$client_non_responding_reminder_no = explode(",",$admin_setting_row['client_non_responding_reminder_no']);
											}
											
											if(is_array($client_non_responding_reminder_no)&& in_array($sent_email_count, $client_non_responding_reminder_no)){
												// $this->create_client_not_responding_task($td,$Firm_Data['firm_id']);
												$sub_task_data = $this->db->query("select id from add_new_task where user_id=".$td['user_id']." and related_to = 'sub_task' and subject = 'Call non-responding Client, Reminder no - ".$sent_email_count."'")->row_array();
												if(!isset($sub_task_data['id'])){
													$this->Create_Sub_task( $td['id'],  "Call non-responding Client, Reminder no - ".$sent_email_count);
												}
												$aws_mailer = new AwsMailer();
												$emailData    = [
													'email_to'      => 'sourav.skr11@gmail.com',
													'email_subject' => 'Client Non Responding',
													'email_body'    => 'Task No : '.$td['id']           
												];
												$IsSend  = $aws_mailer->prepare_email_to_send($emailData, false);
											}
										}
									}
								}
							}
						}
					}
					else
					{
						$this->firm_exc_sr_log .= "client mail empty or service reminder off in firm settings\n";
					}		
				}
				else
				{
					$this->firm_exc_sr_log .= "client status differ from serminder sendable status\n";
					$this->db->update( 'service_reminder_cron_data' , [ 'status'=> 3 ] , "id=".$reminder_data['id'] );
				}
				$return_data=1;
			}
			else
			{
				$this->firm_exc_sr_log .= "inside reminder stop condtion\n";
				$return_data=0;
			}

			if( $reminder_data['create_records'] == 1 )
			{
				$this->firm_exc_sr_log 	.= "inside Create recordes condition\n";

				$this->Setup_Next_Frequency_Data( $reminder_data['client_id'] , $reminder_data['service_id'] , $reminder_data['client_contacts_id'] );

				$this->firm_exc_sr_log 	.= "Setup next frequncy reminders\n";
			}

			$this->firm_exc_sr_log .= $key." Reminder Execution End\n";
		}
		return $return_data;
	}

	public function create_client_not_responding_task($task_data,$firm_id){
		$task_id = $task_data['id'];
		$firm_assignee_rows = $this->db->query( "Select * from firm_assignees where firm_id = ".$firm_id." and module_name = 'TASK' and module_id = ".$task_id )->result_array();
		$firm_assignee_str = '';
		if($firm_assignee_rows){
			$count_assignee_row = count($firm_assignee_rows);
		foreach($firm_assignee_rows as $key => $row){
			if(($key+1)==$count_assignee_row){
			$firm_assignee_str .= $row['assignees'];
			}
			else{
			$firm_assignee_str .= $row['assignees'].':';
			}
		}
		$firm_assignee_arr = explode(':',$firm_assignee_str);
		$firm_assignee_arr = array_unique($firm_assignee_arr);
		$firm_assignees = implode(":",$firm_assignee_arr);

		$insert_task =[	
			'user_id'				=>	$task_data['user_id'],
			'company_name' 			=>  $task_data['company_name'],
			'client_contacts_id'	=>	$task_data['client_contacts_id'],
			'related_to_services'	=>	$task_data['related_to_services'],
			'subject'				=>	"Call non-responding Client",
			'service_due_date'		=>  $task_data['service_due_date'],
			'firm_id'				=>  $task_data['firm_id'],
			'billable'				=>	'',
			'progress_status'		=>	0,
			'task_status'			=>	'1',
			'related_to'			=>	'service_reminder',
			'start_date'			=> 	date( 'Y-m-d' ),
			'end_date'				=>  date( 'Y-m-d', strtotime('+ 10 days') ),
			'created_date'			=> 	time(),
		];	

		$this->db->insert( 'add_new_task', $insert_task );	

		$insert_firm_assinees =[	
			'firm_id'				=>	$firm_id,
			'module_name' 			=>  'TASK',
			'sub_module_name'		=>	'',
			'module_id'				=>	$this->db->insert_id(),
			'sub_module_id'			=>	0,
			'assignees'				=>  $firm_assignees,
			'task_started'			=>  ''
		];

		$this->db->insert( 'firm_assignees', $insert_firm_assinees);
		}	
	}

	public function Get_Sending_Data_test( $Firm_Data )
	{		
		echo "Inside Get_Sending_Data()";

		$this->firm_exc_sr_log	.= "Inside Get_Sending_Data()\n";

		// $current_date = date('d-m-Y');

		// $current_task_date = date('Y-m-d');

		$current_date = '01-06-2022';

		$current_task_date = '2022-06-01';

		$reminders = $this->db->where( "UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) = UNIX_TIMESTAMP(STR_TO_DATE(  '".$current_date."' ,'%d-%m-%Y' )) AND firm_id=".$Firm_Data['firm_id'] )
		                      ->get( "service_reminder_cron_data" )
		                      ->result_array();

		// $reminders = $this->db->where( "id = 432321 AND firm_id=".$Firm_Data['firm_id'] )
		//                       ->get( "service_reminder_cron_data" )
		//                       ->result_array();
		// echo $this->db->last_query();
		// echo '<pre>';
		// print_r($reminders);
		// exit;					
		
		$this->firm_exc_sr_log .= "Today service reminders\n".json_encode( $reminders )."\n";

		foreach ( $reminders as $key => $reminder_data )
		{
			$check_days = 5;

			if($reminder_data['frequency'] == '1 month')
			{
				$check_days = 15;
			}
			elseif($reminder_data['frequency'] == '3 Months')
			{
				$check_days = 75;
			}
			elseif($reminder_data['frequency'] == '1 Year')
			{
				$check_days = 350;
			}
			elseif($reminder_data['frequency'] == '7 days')
			{
				$check_days = 5;
			}

			$starting_task_date = date('Y-m-d', strtotime($current_task_date. ' - '.$check_days.' days'));

			$check_task_query_status=0;

			$this->firm_exc_sr_log 		.= $key."=Reminder Excution Start=".json_encode( $reminder_data )."\n";

			$service_id_plain = $reminder_data['service_id'];

			$service_id_with_ser = "ser_" . $reminder_data['service_id'];

			$task_query = "SELECT id,task_status,progress_status FROM add_new_task WHERE firm_id=".$Firm_Data['firm_id']." AND company_name = '".$reminder_data['client_id']."' AND client_contacts_id = ".$reminder_data['client_contacts_id']." AND related_to = 'service_reminder' AND (service_due_date BETWEEN '".$starting_task_date."' AND '".$current_task_date."') AND (related_to_services = '".$service_id_plain."' OR related_to_services = '".$service_id_with_ser."')";

			$task_data = $this->db->query($task_query)->result_array();
			$td_statuses=[];
			if (is_array($task_data) ){

				if ( count($task_data) > 0 ){
					foreach($task_data as $td){
						$td_statuses[] = $td['task_status'];
						if(($td['task_status']=="1" || $td['task_status']=="2" || $td['task_status']=="3") && $td['progress_status']==11){
							$check_task_query_status=1;
						}
					}
				} else {
					$check_task_query_status=1;
				}
			} else {
				$check_task_query_status=1;
			}

			if( $reminder_data['status']==0 && $check_task_query_status==1)
			{
				$this->firm_exc_sr_log .= "inside reminder progress condtion\n";

				$client_service_contact_cond  = [
												'client_id' 		=> 	$reminder_data['client_id'] ,
												'service_id'		=>	$reminder_data['service_id'],
												'client_contacts_id'=>	$reminder_data['client_contacts_id']
											];

				$frequency_data = $this->db->where( $client_service_contact_cond )
										   ->get( "client_service_frequency_due" )
										   ->row_array();
			
				if( $frequency_data['is_create_task'] == 0  )
				{	
					$this->firm_exc_sr_log .= "TASK not created yet\n";
					$this->Create_ServiceTask( $reminder_data );
					$this->firm_exc_sr_log .= "Task created\n";
				}

				$query = "SELECT c.user_id,c.id,u.status 
							FROM client as c  
								INNER JOIN user as u 
									ON u.id=c.user_id 
								WHERE c.id=".$reminder_data['client_id'];

				$client_data = $this->db->query( $query )->row_array();

				$this->firm_exc_sr_log .= "client data".json_encode( $client_data )."\n";

				$Reminder_SendAble_Status		=	json_decode( $Firm_Data['reminder_send_status'] , true );

				if( $reminder_data['template_id'] !='' && in_array( $client_data['status'] , $Reminder_SendAble_Status ) )
				{
					echo "status".$client_data['status']."allowed status";
					//print_r( $client_data );
					
					$this->firm_exc_sr_log .= "Inside reminder creatable client status\n";
					
					$this->db->select("main_email,work_email");

					if( $reminder_data['client_contacts_id'] != 0 )
						$this->db->where( "id=".$reminder_data['client_contacts_id'] );
					else 
						$this->db->where( "client_id=".$client_data['user_id']." AND make_primary=1" );

					$contact_details = $this->db->get("client_contacts")->row_array();

					$contact_details['mails'][] 	=	$contact_details['main_email']; 

					if( !empty( $contact_details['work_email'] ) )
					{
						$work_email            	   = json_decode( $contact_details['work_email'] , true );

						$contact_details['mails']  = array_merge( $contact_details['mails'] , $work_email );
					}

					if( !empty( $contact_details['mails'] ) && $Firm_Data['service_reminder'] == 1 )
					{				
						$client 				= [ 
													'client_id' 			=>	$reminder_data['client_id'],
													'client_contacts_id'	=>	$reminder_data['client_contacts_id']
												];

						$DATA 					=	$this->get_reminder_template_data( $client , $reminder_data['template_id'] , $reminder_data['template_type'] );					

						$send_to['to']	= 	$contact_details['mails'];
						$send_to['cc']	=	$Firm_Data['company_email'];	 
						$body 			=   setup_body_content( $Firm_Data['firm_id'] , $DATA['body'] );

                      
	                    $data = [
	                        'send_from'             =>  $Firm_Data['firm_id'],
	                        'send_to'               =>  json_encode( $send_to ),
	                        'subject'               =>  strip_tags( $DATA['subject'] ),
	                        'content'               =>  $body,
	                        'status'                =>  0,
	                        'relational_module'     =>  'service_reminder',
	                        'relational_module_id'  =>  $reminder_data['id'],
	                        'created_at'            =>  date('Y-m-d H:i:s')
	                    ];

                      	$this->db->insert( "email" , $data );

						$this->firm_exc_sr_log .= $this->db->insert_id()."-->reminder insert in email table\n".json_encode($data)."\n";
						$task_query = "SELECT id,task_status,progress_status,sent_email_count,email_count FROM add_new_task WHERE firm_id=".$Firm_Data['firm_id']." AND company_name = '".$reminder_data['client_id']."' AND client_contacts_id = ".$reminder_data['client_contacts_id']." AND related_to = 'service_reminder' AND (service_due_date BETWEEN '".$starting_task_date."' AND '".$current_task_date."') AND (related_to_services = '".$service_id_plain."' OR related_to_services = '".$service_id_with_ser."')";

						$task_data = $this->db->query($task_query)->result_array();
						$td_statuses=[];
						if (is_array($task_data) ){

							if ( count($task_data) > 0 ){
								foreach($task_data as $td){
									$td_statuses[] = $td['task_status'];
									if(($td['task_status']=="1" || $td['task_status']=="2" || $td['task_status']=="3") && $td['progress_status']==11){
										if($td['sent_email_count'] == null)
										{
											$this->db->update( 'add_new_task' , [ 'sent_email_count'=> 1 ] , "id=".$td['id'] );
										}
										else
										{
											$sent_email_count = $td['sent_email_count'] + 1;
											$this->db->update( 'add_new_task' , [ 'sent_email_count'=> $sent_email_count ] , "id=".$td['id'] );
											$client_non_responding_reminder_no = [];
											$admin_setting_row = $this->db->query( "Select * from admin_setting where firm_id = ".$Firm_Data['firm_id'] )->row_array();
											if(isset($admin_setting_row['client_non_responding_reminder_no'])){
												$client_non_responding_reminder_no = explode(",",$admin_setting_row['client_non_responding_reminder_no']);
											}
											
											if(is_array($client_non_responding_reminder_no)&& in_array($sent_email_count, $client_non_responding_reminder_no)){
												// $this->create_client_not_responding_task($td,$Firm_Data['firm_id']);
												$this->Create_Sub_task( $td['id'],  "Call non-responding Client, Reminder no - ".$sent_email_count);
												$aws_mailer = new AwsMailer();
												$emailData    = [
													'email_to'      => 'sourav.skr11@gmail.com',
													'email_subject' => 'Client Non Responding',
													'email_body'    => 'Task No : '.$td['id']           
												];
												$IsSend  = $aws_mailer->prepare_email_to_send($emailData, false);
											}
										}
									}
								}
							}
						}
					}
					else
					{
						$this->firm_exc_sr_log .= "client mail empty or service reminder off in firm settings\n";
					}		
				}
				else
				{
					$this->firm_exc_sr_log .= "client status differ from serminder sendable status\n";
					$this->db->update( 'service_reminder_cron_data' , [ 'status'=> 3 ] , "id=".$reminder_data['id'] );
				}
				$return_data=1;
			}
			else
			{
				$this->firm_exc_sr_log .= "inside reminder stop condtion\n";
				$return_data=0;
			}

			if( $reminder_data['create_records'] == 1 )
			{
				$this->firm_exc_sr_log 	.= "inside Create recordes condition\n";

				$this->Setup_Next_Frequency_Data( $reminder_data['client_id'] , $reminder_data['service_id'] , $reminder_data['client_contacts_id'] );

				$this->firm_exc_sr_log 	.= "Setup next frequncy reminders\n";
			}

			$this->firm_exc_sr_log .= $key." Reminder Execution End\n";
		}
		return $return_data;
	}

	public function today_dues_recurring( $Firm_Data )
	{
		$this->firm_exc_sr_log 	.= "inside today_dues_recurring()\n";

		$today_dues = $this->db->where( [ 'date' => date('d-m-Y') , 'firm_id' => $Firm_Data['firm_id'] ] )
							   ->get( "client_service_frequency_due" )
							   ->result_array();		
		
		$this->firm_exc_sr_log 	.= "Today dues \n".json_encode( $today_dues )."\n";

		foreach ( $today_dues as $key => $today_due_data )
		{
			echo "inside today dues=".$today_due_data['service_id']."client_id =".$today_due_data['client_id'].'client_conatcs='.$today_due_data['client_contacts_id'].'\n';

			$client_service_contact_cond  = [
												'client_id' 		=> 	$today_due_data['client_id'] ,
												'service_id'		=>	$today_due_data['service_id'],
												'client_contacts_id'=>	$today_due_data['client_contacts_id']
											];

			$reminder_data = $this->db->where( $client_service_contact_cond )
									->get( "service_reminder_cron_data" )
									->result_array();

			$reminder_dates = array_column( $reminder_data , 'date' , 'id' );

			$create_records_id = find_last_date( $today_due_data['date'] , $reminder_dates );

			echo  "result of last record=" .$create_records_id;

			if( $create_records_id == 0 )
			{	
				$this->firm_exc_sr_log .= "no reminders after due date \n";

				if( $today_due_data['is_create_task'] == 0 )
				{	
					$this->Create_ServiceTask( $today_due_data );				
					$this->firm_exc_sr_log .= "Task Created\n";
				}

				$this->Setup_Next_Frequency_Data( $today_due_data['client_id'] , $today_due_data['service_id'] , $today_due_data['client_contacts_id'] );
				$this->firm_exc_sr_log .= "Setup next frequncy reminders\n";
			}
			else
			{
				$this->firm_exc_sr_log .= "reminder avilable after due \n";				
			}
		}
	}			
	public function Setup_Next_Frequency_Data( $client_id , $service_id , $client_contacts_id = 0 )
	{	
		// echo "inside Setup_Next_Frequency_Data()";
		if( !in_array( $service_id , $this->no_recurring_service ) )
		{					
			$client_service_contact_cond  = [
												'client_id' 		=> 	$client_id,
												'service_id'		=>	$service_id,
												'client_contacts_id'=>	$client_contacts_id
											];

			$due_data = $this->db->get_where( "client_service_frequency_due" , $client_service_contact_cond )->row_array();


			$is_futuredue_date = is_futuredue_date($due_data['date'],$due_data['frequency']);

			if($is_futuredue_date)
			{
			
				$this->db->delete( "service_reminder_cron_data", $client_service_contact_cond );


				$firm_id		 			=	$due_data['firm_id'];

				if( $client_contacts_id != 0 && $service_id == 4 )
				{
					$service_Data	=	$this->db->select( "is_ptr_as_per_firm_reminder,is_ptr_cus_reminder,personal_tax_return_date,'Annually' as 'frequency_1',4" )
											->get_where( "client_contacts" , "has_ptr_service=1 AND id=".$client_contacts_id )
											->row_array();	
				}
				else
				{
					$Fields = $this->Get_Service_Fields( $service_id );			
					$service_Data 	 = $this->db->query( "SELECT ".$Fields." FROM client WHERE id=".$client_id )->row_array();
				}
				$service_Data 	 			=  	array_combine( $this->f_indexs , $service_Data );

				
				$due = date_format( date_create_from_format( 'd-m-Y' , $due_data['date'] ) , 'Y-m-d' );

				$service_Data['due_date'] = add_relative_date( $due , $due_data['frequency'] );

				$service_Data['duration'] = $due_data['frequency'];

				$Details['firm_id']  	= $firm_id;
				$Details['client_id']  	= $client_id;
				$Details['service_id']  = $service_id;
				$Details['client_contacts_id']  	= $client_contacts_id;
				
				$next_due = date( 'd-m-Y' , strtotime( $service_Data['due_date'] ) );

				$this->db->update('client_service_frequency_due' ,[ 'date'=> $next_due ,'is_create_task'=>0 ,'current_duration_task_id' => 0 ], "id=".$due_data['id'] );

				$this->update_due_date_if_change( $client_id , $service_id , $service_Data['due_date'] ,'', $client_contacts_id );

				//if due date is past task create for that priode
				if( !is_future_date( $next_due ) )
				{	
					// echo "due date is past so it comes inside codition\n";

					$service_due_data = $this->db->get_where('client_service_frequency_due' , 'id='.$due_data['id'] )
					                            ->row_array();

					$this->Create_ServiceTask( $service_due_data );
				}
		
				$this->Insert_ServiceReminder_CronData( $Details , $service_Data );

		        $this->setup_next_recurring_record( $Details['client_id'] , $Details['service_id']  , $client_contacts_id );
	        }		
		}
	}

	public function Create_ServiceTask( $reminder_data )
	{
		$client_service_contact_cond  = [
											'client_id' 		=> 	$reminder_data['client_id'] ,
											'service_id'		=>	$reminder_data['service_id'],
											'client_contacts_id'=>	$reminder_data['client_contacts_id']
										];

        $count_reminders = null;
		
		$current_due_data = $this->db->get_where( 'client_service_frequency_due' , $client_service_contact_cond )
							        ->row_array();

		$task_creatable_status 	=	$this->Common_mdl
		                                 ->get_field_value( 'admin_setting' , 'auto_create_task_status' , 'firm_id' , $reminder_data['firm_id'] );
		$task_creatable_status	=	json_decode( $task_creatable_status , true );
		
		$query 					=	"SELECT c.id,c.user_id,c.crm_company_name,c.firm_id,u.status 
									FROM client as c 
										INNER JOIN user u 
											ON u.id=c.user_id 
									WHERE c.id = ".$reminder_data['client_id'];

		$client_data = $this->db->query( $query )->row_array();

		

		if( $current_due_data['is_create_task'] != 1 && in_array( $client_data['status'],  $task_creatable_status ) )
		{
			$this->firm_exc_sr_log .= "inside task create able client status\n";

			$service = $this->db->select( 'service_name' )
			                    ->get_where( "service_lists","id = ".$reminder_data['service_id'] )
			                    ->row_array();

			$task_name = $service['service_name']."-".$current_due_data['date'];

			$service_due_date = date('Y-m-d', strtotime($current_due_data['date']));

			$check_due_task_exist 	=	"SELECT id FROM add_new_task 
													WHERE `company_name`=".$client_data['id']." AND client_contacts_id=".$reminder_data['client_contacts_id']." AND related_to_services=".$reminder_data['service_id']." AND subject LIKE '%".$task_name."'";
			// echo $check_due_task_exist;													

			$check_due_task_exist	=	$this->db->query( $check_due_task_exist )->num_rows();
			if( !$check_due_task_exist )
			{
				$query1 ="SELECT *  FROM `reminder_setting` 
              WHERE custom_reminder=0 AND firm_id IN (0,".$current_due_data['firm_id'].") AND service_id=".$current_due_data['service_id']." AND 
                id NOT IN ( SELECT super_admin_owned from reminder_setting WHERE firm_id=".$current_due_data['firm_id']." ) 
                ORDER BY due,days DESC";

                $reminder_setting_data = $this->db->query( $query1 )->result_array();

        		$count_reminders = count($reminder_setting_data);

				$progress_status = $this->Common_mdl->get_firm_progress( $client_data['firm_id'] );
				$progress_status = (!empty( $progress_status[0]['id'] ) ? $progress_status[0]['id'] : '' );

				$insert_task =[	
								'user_id'				=>	$client_data['user_id'],
								'company_name' 			=>  $client_data['id'],
								'client_contacts_id'	=>	$reminder_data['client_contacts_id'],
								'related_to_services'	=>	$reminder_data['service_id'],
								'subject'				=>	$client_data['crm_company_name']." ".$task_name,
								'service_due_date'		=>  $service_due_date,
								'firm_id'				=>  $client_data['firm_id'],
								'billable'				=>	'Billable',
								'progress_status'		=>	$progress_status,
								'task_status'			=>	'1',
								'related_to'			=>	'service_reminder',
								'start_date'			=> 	date( 'Y-m-d' ),
								'end_date'				=>  date( 'Y-m-d', strtotime('+ 10 days') ),
								'created_date'			=> 	time(),
								'email_count'			=>  $count_reminders,		
							  ];						

				$this->db->insert( 'add_new_task', $insert_task );

				$Task_id =  $this->db->insert_id();

				$this->db->update( 'client_service_frequency_due' , [ 'is_create_task' => 1 ,'current_duration_task_id' => $Task_id ] , $client_service_contact_cond );



				$this->Common_mdl->task_setting( $Task_id );
				
				$this->load->model('Client_model');
				$this->Client_model->set_task_assignee_by_client_service( $Task_id , $client_data['id'] , $reminder_data['service_id'] );
				/*CREATE SUB TASK*/
				$this->Create_Sub_task( $Task_id );
				/*CREATE SUB TASK*/
	           
				$log = "<b class='text-success'>".$insert_task['subject']."</b></br> Auto Task Created";
				$this->Common_mdl->Create_Log( 'client' , $client_data['user_id'] , $log );
				$this->Common_mdl->Create_Log( 'task' , $Task_id , $log );
				
				/*Send Notification*/		
				if ( !class_exists( 'Task_invoice_model' ) )
					$this->load->model( 'Task_invoice_model' );
				
				$this->Task_invoice_model->task_new_mail( $Task_id , 'for_add' , 1 );
				
				/*End Send  Notification*/

				return $Task_id;		
			}
			else
			{
				$this->firm_exc_sr_log .= "task already exists for this due\n";
				// echo "task already exists for this due\n";
			}
		}
	
	}

	public function Create_Sub_task( $Main_Task_id, $subject = null)
	{	
		// echo "Inside Create_Sub_task() \n";

		if( !class_exists('Service_model') ) 
			$this->load->model('Service_model');
			
		if( !class_exists('Task_section_model') )			
			$this->load->model('Task_section_model');
			
		
		$this->db->where('id' , $Main_Task_id );
		$data 		= $this->db->get('add_new_task')->row_array();
		//print_r( $data );
		$service_id = $data['related_to_services'];
		$service_id = str_replace("ser_", "", $service_id);

		$service_steps 			= $this->Service_model->get_steps( $service_id , 'service_id' , $data['firm_id'] );
		//$service_steps_details 	= $this->Service_model->get_sub_steps( $service_id , 'service_id' , $data['firm_id'] );
		$Sub_task_ids 			=	[];

		if($subject){
			$Sub_task_ids[] = $this->Task_section_model->task_creation( $Main_Task_id , $subject , 0 ,'');
		}
		else{
			foreach ($service_steps as $key => $step)
			{
				//echo $Main_Task_id.$step['title'].$step['id'];

				$Sub_task_ids[] = $this->Task_section_model->task_creation( $Main_Task_id , $step['title'] , $step['id'] ,'');
				
				/*foreach ($service_steps_details as $key => $steps_details)
				{
					$Sub_task_ids[] = $this->Task_section_model->task_creation( $Main_Task_id , $steps_details['step_content'] , $step['id'] , $steps_details['id']);
				}*/

			}
		}

		if( !empty( $Sub_task_ids ))
		{
			if($subject){
				$task_data = $this->db->query('SELECT * FROM add_new_task WHERE id='.$Main_Task_id)->row_array();
				$existing_sub_task_ids = $task_data['sub_task_id'];
				if($existing_sub_task_ids){
					$existing_sub_task_ids = $existing_sub_task_ids.','.implode( ',' , $Sub_task_ids );
				}
				else{
					$existing_sub_task_ids = implode( ',' , $Sub_task_ids );
				}
				$up['sub_task_id'] = $existing_sub_task_ids; 
				$this->db->update('add_new_task', $up ,'id='.$Main_Task_id );
			}
			else{
				$up['sub_task_id'] = implode( ',' , $Sub_task_ids ); 
	        	$this->db->update('add_new_task', $up ,'id='.$Main_Task_id );
			}
		}
	}
	/*CRON TIME Executing Scripts*/ 

	public function update_due_date_if_change( $client_id , $service_id , $new_d , $old_d ='' , $client_contacts_id = 0 )
	{
		if($new_d != $old_d )
		{	
			//check is it for ccp service
			if( $client_contacts_id != 0 && $service_id == 4 )
			{	
				$this->db->update( "client_contacts" , [ 'personal_tax_return_date' => $new_d ] , "id=".$client_contacts_id );
				$Fields = $this->Get_Service_Fields( $service_id );
			
				$due_date_field = explode(',', $Fields );
				$due_date_field = trim( $due_date_field[2] );

				$this->db->update( 'client' ,[ $due_date_field => $new_d ],"id=".$client_id );
			}
			else
			{
				$Fields = $this->Get_Service_Fields( $service_id );
			
				$due_date_field = explode(',', $Fields );
				$due_date_field = trim( $due_date_field[2] );

				$this->db->update( 'client' ,[ $due_date_field => $new_d ],"id=".$client_id );
			}
		}
	}
	

	public function Setup_Client_ServiceReminders( $client_id , array $Edited_service )
	{		
		/*echo "<pre> service_ids ";
		print_r( $Edited_service );*/
			
		foreach ( $Edited_service as $service_id => $e )
		{
			/*echo "\n inside service foreach \n";*/
			$Service_Fields =	$this->Get_Service_ReminderFields[ $service_id ];			

			if( !empty( $Service_Fields ) )
			{
				$Service_Fields  = implode( ',' ,  $Service_Fields );

				//echo "\n service Fields".$Service_Fields."\n";

				$service_data = $this->db->query( "SELECT ".$Service_Fields." FROM client WHERE id=".$client_id )->row_array();

				$service_data = array_combine( $this->f_indexs , $service_data );

				//echo "service value \n";
				//print_r( $service_data );

				$this->generate_reminders( $client_id , $service_id , $service_data );
			}
		}
	}

	
	/*
		create PTR service reminders to enabled ccp
		@ $client_id 
		@ $service_id 4_ccpid (to find which ccp enabled service) 
	*/
	public function Setup_ClientContactPerson_ServiceReminders( $user_id , $services_id )
	{
		echo "Edited_service service";
		//print_r( $service_id );
		
		$contact_persons	=	$this->db->select( "id,is_ptr_as_per_firm_reminder,is_ptr_cus_reminder,personal_tax_return_date,'Annually' as 'frequency_1',4" )
						->get_where( "client_contacts" , "has_ptr_service=1 AND personal_tax_return_date!='' AND client_id=".$user_id )
						->result_array();
		foreach ( $contact_persons as $contact_person ) {

			$client_contacts_id 	=	array_shift( $contact_person );

			if(!empty( $services_id ) && in_array( '4_'.$client_contacts_id ,  $services_id ) )
			{
				echo "<pre>client_contacts=".$client_contacts_id."service enabled\n";

				$service_data 	=	array_combine( $this->f_indexs , $contact_person );
				
				$client_id 			=	$this->Common_mdl->get_field_value( 'client' , 'id' , 'user_id' , $user_id );

				//4 reffre to ptr
				$this->generate_reminders( $client_id , 4 , $service_data , $client_contacts_id );
			}
		}
	}

	public function generate_reminders( $client_id , $service_id , $service_data , $client_contacts_id = 0 )
	{
		$firm_id = $this->db->select('firm_id')
		                    ->get_where( 'client' ,'id='.$client_id )
		                    ->row('firm_id');

		$service_client_ccp_cond = " client_id = ".$client_id." and service_id =".$service_id." and client_contacts_id=".$client_contacts_id;
		
		$this->db->delete("service_reminder_cron_data", $service_client_ccp_cond );
		$this->db->delete("draft_service_reminder"  , $service_client_ccp_cond );
		//if due date is empty it means service off
		if( empty( $service_data['due_date'] ) )
		{
			$this->db->delete("client_service_frequency_due"  , $service_client_ccp_cond );
		}	
			

		//date & duration must not be empty
		if( $service_data['due_date'] !='' && $service_data['duration']!='' )
		{ 
			//it will return future date if date is too past (not for yearly duration )
			$data    = Next_Frequency( $service_data['duration'] , $service_data['due_date'] );

			$this->update_due_date_if_change( $client_id , $service_id , $data['due_date'] , $service_data['due_date'] , $client_contacts_id );


			$check_Frequency_data = $this->db->get_where("client_service_frequency_due", $service_client_ccp_cond )->row_array();

			$frequency_data = [
					'client_id'  				=> 	$client_id,
					'client_contacts_id'		=>	$client_contacts_id,
					'service_id'				=> 	$service_id,
					'date'						=> 	date( 'd-m-Y' , strtotime( $data['due_date'] ) ),
					'frequency'					=> 	$data['duration'],
					'firm_id'					=> 	$firm_id,
					'is_create_task' 			=> 	0,
					'current_duration_task_id'	=>	0
				];

			if( !empty( $check_Frequency_data ) )
			{
				if( $data['due_date'] != $check_Frequency_data['date'] )
				{	
					//echo "inside frequency already exit function\n";

					//print_r( $frequency_data );

					$this->db->update("client_service_frequency_due" , $frequency_data ,'id='.$check_Frequency_data['id']);
				}
			}
			else
			{
				//echo "inside frequency insert function\n";						

				$this->db->insert( "client_service_frequency_due" , $frequency_data );
			}

			//if due date is past task create for that priode
			if( !is_future_date( $frequency_data['date'] ) )
			{	
				//echo "due date is past so it comes inside codition\n";	
				$this->Create_ServiceTask( $frequency_data );
			}

			$service_data['due_date'] 		=	$data['due_date'];
			$service_data['duration'] 		=	$data['duration'];
			$Details['firm_id'] 			=	$firm_id;
			$Details['client_id'] 			=	$client_id;
			$Details['service_id'] 			=	$service_id;
			$Details['client_contacts_id']  =	$client_contacts_id;
			
			$this->Insert_ServiceReminder_CronData( $Details , $service_data );			

	        $this->setup_next_recurring_record( $Details['client_id'] , $Details['service_id'] , $client_contacts_id );	
		}
		
	}

	/*
	@used to get service reminders
	$Details parameter value differ depent on second parameter
	$Type = 1 means custom reminder it required client_id,service_id,client_contact_id
	$Type =  measn defult reminder it required firm_id,service_id
	*/
	public function get_service_reminders( $Details , $Type =0 )
	{
		if( $Type == 1)
		{
			$query = "SELECT id,due_type,due_days,'1' as is_custom FROM custom_service_reminder 
						WHERE client_id=".$Details['client_id']." AND service_id =".$Details['service_id']." AND client_contacts_id=".$Details['client_contacts_id'];
		}
		else
		{
			$query ="SELECT id,due AS due_type,days AS due_days,'0' as is_custom  FROM `reminder_setting` 
							WHERE custom_reminder=0 AND firm_id IN (0,".$Details['firm_id'].") AND service_id=".$Details['service_id']." AND 
								id NOT IN (	SELECT super_admin_owned from reminder_setting WHERE firm_id=".$Details['firm_id']." ) 
								ORDER BY due,days DESC";
			
		}

		return $this->db->query( $query )->result_array();
	}

	public function Insert_ServiceReminder_CronData( array $Details ,array $service_Data )
	{			
		// echo "inside Insert_ServiceReminder_CronData()\n";

		if( $service_Data['default_r'] != '' && $service_Data['due_date']!='')
		{	
			$Reminders = $this->get_service_reminders( $Details );
		}
		else if( $service_Data['custom_r'] != '' && $service_Data['due_date']!='' )
		{   		
			$Reminders = $this->get_service_reminders( $Details , 1);
		}

		// echo "reminder row count".count( $Reminders )."\n"; 

		if( !empty( $Reminders ) )
		{
			foreach($Reminders as $value)
			{
				$Date = Calculate_Due( $service_Data['due_date'] , $value['due_type'] , $value['due_days'] );

				$data =[
							'firm_id' 		=> $Details['firm_id'],
							'client_id' 	=> $Details['client_id'],
							'service_id' 	=> $Details['service_id'],
							'client_contacts_id'		=> $Details['client_contacts_id'],
							'template_id' 	=> $value['id'],
							'template_type' => $value['is_custom'],
							'frequency'		=> $service_Data['duration'],
							'date' 			=> $Date
						];
				if( is_future_date( $Date ) )
				{
					$this->db->insert( 'service_reminder_cron_data', $data );			 
				}
				else
				{
					$this->db->insert( 'draft_service_reminder', $data );		
				}
			}
		}	
	}

	public function setup_next_recurring_record( $client_id , $service_id , $client_contacts_id = 0)
	{
		// echo "setup_next_recurring_record()\n";

		//Acc && Conf only Should sync Ch
		if( $service_id !=1 && $service_id != 2 )
		{
			$client_service_ccp_cond = " client_id= ".$client_id." AND service_id=".$service_id."  AND client_contacts_id=".$client_contacts_id;

			$this->db->update( 'service_reminder_cron_data' , [ 'create_records' => 0 ] , $client_service_ccp_cond );

			$data = $this->db->get_where( "client_service_frequency_due" , $client_service_ccp_cond )->row_array();

			$Cron_data = $this->db->get_where("service_reminder_cron_data" , $client_service_ccp_cond )->result_array();

			$Cron_date = array_column( $Cron_data , 'date' , 'id');

			/*
				echo "1=======================";
				print_r( $Cron_date );
			*/

			$create_records_id = find_last_date( $data['date'] , $Cron_date );

			// echo "create_records_id==".$create_records_id;

			if( $create_records_id != 0 && is_future_date( $Cron_date[ $create_records_id ] ) )
			{	
				$this->db->update( 'service_reminder_cron_data' , ['create_records' => 1] , "id=".$create_records_id );
			}
			else if( !is_future_date( $data['date'] ) )
			{
				$this->Setup_Next_Frequency_Data( $client_id , $service_id , $client_contacts_id );
			}
		}
	}
	public function get_reminder_template_data( $client_id , $template_id , $is_custom )
	{
		if( $is_custom !='0' )
		{
			$Template = $this->db->query("SELECT subject,body,headline_color FROM custom_service_reminder WHERE id=".$template_id)->row_array();
		}
		else
		{
			$Template = $this->db->query("SELECT subject,message as body,headline_color  FROM reminder_setting WHERE id=".$template_id)->row_array();
		}
					
		/*$Color_code = 'color:#111010';

		if( $Template['headline_color'] )
		{
			$Color_code = "color:".$Template['headline_color'];
		}		
		$heading = "<b style='".$Color_code."'>Service Reminder</b></br>";*/

		$Content['subject'] = $Template['subject'];
		$Content['body']    = $Template['body'];

		if( ! function_exists('Decode_ClientTable_Datas') )
			$this->load->helper('template_decode');

		$client_contacts_id = 0 ;
		
		if( is_array( $client_id ) )
		{	
			$client_contacts_id = 	$client_id['client_contacts_id'];
			$client_id   		=	$client_id['client_id'];			
		}

		

		$DATA = Decode_ClientTable_Datas( $client_id , $Content , $client_contacts_id );

		

		return $DATA;
	}
	
/*
	public function QuartersBase_ServiceReminder( $client_id, $service_id ,array $service_Data)	
	{
		if( !empty( $service_Data[2] ) )
		{			
			$array = $this->Get_Months( $service_Data[2] );
			foreach ($array as $key => $value)
			{
				$Due_Date =  strtotime( date('Y').'-'.$value.'-01' );
				$service_Data[2] =  date( 'Y-m-t' , $Due_Date );
				
				$this->Insert_ServiceReminder_CronData( $client_id , $service_id , $service_Data );
			}
		}

	}*/
	

	
	public function Add_NewServiceReminder_Cron($Tem_id)
	{
		$TEM_DATA = $this->db->query("SELECT * FROM reminder_setting WHERE id=".$Tem_id)->row_array();
		$FIRM_CON = ( $TEM_DATA['firm_id'] != 0 ? " and firm_id = ".$TEM_DATA['firm_id'] : " " );

		if( $TEM_DATA['service_id']<= 15 || $TEM_DATA['service_id']>16 )
		{
			if( $TEM_DATA['service_id'] == 4 )
			{	
				$is_super_admin = ( $TEM_DATA['firm_id'] != 0 ? " and c.firm_id = ".$TEM_DATA['firm_id'] : " " );

				$query			=	"SELECT c.id,cc.id as client_contacts_id,c.firm_id,cc.personal_tax_return_date 
										FROM client_contacts as cc 
										INNER JOIN client as c 
										ON c.user_id = cc.client_id ".$is_super_admin." 
										WHERE cc.is_ptr_as_per_firm_reminder!='' and cc.has_ptr_service !=0";
			}
			else
			{

				$Fields = $this->Get_Service_ReminderFields();	
				$Fields = array_chunk( explode( ',' , $Fields ) , 5 );

				if( $TEM_DATA['service_id'] <= 15 )
				{
				  $Fields = $Fields[ $TEM_DATA['service_id']-1 ];			  
	            }
	            /*else if($TEM_DATA['service_id']>16)
				{ 
				  foreach($Fields as $key => $value) 
				  {
				  	if($value[4] != $TEM_DATA['service_id'])
				  	{ 
				  	   unset($Fields[$key]);
				  	}
				  	else
				  	{
	                   $Fields = $value;
				  	}
				  }
	            }*/			
				
				$query = "SELECT id,firm_id,'0' as client_contacts_id,".$Fields[2]." as E_date FROM client WHERE ".$Fields[0]."!='' and ".$Fields[2]." !='' ".$FIRM_CON;
			} 

			$clients = $this->db->query($query)->result_array();
			foreach( $clients as $key => $value )
			{	
				$data = $this->db->query("SELECT * FROM client_service_frequency_due WHERE client_id= ".$value['id']." AND service_id=".$TEM_DATA['service_id']." AND client_contacts_id=".$value['client_contacts_id'])->row_array();
				/*	 */ 
				if( empty($data))continue;

				$date = date_format( date_create_from_format('d-m-Y', $data['date'] ) ,'Y-m-d' );

				$Cron_D = Calculate_Due( $date , $TEM_DATA['due'] , $TEM_DATA['days'] );

					$data = [
								'firm_id'   		=> 	$value['firm_id'],
								'client_id' 		=> 	$value['id'],
								'service_id'		=> 	$TEM_DATA['service_id'],
								'template_id'		=> 	$TEM_DATA['id'],
								'date'       		=> 	$Cron_D,					
								'status' 			=> 	0,
								'frequency'			=> 	$data['frequency'],	
								'client_contacts_id'=>	$value['client_contacts_id']
							];
					$this->db->insert( 'service_reminder_cron_data' , $data );	
					$this->setup_next_recurring_record( $value['id'] , $TEM_DATA['service_id'] , $value['client_contacts_id'] );	
			}		
		}
		
	}	      
	

	public function Edit_ExistServiceReminder_Cron($Tem_id)
	{ 
		$TEM_DATA = $this->db->query("SELECT * FROM reminder_setting WHERE id=".$Tem_id)->row_array(); 

		if( $TEM_DATA['service_id'] == 4 )
		{
			$query = "SELECT sc.id,sc.client_id,sc.client_contacts_id,cc.personal_tax_return_date as E_date FROM 			service_reminder_cron_data AS sc 
						INNER JOIN client_contacts AS cc on cc.id = sc.client_contacts_id 
							WHERE sc.template_id=".$TEM_DATA['id']." and sc.template_type=0";
		}
		else
		{			
			$Fields = $this->Get_Service_ReminderFields();	
			$Fields = array_chunk( explode( ',' , $Fields ) , 5 );	

			if($TEM_DATA['service_id'] <= 15)
			{
			  $Fields = $Fields[ $TEM_DATA['service_id']-1 ];			  
            }
            else if($TEM_DATA['service_id']>16)
			{ 
			  foreach($Fields as $key => $value) 
			  {
			  	if($value[4] != $TEM_DATA['service_id'])
			  	{ 
			  	   unset( $Fields[$key] );
			  	}
			  	else
			  	{
                   $Fields = $value;
			  	}
			  }
            }

			$query = "SELECT sc.id,sc.client_id,'0' as client_contacts_id,c.".$Fields[2]." as E_date FROM 			service_reminder_cron_data AS sc 
						INNER JOIN client AS c on c.id = sc.client_id 
							WHERE sc.template_id=".$TEM_DATA['id']." and sc.template_type=0";
		}


		$CLIENTS = $this->db->query( $query )->result_array();

		foreach ( $CLIENTS as $key => $value )
		{		
			$data = $this->db->query("SELECT * FROM client_service_frequency_due WHERE client_id= ".$value['client_id']." AND service_id=".$TEM_DATA['service_id']." AND client_contacts_id=".$value['client_contacts_id'])->row_array();
			
			if( empty( $data ) ) continue;
			
			$frequency_date = date_format( date_create_from_format('d-m-Y', $data['date'] ) ,'Y-m-d' );

			$date = Calculate_Due( $frequency_date , $TEM_DATA['due'] , $TEM_DATA['days'] );

			$this->db->update( 'service_reminder_cron_data',['date'=>$date],"id=".$value['id'] );
			
			$this->setup_next_recurring_record( $value['client_id'] , $TEM_DATA['service_id'] , $value['client_contacts_id'] );
		}
		
	}	

	/*
	@From_id is super_admin_owned id
	@To_id  is firm Newly created one.
	*/
	public function Shift_ServiceReminders_Cron( $TEM_id)
	{
		$New_Temp = $this->db->query("SELECT * FROM reminder_setting WHERE id=".$TEM_id)->row_array();

		$this->db->query("UPDATE service_reminder_cron_data SET  template_id=".$New_Temp['id']." WHERE template_type=0 and template_id=".$New_Temp['super_admin_owned']." and firm_id=".$New_Temp['firm_id']);
	}

	Public function Delete_ServiceReminder_Cron($temp_id)
	{
		$this->move_recorde_creation( $temp_id );
		$this->db->delete( "service_reminder_cron_data" , "template_type=0 and template_id=".$temp_id );
		return 1;
	}

	public function move_recorde_creation( $id )
	{
		$data = $this->db->query("SELECT * FROM service_reminder_cron_data WHERE create_records=1 AND template_type=0 and template_id=".$id)->result_array();

		foreach ($data as $key => $value)
		{	
			$this->db->delete( 'service_reminder_cron_data' , 'id='.$value['id'] );
			$this->setup_next_recurring_record( $value['client_id'] , $value['service_id'] , $value['client_contacts_id']);
		}
	}

	Public function Reminder_Status_Change ( $task_id )
	{
       	$task_data = $this->Common_mdl->select_record( 'add_new_task' , 'id' , $task_id );

		$this->db->where( 'firm_id' , $task_data['firm_id'] );
		$settings = $this->db->get('admin_setting')->row_array();

		if( !empty( $settings['reminder_stop_status'] ) )
		{
			$stop_status = json_decode( $settings['reminder_stop_status'] , true );
			if( is_array( $stop_status ) && in_array( $task_data['progress_status'] , $stop_status ) )
			{
				$task_data['company_name'] = intval($task_data['company_name']);
				$task_data['related_to_services'] = intval($task_data['related_to_services']);
				$where = [
					'client_id'  			=> $task_data['company_name'],
					'client_contacts_id'	=> $task_data['client_contacts_id'],
					'service_id' 			=> $task_data['related_to_services']
				];
				
				$client_service_frequency_due  = $this->db->select( 'frequency' )
												       ->where( $where )
												       ->get( 'client_service_frequency_due' )	
												       ->row();

		        $frequency = $client_service_frequency_due->frequency;

		        $check_days = 5;

				if($frequency == '1 month')
				{
					$check_days = 15;
				}
				elseif($frequency == '3 Months')
				{
					$check_days = 75;
				}
				elseif($frequency == '1 Year')
				{
					$check_days = 350;
				}
				elseif($frequency == '7 days')
				{
					$check_days = 5;
				}
				//echo $this->db->last_query()."query";
				//if( $current_duration_task_id == $task_data['id'] )
				//{
					$where = [
						'client_id'  			=> $task_data['company_name'],
						'client_contacts_id'	=> $task_data['client_contacts_id'],
						'service_id' 			=> $task_data['related_to_services']
					];
					$start_date = $task_data['start_date'];
					$end_date = date('Y-m-d', strtotime($start_date. ' + '.$check_days.' days'));
					$timestamp = strtotime($start_date);
					$start_date = date("d-m-Y", $timestamp);
					$timestamp = strtotime($end_date);
					$end_date = date("d-m-Y", $timestamp);
					// $this->db->update( 'service_reminder_cron_data' , [ "status" => 2 ] , $where );
					$query1 = "UPDATE service_reminder_cron_data set status=2 WHERE client_id = ".$task_data['company_name']." AND client_contacts_id=".$task_data['client_contacts_id']." AND service_id=".$task_data['related_to_services']." AND (UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) BETWEEN UNIX_TIMESTAMP(STR_TO_DATE(  '".$start_date."' ,'%d-%m-%Y' )) AND UNIX_TIMESTAMP(STR_TO_DATE(  '".$end_date."' ,'%d-%m-%Y' )))";

					$this->db->query( $query1 );
				//}
			}else{
				
				$where = [
					'client_id'  			=> $task_data['company_name'],
					'client_contacts_id'	=> $task_data['client_contacts_id'],
					'service_id' 			=> $task_data['related_to_services']
				];
				
				
				$current_duration_task_id  = $this->db->select( 'current_duration_task_id' )
												       ->where( $where )
												       ->get( 'client_service_frequency_due' )					
													   ->row( 'current_duration_task_id' );
				//echo $this->db->last_query()."query";
				// if( $current_duration_task_id == $task_data['id'] )
				// {
					$where = [
						'client_id'  			=> $task_data['company_name'],
						'client_contacts_id'	=> $task_data['client_contacts_id'],
						'service_id' 			=> $task_data['related_to_services']
					];
					$this->db->update( 'service_reminder_cron_data' , [ "status" => 0 ] , $where );
				//}
				
			}
		}
		return  $this->db->affected_rows();
	}

	public Function Get_Service_ReminderFields()
	{
		$feilds = "
		crm_confirmation_email_remainder,crm_confirmation_add_custom_reminder,crm_confirmation_statement_date,'Annually' as 'frequency_1',1,
		crm_accounts_next_reminder_date,crm_accounts_custom_reminder,crm_hmrc_yearend,'Annually' as 'frequency_2',2,
		crm_company_next_reminder_date,crm_company_custom_reminder,crm_accounts_due_date_hmrc,'Annually' as 'frequency_3',3,
		crm_personal_next_reminder_date,crm_personal_custom_reminder,crm_personal_tax_return_date,'Annually' as 'frequence_4',4,
		crm_vat_next_reminder_date,crm_vat_add_custom_reminder,crm_vat_quater_end_date,crm_vat_quarters,5,
		crm_payroll_next_reminder_date,crm_payroll_add_custom_reminder,crm_payroll_run_date,crm_payroll_run,6,
		crm_pension_next_reminder_date,crm_pension_add_custom_reminder,crm_pension_subm_due_date,'Annually' as 'frequency_5',7,
		crm_cis_next_reminder_date,crm_cis_add_custom_reminder,crm_cis_contractor_start_date,crm_cis_frequency,8,
		crm_cissub_next_reminder_date,crm_cissub_add_custom_reminder,crm_cis_subcontractor_start_date,crm_cis_subcontractor_frequency,9,
		crm_bookkeep_next_reminder_date,crm_bookkeep_add_custom_reminder,crm_next_booking_date,crm_bookkeeping,10,
		crm_p11d_next_reminder_date,crm_p11d_add_custom_reminder,crm_next_p11d_return_due,'Annually' as 'frequency_7',11,
		crm_manage_next_reminder_date,crm_manage_add_custom_reminder,crm_next_manage_acc_date,crm_manage_acc_fre,12,
		crm_insurance_next_reminder_date,crm_insurance_add_custom_reminder,crm_insurance_renew_date,'Annually' as 'frequency_8',13,
		crm_registered_next_reminder_date,crm_registered_add_custom_reminder,crm_registered_renew_date,'Annually' as 'frequency_9',14,
		crm_investigation_next_reminder_date,crm_investigation_add_custom_reminder,crm_investigation_end_date,'Annually' as 'frequency_10',15";
		/*$records = $this->db->query('SELECT services_subnames,id FROM service_lists WHERE id>16')->result_array();
        $count = 11;
		foreach ($records as $key => $value) 
		{
		   $feilds .= ",crm_".$value['services_subnames']."_email_remainder,crm_".$value['services_subnames']."_add_custom_reminder,crm_".$value['services_subnames']."_statement_date,'Annually' as 'frequency_".$count."',".$value['id']."";
		   $count++;		   
		}*/
		
		return $feilds;
	}
	
	public function Get_Service_Fields( $id )
	{
		$Fields = $this->Get_Service_ReminderFields();		
		$Fields = array_chunk( explode( ',' , $Fields ) , 5 );
		$Fields =  implode( ',' , $Fields[ $id-1 ] );			  
		/*if($id <= 15)
		{
        }
        else if($id>16)
		{ 
		  foreach($Fields as $key => $value) 
		  {
		  	if($value[4] != $id)
		  	{ 
		  	   unset($Fields[$key]);
		  	}
		  	else
		  	{
               $Fields = $value;
		  	}
		  }
		  $Fields =  implode(',' , $Fields);
        }*/
		return $Fields;
	}
	/*public function update_vat_lastquater( $service_id , $date , $client_id )
	{	
		if( $service_id == 5 )
		{
			
			$cli['crm_vat_quater_end_date'] = date_format( date_create_from_format( 'd-m-Y' , $date ) , 'Y-m-d' );

			$this->db->update( 'client' , $cli , 'id='.$client_id );
		}
	}*/

	public function check_auto_task_status_update_reminders( $firm_id )
	{
		$service_ids = range( 1, 15 );
		
		$stop_status = $this->db->select( 'reminder_stop_status' )
								 ->where( 'firm_id' , $firm_id )
								 ->get( 'admin_setting' )
								 ->row('reminder_stop_status');

		$stop_status  = json_decode( $stop_status , true );

		if( $stop_status )
		{
			$stop_status = implode( ',' , array_filter( $stop_status ) );
			
			//echo "Reminder stop status=".$stop_status."\n";

			foreach ( $service_ids as $key => $service_id) {
			
			//echo "Service_id=".$service_id."\n";
			
			$condtion ="firm_id=".$firm_id." AND service_id=".$service_id." AND is_create_task=1 AND current_duration_task_id!=0";

			$task_ids = $this->db->select( "current_duration_task_id" )
			 					 ->where( $condtion )
						         ->get( "client_service_frequency_due" )
			 					 ->result_array();

			$task_ids = implode( ',' , array_column( $task_ids , 'current_duration_task_id' ) );

			if( !empty( $task_ids ) )
			{
				$query = "SELECT id,company_name,client_contacts_id,subject,progress_status FROM add_new_task 
					WHERE id IN (".$task_ids.") AND progress_status IN ( ".$stop_status." )";

				//echo "Query = ".$query."\n";
				$data = $this->db->query( $query )->result_array();

					//echo "<pre>";
				
					foreach ( $data as $key => $task ) {
						
						//print_r( $task );

						//echo "cron loop=".$task['company_name'];

						//$query0 = "SELECT * FROM service_reminder_cron_data WHERE status=0 AND client_id = ".$task['company_name']." AND client_contacts_id=".$task['client_contacts_id']." AND service_id=".$service_id;

						//print_r(  $this->db->query( $query0 )->result_array()  );

						$query1 = "UPDATE service_reminder_cron_data set status=2 WHERE status=0 AND client_id = ".$task['company_name']." AND client_contacts_id=".$task['client_contacts_id']." AND service_id=".$service_id;

						$this->db->query( $query1 );
					}
					//echo "=======================================";
				}			
			}
		}
	}
	public function update_reminder_status( $queue_mail_data )
	{
		if( $queue_mail_data['status'] == 1 )
          {
          	$client_id = $this->db->get_where('service_reminder_cron_data' , "id=".$queue_mail_data['relational_module_id'] )->row('client_id');
          	
            $this->db->update("service_reminder_cron_data", ['status'=>1] , "id=".$queue_mail_data['relational_module_id'] );

            if(is_null($client_id)!=1){
            	$client_reminders = $this->db
                  ->where( "client_id=".$client_id )
                  ->limit(50)
                  ->get( "client_reminder" )
                  ->result_array();            
            	$check_flag = 0;
            	foreach ($client_reminders as $reminder) {
            		if($reminder['reminder_subject'] == $queue_mail_data['subject'])
            		{
            			$check_flag = 1;
            		}
            	}
            	if($check_flag == 0)
            	{
            		$this->db->insert( "client_reminder" , ['client_id' => $client_id,'reminder_subject' => $queue_mail_data['subject'],"reminder_message" => $queue_mail_data['content'] ] );
            	}
            }
            

            $log = "<b class='text-success'>".$queue_mail_data['subject']."</b></br> Service Reminder Sent SuccessFully.";

            $user_id = $this->Common_mdl->get_field_value( 'client' , 'user_id' , 'id' , $client_id );
            $this->Common_mdl->Create_Log('client' , $user_id , $log );
          }
          else if( $queue_mail_data['status'] == 2 )
          {
            $this->db->update( "service_reminder_cron_data" , [ 'status' => 5 ] , "id=".$queue_mail_data['relational_module_id'] );
          }
	}

	
	public function get_service_details_array( $service_field_value )
	{	
		$return  = ["tab"=>'',"reminder"=>'',"text"=>'',"invoice"=>''];
                           
	    if( gettype( $service_field_value ) == "string" &&  !empty( json_decode( $service_field_value  , true ) ) )
	    {
	        $return  = array_replace( $return, json_decode( $service_field_value  , true ) );
	    }
	    return $return;
	}
	public function setup_client_service_activity_log( $old_data , $user_id )
	{	
		$services_field	=	['conf_statement','accounts','company_tax_return','personal_tax_return','vat','payroll','workplace','cis','cissub','bookkeep','p11d','management','investgate','registered','taxinvest','taxadvice'];
		
		if( empty( $old_data ) )
		{
			$old_data = array_fill_keys( $services_field , '' );
		}

		$updated_data =	$this->db->select( implode( ',' ,  $services_field ) )
								 ->get_where( 'client' , 'user_id='.$user_id )
								 ->row_array();

		foreach ( $services_field as $service_id => $field_name ) {

			$service_id 	+= 1;
			$service_name 	=	$this->db->select( 'service_name' )
										->get_where( 'service_lists' , 'id='.$service_id )
										->row( 'service_name' );

			//echo "service name = ".$service_name."\n";

			$old_service_data 	=	$this->get_service_details_array( $old_data[ $field_name ] );
			
			//echo "old service data\n";
			//print_r( $old_service_data );

			$new_service_data	=	$this->get_service_details_array( $updated_data[ $field_name ] );
			//echo "new service data";
			//print_r( $new_service_data );

			$highlight_text			=	'';

			if( empty( $old_service_data['tab'] ) && !empty( $new_service_data['tab'] ) )
			{
				$highlight_text 	.=	"<b class='text-success'>".$service_name." : </b><b> Enabled</b></br>";
			}
			else if( !empty( $old_service_data['tab'] ) && empty( $new_service_data['tab'] ) )
			{
				$highlight_text 	.=	"<b class='text-danger'>".$service_name." : </b><b> Disabled</b></br>";
			}


			if( empty( $old_service_data['reminder'] ) && !empty( $new_service_data['reminder'] ) )
			{
				$highlight_text 	.=	"<b class='text-success'>".$service_name." Reminder : </b><b> Enabled</b></br>";
			}
			else if( !empty( $old_service_data['reminder'] ) && empty( $new_service_data['reminder'] ) )
			{
				$highlight_text 	.=	"<b class='text-danger'>".$service_name." Reminder :  </b><b> Disabled</b></br>";
			}
			
			//echo "text=".$highlight_text;

			if( !empty( $highlight_text ) )
			{
            	$this->Common_mdl->Create_Log( 'client' , $user_id , $highlight_text );
			}
		}
	}
	public function setup_client_contact_service_activity_log( $old_datas , $user_id )
	{	


		$new_datas	=	$this->db->order_by('id')
								->get_where( 'client_contacts' , 'client_id='.$user_id )
								->result_array();


		foreach ( $new_datas as $key => $new_data ) {
			
			$old_data 		=	!empty( $old_datas[ $key ] ) ? $old_datas[ $key ] : [];

			$service_name 	= 	$new_data['first_name'].''.$new_data['last_name'].' - Personal Tax Return';
						
			$highlight_text			=	'';

			if( empty( $old_data['has_ptr_service'] ) && !empty( $new_data['has_ptr_service'] ) )
			{
				$highlight_text 	.=	"<b class='text-success'>".$service_name." : </b><b> Enabled</b></br>";
			}
			else if( !empty( $old_data['has_ptr_service'] ) && empty( $new_data['has_ptr_service'] ) )
			{
				$highlight_text 	.=	"<b class='text-danger'>".$service_name." : </b><b> Disabled</b></br>";
			}


			if( empty( $old_data['has_ptr_reminder'] ) && !empty( $new_data['has_ptr_reminder'] ) )
			{
				$highlight_text 	.=	"<b class='text-success'>".$service_name." Reminder : </b><b> Enabled</b></br>";
			}
			else if( !empty( $old_data['has_ptr_reminder'] ) && empty( $new_data['has_ptr_reminder'] ) )
			{
				$highlight_text 	.=	"<b class='text-danger'>".$service_name." Reminder :  </b><b> Disabled</b></br>";
			}
			
			//echo "text=".$highlight_text;

			if( !empty( $highlight_text ) )
			{
            	$this->Common_mdl->Create_Log( 'client' , $user_id , $highlight_text );
			}
		}
	}

	// public function Get_Sending_Data_test_run( $Firm_Data )
	// {		
	// 	//echo "Inside Get_Sending_Data()";

	// 	$return_data=0;

	// 	$this->firm_exc_sr_log	.= "Inside Get_Sending_Data()\n";


	// 	$reminders = $this->db->where( "firm_id=".$Firm_Data['firm_id']  . " AND date='06-10-2020'")
	// 	                      ->get( "service_reminder_cron_data" )
	// 	                      ->result_array();
		
	// 	$this->firm_exc_sr_log .= "Today service reminders\n".json_encode( $reminders )."\n";

	// 	//print_r($reminders);

	// 	$RETURN = [];
	// 	$ii = 1;
	// 	foreach ( $reminders as $key => $reminder_data )
	// 	{
	// 		$check_task_query_status=0;

	// 		$this->firm_exc_sr_log 		.= $key."=Reminder Excution Start=".json_encode( $reminder_data )."\n";

	// 		$service_id_plain = $reminder_data['service_id'];

	// 		$service_id_with_ser = "ser_" . $reminder_data['service_id'];

	// 		$task_query = "SELECT task_status,progress_status FROM add_new_task WHERE firm_id='".$Firm_Data['firm_id']."' AND company_name = '".$reminder_data['client_id']."' AND client_contacts_id = '".$reminder_data['client_contacts_id']."' AND related_to!='subtask' AND (related_to_services = '".$service_id_plain."' OR related_to_services = '".$service_id_with_ser."')";

	// 		$task_data = $this->db->query($task_query)->result_array();

	// 		if(count($task_data)>0){
	// 			foreach($task_data as $td){
	// 				if(($td['task_status']=="1" || $td['task_status']=="2" || $td['task_status']=="3") && $td['progress_status']==11){
	// 					$check_task_query_status=1;
	// 				}
	// 			}
	// 		}else{
	// 			$check_task_query_status=1;
	// 		}

	// 		echo $ii++ . ") " . $reminder_data['client_id'] . '<br>';
			

	// 		if( $reminder_data['status'] == 0 && $check_task_query_status==1)
	// 		{
	// 			$this->firm_exc_sr_log .= "inside reminder progress condtion\n";

	// 			$client_service_contact_cond  = [
	// 											'client_id' 		=> 	$reminder_data['client_id'] ,
	// 											'service_id'		=>	$reminder_data['service_id'],
	// 											'client_contacts_id'=>	$reminder_data['client_contacts_id']
	// 										];

	// 			$frequency_data = $this->db->where( $client_service_contact_cond )
	// 									   ->get( "client_service_frequency_due" )
	// 									   ->row_array();
			
	// 			if( $frequency_data['is_create_task'] == 0  )
	// 			{	
	// 				$this->firm_exc_sr_log .= "TASK not created yet\n";
	// 				$this->Create_ServiceTask( $reminder_data );
	// 				$this->firm_exc_sr_log .= "Task created\n";
	// 			}

	// 			$query = "SELECT c.user_id,c.id,u.status 
	// 						FROM client as c  
	// 							INNER JOIN user as u 
	// 								ON u.id=c.user_id 
	// 							WHERE c.id=".$reminder_data['client_id'];

	// 			$client_data = $this->db->query( $query )->row_array();

	// 			$this->firm_exc_sr_log .= "client data".json_encode( $client_data )."\n";

	// 			$Reminder_SendAble_Status		=	json_decode( $Firm_Data['reminder_send_status'] , true );

	// 			if( $reminder_data['template_id'] !='' && in_array( $client_data['status'] , $Reminder_SendAble_Status ) )
	// 			{
	// 				//echo "status".$client_data['status']."allowed status";
	// 				//print_r( $client_data );
					
	// 				$this->firm_exc_sr_log .= "Inside reminder creatable client status\n";
					
	// 				$this->db->select("main_email,work_email");

	// 				if( $reminder_data['client_contacts_id'] != 0 )
	// 					$this->db->where( "id=".$reminder_data['client_contacts_id'] );
	// 				else 
	// 					$this->db->where( "client_id=".$client_data['user_id']." AND make_primary=1" );

	// 				$contact_details = $this->db->get("client_contacts")->row_array();

	// 				$contact_details['mails'][] 	=	$contact_details['main_email']; 

	// 				if( !empty( $contact_details['work_email'] ) )
	// 				{
	// 					$work_email            	   = json_decode( $contact_details['work_email'] , true );

	// 					$contact_details['mails']  = array_merge( $contact_details['mails'] , $work_email );
	// 				}

	// 				if( !empty( $contact_details['mails'] ) && $Firm_Data['service_reminder'] == 1 )
	// 				{				
	// 					$client 				= [ 
	// 												'client_id' 			=>	$reminder_data['client_id'],
	// 												'client_contacts_id'	=>	$reminder_data['client_contacts_id']
	// 											];

	// 					$DATA 					=	$this->get_reminder_template_data( $client , $reminder_data['template_id'] , $reminder_data['template_type'] );					

	// 					$send_to['to']	= 	$contact_details['mails'];
	// 					$send_to['cc']	=	$Firm_Data['company_email'];	 
	// 					$body 			=   setup_body_content( $Firm_Data['firm_id'] , $DATA['body'] );

                      
	//                     $data = [
	//                         'send_from'             =>  $Firm_Data['firm_id'],
	//                         'send_to'               =>  json_encode( $send_to ),
	//                         'subject'               =>  strip_tags( $DATA['subject'] ),
	//                         'content'               =>  $body,
	//                         'status'                =>  0,
	//                         'relational_module'     =>  'service_reminder',
	//                         'relational_module_id'  =>  $reminder_data['id'],
	//                         'created_at'            =>  date('Y-m-d H:i:s')
	//                     ];

 //                      	$this->db->insert( "email" , $data );

	// 					$this->firm_exc_sr_log .= $this->db->insert_id()."-->reminder insert in email table\n".json_encode($data)."\n";
	// 				}
	// 				else
	// 				{
	// 					$this->firm_exc_sr_log .= "client mail empty or service reminder off in firm settings\n";
	// 				}		
	// 			}
	// 			else
	// 			{
	// 				$this->firm_exc_sr_log .= "client status differ from serminder sendable status\n";
	// 				$this->db->update( 'service_reminder_cron_data' , [ 'status'=> 3 ] , "id=".$reminder_data['id'] );
	// 			}
	// 			$return_data=1;
	// 		}
	// 		else
	// 		{
	// 			$this->firm_exc_sr_log .= "inside reminder stop condtion\n";
	// 			$return_data=0;
	// 		}

	// 		if( $reminder_data['create_records'] == 1 )
	// 		{
	// 			$this->firm_exc_sr_log 	.= "inside Create recordes condition\n";

	// 			$this->Setup_Next_Frequency_Data( $reminder_data['client_id'] , $reminder_data['service_id'] , $reminder_data['client_contacts_id'] );

	// 			$this->firm_exc_sr_log 	.= "Setup next frequncy reminders\n";
	// 		}

	// 		$this->firm_exc_sr_log .= $key." Reminder Execution End\n";
	// 	}
	// 	return $return_data;
	// }
	
}
