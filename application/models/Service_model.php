<?php
/*@Author Ajith*/
class Service_model extends CI_Model 
{
	public $services_fields = [
		'conf_statement',
		'accounts',
		'company_tax_return',
		'personal_tax_return',
		'vat',
		'payroll',
		'workplace',
		'cis',
		'cissub',
		'bookkeep',
		'p11d',
		'management',
		'investgate',
		'registered',
		'taxadvice',
		'taxinvest'
	];
	/*firm_id come from cron functions*/
	public function get_mycustomised_ids( $table , $firm_id=0)
	{
		if( !$firm_id ) 
			$firm_id   = $this->session->userdata('firm_id');

		$this->db->select( "DISTINCT(super_admin_owned) as ids" );
		$this->db->where( "firm_id" , $firm_id );
		$result = $this->db->get( $table )->result_array();

		if( !empty( $result ) )
		{
			return implode(',' , array_column( $result , 'ids' ) );
		}
		else
		{
			return;
		}
	}

	public function get_firm_workflow()
	{
		$firm_id   = $this->session->userdata('firm_id');
		$cus       = $this->get_mycustomised_ids('work_flow');

		$this->db->where_in( 'firm_id' , [ 0, $firm_id ] );
		$this->db->where( 'status' , 0 );
		
		if( !empty( $cus ) )
		{
			$this->db->where_not_in('id' , explode( ',' , $cus ) );
		}

		return $this->db->get('work_flow')->result_array();
	}

	public function action( array $source )
	{	
		$firm_id   = $this->session->userdata('firm_id');
		
		$table	   = $source['table'];	
		$clone     = $source['clone'];
		$overwrite = $source['overwrite'];

		if( $firm_id != $clone['firm_id'] )
		{
			$overwrite['id']      			= '';
			$overwrite['firm_id'] 			= $firm_id;
			$overwrite['super_admin_owned']	= $clone['id'];

			$data = array_replace( $clone , $overwrite );
			$this->db->insert( $table , $data );

			return ["insert" => $this->db->insert_id() ];
		}
		else
		{	
			if( !empty( $overwrite ) )
			{
				$this->db->where('id' , $clone['id'] );

				if( $firm_id == '0' )
				{
					$this->db->or_where('super_admin_owned' , $clone['id'] );
				}

				$this->db->update( $table , $overwrite );
				return [ 'update'=> 1 ];
			}
		}
	}

	public function workflow_action( $id , array $data )
	{
		$this->db->where( 'id' , $id );
		
		$source['table']	 = "work_flow";
		$source['clone']	 = $this->db->get('work_flow')->row_array();
		$source['overwrite'] = $data;	
		
		$result 			 = $this->action( $source );

		if( !empty( $result["insert"] ) )
		{
			$this->step_action( ['work_flow_id' => $id ] , ['work_flow_id' => $result["insert"] ]);
		}
	}
	public function step_action( array $condition  , array $data )
	{
		$firm_id   = $this->session->userdata('firm_id');

		$cus = $this->get_mycustomised_ids('service_steps');

		$this->db->where( $condition );
		$this->db->where_in('firm_id' , [ 0 , $firm_id ] );

		if( !empty( $cus ) )
		{
			$this->db->where_not_in('id' , explode( ',' , $cus ) );
		}

		$steps     = $this->db->get('service_steps')->result_array();

		foreach ($steps as $value)
		{	
			$source['table']	 = "service_steps";
			$source['clone']	 = $value;
			$source['overwrite'] = $data;

			$result 			 = $this->action( $source );

			if( !empty( $result["insert"] ) )
			{
				$this->db->where( 'id' , $result["insert"] );
			}
			else	
			{
				$this->db->where( 'id' , $value['id'] );
			}
				$overwrite_data = $this->db->get('service_steps')->row_array();
				$overwrite_data = ['step_id' => $overwrite_data["id"] , 'work_flow_id'=> $overwrite_data['work_flow_id'] ];
				$this->sub_step_action( [ 'step_id' => $value['id'] ] , $overwrite_data );
		}
	}
	public function sub_step_action( array $condition  , array $data )
	{	
		$firm_id   = $this->session->userdata('firm_id');


		$cus = $this->get_mycustomised_ids('service_step_details');

		$this->db->where( $condition );
		$this->db->where_in('firm_id' , [ 0 , $firm_id ] );
		if( !empty( $cus ) )
		{
			$this->db->where_not_in('id' ,  $cus );
		}

		$sub_steps = $this->db->get('service_step_details')->result_array();
		
		foreach ($sub_steps as $value)
		{	
			$source['table']	 = "service_step_details";
			$source['clone']	 = $value;
			$source['overwrite'] = $data;

			$result 			 = $this->action( $source );	
		}
	}
	public function firm_currence()
	{
		$firm_id   = $this->session->userdata('firm_id');
		
		return $this->db->query("SELECT c.country,c.currency FROM admin_setting as a INNER JOIN crm_currency as c ON c.country = a.crm_currency  WHERE firm_id=".$firm_id)->row_array();
	}
	public function get_service_workflow( $id , $is_workflow)
	{

		$firm_id   = $this->session->userdata('firm_id');
		if( $is_workflow == '1' )
		{
			 return $this->db->query("SELECT `id`,`service_name`,'0' as price FROM work_flow WHERE id=".$id)->row_array();
		}
		else
		{
	    	return $this->db->query( 'SELECT `id`,`service_name`,`firm'.$firm_id.'_price` as price FROM service_lists WHERE id='.$id )->row_array();		
		}
	}
	/*firm_id come from cron functions*/
	public function get_steps( $id , $field , $firm_id = 0)
	{	
		
		if( !$firm_id ) 
			$firm_id   = $this->session->userdata('firm_id');

		$cus = $this->get_mycustomised_ids( 'service_steps' , $firm_id );
			
		$condition = '';
		if( !empty( $cus ) )
		{
			$condition = " AND id NOT IN (".$cus.") ";
		}

		return 	$this->db->query('select * from service_steps where status!=1 AND `'.$field.'`='.$id.' AND firm_id IN (0,'.$firm_id.') '.$condition.'order by order_no asc')->result_array();	
	}
	/*firm_id come from cron functions*/
	public function get_sub_steps( $id , $field , $firm_id = 0)
	{
		if( !$firm_id ) 
			$firm_id   = $this->session->userdata('firm_id');

		$cus = $this->get_mycustomised_ids( 'service_step_details' , $firm_id);
			
		$condition = '';
		if( !empty( $cus ) )
		{
			$condition = " AND id NOT IN (".$cus.") ";
		}
		return $this->db->query('select * from service_step_details where status!=1 AND `'.$field.'`='.$id.' AND firm_id IN (0,'.$firm_id.') '.$condition)->result_array();
	}


	public function index(){
		//echo "Model Called";
	}

	public function service_list(){
		$user_id=$_SESSION['id'];
		$query1=$this->db->query("SELECT * FROM `user` where  autosave_status!='1' and crm_name!='' and role=4 and firm_admin_id='".$user_id."' order by id DESC");
		$results1 = $query1->result_array();
		$res=array();
		foreach ($results1 as $key => $value) {
		array_push($res, $value['id']);
		}  
		$im_val=implode(',',$res);
		$count=explode(',',$im_val);
		if($count['0']!=''){
		$data=$this->db->query("select crm_company_name,conf_statement,accounts,company_tax_return,personal_tax_return,payroll, workplace, vat,
            cis, cissub,  p11d, bookkeep, management, investgate, registered, taxadvice,taxinvest from client WHERE autosave_status=0 and user_id in (".$im_val.") order by id desc")->result_array();
		}else{
	     	$data=0;
	     }
		return $data;
		
	}

	public function firm_service_details()
	{
		$enabled_service  	= 	$this->Common_mdl->get_FirmEnabled_Services();
        $enabled_service    = 	array_filter( array_keys( $enabled_service ) );        
		$services			=  	$this->db->select("id,service_name,services_subnames")
										->where_in( "id" , $enabled_service )
							    		->order_by("service_name","ASC")
							    		->get("service_lists")
							    		->result_array();							    		 
		return $services;
	}

	public function get_defualt_service_assignee_node_id( $firm_id = FALSE )
	{
		$return = [];
		if( !$firm_id ) $firm_id = $_SESSION['firm_id'];
		$this->load->model('User_management');
		$sid_uid = $this->User_management->get_service_wise_assignees( $firm_id );
		
		/*echo "service wise user_id<pre>\n";
		print_r( $sid_uid );*/


		foreach ( $sid_uid as $s_id => $u_id )
		{	
			$node_id =	$this->User_management->get_node_id_by_users_id( $u_id );
			 /*echo $s_id."\nservice wise node_id\n";
			 print_r( $node_id );*/

			$return[ $s_id ]['manager']   =	$node_id;
			$return[ $s_id ]['assignee']  =	$node_id;		
		}
		return $return;
	}

	public function pulk_update_service_assignee()
	{
		echo "<pre>";
		function parent( $start )
		{
		    $arr      = [];
		    $current  = $start;
		    do
		    {
		      $arr[]  = $current;
		      $current = $this->db->get_where( 'organisation_tree', "id=".$current )->row('parent_id');
		    }
		    while($current);

		    return implode( ':' , array_reverse( $arr ) );
		}

		$service_field = $this->services_fields;

		$a_firm = $this->db->select("firm_id,crm_company_name")
		                   ->get_where("firm","is_deleted!=1")
		                   ->result_array();

		foreach ( $a_firm as $key => $f_v )
		{
		  echo $f_v['crm_company_name']."=Firm=".$f_v['firm_id']."\n";

		  $data = $this->get_defualt_service_assignee_node_id( $f_v['firm_id'] );

		  foreach ( $data as $service_id => $value )
		  {

		    $ser_f = $service_field[ $service_id-1 ];

		    echo $service_id."=service_field=".$ser_f."\n";

		    if( !empty( $value['manager'] ) )
		    {
		      echo "service assignee not empty\n";

		      $assignees_array = array_map( 'parent', $value['manager'] );

		      print_r( $assignees_array );
		      echo "assignee prents_nodes\n";

		      $clients = $this->db->query("SELECT id FROM client WHERE firm_id =".$f_v['firm_id']." and ".$ser_f." LIKE '%\"tab\":\"on\"%'")->result_array();
		      
		      print_r( $clients );
		      echo "service_enabled cliets\n";

		      foreach ( $clients as $client )
		      {
		        $check = $this->db->query("SELECT id FROM firm_assignees WHERE module_name='CLIENT' and module_id=".$client['id']." and sub_module_id=".$service_id)->num_rows();

		        if( !$check )
		        {
		          echo $client['id']."==not assignee previously\n";

		            foreach ( $assignees_array as $assignee )
		            {
		              $insert_data =[];
		              $insert_data['firm_id']           = $f_v['firm_id'];
		              $insert_data['module_id']         = $client['id'];
		              $insert_data['sub_module_id']     = $service_id;
		              $insert_data['module_name']       = 'CLIENT';
		              $insert_data['assignees']         = $assignee;
		              $insert_data['sub_module_name']   = "service_manager";

		              $this->db->insert( "firm_assignees" , $insert_data );

		              $insert_data['sub_module_name']   = "service_assignee";

		              $this->db->insert( "firm_assignees" , $insert_data );
		            }
		        }else
		        {
		          echo $client['id']."==assigneed previously\n";
		        }
		      }
		    }
		    else
		    {
		      echo "service assignee empty\n";
		    }
		  }
		}
	}

}
?>