<?php 
/*Coded By Ajith*/
Class User_management extends CI_Model
{
	public function Firm_Roles_List( $firm_id )
	{
		$data 	= 	$this->db->where( 'firm_id' , $firm_id )
							->order_by( 'id' , 'ASC' )
		 					->get('roles_section')->result_array();
		return $data;		 					
	}

	public function Is_Permision_Given( $role_id , $module , $option )
	{
		$condition = array(
			'firm_id' => $_SESSION['firm_id'],
			'role_id' => $role_id
		);
    
    	$query	=	$this->db->select( $module )
    					 	->get_where( 'role_permission1' , $condition )
    					 	->row_array();

    	$value 	=	explode( ',' , $query[ $module ] );

    	if( count( $value ) > 1 )
    	{

        	return $value[ $option ];
    	}
    	else
    	{
        	return 0;
    	}
	}

	

	public function Insert_Role()
	{
      //role permission table
      $data['role']		=	$this->input->post( 'role' );
      $data['firm_id']	=	$_SESSION['firm_id'];
      $this->db->insert('Role',$data);

      //role_section permission table
      $data['parent_id']	=	$this->input->post('parent_id');
      $this->db->insert( 'roles_section' , $data );
      $role_id = $this->db->insert_id();

      //role permission table
      unset($data);
      $data['role']		=	$this->input->post('role');
      $data['firm_id']	=	$_SESSION['firm_id'];
      $data['role_id']	=	$role_id;
      
      if( !empty( $this->input->post('settings_section') ) )
      {
		  foreach( $this->input->post('settings_section') as $field )
		  {    
		    $value    = [];
		    $value[]  = isset( $this->input->post('bulkview')[$field] )     ? '1' : '0';  
		    $value[]  = isset( $this->input->post('bulkcreates')[$field] )  ? '1' : '0';
		    $value[]  = isset( $this->input->post('bulkedit')[$field] )     ? '1' : '0';
		    $value[]  = isset( $this->input->post('bulkdelete')[$field] )   ? '1' : '0';
		    $data[ $field ] = implode( ',' , $value );  

		    $vis_key  = 'viewall_'.$field;

		    if( $this->input->post( $vis_key ) !='' )
		    {
		    	$data[ $vis_key ] = $this->input->post( $vis_key );
		    }
		  }
      }

      $this->db->insert( 'role_permission1' , $data );
      return $this->db->affected_rows();
	}
	public function Update_Role( $role_section_id )
	{
		$condition = array(
			'firm_id' 	=> $_SESSION['firm_id'],
			'id' 		=> $role_section_id
		);

		$role_data 	=  	$this->db->get_where( "roles_section" , $condition )
							   ->row_array();

		if( $role_data['parent_id'] == 0 )
		{
		    $_POST['parent_id'] = 0;
		}

		$role_section_data = array(
			'parent_id'	=>	$this->input->post('parent_id'),
			'role'		=>	$this->input->post('role')
		);
		$this->db->update( 'roles_section' , $role_section_data ,"id =".$role_section_id );      
		$data['role']	=	$this->input->post('role');

		if( !empty( $this->input->post('settings_section') ) )
		{
			foreach( $this->input->post('settings_section') as $field )
			{   
				$value	  = [];
				$value[]  = isset( $this->input->post('bulkview')[$field] )     ? '1' : '0';  
				$value[]  = isset( $this->input->post('bulkcreates')[$field] )  ? '1' : '0';
				$value[]  = isset( $this->input->post('bulkedit')[$field] )     ? '1' : '0';
				$value[]  = isset( $this->input->post('bulkdelete')[$field] )   ? '1' : '0';
				$vis_key  = 'viewall_'.$field;

			    if( $this->input->post( $vis_key ) !='' )
			    {
			    	$data[ $vis_key ] = $this->input->post( $vis_key );
			    }

				$data[ $field ] = implode( ',' , $value );  
			}
		}
		$condition = array(
			'role_id'	=>	$role_section_id,
			'firm_id'	=>	$_SESSION['firm_id']
		);
		$this->db->where( $condition );
		$this->db->update( 'role_permission1' , $data );
		
		return $this->db->affected_rows();				
	}

	public function Hierarchy_RoleDelete( $role_section_id )
	{
		$condition = array(
			'firm_id' 	=>	$_SESSION['firm_id'],
			'id' 		=>	$role_section_id
		);
    
    	$role_data	=	$this->db->get_where( 'roles_section' , $condition )
    					 	     ->row_array();		

      	//move curent role child to his parent role
      	$this->db->update('roles_section', ['parent_id'=>$role_data['parent_id']] , "parent_id =".$role_data['id'] );

      	$this->db->delete( "roles_section" , $condition );
      	
      	return $this->db->affected_rows();
	}

	public function Delete_staffs( $ids )
	{
		$ids = explode( ',' , $ids );

		if( !class_exists('Common_mdl') )		
			$this->modal->load('Common_mdl');
		

		foreach ($ids as $id)
		{
            $this->db->delete( 'user' , ['id'=> $id] );
            $this->db->delete( 'staff_form' , ['user_id' => $id ] );
            
            $org_ids = $this->db->query("select id from organisation_tree where user_id =".$id)->result_array();
            
            foreach ( $org_ids as $value )
            {              
            	delete_OrgChart_Node( $value['id'] );            
            }
            $this->Common_mdl->Increase_Decrease_UserCounts( $_SESSION['firm_id'] , '+1');
		}
		return 1;
	} 
	public function Get_FirmStaffs()
	{
		$emp =  $this->db->select( "id,crm_name" )
						->where( 'firm_id' , $_SESSION['firm_id'] )
						->where( "(user_type='FA' OR user_type='FU')")	
						->where( 'status' , 1 )						
						->get( 'user' )
						->result_array();		
		return $emp;
	}

	public function get_service_wise_assignees( $firm_id )
	{
		$return = [];

		$service = $this->Common_mdl->get_FirmEnabled_Services();
		$service = array_filter( array_keys( $service ) ); 
		asort( $service );

		foreach ( $service as $ser_id )
		{
			$staffs = $this->db->select('user_id')
						->get_where('staff_form' ,'FIND_IN_SET( '.$ser_id.' , allocated_service) and firm_id='.$firm_id)
						->result_array();
			$return[ $ser_id ] = array_column( $staffs , 'user_id' );
		}
		return $return;
	}
	/*
		used to get parent nodes in orgnation tree 
		@ $StartNode orgnation_tree id
	*/
	public function GetParent_Nodes( $StartNode )
	{
		$hierarchy      = [];
		$current  		= $StartNode;
		do
		{
		  $hierarchy[]  = $current;
		  $current = $this->db->get_where( 'organisation_tree', "id=".$current )->row('parent_id');
		}
		while($current);

		return implode( ':' , array_reverse( $hierarchy ) );
	}
	/*
		used to get staff node id (org_tree position)  
	*/
	public function get_node_id_by_users_id( array $user_id )
    { 
      $return = [];

	    if( !empty( $user_id ) )
	    {
	      	$ids = $this->db->select('id')
	                  ->where_in('user_id', $user_id )
	                  ->group_by('user_id')
	                  ->get("organisation_tree")
	                  ->result_array();

	      	$return = array_column( $ids , 'id' );
      	}
      return $return;
    }
    public function AddRemoveStaffFrom_ClientServices(array $new_service ,array $old_service , $user_id )
    {	
		/*echo  "update servcie";
    	print_r( $new_service );
    	echo  "\nold servcie";
    	print_r( $old_service );
    	echo  "\n";*/

    	$response	  = [];
    	$firm_id	  = $this->session->userdata('firm_id');
    	$old_service  = array_filter( $old_service );
    	$new_service  = array_filter( $new_service );
    	$AddRemove[0] = array_diff( $old_service , $new_service ); 
    	$AddRemove[1] = array_diff( $new_service , $old_service ); 

    	$tree_node    = $this->get_node_id_by_users_id( [$user_id] );
    	if( !empty( $tree_node[0] ) )
    	{	
    		/*ECHO "STAFF HAS NODE ID\n";*/
	    	$hierarchy_node =  $this->GetParent_Nodes( $tree_node[0] );		

			/*echo "Staff hierarchy_node\n".$hierarchy_node."\n";*/

	    	$this->load->model("Service_model");
	    	$service = $this->Service_model->services_fields;
	    	foreach ( $AddRemove as $status => $service_ids )
	    	{
	    		/*echo "Action=".$status."\n";*/

	    		foreach ( $service_ids as $service_id )
	    		{
	    			$field 	 = $service[ $service_id-1 ];
	    			/*echo "Service_field=".$field."\n";*/

	    			$query 	 = "SELECT id FROM client where firm_id=".$firm_id." 
	    			AND ".$field." LIKE '%\"tab\":\"on\"%'";

	    			$clients = $this->db->query( $query )->result_array();

	    			$clients_ids = array_column( $clients , 'id' );

					foreach ( $clients_ids as $client_id )
					{	
						/*echo "client_id=".$client_id."\n";*/			
						if( $status == 0 )
						{
							$this->db->where( 'assignees', $hierarchy_node )
									 ->where_in( 'sub_module_name', ['service_manager','service_assignee'] )
									 ->where( 'module_id', $client_id )
									 ->where( 'sub_module_id', $service_id )
									 ->where( 'module_name' , 'CLIENT' )
									 ->delete('firm_assignees');
									 /*echo $this->db->last_query()."\n".$this->db->affected_rows()."\n";
									 echo "removed\n";*/
						}
						else
						{
							$insert_data	=	[];

							$insert_data['firm_id']           = $firm_id;
				            $insert_data['module_name']       = 'CLIENT';
				            $insert_data['module_id']         = $client_id;
				            $insert_data['sub_module_name']   = "service_manager";
				            $insert_data['sub_module_id']     = $service_id;
				            $insert_data['assignees']         = $hierarchy_node;

				            $this->db->insert( "firm_assignees" , $insert_data );

				            $insert_data['sub_module_name']   = "service_assignee";

				            $this->db->insert( "firm_assignees" , $insert_data );

				            /*echo "Added\n";*/
						}
					}
	    		}
	    	}
    	}
    	else
    	{	
    		ECHO "STAFF empty NODE ID\n";
    		$response = ['result'=>0 ,"messgae"=>"not in tree"];
    	}

    } 
}


?>