<?php
class Demo_model extends CI_Model {


  function __construct() 
  {
    parent::__construct();
   // $this->load->database("default");
    $this->final_array =array();
  }
  

  public function index(){
    //echo "Model Called";
  }


function getHeader( $PARENT_LI , $CHILD_UL , $CHILD_LI )
{ 

  $menu_data = '';
  $Menus = $this->GetMenus();
  foreach ($Menus as $datas)
  {
    
    $url =  $this->getMenuURL( $datas['href'] );
    
    $ParentLi = str_replace('{URL}', $url , $PARENT_LI );  
    $ParentLi = str_replace('{ICON_PATH}', $datas['icon'] , $ParentLi );  
    $ParentLi = str_replace('{MENU_LABEL}', strtoupper( $datas['text'] ) , $ParentLi );  

    $childCnt ='';

    if( isset( $datas['children'] ) )
    {
      $childCnt =  $this->recursive_menu( $datas['children'] , $CHILD_UL , $CHILD_LI );
    }

//echo htmlspecialchars($ParentLi)."<br>";
//echo  htmlspecialchars($childCnt)."child<br>";

    $ParentLi = str_replace('{CHILD_CONTENT}', $childCnt , $ParentLi ); 




    $menu_data.=$ParentLi; 
  }

 // echo htmlspecialchars( $menu_data ); 

  return $menu_data;

}

function GetMenus()
  {

     // $CI =& get_instance();
      $getMenu  = $this->db->query("select * from menu_manage where  firm_id = ".$_SESSION['firm_id']." ")->row_array();

      $mainMenus=json_decode($getMenu['menu_details'],true);


      // print_r($mainMenus);
      // exit();

      $menu = [];

      foreach($mainMenus as $key=> $data)
      {

          $PermissionCheck = $this->PermissionCheck( $data );
          
          if ( !$PermissionCheck ) continue;

          $result = $this->subMenus( $data['children']  );

          if( !$result ) 
          {
              $menu[] = $data;
          }
          else if( $result != 1 )
          {
              $data['children'] = $result;
              $menu[] = $data;
          }
          
      }
      return $menu;
      
  }
  function PermissionCheck( $data , $sectionSettings=0)
  { 
      //$CI =& get_instance();
      $rolePermission = 1;

      if( !empty( $data['href'] ) )
      {
        $modualData = $this->db->query("select * from module_mangement where module_mangement_id = ".$data['href'] )->row_array();
        
        $Module = trim( $modualData['module'] );

        $PermissionType = trim( $modualData['required_premission'] ); 

        $result = [];
        $needl = ",";

        if( substr_count( $PermissionType ,'||') )
        {
            $needl = "||";
        }
        else if( substr_count( $PermissionType ,'&&') )
        {
            $needl = "&&";
        }

        $PermissionType = explode( $needl , $PermissionType );              

        //print_r($modualData);
    

        foreach ($PermissionType as $perVal) 
        {
            $result[] = $_SESSION['permission'][ $Module ][ $perVal ]; 
        }

        $result = array_keys( $result , 1);

        if( ( $needl=="," || $needl=="||" ) && count($result) )
        {
            $rolePermission = 1;
        }
        else if ( $needl=="&&" &&  count($PermissionType) == count($result) )
        {
            $rolePermission = 1;                
        }
        else 
        {
            $rolePermission = 0;
        }

          //echo $Module."----".$rolePermission."---";print_r($result);
      }

      return $rolePermission;        
  }
  function subMenus($subMenus )
  {

      //$CI =& get_instance();
      // $subMenus  = $this->db->query("select * from menu_management where parent_id =$id ORDER BY menu_management_id ASC ")->result_array();   
      if( !empty($subMenus) )
      {           
          $pre = 0;
          $submenu  = [];
          foreach ($subMenus as $data)
          {
              $PermissionCheck = $this->PermissionCheck( $data );

              if( !$PermissionCheck ) continue;
              $pre = 1;

              $result = $this->subMenus( $data['children'] );

              if( !$result )
              {
                  $submenu[] = $data;
              }
              else if( $result!= 1 )
              {
                  $data['children'] = $result;
                  $submenu[] = $data; 
              }

          }
          
          if ($pre) return $submenu;
          else return 1;
      }
      else 
      {
          return 0;
      }
  }
  function getMenuURL( $id )
{ 
  //$CI =& get_instance();
  
  $url="javascript:void(0)";

  if ( !empty( $id ) )
    {
      
      $url = $this->db->query(" SELECT url FROM `module_mangement` where module_mangement_id = $id ")->row_array();
      
      if ( strpos( $url['url'] , "#" ) === 0 )        
      {
        $url = $url['url'];
      
      }
      else
      {
        $url = base_url().$url['url'];
      }

    }

    return $url;
}
function recursive_menu( $Child_menu , $CHILD_UL , $CHILD_LI )
{
  
  //$CI =& get_instance();
  //print_r($Child_menu);

  $menu_data = '';

  foreach ($Child_menu as $datas)
  {
                          
    $url = getMenuURL( $datas['url'] );

    $ChildLi = str_replace('{URL}', $url , $CHILD_LI );  
    $ChildLi = str_replace('{MENU_LABEL}', strtoupper( $datas['text'] ) , $ChildLi );  

    $childCnt ='';

     if( isset( $datas['children'] ) )
    {
      $childCnt = $this->recursive_menu( $datas['children'] , $CHILD_UL , $CHILD_LI );
    }

    $ChildLi = str_replace('{CHILD_CONTENT}', $childCnt , $ChildLi ); 

    $menu_data.= $ChildLi; 

  }
    
  $menu_data = str_replace('{CHILD_CONTENT}', $menu_data , $CHILD_UL );  
   
  return  $menu_data;
}
  public function update_task_assignDetails(array $assign,$task_id)
{
/** for update assigne data maintaine added and removed users 20-10-2018 **/
            $it_assign = $assign;
            // print_r( $it_assign);
            // exit;
            // $it_team = $assign['team'];
            // $it_department = $assign['department'];
            //$get_prev_assigne_data=$this->Task_section_model->get_assigne_data('staff',$task_id);
          /** for staff assignee **/
           $get_prev_assigne_data=$this->db->query("select * from task_assignee_add_remove_list where task_id=".$task_id."  ")->result_array();

            $get_prev_assigne=array();
            $get_prev_ids=array();
            $get_added_removed_date=array();
            $get_prev_status=array();
            $get_prev_working_hrs=array();
            if(count($get_prev_assigne_data)>0)
            {
              foreach ($get_prev_assigne_data as $prev_key => $prev_value) {
                array_push($get_prev_assigne, $prev_value['assignee_ids']);
                array_push($get_prev_ids, $prev_value['id']);
                array_push($get_added_removed_date, $prev_value['added_removed_date']);
                array_push($get_prev_status, $prev_value['status']);
                array_push($get_prev_working_hrs,$prev_value['removed_workers_timing']);
              }
            }            
            if(count($get_prev_assigne_data)>0)
            {
              $ex_assign=$get_prev_assigne;
              $ex_result=array_diff($ex_assign,$it_assign); //find remove person
             if(count($ex_result)>0)
             {
              foreach ($ex_result as $ex_key => $ex_value) {
                $its_key = array_search ($ex_value, $ex_assign);
                if($get_prev_status[$its_key]!='removed')
                {
                  $add_remove_list=array();
                                     /** if it removed added a overall timing **/
                    $get_individual_timer_data=$this->db->query("select * from individual_task_timer where user_id=".$get_prev_assigne[$ex_key]." and task_id=".$task_id." ")->row_array();
                     $res_timing=$this->Task_section_model->calculate_timing_status_removed($get_individual_timer_data['time_start_pause']);
                   // echo $res."jhdfhgf gjhggjhgh";
                  /** end of overall timing **/
                  if($get_prev_working_hrs[$its_key]!='')
                  {
                  $add_remove_list['removed_workers_timing']=$get_prev_working_hrs[$its_key].",".$res_timing;
                  }
                  else
                  {
                    $add_remove_list['removed_workers_timing']=$res_timing;
                  }
                  $add_remove_list['status']='removed';
                  $add_remove_list['added_removed_date']=$get_added_removed_date[$its_key].",".time();
                  $this->Common_mdl->update('task_assignee_add_remove_list',$add_remove_list,'id',$get_prev_ids[$ex_key]);
                }
              }
             }
            }
            foreach ($it_assign as $itas_key => $itas_value) {
              $get_count=$this->db->query("SELECT count(*) as it_count,GROUP_CONCAT(id) as dbid,added_removed_date,status FROM `task_assignee_add_remove_list` where assignee_ids=".$itas_value." and task_id=".$task_id." ")->row_array();
              if($get_count['it_count']==0)
              {
                $add_remove_list=array();
                  $add_remove_list['task_id']=$task_id;
                  $add_remove_list['assignee_ids']=$itas_value;
                  //$add_remove_list['staff_team_department']='staff';
                  $add_remove_list['status']='active';
                  $add_remove_list['removed_workers_timing']='';
                  $add_remove_list['added_removed_date']=time();
                  $add_remove_list['created_by']=$_SESSION['id'];
                  $add_remove_list['created_time']=time();
                  $this->Common_mdl->insert('task_assignee_add_remove_list',$add_remove_list);
              }
              else
              {
                $it_id=$get_count['dbid'];
                if($get_count['status']!='active'){
                  $add_remove_list=array();
                  $add_remove_list['status']='active';
                  $add_remove_list['added_removed_date']=$get_count['added_removed_date'].",".time();
                  $this->Common_mdl->update('task_assignee_add_remove_list',$add_remove_list,'id',$it_id);
               }
              }
            }

          }


  function Firm_column_settings($table)

{

  $firm_id = "firm".$_SESSION['firm_id'];
  
  $column_setting = $this->db->query("select sub_id,name,visible_".$firm_id.",order_".$firm_id.",cusname_".$firm_id." from firm_column_settings where table_name='".$table."' order by order_".$firm_id." ASC")->result_array();

  //print_r($column_setting);


    
  $hidden_coulmns = array_keys( array_column( $column_setting , 'visible_'.$firm_id , 'name' ) , 0 );

  $column_order = array_column( $column_setting , "sub_id" );

   $sort_order = array_column( $column_setting , "name" );

  $hidden_coulmns = array_map(function ($a){return trim( $a ).'_TH';}, $hidden_coulmns );

  //$column_order = array_map(function ($a){return trim( $a ).'_TH';}, $column_order );
  $return['sort_order'] = json_encode( $sort_order );

  $return['hidden'] = json_encode( $hidden_coulmns );

  $return['order'] = json_encode( $column_order );

  return $return;
}



    function home_page_add($data){
        $this->db->insert('page_content',$data);
        return $this->db->insert_id();
    }


   function home_page_edit($data){
        $this->db->insert('page_content',$data);
        return $this->db->insert_id();
    }








  }


  ?>