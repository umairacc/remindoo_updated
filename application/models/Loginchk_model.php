<?php

Class Loginchk_model extends CI_Model { 
	
    Public function __construct() 
    { 
      parent::__construct(); 
      $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
      $this->output->set_header("Pragma: no-cache");
      $this->load->database();
      $this->load->model(array('Common_mdl','Security_model'));

    } 

    public function superAdmin_LoginCheck()
    {
      if( isset( $_SESSION['is_superAdmin_login'] ) )
      {
        return true;    
      }
      else
      { 
        redirect('super_admin');
      }
    }

public function login_chk(){
  
  $username = $this->input->post('username');
  $password = $this->input->post('password');
  $pswd = md5($password);
 
 // $pswd = base64_decode(urldecode($password));
  $this->db->select('id,crm_name,user_type,username,crm_email_id,status,role,firm_id');
  $this->db->where('username', $username);
  $this->db->where('password', $pswd);
  $query = $this->db->get('user');

  
  //echo $query->firm_id;die();

  //echo "select * from user where username = '".$username."' AND password = '".$pswd."'";

 // $query = $this->db->query("select * from user where username = '".$username."' AND password = '".$pswd."'");
  //echo $this->db->last_query();


   if($query -> num_rows() == 1)
   {
      $rec = $query->row();
      //check login user firm is deleted
      $this->db->where('firm_id' , $rec->firm_id );
      $this->db->where('is_deleted' , 1 );
      $is_deleted = $this->db->get('firm')->num_rows();
      if( $is_deleted )
      {
        return 0;
      }

    //clear other session datas (don't allow two user login at same time)

    //this session identify if super admin login to firm portal. 
    if( !empty( $_SESSION['SA_TO_FA'] ) )
    {
      $_SESSION = [];
      $_SESSION['SA_TO_FA'] = 1; 
    }
    else
    {
      $_SESSION = [];
    }
    
    $this->Firm_setting_mdl->Check_LatestSetup( $rec->firm_id );
    setcookie('remindoo_logout', 'remindoo_FirmAdmin',NULL,"/");
    $this->session->set_userdata('userId', $rec->id);
    $this->session->set_userdata('roleId', $rec->role);
    $this->session->set_userdata('user_type', $rec->user_type);
    $this->session->set_userdata('userName', $rec->username);
    $this->session->set_userdata('userEmail', $rec->crm_email_id);
    $rolename = $this->Common_mdl->getRole($rec->id);
    $this->session->set_userdata('roleName', $rolename);
    // if($rec->user_type=='FA')
    // {   $this->db->select('id');
    //     $this->db->where('firm_id', $rec->firm_id);
    //     $this->db->where('user_type','FC');
    //     $query1 = $this->db->get('user');
    //     $rec1= $query1->result_array();
    //     $doc_userid=array();
    //     if(count($rec1)>0)
    //     {
    //       foreach ($rec1 as $key => $value) {

    //        array_push($doc_userid,$value['id']);
    //       }
    //     }
    //     $_SESSION['document_userid']=$doc_userid;
    //     $_SESSION['document_permission']=array('1','1','1','1');

    // }else if($rec->user_type=='FU')
    // {
    //     $res=array();
    //     $ques=Get_Assigned_Datas('CLIENT');


    //         foreach ($ques as $key => $value) {
    //           //echo $value;

    //       $res1=Get_Module_Assigees( 'CLIENT' , $value );

    //      if (in_array($_SESSION['userId'], $res1))
 
    //       {
    //         $res2[]=$value;
            
    //       }
    
    //     }

    //   $res=(!empty($res2))?array_unique($res2):'-1';
    //   $_SESSION['document_userid']= $res;
    //   $_SESSION['document_permission']=array('1','1','1','0');
    // }
    // else if($rec->user_type=='FC')
    // {
    //   $_SESSION['document_userid']= $rec->id;
    //   $_SESSION['document_permission']=array('1','1','1','1');
    // }
    // else
    // {
    //   $_SESSION['document_userid']= NULL;
    //   $_SESSION['document_permission']=NULL;
    // }
    return $query->row();
   }
   else
   {
     return false;
   }
}


public function update( $table, $data, $field, $value )
  {

      $update = $this->db->where($field, $value)->update($table ,$data);
      //echo $this->db->last_query();die;
      
      if ($this->db->affected_rows() > 0) {
          return 1;
      }
      else {
          return 0;
      }
  }

public function send_user_login_nofification()
{
  $query        = "SELECT f.company_name,fm.smtp_email FROM  admin_setting as f 
                    INNER JOIN mail_protocol_settings fm 
                    ON fm.firm_id = ".$_SESSION['firm_id']."
                    WHERE f.firm_id=".$_SESSION['firm_id'];

  $firm_details = $this->db->query( $query )->row_array();

  $subject   =  "Remindoo Login Alert";

  $message   = "<h4>Hi ".$firm_details['company_name']."</h4>\n";
  $message  .= "<b>".$_SESSION['crm_name']." ".$_SESSION['last_name']."</b> has logged in From ".$this->input->ip_address()." IP";
  $message  .= "<p>".date('l,d F Y \a\\t h:i A')."</p>"; 
  $message  .= "<p>Thanks.</p>";

  firm_settings_send_mail( 0 , $firm_details['smtp_email'] , $subject , $message );
}


//end  class
}
?>