<?php
/*
	@author Ajith 
*/
class Client_model extends CI_Model
{
	public function client_AddEdit_info($user_id)
	{
		$this->load->model('service_model');
		$data   = [];

		$data['enabled_service']            = $this->Common_mdl->get_FirmEnabled_Services();

		$data['enabled_service_detail']     = $this->service_model->firm_service_details();

		$default_service_assignee           = $this->service_model->get_defualt_service_assignee_node_id();


		$data['client']                     = $data['user'] = $data['contactRec'] = [];
		$data['user_ids']                   = $data['assignees_node'] = '';

		if ($user_id) {
			$data['client']           = $this->db->where('user_id = ' . $user_id . " and firm_id=" . $_SESSION['firm_id'])
				->get('client')
				->row_array();
			/*for security check*/
			if (empty($data['client'])) {
				redirect('user');
			}
			$data['contactRec']         = $this->db->where("client_id", $user_id)
				->order_by("make_primary", "DESC")
				->get("client_contacts")
				->result_array();
			$data['user']               = $this->db->get_where("`user`", "id=" . $user_id)
				->row_array();
			$data['client_legalform']   = $this->db->get_where('client_legalform', 'user_id=' . $user_id)
				->row_array();
			$data['user_ids'] = $user_id;


			$data['assignees_node']   = $this->db->select("assignees")
				->where("sub_module_name='' and module_name='CLIENT' and module_id=" . $data['client']['id'] . " and firm_id=" . $_SESSION['firm_id'])
				->get("firm_assignees")
				->result_array();
			$data['assignees_node']   = json_encode(array_column($data['assignees_node'], 'assignees'));

			/*Genrate New Invitation link*/
			if ($data['client']['crm_other_send_invit_link'] == '') {
				$data['client']['crm_other_send_invit_link'] = $this->Common_mdl
					->Get_ClientReferral_Code($data['client']['crm_company_name']);
			}
			/*Genrate New Invitation link*/
			$assigneed_member         =  $this->Client_model
				->Get_Service_Assignees($data['client']['id']);
			if (!empty($assigneed_member)) {
				$default_service_assignee = array_replace($default_service_assignee, $assigneed_member);
			}
		}

		$data['referby']              = $this->db->query("SELECT c.id,c.crm_first_name,c.crm_company_name 
                                        FROM user as u  inner join client as c on c.user_id = u.id 
                                        WHERE u.firm_id = " . $_SESSION['firm_id'] . " and u.user_type = 'FC'")
			->result_array();

		$data['reminder_templates']   = $this->Common_mdl->getReminderTemplates();

		$data['countries']            = $this->db->order_by("name", "ASC")
			->get("countries")
			->result_array();
		/*For Firm Fields Settings*/
		$data['sections']             = $this->db->select("DISTINCT(section) as section")
			->where("firm_id IN( " . $_SESSION['firm_id'] . ",0) AND section REGEXP '^[0-9]+$' ")
			->get("client_fields_management")
			->result_array();
		$data['sections']             = array_column($data['sections'], 'section');
		$settings                     = $this->Common_mdl
			->Get_Client_fields_ManagementSettings($_SESSION['firm_id']);
		$data = array_merge($data, $settings);

		/*For Firm Fields Settings*/

		$data['default_service_assignee'] = json_encode($default_service_assignee);
		// echo json_encode(GetAssignees_SelectBox()); die;
		return $data;
	}

	public function change_client_status($id, $status)
	{
		$status_info = ['Inactive', 'Active', 'Inactive', 'Frozen', 'Draft', 'Archive'];

		$firm_id =  $this->session->userdata('firm_id');
		$data    =  array("status" => $status);

		$old_status = $this->Common_mdl->get_field_value('user', 'status', 'id', $id);

		$query   =  $this->Common_mdl->update('user', $data, 'id', $id);
		$data    =  array("crm_company_status" => $status);
		$query   =  $this->Common_mdl->update('client', $data, 'user_id', $id);


		$log     =  "<p>Status Changed From <span class='text-warning'>" . $status_info[$old_status] . "</span> To <span class='text-success'>" . $status_info[$status] . "</span> By <span style='font-style: italic;'>" . $_SESSION['crm_name'] . "</span></p>";

		$this->Common_mdl->Create_Log('client', $id, $log);


		$getClientstatus = $this->Get_Assigned_client($firm_id);


		$getClientstatus = array_column($getClientstatus, 'status');

		$status_info = ['Active' => [1], 'Inactive' => [0, 2], 'Frozen' => [3], 'Archive' => [5]];
		foreach ($status_info as $key => $value) {
			$status_info[$key] =  count(array_intersect($getClientstatus, $value));
		}

		$status_info['All'] = count($getClientstatus);

		return  json_encode($status_info);
	}
	public function bulk_archive_unarchive($ids, $status)
	{
		$ids = json_decode($ids, true);

		foreach ($ids as $id) {
			if ($status  == "archive") {
				$this->db->query("update user set old_status=status,status=5 where id=$id and status!=5");

				$query = "UPDATE client SET
                 company_old_status=crm_company_status,
                 crm_company_status=5 
                 WHERE user_id=$id AND crm_company_status!=5";

				$this->db->query($query);
			} else if ($status  == "unarchive") {
				$this->db->query("update user set status=@s:=status,status=old_status,old_status=@s where id=$id");

				$query = "UPDATE client SET 
                       crm_company_status=@S:=crm_company_status,
                       crm_company_status=company_old_status,
                       company_old_status=@S
                       WHERE user_id=$id";

				$this->db->query($query);
			}
		}
		return $this->db->affected_rows();
	}
	public function Get_Assigned_client($firm_id, $assignee = 0)
	{

		$Assigned_Client = Get_Assigned_Datas('CLIENT', $assignee);

		$Assigned_Client = implode(',', $Assigned_Client);

		$getallUser = [];
		if (!empty($Assigned_Client)) {

			$client = $this->db->query("select user_id from client where firm_id=" . $firm_id . " and id IN (" . $Assigned_Client . ") order by id DESC")->result_array();

			$user_ids = implode(',', array_column($client, 'user_id'));

			if (!empty($user_ids)) {
				//if change here u shoud change status_change function for get client status
				$getallUser  = $this->db->query("SELECT * FROM user where firm_id =" . $firm_id . " and user_type='FC' and id IN (" . $user_ids . ") order by id DESC")->result_array();
			}
		}
		return $getallUser;
	}
	public function get_assigned_client_status_count()
	{
		$status    = ['in_active' => 0, '1' => 0, '3' => 0, '4' => 0, '5' => 0];

		$firm_id                = $this->session->userdata('firm_id');
		$getallUser             = $this->Get_Assigned_client($firm_id);

		$get_all_status         = array_count_values(array_column($getallUser, 'status'));

		foreach ($get_all_status as $key => $val) {
			if (in_array($key, ['', '0', '2'])) $status['in_active'] += $val;
			else $status[$key] = $val;
		}

		$status['All']         =  array_sum($status);
		return $status;
	}

	public function is_enabled($value)
	{
		$value = strtolower($value);
		return (($value == 'on' || $value == 'y') ? 'on' : '');
	}

	public function validate_csv_fields($File)
	{
		$handler = fopen($File['tmp_name'], 'r');
		fgetcsv($handler);
		$CSVErrors = [];

		while (($line = fgetcsv($handler)) !== FALSE) {

			$Error = [];

			$company_name   = $line[1];
			$company_type = $line[2];
			$company_number = $line[3];

			if ($company_name   == '') {
				$Error['cmpny_name'] = 'Company Name is Required';
			}
			if ($company_number == '') {
				$Error['cmpny_no']   = 'Company Number is Required';
			} else {

				$check_cn = $this->Common_mdl->GetAllWithWheretwo('client', 'firm_id', $_SESSION['firm_id'],  'crm_company_number', $company_number);
				if (!empty($check_cn[0])) {
					$Error['cmpny_no']   = 'Company Number is already exist';
				}
			}

			$company_types =  array_keys($this->Common_mdl->Company_types());

			(empty($company_type)) ?  $Error['company_type'] = 'Company Type is Required' : (!in_array(strtolower($company_type), $company_types) ? $Error['company_type'] = 'Company Type is invalid' : '');

			(!empty($line[15])) ? (!in_array(strtolower($line[15]), ['active', 'inactive', 'frozen', 'archive']) ? $Error['company_status'] = 'Company Status is invalid' : '') : '';

			$date_validation = ['Incorporation Date' => $line[8], 'Letter Of Engagement Sign Date' => $line[17], 'Confirmation Statement Date' => $line[64], 'Confirmation Statement Due Date' => $line[65], 'Last Statement Made Up To Date' => $line[66], 'Accounts Next Made Up To' => $line[75], 'Ch Accounts Next Due' => $line[76], 'Last Accounts Made Up To Date' => $line[77], 'Accounts Due Date Hmrc' => $line[79], 'Accounts Tax Date Hmrc' => $line[80], 'Personal Tax Return Date' => $line[87], 'Personal Due Date Return' => $line[88], 'Personal Due Date Online' => $line[89], 'Payroll Reg Date' => $line[93], 'First Pay Date' => $line[95], 'Payroll Run Date' => $line[97], 'Rti Deadline Date' => $line[98], 'Paye Scheme Ceased Date' => $line[131], 'Staging Date' => $line[103], 'Pension Subm Due Date' => $line[105], 'Postponement Date' => $line[106], 'The Pensions Regulator Opt Out Date' => $line[107], 'Re Enrolment Date' => $line[108], 'Declaration Of Compliance Due Date' => $line[109], 'C.I.S Frequency' => $line[116], 'Cis Contractor Start Date ' => $line[118], 'Cis Subcontractor Start Date' => $line[122], 'P11d Start Date' => $line[125], 'P11d First Benefit Date' => $line[127], 'P11d Due Date' => $line[128], 'P11d Paye Scheme Ceased Date' => $line[131], 'Vat Date Of Registration' => $line[134], 'Vat Quater End Date' => $line[136], 'Vat Due Date' => $line[138], 'Next Booking Date' => $line[147], 'Next Manage Acc Date' => $line[153], 'Insurance Start Date' => $line[157], 'Insurance Renew Date ' => $line[158], 'Registered Start Date' => $line[162], 'Registered Renew Date' => $line[163], 'Investigation Start Date' => $line[167], 'Investigation End Date' => $line[168]];

			foreach ($date_validation as $indice => $val) {
				if (!empty($val) && empty(strtotime($val))) {
					$Error[str_replace(' ', '_', $indice)] = $indice . ' has invalid date';
				}
			}

			$contact_fname  = $line[4];
			$main_email     = $line[5];

			if ($contact_fname == '') {
				$Error['check_contact_name'] = 'Contact Person Name is Required';
			}

			if ($main_email == '') {
				$Error['chk_conatct_email']  = 'Main Email is Required';
			} else {
				$regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
				if (!preg_match($regex, $main_email)) {
					$Error['chk_conatct_email'] = 'Main Email is invalid';
				}
			}

			$emails = ['Email' => $line[58], 'Contact Work Email' => $line[37]];
			$regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';

			foreach ($emails as $indice1 => $val1) {
				if (!empty($val1) && !preg_match($regex, $val1)) {
					$Error[str_replace(' ', '_', $indice1)] = $indice1 . ' is invalid';
				}
			}

			$hrefs = ['Business Website' => $line[20], 'Company Url' => $line[18], 'Officers Url' => $line[19]];

			foreach ($hrefs as $indice2 => $val2) {
				if (!empty($val2) && !filter_var($val2, FILTER_VALIDATE_URL)) {
					$Error[str_replace(' ', '_', $indice2)] = $indice2 . ' is invalid';
				}
			}

			$phone_numbers = ['Contact Mobile' => $line[27], 'Contact Landline' => $line[36]];

			foreach ($phone_numbers as $indice3 => $val3) {
				if (!empty($val3) && (!is_numeric($val3) || strlen($val3) < 10 || strlen($val3) > '18')) {
					$Error[str_replace(' ', '_', $indice3)] = $indice3 . ' is invalid';
				}
			}

			(!empty($line[152])) ? (!in_array(strtolower($line[152]), ['weekly', 'monthly', 'quarterly', 'annually']) ? $Error['manage_acc_fre'] = 'Manage Acc Fre is invalid' : '') : '';

			(!empty($line[149])) ? (!in_array(strtolower($line[149]), ['weekly', 'monthly', 'quarterly', 'annually']) ? $Error['bookkeeping_to_done'] = 'Bookkeeping To Done is invalid' : '') : '';

			(!empty($line[137])) ? (!in_array(strtolower($line[137]), ['jan/apr/jul/oct', 'feb/may/aug/nov', 'mar/jun/sep/dec', 'monthly']) ? $Error['vat_quarters'] = 'Vat Quarters  is invalid' : '') : '';

			(!empty($line[135])) ? (!in_array(strtolower($line[135]), ['monthly', 'quarterly', 'yearly']) ? $Error['vat_frequency'] = 'Vat Frequency is invalid' : '') : '';

			(!empty($line[96])) ? (!in_array(strtolower($line[96]), ['monthly', 'weekly', 'forthnightly']) ? $Error['payroll_run'] = 'Payroll Run is invalid' : '') : '';

			(!empty($line[22])) ? (!in_array(strtolower($line[22]), ['mr', 'mrs', 'miss', 'dr', 'ms', 'prof']) ? $Error['contact_title'] = 'Contact Title is invalid' : '') : '';

			(!empty($line[42])) ? (!in_array(strtolower($line[42]), ['director', 'director/shareholder', 'shareholder', 'accountant', 'bookkeeper', 'other']) ? $Error['contact_type'] = 'Contact Type is invalid' : '') : '';

			(!empty($line[43])) ? (!in_array(strtolower($line[43]), ['single', 'living together', 'engaged', 'married', 'civil partner', 'separated', 'divorced', 'widowed']) ? $Error['contact_marital_status'] = 'Contact Marital Status is invalid' : '') : '';

			$username       = $line[6];
			$password       = $line[7];

			if ($username != '') {
				$Deleted_firms        = $this->Common_mdl->Get_Deleted_FirmsIds();
				$Remove_Deleted_Firms = (!empty($Deleted_firms) ? ' AND firm_id NOT IN(' . $Deleted_firms . ') ' : '');

				$sql = $this->db->query("select username from user where username='" . $username . "'" . $Remove_Deleted_Firms)->num_rows();
				if ($sql) {
					$Error['username'] = 'User Name already Exists';
				}
			}
			if ($username != '' && $password == '') {
				$Error['password'] = 'Password is Required';
			}



			if (!empty($Error)) {
				$CSVErrors[] = $Error;
			}
		}
		fclose($handler);

		return $CSVErrors;
	}

	public function import_csv_datas($File)
	{
		$csvFile = fopen($File['tmp_name'], 'r');
		fgetcsv($csvFile);

		$status       = array(1 => 'active', 0 => 'Inactive', 3 => 'Frozen', 5 => 'Archive');
		$company_type =  $this->Common_mdl->Company_types();

		while (($line = fgetcsv($csvFile)) !== FALSE) {
			$password_import  =   (isset($line[7]) && ($line[7] != '')) ? $line[7] : '';

			$data = array(
				"crm_name"              =>  $line[1],
				"username"              =>  $line[6],
				"password"              =>  md5($password_import),
				"confirm_password"      =>  $password_import,
				"crm_email_id"          =>  $line[58],
				"crm_phone_number"      =>  $line[57],
				"status"                => (!empty($client_status) ? $client_status  : ''),
				"frozen_freeze"         =>  0,
				"CreatedTime"           =>  time(),
				"autosave_status"       =>  0,
				'user_type'             =>  'FC',
				'firm_id'               =>  $_SESSION['firm_id']
			);
			$inser_data = $this->db->insert("user", $data);

			if ($inser_data) {
				$client_status    =   array_search($line[15], $status);
				$client_status    = (!empty($client_status) ? $client_status : '');

				$client_company_type = array_search(strtolower($line[2]), $company_type);
				$client_company_type = (!empty($client_company_type) ? $client_company_type : '');

				$in = $this->db->insert_id();
				$referral_code = $this->Common_mdl->Get_ClientReferral_Code($line[1]);

				$data =  array(
					"user_id"                                       =>  $in,
					'firm_id'                                       =>  $_SESSION['firm_id'],
					"crm_company_name"                              =>  $line[1],
					"crm_company_name1"                             =>  $line[1],
					"crm_legal_form"                                =>  $client_company_type,
					//"crm_allocation_holder"                       =>  $line[3], 
					"crm_company_number"                            =>  $line[3],
					"crm_company_url"                               =>  $line[18],
					"crm_officers_url"                              =>  $line[19],
					"crm_incorporation_date"                        =>  $line[8],
					"crm_registered_in"                             =>  $line[9],
					"crm_address_line_one"                          =>  $line[10],
					"crm_address_line_two"                          =>  $line[11],
					"crm_address_line_three"                        =>  $line[12],
					"crm_town_city"                                 =>  $line[13],
					"crm_post_code"                                 =>  $line[14],
					"crm_company_status"                            =>  $client_status,
					"crm_company_type"                              =>  $client_company_type,
					"crm_company_sic"                               =>  $line[16],
					"crm_letter_sign"                               =>  $line[17],
					"crm_business_website"                          =>  $line[20],
					"crm_accounting_system"                         =>  $line[21],
					//"crm_companies_house_authorisation_code"      =>  $line[21],
					//"crm_company_utr"                             =>  $line[23],
					//"crm_accounts_office_reference"               =>  $line[24],
					//"crm_paye_ref_number"                         =>  $line[25],
					"crm_vat_number"                                =>  $line[133],
					"crm_refered_by"                                =>  $referral_code,
					"crm_assign_client_id_verified"                 =>  $this->is_enabled($line[46]),
					"crm_assign_type_of_id"                         =>  $line[47],
					"crm_assign_proof_of_address"                   =>  $this->is_enabled($line[48]),
					"crm_assign_meeting_client"                     =>  $this->is_enabled($line[49]),
					"crm_assign_source"                             =>  $line[50],
					//"crm_refered_by"                              =>  $line[51],
					//"crm_assign_relationship_client"              =>  $line[52],
					"crm_assign_notes"                              =>  $line[53],
					"crm_previous_accountant"                       =>  $line[54],
					"crm_other_name_of_firm"                        =>  $line[55],
					"crm_other_address"                             =>  $line[56],
					"crm_other_contact_no"                          =>  $line[57],
					"crm_email"                                     =>  $line[58],
					"crm_other_chase_for_info"                      =>  $line[59],
					"crm_other_notes"                               =>  $line[60],
					"crm_other_internal_notes"                      =>  $line[61],
					//"crm_other_invite_use"                        =>  $line[43],
					//"crm_other_crm"                               =>  $line[44],
					//"crm_other_proposal"                          =>  $line[45],
					//"crm_other_task"                              =>  $line[46],
					//"crm_other_send_invit_link"                   =>  $line[47],
					"crm_other_username"                            =>  $line[6],
					"crm_other_password"                            =>  $line[7],
					//"crm_other_any_notes"                         =>  $line[53],
					"conf_statement"                                =>  '{"tab":"' . $this->is_enabled($line[62]) . '"}',
					"crm_confirmation_auth_code"                    =>  $line[63],
					"crm_confirmation_statement_date"               =>  $line[64],
					"crm_confirmation_statement_due_date"           =>  $line[65],
					"crm_confirmation_statement_last_made_up_to_date" =>  $line[66],
					"crm_confirmation_officers"                     =>  $line[67],
					"crm_share_capital"                             =>  $line[68],
					"crm_shareholders"                              =>  $line[69],
					"crm_people_with_significant_control"           =>  $line[70],
					/* "crm_confirmation_notes"                      =>  $line[62],*/
					"accounts"                                      =>  '{"tab":"' .  $this->is_enabled($line[71]) . '"}',
					"crm_accounts_auth_code"                        =>  $line[72],
					"crm_accounts_utr_number"                       =>  $line[73],
					"crm_companies_house_email_remainder"           =>  $this->is_enabled($line[74]),
					"crm_ch_yearend"                                =>  $line[75],
					"crm_ch_accounts_next_due"                      =>  $line[76],
					/*"crm_accounts_notes"                          =>  $line[69],*/
					"crm_accounts_last_made_up_to_date"             =>  $line[77],
					"company_tax_return"                            =>  '{"tab":"' . $this->is_enabled($line[78]) . '"}',
					"crm_accounts_due_date_hmrc"                    =>  $line[79],
					"crm_accounts_tax_date_hmrc"                    =>  $line[80],
					"personal_tax_return"                           =>  '{"tab":"' . $this->is_enabled($line[81]) . '"}',
					"crm_personal_utr_number"                       =>  $line[82],
					"crm_ni_number"                                 =>  $line[83],
					"crm_property_income"                           =>  $this->is_enabled($line[84]),
					"crm_additional_income"                         =>  $this->is_enabled($line[85]),
					"crm_personal_tax_return_date"                  =>  $line[87],
					"crm_personal_due_date_return"                  =>  $line[88],
					"crm_personal_due_date_online"                  =>  $line[89],
					/*"crm_personal_notes"                          =>  $line[81],*/
					"payroll"                                       =>  '{"tab":"' . $this->is_enabled($line[90]) . '"}',
					"crm_payroll_acco_off_ref_no"                   =>  $line[91],
					"crm_paye_off_ref_no"                           =>  $line[92],
					"crm_payroll_reg_date"                          =>  $line[93],
					"crm_no_of_employees"                           =>  $line[94],
					"crm_first_pay_date"                            =>  $line[95],
					"crm_payroll_run"                               =>  $line[96],
					"crm_payroll_run_date"                          =>  $line[97],
					"crm_rti_deadline"                              =>  $line[98],
					"crm_previous_year_require"                     =>  $this->is_enabled($line[99]),
					"crm_paye_scheme_ceased"                        =>  $line[100],
					"crm_payroll_if_yes"                            =>  $line[101],
					"workplace"                                     =>  '{"tab":"' . $this->is_enabled($line[102]) . '"}',
					"crm_staging_date"                              =>  $line[103],
					"crm_pension_id"                                =>  $line[104],
					"crm_pension_subm_due_date"                     =>  $line[105],
					"crm_postponement_date"                         =>  $line[106],
					"crm_the_pensions_regulator_opt_out_date"       =>  $line[107],
					"crm_re_enrolment_date"                         =>  $line[108],
					"crm_declaration_of_compliance_due_date"        =>  $line[109],
					"crm_declaration_of_compliance_submission"      =>  $line[110],
					"crm_paye_pension_provider"                     =>  $line[111],
					"crm_paye_pension_provider_password"            =>  $line[112],
					"crm_employer_contri_percentage"                =>  $line[113],
					"crm_employee_contri_percentage"                =>  $line[114],
					/*"crm_pension_notes"                           =>  $line[106],*/
					"cis"                                           =>  '{"tab":"' . $this->is_enabled($line[115]) . '"}',
					"crm_cis_frequency"                             =>  $line[116],
					"crm_cis_contractor"                            =>  $this->is_enabled($line[117]),
					"crm_cis_contractor_start_date"                 =>  $line[118],
					"crm_cis_scheme_notes"                          =>  $line[119],
					"cissub"                                        =>  '{"tab":"' . $this->is_enabled($line[120]) . '"}',
					"crm_cis_subcontractor"                         =>  $this->is_enabled($line[121]),
					"crm_cis_subcontractor_start_date"              =>  $line[122],
					"crm_cis_subcontractor_scheme_notes"            =>  $line[123],

					"p11d"                                          => '{"tab":"' . $this->is_enabled($line[124]) . '"}',

					"crm_p11d_latest_action_date"                   =>  $line[125],
					"crm_p11d_latest_action"                        =>  $line[126],
					"crm_latest_p11d_submitted"                     =>  $line[127],
					"crm_next_p11d_return_due"                      =>  $line[128],
					"crm_p11d_previous_year_require"                =>  $this->is_enabled($line[129]),
					"crm_p11d_payroll_if_yes"                       =>  $line[130],
					"crm_p11d_records_received"                     =>  $line[131],
					/*"crm_p11d_notes"=>$line[121],*/
					"vat"                                           =>  '{"tab":"' . $this->is_enabled($line[132]) . '"}',
					"crm_vat_number_one"                            =>  $line[133],
					"crm_vat_date_of_registration"                  =>  $line[134],
					"crm_vat_frequency"                             =>  $line[135],
					"crm_vat_quater_end_date"                       =>  $line[136],
					"crm_vat_quarters"                              =>  $line[137],
					"crm_vat_due_date"                              =>  $line[138],
					"crm_vat_scheme"                                =>  $line[139],
					"crm_flat_rate_category"                        =>  $line[140],
					"crm_flat_rate_percentage"                      =>  $line[141],
					"crm_direct_debit"                              =>  $this->is_enabled($line[142]),
					"crm_annual_accounting_scheme"                  =>  $this->is_enabled($line[143]),
					"crm_box5_of_last_quarter_submitted"            =>  $line[144],
					"crm_vat_address"                               =>  $line[145],
					/*"crm_vat_notes"=>$line[135],*/
					"bookkeep"                                      =>  '{"tab":"' . $this->is_enabled($line[146]) . '"}',
					"crm_next_booking_date"                         =>  $line[147],
					"crm_method_bookkeeping"                        =>  $line[148],
					"crm_bookkeeping"                               =>  $line[149],
					"crm_client_provide_record"                     =>  $line[150],

					"management"                                    =>  '{"tab":"' . $this->is_enabled($line[151]) . '"}',
					"crm_manage_acc_fre"                            =>  $line[152],
					"crm_next_manage_acc_date"                      =>  $line[153],
					"crm_manage_method_bookkeeping"                 =>  $line[154],
					"crm_manage_client_provide_record"              =>  $line[155],
					/*"crm_manage_notes"=>$line[145],*/
					"investgate"                                    =>  '{"tab":"' . $this->is_enabled($line[156]) . '"}',
					"crm_invesitgation_insurance"                   =>  $line[157],
					"crm_insurance_renew_date"                      =>  $line[158],
					"crm_insurance_provider"                        =>  $line[159],
					"crm_claims_note"                               =>  $line[160],

					"registered"                                    =>  '{"tab":"' . $this->is_enabled($line[161]) . '"}',
					"crm_registered_start_date"                     =>  $line[162],
					"crm_registered_renew_date"                     =>  $line[163],
					"crm_registered_office_inuse"                   =>  $line[164],
					"crm_registered_claims_note"                    =>  $line[165],

					"taxadvice"                                     =>  '{"tab":"' . $this->is_enabled($line[166]) . '"}',
					"crm_investigation_start_date"                  =>  $line[167],
					"crm_investigation_end_date"                    =>  $line[168],
					/*"crm_investigation_note"                      =>  $line[158],*/
					/*"crm_tax_investigation_note"                  =>  $line[159],*/
					"status"                                        =>  2,
					"created_date"                                  =>  time()
				);
				$insert_client = $this->db->insert("client", $data);
				$count[] = $client_id = $this->db->insert_id();

				$log = "Client Created from Import Section";

				$this->Common_mdl->Create_Log('client', $in, $log);


				$data = array(
					"client_id"         =>  $in,
					"title"             =>  $line[22],
					"first_name"        =>  $line[4],
					"middle_name"       =>  $line[24],
					"surname"           =>  $line[25],
					"preferred_name"    =>  $line[26],
					"mobile"            =>  $line[27],
					"main_email"        =>  $line[5],
					"nationality"       =>  $line[28],
					"psc"               =>  $line[29],
					"ni_number"         =>  $line[30],
					"address_line1"     =>  $line[32],
					"address_line2"     =>  $line[33],
					"town_city"         =>  $line[34],
					"post_code"         =>  $line[35],
					"landline"          =>  $line[36],
					"work_email"        =>  $line[37],
					"date_of_birth"     =>  $line[38],
					"nature_of_control" =>  $line[39],
					"utr_number"        =>  $line[40],
					"shareholder"       =>  $line[41],
					"contact_type"      =>  $line[42],
					"marital_status"    =>  $line[43],
					"make_primary"      =>  1,
					"created_date"      =>  time()
				);
				$this->db->insert("client_contacts", $data);
				$this->Common_mdl->Increase_Decrease_ClientCounts($_SESSION['firm_id'], '-1');
				$data = $this->db->query("SELECT ot.id FROM user as u INNER JOIN organisation_tree as ot ON ot.user_id= u.id WHERE u.firm_id=" . $_SESSION['firm_id'] . " and u.user_type='FA'")->row_array();

				$this->Update_Client_assignee($client_id, $data);

				/*For Create Document to the client*/
				$this->Common_mdl->createparentDirectory($in);
			}
			$limit  = $this->Common_mdl->Check_Firm_Clients_limit($_SESSION['firm_id']);
			if (!($limit > 0 || $limit == 'unlimited')) {
				break;
			}
		}
		fclose($csvFile);

		return $count;
	}
	/*from notification*/
	public function Update_CH_Datas($client_id, $new_datas)
	{
		if (!empty($new_datas)) {
			$this->db->update('client', $new_datas, 'id=' . $client_id);

			$user_id = $this->Common_mdl->get_field_value('client', 'user_id', 'id', $client_id);

			$log = "Company house Data Updated";
			$this->Common_mdl->Create_Log('client', $user_id, $log);

			$this->db->where("module='client' and notification_type='CH' and sender_id=" . $client_id)
				->update('notification_management', ['status' => 0]);

			$IS_service_data_edited  = [];
			if (!empty($new_datas['crm_hmrc_yearend']))
				$IS_service_data_edited[2] = 1;

			if (!empty($new_datas['crm_confirmation_statement_date']))
				$IS_service_data_edited[1] = 1;

			$this->Service_Reminder_Model->Setup_Client_ServiceReminders($client_id, $IS_service_data_edited);
			return 1;
		}
		return 0;
	}
	/*from client list page select all sync button*/
	public function sync_ch(array $ids)
	{
		$client_ids = $this->db->select('id')
			->where_in('user_id', $ids)
			->get('client')
			->result_array();

		foreach ($client_ids as $array) {

			$new_datas = $this->CompanyHouse_Model->Find_NewData_Updates($array['id']);

			$this->Update_CH_Datas($array['id'], $new_datas);
		}
		return 1;
	}

	public function delete_clients($ids)
	{
		$ids = json_decode($_POST['id'], true);


		foreach ($ids as $id) {

			$client_id = $this->db->select('id')
				->get_where('client', 'user_id=' . $id)
				->row('id');

			$this->Common_mdl->delete('user', 'id', $id);
			$this->Common_mdl->delete('client', 'user_id', $id);
			/** for schedule delete option **/
			$this->Common_mdl->delete('timeline_services_notes_added', 'user_id', $id);
			/*for reminder deletion*/
			$this->db->delete('client_service_frequency_due', 'client_id=' . $client_id);
			$this->db->delete('service_reminder_cron_data', 'client_id=' . $client_id);
			$this->db->delete('client_reminder', 'client_id=' . $client_id);

			$this->Common_mdl->Increase_Decrease_ClientCounts($_SESSION['firm_id'], '+1');
		}
		return 1;
	}
	public function get_queue_service_reminders($client_id)
	{
		$query = "SELECT a.id,b.service_name,a.client_contacts_id,a.template_type,a.template_id,a.status,UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) as date  FROM `service_reminder_cron_data` AS a INNER JOIN service_lists AS b on b.id = a.service_id WHERE a.client_id = " . $client_id . " and (a.status=0 or a.status=2) order by a.client_contacts_id,b.service_name,date ASC";

		return $this->service_reminders($client_id, $query);
	}
	public function get_draft_service_reminders($client_id)
	{
		$query = "SELECT a.id,b.service_name,a.client_contacts_id,a.template_type,a.template_id,UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) as date  FROM `draft_service_reminder` AS a INNER JOIN service_lists AS b on b.id = a.service_id WHERE a.client_id = " . $client_id . " order by a.client_contacts_id,b.service_name,date ASC";
		return $this->service_reminders($client_id, $query);
	}
	public function service_reminders($client_id, $query)
	{
		if (!class_exists('Service_Reminder_Model'))
			$this->load->model('Service_Reminder_Model');

		$queue_reminders = $this->db->query($query)->result_array();

		foreach ($queue_reminders as $key => $value) {

			//if client_contacts_id exit we should send as array.
			if ($value['client_contacts_id'] != 0) {
				$client            = [
					'client_id'         =>  $client_id,
					'client_contacts_id' =>  $value['client_contacts_id']
				];
			} else {
				$client               = $client_id;
			}

			$content = $this->Service_Reminder_Model->get_reminder_template_data($client, $value['template_id'], $value['template_type']);

			$queue_reminders[$key] = array_merge($value, $content);
		}

		return $queue_reminders;
	}
	public function send_draft_reminder($draft_id)
	{
		$data  = $this->db->get_where('draft_service_reminder', 'id=' . $draft_id)->row_array();

		if ($data['service_id'] == 4 && $data['client_contacts_id'] != 0) {
			$query =  "SELECT client_id as `user_id`,main_email FROM client_contacts WHERE id=" . $data['client_contacts_id'];
		} else {

			$query = "SELECT c.user_id,cc.main_email FROM client as c INNER JOIN client_contacts as cc on cc.make_primary=1 and cc.client_id = c.user_id WHERE c.id=" . $data['client_id'];
		}

		$client_data = $this->db->query($query)->row_array();

		$content['subject'] = $this->input->post('subject');
		$content['body']    = $this->input->post('body');

		$IsSend = firm_settings_send_mail($_SESSION['firm_id'], $client_data['main_email'], $content['subject'], $content['body']);

		if ($IsSend['result']) {
			$insert_data = [
				'client_id'         =>  $data['client_id'],
				'reminder_subject'  =>  $content['subject'],
				'reminder_message'  =>  $content['body']
			];
			$this->db->insert("client_reminder",  $insert_data);

			$log  = $content['subject'] . "<b> Draft Reminder Send By </b> <i>" . $_SESSION['crm_name'] . "</i>";
			$this->Common_mdl->Create_Log('client', $client_data['user_id'],  $log);

			$this->db->delete('draft_service_reminder', 'id=' . $data['id']);

			return $this->db->affected_rows();
		}
	}

	public function delete_reminders()
	{
		$ids    = $this->input->post('ids');
		$table  = $this->input->post('table');
		$this->db->where_in('id', $ids);
		$this->db->delete($table);
		return 1;
	}

	public function list_get_source()
	{
		$output = array(
			"draw"                  =>  $_POST['draw'],
			"recordsTotal"          =>  0,
			"recordsFiltered"       =>  0,
			"data"                  =>  [],
			"query"                 =>  '',
		);

		//Assined clients to the logined user
		$Assigned_Client = $this->input->post('assigned_client');

		if (empty($Assigned_Client)) {
			echo json_encode($output);
			return;
		}

		$FirmFields_Settings = $this->FirmFields_model->Get_FirmFields_Settings($_SESSION['firm_id']);

		$fields = implode(',', array_column($FirmFields_Settings, 'query'));


		//for show number of records 
		$total_c = $this->db->select('count(id) as total')
			->get_where('user', "id IN(" . $Assigned_Client . ")")
			->row('total');

		//if search clients by specific assignees
		if (!empty($_POST['column_filter']['client__client_assignees'])) {
			$assigness = $_POST['column_filter']['client__client_assignees'];

			unset($_POST['column_filter']['client__client_assignees']);

			$Assigned_Client = [];
			foreach ($assigness as $value) {

				$result = get_specific_assignee_module_data('CLIENT', $value);
				$Assigned_Client = array_merge($Assigned_Client, $result);
			}

			if (empty($Assigned_Client)) {
				echo json_encode($output);
				return;
			}

			//change client id into user id

			$Assigned_Client = $this->db->where_in('id', $Assigned_Client)
				->select("user_id")
				->get("client")
				->result_array();

			$Assigned_Client = implode(',', array_column($Assigned_Client, 'user_id'));

			if (empty($Assigned_Client)) {
				echo json_encode($output);
				return;
			}
		}



		$BASE_Q = "SELECT user.id as 'user_id'," . $fields . ",'action' FROM user 
                INNER JOIN client on client.user_id=user.id 
                LEFT JOIN client_contacts on client_contacts.client_id=user.id and client_contacts.make_primary=1
                WHERE user.id IN (" . $Assigned_Client . ")";

		$Having         = "";

		$column_filter  = "";

		//change column filter client status
		if (!empty($_POST['column_filter']['client__crm_company_status'])) {
			$_POST['filter']['user__status'] = implode('|', $_POST['column_filter']['client__crm_company_status']);
			unset($_POST['column_filter']['client__crm_company_status']);
		}

		//for json data filtering 
		$json_column = ['client_contacts__landline', 'client_contacts__work_email'];
		foreach ($json_column as $v) {
			if (!empty($_POST['column_filter'][$v])) {
				$field = str_replace('__', '.', $v);

				$like_group = "";
				foreach ($_POST['column_filter'][$v] as $value) {
					$like_group .= (!empty($like_group) ? ' OR ' : ' ( ') . " " . $field . " LIKE '%" . $value . "%' ";
				}
				$like_group .= " ) ";

				$column_filter .= " AND " . $like_group;

				//don't add as where condtion
				unset($_POST['column_filter'][$v]);
			}
		}


		//for each column filters
		$_POST['column_filter'] = (!empty($_POST['column_filter']) ? $_POST['column_filter'] : []);

		foreach ($_POST['column_filter'] as $key => $value) {
			$field_array = explode('__', $key, 2);

			if (!empty($value)) {
				$value = array_map(function ($v) {
					return addslashes($v);
				}, $value);

				$value = "'" . implode("','", $value) . "'";

				//for Custom Fields
				if ($field_array[0] == 'tblcustomfieldsvalues') {
					$Having        .= (!empty($Having) ? " AND " : " HAVING ") . "`" . $field_array[1] . "` IN (" . $value . ") ";
				} else {
					$field = $field_array[0] . "." . $field_array[1];

					$column_filter .= " AND " . $field . " IN (" . $value . ") ";
				}
			}
		}


		//other primary filters (tab,toolbar filter)
		$filter = "";
		$_POST['filter'] = (!empty($_POST['filter']) ? $_POST['filter'] : []);
		foreach ($_POST['filter'] as $key => $value) {
			$field = explode('__', $key, 2);

			$field = $field[0] . "." . $field[1];

			if (!empty($value)) {

				if (strpos($value, '|') !== FALSE) {
					$value = "'" . implode("','", explode('|', $value)) . "'";
					$filter .= " AND " . $field . " IN (" . $value . ")";
				} else {
					$filter .= " AND " . $field . "=" . $value;
				}
			}
		}



		$from_to = "";
		if (!empty($_POST['created_from'])) {
			$_POST['created_from'] = date_format(date_create_from_format('d-m-Y', $_POST['created_from']), 'U');
			$from_to .= " AND user.CreatedTime > " . $_POST['created_from'];
		}

		if (!empty($_POST['created_to'])) {
			$_POST['created_to'] = date_format(date_create_from_format('d-m-Y', $_POST['created_to']), 'U');
			$from_to .= " AND user.CreatedTime < " . $_POST['created_to'];
		}

		//for overall search 
		if (!empty($_POST['search']['value'])) {

			$displayed_column = array_column($FirmFields_Settings, 'list_page_visibility', 'column');

			$displayed_column = array_keys($displayed_column, '1');

			$Having_Like = "";
			$search = strtolower(addslashes($_POST['search']['value']));

			foreach ($displayed_column as $dis_column) {
				$Having_Like  .= (!empty($Having_Like) ? " OR " : " ( ") . "LCASE(`" . $dis_column . "`) LIKE '%" . $search . "%'";
			}

			$Having_Like .= " ) ";

			$Having .= (!empty($Having) ? " AND " : " HAVING ") . $Having_Like;
		}



		$ordering = "";
		$_POST['order'] = (!empty($_POST['order']) ? $_POST['order'] : []);

		foreach ($_POST['order'] as $key => $value) {

			$field_array = explode('__', $key, 2);

			//don't add table name to sub jquery fields 
			if ($field_array[0] == "tblcustomfieldsvalues") {
				$field = "`" . $field_array[1] . "`";
			} else {
				$field = $field_array[0] . "." . $field_array[1];
			}

			$ordering .= (!empty($ordering) ? "," . $field . " " . $value : " ORDER BY " . $field . " " . $value);
		}

		/* AFTER WHERE CONDITION PART*/
		$BASE_Q .= $column_filter;
		$BASE_Q .= $filter;
		$BASE_Q .= $from_to;

		/*HAVING CONDITION PART*/
		$BASE_Q .= $Having;

		/*ORDERING PART*/
		$BASE_Q .= $ordering;


		/*LIMIT PART*/
		///$BASE_Q .= " LIMIT ".$_POST['start'].",".$_POST['length'];
	
		$rows1   = $this->db->query($BASE_Q)->result_array();

		$rows = [];
		$total_hrs = 0;
		$total_fees = 0;
		foreach ($rows1 as $row) {
			$service_check = $this->db->query('SELECT has_ptr_service from client_contacts WHERE client_id =' . $row['user_id'])->row_array();
			if (isset($row['crm_personal_tax_return_date']) && ($row['crm_personal_tax_return_date'] != '')) {
				if (isset($service_check['has_ptr_service']) && ($service_check['has_ptr_service'] != 1)) {
					$row['crm_personal_tax_return_date'] = '--';
				}
			}
			$total_hrs += (float)$row[666];
			$total_fees += (float)$row[665];
			$rows[] = $row;
		}

		$FILTERD_RECORDE = count($rows);

		$rows   = array_slice($rows, $_POST['start'], $_POST['length']);

		/*ADD EXTRA STYLE AND INFO TO DATA*/
		$rows   = $this->add_list_content($rows);

		
		/*CHANGE INDEX ARRAY OF ROWS */
		$rows   = array_map(function ($row) {
			return array_values($row);
		}, $rows);


		$output["recordsTotal"]     =  $total_c;
		$output["recordsFiltered"]  =  $FILTERD_RECORDE;
		$output["data"]             =  $rows;
		$output["query"]            =  $BASE_Q;
		$output["total_hrs"]        =  $total_hrs;
		$output["total_fees"]       =  $total_fees;

		echo json_encode($output);
	}

	public function get_column_filters($table, $column)
	{
		$helper_f["crm_company_status"]  =  'ClientColumn_Status_SelectBox';
		$helper_f["client_assignees"]    =  'ClientColumn_FirmStaffs_SelectBox';
		$helper_f["crm_legal_form"]      =  'get_firm_type';
		$helper_f["landline"]            =  'get_client_landline';
		$helper_f["work_email"]          =  'get_client_work_email';


		if ($table == "tblcustomfieldsvalues") {
			$data = $this->db->select("DISTINCT(value) as value")
				->get_where('tblcustomfieldsvalues', "value !='' and fieldid=" . $column . " and relid in(" . $_POST['client_user_id'] . ")")
				->result_array();
		} else {
			//user_id fields name diffrent in each table

			if ($table == "client_contacts")
				$assign_c = " and client_id in(" . $_POST['client_user_id'] . ")";
			else if ($table == "user")
				$assign_c = " and id in(" . $_POST['client_user_id'] . ")";
			else
				$assign_c = " and user_id in(" . $_POST['client_user_id'] . ")";

			$data = $this->db->select("DISTINCT(" . $column . ") as value")
				->get_where($table, $column . "!='' " . $assign_c)
				->result_array();
		}

		$data = array_column($data, 'value');

		$return = [];

		if (!empty($helper_f[$column])) {
			$return = $helper_f[$column]();
		} else {
			foreach ($data as $value) {
				$return[] =  [$value, Change_DataFormat($value)];
			}
		}

		return $return;
	}

	public function add_list_content($rows)
	{

		$column_helper = [
			'crm_company_name'                    =>  'ClientColumn_Client',
			'crm_legal_form'                      =>  'ClientColumn_Type',
			'crm_company_type'                    =>  'ClientColumn_Type',
			'crm_company_status'                  =>  'ClientColumn_Status',
			'contact_landline'                    =>  'ClientColumn_landlines',
			'contact_work_email'                  =>  'ClientColumn_work_emails',
			'proof_attach_file'                   =>  'ClientColumn_addBaseUrl',
			'client_assignees'                    =>  'ClientColumn_assignees',
			'CreatedTime'                         =>  'ClientColumn_created_date',
			'crm_confirmation_statement_due_date' =>  'ClientColumn_check_CS_ch_overdue',
			'crm_ch_accounts_next_due'            =>  'ClientColumn_check_Acc_ch_overdue'
		];

		foreach ($rows as $k1 => $row) {
			$user_id = $row['user_id'];

			foreach ($row as $k2 => $column) {
				if ($k2 == 'user_id') {
					$row[$k2] = select_row($user_id);
				} else if ($k2 == 'action') {
					$row[$k2] = action_buttons($user_id);
				} else if (isset($column_helper[$k2])) {
					$row[$k2] = $column_helper[$k2]($user_id);
				} else {
					$row[$k2] = Change_DataFormat($column);
				}
				$rows[$k1] = $row;
			}
		}
		return $rows;
	}

	public function update_client_contacts()
	{
		$old_contact_persons =  $this->db->order_by('id')
			->get_where('client_contacts', 'client_id=' . $_POST['user_id'])
			->result_array();


		foreach ($_POST['first_name'] as $key => $details) {
			$data = array();

			$data['title']          =  !empty($_POST['title'][$key]) ? $_POST['title'][$key] : '';
			$data['first_name']     =  !empty($_POST['first_name'][$key]) ? $_POST['first_name'][$key] : '';
			$data['middle_name']    =  !empty($_POST['middle_name'][$key]) ? $_POST['middle_name'][$key] : '';
			$data['last_name']      =  !empty($_POST['last_name'][$key]) ? $_POST['last_name'][$key] : '';
			$data['surname']        =  !empty($_POST['surname'][$key]) ? $_POST['surname'][$key] : '';

			$data['preferred_name'] =  !empty($_POST['preferred_name'][$key]) ? $_POST['preferred_name'][$key] : '';

			$data['mobile']         =  !empty($_POST['mobile'][$key]) ? $_POST['mobile'][$key] : '';
			$data['main_email']     =  !empty($_POST['main_email'][$key]) ? $_POST['main_email'][$key] : '';
			$data['nationality']    =  !empty($_POST['nationality'][$key]) ? $_POST['nationality'][$key] : '';
			$data['psc']            =  !empty($_POST['psc'][$key]) ? $_POST['psc'][$key] : '';
			$data['shareholder']    =  !empty($_POST['shareholder'][$key]) ? $_POST['shareholder'][$key] : '';
			$data['content_type']   =  !empty($_POST['content_type'][$key]) ? $_POST['content_type'][$key] : '';
			$data['address_line1']  =  !empty($_POST['address_line1'][$key]) ? $_POST['address_line1'][$key] : '';
			$data['address_line2']  =  !empty($_POST['address_line2'][$key]) ? $_POST['address_line2'][$key] : '';
			$data['town_city']      =  !empty($_POST['town_city'][$key]) ? $_POST['town_city'][$key] : '';
			$data['post_code']      =  !empty($_POST['post_code'][$key]) ? $_POST['post_code'][$key] : '';
			$data['date_of_birth']  =  !empty($_POST['date_of_birth'][$key]) ? $_POST['date_of_birth'][$key] : '';
			$data['send_birthday_reminders']  =  !empty($_POST['send_birthday_reminders'][$key]) ? $_POST['send_birthday_reminders'][$key] : 0;
			$data['nature_of_control'] =  !empty($_POST['nature_of_control'][$key]) ? $_POST['nature_of_control'][$key] : '';

			$data['marital_status'] =  !empty($_POST['marital_status'][$key]) ? $_POST['marital_status'][$key] : '';

			$data['occupation']     =  !empty($_POST['occupation'][$key]) ? $_POST['occupation'][$key] : '';
			$data['appointed_on']   =  !empty($_POST['appointed_on'][$key]) ? $_POST['appointed_on'][$key] : '';
			$data['country_of_residence'] =  !empty($_POST['country_of_residence'][$key]) ? $_POST['country_of_residence'][$key] : '';

			$data['other_custom']   =  !empty($_POST['other_custom'][$key]) ? $_POST['other_custom'][$key] : '';


			$data['has_ptr_service']      =  !empty($_POST['ccp_ptr_service'][$key]) ? $_POST['ccp_ptr_service'][$key] : '';
			$data['has_ptr_reminder']     =  !empty($_POST['ccp_ptr_reminder'][$key]) ? $_POST['ccp_ptr_reminder'][$key] : '';
			$data['has_ptr_invoice']      =  !empty($_POST['ccp_ptr_invoice'][$key]) ? $_POST['ccp_ptr_invoice'][$key] : '';
			$data['has_ptr_text']         =  !empty($_POST['ccp_ptr_text'][$key]) ? $_POST['ccp_ptr_text'][$key] : '';


			$data['ni_number']      =  !empty($_POST['ccp_ni_number'][$key]) ? $_POST['ccp_ni_number'][$key] : '';
			$data['utr_number']     =  !empty($_POST['ccp_personal_utr_number'][$key]) ? $_POST['ccp_personal_utr_number'][$key] : '';

			$data['property_income']              =  !empty($_POST['ccp_property_income'][$key]) ? $_POST['ccp_property_income'][$key] : '';
			$data['additional_income']            =  !empty($_POST['ccp_additional_income'][$key]) ? $_POST['ccp_additional_income'][$key] : '';
			$data['property_income_notes']        =  !empty($_POST['ccp_property_income_notes'][$key]) ? $_POST['ccp_property_income_notes'][$key] : '';

			$data['personal_tax_return_date']     = '';
			if (!empty($_POST['ccp_personal_tax_return_date'][$key])) {
				$data['personal_tax_return_date']     =   Change_Date_Format($_POST['ccp_personal_tax_return_date'][$key], 'd-m-Y', 'Y-m-d');
			}

			$data['personal_due_date_return']     =  '';
			if (!empty($_POST['ccp_personal_due_date_return'][$key])) {
				$data['personal_due_date_return']     =  Change_Date_Format($_POST['ccp_personal_due_date_return'][$key], 'd-m-Y', 'Y-m-d');
			}

			$data['personal_due_date_online']     =  '';
			if (!empty($_POST['ccp_personal_due_date_online'][$key])) {
				$data['personal_due_date_online']     =  Change_Date_Format($_POST['ccp_personal_due_date_online'][$key], 'd-m-Y', 'Y-m-d');
			}

			$data['is_ptr_as_per_firm_reminder']  =  !empty($_POST['ccp_ptr_as_per_firm_reminder'][$key]) ? $_POST['ccp_ptr_as_per_firm_reminder'][$key] : '';
			$data['is_ptr_cus_reminder']          =  !empty($_POST['ccp_ptr_cus_reminder'][$key]) ? $_POST['ccp_ptr_cus_reminder'][$key] : '';

			$data['personal_tax_return_required'] = !empty($_POST['personal_tax_return_required'][$key]) ? $_POST['personal_tax_return_required'][$key] : '';

			$data['work_email']     = '';
			if (isset($_POST['work_email'][$key])) {
				$data['work_email'] =  json_encode(array_filter($_POST['work_email'][$key]));
			}


			$data['landline'] = '';
			if (!empty($_POST['landline'][$key])) {
				$_POST['landline'][$key] = array_filter($_POST['landline'][$key]);

				$data['landline'] =  json_encode($_POST['landline'][$key]);
			}

			$data['pre_landline'] = '';
			if (!empty($_POST['pre_landline'][$key]) && !empty($_POST['landline'][$key])) {
				$_POST['pre_landline'][$key] = array_intersect_key($_POST['pre_landline'][$key], $_POST['landline'][$key]);
				$data['pre_landline'] =  json_encode($_POST['pre_landline'][$key]);
			}

			$mk = (isset($_POST['make_primary'])) ? $_POST['make_primary'] : '0';

			$data['make_primary'] = '0';
			if ($mk == $key) {
				$data['make_primary'] = '1';
			}

			$data['client_id']      =  !empty($_POST['user_id']) ? $_POST['user_id'] : '';

			if ($_POST['contact_table_id'][$key] != '') {
				$this->db->update('client_contacts', $data, "id =" . $_POST['contact_table_id'][$key]);
			} else {
				$this->Common_mdl->insert('client_contacts', $data);
				$_POST['contact_table_id'][$key] = $this->db->insert_id();
			}

			if(isset($_POST['user_id']) && ($_POST['user_id'] != '')){
				$this->welcome_mail($_POST['user_id']);
			}
		}

		//don't call service reminder function when auto
		if (!empty($_POST['submit'])) {
			if (!class_exists('Service_Reminder_Model'))
				$this->load->model('Service_Reminder_Model');

			$Edited_service = array_intersect($_POST['is_service_data_edited'], [1]);
			$Edited_service = array_keys($Edited_service);

			ob_start();
			$this->Service_Reminder_Model->Setup_ClientContactPerson_ServiceReminders($_POST['user_id'], $Edited_service);
			ob_end_clean();
		}
		$this->Service_Reminder_Model->setup_client_contact_service_activity_log($old_contact_persons, $_POST['user_id']);
		//for custom fields save  
		$ccp_custom_fields = [];
		if (!empty($_POST['custom_fields']['Contact_Person'])) {
			$ccp_custom_fields['Contact_Person']  = $_POST['custom_fields']['Contact_Person'];
		}
		if (!empty($_POST['custom_fields']['4'])) {
			$ccp_custom_fields['4']  = $_POST['custom_fields']['4'];
		}

		foreach ($ccp_custom_fields as $section_name => $section_fields) {

			foreach ($section_fields as $fieldid => $fields) {

				foreach ($fields as $cnt => $value) {

					$relid      = $_POST['user_id'] . "--" . $_POST['contact_table_id'][$cnt];
					$condition  = [
						'relid'       =>  $relid,
						'fieldid'     =>  $fieldid,
						'section_name' =>  $section_name
					];
					$data       = array_merge($condition, ['value' => $value]);
					$this->Common_mdl->UpdateExit_or_Insert('tblcustomfieldsvalues', $data, $condition);
				}
			}
		}
		return $_POST['contact_table_id'];
	}

	public function welcome_mail($user_id)
	{
		$Client_detils = $this->db->query("select id,firm_id from client where user_id=" . $user_id . " and crm_welcome_email_sent != 1 and crm_other_invite_use !='' ")->row_array();
		$contact_data = $this->db->query('select * from client_contacts where client_id=' . $user_id . ' and make_primary=1 and main_email!=""')->row_array();
		//9 refer to action_id of email_templates_actions it connect with email_tempate.
		$EMAIL_TEMPLATE = $this->Common_mdl->getTemplates(9);
		if (!empty($EMAIL_TEMPLATE) && !empty($Client_detils) && !empty($contact_data)) {

			$EMAIL_TEMPLATE = end($EMAIL_TEMPLATE);

			$decode['subject'] = html_entity_decode($EMAIL_TEMPLATE['subject']);
			$decode['body']    = html_entity_decode($EMAIL_TEMPLATE['body']);

			$decoded = Decode_ClientTable_Datas($Client_detils['id'], $decode);

			$Issend = firm_settings_send_mail($Client_detils['firm_id'], $contact_data['main_email'], $decoded['subject'], $decoded['body']);

			if ($Issend['result'] == 1) {
				$this->db->update("client", ['crm_welcome_email_sent' => 1], "user_id=" . $user_id);

				$log = "Welcome Mail Send";
				$this->Common_mdl->Create_Log('client', $user_id, $log);
			}
		}
	}
	public function Get_Service_Assignees($client_id)
	{
		$return = [];

		$enabled_service    =   $this->Common_mdl->get_FirmEnabled_Services();
		$enabled_service    =   array_filter(array_keys($enabled_service));

		function return_last_node($row)
		{
			$return = '';
			if (!empty($row)) {
				$data   = explode(":",  $row['assignees']);
				$return = end($data);
			}
			return $return;
		}

		foreach ($enabled_service as $s_id) {
			$service_manager  =  $this->db->select('assignees')
				->where("module_name='CLIENT' and module_id=" . $client_id . " and sub_module_name='service_manager' and sub_module_id=" . $s_id)
				->get('firm_assignees')
				->result_array();


			$service_manager = array_filter(array_map('return_last_node', $service_manager));


			$service_assignee = $this->db->select('assignees')
				->where("module_name='CLIENT' and module_id=" . $client_id . " and sub_module_name='service_assignee' and sub_module_id=" . $s_id)
				->get('firm_assignees')
				->result_array();

			$service_assignee = array_filter(array_map('return_last_node', $service_assignee));

			if (!empty($service_manager) || !empty($service_assignee)) {
				$return[$s_id]['manager']    = $service_manager;
				$return[$s_id]['assignee']   = $service_assignee;
			}
		}

		return $return;
	}
	public function Get_Service_Assignee_Details($client_id)
	{
		$this->load->model('service_model');
		$service_datails      = $this->service_model->firm_service_details();
		$service_datails      = array_column($service_datails, 'service_name', 'id');

		$return_data          = [];

		$assignees            = $this->Get_Service_Assignees($client_id);

		foreach ($assignees as $s_id => $assignee_nodeids) {

			foreach ($assignee_nodeids as $assignee => $nodeids) {

				foreach ($nodeids as $key => $node_id) {

					$query  = "SELECT user.user_type,user.crm_name FROM organisation_tree as ot
                     INNER JOIN user ON user.id = ot.user_id
                     WHERE ot.id=" . $node_id;

					$source = $this->db->query($query)
						->row_array();

					$s_n  = $service_datails[$s_id];
					$return_data[$s_n][$assignee][$key] = $source['crm_name'];
				}
			}
		}
		return $return_data;
	}


	public function Get_Client_Assignees($client_id)
	{
		$return               = [];

		$assign_to            = Get_Module_Assigees('CLIENT', $client_id);
		foreach ($assign_to as $key => $value) {
			$assignees_det = $this->db->query(
				'SELECT user.user_type,user.crm_name,organisation_tree.* FROM organisation_tree 
          INNER JOIN user ON user.id = organisation_tree.user_id 
          WHERE organisation_tree.user_id =' . $value
			)->row_array();
			$return[] = $assignees_det;
		}

		return $return;
	}


	public function Update_Client_assignee($Client_id, array $assignees)
	{
		$columns  = [
			"module_name"   =>  "CLIENT",
			"module_id"     =>  $Client_id,
			"firm_id"       =>  $_SESSION['firm_id'],
		];

		$this->db->delete("firm_assignees", $columns);


		foreach ($assignees as $key => $value) {
			$columns['assignees'] = $value;
			$this->db->insert("firm_assignees", $columns);
		}

		$service_assignees   =   [];

		if (!empty($_POST['service_manager'])) {
			$service_assignees['service_manager'] = $_POST['service_manager'];
		}

		if (!empty($_POST['service_assignee'])) {
			$service_assignees['service_assignee'] = $_POST['service_assignee'];
		}

		foreach ($service_assignees as $m_name => $sid_node_data) {

			$columns['sub_module_name']  = $m_name;

			foreach ($sid_node_data as $s_id => $node_ids) {

				$columns['sub_module_id']    =  $s_id;

				$node_ids = array_filter(explode(',', $node_ids));
				foreach ($node_ids as $node_id) {
					$columns['assignees']   = $node_id;
					$this->db->insert("firm_assignees", $columns);
				}
			}
		}
	}

	public function set_task_assignee_by_client_service($task_id, $client_id, $service_id)
	{
		$assignees = $this->db->select('firm_id,assignees')
			->where(['module_name' => 'CLIENT', 'module_id' => $client_id, 'sub_module_id' => $service_id])
			->group_by('assignees')
			->get('firm_assignees')
			->result_array();

		foreach ($assignees as $key => $assignee_data) {
			$data =  [
				'module_name' =>  'TASK',
				'module_id'   =>  $task_id,
				'firm_id'     =>  $assignee_data['firm_id'],
				'assignees'   =>  $assignee_data['assignees']
			];
			$this->db->insert('firm_assignees', $data);
		}
	}
	public function get_client_details($user_id){		
		$data   						= 	[];
		$data['client']       			= 	$this->db->get_where('client', "user_id = " . $user_id)->row_array();
		$data['contactRec']   			= 	$this->db->order_by('make_primary', 'desc')->get_where("client_contacts", "client_id =" . $user_id)->result_array();
		$data['user']         			= 	$this->db->get_where('user', 'id=' . $user_id)->row_array();
		$data['assignees']              = 	$this->Get_Client_Assignees($data['client']['id']);
		$data['service_wise_assigee']   = 	$this->Get_Service_Assignee_Details($data['client']['id']);
		$sql 							= 	"SELECT c.id,
													c.crm_first_name,
													c.crm_company_name 
											FROM user AS u  
										   	INNER JOIN client AS c 
										   		   	ON c.user_id = u.id 
										   	WHERE u.firm_id = " . $_SESSION['firm_id'] . " 
											AND u.user_type = 'FC'";
		$data['referby']          		= 	$this->db->query($sql)->result_array();
		$data['ref_details']			=   $this->getReferalTree($_SESSION['firm_id'], $data['client']['id']);
		$query_res                		= 	$this->db->query("SELECT * FROM add_new_task WHERE company_name IN ('" . $data['client']['id'] . "') AND related_to NOT IN ('leads','sub_task') order by id desc ");
		$data['task_list']        		= 	$query_res->result_array();
		$pro_query                		= 	$this->db->query("SELECT * FROM proposals WHERE company_id=" . $user_id . "  order by id desc ");
		$data['proposals']        		= 	$pro_query->result_array();
		$leads_query              		= 	$this->db->query("SELECT * FROM leads WHERE company IN ('" . $user_id . "') order by id desc ");
		$data['leads']            		= 	$leads_query->result_array();
		
		$this->load->model('Invoice_model');
		
		$data['invoice_details']  		= 	$this->Invoice_model->selectAllRecord('Invoice_details', 'client_email', $user_id);
		$data['reminder_details'] 		= 	$this->Invoice_model->selectAllRecord('client_reminder', 'client_id', $data['client']['id']);
		$data['queue_reminders']  		= 	$this->get_queue_service_reminders($data['client']['id']);
		$data['draft_reminders']  		= 	$this->get_draft_service_reminders($data['client']['id']);
		/*For hided code below*/
		$data['company']          		= 	$data['officers'] = $data['content'] = $data['array_rec3'] = [];
		/*FOR Custom Fields */
		$data['sections']         		= 	$this->db->query("SELECT DISTINCT(section) as section FROM client_fields_management WHERE firm_id=" . $_SESSION['firm_id'] . " AND section REGEXP '^[0-9]+$' ")->result_array();
		$data['sections']         		= 	array_column($data['sections'], 'section');
		$settings                 		= 	$this->Common_mdl->Get_Client_fields_ManagementSettings($_SESSION['firm_id']);
		$data                     		= 	array_merge($data, $settings);
		/*FOR Custom Fields */
		$data['tree_source']      		= 	$this->Common_mdl->Get_ClientReferral_Tree($data['client']['id']);
		return $data;
	}

	public function getReferalTree($firm_id=0, $client_id=0)
	{
		$tree = [];
		if($firm_id > 0 && $client_id > 0){
			$sql = "SELECT c.id AS client_id,
						   c.created_date as created,
						   u.id AS user_id,
						   u.status AS status						   
					FROM user AS u
					INNER JOIN client AS c
							ON c.user_id = u.id
					WHERE u.firm_id = $firm_id 
					AND u.user_type = 'FC' 
					AND c.crm_refered_by REGEXP '^[0-9]+$'
					AND c.crm_refered_by = '$client_id'  
					ORDER BY c.id DESC";
			$ref = $this->db->query($sql)->result_array();
			
			foreach($ref as $r){
				$status = "";
				if($r['status']){
					if($r['status'] == 1)	   { $status = 'Active';	}
					else if($r['status'] == 2) { $status = 'Inactive'; 	}
					else if($r['status'] == 3) { $status = 'Frozen';	}
					else if($r['status'] == 4) { $status = 'Draft';	  	}
					else if($r['status'] == 5) { $status = 'Archive';  	}
				}
				$tree[$r['client_id']] = '<a href="/client/client_infomation/'.$r['user_id'].'" target="_blank">'.$this->getClientName($r['client_id']).'</a> - ' . date("d/m/Y", $r['created']).' - '.$status;
			}
		}
		return $tree;
	}

	public function getClientName($id=0)
	{
		$client = [];
		if($id){
			$sql    = "SELECT crm_company_name FROM client WHERE id = $id ORDER BY id DESC LIMIT 1";
			$client = $this->db->query($sql)->result_array();			
		}
		return !empty($client) ? $client[0]['crm_company_name'] : "";
	}

	public function delete_contact_person($id)
	{
		$this->db->delete('client_contacts', ['id' => $id]);
		$this->db->delete('service_reminder_cron_data', ['client_contacts_id' => $id]);
		$this->db->delete('client_service_frequency_due', ['client_contacts_id' => $id]);
		echo 1;
	}

	public function time_line($user_id)
	{
		$this->load->helper('array');

		// $LQ = "SELECT `id`, `user_id`,
        //           `log`,
        //           '' AS `service`,
        //           '' AS `section`,
        //           '' AS `date`,
        //           `createdTime` as `created_time`,
        //           `pinned_to_top`,
        //           'log' as type
        // FROM activity_log
        // WHERE module='client' and module_id =" . $user_id;

		// $NQ = "SELECT `id`, `user_id`,
        //           `notes` as `log`,
        //           `services_type` AS `service`,
        //           `module` AS `section`,
        //           `date`,
        //           `created_time`,
        //           `pinned_to_top`,
        //           'notes' as `type` 
        //   FROM timeline_services_notes_added
		//   WHERE 1 ";

			$LQ = "SELECT `id`,
				`log`,
				'' AS `service`,
				'' AS `section`,
				'' AS `date`,
				`createdTime` as `created_time`,
				`pinned_to_top`,
				'log' as type
			FROM activity_log
			WHERE module='client' and module_id =" . $user_id;

			$NQ = "SELECT `id`,
				`notes` as `log`,
				`services_type` AS `service`,
				`module` AS `section`,
				`date`,
				`created_time`,
				`pinned_to_top`,
				'notes' as `type` 
			FROM timeline_services_notes_added
			WHERE user_id=" . $user_id;

		if (!empty($this->input->post('search'))) {
			$LQ   .=  " AND log LIKE '%" . $this->input->post('search') . "%'";

			$NQ   .=  " AND notes LIKE '%" . $this->input->post('search') . "%'";
		}

		if (!empty($_POST['service'])) {

			$service_filter  =   array_map(
				function ($v) {
					return " log LIKE '%" . $v . "%' ";
				},
				$this->input->post('service')
			);
			$service_filter = implode('OR', $service_filter);
			if (!empty($service_filter))
				$LQ   .=  " AND (" . $service_filter . ") ";

			$servcie_filter = array_quoted_str($this->input->post('service'));
			$NQ   .=  " AND services_type IN (" . $servcie_filter . ")";
		}

		$query    = $LQ . " UNION ALL " . $NQ;

		if (!empty($this->input->post('section_filter'))) {
			$section_filter = array_quoted_str($this->input->post('section_filter'));
			$query  = $NQ . " AND module IN (" . $section_filter . ")";
		}

		$total_recorde = $this->db->query($query)->num_rows();

		$this->load->library('pagination');
		$config['base_url']           = '';
		$config['total_rows']         = $total_recorde;
		$config['per_page']           = $this->input->post('pagination_length');
		$config['num_links']          = 2;
		$config['uri_segment']        = 4;
		$config['display_prev_link']  = TRUE;
		$config['full_tag_open']      = '<div class="pagination-buttons">';
		$config['full_tag_close']     = '</div>';
		$config['first_link']         = '<<';
		$config['last_link']          = '>>';
		$config['next_link']          = '>';
		$config['prev_link']          = '<';
		$config['cur_tag_open']       = '&nbsp;<span class="button active current pgn_links">';
		$config['cur_tag_close']      = '</span>';
		$config['num_tag_open']       = '<span class="button button_pagination_nav">';
		$config['num_tag_close']      = '</span>&nbsp;';
		$config['next_tag_open']      = '<span class="button button_pagination_nav">';
		$config['next_tag_close']     = '</span>&nbsp;';
		$config['prev_tag_open']      = '<span class="button button_pagination_nav">';
		$config['prev_tag_close']     = '</span>&nbsp;';

		$this->pagination->initialize($config);

		$return['pagination'] = $this->pagination->create_links();

		$query    .=  " ORDER BY pinned_to_top DESC, created_time DESC LIMIT " . $this->input->post('pagination_start') . "," . $this->input->post('pagination_length');
		// echo $query;
		$source  = $this->db->query($query)->result_array();

		$return['content']  = $this->load->view('Timeline/timeline_content', ['source' => $source], true);

		return $return;
	}

	public function SaveTimelineNotes()
	{
		$datas['module']          =   $this->input->post('notes_section');
		$datas['services_type']   =   $this->input->post('services_type');
		$datas['notes']           =   $this->input->post('feed_msg');
		$datas['created_time']    =   time();
		$datas['date']            =   $this->input->post('date');
		$datas['user_id']         =   $this->input->post('user_id');

		if (!empty($this->input->post('notes_id'))) {
			unset($datas['created_time']);
			$res =  $this->db->update('timeline_services_notes_added', $datas, "id=" . $this->input->post('notes_id'));
		} else {
			$this->db->insert('timeline_services_notes_added', $datas);
			$res =  $this->db->insert_id();
		}

		$client = $this->db->select('crm_company_name,id')
			->get_where('client', 'user_id=' . $this->input->post('user_id'))
			->row_array();

		$assignee =  get_specific_module_data_assignee('CLIENT', $client['id']);


		$assignee_mail  = $this->db->select('crm_email_id')
			->where_in('id', $assignee)
			->get('user')
			->result_array();

		$assignee_mail  = array_column($assignee_mail, 'crm_email_id');


		if (!empty($assignee_mail)) {
			$content    =   "<table style='border:0px'>";
			$content   .=   "<tr><td>Client Name</td><td> : <span style='color:#2dc26b'>" . $client['crm_company_name'] . "</td></tr>";
			$content   .=   "<tr><td>Section</td><td> : <span style='color:#ffc90e'>" . $datas['module'] . "</td></tr>";
			$content   .=   "<tr><td>Created At</td><td> : <span style='color:#3598db'>" . date('d-m-Y h:i:s A') . "</td></tr>";

			if (!empty($datas['services_type']))
				$content   .=   "<tr><td>Service</td><td> : <span style='color:#e03e2d'>" . $datas['services_type'] . "</td></tr>";
			if (!empty($datas['date']))
				$content   .=   "<tr><td>Date</td><td> : <span style='color:#3598db'>" . $datas['date'] . "</td></tr>";

			$content   .=   "<tr><td>Notes</td><td> : " . $datas['notes'] . "</td></tr>";
			$content   .=   "</table>";

			$content   .=   "<p><a href='" . base_url() . "client/client_infomation/" . $this->input->post('user_id') . "'>Click here to view</a></p>";

			$Subject   .=   "Notes Added By " . $_SESSION['crm_name'];

			firm_settings_send_mail($_SESSION['firm_id'], ['to' => $assignee_mail], $Subject, $content);
		}

		return ['result' => $res];
	}

	public function DeleteTimelineNotes($id)
	{
		$this->db->delete('timeline_services_notes_added', 'id=' . $id);

		return ['result' => $this->db->affected_rows()];
	}
}
