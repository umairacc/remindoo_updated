<?php
Class Firm_setting_mdl extends CI_Model {

        public function SetUp_BasicSettings($id)
        {  
            $this->insert_admin_settings($id);
            $this->dynamic_column_add($id);
            $this->dynamic_group_add($id);
            $this->dynamic_role_add($id);
            $this->Add_FA_As_Staff($id);
            $this->Setup_default_Menus($id);            
            $this->Setup_Default_WebToLead_Template($id);
        }
        public function Check_LatestSetup($id)
        {
            $this->SetUp_BasicSettings( $id );
        }

        public function Add_FA_As_Staff( $firm_id )
        {
            $this->db->where( 'firm_id'     , $firm_id );
            $this->db->where( 'user_type'   , 'FA' );
            $User_data = $this->db->get('user')->row_array();

            $this->db->where( 'user_id' , $User_data['id'] );
            $Firm_data = $this->db->query( "SELECT * FROM firm where firm_id=".$firm_id )->row_array();
            $data =
            [
                'user_id'           =>  $Firm_data['user_id'],
                'firm_id'           =>  $firm_id,
                'first_name'        =>  $Firm_data['crm_company_name'],
                'email_id'          =>  $Firm_data['firm_mailid'],
                'telephone_number'  =>  $Firm_data['country_code'].'-'.$Firm_data['firm_contact_no'],
                'roles'             =>  $User_data['role'],
                'username'          =>  $User_data['username'],          
                'password'          =>  $User_data['password'],
                'confirm_password'  =>  $User_data['confirm_password']          
            ];
            $CheckAlreadyExisit = $this->db->get('staff_form')->num_rows();
            if( !$CheckAlreadyExisit )
            {
                $this->db->insert( 'staff_form' , $data );
            }
            else
            {
                $this->db->update( 'staff_form' , $data ,'user_id ='.$Firm_data['user_id'] );
            }
        }
        public function Added_User_SectionSettings( $user_id='' ) 
        {            
            $this->db->where('user_id', $user_id );
            $CheckAlreadyExit = $this->db->get('section_settings')->num_rows();
            if( !$CheckAlreadyExit )
            {
                $this->db->insert('section_settings',['user_id'=>$user_id]);
                return $this->db->insert_id();
            }

        }
        public function insert_admin_settings( $firm_id )
        {   
            $CheckAlreadyExisit = $this->db->query("SELECT firm_id FROM admin_setting WHERE firm_id=".$firm_id)->num_rows();

            $progress_order='["11","10","9","6","7","8","5","3","4","2","24","21","22"]';

            if(! $CheckAlreadyExisit )    
            {

                $firm_data = $this->db->query("select * from firm where firm_id=".$firm_id)->row_array();
                
                $service = array_fill_keys( range(1, 16) , 1 );
                $this->Added_User_SectionSettings( $firm_data['user_id'] );
                $data = [
                        'user_id'=>$firm_data['user_id'],
                        'firm_id'=>$firm_data['firm_id'],   
                        'crm_currency' => 'GBP',
                        'company_name' => $firm_data['crm_company_name'],
                        'company_email'=> $firm_data['firm_mailid'],
                        'services'      => json_encode($service), 
                        'country_code' => $firm_data['country_code'], 
                        'company_contact_no'=>$firm_data['firm_contact_no'],
                        'reminder_send_status'    => '[1,3]',
                        'auto_create_task_status' => '[1,3]',
                        'client_reminder_time'=>'10:30 AM',
                        'created_date'=>time(),
                        'service_reminder'=>1,
                        'missing_details'=>1,
                        'missing_task'=>0,
                        'meeting_reminder'=>1,
                        'deadline_reminder'=>1,
                        'page_length'       => 100,
                        'header_footer_background_color'  => '#0a0909',
                        'progress_order'=>$progress_order
                        ];
                $this->db->insert('admin_setting',$data);                    
                return $this->db->insert_id();

            }
            else
            {
                $result=$this->db->query("SELECT progress_order FROM admin_setting WHERE firm_id=".$firm_id."")->row_array();
                //print_r($result);exit;

                if($result['progress_order']==''){
                 $data['progress_order']=$progress_order;
                 $this->db->where('firm_id',$firm_id);
                 $this->db->update('admin_setting',$data);
             }
             
            }
        }
        public function update_admin_settings()
        {
            if( ! class_exists( 'Common_mdl' ) ) $this->load->model( 'Common_mdl' );

            $admin = $this->Common_mdl->select_record('admin_setting','firm_id', $_SESSION['firm_id'] );     

            $data['logo_image'] = $admin['logo_image'];

            if ( !empty( $_FILES['logo']['name'] ) )
            {
              $data['logo_image'] = $this->Common_mdl->do_upload( $_FILES['logo'] , 'uploads/firm_logo' );
            }
            
            $data['company_name']       = $this->input->post( 'company_name' );
            $data['company_email']      = $this->input->post( 'company_email' );
            $data['country_code']       = $this->input->post( 'country_code' );
            $data['company_contact_no'] = $this->input->post( 'company_contact_no' );
            $data['page_length']        = $this->input->post( 'page_length' );  
            $data['crm_currency']       = $this->input->post( 'currency' );//₨';

            $data['client_reminder_time']    =  ( !empty( $this->input->post( 'client_reminder_time' ) ) ?  $this->input->post( 'client_reminder_time' ) :'10:30 AM' );
            $data['reminder_send_status']    =  json_encode( $this->input->post( 'reminder_send_status' ) );
            $data['auto_create_task_status'] =  json_encode( $this->input->post( 'auto_create_task_status') );
            $data['reminder_stop_status']    =  json_encode( $this->input->post( 'reminder_stop_status' ) );

            $data['service_reminder']       = (!empty( $this->input->post( 'service_reminder' ) ) ? $this->input->post( 'service_reminder' )  : 0 );
            $data['missing_details']        = (!empty( $this->input->post( 'missing_details') )   ? $this->input->post( 'missing_details' )   : 0 );
            $data['missing_task']           = (!empty( $this->input->post( 'missing_task') )      ? $this->input->post( 'missing_details' )   : 0 );
            $data['meeting_reminder']       = (!empty( $this->input->post( 'meeting_reminder') )  ? $this->input->post( 'meeting_reminder' )  : 0 );
            $data['header_footer_background_color'] = $this->input->post( 'background_color' );
            $data['header1']                = $this->input->post( 'header1' );
            $data['footer1']                = $this->input->post( 'footer1' );
            if($this->input->post( 'client_non_responding_reminder_no' ) && is_array($this->input->post( 'client_non_responding_reminder_no' ))){
                $data['client_non_responding_reminder_no'] = implode(",",$this->input->post( 'client_non_responding_reminder_no' ));
            }
            
            $this->db->update( 'admin_setting' , $data , 'firm_id='.$_SESSION['firm_id'] );

            $mail_settings[ 'smtp_server' ]     = $this->input->post( 'smtp_server' );
            $mail_settings[ 'smtp_email'  ]     = $this->input->post( 'smtp_email' );
            $mail_settings[ 'smtp_from_name' ]  = $this->input->post( 'smtp_from_name' );
            $mail_settings[ 'smtp_security' ]   = $this->input->post( 'smtp_security' );
            $mail_settings[ 'smtp_port' ]       = $this->input->post( 'smtp_port' );
            $mail_settings[ 'smtp_username' ]   = $this->input->post( 'smtp_username' );
            $mail_settings[ 'smtp_password' ]   = $this->input->post( 'smtp_password' );

            $Is_exists = $this->db->get_where( 'mail_protocol_settings' , 'firm_id='.$_SESSION['firm_id'] )
                                  ->num_rows();
            if( !$Is_exists )
            {   
                $mail_settings['firm_id'] = $_SESSION['firm_id'];
                
                $this->db->insert('mail_protocol_settings' , $mail_settings );
            }
            else
            {
                $this->db->update('mail_protocol_settings' , $mail_settings , 'firm_id='.$_SESSION['firm_id'] );
            }


            if( $this->db->affected_rows() )
            {  
              $firm_data['crm_company_name']  = $this->input->post( 'company_name' );
              $firm_data['firm_contact_no']   = $this->input->post( 'company_contact_no' );
              $firm_data['firm_mailid']       = $this->input->post( 'company_email' );

              $this->db->update( 'firm' , $firm_data , "firm_id=".$_SESSION['firm_id'] );
            }

            $_SESSION['firm_page_length']  = $this->input->post( 'page_length' );

            /*STOP servcie reminder if settings change*/
            $old_reminder_stop  =  json_decode( $admin['reminder_stop_status'] , true );

            if( $old_reminder_stop )
            {

                $new_added = array_diff( $this->input->post( 'reminder_stop_status' ) ,  $old_reminder_stop );
                

                if( count( $new_added ) )
                {
                    if( ! class_exists( 'Service_Reminder_Model' ) ) $this->load->model( 'Service_Reminder_Model' );

                    $this->Service_Reminder_Model->check_auto_task_status_update_reminders( $_SESSION['firm_id'] );
                }
            }
            return 1;
        }

        function dynamic_group_add( $firm_id )
        {
            $CheckAlreadyExisit = $this->db->query("SELECT firm_id FROM organisation_tree WHERE parent_id=0 AND firm_id=$firm_id")->num_rows();
            if(!$CheckAlreadyExisit)
            {
                $user_id = $this->db->query("select user_id from firm where firm_id=".$firm_id)->row_array();
                $data=array('firm_id'=>$firm_id,'title'=>'Administration','user_id'=>$user_id['user_id'],'parent_id'=>0);
                $this->db->insert('organisation_tree',$data);   
            }

        }

       function dynamic_role_add( $firm_id )
       {
            $CheckAlreadyExisit = $this->db->query("SELECT firm_id FROM roles_section WHERE role='Administration' AND firm_id=$firm_id")->num_rows();
            if(!$CheckAlreadyExisit)
            {

                $data=array('firm_id'=>$firm_id,'role'=>'Administration');
                $this->db->insert('Role',$data);

                $this->db->insert('roles_section',$data); 
                $id=$this->db->insert_id();
                $this->db->update('user',['role'=>$id]," firm_id=".$firm_id." and user_type='FA' ");
                
                $role_permission = 
                            array(
                                    'firm_id' => $firm_id ,
                                    'role_id' => $id,
                                    'role' => 'Administration',
                                    'Dashboard' => '1,1,1,1',
                                    'Task' => '1,1,1,1',
                                    'Leads' => '1,1,1,1',
                                    'Webtolead' => '1,1,1,1',
                                    'Proposal_Dashboad' => '1,1,1,1',
                                    'Create_Proposal' => '1,1,1,1',
                                    'Catalog' => '1,1,1,1',
                                    'Template' => '1,1,1,1',
                                    'Setting' => '1,1,1,1',
                                    'Client_Section' => '1,1,1,1',
                                    'Add_Client' => '1,1,1,1','
                                    Add_From_Company_House' => '1,1,1,1',
                                    'Import_Client' => '1,1,1,1',
                                    'Services_Timeline' => '1,1,1,1',
                                    'Deadline_manager' => '1,1,1,1',
                                    'Reports' => '1,1,1,1',
                                    'Admin_Settings' => '1,1,1,1',
                                    'Firm_Fields_Settings' => '1,1,1,1',
                                    'Menu_Management_Settings' => '1,1,1,1',
                                    'Reminder_Settings' => '1,1,1,1','
                                    User_and_Management' => '1,1,1,1',
                                    'Staff_Custom_Firmlist' => '1,1,1,1',
                                    'Tickets' => '1,1,1,1',
                                    'Chat' => '1,1,1,1',
                                    'Documents' => '1,1,1,1',
                                    'Invoices' => '1,1,1,1','
                                    Give_Feedback' => '1,1,1,1',
                                    'Task_Column_Settings' => '1,1,1,1',
                                    'Firm_Tickets' => '0,0,1,0',
                                    'Support_Tickets' => '1,1,1,1',
                                    'My_Profile' => '1,1,1,1',
                                    'Section_Settings' => '1,1,1,1',
                                    'Notification_Settings' => '1,1,1,1',
                                    'Pricelist_Settings' => '1,1,1,1',
                                    'Services' => '1,1,1,1',
                                    'Email_Template' => '1,1,1,1',
                                    'Term_and_Condition'=>'1,1,1,1'
                                );

                $this->db->insert('role_permission1',$role_permission);


            }    
    }


       function dynamic_column_add( $firm_id ){
        $val='visible_firm'.$firm_id;

      //  $col_array=array('visible_firm','order_firm','cusname_firm');

        if (!$this->db->field_exists($val, 'firm_column_settings'))
            {

                $this->db->query('ALTER TABLE firm_column_settings ADD COLUMN visible_firm'.$firm_id.' int(11);');
                $this->db->query('ALTER TABLE firm_column_settings ADD COLUMN order_firm'.$firm_id.' int(11);');
                $this->db->query('ALTER TABLE firm_column_settings ADD COLUMN cusname_firm'.$firm_id.' varchar(250);');

               $this->db->query('UPDATE `firm_column_settings` SET  visible_firm'.$firm_id.' = `visible_firm0`' );
               $this->db->query('UPDATE `firm_column_settings` SET  order_firm'.$firm_id.' = `order_firm0`' );
               $this->db->query('UPDATE `firm_column_settings` SET  cusname_firm'.$firm_id.' = `cusname_firm0` ');

            }

            
            if (!$this->db->field_exists("firm".$firm_id."_price", 'service_lists'))
            {
                $this->db->query('ALTER TABLE service_lists ADD COLUMN `firm'.$firm_id.'_price` varchar(255);');

                if( $this->db->affected_rows() )
                {
                    $this->db->query("UPDATE service_lists SET `firm".$firm_id."_price` = firm0_price");
                }

               /* $WorkFlow = $this->db->query('SELECT * FROM work_flow WHERE firm_id=0')->result_array();
                foreach ($WorkFlow as $key => $value)
                {
                    unset( $value['id'] );
                    $value['firm_id'] = $firm_id;
                    $this->db->insert('work_flow', $value);
                }

                $steps = $this->db->query('SELECT * FROM service_steps WHERE firm_id=0 ORDER BY order_no ASC')->result_array();
                foreach ($steps as $key => $value)
                {   
                    $sub_steps = $this->db->query('SELECT * FROM service_step_details WHERE step_id='.$value['id'].' AND  firm_id=0 ORDER BY id ASC')->result_array();

                    unset( $value['id'] );
                    $value['firm_id'] = $firm_id;
                    $this->db->insert('service_steps', $value);
                    
                    foreach ($sub_steps as $key => $sub_value)
                    {   
                        unset( $sub_value['id'] );
                        $sub_value['firm_id'] = $firm_id;
                        $sub_value['step_id'] = $this->db->insert_id();
                        $this->db->insert('service_step_details', $sub_value );
                    }
                }*/
            }   



          
     //exit;
    }
    public function Setup_Default_WebToLead_Template( $firm_id )
    {
        $CheckAlreadyExisit =  $this->db->query("SELECT id FROM web_lead_template WHERE default_template=1 AND firm_id=$firm_id")->num_rows();    

        if(!$CheckAlreadyExisit)
        {
            
            $clone    = $this->db->query("SELECT * FROM web_lead_template WHERE default_template=1 AND firm_id=0")->row_array();
            $clone['firm_id'] = $firm_id;               
            unset($clone['id']);
            
            $this->db->insert('web_lead_template',$clone);
            $id = $this->db->insert_id();
            
            $assignee = $this->db->query("SELECT id FROM organisation_tree WHERE firm_id=$firm_id AND parent_id=0")->row_array();
            $data = ['firm_id'=>$firm_id,'module_name'=>'WEB_LEADS','module_id'=>$id,'assignees'=>$assignee['id'] ];
            $this->db->insert('firm_assignees',$data);        
        }

    }

    public function Setup_default_Menus( $firm_id )
    {
        /*,$colne_parent =0 ,$new_parent = 0 old param*/ 
        /*$data = $this->db->query("SELECT * FROM menu_management WHERE firm_id=0 and parent_id = $colne_parent ")->result_array();
        foreach ($data as $key => $value)
        {
            $value['firm_id'] = $firm_id;
            $value['parent_id'] = $new_parent;
            $id = $value['menu_management_id'];   
            unset($value['menu_management_id']);
            
            $this->db->insert("menu_management", $value);
            $insert_id = $this->db->insert_id();
            $this->Setup_default_Menus( $firm_id , $id , $insert_id );
        }*/

        $CheckAlreadyExisit = $this->db->query('select * from menu_manage WHERE firm_id="'.$firm_id.'" ')->num_rows();
        if( !$CheckAlreadyExisit )
        {
          $admin_data=$this->db->query('select `menu_details` from menu_manage WHERE firm_id="0" ')->row_array();
          $admin_data['firm_id']=$firm_id;
          $this->db->insert('menu_manage',$admin_data);
        }
    }
    public function send_welcome_mail($firm_id,$subscription=false)
    {
        $firm_data = $this->db->query("select crm_company_name,firm_mailid as mail_id from firm where firm_id='".$firm_id."'")->row_array();
        $user_data = $this->db->query("select username,confirm_password as password from user where firm_id='".$firm_id."' and user_type='FA'")->row_array();

        // $this->load->library('email');
        // $this->email->set_mailtype("html");
        // $this->email->from('info@remindoo.org');
        // $this->email->to($firm_data['mail_id']);         

        $body = "<h2>Hi ".$firm_data['crm_company_name']."</h2>";               

        if($subscription == true)
        {
           $email_subject="Account Verification";
           $body.= "<p>Thanks For Choosing Us.</p>";
           $body.= "<p><a href='".base_url()."Email/Verify/".$firm_id."'>Click here to verify your account with us.</a></p>";
           $body.= "<p>Have a nice day!</p>";
        }
        else
        {
           $email_subject="Welcome To Remindoo";
           $body.= "<h4>Welcome To Remindoo</h4>"; 
           $body.= "<p>Your Username: <b>".$user_data['username']."</b></p>";
           $body.= "<p>Your Password: <b>".$user_data['password']."</b></p>";
           $body.= "<p><a href='".base_url()."'>Click here to login.</a></p>";
           $body.= "<p>Thanks.</p>";
        }


        $send = firm_settings_send_mail( 0 ,$firm_data['mail_id'] , $email_subject , $body ); 
        
        // $body = $this->load->view('email/welcome-email-mail.php',['body'=>$body], TRUE);
        // $this->email->message($body);
        // $send = $this->email->send();
        if(!$subscription && $send)
        {   
          $this->db->update('firm',['is_welcome_mail_send'=>1],"firm_id =".$firm_id);
        }
    }
    public function Delete_Firms(array $ids)
    {
        $ids = array_filter( $ids );
        if(!empty( $ids ))
        {
            $ids = implode(',', $ids); 
            $this->db->update('firm' , ['is_deleted' => 1 ,'status'=>0] ,"firm_id IN(".$ids.")");
            $this->db->update('admin_setting' , ['service_reminder' => 0] ,"firm_id IN(".$ids.")");
            return 1;
        }
    }
    public function Get_FirmSubscribed_Plan( $firm_id )
    {
        $this->db->where('firm_id' , $firm_id );
        $data = $this->db->get('firm')->row_array();

        if( !empty( $data['plan_details'] ) )
        {
            $plan_details = json_decode( $data['plan_details'] ,true);

            $this->db->where('id' , $plan_details['plan_id'] );
            return $this->db->get('subscription_plan')->row_array();
        }
        else
        {
            return '';
        }
    }
    Public function Add_SelectedPlan( $firm_id , $plan_id )
    {   
        $select_plan = $this->Get_FirmSubscribed_Plan( $firm_id );

        $this->db->where( 'id' , $plan_id );
        $current_plan = $this->db->get('subscription_plan')->row_array();

        if( empty($select_plan) || $select_plan['id']!=$current_plan['id'] )
        {
            $data['plan_type'] = $current_plan['type'];
            $plan_details = [
              'plan_id'     => $current_plan['id'],
              'plan_amount' => $current_plan['plan_amount']
              /*'currency'    => $current_plan['currency'],*/
            ];
            $data['plan_details'] = json_encode($plan_details);
            $data['last_subscribed_date'] = time();

            $this->db->update('firm',$data,"firm_id=".$firm_id);
        }                
    }

}