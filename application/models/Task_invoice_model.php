<?php
Class Task_invoice_model extends CI_Model {

       public function __construct()
    {
        parent::__construct();
       $this->load->model(array('Common_mdl','Security_model','Invoice_model','Service_model'));
    }

	// for billable task invoice
 public function task_billable_createInvoice($task_id,$status=false)
      {
        $task_array=array();       
        array_push($task_array,$task_id);

        $service_name=$this->db->query('SELECT related_to_services from add_new_task where id = '.$task_id.'')->row_array();
        $settings_val=$this->db->query('select `id`,`service_name` from service_lists where id <16 ')->result_array();

        $work_flow= $this->Service_model->get_firm_workflow();
        $Rservice='';

      if(isset($service_name["related_to_services"]) && $service_name["related_to_services"]!="" && $service_name["related_to_services"] )
          {
             $services=explode("_",$service_name["related_to_services"]);
            if(isset($services[1]) && $services[0]=='wor')
            {
              $table='work_flow';
              $service_id=$services[1];
              $service_array=$work_flow;
            }
            else
            {
               $table='service_lists';
              $service_id=(isset($services[1]))?$services[1]:$services[0];
               $service_array=$settings_val;
            }

            $service_key = array_search($service_id, array_column($service_array, 'id'));
            $Rservice=$service_array[$service_key]["service_name"];
            }


        $username=$this->Common_mdl->getUserProfileName($_SESSION['id']); 
        $activity_datas['log'] = "Task Invoice created ";
        $activity_datas['createdTime'] = time();
        $activity_datas['module'] = 'Task';
        $activity_datas['sub_module']='';
        $activity_datas['user_id'] = $_SESSION['id'];
        $activity_datas['module_id'] = $task_id;

        $this->Common_mdl->insert('activity_log',$activity_datas);


          $tsk_invoice_id='';
          $reminder_mail='';
          $task_data=$this->db->query("select * from add_new_task where id=".$task_id)->result_array();
          $task_sel_status=$task_data[0]['task_status'];
          /** cnd check for reminder completed mail send **/
          $company_name=$task_data[0]['company_name'];
          $get_company_data=$this->db->query("select * from client where id=".$company_name)->row_array();
          $res_table=$this->db->query("select * from reminder_setting where custom_task_created=".$task_id)->result_array();
          $ser_result='';
          if($task_sel_status=='5'){
                $new_array=array("4"=>"conf_statement","3"=>"accounts","5"=>"company_tax_return","6"=>"personal_tax_return","7"=>"payroll","8"=>"workplace","10"=>"cis","11"=>"cissub","12"=>"p11d","13"=>"bookkeep","14"=>"management","15"=>"vat");
                if(count($res_table)>0){
                $service_id=$res_table[0]['service_id'];
                $service_val=$new_array[$service_id];
            
                $get_ser_val=$get_company_data[$service_val];
                $ser_result=(isset(json_decode($get_ser_val)->invoice))? json_decode($get_ser_val)->invoice : '';
                }
                else{
                  $ser_result='';
                }
               // echo "select * from reminder_setting where custom_task_created=".$id;
            
                if(count($res_table)>0 && $ser_result!=''){
                 $reminder_mail='yes';
                }
                //  $invoice_id=$this->Task_invoice_model->task_billable_createInvoice($id);
          }
      /** end of reminder completed chk 07-07-2018 **/
            $tsk_invoice_id=$task_data[0]['invoice_id'];  
            $task_name=$task_data[0]['subject'];
            $task_startdate=$task_data[0]['start_date'];
            $task_enddate=$task_data[0]['end_date'];
            $date1=date_create($task_startdate);
            $date2=date_create($task_enddate);
            $diff=date_diff($date1,$date2);
            $diffence=$diff->format("%a");
            $cli_id = $task_data[0]['company_name'];
            $cli_rec_client = $this->Invoice_model->selectRecord('client', 'id', $cli_id);  
            $cli_rec = $this->Invoice_model->selectRecord('user', 'id', $cli_rec_client['user_id']);           
            $due_date = strtotime($task_data[0]['end_date']);
            $invoiceDate = strtotime(date('Y-m-d'));
            $invoice_no = mt_rand(100000,999999);
            $reference = '';
            $amt_det = 2;//tax exclusive
            if($amt_det == 1)
            {
                  // $amounts_type = 'Tax Exclusive';
                  // $tax_rate = $_POST['tax_rate'];
                  // $tax_amount = $_POST['tax_amount'];
            } else {
                  $amounts_type = 'Tax Inclusive';
                  $tax_rate = '';
                  $tax_amount = '';
            }
            if($tsk_invoice_id!='')
            {
                if($cli_rec['crm_email_id'] === NULL) {
                $cli_rec['crm_email_id']='admin@remindoo.org';
                }
                $data = array('client_email' => $cli_rec['id'],
                //'invoice_date' => $invoiceDate,
                'invoice_duedate' => $due_date
                );
                $ins_id =$tsk_invoice_id;
                $this->Common_mdl->update('Invoice_details', $data,'client_id',$tsk_invoice_id);
            }
            else
            {
                  if($cli_rec['crm_email_id'] === NULL) {
                    $cli_rec['crm_email_id']='admin@remindoo.org';
                  }
                  $data = array('client_email' => $cli_rec['id'],
                  'invoice_date' => $invoiceDate,
                  'invoice_duedate' => $due_date,
                  'invoice_no' => $invoice_no,
                  'reference' => $reference,
                  'amounts_type' => $amounts_type,
                  'status' => 'Active', 
                  'created_at' => time()
                  );          
            $ins_id = $this->Invoice_model->insert('Invoice_details', $data);
            }


            if($task_data[0]['sub_task_id']==''){         
                  $Assigned_Task = Get_Module_Assigees('TASK',$task_id);
                  $Assigned_Task = implode(',', $Assigned_Task );
                  $get_assigned=$Assigned_Task;
             
            }else{

                $sub_task_id=explode(',',$task_data[0]['sub_task_id']);
                for($i=0;$i<count($sub_task_id);$i++){     
                        array_push($task_array,$sub_task_id[$i]);
                }
                /*Sugan */
                $sql=$this->db->query("select * from add_new_task where id in (".$task_data[0]['sub_task_id'].") ")->result_array();            
                 $Assigned_Task = Get_Module_Assigees('TASK',$task_id);
                 $Assigned_Task = implode(',', $Assigned_Task );

                  $get_assigned=$Assigned_Task;
                  // $get_team=implode(',',array_filter(array_unique($teams)));
                  // $get_department=implode(',',array_filter(array_unique($departments)));
            }


      $staff_datas=array();
      if($get_assigned!='')
                   {
                    foreach (explode(',',$get_assigned) as $key => $value) {
                       $get_user_data=$this->Common_mdl->GetAllWithWhere('staff_form','user_id',$value);
                       if(count($get_user_data)>0){
                         $it_name=$get_user_data[0]['first_name']." ".$get_user_data[0]['last_name'];
                         $it_email=$get_user_data[0]['email_id'];
                         $it_rate=$get_user_data[0]['hourly_rate'];
                         $it_assign='assign';
                         $its_overall_rate=$it_rate*$diffence;
                         array_push($staff_datas, array("name"=>$it_name,"email"=>$it_email,"rate"=>$it_rate,"overall_rate"=>$its_overall_rate,"assign"=>$it_assign,"team"=>"","department"=>"",'staff_id'=>$value));
             
                       }
                    }
                 
                   }
       

                 if(isset($ins_id))
                {     
                    $clientId = $ins_id;
                }
                $total_val_arry=array();
                /** for edit **/
                if($tsk_invoice_id!='')
                {
                      $this->Common_mdl->delete('products_details','client_id',$tsk_invoice_id);
                }
/** end of edit */


/*Sugan */
//echo '<pre>'; print_r($staff_datas);
    $value=array_unique($staff_datas, SORT_REGULAR);
    $task_ids=implode(',',$task_array); 

        

            $check_query=$this->db->query("select * from individual_task_timer where  task_id  in (".$task_ids.")")->result_array();         
            $worked_staff=array();
            foreach($check_query as $staff_check){
                array_push($worked_staff,$staff_check['user_id']);
               //$itmname = $st_value['name'];  
            }          

        $bb_array=array();

        for($i=0;$i<count($task_array);$i++){

            //$task_id=$task_array[$i];

            $t_id=implode(',',$task_array);    
/*Sugan */


        foreach ($staff_datas as $st_key => $st_value) {

                 /*Sugan */
                if (in_array($st_value['staff_id'], $worked_staff) &&  !in_array($st_value['staff_id'], $bb_array))
                {

                    $working_hours=$this->Common_mdl->get_working_hours( $t_id,$st_value['staff_id']);                  
                        // print_r($working_hours);
                        // exit;
                    array_push($bb_array,$st_value['staff_id']);

                   /*Sugan */
             
                     if($st_value['assign']=='assign')
                                {


                                     $itmname=($Rservice!='')?$Rservice:$st_value['name']; 

                                   //$itmname = $st_value['name'];


                                }
                                if($st_value['assign']=='team')
                                {
                                   $itmname = $st_value['name']."--".$st_value['team'];
                                }
                                if($st_value['assign']=='department')
                                {
                                   $itmname = $st_value['name']."--".$st_value['team']."--".$st_value['department'];  
                                }
                  /*sugan */                    
                 // $descr = $t_id."--".$task_name;
                  $descr =$task_name." Worked by ".$st_value['name'] ;
                  $qty = $working_hours;// for start end date
                  $uprice = $st_value['rate'];
                  $dis = 0;
                  $acc = '';
                  $taxrate = 0;
                  $taxamount = 0;
                  $amt_gbp = $working_hours * $st_value['rate'];
                  array_push($total_val_arry, $amt_gbp);
                  $data1 = array('client_id' => $clientId,
                                    'item' => $itmname,
                                    'description' => $descr,
                                    'quantity' => $qty,
                                    'unit_price' => $uprice,
                                    'discount' => $dis,
                                    'account' => $acc,
                                    'tax_rate' => $taxrate,
                                    'tax_amount' => $taxamount,
                                    'amount_gbp' => $amt_gbp
                                    
                  );
                  /*sugan */
            
            $sql = $this->Invoice_model->insert('products_details', $data1);
            
     /*Sugan */   }     /*Sugan */
        }//Foreach end 
    }

    $total_sum=array_sum($total_val_arry);
     
 
            $sub_total = $total_sum;
            $vat = 0;
            $adjust_tax = 0;
            $grand_total = $total_sum;
            $transaction_payment = 'pending';
           /* if($transaction_payment == 'approve')
            {
                  $payment_status = 'approve';
            } else {
                  $payment_status = 'decline';
            }*/
/** for edit option **/
if($tsk_invoice_id!='')
{
       $data2 = array('client_id' => $clientId,
                                    'sub_total' => $sub_total,
                                    'VAT' => $vat,
                                    'adjust_to_tax' => $adjust_tax,
                                    'grand_total' => $grand_total, 
                                    'payment_status' => $transaction_payment
                              );


          $result =  $this->Common_mdl->update('amount_details',$data2,'client_id',$tsk_invoice_id);
}
else
{
       $data2 = array('client_id' => $clientId,
                                    'sub_total' => $sub_total,
                                    'VAT' => $vat,
                                    'adjust_to_tax' => $adjust_tax,
                                    'grand_total' => $grand_total, 
                                    'payment_status' => $transaction_payment
                              );


             $result = $this->Invoice_model->insert('amount_details', $data2);
}

/** end of edit option **/
          

             $data['grand_total']=$sub_total;
             $data['invoice_no']=$invoice_no;
             $data['invoice_date']=date('Y-m-d',$invoiceDate);
             $data['invoice_startdate']=date('Y-m-d',$due_date);
             $data['staff_datas']=$staff_datas;
             $data['task_id']=$task_id;
             $data['task_name']=$task_name;
             $data['diffence']=$diffence;
             $data['sub_tot']=$sub_total;

                if($cli_rec['crm_email_id'] === NULL) {
                    $cli_rec['crm_email_id']='admin@remindoo.org';
                }
                //
              $main_email=$this->db->query("select main_email from client_contacts where  client_id=".$cli_rec_client['user_id']."  and make_primary=1")->result_array();

              $cli_rec['crm_email_id']=$main_email[0]['main_email'];  

                $check_val=$this->Common_mdl->select_record('add_new_task','id',$task_id);
             $sender_id=$this->Common_mdl->select_record('admin_setting','firm_id', $check_val['firm_id']);
                       if(!empty($sender_id) && $sender_id['company_email']!='' )
                       {

                        $sender_id=$sender_id['company_email'];
                       }
                       else
                       {
                        $sender_id='info@remindoo.org';
                       }

              //by ajith
if($tsk_invoice_id==''){
   if(count($res_table)==0){

      $body = $this->load->view('users/invoice_remainder_email1.php', $data, TRUE);
      //$send = firm_settings_send_mail( $_SESSION['firm_id'] , $cli_rec['crm_email_id'] , 'Invoice' , $body );
      $send = $send['result'];

   }
   else
   {
    if($reminder_mail!='')
    {

         $body = $this->load->view('users/invoice_remainder_email1.php', $data, TRUE);
         //$send = firm_settings_send_mail( $_SESSION['firm_id'] , $cli_rec['crm_email_id'] , 'Invoice' , $body );
         $send = $send['result'];
         
    }

   }    
   
  }
  else if($status=='not')
    {

          $body = $this->load->view('users/invoice_remainder_email1.php', $data, TRUE);
          //$send = firm_settings_send_mail( $_SESSION['firm_id'] , $cli_rec['crm_email_id'] , 'Invoice' , $body );
          //$send = $send['result'];
    }
    else if($reminder_mail!='')
    {
      $new_array=array("4"=>"conf_statement","3"=>"accounts","5"=>"company_tax_return","6"=>"personal_tax_return","7"=>"payroll","8"=>"workplace","10"=>"cis","11"=>"cissub","12"=>"p11d","13"=>"bookkeep","14"=>"management","15"=>"vat");
    

           $body = $this->load->view('users/invoice_remainder_email1.php', $data, TRUE);
          //$send = firm_settings_send_mail( $_SESSION['firm_id'] , $cli_rec['crm_email_id'] , 'Invoice' , $body );
          //$send = $send['result'];
    }
    $data_val['invoice_id']=$clientId;
    $this->Common_mdl->update('add_new_task',$data_val,'id',$task_id);

return $clientId;
            }
/*** for email notification **/

public function task_new_mail( $task_id , $status , $store_as_queue = 0 )

{
      $task_data=$this->db->query("select * from add_new_task where id=".$task_id)->result_array();
             $get_task_name=$task_data[0]['subject'];
       if($status=='for_add'){

         $Assigned_Task = Get_Module_Assigees('TASK',$task_id);
         }
         else if($status=='for_delete')
         {
             $Assigned_Task = Get_Module_Assigees('TASK',$task_id);
         }
         else if($status=='for_reminder')
         {

             $Assigned_Task = Get_Module_Assigees('TASK',$task_id);
         }
         else
        {
             $Assigned_Task = array();

        }
   
$staff_datas=array();
if(isset($_SESSION['id']))
{
    $sender=$_SESSION['id'];
}
else
{
    $sender=0;
}
/** for manager **/
 $Sent_users = [];
 $Notify_Content = "You Have Assign to ".$get_task_name." Task";
 if($Assigned_Task!='')
                 {
                  foreach ($Assigned_Task as $key => $value)
                  {
                      $get_user_data=$this->Common_mdl->GetAllWithWhere('user','id',$value);
                           if(count($get_user_data)>0)
                           {
                            $userId   = $get_user_data[0]['id'];
                            $it_name  = $get_user_data[0]['crm_name'];
                            $it_email = $get_user_data[0]['crm_email_id'];

                            if($it_email!=''  )
                            {
                              $this->weblead_email($get_task_name,$task_id,$it_name,$it_email,'manager','','',$status , $store_as_queue);
                            }
                            $this->Common_mdl->Send_Notification( 'Task' ,'Task Assign', $userId , $Notify_Content,$sender,$task_id );
                            $Sent_users[] = $userId;
                           }
                  }
               
                 }

}
 public function weblead_email($lead_name,$lead_id,$name,$email_id,$field,$team,$depart,$status , $store_as_queue)
    {

       $data2['username'] = $name;

        $company_name='-';
        $task_status='-';
        $progress_status='-';
        $due_date='-';

        $assign_via="This task ";


       if(isset($_SESSION['id']))
       {
              $assign_via = $this->Common_mdl->getUserProfileName($_SESSION['id']);

       $sender_details=$this->db->query('select company_name,crm_name from admin_setting where user_id='.$_SESSION['id'].'')->row_array();

         // $role_details=$this->Common_mdl->getRole($_SESSION['id']);
          $role='';
          $sender_company=$sender_details['company_name'];
          $sender_name=$sender_details['crm_name'];
        }

        $firm_id=$this->Common_mdl->selectRecord('add_new_task', $lead_id, 'id');

       // print_r($firm_id);

       $task_template=task_mail_content_details($firm_id);

        // if(isset($firm_id[0]['company_name'] ) && $firm_id[0]['company_name']!='' && is_numeric($firm_id[0]['company_name']))
        // {
        //  // echo "chk";
        //   $company_name=$this->Common_mdl->select_record('client','id',$firm_id[0]['company_name']);
        //   $company_name=$company_name['crm_company_name'];

        // }
        // // print_r($company_name);

        //   if(isset($firm_id[0]['task_status'] ))
        // {
        //   $due_date=date('d-m-Y',strtotime($firm_id[0]["end_date"]));
         

        // }

        //   if(isset($firm_id[0]['task_status'] ) && $firm_id[0]['task_status']!='' && is_numeric($firm_id[0]['task_status']))
        // {
        //   $task_status=$this->Common_mdl->select_record('task_status', 'id',$firm_id[0]['task_status']);
        //   $task_status=$task_status['status_name'];

        // }

        //   if(isset($firm_id[0]['progress_status'] ) && $firm_id[0]['progress_status']!='' && is_numeric($firm_id[0]['progress_status']))
        // {
        //   $progress_status=$this->Common_mdl->select_record('task_status', 'id', $firm_id[0]['progress_status']);
        //   $progress_status=$progress_status['status_name'];

        // }





                  // $body1=$proposal_content['body'];
          // $subject=$proposal_content['subject'];
        //  $role=$role_details['role'];

          $link='<a href='.base_url().'user/task_details/'.$lead_id.' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here To view Task</a>';



        //    $user_id = $query['id'];
       if($status!='for_delete'){
            if($status=='for_reminder'){

               $EMAIL_TEMPLATE = $this->Common_mdl->getTemplates(19,false,$firm_id[0]['firm_id']);

                }

          else{

                $EMAIL_TEMPLATE = $this->Common_mdl->getTemplates(18,false,$firm_id[0]['firm_id']);


            }
      }
        else
        {
            $EMAIL_TEMPLATE = $this->Common_mdl->getTemplates(20,false,$firm_id[0]['firm_id']);

            // $link='<a href='.base_url().'user/task_details style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here To view Task</a>';

        }






           $a1  =   array(':: Staff Name::'=>$name,
                          '::Username::'=>$name,
                          ':: Task Link::'=>$link,
                          '::Client Name::'=>$task_template['company_name'],
                          '::TaskDueDate::'=>$task_template['due_date'],
                          '::TaskProgress::'=>$task_template['progress_status'],
                          '::TaskStatus::'=>$task_template['task_status'],
                          ':: Task Name::'=>$task_template['task_name'],
                          ':: Assign via::' => $assign_via
                        );
           
              // print_r($EMAIL_TEMPLATE);exit;
              if(!empty($EMAIL_TEMPLATE))
              {
                $EMAIL_TEMPLATE = end($EMAIL_TEMPLATE);

                     
                  $data2['firm_id']         =   $firm_id[0]['firm_id'];
                  $data2['username']        =   $name;
                  $email_subject            =   html_entity_decode( $EMAIL_TEMPLATE['subject'] );
                  $email_subject            =   strtr( $email_subject ,$a1 );
                  $data2['email_contents']  =   html_entity_decode( $EMAIL_TEMPLATE['body'] );
                  $data2['email_contents']  =   strtr($data2['email_contents'] ,$a1);
                  $data2['title']           =   html_entity_decode($EMAIL_TEMPLATE['title']);
                  //email template
                  $body = "</br>".$data2['email_contents'];

                  // echo $body;exit;

                 
                  // $body = $this->load->view('remainder_email.php',$data2, TRUE);
                  //redirect('login/email_format');

                  if( $store_as_queue )
                    {
                      $body = setup_body_content( $firm_id[0]['firm_id'] , $body );
                      
                      $data = [
                        'send_from'             =>  $firm_id[0]['firm_id'],
                        'send_to'               =>  json_encode( [ 'to' => $email_id ] ),
                        'subject'               =>  strip_tags( $email_subject ),
                        'content'               =>  $body,
                        'status'                =>  0,
                        'relational_module'     => 'task',
                        'relational_module_id'  =>  $lead_id,
                        'created_at'            =>  date('Y-m-d H:i:s')
                      ];

                      $this->db->insert( "email" , $data );

                    }
                    else
                    {
                      $send = firm_settings_send_mail( $firm_id[0]['firm_id'] , $email_id , strip_tags( $email_subject ) , $body );
                    }
                    /*$send = $send['result'];

                  // echo $email_id;
                  if($send)
                  {
                  //  echo "email send ";
                  }
                  else
                  {
                   // echo "email not send ";
                  }*/
          }
    }
/** end of email notification **/

    public function  startEndDate( $sD , $eD )
    {

      $sD = (!empty( $sD ) ? strtotime( $sD ) : strtotime('now') );
      $sD = date('d-m-Y' , $sD );

      $eD = (!empty( $eD ) ? strtotime( $eD ) : strtotime('now + 10 days') );
      $eD = date('d-m-Y' , $eD );
      
      return [ $sD , $eD ];

    }
/** for task cretea **/
  public function for_sample_client_task($client_id)
    {
      
     $get_client_data=$this->db->query("select * from client where user_id=".$client_id."")->row_array(); 
      $get_reminder_data=$this->db->query("SELECT * FROM `reminder_setting` where user_id=".$client_id." and custom_task_created='' order by id desc")->result_array();
      foreach ($get_reminder_data as $rem_key => $rem_value) {
            if($get_client_data['crm_company_name']!=''){
              $task_cmpny_name=$get_client_data['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
      //  echo "abc";
        //$subject_data=$task_cmpny_name."-".$rem_value['service_type'];
        $subject_data=$rem_value['service_type']."-".$task_cmpny_name;
        $company_name=$client_id;
        if($rem_value['service_id']==4)// confirmation
        {

          $date = $this->startEndDate( $get_client_data['crm_confirmation_statement_date'] , $get_client_data['crm_confirmation_statement_due_date'] );

          $start_data = $date[0];
          $end_date = $date[1];
        }
        if($rem_value['service_id']==3)//accounts
        {
          $date = $this->startEndDate( $get_client_data['crm_ch_yearend'] , $get_client_data['crm_ch_accounts_next_due'] );

          $start_data = $date[0];
          $end_date = $date[1];
        }
        if($rem_value['service_id']==5)//company tax return
        {
          $date = $this->startEndDate( $get_client_data['crm_accounts_tax_date_hmrc'] , $get_client_data['crm_accounts_due_date_hmrc'] ); 

          $start_data = $date[0];
          $end_date = $date[1];
        }
        if($rem_value['service_id']==6)//personal tax return
        {
          $date = $this->startEndDate( $get_client_data['crm_personal_tax_return_date'] , $get_client_data['crm_personal_due_date_return'] );

          $start_data = $date[0];
          $end_date = $date[1];
        }
       if($rem_value['service_id']==7)//payroll
        {
          $date = $this->startEndDate( $get_client_data['crm_payroll_run_date'] ,  $get_client_data['crm_rti_deadline'] );
          $start_data = $date[0];
          $end_date = $date[1];
        }
        if($rem_value['service_id']==8)//Workplace Pension
        {
          $date = $this->startEndDate( $get_client_data['crm_staging_date'] , $get_client_data['crm_pension_subm_due_date'] );

          $start_data = $date[0];
          $end_date = $date[1];
        }
        if($rem_value['service_id']==10)// cis
        {
          $date = $this->startEndDate( $get_client_data['crm_cis_contractor_start_date'] ,  $get_client_data['crm_cis_subcontractor_start_date'] );

          $start_data = $date[0];
          $end_date = $date[1]; // clarification both are same
        }
        if($rem_value['service_id']==11)// cis sub
        {
          $date = $this->startEndDate( $get_client_data['crm_cis_subcontractor_start_date'] , $get_client_data['crm_cis_subcontractor_start_date'] );

          $start_data = $date[0];
          $end_date = $date[1]; //both are same
        }
         if($rem_value['service_id']==12)// P11D
        {
          $date = $this->startEndDate( $get_client_data['crm_p11d_latest_action_date'] , $get_client_data['crm_next_p11d_return_due'] );

          $start_data = $date[0];
          $end_date = $date[1]; //
        }
        if($rem_value['service_id']==13)// Bookkeeping
        { 
          $date = $this->startEndDate( $get_client_data['created_date'] , $get_client_data['crm_next_booking_date'] );

          $start_data = $date[0];
          $end_date = $date[1]; //
        } 
        if($rem_value['service_id']==14)// management
        { 
          $date = $this->startEndDate( $get_client_data['created_date'] , $get_client_data['crm_next_manage_acc_date'] );

          $start_data = $date[0];
          $end_date = $date[1]; //
        }
        if($rem_value['service_id']==15)// vat
        { 
          $date = $this->startEndDate( $get_client_data['crm_vat_date_of_registration'] , $get_client_data['crm_vat_due_date'] );

          $start_data = $date[0];
          $end_date = $date[1]; //
        }
        $team_arr=array();
         $depart_arr=array();
         $assign_arr=array();
        $managed_arr=array();


     /*if($get_client_data['select_responsible_type']!=''){ }*/// assignto tab has values
        $team=$this->Common_mdl->GetAllWithWhere('responsible_team','client_id',$client_id);
        if(!empty($team)){
        foreach ($team as $tm_key => $tm_value) {
         array_push($team_arr, $tm_value['team']);
          }
        }

       
        $department=$this->Common_mdl->GetAllWithWhere('responsible_department','client_id',$client_id);
        if(!empty($department)){
        foreach ($department as $de_key => $de_value) {
         array_push($depart_arr, $de_value['depart']);
          }
        }

       
        $assign=$this->Common_mdl->GetAllWithWhere('responsible_user','client_id',$client_id);
        if(!empty($assign)){
        foreach ($assign as $as_key => $as_value) {
         array_push($assign_arr, $as_value['manager_reviewer']);
         array_push($managed_arr,$as_value['assign_managed']);
          }
        }
      

            $data['subject']=$subject_data;
            $data['service_due_date']=$start_data;
            $data['start_date']=$start_data;
            $data['end_date']=$end_date;

              

            $data['company_name']=$get_client_data['id'];

             $data['worker'] = implode(',',array_unique($assign_arr));
            $data['team'] = implode(',',array_unique($team_arr));
            $data['department'] = implode(',',array_unique($depart_arr));
            $data['manager']=implode(',', array_unique($managed_arr));
           $for_old_values['assign']=array_unique($assign_arr);
            $for_old_values['team']=array_unique($team_arr);
            $for_old_values['department']=array_unique($depart_arr);
            $for_old_values['manager']=array_unique($managed_arr);
            $data['for_old_assign']=json_encode($for_old_values);
            $data['created_date']=time();
            $data['create_by']=$_SESSION['id'];
            $data['priority'] = 'Low';
            $data['related_to'] = 'tasks';
            $data['task_status']='notstarted';
            $data['firm_id'] = $get_client_data['firm_id'];

           
            $assignees = $this->db->query("SELECT assignees FROM firm_assignees WHERE module_name = 'CLIENT' AND module_id = '".$client_id."' AND firm_id = '".$get_client_data['firm_id']."'")->row_array();
            $assignees = $assignees['assignees'];
            
            $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();

            $data = ['firm_id'=>$get_client_data['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);

            $invoice_id='';
            // if((isset($_POST['billable'])&&($_POST['billable']!=''))){
            //         $invoice_id=$this->Task_invoice_model->task_billable_createInvoice($in);
            //         if($invoice_id!='')
            //         {
            //             $datas['invoice_id']=$invoice_id;
            //              $result =  $this->Common_mdl->update('add_new_task',$datas,'id',$in);
            //         }
            //     }

                /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             $datas['custom_task_created']=$in;
             $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$rem_value['id']);

      }


    }
/** end of task create **/
/** client firm setings based task cretaed **/
    public function for_default_cus_reminder($client_id)
    {
       $client=$this->db->query("select * from client where user_id=".$client_id."")->row_array();
       $em_re_conf_statement='';
       $em_re_accounts='';$em_re_vat='';
       $em_re_company_tax='';$em_re_personal_tax='';$em_re_payroll='';$em_re_pension='';
       $em_re_cis='';$em_re_p11d='';$em_re_bookkeep='';$em_re_manage='';$em_re_insurance='';
       $em_re_registered='';$em_re_investigation='';
      
       $assignees = $this->db->query("SELECT assignees FROM firm_assignees WHERE module_name = 'CLIENT' AND module_id = '".$client_id."' AND firm_id = '".$client['firm_id']."'")->row_array();
       $assignees = $assignees['assignees'];
       
      if(isset($client['crm_confirmation_email_remainder']) && ($client['crm_confirmation_email_remainder']!='') ){
        if(isset($client['crm_confirmation_create_task_reminder']) && $client['crm_confirmation_create_task_reminder']!='')
        {
          $em_re_conf_statement='on';
        }
      }

    if(isset($client['crm_accounts_next_reminder_date']) && ($client['crm_accounts_next_reminder_date']!='') ){
        if(isset($client['crm_accounts_create_task_reminder']) && $client['crm_accounts_create_task_reminder']!='')
        {
      $em_re_accounts='on';
        }
     }

     if(isset($client['crm_company_next_reminder_date']) && ($client['crm_company_next_reminder_date']!='') ){
       if(isset($client['crm_company_create_task_reminder']) && $client['crm_company_create_task_reminder']!='')
        {
         $em_re_company_tax='on';
        }
     }
    if(isset($client['crm_personal_next_reminder_date']) && ($client['crm_personal_next_reminder_date']!='') ){
         if(isset($client['crm_personal_task_reminder']) && $client['crm_personal_task_reminder']!='')
        {
      $em_re_personal_tax='on';
        }
     }
    if(isset($client['crm_payroll_next_reminder_date']) && ($client['crm_payroll_next_reminder_date']!='') ){
       if(isset($client['crm_payroll_create_task_reminder']) && $client['crm_payroll_create_task_reminder']!='')
        {
      $em_re_payroll='on';
        }
     }
    if(isset($client['crm_pension_next_reminder_date']) && ($client['crm_pension_next_reminder_date']!='') ){
         if(isset($client['crm_pension_create_task_reminder']) && $client['crm_pension_create_task_reminder']!='')
        {
      $em_re_pension='on';
       }
     }
    if(isset($client['crm_cis_next_reminder_date']) && ($client['crm_cis_next_reminder_date']!='') ){
       if(isset($client['crm_cis_create_task_reminder']) && $client['crm_cis_create_task_reminder']!='')
        {
      $em_re_cis='on';
        }
     }

    if(isset($client['crm_p11d_next_reminder_date']) && ($client['crm_p11d_next_reminder_date']!='') ){
        if(isset($client['crm_p11d_create_task_reminder']) && $client['crm_p11d_create_task_reminder']!='')
        {
      $em_re_p11d='on';
       }
     }
      if(isset($client['crm_vat_next_reminder_date']) && ($client['crm_vat_next_reminder_date']!='') ){
      if(isset($client['crm_vat_create_task_reminder']) && $client['crm_vat_create_task_reminder']!='')
        {
      $em_re_vat='on';
       }
     }
     if(isset($client['crm_bookkeep_next_reminder_date']) && ($client['crm_bookkeep_next_reminder_date']!='') ){
      if(isset($client['crm_bookkeep_create_task_reminder']) && $client['crm_bookkeep_create_task_reminder']!='')
        {
      $em_re_bookkeep='on';
       }
     }
     if(isset($client['crm_manage_next_reminder_date']) && ($client['crm_manage_next_reminder_date']!='') ){
        if(isset($client['crm_manage_create_task_reminder']) && $client['crm_manage_create_task_reminder']!='')
        {
      $em_re_manage='on';
        }
     }
     if(isset($client['crm_insurance_next_reminder_date']) && ($client['crm_insurance_next_reminder_date']!='') ){
       if(isset($client['crm_insurance_create_task_reminder']) && $client['crm_insurance_create_task_reminder']!='')
        {
      $em_re_insurance='on';
        }
     }
     if(isset($client['crm_registered_next_reminder_date']) && ($client['crm_registered_next_reminder_date']!='') ){
       if(isset($client['crm_registered_create_task_reminder']) && $client['crm_registered_create_task_reminder']!='')
        {
      $em_re_registered='on';
        }
     }
     if(isset($client['crm_investigation_next_reminder_date']) && ($client['crm_investigation_next_reminder_date']!='') ){
       if(isset($client['crm_investigation_create_task_reminder']) && $client['crm_investigation_create_task_reminder']!='')
        {
      $em_re_investigation='on';
        }
     }

        $company_name=$client_id;
 $team_arr=array();
         $depart_arr=array();
         $assign_arr=array();
        $managed_arr=array();


     if($client['select_responsible_type']!=''){ // assignto tab has values
        $team=$this->Common_mdl->GetAllWithWhere('responsible_team','client_id',$client_id);
        if(!empty($team)){
        foreach ($team as $tm_key => $tm_value) {
         array_push($team_arr, $tm_value['team']);
          }
        }

       
        $department=$this->Common_mdl->GetAllWithWhere('responsible_department','client_id',$client_id);
        if(!empty($department)){
        foreach ($department as $de_key => $de_value) {
         array_push($depart_arr, $de_value['depart']);
          }
        }

       
        $assign=$this->Common_mdl->GetAllWithWhere('responsible_user','client_id',$client_id);
        if(!empty($assign)){
        foreach ($assign as $as_key => $as_value) {
         array_push($assign_arr, $as_value['manager_reviewer']);
         array_push($managed_arr,$as_value['assign_managed']);
          }
        }
      }

           // $data['subject']=$subject_data;

          $data['company_name']=$client['id'];

          $data['worker'] = implode(',',array_unique($assign_arr));
          $data['team'] = implode(',',array_unique($team_arr));
          $data['department'] = implode(',',array_unique($depart_arr));
          $data['manager']=implode(',', array_unique($managed_arr));
          $for_old_values['assign']=array_unique($assign_arr);
          $for_old_values['team']=array_unique($team_arr);
          $for_old_values['department']=array_unique($depart_arr);
          $for_old_values['manager']=array_unique($managed_arr);
          $data['for_old_assign']=json_encode($for_old_values);
          $data['created_date']=time();
          $data['create_by']=$_SESSION['id'];
          $data['priority'] = 'Low';
          $data['related_to'] = 'tasks';
          $data['task_status']='notstarted';
          $data['firm_id'] = $client['firm_id'];
          
  if($em_re_conf_statement!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=4 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {
          if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
            $subject_data=$conf_value['service_type']."-".$task_cmpny_name;
            
            $date = $this->startEndDate( $client['crm_confirmation_statement_date'] , $client['crm_confirmation_statement_due_date'] );
            $start_data= $date[0];
            $end_date= $date[1];

             $data['subject']=$subject_data;$data['start_date']=$start_data;
             $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();

            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);

            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }
    if($em_re_accounts!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=3 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {

         if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
          $subject_data=$conf_value['service_type']."-".$task_cmpny_name;

          $date = $this->startEndDate( $client['crm_ch_yearend'] , $client['crm_ch_accounts_next_due'] );
          $start_data= $date[0];
          $end_date= $date[1];
             $data['subject']=$subject_data;$data['start_date']=$start_data;
             $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();

            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);

            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }
      if($em_re_company_tax!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=5 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {
          if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
          $subject_data=$conf_value['service_type']."-".$task_cmpny_name;
          $date = $this->startEndDate( $client['crm_accounts_tax_date_hmrc'] , $client['crm_accounts_due_date_hmrc'] );

          $start_data = $date[0];
          $end_date = $date[1];

             $data['subject']=$subject_data;$data['start_date']=$start_data;
             $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();

            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);

            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
            if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }
        if($em_re_personal_tax!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=6 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {
          if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
            $subject_data=$conf_value['service_type']."-".$task_cmpny_name;
            
            $date = $this->startEndDate( $client['crm_personal_tax_return_date'] , $client['crm_personal_due_date_return'] ); 

            $start_data= $date[0];
          $end_date= $date[1];
            $data['subject']=$subject_data;$data['start_date']=$start_data;
            $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();
            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);
            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }

  if($em_re_payroll!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=7 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {
          if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
            $subject_data=$conf_value['service_type']."-".$task_cmpny_name;
            $date = $this->startEndDate( $client['crm_payroll_run_date'] , $client['crm_rti_deadline'] ); 
            $start_data= $date[0];
            $end_date= $date[1];
             $data['subject']=$subject_data;$data['start_date']=$start_data;
             $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();
            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);
            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }

    if($em_re_pension!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=8 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {
          if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
           $subject_data=$conf_value['service_type']."-".$task_cmpny_name;
           
           $date = $this->startEndDate( $client['crm_staging_date'] , $client['crm_pension_subm_due_date'] );

           $start_data= $date[0];
          $end_date= $date[1];
             $data['subject']=$subject_data;$data['start_date']=$start_data;
             $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();
            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);
            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }
 if($em_re_cis!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=10 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {
          if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
          $subject_data=$conf_value['service_type']."-".$task_cmpny_name;
          $date = $this->startEndDate( $client['crm_cis_contractor_start_date'] , $client['crm_cis_subcontractor_start_date'] );

          $start_data= $date[0];
          $end_date= $date[1]; // clarification both are same
             $data['subject']=$subject_data;$data['start_date']=$start_data;
             $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();
            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);
            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }
   if($em_re_p11d!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=12 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {
          if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
            $subject_data=$conf_value['service_type']."-".$task_cmpny_name;
            
            $date = $this->startEndDate( $client['crm_p11d_latest_action_date'] , $client['crm_next_p11d_return_due'] );

          $start_data= $date[0];
          $end_date= $date[1];//
             $data['subject']=$subject_data;$data['start_date']=$start_data;
             $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();
            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);
            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }
     if($em_re_bookkeep!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=13 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {
          if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
         $subject_data=$conf_value['service_type']."-".$task_cmpny_name;
         
         $date = $this->startEndDate( $client['created_date'] , $client['crm_next_booking_date'] );

         $start_data= $date[0];
          $end_date= $date[1];//
             $data['subject']=$subject_data;$data['start_date']=$start_data;
             $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();
            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);
            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }
 if($em_re_manage!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=14 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {
          if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
            $subject_data=$conf_value['service_type']."-".$task_cmpny_name;
            
            $date = $this->startEndDate( $client['created_date'] ,  $client['crm_next_manage_acc_date']) ;

    $start_data= $date[0];
          $end_date= $date[1]; //
             $data['subject']=$subject_data;$data['start_date']=$start_data;
             $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();
            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);
            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }

 if($em_re_manage!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=14 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {
          if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
            $subject_data=$conf_value['service_type']."-".$task_cmpny_name;
            $date = $this->startEndDate( $client['created_date'] , $client['crm_next_manage_acc_date'] );

          $start_data= $date[0];
          $end_date= $date[1]; //
             $data['subject']=$subject_data;$data['start_date']=$start_data;
             $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();
            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);
            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }
   if($em_re_vat!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=15 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {
          if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
        $subject_data=$conf_value['service_type']."-".$task_cmpny_name;
        
        $date = $this->startEndDate( $client['crm_vat_date_of_registration'] , $client['crm_vat_date_of_registration'] );

       $start_data= $date[0];
          $end_date= $date[1];//

             $data['subject']=$subject_data;$data['start_date']=$start_data;
             $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();
            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);
            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }
   if($em_re_investigation!='')
  {
    $get_con_data=$this->db->query('SELECT * FROM `reminder_setting` where service_id=9 and custom_reminder=0 and  ! FIND_IN_SET(  '.$client_id.', user_id )  ')->result_array();
    if(!empty($get_con_data)){
      foreach ($get_con_data as $conf_key => $conf_value) {
          if($client['crm_company_name']!=''){
              $task_cmpny_name=$client['crm_company_name'];
            }
            else
            {
              $task_cmpny_name=$this->Common_mdl->get_crm_name($client_id);
            }
           //$subject_data=$task_cmpny_name."-".$conf_value['service_type'];
        $subject_data=$conf_value['service_type']."-".$task_cmpny_name;
        
        $date = $this->startEndDate( $client['crm_invesitgation_insurance'] , $client['crm_insurance_renew_date'] );

        $start_data= $date[0];
          $end_date= $date[1];//

             $data['subject']=$subject_data;$data['start_date']=$start_data;
             $data['service_due_date']=$start_data;
            $data['end_date']=$end_date;
             $insert_data=$this->db->insert( 'add_new_task', $data );
            $in=$this->db->insert_id();
            $data = ['firm_id'=>$client['firm_id'],'module_name'=>'TASK','module_id'=>$in,'assignees'=>$assignees ];
            $this->db->insert('firm_assignees',$data);
            $invoice_id='';
                    /** new task send a mail to a assignee **/
             $this->Task_invoice_model->task_new_mail($in,'for_add');
             if($conf_value['user_id']!='')
             {
               $datas['user_id']=$conf_value['user_id'].",".$client_id;
             }
             else
             {
              $datas['user_id']=$client_id; 
             }
             if($conf_value['custom_task_created']!='')
             {
               $datas['custom_task_created']=$conf_value['custom_task_created'].",".$in;
             }
             else
             {
              $datas['custom_task_created']=$in; 
             }
            $result =  $this->Common_mdl->update('reminder_setting',$datas,'id',$conf_value['id']);

      }
    }
  }


    }
/** end of firm based task created **/

}
