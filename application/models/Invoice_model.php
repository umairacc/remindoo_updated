<?php
Class Invoice_model extends CI_Model {

	public function getallrecords($table)
	{
		$query = $this->db->query("SELECT * FROM $table")->result_array();
		return $query;
	}

	public function insert($table, $data)
	{ 
		// echo "<pre>"; print_r($data); exit();
		$this->db->insert($table, $data);
		//echo $this->db->last_query(); exit();
		if($this->db->affected_rows() > 0)
		{
			$insertId = $this->db->insert_id();
			return $insertId;
		} else {
			return 0;
		}
	} 

	public function insertRecords($table, $con, $fld, $data1)
	{ 
		//echo "<pre>"; print_r($data1); exit();
		$this->db->where($con, $fld);
		$this->db->insert($table, $data1);
		//echo $this->db->last_query(); exit();
		if($this->db->affected_rows() > 0)
		{
			$insertId = $this->db->insert_id();
			return $insertId;
		} else {
			return 0;
		}
	} 

	public function selectRecord($table, $field, $con)
    {
        $query=$this->db->query("SELECT * from $table where $field='$con' " );  
        //echo $this->db->last_query(); die();
        return $query->row_array();
    }

    public function selectTotRecord($table, $field, $con){
        return $this->db->query("SELECT GROUP_CONCAT(user_id) UID FROM $table WHERE $field IN($con)")->result_array();
    }

	public function selectAllRecord($table, $field, $con)
    {
        $query=$this->db->query("SELECT * from $table where $field='$con' " );  
        //echo $this->db->last_query(); die();
        return $query->result_array();
    }

    public function selectAllRecords($table, $field, $con){        
        $query = $this->db->query("SELECT * FROM $table WHERE $field IN($con) " );  
        return $query->result_array();
    }

   public function update($table, $con, $field, $data)
   { 
        $query = $this->db->where($con, $field)->update($table, $data);
      
        if($query > 0)
        {
            return true;
        } else {
            return false;
        }
   }

   public function updateRecords($table, $con, $field, $con1, $field1, $data1)
   {
   		$config = array($con => $field, $con1 => $field1);
        $query = $this->db->where($config)->update($table, $data1);
        //echo $this->db->last_query(); die;
        if($query > 0)
        {
            return true;
        } else {
            return false;
        }
   }

   public function delete($table, $con, $fld)
    {  
    	$config = array($con => $fld);
    	$this->db->where($config);
  		$q = $this->db->delete($table);
  		//echo $this->db->last_query(); die;
    	if ($this->db->affected_rows() > 0) {
            return 1;
        }
        else {
            return 0;
        }
    
    }


    public function getInvoiceByType($table,$field,$status)
    {
        $records = $this->db->query('SELECT * FROM Invoice_details LEFT JOIN amount_details ON Invoice_details.client_id = amount_details.client_id where '.$table.'.'.$field.' = "'.$status.'"')->result_array();
        
        return $records;
    }

    public function selectInvoiceByType($table,$field,$status,$field1,$client_ids){
        if(!empty($table) && !empty($field) && !empty($status)){
            $condition = ''.$table.'.'.$field.' = "'.$status.'" AND';
        }else{
            $condition = "";
        }         
        $sql       = "SELECT Invoice_details.*,
                             amount_details.*, 
                             amount_details.created_at as amount_created_at,
                             amount_details.id as amount_details_id
                      FROM Invoice_details 
                      LEFT JOIN amount_details 
                             ON Invoice_details.client_id = amount_details.client_id 
                      WHERE $condition FIND_IN_SET(Invoice_details.$field1, '$client_ids')
                      ORDER BY Invoice_details.invoice_date DESC";        

        $records   = $this->db->query($sql)->result_array();

        $final_rec = [];        

        $this->load->model('Task_creating_model', 'Task_creating');
        $this->load->model('Common_mdl', 'Common_mdl');           

        foreach ($records as $key => $record) {
            $query                              = "SELECT id, 
                                                          task_status, 
                                                          progress_status 
                                                   FROM add_new_task 
                                                   WHERE user_id=$record[client_email]
                                                   LIMIT 1";
            $new_task                           = $this->db->query($query)->row_array();
            $final_rec[]                        = $record;
            $final_rec[$key]['task_status']     = '';
            $final_rec[$key]['progress_status'] = '';
            $final_rec[$key]['assigned_to']     = '';
            
            if($new_task){
                if(isset($new_task[0])){
                    $new_task = $new_task[0];
                }
                $task_status_id     = $new_task['task_status'];
                $progress_status_id = $new_task['progress_status'];
                $query              = "SELECT status_name FROM task_status WHERE id='$task_status_id' LIMIT 1";
                $task_status        = $this->db->query($query)->row_array();
                $query              = "SELECT status_name FROM progress_status WHERE id=$progress_status_id LIMIT 1";
                $progress_status    = $this->db->query($query)->row_array();
                
                if($task_status){
                    $final_rec[$key]['task_status'] = $task_status['status_name'];
                }
                if($progress_status){
                    $final_rec[$key]['progress_status'] = $progress_status['status_name'];
                }
                $assign_data = [];
                $query       = "SELECT  module_id,
                                        GROUP_CONCAT(assignees) as get_assignees 
                                FROM firm_assignees 
                                WHERE module_name='TASK' 
                                AND module_id IN ($new_task[id])";
                $S_V         = $this->db->query($query)->result_array();                

                foreach ($S_V as $S_Vkey => $value) {
                    $ex_vals                                = explode(",", $value['get_assignees']);
                    $assign_data[$S_Vkey]['get_assignees']  = $this->Task_creating->get_module_user_data($ex_vals);
                    $assign_data[$S_Vkey]['module_id']      = $value['module_id'];
                }
                $assign_user_key                = array_search($new_task["id"], array_column($assign_data, 'module_id'));
                $ex_val                         = $assign_data[$assign_user_key]["get_assignees"];
                $final_rec[$key]['assigned_to'] = $ex_val;
            }
            $client_services             = $this->Common_mdl->getClientServices($record['client_email']);
            $final_rec[$key]['services'] = '';
            $count_services              = count($client_services);
            
            foreach ($client_services as $service_key => $service) {
                if(($service_key+1) == $count_services){
                    $final_rec[$key]['services'] .= $service['service_name'];
                }else{
                    $final_rec[$key]['services'] .= $service['service_name'].', ';
                }
            }            
        }
        return $final_rec;
    }

    public function getRepeatInvoice($field,$client_ids){        
        $query   = "SELECT  repeatInvoice_details.*,
                            repeatamount_details.* 
                    FROM repeatInvoice_details 
                    LEFT JOIN repeatamount_details 
                           ON repeatInvoice_details.client_id = repeatamount_details.client_id                     
                    WHERE FIND_IN_SET(repeatInvoice_details.$field,'$client_ids')";
        $records = $this->db->query($query)->result_array();
        return $records;
    }
    
    public function UpdateAllExpiredInvoice($pending){
        $clidArr = [];
        foreach ($pending as $key => $value){
            if(!empty($value['invoice_duedate']) && $value['invoice_duedate'] <= time()){
                $clidArr[] = $value['client_id'];               
            }            
        }
        $clids = implode(',', $clidArr);
        if(count($clidArr) > 0){
            $this->db->query("UPDATE Invoice_details SET invoice_duedate = 'Expired' WHERE client_id IN($clids)");
        }
        return true;   
    }

    public function UpdateExpiredInvoice($clients_user_ids)
    {
        $pending = $this->selectInvoiceByType('amount_details','payment_status','pending','client_email', $clients_user_ids);

        foreach ($pending as $key => $value) 
        {
            if(!empty($value['invoice_duedate']) && $value['invoice_duedate'] <= time())
            {
               $this->db->query('UPDATE Invoice_details SET invoice_duedate = "Expired" WHERE client_id = "'.$value['client_id'].'"');
            }
        }

        return true;   
    }

    public function CreateInvoice($client)
    {
        $client_services = $this->Common_mdl->getClientServices($client['user_id']);
        $client_enabled_services = array();        

        function firm_enabled($val)
        {          
           return $val['service_name'];
        }

        $client_enabled_services = array_map('firm_enabled',$client_services); 

        $subscribed = json_decode($client['client_subscribed_services']);

        $item_name = explode(',',$subscribed->item_name);
        $description = explode(',',$subscribed->description);
        $quantity = explode(',',$subscribed->quantity);
        $unit_price = explode(',',$subscribed->amount);
        $discount = explode(',',$subscribed->discount);        
        $tax_amount = explode(',',$subscribed->tax_amount);
        $total = "";
        $pro_ids = array();

        foreach ($item_name as $key => $value) 
        {            
            if(in_array($item_name[$key], $client_enabled_services))
            { 
                $itmname = $item_name[$key];  
                $descr = $description[$key];
                $qty = $quantity[$key];
                $uprice = $unit_price[$key];
                $dis = $discount[$key]; 
                $taxamount = $tax_amount[$key];
                $amt = (($uprice*$qty)+($taxamount)) - $dis;
                $tax_rate = ($taxamount/$uprice)*100;
                $total += $amt;
                $dis = ($dis/$amt)*100;
                
                $data1 = array('client_id' => '',
                            'item' => $itmname,
                            'description' => $descr,
                            'quantity' => $qty,
                            'unit_price' => $uprice,
                            'discount' => $dis,
                            'account' => '',
                            'tax_rate' => $tax_rate,
                            'tax_amount' => $taxamount,
                            'amount_gbp' => $amt                        
                );
            
                $this->Invoice_model->insert('products_details', $data1); 
                $pro_ids[] = $this->db->insert_id();
            }           
         }           
           
         if(count($pro_ids)>0)
         {
            $invoice_no = mt_rand(100000,999999);

            $data = array('client_email' => $client['user_id'],
                        'invoice_date' => strtotime(date('Y-m-d')),
                        'invoice_no' => $invoice_no,                    
                        'amounts_type' => 'Tax Exclusive',
                        'status' => 'Active', 
                        'created_at' => time(),  
                        'invoice_type' => 'subscription'                  
            );

            $this->Invoice_model->insert('Invoice_details', $data);

            $primary_id = $this->db->insert_id();

            if(!empty($primary_id))
            {   
               $clientId = $primary_id;
            }
            
            $client_data = array('client_id' => $clientId);

            foreach($pro_ids as $key => $value) 
            {
                $this->Invoice_model->update('products_details','id',$value,$client_data);
            }             

            $data2 = array('client_id' => $clientId,
                           'sub_total' => $total,
                           'VAT' => '',
                           'adjust_to_tax' => '',
                           'grand_total' => $total, 
                           'payment_status' => 'pending'
                       );

            $this->Invoice_model->insert('amount_details', $data2);
         }         

         return true;
    }
}

?>