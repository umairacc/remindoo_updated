<?php
class CompanyHouse_Model extends CI_Model
{
	/*Coded BY Ajith*/
  /*===============================================*/
	public $key = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
  public function Execute_Curl($URL)
  {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $URL); 
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_HEADER, false);
      curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_USERPWD,$this->key);
      $response = curl_exec($curl);
      if($errno = curl_errno($curl)) 
      {
          $error_message = curl_strerror($errno);
          $RETURN['error'] = "cURL error ({$errno}):\n {$error_message}";
      }
      else
      {
        curl_close($curl);
        $RETURN['response'] = json_decode($response,true);
      }
      return $RETURN;
  }
  /*
    @$TERMS IS USER SEARCH DATA.
  */
	public function Search_BY_Terms($TERMS)
	{
		  $searchKey = urlencode($TERMS);
      $items_per_page = '10';
      $URL = 'https://api.companieshouse.gov.uk/search/companies?q='.$searchKey.'&items_per_page='.$items_per_page.'&start_index=0';
      return $this->Execute_Curl($URL);
	   
	}
	public function Get_Company_Details($company_number)
	{
        $items_per_page = '10';
        $URL = 'https://api.companieshouse.gov.uk/company/'.trim($company_number).'/';
        return $this->Execute_Curl($URL);
  }
  public function Get_Company_Officers($company_number)
  {
    $URL = 'https://api.companieshouse.gov.uk/company/'.trim($company_number).'/officers?register_type=directors&register_view=false';
    return $this->Execute_Curl($URL);
  }
  public function Get_Specific_Officer($URL_SLUG)
  {
    $URL = 'https://api.companieshouse.gov.uk'.$URL_SLUG;
    return $this->Execute_Curl($URL);
  }
  /*===================================*/

	/*
		@$TERMS IS USER SEARCH DATA.
		$XT_CNO IS NEED TO DISABLED 
	*/
	public function SearchResults($TERMS,$XT_CNO)
   {
  		$RETURN = $this->Search_BY_Terms($TERMS);
        $content = '';
  		if(!isset( $RETURN['error'] ))
  		{
 		 	     $data = $RETURN['response'];
	        if(empty($data) || ($data['total_results']==0))
	        {
	            $content .='No results Found';
	        }
	        else
	        {
	          foreach ($data['items'] as $key => $value) 
	          {
	            $isExist = array_search( $value['company_number'] , $XT_CNO );

	            if( $isExist !== FALSE  )
	            {
	              $content .='<a href="'.base_url().'client/client_infomation/'.$isExist.'" class="text-success" >';
	            }
              else
              {
                $content .='<a href="#" class="" onclick="return getCompanyRec('."'".$value['company_number']."'".');" >';
              }
                $content .='<h3>'.$value['title'].'</h3>
                <span>'.$value['description'].'</span>
                </a>';
	          }
	        }
        }
        else
        {
          $content = $RETURN['error'];
        }
        return $content;
   	}

   	public function Get_Profile_Content( $company_number )
   	{
   		$company_details = $this->Get_Company_Details( $company_number );
   		$officers = $this->Get_Company_Officers( $company_number );
   		$content = '';
   		if(!isset($company_details['error']) && !isset($officers['error']) )
   		{	
   			$officers = $officers['response'];
   			$company_details = $company_details['response'];

   			$charge=(isset($company_details['has_charges']) && ($company_details['has_charges']==false))?'0':'1';

        $AccRefDay = (!empty($company_details['accounts']['accounting_reference_date']['day']))?$company_details['accounts']['accounting_reference_date']['day']:'';

        $AccRefMon =  (!empty($company_details['accounts']['accounting_reference_date']['month']))?$company_details['accounts']['accounting_reference_date']['month']:'';

       	$date_of_creation = (!empty($company_details['date_of_creation']))?$company_details['date_of_creation']:'';

        $company_status = (!empty($company_details['company_status']))?$company_details['company_status']:'';

       		$content ='<div class="br_company_profile">
                    <h3>'.$company_details['company_name'].'</h3>
                    <div class="company_default-Modal">
                    <div class="company_numbers">
                    <span>Company Number</span>    
                    <p>'.$company_details['company_number'].'</p>
                    </div>
                    <div class="company_numbers">
                    <span>Company Status</span>    
                    <p>'.$company_status.'</p>
                    </div>    
                    <div class="company_numbers">
                    <span>Incorporated Date</span>    
                    <p>'.$date_of_creation.'</p>
                    </div>
                    <div class="company_numbers">
                    <span>Accounts Reference Date</span>    
                    <p>'.
                    $AccRefDay.'/'.$AccRefMon
                    .'</p>
                    </div>
                    <div class="company_numbers">
                    <span>Charges</span>    
                    <p><strong>'.$charge.'</strong></p>
                    </div>
                    <div class="company_numbers">
                    <span>Current Directors</span>    
                    <p>';

            foreach ($officers['items'] as $key => $value)
            {
              if(empty($value['resigned_on'])) 
              {
            	  $content .= $value['name'].'<br>';
              }
          	} 
				
			$content .='</p>
          </div>
          <div class="company_numbers">
          <span>Resigned Directors</span>    
          <p>';
          foreach ($officers['items'] as $key => $value)
          {
              if(!empty($value['resigned_on']))
              {
              	$content .= $value['name'].'<br>';
          	  }
          }
              $content .='</p>
              </div>
              <div class="company_chooses fields">
              <a href="#" onclick="return getCompanyView('."'".$company_details['company_number']."'".');">Save & Continue</a>
              </div>
              </div>
              </div>';
   	 }
     else
     {
      $content = "cURL Error";
     }
   	    return $content;
  }

  public function Get_Company_Contact_Content( $company_number , $user_id )
  {
    $officers = $this->Get_Company_Officers( $company_number );  
    $content = '';
    if( !isset( $company_details['error'] ) )
    {
      $officers = $officers['response'];
     
      $array = array('First','Second','Third','Fourth','Fifth','Sixth','Seventh','Eight','Nineth','Tenth');
      $i=0;
      foreach ($officers['items'] as $key => $value) 
      {

        if($value['officer_role'] !='' && !isset($value['resigned_on']))
        {          
          $dateofbirth=(isset($value['date_of_birth']['year'])&&$value['date_of_birth']['year']!='') ? $value['date_of_birth']['year'] : '';
            $user = $this->db->query("select * from user where id =".$user_id)->row_array();
            $table = "client_contacts";
            $condition_field = "client_id";
            if($user['user_type'] == "FA")
            {
              $table = "firm_contacts";
              $condition_field = "user_id";
            }
            $get_db_user_contact=$this->db->query('select * from '.$table.' where `'.$condition_field.'`='.$user_id)->result_array();  
          
          $slug = array_column( $get_db_user_contact , 'url_slug' );
          $primary_exist = array_column( $get_db_user_contact , 'make_primary');

          $primary_exist = in_array(1, $primary_exist );
          $contact_exist = in_array($value['links']['officer']['appointments'], $slug);

          if(!$contact_exist && !$primary_exist && $i==0)
          {
            $content .='
            <div class="trading_tables"> 
            <div class="trading_rate">
            <p>
            <span><strong>'.$value['name'].'</strong></span>
            <span>Born '.$dateofbirth.'</span>
            <span class="fields" id="usecontact'.$array[$i].'"><a href="#" onclick="return getmaincontact('."'".$company_number."'".','."'".$value['links']['officer']['appointments']."'".','."'".$array[$i]."'".','.$user_id.',this);">Use as Primary Contact</a></span>
            </p> 
            </div>
            </div>';
          }
          else if(!$contact_exist)
          {
              $content .='
            <div class="trading_tables"> 
            <div class="trading_rate">
            <p>
            <span><strong>'.$value['name'].'</strong></span>
            <span>Born '.$dateofbirth.'</span>
            <span class="fields" id="usecontact'.$array[$i].'"><a href="#" onclick="return getmaincontact('."'".$company_number."'".','."'".$value['links']['officer']['appointments']."'".','."'".$array[$i]."'".','.$user_id.',this);">Use as Secondary Contact</a></span>
            </p> 
            </div>
            </div>';
          }
          else
          {
            $content .='
            <div class="trading_tables"> 
            <div class="trading_rate">
            <p>
            <span><strong>'.$value['name'].'</strong></span>
            <span>Born '.$dateofbirth.'</span>
            <span class="fields" id="usecontact'.$array[$i].'"><span class="succ_contact"><a href="#">Added</a></span></span>
            </p> 
            </div>
            </div>';
          }  
          $i++;
        }
      }
    }
    else
    {
      $content .='<span>Data Not Found</span>';  
    }
    return $content;
  }

  public function Add_Contact($table_name)
  {
    $companyNo = $_POST['companyNo'];//
    $appointments = $_POST['appointments'];//
    $count_house=(isset($_POST['count']))?$_POST['count']:'';
    
    $response = $this->Get_Specific_Officer($appointments);
    $URL = 'https://api.companieshouse.gov.uk/company/'.$companyNo.'/persons-with-significant-control/';
    $response1 = $this->Execute_Curl($URL);
  
    if(!isset($response['error']) )
    {        
      $array_rec = $response['response'];        
      $implode_nature='';
      $psc='';    
  
      if(!isset($response1['error']) && isset($response1['response1']['items'])) 
      {
        foreach ($response1['response1']['items'] as $key => $value)
        {
           $nature_of_control=$value['natures_of_control']; 
           $psc='yes';    
        }       
        $implode_nature=implode(',', $nature_of_control);
      }
   
      
      $content           = $array_rec['items'][0];
      $title             = (!empty($content['name_elements']['title']))? $content['name_elements']['title']:'';
      $fore_name         = (!empty($content['name_elements']['forename']))? $content['name_elements']['forename']:'';
      $otherfore_name    = (!empty($content['name_elements']['other_forenames']))? $content['name_elements']['other_forenames']:'';
      $sur_name          = (!empty($content['name_elements']['surname']))? $content['name_elements']['surname']:'';
      $address_line_1    = (!empty($content['address']['address_line_1']))? $content['address']['address_line_1']:'';
      $address_line_2    = (!empty($content['address']['address_line_2']))? $content['address']['address_line_2']:'';
      $premises          = (!empty($content['address']['premises']))? $content['address']['premises']:'';
      $region            = (!empty($content['address']['region']))? $content['address']['region']:'';
      $country           = (!empty($content['address']['country']))? $content['address']['country']:'';
      $locality          = (!empty($content['address']['locality']))? $content['address']['locality']:'';
      $postal_code       = (!empty($content['address']['postal_code']))? $content['address']['postal_code']:'';
      $nationality       = (!empty($content['nationality']))? $content['nationality']:'';
      $occupation        = (!empty($content['occupation']))? $content['occupation']:'';
      $appointed_on      = (!empty($content['appointed_on']))? date("d-m-Y",strtotime( $content['appointed_on'] ) ):'';
      $country_of_residence= (!empty($content['country_of_residence']))? $content['country_of_residence']:'';

      $date_of_birth = "";
      if( !empty( $array_rec['date_of_birth']['month'] ) && !empty( $array_rec['date_of_birth']['year'] ) )
      {
        $date_of_birth = '00-'.$array_rec['date_of_birth']['month'].'-'.$array_rec['date_of_birth']['year'];
      } 



      $data_c = array();

      if( $table_name =='firm_contacts' )
      {
        $data_c['user_id'] = $_POST['user_id'];
      }
      else
      {
        $data_c['client_id'] = $_POST['user_id'];
      }
        
      $data_c['title'] = $title;
      $data_c['first_name'] = $fore_name.' '.$otherfore_name;
      $data_c['last_name'] = $sur_name;
      $data_c['preferred_name'] = $otherfore_name;
      $data_c['address_line1'] = $premises." ".$address_line_1;
      $data_c['address_line2'] = $address_line_2;
      $data_c['premises'] = $premises;
      $data_c['region'] = $region;
      $data_c['country'] = $country;
      $data_c['locality'] = $locality;
      $data_c['post_code'] = $postal_code;
      $data_c['nationality'] = $nationality;
      $data_c['occupation'] = $occupation;
      $data_c['appointed_on'] = $appointed_on;
      $data_c['country_of_residence'] = $country_of_residence;
      $data_c['psc'] = $psc;
      $data_c['nature_of_control'] = $implode_nature;
      $data_c['url_slug'] = $appointments;
      $data_c['created_date'] = time();
      $data_c['contact_count']=$count_house;
      $data_c['date_of_birth'] = $date_of_birth;  

      if($count_house=='First')
      {
        $data_c['make_primary ']='1';
      }
      $result = $this->db->insert($table_name,$data_c);
      $data_c['contact_id'] = $this->db->insert_id();
      return $data_c;
  }

  }
  public function Find_NewData_Updates( $client_id )
    {
      $result = [];

      $this->db->where('id' , $client_id );
      $data = $this->db->get('client')->row_array();

      $source = $this->Get_Company_Details( $data['crm_company_number'] );
      
      if( empty( $source['error'] ) && ( !empty( $source['response'] )  && empty( $source['response']['errors'] ) ) )
      {
        if( !class_exists('Common_mdl') )
        {
          $this->load->model('Common_mdl');    
        }
        $Company_types  = $this->Common_mdl->Company_types();
        
        $up   = array(
              'crm_company_name',
              'crm_company_type',
              'crm_ch_status',
              'crm_post_code',
              'crm_incorporation_date',
              'crm_registered_in',
              'crm_company_url',
              'crm_officers_url',
              'crm_hmrc_yearend',
              'crm_accounts_last_made_up_to_date',
              'crm_ch_accounts_next_due',
              'is_ch_accounts_overdue',
              'crm_confirmation_statement_date',
              'crm_confirmation_statement_last_made_up_to_date',
              'crm_confirmation_statement_due_date',
              'is_ch_cs_overdue',
              'crm_address_line_one',
              'crm_address_line_two',
              'crm_address_line_three',
              'crm_town_city',

            );

        $old[] = $data['crm_company_name'];
        $old[] = $data['crm_company_type'];
        $old[] = $data['crm_ch_status'];  
        $old[] = $data['crm_post_code'];
        $old[] = $data['crm_incorporation_date'];
        $old[] = $data['crm_registered_in'];
        $old[] = $data['crm_company_url'];
        $old[] = $data['crm_officers_url'];
        
        $old[] = $data['crm_hmrc_yearend'];
        $old[] = $data['crm_accounts_last_made_up_to_date'];
        $old[] = $data['crm_ch_accounts_next_due'];
        $old[] = ( $data['is_ch_accounts_overdue']==0 ? '' : $data['is_ch_accounts_overdue'] );
    
        $old[] = $data['crm_confirmation_statement_date'];
        $old[] = $data['crm_confirmation_statement_last_made_up_to_date'];
        $old[] = $data['crm_confirmation_statement_due_date'];
        $old[] = ( $data['is_ch_cs_overdue']==0 ? '' : $data['is_ch_cs_overdue'] );

        $old[] = $data['crm_address_line_one'];
        $old[] = $data['crm_address_line_two'];
        $old[] = $data['crm_address_line_three'];
        $old[] = $data['crm_town_city'];
                
        $c        = array_combine($up, $old);
        
        

        $company_details = $source['response'];                
       
        $new[] = (!empty( $company_details['company_name'] ) ? $company_details['company_name'] :'');
        $new[] = (!empty( $company_details['type'] )? $Company_types[ $company_details['type'] ] :''); 
        $new[] = (!empty( $company_details['company_status'] )? $company_details['company_status'] :'');

        $new[] = (!empty( $company_details['crm_post_code'] ) ? $company_details['crm_post_code']:'');
        $new[] = (!empty( $company_details['date_of_creation'] ) ? $company_details['date_of_creation']:'');
        $new[] = (!empty( $company_details['jurisdiction'] ) ? $company_details['jurisdiction']:'');

        $company_url  = '';
        $officers_url = '';

        if( !empty( $company_details['links'] ) )
        {
            $company_url  = 'https://beta.companieshouse.gov.uk'.$company_details['links']['self'];
            $officers_url = (isset($company_details['links']['officers']))?'https://beta.companieshouse.gov.uk'.$company_details['links']['officers']:'';
        }
        $new[] = $company_url;
        $new[] = $officers_url;

        $new[] = (/*!empty( $accounts['tab'] )  &&*/ !empty( $company_details['accounts']['next_made_up_to'] )? $company_details['accounts']['next_made_up_to']:'');

        $new[] = (/*!empty( $accounts['tab'] ) && */!empty( $company_details['accounts']['last_accounts']['made_up_to'] ))?$company_details['accounts']['last_accounts']['made_up_to']:'';

        $new[] = (/*!empty( $accounts['tab'] ) &&*/ !empty( $company_details['accounts']['next_due'] ) )?$company_details['accounts']['next_due']:'';
        $new[] = (/*!empty( $accounts['tab'] ) &&*/ !empty( $company_details['accounts']['overdue'] ) )?$company_details['accounts']['overdue']:'';


        $new[] = (/*!empty( $conf_statement['tab'] ) &&*/ !empty( $company_details['confirmation_statement']['next_made_up_to'] ) )? $company_details['confirmation_statement']['next_made_up_to']:'';

        $new[] = (/*!empty( $conf_statement['tab'] ) &&*/ !empty($company_details['confirmation_statement']['last_made_up_to']))?$company_details['confirmation_statement']['last_made_up_to']:'';

        $new[] = (/*!empty( $conf_statement['tab'] ) &&*/ !empty($company_details['confirmation_statement']['next_due']))? $company_details['confirmation_statement']['next_due']:'';
        
        $new[] = (/*!empty( $conf_statement['tab'] ) &&*/ !empty($company_details['confirmation_statement']['overdue']))? $company_details['confirmation_statement']['overdue']:'';

        $new[] = (!empty($company_details['registered_office_address']['address_line_1']))? $company_details['registered_office_address']['address_line_1']:'';
        $new[] = (!empty($company_details['registered_office_address']['address_line_2']))? $company_details['registered_office_address']['address_line_2']:'';
        $new[] = (!empty($company_details['registered_office_address']['address_line_3']))? $company_details['registered_office_address']['address_line_3']:'';
        
        $new[] = (!empty($company_details['registered_office_address']['locality']))? $company_details['registered_office_address']['locality']:''; 
        $c1        = array_combine($up, $new);        
        
        $result    = array_diff_assoc($c1,$c); 

        

      } 
      return $result;
    }





   //  public function auto_SearchResults($TERMS,$XT_CNO)
   // {
   //    $RETURN = $this->Search_BY_Terms($TERMS);
   //      $content = array();
   //    if(!isset( $RETURN['error'] ))
   //    {
   //         $data = $RETURN['response'];
   //        if(empty($data) || ($data['total_results']==0))
   //        {
   //            $content[]='';
   //        }
   //        else
   //        {
   //          foreach ($data['items'] as $key => $value) 
   //          {
             
   //            if(!in_array($value['company_number'],$XT_CNO))
   //            {
   //              $company_details = $this->Get_Company_Details($value['company_number']);
   //                if(!isset($company_details['error']))
   //                {
   //                  $address_line_1='';
   //                  $company_details = $company_details['response'];
   //                  // unset($company_details['accounts']);
   //                  //  unset($company_details['previous_company_names']);
   //                   $content[$key]['company_name']=$company_details['company_name'];

   //                   $address_line_1=(isset($company_details['registered_office_address']['address_line_1']))?$company_details['registered_office_address']['address_line_1']:'';

   //                   $address_line_2=(isset($company_details['registered_office_address']['address_line_2']))?$company_details['registered_office_address']['address_line_2']:'';

   //                   $address_line_3=(isset($company_details['registered_office_address']['address_line_3']))?$company_details['registered_office_address']['address_line_3']:'';

   //                    $content[$key]['address_line']=$address_line_1.'<br>'.$address_line_2.'<br>'.$address_line_3;

   //                    $content[$key]['postal_code']=(isset($company_details['registered_office_address']['postal_code']))?$company_details['registered_office_address']['postal_code']:'';

   //                   $content[$key]['country']=(isset($company_details['registered_office_address']['country']))?$company_details['registered_office_address']['country']:'';


   //                }
                
   //            }
                

   //          }
   //        }
   //      }
   //      else
   //      {
   //        $content = $RETURN['error'];
   //      }
   //      return $content;
   //  } 



     public function auto_SearchResults($TERMS,$XT_CNO)
   {
      $RETURN = $this->Search_BY_Terms($TERMS);
        $content = array();
      if(!isset( $RETURN['error'] ))
      {
           $data = $RETURN['response'];
          if(empty($data) || ($data['total_results']==0))
          {
              $content='';
          }
          else
          {
           // print_r($data);
            foreach ($data['items'] as $key => $value) 
            {
             
              if(!in_array($value['company_number'],$XT_CNO))
              {
                $content[$key]['company_name']=$value['title'];
                $content[$key]['company_id']=(isset($value['company_number']) && $value['company_number'] !='') ? : '';
                 $content[$key]['address_line']=(isset($value['address_snippet']) && $value['address_snippet'] !='') ? : '';
                 $content[$key]['postal_code']=(isset($value['address']['postal_code']) && $value['address']['postal_code'] !='') ? $value['address']['postal_code']  :'';
                 $content[$key]['description']=(isset($value['description']) && $value['description'] !='')? $value['description'] :'' ;
                 //$company_details=$this->Get_Company_Details($value['company_number']); 


                 //  if(!isset($company_details['error']))
                 //  {
                 //    $address_line_1='';
                 //    $company_details = $company_details['response'];
                  
                 //     $content[$key]['company_name']=$company_details['company_name'];

                 //     $address_line_1=(isset($company_details['registered_office_address']['address_line_1']))?$company_details['registered_office_address']['address_line_1']:'';

                 //     $address_line_2=(isset($company_details['registered_office_address']['address_line_2']))?$company_details['registered_office_address']['address_line_2']:'';

                 //     $address_line_3=(isset($company_details['registered_office_address']['address_line_3']))?$company_details['registered_office_address']['address_line_3']:'';

                 //      $content[$key]['address_line']=$address_line_1.'<br>'.$address_line_2.'<br>'.$address_line_3;

                 //      $content[$key]['postal_code']=(isset($company_details['registered_office_address']['postal_code']))?$company_details['registered_office_address']['postal_code']:'';

                 //     $content[$key]['country']=(isset($company_details['registered_office_address']['country']))?$company_details['registered_office_address']['country']:'';


                 //  }
                
              }
                

            }
          }
        }
        else
        {
          $content = $RETURN['error'];
        }
        return $content;
    }

     

}   
?>