<?php
/*
	@author Ajith 
*/
class FirmFields_model extends CI_Model
{

	public function Customised_Fields( $Firm_id )
	{
		$this->db->select( "GROUP_CONCAT( DISTINCT ( super_admin_owned ) ) AS customised_ids " );
		$result = $this->db->get_where( "client_fields_management" , "firm_id = ".$Firm_id );

		return $result->row_array();
	}

	public function Get_FirmFields_Settings( $firm_id ,$list_page_visibility = 1)
	{
		$user_id = $this->session->userdata('id');

		$data  = $this->Customised_Fields( $firm_id );

		$ex_where = '';

		if($list_page_visibility == 1)
		{
			$ex_where .= " AND list_page_visibility=1 ";
		}
		if( !empty( $data['customised_ids'] ) ) 
		{
			$ex_where .= " AND id NOT IN (".$data['customised_ids'].") ";
		}

		$user_clause = "user_id = ".$user_id;

		if(isset($_SESSION['user_type']) && ($_SESSION['user_type'] == 'FA' || $_SESSION['user_type'] == 'SA') ){
			$user_clause = "(user_id = ".$user_id."  OR label = 'Yearly Fee' OR label = 'Average Time Year In Hours')";
		}

		$query = "SELECT * FROM client_fields_management WHERE firm_id IN (0,".$firm_id.") AND ".$user_clause." AND cnt_visibility=1  ".$ex_where." GROUP BY field_propriety ORDER BY list_page_order ASC,id ASC";
		
		$rows =  $this->db->query( $query )->result_array();

		if(!$rows)
		{
			$query = "SELECT * FROM client_fields_management WHERE firm_id IN (0,".$firm_id.") AND cnt_visibility=1  ".$ex_where." GROUP BY field_propriety ORDER BY list_page_order ASC,id ASC";
		
			$rows =  $this->db->query( $query )->result_array();
		}

		return $this->add_actual_table_column( $rows );
	}
	public function add_actual_table_column( $rows )
	{
		foreach ($rows as $key => $value) {

			$query = '';
			
			if( $value['field_type'] == 1 )
			{
			    $table  = 'tblcustomfieldsvalues';
			    $column = $value['field_propriety'];
			    
			    $relid  = 'user.id';

			    if( $value['section'] == 'Contact_Person' )
			    {
			    	$relid 	= "CONCAT(user.id,'--',client_contacts.id)";
			    }

				$query  = '(SELECT value FROM tblcustomfieldsvalues where relid='.$relid.' and fieldid='.$column.' LIMIT 1) as \''.$column.'\'';
			}
			else if(preg_match("/^contact_/i", $value['field_propriety']))
			{
				$table  = 'client_contacts';
			    $column = str_replace( 'contact_' , '' , $value['field_propriety'] );
			}                                        
			else if(preg_match("/^usertable_/i", $value['field_propriety']))
			{
				$table  = 'user';
			    $column = str_replace( 'usertable_' , '' , $value['field_propriety'] );
			}
			else 
			{
				$table  = 'client';
			    $column = $value['field_propriety'];
			}

			$rows[ $key ]['table'] 	=	$table;
			$rows[ $key ]['column']	=	$column;
			$rows[ $key ]['query']	=	( !empty( $query ) ? $query : $table.'.'.$column.' as \''.$value['field_propriety'].'\'' );

		}
		return $rows;
	}
	public function MyFirm_CustomFields( $firm_id )
	{
		$cus = $this->FirmFields_model->Customised_Fields( $firm_id );

	    $this->db->select("GROUP_CONCAT(field_propriety) as ids");
	    $this->db->where('field_type', 1 );
	    $this->db->where_in('firm_id', ['0' , $firm_id] );
	    if( !empty( $cus['customised_ids'] ) )
	    {
	        $this->db->where_not_in('id' , explode( ',' , $cus['customised_ids'] ) );
	    }
	    return $this->db->get("client_fields_management")->row_array();
	}

	public function GetMandatory_Feilds( $Firm_id )
	{
		$data = $this->Customised_Fields( $Firm_id );

		$mandatory =  "'contact_first_name','contact_main_email','crm_confirmation_statement_date','crm_personal_tax_return_date','crm_next_booking_date','crm_next_manage_acc_date','crm_insurance_renew_date','crm_registered_renew_date','crm_hmrc_yearend','crm_accounts_due_date_hmrc','crm_payroll_run_date','crm_pension_subm_due_date','crm_next_p11d_return_due','crm_vat_quarters','crm_cis_contractor_start_date','crm_cis_subcontractor_start_date','crm_investigation_end_date','crm_company_name','crm_legal_form','crm_company_number'";

		$this->db->select( "GROUP_CONCAT(id) AS ids " );
		$where = " firm_id IN (0,".$Firm_id.") AND field_propriety IN (".$mandatory.") ";
		
		if( !empty( $data['customised_ids'] ) )
		{
			$where .= "AND id NOT IN (".$data['customised_ids'].") ";
		}

		$this->db->where( $where );
		$results = $this->db->get("client_fields_management")->row_array();
		return $results;
	}

	public function GetServiceSections( $firm_id )
	{
		if( $firm_id != 0 )
      {
        $services = $this->Common_mdl->get_FirmEnabled_Services();

        $disabled_services = implode(',', array_filter( array_keys( $services ) )  );         
      }
      $remove_services = ( !empty( $disabled_services ) ? " AND section IN(".$disabled_services.") ": "" );

      $Dynamic_service  = $this->db->query("SELECT DISTINCT(a.section) as section,b.service_name FROM client_fields_management as a LEFT JOIN service_lists as b ON b.id = a.section WHERE a.firm_id = 0 AND section REGEXP '^[0-9]+$' ".$remove_services ." ORDER BY b.service_name ASC" )->result_array();

      return $Dynamic_service; 
	}

	public function GetSectionData( $firm_id )
	{

        $sections = 
        [
          'required_information',
          'important_details',
          'Contact_Person',
          'AML_check',
          'others',
          'referral'
        ];

      $Dynamic_service = $this->GetServiceSections( $firm_id );
      $sections = array_merge( $sections , array_column( $Dynamic_service , 'section' ) );
      
      $data = $this->Customised_Fields( $firm_id );
      $Avoid_Edited_Defult = "";
	   if( !empty( $data['customised_ids'] ) )
	   {
		    $Avoid_Edited_Defult .= "AND id NOT IN (".$data['customised_ids'].") ";
	   }

      $section_data = [];
      foreach ($sections as $key => $sections_value)
      {
        $content = $this->db->query("SELECT DISTINCT(content_name) AS content_name FROM client_fields_management WHERE section='".$sections_value."' AND firm_id=0")->result_array();

        foreach ($content as $key => $content_value)
        {            
            $query = "SELECT id,section as section_name,label as field_name,cnt_order as order_no,field_type as custom_id FROM client_fields_management WHERE firm_id IN (0,".$firm_id.") AND cnt_visibility=1 AND section = '".$sections_value."' AND content_name = '".$content_value['content_name']."' ".$Avoid_Edited_Defult." GROUP BY order_no ORDER BY cnt_order ASC"; 

            $section_data[ $sections_value ][ $content_value['content_name'] ] = $this->db->query( $query )->result_array();    
        }  
      }

      return $section_data;
	}

	public function array_overright(array $old ,array $new )
	{
			$overright_able = array_intersect_key( $new , $old );
			return array_replace( $old , $overright_able );
	}

	public function check_dump_field( $firm_id , $clone , $overright ) 
	{
		$user_id = $this->session->userdata('id');
		$default_table = $overright[0];
		$custom_table  = $overright[1];
		$default_table['user_id'] = $user_id;
		if( $clone['firm_id'] != $firm_id )
		{
			$custom_table['id']      = $default_table['id']      = '';
			$custom_table['firm_id'] = $default_table['firm_id'] = $firm_id;

			if( $clone['field_type'] == 1 )			
			{
				$this->db->where( "id", $clone['field_propriety'] );
				$cus_clone      = $this->db->get( "tblcustomfields" )->row_array();

				$data           = array_replace( $cus_clone , $custom_table );
				$this->db->insert( "tblcustomfields" , $data );

				$default_table['field_propriety'] = $this->db->insert_id();
			}

			$default_table['super_admin_owned']   = $clone['id']; 
			$data                                 = array_replace( $clone , $default_table );
			$this->db->insert( "client_fields_management" , $data );

			return  $this->db->insert_id();
		}
		else
		{	
			$data  = $this->Customised_Fields( $firm_id );
			$ex_where = '';
			if($list_page_visibility == 1)
			{
				$ex_where .= " AND list_page_visibility=1 ";
			}
			if( !empty( $data['customised_ids'] ) ) 
			{
				$ex_where .= " AND id NOT IN (".$data['customised_ids'].") ";
			}
			$datum = array_replace( $clone , $default_table );
			$query = "SELECT * FROM client_fields_management WHERE firm_id IN (0,".$firm_id.") AND cnt_visibility=1 AND user_id = ".$user_id." AND field_propriety = '".$datum['field_propriety']."' ".$ex_where."  GROUP BY field_propriety ORDER BY list_page_order ASC,id ASC";
		
			$row =  $this->db->query( $query )->result_array();
			
			if(count($row))
			{
				$this->db->where( 'id' , $clone['id'] );
				//is superadmin
				if( $firm_id == 0 )
				{
					$this->db->or_where( 'super_admin_owned' , $clone['id']);
				}
				$this->db->update("client_fields_management", $default_table );
			}
			else
			{
				$clone_new = $clone;
				unset($clone_new['id']);
				$data                                 = array_replace( $clone_new , $default_table );
				$this->db->insert( "client_fields_management" , $data );
			}
			
			if( $clone['field_type'] == 1 && !empty( $custom_table ) )			
			{
				$where = "id =".$clone['field_propriety'];
				//is superadmin
				if( $firm_id == 0 )
				{
					$this->db->select( "GROUP_CONCAT(field_propriety) as cus_id " );
					$this->db->where( 'id' , $clone['id'] );
					$this->db->or_where( 'super_admin_owned' , $clone['id'] );
					$result = $this->db->get( 'client_fields_management' )->row_array();	

					if( !empty( $result['cus_id'] ) )
					{
						$where .= " OR id IN(".$result['cus_id'].")";
					}
				}	
				$this->db->update( "tblcustomfields", $custom_table , $where );
			}	

			return 1;
		}	
	}



	public function Update_FirmField( $firm_id )
	{
		$overright = array(
			[
				'label'=> $_POST['name'],
			],
			[
				'name'   				=> $_POST['name'],
				'type'   				=> (!empty( $_POST['type'] ) ? $_POST['type'] : ''),
				'options' 				=> (!empty( $_POST['options'] ) ? $_POST['options'] : ''),
				'required'				=> (!empty( $_POST['required'] ) ? $_POST['required'] : '')
			]
		);

		$clone  = $this->Common_mdl->select_record( "client_fields_management" , "id" , $_POST['id'] );

		return $this->check_dump_field( $firm_id , $clone , $overright );		
	}
	public function Delete_FirmFields( $firm_id )
	{
		$overright = array(
			[
			 'cnt_visibility'	        => 0,
			 'cnt_order'				=> 0
			],
			[
				'active'   				=> 0
			]
		);

		$clone  = $this->Common_mdl->select_record( "client_fields_management" , "id" , $_POST['id'] );
		return $this->check_dump_field( $firm_id, $clone , $overright );
		
	}
	public function UpdateOrder_FirmFields( $firm_id )
	{
		$i=1;
		foreach ($_POST['datas'] as $value)
		{
			$overright = array(
				['cnt_order'				=> $i],
				[]				
			);
			$clone  = $this->Common_mdl->select_record( "client_fields_management" , "id" , $value );
			$this->check_dump_field( $firm_id , $clone , $overright );			
			$i++;
		}
	}
	public function Update_ListPageOrder_FirmFields( $firm_id )
	{

	  $order = $_POST['reorder'];
      sort($order,SORT_NUMERIC);
      foreach ($_POST['column_cnst'] as $key => $value)
      {
        $column_Const = strtolower( trim( $value ) );
        $overright = array(
        	['list_page_order' => $order[$key]],
        	[]
        );
        //echo $column_Const."===".$order[$key];
        $this->Update_ByCnst_FirmFields( $firm_id , $column_Const , $overright );
      }
      return 1;
	}
	public function Update_ListPageColVis_FirmFields( $firm_id )
	{

	  $data = $this->Get_FirmFields_Settings( $firm_id );

	  $exists = array_column( $data , 'field_propriety' );

      $update['hidden'] = array_diff( $exists , $_POST['Showhidden'] ); 

      $update['show'] =  array_diff( $_POST['Showhidden'] , $exists );
      
      //print_r( $update );

      foreach ($update as $key => $value)
      {
          $action = ( $key=="show" ? 1 : 0 );
          $overright = array(
        	['list_page_visibility' => $action],
        	[]
          );

          foreach ($value as $Const)
          {
          	$this->Update_ByCnst_FirmFields( $firm_id , $Const , $overright);
          }
      }
      return 1;
	}

	public function Update_ByCnst_FirmFields( $firm_id , $Const , $overright )
	{
		$user_id = $this->session->userdata('id');
        $where = [
        	'field_propriety' => $Const,
        	'firm_id'         => $firm_id,
        ];

        $this->db->where( $where );
        $clone = $this->db->get( "client_fields_management" )->row_array();
        if( empty( $clone ) )
        {	
        	$where['firm_id'] = 0;
        	$this->db->where( $where );
        	$clone = $this->db->get( "client_fields_management" )->row_array();

        	//check if it's custom field,Const deffer in already cloned field 
        	if( $clone['field_type'] == 1 && is_numeric( $Const ) )
        	{	
        		$Cus_field_cln_where = [
        			'super_admin_owned' => $clone['id'],
        			'firm_id'			=> $firm_id
        		];

        		$this->db->where( $Cus_field_cln_where );
        		$Is_cloned = $this->db->get( "client_fields_management" )->row_array();

        		if( !empty( $Is_cloned ) ) $clone = $Is_cloned;
        	}
        }

         $this->check_dump_field( $firm_id , $clone , $overright );
	}

	public function get_coustom_Field_by_slug( $slug , $user_id )
	{
		$custom_f = $this->db->select('id')
                                ->get_where( 'tblcustomfields' , [ 'firm_id' => $_SESSION['firm_id'] , 'slug' => $slug ] )
                                ->row_array();
            
        if( !count ( $custom_f ) )
        {
        
            $custom_f = $this->db->select( 'id' )
                                ->get_where( 'tblcustomfields' , [ 'firm_id' => 0 , 'slug' => $slug ] )
                                ->row_array();
        }

        $has_value = $this->db->select('id')
                            ->get_where( 'tblcustomfieldsvalues' , [ 'fieldid' => $custom_f['id'] , 'relid' => $user_id ] )
                            ->row_array();
		return $has_value;                            
	}
}
?>