<?php
 
  
 
function getMenuURLS( $id ,$head_menus)
{ 
  $CI =& get_instance();
  
  $url="javascript::void(0)";

  if ( !empty( $id ) )
    {
 
             $individual_key = array_search($id, array_column($head_menus, 'module_mangement_id'));
        $url=$head_menus[$individual_key];

      
      if ( strpos( $url['url'] , "#" ) === 0 )        
      {
        $url = $url['url'];
      
      }
      else
      {
        $url = base_url().$url['url'];
      }

    }

    return $url;
}

function getHeaders( $PARENT_LI , $CHILD_UL , $CHILD_LI , $head_menus)
{ 
  $CI =& get_instance();

  $menu_data = '';
  $Menus = GetMenuss($head_menus);
  foreach ($Menus as $datas)
  {
    
    $url =  getMenuURLs( $datas['href'] ,$head_menus);
    
    $ParentLi = str_replace('{URL}', $url , $PARENT_LI );  
    $ParentLi = str_replace('{ICON_PATH}', base_url().$datas['icon'] , $ParentLi );  
    $ParentLi = str_replace('{MENU_LABEL}',  $datas['text']  , $ParentLi );  

    $childCnt ='';

    if( isset( $datas['children'] ) )
    {
      $childCnt =  recursive_menus( $datas['children'] , $CHILD_UL , $CHILD_LI,$head_menus );
    }

//echo htmlspecialchars($ParentLi)."<br>";
//echo  htmlspecialchars($childCnt)."child<br>";

    $ParentLi = str_replace('{CHILD_CONTENT}', $childCnt , $ParentLi ); 




    $menu_data.=$ParentLi; 
  }

 // echo htmlspecialchars( $menu_data ); 

  return $menu_data;

}


function recursive_menus( $Child_menu , $CHILD_UL , $CHILD_LI,$head_menus )
{
  
  $CI =& get_instance();

   $url="javascript::void(0)";

   $menu_data = '';

  foreach ($Child_menu as $datas)
  {
                          
    $url = getMenuURLs( $datas['href'],$head_menus );

    $ChildLi = str_replace('{URL}', $url , $CHILD_LI );  
    $ChildLi = str_replace('{MENU_LABEL}',  $datas['text']  , $ChildLi );  

    $childCnt ='';

     if( isset( $datas['children'] ) )
    {
      $childCnt = recursive_menus( $datas['children'] , $CHILD_UL , $CHILD_LI,$head_menus );
    }

    $ChildLi = str_replace('{CHILD_CONTENT}', $childCnt , $ChildLi ); 

    $menu_data.= $ChildLi; 

  }
    
  $menu_data = str_replace('{CHILD_CONTENT}', $menu_data , $CHILD_UL );  
   
  return  $menu_data;


}

 
   function PermissionChecks( $data , $sectionSettings=0,$head_menus)
  { 
      $CI =& get_instance();
      $rolePermission = 1;

      if( !empty( $data['href'] ) )
      {
    
          $individual_key = array_search($data['href'], array_column($head_menus, 'module_mangement_id'));
        $modualData=$head_menus[$individual_key];
      
        
        $Module = trim( $modualData['module'] );

        $PermissionType = trim( $modualData['required_premission'] ); 

        $result = [];
        $needl = ",";

        if( substr_count( $PermissionType ,'||') )
        {
            $needl = "||";
        }
        else if( substr_count( $PermissionType ,'&&') )
        {
            $needl = "&&";
        }

        $PermissionType = explode( $needl , $PermissionType );              

        //print_r($modualData);
    

        foreach ($PermissionType as $perVal) 
        {
            $result[] = $_SESSION['permission'][ $Module ][ $perVal ]; 
        }

        $result = array_keys( $result , 1);

        if( ( $needl=="," || $needl=="||" ) && count($result) )
        {
            $rolePermission = 1;
        }
        else if ( $needl=="&&" &&  count($PermissionType) == count($result) )
        {
            $rolePermission = 1;                
        }
        else 
        {
            $rolePermission = 0;
        }

          //echo $Module."----".$rolePermission."---";print_r($result);
      }

      return $rolePermission;        
  }

  function subMenuss($subMenus,$head_menus )
  {

      $CI =& get_instance();
      // $subMenus  = $CI->db->query("select * from menu_management where parent_id =$id ORDER BY menu_management_id ASC ")->result_array();   
   if( !empty($subMenus) )
      {           
          $pre = 0;
          $submenu  = [];
          foreach ($subMenus as $data)
          {
              $PermissionCheck = PermissionChecks( $data ,0 ,$head_menus);

              if( !$PermissionCheck ) continue;
              $pre = 1;

              $result = subMenuss( $data['children'],$head_menus );

              if( !$result )
              {
                  $submenu[] = $data;
              }
              else if( $result!= 1 )
              {
                  $data['children'] = $result;
                  $submenu[] = $data; 
              }

          }
          
          if ($pre) return $submenu;
          else return 1;
      }
      else 
      {
          return 0;
      }
  }
  
  function GetMenuss($head_menus)
  {

      $CI =& get_instance();
      $getMenu  = $CI->db->query("select * from menu_manage where  firm_id = ".$_SESSION['firm_id']." ")->row_array();

      $mainMenus=json_decode($getMenu['menu_details'],true);


      // print_r($mainMenus);
      // exit();

      $menu = [];

      foreach($mainMenus as $key=> $data)
      {

          $PermissionCheck = PermissionChecks( $data,0,$head_menus );
          
          if ( !$PermissionCheck ) continue;

          $result = subMenuss( $data['children'] ,$head_menus );

          if( !$result ) 
          {
              $menu[] = $data;
          }
          else if( $result != 1 )
          {
              $data['children'] = $result;
              $menu[] = $data;
          }
          
      }
      return $menu;
      
  }



?>
