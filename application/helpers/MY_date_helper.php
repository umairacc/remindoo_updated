<?php
function find_last_date( $from , array $dates)
{
	// echo "inside find_last_date()\n";
	$Cron_date  = array_map(
								function( $v )
								{
									return date_format( date_create_from_format( 'd-m-Y' , $v ) , 'U');
								},
								$dates
							); 
	arsort($Cron_date);

	//echo "2=======================";
	//print_r($Cron_date);
		
	$due = date_format( date_create_from_format( 'd-m-Y' , $from ) , 'U');

	$create_records_id = 0;
	foreach ( $Cron_date as $key => $value )
	{
		//echo $due." < ".$value;
		if($due < $value) 
		{
			$create_records_id = $key;
			break;
		} 			
	}
	return $create_records_id;
}

function is_future_date( $date , $f = 'd-m-Y')
{
	// echo "inside is_future_date()\n";
	$date  =  date_format( date_create_from_format( $f , $date ) , 'U' );
	$today =  strtotime('now');

	if( $date > $today )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function is_futuredue_date( $date , $frequency)
{
	// echo "inside is_future_date()\n";

	$check_days = 5;

	if($frequency == '1 month')
	{
		$check_days = 15;
	}
	elseif($frequency == '3 Months')
	{
		$check_days = 75;
	}
	elseif($frequency == '1 Year')
	{
		$check_days = 350;
	}
	elseif($frequency == '7 days')
    {
     $check_days = 5;
    }
	$now = time(); // or your date as well
	$your_date = strtotime($date);
	$datediff = $your_date - $now;

	$datediff = round($datediff / (60 * 60 * 24));

	if( $check_days > $datediff )
	{
		return true;
	}
	else
	{
		return false;
	}
}


function Calculate_Due($date,$due_type,$days)
{ 
	// echo "inside Calculate_Due()\n";
	$Date = date_create_from_format('Y-m-d',$date);
				
	if( $due_type =='due_by' )
	{
		date_sub($Date,date_interval_create_from_date_string($days." days"));
	}
	else if(  $due_type == 'overdue_by' )
	{
		date_add($Date,date_interval_create_from_date_string($days." days"));
	}
	return date_format($Date,'d-m-Y');
}

function add_relative_date( $date , $rel )
{
	$relative = '+'.$rel;

	if(date('d',strtotime($date)) > '28')
	{
       $date = '28-'.date('m-Y',strtotime($date));
	}

    $date = new DateTime( $date );

    $start_day = $date->format('j'); 
    //var_dump( $date->format('Y-m-d') );

    $date->modify( $relative );

    //$end_day = $date->format('j'); removed as per client shared logic

    //var_dump($date->format('Y-m-d'));
    if(stripos( $rel , 'day' ) === FALSE)//$start_day != $end_day &&  removed as per client shared logic
    { 
        $date->modify('last day of this month'); // changed from last day of last month as per client shared logic
    }

    return $date->format('Y-m-d');
}
	
function Next_Frequency( $Frequency , $Related_Date )
{
	echo "inside Next_Frequency() Type =".$Frequency."\n";


		list( $Ryear,$Rmonth,$Rdate ) = explode( '-' , $Related_Date );

		if( strpos( $Frequency, 'Annual End' ) !== FALSE )
		{
			$d = $Related_Date;
			if( !is_future_date( $d , 'Y-m-d' ) )
			{
				$d = add_relative_date( $Related_Date , '1 Year');

			}

			$date = [ $d , '1 Year'];
		}
		else
		{
			switch ( trim( $Frequency ) )
			{
				case 'Jan/Apr/Jul/Oct':
				case 'Mar/Jun/Sep/Dec':
				case 'Feb/May/Aug/Nov':
					$d = $Related_Date;
					if( !is_future_date( $d , 'Y-m-d' ) )
					{						
						$d = add_relative_date( $Related_Date , '3 Months');
					}				
					$date = [ $d , '3 Months' ];
					break;	
				case 'Quarterly':
					  $date = [ $Related_Date , '3 Months' ];
					break;	
				case 'Monthly':
					/*if( checkdate( date('m') , $Rdate , date('Y') ) ) $d = date('Y').'-'.date('m').'-'.$Rdate;
				    else $d = date('Y-m-t');*/
					$date = [ $Related_Date , '1 month' ];
					break;
				case 'Weekly':
					// $date = [ Get_Week( 'weekly', $Related_Date ) , '7 days' ];
					$date = [ $Related_Date , '7 days' ];
					break;
				case 'Forthnightly':
					$date = [ $Related_Date , '14 Days' ];
					break;
				case 'Annually':
					$date = [ $Related_Date , '1 Year' ];
					break;
			}
		}

		echo "return data from Next_Frequency\n";
		print_r($date);
		return array_combine( ['due_date','duration'] , $date );		

}
function getListOfdays($y, $m,$D)
{
    $data =  new DatePeriod(
        new DateTime("first ".$D." of $y-$m"),
        DateInterval::createFromDateString('next '.$D),
        new DateTime("last day of $y-$m")
    );

    $list = [];
    foreach ($data as $key => $value)
    {
    	$list[] = $value->format('U');
    }
    return $list;
}
function GetQuaters($Month)
{	
	echo "inside GetQuaters()\n";
	$Months[] = $Month; 

	for($i=0;$i<11;$i++)
	{
		if( $Month == 12 )
		{
			$Months[] = $Month = 1;
		}
		else
		{
			$Months[] = $Month += 1;	
		}	
	}

	return array_chunk($Months, 3);		
}

function Next_Quater( $start )
{
	echo "inside Next_Quater()\n";
	$Quatrelys = GetQuaters( $start );
	//print_r( $Quatrelys );
	foreach ($Quatrelys as $key => $value)
	{
		if( in_array( date('m') , $value ) )
		{	
			//$YMD = $value[0];
			$YMD = date('Y').'-'.$value[0].'-01';
			//echo 'current month-'.$value[0];
			return EndDate( $YMD ); 
		}
	}

}

function isFuture_Month( $m )
{
	if( $m < date('m') )
	{
		return date("Y", strtotime('Next Year') )."-". $m ."-01";					
	}
	else
	{
		return  date('Y').'-'. $m .'-01';
	}
}

function EndDate( $YMD )
{  echo "inside EndDate";
	return date( 'Y-m-t' , strtotime(  $YMD ) );
}

function Get_Week( $Type , $Related_Date )
{	echo "inside Get_Week";
	$Related_Date 	= date_create_from_format( 'Y-m-d' , $Related_Date );
	$DAY 			= date_format( $Related_Date , "N");
	$Date           = date_format( $Related_Date , "l");
	
	if($Type == 'weekly')
	{
		if(date('N') == $DAY)
		{
			return date('Y-m-d');
		}
		else
		{
			return date('Y-m-d' , strtotime("next ".$Date) );
		}	
	}
	else if($Type == 'Forthnightly')
	{
		$List 		=  getListOfdays( date('Y') , date('m') , $Date );
		//print_r($List);
		
		if( date('d') < date('d' , $List[1]) )
		{
			return date("Y-m-d",$List[1]);
		}
		else if( date('d') < date('d' , $List[3]) )
		{
			return date("Y-m-d",$List[3]);
		}
		else
		{
			$List 		=  getListOfdays( date('Y' ,strtotime('next month'))  , date( 'm' ,strtotime('next month') ) , $Date );

			return 	date("Y-m-d",$List[1]);
		}
	}
}

function Get_Interval_days( $from , $to )
{
	$earlier = new DateTime( $from );
	$later   = new DateTime( $to );
	return $earlier->diff($later)->format("%r%a");
}

?>