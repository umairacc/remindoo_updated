  
<?php       
 function Change_DataFormat($string) 
 {
    $string = trim( $string );
    $matches = array();
    $pattern = '/^([0-9]{4})\-([0-9]{1,2})\-([0-9]{1,2})$/';
    if (!preg_match($pattern, $string, $matches))
    {
      return  "<td data-search='".$string."'>".$string."</td>";
    }
    else 
    {
      $data  = $matches[3]."-".$matches[2]."-".$matches[1];

      return "<td data-search='".$data."' >".$data."</td>";

    }
    
 }
function ClientColumn_ColVis_Content()
{
  $CI =& get_instance();
  $firmId = 0;
  
 /* $rows = $CI->db->query("select name,visible_firm".$firmId.",order_firm".$firmId.",cusname_firm".$firmId." from firm_column_settings where table_name ='client_list' order by order_firm".$firmId." asc
  ")->result_array();*/
  $rows = $CI->FirmFields_model->Get_FirmFields_Settings( $_SESSION['firm_id'] , 0);
  /*$CI->db->query("SELECT * FROM client_fields_management WHERE firm_id=".$_SESSION['firm_id']." AND cnt_visibility=1  ORDER BY list_page_order ASC")->result_array();*/

  $text = "<div class='colvis_column_list'><div class='close'>
        <button type='button' class='close' data-dismiss='modal'>×</button>
      </div><ul>";
  foreach ($rows as $key => $value)
  {
      $IfSelected = "";
      
      if( trim( $value["list_page_visibility"] ) == 1)
      {
        $IfSelected = "active";
        
      } 

      $text .= "<li data-const='".$value['field_propriety']."' class='".$IfSelected."'><span>".$value["label"]."</span></li>";
  }
  $text.="</ul></div>";

  return $text;
}

function ClientColumn_Status_SelectBox()
{
  $text = "<select multiple='true' searchable='true' class='filter_check' style='display:none'>
            <option value='Active' >Active</option>
            <option value='Inactive' >Inactive</option>
            <option value='Frozen' >Frozen</option>
            <option value='Draft' >Draft</option>
            <option value='Archive' >Archive</option>
          </select>";
          return $text;
}

function ClientColumn_FirmStaffs_SelectBox()
{
  $CI =&  get_instance();
  $text = "<select multiple='true' searchable='true' class='filter_check search_assignees' style='display:none'>";
  
  $CI->load->model('User_management');
  $staffs = $CI->User_management->Get_FirmStaffs();
  foreach ($staffs as $value) {
    $text .= "<option value='".$value['id']."'>".$value['crm_name']."</option>";
  }

  $text .= "</select>";
  
  return $text;
}
 
function ClientColumn_landlines( $User_data , $Client_Data , $contact_data )
{ 

  $type   = json_decode( $contact_data['pre_landline'] , true );
  $number = json_decode( $contact_data['landline'] , true );
  
  $text   = [];
  if( !empty( $type )  && !empty( $number ) )
  {
    foreach ($number as $key => $value)
    {
      $text[] = $type[ $key ]." ".$value;
    }
  }
  $text = implode(',',$text);
  return "<td data-search='".$text."'>".$text."</td>";
}
function ClientColumn_work_emails( $User_data , $Client_Data , $contact_data ) 
{
  $text = "";
  $work_email   = json_decode( $contact_data['work_email'] , true );
  
  if( !empty( $work_email ) ) $text = implode(',',$work_email);

  return "<td data-search='".$text."'>".$text."</td>";
}
function ClientColumn_addBaseUrl( $User_data , $Client_Data , $contact_data ) 
{
  //echo $Client_Data['proof_attach_file']."attachment";

  $data = explode(',',$Client_Data['proof_attach_file']);

  $search_text = [];
  $tag         = [];
  if( !empty( $data ) )
  {
    $data = array_filter( $data );
    //print_r($data);echo "array";
    foreach ($data as $key => $value)
    {

      $file_name = explode('/',$value);
      $file_name = end($file_name);
      $tag[] = "<a href='".base_url().$value."' traget='_blank'>".$file_name."</a>";
      $search_text[] = $file_name;
      # code...
    }
  }
  return "<td data-search='".implode(',', $search_text)."'>".implode(',', $tag )."</td>";
}
function add_CH_status( $CH_status )
{
  $return = '';

  if( ! empty( $CH_status ) )
  {
    $color_code = ['active'=>'green','dissolved'=>'red'];
    $color = 'yellow';
    if ( isset( $color_code[ $CH_status ] ) )
      $color  = $color_code[ $CH_status ];

    $return = "<b style='color:".$color."'> (".ucfirst( $CH_status ).")</b>";     
  }
  return $return;
}
function ClientColumn_Client ( $User_data , $Client_Data , $contact_data ) 
{
  $CI =& get_instance();

  $data = ( $Client_Data['crm_company_name']!='' ? $Client_Data['crm_company_name'] : $User_data['crm_name'] );

  $data = trim( $data );
  $status_label = add_CH_status( trim ( $Client_Data['crm_ch_status'] ) );

  $text = "<td data-search='".$data."'>
            <a href='".base_url()."client/client_infomation/".$User_data['id']."'>".$data.'</a>'.$status_label;
  $result=$CI->Common_mdl->empty_field_find($User_data['id']); 
  if(count($result) >= 1)
  {

    $text .="<a href='javascript:;' class='missing_info' style=''>
              <i class='fa fa-info-circle' aria-hidden='true' style='color:red'></i>
            </a>
            <div class='missing_details' style='color:red; display: none;'>";
    

    foreach($result as $res)
    {
      $text .='<div class="missing_section"> Missing:'.$res.'</div>';
    }
    
    $mail = (!empty( $contact_data['main_email'] ) ? $contact_data['main_email'] : '');
    
    $text .= "<input type='hidden' class='missing_details_email' value='".$mail."'><input type='hidden' class='missing_details_client_id' value='".$Client_Data['id']."'>";

    $text .= "<textarea  class='missing_details_content' style='display:none;'>";
   
    $i=1;
    
    $text .= "Following Details Are Missing<br/>";

    foreach($result as $res)
    {
      $text .= "<b>".$i.") ". $res."</b></br>";
      $i++;
    }
    $text .= "</textarea><button type='button' class='close misclose' data-dismiss='modal'>×</button>";

    $query=$CI->db->query('SELECT firm_id FROM admin_setting WHERE firm_id="'.$_SESSION['firm_id'].'" AND  missing_details=1')->num_rows();
    if($query && $mail!='')
    {  
          $text .="<div class='mailsentcls'>
            <a href='javascript:;' class='Open_MissingMailCompos'>Send</a>
          </div>";
    }
    $text.="</div>";
  }
  $text.="</td>";   
  return $text;
}

function ClientColumn_Type ( $User_data , $Client_Data , $contact_data ) 
{
        $CI =& get_instance();

  $Company_types = array('LTD' => 'Private Limited company', 'PLTD' => 'Public Limited company', 'LLP' => 'Limited Liability Partnership', 'P`ship' => 'Partnership', 'S.A'=>'Self Assessment','TT'=>'Trust','Charity'=>'Charity','other'=>'Other');
  
  $key = array_search( trim( $Client_Data['crm_legal_form'] ), $Company_types);

   return "<td data-search='".$key."'>".$key."</td>";

}
function ClientColumn_Status ( $User_data , $Client_Data , $contact_data ) 
{
  $CI =& get_instance();
  
  //if status empty
  if( $User_data['status'] == '' ) $User_data['status'] = 0;

  $status = [ ['Inactive',''] , ['Active',''] ,['Inactive',''], ['Frozen',''] , ['Draft',''] , ['Archive',''] ];

  $status[$User_data['status']][1] = 'selected="selected"';

  $HasPermission = ($_SESSION['permission']['Client_Section']['edit']!=0 ? true :false);

  $text = "<td data-search='".$status[$User_data['status']][0]."'><select name='status".$User_data['id']."' id='status".$User_data['id']."' class='client_status' data-id='".$User_data['id']."'" ;

          if($User_data['status']=='4' || !$HasPermission ) $text .= "disabled='disabled'";

           $text .= "><option value='0' ".$status[0][1].">Inactive</option>
           <option value='1' ".$status[1][1].">Active</option>
           <option value='3' ".$status[3][1].">Frozen</option>";
                                                    
          if($User_data['status']=='4') $text .="<option value='4' ".$status[4][1].">Draft</option>";
                                                                 
          $text .= "<option value='5' ".$status[5][1].">Archive</option></select></td>";
  return $text;


}
function  ClientColumn_staff_manager ( $User_data , $Client_Data , $contact_data ) 
{
  $CI =& get_instance();

  $staff = $CI->Common_mdl->GetAllWithWhere('responsible_user','client_id',$User_data['id']);

    $staffs = array_filter( array_column( $staff , 'manager_reviewer') ); 
    
    if(!empty($staffs)) 
    {
    
    
    $staff1=implode(',', $staffs);

    $staff_form = $CI->db->query('select first_name from staff_form where user_id in ('.$staff1.')')->result_array();
  
    $val = array_filter( array_column( $staff_form , 'first_name') ); 


    if(!empty($val))
    {
      $staff2=implode(',', $val);
      $val=array();
    } 
    else
    {
      $staff2='';
    }
    
    }
    else
    {
      $staff2='';
    }
    return "<td data-search='".$staff2."'>".$staff2."</td>";
}

 
function  ClientColumn_staff_managed ( $User_data , $Client_Data , $contact_data ) 
{
        $CI =& get_instance();

  $staff = $CI->Common_mdl->GetAllWithWhere('responsible_user','client_id',$User_data['id']);

                                                        
    $staffs = array_filter( array_column( $staff , 'assign_managed') ); 


    if(!empty($staffs)) 
    {

    //  echo '<pre>'; print_r($staffs); 
    $staff2=implode(',',$staffs);

    
    $managed = $CI->db->query('select crm_name from user where id in ('.$staff2.')')->result_array();

    $val = array_filter( array_column( $managed , 'crm_name') ); 
    

      if(!empty($val))
      {
        $staff4=implode(',', $val);
      } 
      else 
      {
        $staff4='';
      }
    }
    else
    {
     $staff4='';
    }
    
  return "<td data-search='".$staff4."'>".$staff4."</td>";
}

function ClientColumn_team ( $User_data , $Client_Data , $contact_data ) 
{
        $CI =& get_instance();


   $team=$CI->Common_mdl->GetAllWithWhere('responsible_team','client_id',$User_data['id']);
   $teams = array_filter( array_column( $team, 'team' ) );
      
      if(!empty($teams)) 
      {
        $team1=implode(',', $teams);
        
        $teamed = $CI->db->query('select team from team where id in ('.$team1.')')->result_array();
        
        $val = array_filter( array_column( $teamed, 'team' ) );

        if(!empty($val))
        {
          $team2=implode(',', $val);
        } 
        else
        {
          $team2='';
        }
      } 
      else
      {
        $team2='';
      }
                                           
   return "<td data-search='".$team2."'>".$team2."</td>";

}

function ClientColumn_team_allocation( $User_data , $Client_Data , $contact_data ) 
{
        $CI =& get_instance();

   $team=$CI->Common_mdl->GetAllWithWhere('responsible_team','client_id',$User_data['id']);
   $team3 = "";
    if(is_array($team) && count($team) > 0)
    {
      $teams = array_filter( array_column( $team , 'allocation_holder') );

      if(!empty($teams))
      {
        $team3=implode(',', $teams);
      }
      else
      {
        $team3='';
      }

    }
    
    return "<td data-search='".$team3."'>".$team3."</td>";

}
function ClientColumn_department( $User_data , $Client_Data , $contact_data )  

{
        $CI =& get_instance();

   $department=$CI->Common_mdl->GetAllWithWhere('responsible_department','client_id',$User_data['id']);
   $depart1="";
    if(is_array($department) && count($department) > 0)
    {   
      $departs = array_column( $department , 'depart' );
    }
    
    if(!empty($departs))
    {
      $depart1=implode(',', $departs);
    } 
    else
    {
      $depart1='';
    }

  return "<td data-search='".$depart1."'>".$depart1."</td>";

}

function ClientColumn_department_allocation( $User_data , $Client_Data , $contact_data )  

{
          $CI =& get_instance();
          $depart3="";
   $department=$CI->Common_mdl->GetAllWithWhere('responsible_department','client_id',$User_data['id']);

    if(is_array($department) && count($department) > 0)
    {
      $departs=array_column($department,'allocation_holder');
    } 
    if(!empty($departs))
    {
    $depart3=implode(',', $departs);
    
    } else {
      $depart3='';
    }

    return "<td data-search='".$depart3."'>".$depart3."</td>";
}
                                                            
function ClientColumn_member_manager( $User_data , $Client_Data , $contact_data )
{
        $CI =& get_instance();
        $member1="";
  $member=$CI->Common_mdl->GetAllWithWhere('responsible_members','client_id',$User_data['id']);

  if(is_array($member) && count($member) > 0)
  {                                        
    foreach ($member as $key => $value) {
    $members[]=$value['manager_reviewer'];
    }
  }
  
  if(!empty($members))
  {
    $member1=implode(',', $members);
  }
  else
  {
    $member1='';
  }

return "<td data-search='".$member1."'>".$member1."</td>";

}                                                                                                   



function ClientColumn_member_managed( $User_data , $Client_Data , $contact_data )
{
        $CI =& get_instance();

  $member=$CI->Common_mdl->GetAllWithWhere('responsible_members','client_id',$User_data['id']);
  $member2="";
  if(is_array($member) && count($member) > 0)
  { 
      foreach ($member as $key => $value)
      {
      $members[]=$value['assign_managed'];
      }
  }
  if(!empty($members))  
  {
    $member2=implode(',', $members);
  }
  else
  {
    $member2='';
  }
  return "<td data-search='".$member2."'>".$member2."</td>";

}

function ClientColumn_client_AddtedFrom( $User_data , $Client_Data , $contact_data )
{

  $source = ['Manual','Company House','Import','From Leads'];

  if(isset($source[ $Client_Data['status'] ]))
  {
  echo "<td data-search='".$source[ $Client_Data['status'] ]."'>".$source[ $Client_Data['status'] ]."</td>";
  }
  else
  {
    echo "<td data-search=''></td>";
  }


} 

function ClientColumn_created_date( $User_data , $Client_Data , $contact_data )
{

    $date = date('d-m-Y',trim($User_data['CreatedTime']) );;

    echo "<td data-search='".$date."' data-sort='".$User_data['CreatedTime']."'>".$date."</td>";  
}
function ClientColumn_assignees( $User_data , $Client_Data , $contact_data )
{
  $CI =& get_instance();

  $assignees = get_specific_module_data_assignee('CLIENT' , $Client_Data['id'] );
  if( !empty( $assignees ) )
  {
    
    $assignees = $CI->db->select( 'crm_name' )
                        ->where_in( 'id' , $assignees )
                        ->get( 'user' )
                        ->result_array();
    $assignees = implode(',' , array_column( $assignees , 'crm_name' ) );

    echo '<td data-search="'.$assignees.'">'.$assignees.'</td>';
  }

} 

function ClientColumn_check_CS_ch_overdue(  $User_data , $Client_Data , $contact_data  )
{
  $lable = $due_date = Change_Date_Format( $Client_Data['crm_confirmation_statement_due_date'] );

  if( $Client_Data['is_ch_cs_overdue'] ) 
  {
    $lable = "<b style='color:red;'>". $due_date ." - Overdue</b>";
  }

  return "<td data-search='".$due_date."' data-sort='".$due_date."'>".$lable."</td>";
}
function ClientColumn_check_Acc_ch_overdue( $User_data , $Client_Data , $contact_data  )
{
  $lable = $due_date = Change_Date_Format( $Client_Data['crm_ch_accounts_next_due'] );

  if( $Client_Data['is_ch_accounts_overdue'] ) 
  {
    $lable = "<b style='color:red;'>".$due_date." - Overdue</b>";
  }

  return "<td data-search='".$due_date."' data-sort='".$due_date."'>".$lable."</td>";
}
?>