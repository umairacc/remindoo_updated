<?php

function setup_body_content( $firm_id , $body )
{
    $CI =& get_instance();

    $firm_settings  = $CI->db->select( 'header1,footer1,header_footer_background_color' )->get_where( "admin_setting" , "firm_id=".$firm_id )->row_array();

    $data['header'] =  $firm_settings['header1'];
    $data['body']   =  $body;
    $data['footer'] =  $firm_settings['footer1'];
    $data['background_color'] =  $firm_settings['header_footer_background_color'];
    
    $body = $CI->load->view( 'email/basic_template_view' , $data , true );

    return $body;
}

function firm_settings_send_mail( $firm_id , $to , $subject , $body , $debug_level = 0 )
{   
    $CI =& get_instance();
    $attachments = [];
    $recipient   = $to;

    $CI->load->library('phpmailer_lib');
    $mail           = $CI->phpmailer_lib->load();
    

    /*check  if has any attachment*/
    if( is_array( $body ) )
    {

        if( !empty( $body['attachment'] ) )
        {
            if( !is_array( $body['attachment'] ) )
            {
                $attachments[] = $body['attachment'];
            }
            else
            {
                $attachments = $body['attachment'];
            }
        }
        $body = $body['body'];
    }
    
    $body           = setup_body_content( $firm_id , $body );
    


    $response       = ['result'=>1,'message'=>''];
    $smtp_details   = $CI->db->get_where( "mail_protocol_settings", "firm_id=".$firm_id )->row_array();

    if( $debug_level )
    {
        echo "\nSMTP details\n";
        print_r( $smtp_details );
        echo "\n";        
    }
  
    // SMTP configuration
    $_SESSION['debug_smtp'] = '';
    $mail->isSMTP();
    $mail->Hostname     = 'remindoo.co';
    $mail->SMTPDebug    = $debug_level;
    $mail->Debugoutput  = function( $str , $level ){ echo  "</br><b>".$str."</b>"; $_SESSION['debug_smtp'] .= "</br><b>".$str."</b>"; };
    $mail->Host         = trim( $smtp_details['smtp_server'] );
    $mail->SMTPAuth     = true;
    $mail->Username     = trim( $smtp_details['smtp_username'] );
    $mail->Password     = $smtp_details['smtp_password'];
    $mail->SMTPSecure   = $smtp_details['smtp_security'];
    $mail->Port         = $smtp_details['smtp_port'];
    // Set email format to HTML
    $mail->isHTML( true );

    $mail->CharSet = 'UTF-8';

    
    $mail->setFrom( $smtp_details['smtp_email'] , $smtp_details['smtp_from_name'] );


    // Add a recipient

    if(get_instance()->config->item('environment') != 'production')
    {
        // $to     = "iitb.riteshag@gmail.com";
        if( is_array( $recipient ) )
        {
            $new_recipient['to'][] ='sourav.skr11@gmail.com';
            $new_recipient['to'][] ='srv.skr19@gmail.com';
            if(isset($recipient['bcc'])){
                $new_recipient['bcc'][] ='sudeshnacsesit@gmail.com';
            }
            $recipient = $new_recipient;
        }
        else{
            $recipient = 'sourav.skr11@gmail.com';
        }
    }

    //check if multiple recipient
    if( is_array( $recipient ) )
    {
        if( !empty( $recipient['to']) )
        {
            if( is_array( $recipient['to'] ) )
            {
                foreach ( $recipient['to'] as $to_address ) $mail->addAddress( $to_address );
            }
            else
            {
                $mail->addAddress( $recipient['to'] );
            }
        }
        if( !empty( $recipient['cc']) )
        {
            if( is_array( $recipient['cc'] ) )
            {
                foreach ( $recipient['cc'] as $cc_address ) $mail->addCC( $cc_address );
            }
            else
            {
                $mail->addCC( $recipient['cc'] );
            }
        }
        if( !empty( $recipient['bcc']) )
        {
            if( is_array( $recipient['bcc'] ) )
            {
                foreach ( $recipient['bcc'] as $bcc_address ) $mail->addBCC( $bcc_address );
            }
            else
            {
                $mail->addBCC( $recipient['bcc'] );
            }
        }
    }
    else
    {    
        $mail->addAddress( $recipient );
    }


    // Add cc or bcc 
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    // Email subject
    $mail->Subject = strip_tags( html_entity_decode( $subject ) );



    // Email body content        
    $mail->Body = html_entity_decode( $body );

    foreach ($attachments as $path) {
        
        $mail->addAttachment( $path );
    }
        
    try
    {
        if( !$mail->send() )
        {               
            $response['result'] = 0;
        }
        $response['message'] = $_SESSION['debug_smtp'];
        unset( $_SESSION['debug_smtp'] );
    }
    catch (Exception $e)
    {
        $response['Exception'] = $e->errorMessage(); //Pretty error messages from PHPMailer
    }
    catch (\Exception $e)
    {   
        //The leading slash means the Global PHP Exception class will be caught
        $response['Exception'] = $e->getMessage(); //Boring error messages from anything else!
    }

   return $response;   
}
?>