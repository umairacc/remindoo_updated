<?php
function Decode_ClientTable_Datas( $Client_id ,array $Contents ,$client_contacts_id = 0 )
{
	$CI =& get_instance();
	
	$Datas['CLIENT_DATA'] 			= $CI->db->query("SELECT * FROM `client` WHERE id=".$Client_id)->row_array();
	
	if( $client_contacts_id != 0 )
	{
		$Datas['CLIENT_CONTACT_DATA']	= $CI->db->query( "SELECT * FROM `client_contacts` WHERE id=".$client_contacts_id )->row_array();
	}
	else
	{
		$Datas['CLIENT_CONTACT_DATA']	= $CI->db->query("SELECT * FROM `client_contacts` WHERE make_primary=1 AND client_id=".$Datas['CLIENT_DATA']['user_id'])->row_array();	
	}

	$Datas['USER_DATA']	 			= $CI->db->query("SELECT * FROM `user` WHERE id=".$Datas['CLIENT_DATA']['user_id'])->row_array();
	$Datas['FIRM_SET_DATA']			= $CI->db->query("SELECT * FROM `admin_setting` WHERE firm_id=".$Datas['CLIENT_DATA']['firm_id'])->row_array();
	return Content_Decode( $Datas , $Contents );
}
function Decode_LeadTable_Datas( $lead_id , array $Contents){
	$CI =& get_instance();
	$Datas['LEAD_DATA'] 			= $CI->db->query("SELECT * FROM `leads` WHERE id=".$lead_id)->row_array();
	$firm_id						= $Datas['LEAD_DATA']['firm_id'];
	$Datas['FIRM_SET_DATA']			= $CI->db->query("SELECT * FROM `admin_setting` WHERE firm_id=".$firm_id)->row_array();
	return Lead_Content_Decode( $Datas , $Contents );
}
function  Lead_Content_Decode( $Datas , $Contents )
{	
	$CI =& get_instance();
	$CI->load->helper('firm_columnsettings');

	$LEAD_DATA 			=	( !empty( $Datas['LEAD_DATA']	) ? $Datas['LEAD_DATA'] : [] );
	$FIRM_SET_DATA			=	( !empty( $Datas['FIRM_SET_DATA'] ) ? $Datas['FIRM_SET_DATA']: [] );

	/*For Common Tokens*/
	$genral_token = array(
		'::first_name::'				=>	( !empty( $LEAD_DATA['name'] ) ? $LEAD_DATA['name'] :''),
		'::last_name::'					=>	'',
		'::Client Username::' 			=>	( !empty( $LEAD_DATA['name'] ) ? $LEAD_DATA['name'] :''),
		'::Client First Name::' 		=>	( !empty( $LEAD_DATA['name'] ) ? $LEAD_DATA['name'] :''),
		'::Username::'					=>	'',
		'::Client Password::' 			=>	'',
		'::Client Name::' 				=>	( !empty( $LEAD_DATA['name'] ) ? $LEAD_DATA['name'] :''),
		':: Staff Name::'				=>	'',
		'::Client Company::' 			=>	( !empty( $LEAD_DATA['company'] ) ? $LEAD_DATA['company'] :''),
		': Client Missing Datas::' 		=>	'',
		':: Client Invitation Link::' 	=>  '',
		'::CHASE_INFO_NOTES::' 			=>	'',
		':: ClientPhoneno::' 			=>	( !empty( $LEAD_DATA['phone'] ) ? $LEAD_DATA['phone'] :''),
		':: ClientWebsite::' 			=>	( !empty( $LEAD_DATA['website'] ) ? $LEAD_DATA['website'] :''),
		'::SenderCompanyAddress::' 		=>	'',
		'::SenderCompany::' 			=>	( !empty( $FIRM_SET_DATA['company_name'] ) ? $FIRM_SET_DATA['company_name'] :''),
		'::URL::' 						=>	base_url(),
		'::senderPhoneno::' 			=>	( !empty( $FIRM_SET_DATA['company_contact_no'] ) ? $FIRM_SET_DATA['company_contact_no'] :''),
		'::SenderEmail::'  				=>	( !empty( $FIRM_SET_DATA['company_email'] ) ? $FIRM_SET_DATA['company_email'] :'')
		);

	foreach($Contents as $key=>$val )
	{
		$Contents[ $key ] = strtr( $val , $genral_token );
	}

	
	foreach($LEAD_DATA as $key => $val)
	{
		//change date formate if data is date
		$val = isdate_change_format( $val );

		$k = '{'.$key.'}';
		$k1 = str_replace('crm_','',$key);
		$k1 = "::".trim($k1)."::";
		$LEAD_DATA[$k] = $LEAD_DATA[$k1] = $val;
		unset($LEAD_DATA[$key]);
	}
	
	foreach($Contents as $key=>$val )
	{
		$Contents[ $key ] = strtr( $val , $LEAD_DATA );
	}

	/*Remove Un decode tokens */
	foreach($Contents as $key=>$val )
	{
		$Contents[ $key ] =	remove_undecode_tokens( $val );
	}

	return $Contents;
}
function Decode_StaffTable_Datas( $user_id , array $Contents  )
{
	$CI =& get_instance();
	$Datas['USER_DATA']	 			= $CI->db->query("SELECT * FROM `user` WHERE id=".$user_id)->row_array();
	$firm_id						= $Datas['USER_DATA']['firm_id'];
	$Datas['FIRM_SET_DATA']			= $CI->db->query("SELECT * FROM `admin_setting` WHERE firm_id=".$firm_id)->row_array();

	return Content_Decode( $Datas , $Contents );
}
function  Content_Decode( $Datas , $Contents )
{	
	$CI =& get_instance();
	$CI->load->helper('firm_columnsettings');

	$CLIENT_DATA 			=	( !empty( $Datas['CLIENT_DATA']	) ? $Datas['CLIENT_DATA'] : [] );
	$CLIENT_CONTACT_DATA	=	( !empty( $Datas['CLIENT_CONTACT_DATA'] ) ? $Datas['CLIENT_CONTACT_DATA'] :[] );
	$USER_DATA				=	( !empty( $Datas['USER_DATA'] ) ? $Datas['USER_DATA'] : [] );
	$FIRM_SET_DATA			=	( !empty( $Datas['FIRM_SET_DATA'] ) ? $Datas['FIRM_SET_DATA']: [] );

	/*For Common Tokens*/
	$genral_token = array(
		'::first_name::'				=>	( !empty( $CLIENT_CONTACT_DATA['first_name'] ) ? $CLIENT_CONTACT_DATA['first_name'] :''),
		'::last_name::'					=>	( !empty( $CLIENT_CONTACT_DATA['last_name'] ) ? $CLIENT_CONTACT_DATA['last_name'] :''),
		'::Client Username::' 			=>	( !empty( $USER_DATA['username'] ) ? $USER_DATA['username'] : '' ),
		'::Username::'					=>	( !empty( $USER_DATA['username'] ) ? $USER_DATA['username'] : '' ),	
		'::Client Password::' 			=>	( !empty( $USER_DATA['confirm_password'] ) ? $USER_DATA['confirm_password'] :''),
		'::Client Name::' 				=>	( !empty( $CLIENT_CONTACT_DATA['first_name'] ) ? $CLIENT_CONTACT_DATA['first_name'] :''),
		':: Staff Name::'				=>	( !empty( $USER_DATA['crm_name'] ) ? $USER_DATA['crm_name'] :''),
		'::Client Company::' 			=>	( !empty( $CLIENT_DATA['crm_company_name'] ) ? $CLIENT_DATA['crm_company_name'] :''),
		': Client Missing Datas::' 		=>	'',
		':: Client Invitation Link::' 	=>  ( !empty( $CLIENT_DATA['crm_other_send_invit_link'] ) ? $CLIENT_DATA['crm_other_send_invit_link'] :''),
		'::CHASE_INFO_NOTES::' 			=>	( !empty( $CLIENT_DATA['crm_other_notes'] ) ? $CLIENT_DATA['crm_other_notes'] :''),
		':: ClientPhoneno::' 			=>	( !empty( $CLIENT_CONTACT_DATA['mobile'] )? $CLIENT_CONTACT_DATA['mobile'] : ''),
		':: ClientWebsite::' 			=>	( !empty( $CLIENT_DATA['crm_company_url'] ) ? $CLIENT_DATA['crm_company_url'] :''),
		'::SenderCompanyAddress::' 		=>	'',
		'::SenderCompany::' 			=>	( !empty( $FIRM_SET_DATA['company_name'] ) ? $FIRM_SET_DATA['company_name'] :''),
		'::URL::' 						=>	base_url(),
		'::senderPhoneno::' 			=>	( !empty( $FIRM_SET_DATA['company_contact_no'] ) ? $FIRM_SET_DATA['company_contact_no'] :''),
		'::SenderEmail::'  				=>	( !empty( $FIRM_SET_DATA['company_email'] ) ? $FIRM_SET_DATA['company_email'] :'')
		);

	foreach($Contents as $key=>$val )
	{
		$Contents[ $key ] = strtr( $val , $genral_token );
	}

	
	foreach($CLIENT_DATA as $key => $val)
	{
		//change date formate if data is date
		$val = isdate_change_format( $val );

		$k = '{'.$key.'}';
		$k1 = str_replace('crm_','',$key);
		$k1 = "::".trim($k1)."::";
		$CLIENT_DATA[$k] = $CLIENT_DATA[$k1] = $val;
		unset($CLIENT_DATA[$key]);
	}
	
	foreach($Contents as $key=>$val )
	{
		$Contents[ $key ] = strtr( $val , $CLIENT_DATA );
	}

	/*Remove Un decode tokens */
	foreach($Contents as $key=>$val )
	{
		$Contents[ $key ] =	remove_undecode_tokens( $val );
	}

	return $Contents;
}
function  decode_notification_template( $source , $templated )
{
	

		/*for avoid json data*/
		$array =	array(
						'conf_statement'		=> 	'',
						'accounts'				=> 	'',
						'company_tax_return'	=>	'',
						'personal_tax_return'	=>	'',
						'payroll'				=>	'',
						'workplace'				=>	'',
						'vat'					=>	'',
						'cis'					=>	'',
						'cissub'				=>	'',
						'p11d'					=>	'',
						'bookkeep'				=>	'',
						'management'			=>	'',
						'investgate'			=>	'',
						'registered'			=>	'',
						'taxinvest'				=>	'',		
						'taxadvice'				=>	''
					);
		$source = array_merge( $source , $array );
		/*for avoid json data*/

    foreach($source as $key => $value) {
	

    	unset( $source[$key] );
        $key = "{".$key."}";
        $source[$key] = $value;
    }
    return strtr( $templated , $source );
}
function remove_undecode_tokens( $Contents )
{
	$Contents =	preg_replace('/::[^:]*::/',"", $Contents );
	$Contents =	preg_replace('/{[^}]*}/',"", $Contents );
	return $Contents;
}

function task_mail_content_details($firm_id)
{
		$CI =& get_instance();
	    $company_name='-';
        $task_status='-';
        $progress_status='-';
        $due_date='-';
        $task_name="-";
        $mail_content=[];




        if(isset($firm_id[0]['company_name'] ) && $firm_id[0]['company_name']!='' && is_numeric($firm_id[0]['company_name']))
        {
         // echo "chk";
         // $company_name=$this->Common_mdl->select_record('client','id',$firm_id[0]['company_name']);

          $company_name=$CI->db->query("SELECT * FROM `client` WHERE id=".$firm_id[0]['company_name'])->row_array();


          //$CI->db->query("SELECT * FROM `task_status` WHERE id=".$firm_id[0]['task_status'])->row_array();
          $company_name=$company_name['crm_company_name'];

        }
        // print_r($company_name);

          if(isset($firm_id[0]['end_date'] ))
        {
          $due_date=date('d-m-Y',strtotime($firm_id[0]["end_date"]));
         

        }

              if(isset($firm_id[0]['subject'] ))
        {
          $task_name=$firm_id[0]["subject"];
         

        }


          if(isset($firm_id[0]['task_status'] ) && $firm_id[0]['task_status']!='' && is_numeric($firm_id[0]['task_status']))
        {
          $task_status=$CI->db->query("SELECT * FROM `task_status` WHERE id=".$firm_id[0]['task_status'])->row_array();

          $task_status=$task_status['status_name'];

        }

          if(isset($firm_id[0]['progress_status'] ) && $firm_id[0]['progress_status']!='' && is_numeric($firm_id[0]['progress_status']))
        {
         // $progress_status=$this->Common_mdl->select_record('task_status', 'id', $firm_id[0]['progress_status']);
          $progress_status=$CI->db->query("SELECT * FROM `progress_status` WHERE id=".$firm_id[0]['progress_status'])->row_array();
          $progress_status=$progress_status['status_name'];

        }

        $mail_content=['company_name'=>$company_name,'due_date'=>$due_date,'task_status'=>$task_status,'progress_status'=>$progress_status,'task_name'=>$task_name];

        return $mail_content;

}    
?>