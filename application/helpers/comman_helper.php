<?php
 function GetAssignees_SelectBox_Content( $defult_select=0,$disable_id=0,$id=0,$data_value=1 )
    { 
      $CI =& get_instance(); 
      $CHILDS = '';

          $DB_DATA = $CI->db->query("select * from roles_section where firm_id = ".$_SESSION['firm_id']." and  parent_id=".$id." ")->result_array();  

        
          foreach( $DB_DATA as $val )
          {
            
              $Extraclass = "";

              if($defult_select!=0 && $defult_select==$val['id'] )
              {
                $Extraclass .= ' active';
              }
              if($disable_id!=0 && $disable_id==$val['id'] )
              {
                $Extraclass .= ' disabled';
              }
              
            $CHILDS.= '<a class="dropdown-item remove_member '.$Extraclass.'" data-value="'.$val['id'].'" data-level="'.$data_value.'" href="#"><i class="fa fa-dot-circle-o"></i>'.$val['role'].'</a>';

            $CHILDS.= GetAssignees_SelectBox_Content($defult_select,$disable_id, $val['id'] , $data_value+1 );
          }
          return $CHILDS;
    }

function GetAssignees_SelectBox( $id=0 , $data_value='')
{   
  $CI      =& get_instance();  
  $CHILDS  = [];
  $FIRM_ID = $_SESSION['firm_id'];
  $SQL     = "SELECT OT.* 
              FROM organisation_tree OT
              INNER JOIN user U
                      ON U.id = OT.user_id
                     AND U.status IN('1')
              WHERE OT.firm_id = $FIRM_ID
              AND OT.parent_id = $id";  
  
  //"select * from organisation_tree where firm_id = ".$_SESSION['firm_id']." and  parent_id=".$id." ";
  $DB_DATA = $CI->db->query($SQL)->result_array();  

    
  foreach( $DB_DATA as $val ){
    $CHILD_DATA = [];
    $USER_ID    = $val['user_id'];
    $SQL        = "SELECT crm_name 
                  FROM user 
                  WHERE id = $USER_ID
                  AND status IN('1')";
    $user_info  = $CI->db->query($SQL)->row_array();
    
    if(empty( $data_value )){
      $value = $val['id'];
    }else{
      $value = $data_value.':'.$val['id'];
    }
    $CHILD_DATA['id']    = $value.'_'.$val['user_id'];
    $CHILD_DATA['title'] = $user_info['crm_name'];
    $hasChild            = GetAssignees_SelectBox( $val['id'] , $value );

    if( !empty( $hasChild ) ){
      $CHILD_DATA['subs'] = $hasChild;
    }
    $CHILDS[] = $CHILD_DATA;
  }
  return $CHILDS;
}

function Has_view_all_permission( $Module , $user_id )
{
   $CI     =& get_instance();
   $result = 0;

   $Module_view = ['CLIENT'=>'viewall_Client_Section','TASK'=>'viewall_Task','LEADS'=>'viewall_Leads'];

   if( !empty( $Module_view[ $Module ] ) )
   { 
     $field    = $Module_view[ $Module ];

     if( $user_id )
     {
       $query  = "SELECT rp.`".$field."` as result FROM user as u
                   INNER JOIN role_permission1 rp 
                   ON rp.role_id=u.role
                   WHERE u.id=".$user_id;
     }
     else
     { 
       $role_id  = $_SESSION['roleId'];

       if(!empty($role_id))
       {
          $query  = "SELECT `".$field."` as result FROM role_permission1 WHERE role_id=".$role_id;
       }
       else if($_SESSION['role'] == 'super_admin')
       {
          $query  = "SELECT rp.`".$field."` as result FROM user as u INNER JOIN role_permission1 rp ON rp.role_id=u.role WHERE u.id=1";
       }
     }
       $result = $CI->db->query( $query )
                         ->row('result');
   }  
   return $result;
}

function isdate_change_format( $string )
{
  $matches = array();
  $pattern = '/^([0-9]{4})\-([0-9]{1,2})\-([0-9]{1,2})$/';
  if ( !preg_match( $pattern, $string, $matches ) ) 
    return $string;
  else  
    return $matches[3]."-".$matches[2]."-".$matches[1];
}
function Get_Assigned_Datas( $Module , $user_id = 0 ){
  $CI         =& get_instance();
  $Module_ids = [];

  //get all records to view all permission users
  if( Has_view_all_permission( $Module , $user_id ) ){
    $firm_id = $_SESSION['firm_id'];
    if($Module == "TASK"){
      $sql  = "SELECT id AS module_id 
              FROM add_new_task 
              WHERE firm_id = $firm_id";
      $data = $CI->db->query($sql)->result_array();  
      $Module_ids = array_column( $data , 'module_id' );
    }else{
      if($user_id){
        $ids = $CI->db->query( "select id from organisation_tree where user_id=".$user_id )->result_array();
 
        foreach ($ids as $key => $value){
          $data       = $CI->db->query("select module_id from firm_assignees where module_name='".$Module."' and (CONCAT(':',assignees,':') REGEXP ':".$value['id'].":$') ")->result_array();
          $Module_ids = array_merge( $Module_ids , array_column( $data , 'module_id' ) );
        }
      }
      else{
        $sql  = "SELECT module_id 
              FROM firm_assignees 
              WHERE module_name = '$Module' 
              AND firm_id = $firm_id";
        $data = $CI->db->query($sql)->result_array();
        $Module_ids = array_column( $data , 'module_id' );
      }
      
    }                  
  }else{
    $Module_ids = get_specific_assignee_module_data( $Module , $user_id );
  }
  return array_unique( $Module_ids );
}

function get_user_filtered_tasks( $Module , $user_id = 0 ){
  $Module_ids = get_specific_assignee_module_data( $Module , $user_id );
  return array_unique( $Module_ids );
}

function get_user_filtered_tasks_7days( $Module , $user_id = 0 ){
  $CI         =& get_instance();
  $Module_ids = get_specific_assignee_module_data_7days( $Module , $user_id );

  if(!empty($Module_ids)){ 
    $sql        = "SELECT ANT.id AS ID,
                          ANT.subject AS SUBJECT,
                          ANT.start_date AS START_DATE,
                          ANT.end_date AS END_DATE 
                  FROM add_new_task AS ANT
                  WHERE ANT.id IN(".implode(',', $Module_ids).")
                  AND ANT.start_date >= DATE(NOW()) - INTERVAL 7 DAY 
                  AND ANT.start_date <= DATE(NOW())
                  AND ANT.related_to != 'sub_task' 
                  AND ANT.task_status in (1,2,3)
                  ORDER BY ANT.start_date DESC";
    $tasks      = $CI->db->query($sql)->result_array();
  }else{
    $tasks      = [];
  }
  return $tasks;
}

function Get_Module_Assigees( $Module , $Module_id )
{
  $CI =& get_instance();
  $result_val=[];
  $Return = [];
  $data = $CI->db->query("select assignees from firm_assignees where sub_module_name='' and module_name='".$Module."' and module_id=".$Module_id)->result_array();
  $data = array_column($data,'assignees');

     foreach ($data as $key => $sv_value) {
                $tmp=explode(":",$sv_value);
                     $result_val[] = end($tmp);
                         }
  // $data =explode(':', implode(':',$data) );
   $data = implode( ',' , $result_val );
  if( !empty( $data ) )
  {
    $Return = $CI->db->query("select user_id from organisation_tree where id IN (".$data.") ")->result_array();
    $Return = array_unique( array_column( $Return,'user_id') );
  }
  return $Return;
}
//used to get final assignee (ticked assignees)
function get_specific_module_data_assignee( $Module , $Module_id )
{
  $CI =& get_instance();
  $Return = [];
  $data = $CI->db->query("select assignees from firm_assignees   where module_name='".$Module."' and module_id=".$Module_id)->result_array();
 
  $data = array_map(function( $v ){
   
   return ( strrpos( $v , ':' ) !== FALSE ? substr( $v , strrpos($v, ':')+1 ): $v ); 
     
     } , array_column( $data , 'assignees' ) );

  $data = implode(',',$data);

  if(!empty($data))
  {
    $Return = $CI->db->query("select user_id from organisation_tree where id IN (".$data.") ")->result_array();
    $Return = array_unique( array_column( $Return,'user_id') );
  }
  return $Return;
}
//used to get assigned data to a user,assignee shoud ticked.
function get_specific_assignee_module_data( $Module , $user_id = 0 ){  
  $CI =& get_instance();
  
  if( !$user_id ){
    $user_id = $_SESSION['userId'];
  }

  $Module_ids = [];  
  $ids = $CI->db->query( "select id from organisation_tree where user_id=".$user_id )->result_array();
 
  foreach ($ids as $key => $value){
    $data       = $CI->db->query("select module_id from firm_assignees where module_name='".$Module."' and (CONCAT(':',assignees,':') REGEXP ':".$value['id'].":$') ")->result_array();
    $Module_ids = array_merge( $Module_ids , array_column( $data , 'module_id' ) );
  }
  return array_unique( $Module_ids );

  // $sql = "SELECT DISTINCT(FA.module_id)
  //         FROM organisation_tree AS OT
  //         INNER JOIN firm_assignees AS FA
  //             ON FA.module_name='$Module' 
  //                AND OT.id IN(REPLACE(FA.assignees, ':', ','))
  //         WHERE OT.user_id=$user_id";

  // $data = $CI->db->query($sql)->result_array();
  // return $data;
}

function stop_overtimes(){
  $CI           =& get_instance();
  $sql          = "SELECT *, (CHAR_LENGTH(ITT.time_start_pause) - CHAR_LENGTH(REPLACE(ITT.time_start_pause, ',', '')) + 1) as TotalValue
                  FROM individual_task_timer AS ITT
                  HAVING TotalValue % 2 = 1";  
  $overtimeArr  = $CI->db->query($sql)->result_array();   
  $overtimeIds  = [];

  foreach($overtimeArr as $ot){
    if(!empty($ot['time_start_pause'])){
      $time_start_pause = explode(',', $ot['time_start_pause']);
      $last_entry       = end($time_start_pause);
      $diff             = timestamp_diff($last_entry, time());
      
      if( $diff['year'] > 0 || $diff['month'] > 0 || $diff['day'] > 0 || $diff['hour'] > 7 || ($diff['hour'] == 7 && $diff['minuite'] >= 30) ){
        $overtimeIds[] = $ot['id'];
      }    
    }
  }

  if(!empty($overtimeIds)){
    $overtime_ids = implode(',', $overtimeIds);
    $sql          = "UPDATE individual_task_timer 
                    SET time_start_pause = concat(ifnull(time_start_pause,''), ',".time()."')
                    WHERE id IN($overtime_ids)";
    return $CI->db->query($sql);
  }else{
    return false;
  }
}

function timestamp_diff($ts1, $ts2){
  $datetime1 = new DateTime(date('Y-m-d h:i:s', (int)$ts1));//start time
  $datetime2 = new DateTime(date('Y-m-d h:i:s', (int)$ts2));//end time
  $interval  = $datetime1->diff($datetime2);
  $result    = [
    'year'    => $interval->format('%Y'),
    'month'   => $interval->format('%m'),
    'day'     => $interval->format('%d'),
    'hour'    => $interval->format('%h'),
    'minuite' => $interval->format('%i'),
    'second'  => $interval->format('%s')
  ];
  return $result;
}

function get_specific_assignee_module_data_7days( $Module , $user_id = 0 ){  
  $CI         =& get_instance();
  $Module_ids = [];
  
  if( !$user_id ){
    $user_id = $_SESSION['userId'];
  }
  $ids = $CI->db->query( "select id from organisation_tree where user_id=".$user_id )->result_array();
 
  foreach ($ids as $key => $value){
    $sql        = "SELECT module_id 
                  FROM firm_assignees 
                  WHERE module_name='".$Module."' 
                  AND (CONCAT(':',assignees,':') REGEXP ':".$value['id'].":$') 
                  AND NOT CONCAT(':', task_started, ':') REGEXP ':$user_id:'";
    $data       = $CI->db->query($sql)->result_array();
    $Module_ids = array_merge( $Module_ids , array_column( $data , 'module_id' ) );
  }
  return array_unique( $Module_ids );
}

function get_firm_page_length()
{
  $CI   =& get_instance();
  if( empty( $_SESSION['firm_page_length'] ) )
  {
    $data = $CI->db->select('page_length')
                   ->get_where('admin_setting' , 'firm_id='.$_SESSION['firm_id'])
                   ->row_array();
    $_SESSION['firm_page_length'] = (!empty( $data['page_length'] ) ? $data['page_length'] : 100);
  }

  return $_SESSION['firm_page_length'];                 
}

function delete_OrgChart_Node($id){ 
    $CI =& get_instance();

    if(!empty( $id )){
      /* Fetch row from organisation_tree where id = <given id> */
      $nodeData = $CI->db->query("select * from organisation_tree where firm_id=".$_SESSION['firm_id']." and id= ".$id)->row_array();
      /* As we are going to remove the specified user so we will now replace the parent_id of it's childs 
      with the parent_id of the specified user that we are going to delete. So the childs will be reassigned
      To there immidiate parent after delete. */
      $CI->db->update('organisation_tree', ['parent_id'=>$nodeData['parent_id']], "parent_id=".$nodeData['id']);
      /* Delete the entry for specified user from the organisation_tree (Hard delete) */
      $CI->db->query("delete from organisation_tree where id =".$id);
      /* Fetch all entries from firm_assignees table where the sepcified user is assigned */
      $data = $CI->db->query("select * from firm_assignees where  (CONCAT(':',assignees,':') REGEXP ':".$id.":') ")->result_array();

      foreach ($data as $key => $value){
        $assignee = explode(':', $value['assignees'] ); 
        $key      = array_search( $id , $assignee );    
                
        if(in_array($nodeData['parent_id'], $assignee)){
          /* Unset the user to unassign from the task */
          unset($assignee[$key] );                          
        }else{
          /* Replace the specified user_id with it's parent_id to reasign the task to it's immidiate parent */      
          $assignee[$key] = $nodeData['parent_id'];
        }
        $assignee = implode(':',$assignee);             
        /* Update to unassign the specified user from the task */
        $CI->db->update('firm_assignees', ['assignees'=>$assignee],"id=".$value['id']);
      }
      return  $CI->db->affected_rows();
    }
  }

function find_underlevel_users($role,$user_id)
  {	$break=0;
    do{
      if($break==1000)break;
      $CI =& get_instance();
      $data=$CI->db->query("select * from assign_member where assign_to_id IN ($user_id)")->result_array();
      //echo "<pre>";print_r($data);die;
      $t='';
      $atr='';
      foreach ($data as $v) 
      {
        if($role==($v['assign_to_roll']+1))
        {
          $t.=($t=='')?$v['members_id']:",".$v['members_id'];
        }
        else 
        {
          $atr.=($atr==''?$v['members_id']:",".$v['members_id']);
        }  
      }
      $user_id=$atr;
     $break++;
   }while($t=='');
  
  return $CI->db->query("select * from user where id IN ($t)")->result_array();
  }


function dateDiffrents( $D1 , $D2 )
{
    $d1 = date_create( $D1 );
    $d2 = date_create( $D2 );
    $interval=date_diff( $d1 ,$d2 );
          
    return intval( $interval->format('%R%a') );
}

function Change_Date_Format( $date , $C_formate = 'Y-m-d' , $R_formate = 'd-m-Y' )
{
  $date = date_create_from_format( $C_formate , trim ( $date ) );
  if( $date )
  {
    return date_format( $date , $R_formate );
  }
  return $date;

}
function Firm_column_settings($table)

{
  $CI =& get_instance();
  $firm_id = "firm".$_SESSION['firm_id'];
  
  $column_setting = $CI->db->query("select name,visible_".$firm_id.",order_".$firm_id.",cusname_".$firm_id." from firm_column_settings where table_name='".$table."' order by order_".$firm_id." ASC")->result_array();

    
  $hidden_coulmns = array_keys( array_column( $column_setting , 'visible_'.$firm_id , 'name' ) , 0 );

  $column_order = array_column( $column_setting , 'name' );

  $hidden_coulmns = array_map(function ($a){return trim( $a ).'_TH';}, $hidden_coulmns );

  $column_order = array_map(function ($a){return trim( $a ).'_TH';}, $column_order );

  $return['hidden'] = json_encode( $hidden_coulmns );

  $return['order'] = json_encode( $column_order );

  return $return;
}

function SuperAdmin_column_settings($table)

{
  $CI =& get_instance();

  
  $column_setting = $CI->db->query("select name,visible,orders,cusname from super_admin_column_settings where table_name='".$table."' order by orders ASC")->result_array();

    
  $hidden_coulmns = array_keys( array_column( $column_setting , 'visible' , 'name' ) , 0 );

  $column_order = array_column( $column_setting , 'name' );

  $hidden_coulmns = array_map(function ($a){return trim( $a ).'_TH';}, $hidden_coulmns );

  $column_order = array_map(function ($a){return trim( $a ).'_TH';}, $column_order );

  $return['hidden'] = json_encode( $hidden_coulmns );

  $return['order'] = json_encode( $column_order );

  return $return;
}

  
 
function getMenuURL( $id )
{ 
  $CI =& get_instance();
  
  $url="javascript::void(0)";

  if ( !empty( $id ) )
    {
      
      $url = $CI->db->query(" SELECT url FROM `module_mangement` where module_mangement_id = $id ")->row_array();
      
      if ( strpos( $url['url'] , "#" ) === 0 )        
      {
        $url = $url['url'];
      
      }
      else
      {
        $url = base_url().$url['url'];
      }

    }

    return $url;
}

function getHeader( $PARENT_LI , $CHILD_UL , $CHILD_LI )
{ 
  $CI =& get_instance();

  $menu_data = '';
  $Menus = GetMenus();
  foreach ($Menus as $datas)
  {
    
    $url =  getMenuURL( $datas['href'] );
    
    $ParentLi = str_replace('{URL}', $url , $PARENT_LI );  
    $ParentLi = str_replace('{ICON_PATH}', base_url().$datas['icon'] , $ParentLi );  
    $ParentLi = str_replace('{MENU_LABEL}',  $datas['text']  , $ParentLi );  

    $childCnt ='';

    if( isset( $datas['children'] ) )
    {
      $childCnt =  recursive_menu( $datas['children'] , $CHILD_UL , $CHILD_LI );
    }

//echo htmlspecialchars($ParentLi)."<br>";
//echo  htmlspecialchars($childCnt)."child<br>";

    $ParentLi = str_replace('{CHILD_CONTENT}', $childCnt , $ParentLi ); 




    $menu_data.=$ParentLi; 
  }

 // echo htmlspecialchars( $menu_data ); 

  return $menu_data;

}


function recursive_menu( $Child_menu , $CHILD_UL , $CHILD_LI )
{
  
  $CI =& get_instance();

   $url="javascript::void(0)";

   $menu_data = '';

  foreach ($Child_menu as $datas)
  {
                          
    $url = getMenuURL( $datas['href'] );

    $ChildLi = str_replace('{URL}', $url , $CHILD_LI );  
    $ChildLi = str_replace('{MENU_LABEL}',  $datas['text']  , $ChildLi );  

    $childCnt ='';

     if( isset( $datas['children'] ) )
    {
      $childCnt = recursive_menu( $datas['children'] , $CHILD_UL , $CHILD_LI );
    }

    $ChildLi = str_replace('{CHILD_CONTENT}', $childCnt , $ChildLi ); 

    $menu_data.= $ChildLi; 

  }
    
  $menu_data = str_replace('{CHILD_CONTENT}', $menu_data , $CHILD_UL );  
   
  return  $menu_data;


}

 
   function PermissionCheck( $data , $sectionSettings=0)
  { 
      $CI =& get_instance();
      $rolePermission = 1;

      if( !empty( $data['href'] ) )
      {
        $modualData = $CI->db->query("select * from module_mangement where module_mangement_id = ".$data['href'] )->row_array();
        
        $Module = trim( $modualData['module'] );

        $PermissionType = trim( $modualData['required_premission'] ); 

        $result = [];
        $needl = ",";

        if( substr_count( $PermissionType ,'||') )
        {
            $needl = "||";
        }
        else if( substr_count( $PermissionType ,'&&') )
        {
            $needl = "&&";
        }

        $PermissionType = explode( $needl , $PermissionType );              

        //print_r($modualData);
    

        foreach ($PermissionType as $perVal) 
        {
            $result[] = $_SESSION['permission'][ $Module ][ $perVal ]; 
        }

        $result = array_keys( $result , 1);

        if( ( $needl=="," || $needl=="||" ) && count($result) )
        {
            $rolePermission = 1;
        }
        else if ( $needl=="&&" &&  count($PermissionType) == count($result) )
        {
            $rolePermission = 1;                
        }
        else 
        {
            $rolePermission = 0;
        }

          //echo $Module."----".$rolePermission."---";print_r($result);
      }

      return $rolePermission;        
  }

  function subMenus($subMenus )
  {

      $CI =& get_instance();
      // $subMenus  = $CI->db->query("select * from menu_management where parent_id =$id ORDER BY menu_management_id ASC ")->result_array();   
   if( !empty($subMenus) )
      {           
          $pre = 0;
          $submenu  = [];
          foreach ($subMenus as $data)
          {
              $PermissionCheck = PermissionCheck( $data );

              if( !$PermissionCheck ) continue;
              $pre = 1;

              $result = subMenus( $data['children'] );

              if( !$result )
              {
                  $submenu[] = $data;
              }
              else if( $result!= 1 )
              {
                  $data['children'] = $result;
                  $submenu[] = $data; 
              }

          }
          
          if ($pre) return $submenu;
          else return 1;
      }
      else 
      {
          return 0;
      }
  }
  
  function GetMenus()
  {

      $CI =& get_instance();
      $getMenu  = $CI->db->query("select * from menu_manage where  firm_id = ".$_SESSION['firm_id']." ")->row_array();

      $mainMenus=json_decode($getMenu['menu_details'],true);


      // print_r($mainMenus);
      // exit();

      $menu = [];

      foreach($mainMenus as $key=> $data)
      {

          $PermissionCheck = PermissionCheck( $data );
          
          if ( !$PermissionCheck ) continue;

          $result = subMenus( $data['children']  );

          if( !$result ) 
          {
              $menu[] = $data;
          }
          else if( $result != 1 )
          {
              $data['children'] = $result;
              $menu[] = $data;
          }
          
      }
      return $menu;
      
  }

  function firm_mail_header($firm_id=false) 
  {
    if($firm_id == false)
    {
       $firm_id = $_SESSION['firm_id'];
    }
    
    $CI =& get_instance();
    $output = $CI->db->query('SELECT header1 FROM admin_setting WHERE firm_id = '.$firm_id.'')->row_array();
    return $output['header1'];
  }

  function firm_mail_footer($firm_id=false) 
  {
    if($firm_id == false)
    {
       $firm_id = $_SESSION['firm_id'];
    }

    $CI =& get_instance();
    $output = $CI->db->query('SELECT footer1 FROM admin_setting WHERE firm_id = '.$firm_id.'')->row_array();
    return $output['footer1'];
  }

  function email_header($firm_id) 
  {
    $CI =& get_instance();
    $output = $CI->db->query('SELECT header1 FROM admin_setting WHERE firm_id = '.$firm_id.'')->row_array();
    return $output['header1'];
  }

  function email_footer($firm_id) 
  {
    $CI =& get_instance();
    $output = $CI->db->query('SELECT footer1 FROM admin_setting WHERE firm_id = '.$firm_id.'')->row_array();
    return $output['footer1'];
  }



//Home page Value getting
if ( ! function_exists('page_content'))
{
    function page_content($var = '')
    { 
      if($var){
      $CI =& get_instance();
      return $CI->db->get_where('page_content',array('keyword' => $var))->row('output');
      }
      // return false;
    }   
}


if (!function_exists('dd')) 
{
  function dd(...$data)
  {
    foreach ($data as $value) {
      echo "<pre>";
      if (is_array($value) || is_object($value)) {
        print_r($value);
      } else {
        var_dump($value);
      }
      echo "</pre>";
    }
    die;
  }
}


?>
