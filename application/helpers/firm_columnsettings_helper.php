  
<?php       
 function Change_DataFormat($string) 
 {
    $string = trim( $string );
    $matches = array();
    $pattern = '/^([0-9]{4})\-([0-9]{1,2})\-([0-9]{1,2})$/';

    if ( !preg_match( $pattern, $string, $matches ) )
    {
      return  $string;
    }
    else 
    {
      $data  = $matches[3]."-".$matches[2]."-".$matches[1];
      return $data;
    }    
 }
function ClientColumn_ColVis_Content()
{
  $CI =& get_instance();
  $firmId = 0;
  
 /* $rows = $CI->db->query("select name,visible_firm".$firmId.",order_firm".$firmId.",cusname_firm".$firmId." from firm_column_settings where table_name ='client_list' order by order_firm".$firmId." asc
  ")->result_array();*/
  $rows = $CI->FirmFields_model->Get_FirmFields_Settings( $_SESSION['firm_id'] , 0);
  /*$CI->db->query("SELECT * FROM client_fields_management WHERE firm_id=".$_SESSION['firm_id']." AND cnt_visibility=1  ORDER BY list_page_order ASC")->result_array();*/

  $text = "<div class='colvis_column_list'><div class='close'>
        <button type='button' class='close' data-dismiss='modal'>×</button>
      </div><ul>";
  foreach ($rows as $key => $value)
  {
      $IfSelected = "";
      
      if( trim( $value["list_page_visibility"] ) == 1)
      {
        $IfSelected = "active";
        
      } 

      $text .= "<li data-const='".$value['field_propriety']."' class='".$IfSelected."'><span>".$value["label"]."</span></li>";
  }
  $text.="</ul></div>";

  return $text;
}

function ClientColumn_Status_SelectBox()
{
 /* $text = "<select multiple='true' searchable='true' class='filter_check' style='display:none'>
            <option value='1' >Active</option>
            <option value='|0|2' >Inactive</option>
            <option value='3' >Frozen</option>
            <option value='4' >Draft</option>
            <option value='5' >Archive</option>
          </select>";*/

          $return = [
            ['1' , 'Active' ],
            ['|0|2' , 'Inactive' ],
            ['3','Frozen'],
            ['4','Draft'],
            ['5','Archive']
          ];
          return $return;
}

function get_firm_type()
{
  $CI =& get_instance();
  $Company_types = $CI->Common_mdl->Company_types();
  
  $Company_types = array_change_key_case( $Company_types , CASE_UPPER );

  $Company_types = array_flip( $Company_types );

  $select = [];

  foreach ($Company_types as $key => $value) {
    $select[] = [ $key , $value ];
  }
  return $select;
}

function ClientColumn_FirmStaffs_SelectBox()
{
  $CI =&  get_instance();
  
  $CI->load->model('User_management');
  $staffs = $CI->User_management->Get_FirmStaffs();
  
  $return = [];
  
  foreach ($staffs as $value) {
    $return[] = [ $value['id'] , $value['crm_name'] ];
  }

  return $return;
}

function get_client_landline()
{
  $CI =& get_instance();
  
  $return = [];  
  $Assigned_Client = $CI->input->post('client_user_id');

  if( !empty( $Assigned_Client ) )
  {

    $client_landline = $CI->db->select("landline")
                            ->get_where( "client_contacts","landline!='' AND make_primary=1 AND client_id IN (".$Assigned_Client.")" )
                            ->result_array();

    $client_landline = array_column( $client_landline , "landline" );


    foreach ($client_landline as $value) {

      $value_array   = json_decode( $value , true );

      foreach ($value_array as $v) {

        $return[] = [ $v , $v ];
      }
    }    
  }
  
  return $return;
}
function get_client_work_email()
{
  $CI =& get_instance();
  
  $return = [];  
  $Assigned_Client = $CI->input->post('client_user_id');

  if( !empty( $Assigned_Client ) )
  {

    $client_landline = $CI->db->select("work_email")
                            ->get_where( "client_contacts","work_email!='' AND make_primary=1 AND client_id IN (".$Assigned_Client.")" )
                            ->result_array();

    $client_work_email = array_column( $client_landline , "work_email" );


    foreach ($client_work_email as $value) {

      $value_array   = json_decode( $value , true );

      foreach ($value_array as $v) {

        $return[] = [ $v , $v ];
      }
    }    
  }
  
  return $return;
}
function ClientColumn_landlines( $id )
{ 
  $CI =&  get_instance();

  $contact_data = $CI->db->select("pre_landline,landline")
                  ->get_where( 'client_contacts' , "client_id=".$id." and make_primary=1" ) 
                  ->row_array(); 

  $type   = json_decode( $contact_data['pre_landline'] , true );
  $number = json_decode( $contact_data['landline'] , true );
  
  $text   = [];
  if( !empty( $type )  && !empty( $number ) )
  {
    foreach ($number as $key => $value)
    {
      $text[] = $value;
    }
  }
  $text = implode(',',$text);
  return $text;
}
function ClientColumn_work_emails( $id ) 
{ 
  $CI =&  get_instance();

  $contact_data = $CI->db->select("work_email")
                  ->get_where( 'client_contacts' , "client_id=".$id." and make_primary=1" ) 
                  ->row_array(); 
  $text = "";
  $work_email   = json_decode( $contact_data['work_email'] , true );
  
  if( !empty( $work_email ) ) $text = implode(',',$work_email);

  return $text;
}
function ClientColumn_addBaseUrl( $id ) 
{
  //echo $Client_Data['proof_attach_file']."attachment";
  $CI =&  get_instance();

  $Client_Data  = $CI->db->select("proof_attach_file")
                  ->get_where( 'client' , 'user_id='.$id )
                  ->row_array();

  $data = explode( ',', $Client_Data['proof_attach_file'] );

  $search_text = [];
  $tag         = [];
  if( !empty( $data ) )
  {
    $data = array_filter( $data );
    //print_r($data);echo "array";
    foreach ($data as $key => $value)
    {

      $file_name = explode('/',$value);
      $file_name = end($file_name);
      $tag[] = "<a href='".base_url().$value."' traget='_blank'>".$file_name."</a>";
      $search_text[] = $file_name;

    }
  }
  return implode(',', $tag );
}
function add_CH_status( $CH_status )
{
  $CI =&  get_instance();

  $return = '';

  if( ! empty( $CH_status ) )
  {
    $color_code = ['active'=>'green','dissolved'=>'red'];
    $color = '#00a2e8';
    if ( isset( $color_code[ $CH_status ] ) )
      $color  = $color_code[ $CH_status ];

    $return = "<b style='color:".$color."'> (".ucfirst( $CH_status ).")</b>";     
  }
  return $return;
}
function ClientColumn_Client( $id ) 
{
  $CI =& get_instance();
  
  $User_data    = $CI->db->select("crm_name,id")
                  ->get_where( 'user' , 'id='.$id )
                  ->row_array();

  $Client_Data  = $CI->db->select("crm_company_name,crm_ch_status,id")
                  ->get_where( 'client' , 'user_id='.$id )
                  ->row_array();

  $contact_data = $CI->db->select("main_email")
                  ->get_where( 'client_contacts' , "client_id=".$id." and make_primary=1" ) 
                  ->row_array();                  


  $data = ( $Client_Data['crm_company_name'] !='' ? $Client_Data['crm_company_name'] : '' );

  $data = trim( $data );

  $status_label = add_CH_status( trim ( $Client_Data['crm_ch_status'] ) );

  $text = "
            <a href='".base_url()."client/client_infomation/".$User_data['id']."' target='_blank'>".$data.'</a>'.$status_label;

  $result=$CI->Common_mdl->empty_field_find( $User_data['id'] ); 

  if(  count($result) >= 1 )
  {

    $text .="<a href='javascript:;' class='missing_info' style=''>
              <i class='fa fa-info-circle' aria-hidden='true' style='color:red'></i>
            </a>
            <div class='missing_details' style='color:red; display: none;'>";
    
    foreach($result as $res)
    {
      $text .='<div class="missing_section"> Missing: '.$res.'</div>';
    }
    
    $mail = (!empty( $contact_data['main_email'] ) ? $contact_data['main_email'] : '');
    
    $text .= "<input type='hidden' class='missing_details_email' value='".$mail."'><input type='hidden' class='missing_details_client_id' value='".$Client_Data['id']."'>";

    $text .= "<textarea  class='missing_details_content' style='display:none;'>";
   
    $i=1;
    
    $text .= "Following Details Are Missing<br/>";

    foreach($result as $res)
    {
      $text .= "<b>".$i.") ". $res."</b></br>";
      $i++;
    }
    $text .= "</textarea><button type='button' class='close misclose' data-dismiss='modal'>×</button>";

    $query=$CI->db->query('SELECT firm_id FROM admin_setting WHERE firm_id="'.$_SESSION['firm_id'].'" AND  missing_details=1')->num_rows();
    if($query && $mail!='')
    {  
          $text .="<div class='mailsentcls'>
            <a href='javascript:;' class='Open_MissingMailCompos'>Send</a>
          </div>";
    }
    $text.="</div>";
  }

  return $text;
}


function ClientColumn_Type ( $id ) 
{
  $CI =& get_instance();

  $Client_Data  = $CI->db->select("crm_legal_form")
                  ->get_where( 'client' , 'user_id='.$id )
                  ->row_array();

  /*$Company_types = array('LTD' => 'Private Limited company', 'PLTD' => 'Public Limited company', 'LLP' => 'Limited Liability Partnership', 'P`ship' => 'Partnership', 'S.A'=>'Self Assessment','TT'=>'Trust','Charity'=>'Charity','other'=>'Other');*/
  
  $Company_types = $CI->Common_mdl->Company_types();

  $key = strtoupper( array_search(  $Client_Data['crm_legal_form'] , $Company_types) );

   return $key;
}
function ClientColumn_Status ( $id ) 
{
  $CI =& get_instance();
  
  $User_data    = $CI->db->select("status,id")
                  ->get_where( 'user' , 'id='.$id )
                  ->row_array();

  //if status empty
  if( $User_data['status'] == '' ) $User_data['status'] = 0;

  $status = [ ['Inactive',''] , ['Active',''] ,['Inactive',''], ['Frozen',''] , ['Draft',''] , ['Archive',''] ];

  $status[$User_data['status']][1] = 'selected="selected"';

  $HasPermission = ($_SESSION['permission']['Client_Section']['edit']!=0 ? true :false);

  $text = "<select name='status".$User_data['id']."' id='status".$User_data['id']."' class='client_status' data-id='".$User_data['id']."'" ;

  if($User_data['status']=='4' || !$HasPermission ) $text .= "disabled='disabled'";

   $text .= "><option value='0' ".$status[0][1].">Inactive</option>
   <option value='1' ".$status[1][1].">Active</option>
   <option value='3' ".$status[3][1].">Frozen</option>";
                                            
  if($User_data['status']=='4') $text .="<option value='4' ".$status[4][1].">Draft</option>";
                                                         
  $text .= "<option value='5' ".$status[5][1].">Archive</option></select>";

  return $text;
}
function  ClientColumn_staff_manager ( $User_data , $Client_Data , $contact_data ) 
{
  $CI =& get_instance();

  $staff = $CI->Common_mdl->GetAllWithWhere('responsible_user','client_id',$User_data['id']);

    $staffs = array_filter( array_column( $staff , 'manager_reviewer') ); 
    
    if(!empty($staffs)) 
    {
    
    
    $staff1=implode(',', $staffs);

    $staff_form = $CI->db->query('select first_name from staff_form where user_id in ('.$staff1.')')->result_array();
  
    $val = array_filter( array_column( $staff_form , 'first_name') ); 


    if(!empty($val))
    {
      $staff2=implode(',', $val);
      $val=array();
    } 
    else
    {
      $staff2='';
    }
    
    }
    else
    {
      $staff2='';
    }
    return $staff2;
}

 
function  ClientColumn_staff_managed ( $User_data , $Client_Data , $contact_data ) 
{
        $CI =& get_instance();

  $staff = $CI->Common_mdl->GetAllWithWhere('responsible_user','client_id',$User_data['id']);

                                                        
    $staffs = array_filter( array_column( $staff , 'assign_managed') ); 


    if(!empty($staffs)) 
    {

    //  echo '<pre>'; print_r($staffs); 
    $staff2=implode(',',$staffs);

    
    $managed = $CI->db->query('select crm_name from user where id in ('.$staff2.')')->result_array();

    $val = array_filter( array_column( $managed , 'crm_name') ); 
    

      if(!empty($val))
      {
        $staff4=implode(',', $val);
      } 
      else 
      {
        $staff4='';
      }
    }
    else
    {
     $staff4='';
    }
    
  return $staff4;
}

function ClientColumn_team ( $User_data , $Client_Data , $contact_data ) 
{
        $CI =& get_instance();


   $team=$CI->Common_mdl->GetAllWithWhere('responsible_team','client_id',$User_data['id']);
   $teams = array_filter( array_column( $team, 'team' ) );
      
      if(!empty($teams)) 
      {
        $team1=implode(',', $teams);
        
        $teamed = $CI->db->query('select team from team where id in ('.$team1.')')->result_array();
        
        $val = array_filter( array_column( $teamed, 'team' ) );

        if(!empty($val))
        {
          $team2=implode(',', $val);
        } 
        else
        {
          $team2='';
        }
      } 
      else
      {
        $team2='';
      }
                                           
   return $team2;

}

function ClientColumn_team_allocation( $User_data , $Client_Data , $contact_data ) 
{
        $CI =& get_instance();

   $team=$CI->Common_mdl->GetAllWithWhere('responsible_team','client_id',$User_data['id']);
   $team3 = "";
    if(is_array($team) && count($team) > 0)
    {
      $teams = array_filter( array_column( $team , 'allocation_holder') );

      if(!empty($teams))
      {
        $team3=implode(',', $teams);
      }
      else
      {
        $team3='';
      }

    }
    
    return $team3;

}
function ClientColumn_department( $User_data , $Client_Data , $contact_data )  

{
        $CI =& get_instance();

   $department=$CI->Common_mdl->GetAllWithWhere('responsible_department','client_id',$User_data['id']);
   $depart1="";
    if(is_array($department) && count($department) > 0)
    {   
      $departs = array_column( $department , 'depart' );
    }
    
    if(!empty($departs))
    {
      $depart1=implode(',', $departs);
    } 
    else
    {
      $depart1='';
    }

  return $depart1;

}

function ClientColumn_department_allocation( $User_data , $Client_Data , $contact_data )  
{
          $CI =& get_instance();
          $depart3="";
   $department=$CI->Common_mdl->GetAllWithWhere('responsible_department','client_id',$User_data['id']);

    if(is_array($department) && count($department) > 0)
    {
      $departs=array_column($department,'allocation_holder');
    } 
    if(!empty($departs))
    {
    $depart3=implode(',', $departs);
    
    } else {
      $depart3='';
    }

    return $depart3;
}
                                                            
function ClientColumn_member_manager( $User_data , $Client_Data , $contact_data )
{
        $CI =& get_instance();
        $member1="";
  $member=$CI->Common_mdl->GetAllWithWhere('responsible_members','client_id',$User_data['id']);

  if(is_array($member) && count($member) > 0)
  {                                        
    foreach ($member as $key => $value) {
    $members[]=$value['manager_reviewer'];
    }
  }
  
  if(!empty($members))
  {
    $member1=implode(',', $members);
  }
  else
  {
    $member1='';
  }

return $member1;

}                                                                                                   



function ClientColumn_member_managed( $User_data , $Client_Data , $contact_data )
{
        $CI =& get_instance();

  $member=$CI->Common_mdl->GetAllWithWhere('responsible_members','client_id',$User_data['id']);
  $member2="";
  if(is_array($member) && count($member) > 0)
  { 
      foreach ($member as $key => $value)
      {
      $members[]=$value['assign_managed'];
      }
  }
  if(!empty($members))  
  {
    $member2=implode(',', $members);
  }
  else
  {
    $member2='';
  }
  return $member2;

}

function ClientColumn_client_AddtedFrom( $User_data , $Client_Data , $contact_data )
{
  $CI =&  get_instance();

  $source = ['Manual','Company House','Import','From Leads'];

  if(isset($source[ $Client_Data['status'] ]))
  {
  return $source[ $Client_Data['status'] ];
  }
  else
  {
  return "";
  }
} 

function ClientColumn_created_date( $id )
{   
  $CI =&  get_instance();

    $User_data  = $CI->db->select("CreatedTime")
                          ->get_where( 'user' , 'id='.$id )
                          ->row_array();

    $date = date('d-m-Y',trim($User_data['CreatedTime']) );;

    return $date;  
}
function ClientColumn_assignees( $id )
{
  $CI =& get_instance();

  $Client_Data  = $CI->db->select("id")
                  ->get_where( 'client' , 'user_id='.$id )
                  ->row_array();


  $assignees = get_specific_module_data_assignee( 'CLIENT' , $Client_Data['id'] );
  $assignees = array_unique( $assignees );

  foreach ( $assignees as $key => $user_id ) {

    $user_name  = $CI->db->select("crm_name")
                     ->get_where( 'user' , 'id='.$user_id )
                     ->row('crm_name');

    $assignees[ $key ]  = $user_name;
  }

  if( !empty( $assignees ) )
  {
    $assignees = implode(',' , $assignees );
    
    return $assignees;
  }
} 

function ClientColumn_check_CS_ch_overdue(  $id  )
{ 
  $CI =&  get_instance();

  $Client_Data  = $CI->db->select("crm_confirmation_statement_due_date,is_ch_cs_overdue")
                  ->get_where( 'client' , 'user_id='.$id )
                  ->row_array();

  $lable =  Change_Date_Format( $Client_Data['crm_confirmation_statement_due_date'] );
  $lable = "<span class='date-overdue' style='position:relative'>" . "<a class='client_cs_acc_dates' style='display:none'>". "Label" .'</a>' . $lable ."</span>";

  if( $Client_Data['is_ch_cs_overdue'] ) 
  {
    $lable = "<b class='date-overdue' style='color:red; position:relative'>" . "<a class='client_cs_acc_dates' style='display:none'>". "Label" .'</a>' . $lable ." - Overdue</b>";
  }
  
  return ( $lable ? $lable :'' );
}
function ClientColumn_check_Acc_ch_overdue( $id  )
{ 
  $CI =&  get_instance();

  $Client_Data  = $CI->db->select("crm_ch_accounts_next_due,is_ch_accounts_overdue")
                  ->get_where( 'client' , 'user_id='.$id )
                  ->row_array();

  $lable = $due_date = Change_Date_Format( $Client_Data['crm_ch_accounts_next_due'] );
  $lable = "<span class='date-overdue' style='position:relative'>" . "<a class='client_cs_acc_dates' style='display:none'>". "Label" .'</a>' . $due_date ."</span>";

  if( $Client_Data['is_ch_accounts_overdue'] ) 
  {
    $lable = "<b class='date-overdue' style='color:red; position:relative'>" . "<a class='client_cs_acc_dates' style='display:none'>". "Label" .'</a>' . $due_date ." - Overdue</b>";
  }

  return ( $lable ? $lable :'' );
}

function action_buttons( $id )
{ 
  $CI =&  get_instance();

  $User_data  = $CI->db->select("status")
                        ->get_where( 'user' , 'id='.$id )
                        ->row_array();
  $btn_html = '<div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <span class="dropdwn-action"></span><span class="dropdwn-action"></span><span class="dropdwn-action"></span>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
  if( $_SESSION['permission']['Client_Section']['edit']!=0 )
  {
    $Is_Disabled = ($User_data['status'] == 5?'disabled':'');
    
    $btn_html .= "<a href='javascript:void(0)' data-id='".$id."' class='ArchiveClient ".$Is_Disabled."' title='Archive'><i class='fa fa-file-archive-o' aria-hidden='true'></i></a>
    <a href='".base_url()."Client/addnewclient/".$id."' title='Edit'><i class='icofont icofont-edit' aria-hidden='true'></i>
    </a>";
  }

  if( $_SESSION['permission']['Client_Section']['delete']!=0 )
  {
    $btn_html .= "<a href='javascript:void(0)' data-toggle='modal' data-target='#deleteconfirmation' data-id='".$id."' class='DeleteClient' title='Delete'>
    <i class='icofont icofont-ui-delete' aria-hidden='true' ></i>
    </a>";
  }

  if($_SESSION['permission']['Task']['create'] != '0')
  {
    $client_data = $CI->db->select("id")->get_where('client' , 'user_id='.$id)->row_array();  

    $btn_html .= "<a href='javascript:void(0);' data-id='".$client_data['id']."' class='AddTask' title='Add'><i class='fa fa-plus-square-o' aria-hidden='true'></i></a>";
  }  
  $btn_html .='</div></div>';

return $btn_html;

  // $text = '<p class="action_01 sss">';

  

  // $text .= "</p>";
  // return $text;
}
function select_row( $id )
{ 
  $CI =&  get_instance();

  return '<label class="custom_checkbox1">
      <input type="checkbox" class="select_client" id="select_client_one" value="'.$id.'"><i></i>
    </label>';
}

?>
    