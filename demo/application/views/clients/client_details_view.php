<?php 
  $this->load->view('includes/header');
  $uri_seg = ( !empty( $client['user_id'] ) ? $client['user_id'] : $this->uri->segment(3) );
?>
<link href="<?php echo base_url();?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/timepicki.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree_select/style.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<style type="text/css">
   .disabled{
   opacity: 0.5;
   pointer-events: none;    
   }
/** 28-08-2018 for hide edit option on company house basic detailes**/
   .edit-button-confim
   {
       display: none;
   }
/** end of 28-08-2018 **/

   li.show-block
   {
      display: none;
      float: left;
      width: 100%;
   }
   .inner-views
   {
      display: block;
   }
   .make_primary_section.active
   {
      background: #4dad2f;
   }
  .sp-replacer 
  {
  width: 275px;
  border-radius: 50px;
  background: #fff;
 }
 .sp-preview
 {
  width: 230px;
  height: 23px;    
 }
 
 .sp-preview, .sp-alpha, .sp-thumb-el
 {
  background-image: none;
  margin: 2px 0px 2px 10px;  
 }

  .sp-dd
  {
    background: url('<?php echo base_url()?>assets/images/new_update1.png');
    background-repeat: no-repeat;
    width: 10px;
    margin: 10px 0px 0px 5px;
  }
  .Service_Assignee_Table td span.field-error
  {
        padding-left: 15px !important;
  }
  /*for assignee dropdown*/
  .comboTreeDropDownContainer
  {  
    max-height: 200px;
  }
</style>
<div class="modal-alertsuccess  alert alert-success-reminder succs" style="display: none;">
  <div class="newupdate_alert">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="pop-realted1"> 
      <div class="position-alert1 message">Reminder Added Successfully.</div>
  </div>
</div>
</div>

<div class="modal-alertsuccess alert alert-success" style="display:none;">
  <div class="newupdate_alert"> <a href="javascript:;" class="close alert_close"  aria-label="close">x</a>
   <div class="pop-realted1">
      <div class="position-alert1 sample_check">
         YOU HAVE SUCCESSFULLY UPDATED CLIENT 
         <div class="alertclsnew">
            <a href="<?php echo base_url('Client/addnewclient'); ?>"> New Client</a>
            <a href="<?php echo base_url(); ?>user"> Go Back</a>
         </div>
      </div>
   </div>
  </div>
</div>

<div class="succs"></div>
<!--  <div class="modal-alertsuccess  alert alert-success succs" style="display:none;">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><div class="pop-realted1">
   <div class="position-alert1">
     Your client was updated successfully.
     </div></div></div>
     
    <div class="modal-alertsuccess  alert alert-danger" style="display:none;">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <div class="pop-realted1">
   <div class="position-alert1">
   Please fill the required fields.
   </div></div></div> -->
<section class="client-details-view hidden-user01 floating_set card-removes pcoded-content " style="padding-bottom: 80px !important;">
   <!-- <h2><i class="fa fa-users fa-6" aria-hidden="true"></i> Client's Information</h2> -->
   <!-- <div class="text-right select-box-option1">
      <div class="form-group">
      <label>Client</label> <select><option>Accotax london ltd</option></select></div>
      </div>   -->
   <form id="insert_form" class="required-save client-firm-info1 validation" method="post" action="" enctype="multipart/form-data">
      <div class="information-tab main-body clientredesign floating_set">
         <div class="space-required">
            <div class="deadline-crm1 floating_set">
              <!--  <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                  <li class="nav-item">
                     <a class="nav-link active" href="<?php echo base_url().'user';?>"><img class="themicon" src="<?php echo base_url();?>assets/images/tabicon1.png" alt="themeicon" /> All Clients</a>
                     <div class="slide"></div>
                  </li> -->
                  <!--  <li class="nav-item">
                     <a class="nav-link" href="<?php echo base_url().'client/addnewclient';?>">Add new client</a>
                     <div class="slide"></div>
                     </li> -->
                  <!-- <li class="nav-item">
                     <a class="nav-link" href="<?php echo base_url().'user';?>">Email all</a>
                     <div class="slide"></div>
                     </li>
                     <li class="nav-item">
                     <a class="nav-link" href="<?php echo base_url().'user';?>">Give feedback</a>
                     <div class="slide"></div>
                     </li> -->
               <!-- </ul> -->

                <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                  <?php
                    if( $_SESSION['permission']['Client_Section']['view']!=0 )
                    {

                      ?>
                       <li class="nav-item">
                          <a class="nav-link" href="<?php echo base_url().'user';?>">
                          <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon1.png" alt="themeicon" />
                       All Clients</a>
                          <div class="slide"></div>
                       </li>
                    <?php 
                      
                    }
                    
                   ?>
                   <li class="nav-item">
                      <a class="nav-link active" href="#addnewclient">
                         <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon2.png" alt="themeicon" /><?php echo (!empty($uri_seg)?'Edit Client':'New Client');?>
                       </a>
                      <div class="slide"></div>
                   </li> 
                </ul>
               <div class="Footer common-clienttab pull-right">

               <!-- <?php if($_SESSION['permission']['Client_Section']['edit']=='1'){ ?> 
              <?php } ?> -->
                     <div class="change-client-bts1">
                        <input type="submit" class="signed-change1 sv_ex" id="save_exit" value ="<?php echo (!empty($uri_seg)?'Update':'Save');?>" name="submit">
                     </div>
                     <div class="divleft">
                        <button  class="signed-change2"  type="button" value="Previous Tab" text="Previous Tab" style="display: none;">Previous 
                        </button>
                     </div>
                     <div class="divright">
                        <button  class="signed-change3"  type="button" value="Next Tab"  text="Next Tab">Next
                        </button>
                     </div>
                  </div>
            </div>
         </div>
         <div class="space-required">
            <div class="document-center client-infom-1 floating_set">
               <div class="Companies_House floating_set">
                  <div class="pull-left">
                  <?php 
                        /*$cmy_role = $client['status'];
                        //echo $cmy_role;
                        if($cmy_role == 0) {
                          $cmy_status = 'Manual'; 
                        } else if($cmy_role == 1) {
                          $cmy_status = 'Company House';
                        }else if($cmy_role==2)
                        {
                          $cmy_status = 'Import';
                        }
                         else if($cmy_role==3){
                          $cmy_status = 'From Lead';
                        }*/ 

                      if(!empty($client['crm_company_name']))
                      { 
                        $client_name = $client['crm_company_name'];
                      }
                      else
                      {
                        $client_name =  "Add Client";//$this->Common_mdl->get_crm_name($client['user_id']);
                      }
                      echo '<h2>'.$client_name.'</h2>';  ?>    
                  </div>
                  
               </div>
               <div class="addnewclient_pages1 floating_set bg_new1">
                  <ol class="CLIENT_CONTENT_TAB nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
                    <!--  <li class="nav-item it0" value="0"><a class="nav-link active" data-toggle="tab" href="#required_information">Required information</a></li> -->                                            
                    <li class="nav-item it0" value="0">
                       <a class="nav-link active" data-id="#required_information" href="javascript:void(0);">Required information</a>
                    </li>
                    
                    <li class="nav-item it18 basic_details_tab" value="18">
                       <a class="nav-link basic_deatils_id" data-id="#details" href="javascript:void(0);"> Basic Details</a>
                    </li>

                    <li class="nav-item it1 for_contact_tab " value="1">
                       <a class="nav-link main_contacttab" data-id="#contacts1" href="javascript:void(0);">Contact</a>
                    </li>
                    <li class="nav-item it2 hide" value="2">
                       <a class="nav-link" data-id="#service" href="javascript:void(0);"> Service</a>
                    </li>
                    <li class="nav-item it2 hide" value="2">
                       <a class="nav-link" data-id="#assignto" href="javascript:void(0);"> Assign to</a>
                    </li>
                    <li class="nav-item it2 hide" value="2">
                       <a class="nav-link" data-id="#amlchecks" href="javascript:void(0);"> AML Checks</a></li>
                    <li class="nav-item it2 hide business-info-tab" value="2" style="display: none;" >
                       <a class="nav-link" data-id="#business-det" href="javascript:void(0);"> Business Details</a>
                    </li>
                    <li class="nav-item it10 other_tabs" value="10">
                       <a class="nav-link" data-id="#other" href="javascript:void(0);"> <span id="other_tab_cnt"></span><span id="old_cnt"></span> Other</a>
                    </li>
                    <!-- for 30-08-2018 -->
                    <li class="nav-item it10" value="10">
                       <a class="nav-link" data-id="#referral" href="javascript:void(0);">Referral</a>
                    </li>

              

                     <?php 

                     $fill = ['conf_statement','accounts','company_tax_return','personal_tax_return','payroll','workplace','vat','cis','cissub','p11d','bookkeep','management','investgate','registered','taxinvest','taxadvice'];

                     $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

                     foreach( $other_services as $key => $value ) 
                     { 
                        $fill[] = $value['services_subnames'];
                     }

                      $tab_on = array_fill_keys( $fill , ["tab"=>0,"reminder"=>0,"text"=>0,"invoice"=>0] );

                      if(!empty($client))
                      {
                        $tab_on = array_intersect_key( $client , $tab_on );
                        function jsonToArray($v)
                        {
                          $return  = ["tab"=>0,"reminder"=>0,"text"=>0,"invoice"=>0];
                           
                          if( gettype($v) == "string" &&  !empty( json_decode( $v  , true ) ) )
                          {
                            $return  = array_replace($return, json_decode( $v  , true ) );
                          }
                          return $return;
                        }
                        $tab_on = array_map('jsonToArray', $tab_on);
                      }
 
                      $import_tab_on  = "display:none";
                      


                      if( in_array("on", array_column( $tab_on , 'tab' ),TRUE ) ) $import_tab_on = "display:block";
                      else if( in_array( 1, array_column( $contactRec , 'has_ptr_service' ) ) ) $import_tab_on = "display:block";

                        ?>
                     <li class="nav-item" id="import_tab_li" style="<?php echo $import_tab_on; ?>">
                       <a class="nav-link" data-id="#import_tab" href="javascript:void(0);"> Important Information</a>
                    </li>
                  </ol>
               </div>
               <!-- tab close --> 
            </div>
         </div>
         <?php
            $rel_id=( !empty($client) ? $client['user_id'] : false);       
            ?>
      <!-- <?php if($_SESSION['permission']['Client_Section']['edit']!='1'){?> permission_deined <?php } ?> --> 
      <div class="newupdate_design">    
         <div class="management_section client-details-tab01 data-mang-06  floating_set realign newmanage-animation">
            <div class="tab-content card">
               <div id="detailss" class="tab-pane fade">
                  <div class="search-client-details01 floating_set">
                     <div class="pull-left detail-pull-left">
                        <span>Details</span>
                     </div>
                     <div class="pull-right edit-tag1"> 
                        <a href="#"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                     </div>
                  </div>
                  <div id="accordion" role="tablist" aria-multiselectable="true">
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Basic Info</a>
                              </h3>
                           </div>
                           <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                              <div class="basic-info-client1">
                                 <table class="client-detail-table01">
                                    <tr>
                                       <th>Client ID</th>
                                       <td><?php if(isset($user['client_id'])){ echo $user['client_id']; } ?></td>
                                       <th>Account Office Reference</th>
                                       <td><?php if(isset($client['crm_accounts_office_reference'])){ echo $client['crm_accounts_office_reference']; } ?></td>
                                    </tr>
                                    <tr>
                                       <th>Client Type</th>
                                       <td><?php if(isset($client['crm_company_type'])){ echo $client['crm_company_type']; } ?></td>
                                       <th>PAYE Reference Number</th>
                                       <td><?php if(isset($client['crm_employers_reference'])){ echo $client['crm_employers_reference']; } ?></td>
                                    </tr>
                                    <tr>
                                       <th>Company Name</th>
                                       <td><?php if(isset($client['crm_company_name'])){ echo $client['crm_company_name']; } ?></td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion1 -->
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingTwo">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">Address</a>
                              </h3>
                           </div>
                           <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                              <div class="basic-info-client1">
                                 <table class="client-detail-table01">
                                    <tr>
                                       <th>Address Line 1</th>
                                       <td><?php if(isset($company['registered_office_address']['address_line_1'])){ echo $company['registered_office_address']['address_line_1']; }  ?></td>
                                       <th>Post Code</th>
                                       <td><?php if(isset($company['registered_office_address']['postal_code'])){ echo $company['registered_office_address']['postal_code']; }  ?></td>
                                    </tr>
                                    <tr>
                                       <th>Address Line 2</th>
                                       <td><?php if(isset($company['registered_office_address']['address_line_2'])){ echo $company['registered_office_address']['address_line_2']; }  ?></td>
                                       <th>country</th>
                                       <td><?php if(isset($company['registered_office_address']['country'])){ echo $company['registered_office_address']['country']; }  ?></td>
                                    </tr>
                                    <tr>
                                       <th>Town/City</th>
                                       <td><?php if(isset($company['registered_office_address']['locality'])){ echo $company['registered_office_address']['locality']; }  ?></td>
                                       <th>Region</th>
                                       <td><?php if(isset($company['registered_office_address']['region'])){ echo $company['registered_office_address']['region']; }  ?></td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion1 -->
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Contact Info</a>
                              </h3>
                           </div>
                           <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                              <div class="basic-info-client1">
                                 <table class="client-detail-table01">
                                    <tr>
                                       <th>Phone</th>
                                       <td><?php if(isset($client['crm_telephone_number'])){ echo $client['crm_telephone_number']; } ?></td>
                                       <th>Website</th>
                                       <td><?php if(isset($client['crm_website'])){ echo $client['crm_website']; } ?></td>
                                    </tr>
                                    <tr>
                                       <th>Email</th>
                                       <td><?php if(isset($client['crm_email'])){ echo $client['crm_email']; } ?></td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion1 -->
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Business Info</a>
                              </h3>
                           </div>
                           <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                              <div class="basic-info-client1">
                                 <table class="client-detail-table01">
                                    <tr>
                                       <th>Business Start Date</th>
                                       <td><?php if(isset($company['date_of_creation'])){ echo $company['date_of_creation']; } ?></td>
                                       <th>VAT Scheme</th>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <th>Book Start Date</th>
                                       <td></td>
                                       <th>VAT Submit Type</th>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <th>Year End Date</th>
                                       <td><?php if(isset($company['accounts']['accounting_reference_date'])){ echo $company['accounts']['accounting_reference_date']['day'].'/'.$company['accounts']['accounting_reference_date']['month']; } ?></td>
                                       <th>VAT Reg.No</th>
                                       <td><?php if(isset($client['vat_number'])){ echo $client['vat_number']; } ?></td>
                                    </tr>
                                    <tr>
                                       <th>Company Reg.No</th>
                                       <td><?php if(isset($company['company_number'])){ echo $company['company_number']; } ?></td>
                                       <th>VAT Reg.Date</th>
                                       <td><?php if(isset($client['crm_vat_date_of_registration'])){ echo $client['crm_vat_date_of_registration']; } ?></td>
                                    </tr>
                                    <tr>
                                       <th>UTR No</th>
                                       <td><?php if(isset($client['crm_company_utr'])){ echo $client['crm_company_utr']; } ?></td>
                                       <th>Company Status</th>
                                       <td><?php if(isset($company['company_status'])){ echo $company['company_status']; } ?></td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion1 -->
                  </div>
               </div>
               <!-- required information tab open-->
               <div id="required_information" class="columns-two tab-pane fade in active">
                  <div class="accordion-panel">
                        <!-- <div class="accordion-heading" role="tab" id="headingOne">
                           <h3 class="card-title accordion-title">
                              <a class="accordion-msg">Required Information</a>
                           </h3>
                        </div> -->
                        <div class="space-required">
                            <div class="main-pane-border1 cmn-errors1" id="required_information_error">
                            </div>
                             <div class="alert-ss"></div>
                        </div>
                        <div id="collapse" class="panel-collapse">
                           <div class="basic-info-client1 CONTENT_REQUIRED_INFORMATION">
                              <div class="form-group row name_fields required_information_sort" data-id="<?php echo $Setting_Order['crm_company_name']; ?>">
                                 <label class="col-sm-4 col-form-label">
                                  <?php echo $Setting_Label['crm_company_name']; ?>
                                  <span class="Hilight_Required_Feilds">*</span>
                                  </label>
                                 <div class="col-sm-8">
                                    <input type="text" name="company_name" id="company_name" placeholder="" value="<?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?>" class="fields">
                                 </div>
                              </div>
                              <div class="form-group row name_fields required_information_sort" data-id="<?php echo $Setting_Order['crm_legal_form']; ?>" >
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_legal_form']; ?></label>
                                 <div class="col-sm-8">
                                    <select name="legal_form" class="form-control fields sel-legal-form" id="legal_form">
                                    <!-- <option value="">Select</option> -->
                                       <option value="Private Limited company" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Private Limited company') {?> selected="selected"<?php } ?> data-id="1">Private Limited company</option>
                                       <option value="Public Limited company" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Public Limited company') {?> selected="selected"<?php } ?> data-id="1">Public Limited company</option>
                                       <option value="Limited Liability Partnership" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Limited Liability Partnership') {?> selected="selected"<?php } ?> data-id="1">Limited Liability Partnership</option>
                                       <option value="Self Employed" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Self Employed') {?> selected="selected"<?php } ?> data-id="2">Self Employed</option>
                                       <option value="Partnership" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Partnership') {?> selected="selected"<?php } ?> data-id="2">Partnership</option>
                                       
                                       <option value="Landlords - UK" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Landlords - UK') {?> selected="selected"<?php } ?> data-id="2">Landlords - UK</option>

                                       <option value="Non Resident Landlord" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Non Resident Landlord') {?> selected="selected"<?php } ?> data-id="2">Non Resident Landlord</option>
                                       
                                       <option value="Overseas Company/Business" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Overseas Company/Business') {?> selected="selected"<?php } ?> data-id="2">Overseas Company/Business</option>

                                       <option value="Overseas Trust" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Overseas Trust') {?> selected="selected"<?php } ?> data-id="2">Overseas Trust</option>

                                       <option value="Trust" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Trust') {?> selected="selected"<?php } ?> data-id="1">Trust</option>
                                       <option value="Charity" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Charity') {?> selected="selected"<?php } ?> data-id="1">Charity</option>
                                       <option value="Other" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Other') {?> selected="selected"<?php } ?> data-id="1">Other</option>
                                    </select>
                                 </div>
                              </div>
                              <!--<div class="form-group row name_fields">
                                 <label class="col-sm-4 col-form-label">Allocation Holder</label>
                                 <div class="col-sm-8">
                                    <input type="text" name="allocation_holders" id="allocation_holders" placeholder="Allocation holder" value="<?php if(isset($client['crm_allocation_holder']) && ($client['crm_allocation_holder']!='') ){ echo $client['crm_allocation_holder'];}?>" class="fields">
                                 </div>
                              </div>-->
                           </div>
                        </div>
                     
                  </div>
               </div>
               <!-- required information -->
               <!-- basic details tab open -->
                <div id="details" class="width-value accord-proposal1 tab-pane fade ">
                <div class="space-required">
                  <div class="main-pane-border1 cmn-errors1" id="basic_details_error">
                     <div class="alert-ss"></div>
                 </div>
               </div>

                              <div class="management_form1 three-animation three-animation_client management_accordion CONTENT_IMPORTANT_DETAILS" id="important_details_section">
                                 <?php 
                                  //  $read_edit=(isset($client['status']) && $client['status']==1)? '' :'display:none;'; 
                                 $read_edit='display:none';
                                    
                                    ?>
                                 <div class="form-group row name_fields sorting  <?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_company_name1']; ?>" <?php echo $Setting_Delete['crm_company_name1']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_company_name1']; ?></label>
                                    <div class="col-sm-8 edit-field-popup1">
                                       <?php if($uri_seg!=''){ ?> 
                                       <a href="<?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){ echo $client['crm_company_url'];} else { echo "#"; }?>" id="company_url_anchor" target="_blank"><?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?></a> 
                                      
                                       <input type="hidden" name="company_name"  placeholder="Accotax Limited" value="<?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?>" class="fields edit_classname" >
                                       <!-- <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                          <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p> -->
                                   
                                    <?php }else { ?> 
                                   

                                    <input type="text" name="company_name1" id="company_name1" placeholder="Accotax Limited" value="<?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?>" class="fields edit_classname" >
                                    <!-- <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>  -->
                                  
                                    <?php } ?>
                                     </div>
                                 </div>

                              <div class="form-group row sorting newappend-03  <?php if(isset($client['crm_company_number']) && ($client['crm_company_number']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_company_number'] ?>">
                                 <label class="col-sm-4 col-form-label">
                                  <?php echo $Setting_Label['crm_company_number']; ?>
                                    <span class="Hilight_Required_Feilds">*</span>
                                  </label>
                               
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="company_number" id="company_number" placeholder="Company number" value="<?php if(isset($client['crm_company_number']) && ($client['crm_company_number']!='') ){ echo $client['crm_company_number'];}?>" class="fields edit_classname"  >
                                    <!-- <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div> -->
                                 </div>
                                 
                              </div>
                              <div class="clearfix"></div>
                              <div class="form-group row date_birth name_fields sorting <?php if(isset($client['crm_incorporation_date']) && ($client['crm_incorporation_date']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_incorporation_date']; ?>" <?php echo $Setting_Delete['crm_incorporation_date']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_incorporation_date']; ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <!--  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
                                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                    <input class="fields edit_classname date_picker_dob <?php if(isset($client['crm_incorporation_date']) && ($client['crm_incorporation_date']!='') ){?>  light-color_company <?php } ?>" type="text" name="date_of_creation" id="date_of_creation" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_incorporation_date']) && ($client['crm_incorporation_date']!='') ){ echo Change_Date_Format($client['crm_incorporation_date']);}?>" >
                                   <!--  <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div> -->
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_registered_in']) && ($client['crm_registered_in']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_registered_in']; ?>" <?php echo $Setting_Delete['crm_registered_in']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_registered_in']; ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="registered_in" id="registered_in" placeholder="" value="<?php if(isset($client['crm_registered_in']) && ($client['crm_registered_in']!='')  ){ echo $client['crm_registered_in'];}?>" class="fields edit_classname" >
                                    <!-- <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div> -->
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_address_line_one']) && ($client['crm_address_line_one']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_address_line_one']; ?>" <?php echo $Setting_Delete['crm_address_line_one']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_address_line_one']; ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="address_line_one" id="address_line_one" placeholder="" value="<?php if(isset($client['crm_address_line_one']) && ($client['crm_address_line_one']!='')  ){ echo $client['crm_address_line_one'];}?>" class="fields edit_classname" >
                                   <!--  <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div> -->
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_address_line_two']) && ($client['crm_address_line_two']!='') ){?>light-color_company<?php } ?>"  data-id="<?php echo $Setting_Order['crm_address_line_two']; ?>" <?php echo $Setting_Delete['crm_address_line_two']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_address_line_two']; ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="address_line_two" id="address_line_two" placeholder="" value="<?php if(isset($client['crm_address_line_two']) && ($client['crm_address_line_two']!='')  ){ echo $client['crm_address_line_two'];}?>" class="fields edit_classname" >
                                   <!--  <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div> -->
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_address_line_three']) && ($client['crm_address_line_three']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_address_line_three']; ?>" <?php echo $Setting_Delete['crm_address_line_three']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_address_line_three']; ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="address_line_three" id="address_line_three" placeholder="" value="<?php if(isset($client['crm_address_line_three']) && ($client['crm_address_line_three']!='')  ){ echo $client['crm_address_line_three'];}?>" class="fields edit_classname" >
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_town_city']) && ($client['crm_town_city']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_town_city']; ?>" <?php echo $Setting_Delete['crm_town_city']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_town_city']; ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="crm_town_city" id="crm_town_city" placeholder="" value="<?php if(isset($client['crm_town_city']) && ($client['crm_town_city']!='')  ){ echo $client['crm_town_city'];}?>" class="fields edit_classname" >
                                   <!--  <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div> -->
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_post_code']) && ($client['crm_post_code']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_post_code']; ?>" <?php echo $Setting_Delete['crm_post_code']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_post_code']; ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="crm_post_code" id="crm_post_code" placeholder="" value="<?php if(isset($client['crm_post_code']) && ($client['crm_post_code']!='')  ){ echo $client['crm_post_code'];}?>" class="fields edit_classname" >
                                    <!-- <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div> -->
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_company_status']; ?>" <?php echo $Setting_Delete['crm_company_status']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_company_status']; ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                 
                                    
                                    <select name="company_status" id="company_status" class="fields edit_classname">
                                      <!-- when we add new client it should be active as default -->
                                       <option value="1" <?php if( !isset( $client['crm_company_status'] ) || $client['crm_company_status'] == '1' ) echo "selected='selected'";?> >Active</option>
                                       <option value="0" <?php if( $client['crm_company_status']=='0' || $client['crm_company_status']=='2' || ( isset( $client['crm_company_status'] ) && $client['crm_company_status']=='' ) ) echo "selected='selected'";?> >Inactive</option>
                                       <option value="3" <?php if( $client['crm_company_status'] == '3' ) echo "selected='selected'";?> >Frozen</option>
                                       <option value="4" <?php if( $client['crm_company_status'] == '4' ) echo "selected='selected'";?>>Draft</option>
                                       <option value="5" <?php if( $client['crm_company_status'] == '5' ) echo "selected='selected'";?>>Archive</option>
                                    </select>
                                    <!-- <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div> -->
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_company_type']) && ($client['crm_company_type']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_company_type']; ?>" <?php echo $Setting_Delete['crm_company_type']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_company_type']; ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="company_type" id="company_type" placeholder="Private Limited Company" value="<?php if(isset($client['crm_company_type']) && ($client['crm_company_type']!='') ){ echo $client['crm_company_type'];}?>" class="fields edit_classname" readonly>
                                    <!-- <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div> -->
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_company_sic']) && ($client['crm_company_sic']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_company_sic']; ?>" <?php echo $Setting_Delete['crm_company_sic']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_company_sic']; ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="company_sic" id="company_sic" value="<?php if(isset($client['crm_company_sic']) && ($client['crm_company_sic']!='') ){ echo $client['crm_company_sic'];}?>" class="fields edit_classname" > 
                                   <!--  <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div> -->
                                    <input type="hidden" name="sic_codes" id="sic_codes" value="">
                                 </div>
                              </div>
                              <div class="form-group row date_birth sorting <?php if(isset($client['crm_engagement_letter']) && ($client['crm_engagement_letter']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_letter_sign']; ?>" <?php echo $Setting_Delete['crm_letter_sign']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_letter_sign']; ?></label>
                                 <div class="col-sm-8">
                                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                    <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="engagement_letter" id="engagement_letter" value="<?php if(isset($client['crm_letter_sign']) && ($client['crm_letter_sign']!='') ){ echo $client['crm_letter_sign'];}?>"/>
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_business_website']) && ($client['crm_business_website']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_business_website']; ?>" <?php echo $Setting_Delete['crm_business_website']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_business_website']; ?></label>
                                 <div class="col-sm-8">
                                    <input type="text" class="fields" name="business_website" id="business_website" placeholder="Business Website" value="<?php if(isset($client['crm_business_website']) && ($client['crm_business_website']!='') ){ echo $client['crm_business_website'];}?>">
                                 </div>
                              </div>
                              <!-- rsptrspt-->

                              <div class="form-group row sorting  <?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_company_url']; ?>" <?php echo $Setting_Delete['crm_company_url']; ?>>
                                 <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_company_url']; ?></label>
                                 <div class="col-sm-8">
                                    <input type="text" name="company_url" id="company_url" value="<?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){ echo $client['crm_company_url'];}?>" class="fields" placeholder="www.google.com">
                                 </div>
                              </div>

                              <div class="form-group row sorting <?php if(isset($client['crm_officers_url']) && ($client['crm_officers_url']!='') ){?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['crm_officers_url']; ?>" <?php echo $Setting_Delete['crm_officers_url']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_officers_url']; ?></label>
                                 <div class="col-sm-8">
                                   
                                    <input type="text" name="officers_url" id="officers_url" value="<?php if(isset($client['crm_officers_url']) && ($client['crm_officers_url']!='') ){ echo $client['crm_officers_url'];}?>" class="fields" placeholder="www.google.com">
                                   
                                 </div>
                              </div>


                              <div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_accounting_system']; ?>" <?php echo $Setting_Delete['crm_accounting_system']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_accounting_system']; ?></label>
                                 <div class="col-sm-8">
                                    <input type="text" class="fields" name="accounting_system_inuse" id="accounting_system_inuse" value="<?php if(isset($client['crm_accounting_system']) && ($client['crm_accounting_system']!='') ){   echo $client['crm_accounting_system'];}?>">
                                 </div>
                              </div>


                               <div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_hashtag']; ?>" <?php echo $Setting_Delete['crm_hashtag']; ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_hashtag']; ?></label>
                                 <div class="col-sm-8">                                              

                                  <input type="text" value="<?php if(!empty($client['crm_hashtag'])){echo $client['crm_hashtag'];}?>" name="client_hashtag" id="tags" class="hashtag" />
                                   
                                 </div>
                              </div>
                              </div>
               </div>
               <!-- basic details tab close -->

               <div id="contacts1" class="tab-pane fade">
                <div class="space-required">
                  <div class="main-pane-border1 main_Contact" id="main_Contact_error">
                     <div class="alert-ss"></div>
                  </div>
               </div>
                  <div class="space-required remove_updatespace">
                     <div class="main-pane-border1 manage-border013">
                        <div class="pane-border1">
                           <div class="management_form1 management_accordion">
                              
                                 
                                 <div class="new_bts">
                                    <!--<select name="person" id="person" class="form-control fields">
                                       <option value="" selected="selected">--Select--</option>
                                       <?php if(isset($user['company_roles'])&& $user['company_roles']==1){ ?>
                                       <option value="opt1">Select Existing Person</option>
                                       <?php } ?>
                                       <option value="opt2">Select New Person</option>
                                    </select>-->
                                       <?php if(!empty($client['crm_company_number'])){ ?>
                                        <a href="javascript:;" id="" class="btn btn-primary add_existing_person person"  data-id="opt1" >Add Existing Person</a> 
                                       <?php } ?>
                                    <a href="javascript:;" id="Add_New_Contact" class="">Add New Person</a> 
                                 </div>
                                     <!-- 01-09-2018 -->
                                     
                                    <!-- 01-09-2018 -->
                               
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="contactss"></div>
                  <!--                                                    <form name="contact_info" id="contact_info" method="post">
                     -->
                     <div class="contact_form companies-house-form">
                  <?php 
                     $i=1;
                     
                     /*echo "<pre>";
                     print_r($contactRec);die;*/

                     foreach ($contactRec as $contactRec_key => $contactRec_value) {
                     
                               $title = $contactRec_value['title'];
                               $first_name = $contactRec_value['first_name'];
                               $middle_name = $contactRec_value['middle_name'];
                               $last_name = $contactRec_value['last_name'];
                               $surname = $contactRec_value['surname']; 
                               $preferred_name = $contactRec_value['preferred_name']; 
                               $mobile = $contactRec_value['mobile']; 
                               $main_email = $contactRec_value['main_email']; 
                               $nationality = $contactRec_value['nationality']; 
                               $psc = $contactRec_value['psc']; 
                               $shareholder = $contactRec_value['shareholder']; 
                               $ni_number = $contactRec_value['ni_number']; 
                               $contact_type = $contactRec_value['contact_type']; 
                               $address_line1 = $contactRec_value['address_line1']; 
                               $address_line2 = $contactRec_value['address_line2']; 
                               $town_city = $contactRec_value['town_city']; 
                               $post_code = $contactRec_value['post_code']; 
                               $landline = $contactRec_value['landline']; 
                               $pre_landline = $contactRec_value['pre_landline'];
                               $work_email   = $contactRec_value['work_email']; 
                               $date_of_birth   = $contactRec_value['date_of_birth']; 
                               $nature_of_control   = $contactRec_value['nature_of_control']; 
                               $marital_status   = $contactRec_value['marital_status']; 
                               $utr_number   = $contactRec_value['utr_number']; 
                               $id   = $contactRec_value['id']; 
                               $client_id   = $contactRec_value['client_id']; 
                               $make_primary   = $contactRec_value['make_primary']; 
                     
                               $contact_names = $this->Common_mdl->numToOrdinalWord($i).' Contact ';
                               $name=ucfirst($contactRec_value['first_name']).' '.ucfirst($contactRec_value['surname']);
                           ?>
      <div class="space-required new_update1">
        <div class="update-data01 make_a_primary  common_div_remove-<?php echo $i; ?>" id="common_div_remove-<?php echo $i; ?>">

            <input type="hidden" class="contact_table_id" name="contact_table_id[<?php echo $i;?>]" value="<?php echo $id;?>">

           <div class="client-info-circle1 floating_set">
              <div class=" remove<?php echo $id;?> contactcc">
                 <div class="main-contact append_contact">
                    <span class="h4"><!-- <?php echo $name;?> -->Contact Person Details</span>
                    <div class="dead-primary1 for_row_count-<?php echo $i; ?>">

                      <input type="hidden" name="make_primary_loop[<?php echo $i;?>]" id="make_primary_loop" value="<?php echo $i; ?>">
                       <?php if($make_primary==1){?>
                           <div class="radio radio-inline">
                          <label>
                           <input type="radio" name="make_primary" id="make_primary<?php echo $i;?>" value="<?php echo $i;?>" checked>
                            <i class="helper" style="display: none;"></i>
                          </label>
                          <a href="javascript:void(0)" class="make_primary_section active" data-id="make_primary<?php echo $i;?>" ><span>Primary Contact</span></a>
                       </div>
                       <?php }else{
                          /** 01-09-2018 **/
                          ?>
                       <div class="radio radio-inline">
                          <label>
                         <!--  <input type="radio" name="" id="" value="" onclick="return make_a_primary(<?php echo $id.','.$client_id;?>);"> -->
                          <input type="radio" name="make_primary" id="make_primary<?php echo $i;?>" value="<?php echo $i;?>">
                          <i class="helper" style="display: none;"></i>
                          </label>
                          <a href="javascript:void(0)" class="make_primary_section"  data-id="make_primary<?php echo $i;?>" ><span>Make a primary</span>
                          </a>
                       </div>
                         
                          <?php
                          } ?>
                    </div>
                    <!-- <a href="#" data-toggle="modal" data-target="#modalcontact<?php echo $id;?>"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> -->
                 </div>
                 <div class="primary-info02 common-spent01 addnewclient">
                    <div class="primary-info03 floating_set ">
                       <div id="collapse" class="panel-collapse">
                          <div class="basic-info-client15  CONTENT_CONTACT_PERSON">
                                <span class="primary-inner contact_sort"
                                data-id="<?php echo $Setting_Order['contact_title'];?>"
                                <?php echo $Setting_Delete['contact_title'];?>>
                                   <label>
                                    <?php echo $Setting_Label['contact_title'];?>
                                   </label>
                                   <!-- <input type="text" class="text-info title"  name="title[]" id="title<?php echo $id;?>" value="<?php echo $title;?>">  -->
                                   <select class="text-info title"  name="title[<?php echo $i;?>]" id="title<?php echo $id;?>">
                                      <option value="Mr" <?php if(isset($title) && $title=='Mr') {?> selected="selected"<?php } ?>>Mr</option>
                                      <option value="Mrs" <?php if(isset($title) && $title=='Mrs') {?> selected="selected"<?php } ?>>Mrs</option>
                                      <option value="Miss" <?php if(isset($title) && $title=='Miss') {?> selected="selected"<?php } ?>>Miss</option>
                                      <option value="Dr" <?php if(isset($title) && $title=='Dr') {?> selected="selected"<?php } ?>>Dr</option>
                                      <option value="Ms" <?php if(isset($title) && $title=='Ms') {?> selected="selected"<?php } ?>>Ms</option>
                                      <option value="Prof" <?php if(isset($title) && $title=='Prof') {?> selected="selected"<?php } ?>>Prof</option>
                                   </select>
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($first_name) && ($first_name!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_first_name'];?>"
                                <?php echo $Setting_Delete['contact_first_name'];?>

                                  >
                                <label><?php echo $Setting_Label['contact_first_name'];?>
                                <span class="Hilight_Required_Feilds">*</span>
                                </label>
                                <input type="text" class="text-info" name="first_name[<?php echo $i;?>]"  value="<?php echo $first_name;?>" data-cntid="<?php echo $i;?>" onchange="update_ccp_name_to_content(this)">
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($middle_name) && ($middle_name!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_middle_name'];?>"
                                <?php echo $Setting_Delete['contact_middle_name'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_middle_name'];?>
                                </label>
                                <input type="text" class="text-info" name="middle_name[<?php echo $i;?>]" id="middle_name<?php echo $id;?>" value="<?php echo $middle_name;?>">
                                </span>
                                 <span class="primary-inner contact_sort <?php  if(isset($last_name) && ($last_name!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_last_name'];?>"
                                <?php echo $Setting_Delete['contact_last_name'];?>>
                                  <label>
                                    <?php echo $Setting_Label['contact_last_name'];?>
                                  </label>
                                  <input type="text" name="last_name[<?php echo $i;?>]"  placeholder="" value="<?php if(isset($last_name) && ($last_name!='') ){ echo $last_name;}?>" class="text-info" data-cntid="<?php echo $i;?>" onchange="update_ccp_name_to_content(this)">
                                  </span>
                                <span class="primary-inner contact_sort <?php  if(isset($surname) && ($surname!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_surname'];?>"
                                <?php echo $Setting_Delete['contact_surname'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_surname'];?>
                                </label>
                                <input type="text" class="text-info" name="surname[<?php echo $i;?>]" id="surname<?php echo $id;?>" value="<?php echo $surname;?>">
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($preferred_name) && ($preferred_name!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_preferred_name'];?>"
                                <?php echo $Setting_Delete['contact_preferred_name'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_preferred_name'];?>
                                </label>
                                <input type="text" class="text-info" name="preferred_name[<?php echo $i;?>]" id="preferred_name<?php echo $id;?>" value="<?php echo $preferred_name;?>">
                                </span> 
                                <span class="primary-inner contact_sort <?php  if(isset($mobile) && ($mobile!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_mobile'];?>"
                                <?php echo $Setting_Delete['contact_mobile'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_mobile'];?>
                                </label>
                                <input type="text" class="text-info" name="mobile_number[<?php echo $i;?>]" id="mobile<?php echo $id;?>" value="<?php echo $mobile;?>">
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($main_email) && ($main_email!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_main_email'];?>"
                                <?php echo $Setting_Delete['contact_main_email'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_main_email'];?>
                                </label>
                                <input type="text" class="text-info" name="main_email[<?php echo $i;?>]" id="email_id<?php echo $id;?>"  value="<?php echo $main_email;?>">
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($nationality) && ($nationality!='') ){ ?>light-color_company<?php } ?>"   data-id="<?php echo $Setting_Order['contact_nationality'];?>"
                                <?php echo $Setting_Delete['contact_nationality'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_nationality'];?>
                                </label>
                                <input type="text" class="text-info" name="nationality[<?php echo $i;?>]" id="nationality<?php echo $id;?>"  value="<?php echo $nationality;?>">
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($psc) && ($psc!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_psc'];?>"
                                <?php echo $Setting_Delete['contact_psc'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_psc'];?>
                                </label>
                                <input type="text" class="text-info" name="psc[<?php echo $i;?>]" id="psc<?php echo $id;?>"  value="<?php echo $psc;?>">
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($shareholder) && ($shareholder!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_shareholder'];?>"
                                <?php echo $Setting_Delete['contact_shareholder'];?>>
                                   <label>
                                     <?php echo $Setting_Label['contact_shareholder'];?>
                                   </label>
                                   <select name="shareholder[<?php echo $i;?>]" id="shareholder<?php echo $id;?>">
                                      <option value="yes" <?php if(isset($shareholder) && $shareholder=='yes') {?> selected="selected"<?php } ?>>Yes</option>
                                      <option value="no" <?php if(isset($shareholder) && $shareholder=='no') {?> selected="selected"<?php } ?>>No</option>
                                   </select>
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($ni_number) && ($ni_number!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_ni_number'];?>"
                                <?php echo $Setting_Delete['contact_ni_number'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_ni_number'];?>
                                </label>
                                <input type="text" class="text-info" name="ccp_ni_number[<?php echo $i;?>]" id="ni_number<?php echo $id;?>"  value="<?php echo $ni_number;?>">
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($contactRec_value['country_of_residence']) && ($contactRec_value['country_of_residence']!='') ){ ?>light-color_company<?php } ?>"  data-id="<?php echo $Setting_Order['contact_country_of_residence'];?>"
                                <?php echo $Setting_Delete['contact_country_of_residence'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_country_of_residence'];?>
                                </label>
                                <input type="text" name="country_of_residence[<?php echo $i;?>]" id="country_of_residence<?php echo $id;?>" class="text-info" value="<?php if(isset($contactRec_value['country_of_residence']) && ($contactRec_value['country_of_residence']!='') ){ echo $contactRec_value['country_of_residence'];}?>">
                                </span>
                             
                             
                                <span class="primary-inner contact_type contact_sort <?php  if(isset($contact_type) && ($contact_type!='') ){ ?>light-color_company<?php } ?>"  data-id="<?php echo $Setting_Order['contact_contact_type'];?>"
                                <?php echo $Setting_Delete['contact_contact_type'];?>>
                                   <label><?php echo $Setting_Label['contact_contact_type'];?></label>
                                   <select name="contact_type" id="contact_type<?php echo $id;?>" class="othercus">
                                      <option value="Director" <?php if(isset($contact_type) && $contact_type=='Director') {?> selected="selected"<?php } ?>>Director</option>
                                      <option value="Director/Shareholder" <?php if(isset($contact_type) && $contact_type=='Director/Shareholder') {?> selected="selected"<?php } ?>>Director/Shareholder</option>
                                      <option value="Shareholder" <?php if(isset($contact_type) && $contact_type=='Shareholder') {?> selected="selected"<?php } ?>>Shareholder</option>
                                      <option value="Accountant" <?php if(isset($contact_type) && $contact_type=='Accountant') {?> selected="selected"<?php } ?>>Accountant</option>
                                      <option value="Bookkeeper" <?php if(isset($contact_type) && $contact_type=='Bookkeeper') {?> selected="selected"<?php } ?>>Bookkeeper</option>
                                      <option value="Other" <?php if(isset($contact_type) && $contact_type=='Other') {?> selected="selected"<?php } ?>>Other(Custom)</option>
                                   </select>
                                </span>
                                <span class="primary-inner spnMulti contact_sort" id="others_customs" style="<?php if($contact_type=="Other"){echo 'display:inline-block';}else {echo "display: none;";}?>" data-id="<?php echo $Setting_Order['contact_other_custom'];?>"
                                <?php echo $Setting_Delete['contact_other_custom'];?> >
                                <label>
                                  <?php echo $Setting_Label['contact_other_custom'];?>
                                </label>
                                <input type="text" class="text-info" name="other_custom[<?php echo $i;?>]" id="other_custom<?php echo $id;?>"  value="<?php if(isset($contactRec_value['other_custom']) && ($contactRec_value['other_custom']!='') ){ echo $contactRec_value['other_custom'];}?>">
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($address_line1) && ($address_line1!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_address_line1'];?>"
                                <?php echo $Setting_Delete['contact_address_line1'];?> >
                                <label>
                                  <?php echo $Setting_Label['contact_address_line1'];?> 
                                </label>
                                <input type="text" class="text-info" name="address_line1[<?php echo $i;?>]" id="address_line1<?php echo $id;?>"  value="<?php echo $address_line1;?>">
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($address_line2) && ($address_line2!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_address_line2'];?>"
                                <?php echo $Setting_Delete['contact_address_line2'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_address_line2'];?>
                                </label>
                                <input type="text" class="text-info" name="address_line2[<?php echo $i;?>]" id="address_line2<?php echo $id;?>"  value="<?php echo $address_line2;?>">
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($town_city) && ($town_city!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_town_city'];?>"
                                <?php echo $Setting_Delete['contact_town_city'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_town_city'];?>
                                </label>
                                <input type="text" class="text-info" name="town_city[<?php echo $i;?>]" id="town_city<?php echo $id;?>"  value="<?php echo $town_city;?>">
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($post_code) && ($post_code!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_post_code'];?>"
                                <?php echo $Setting_Delete['contact_post_code'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_post_code'];?>
                                </label>
                                <input type="text" class="text-info" name="post_code[<?php echo $i;?>]" id="post_code<?php echo $id;?>"  value="<?php echo $post_code;?>">
                                </span>
                <span class="primary-inner landlnecls contact_sort <?php  if(isset($contactRec_value['landline']) && ($contactRec_value['landline']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_landline'];?>"
                                <?php echo $Setting_Delete['contact_landline'];?>>
                              <div class="cmn-land-append1 update-primary pack_add_row_wrpr_landline">
                                   <label><?php echo $Setting_Label['contact_landline'];?></label>
                                <?php 
                                   $land         = json_decode( $contactRec_value['landline'],true);
                                   $pre_landline = json_decode( $contactRec_value['pre_landline'],true);
                                   //print_r( $land);
                                   if(!empty($land)){
                                   foreach($land as $key =>$val) {
                                      
                                      $preselect = ['mobile'=>'','work'=>'','home'=>'','main'=>'','workfax'=>'','homefax'=>''];
                                      foreach ($preselect as $preselect_key => $preselect_val)
                                      {
                                         if( $preselect_key == $pre_landline[$key])
                                            {
                                               $preselect[$preselect_key] = "selected='selected'";
                                            }
                                      }
                                   ?>
                                
                                  <span class="primary-inner success ykpackrow_landline add-delete-work width_check check_width" id="<?php echo $key;?>">
                                  <div class="data_adds">
                                    <select name="pre_landline[<?php echo $i;?>][<?php echo $key;?>]" id="pre_landline">
                                      <option value="mobile" <?php echo $preselect['mobile'];?>>Mobile</option>
                                      <option value="work" <?php echo $preselect['work'];?>>Work</option>
                                      <option value="home" <?php echo $preselect['home'];?>>Home</option>
                                      <option value="main" <?php echo $preselect['main'];?>>Main</option>
                                      <option value="workfax" <?php echo $preselect['workfax'];?>>Work Fax</option>
                                      <option value="homefax" <?php echo $preselect['homefax'];?>>Home Fax</option>
                                    </select>
                                    <input type="text" class="text-info" name="landline[<?php echo $i;?>][<?php echo $key;?>]"   value="<?php echo $val;?>">
                                  </div>
                                  <!-- don't show first one  -->
                                  <?php if( $key != 0 ) {?>
                                  <div class="text-right danger-make1">
                                    <a href="javascript:;" class="btn btn-danger yk_pack_delrow_landline" id="<?php echo $key;?>">
                                      <i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i>
                                    </a>
                                  </div>
                                <?php } ?>
                                  <button type="button" class="btn btn-primary yk_pack_addrow_landline <?php if(!empty( $land[$key+1] ) ) echo 'yyyyyy';?>" data-id="<?php echo $i;?>" data-length="<?php echo $key;?>">
                                    Add Landline
                                  </button>
                                  </span>
                                  <?php
                                    }
                                  }
                                  else {?>
                                  <span class="primary-inner success ykpackrow_landline add-delete-work width_check check_width" id="0">
                                     <div class="data_adds">
                                        <select name="pre_landline[<?php echo $i;?>][0]" id="pre_landline<?php echo $id;?>">
                                          <option value="mobile">Mobile</option>
                                          <option value="work">Work</option>
                                          <option value="home">Home</option>
                                          <option value="main">Main</option>
                                          <option value="workfax">Work Fax</option>
                                          <option value="homefax">Home Fax</option>
                                        </select>
                                        <input type="text" class="text-info" name="landline[<?php echo $i;?>][0]" id="landline<?php echo $id;?>"  value="">
                                     </div>
                                    <button type="button" class="btn btn-primary yk_pack_addrow_landline" data-id="<?php echo $i;?>" data-length="0">Add Landline</button>
                                  </span>
                                <?php } ?>
                
                             
                             </div>
                             </span>
              
                                  </span>
              
                                  <span class="primary-inner displylinblock contact_sort <?php  if(isset($contactRec_value['work_email']) && ($contactRec_value['work_email']!='') ){ ?>light-color_company <?php } ?>" data-id="<?php echo $Setting_Order['contact_work_email'];?>"
                                    <?php echo $Setting_Delete['contact_work_email'];?>>
                                      <div class="update-primary work_lightmail pack_add_row_wrpr_email">
                                             <label><?php echo $Setting_Label['contact_work_email'];?></label>
                                          <div class="remove_one_row ">
                                    <?php 
                                    $work=json_decode($contactRec_value['work_email'],true);
                                    if(!empty($work))
                                    { 
                                      foreach($work as $key =>$val)
                                      {
                                        ?>
                                        <span class="primary-inner success ykpackrow_email add-delete-work">
                                          <input type="text" class="text-info" name="work_email[<?php echo$i;?>][<?php echo $key;?>]" id="work_email<?php echo $id;?>"  value="<?php echo $val;?>">
                                          <?php if( $key != 0 )
                                          {
                                            ?>
                                              <div class="text-right danger-make1">
                                                <a href="javascript:;" class="btn btn-danger yk_pack_delrow_email">
                                                  <i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i>
                                                </a>
                                              </div>
                                            <?php
                                          }
                                          ?>
                                          <!-- add button only for last row  -->
                                          
                                            <button type="button" class="btn btn-primary yk_pack_addrow_email <?php if( !empty( $work[ $key+1 ] ) ) echo "yyyyyy"; ?>" data-id="<?php echo $i;?>" data-length="<?php echo $key;?>">
                                              Add Email
                                            </button>
                                  
                                        </span>
                                      <?php 
                                      }                                       
                                    }
                                    else
                                    {
                                      ?>
                                      <span class="primary-inner success ykpackrow_email add-delete-work">
                                        <input type="text" class="text-info" name="work_email[<?php echo $i;?>][0]" id="work_email<?php echo $id;?>">
                                        <button type="button" class="btn btn-primary yk_pack_addrow_email" data-id="<?php echo $i;?>" data-length="0">
                                          Add Email
                                        </button>
                                      </span>
                                    <?php 
                                    }
                                    ?>
                                    </div>
                                    </div>
                                </span> 

                                <span class="primary-inner contact_sort date_birth <?php  if(isset($date_of_birth) && ($date_of_birth!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_date_of_birth'];?>"
                                <?php echo $Setting_Delete['contact_date_of_birth'];?>>
                                   <label>
                                     <?php echo $Setting_Label['contact_date_of_birth'];?>
                                   </label>
                                   <div class="picker-appoint">
                                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> <input type="text" class="text-info date_picker_dob" name="date_of_birth[<?php echo $i;?>]"  placeholder="dd-mm-yyyy" value="<?php if(isset($date_of_birth) && ($date_of_birth!='') ){ echo $date_of_birth;}?>">
                                   </div>
                                </span>


                                <span class="primary-inner contact_sort <?php  if(isset($nature_of_control) && ($nature_of_control!='') ){ ?>light-color_company<?php } ?>"  data-id="<?php echo $Setting_Order['contact_nature_of_control'];?>"
                                <?php echo $Setting_Delete['contact_nature_of_control'];?>>
                                <label><?php echo $Setting_Label['contact_nature_of_control'];?></label>
                                <input type="text" class="text-info" name="nature_of_control[<?php echo $i;?>]" id="nature_of_control<?php echo $id;?>"  value="<?php echo $nature_of_control;?>">
                                </span>


                                <span class="primary-inner contact_sort <?php  if(isset($marital_status) && ($marital_status!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_marital_status'];?>"
                                <?php echo $Setting_Delete['contact_marital_status'];?>>
                                   <label>
                                     <?php echo $Setting_Label['contact_marital_status'];?>
                                   </label>                                
                                   <select name="marital_status[<?php echo $i;?>]" id="marital_status<?php echo $id;?>">
                                      <option value="Single" <?php if(isset($marital_status) && $marital_status=='Single') {?> selected="selected"<?php } ?>>Single</option>
                                      <option value="Living_together" <?php if(isset($marital_status) && $marital_status=='Living_together') {?> selected="selected"<?php } ?>>Living together</option>
                                      <option value="Engaged" <?php if(isset($marital_status) && $marital_status=='Engaged') {?> selected="selected"<?php } ?>>Engaged</option>
                                      <option value="Married" <?php if(isset($marital_status) && $marital_status=='Married') {?> selected="selected"<?php } ?>>Married</option>
                                      <option value="Civil_partner" <?php if(isset($marital_status) && $marital_status=='Civil_partner') {?> selected="selected"<?php } ?>>Civil partner</option>
                                      <option value="Separated" <?php if(isset($marital_status) && $marital_status=='Separated') {?> selected="selected"<?php } ?>>Separated</option>
                                      <option value="Divorced" <?php if(isset($marital_status) && $marital_status=='Divorced') {?> selected="selected"<?php } ?>>Divorced</option>
                                      <option value="Widowed" <?php if(isset($marital_status) && $marital_status=='Widowed') {?> selected="selected"<?php } ?>>Widowed</option>
                                   </select>
                                </span>
                                <span class="primary-inner contact_sort <?php  if(isset($utr_number) && ($utr_number!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_utr_number'];?>"
                                <?php echo $Setting_Delete['contact_utr_number'];?>>
                                <label>
                                  <?php echo $Setting_Label['contact_utr_number'];?>
                                </label>
                                <input type="text" class="text-info" name="ccp_personal_utr_number[<?php echo $i;?>]" id="utr_number<?php echo $id;?>"  value="<?php echo $utr_number;?>">
                                </span>


                                <span class="primary-inner contact_sort <?php  if(isset($contactRec_value['occupation']) && ($contactRec_value['occupation']!='') ){ ?>light-color_company<?php } ?>"
                                  data-id="<?php echo $Setting_Order['contact_occupation'];?>"
                                <?php echo $Setting_Delete['contact_occupation'];?>
                                >
                                <label>
                                  <?php echo $Setting_Label['contact_occupation'];?>
                                </label>
                                <input type="text" class="text-info" id="occupation<?php echo $id;?>"  name="occupation[<?php echo $i;?>]" value="<?php if(isset($contactRec_value['occupation']) && ($contactRec_value['occupation']!='') ){ echo $contactRec_value['occupation'];}?>">
                                </span>

                                <span class="primary-inner date_birth contact_sort <?php  if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_appointed_on'];?>"
                                <?php echo $Setting_Delete['contact_appointed_on'];?>> 
                                  <label>
                                    <?php echo $Setting_Label['contact_appointed_on'];?>
                                  </label>
                                  <div class="picker-appoint">
                                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                      <input type="text" class="text-info date_picker_dob" id="appointed_on<?php echo $id;?>"  name="appointed_on[<?php echo $i;?>]" value="<?php if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ echo $contactRec_value['appointed_on'];}?>">
                                  </div>
                                </span>
                                <span class="primary-inner date_birth contact_sort <?php  if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_appointed_on'];?>"
                                <?php echo $Setting_Delete['contact_appointed_on'];?>> 
                                  <label>
                                    <?php echo $Setting_Label['contact_appointed_on'];?>
                                  </label>
                                  <div class="picker-appoint">
                                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                      <input type="text" class="text-info date_picker_dob" id="appointed_on<?php echo $id;?>"  name="appointed_on[<?php echo $i;?>]" value="<?php if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ echo $contactRec_value['appointed_on'];}?>">
                                  </div>
                                </span>
                                <span class="primary-inner date_birth contact_sort <?php  if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_appointed_on'];?>"
                                <?php echo $Setting_Delete['contact_appointed_on'];?>> 
                                  <label>
                                    <?php echo $Setting_Label['contact_appointed_on'];?>
                                  </label>
                                  <div class="picker-appoint">
                                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                      <input type="text" class="text-info date_picker_dob" id="appointed_on<?php echo $id;?>"  name="appointed_on[<?php echo $i;?>]" value="<?php if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ echo $contactRec_value['appointed_on'];}?>">
                                  </div>
                                </span>
                                <span class="primary-inner contact_sort"  data-id="<?php echo $Setting_Order['contact_personal_tax_return_required'];?>"
                                <?php echo $Setting_Delete['contact_personal_tax_return_required'];?>> 
                                  <label>
                                    <?php echo $Setting_Label['contact_personal_tax_return_required'];?>
                                  </label>
                                  <div class="picker-appoint">          
                                      <select name="personal_tax_return_required[<?php echo $i;?>]" class="personal_tax_return_required" data-id="<?php echo $i;?>">
                                        <option value="">Select</option>
                                        <option value="no" <?php if($contactRec_value['personal_tax_return_required']=='no') echo "selected='selected'"; ?>>No</option>
                                        <option value="yes" <?php if($contactRec_value['personal_tax_return_required']=='yes') echo "selected='selected'"; ?>>Yes</option>
                                      </select>
                                  </div>
                                </span>
                                                                                              
                              <?php 
                                $_POST['cnt'] = $i;
                                $user_id_conact_id = $uri_seg."--".$contactRec_value['id'];
                                echo render_custom_fields_one( 'Contact_Person' , $user_id_conact_id);
                                unset( $_POST['cnt'] );
                              ?>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        </div> 
      <!--   <input type="hidden" name="make_primary[]" id="make_primary[]" value="<?php echo $make_primary;?>"> -->
      <?php $i++;  } ?> 
      <input type="hidden" name="incre" id="incre" value="<?php echo $i-1;?>">
      <!--                        </form>
         -->
         </div>
      <!-- <div class="contact_form companies-house-form"></div> -->
      <!-- <input type="hidden" name="append_cnt" id="append_cnt" value=""> -->
      <!-- 05-07-2018 -->
      <input type="hidden" name="append_cnt" id="append_cnt" value="<?php echo $i-1; ?>">
      <!-- end of 05-07-2018 -->
   </div>
   <!-- contact1 -->







                                    <!-- Business Details -->   
                                     <?php 
               $service_all = array_intersect( array_column($tab_on,'tab') , ['on'] ); 
               $service_all = count($service_all)==16 ? "checked='checked'" : "";

               $reminder_all = array_intersect( array_column($tab_on,'reminder') , ['on'] ); 
               $reminder_all = count($reminder_all)==16 ? "checked='checked'" : "";

               $text_all = array_intersect( array_column($tab_on,'text') , ['on'] ); 
               $text_all = count($text_all)==16 ? "checked='checked'" : "";

               $inv_all = array_intersect( array_column($tab_on,'invoice') , ['on'] ); 
               $inv_all = count($inv_all)==16 ? "checked='checked'" : "";

               $client_enabled_service  = [];

               ?>
               <!-- service tab alway stay up to importend tab service tab content variable used importent tab -->
               <div id="service" class="tab-pane fade">
                  <div class="space-required">
                     <div class="basic-available-data">
                        <div class="basic-available-data1">
                           <table class="client-detail-table01 service-table-client">
                              <thead>
                                 <tr>
                                    <th>SERVICES NAME </th>
                                    <!-- <th class="switching"> <input type="checkbox" class="js-small f-right checkall" name="checkall" data-id="consu" checked="checked">  SERVICES( TABS )</th> -->
                                     <th class="switching"> <input type="checkbox" <?php echo $service_all; ?>  class="js-small f-right checkall" name="checkall" data-id="consu">  SERVICES</th>
                                    <th class="reminds"><input type="checkbox" <?php echo $reminder_all; ?> class="checkall_reminder js-small"  name="checkall" data-id="consu"> <span class="js-services1">Reminders</span></th>
                                    <th><input type="checkbox" <?php echo $text_all; ?> class="checkall_text js-small"  name="checkall" data-id="consu"> <span class="js-services1">Text</span></th>
                                    <th><input type="checkbox" <?php echo $inv_all; ?> class="checkall_notification js-small"  name="checkall" data-id="consu"> <span class="js-services1">Invoice Notifications</span></th>
                                 </tr>
                              </thead>
                            <tfoot class="ex_data1">
                                <tr>
                                <th>
                                </th>
                                </tr>
                            </tfoot>
                              <tbody>
                                    <?php 
                                        //for show assignees
                                        $client_enabled_service[2] = $acc = ( $tab_on['accounts']['tab']!==0 ) ?  "checked='checked'" : "";                        
                                        $accre = ( $tab_on['accounts']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $acctext = ( $tab_on['accounts']['text']!==0 ) ? "checked='checked'" : "";
                                        $twot = ( $tab_on['accounts']['text']!==0 ) ? "" :"display:none;";
                                        $accinvoice = ( $tab_on['accounts']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $twot_inv = ( $tab_on['accounts']['invoice']!==0 ) ? "" :"display:none;";

                                      if($acc!='' || isset($enabled_service[2]) )
                                      {
                                       ?>
                                      
                                 <tr>
                                    <td>Accounts</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="accounts[tab]" data-id="accounts" <?php echo $acc;?>></td>
                                    <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="accounts[reminder]" <?php echo $accre;?> data-id="enableacc"></td>
                                    <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="accounts[text]" <?php echo $acctext;?> data-id="twot"></td>
                                    <td class="swi3 invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="accounts[invoice]" <?php echo $accinvoice;?> data-id="inas"></td>
                                 </tr>
                                    <?php 
                                      }

                                         //for show assignees
                                      $client_enabled_service[10] = $bookkeep = ( $tab_on['bookkeep']['tab']!==0 ) ?  "checked='checked'" : "";   
                                        

                                        $bookkeepre = ( $tab_on['bookkeep']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        

                                        $bookkeeptext = ( $tab_on['bookkeep']['text']!==0 ) ? "checked='checked'" : "";

                                        $bookkeepinvoice = ( $tab_on['bookkeep']['invoice']!==0 ) ? "checked='checked'" : "";

                                    if( $bookkeep!='' || isset($enabled_service[10]) )   
                                    {
                                       ?>                                    
                                  <tr>
                                    <td>Bookkeeping</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="bookkeep[tab]" data-id="bookkeep" <?php echo $bookkeep;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="bookkeep[reminder]" <?php echo $bookkeepre;?> data-id="enablebook"></td>
                                    </td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="bookkeep[text]" <?php echo $bookkeeptext;?> data-id="elevent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="bookkeep[invoice]" <?php echo $bookkeepinvoice;?> data-id="inbook"></td>
                                 </tr>
                                    <?php                     
                                    }
                                      //for show assignees
                                        $client_enabled_service[1] = $cst = ( $tab_on['conf_statement']['tab']!==0  ?  "checked='checked'" : "" );
                                      $cstre = ( $tab_on['conf_statement']['reminder']!==0 ) ?  "checked='checked'" : "";
                                      $csttext = ( $tab_on['conf_statement']['text']!==0 ) ? "checked='checked'" : "";
                                      $cstinvoice = ( $tab_on['conf_statement']['invoice']!==0 ) ? "checked='checked'" : "";

                                      if($cst!='' || isset($enabled_service[1]) ) 
                                      {
                                       ?>                                      
                                 <tr>
                                    <td>Confirmation Statement </td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="confirm[tab]" data-id="cons" <?php echo $cst;?>></td>
                                    <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="confirm[reminder]" <?php echo $cstre;?> data-id="enableconf"></td>
                                    <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="confirm[text]" <?php echo $csttext;?> data-id="onet"></td>
                                    <td class="swi3 invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="confirm[invoice]" <?php echo $cstinvoice;?> data-id="incs"></td>
                                 </tr>
                                    <?php 
                                    }
                                        //for show assignees
                                      $client_enabled_service[3] = $tax = ( $tab_on['company_tax_return']['tab']!==0 ) ?  "checked='checked'" : "";   
                                        $taxre = ( $tab_on['company_tax_return']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $taxtext = ( $tab_on['company_tax_return']['text']!==0 ) ? "checked='checked'" : "";
                                        $taxinvoice = ( $tab_on['company_tax_return']['invoice']!==0 ) ? "checked='checked'" : "";
                                    if($tax!='' || isset($enabled_service[3]))
                                    {
                                    ?>
                                 <tr>
                                    <td>Company Tax Return</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="companytax[tab]" data-id="companytax" <?php echo $tax;?>></td>
                                    <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="companytax[reminder]" <?php echo $taxre;?> data-id="enabletax"></td>
                                    <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="companytax[text]" <?php echo $taxtext;?> data-id="threet"></td>
                                    <td class="swi3 invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="companytax[invoice]" <?php echo $taxinvoice;?> data-id="inct"></td>
                                 </tr>
                                    <?php 
                                      }
                                        //for show assignees
                                        $client_enabled_service[8] = $contra = ( $tab_on['cis']['tab']!==0 ) ?  "checked='checked'" : "";   
                                        $contrare = ( $tab_on['cis']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $contratext = ( $tab_on['cis']['text']!==0 ) ? "checked='checked'" : "";
                                        $contrainvoice = ( $tab_on['cis']['invoice']!==0 ) ? "checked='checked'" : "";
                                       if($contra!='' || isset($enabled_service[8]) )
                                       {
                                       ?>
                                   <tr>
                                    <td>CIS - Contractor</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="cis[tab]" data-id="cis" <?php echo $contra;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="cis[reminder]" <?php echo $contrare;?> data-id="enablecis"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="cis[text]" <?php echo $contratext;?> data-id="eightt"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="cis[invoice]" <?php echo $contrainvoice;?> data-id="incis"></td>
                                 </tr>
                                    <?php 
                                        }

                                        //for show assignees
                                        $client_enabled_service[9] = $contrasub = ( $tab_on['cissub']['tab']!==0 ) ?  "checked='checked'" : "";   

                                        $contrasubre = ( $tab_on['cissub']['reminder']!==0 ) ?  "checked='checked'" : "";

                                        $contrasubtext = ( $tab_on['cissub']['text']!==0 ) ? "checked='checked'" : "";

                                        $contrasubinvoice = ( $tab_on['cissub']['invoice']!==0 ) ? "checked='checked'" : "";
                                        
                                        if($contrasub!='' || isset($enabled_service[9]))
                                        {
                                          
                                       ?>
                                  <tr>
                                    <td>CIS - Sub Contractor</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="cissub[tab]" data-id="cissub" <?php echo $contrasub;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="cissub[reminder]" <?php echo $contrasubre;?> data-id="enablecissub"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="cissub[text]" <?php echo $contrasubtext;?> data-id="ninet"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="cissub[invoice]" <?php echo $contrasubinvoice;?> data-id="incis_sub"></td>
                                 </tr>
                                    <?php 
                                    }
                                        //for show assignees
                                          $client_enabled_service[13] = $investgate = ( $tab_on['investgate']['tab']!==0 ) ?  "checked='checked'" : "";   

                                        $investgatere = ( $tab_on['investgate']['reminder']!==0 ) ?  "checked='checked'" : "";

                                        $investgatetext = ( $tab_on['investgate']['text']!==0 ) ? "checked='checked'" : "";

                                        $investgateinvoice = ( $tab_on['investgate']['invoice']!==0 ) ? "checked='checked'" : "";
                                       if($investgate!='' || isset($enabled_service[13]) )
                                       {
                                        
                                       ?>
                                  <tr>
                                    <td>Tax Investigation Insurance</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="investgate[tab]" data-id="investgate" <?php echo $investgate;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="investgate[reminder]" <?php echo $investgatere;?> data-id="enableinvest"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="investgate[text]" <?php echo $investgatetext;?> data-id="thirteent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="investgate[invoice]" <?php echo $investgateinvoice;?> data-id="ininves"></td>
                                 </tr>
                                    <?php 
                                  }


                                        //for show assignees
                                        $client_enabled_service[12] = $management = ( $tab_on['management']['tab']!==0 ) ?  "checked='checked'" : "";   

                                        $managementre = ( $tab_on['management']['reminder']!==0 ) ?  "checked='checked'" : "";

                                        $managementtext = ( $tab_on['management']['text']!==0 ) ? "checked='checked'" : "";

                                        $managementinvoice = ( $tab_on['management']['invoice']!==0 ) ? "checked='checked'" : "";
                                      if($management!='' || isset($enabled_service[12]) )
                                      {  
                                       ?>
                                 <tr>
                                    <td>Management Accounts</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="management[tab]" data-id="management" <?php echo $management;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="management[reminder]" <?php echo $managementre;?> data-id="enablemanagement"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="management[text]" <?php echo $managementtext;?> data-id="twelvet"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="management[invoice]" <?php echo $managementinvoice;?> data-id="inma"></td>
                                 </tr>
                                    <?php 
                                       }
                                        //for show assignees
                                        $client_enabled_service[6] = $pay = ( $tab_on['payroll']['tab']!==0 ) ?  "checked='checked'" : "";   
                                         
                                        $payre = ( $tab_on['payroll']['reminder']!==0 ) ?  "checked='checked'" : "";

                                        $paytext = ( $tab_on['payroll']['text']!==0 ) ? "checked='checked'" : "";

                                        $payinvoice = ( $tab_on['payroll']['invoice']!==0 ) ? "checked='checked'" : "";
                                       if($pay!='' || isset($enabled_service[6]))
                                       {
                                        
                                       ?>
                                 <tr>
                                    <td>Payroll</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="payroll[tab]" data-id="payroll" <?php echo $pay;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="payroll[reminder]" <?php echo $payre;?> data-id="enablepay"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="payroll[text]" <?php echo $paytext;?> data-id="fivet"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="payroll[invoice]" <?php echo $payinvoice;?> data-id="inpr"></td>
                                 </tr>
                                    <?php 
                                       }                             
                                         //for show assignees
                                          $client_enabled_service[11 ] = $p11d = ( $tab_on['p11d']['tab']!==0 ) ?  "checked='checked'" : "";   

                                        $p11dre = ( $tab_on['p11d']['reminder']!==0 ) ?  "checked='checked'" : "";

                                        $p11dtext = ( $tab_on['p11d']['text']!==0 ) ? "checked='checked'" : "";

                                        $p11dinvoice = ( $tab_on['p11d']['invoice']!==0 ) ? "checked='checked'" : "";

                                        if( $p11d!='' || isset($enabled_service[11]) )
                                        {
                                       ?>
                                 <tr>
                                    <td>P11D</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="p11d[tab]" data-id="p11d" <?php echo $p11d;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="p11d[reminder]" <?php echo $p11dre;?> data-id="enableplld"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="p11d[text]" <?php echo $p11dtext;?> data-id="tent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="p11d[invoice]" <?php echo $p11dinvoice;?> data-id="inp11d"></td>
                                 </tr>
                                    <?php 
                                      }

                                          //for show assignees
                                          $client_enabled_service[14] = $registered = ( $tab_on['registered']['tab']!==0 ) ?  "checked='checked'" : "";   

                                        $registeredre = ( $tab_on['registered']['reminder']!==0 ) ?  "checked='checked'" : "";

                                        $registeredtext = ( $tab_on['registered']['text']!==0 ) ? "checked='checked'" : "";

                                        $registeredinvoice = ( $tab_on['registered']['invoice']!==0 ) ? "checked='checked'" : "";
                                       if($registered!='' || isset($enabled_service[14]) )
                                       {
                                       ?>
                                  <tr>
                                    <td>Registered Address</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="registered[tab]" data-id="registered" <?php echo $registered;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="registered[reminder]" <?php echo $registeredre;?> data-id="enablereg"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="registered[text]" <?php echo $registeredtext;?> data-id="fourteent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="registered[invoice]" <?php echo $registeredinvoice;?> data-id="inreg"></td>
                                 </tr>
                                    <?php 
                                      }
                                        //for show assignees
                                          $client_enabled_service[15] = $taxadvice = ( $tab_on['taxadvice']['tab']!==0 ) ?  "checked='checked'" : "";   

                                        $taxadvicere = ( $tab_on['taxadvice']['reminder']!==0 ) ?  "checked='checked'" : "";

                                        $taxadvicetext = ( $tab_on['taxadvice']['text']!==0 ) ? "checked='checked'" : "";

                                        $taxadviceinvoice = ( $tab_on['taxadvice']['invoice']!==0 ) ? "checked='checked'" : "";
                                        
                                      if($taxadvice!='' || isset($enabled_service[15]))
                                       {
                                       ?>
                                       
                                 <tr>
                                    <td>Beneficial Ownership Registration - Overseas</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="taxadvice[tab]" data-id="taxadvice" <?php echo $taxadvice;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="taxadvice[reminder]" <?php echo $taxadvicere;?> data-id="enabletaxad"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="taxadvice[text]" <?php echo $taxadvicetext;?> data-id="fifteent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="taxadvice[invoice]" <?php echo $taxadviceinvoice;?> data-id="intaxadv"></td>
                                 </tr>
                                    <?php    
                                      }                                    
                                        //for show assignees
                                        $client_enabled_service[5] = $vat = ( $tab_on['vat']['tab']!==0 ) ?  "checked='checked'" : "";   

                                        $vatre = ( $tab_on['vat']['reminder']!==0 ) ?  "checked='checked'" : "";

                                        $vattext = ( $tab_on['vat']['text']!==0 ) ? "checked='checked'" : "";

                                        $vatinvoice = ( $tab_on['vat']['invoice']!==0 ) ? "checked='checked'" : "";

                                        if($vat!='' || isset($enabled_service[5]) )
                                        {
                                        
                                       ?>
                                 <tr>
                                    <td>VAT Returns</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="vat[tab]" data-id="vat" <?php echo $vat;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="vat[reminder]" <?php echo $vatre;?> data-id="enablevat"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="vat[text]" <?php echo $vattext;?> data-id="sevent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="vat[invoice]" <?php echo $vatinvoice;?> data-id="invt"></td>
                                 </tr>                                 
                                    <?php 
                                      }

                                        //for show assignees
                                        $client_enabled_service[ 7 ] = $work = ( $tab_on['workplace']['tab']!==0 ) ?  "checked='checked'" : "";   
                                         
                                        $workre = ( $tab_on['workplace']['reminder']!==0 ) ?  "checked='checked'" : "";

                                        $worktext = ( $tab_on['workplace']['text']!==0 ) ? "checked='checked'" : "";

                                        $workinvoice = ( $tab_on['workplace']['invoice']!==0 ) ? "checked='checked'" : "";

                                        if($work!='' || isset($enabled_service[7]) )
                                        {
                                          
                                       ?>
                                 <tr>
                                    <td>WorkPlace Pension - AE</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="workplace[tab]" data-id="workplace" <?php echo $work;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="workplace[reminder]" <?php echo $workre;?> data-id="enablework"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="workplace[text]" <?php echo $worktext;?> data-id="sixt"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="workplace[invoice]" <?php echo $workinvoice;?> data-id="inpn"></td>
                                 </tr>
                                    <?php 
                                       }

                                         $inves = ( $tab_on['taxinvest']['tab']!==0 ) ?  "checked='checked'" : "";   

                                        $invesre = ( $tab_on['taxinvest']['reminder']!==0 ) ?  "checked='checked'" : "";

                                        $investext = ( $tab_on['taxinvest']['text']!==0 ) ? "checked='checked'" : "";

                                        $invesinvoice = ( $tab_on['taxinvest']['invoice']!==0 ) ? "checked='checked'" : "";

                                       ?>
                                 <tr style="display: none;">
                                    <td>Tax Investigation</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="taxinvest[tab]" data-id="taxinvest" <?php echo $inves;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="taxinvest[reminder]" <?php echo $invesre;?> data-id="enabletaxinves"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="taxinvest[text]" <?php echo $investext;?> data-id="sixteent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="taxinvest[invoice]" <?php echo $invesinvoice;?> data-id="intaxinves"></td>
                                 </tr>
                            <?php
                            //for contact person personal tax return  
                            $i = 1;
                            $client_enabled_service[ 4 ] = "";
                            foreach ($contactRec as $key => $contactRec_val) {
                              $ser    =  ( $contactRec_val['has_ptr_service'] == 1 ?"checked='checked'":'' );
                              $rem    =  ( $contactRec_val['has_ptr_reminder'] == 1 ?"checked='checked'":'' );
                              $inv    =   ( $contactRec_val['has_ptr_invoice'] == 1 ?"checked='checked'":'' );
                              $text   =   ( $contactRec_val['has_ptr_text'] == 1 ?"checked='checked'":'' );
                              ?>
                              <tr>
                                <td>
                                    Personal Tax Return
                                    <b class="ccp_name_<?php echo $i;?>"> - <?php echo $contactRec_val['first_name'].' '.$contactRec_val['last_name'];?></b>
                                </td>
                                <td class="switching">
                                    <input type="checkbox" class="js-small f-right services ccp" name="ccp_ptr_service[<?php echo $i;?>]" data-id="ccp_<?php echo $i?>" value="1" <?php echo $ser;?>>
                                </td>
                                <td class="swi1 splwitch">
                                    <input type="checkbox" class="js-small f-right reminder ccp" name="ccp_ptr_reminder[<?php echo $i;?>]"  data-id="ccp_<?php echo $i?>_reminder" value="1" <?php echo $rem;?> >
                                </td>
                                <td class="swi2 textwitch">
                                    <input type="checkbox" class="js-small f-right text ccp" name="ccp_ptr_text[<?php echo $i;?>]" data-id="ccp_<?php echo $i?>" value="1" <?php echo $inv;?> > 
                                </td>
                                <td class="swi3 invoicewitch">
                                    <input type="checkbox" class="js-small f-right invoice ccp" name="ccp_ptr_invoice[<?php echo $i;?>]"  data-id="ccp_<?php echo $i?>" value="1" <?php echo $text;?> >
                                </td>
                            </tr>                              
                              <?php
                              if($contactRec_val['has_ptr_service'] == 1 ) $client_enabled_service[ 4 ]  = 1;
                            $i++;
                            }

                            /*$client_services = $this->Common_mdl->getServicelist();

                             foreach($client_services as $key => $value) 
                             {
                                 if($value['id']>16)
                                 {

                                 $tab = ($tab_on[$value['services_subnames']]['tab']!==0 ) ?  "checked='checked'" : "";                        
                                 $reminder = ($tab_on[$value['services_subnames']]['reminder']!==0 ) ?  "checked='checked'" : "";
                                 $text = ($tab_on[$value['services_subnames']]['text']!==0 ) ? "checked='checked'" : "";
                                
                                 $invoice = ($tab_on[$value['services_subnames']]['invoice']!==0) ? "checked='checked'" : "";
                                 

                               if($tab!='' || isset($enabled_service[$value['id']]))
                               {
                                ?>
                               
                          <tr>
                             <td><?php echo $value['service_name']; ?></td>
                             <td class="switching"><input type="checkbox" class="js-small other_ser f-right services" name="<?php echo $value['services_subnames']; ?>[tab]" data-id="tab_<?php echo $value['services_subnames']; ?>"<?php echo $tab;?>></td>
                             <td class="swi1 splwitch"><input type="checkbox" class="js-small other_ser f-right reminder" name="<?php echo $value['services_subnames']; ?>[reminder]" <?php echo $reminder;?> data-id="enable_<?php echo $value['services_subnames']; ?>"></td>
                             <td class="swi2 textwitch"><input type="checkbox" class="js-small other_ser f-right text" name="<?php echo $value['services_subnames']; ?>[text]" <?php echo $text;?> data-id="twot<?php echo $value['services_subnames']; ?>"></td>
                             <td class="swi3 invoicewitch"><input type="checkbox" class="js-small other_ser f-right invoice" name="<?php echo $value['services_subnames']; ?>[invoice]" <?php echo $invoice;?> data-id="inas<?php echo $value['services_subnames']; ?>"></td>
                          </tr>

                        <?php } } }*/ ?>  
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Don't move this position to any where -->
               <!-- service -->

               <div id="assignto" class="tab-pane fade">
                  <div class="space-required">
                      <div class="main-pane-border1" id="assignto_error">
                         <div class="alert-ss"></div>
                      </div>
                  </div>
                  <div class="form-group">
                    <div class="client_assignees">
                      <label class="col-form-label">Client Assignees</label> 
                      <input type="text" id="tree_select" placeholder="Select">
                      <!-- IT's only fro assinees validation -->
                      <input type="hidden" id="tree_select_values" name="assignees_values" placeholder="Select">
                    </div>
                    <div class="Service_Assignee_Table" style="<?php echo $import_tab_on;?>">
                      <table>
                        <thead>
                          <tr>
                            <th>Service</th>
                            <th>Manager</th>
                            <th>Assignee</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 

                          foreach ($enabled_service_detail as $key => $value) {
                            $IS_enable = ""; 
                            if( empty( $client_enabled_service[ $value['id'] ] ) ) $IS_enable ="display:none";                          
                            ?>
                          <tr id="service_assignee_tr_<?php echo $value['id'];?>" style="<?php echo $IS_enable ?>">
                            <td>
                              <?php echo $value['service_name'];?>
                            </td>
                            <td>
                              <input type="text" id="service_manager_<?php echo $value['id'];?>">
                              <input type="hidden" name="service_manager[<?php echo $value['id'];?>]" >
                            </td>
                            <td>
                              <input type="text" id="service_assignee_<?php echo $value['id'];?>">
                              <input type="hidden" name="service_assignee[<?php echo $value['id'];?>]">
                            </td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>                   
               </div>
               <!-- assignto --> 
<!-- import_tab open -->
<div id="import_tab" class="tab-pane fade">
  <div class="space-required">
      <div class="main-pane-border1 cmn-errors1" id="services_information_error">
         <div class="alert-ss"></div>
      </div>
      <?php $this->load->view('clients/service_content_view',['tab_on'=>$tab_on,'uri_seg'=>$uri_seg ]); ?>
  </div>
</div>
<!-- import_tab close -->
  

               <!-- Filing Allocation -->
                                    <div id="filing-allocation" class="tab-pane fade ">
                                       
                                       <div class="masonry-container floating_set ">
                                          <div class="grid-sizer"></div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Filing Allocation</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Company Number</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_cmyno" id="fa_cmyno" value="<?php if(isset($client_legalform['company_no'])){ echo $client_legalform['company_no']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts date_birth">
                                                         <label class="col-sm-4 col-form-label">Incorporation Date</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker da" placeholder="dd-mm-yyyy" type="text" name="fa_incordate" id="fa_incordate" value="<?php if(isset($client_legalform['incorporation_date'])){ echo $client_legalform['incorporation_date']; } ?>"/>
                                                         </div>
                                                      </div>   
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Registered Address</label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="3" placeholder="" name="fa_registadres" id="fa_registadres" class="fields"><?php if(isset($client_legalform['regist_address'])){ echo $client_legalform['regist_address']; } ?></textarea>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Turn Over</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_turnover" id="fa_turnover" value="<?php if(isset($client_legalform['turnover'])){ echo $client_legalform['turnover']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row assign_cus_type date_birth">
                                                         <label class="col-sm-4 col-form-label">Date Of Trading</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="fa_dateoftrading" id="fa_dateoftrading" value="<?php if(isset($client_legalform['date_of_trading'])){ echo $client_legalform['date_of_trading']; } ?>"/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields spanassign" >
                                                         <label class="col-sm-4 col-form-label">SIC Code</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" name="fa_siccode" id="fa_siccode" placeholder="" class="fields" value="<?php if(isset($client_legalform['sic_code'])){ echo $client_legalform['sic_code']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Nuture Of Business</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_nutureofbus" id="fa_nutureofbus" value="<?php if(isset($client_legalform['nuture_of_business'])){ echo $client_legalform['nuture_of_business']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Company UTR</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_cmyutr" id="fa_cmyutr" value="<?php if(isset($client_legalform['company_utr'])){ echo $client_legalform['company_utr']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Companies House Authorisation Code</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_cmyhouse" id="fa_cmyhouse" value="<?php if(isset($client_legalform['company_house_auth_code'])){ echo $client_legalform['company_house_auth_code']; } ?>">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->   
                                          
                                       </div>
                                    </div>
                                    <!-- Filing Allocation -->

                                 <!-- Business Details -->
                                    <div id="business-det" class="tab-pane fade ">
                                       <div class="masonry-container floating_set ">
                                          <div class="grid-sizer"></div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Business Details</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Trading As</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="bus_tradingas" id="bus_tradingas" value="<?php if(isset($client_legalform['trading_as'])){ echo $client_legalform['trading_as']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts date_birth">
                                                         <label class="col-sm-4 col-form-label">Commenced Trading</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker datepicker" placeholder="dd-mm-yyyy" type="text" name="bus_commencedtrading" id="bus_commencedtrading" value="<?php if(isset($client_legalform['commenced_trading'])){ echo $client_legalform['commenced_trading']; } ?>"/>
                                                         </div>
                                                      </div>   
                                                      <div class="form-group row radio_bts date_birth">
                                                         <label class="col-sm-4 col-form-label">Registered for SA</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker datepicker" placeholder="dd-mm-yyyy" type="text" name="bus_regist" id="bus_regist" value="<?php if(isset($client_legalform['register_sa'])){ echo $client_legalform['register_sa']; } ?>"/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Turn Over</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="bus_turnover" id="bus_turnover" value="<?php if(isset($client_legalform['bus_turnover'])){ echo $client_legalform['bus_turnover']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Nuture Of Business</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="bus_nutureofbus" id="bus_nutureofbus" value="<?php if(isset($client_legalform['bus_nuture_of_business'])){ echo $client_legalform['bus_nuture_of_business']; } ?>">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->   
                                          
                                       </div>
                                    </div>











              
               
              
                 
               <!-- aml checks -->
               <div id="amlchecks" class="tab-pane fade">
                  <div class="space-required">
                    <div class="main-pane-border1" id="amlchecks_error">
                       <div class="alert-ss"></div>
                    </div>
                 </div>                  
                  <div class="masonry-container floating_set">
                     <div class="grid-sizer"></div>
                    
                     
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Anti Money Laundering Checks    </a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1 CONTENT_ANTI_MONEY_LAUNDERING_CHECKS">
                                 <div class="form-group row radio_bts aml_sort" data-id="<?php echo $Setting_Order['crm_assign_client_id_verified']; ?>" <?php echo $Setting_Delete['crm_assign_client_id_verified']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_assign_client_id_verified']; ?></label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="client_id_verified" id="client_id_verified" <?php if(isset($client['crm_assign_client_id_verified']) && ($client['crm_assign_client_id_verified']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row assign_cus_type client_id_verified_data aml_sort" <?php if(isset($client['crm_assign_client_id_verified']) && ($client['crm_assign_client_id_verified']!==0) ){ }else{ ?> style="display: none;" <?php } ?> data-id="<?php echo $Setting_Order['crm_assign_type_of_id']; ?>" <?php echo $Setting_Delete['crm_assign_type_of_id']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_assign_type_of_id']; ?></label>
                                    <div class="col-sm-8">
                                     <div class="dropdown-sin-11 lead-form-st">
                                       <select name="type_of_id[]" id="person" class="form-control fields assign_cus" placeholder="Select" multiple="multiple">
                                          <option value="" disabled="disabled">--Select--</option>
                                          <?php
                                          $new_arr=array();
                                          $all_val=(isset($client['crm_assign_type_of_id']) && $client['crm_assign_type_of_id']!='')? explode(',',$client['crm_assign_type_of_id']):$new_arr;
                                           ?>
                                          <option value="Passport" <?php if(in_array('Passport',$all_val)) {?> selected="selected"<?php } ?>>Passport</option>
                                          <option value="Driving_License" <?php if(in_array('Driving_License',$all_val)) {?> selected="selected"<?php } ?>>Driving License</option>
                                          <option value="Other_Custom" <?php if(in_array('Other_Custom',$all_val)) {?> selected="selected"<?php } ?>>Other Custom</option>
                                       </select>
                                       </div>
                                    </div>
                                 </div>
                                <!-- 29-08-2018 for multiple image upload option -->
                                       <div class="form-group row aml_sort" data-id="<?php echo $Setting_Order['proof_attach_file']; ?>" <?php echo $Setting_Delete['proof_attach_file']; ?>>
                                             <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['proof_attach_file']; ?></label>
                                             <div class="col-sm-8">
                       <div class="custom_upload">
                                                <label for="proof_attach_file"></label><input type="file" name="proof_attach_file[]" id="proof_attach_file" multiple="multiple" >
                        </div>
                                             </div>
                                      
                                       <div class="attach-files showtm f-right">
                                       <?php 
                                       if(isset($client['proof_attach_file'])){
                                          ?>
                                       <div class="jFiler-items jFiler-row">
                                           <ul class="jFiler-items-list jFiler-items-grid">
                                          <?php
                                       $ex_attach=array_filter( explode( ',', $client['proof_attach_file'] ) );
                                       foreach ($ex_attach as $attach_key => $attach_value)
                                       {
                                             $replace_val=str_replace(base_url(),'',$attach_value);
                                              $ext = explode(".", $replace_val);
                                              $ext  = end( $ext );
                                           
                                             $res=$this->Common_mdl->geturl_image_or_not($ext);
                                            // echo $res;
                                             if($res=='image'){
                                                ?>
                                             <li class="jFiler-item for_img_<?php echo $attach_key; ?>" data-jfiler-index="3" style="">
                                               <input type="hidden" name="already_upload_img[]" value="<?php echo $attach_value; ?>" >
                                                 <div class="jFiler-item-container">
                                                    <div class="jFiler-item-inner">
                                                       <div class="jFiler-item-thumb">
                                                          <div class="jFiler-item-status"></div>
                                                          <div class="jFiler-item-info">                                        
                                                                                         
                                                          </div>
                                                          <div class="jFiler-item-thumb-image"><img src="<?php echo base_url().$attach_value;?>" draggable="false"></div>
                                                       
                                                    </div>
                                                    <div class="jFiler-item-assets jFiler-row">
                                                          <ul class="list-inline pull-left">
                                                             <li>
                                                                <div class="jFiler-jProgressBar" style="display: none;">
                                                                   <div class="bar"></div>
                                                                </div>
                                                               
                                                             </li>
                                                          </ul>
                                                          <ul class="list-inline pull-right">
                                                             <li><a class="icon-jfi-trash jFiler-item-trash-action remove_file" data-id="<?php echo $attach_key ?>"></a></li>
                                                          </ul>
                                                       </div>
                                                 </div>
                                              </li>
                                                <?php
                                                } // if image
                                                else{ ?>
                                              <li class="jFiler-item jFiler-no-thumbnail for_img_<?php echo $attach_key; ?>" data-jfiler-index="2" style="">
                                               <input type="hidden" name="already_upload_img[]" value="<?php echo $attach_value; ?>" >
                                                 <div class="jFiler-item-container">
                                                    <div class="jFiler-item-inner">
                                                       <div class="jFiler-item-thumb">
                                                          <div class="jFiler-item-status"></div>
                                                          <a href="<?php echo base_url().$attach_value; ?>" target="_blank" ><div class="jFiler-item-info">                                                               </div></a>
                                                          <div class="jFiler-item-thumb-image"><span class="jFiler-icon-file f-file f-file-ext-odt" style="background-color: rgb(63, 79, 211);"><?php echo $attach_value; ?></span></div>
                                                       </div>
                                                       <div class="jFiler-item-assets jFiler-row">
                                                          <ul class="list-inline pull-left">
                                                             <li>
                                                                <div class="jFiler-jProgressBar" style="display: none;">
                                                                   <div class="bar"></div>
                                                                </div>
                                                               
                                                             </li>
                                                          </ul>
                                                          <ul class="list-inline pull-right">
                                                             <li><a class="icon-jfi-trash jFiler-item-trash-action remove_file" data-id="<?php echo $attach_key ?>" ></a></li>
                                                          </ul>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </li>
                                             <?php } //else end

                                               } // foreach
                                               ?>
                                               </ul>
                                               </div>
                                               <?php
                                          }
                                       ?>
                                      </div>
                                       </div>
                                 <!-- en dof image upload for type of ids -->
                                 <div class="form-group row name_fields spanassign for_other_custom_choose aml_sort" <?php if(in_array('Other_Custom',$all_val) && (isset($client['crm_assign_client_id_verified']) && ($client['crm_assign_client_id_verified']!==''))) { }else { ?> style="display:none" <?php } ?>  data-id="<?php echo $Setting_Order['crm_assign_other_custom']; ?>" <?php echo $Setting_Delete['crm_assign_other_custom']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_assign_other_custom']; ?></label>
                                    <div class="col-sm-8">
                                       <input type="text" name="assign_other_custom" id="assign_other_custom" placeholder="" value="<?php if(isset($client['crm_assign_other_custom']) && ($client['crm_assign_other_custom']!='') ){ echo $client['crm_assign_other_custom'];}?>" class="fields">
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts aml_sort" data-id="<?php echo $Setting_Order['crm_assign_proof_of_address']; ?>" <?php echo $Setting_Delete['crm_assign_proof_of_address']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_assign_proof_of_address']; ?></label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="proof_of_address" id="proof_of_address" <?php if(isset($client['crm_assign_proof_of_address']) && ($client['crm_assign_proof_of_address']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts aml_sort" data-id="<?php echo $Setting_Order['crm_assign_meeting_client']; ?>" <?php echo $Setting_Delete['crm_assign_meeting_client']; ?>  >
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_assign_meeting_client']; ?></label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="meeting_client" id="meeting_client" <?php if(isset($client['crm_assign_meeting_client']) && ($client['crm_assign_meeting_client']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                                         
                  </div>
               </div>
               <!-- end of aml checks -->
               <div id="other" class="tab-pane fade">
                 <div class="space-required ">
                    <div class="main-pane-border1" id="other_error">
                       <div class="alert-ss"></div>
                    </div>
                 </div>
                  <div class="masonry-container floating_set">
                     <div class="grid-sizer"></div>
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg"><?php echo $Setting_Label['crm_previous_accountant'];?>

                                       <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" <?php if(isset($client['crm_previous_accountant']) && ($client['crm_previous_accountant']=='on') ){?> checked="checked"<?php } ?> data-id="preacc" >
                                 </a>
                              </h3>
                           </div>
                           <?php 
                                    (isset($client['crm_previous_accountant']) && $client['crm_previous_accountant'] == 'on') ? $pre =  "block" : $pre = 'none';
                            ?>
                           <div id="collapse" class="panel-collapse preacc_content_toggle" style="display:<?php echo $pre;?>;">
                              <div class="basic-info-client1 CONTENT_PREVIOUS_ACCOUNTS" id="others_box1">
                             
                                 
                                    <div class="form-group row name_fields others_details_sort" data-id="<?php echo $Setting_Order['crm_other_name_of_firm']; ?>" <?php echo $Setting_Delete['crm_other_name_of_firm']; ?>>
                                       <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_other_name_of_firm']; ?> </label>
                                       <div class="col-sm-8">
                                          <input type="text" name="name_of_firm" id="name_of_firm" placeholder="" value="<?php if(isset($client['crm_other_name_of_firm']) && ($client['crm_other_name_of_firm']!='') ){ echo $client['crm_other_name_of_firm'];}?>" class="fields">
                                       </div>
                                    </div>
                                    <div class="form-group row others_details_sort" data-id="<?php echo $Setting_Order['crm_other_address']; ?>" <?php echo $Setting_Delete['crm_other_address']; ?>>
                                       <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_other_address']; ?></label>
                                       <div class="col-sm-8">
                                          <textarea rows="4" name="other_address" id="other_address" class="form-control fields"><?php if(isset($client['crm_other_address']) && ($client['crm_other_address']!='') ){ echo $client['crm_other_address'];}?></textarea>
                                       </div>
                                    </div>
                                 
                                 <!-- preacc close-->
                                 <div class="form-group row name_fields  others_details_sort" data-id="<?php echo $Setting_Order['crm_other_contact_no']; ?>" >
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_other_contact_no']; ?></label>
                                    <div class="col-sm-8">
                                       <!-- <input type="number" name="other_contact_no" id="other_contact_no" placeholder="" value="<?php if(isset($user['crm_phone_number']) && ($user['crm_phone_number']!='') ){ echo $user['crm_phone_number'];}?>" class="fields"> -->
                                       <!-- <input type="number" class="con-code" name="cun_code" name="cun_code" value="<?php if(isset($client['crm_other_contact_no']) && ($client['crm_other_contact_no']!='') ){ echo substr($client['crm_other_contact_no'], 0, 2);} ?>"> -->
                                       <div class="country_select_div">
                                       <select class="con-code" name="cun_code" placeholder="Country Code">
                                        <?php 
                                          foreach ( $countries as $value )
                                          {
                                            $select = "";
                                            if( !empty($client) )
                                            {
                                              $code = explode('-', $client['crm_other_contact_no']);
                                              if( $code[0] == $value['id'] )
                                              {
                                                $select = "selected='selected'"; 
                                              }
                                            }
                                            echo "<option value='".$value['id']."' ".$select.">".$value['sortname']."</option>";
                                          }
                                        ?>
                                        </select>
                                       </div>

                                       <input type="number" name="pn_no_rec" id="pn_no_rec" class="fields" value="<?php if(isset($client['crm_other_contact_no']) && ($client['crm_other_contact_no']!='') ){
                                        $ph = explode('-', $client['crm_other_contact_no']);
                                        echo $ph[1];
                                        } ?>">
                                    </div>
                                 </div>
                                 <div class="form-group row name_fields others_details_sort " data-id="<?php echo $Setting_Order['crm_other_email']; ?>" <?php echo $Setting_Delete['crm_other_email']; ?> >
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_other_email']; ?></label>
                                    <div class="col-sm-8">
                                       <input type="email" name="emailid" id="emailid" placeholder="" value="<?php if(isset($client['crm_other_email']) && ($client['crm_other_email']!='') ){ echo $client['crm_other_email'];}?>" class="fields">
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts others_details_sort " data-id="<?php echo $Setting_Order['crm_other_chase_for_info']; ?>" <?php echo $Setting_Delete['crm_other_chase_for_info']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_other_chase_for_info']; ?></label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="chase_for_info" data-id="chase_for_info" id="chase_for_info" <?php if(isset($client['crm_other_chase_for_info']) && ($client['crm_other_chase_for_info']=='on') ){?> checked="checked"<?php } ?> value="on">
                                    </div>
                                 </div>
                                 
                                
                                 <div class="form-group row for_other_notes others_details_sort" data-id="<?php echo $Setting_Order['crm_other_notes']; ?>" <?php if(isset($client['crm_other_chase_for_info']) && ($client['crm_other_chase_for_info']=="on") ){ ?> <?php }else{ ?> style="display:none;" <?php } ?> >
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_other_notes']; ?></label>
                                    <div class="col-sm-8">
                                       <textarea rows="4" name="other_notes" id="other_notes" class="form-control fields"><?php if(isset($client['crm_other_notes']) && ($client['crm_other_notes']!='') ){ echo $client['crm_other_notes'];}?></textarea>
                                    </div>
                                 </div>
                                
                      
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Additional Information - Internal Notes</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1 CONTENT_ADDITIONAL_INFORMATION-INTERNAL_NOTES" id="others_box2">
                                 <div class="form-group row others_details_sort" data-id="<?php echo $Setting_Order['crm_other_internal_notes']; ?>" <?php echo $Setting_Delete['crm_other_internal_notes']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_other_internal_notes']; ?></label>
                                    <div class="col-sm-8">
                                       <textarea rows="4" name="other_internal_notes" id="other_internal_notes" class="form-control fields"><?php if(isset($client['crm_other_internal_notes']) && ($client['crm_other_internal_notes']!='') ){ echo $client['crm_other_internal_notes'];}?></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                       <!-- accordion-panel for client login-->
                       <?php 
                          $showLogin = "display:none;";
                          $showLoginchek = "";
                          if( !empty( $client['crm_other_invite_use'] ) )
                          {
                            $showLogin = "display:block";
                            $showLoginchek = "checked='checked'";
                          }                        
                       ?>
                       
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                <a class="accordion-msg" style="display: inline-block;">
                                  <?php echo $Setting_Label['crm_other_invite_use'];?>
                                  <input type="checkbox" class="js-small f-right fields" name="invite_use" id="invite_use"  data-id="show_login" value="1" <?php echo $showLoginchek;?> >
                                </a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse show_login" style="<?php echo $showLogin;?>">
                              <div class="basic-info-client1 CONTENT_CLIENT_LOGIN" id="others_box3">
                                    <!-- 30-08-2018 -->
                                 <div class="form-group row name_fields others_details_sort" data-id="<?php echo $Setting_Order['usertable_username']; ?>" <?php echo $Setting_Delete['usertable_username']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['usertable_username']; ?></label>
                                    <div class="col-sm-8">
                                       <input type="text" name="user_name" id="username" placeholder="User Name" value="<?php if(!empty($user['username']) && ($user['username']!='') ){ echo $user['username'];}?>" data-id="<?php echo (!empty($user['id'])?$user['id']:'') ?>" class="fields user_name">
                                        <span class="v_err" style="color:red;display:none">User name already Exists</span>
                                    </div>
                                 </div>
                                 <div class="form-group row name_fields others_details_sort" data-id="<?php echo $Setting_Order['usertable_password']; ?>" <?php echo $Setting_Delete['usertable_password']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['usertable_password']; ?></label>
                                    <div class="col-sm-8">
                                       <input type="password" name="password" id="password" placeholder="Password" value="<?php if(isset($user['confirm_password']) && ($user['confirm_password']!='') ){ echo $user['confirm_password'];}?>" class="fields">
                                    </div>
                                 </div>
                                  <div class="form-group row name_fields others_details_sort" data-id="<?php  $query=$Setting_Order['usertable_confirm_password']; echo $query+1; ?>" <?php echo $Setting_Delete['usertable_confirm_password']; ?>>
                                       <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['usertable_confirm_password']; ?></label>
                                       <div class="col-sm-8">
                                          <input type="password" name="confirm_password" id="confirm_password" placeholder="confirm password" value="<?php if(isset($user['confirm_password']) && ($user['confirm_password']!='') ){ echo $user['confirm_password'];}?>" class="fields"> 
                                       </div>
                                    </div>
                                 <!-- end of 30-08-2018 -->
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                      <!-- accordion-panel --> 
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Client Source</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1 CONTENT_CLIENT_SOURCE" id="others_box4">
                                 <div class="form-group row others_details_sort" data-id="<?php echo $Setting_Order['crm_assign_source']; ?>" <?php echo $Setting_Delete['crm_assign_source']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_assign_source']; ?></label>
                                    <div class="col-sm-8">
                                       <select name="source" id="source" class="form-control fields">
                                          <option value="" selected="selected">--Select--</option>
                                          <option value="Google" <?php if(isset($client['crm_assign_source']) && $client['crm_assign_source']=='Google') {?> selected="selected"<?php } ?>>Google</option>
                                          <option value="FB" <?php if(isset($client['crm_assign_source']) && $client['crm_assign_source']=='FB') {?> selected="selected"<?php } ?>>FB</option>
                                          <option value="Website" <?php if(isset($client['crm_assign_source']) && $client['crm_assign_source']=='Website') {?> selected="selected"<?php } ?>>Website</option>
                                          <option value="Existing Client" <?php if(isset($client['crm_assign_source']) && $client['crm_assign_source']=='Existing Client') {?> selected="selected"<?php } ?>>Existing Client</option>
                                       </select>
                                    </div>
                                 </div>
                              
                                 <div class="form-group row name_fields dropdown_source others_details_sort" <?php if(isset($client['crm_assign_source']) && $client['crm_assign_source']=='Existing Client') {?> <?php }else{ ?> style="display: none;" <?php } ?> data-id="<?php echo $Setting_Order['crm_refered_by']; ?>" <?php echo $Setting_Delete['crm_refered_by']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_refered_by']; ?></label>
                                    <div class="col-sm-8 ">
                                    <div class="dropdown-sin-3">
                                       <!--  <input type="text" name="refer_exist_client" id="refer_exist_client" placeholder="" value="" class="fields"> -->
                                       <select name="refered_by" id="refered_by" class="fields">
                                          <option value="">select</option>
                                          <?php foreach ($referby as $referbykey => $referbyvalue) {
                                             # code...
                                             ?>
                                          <option value="<?php echo $referbyvalue['id'];?>" <?php if(isset($client['crm_refered_by']) && $client['crm_refered_by']==$referbyvalue['id']) {?> selected="selected"<?php } ?>><?php echo $referbyvalue['crm_company_name']."-".$referbyvalue['crm_first_name'];?></option>
                                          <?php } ?>
                                       </select>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <?php
                                    $crm_assign_relationship_client=( isset($client['crm_assign_relationship_client']) ? $client['crm_assign_relationship_client'] : false);   
                                     $crm_assign_relationship=render_custom_fields_edit( 'relationship_client',$rel_id,$crm_assign_relationship_client);  
                                     if($crm_assign_relationship!='')
                                     {
                                      echo $crm_assign_relationship;
                                     }
                                     else
                                     {
                                    ?>
                                 <div class="form-group row name_fields others_details_sort" data-id="<?php echo $Setting_Order['crm_assign_relationship_client']; ?>" <?php echo $Setting_Delete['crm_assign_relationship_client']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_assign_relationship_client']; ?></label>
                                    <div class="col-sm-8">
                                       <input type="text" name="relationship_client" id="relationship_client" placeholder="" value="<?php if(isset($client['crm_assign_relationship_client']) && ($client['crm_assign_relationship_client']!='') ){ echo $client['crm_assign_relationship_client'];}?>" class="fields">
                                    </div>
                                 </div>
                                 <?php } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <!-- accordion-panel -->
                     <div class="accordion-panel" style="display: none;">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Notes if any other</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="others_box5">
                                 <div class="form-group row others_details_sort" data-id="<?php echo $Setting_Order['crm_other_any_notes']; ?>" <?php echo $Setting_Delete['crm_other_any_notes']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Order['crm_other_any_notes']; ?></label>
                                    <div class="col-sm-8">
                                       <textarea rows="4" name="other_any_notes" id="other_any_notes" class="form-control fields"><?php if(isset($client['crm_other_any_notes']) && ($client['crm_other_any_notes']!='') ){ echo $client['crm_other_any_notes'];}?></textarea>
                                    </div>
                                 </div>
                                 <?php
                                   // echo render_custom_fields_one( 'other',$rel_id);  ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                  </div>
               </div>
               <!-- other -->
               <!-- referral tab section -->
               <div id="referral" class="tab-pane fade">
                  <div class="masonry-container floating_set">
                     <div class="grid-sizer"></div>
                     
                 
                     <!-- accordion-panel -->
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Client Referral</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1 CONTENT_INVITE_CLIENT" id="referals_box1">
                                 
                                 <div class="nonediv" style="display: none;">
                                 <div class="form-group row radio_bts referal_details_sort">
                                    <label class="col-sm-4 col-form-label">crm</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="other_crm" id="other_crm" <?php if(isset($client['crm_other_crm']) && ($client['crm_other_crm']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts referal_details_sort">
                                    <label class="col-sm-4 col-form-label">Proposal</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="other_proposal" id="other_proposal" <?php if(isset($client['crm_other_proposal']) && ($client['crm_other_proposal']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts referal_details_sort">
                                    <label class="col-sm-4 col-form-label">Tasks</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="other_task" id="other_task" <?php if(isset($client['crm_other_task']) && ($client['crm_other_task']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 </div><!-- display none element -->
                                 <div class="form-group row name_fields referal_details_sort" data-id="<?php echo $Setting_Order['crm_other_send_invit_link']; ?>"  <?php echo $Setting_Delete['crm_other_send_invit_link']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_other_send_invit_link']; ?></label>
                                    <div class="col-sm-8">
                                       <input type="text" name="send_invit_link" id="send_invit_link" placeholder="" value="<?php if(isset($client['crm_other_send_invit_link']) && ($client['crm_other_send_invit_link']!='') ){ echo $client['crm_other_send_invit_link'];}?>" class="fields" readonly="readonly">
                                    </div>
                                 </div>
                          
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                    
               </div>
               <!-- referral atb section -->
              
               <!-- confirmation-statement -->
               
             
               <div id="notes" class="tab-pane fade" style="display: none;">
                  <div class="accordion-panel">
                     <div class="box-division03">
                        <div class="accordion-heading" role="tab" id="headingOne">
                           <h3 class="card-title accordion-title">
                              <a class="accordion-msg">Additional Information - Internal Notes</a>
                           </h3>
                        </div>
                        <div id="collapse" class="panel-collapse">
                           <div class="basic-info-client1">
                              <div class="form-group row ">
                                 <label class="col-sm-4 col-form-label">Notes</label>
                                 <div class="col-sm-8">
                                    <textarea rows="4" name="notes_info" id="notes_info" class="form-control fields"><?php if(isset($client['crm_notes_info']) && ($client['crm_notes_info']!='') ){ echo $client['crm_notes_info'];}?></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- accordion-panel -->
               </div>
               <!-- Notes tab close -->
               <div id="documents" class="tab-pane fade">
                  <div class="accordion-panel">
                     <div class="box-division03">
                        <div class="accordion-heading" role="tab" id="headingOne">
                           <h3 class="card-title accordion-title">
                              <a class="accordion-msg">Documents</a>
                           </h3>
                        </div>
                        <div id="collapse" class="panel-collapse">
                           <div class="basic-info-client1">
                              <div class="form-group row">
                                 <label class="col-sm-4 col-form-label">upload Document</label>
                                 <div class="col-sm-8">
                                    <input type="file" name="document" id="document" class="fields">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- accordion-panel -->
               </div>
               <!-- documents tab close -->
              
               <!-- task tab tab close -->
               <div class="space-required" style="<?php echo (!empty($client)?'display: block':'display: none');?>">
                  <div class="comp_cre"> 
                     <span><?php if(isset($client['created_date'])){ echo 'Company Created'.' '.date('F j, Y, g:i',$client['created_date']); } ?></span>
                     <span><?php if(isset($user['CreatedTime'])){ echo ';Updated'.' '.date('F j, Y, g:i',$user['CreatedTime']); } ?></span>
                  </div>
               </div>
            </div>
            <!-- tabcontent -->
         </div>
         <div class="floation_set text-right accordion_ups">
            <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client['user_id']) && ($client['user_id']!='') ){ echo $client['user_id'];}?>" class="fields">
            <input type="hidden" name="client_id" id="client_id" value="<?php if(isset($user['client_id']) && ($user['client_id']!='') ){ echo $user['client_id'];}?>" class="fields">
            <input type="hidden" id="company_house" name="company_house" value="<?php if(isset($user['company_roles']) && ($user['company_roles']!='') ){ echo $user['company_roles'];}?>" class="fields">
            <!-- <input type="submit" class="add_acc_client" value="Save & Exit" id="submit"/> -->
         </div>
      </div>
      <!-- information-tab -->
    </div>
   </form>
</section>
<!-- client-details-view -->
<!-- <?php  
   foreach ($contactRec as $contactRec_key => $contactRec_value) { 
   $id   = $contactRec_value['id']; 
      ?>
<div id="modalcontact<?php echo $id;?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
      
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align-last: center">contact Delete</h4>
         </div>
         <div class="modal-body">
            <p>Are you sure want to delete?</p>
         </div>
         <div class="modal-footer">
            <a href="#" class="delcontact" data-value="<?php echo $id;?>">Delete</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<?php  } ?> -->
<!--modal 2-->


<div class="modal fade remind_popup_client" id="myReminders" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reminders</h4>
         </div>
         <!--   <form id="insert_form1" class="required-save client-firm-info1 validation" method="post" action="" enctype="multipart/form-data">   -->
<!--          <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client['user_id']) && ($client['user_id']!='') ){ echo $client['user_id'];}?>" class="fields">
 -->
         <input type="hidden" name="client_id" id="client_id" value="<?php if(isset($user['client_id']) && ($user['client_id']!='') ){ echo $user['client_id'];}?>" class="fields">
         <input type="hidden" id="company_house" name="company_house" value="<?php if(isset($user['company_roles']) && ($user['company_roles']!='') ){ echo $user['company_roles'];}?>" class="fields">
         <input type="hidden" name="emailid" id="emailid" placeholder="" value="<?php if(isset($client['crm_email']) && ($client['crm_email']!='') ){ echo $client['crm_email'];}?>" class="fields">
         <input type="hidden" name="user_name" id="username" placeholder="" value="<?php if(isset($user['username']) && ($user['username']!='') ){ echo $user['username'];}?>" class="fields">
         <div class="modal-body">
            <div class="body-popup-content">
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default save" data-dismiss="modal">Close</button>        
         </div>
      </div>
      <!--   </form> -->
   </div>
</div>
<!-- modal-close -->
<!-- modal 2-->

<!-- new person add contact delete modal popup-->
<div id="modalnewperson" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align-last: center">contact Delete</h4>
         </div>
         <div class="modal-body">
            <p>Are you sure want to delete?</p>
         </div>
         <div class="modal-footer">
            <a href="#" class="delnewperson" data-value="">Delete</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- new person add contact delete modal popup-->


<!--modal close-->
<div class="modal fade" id="add-reminder" role="dialog">
   <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close reminder_close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">add reminder</h4>
         </div>
         <div class="alert alert-success-re" style="display:none;"
            ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Remainder Added.</div>
         <div class="alert alert-danger-re" style="display:none;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Not Added.
         </div>
         <form action="" class="validation edit_field_form" method="post" accept-charset="utf-8" novalidate="novalidate">
            <input type="hidden" name="client_contacts_id">
            <div class="modal-body">
               <div class="modal-topsec">
                  <div class="send-invoice">
                     <div class="form-group">
                        <span class="invoice-notify">send</span>
                        <select name="due">
                           <option value="due_by">Before</option>
                           <option value="overdue_by">After</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <span class="days1">days</span>
                        <input type="text" class="due-days" name="days">
                          <span class="error_msg_days" style="color: red;display: none;">This Field is Required</span>
                     </div>
                      <div class="form-group" style="display: none;">
                        <span class="days1">Color Code</span>
                          <input type='text' class="color_picker" name="color"/>
                      </div>
                      <div class="form-group">
                            <span class="days1"> Email Template </span>
                            <select name="email_template" class="email_template">
                            <option value="">Select</option>
                              <?php foreach($reminder_templates as $key => $value1){ ?>              
                              <option value="<?php echo $value1['id']; ?>"><?php echo ucwords($value1['reminder_heading']); ?></option>
                              <?php } ?>
                            </select>

                            <span class="error_msg_email" style="color: red;display: none;">This Field is Required</span>
                      </div>
                  </div>            
                   <div class="append_placeholder">
                   </div>
               </div>
               <!--modal-topsec-->
               <div class="modal-bottomsec">
                  <div class="form-group">
                     <input type="hidden" name="service_id" id="service_id" value="">
                  </div>
                  <div class="form-group">
                  <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="">
                    <span class="error_msg_subject" style="color: red;display: none;">This Field is Required</span>
                    </div>
                  <div class="form-group">
                    <textarea name="message" class="editor" id="add_custom_reminder_content"></textarea>
                  <span class="error_msg_message" style="color: red;display: none;">This Field is Required</span></div>
               </div>
            </div>
            <div class="modal-footer">
               <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
               <!-- <button type="submit" class="reminder-delete">delete</button> -->
               <input type="hidden" name="client_id" value="<?php echo (!empty($client['id'])?$client['id']:''); ?>">
               <input type="hidden" id="clicked_checkbox_name">
               <button type="submit" class="reminder-save">save</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade" id="edit-reminder" role="dialog">
   <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit reminder</h4>
         </div>
      
         <form action="" id="edit_cus_reminder_form" method="post" accept-charset="utf-8" novalidate="novalidate">
            <div class="modal-body">
               <div class="modal-topsec">
                  <div class="send-invoice send-invoice_cls">
                     <div class="form-group">
                        <span class="invoice-notify">send</span>
                        <select name="due">
                           <option value="due_by">Before</option>
                           <option value="overdue_by">After</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <span class="days1">days</span>
                        <input type="text" class="due-days" name="days">
                          <span class="error_msg_days" style="color: red;display: none;">This Field is Required</span>
                     </div>
                     <div class="form-group" style="display: none;">
                        <span class="days1">Color Code</span>
                        <input type="text" class="edit_color_picker" name="color">                          
                     </div>
                     <div class="form-group">
                          <span class="days1">Email Template</span>
                          <select name="email_template" class="email_template">
                            <option value="">Select</option>
                            <?php foreach($reminder_templates as $key => $value1){ ?>              
                            <option value="<?php echo $value1['id']; ?>"><?php echo ucwords($value1['reminder_heading']); ?></option>
                            <?php } ?>
                          </select>

                          <span class="error_msg_email" style="color: red;display: none;">This Field is Required</span>
                    </div>
                  </div>
                   
                  <!-- <div class="insert-placeholder">
                      <span target="message" class="add_merge">{crm_ch_accounts_next_due}</span><br>
                        <span target="message" class="add_merge">{crm_accounts_custom_reminder}</span><br>
                     <div class="form-group">
                        <select name="placeholder" id="placeholder">
                           <option value="">--select placeholder--</option>
                           <option value="4">Confirmation</option>
                           <option value="3">Accounts</option>
                           <option value="5">Company tax return</option>
                           <option value="6">Personal tax return</option>
                           <option value="7">Payroll</option>
                           <option value="8">Workplace Pension</option>
                           <option value="10">CIS</option>
                           <option value="11">CIS SUB</option>
                           <option value="12">P11D</option>
                           <option value="13">Book keeping</option>
                           <option value="14">Management</option>
                           <option value="15">VAT</option>
                        </select>
                     </div>
                  </div> -->
                     <div class="append_placeholder">
                     </div>
               </div>
               <!--modal-topsec-->
               <div class="modal-bottomsec" style="float: none; ">
                  <div class="form-group">
                     <input type="hidden" name="service_id" id="service_id" value="">
                  </div>
                  <div class="form-group" style="position: inherit;">
                  <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="">
                    <span class="error_msg_subject" style="color: red;display: none;">This Field is Required</span>
                  </div>
                  <div class="form-group">
                    <textarea name="message" class="editor" id="edit_custom_reminder_content"></textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
               <!-- <button type="submit" class="reminder-delete">delete</button> -->
               <input type="hidden" name="table_id">
               <button type="submit" class="reminder-save">save</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="add_custom_fields_section" style="display: none;">
<?php
    $static_sections = 
          [        
            'required_information',
            'important_details',
            'AML_check',
            'others',
            'referral'
          ];
    $sections = array_merge( $static_sections , $sections ); 
    //print_r( $sections );
    foreach ($sections as $key => $section_name)
    {
      echo render_custom_fields_one( $section_name ,$rel_id );
    }
?>  
</div>

<div class="modal fade all_layout_modal" id="company_house_contact" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title"> Companines House Contact</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">                
            <div class="main_contacts" style="display:none">                   
            </div>
         </div>
         <!-- modalbody -->
      </div>
   </div>
</div>

<!-- FOR LOGOUT CONFIRMATION -->
<div class="modal fade" id="myAlert" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alert</h4>
         </div>
         <div class="modal-body">
            <p>  
               Do you want to Exit? 
            </p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default no" data-dismiss="modal">No</button> 
            <button type="button" class="btn btn-default exit">Yes</button>        
         </div>
      </div>
   </div>
</div>
<!-- FOR LOGOUT CONFIRMATION -->

<!-- add reminder -->
<?php $this->load->view('includes/footer');?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/client_page.js"></script>-->
<script src="<?php echo base_url();?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/icontains.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<?php $this->load->view('clients/client_validation_rules',['uri_seg'=>$uri_seg]);?>
<script src="<?php echo base_url();?>assets/js/add_client/Custom_Field_handler.js"></script>
<script>
$(document).ready(function() {  
  /*For get new invitation if it new client creation*/

  <?php if( empty($uri_seg) ) { ?>

  $('input[id="company_name"]').blur(function(){
    var term = $(this).val();
    $.ajax({
             url: '<?php echo base_url();?>client/Get_NewClient_Refrral_ByName',
             dataType : 'json',
             type:'post',
             data:{'terms':term},
             beforeSend:function()
             {
              $('#save_exit.sv_ex').addClass('disabled');
             },
             success: function(data)
             {
              $('input[name="send_invit_link"]').val( data['code'] );
              $('#save_exit.sv_ex').removeClass('disabled');
             }
           });
  });

  <?php } ?>

  /*For get new invitation if it new client creation*/

  /*For find edited service*/
  $(document).on("change blur",".LISTEN_CHANGES",function(){
    $(this).closest("div.LISTEN_CONTENT").find("input.UPDATE_LISTENED").val(1);
  });
  /*For find edited service*/

  $(document).on('change',"select.personal_tax_return_required", function(){
    
    var id = $(this).attr('data-id');
    
    console.log( "check val"+ $(this).val() );

    if( $(this).val() == 'yes' )
    {
      console.log( "Inside if "+ $(this).val() );
      $("input[name='ccp_ptr_service["+id+"]']").not(':checked').trigger('click');
    }
    else
    {
      console.log( "Inside else "+ $(this).val() );
      $("input[name='ccp_ptr_service["+id+"]']:checked").trigger('click');
    }
  });

  var tree_select = <?php echo json_encode( GetAssignees_SelectBox() ); ?>;
       $('#tree_select ,input[id^="service_manager_"] , input[id^="service_assignee_"]').comboTree({ 
          source : tree_select,
          isMultiple: true
       });

  $('.hashtag').tagsinput( { allowDuplicates: true } );
  $('.hashtag').on('itemAdded', function(item, tag) {$('.hashtag').tagsinput('items');});   

  //when page load
  var STAR_cnts = $("div#import_tab ul").find(".ccp_ptr_cnt.masonry-container");
  arrange_SATR_custom_fields( STAR_cnts );

  });  

  var client_user_id = "<?php if( !empty( $client['user_id'] ) ){ echo $client['user_id'];}?>";
  var start = new Date();
  start.setFullYear(start.getFullYear() - 120);

  var end = new Date();
  end.setFullYear(end.getFullYear());
  
  var datepicker_config = { 
    dateFormat: 'dd-mm-yy',
    changeMonth: true,
    changeYear: true,
    yearRange: start.getFullYear() + ':' + end.getFullYear()
  };

  var save_client = function( callback ='' )
  {
    $("input[name='user_id']").val( client_user_id );

    //for get custom service reminder templates id to update client id
     var customReminder_id=[];
     $("input[type='checkbox']:checked.cus_reminder").each(function()
        {
           customReminder_id.push( $(this).val() );
        });

    var formData = new FormData($("#insert_form")[0]);
    formData.append('add_reminder_ids',customReminder_id.join());

    //$(".LoadingImage").show();
    $.ajax({
       url: '<?php echo base_url();?>client/insert_client/',
       dataType     : 'json',
       type         : 'POST',
       data         : formData,
       contentType  : false,
       processData  : false,
       success      : function(data) {
        //var Contact_id = $('#user_id').val();
        if( client_user_id == '' )
        {
          var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?id='+data.user_id;

          window.history.pushState({path:newurl},'',newurl);
          client_user_id = data.user_id;

          $("input[id='user_id'] , input[name='user_id']").val( client_user_id );
        }

        if( callback !='')
        {
          callback( show_success );
        }            
       },
       error: function() {
          // alert('faileddd');
        }
     });   
   return false;
  };
  var client_auto_save = debounce( save_client , 5000 );

  var contact_add = function( callback = '' )
  {
    var formData = new FormData( $("#insert_form")[0] );
    //summit require for reminder creation 
    if( callback != '' ) formData.append( 'submit','submit' );

     $.ajax({
      url         : '<?php echo base_url();?>client/update_client_contacts/',
      type        : 'POST',
      dataType    : 'json',
      data        : formData,
      contentType : false,
      processData : false,
      success     : function(data) {

        console.log( data );

        if(data.result == 1)
        {
          $.each( data.data , function(i,v){

            console.log( i+"index"+v ); 
            
            $('input[name="contact_table_id['+i+']"]').val( v );

          } );

          if( callback != '' )callback();
        }
      },
    });
 };
 var clientContact_auto_save = debounce( contact_add , 5000 );

  var show_success = function()
  { 
    $('.LoadingImage').hide();
    $(".alert-success").show();
    $('#save_exit.sv_ex').val( "Submit" );
  };

  var setup_switchery = function( content , class_name='.js-small' )
  {
    var index = switcheryElm.length;
    content.find(class_name).each(function (i, html) {
      var switchery = new Switchery(html, {
                color: '#22b14c',
                jackColor: '#fff',
                size: 'small'
            });
      html.setAttribute( 'data-switcheryId' , index );
      switcheryElm[index] = switchery;
      index++;
    });
  };
  var move_ccp_contents = function()
  {
      $('.contact_form .new_update1:last').find('div.service_tr tr').appendTo("table.service-table-client tbody");
      setup_switchery( $("table.service-table-client tbody tr:last") );

      $(".contact_form .new_update1:last .date_picker_dob").each(function(){
        $(this).datepicker( datepicker_config );       
      });

      $('.contact_form .new_update1:last').find('div.service_content li').appendTo("div#import_tab ul");
      setup_switchery( $("div#import_tab ul li:last") );

      $("div#import_tab ul li:last .date_picker_dob").click(function(){
          $(this).datepicker( datepicker_config );
      });

      
      var STAR_cnt = $("div#import_tab ul li:last").find(".ccp_ptr_cnt.masonry-container");
      arrange_SATR_custom_fields( STAR_cnt );
  };

  

  var update_ccp_name_to_content = function(obj)
  {
    var cntid = $(obj).data('cntid');

    var fname = $("input[name='first_name["+cntid+"]']").val();
    var lname = $("input[name='last_name["+cntid+"]']").val();
    
    $(".ccp_name_"+cntid).text( "-"+fname+" "+lname )
  };
       
  $( ".date_picker_dob" ).datepicker( datepicker_config );

$(document).ready(function(){
     
jQuery.validator.addMethod("contact_no",function(inputtxt ,element)
{
   var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
   console.log(this.optional(element)+"inside contact_no");
  return this.optional(element) || phoneno.test(inputtxt);
       
}, "Enter valid contact number.");

jQuery.validator.addMethod("check_date",function(inputtxt ,element)
{
  var date = inputtxt.split('-');

  if( this.optional(element) )
  {
    return true;
  }
  else
  {
    //Declare Regex 
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var dtArray = inputtxt.match(rxDatePattern); // is format OK?
    
    if ( dtArray == null )
    {
        return false;
    }

    if (dtArray[1] < 1 || date[1]> 31)
    {
      return false;
    }
    
    if (dtArray[3] < 1 || dtArray[3] > 12)
    {
      return false;
    }  
      return true;
  }      
}, "Enter valid date.");

    

$("#insert_form").validate({   
  ignore: false,
  errorPlacement: function(error, element) {     
      if( ! ($(element)[0].nodeName=="INPUT" &&  $(element).attr('type') == 'hidden') )
      {
         error.insertAfter(element);
      }
      var err=error.prop('outerHTML');
     
      if($("#required_information").has(element).length){                    
           $('#required_information_error').append(err);
           $('#required_information_error').find('.field-error').each(function(){
             if($(this).html()!=''){
                $(this).addClass('required-errors');
             }
           });                   
      }          
     if($("#details").has(element).length){
        $('#basic_details_error').append(err);
          $('#basic_details_error').find('.field-error').each(function(){
              if($(this).html()!=''){
                    $(this).addClass('required-errors');
              }
          });                     
     }
     if($("#contacts1").has(element).length){
         $('#main_Contact_error').append(err);
         $('#main_Contact_error').find('.field-error').each(function(){
           if($(this).html()!=''){
              $(this).addClass('required-errors');
           }
         }); 
     }
     if($("#service").has(element).length){
        $('#service_error').append(err);
        $('#service_error').find('.field-error').each(function(){
           if($(this).html()!=''){
              $(this).addClass('required-errors');
           }
        });
     }
     if($("#assignto").has(element).length)
     {
        $('#assignto_error').append(err);

         $('#assignto_error').find('.field-error').each(function(){
            if( $(this).html() != '' )
            {
              $(this).addClass('required-errors');
            }
         });

         if( $(element).parents('.Service_Assignee_Table').length ) 
         {
            $(element).closest('td').append(err);
         }


     }
     if($("#amlchecks").has(element).length){
        $('#amlchecks_error').append(err);
        $('#amlchecks_error').find('.field-error').each(function(){
              if($(this).html()!=''){
                  $(this).addClass('required-errors');
              }
         }); 
     }
     if($("#other").has(element).length){
        $('#other_error').append(err);
        $('#other_error').find('.field-error').each(function(){
              if( $(this).html() != '' ){
                  $(this).addClass('required-errors');
              }
         }); 
     }

     if( $("#referral").has(element).length ){

        $('#referral_error').append(err);
        $('#referral_error').find('.field-error').each(function(){
            if( $(this).html() != '' )
            {
                $(this).addClass('required-errors');
            }
          });

     }

     if($("#import_tab").has(element).length)
    {
      $('#services_information_error').append( err );
      $('#services_information_error').find( '.field-error' ).addClass('required-errors');
    }
  },
  rules: Rules,
  messages:Message,
  errorElement: "span", 
  errorClass: "field-error",                             
  submitHandler: function(){ 
    
    $(".LoadingImage").show();

    save_client( contact_add );
  },
});
  
trigger_contact_person_validation();
 
$( ".datepicker" ).each(function( index ) {
 $(this).removeClass('datepicker');
 $(this).addClass('datepicker-13');
});
  

$( ".datepicker-13" ).datepicker({
  dateFormat: 'dd-mm-yy',
  // minDate:0,
  changeMonth: true,
  changeYear: true,
  onSelect:function(selectedDate){
  //  alert($(this).attr('name'));
       $("input[name='"+$(this).attr('name')+"']").each(function() {
          $(this).attr('value',selectedDate);
         });
      },
});

$(document).on( 'click', '.datepicker-13,.hasDatepicker', function() {
 $( ".datepicker-13,.hasDatepicker" ).datepicker({
     dateFormat: 'dd-mm-yy',
    //   minDate:0,
    changeMonth: true,
    changeYear: true,
    onSelect:function(selectedDate){
   //  alert($(this).attr('name'));
         $("input[name='"+$(this).attr('name')+"']").each(function() {
            $(this).attr('value',selectedDate);
           });
        },
     });
 });
             

$( ".services" ).each(function(  ) {
    if($(this).is(':checked')){               
           $(this).parents('.switching').find('.switchery').addClass('default');
     }
});

$( ".reminder" ).each(function(  ) {
  if($(this).is(':checked')){
         $(this).parents('.splwitch').find('.switchery').addClass('default');  
  }

});

$( ".text" ).each(function(  ) {
  if($(this).is(':checked')){
         $(this).parents('.textwitch').find('.switchery').addClass('default');   
    }
});

$( ".invoice" ).each(function(  ) {
    if($(this).is(':checked')){
           $(this).parents('.invoicewitch').find('.switchery').addClass('default');      
    }
});




$('.checkall').parents('tr').find("th input").each(function(){
  if( $(this).is(":checked") ) $(this).parent().find('.switchery').removeClass('disabled');     
});  

  
/*For add custom service reminder*/
/*======================================================================================================*/

//uncheck custom reminder option when when enable as per firm.
$(document).on('change','.as_per_firm',function(){
  if($(this).prop('checked') == true)
   {
      $(this).closest('.panel-collapse').find('input[type="checkbox"]:checked.cus_reminder').trigger('click');                  
   }
});

//due date validation
$(".due-days").keypress(function (e) {
   //if the letter is not digit then display error and don't type anything
   if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      //display error message
            return false;
    }
 });

//for get placeholder base on selected service( old function not use now )
$(document).on('change','#placeholder',function(e){ 
 // alert('text');
     var query = $(this).val();  
     if(query != '')  
     {  
          $.ajax({  
               url:"<?php echo base_url();?>client/get_placeholder/",  
               method:"POST",  
               data:{query:query},  
               success:function(data)  
               {  
                  $('.append_placeholder').fadeIn();  
                  $('.append_placeholder').html(data); 
                  $(".add_merge").on('click',function() { // bulk checked
                  var target = $(this).attr('target');
                  var name=$(this).html();

                  $('[name='+target+']').append(name);

                  }); 
               }
          });  
     }
     if(query=='')
     {
       $('.append_placeholder').fadeOut();
     }  
}); 
   
//append placeholder inside the content 
$(document).on('click','.add_merge',function() 
{
    var target = $(this).attr('target');  
    var name=$(this).html();    
    var message = $('[name='+target+']').val();
    name = message.concat(name); 
    
    $('[name='+target+']').val(name);       
});

//color code
$(".color_picker").spectrum({
    preferredFormat: "hex",
    color: "#0a0909"
});
$('.sp-dd').html('');

//when close cus reminder popup need to diable cus reminder chekbox
$(".reminder_close").click(function(){
       $('input[name="'+$('#clicked_checkbox_name').val()+'"]').trigger('click');
     }); 

//get emial template content
$(".email_template").change(function(){
  console.log('inside on chnage #email_template');
  var id= $(this).val();
  var form = $(this).closest('form');
   $.ajax({
           url: '<?php echo base_url();?>User/get_template',
           type : 'POST',
           data : {'id':id},                     
           beforeSend: function()
           {
               $(".LoadingImage").show();
           },
           success: function(msg)
           {
              $(".LoadingImage").hide();                   
              var json = JSON.parse(msg); 
              form.find(".invoice-msg").val( json['subject'] );
              var cnt_id  = form.find(".editor").attr('id');
              var body    = tinymce.get( cnt_id ).setContent( json['body'] );
            }
         });
  });

$(".edit_field_form").submit(function () {
// console.log($(".edit_field_form").find("input[name='service_id']").val());
  tinyMCE.triggerSave();
  var clikedForm = $(this); // Select Form
  var user_id = $('#user_id').val();
  var days    = clikedForm.find("input[name='days']").val().trim();
  var subject = clikedForm.find("input[name='subject']").val().trim();
  var cnt_id  = clikedForm.find("textarea[name='message']").attr('id');
  var body    = tinymce.get( cnt_id ).getContent();
  clikedForm.find("textarea[name='message']").val( body );

  if(days=='')
  {
     $('.error_msg_days').show();
  }
  else
  {
     $('.error_msg_days').hide();
  }
  if(subject=='')
  {
     $('.error_msg_subject').show();
  }
  else
  {  

     $('.error_msg_subject').hide();
  }
  if(body=='')
  {

     $('.error_msg_message').show();
  }
  else
  {

     $('.error_msg_message').hide();
  }
   if(days!='' && subject!='' && body!='')
  {
        
     $.ajax({
       type: "POST",
       dataType:'json',
       url: '<?php echo base_url();?>client/AddCustom_ServiceReminder/',
       data: $(this).serialize()+"&user_id="+client_user_id,
       beforeSend:Show_LoadingImg,
       success: function (data) { 
          // Inserting html into the result div
          //console.log(data);
          Hide_LoadingImg();
          if(data.result != 0)
          {
             $('input[name="'+$("#clicked_checkbox_name").val()+'"]').val(data.result);
             $('.alert-success-reminder').show();
             setTimeout(function() { 
                $('.alert-success-reminder').hide();
                //$('#add-reminder').css('display','none');
                $('#add-reminder').modal('hide');
                $('.modal-backdrop.show').hide();
             },1000);
          }        
       },
       error: function(jqXHR, text, error){
          // Displaying if there are any errors
             $('#result').html(error);           
      }
  });
  return false;
  }
  else
  {
      return false;
  }
});

//get custom reminder saved data from server
$('a.date-editop[id^="cus_reminder"]').click(function(){
  var id = $(this).attr('id').split('_');
  $.ajax(
    {
      url:"<?php echo base_url()?>client/Get_CusReminder_Data/"+id[2],
      dataType :'json',
      beforeSend:Show_LoadingImg,
      success:function(data) 
      {
        Hide_LoadingImg();
        $('#edit-reminder').find("select[name='due']").val(data.due_type);
        $('#edit-reminder').find("input[name='days']").val(data.due_days);
        $('#edit-reminder').find("input[name='subject']").val(data.subject);

        var  cnt_id = $('#edit-reminder').find("textarea[name='message']").attr("id");

        tinymce.get( cnt_id ).setContent( data.body );

        $('#edit-reminder').find("input[name='table_id']").val(data.id);
        var  color_code ="#0a0909";
        if(data.headline_color!='')
        {
          color_code = data.headline_color;
        }
        $(".edit_color_picker").spectrum({
          preferredFormat: "hex",                
          color: color_code
        });
        $('.sp-dd').html('');
        $('#edit-reminder').modal('show');
      }
    })
});

// update custom reminder     
$("#edit_cus_reminder_form").validate(
{
rules:
{
  days:{required:true},
  subject:{required:true},
  due:{required:true},
  message:{required:true}
},
submitHandler:function()
{
  var Form = $('#edit_cus_reminder_form');
  tinyMCE.triggerSave();
  var FormData = Form.serialize();
  $.ajax({
      url:"<?php echo base_url()?>client/Update_CusReminder",
      dataType : 'json',
      type : 'POST',
      data : FormData,
      beforeSend:Show_LoadingImg,
      success:function(res)
      {
        Hide_LoadingImg();
        if(res.result)
        { 
          //update subject
          var table_id = Form.find('input[name="table_id"]').val();
          $(".cus_reminder_subject_"+table_id).html( Form.find('input[name="subject"]').val() );


          $('.alert-success-reminder').find('.message').text('Reminder Updated Successfully');
          $('.alert-success-reminder').show();
          setTimeout(function() {$('.alert-success-reminder').hide();$('#edit-reminder').modal('hide');},1000);
        }
      }
  });
}
});
/*===================================================================================================*/

   


/**Client Contact Person Tab Related Script*/
/**=================================================================================================**/
//date of bith validation (CH date).
$(document).on( 'focus', 'input[name^="date_of_birth"]' ,function(){
  var date = $(this).val();

  if( date !='' )
  {
    var date = date.split('-');

    if( date[0] == '00' )
    {
      $(this).datepicker( "setDate" , '01-'+date[1]+'-'+date[2] );
    }
  }
});

//add exsisting contact person from CH
 $(".person").click(function(){
    var companyNo=$("#company_number").val();
    var user_id = '<?php echo $uri_seg; ?>';      
      $.ajax({
          url: '<?php echo base_url();?>client/selectcompany/'+user_id,
          type: 'post',
          dataType: 'JSON',
          data: { 'companyNo':companyNo},
          beforeSend : function(){$('.LoadingImage').show();},
          success: function( data ){
            $('#company_house_contact').find('.main_contacts').html(data.html).show().find('.view_contacts').hide();            
            $('#company_house_contact').modal('show');
            $('.LoadingImage').hide();
            trigger_contact_person_validation();
         },
         error: function( errorThrown ){
            $('#company_house_contact').find('.main_contacts').html('Data Not found.').show();
            $('#company_house_contact').modal('show');
            $('.LoadingImage').hide();
         }

      });
   
      
  });   

/*Add new contact person (get content from server)**/
$("#Add_New_Contact").click(function(){

  var cnt = $("#append_cnt").val();
  cnt = (cnt=='' ? 1 : parseInt(cnt)+1 );

  $("#append_cnt").val(cnt).valid();

  $.ajax({
     url: '<?php echo base_url();?>client/new_contact/',
     type: 'post',     
     data: {'cnt':cnt,'incre':'1',user_id:  client_user_id },
      beforeSend: function() {
                $(".LoadingImage").show();
              },
     success: function( data ){

        $('.contact_form').append(data);

        move_ccp_contents();

        $(".LoadingImage").hide();


         $('.make_a_primary').each(function(){

             var countofdiv = $('.make_a_primary').length;
             if( countofdiv >1 )
             {
                var id=$(this).attr('id').split('-')[1];
                $('.for_remove-'+id).remove();
                $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
             }

          });
          
          sorting_contact_person( $('.contact_form').find(".CONTENT_CONTACT_PERSON").filter(":last") );

         /** end of remove button **/
           /*$(".date_picker").datepicker({ dateFormat: 'dd-mm-yy',
              changeMonth: true,
             changeYear: true, }).val();*/
        
          trigger_contact_person_validation();
          Important_tab_toggle( $("div#import_tab ul li:last .toggle") );
         },
         error: function( errorThrown ){
             console.log( errorThrown );
         }
     });
});

//contact person remove button
$(document).on('click','.contact_remove',function(){

  var countno=$(this).attr('id').split('-')[1];
  var parent = $('.common_div_remove-'+countno);

  var action  = function ()
  {
     if( parent.find('.contact_table_id').val() !='' )
     {
        var id = parent.find('.contact_table_id').val();
        $.ajax(
           {
              url:"<?php echo base_url();?>client/Client_Contact_Delete/"+id,
              beforeSend:Show_LoadingImg,
              success:function(res){ Hide_LoadingImg();}
           });
     }
     parent.remove();

    //delete ptr content
    $("#ccp_"+countno+"_personal_tax_returns").closest('li').remove();
    $("input[name='ccp_ptr_service["+countno+"]']").closest('tr').remove();

     var countofdiv = $('.make_a_primary').length;
     if(countofdiv == 1)
     {
        $('.contact_remove').css('display','none');
        $('.btn.btn-danger.remove').css('display','none');
     }
     $('#Confirmation_popup').modal('hide');
  };

  Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to remove this contact ?'}); 
  $('#Confirmation_popup').modal('show');
});


//append contact person remove button when page load 
$('.make_a_primary').each(function(){

     var countofdiv=$('.make_a_primary').length;
     if(countofdiv>1){
     var id=$(this).attr('id').split('-')[1];
     $('.for_remove-'+id).remove();
   $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
  }

});

 
//make primary contact click button
$(document).on('click','.make_primary_section',function(){
  
  $('.make_primary_section').removeClass('active');
  
  $(".LoadingImage").show();
  
  var its_data_id=$(this).attr('data-id');
  
  $('#'+its_data_id).prop('checked',true).trigger('change').valid();   
  
  $('.make_primary_section').each(function(){
    $(this).html('<span>Make A Primary</span>');
  });

  $(this).addClass('active');
  $(this).html('<span style="color:red;">Primary Contact</span>'); 
  trigger_contact_person_validation();
  $(".LoadingImage").hide();
});


//new email row
$(document).on('click','.yk_pack_addrow_landline',function(e)
{

  $(this).addClass('yyyyyy');

  e.preventDefault();
  var id= $(this).data('id');
  var i=  1;
  var parent = $(this).parents('.pack_add_row_wrpr_landline');

  var len = parseInt( $(this).attr("data-length") )+1;

  parent.append('<span class="primary-inner success ykpackrow_landline add-delete-work width_check" id="'+len+'"><div class="data_adds"><select name="pre_landline['+id+']['+len+']" id="pre_landline"><option value="mobile">Mobile</option><option value="work">Work</option><option value="home">Home</option><option value="main">Main</option><option value="workfax">Work Fax</option><option value="homefax">Home Fax</option></select><input type="text" class="text-info" name="landline['+id+']['+len+']" value=""></div><div class="text-right danger-make1"><a href="javascript:;" class="btn btn-danger yk_pack_delrow_landline" id="'+len+'"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></div>   <button type="button" class="btn btn-primary yk_pack_addrow_landline" data-id="'+id+'" data-length="'+len+'">Add Landline</button></span>');
  i++;
    // alert($('.yk_pack_addrow_landline').length);
  if( parent.find('.yk_pack_addrow_landline').length > 2){
   // alert('ok');
    $(this).parents('.primary-inner').addClass('check_width');

  }
  trigger_contact_person_validation();
});

//remove landline row
$(document).on('click','.yk_pack_delrow_landline',function(e)
{
  var parent = $(this).parents('.pack_add_row_wrpr_landline');
  $(this).parents('.ykpackrow_landline').remove();

   if( parent.find('.yk_pack_addrow_landline').length == 1)
   {
        parent.find('.yk_pack_addrow_landline').removeClass('yyyyyy');
   }
   else
   {
    parent.find('span:last-child .yk_pack_addrow_landline').removeClass('yyyyyy');
   }
});

//new email row  
$(document).on('click','.yk_pack_addrow_email',function(e)
{
 //alert('op');
  e.preventDefault();
  $(this).addClass('yyyyyy');
  var id     = $(this).data('id');
  var parent = $(this).parents('.pack_add_row_wrpr_email');
  
  var len = parseInt($(this).attr("data-length"))+1;

  parent.append('<span class="primary-inner success ykpackrow_email add-delete-work"><input type="email" class="text-info" name="work_email['+id+']['+len+']" value=""><div class="text-right danger-make1"><a href="javascript:;" class="btn btn-danger yk_pack_delrow_email"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></div> <button type="button" class="btn btn-primary yk_pack_addrow_email" data-id="'+id+'" data-length="'+len+'">Add Email</button></span>');
   // alert($('.yk_pack_addrow_email').length);
 if(parent.find('.yk_pack_addrow_email').length > 2)
 {
     $(this).parents('.primary-inner').addClass('check_width');
  }
});

// remove email row   
$(document).on('click','.yk_pack_delrow_email',function(e)
{
   var parent = $(this).closest('.pack_add_row_wrpr_email');
   $(this).parents('.ykpackrow_email').remove();

   if(parent.find('.yk_pack_addrow_email').length == 1)
   {
        parent.find('.yk_pack_addrow_email').removeClass('yyyyyy');
   }
   else
   {   
     parent.find('span:last-child .yk_pack_addrow_email').removeClass('yyyyyy');
   }    
});  

/*===================================================================================================*/


/** general action scripts **/
/*============================================================================================*/

//copy data between each content         
$('input[id$="auth_code"]').keyup(function () {
  $('.confirmation_auth_code').val($(this).val());
  $('.accounts_auth_code').val($(this).val());
});

//inside previous acc
$('.country_select_div').dropdown();

//tinymce for custom reminder
tinymce_config['images_upload_url'] = "<?php echo base_url();?>Firm/image_upload_from_editor";
tinymce_config['selector']          = '#add_custom_reminder_content ,#edit_custom_reminder_content';
tinymce.init( tinymce_config );

//for change tabs by client/company type
$(document).on('change', '.sel-legal-form', function() {

  var selectVal = $(this).find(':selected').val();

  $('#company_type').val(selectVal);
  $('#company_type').prop('readonly', true);

  var form_type1 = [ "Public Limited company" , "Private Limited company" , "Limited Liability Partnership" ,"Overseas Company/Business" ];
  var form_type2 = [ "Partnership" , "Self Employed" , "Landlords - UK" ,"Non Resident Landlord"];
    

  if(  form_type1.indexOf( selectVal ) !== -1 )
  {

    $('.filing-alcat-tab').show();
    $('.business-info-tab').hide();
    // $('.business-info-tab').show();
    /** 16-08-2018 shown shown basic details tab **/
    $('.basic_details_tab').show();
    /** end of 16-08-2018 **/
  }
  else if( form_type2.indexOf( selectVal ) !== -1 )
  {

    $('.business-info-tab').show();
    $('.filing-alcat-tab').hide();
    /** 16-08-2018 shown hide basic details tab **/
    $('.basic_details_tab').hide();
    /** end of 16-08-2018 **/
  }
  else
  {
    $('.business-info-tab').hide();
    $('.filing-alcat-tab').hide();
    /** 16-08-2018 shown hide basic details tab **/
    $('.basic_details_tab').hide();
    /** end of 16-08-2018 **/
  }
 });
//beggning need show correct tab by company type
$('.sel-legal-form').trigger('change');      

//aml check client varification type select box.
//show text box if user select others option
$('.assign_cus_type').find('.dropdown-sin-11').on('click',function(){

  $(this).find('.dropdown-main > ul > li').click(function(){
    var custom=$(this).attr('data-value');
    var customclass=$(this).attr('class');
    if(custom=='Other_Custom')
    {   
      //if(customclass=='dropdown-option dropdown-chose'){
      if (customclass.indexOf('dropdown-chose') > -1){
             $('.for_other_custom_choose').css('display','none');
      }
      else
      {
         $('.for_other_custom_choose').css('display','');
      }          
    }
 
   });

   $(this).find('.dropdown-clear-all').on('click',function(){
      $('.for_other_custom_choose').css('display','none');
   });
});


//past current data to other same element
 $(document).on('change',"input",function(){

  var name=$(this).attr('name');
  var thisval=$(this).val();
  var type=$(this).attr('type');
  
  if(name == 'company_name' && $('#company_url_anchor').length )
  {
     $('#company_url_anchor').text(thisval);
  }

  var vallen=$("[name^='"+name+"']").length;

  console.log('same elemet'+vallen);
 
  if(vallen >1 && (type=="text" || type=="number"))
  { 
    console.log('inside the condition'+thisval);

    $("[name^='"+name+"']").each(function() {

      //not inside the contact person
      console.log("not inside"+$(this).parents('.make_a_primary').length);

      if(!$(this).parents('.make_a_primary').length )
      {
        console.log('not primary content'+$("[name^='"+name+"']").attr('name'));
        $("[name^='"+name+"']").val(thisval);
      }
    });
  }
});

//past current data to other same element
$(document).on('change',"textarea",function(){
 var name=$(this).attr('name');
 var thisval=$(this).val();
 var vallen=$("[name^='"+name+"']").length;
 
 if(vallen >1)
 {
    $("[name^='"+name+"']").each(function() {
     
       //not inside the contact person
      /*if(! $('.make_a_primary').has( $(this)[0] ) )
      {
      }*/
        $(this).val(thisval);
   });
 }    
});

/*for get confirm while click header link */
$('.atleta').click(function(e) {
      //alert('ok');
      e.preventDefault();
      var h = $("a",this).attr('href');
        if(h.indexOf('http') != -1)
        {

          $("#myAlert").modal(); 
     
          $(".exit").click(function(){
              window.location.href = h;
          });
        }
});


$('.dropdown-sin-21').dropdown({
  limitCount: 5,
  input: '<input type="text" maxLength="20" placeholder="Search">'
});

$('.dropdown-sin-2').dropdown({

 input: '<input type="text" maxLength="20" placeholder="Search">'
});
$('.dropdown-sin-3').dropdown({

 input: '<input type="text" maxLength="20" placeholder="Search">'
});
  $('.dropdown-sin-4').dropdown({

 input: '<input type="text" maxLength="20" placeholder="Search">'
});

$('.dropdown-sin-5').dropdown({

 input: '<input type="text" maxLength="20" placeholder="Search">'
});
$('.dropdown-sin-11').dropdown({
 
    input: '<input type="text" maxLength="20" placeholder="Search">'
});

//reffreal client select box
$(document).on('change','#source',function(){
       var value=$(this).val();
       if(value=='Existing Client')
       {
          $('.dropdown_source').css('display','');
          $('.textfor_source').css('display','none');
       }
       else
       {
          $('.dropdown_source').css('display','none');
          $('.textfor_source').css('display','');
       }
});

// Other custom label
$(document).on('change','.othercus',function(e){
  //$(".othercus").change(function(){
  var custom = $(':selected',this).val();
  var client_id = $('#user_id').val();
  //alert(custom);

  if(custom=='Other')
  {
  // $('.contact_type').next('span').show();
  $(this).closest('.contact_type').next('.spnMulti').show();
  } else {
  $('.contact_type').next('span').hide();
  }
}); 

//image remove option in aml check content 
$(document).on('click','.remove_file',function(){
    $('#image_name').val('');
    //$('.extra_view_40').hide();
    var id=$(this).attr('data-id');
    $('.for_img_'+id).remove();
 });   

//for design porpose need for inside importent tab      
$('.js-small').parent('.col-sm-8').addClass('addbts-radio');

var Important_tab_toggle = function( selecter = '.accordion-views .toggle' ){

  $( selecter ).click(function(e) {

    $(this).next('.inner-views').slideToggle(300);
    $(this).toggleClass('this-active');
  });

};

Important_tab_toggle();

//attach chnage event for auto save 
$(document).on('change',"#insert_form  input[name],select[name],textarea[name]",function(){

  //console.log("change",$(this).attr('name') );  
  if( $(this).val().trim() != '' )
  {    
    //if element is inside contact tab it should call contact person save function
    if( ( $(this).parents('.make_a_primary').length || $(this).parents('.ccp_ptr_cnt').length ) )
    {  
      clientContact_auto_save();
    }
    else
    {
      client_auto_save();
    }
  }
});

//trigger validation to given content elements.
function tab_validate( tab_content_id='' )
{  
  var valid = true;

  if( tab_content_id == '')
  {
    tab_content_id = $('ol.CLIENT_CONTENT_TAB.nav-tabs a.nav-link.active').attr("data-id");
  }      
  
  $( tab_content_id ).find('input[name], select[name] ,textarea[name]').each(function(){

      /*console.log( $(this).attr('name') + " current element" );
      console.log( $(this)[0].outerHTML );*/

      if (! $(this).valid() )
      {
        valid = false;
      }
  });

  return valid;
}

//when click on submit botton we need to validate tabs from the beginning
$('input#save_exit').click(function(){

  $('ol.CLIENT_CONTENT_TAB.nav-tabs li a').each(function(){

    var id = $(this).attr('data-id');

    if( !tab_validate( id ) )
    {
        $(this).trigger('click');
        return false;
    }
  });
});

 // prev click
 $('.signed-change2').click(function(){

    $('ol.CLIENT_CONTENT_TAB.nav-tabs .active').closest('li').prevAll('li:visible').eq(0).find('a').trigger('click');     
 });

 // next click
 $('.signed-change3').click(function(){

      $('ol.CLIENT_CONTENT_TAB.nav-tabs .active').closest('li').nextAll("li:visible").eq(0).find('a').trigger('click');
 });

 //tab button click
$("ol.CLIENT_CONTENT_TAB.nav-tabs li.nav-item a.nav-link").click(function(){
  
  var clicked_tab = $(this).parent('li').index();
  var current_tab = $("ol.CLIENT_CONTENT_TAB li.nav-item.active").index();


  var allowed     =  ( clicked_tab < current_tab ? 1 : tab_validate() );

  if( allowed )
  {
    var id = $(this).attr('data-id');
    $(this).closest(".nav-tabs").find("a.nav-link").removeClass('active');
    $(this).addClass('active');
    
    $(this).closest(".nav-tabs").find("li.nav-item").removeClass("active");
    $(this).parent().addClass("active");      

    $(".newupdate_design .tab-content .tab-pane").removeClass("active show");
    $(".newupdate_design .tab-content").find(id).addClass("active show");
    
    var items = $(this).closest(".nav-tabs").find("li.nav-item").filter(":visible");
    

       items.removeClass("active-previous");
    var item_len = items.length - 1;


    for( var i=0;i<=item_len;i++)
    {
       if( $(items[i]).hasClass('active') )
       {
          break;
       }
       $(items[i]).addClass("active-previous");
    }

    if(i==item_len)
    {
       $('.signed-change3').hide();
       
    }
    else
    {
       $('.signed-change3').show();    
       
    }

    if(i==0)
    {
       $('.signed-change2').hide();
       
    }
    else
    {
       $('.signed-change2').show();    
       
    }

  }

});




/*
  items_obj   are tree items
  node_id     assinee node ids possible value last node id or hirarcy id
  is_sel_1st  is need to select first item if node_id empty
*/
function select_combo_tree( items_obj , node_id , is_sel_1st = false )
{   
    console.log('inside select_combo_tree item length'+ items_obj.length);
    items_obj.each(function(){

      var hid_uid = $(this).attr('data-id');
      //console.log( "data-id" , hid_uid);
          hid_uid = hid_uid.split('_');

      //get end node id
      var end_hid = hid_uid[0].split(':');
          end_hid = end_hid[end_hid.length - 1];
          //console.log("end user_id" + end_hid );

       //for new client add page make firm admin default select
       if(is_sel_1st && !node_id.length )
       {
        $(this).find('input[type="checkbox"]').trigger('click');
        return false;
       }
       
       if( node_id.indexOf( hid_uid[0] ) != -1 || node_id.indexOf( end_hid ) != -1 )
       {
        $(this).find('input[type="checkbox"]').trigger('click');
       }
    });
}


//when tree checkbox enable neet to store ids in next input
$('.comboTreeItemTitle').click(function(){  
    
    var parent_div  = $(this).closest('div.comboTreeWrapper'); 
    var id          = $(this).attr('data-id').split('_');
    var selected    = [];

    parent_div.find('.comboTreeItemTitle').each(function(){

      var id1         = $(this).attr('data-id');
      var id1         = id1.split('_');

      if($(this).find('input[type="checkbox"]').is(":checked"))
      {
         selected.push(id1[0]);
      }
      //console.log(id[1]+"=="+id1[1]);
      if( id[1] == id1[1] )
      {
        $(this).toggleClass('disabled');
      }
    });
    parent_div.siblings('input[type="hidden"]').val( selected.join() ).valid();
          
    $(this).removeClass('disabled');
});

 
var assignees_node = <?php echo (!empty($assignees_node)?$assignees_node:"[]");?>;

var items = $('div.client_assignees span.comboTreeItemTitle');

select_combo_tree( items , assignees_node , 1 );


//select default services assignees
var default_service_assignee  = <?php echo $default_service_assignee;?>;

$.each( default_service_assignee ,function( i , data ){
/*console.log('service_id ',i);
console.log('assignee ',data);*/

var tr = $('tr#service_assignee_tr_'+i);

var manager_items = tr.find('td:nth-child(2) span.comboTreeItemTitle');
select_combo_tree( manager_items , data['manager'] );

var assignee_items = tr.find('td:nth-child(3) span.comboTreeItemTitle');
select_combo_tree( assignee_items , data['assignee'] );

});

//success mssage close button
$(document).on('click','.close.alert_close',function(){ $('.alert.alert-success').hide(); });

/*============================================================================================*/
});
var other_services = <?php echo json_encode( $other_services ); ?>;
</script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/add_client/checkbox_toggleAction.js"></script>