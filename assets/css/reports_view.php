<?php $this->load->view('includes/header'); ?>
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set f4reports">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" data-target="#reports-tab" href="">Reports</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#clients-reports" href="">clients</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#proposals-reports" href="">proposals</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#task-reports" href="">tasks</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#services-reports" href="">services</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#leads-reports" href="">leads</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#deadlines-reports" href="">Deadlines</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#invoices-reports" href="">invoices</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                                 <div class="toolbar-table f-right">
                                    <div class="filter-task1">
                                       <h2>Filter:</h2>
                                       <ul>
                                          <li><img src="http://remindoo.org/CRMTool/assets/images/by-date1.png" alt="data"><input type="text" id="datewise_filter" name="de" placeholder="By Date" class="date-picker" value="<?php echo date('Y-m-d'); ?>"></li>
                                          <li class="fr-wid">
                                             <img src="http://remindoo.org/CRMTool/assets/images/by-date2.png" alt="data">
                                             <select id="clientwise_filter" name="stati">
                                                <option value="0" hidden="">By Client</option>
                                                <?php
                                                $query = $this->db->query("SELECT * FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' order by id DESC")->result_array();
                                                foreach ($query as $que) { ?>
                                                   <option value="<?php echo $que['id']; ?>"><?php echo $que['crm_name']; ?></option>
                                                <?php   } ?>
                                             </select>
                                          </li>
                                          <li class="fr-wid">
                                             <img src="http://remindoo.org/CRMTool/assets/images/by-date2.png" alt="data">
                                             <select id="userwise_filter" name="stati">
                                                <option value="0" hidden="">By User</option>
                                                <?php if (count($staff_form)) { ?>
                                                   <option disabled>Staff</option>
                                                   <?php foreach ($staff_form as $s_key => $s_val) { ?>
                                                      <option value="<?php echo $s_val['id']; ?>"><?php echo $s_val['crm_name']; ?></option>
                                                <?php }
                                                } ?>
                                                <?php if (count($team)) { ?>
                                                   <option disabled>Team</option>
                                                   <?php foreach ($team as $s_key => $s_val) { ?>
                                                      <option value="tm_<?php echo $s_val['id']; ?>"><?php echo $s_val['team']; ?></option>
                                                <?php }
                                                } ?>
                                                <?php if (count($department)) { ?>
                                                   <option disabled>Department</option>
                                                   <?php foreach ($department as $s_key => $s_val) { ?>
                                                      <option value="de_<?php echo $s_val['id']; ?>"><?php echo $s_val['new_dept']; ?></option>
                                                <?php }
                                                } ?>
                                             </select>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="year-status">
                                       <!--  <span class="lt-year">last year</span> -->
                                       <ul class="status-list">
                                          <li><a href="javascript:;" class="all fill" id="all"> All </a></li>
                                          <li><a href="javascript:;" class="week fill" id="week"> week </a></li>
                                          <li><a href="javascript:;" class="three_week fill" id="three_week">3 week</a></li>
                                          <li><a href="javascript:;" class="month fill" id="month"> month </a></li>
                                          <li><a href="javascript:;" class="Three_month fill" id="Three_month"> 3 months </a></li>
                                          <!--  <li>year</li> -->
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div id="append_content">
                                 <div class="all_user-section2 floating_set fr-reports">
                                    <div class="tab-content">
                                       <div class="reports-tabs tab-pane fade active" id="reports-tab">
                                          <div class="card-comment">
                                             <div class="card-block-small">
                                                <div class="left-sireport col-xs-12  col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Clients
                                                         <div class="comment-btn">
                                                            <button class="btn bg-c-blue btn-round btn-comment" data-toggle="modal" data-target="#client_download" data-backdrop="static" data-keyboard="false">Run</button>
                                                            <button class="btn bg-c-pink btn-round btn-comment" data-toggle="modal" data-target="#client_customization" data-backdrop="static" data-keyboard="false">Customise</button>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                         <select id="filter_client">
                                                            <option value="year">This Year</option>
                                                            <option value="month">This Month</option>
                                                         </select>
                                                         <div class="card-block fileters_client new123">
                                                            <div id="chart_Donut11" style="width: 100%;"></div>
                                                            <!-- progress bar section -->
                                                            <div class="prodiv12">
                                                               <?php
                                                               if ($Private['limit_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $Private['limit_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Private['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($Public['limit_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $Public['limit_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Public['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($limited['limit_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $limited['limit_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $limited['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($Partnership['limit_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $Partnership['limit_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Partnership['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($self['limit_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $self['limit_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $self['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($Trust['limit_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $Trust['limit_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Trust['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($Charity['limit_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $Charity['limit_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Charity['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($Other['limit_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $Other['limit_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Other['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                            </div>
                                                            <!-- progress bar section -->
                                                         </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Task
                                                         <div class="comment-btn">
                                                            <button class="btn bg-c-blue btn-round btn-comment " data-toggle="modal" data-target="#task_download" data-backdrop="static" data-keyboard="false">Run</button>
                                                            <button class="btn bg-c-pink btn-round btn-comment " data-toggle="modal" data-target="#task_customization" data-backdrop="static" data-keyboard="false">Customise</button>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                         <select id="filter_task">
                                                            <option value="year">This Year</option>
                                                            <option value="month">This Month</option>
                                                         </select>
                                                         <div class="card-block fileters_Tasks">
                                                            <div id="pie_chart" style="width: 100%;"></div>
                                                            <!-- progress bar section -->
                                                            <div class="prodiv12">
                                                               <?php
                                                               if ($notstarted_tasks['task_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $notstarted_tasks['task_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $notstarted_tasks['task_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($completed_tasks['task_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $completed_tasks['task_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_tasks['task_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($inprogress_tasks['task_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $inprogress_tasks['task_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $inprogress_tasks['task_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($awaiting_tasks['task_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $awaiting_tasks['task_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $awaiting_tasks['task_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($testing_tasks['task_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $testing_tasks['task_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $testing_tasks['task_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                            </div>
                                                            <!-- progress bar section -->
                                                         </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Proposal
                                                         <div class="comment-btn">
                                                            <button class="btn bg-c-blue btn-round btn-comment " data-toggle="modal" data-target="#proposal_download" data-backdrop="static" data-keyboard="false">Run</button>
                                                            <button class="btn bg-c-pink btn-round btn-comment" data-toggle="modal" data-target="#proposal_customization" data-backdrop="static" data-keyboard="false">Customise</button>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                         <select id="filter_proposal">
                                                            <option value="year">This Year</option>
                                                            <option value="month">This Month</option>
                                                         </select>
                                                         <div class="card-block filter_proposal">
                                                            <div id="piechart1" style="width: 100%; "></div>
                                                            <!-- progress bar section -->
                                                            <div class="prodiv12">
                                                               <?php
                                                               if ($accept_proposal['proposal_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $accept_proposal['proposal_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $accept_proposal['proposal_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($indiscussion_proposal['proposal_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $indiscussion_proposal['proposal_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $indiscussion_proposal['proposal_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($sent_proposal['proposal_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $sent_proposal['proposal_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $sent_proposal['proposal_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($viewed_proposal['proposal_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $viewed_proposal['proposal_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $viewed_proposal['proposal_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($declined_proposal['proposal_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $declined_proposal['proposal_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $declined_proposal['proposal_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($archive_proposal['proposal_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $archive_proposal['proposal_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $archive_proposal['proposal_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($draft_proposal['proposal_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $draft_proposal['proposal_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $draft_proposal['proposal_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                            </div>
                                                            <!-- progress bar section -->
                                                         </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12  col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Leads
                                                         <div class="comment-btn">
                                                            <button class="btn bg-c-blue btn-round btn-comment" data-toggle="modal" data-target="#leads_download" data-backdrop="static" data-keyboard="false">Run</button>
                                                            <button class="btn bg-c-pink btn-round btn-comment" data-toggle="modal" data-target="#leads_customization" data-backdrop="static" data-keyboard="false">Customise</button>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                         <select id="filter_leads">
                                                            <option value="year">This Year</option>
                                                            <option value="month">This Month</option>
                                                         </select>
                                                         <div class="card-block filter_leads">
                                                            <div id="piechart_1" style="width: 100%;"></div>
                                                            <!-- progress bar section -->
                                                            <div class="prodiv12">
                                                               <?php
                                                               foreach ($leads_status as $status) {
                                                                  $count = $this->Report_model->lead_details($status['id']);
                                                                  if ($count['lead_count'] != '0') { ?>
                                                                     <div class="pro-color">
                                                                        <span class="procount"><?php echo $count['lead_count']; ?></span>
                                                                        <div class="fordis">
                                                                           <div class="progress progress-xs">
                                                                              <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $count['lead_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                               <?php }
                                                               } ?>
                                                            </div>
                                                            <!-- progress bar section -->
                                                         </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Service
                                                         <div class="comment-btn">
                                                            <button class="btn bg-c-blue btn-round btn-comment" data-toggle="modal" data-target="#service_download" data-backdrop="static" data-keyboard="false">Run</button>
                                                            <button class="btn bg-c-pink btn-round btn-comment" data-toggle="modal" data-target="#service_customization" data-backdrop="static" data-keyboard="false">Customise</button>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                         <select id="filter_service">
                                                            <option value="year">This Year</option>
                                                            <option value="month">This Month</option>
                                                         </select>
                                                         <div class="card-block filter_service">
                                                            <div id="piechart_service" style="width: 100%; "></div>
                                                            <!-- progress bar section -->
                                                            <div class="prodiv12">
                                                               <?php
                                                               if ($conf_statement['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $conf_statement['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $conf_statement['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($accounts['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $accounts['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $accounts['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($company_tax_return['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $company_tax_return['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $company_tax_return['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($personal_tax_return['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $personal_tax_return['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $personal_tax_return['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($vat['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $vat['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $vat['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($payroll['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $payroll['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $payroll['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($workplace['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $workplace['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $workplace['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($cis['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $cis['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $cis['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($cissub['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $cissub['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $cissub['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($p11d['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $p11d['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $p11d['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($management['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $management['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $management['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($bookkeep['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $bookkeep['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $bookkeep['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($investgate['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $investgate['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $investgate['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($registered['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $registered['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $registered['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($taxadvice['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $taxadvice['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $taxadvice['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($taxinvest['service_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $taxinvest['service_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $taxinvest['service_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                            </div>
                                                            <!-- progress bar section -->
                                                         </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12  col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Deadline Manager
                                                         <div class="comment-btn">
                                                            <button class="btn bg-c-blue btn-round btn-comment" data-toggle="modal" data-target="#deadline_download" data-backdrop="static" data-keyboard="false">Run</button>
                                                            <button class="btn bg-c-pink btn-round btn-comment" data-toggle="modal" data-target="#deadline_customization" data-backdrop="static" data-keyboard="false">Customise</button>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                         <select id="filter_deadline">
                                                            <option value="year">This Year</option>
                                                            <option value="month">This Month</option>
                                                         </select>
                                                         <div class="card-block filter_deadline">
                                                            <div id="piechart_deadline" style="width: 100%; "></div>
                                                            <!-- progress bar section -->
                                                            <div class="prodiv12">
                                                               <?php if ($de_accounts['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_accounts['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_accounts['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php if ($de_company_tax_return['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_company_tax_return['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_company_tax_return['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php if ($de_conf_statement['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_conf_statement['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_conf_statement['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php if ($de_personal_tax_return['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_personal_tax_return['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_personal_tax_return['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php if ($de_vat['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_vat['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_vat['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php if ($de_payroll['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_payroll['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_payroll['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?> <?php if ($de_workplace['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_workplace['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_workplace['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?> <?php if ($de_cis['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_cis['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_cis['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?> <?php if ($de_cissub['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_cissub['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_cissub['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?> <?php if ($de_p11d['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_p11d['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_p11d['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php if ($de_management['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_management['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_management['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php if ($de_bookkeep['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_bookkeep['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_bookkeep['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php if ($de_investgate['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_investgate['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_investgate['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php if ($de_registered['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_registered['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_registered['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php if ($de_taxadvice['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_taxadvice['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_taxadvice['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php if ($de_taxinvest['dead_count'] != '0') { ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"> <?php echo $de_taxinvest['dead_count']; ?></span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_taxinvest['dead_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                            </div>
                                                            <!-- progress bar section -->
                                                         </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Invoice
                                                         <div class="comment-btn">
                                                            <button class="btn bg-c-blue btn-round btn-comment " data-toggle="modal" data-target="#invoice_download" data-backdrop="static" data-keyboard="false">Run</button>
                                                            <button class="btn bg-c-pink btn-round btn-comment " data-toggle="modal" data-target="#invoice_customization" data-backdrop="static" data-keyboard="false">Customise</button>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                         <select id="filter_invoice">
                                                            <option value="year">This Year</option>
                                                            <option value="month">This Month</option>
                                                         </select>
                                                         <div class="card-block fileters_invoice">
                                                            <div id="piechart_invoice" style="width: 100%;"></div>
                                                            <!-- progress bar section -->
                                                            <div class="prodiv12">
                                                               <?php
                                                               if ($approved_invoice['invoice_count'] != '0') {
                                                               ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $approved_invoice['invoice_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $approved_invoice['invoice_count'] . '%'; ?> ;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($cancel_invoice['invoice_count'] != '0') {
                                                               ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $cancel_invoice['invoice_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $cancel_invoice['invoice_count'] . '%'; ?> ;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                               <?php
                                                               if ($expired_invoice['invoice_count'] != '0') {
                                                               ?>
                                                                  <div class="pro-color">
                                                                     <span class="procount"><?php echo $expired_invoice['invoice_count']; ?> </span>
                                                                     <div class="fordis">
                                                                        <div class="progress progress-xs">
                                                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $expired_invoice['invoice_count'] . '%'; ?> ;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               <?php } ?>
                                                            </div>
                                                            <!-- progress bar section -->
                                                         </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc sumlast">
                                                      <h6>
                                                         Summary
                                                         <div class="comment-btn">
                                                            <button class="btn bg-c-blue btn-round btn-comment " data-toggle="modal" data-target="#summary_download" data-backdrop="static" data-keyboard="false">Run</button>
                                                            <button class="btn bg-c-pink btn-round btn-comment " data-toggle="modal" data-target="#summary_customization" data-backdrop="static" data-keyboard="false">Customise</button>
                                                         </div>
                                                      </h6>
                                                      <!-- progress bar section -->
                                                      <div class="prodiv">
                                                         <div class="pro-color">
                                                            <span class="procount primary"><?php echo $active_user_count['activeuser_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Active Users</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-primary" role="progressbar" style="width: <?php echo $active_user_count['activeuser_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pro-color">
                                                            <span class="procount warning"><?php echo $inactive_user_count['inactiveuser_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Inactive Users</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-warning" role="progressbar" style="width: <?php echo $inactive_user_count['inactiveuser_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pro-color">
                                                            <span class="procount danger"><?php echo $completed_task_count['task_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Completed Tasks</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_task_count['task_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pro-color success">
                                                            <span class="procount"><?php echo $overall_leads['leads_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Over all Leads</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-success" role="progressbar" style="width: <?php echo $overall_leads['leads_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <!-- progress bar section -->
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="clients-reports">
                                          <div class="card-block-small">
                                             <div id="Tabs_client"></div>
                                             <div class="prodiv12">
                                                <?php
                                                if ($Private['limit_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"> <?php echo $Private['limit_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Private['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                <?php
                                                if ($Public['limit_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"> <?php echo $Public['limit_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Public['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                <?php
                                                if ($limited['limit_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"> <?php echo $limited['limit_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $limited['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                <?php
                                                if ($Partnership['limit_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"> <?php echo $Partnership['limit_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Partnership['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                <?php
                                                if ($self['limit_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"> <?php echo $self['limit_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $self['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                <?php
                                                if ($Trust['limit_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"> <?php echo $Trust['limit_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Trust['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                <?php
                                                if ($Charity['limit_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"> <?php echo $Charity['limit_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Charity['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                <?php
                                                if ($Other['limit_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"> <?php echo $Other['limit_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Other['limit_count'] . '%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                             </div>
                                             <div id="Tabs_Users"></div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="task-reports">
                                          <div class="card-block-small">
                                             <div id="Tabs_task"></div>
                                             <div class="prodiv12">
                                                <?php
                                                if ($notstarted_tasks['task_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"><?php echo $notstarted_tasks['task_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $notstarted_tasks['task_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                <?php
                                                if ($completed_tasks['task_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"><?php echo $completed_tasks['task_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_tasks['task_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                <?php
                                                if ($inprogress_tasks['task_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"><?php echo $inprogress_tasks['task_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $inprogress_tasks['task_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                <?php
                                                if ($awaiting_tasks['task_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"><?php echo $awaiting_tasks['task_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $awaiting_tasks['task_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                <?php
                                                if ($testing_tasks['task_count'] != '0') { ?>
                                                   <div class="pro-color">
                                                      <span class="procount"><?php echo $testing_tasks['task_count']; ?></span>
                                                      <div class="fordis">
                                                         <div class="progress progress-xs">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $testing_tasks['task_count'] . '%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                             </div>
                                             <div id="Tabs_priority"></div>
                                             <div id="monthly_task" style="width: 1200px; height: 500px"></div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="proposals-reports">
                                          <div class="card-block-small">
                                             <div class="row history">
                                                <div class="card">
                                                   <div class="card-header">
                                                      <h5>This Month Proposal History</h5>
                                                   </div>
                                                   <div class="card-block">
                                                      <div class="main-timeline">
                                                         <div class="cd-timeline cd-container">
                                                            <?php
                                                            foreach ($proposal_history as $p_h) {
                                                               $randomstring = generateRandomString('100');
                                                            ?>
                                                               <div class="cd-timeline-block">
                                                                  <div class="cd-timeline-icon bg-primary">
                                                                     <i class="icofont icofont-ui-file"></i>
                                                                  </div>
                                                                  <div class="cd-timeline-content card_main">
                                                                     <div class="p-20">
                                                                        <div class="timeline-details">
                                                                           <p class="m-t-20">
                                                                              <!-- <a href="<?php //base_url().'proposal_page/step_proposal/'
                                                                                             ?><?php //echo $randomstring; 
                                                                                                ?><?php echo '---'; ?> <?php //echo $p_h['id']; 
                                                                                                                                                                        ?>">--><?php echo $p_h['proposal_name']; ?>
                                                                              <!-- </a> -->
                                                                           </p>
                                                                        </div>
                                                                     </div>
                                                                     <span class="cd-date"><?php
                                                                                             $timestamp = strtotime($p_h['created_at']);
                                                                                             $newDate = date('d F Y H:i', $timestamp);
                                                                                             echo $newDate;
                                                                                             ?></span>
                                                                  </div>
                                                               </div>
                                                            <?php } ?>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="leads-reports">
                                          <div class="card-block-small">
                                             <div class="row history">
                                                <div class="card">
                                                   <div class="card-header">
                                                      <h5>This Month Leads History</h5>
                                                   </div>
                                                   <div class="card-block">
                                                      <div class="main-timeline">
                                                         <div class="cd-timeline cd-container">
                                                            <?php
                                                            foreach ($leads_history as $lead) {
                                                               // $randomstring=generateRandomString('100');
                                                            ?>
                                                               <div class="cd-timeline-block">
                                                                  <div class="cd-timeline-icon bg-primary">
                                                                     <i class="icofont icofont-ui-file"></i>
                                                                  </div>
                                                                  <div class="cd-timeline-content card_main">
                                                                     <div class="p-20">
                                                                        <div class="timeline-details">
                                                                           <p class="m-t-20">
                                                                              <!-- <a href="<?php //base_url().'proposal_page/step_proposal/'
                                                                                             ?><?php //echo $randomstring; 
                                                                                                ?><?php echo '---'; ?> <?php //echo $p_h['id']; 
                                                                                                                                                                        ?>">--><?php echo $p_h['name']; ?>
                                                                              <!-- </a> -->
                                                                           </p>
                                                                        </div>
                                                                     </div>
                                                                     <span class="cd-date"><?php
                                                                                             $timestamp = $p_h['createdTime'];
                                                                                             $newDate = date('d F Y H:i', $timestamp);
                                                                                             echo $newDate;
                                                                                             ?></span>
                                                                  </div>
                                                               </div>
                                                            <?php } ?>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="services-reports">
                                          <div class="card-block-small">
                                             <div id="Tabs_service" style="width:100%;"></div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="deadlines-reports">
                                          <div class="card-block-small">
                                             <div id="Tabs_deadline" style="width:100%;"></div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="invoices-reports">
                                          <div class="card-block-small">
                                             <div id="Tabs_invoice" style="width:100%;"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- home-->
                                    <!-- frozen -->
                                 </div>
                                 <!-- ul after div -->
                              </div>
                           </div>
                           <!-- admin close -->
                        </div>
                        <!-- Register your self card end -->
                     </div>
                  </div>
               </div>
               <!-- Page body end -->
            </div>
         </div>
         <!-- Main-body end -->
         <div id="styleSelector">
         </div>
      </div>
   </div>
</div>
<!-- email all -->
<input type="hidden" name="user_id" id="user_id" value="">
<div class="modal fade" id="client_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">
            <a href="<?php echo base_url(); ?>Reports/chart" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="invoice_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">
            <a href="<?php echo base_url(); ?>Filter/invoice" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="invoice_exportexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="invoice_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">
            <a href="<?php echo base_url(); ?>Filter/invoice_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="invoice_exportexcel_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="summary_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">
            <a href="<?php echo base_url(); ?>Filter/summary" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportexcel_summary()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="summary_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">
            <a href="<?php echo base_url(); ?>Filter/summary_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportexcel_summary_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="summary_customization" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customization</h4>
         </div>
         <div class="modal-body">
            <div class="col-md-12 cust1-popup">
               <div class="col-md-6">
                  <label>From Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date-picker" id="summary_from_date" value="" placeholder="dd/mm/yyyy">
               </div>
               <div class="col-md-6">
                  <label>To Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date-picker" id="summary_to_date" value="" placeholder="dd/mm/yyyy">
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Priority</label>
               </div>
               <div class="col-md-6">
                  <select id="summary_option">
                     <option value="all">All</option>
                     <option value="user">User</option>
                     <option value="leads">Leads</option>
                     <option value="task">Tasks</option>
                  </select>
               </div>
            </div>
            <!--  <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                <label>Status</label>
                </div>
                <div class="col-md-6">
                <select id="status" name="stati">
                <option value="all" hidden="">Any</option>          
                <option value="1">Active</option>
                <option value="2">In-Active</option>
                <option value="3">Fozen</option>
                </select> 
                </div>
               </div>   -->
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default select_template" id="summary_customisation">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="invoice_customization" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customization</h4>
         </div>
         <div class="modal-body">
            <div class="col-md-12 cust1-popup">
               <div class="col-md-6">
                  <label>From Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date-picker" id="invoice_from_date" value="" placeholder="dd/mm/yyyy">
               </div>
               <div class="col-md-6">
                  <label>To Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date-picker" id="invoice_to_date" value="" placeholder="dd/mm/yyyy">
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Priority</label>
               </div>
               <div class="col-md-6">
                  <select id="invoice_status">
                     <option value="all">All</option>
                     <option value="Expired">Expired</option>
                     <option value="approve">Approve</option>
                     <option value="decline">Decline</option>
                  </select>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default select_template" id="invoice_customisation">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="client_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">
            <a href="<?php echo base_url(); ?>Reports/chart_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportexcel_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="task_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">
            <a href="<?php echo base_url(); ?>Reports/task_pdf_download" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exporttaskexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="task_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">
            <a href="<?php echo base_url(); ?>Reports/task_pdf_download_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exporttaskexcel_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="proposal_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">
            <a href="<?php echo base_url(); ?>Report/proposalpdf_download" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportproposalexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="proposal_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">
            <a href="<?php echo base_url(); ?>Report/proposalpdf_download_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportproposalexcel_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="leads_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">
            <a href="<?php echo base_url(); ?>Report/leadspdf_download" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportleadsexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="leads_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">
            <a href="<?php echo base_url(); ?>Report/leadspdf_download_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportleadsexcel_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="service_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body service_session_not">
            <a href="<?php echo base_url(); ?>Report/services" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="service_exportexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="service_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body service_session">
            <a href="<?php echo base_url(); ?>Report/services_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="service_exportexcel1()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="deadline_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body service_session_not">
            <a href="<?php echo base_url(); ?>Report/deadline" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="deadline_exportexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="deadline_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body service_session_not">
            <a href="<?php echo base_url(); ?>Reports/deadline_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="deadline_exportexcel_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- filter add -->
<div class="modal fade" id="client_customization" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customization</h4>
         </div>
         <div class="modal-body">
            <div class="col-md-12 cust1-popup">
               <div class="col-md-6">
                  <label>From Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date-picker" id="from_date" value="" placeholder="dd/mm/yyyy">
               </div>
               <div class="col-m