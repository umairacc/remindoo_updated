<?php $this->load->view('includes/header');?>
<style>
  /*span.newonoff {
  background: #4680ff;
  padding: 6px 15px;
  display: inline-block;
  color: #fff;
  border-radius: 5px;
  }*/
  button.btn.btn-info.btn-lg.newonoff {
  padding: 3px 10px;
  height: initial;
  font-size: 15px;
  border-radius: 5px;
  }

</style>
<div class="pcoded-content">
  <div class="pcoded-inner-content">
    <!-- Main-body start -->
    <div class="main-body">
      <div class="page-wrapper">
        <!-- Page body start -->
        <div class="page-body">
          <div class="row">
            <div class="col-sm-12">
              <!-- Register your self card start -->
              <div class="card">
                <!-- admin start-->
                <div class="client_section col-xs-12 floating_set">
                  <!-- <div class="all-clients floating_set">
                    <div class="tab_section_01 floating_set">
                    <ul class="client_tab">
                     <li class="active"><a href="<?php echo  base_url()?>user">All Clients</a></li>
                     <li><a href="<?php echo  base_url()?>client/addnewclient">Add new client</a></li>
                    </ul>	
                    
                    <ul class="client_tab">
                    <li><a href="#">Email All</a></li>
                    <li><a href="#">filters</a></li>
                    </ul> 
                    </div> 
                    
                    <div class="text-left search_section1 floating_set">
                    <div class="search_box01">
                    
                    <div class="pcoded-search">
                    
                                                 <span class="searchbar-toggle">  </span>
                    
                                                 <div class="pcoded-search-box ">
                    
                                                     <input type="text" placeholder="Search">
                    
                                                     <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
                    
                                                 </div>
                    
                                             </div>
                    
                    </div>
                    </div>
                    
                    
                    
                    </div> <!-- all-clients -->
                  <div class="all_user-section floating_set">
                    <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#allusers">All Clients</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#newactive">Active Client</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#newinactive">No Longer client</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#frozen">Frozen/Freeze</a>
                        <div class="slide"></div>
                      </li>
                    </ul>
                    <!-- <div class="search-entry">
                    <div class="dataTables_length" id="alluser_length"><label>Show <select name="alluser_length" aria-controls="alluser" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div>
                    	<div id="alluser_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="alluser"></label></div>

                    </div> -->
                    <div class="all_user-section2 floating_set">
                      <div class="tab-content">
                        <div id="allusers" class="tab-pane fade in active">
                          <div class="count-value1 floating_set ">


                            <!--<ul class="pull-left">
                              <li><select><option>1</option><option>2</option><option>3</option></select></li> -->
                            <!-- <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li> -->
                            <!-- <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i><span>selected</span></a></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                              </ul>	 -->
                            <!-- <ul class="pull-right">
                              <form>
                              	<label>Search all columns</label>
                              	<input type="text" name="text">
                              </form>
                              </ul> -->
                              <button id="deleteTriger" class="deleteTri" style="display:none;">Delete</button>
                              <button type="button" class="data-turn99" data-toggle="modal" data-target="#myModalcolumn">Edit columns</button>	
                          </div>
                          <div class="client_section3 table-responsive floating_set ">
                            <div id="status_succ"></div>
                            <div class="all-usera1">
                              <table class="table client_table1 text-center display nowrap" id="alluser" cellspacing="0" width="100%">
                                <thead>
                                  <tr class="text-uppercase">
                                    <th><input type="checkbox"  id="bulkDelete"  /></th>
                                    <?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
                                    <th>Profile</th>
                                    <?php } ?>			
                                    <th class="select-filter">Name</th>
                                    <!-- <th>CRM-Username</th> -->
                                    <th>Updates</th>
                                    <th>Active</th>
                                    <th class="select-filter">CompanyStatus</th>
                                    <th class="select-filter">User Type</th>
                                    <th class="select-filter">Status</th>
                                    <?php if(isset($column_setting[0]['user_id'])&&($column_setting[0]['user_id']==1)){?>
                                    <th>Client Id</th>
                                    <?php } 
                                      //if(isset($column_setting[0]['company_name'])&&($column_setting[0]['company_name']==1)){?>							
                                    <th <?php echo (isset($column_setting[0]['company_name'])&&($column_setting[0]['company_name']==1)) ? '' : 'style="display:none;"'; ?> class="select-filter">Company Name</th>
                                    <?php //} 
                                      if(isset($column_setting[0]['legal_form'])&&($column_setting[0]['legal_form']==1)){?>							
                                    <th>Legal Form</th>
                                    <?php } if(isset($column_setting[0]['allocation_holder'])&&($column_setting[0]['allocation_holder']==1)){?>							
                                    <th>Allocation Holder</th>
                                    <?php } if(isset($column_setting[0]['company_number'])&&($column_setting[0]['company_number']==1)){?>							
                                    <th>Company Number</th>
                                    <?php } if(isset($column_setting[0]['company_url'])&&($column_setting[0]['company_url']==1)){?>							
                                    <th>Company Url</th>
                                    <?php } if(isset($column_setting[0]['officers_url'])&&($column_setting[0]['officers_url']==1)){?>							
                                    <th>Officers Url</th>
                                    <?php } if(isset($column_setting[0]['incorporation_date'])&&($column_setting[0]['incorporation_date']==1)){?>							
                                    <th>Incorporation Date</th>
                                    <?php } if(isset($column_setting[0]['register_address'])&&($column_setting[0]['register_address']==1)){?>							
                                    <th>Register Address</th>
                                    <?php } if(isset($column_setting[0]['company_type'])&&($column_setting[0]['company_type']==1)){?>	
                                    <th>Company Type</th>
                                    <?php } if(isset($column_setting[0]['company_sic'])&&($column_setting[0]['company_sic']==1)){?>	
                                    <th>Company SIC</th>
                                    <?php } if(isset($column_setting[0]['company_utr'])&&($column_setting[0]['company_utr']==1)){?>	
                                    <th>Company UTR</th>
                                    <?php } if(isset($column_setting[0]['companies_house_authorisation_code'])&&($column_setting[0]['companies_house_authorisation_code']==1)){?>	
                                    <th>Authentication Code</th>
                                    <?php } if(isset($column_setting[0]['managed'])&&($column_setting[0]['managed']==1)){?>	
                                    <th>Managed</th>
                                    <?php } if(isset($column_setting[0]['tradingas'])&&($column_setting[0]['tradingas']==1)){?>	
                                    <th>Trading As</th>
                                    <?php } if(isset($column_setting[0]['commenced_trading'])&&($column_setting[0]['commenced_trading']==1)){?>	
                                    <th>Commenced Trading</th>
                                    <?php } if(isset($column_setting[0]['registered_for_sa'])&&($column_setting[0]['registered_for_sa']==1)){?>	
                                    <th>Registered for SA</th>
                                    <?php } if(isset($column_setting[0]['business_details_turnover'])&&($column_setting[0]['business_details_turnover']==1)){?>	
                                    <th>Business Turnover</th>
                                    <?php } if(isset($column_setting[0]['business_details_nature_of_business'])&&($column_setting[0]['business_details_nature_of_business']==1)){?>	
                                    <th>Nature of business</th>
                                    <?php } if(isset($column_setting[0]['person'])&&($column_setting[0]['person']==1)){?>	
                                    <th>Person</th>
                                    <?php } if(isset($column_setting[0]['title'])&&($column_setting[0]['title']==1)){?>	
                                    <th>Title</th>
                                    <?php } if(isset($column_setting[0]['first_name'])&&($column_setting[0]['first_name']==1)){?>	
                                    <th>First Name</th>
                                    <?php } if(isset($column_setting[0]['middle_name'])&&($column_setting[0]['middle_name']==1)){?>	
                                    <th>Middle Name</th>
                                    <?php } if(isset($column_setting[0]['last_name'])&&($column_setting[0]['last_name']==1)){?>	
                                    <th>Last Name</th>
                                    <?php } if(isset($column_setting[0]['create_self_assessment_client'])&&($column_setting[0]['create_self_assessment_client']==1)){?>	
                                    <th>Create Self Assessment Client</th>
                                    <?php } if(isset($column_setting[0]['client_does_self_assessment'])&&($column_setting[0]['client_does_self_assessment']==1)){?>	
                                    <th>Client Does Self Assessment</th>
                                    <?php } if(isset($column_setting[0]['preferred_name'])&&($column_setting[0]['preferred_name']==1)){?>	
                                    <th>Perferred Name</th>
                                    <?php } if(isset($column_setting[0]['date_of_birth'])&&($column_setting[0]['date_of_birth']==1)){?>	
                                    <th>DOB</th>
                                    <?php } if(isset($column_setting[0]['email'])&&($column_setting[0]['email']==1)){?>	
                                    <th>Email</th>
                                    <?php } if(isset($column_setting[0]['postal_address'])&&($column_setting[0]['postal_address']==1)){?>	
                                    <th>Postal Address</th>
                                    <?php } if(isset($column_setting[0]['telephone_number'])&&($column_setting[0]['telephone_number']==1)){?>	
                                    <th>Telephone Number</th>
                                    <?php } if(isset($column_setting[0]['mobile_number'])&&($column_setting[0]['mobile_number']==1)){?>		
                                    <th>Mobile Number</th>
                                    <?php } if(isset($column_setting[0]['ni_number'])&&($column_setting[0]['ni_number']==1)){?>								
                                    <th>NI Number</th>
                                    <?php } if(isset($column_setting[0]['personal_utr_number'])&&($column_setting[0]['personal_utr_number']==1)){?>								
                                    <th>Personal UTR Number</th>
                                    <?php } if(isset($column_setting[0]['terms_signed'])&&($column_setting[0]['terms_signed']==1)){?>								
                                    <th>Terms Signed</th>
                                    <?php } if(isset($column_setting[0]['photo_id_verified'])&&($column_setting[0]['photo_id_verified']==1)){?>								
                                    <th>Photoid Verified</th>
                                    <?php } if(isset($column_setting[0]['address_verified'])&&($column_setting[0]['address_verified']==1)){?>								
                                    <th>Address Verified</th>
                                    <?php } if(isset($column_setting[0]['previous'])&&($column_setting[0]['previous']==1)){?>								
                                    <th>Previous</th>
                                    <?php } if(isset($column_setting[0]['current'])&&($column_setting[0]['current']==1)){?>								
                                    <th>Current</th>
                                    <?php } if(isset($column_setting[0]['ir_35_notes'])&&($column_setting[0]['ir_35_notes']==1)){?>								
                                    <th>Ir35 Notes</th>
                                    <?php } if(isset($column_setting[0]['previous_accountant'])&&($column_setting[0]['previous_accountant']==1)){?>								
                                    <th>Previous Accountant</th>
                                    <?php } if(isset($column_setting[0]['refered_by'])&&($column_setting[0]['refered_by']==1)){?>								
                                    <th>Refered By</th>
                                    <?php } if(isset($column_setting[0]['allocated_office'])&&($column_setting[0]['allocated_office']==1)){?>								
                                    <th>Allocated Office</th>
                                    <?php } if(isset($column_setting[0]['inital_contact'])&&($column_setting[0]['inital_contact']==1)){?>								
                                    <th>Initial Contact</th>
                                    <?php } if(isset($column_setting[0]['quote_email_sent'])&&($column_setting[0]['quote_email_sent']==1)){?>								
                                    <th>Quote Email Sent</th>
                                    <?php } if(isset($column_setting[0]['welcome_email_sent'])&&($column_setting[0]['welcome_email_sent']==1)){?>								
                                    <th>Welcome Email</th>
                                    <?php } if(isset($column_setting[0]['internal_reference'])&&($column_setting[0]['internal_reference']==1)){?>								
                                    <th>Internal Reference</th>
                                    <?php } if(isset($column_setting[0]['profession'])&&($column_setting[0]['profession']==1)){?>								
                                    <th>Profession</th>
                                    <?php } if(isset($column_setting[0]['website'])&&($column_setting[0]['website']==1)){?>								
                                    <th>Website</th>
                                    <?php } if(isset($column_setting[0]['accounting_system'])&&($column_setting[0]['accounting_system']==1)){?>								
                                    <th>Accounting System</th>
                                    <?php } if(isset($column_setting[0]['notes'])&&($column_setting[0]['notes']==1)){?>								
                                    <th>Notes</th>
                                    <?php } if(isset($column_setting[0]['other_urgent'])&&($column_setting[0]['other_urgent']==1)){?>								
                                    <th>OtherUrgent</th>
                                    <?php } if(isset($column_setting[0]['accounts'])&&($column_setting[0]['accounts']==1)){?>								
                                    <th>Accounts</th>
                                    <?php } if(isset($column_setting[0]['bookkeeping'])&&($column_setting[0]['bookkeeping']==1)){?>								
                                    <th>Bookkeeping</th>
                                    <?php } if(isset($column_setting[0]['ct600_return'])&&($column_setting[0]['ct600_return']==1)){?>								
                                    <th>CT600 Return</th>
                                    <?php } if(isset($column_setting[0]['payroll'])&&($column_setting[0]['payroll']==1)){?>								
                                    <th>Payroll</th>
                                    <?php } if(isset($column_setting[0]['auto_enrolment'])&&($column_setting[0]['auto_enrolment']==1)){?>								
                                    <th>Auto-Enrollement</th>
                                    <?php } if(isset($column_setting[0]['vat_returns'])&&($column_setting[0]['vat_returns']==1)){?>								
                                    <th>VAT Returns</th>
                                    <?php } if(isset($column_setting[0]['confirmation_statement'])&&($column_setting[0]['confirmation_statement']==1)){?>								
                                    <th>Confirmation Statement</th>
                                    <?php } if(isset($column_setting[0]['cis'])&&($column_setting[0]['cis']==1)){?>								
                                    <th>CIS</th>
                                    <?php } if(isset($column_setting[0]['p11d'])&&($column_setting[0]['p11d']==1)){?>								
                                    <th>P11D</th>
                                    <?php } if(isset($column_setting[0]['invesitgation_insurance'])&&($column_setting[0]['invesitgation_insurance']==1)){?>								
                                    <th>Investigation Insurance</th>
                                    <?php } if(isset($column_setting[0]['services_registered_address'])&&($column_setting[0]['services_registered_address']==1)){?>								
                                    <th>Registered Address</th>
                                    <?php } if(isset($column_setting[0]['bill_payment'])&&($column_setting[0]['bill_payment']==1)){?>								
                                    <th>Bill Payment</th>
                                    <?php } if(isset($column_setting[0]['advice'])&&($column_setting[0]['advice']==1)){?>								
                                    <th>Advice</th>
                                    <?php } if(isset($column_setting[0]['monthly_charge'])&&($column_setting[0]['monthly_charge']==1)){?>								
                                    <th>Monthly Charge</th>
                                    <?php } if(isset($column_setting[0]['annual_charge'])&&($column_setting[0]['annual_charge']==1)){?>								
                                    <th>Annual Charge</th>
                                    <?php } if(isset($column_setting[0]['accounts_periodend'])&&($column_setting[0]['accounts_periodend']==1)){?>								
                                    <th>Accounts Periodend</th>
                                    <?php } if(isset($column_setting[0]['ch_yearend'])&&($column_setting[0]['ch_yearend']==1)){?>								
                                    <th>CH Year End</th>
                                    <?php } if(isset($column_setting[0]['hmrc_yearend'])&&($column_setting[0]['hmrc_yearend']==1)){?>								
                                    <th>HMRC Year End</th>
                                    <?php } if(isset($column_setting[0]['ch_accounts_next_due'])&&($column_setting[0]['ch_accounts_next_due']==1)){?>								
                                    <th>CH Accounts Next Due</th>
                                    <?php } if(isset($column_setting[0]['ct600_due'])&&($column_setting[0]['ct600_due']==1)){?>								
                                    <th>CT600 Due</th>
                                    <?php } if(isset($column_setting[0]['companies_house_email_remainder'])&&($column_setting[0]['companies_house_email_remainder']==1)){?>								
                                    <th>CH Email Reminder</th>
                                    <?php } if(isset($column_setting[0]['latest_action'])&&($column_setting[0]['latest_action']==1)){?>								
                                    <th>Latest Action</th>
                                    <?php } if(isset($column_setting[0]['latest_action_date'])&&($column_setting[0]['latest_action_date']==1)){?>								
                                    <th>Latest Action Date</th>
                                    <?php } if(isset($column_setting[0]['records_received_date'])&&($column_setting[0]['records_received_date']==1)){?>								
                                    <th>Records Received</th>
                                    <?php } if(isset($column_setting[0]['confirmation_statement_date'])&&($column_setting[0]['confirmation_statement_date']==1)){?>								
                                    <th>Confirmation Statement Date</th>
                                    <?php } if(isset($column_setting[0]['confirmation_statement_due_date'])&&($column_setting[0]['confirmation_statement_due_date']==1)){?>								
                                    <th>Confirmation Statement Due</th>
                                    <?php } if(isset($column_setting[0]['confirmation_latest_action'])&&($column_setting[0]['confirmation_latest_action']==1)){?>								
                                    <th>CS Latest Action</th>
                                    <?php } if(isset($column_setting[0]['confirmation_latest_action_date'])&&($column_setting[0]['confirmation_latest_action_date']==1)){?>								
                                    <th>CS Latest Action Date</th>
                                    <?php } if(isset($column_setting[0]['confirmation_records_received_date'])&&($column_setting[0]['confirmation_records_received_date']==1)){?>								
                                    <th>CS Records Received</th>
                                    <?php } if(isset($column_setting[0]['confirmation_officers'])&&($column_setting[0]['confirmation_officers']==1)){?>								
                                    <th>CS Officers</th>
                                    <?php } if(isset($column_setting[0]['share_capital'])&&($column_setting[0]['share_capital']==1)){?>								
                                    <th>CS Share Capital</th>
                                    <?php } if(isset($column_setting[0]['shareholders'])&&($column_setting[0]['shareholders']==1)){?>								
                                    <th>CS Shareholders</th>
                                    <?php } if(isset($column_setting[0]['people_with_significant_control'])&&($column_setting[0]['people_with_significant_control']==1)){?>								
                                    <th>CS People with Significant Control</th>
                                    <?php } if(isset($column_setting[0]['vat_quarters'])&&($column_setting[0]['vat_quarters']==1)){?>								
                                    <th>VAT Quarters</th>
                                    <?php } if(isset($column_setting[0]['vat_quater_end_date'])&&($column_setting[0]['vat_quater_end_date']==1)){?>								
                                    <th>VAT Quarter End</th>
                                    <?php } if(isset($column_setting[0]['next_return_due_date'])&&($column_setting[0]['next_return_due_date']==1)){?>								
                                    <th>VAT Next Return Date</th>
                                    <?php } if(isset($column_setting[0]['vat_latest_action'])&&($column_setting[0]['vat_latest_action']==1)){?>								
                                    <th>VAT Latest Action</th>
                                    <?php } if(isset($column_setting[0]['vat_latest_action_date'])&&($column_setting[0]['vat_latest_action_date']==1)){?>								
                                    <th>VAT Latest Action Date</th>
                                    <?php } if(isset($column_setting[0]['vat_records_received_date'])&&($column_setting[0]['vat_records_received_date']==1)){?>								
                                    <th>VAT Records Received</th>
                                    <?php } if(isset($column_setting[0]['vat_member_state'])&&($column_setting[0]['vat_member_state']==1)){?>								
                                    <th>VAT Member State</th>
                                    <?php } if(isset($column_setting[0]['vat_number'])&&($column_setting[0]['vat_number']==1)){?>								
                                    <th>VAT Number</th>
                                    <?php } if(isset($column_setting[0]['vat_date_of_registration'])&&($column_setting[0]['vat_date_of_registration']==1)){?>								
                                    <th>VAT Date of Registration</th>
                                    <?php } if(isset($column_setting[0]['vat_effective_date'])&&($column_setting[0]['vat_effective_date']==1)){?>								
                                    <th>VAT Effective Date</th>
                                    <?php } if(isset($column_setting[0]['vat_estimated_turnover'])&&($column_setting[0]['vat_estimated_turnover']==1)){?>								
                                    <th>Estimated Turnover</th>
                                    <?php } if(isset($column_setting[0]['transfer_of_going_concern'])&&($column_setting[0]['transfer_of_going_concern']==1)){?>								
                                    <th>Transfer of Going Concern</th>
                                    <?php } if(isset($column_setting[0]['involved_in_any_other_business'])&&($column_setting[0]['involved_in_any_other_business']==1)){?>								
                                    <th>Involved in Any Other Business</th>
                                    <?php } if(isset($column_setting[0]['flat_rate'])&&($column_setting[0]['flat_rate']==1)){?>								
                                    <th>Flat Rate</th>
                                    <?php } if(isset($column_setting[0]['direct_debit'])&&($column_setting[0]['direct_debit']==1)){?>								
                                    <th>Direct Debit</th>
                                    <?php } if(isset($column_setting[0]['annual_accounting_scheme'])&&($column_setting[0]['annual_accounting_scheme']==1)){?>								
                                    <th>Annual Accounting Scheme</th>
                                    <?php } if(isset($column_setting[0]['flat_rate_category'])&&($column_setting[0]['flat_rate_category']==1)){?>								
                                    <th>Flat Rate Category</th>
                                    <?php } if(isset($column_setting[0]['month_of_last_quarter_submitted'])&&($column_setting[0]['month_of_last_quarter_submitted']==1)){?>								
                                    <th>Month of Last Quarter Submitted</th>
                                    <?php } if(isset($column_setting[0]['box5_of_last_quarter_submitted'])&&($column_setting[0]['box5_of_last_quarter_submitted']==1)){?>								
                                    <th>Box 5 of Last Quarter Submitted</th>
                                    <?php } if(isset($column_setting[0]['vat_address'])&&($column_setting[0]['vat_address']==1)){?>								
                                    <th>VAT Address</th>
                                    <?php } if(isset($column_setting[0]['vat_notes'])&&($column_setting[0]['vat_notes']==1)){?>								
                                    <th>VAT Notes</th>
                                    <?php } if(isset($column_setting[0]['employers_reference'])&&($column_setting[0]['employers_reference']==1)){?>								
                                    <th>Employers Reference</th>
                                    <?php } if(isset($column_setting[0]['accounts_office_reference'])&&($column_setting[0]['accounts_office_reference']==1)){?>								
                                    <th>Accounts Office Reference</th>
                                    <?php } if(isset($column_setting[0]['years_required'])&&($column_setting[0]['years_required']==1)){?>								
                                    <th>Years Required</th>
                                    <?php } if(isset($column_setting[0]['annual_or_monthly_submissions'])&&($column_setting[0]['annual_or_monthly_submissions']==1)){?>								
                                    <th>Annual/Monthly Submissions</th>
                                    <?php } if(isset($column_setting[0]['irregular_montly_pay'])&&($column_setting[0]['irregular_montly_pay']==1)){?>								
                                    <th>Irregular Monthly Pay</th>
                                    <?php } if(isset($column_setting[0]['nil_eps'])&&($column_setting[0]['nil_eps']==1)){?>								
                                    <th>Nil EPS</th>
                                    <?php } if(isset($column_setting[0]['no_of_employees'])&&($column_setting[0]['no_of_employees']==1)){?>								
                                    <th>Number of Employees</th>
                                    <?php } if(isset($column_setting[0]['first_pay_date'])&&($column_setting[0]['first_pay_date']==1)){?>								
                                    <th>First Pay Date</th>
                                    <?php } if(isset($column_setting[0]['rti_deadline'])&&($column_setting[0]['rti_deadline']==1)){?>								
                                    <th>RTI Deadline</th>
                                    <?php } if(isset($column_setting[0]['paye_scheme_ceased'])&&($column_setting[0]['paye_scheme_ceased']==1)){?>								
                                    <th>PAYE Scheme Ceased</th>
                                    <?php } if(isset($column_setting[0]['paye_latest_action'])&&($column_setting[0]['paye_latest_action']==1)){?>								
                                    <th>PAYE Latest Action</th>
                                    <?php } if(isset($column_setting[0]['paye_latest_action_date'])&&($column_setting[0]['paye_latest_action_date']==1)){?>								
                                    <th>PAYE Latest Action Date</th>
                                    <?php } if(isset($column_setting[0]['paye_records_received'])&&($column_setting[0]['paye_records_received']==1)){?>								
                                    <th>PAYE Records Received</th>
                                    <?php } if(isset($column_setting[0]['next_p11d_return_due'])&&($column_setting[0]['next_p11d_return_due']==1)){?>								
                                    <th>Next P11D Return Due</th>
                                    <?php } if(isset($column_setting[0]['latest_p11d_submitted'])&&($column_setting[0]['latest_p11d_submitted']==1)){?>								
                                    <th>Latest P11D Submitted</th>
                                    <?php } if(isset($column_setting[0]['p11d_latest_action'])&&($column_setting[0]['p11d_latest_action']==1)){?>								
                                    <th>P11D Latest Action</th>
                                    <?php } if(isset($column_setting[0]['p11d_latest_action_date'])&&($column_setting[0]['p11d_latest_action_date']==1)){?>								
                                    <th>P11D Latest Action Date</th>
                                    <?php } if(isset($column_setting[0]['p11d_records_received'])&&($column_setting[0]['p11d_records_received']==1)){?>								
                                    <th>P11D Records Received</th>
                                    <?php } if(isset($column_setting[0]['cis_contractor'])&&($column_setting[0]['cis_contractor']==1)){?>								
                                    <th>CIS Contractor</th>
                                    <?php } if(isset($column_setting[0]['cis_subcontractor'])&&($column_setting[0]['cis_subcontractor']==1)){?>								
                                    <th>CIS Subcontractor</th>
                                    <?php } if(isset($column_setting[0]['cis_deadline'])&&($column_setting[0]['cis_deadline']==1)){?>								
                                    <th>CIS Deadline</th>
                                    <?php } if(isset($column_setting[0]['auto_enrolment_latest_action'])&&($column_setting[0]['auto_enrolment_latest_action']==1)){?>								
                                    <th>Auto-Enrollement Latest Action</th>
                                    <?php } if(isset($column_setting[0]['auto_enrolment_latest_action_date'])&&($column_setting[0]['auto_enrolment_latest_action_date']==1)){?>								
                                    <th>Auto-Enrollement Latest Action Date</th>
                                    <?php } if(isset($column_setting[0]['auto_enrolment_records_received'])&&($column_setting[0]['auto_enrolment_records_received']==1)){?>								
                                    <th>Auto-Enrollement Record Received</th>
                                    <?php } if(isset($column_setting[0]['auto_enrolment_staging'])&&($column_setting[0]['auto_enrolment_staging']==1)){?>								
                                    <th>Auto-Enrollement Staging</th>
                                    <?php } if(isset($column_setting[0]['postponement_date'])&&($column_setting[0]['postponement_date']==1)){?>								
                                    <th>Postponement Date</th>
                                    <?php } if(isset($column_setting[0]['the_pensions_regulator_opt_out_date'])&&($column_setting[0]['the_pensions_regulator_opt_out_date']==1)){?>								
                                    <th>The Pensions Regulator Opt Out Date</th>
                                    <?php } if(isset($column_setting[0]['paye_re_enrolment_date'])&&($column_setting[0]['paye_re_enrolment_date']==1)){?>								
                                    <th>Re-Enrolment Date</th>
                                    <?php } if(isset($column_setting[0]['paye_pension_provider'])&&($column_setting[0]['paye_pension_provider']==1)){?>								
                                    <th>Pension Provider</th>
                                    <?php } if(isset($column_setting[0]['pension_id'])&&($column_setting[0]['pension_id']==1)){?>								
                                    <th>Pension ID</th>
                                    <?php } if(isset($column_setting[0]['declaration_of_compliance_due_date'])&&($column_setting[0]['declaration_of_compliance_due_date']==1)){?>								
                                    <th>Declaration of Compliance Due</th>
                                    <?php } if(isset($column_setting[0]['declaration_of_compliance_submission'])&&($column_setting[0]['declaration_of_compliance_submission']==1)){?>								
                                    <th>Declaration of Compliance Submission</th>
                                    <?php } if(isset($column_setting[0]['pension_deadline'])&&($column_setting[0]['pension_deadline']==1)){?>								
                                    <th>Pension Deadline</th>
                                    <?php } if(isset($column_setting[0]['note'])&&($column_setting[0]['note']==1)){?>								
                                    <th>PAYE Notes</th>
                                    <?php } if(isset($column_setting[0]['registration_fee_paid'])&&($column_setting[0]['registration_fee_paid']==1)){?>								
                                    <th>Registration Fee Paid</th>
                                    <?php } if(isset($column_setting[0]['64_8_registration'])&&($column_setting[0]['64_8_registration']==1)){?>								
                                    <th>64-8 Registration</th>
                                    <?php } if(isset($column_setting[0]['packages'])&&($column_setting[0]['packages']==1)){?>								
                                    <th>Packages</th>
                                    <?php } if(isset($column_setting[0]['gender'])&&($column_setting[0]['gender']==1)){?>								
                                    <th>Gender</th>
                                    <?php } ?>	
                                    <th>Actions</th>
                                  </tr>
                                </thead>
                                 <tfoot>
            <tr>
               <th><input type="checkbox"  id="bulkDelete"/></th>
                                    <?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
                                    <th>Profile</th>
                                    <?php } ?>      
                                    <th>Name</th>
                                    <!-- <th>CRM-Username</th> -->
                                    <th>Updates</th>
                                    <th>Active</th>
                                    <th>CompanyStatus</th>
                                    <th>User Type</th>
                                    <th>Status</th>
                                    <?php if(isset($column_setting[0]['user_id'])&&($column_setting[0]['user_id']==1)){?>
                                    <th>Client Id</th>
                                    <?php } 
                                      //if(isset($column_setting[0]['company_name'])&&($column_setting[0]['company_name']==1)){?>             
                                    <th <?php echo (isset($column_setting[0]['company_name'])&&($column_setting[0]['company_name']==1)) ? '' : 'style="display:none;"'; ?> >Company Name</th>
                                    <?php //} 
                                      if(isset($column_setting[0]['legal_form'])&&($column_setting[0]['legal_form']==1)){?>             
                                    <th>Legal Form</th>
                                    <?php } if(isset($column_setting[0]['allocation_holder'])&&($column_setting[0]['allocation_holder']==1)){?>             
                                    <th>Allocation Holder</th>
                                    <?php } if(isset($column_setting[0]['company_number'])&&($column_setting[0]['company_number']==1)){?>             
                                    <th>Company Number</th>
                                    <?php } if(isset($column_setting[0]['company_url'])&&($column_setting[0]['company_url']==1)){?>             
                                    <th>Company Url</th>
                                    <?php } if(isset($column_setting[0]['officers_url'])&&($column_setting[0]['officers_url']==1)){?>             
                                    <th>Officers Url</th>
                                    <?php } if(isset($column_setting[0]['incorporation_date'])&&($column_setting[0]['incorporation_date']==1)){?>             
                                    <th>Incorporation Date</th>
                                    <?php } if(isset($column_setting[0]['register_address'])&&($column_setting[0]['register_address']==1)){?>             
                                    <th>Register Address</th>
                                    <?php } if(isset($column_setting[0]['company_type'])&&($column_setting[0]['company_type']==1)){?> 
                                    <th>Company Type</th>
                                    <?php } if(isset($column_setting[0]['company_sic'])&&($column_setting[0]['company_sic']==1)){?> 
                                    <th>Company SIC</th>
                                    <?php } if(isset($column_setting[0]['company_utr'])&&($column_setting[0]['company_utr']==1)){?> 
                                    <th>Company UTR</th>
                                    <?php } if(isset($column_setting[0]['companies_house_authorisation_code'])&&($column_setting[0]['companies_house_authorisation_code']==1)){?> 
                                    <th>Authentication Code</th>
                                    <?php } if(isset($column_setting[0]['managed'])&&($column_setting[0]['managed']==1)){?> 
                                    <th>Managed</th>
                                    <?php } if(isset($column_setting[0]['tradingas'])&&($column_setting[0]['tradingas']==1)){?> 
                                    <th>Trading As</th>
                                    <?php } if(isset($column_setting[0]['commenced_trading'])&&($column_setting[0]['commenced_trading']==1)){?> 
                                    <th>Commenced Trading</th>
                                    <?php } if(isset($column_setting[0]['registered_for_sa'])&&($column_setting[0]['registered_for_sa']==1)){?> 
                                    <th>Registered for SA</th>
                                    <?php } if(isset($column_setting[0]['business_details_turnover'])&&($column_setting[0]['business_details_turnover']==1)){?> 
                                    <th>Business Turnover</th>
                                    <?php } if(isset($column_setting[0]['business_details_nature_of_business'])&&($column_setting[0]['business_details_nature_of_business']==1)){?> 
                                    <th>Nature of business</th>
                                    <?php } if(isset($column_setting[0]['person'])&&($column_setting[0]['person']==1)){?> 
                                    <th>Person</th>
                                    <?php } if(isset($column_setting[0]['title'])&&($column_setting[0]['title']==1)){?> 
                                    <th>Title</th>
                                    <?php } if(isset($column_setting[0]['first_name'])&&($column_setting[0]['first_name']==1)){?> 
                                    <th>First Name</th>
                                    <?php } if(isset($column_setting[0]['middle_name'])&&($column_setting[0]['middle_name']==1)){?> 
                                    <th>Middle Name</th>
                                    <?php } if(isset($column_setting[0]['last_name'])&&($column_setting[0]['last_name']==1)){?> 
                                    <th>Last Name</th>
                                    <?php } if(isset($column_setting[0]['create_self_assessment_client'])&&($column_setting[0]['create_self_assessment_client']==1)){?> 
                                    <th>Create Self Assessment Client</th>
                                    <?php } if(isset($column_setting[0]['client_does_self_assessment'])&&($column_setting[0]['client_does_self_assessment']==1)){?> 
                                    <th>Client Does Self Assessment</th>
                                    <?php } if(isset($column_setting[0]['preferred_name'])&&($column_setting[0]['preferred_name']==1)){?> 
                                    <th>Perferred Name</th>
                                    <?php } if(isset($column_setting[0]['date_of_birth'])&&($column_setting[0]['date_of_birth']==1)){?> 
                                    <th>DOB</th>
                                    <?php } if(isset($column_setting[0]['email'])&&($column_setting[0]['email']==1)){?> 
                                    <th>Email</th>
                                    <?php } if(isset($column_setting[0]['postal_address'])&&($column_setting[0]['postal_address']==1)){?> 
                                    <th>Postal Address</th>
                                    <?php } if(isset($column_setting[0]['telephone_number'])&&($column_setting[0]['telephone_number']==1)){?> 
                                    <th>Telephone Number</th>
                                    <?php } if(isset($column_setting[0]['mobile_number'])&&($column_setting[0]['mobile_number']==1)){?>   
                                    <th>Mobile Number</th>
                                    <?php } if(isset($column_setting[0]['ni_number'])&&($column_setting[0]['ni_number']==1)){?>               
                                    <th>NI Number</th>
                                    <?php } if(isset($column_setting[0]['personal_utr_number'])&&($column_setting[0]['personal_utr_number']==1)){?>               
                                    <th>Personal UTR Number</th>
                                    <?php } if(isset($column_setting[0]['terms_signed'])&&($column_setting[0]['terms_signed']==1)){?>               
                                    <th>Terms Signed</th>
                                    <?php } if(isset($column_setting[0]['photo_id_verified'])&&($column_setting[0]['photo_id_verified']==1)){?>               
                                    <th>Photoid Verified</th>
                                    <?php } if(isset($column_setting[0]['address_verified'])&&($column_setting[0]['address_verified']==1)){?>               
                                    <th>Address Verified</th>
                                    <?php } if(isset($column_setting[0]['previous'])&&($column_setting[0]['previous']==1)){?>               
                                    <th>Previous</th>
                                    <?php } if(isset($column_setting[0]['current'])&&($column_setting[0]['current']==1)){?>               
                                    <th>Current</th>
                                    <?php } if(isset($column_setting[0]['ir_35_notes'])&&($column_setting[0]['ir_35_notes']==1)){?>               
                                    <th>Ir35 Notes</th>
                                    <?php } if(isset($column_setting[0]['previous_accountant'])&&($column_setting[0]['previous_accountant']==1)){?>               
                                    <th>Previous Accountant</th>
                                    <?php } if(isset($column_setting[0]['refered_by'])&&($column_setting[0]['refered_by']==1)){?>               
                                    <th>Refered By</th>
                                    <?php } if(isset($column_setting[0]['allocated_office'])&&($column_setting[0]['allocated_office']==1)){?>               
                                    <th>Allocated Office</th>
                                    <?php } if(isset($column_setting[0]['inital_contact'])&&($column_setting[0]['inital_contact']==1)){?>               
                                    <th>Initial Contact</th>
                                    <?php } if(isset($column_setting[0]['quote_email_sent'])&&($column_setting[0]['quote_email_sent']==1)){?>               
                                    <th>Quote Email Sent</th>
                                    <?php } if(isset($column_setting[0]['welcome_email_sent'])&&($column_setting[0]['welcome_email_sent']==1)){?>               
                                    <th>Welcome Email</th>
                                    <?php } if(isset($column_setting[0]['internal_reference'])&&($column_setting[0]['internal_reference']==1)){?>               
                                    <th>Internal Reference</th>
                                    <?php } if(isset($column_setting[0]['profession'])&&($column_setting[0]['profession']==1)){?>               
                                    <th>Profession</th>
                                    <?php } if(isset($column_setting[0]['website'])&&($column_setting[0]['website']==1)){?>               
                                    <th>Website</th>
                                    <?php } if(isset($column_setting[0]['accounting_system'])&&($column_setting[0]['accounting_system']==1)){?>               
                                    <th>Accounting System</th>
                                    <?php } if(isset($column_setting[0]['notes'])&&($column_setting[0]['notes']==1)){?>               
                                    <th>Notes</th>
                                    <?php } if(isset($column_setting[0]['other_urgent'])&&($column_setting[0]['other_urgent']==1)){?>               
                                    <th>OtherUrgent</th>
                                    <?php } if(isset($column_setting[0]['accounts'])&&($column_setting[0]['accounts']==1)){?>               
                                    <th>Accounts</th>
                                    <?php } if(isset($column_setting[0]['bookkeeping'])&&($column_setting[0]['bookkeeping']==1)){?>               
                                    <th>Bookkeeping</th>
                                    <?php } if(isset($column_setting[0]['ct600_return'])&&($column_setting[0]['ct600_return']==1)){?>               
                                    <th>CT600 Return</th>
                                    <?php } if(isset($column_setting[0]['payroll'])&&($column_setting[0]['payroll']==1)){?>               
                                    <th>Payroll</th>
                                    <?php } if(isset($column_setting[0]['auto_enrolment'])&&($column_setting[0]['auto_enrolment']==1)){?>               
                                    <th>Auto-Enrollement</th>
                                    <?php } if(isset($column_setting[0]['vat_returns'])&&($column_setting[0]['vat_returns']==1)){?>               
                                    <th>VAT Returns</th>
                                    <?php } if(isset($column_setting[0]['confirmation_statement'])&&($column_setting[0]['confirmation_statement']==1)){?>               
                                    <th>Confirmation Statement</th>
                                    <?php } if(isset($column_setting[0]['cis'])&&($column_setting[0]['cis']==1)){?>               
                                    <th>CIS</th>
                                    <?php } if(isset($column_setting[0]['p11d'])&&($column_setting[0]['p11d']==1)){?>               
                                    <th>P11D</th>
                                    <?php } if(isset($column_setting[0]['invesitgation_insurance'])&&($column_setting[0]['invesitgation_insurance']==1)){?>               
                                    <th>Investigation Insurance</th>
                                    <?php } if(isset($column_setting[0]['services_registered_address'])&&($column_setting[0]['services_registered_address']==1)){?>               
                                    <th>Registered Address</th>
                                    <?php } if(isset($column_setting[0]['bill_payment'])&&($column_setting[0]['bill_payment']==1)){?>               
                                    <th>Bill Payment</th>
                                    <?php } if(isset($column_setting[0]['advice'])&&($column_setting[0]['advice']==1)){?>               
                                    <th>Advice</th>
                                    <?php } if(isset($column_setting[0]['monthly_charge'])&&($column_setting[0]['monthly_charge']==1)){?>               
                                    <th>Monthly Charge</th>
                                    <?php } if(isset($column_setting[0]['annual_charge'])&&($column_setting[0]['annual_charge']==1)){?>               
                                    <th>Annual Charge</th>
                                    <?php } if(isset($column_setting[0]['accounts_periodend'])&&($column_setting[0]['accounts_periodend']==1)){?>               
                                    <th>Accounts Periodend</th>
                                    <?php } if(isset($column_setting[0]['ch_yearend'])&&($column_setting[0]['ch_yearend']==1)){?>               
                                    <th>CH Year End</th>
                                    <?php } if(isset($column_setting[0]['hmrc_yearend'])&&($column_setting[0]['hmrc_yearend']==1)){?>               
                                    <th>HMRC Year End</th>
                                    <?php } if(isset($column_setting[0]['ch_accounts_next_due'])&&($column_setting[0]['ch_accounts_next_due']==1)){?>               
                                    <th>CH Accounts Next Due</th>
                                    <?php } if(isset($column_setting[0]['ct600_due'])&&($column_setting[0]['ct600_due']==1)){?>               
                                    <th>CT600 Due</th>
                                    <?php } if(isset($column_setting[0]['companies_house_email_remainder'])&&($column_setting[0]['companies_house_email_remainder']==1)){?>               
                                    <th>CH Email Reminder</th>
                                    <?php } if(isset($column_setting[0]['latest_action'])&&($column_setting[0]['latest_action']==1)){?>               
                                    <th>Latest Action</th>
                                    <?php } if(isset($column_setting[0]['latest_action_date'])&&($column_setting[0]['latest_action_date']==1)){?>               
                                    <th>Latest Action Date</th>
                                    <?php } if(isset($column_setting[0]['records_received_date'])&&($column_setting[0]['records_received_date']==1)){?>               
                                    <th>Records Received</th>
                                    <?php } if(isset($column_setting[0]['confirmation_statement_date'])&&($column_setting[0]['confirmation_statement_date']==1)){?>               
                                    <th>Confirmation Statement Date</th>
                                    <?php } if(isset($column_setting[0]['confirmation_statement_due_date'])&&($column_setting[0]['confirmation_statement_due_date']==1)){?>               
                                    <th>Confirmation Statement Due</th>
                                    <?php } if(isset($column_setting[0]['confirmation_latest_action'])&&($column_setting[0]['confirmation_latest_action']==1)){?>               
                                    <th>CS Latest Action</th>
                                    <?php } if(isset($column_setting[0]['confirmation_latest_action_date'])&&($column_setting[0]['confirmation_latest_action_date']==1)){?>               
                                    <th>CS Latest Action Date</th>
                                    <?php } if(isset($column_setting[0]['confirmation_records_received_date'])&&($column_setting[0]['confirmation_records_received_date']==1)){?>               
                                    <th>CS Records Received</th>
                                    <?php } if(isset($column_setting[0]['confirmation_officers'])&&($column_setting[0]['confirmation_officers']==1)){?>               
                                    <th>CS Officers</th>
                                    <?php } if(isset($column_setting[0]['share_capital'])&&($column_setting[0]['share_capital']==1)){?>               
                                    <th>CS Share Capital</th>
                                    <?php } if(isset($column_setting[0]['shareholders'])&&($column_setting[0]['shareholders']==1)){?>               
                                    <th>CS Shareholders</th>
                                    <?php } if(isset($column_setting[0]['people_with_significant_control'])&&($column_setting[0]['people_with_significant_control']==1)){?>               
                                    <th>CS People with Significant Control</th>
                                    <?php } if(isset($column_setting[0]['vat_quarters'])&&($column_setting[0]['vat_quarters']==1)){?>               
                                    <th>VAT Quarters</th>
                                    <?php } if(isset($column_setting[0]['vat_quater_end_date'])&&($column_setting[0]['vat_quater_end_date']==1)){?>               
                                    <th>VAT Quarter End</th>
                                    <?php } if(isset($column_setting[0]['next_return_due_date'])&&($column_setting[0]['next_return_due_date']==1)){?>               
                                    <th>VAT Next Return Date</th>
                                    <?php } if(isset($column_setting[0]['vat_latest_action'])&&($column_setting[0]['vat_latest_action']==1)){?>               
                                    <th>VAT Latest Action</th>
                                    <?php } if(isset($column_setting[0]['vat_latest_action_date'])&&($column_setting[0]['vat_latest_action_date']==1)){?>               
                                    <th>VAT Latest Action Date</th>
                                    <?php } if(isset($column_setting[0]['vat_records_received_date'])&&($column_setting[0]['vat_records_received_date']==1)){?>               
                                    <th>VAT Records Received</th>
                                    <?php } if(isset($column_setting[0]['vat_member_state'])&&($column_setting[0]['vat_member_state']==1)){?>               
                                    <th>VAT Member State</th>
                                    <?php } if(isset($column_setting[0]['vat_number'])&&($column_setting[0]['vat_number']==1)){?>               
                                    <th>VAT Number</th>
                                    <?php } if(isset($column_setting[0]['vat_date_of_registration'])&&($column_setting[0]['vat_date_of_registration']==1)){?>               
                                    <th>VAT Date of Registration</th>
                                    <?php } if(isset($column_setting[0]['vat_effective_date'])&&($column_setting[0]['vat_effective_date']==1)){?>               
                                    <th>VAT Effective Date</th>
                                    <?php } if(isset($column_setting[0]['vat_estimated_turnover'])&&($column_setting[0]['vat_estimated_turnover']==1)){?>               
                                    <th>Estimated Turnover</th>
                                    <?php } if(isset($column_setting[0]['transfer_of_going_concern'])&&($column_setting[0]['transfer_of_going_concern']==1)){?>               
                                    <th>Transfer of Going Concern</th>
                                    <?php } if(isset($column_setting[0]['involved_in_any_other_business'])&&($column_setting[0]['involved_in_any_other_business']==1)){?>               
                                    <th>Involved in Any Other Business</th>
                                    <?php } if(isset($column_setting[0]['flat_rate'])&&($column_setting[0]['flat_rate']==1)){?>               
                                    <th>Flat Rate</th>
                                    <?php } if(isset($column_setting[0]['direct_debit'])&&($column_setting[0]['direct_debit']==1)){?>               
                                    <th>Direct Debit</th>
                                    <?php } if(isset($column_setting[0]['annual_accounting_scheme'])&&($column_setting[0]['annual_accounting_scheme']==1)){?>               
                                    <th>Annual Accounting Scheme</th>
                                    <?php } if(isset($column_setting[0]['flat_rate_category'])&&($column_setting[0]['flat_rate_category']==1)){?>               
                                    <th>Flat Rate Category</th>
                                    <?php } if(isset($column_setting[0]['month_of_last_quarter_submitted'])&&($column_setting[0]['month_of_last_quarter_submitted']==1)){?>               
                                    <th>Month of Last Quarter Submitted</th>
                                    <?php } if(isset($column_setting[0]['box5_of_last_quarter_submitted'])&&($column_setting[0]['box5_of_last_quarter_submitted']==1)){?>               
                                    <th>Box 5 of Last Quarter Submitted</th>
                                    <?php } if(isset($column_setting[0]['vat_address'])&&($column_setting[0]['vat_address']==1)){?>               
                                    <th>VAT Address</th>
                                    <?php } if(isset($column_setting[0]['vat_notes'])&&($column_setting[0]['vat_notes']==1)){?>               
                                    <th>VAT Notes</th>
                                    <?php } if(isset($column_setting[0]['employers_reference'])&&($column_setting[0]['employers_reference']==1)){?>               
                                    <th>Employers Reference</th>
                                    <?php } if(isset($column_setting[0]['accounts_office_reference'])&&($column_setting[0]['accounts_office_reference']==1)){?>               
                                    <th>Accounts Office Reference</th>
                                    <?php } if(isset($column_setting[0]['years_required'])&&($column_setting[0]['years_required']==1)){?>               
                                    <th>Years Required</th>
                                    <?php } if(isset($column_setting[0]['annual_or_monthly_submissions'])&&($column_setting[0]['annual_or_monthly_submissions']==1)){?>               
                                    <th>Annual/Monthly Submissions</th>
                                    <?php } if(isset($column_setting[0]['irregular_montly_pay'])&&($column_setting[0]['irregular_montly_pay']==1)){?>               
                                    <th>Irregular Monthly Pay</th>
                                    <?php } if(isset($column_setting[0]['nil_eps'])&&($column_setting[0]['nil_eps']==1)){?>               
                                    <th>Nil EPS</th>
                                    <?php } if(isset($column_setting[0]['no_of_employees'])&&($column_setting[0]['no_of_employees']==1)){?>               
                                    <th>Number of Employees</th>
                                    <?php } if(isset($column_setting[0]['first_pay_date'])&&($column_setting[0]['first_pay_date']==1)){?>               
                                    <th>First Pay Date</th>
                                    <?php } if(isset($column_setting[0]['rti_deadline'])&&($column_setting[0]['rti_deadline']==1)){?>               
                                    <th>RTI Deadline</th>
                                    <?php } if(isset($column_setting[0]['paye_scheme_ceased'])&&($column_setting[0]['paye_scheme_ceased']==1)){?>               
                                    <th>PAYE Scheme Ceased</th>
                                    <?php } if(isset($column_setting[0]['paye_latest_action'])&&($column_setting[0]['paye_latest_action']==1)){?>               
                                    <th>PAYE Latest Action</th>
                                    <?php } if(isset($column_setting[0]['paye_latest_action_date'])&&($column_setting[0]['paye_latest_action_date']==1)){?>               
                                    <th>PAYE Latest Action Date</th>
                                    <?php } if(isset($column_setting[0]['paye_records_received'])&&($column_setting[0]['paye_records_received']==1)){?>               
                                    <th>PAYE Records Received</th>
                                    <?php } if(isset($column_setting[0]['next_p11d_return_due'])&&($column_setting[0]['next_p11d_return_due']==1)){?>               
                                    <th>Next P11D Return Due</th>
                                    <?php } if(isset($column_setting[0]['latest_p11d_submitted'])&&($column_setting[0]['latest_p11d_submitted']==1)){?>               
                                    <th>Latest P11D Submitted</th>
                                    <?php } if(isset($column_setting[0]['p11d_latest_action'])&&($column_setting[0]['p11d_latest_action']==1)){?>               
                                    <th>P11D Latest Action</th>
                                    <?php } if(isset($column_setting[0]['p11d_latest_action_date'])&&($column_setting[0]['p11d_latest_action_date']==1)){?>               
                                    <th>P11D Latest Action Date</th>
                                    <?php } if(isset($column_setting[0]['p11d_records_received'])&&($column_setting[0]['p11d_records_received']==1)){?>               
                                    <th>P11D Records Received</th>
                                    <?php } if(isset($column_setting[0]['cis_contractor'])&&($column_setting[0]['cis_contractor']==1)){?>               
                                    <th>CIS Contractor</th>
                                    <?php } if(isset($column_setting[0]['cis_subcontractor'])&&($column_setting[0]['cis_subcontractor']==1)){?>               
                                    <th>CIS Subcontractor</th>
                                    <?php } if(isset($column_setting[0]['cis_deadline'])&&($column_setting[0]['cis_deadline']==1)){?>               
                                    <th>CIS Deadline</th>
                                    <?php } if(isset($column_setting[0]['auto_enrolment_latest_action'])&&($column_setting[0]['auto_enrolment_latest_action']==1)){?>               
                                    <th>Auto-Enrollement Latest Action</th>
                                    <?php } if(isset($column_setting[0]['auto_enrolment_latest_action_date'])&&($column_setting[0]['auto_enrolment_latest_action_date']==1)){?>               
                                    <th>Auto-Enrollement Latest Action Date</th>
                                    <?php } if(isset($column_setting[0]['auto_enrolment_records_received'])&&($column_setting[0]['auto_enrolment_records_received']==1)){?>               
                                    <th>Auto-Enrollement Record Received</th>
                                    <?php } if(isset($column_setting[0]['auto_enrolment_staging'])&&($column_setting[0]['auto_enrolment_staging']==1)){?>               
                                    <th>Auto-Enrollement Staging</th>
                                    <?php } if(isset($column_setting[0]['postponement_date'])&&($column_setting[0]['postponement_date']==1)){?>               
                                    <th>Postponement Date</th>
                                    <?php } if(isset($column_setting[0]['the_pensions_regulator_opt_out_date'])&&($column_setting[0]['the_pensions_regulator_opt_out_date']==1)){?>               
                                    <th>The Pensions Regulator Opt Out Date</th>
                                    <?php } if(isset($column_setting[0]['paye_re_enrolment_date'])&&($column_setting[0]['paye_re_enrolment_date']==1)){?>               
                                    <th>Re-Enrolment Date</th>
                                    <?php } if(isset($column_setting[0]['paye_pension_provider'])&&($column_setting[0]['paye_pension_provider']==1)){?>               
                                    <th>Pension Provider</th>
                                    <?php } if(isset($column_setting[0]['pension_id'])&&($column_setting[0]['pension_id']==1)){?>               
                                    <th>Pension ID</th>
                                    <?php } if(isset($column_setting[0]['declaration_of_compliance_due_date'])&&($column_setting[0]['declaration_of_compliance_due_date']==1)){?>               
                                    <th>Declaration of Compliance Due</th>
                                    <?php } if(isset($column_setting[0]['declaration_of_compliance_submission'])&&($column_setting[0]['declaration_of_compliance_submission']==1)){?>               
                                    <th>Declaration of Compliance Submission</th>
                                    <?php } if(isset($column_setting[0]['pension_deadline'])&&($column_setting[0]['pension_deadline']==1)){?>               
                                    <th>Pension Deadline</th>
                                    <?php } if(isset($column_setting[0]['note'])&&($column_setting[0]['note']==1)){?>               
                                    <th>PAYE Notes</th>
                                    <?php } if(isset($column_setting[0]['registration_fee_paid'])&&($column_setting[0]['registration_fee_paid']==1)){?>               
                                    <th>Registration Fee Paid</th>
                                    <?php } if(isset($column_setting[0]['64_8_registration'])&&($column_setting[0]['64_8_registration']==1)){?>               
                                    <th>64-8 Registration</th>
                                    <?php } if(isset($column_setting[0]['packages'])&&($column_setting[0]['packages']==1)){?>               
                                    <th>Packages</th>
                                    <?php } if(isset($column_setting[0]['gender'])&&($column_setting[0]['gender']==1)){?>               
                                    <th>Gender</th>
                                    <?php } ?>
            </tr>
        </tfoot>
                                <tbody>
                                  <?php foreach ($getallUser as $getallUserkey => $getallUservalue) {
                                    $update=$this->Common_mdl->select_record('update_client','user_id',$getallUservalue['id']);
                                    $company_status=$this->Common_mdl->select_record('client','user_id',$getallUservalue['id']);
                                        if($getallUservalue['role']=='1'){
                                            $role = 'Super Admin';
                                            $edit='#';
                                        }elseif($getallUservalue['role']=='2'){
                                            $role = 'Admin';
                                            $edit='#';
                                        }elseif($getallUservalue['role']=='3'){
                                          $role = 'Staff';
                                          $edit=base_url().'user/view_staff/'.$getallUservalue['id'];
                                          $house='Manual';
                                        }elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
                                            $role = 'Client';
                                            $edit=base_url().'Client/add_client/'.$getallUservalue['id'];
                                            $house='Manual';
                                        }elseif($getallUservalue['role']=='4' && $company_status['status']==1){
                                            $role = 'Client';
                                            $edit=base_url().'Client/add_client/'.$getallUservalue['id'];
                                            $house='Company House';
                                        }elseif($getallUservalue['role']=='4' && $company_status['status']==2){
                                            $role = 'Client';
                                            $edit=base_url().'Client/add_client/'.$getallUservalue['id'];
                                            $house='Import';
                                        }
                                        if($getallUservalue['status']=='1'){
                                            $status_id = 'checkboxid1';  
                                            $chked = 'selected';
                                            $status_val = '1';
                                            $active='Active';
                                        }elseif($getallUservalue['status']=='2' || $getallUservalue['status']=='0' ){
                                            $status_id = 'checkboxid2';  
                                            $chked = 'selected';
                                            $status_val = '2';
                                            $active='Inactive';
                                        } elseif($getallUservalue['status']=='3'){
                                        	$status_id = 'checkboxid3';  
                                            $chked = 'selected';
                                            $status_val = '3';
                                            $active='Frozen';
                                        }
                                    ?>
                                  <tr id="<?php echo $getallUservalue["id"]; ?>">
                                    <td>
                                      <input type='checkbox'  class='deleteRow' value="<?php echo $getallUservalue['id'];?>"  />
                                    </td>
                                    <?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
                                    <td class="user_imgs"><img src="<?php echo base_url().'/uploads/'.$getallUservalue['crm_profile_pic'];?>" alt="img"></td>
                                    <?php } ?>
                                    <td><?php echo ucfirst($getallUservalue['crm_name']);?></td>
                                    <!-- <td><?php echo ucfirst($getallUservalue['username']);?></td> -->
                                    <td>
                                      <!-- <span class="newonoff noteslink" data-rowid="<?php echo $getallUservalue['id'];?>"><?php echo ($update['counts']!='')? ($update['counts']):'0';?>  new</span> -->
                                      <button type="button" class="data-turn99 newonoff" data-toggle="modal" data-target="#myModal<?php echo $getallUservalue['id'];?>"><?php echo ($update['counts']!='')? ($update['counts']):'0';?>  new</button>
                                    </td>
                                    <td>
                                      <!-- <p class="onoff" ><input type="checkbox" name="status<?php echo $getallUservalue['id'];?>" value="<?php echo $getallUservalue['id'];?>" id="<?php echo $status_id;?>" class="status" <?php echo $chked;?> ><label for="<?php echo $status_id;?>"></label></p> -->
                                      <!-- <p class="onoff"><input type="checkbox" name="status<?php echo $getallUservalue['id'];?>" value="<?php echo $getallUservalue['id'];?>" id="status<?php echo $getallUservalue['id'];?>" class="status" <?php echo $chked;?>><label for="status<?php echo $getallUservalue['id'];?>"></label></p>  -->
                                      <select name="status<?php echo $getallUservalue['id'];?>" id="status<?php echo $getallUservalue['id'];?>" class="status" data-id="<?php echo $getallUservalue['id'];?>">
                                        <option value="1" <?php if(isset($getallUservalue['status']) && $getallUservalue['status']=='1') {?> selected="selected"<?php } ?> >Active</option>
                                        <option value="2" <?php if(isset($getallUservalue['status']) && ($getallUservalue['status']=='0'|| $getallUservalue['status']=='2' )) {?> selected="selected"<?php } ?>>Inactive</option>
                                        <option value="3" <?php if(isset($getallUservalue['status']) && $getallUservalue['status']=='3') {?> selected="selected"<?php } ?>>frozen</option>
                                      </select>
                                    </td>
                                    <td id="innac"><span><?php echo ucfirst($company_status['crm_company_status']);?></span></td>
                                    <td><?php echo $role;?></td>
                                    <td><?php echo $house;?></td>
                                    <?php if(isset($column_setting[0]['user_id'])&&($column_setting[0]['user_id']==1)){?>
                                    <td><?php echo $company_status['user_id'];?></td>
                                    <?php } //if(isset($column_setting[0]['company_name'])&&($column_setting[0]['company_name']==1)){?>
                                    <td <?php echo (isset($column_setting[0]['company_name'])&&($column_setting[0]['company_name']==1)) ? '' : 'style="display:none;"'; ?>><?php echo $company_status['crm_company_name'];?></td>
                                    <?php //} 
                                      if(isset($column_setting[0]['legal_form'])&&($column_setting[0]['legal_form']==1)){?>							
                                    <td><?php echo $company_status['crm_legal_form'];?></td>
                                    <?php } if(isset($column_setting[0]['allocation_holder'])&&($column_setting[0]['allocation_holder']==1)){?>							
                                    <td><?php echo $company_status['crm_allocation_holder'];?></td>
                                    <?php } if(isset($column_setting[0]['company_number'])&&($column_setting[0]['company_number']==1)){?>							
                                    <td><?php echo $company_status['crm_company_number'];?></td>
                                    <?php } if(isset($column_setting[0]['company_url'])&&($column_setting[0]['company_url']==1)){?>							
                                    <td><?php echo $company_status['crm_company_url'];?></td>
                                    <?php } if(isset($column_setting[0]['officers_url'])&&($column_setting[0]['officers_url']==1)){?>							
                                    <td><?php echo $company_status['crm_officers_url'];?></td>
                                    <?php } if(isset($column_setting[0]['incorporation_date'])&&($column_setting[0]['incorporation_date']==1)){?>							
                                    <td><?php echo $company_status['crm_incorporation_date'];?></td>
                                    <?php } if(isset($column_setting[0]['register_address'])&&($column_setting[0]['register_address']==1)){?>							
                                    <td><?php echo $company_status['crm_register_address'];?></td>
                                    <?php } if(isset($column_setting[0]['company_type'])&&($column_setting[0]['company_type']==1)){?>	
                                    <td><?php echo $company_status['crm_company_type'];?></td>
                                    <?php } if(isset($column_setting[0]['company_sic'])&&($column_setting[0]['company_sic']==1)){?>	
                                    <td><?php echo $company_status['crm_company_sic'];?></td>
                                    <?php } if(isset($column_setting[0]['company_utr'])&&($column_setting[0]['company_utr']==1)){?>	
                                    <td><?php echo $company_status['crm_company_utr'];?></td>
                                    <?php } if(isset($column_setting[0]['companies_house_authorisation_code'])&&($column_setting[0]['companies_house_authorisation_code']==1)){?>	
                                    <td><?php echo $company_status['crm_companies_house_authorisation_code'];?></td>
                                    <?php } if(isset($column_setting[0]['managed'])&&($column_setting[0]['managed']==1)){?>	
                                    <td><?php echo $company_status['crm_managed'];?></td>
                                    <?php } if(isset($column_setting[0]['tradingas'])&&($column_setting[0]['tradingas']==1)){?>	
                                    <td><?php echo $company_status['crm_tradingas'];?></td>
                                    <?php } if(isset($column_setting[0]['commenced_trading'])&&($column_setting[0]['commenced_trading']==1)){?>	
                                    <td><?php echo $company_status['crm_commenced_trading'];?></td>
                                    <?php } if(isset($column_setting[0]['registered_for_sa'])&&($column_setting[0]['registered_for_sa']==1)){?>	
                                    <td><?php echo $company_status['crm_registered_for_sa'];?></td>
                                    <?php } if(isset($column_setting[0]['business_details_turnover'])&&($column_setting[0]['business_details_turnover']==1)){?>	
                                    <td><?php echo $company_status['crm_business_details_turnover'];?></td>
                                    <?php } if(isset($column_setting[0]['business_details_nature_of_business'])&&($column_setting[0]['business_details_nature_of_business']==1)){?>	
                                    <td><?php echo $company_status['crm_business_details_nature_of_business'];?></td>
                                    <?php } if(isset($column_setting[0]['person'])&&($column_setting[0]['person']==1)){?>	
                                    <td><?php echo $company_status['crm_person'];?></td>
                                    <?php } if(isset($column_setting[0]['title'])&&($column_setting[0]['title']==1)){?>	
                                    <td><?php echo $company_status['crm_title'];?></td>
                                    <?php } if(isset($column_setting[0]['first_name'])&&($column_setting[0]['first_name']==1)){?>	
                                    <td><?php echo $company_status['crm_first_name'];?></td>
                                    <?php } if(isset($column_setting[0]['middle_name'])&&($column_setting[0]['middle_name']==1)){?>	
                                    <td><?php echo $company_status['crm_middle_name'];?></td>
                                    <?php } if(isset($column_setting[0]['last_name'])&&($column_setting[0]['last_name']==1)){?>	
                                    <td><?php echo $company_status['crm_last_name'];?></td>
                                    <?php } if(isset($column_setting[0]['create_self_assessment_client'])&&($column_setting[0]['create_self_assessment_client']==1)){?>	
                                    <td><?php echo $company_status['crm_create_self_assessment_client'];?></td>
                                    <?php } if(isset($column_setting[0]['client_does_self_assessment'])&&($column_setting[0]['client_does_self_assessment']==1)){?>	
                                    <td><?php echo $company_status['crm_client_does_self_assessment'];?></td>
                                    <?php } if(isset($column_setting[0]['preferred_name'])&&($column_setting[0]['preferred_name']==1)){?>	
                                    <td><?php echo $company_status['crm_preferred_name'];?></td>
                                    <?php } if(isset($column_setting[0]['date_of_birth'])&&($column_setting[0]['date_of_birth']==1)){?>	
                                    <td><?php echo $company_status['crm_date_of_birth'];?></td>
                                    <?php } if(isset($column_setting[0]['email'])&&($column_setting[0]['email']==1)){?>	
                                    <td><?php echo $company_status['crm_email'];?></td>
                                    <?php } if(isset($column_setting[0]['postal_address'])&&($column_setting[0]['postal_address']==1)){?>	
                                    <td><?php echo $company_status['crm_postal_address'];?></td>
                                    <?php } if(isset($column_setting[0]['telephone_number'])&&($column_setting[0]['telephone_number']==1)){?>	
                                    <td><?php echo $company_status['crm_telephone_number'];?></td>
                                    <?php } if(isset($column_setting[0]['mobile_number'])&&($column_setting[0]['mobile_number']==1)){?>		
                                    <td><?php echo $company_status['crm_mobile_number'];?></td>
                                    <?php } if(isset($column_setting[0]['ni_number'])&&($column_setting[0]['ni_number']==1)){?>								
                                    <td><?php echo $company_status['crm_ni_number'];?></td>
                                    <?php } if(isset($column_setting[0]['personal_utr_number'])&&($column_setting[0]['personal_utr_number']==1)){?>								
                                    <td><?php echo $company_status['crm_personal_utr_number'];?></td>
                                    <?php } if(isset($column_setting[0]['terms_signed'])&&($column_setting[0]['terms_signed']==1)){?>								
                                    <td><?php echo $company_status['crm_terms_signed'];?></td>
                                    <?php } if(isset($column_setting[0]['photo_id_verified'])&&($column_setting[0]['photo_id_verified']==1)){?>								
                                    <td><?php echo $company_status['crm_photo_id_verified'];?></td>
                                    <?php } if(isset($column_setting[0]['address_verified'])&&($column_setting[0]['address_verified']==1)){?>								
                                    <td><?php echo $company_status['crm_address_verified'];?></td>
                                    <?php } if(isset($column_setting[0]['previous'])&&($column_setting[0]['previous']==1)){?>								
                                    <td><?php echo $company_status['crm_previous'];?></td>
                                    <?php } if(isset($column_setting[0]['current'])&&($column_setting[0]['current']==1)){?>								
                                    <td><?php echo $company_status['crm_current'];?></td>
                                    <?php } if(isset($column_setting[0]['ir_35_notes'])&&($column_setting[0]['ir_35_notes']==1)){?>								
                                    <td><?php echo $company_status['crm_ir_35_notes'];?></td>
                                    <?php } if(isset($column_setting[0]['previous_accountant'])&&($column_setting[0]['previous_accountant']==1)){?>								
                                    <td><?php echo $company_status['crm_previous_accountant'];?></td>
                                    <?php } if(isset($column_setting[0]['refered_by'])&&($column_setting[0]['refered_by']==1)){?>								
                                    <td><?php echo $company_status['crm_refered_by'];?></td>
                                    <?php } if(isset($column_setting[0]['allocated_office'])&&($column_setting[0]['allocated_office']==1)){?>								
                                    <td><?php echo $company_status['crm_allocated_office'];?></td>
                                    <?php } if(isset($column_setting[0]['inital_contact'])&&($column_setting[0]['inital_contact']==1)){?>								
                                    <td><?php echo $company_status['crm_inital_contact'];?></td>
                                    <?php } if(isset($column_setting[0]['quote_email_sent'])&&($column_setting[0]['quote_email_sent']==1)){?>								
                                    <td><?php echo $company_status['crm_quote_email_sent'];?></td>
                                    <?php } if(isset($column_setting[0]['welcome_email_sent'])&&($column_setting[0]['welcome_email_sent']==1)){?>								
                                    <td><?php echo $company_status['crm_welcome_email_sent'];?></td>
                                    <?php } if(isset($column_setting[0]['internal_reference'])&&($column_setting[0]['internal_reference']==1)){?>								
                                    <td><?php echo $company_status['crm_internal_reference'];?></td>
                                    <?php } if(isset($column_setting[0]['profession'])&&($column_setting[0]['profession']==1)){?>								
                                    <td><?php echo $company_status['crm_profession'];?></td>
                                    <?php } if(isset($column_setting[0]['website'])&&($column_setting[0]['website']==1)){?>								
                                    <td><?php echo $company_status['crm_website'];?></td>
                                    <?php } if(isset($column_setting[0]['accounting_system'])&&($column_setting[0]['accounting_system']==1)){?>								
                                    <td><?php echo $company_status['crm_accounting_system'];?></td>
                                    <?php } if(isset($column_setting[0]['notes'])&&($column_setting[0]['notes']==1)){?>								
                                    <td><?php echo $company_status['crm_notes'];?></td>
                                    <?php } if(isset($column_setting[0]['other_urgent'])&&($column_setting[0]['other_urgent']==1)){?>								
                                    <td><?php echo $company_status['crm_other_urgent'];?></td>
                                    <?php } if(isset($column_setting[0]['accounts'])&&($column_setting[0]['accounts']==1)){?>								
                                    <td><?php echo $company_status['crm_accounts'];?></td>
                                    <?php } if(isset($column_setting[0]['bookkeeping'])&&($column_setting[0]['bookkeeping']==1)){?>								
                                    <td><?php echo $company_status['crm_bookkeeping'];?></td>
                                    <?php } if(isset($column_setting[0]['ct600_return'])&&($column_setting[0]['ct600_return']==1)){?>								
                                    <td><?php echo $company_status['crm_ct600_return'];?></td>
                                    <?php } if(isset($column_setting[0]['payroll'])&&($column_setting[0]['payroll']==1)){?>								
                                    <td><?php echo $company_status['crm_payroll'];?></td>
                                    <?php } if(isset($column_setting[0]['auto_enrolment'])&&($column_setting[0]['auto_enrolment']==1)){?>								
                                    <td><?php echo $company_status['crm_auto_enrolment'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_returns'])&&($column_setting[0]['vat_returns']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_returns'];?></td>
                                    <?php } if(isset($column_setting[0]['confirmation_statement'])&&($column_setting[0]['confirmation_statement']==1)){?>								
                                    <td><?php echo $company_status['crm_confirmation_statement'];?></td>
                                    <?php } if(isset($column_setting[0]['cis'])&&($column_setting[0]['cis']==1)){?>								
                                    <td><?php echo $company_status['crm_cis'];?></td>
                                    <?php } if(isset($column_setting[0]['p11d'])&&($column_setting[0]['p11d']==1)){?>								
                                    <td><?php echo $company_status['crm_p11d'];?></td>
                                    <?php } if(isset($column_setting[0]['invesitgation_insurance'])&&($column_setting[0]['invesitgation_insurance']==1)){?>								
                                    <td><?php echo $company_status['crm_invesitgation_insurance'];?></td>
                                    <?php } if(isset($column_setting[0]['services_registered_address'])&&($column_setting[0]['services_registered_address']==1)){?>								
                                    <td><?php echo $company_status['crm_services_registered_address'];?></td>
                                    <?php } if(isset($column_setting[0]['bill_payment'])&&($column_setting[0]['bill_payment']==1)){?>								
                                    <td><?php echo $company_status['crm_bill_payment'];?></td>
                                    <?php } if(isset($column_setting[0]['advice'])&&($column_setting[0]['advice']==1)){?>								
                                    <td><?php echo $company_status['crm_advice'];?></td>
                                    <?php } if(isset($column_setting[0]['monthly_charge'])&&($column_setting[0]['monthly_charge']==1)){?>								
                                    <td><?php echo $company_status['crm_monthly_charge'];?></td>
                                    <?php } if(isset($column_setting[0]['annual_charge'])&&($column_setting[0]['annual_charge']==1)){?>								
                                    <td><?php echo $company_status['crm_annual_charge'];?></td>
                                    <?php } if(isset($column_setting[0]['accounts_periodend'])&&($column_setting[0]['accounts_periodend']==1)){?>								
                                    <td><?php echo $company_status['crm_accounts_periodend'];?></td>
                                    <?php } if(isset($column_setting[0]['ch_yearend'])&&($column_setting[0]['ch_yearend']==1)){?>								
                                    <td><?php echo $company_status['crm_ch_yearend'];?></td>
                                    <?php } if(isset($column_setting[0]['hmrc_yearend'])&&($column_setting[0]['hmrc_yearend']==1)){?>								
                                    <td><?php echo $company_status['crm_hmrc_yearend'];?></td>
                                    <?php } if(isset($column_setting[0]['ch_accounts_next_due'])&&($column_setting[0]['ch_accounts_next_due']==1)){?>								
                                    <td><?php echo $company_status['crm_ch_accounts_next_due'];?></td>
                                    <?php } if(isset($column_setting[0]['ct600_due'])&&($column_setting[0]['ct600_due']==1)){?>								
                                    <td><?php echo $company_status['crm_ct600_due'];?></td>
                                    <?php } if(isset($column_setting[0]['companies_house_email_remainder'])&&($column_setting[0]['companies_house_email_remainder']==1)){?>								
                                    <td><?php echo $company_status['crm_companies_house_email_remainder'];?></td>
                                    <?php } if(isset($column_setting[0]['latest_action'])&&($column_setting[0]['latest_action']==1)){?>								
                                    <td><?php echo $company_status['crm_latest_action'];?></td>
                                    <?php } if(isset($column_setting[0]['latest_action_date'])&&($column_setting[0]['latest_action_date']==1)){?>								
                                    <td><?php echo $company_status['crm_latest_action_date'];?></td>
                                    <?php } if(isset($column_setting[0]['records_received_date'])&&($column_setting[0]['records_received_date']==1)){?>								
                                    <td><?php echo $company_status['crm_records_received_date'];?></td>
                                    <?php } if(isset($column_setting[0]['confirmation_statement_date'])&&($column_setting[0]['confirmation_statement_date']==1)){?>								
                                    <td><?php echo $company_status['crm_confirmation_statement_date'];?></td>
                                    <?php } if(isset($column_setting[0]['confirmation_statement_due_date'])&&($column_setting[0]['confirmation_statement_due_date']==1)){?>								
                                    <td><?php echo $company_status['crm_confirmation_statement_due_date'];?></td>
                                    <?php } if(isset($column_setting[0]['confirmation_latest_action'])&&($column_setting[0]['confirmation_latest_action']==1)){?>								
                                    <td><?php echo $company_status['crm_confirmation_latest_action'];?></td>
                                    <?php } if(isset($column_setting[0]['confirmation_latest_action_date'])&&($column_setting[0]['confirmation_latest_action_date']==1)){?>								
                                    <td><?php echo $company_status['crm_confirmation_latest_action_date'];?></td>
                                    <?php } if(isset($column_setting[0]['confirmation_records_received_date'])&&($column_setting[0]['confirmation_records_received_date']==1)){?>								
                                    <td><?php echo $company_status['crm_confirmation_records_received_date'];?></td>
                                    <?php } if(isset($column_setting[0]['confirmation_officers'])&&($column_setting[0]['confirmation_officers']==1)){?>								
                                    <td><?php echo $company_status['crm_confirmation_officers'];?></td>
                                    <?php } if(isset($column_setting[0]['share_capital'])&&($column_setting[0]['share_capital']==1)){?>								
                                    <td><?php echo $company_status['crm_share_capital'];?></td>
                                    <?php } if(isset($column_setting[0]['shareholders'])&&($column_setting[0]['shareholders']==1)){?>								
                                    <td><?php echo $company_status['crm_shareholders'];?></td>
                                    <?php } if(isset($column_setting[0]['people_with_significant_control'])&&($column_setting[0]['people_with_significant_control']==1)){?>								
                                    <td><?php echo $company_status['crm_people_with_significant_control'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_quarters'])&&($column_setting[0]['vat_quarters']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_quarters'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_quater_end_date'])&&($column_setting[0]['vat_quater_end_date']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_quater_end_date'];?></td>
                                    <?php } if(isset($column_setting[0]['next_return_due_date'])&&($column_setting[0]['next_return_due_date']==1)){?>								
                                    <td><?php echo $company_status['crm_next_return_due_date'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_latest_action'])&&($column_setting[0]['vat_latest_action']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_latest_action'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_latest_action_date'])&&($column_setting[0]['vat_latest_action_date']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_latest_action_date'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_records_received_date'])&&($column_setting[0]['vat_records_received_date']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_records_received_date'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_member_state'])&&($column_setting[0]['vat_member_state']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_member_state'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_number'])&&($column_setting[0]['vat_number']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_number'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_date_of_registration'])&&($column_setting[0]['vat_date_of_registration']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_date_of_registration'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_effective_date'])&&($column_setting[0]['vat_effective_date']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_effective_date'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_estimated_turnover'])&&($column_setting[0]['vat_estimated_turnover']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_estimated_turnover'];?></td>
                                    <?php } if(isset($column_setting[0]['transfer_of_going_concern'])&&($column_setting[0]['transfer_of_going_concern']==1)){?>								
                                    <td><?php echo $company_status['crm_transfer_of_going_concern'];?></td>
                                    <?php } if(isset($column_setting[0]['involved_in_any_other_business'])&&($column_setting[0]['involved_in_any_other_business']==1)){?>								
                                    <td><?php echo $company_status['crm_involved_in_any_other_business'];?></td>
                                    <?php } if(isset($column_setting[0]['flat_rate'])&&($column_setting[0]['flat_rate']==1)){?>								
                                    <td><?php echo $company_status['crm_flat_rate'];?></td>
                                    <?php } if(isset($column_setting[0]['direct_debit'])&&($column_setting[0]['direct_debit']==1)){?>								
                                    <td><?php echo $company_status['crm_direct_debit'];?></td>
                                    <?php } if(isset($column_setting[0]['annual_accounting_scheme'])&&($column_setting[0]['annual_accounting_scheme']==1)){?>								
                                    <td><?php echo $company_status['crm_annual_accounting_scheme'];?></td>
                                    <?php } if(isset($column_setting[0]['flat_rate_category'])&&($column_setting[0]['flat_rate_category']==1)){?>								
                                    <td><?php echo $company_status['crm_flat_rate_category'];?></td>
                                    <?php } if(isset($column_setting[0]['month_of_last_quarter_submitted'])&&($column_setting[0]['month_of_last_quarter_submitted']==1)){?>								
                                    <td><?php echo $company_status['crm_month_of_last_quarter_submitted'];?></td>
                                    <?php } if(isset($column_setting[0]['box5_of_last_quarter_submitted'])&&($column_setting[0]['box5_of_last_quarter_submitted']==1)){?>								
                                    <td><?php echo $company_status['crm_box5_of_last_quarter_submitted'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_address'])&&($column_setting[0]['vat_address']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_address'];?></td>
                                    <?php } if(isset($column_setting[0]['vat_notes'])&&($column_setting[0]['vat_notes']==1)){?>								
                                    <td><?php echo $company_status['crm_vat_notes'];?></td>
                                    <?php } if(isset($column_setting[0]['employers_reference'])&&($column_setting[0]['employers_reference']==1)){?>								
                                    <td><?php echo $company_status['crm_employers_reference'];?></td>
                                    <?php } if(isset($column_setting[0]['accounts_office_reference'])&&($column_setting[0]['accounts_office_reference']==1)){?>								
                                    <td><?php echo $company_status['crm_accounts_office_reference'];?></td>
                                    <?php } if(isset($column_setting[0]['years_required'])&&($column_setting[0]['years_required']==1)){?>								
                                    <td><?php echo $company_status['crm_years_required'];?></td>
                                    <?php } if(isset($column_setting[0]['annual_or_monthly_submissions'])&&($column_setting[0]['annual_or_monthly_submissions']==1)){?>								
                                    <td><?php echo $company_status['crm_annual_or_monthly_submissions'];?></td>
                                    <?php } if(isset($column_setting[0]['irregular_montly_pay'])&&($column_setting[0]['irregular_montly_pay']==1)){?>								
                                    <td><?php echo $company_status['crm_irregular_montly_pay'];?></td>
                                    <?php } if(isset($column_setting[0]['nil_eps'])&&($column_setting[0]['nil_eps']==1)){?>								
                                    <td><?php echo $company_status['crm_nil_eps'];?></td>
                                    <?php } if(isset($column_setting[0]['no_of_employees'])&&($column_setting[0]['no_of_employees']==1)){?>								
                                    <td><?php echo $company_status['crm_no_of_employees'];?></td>
                                    <?php } if(isset($column_setting[0]['first_pay_date'])&&($column_setting[0]['first_pay_date']==1)){?>								
                                    <td><?php echo $company_status['crm_first_pay_date'];?></td>
                                    <?php } if(isset($column_setting[0]['rti_deadline'])&&($column_setting[0]['rti_deadline']==1)){?>								
                                    <td><?php echo $company_status['crm_rti_deadline'];?></td>
                                    <?php } if(isset($column_setting[0]['paye_scheme_ceased'])&&($column_setting[0]['paye_scheme_ceased']==1)){?>								
                                    <td><?php echo $company_status['crm_paye_scheme_ceased'];?></td>
                                    <?php } if(isset($column_setting[0]['paye_latest_action'])&&($column_setting[0]['paye_latest_action']==1)){?>								
                                    <td><?php echo $company_status['crm_paye_latest_action'];?></td>
                                    <?php } if(isset($column_setting[0]['paye_latest_action_date'])&&($column_setting[0]['paye_latest_action_date']==1)){?>								
                                    <td><?php echo $company_status['crm_paye_latest_action_date'];?></td>
                                    <?php } if(isset($column_setting[0]['paye_records_received'])&&($column_setting[0]['paye_records_received']==1)){?>								
                                    <td><?php echo $company_status['crm_paye_records_received'];?></td>
                                    <?php } if(isset($column_setting[0]['next_p11d_return_due'])&&($column_setting[0]['next_p11d_return_due']==1)){?>								
                                    <td><?php echo $company_status['crm_next_p11d_return_due'];?></td>
                                    <?php } if(isset($column_setting[0]['latest_p11d_submitted'])&&($column_setting[0]['latest_p11d_submitted']==1)){?>								
                                    <td><?php echo $company_status['crm_latest_p11d_submitted'];?></td>
                                    <?php } if(isset($column_setting[0]['p11d_latest_action'])&&($column_setting[0]['p11d_latest_action']==1)){?>								
                                    <td><?php echo $company_status['crm_p11d_latest_action'];?></td>
                                    <?php } if(isset($column_setting[0]['p11d_latest_action_date'])&&($column_setting[0]['p11d_latest_action_date']==1)){?>								
                                    <td><?php echo $company_status['crm_p11d_latest_action_date'];?></td>
                                    <?php } if(isset($column_setting[0]['p11d_records_received'])&&($column_setting[0]['p11d_records_received']==1)){?>								
                                    <td><?php echo $company_status['crm_p11d_records_received'];?></td>
                                    <?php } if(isset($column_setting[0]['cis_contractor'])&&($column_setting[0]['cis_contractor']==1)){?>								
                                    <td><?php echo $company_status['crm_cis_contractor'];?></td>
                                    <?php } if(isset($column_setting[0]['cis_subcontractor'])&&($column_setting[0]['cis_subcontractor']==1)){?>								
                                    <td><?php echo $company_status['crm_cis_subcontractor'];?></td>
                                    <?php } if(isset($column_setting[0]['cis_deadline'])&&($column_setting[0]['cis_deadline']==1)){?>								
                                    <td><?php echo $company_status['crm_cis_deadline'];?></td>
                                    <?php } if(isset($column_setting[0]['auto_enrolment_latest_action'])&&($column_setting[0]['auto_enrolment_latest_action']==1)){?>								
                                    <td><?php echo $company_status['crm_auto_enrolment_latest_action'];?></td>
                                    <?php } if(isset($column_setting[0]['auto_enrolment_latest_action_date'])&&($column_setting[0]['auto_enrolment_latest_action_date']==1)){?>								
                                    <td><?php echo $company_status['crm_auto_enrolment_latest_action_date'];?></td>
                                    <?php } if(isset($column_setting[0]['auto_enrolment_records_received'])&&($column_setting[0]['auto_enrolment_records_received']==1)){?>								
                                    <td><?php echo $company_status['crm_auto_enrolment_records_received'];?></td>
                                    <?php } if(isset($column_setting[0]['auto_enrolment_staging'])&&($column_setting[0]['auto_enrolment_staging']==1)){?>								
                                    <td><?php echo $company_status['crm_auto_enrolment_staging'];?></td>
                                    <?php } if(isset($column_setting[0]['postponement_date'])&&($column_setting[0]['postponement_date']==1)){?>								
                                    <td><?php echo $company_status['crm_postponement_date'];?></td>
                                    <?php } if(isset($column_setting[0]['the_pensions_regulator_opt_out_date'])&&($column_setting[0]['the_pensions_regulator_opt_out_date']==1)){?>								
                                    <td><?php echo $company_status['crm_the_pensions_regulator_opt_out_date'];?></td>
                                    <?php } if(isset($column_setting[0]['paye_re_enrolment_date'])&&($column_setting[0]['paye_re_enrolment_date']==1)){?>								
                                    <td><?php echo $company_status['crm_paye_re_enrolment_date'];?></td>
                                    <?php } if(isset($column_setting[0]['paye_pension_provider'])&&($column_setting[0]['paye_pension_provider']==1)){?>								
                                    <td><?php echo $company_status['crm_paye_pension_provider'];?></td>
                                    <?php } if(isset($column_setting[0]['pension_id'])&&($column_setting[0]['pension_id']==1)){?>								
                                    <td><?php echo $company_status['crm_pension_id'];?></td>
                                    <?php } if(isset($column_setting[0]['declaration_of_compliance_due_date'])&&($column_setting[0]['declaration_of_compliance_due_date']==1)){?>								
                                    <td><?php echo $company_status['crm_declaration_of_compliance_due_date'];?></td>
                                    <?php } if(isset($column_setting[0]['declaration_of_compliance_submission'])&&($column_setting[0]['declaration_of_compliance_submission']==1)){?>								
                                    <td><?php echo $company_status['crm_declaration_of_compliance_submission'];?></td>
                                    <?php } if(isset($column_setting[0]['pension_deadline'])&&($column_setting[0]['pension_deadline']==1)){?>								
                                    <td><?php echo $company_status['crm_pension_deadline'];?></td>
                                    <?php } if(isset($column_setting[0]['note'])&&($column_setting[0]['note']==1)){?>								
                                    <td><?php echo $company_status['crm_note'];?></td>
                                    <?php } if(isset($column_setting[0]['registration_fee_paid'])&&($column_setting[0]['registration_fee_paid']==1)){?>								
                                    <td><?php echo $company_status['crm_registration_fee_paid'];?></td>
                                    <?php } if(isset($column_setting[0]['64_8_registration'])&&($column_setting[0]['64_8_registration']==1)){?>								
                                    <td><?php echo $company_status['crm_64_8_registration'];?></td>
                                    <?php } if(isset($column_setting[0]['packages'])&&($column_setting[0]['packages']==1)){?>								
                                    <td><?php echo $company_status['crm_packages'];?></td>
                                    <?php } if(isset($column_setting[0]['gender'])&&($column_setting[0]['gender']==1)){?>								
                                    <td><?php echo $company_status['crm_gender'];?></td>
                                    <?php } ?>
                                    <td>
                                      <p class="action_01">
                                        <a href="<?php echo base_url().'/user/delete/'.$getallUservalue['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                        <a href="<?php echo $edit;?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                        <!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                      </p>
                                    </td>
                                  </tr>
                                  <?php } ?>
                                </tbody>
                                
                              </table>
                            </div>
                          </div>
                        </div>
                        <!-- home-->
                        <div id="newactive" class="tab-pane fade">
                          <div class="count-value1 floating_set">
                            <!--<ul class="pull-left">
                              <li><select><option>1</option><option>2</option><option>3</option></select></li>
                               <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i><span>selected</span></a></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li> 	
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                              </ul>	
                              
                              <ul class="pull-right">
                              <form>
                              	<label>Search all columns</label>
                              	<input type="text" name="text">
                              </form>
                              </ul>	-->
                          </div>
                          <div class="client_section3 table-responsive floating_set">
                            <table class="table client_table1 text-center" id="newactives">
                              <thead>
                                <tr class="text-uppercase">
                                  <th>Profile</th>
                                  <th>Name</th>
                                  <th>Username</th>
                                  <th>Status</th>
                                  <th>User Type</th>
                                  <th>Actions</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($getallUser as $getallUserkey => $getallUservalue) {
                                  if($getallUservalue['role']=='1'){
                                      $role = 'Super Admin';
                                  }elseif($getallUservalue['role']=='2'){
                                      $role = 'Admin';
                                  }elseif($getallUservalue['role']=='3'){
                                    $role = 'Staff';
                                    $edit=base_url().'user/view_staff/'.$getallUservalue['id'];
                                  }elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
                                      $role = 'Client';
                                      $edit=base_url().'Client/add_client/'.$getallUservalue['id'];
                                      $house='Manual';
                                  }elseif($getallUservalue['role']=='4' && $company_status['status']==1){
                                      $role = 'Client';
                                      $edit=base_url().'Client/add_client/'.$getallUservalue['id'];
                                      $house='Company House';
                                  }elseif($getallUservalue['role']=='4' && $company_status['status']==2){
                                      $role = 'Client';
                                      $edit=base_url().'Client/add_client/'.$getallUservalue['id'];
                                      $house='Import';
                                  }
                                  if($getallUservalue['status']=='1'){
                                      $status_id = 'checkboxid1';  
                                      $chked = 'checked';
                                      $status_val = '1';
                                      $active='Active';
                                  /*elseif($getallUservalue['status']=='2' || $getallUservalue['status']=='0' ){
                                      $status_id = 'checkboxid2';  
                                      $chked = 'unchecked';
                                      $status_val = '2';
                                      $active='Inactive';
                                  }*/
                                  ?>
                                <tr>
                                  <td class="user_imgs"><img src="<?php echo base_url().'/uploads/'.$getallUservalue['crm_profile_pic'];?>" alt="img"></td>
                                  <td><?php echo ucfirst($getallUservalue['crm_name']);?></td>
                                  <td><?php echo ucfirst($getallUservalue['username']);?></td>
                                  <td>
                                    <p class="" ><?php echo $active;?></p>
                                  </td>
                                  <td><?php echo $role;?></td>
                                  <td>
                                    <p class="action_01">
                                      <a href="<?php echo base_url().'/user/delete/'.$getallUservalue['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                      <a href="<?php echo $edit;?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                      <!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                    </p>
                                  </td>
                                </tr>
                                <?php } } ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- new_user -->
                        <div id="newinactive" class="tab-pane fade">
                          <div class="count-value1 floating_set">
                            <!--<ul class="pull-left">
                              <li><select><option>1</option><option>2</option><option>3</option></select></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i><span>selected</span></a></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li> 	
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                              </ul>	
                              
                              <ul class="pull-right">
                              <form>
                              	<label>Search all columns</label>
                              	<input type="text" name="text">
                              </form>
                              </ul>	-->
                          </div>
                          <div class="client_section3 table-responsive floating_set">
                            <table class="table client_table1 text-center" id="newinactives">
                              <thead>
                                <tr class="text-uppercase">
                                  <th>Profile</th>
                                  <th>Name</th>
                                  <th>Username</th>
                                  <th>Status</th>
                                  <th>User Type</th>
                                  <th>Actions</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($getallUser as $getallUserkey => $getallUservalue) {
                                  if($getallUservalue['role']=='1'){
                                      $role = 'Super Admin';
                                  }elseif($getallUservalue['role']=='2'){
                                      $role = 'Admin';
                                  }elseif($getallUservalue['role']=='3'){
                                    $role = 'Staff';
                                    $edit=base_url().'user/view_staff/'.$getallUservalue['id'];
                                  }elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
                                      $role = 'Client';
                                      $edit=base_url().'Client/add_client/'.$getallUservalue['id'];
                                      $house='Manual';
                                  }elseif($getallUservalue['role']=='4' && $company_status['status']==1){
                                      $role = 'Client';
                                      $edit=base_url().'Client/add_client/'.$getallUservalue['id'];
                                      $house='Company House';
                                  }elseif($getallUservalue['role']=='4' && $company_status['status']==2){
                                      $role = 'Client';
                                      $edit=base_url().'Client/add_client/'.$getallUservalue['id'];
                                      $house='Import';
                                  }
                                  /*if($getallUservalue['status']=='1'){
                                      $status_id = 'checkboxid1';  
                                      $chked = 'checked';
                                      $status_val = '1';
                                      $active='Active';
                                  else*/ if($getallUservalue['status']=='2' || $getallUservalue['status']=='0' ){
                                      $status_id = 'checkboxid2';  
                                      $chked = 'unchecked';
                                      $status_val = '2';
                                      $active='Inactive';
                                  
                                  ?>
                                <tr>
                                  <td class="user_imgs"><img src="<?php echo base_url().'/uploads/'.$getallUservalue['crm_profile_pic'];?>" alt="img"></td>
                                  <td><?php echo ucfirst($getallUservalue['crm_name']);?></td>
                                  <td><?php echo ucfirst($getallUservalue['username']);?></td>
                                  <td>
                                    <p class="" ><?php echo $active;?></p>
                                  </td>
                                  <td><?php echo $role;?></td>
                                  <td>
                                    <p class="action_01">
                                      <a href="<?php echo base_url().'/user/delete/'.$getallUservalue['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                      <a href="<?php echo $edit;?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                      <!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                    </p>
                                  </td>
                                </tr>
                                <?php } } ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- inactive_user -->
                        <div id="frozen" class="tab-pane fade">
                          <div class="count-value1 floating_set">
                            <!--<ul class="pull-left">
                              <li><select><option>1</option><option>2</option><option>3</option></select></li>
                               <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i><span>selected</span></a></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li> 	
                              <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                              </ul>	
                              
                              <ul class="pull-right">
                              <form>
                              	<label>Search all columns</label>
                              	<input type="text" name="text">
                              </form>
                              </ul>-->	
                          </div>
                          <div class="client_section3 table-responsive floating_set">
                            <div id="suspent_succ"></div>
                            <table class="table client_table1 text-center" id="frozens">
                              <thead>
                                <tr class="text-uppercase">
                                  <th>Profile</th>
                                  <th>Name</th>
                                  <th>Username</th>
                                  <!-- <th>CRM-Active</th> -->
                                  <th>Status</th>
                                  <th>User Type</th>
                                  <th>Actions</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($getallUser as $getallUserkey => $getallUservalue) {
                                  if($getallUservalue['role']=='1'){
                                      $role = 'Super Admin';
                                      $edit='#';
                                  }elseif($getallUservalue['role']=='2'){
                                      $role = 'Admin';
                                      $edit='#';
                                  }elseif($getallUservalue['role']=='3'){
                                    $role = 'Staff';
                                    $edit=base_url().'user/view_staff/'.$getallUservalue['id'];
                                  }elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
                                      $role = 'Client';
                                      $edit=base_url().'Client/add_client/'.$getallUservalue['id'];
                                      $house='Manual';
                                  }elseif($getallUservalue['role']=='4' && $company_status['status']==1){
                                      $role = 'Client';
                                      $edit=base_url().'Client/add_client/'.$getallUservalue['id'];
                                      $house='Company House';
                                  }elseif($getallUservalue['role']=='4' && $company_status['status']==2){
                                      $role = 'Client';
                                      $edit=base_url().'Client/add_client/'.$getallUservalue['id'];
                                      $house='Import';
                                  }
                                  if($getallUservalue['status']=='1'){
                                      $status_id = 'checkboxid1';  
                                      $chked = 'checked';
                                      $status_val = '1';
                                      $active='Active';
                                  }elseif($getallUservalue['status']=='2' || $getallUservalue['status']=='0' ){
                                      $status_id = 'checkboxid2';  
                                      $chked = 'unchecked';
                                      $status_val = '2';
                                      $active='Inactive';
                                  }
                                  
                                  if($getallUservalue['frozen_freeze']=='0'){
                                      
                                      $status_id = 'checkbox0';  
                                      $chked = 'unchecked';
                                      $status_val = '0';
                                      $frozen='Frozen';
                                  }elseif($getallUservalue['frozen_freeze']=='1'){
                                      $status_id = 'checkbox1';  
                                      $chked = 'checked';
                                      $status_val = '1';
                                      $frozen='Payment';
                                  }
                                  ?>
                                <tr>
                                  <td class="user_imgs"><img src="<?php echo base_url().'/uploads/'.$getallUservalue['crm_profile_pic'];?>" alt="img"></td>
                                  <td><?php echo ucfirst($getallUservalue['crm_name']);?></td>
                                  <td><?php echo ucfirst($getallUservalue['username']);?></td>
                                  <!-- <td>
                                    <p class="onoff"><input type="checkbox" name="suspent<?php echo $getallUservalue['id'];?>" value="<?php echo $getallUservalue['id'];?>" id="suspent<?php echo $getallUservalue['id'];?>" class="suspent" <?php echo $chked;?>><label for="suspent<?php echo $getallUservalue['id'];?>"></label></p>
                                    
                                    
                                    
                                    </td> -->
                                  <td>
                                    <p id="frozen<?php echo $getallUservalue['id'];?>">
                                      <?php echo $frozen;?>
                                    </p>
                                  </td>
                                  <td><?php echo $role;?></td>
                                  <td>
                                    <p class="action_01">
                                      <a href="<?php echo base_url().'/user/delete/'.$getallUservalue['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                      <a href="<?php echo $edit;?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                      <!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                    </p>
                                  </td>
                                </tr>
                                <?php } ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- frozen -->
                      </div>
                    </div>
                    <!-- admin close -->
                  </div>
                  <!-- Register your self card end -->
                </div>
              </div>
            </div>
            <!-- Page body end -->
          </div>
        </div>
        <!-- Main-body end -->
        <div id="styleSelector">
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
  <h1>Warning!!</h1>
  <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
  <div class="iew-container">
    <ul class="iew-download">
      <li>
        <a href="http://www.google.com/chrome/">
          <img src="assets/images/browser/chrome.png" alt="Chrome">
          <div>Chrome</div>
        </a>
      </li>
      <li>
        <a href="https://www.mozilla.org/en-US/firefox/new/">
          <img src="assets/images/browser/firefox.png" alt="Firefox">
          <div>Firefox</div>
        </a>
      </li>
      <li>
        <a href="http://www.opera.com">
          <img src="assets/images/browser/opera.png" alt="Opera">
          <div>Opera</div>
        </a>
      </li>
      <li>
        <a href="https://www.apple.com/safari/">
          <img src="assets/images/browser/safari.png" alt="Safari">
          <div>Safari</div>
        </a>
      </li>
      <li>
        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
          <img src="assets/images/browser/ie.png" alt="">
          <div>IE (9 & above)</div>
        </a>
      </li>
    </ul>
  </div>
  <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<?php
  $data = $this->db->query("SELECT * FROM update_client")->result_array();
  foreach($data as $row) { ?>
<!-- Modal -->
<div id="myModal<?php echo $row['user_id'];?>" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">CRM-Updated fields</h4>
      </div>
      <div class="modal-body">
        <?php if(isset($row['company_name'])&&($row['company_name']!='0')){?>
        <p>Company Name: <span><?php echo $row['company_name'];?></span></p>
        <?php } ?> 
        <?php if(isset($row['company_url'])&&($row['company_url']!='0')){?>
        <p>Company URL: <span><?php echo $row['company_url'];?></span></p>
        <?php } ?>
        <?php if(isset($row['officers_url'])&&($row['officers_url']!='0')){?>
        <p>Officers URL: <span><?php echo $row['officers_url'];?></span></p>
        <?php } ?>
        <?php if(isset($row['incorporation_date'])&&($row['incorporation_date']!='0')){?>
        <p>Incorporation Date: <span><?php echo $row['incorporation_date'];?></span></p>
        <?php } ?>
        <?php if(isset($row['register_address'])&&($row['register_address']!='0')){?>
        <p>Registered Address: <span><?php echo $row['register_address'];?></span></p>
        <?php }  ?>
        <?php if(isset($row['company_status'])&&($row['company_status']!='0')){?>
        <p>Company Status: <span><?php echo $row['company_status'];?></span></p>
        <?php }  ?>
        <?php if(isset($row['company_type'])&&($row['company_type']!='0')){?>
        <p>Company Type: <span><?php echo $row['company_type'];?></span></p>
        <?php } ?>
        <?php if(isset($row['accounts_periodend'])&&($row['accounts_periodend']!='0')){?>
        <p>Accounts Period End: <span><?php echo $row['accounts_periodend'];?></span></p>
        <?php } ?>
        <?php if(isset($row['hmrc_yearend'])&&($row['hmrc_yearend']!='0')){?>
        <p>HMRC Year End: <span><?php echo $row['hmrc_yearend'];?></span></p>
        <?php } ?>
        <?php if(isset($row['ch_accounts_next_due'])&&($row['ch_accounts_next_due']!='0')){?>
        <p>CH Accounts Next Due: <span><?php echo $row['ch_accounts_next_due'];?></span></p>
        <?php } ?>
        <?php if(isset($row['confirmation_statement_date'])&&($row['confirmation_statement_date']!='0')){?>
        <p>Confirmation Statement Date: <span><?php echo $row['confirmation_statement_date'];?></span></p>
        <?php } ?>
        <?php if(isset($row['confirmation_statement_due_date'])&&($row['confirmation_statement_due_date']!='0')){?>
        <p>Confirmation Statement Due: <span><?php echo $row['confirmation_statement_due_date'];?></span></p>
        <?php } ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- /.modal -->
<?php } ?>
<?php $this->load->view('users/column_setting_view');?>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->

 <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script>
  $(document).ready(function() {
  

    $("#alluser").DataTable({
      "iDisplayLength": 20,
    "scrollX": true,
   "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () {
            this.api().columns('.select-filter').every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }

    //"processing": true
       
    });
  $("#newactives").dataTable({
       "iDisplayLength": 20,
       // "scrollX": true,
    "dom": '<"top"fl>rt<"bottom"ip><"clear">'
    });
  $("#newinactives").dataTable({
       "iDisplayLength": 20,
        //"scrollX": true,
    "dom": '<"top"fl>rt<"bottom"ip><"clear">'
    });
  $("#frozens").dataTable({
       "iDisplayLength": 20,
        //"scrollX": true,
    "dom": '<"top"fl>rt<"bottom"ip><"clear">'
    });
  
  

  
  $("#bulkDelete").on('click',function() { // bulk checked
  			var status = this.checked;

  			$(".deleteRow").each( function() {
  				$(this).prop("checked",status);

  			});
        if(status==true)
        {
          $('#deleteTriger').show();
        } else {
          $('#deleteTriger').hide();
        }
  		});

  $(".deleteRow").on('click',function() { // bulk checked
  
        var status = this.checked;

        if(status==true)
        {
          $('#deleteTriger').show();
        } else {
          $('#deleteTriger').hide();
        }
      });
  		
  		$('#deleteTriger').on("click", function(event){ // triggering delete one by one
       
  			if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
  				var ids = [];
  				$('.deleteRow').each(function(){
  					if($(this).is(':checked')) { 
  						ids.push($(this).val());
  					}
  				});
  				var ids_string = ids.toString();  // array to string conversion 
  				$.ajax({
  					type: "POST",
  					url: "<?php echo base_url();?>user/multiple_delete/",
  					data: {data_ids:ids_string},
  					success: function(response) {
  						//dataTable.draw(); // redrawing datatable
  						var emp_ids = response.split(",");
   for (var i=0; i < emp_ids.length; i++ ) {
  
   	$("#"+emp_ids[i]).remove(); 
   }
   
  					},
  					//async:false,
  				});
  			}
  		});
  
  // active/inactive status
  /*$('.status').click(function() {
    if($(this).is(':checked'))
        var stat = '1';
    else
        var stat = '2';
    var rec_id = $(this).val();
    var $this = $(this);
     $.ajax({
        url: '<?php echo base_url();?>user/statusChange/',
        type: 'post',
        data: { 'rec_id':rec_id,'status':stat },
        success: function( data ){
        	//alert('ggg');
            $("#status_succ").html('<div class="card borderless-card"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
            if(stat=='1'){
            	
            	 $this.closest('td').next('td').html('Active');
  
            } else {
            	 $this.closest('td').next('td').html('Inactive');
            }
            },
            error: function( errorThrown ){
                console.log( errorThrown );
            }
        });
  });*/
  
  
  //$(".status").change(function(e){
  $('#alluser').on('change','.status',function () {
  //e.preventDefault();
  
  
   var rec_id = $(this).data('id');
   var stat = $(this).val();
  
  $.ajax({
        url: '<?php echo base_url();?>user/statusChange/',
        type: 'post',
        data: { 'rec_id':rec_id,'status':stat },
        timeout: 3000,
        success: function( data ){
        	//alert('ggg');
            $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });
            setTimeout(resetAll,3000);
            if(stat=='3'){
            	
            	 //$this.closest('td').next('td').html('Active');
            	 $('#frozen'+rec_id).html('Frozen');
  
            } else {
            	 $this.closest('td').next('td').html('Inactive');
            }
            },
            error: function( errorThrown ){
                console.log( errorThrown );
            }
        });
   });
  
  // payment/non payment status
  $('.suspent').click(function() {
    if($(this).is(':checked'))
        var stat = '1';
    else
        var stat = '0';
    var rec_id = $(this).val();
    var $this = $(this);
     $.ajax({
        url: '<?php echo base_url();?>user/suspentChange/',
        type: 'post',
        data: { 'rec_id':rec_id,'status':stat },
        success: function( data ){
        	//alert('ggg');
            $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
            if(stat=='1'){
            	
            	 $this.closest('td').next('td').html('Payment');
  
            } else {
            	 $this.closest('td').next('td').html('Non payment');
            }
            },
            error: function( errorThrown ){
                console.log( errorThrown );
            }
        });
  });
  });
  
</script>
<script>
  /* var notes = {};
  
  <?php
    $data = $this->db->query("SELECT * FROM update_client")->result_array();
    foreach($data as $row) {
    
      echo 'notes["'.$row['user_id'].'"] = "<p>company status:'.$row['company_status'].'</p><br><p>account period end:'.$row['accounts_periodend'].'</p>";';
    
     //echo 'notes["'.$row['user_id'].'"] = "'.(isset($row['company_status'])&&($row['company_status']!=0)) ? .'<p>company status:'.$row['company_status'].'</p><br><p>account period end:'.$row['accounts_periodend'].'</p>";';
    
      //echo 'notes["'.$row['user_id'].'"]'= (isset($row['company_status'])&&($row['company_status']!=0)) ? $row['company_status']:'';
    }
    ?>
  
  $(document).on("click", ".noteslink", function() {
    var id = $(this).data("rowid");
    $("#myModalnote .modal-body").html(notes[id]);
    $("#myModalnote").modal("show");
  });
  */
  
 

  
</script>

<script type="text/javascript">

$(document).ready(function(){
  $('#column_setting').on('submit', function (e) {

          e.preventDefault();
 $(".LoadingImage").show();
          $.ajax({
            type: 'post',
            url: '<?php echo base_url()."User/column_update";?>',
            data: $('form').serialize(),
            success: function (data) {
                if(data == '1'){
                // $('#new_user')[0].reset();
                $('.alert-success').show();
                $('.alert-danger').hide();
                location.reload();
               // $('.all-usera1').load('<?php echo base_url()?>user .all-usera1');
                
              
                }
                else if(data == '0'){
                // alert('failed');
                $('.alert-danger').show();
                $('.alert-success').hide();
                }
                $(".LoadingImage").hide();
            }
          });

        });




});

</script>