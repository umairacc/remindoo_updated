<?php
class Proposals extends CI_Controller {
	public function __construct(){
			parent::__construct();
			$this->load->model(array('Common_mdl','Proposal_model','Security_model'));
		   $this->load->library('Excel');
		   $this->load->library('encryption');
		}
		public function index()
		{
			$this->Proposal_model->index();             
		}

		public function company_details(){
			$user_id=$this->session->userdata['userId'];
			$crm_company_name=$_POST['crm_company_name'];

          if (strpos($crm_company_name, 'Leads_') !== false) {

          $leads_id=explode('_',$crm_company_name);
         //  echo $leads_id[1];
          // echo 'true';

           $data=$this->Common_mdl->select_record('leads','id',$leads_id[1]);

           $company_name=$this->Common_mdl->select_record('client','id',$data['company']);


        $image=$this->Common_mdl->select_record('user','id',$user_id);
        if($image['crm_profile_pic']==''){
          $image_name='no-image-found.jpg';
          $data['image']='<img src="'.base_url().'uploads/'.$image_name.'" alt="image">';
        }else{
          $data['image']='<img src="'.base_url().'uploads/'.$image['crm_profile_pic'].'" alt="image">';
        }

            $data['content']='';

            $data['content']='<span>'.strtoupper($data['name']).'</span><input type="hidden" name="receiver_company_name" id="receiver_company_name" value="'.$data['name'].'">

                        <strong>'.$data['email_address'].'</strong><input type="hidden" name="receiver_mail_id" id="receiver_mail_id" value="'.$data['email_address'].'">';
            if($data['phone']!=''){
            $data['content'].='<a href="#"><b>ph:</b>'.$data['phone'].'</a>';
            }

          }else{


			$data=$this->Common_mdl->select_record('client','user_id',$crm_company_name);
			$image=$this->Common_mdl->select_record('user','id',$user_id);
			if($image['crm_profile_pic']==''){
				$image_name='no-image-found.jpg';
				$data['image']='<img src="'.base_url().'uploads/'.$image_name.'" alt="image">';
			}else{
			$data['image']='<img src="'.base_url().'uploads/'.$image['crm_profile_pic'].'" alt="image">';
			}
		
			$data['content']='';

			$data['content']='<span>'.strtoupper($data['crm_company_name']).'</span><input type="hidden" name="receiver_company_name" id="receiver_company_name" value="'.$data['crm_company_name'].'">
                                    <strong>'.$data['crm_email'].'</strong><input type="hidden" name="receiver_mail_id" id="receiver_mail_id" value="'.$data['crm_email'].'">';
            if($data['crm_mobile_number']!=''){
                $data['content'].='<a href="#"><b>ph:</b>'.$data['crm_mobile_number'].'</a>';
            }
        }                                ;
			echo json_encode($data);	
		}	

		public function service_category_update(){
			 $user_id=$this->session->userdata['userId'];  
     	    $value=array('service_name'=>$_POST['service_name'],'service_price'=>$_POST['service_price'],'service_unit'=>$_POST['service_unit'],'service_description'=>$_POST['service_description'],'service_qty'=>$_POST['service_qty'],'service_category'=>$_POST['service_category'],'status'=>'active','user_id'=>$user_id);
            $data['status']=$this->Common_mdl->insert('proposal_service',$value);
            echo json_encode($data);
		}	

		public function product_category_update(){			
			$user_id=$this->session->userdata['userId'];          
            $value=array('product_name'=>$_POST['product_name'],'product_price'=>$_POST['product_price'],'product_description'=>$_POST['product_description'],'product_category'=>$_POST['product_category'],'status'=>'active','user_id'=>$user_id);
             $data['status']=$this->Common_mdl->insert('proposal_products',$value);
             echo json_encode($data);
		}

		public function subscription_category_update(){
			 $user_id=$this->session->userdata['userId'];
            $value=array('subscription_name'=>$_POST['subscription_name'],'subscription_price'=>$_POST['subscription_price'],'subscription_unit'=>$_POST['subscription_unit'],'status'=>'active','user_id'=>$user_id,'subscription_description'=>$_POST['subscription_description'],'subscription_category'=>$_POST['subscription_category']);
            $data['status']=$this->Common_mdl->insert('proposal_subscriptions',$value);
             echo json_encode($data);
		}
		 public function tab_details(){
		 	     $currency_symbols = array(
                          'AED' => '&#1583;.&#1573;', // ?
                          'AFN' => '&#65;&#102;',
                          'ALL' => '&#76;&#101;&#107;',
                          'AMD' => '',
                          'ANG' => '&#402;',
                          'AOA' => '&#75;&#122;', // ?
                          'ARS' => '&#36;',
                          'AUD' => '&#36;',
                          'AWG' => '&#402;',
                          'AZN' => '&#1084;&#1072;&#1085;',
                          'BAM' => '&#75;&#77;',
                          'BBD' => '&#36;',
                          'BDT' => '&#2547;', // ?
                          'BGN' => '&#1083;&#1074;',
                          'BHD' => '.&#1583;.&#1576;', // ?
                          'BIF' => '&#70;&#66;&#117;', // ?
                          'BMD' => '&#36;',
                          'BND' => '&#36;',
                          'BOB' => '&#36;&#98;',
                          'BRL' => '&#82;&#36;',
                          'BSD' => '&#36;',
                          'BTN' => '&#78;&#117;&#46;', // ?
                          'BWP' => '&#80;',
                          'BYR' => '&#112;&#46;',
                          'BZD' => '&#66;&#90;&#36;',
                          'CAD' => '&#36;',
                          'CDF' => '&#70;&#67;',
                          'CHF' => '&#67;&#72;&#70;',
                          'CLF' => '', // ?
                          'CLP' => '&#36;',
                          'CNY' => '&#165;',
                          'COP' => '&#36;',
                          'CRC' => '&#8353;',
                          'CUP' => '&#8396;',
                          'CVE' => '&#36;', // ?
                          'CZK' => '&#75;&#269;',
                          'DJF' => '&#70;&#100;&#106;', // ?
                          'DKK' => '&#107;&#114;',
                          'DOP' => '&#82;&#68;&#36;',
                          'DZD' => '&#1583;&#1580;', // ?
                          'EGP' => '&#163;',
                          'ETB' => '&#66;&#114;',
                          'EUR' => '&#8364;',
                          'FJD' => '&#36;',
                          'FKP' => '&#163;',
                          'GBP' => '&#163;',
                          'GEL' => '&#4314;', // ?
                          'GHS' => '&#162;',
                          'GIP' => '&#163;',
                          'GMD' => '&#68;', // ?
                          'GNF' => '&#70;&#71;', // ?
                          'GTQ' => '&#81;',
                          'GYD' => '&#36;',
                          'HKD' => '&#36;',
                          'HNL' => '&#76;',
                          'HRK' => '&#107;&#110;',
                          'HTG' => '&#71;', // ?
                          'HUF' => '&#70;&#116;',
                          'IDR' => '&#82;&#112;',
                          'ILS' => '&#8362;',
                          'INR' => '&#8377;',
                          'IQD' => '&#1593;.&#1583;', // ?
                          'IRR' => '&#65020;',
                          'ISK' => '&#107;&#114;',
                          'JEP' => '&#163;',
                          'JMD' => '&#74;&#36;',
                          'JOD' => '&#74;&#68;', // ?
                          'JPY' => '&#165;',
                          'KES' => '&#75;&#83;&#104;', // ?
                          'KGS' => '&#1083;&#1074;',
                          'KHR' => '&#6107;',
                          'KMF' => '&#67;&#70;', // ?
                          'KPW' => '&#8361;',
                          'KRW' => '&#8361;',
                          'KWD' => '&#1583;.&#1603;', // ?
                          'KYD' => '&#36;',
                          'KZT' => '&#1083;&#1074;',
                          'LAK' => '&#8365;',
                          'LBP' => '&#163;',
                          'LKR' => '&#8360;',
                          'LRD' => '&#36;',
                          'LSL' => '&#76;', // ?
                          'LTL' => '&#76;&#116;',
                          'LVL' => '&#76;&#115;',
                          'LYD' => '&#1604;.&#1583;', // ?
                          'MAD' => '&#1583;.&#1605;.', //?
                          'MDL' => '&#76;',
                          'MGA' => '&#65;&#114;', // ?
                          'MKD' => '&#1076;&#1077;&#1085;',
                          'MMK' => '&#75;',
                          'MNT' => '&#8366;',
                          'MOP' => '&#77;&#79;&#80;&#36;', // ?
                          'MRO' => '&#85;&#77;', // ?
                          'MUR' => '&#8360;', // ?
                          'MVR' => '.&#1923;', // ?
                          'MWK' => '&#77;&#75;',
                          'MXN' => '&#36;',
                          'MYR' => '&#82;&#77;',
                          'MZN' => '&#77;&#84;',
                          'NAD' => '&#36;',
                          'NGN' => '&#8358;',
                          'NIO' => '&#67;&#36;',
                          'NOK' => '&#107;&#114;',
                          'NPR' => '&#8360;',
                          'NZD' => '&#36;',
                          'OMR' => '&#65020;',
                          'PAB' => '&#66;&#47;&#46;',
                          'PEN' => '&#83;&#47;&#46;',
                          'PGK' => '&#75;', // ?
                          'PHP' => '&#8369;',
                          'PKR' => '&#8360;',
                          'PLN' => '&#122;&#322;',
                          'PYG' => '&#71;&#115;',
                          'QAR' => '&#65020;',
                          'RON' => '&#108;&#101;&#105;',
                          'RSD' => '&#1044;&#1080;&#1085;&#46;',
                          'RUB' => '&#1088;&#1091;&#1073;',
                          'RWF' => '&#1585;.&#1587;',
                          'SAR' => '&#65020;',
                          'SBD' => '&#36;',
                          'SCR' => '&#8360;',
                          'SDG' => '&#163;', // ?
                          'SEK' => '&#107;&#114;',
                          'SGD' => '&#36;',
                          'SHP' => '&#163;',
                          'SLL' => '&#76;&#101;', // ?
                          'SOS' => '&#83;',
                          'SRD' => '&#36;',
                          'STD' => '&#68;&#98;', // ?
                          'SVC' => '&#36;',
                          'SYP' => '&#163;',
                          'SZL' => '&#76;', // ?
                          'THB' => '&#3647;',
                          'TJS' => '&#84;&#74;&#83;', // ? TJS (guess)
                          'TMT' => '&#109;',
                          'TND' => '&#1583;.&#1578;',
                          'TOP' => '&#84;&#36;',
                          'TRY' => '&#8356;', // New Turkey Lira (old symbol used)
                          'TTD' => '&#36;',
                          'TWD' => '&#78;&#84;&#36;',
                          'TZS' => '',
                          'UAH' => '&#8372;',
                          'UGX' => '&#85;&#83;&#104;',
                          'USD' => '&#36;',
                          'UYU' => '&#36;&#85;',
                          'UZS' => '&#1083;&#1074;',
                          'VEF' => '&#66;&#115;',
                          'VND' => '&#8363;',
                          'VUV' => '&#86;&#84;',
                          'WST' => '&#87;&#83;&#36;',
                          'XAF' => '&#70;&#67;&#70;&#65;',
                          'XCD' => '&#36;',
                          'XDR' => '',
                          'XOF' => '',
                          'XPF' => '&#70;',
                          'YER' => '&#65020;',
                          'ZAR' => '&#82;',
                          'ZMK' => '&#90;&#75;', // ?
                          'ZWL' => '&#90;&#36;',
                        ); 

	
	if($_POST['currency_symbol']==''){
		$curr_symbols='EUR';
	}else{
		$curr_symbols=$_POST['currency_symbol'];	
	}

  
//if(isset($_POST['tax_details']) ){
    $tax_details=$_POST['tax_details'];
// }else{
//  $tax_details=0;
//  }
 	
     $data['table']='';
		 	$data['table']='<div id="gg_body_content"><table align="left" cellpadding="10" class="ui-draggable ui-draggable-handle editable test123" style="width:100%; margin:0 auto;border:1px solid #ccc;border-collapse:collapse;"><tbody>
			<tr><th align="left" colspan="2" style="border:1px solid #ccc;">Services</th><th align="left"  style="border:1px solid #ccc;">Price/Unit</th><th align="left" style="border:1px solid #ccc;">Qty</th></tr>';

			if(!empty($_POST['item_name'][0])){
			for($i=0;$i<count($_POST['item_name']);$i++){
			if($_POST['service_optional'][$i]=='1'){
			   // $optional='<sup>(optional)</sup>';

          $optional='<sup>(optional)</sup><div class="checkbox-fade fade-in-primary"><label><input type="checkbox" name="optional" class="optional_check" data-id="service" id="'.$i.'" onchange="edit_optional(this);" ><span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span></label></div>';
			}else{
				$optional='';
			}
			if($_POST['service_quantity'][$i]=='1'){
			  // $quantity='true';
        $quantity='<div class="quantity_class" style="display:none;"><input type="text" name="service_quantity_field[]" id="quantity" class="quantity service_qty" value="" onchange="edit_checking()" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"></div><span class="quantity_cla">'.$_POST['qty'][$i].'</span><button type="button" name="check_quantity" data-id="service" class="check_quantity edit_quantity btn btn-primary" onclick="Quantity_checking(this)">Edit</button><button type="button" name="check_quantity" data-id="service" class="check_quantity update_quantity btn btn-primary" onclick="edit_checking()" style="display:none;">Update</button>';
			}else{
        $quantity='<div class="quantity_class" style="display:none;"><input type="text" name="service_quantity_field[]" id="quantity" class="quantity service_qty" value="" onchange="edit_checking()" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"></div><span class="quantity_cla">'.$_POST['qty'][$i].'</span>';
			//	$quantity='false';
			}


				$data['table'].='<tr class="service_tr"><td colspan="2" style="border-right:1px solid #ccc;">'.$_POST['item_name'][$i].''.$optional.'</td><td style="border-right:1px solid #ccc;"><input type="hidden" name="edit_price" class="edit_price services_price" value="'.$_POST['price'][$i].'" onchange="edit_checking()"><span class="price_section">'.$_POST['price'][$i].'</span>/'.$_POST['unit'][$i].'</td><td style="border-right:1px solid #ccc;">'.$quantity.'</td><input type="hidden" id="edit_service_tax" class="edit_service_tax" value="'.$_POST['tax'][$i].'"><input type="hidden" id="edit_service_discount" class="edit_service_discount" value="'.$_POST['discount'][$i].'"></tr>';
			
			}}

		if(!empty($_POST['product_name'][0])){

      $data['table'].='<tr><th align="left" colspan="2" style="border:1px solid #ccc;">Product</th><th align="left"  style="border:1px solid #ccc;">Price/Unit</th><th align="left" style="border:1px solid #ccc;">Qty</th></tr>';
		for($i=0;$i<count($_POST['product_name']);$i++){
			if($_POST['product_optional'][$i]=='1'){
			 //  $optional='<sup>(optional)</sup>';
          $optional='<sup>(optional)</sup><div class="checkbox-fade fade-in-primary"><label><input type="checkbox" name="optional" class="optional_check" data-id="product" id="'.$i.'" onchange="edit_optional(this);" ><span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span></label></div>';
			}else{
				$optional='';
			}
			if($_POST['product_quantity'][$i]=='1'){

        $quantity='<div class="quantity_class" style="display:none;"><input type="text" name="product_quantity_field[]" id="product_quantity" class="quantity product_qtys" value="" onchange="edit_checking()" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"></div><span class="quantity_cla">'.$_POST['product_qty'][$i].'</span><button type="button" name="check_quantity" data-id="service" class="check_quantity edit_quantity btn btn-primary" onclick="Quantity_checking(this)">Edit</button><button type="button" name="check_quantity" data-id="service" class="check_quantity update_quantity btn btn-primary" onclick="edit_checking()" style="display:none;">Update</button>';
			 //  $quantity='true';
			}else{

        $quantity='<div class="quantity_class" style="display:none;"><input type="text" name="product_quantity_field[]" id="product_quantity" class="quantity product_qtys" value="" onchange="edit_checking()" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"></div><span class="quantity_cla">'.$_POST['product_qty'][$i].'</span>';
			///	$quantity='false';
			}

			$data['table'].='<tr class="product_tr"><td colspan="2" style="border-right:1px solid #ccc;">'.$_POST['product_name'][$i].''.$optional.'</td><td style="border-right:1px solid #ccc;"><input type="hidden" name="price=" class="edit_price products_price" value="'.$_POST['product_price'][$i].'" onchange="edit_checking()"><span class="price_section">'.$_POST['product_price'][$i].'</span></td><td style="border-right:1px solid #ccc;">'.$quantity.'</td><input type="hidden" id="edit_product_tax" class="edit_product_tax" value="'.$_POST['product_tax'][$i].'"><input type="hidden" id="edit_product_discount" class="edit_product_discount" value="'.$_POST['product_discount'][$i].'"></tr>';
		}}

		if(!empty($_POST['subscription_name'][0])){

        $data['table'].='<tr><th align="left" colspan="2" style="border:1px solid #ccc;">Subscription</th><th align="left"  style="border:1px solid #ccc;">Price/Unit</th><th align="left" style="border:1px solid #ccc;">Qty</th></tr>';

		for($i=0;$i<count($_POST['subscription_name']);$i++){
			if($_POST['subscription_optional'][$i]=='1'){
			  // $optional='<sup>(optional)</sup>';

          $optional='<sup>(optional)</sup><div class="checkbox-fade fade-in-primary"><label><input type="checkbox" name="optional" class="optional_check" data-id="subscription" id="'.$i.'" onchange="edit_optional(this);" ><span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span></label></div>';
			}else{
				$optional='';
			}
			if($_POST['subscription_quantity'][$i]=='1'){
        $quantity='<div class="quantity_class" style="display:none;"><input type="text" name="subscription_quantity_field" id="subscription_quantity" class="quantity subscription_qty" value="" onchange="edit_checking()" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"></div><span class="quantity_cla">1</span><button type="button" name="check_quantity" data-id="service" class="check_quantity edit_quantity btn btn-primary" onclick="Quantity_checking(this)">Edit</button><button type="button" name="check_quantity" data-id="service" class="check_quantity update_quantity btn btn-primary" onclick="edit_checking()" style="display:none;">Update</button>';
			  // $quantity='true';
			}else{
        $quantity='<div class="quantity_class" style="display:none;"><input type="text" name="subscription_quantity_field" id="subscription_quantity" class="quantity subscription_qty" value="" onchange="edit_checking()" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"></div><span class="quantity_cla">1</span>';
			//	$quantity='false';
			}

			$data['table'].='<tr class="subscription_tr"><td colspan="2" style="border-right:1px solid #ccc;">'.$_POST['subscription_name'][$i].''.$optional.'</td><td style="border-right:1px solid #ccc;"><input type="hidden" name="price=" class="edit_price subscription_prices" value="'.$_POST['subscription_price'][$i].'" onchange="edit_checking()"><span class="price_section">'.$_POST['subscription_price'][$i].'</span>/'.$_POST['subscription_unit'][$i].'</td><td style="border-right:1px solid #ccc;" contenteditable="">'.$quantity.'</td><input type="hidden" id="edit_subscription_tax" class="edit_subscription_tax" value="'.$_POST['subscription_tax'][$i].'"><input type="hidden" id="edit_subscription_discount" class="edit_subscription_discount" value="'.$_POST['subscription_discount'][$i].'"></tr>';
		}}

		$data['table'].='<tr><td colspan="2" style="border-right:1px solid #ccc; "></td><td style="border-right:1px solid #ccc;">Sub Total </td><td style="border-right:1px solid #ccc;"><span class="Sub_total">'.$_POST['total_total'].'</span><input type="hidden" name="grand_total" class="sub_total_field" value=""> </td></tr><tr><td colspan="2" style="border-right:1px solid #ccc;"></td><td style="border-right:1px solid #ccc;">Total </td><td style="border-right:1px solid #ccc;"><span class="currency_symbol">'.$currency_symbols[$curr_symbols].'</span><span class="grand_total_1">'.$_POST['grand_total'].'</span><input type="hidden" name="grand_total" class="grand_total_field" value="">       <input type="hidden" id="over_alltax" class="over_alltax" value="'.$tax_details.'"><input type="hidden" id="tax_option" class="tax_option" value="'.$_POST['tax_option'].'"><input type="hidden" id="discount_option" class="discount_option" value="'.$_POST['discount_option'].'"><input type="hidden" id="over_alldiscount" class="over_alldiscount" value="'.$_POST['discount_amount'].'"></td></tr>';

		
		$data['table'].='</tbody></table></div>';
		echo json_encode($data);
		 }

		 public function template_content(){
		 	$id=$_POST['id'];

		 	$template=$this->Common_mdl->select_record('proposal_template','id',$id);
		 	// echo '</pre>';
		 	// print_r($template);
		 	$data['template_heads']=explode('[Price Table]',$template['template_content']);
          $body1=$data['template_heads'][0];
          $random_string=$this->generateRandomString();
          $a1  =   array('[Username]'=>'username','[Client Name]'=>'client name','[Accountant Name]'=>'account name','[Task Number]'=>'task number','[Task Name]'=>'task name','[Task Due Date]'=>'task due date','[Task Client]'=>'task client','[Firm Name]'=>'firmname','[accountant number]'=>'accoutant number','[invoice amount]'=>'invoice amount','[invoice due date]'=>'invoice due date');
          $template_heads  =   strtr($body1,$a1); 


		 	$data['template_head']=$data['template_heads'][0];

       $body2=$data['template_heads'][1];
          $random_string=$this->generateRandomString();
          $a1  =  array('[Username]'=>'username','[Client Name]'=>'client name','[Accountant Name]'=>'account name','[Task Number]'=>'task number','[Task Name]'=>'task name','[Task Due Date]'=>'task due date','[Task Client]'=>'task client','[Firm Name]'=>'firmname','[accountant number]'=>'accoutant number','[invoice amount]'=>'invoice amount','[invoice due date]'=>'invoice due date');
          $template_heads1  =   strtr($body2,$a1); 

		 	$data['template_footer']=$data['template_heads'][1];
		 	echo json_encode($data);
		 }

		 public function New_proposal(){
		 	$title='new_proposal';
		 	$name=$_POST['company_name'];	
		 	$proposal_name=$_POST['proposal_name'];	
		 	$receiver_mail_id=$_POST['receiver_mail_id'];
		 	$sender_company=$_POST['sender_company'];
		 	// if($_POST['company_name']!=''){
		 	// 	$company_details=$this->Common_mdl->select_record('client','crm_company_name',$company_name);
		 	// 	$name=$company_details['crm_first_name'];
		 	// }else{
		 	// 	$name=$receiver_mail_id;
		 	// }
		 	
		 	$datas=$this->Common_mdl->select_record('proposal_proposaltemplate','title',$title);
		 	$views=$datas['body'];
		 	$views1=$datas['subject'];
		 	$url=base_url().'proposals/';
//echo $name;
		   $a  =   array('::ClientName::'=>$name,'::ProposalCodeNumber::'=>$url);
		   $content['email_content']  =   strtr($views,$a); 
		   $a1  =   array('::ProposalName::'=>$proposal_name,'::SenderCompany::'=>$sender_company);
		   $content['email_subject']  =   strtr($views1,$a1); 
		 	$data['body'] ='<p id="mail_subject">'.$content['email_subject'].'</p></br><p>'.$content['email_content'].'</p>';
		 	echo json_encode($data);
		 }	
		
		  public function e_signature(){
		 	$this->load->view('proposal/my_sign');
		 }

		 public function save_sign(){
		 	$p_id=$_SESSION['proposals'];
		 	//echo $p_id;
			$result = array();
			$imagedata = base64_decode($_POST['img_data']);
			$filename = md5(date("dmYhisA"));
			//Location to where you want to created sign image
			$file_name = 'uploads/doc_signs/'.$filename.'.png';
			file_put_contents($file_name,$imagedata);
			$result['status'] = 1;
			$result['file_name'] = $file_name;
			$user_id=$this->session->userdata['userId'];  
			$data=array('user_id'=>$user_id,'signature_path'=>$file_name,'status'=>'active','proposal_no'=>$p_id);			
			$insert=$this->Common_mdl->insert('pdf_signatures',$data);
			echo json_encode($result); 				
		}

		public function proposals_send(){

		// print_r($_POST);
		// 	echo $_POST['company_name'];
		// 	exit;

		if(!empty($_FILES['userFiles']['name'])){
		  $filesCount = count($_FILES['userFiles']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                $uploadPath = 'attachment/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|pdf';                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
                }
            }
        if(!empty($uploadData)){
				foreach($uploadData as $up) {
				$file_name[] = $up['file_name'];
				}
				$images=implode(',',$file_name);
			}else{
				$images='';
			}
			}
			$user_id=$this->session->userdata['userId'];  
			if(isset($_POST['pdf_version'])){
			 $pdf_version=$_POST['pdf_version'];
			}else{
			  $pdf_version='off'; 
			}
			if(isset($_POST['pdf_table_content'])){
			 $pdf_table_content=$_POST['pdf_table_content'];
			}else{
			 $pdf_table_content='off';
			}
			if(isset($_POST['email_signature'])){ 
				$email_signature=$_POST['email_signature'];
			}else{ 
			    $email_signature='off';
			}
			if(isset($_POST['password'])){
				$password=$_POST['password'];
			}else{
				$password='off';
			}
			if(isset($_POST['proposal_password'])){ 
				$proposal_password=$_POST['proposal_password'];
			}else{ 
				$proposal_password=''; 
			}
			if(isset($_POST['expiration'])){ 
				$expiration=$_POST['expiration'];
			}else{
				$expiration='off';
			}
			if(isset($_POST['expiration_date'])){
				$expiration_date=$_POST['expiration_date'];
			}else{ 
				$expiration_date=''; 
			}
			if(isset($_POST['reminder_mail'])){
				$reminder_mail=$_POST['reminder_mail'];
			}else{ 
				$reminder_mail='off'; 
			}





		   $p_no=$_POST['p_no'];
	  	 $data['records']['proposal_contents']=$_POST['proposal_contents'];
     	 $userid=$this->session->userdata['userId']; 
       $data['images_section']=$_POST['images_section'];     
         $data['signature']=$this->db->query('select * from pdf_signatures where proposal_no='.$p_no.' Order By id Desc Limit 1')->row_array();
         $proposal_name=$_POST['proposal_name'];
           $data['images_section']=$_POST['images_section'];    
         $this->load->library('Tc');
         $pdf = new Tc('L', 'mm', 'A4', true, 'UTF-8', false); 
         $pdf->AddPage('L','A4');
         $html = $this->load->view('proposal/test_pdf',$data,true);    
         $footer_logo_html=base_url().'uploads/doc_signs/15904e140653b540bf1f247e49140ae4.png';
         $pdf->SetTitle($proposal_name);
         $pdf->SetHeaderMargin(30);
         $pdf->SetTopMargin(20);
         $pdf->setFooterMargin(20);
         $pdf->SetAutoPageBreak(true);
         $pdf->SetAuthor('Author');
         $pdf->SetDisplayMode('real', 'default');
         $pdf->WriteHTML($html);
         $dir = 'uploads/proposal_pdf/';
         $timestamp = date("Y-m-d_H:i:s");
         $filename=$proposal_name.'_'.$timestamp.'.pdf';       
         $pdf->Output(FCPATH . $dir . $filename, 'F');


         if($_POST['company_name']==''){

          if($_POST['receiver_mail_id']==''){

        $companyid=$this->db->query("SELECT * FROM `client` where `user_id`='".$_POST['company_name']."'")->row_array();
         $company_id=$companyid['crm_company_name'];
          $user_id=$companyid['user_id'];

         }else{
          $company_id='0';
         } 

       }else{


        $crm_company_name=$_POST['company_name'];

          if (strpos($crm_company_name, 'Leads_') !== false) {

          $leads_id=explode('_',$crm_company_name);
         //  echo $leads_id[1];
          // echo 'true';

           $leads_name=$this->Common_mdl->select_record('leads','id',$leads_id[1]);

           $company_id=$leads_name['name'];
           $data['lead_id']=$leads_id[1];

         }else{


         $companyid=$this->db->query("SELECT * FROM `client` where `user_id`='".$_POST['company_name']."'")->row_array();
         $company_id=$companyid['crm_company_name'];
          $user_id=$companyid['user_id'];
        }
       }


        //  if($_POST['receiver_mail_id']==''){

     	  // $companyid=$this->db->query("SELECT * FROM `client` where `id`='".$_POST['company_name']."'")->row_array();
        //  $company_id=$companyid['crm_company_name'];
        //   $user_id=$companyid['user_id'];

        //  }else{
        //  	$company_id='0';
        //  }         

          if(isset($_POST['sender_company'])){
          	$sender_company_name=$_POST['sender_company'];
          }else{
          	$sender_company_name='Accotax Private Limited';
          }

          if(isset($_POST['sender_mail_id'])){
          	$sender_company_mailid=$_POST['sender_mail_id'];
          }else{
          	$sender_company_mailid='info@remindoo.org';
          } 

         $lead_id=(isset($_POST['lead_id'])&&($_POST['lead_id']!=''))? $_POST['lead_id']:'0'; 




          if($_POST['receiver_mail_id']==''){
          $email_id=$this->db->query("SELECT * FROM `client` where `id` = '".$_POST['company_name']."'")->row_array();
            $user_id=$email_id['user_id'];
               $details=$this->db->query("SELECT * FROM `user` where `id` = '".$user_id."'")->row_array();
            $client_name=$details['crm_name'];
            $user_name=$details['username'];
          }else{
            $user_name=$_POST['receiver_mail_id'];
            $client_name='';
          }


          $user_details= $this->db->query("SELECT * FROM `user` where `id` = '".$_SESSION['id']."'")->row_array();
          // echo '<pre>';
          // print_r($user_details);
          // echo '</pre>';
          $account_name=$user_details['account_name'];
          $account_number=$user_details['account_no'];

        $body1=$_POST['proposal_contents_new'];
          $random_string=$this->generateRandomString();
           $a1  =   array('[Username]'=>$user_name,'[Client Name]'=>$client_name,'[Accountant Name]'=>$account_name,'[Task Number]'=>'task number','[Task Name]'=>'task name','[Task Due Date]'=>'task due date','[Task Client]'=>'task client','[Firm Name]'=>'firmname','[accountant number]'=> $account_number,'[invoice amount]'=>'invoice amount','[invoice due date]'=>'invoice due date');
          $proposal_content  =   strtr($body1,$a1); 




$ima_ges=explode(',',$_POST['attch_image']);
$attachhh=array();
for($i=0;$i<count($ima_ges);$i++){
    $imgExts = array("gif", "jpg", "jpeg", "png", "tiff", "tif");
    $urls = $ima_ges[$i];
    $urlExt = pathinfo($urls, PATHINFO_EXTENSION);

      $url1=pathinfo($urls)['filename'];
        $strlower = preg_replace("~[\\\\/:*?'&,<>|]~", '', $url1);
        $lesg = str_replace(' ', '_', $strlower);
      //  $lesg_s = str_replace('-', '_', $lesg);
        $lesgs = str_replace('.', '_', $lesg);

        //  echo pathinfo($url1)['extension']; // "ext"

          $ext = pathinfo($urls, PATHINFO_EXTENSION);

      // echo   $lesgs=pathinfo($url1)['filename']; // "file"
       $att_image=$lesgs.'.'.$ext;     

       if($att_image!='.'){
        array_push($attachhh,$att_image);
      }
 }
// echo '<pre>';
// print_r($attachhh);
// echo '</pre>';



if(isset($_POST['service_optional'])){

 $service_optional=implode(',',$_POST['service_optional']);
  }else{
    $service_optional='';
  }
 if(isset($_POST['service_quantity'])){ 
  $service_quantity=implode(',',$_POST['service_quantity']);
   }else{
    $service_quantity='';
   }
if(isset($_POST['product_optional'])){

$product_optional=implode(',',$_POST['product_optional']);
}else{
  $product_optional='';
}
if(isset($_POST['product_quantity'])){ 
  $product_quantity=implode(',',$_POST['product_quantity']);
}else{
  $product_quantity='';
}
if(isset($_POST['subscription_optional'])){ 
  $subscription_optional=implode(',',$_POST['subscription_optional']);
   }else{
    $subscription_optional='';
   }
if(isset($_POST['subscription_quantity'])){ 
  $subscription_quantity=implode(',',$_POST['subscription_quantity']);
   }else{
    $subscription_quantity='';
   }




 $attaching_image=implode(',',$attachhh);
 //exit;

		  	$data=array('proposal_name'=>$_POST['proposal_name'],
            				'proposal_type'=>$_POST['proposal_type'],
            				'company_name'=>$company_id,
            				'proposal_no'=>$_POST['proposal_no'],
            				'item_name'=>implode(',',$_POST['item_name']),
            				'service_category'=>implode(',',$_POST['service_category_name']),
            				'price'=>implode(',',$_POST['price']),
            				'unit'=>implode(',',$_POST['unit']),
            				'qty'=>implode(',',$_POST['qty']),
            				'tax'=>implode(',',$_POST['service_tax']),
            				'discount'=>implode(',',$_POST['service_discount']),
            				'description'=>implode(',',$_POST['description']),
            				'product_name'=>implode(',',$_POST['product_name']),
            				'product_category_name'=>implode(',',$_POST['product_category_name']),
            				'product_price'=>implode(',',$_POST['product_price']),
            				'product_qty'=>implode(',',$_POST['product_qty']),
            				'product_tax'=>implode(',',$_POST['product_tax']),
            				'product_discount'=>implode(',',$_POST['product_discount']),
            				'product_description'=>implode(',',$_POST['product_description']),
            				'subscription_name'=>implode(',',$_POST['subscription_name']),
            				'subscription_category_name'=>implode(',',$_POST['subscription_category_name']),
            				'subscription_price'=>implode(',',$_POST['subscription_price']),
            				'subscription_unit'=>implode(',',$_POST['subscription_unit']),
            				'subscription_tax'=>implode(',',$_POST['subscription_tax']),
            				'subscription_discount'=>implode(',',$_POST['subscription_discount']),
            				'subscription_description'=>implode(',',$_POST['subscription_description']),
            				'proposal_contents'=>$proposal_content,
            				'proposal_mail'=>$_POST['proposal_mail'],
            				'pdf_version'=>$pdf_version,
            				'pdf_table_content'=>$pdf_table_content,
            				'email_signature'=>$email_signature,
            				'password'=>$password,
            				'pdf_password'=>$proposal_password,
            				'expiration'=>$expiration,
            				'expiration_date'=>$expiration_date,
            				'reminder_mail'=>$reminder_mail,
            				'status'=>$_POST['status'],
                    'user_id'=>$userid,
                    'attachment'=>$attaching_image,
                    'pdf_file'=>$filename,
                    'currency'=>$_POST['currency_symbol'],
                    'pricelist'=>$_POST['pricelist'],
                    'company_id'=>$user_id,
                    'sender_company_name'=>$sender_company_name,
                    'sender_company_mailid'=>$sender_company_mailid,
                    'grand_total'=>$_POST['grand_total'],
                    'total_amount'=>$_POST['total_total'],
                    'lead_id'=>$lead_id,
                    'receiver_mail_id'=>$_POST['receiver_mail_id'],'document_attachment'=>$_POST['document_attachments'],'discount_option'=>$_POST['discount'],'tax_option'=>$_POST['tax'],'receiver_company_name'=>$_POST['receiver_company_name'],
                    'created_at'=>date('Y-m-d h:i:s'),
                      'service_optional'=>$service_optional,
                      'service_quantity'=>$service_quantity,
                      'product_optional'=>$product_optional,
                      'product_quantity'=>$product_quantity,
                      'subscription_optional'=>$subscription_optional,
                      'subscription_quantity'=>$subscription_quantity,'images_section'=>$_POST['images_section'],'tax_amount'=>$_POST['tax_amount'],
                      'discount_amount'=>$_POST['discount_amount']
                  );



         $crm_company_name=$_POST['company_name'];

          if (strpos($crm_company_name, 'Leads_') !== false) {

          $leads_id=explode('_',$crm_company_name);
         //  echo $leads_id[1];
          // echo 'true';

           $leads_name=$this->Common_mdl->select_record('leads','id',$leads_id[1]);

           $company_id=$leads_name['name'];
           $data['lead_id']=$leads_id[1];

         }

         

			if(!empty($uploadData)){				
					foreach($file_name as $key => $value) {					
						$file = $file_name[$key];						
				  }
				}
				
				if($sender_company_mailid==''){
					$from='info@remindoo.org';
				}else{
					$from=$sender_company_mailid;
				}

			if($_POST['proposal_mail']!='' && $_POST['proposal_contents']!=''){



				$last_id=$this->Common_mdl->insert('proposals',$data);   

				$data=array('proposal_id'=>$last_id,'tag'=>'insert','created_at'=>date('Y-m-d h:i:s'));
				$inse=$this->Common_mdl->insert('proposal_history',$data);  

			if($_POST['status']=='sent'){

				if($_POST['receiver_mail_id']==''){
					$emailid=$this->db->query("SELECT `crm_email` FROM `client` where `id` = '".$_POST['company_name']."'")->row_array();
				    $email=$emailid['crm_email'];
			    }else{
			    	$email=$_POST['receiver_mail_id'];
			    }
				$email_subject=$_POST['subject'];
				$body1=$_POST['proposal_mail'];
				$random_string=$this->generateRandomString();
				$a1  =   array('http://remindoo.org/CRMTool/proposals/'=> base_url().'Proposal/proposal/'.$random_string.'---'.$last_id.'');
		        $body  =   strtr($body1,$a1); 
			    $this->email->set_mailtype("html");
				$this->email->from($from);
				$this->email->to($email);
				$this->email->subject($email_subject);
				$this->email->message($body);
				$this->email->attach(FCPATH.'uploads/proposal_pdf/'.$filename);

				if($_POST['document_attachments']!=''){
					$attaching=explode(',',$_POST['document_attachments']);
					for($i=0;$i< count($attaching);$i++){
						$this->email->attach($attaching[$i]);
					}
				}

				if(!empty($attaching_image)){	
      $attaching_image=explode(',',$attaching_image);			
					for($i=0;$i<count($attaching_image);$i++) {
						$file = base_url().'attachment/'.$attaching_image[$i];						
						$this->email->attach($file);
				  }
				}				
				$send = $this->email->send();	

				// echo $send;
				// exit;
			}	


				$random_string=$this->generateRandomString();
				redirect('Proposal_page/step_proposal/'.$random_string.'---'.$last_id.'');
	}else{
		redirect('proposal_page/step_proposal');
	}
		}

		/* 01.06.2018 */
		public function proposals_update(){

		if(!empty($_FILES['userFiles']['name'])){
		  $filesCount = count($_FILES['userFiles']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                $uploadPath = 'attachment/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|pdf';                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
                }
            }
            if(!empty($uploadData)){
				foreach($uploadData as $up) {
				$file_name[] = $up['file_name'];
				}
				$images=implode(',',$file_name);
			}else{
				$images='';
			}
			}
			$user_id=$this->session->userdata['userId'];  
			if(isset($_POST['pdf_version'])){
			 $pdf_version=$_POST['pdf_version'];
			}else{
			  $pdf_version='off'; 
			}
			if(isset($_POST['pdf_table_content'])){
			 $pdf_table_content=$_POST['pdf_table_content'];
			}else{
			 $pdf_table_content='off';
			}
			if(isset($_POST['email_signature'])){ 
				$email_signature=$_POST['email_signature'];
			}else{ 
			    $email_signature='off';
			}
			if(isset($_POST['password'])){
				$password=$_POST['password'];
			}else{
				$password='off';
			}
			if(isset($_POST['proposal_password'])){ 
				$proposal_password=$_POST['proposal_password'];
			}else{ 
				$proposal_password=''; 
			}
			if(isset($_POST['expiration'])){ 
				$expiration=$_POST['expiration'];
			}else{
				$expiration='off';
			}
			if(isset($_POST['expiration_date'])){
				$expiration_date=$_POST['expiration_date'];
			}else{ 
				$expiration_date=''; 
			}
			if(isset($_POST['reminder_mail'])){
				$reminder_mail=$_POST['reminder_mail'];
			}else{ 
				$reminder_mail='off'; 
			}

	  		$p_no=$_POST['p_no'];
	  	 $data['records']['proposal_contents']=$_POST['proposal_contents'];
     	 $userid=$this->session->userdata['userId'];     
			if($email_signature=='on'){   
			$data['signature']=$this->db->query('select * from pdf_signatures where proposal_no='.$p_no.' Order By id Desc Limit 1')->row_array();
			}

         $proposal_name=$_POST['proposal_name'];
         $this->load->library('Tc');
         $pdf = new Tc('L', 'mm', 'A4', true, 'UTF-8', false); 
         $pdf->AddPage('L','A4');
         $html = $this->load->view('proposal/test_pdf',$data,true);    
         $footer_logo_html=base_url().'uploads/doc_signs/15904e140653b540bf1f247e49140ae4.png';
         $pdf->SetTitle($proposal_name);
         $pdf->SetHeaderMargin(30);
         $pdf->SetTopMargin(20);
         $pdf->setFooterMargin(20);
         $pdf->SetAutoPageBreak(true);
         $pdf->SetAuthor('Author');
         $pdf->SetDisplayMode('real', 'default');
         $pdf->WriteHTML($html);
         $dir = 'uploads/proposal_pdf/';
         $timestamp = date("Y-m-d_H:i:s");
         $filename=$proposal_name.'_'.$timestamp.'.pdf';       
        $pdf->Output(FCPATH . $dir . $filename, 'F');




         if($_POST['company_name']==''){

          if($_POST['receiver_mail_id']==''){

        $companyid=$this->db->query("SELECT * FROM `client` where `user_id`='".$_POST['company_name']."'")->row_array();
         $company_id=$companyid['crm_company_name'];
          $user_id=$companyid['user_id'];

         }else{
          $company_id='0';
         } 

       }else{


        $crm_company_name=$_POST['company_name'];

          if (strpos($crm_company_name, 'Leads_') !== false) {

          $leads_id=explode('_',$crm_company_name);
         //  echo $leads_id[1];
          // echo 'true';

           $leads_name=$this->Common_mdl->select_record('leads','id',$leads_id[1]);

           $company_id=$leads_name['name'];
           $data['lead_id']=$leads_id[1];

         }else{


         $companyid=$this->db->query("SELECT * FROM `client` where `user_id`='".$_POST['company_name']."'")->row_array();
         $company_id=$companyid['crm_company_name'];
          $user_id=$companyid['user_id'];
        }
       }

       

          if(isset($_POST['sender_company'])){
            $sender_company_name=$_POST['sender_company'];
          }else{
            $sender_company_name='Accotax Private Limited';
          }

          if(isset($_POST['sender_mail_id'])){
            $sender_company_mailid=$_POST['sender_mail_id'];
          }else{
            $sender_company_mailid='info@remindoo.org';
          } 

         $lead_id=(isset($_POST['lead_id'])&&($_POST['lead_id']!=''))? $_POST['lead_id']:'0'; 




          if($_POST['receiver_mail_id']==''){
          $email_id=$this->db->query("SELECT * FROM `client` where `id` = '".$_POST['company_name']."'")->row_array();
            $user_id=$email_id['user_id'];
               $details=$this->db->query("SELECT * FROM `user` where `id` = '".$user_id."'")->row_array();
            $client_name=$details['crm_name'];
            $user_name=$details['username'];
          }else{
            $user_name=$_POST['receiver_mail_id'];
            $client_name='';
          }


          $user_details= $this->db->query("SELECT * FROM `user` where `id` = '".$_SESSION['id']."'")->row_array();
          // echo '<pre>';
          // print_r($user_details);
          // echo '</pre>';
          $account_name=$user_details['account_name'];
          $account_number=$user_details['account_no'];

        $body1=$_POST['proposal_contents_new'];
          $random_string=$this->generateRandomString();
           $a1  =   array('[Username]'=>$user_name,'[Client Name]'=>$client_name,'[Accountant Name]'=>$account_name,'[Task Number]'=>'task number','[Task Name]'=>'task name','[Task Due Date]'=>'task due date','[Task Client]'=>'task client','[Firm Name]'=>'firmname','[accountant number]'=> $account_number,'[invoice amount]'=>'invoice amount','[invoice due date]'=>'invoice due date');
          $proposal_content  =   strtr($body1,$a1); 



          $ima_ges=explode(',',$_POST['attch_image']);
        $attachhh=array();
    for($i=0;$i<count($ima_ges);$i++){
    $imgExts = array("gif", "jpg", "jpeg", "png", "tiff", "tif");
    $urls = $ima_ges[$i];
    $urlExt = pathinfo($urls, PATHINFO_EXTENSION);

      $url1=pathinfo($urls)['filename'];
        $strlower = preg_replace("~[\\\\/:*?'&,<>|]~", '', $url1);
        $lesg = str_replace(' ', '_', $strlower);
      //  $lesg_s = str_replace('-', '_', $lesg);
        $lesgs = str_replace('.', '_', $lesg);

        //  echo pathinfo($url1)['extension']; // "ext"

          $ext = pathinfo($urls, PATHINFO_EXTENSION);

      // echo   $lesgs=pathinfo($url1)['filename']; // "file"
       $att_image=$lesgs.'.'.$ext;     
        array_push($attachhh,$att_image);
 }
// echo '<pre>';
// print_r($attachhh);
// echo '</pre>';

   if(isset($_POST['service_optional'])){

 $service_optional=implode(',',$_POST['service_optional']);
  }else{
    $service_optional='';
  }
 if(isset($_POST['service_quantity'])){ 
  $service_quantity=implode(',',$_POST['service_quantity']);
   }else{
    $service_quantity='';
   }
if(isset($_POST['product_optional'])){

$product_optional=implode(',',$_POST['product_optional']);
}else{
  $product_optional='';
}
if(isset($_POST['product_quantity'])){ 
  $product_quantity=implode(',',$_POST['product_quantity']);
}else{
  $product_quantity='';
}
if(isset($_POST['subscription_optional'])){ 
  $subscription_optional=implode(',',$_POST['subscription_optional']);
   }else{
    $subscription_optional='';
   }
if(isset($_POST['subscription_quantity'])){ 
  $subscription_quantity=implode(',',$_POST['subscription_quantity']);
   }else{
    $subscription_quantity='';
   }


 $attaching_image=implode(',',$attachhh);

        $data=array('proposal_name'=>$_POST['proposal_name'],
                    'proposal_type'=>$_POST['proposal_type'],
                    'company_name'=>$company_id,
                    'proposal_no'=>$_POST['proposal_no'],
                    'item_name'=>implode(',',$_POST['item_name']),
                    'service_category'=>implode(',',$_POST['service_category_name']),
                    'price'=>implode(',',$_POST['price']),
                    'unit'=>implode(',',$_POST['unit']),
                    'qty'=>implode(',',$_POST['qty']),
                    'tax'=>implode(',',$_POST['service_tax']),
                    'discount'=>implode(',',$_POST['service_discount']),
                    'description'=>implode(',',$_POST['description']),
                    'product_name'=>implode(',',$_POST['product_name']),
                    'product_category_name'=>implode(',',$_POST['product_category_name']),
                    'product_price'=>implode(',',$_POST['product_price']),
                    'product_qty'=>implode(',',$_POST['product_qty']),
                    'product_tax'=>implode(',',$_POST['product_tax']),
                    'product_discount'=>implode(',',$_POST['product_discount']),
                    'product_description'=>implode(',',$_POST['product_description']),
                    'subscription_name'=>implode(',',$_POST['subscription_name']),
                    'subscription_category_name'=>implode(',',$_POST['subscription_category_name']),
                    'subscription_price'=>implode(',',$_POST['subscription_price']),
                    'subscription_unit'=>implode(',',$_POST['subscription_unit']),
                    'subscription_tax'=>implode(',',$_POST['subscription_tax']),
                    'subscription_discount'=>implode(',',$_POST['subscription_discount']),
                    'subscription_description'=>implode(',',$_POST['subscription_description']),
                    'proposal_contents'=>$proposal_content,
                    'proposal_mail'=>$_POST['proposal_mail'],
                    'pdf_version'=>$pdf_version,
                    'pdf_table_content'=>$pdf_table_content,
                    'email_signature'=>$email_signature,
                    'password'=>$password,
                    'pdf_password'=>$proposal_password,
                    'expiration'=>$expiration,
                    'expiration_date'=>$expiration_date,
                    'reminder_mail'=>$reminder_mail,
                    'status'=>$_POST['status'],'user_id'=>$userid,'attachment'=>$attaching_image,'pdf_file'=>$filename,'currency'=>$_POST['currency_symbol'],'pricelist'=>$_POST['pricelist'],'company_id'=>$user_id,'sender_company_name'=>$sender_company_name,'sender_company_mailid'=>$sender_company_mailid,'grand_total'=>$_POST['grand_total'],'total_amount'=>$_POST['total_total'],'lead_id'=>$lead_id,'receiver_mail_id'=>$_POST['receiver_mail_id'],'document_attachment'=>$_POST['document_attachments'],'discount_option'=>$_POST['discount'],'tax_option'=>$_POST['tax'],'receiver_company_name'=>$_POST['receiver_company_name'],'service_optional'=>$service_optional,
                      'service_quantity'=>$service_quantity,
                      'product_optional'=>$product_optional,
                      'product_quantity'=>$product_quantity,
                      'subscription_optional'=>$subscription_optional,
                      'subscription_quantity'=>$subscription_quantity,'images_section'=>$_POST['images_section'],'tax_amount'=>$_POST['tax_amount'],
                      'discount_amount'=>$_POST['discount_amount']
                  );

      if(!empty($uploadData)){        
          foreach($file_name as $key => $value) {         
            $file = $file_name[$key];           
          }
        }
        
        if($sender_company_mailid==''){
          $from='info@remindoo.org';
        }else{
          $from=$sender_company_mailid;
        }

   //     $companyid=$this->db->query("SELECT * FROM `client` where `id`='".$_POST['company_name']."'")->row_array();
   //       $company_id=$companyid['crm_company_name'];
   //        $user_id=$companyid['user_id'];

   //        if(isset($_POST['sender_company_name'])){
   //        	$sender_company_name=$_POST['sender_company_name'];
   //        }else{
   //        	$sender_company_name='Accotax Private Limited';
   //        }

   //        if(isset($_POST['sender_company_mailid'])){
   //        	$sender_company_mailid=$_POST['sender_company_mailid'];
   //        }else{
   //        	$sender_company_mailid='info@remindoo.org';
   //        }

			// $data=array('proposal_name'=>$_POST['proposal_name'],
			// 	'proposal_type'=>$_POST['proposal_type'],
			// 	'company_name'=>$company_id,
			// 	'proposal_no'=>$_POST['proposal_no'],
			// 	'item_name'=>implode(',',$_POST['item_name']),
			// 	'service_category'=>implode(',',$_POST['service_category_name']),
			// 	'price'=>implode(',',$_POST['price']),
			// 	'unit'=>implode(',',$_POST['unit']),
			// 	'qty'=>implode(',',$_POST['qty']),
			// 	'tax'=>implode(',',$_POST['service_tax']),
			// 	'discount'=>implode(',',$_POST['service_discount']),
			// 	'description'=>implode(',',$_POST['description']),
			// 	'product_name'=>implode(',',$_POST['product_name']),
			// 	'product_category_name'=>implode(',',$_POST['product_category_name']),
			// 	'product_price'=>implode(',',$_POST['product_price']),
			// 	'product_qty'=>implode(',',$_POST['product_qty']),
			// 	'product_tax'=>implode(',',$_POST['product_tax']),
			// 	'product_discount'=>implode(',',$_POST['product_discount']),
			// 	'product_description'=>implode(',',$_POST['product_description']),
			// 	'subscription_name'=>implode(',',$_POST['subscription_name']),
			// 	'subscription_category_name'=>implode(',',$_POST['subscription_category_name']),
			// 	'subscription_price'=>implode(',',$_POST['subscription_price']),
			// 	'subscription_unit'=>implode(',',$_POST['subscription_unit']),
			// 	'subscription_tax'=>implode(',',$_POST['subscription_tax']),
			// 	'subscription_discount'=>implode(',',$_POST['subscription_discount']),
			// 	'subscription_description'=>implode(',',$_POST['subscription_description']),
			// 	'proposal_contents'=>$_POST['proposal_contents'],
			// 	'proposal_mail'=>$_POST['proposal_mail'],
			// 	'pdf_version'=>$pdf_version,
			// 	'pdf_table_content'=>$pdf_table_content,
			// 	'email_signature'=>$email_signature,
			// 	'password'=>$password,
			// 	'pdf_password'=>$proposal_password,
			// 	'expiration'=>$expiration,
			// 	'expiration_date'=>$expiration_date,
			// 	'reminder_mail'=>$reminder_mail,
			// 	'status'=>$_POST['status'],'user_id'=>$userid,'attachment'=>$images,'pdf_file'=>$filename,'currency'=>$_POST['currency'],'pricelist'=>$_POST['pricelist'],'company_id'=>$user_id,'sender_company_name'=>$sender_company_name,'sender_company_mailid'=>$sender_company_mailid,'grand_total'=>$_POST['grand_total'],'total_amount'=>$_POST['total_total']);

			// if(!empty($uploadData)){				
			// 		foreach($file_name as $key => $value) {
			// 			//print_r($value);
			// 			//print_r($key);
			// 			$file = $file_name[$key];
			// 			//echo $file;
			// 			//$this->email->attach($file);
			// 	  }
			// 	}
				
				// if($sender_company_mailid==''){
				// 	$from='info@remindoo.org';
				// }else{
				// 	$from=$sender_company_mailid;
				// }


				$proposal_id=$_POST['proposal_id'];
				$last_id=$proposal_id;

				$id=$this->Common_mdl->update('proposals',$data,'id',$proposal_id);

				$data=array('proposal_id'=>$proposal_id,'tag'=>'update','created_at'=>date('Y-m-d h:i:s'));
				$inse=$this->Common_mdl->insert('proposal_history',$data);




			if($_POST['status']=='sent'){
        if($_POST['receiver_mail_id']==''){
          $emailid=$this->db->query("SELECT `crm_email` FROM `client` where `id` = '".$_POST['company_name']."'")->row_array();
            $email=$emailid['crm_email'];
          }else{
            $email=$_POST['receiver_mail_id'];
          }
			//	$emailid=$this->db->query("SELECT `crm_email` FROM `client` where `id` = '".$_POST['company_name']."'")->row_array();
			  //  $email=$emailid['crm_email'];
				$email_subject=$_POST['subject'];
				$body1=$_POST['proposal_mail'];
				$random_string=$this->generateRandomString();
				$a1  =   array('http://remindoo.org/CRMTool/proposals/'=> base_url().'Proposal/proposal/'.$random_string.'---'.$last_id.'');
		        $body  =   strtr($body1,$a1); 
			    $this->email->set_mailtype("html");
				$this->email->from($from);
				$this->email->to($email);
				$this->email->subject($email_subject);
				$this->email->message($body);
        $this->email->attach(base_url().'uploads/proposal_pdf/'.$filename);

        if($_POST['document_attachments']!=''){
          $attaching=explode(',',$_POST['document_attachments']);
          for($i=0;$i< count($attaching);$i++){
            $this->email->attach($attaching[$i]);
          }
        }

			if(!empty($attaching_image)){  
      $attaching_image=explode(',',$attaching_image);     
          for($i=0;$i<count($attaching_image);$i++) {
            $file = base_url().'attachment/'.$attaching_image[$i];            
            $this->email->attach($file);
          }
        }   				
				$send = $this->email->send();	
			}	

      $proposal_status=$this->Common_mdl->select_record('proposals','id',$proposal_id);

      if($proposal_status['status']=='accepted'){

        $this->invoive_create($proposal_id);	

      }
				$random_string=$this->generateRandomString();
				redirect('proposal/');
		}



		/*01.06.2018 */


		public function generateRandomString($length = 100) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
		}

		 public function mailsettings()
    {   
        $this->load->library('email');
        $config['wrapchars'] = 76; // Character count to wrap at.
        $config['mailtype'] = 'html'; 
        $this->email->initialize($config);  
    }

    public function status_change(){
    	$id=$_POST['id'];
    	$status=$_POST['status'];


//echo $id;

    //	echo $status;
    	if($status=='accepted'){




    		$data['records']=$this->Common_mdl->select_record('proposals','id',$id); 




        $pro=$this->Common_mdl->select_record('proposals','id',$id); 
        $lead_id=$pro['lead_id'];

        // echo '<pre>';

        // print_r($pro);
        // echo '</pre>';

        // exit;

          if($pro['status']!='accepted'){  

         //  $id=$_POST['id'];
         // $value=$_POST['status'];

          $lead_id=$pro['lead_id'];
          if($lead_id!=0){


          //echo $id;
          $l_name = $this->Common_mdl->select_record('leads_status','status_name','customer');
          $status_id=$l_name['id'];
          $da_ta['lead_status']=$status_id;
          if($this->Common_mdl->update('leads',$da_ta,'id',$lead_id))
          {
          echo $l_name['status_name'];
          $this->Proposal_model->leads_convert_client($lead_id); // convert to client
          $this->Proposal_model->leads_convert_task($lead_id); // convert to task
          //echo "update";
          } 

        //  echo $pro['id']; 
           $this->invoive_create($pro['id']);

         }else{


           $task_id=array();

            $pro=$this->Common_mdl->select_record('proposals','id',$id); 

              $datas['subject']='create/update Client-'.$pro['company_name'];
            $datas['start_date']=date('Y-m-d');
            $datas['user_id']=$_SESSION['id'];
            $datas['end_date']=$date = date('Y-m-d', strtotime('+2 days'));;
            $datas['company_name']= $pro['company_id'];
            $datas['worker'] = '';
            $datas['team'] = '';
            $datas['department'] = '';
            $datas['manager']='';
            $datas['billable']='Billable';
            $datas['description']='';
            $datas['lead_id']=$pro['lead_id'];          
            $datas['created_date']=time();
            $datas['create_by']=$_SESSION['id'];
            $datas['priority'] = 'Low';
            $datas['related_to'] = 'Auto created task';
            $datas['task_status']='notstarted';        
            $insert_data=$this->db->insert( 'add_new_task', $datas );

           // $task_id=$this->db->insert_id();

           array_push($task_id,$this->db->insert_id());

            $datas['subject']='Create invoice-'.$pro['company_name'];
            $datas['start_date']=date('Y-m-d');
            $datas['user_id']=$_SESSION['id'];
            $datas['end_date']=$date = date('Y-m-d', strtotime('+2 days'));;
            $datas['company_name']=$pro['company_id'];
            $datas['worker'] = '';
            $datas['team'] = '';
            $datas['department'] = '';
            $datas['manager']='';
            $datas['billable']='Billable';       
            $datas['created_date']=time();
            $datas['create_by']=$_SESSION['id'];
            $datas['priority'] = 'Low';
            $datas['related_to'] = 'Auto created task';
            $datas['task_status']='notstarted'; 
            $datas['lead_id']=$pro['lead_id'];       
            $insert_data=$this->db->insert( 'add_new_task', $datas ); 

            array_push($task_id,$this->db->insert_id());

             $convert_data_proposal['task_id']=implode(',',$task_id);

            $this->Common_mdl->update('proposals',$convert_data_proposal,'id',$id);


             $this->invoive_create($pro['id']);

         }
          }

        //  $data=array('proposal_id'=>$id,'tag'=>'accepted','created_at'=>date('Y-m-d h:i:s'));
        // $inse=$this->Common_mdl->insert('proposal_history',$data);



        $proposal_no=$pro['proposal_no'];
        $proposal_name=$pro['proposal_name'];
        $company_name=$pro['company_name'];
        $currency=$pro['currency'];
        $expiration_date=$pro['expiration_date'];

        if($company_name==0){
          $company_name=$pro['receiver_company_name'];
        }

        $senders_details=$this->Common_mdl->select_record('user','id',$pro['user_id']);
        $sender_email=$senders_details['crm_email_id']; 
        $sender_name=$senders_details['crm_name']; 
        $sender_last_name=$senders_details['crm_last_name']; 
        $sender_country=$senders_details['crm_country'];
        $sender_city=$senders_details['crm_city'];
        $sender_religion=$senders_details['crm_state']; 
         $sender_company_details=$this->Common_mdl->select_record('client','user_id',$pro['user_id']);
         if($sender_company_details[0]==''){
          $sender_company=$pro['sender_company_name'];
          $sender_mobile_number='';
         }else{
          $sender_company=$sender_company_details['crm_company_name'];
          $sender_mobile_number=$sender_company_details['crm_mobile_number'];
         }


    		$sender_company_mailid=$pro['sender_company_mailid'];
             $proposal_name=$pro['proposal_name'];
             $company=$pro['company_id'];

             if($company!=1){
             $company_name=$pro['company_name'];
         	}else{
         		 $company_name=$pro['receiver_mail_id'];
         	}
             $sender_details=$this->Common_mdl->select_record('proposals','id',$id);
             $sender_id=$sender_details['company_id'];  

             if($sender_id!=1){
                $user_details=$this->Common_mdl->select_record('user','id',$sender_id);
                $client_details=$this->Common_mdl->select_record('client','user_id',$sender_id);
                $client_website=$client_details['crm_company_url'];
                $client_mobile_number=$client_details['crm_mobile_number'];
                $email=$user_details['crm_email_id']; 
                $name=$user_details['crm_name']; 
                $last_name=$user_details['crm_last_name']; 
                $country=$user_details['crm_country'];
                $city=$user_details['crm_city'];
                $religion=$user_details['crm_state'];             
          	}else{
         	    	$email=$sender_details['receiver_mail_id'];	
                $client_website='';
                $client_mobile_number='';
                $name=''; 
                $last_name=''; 
                $country='';
                $city='';
                $religion=''; 
          	}




          $proposal_content=$this->Common_mdl->select_record('proposal_proposaltemplate','title','accept_proposal_sender');

          $pdf_signature=$this->Common_mdl->select_record('pdf_signatures','proposal_no',$id);

          if($pdf_signature[0]==''){
            $email_signature='';
          }else{
            $email_signature='<img src='.$pdf_signature['signature_path'].'>';
          }



          $body1=$proposal_content['body'];
          $subject=$proposal_content['subject'];

          $random_string=$this->generateRandomString();
          $link=base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id;
        //  echo $link;
           $a1  =   array('::Client Name::'=>$name,
            '::Client Company::'=>$company_name,
            '::Client Company City::'=>$city,
            '::Client Company Country::'=>$country,
            '::Client Company Religion::'=>$religion,
            '::Client First Name::'=>$name,
            '::Client Last Name::'=>$last_name,
            '::Client Name::'=>$name,
            '::ClientPhoneno::'=> $client_mobile_number,
            '::ClientWebsite::'=>$client_website,
            ':: ClientProposallink::'=>$link,
            ':: ProposalCurrency::'=>$currency,
            ':: ProposalExpirationDate::'=>$expiration_date,
            ':: ProposalName::'=>$proposal_name,
            ':: ProposalCodeNumber::'=>$proposal_no,
            '::SenderCompany::'=>$sender_company,
            '::SenderCompanyAddress::'=>'',
            '::SenderCompanyCity::'=>$sender_city,
            '::SenderCompanyCountry::'=>$sender_country,
            '::SenderCompanyReligion::'=>$sender_religion,
            '::SenderEmail::'=>$sender_email,
            '::SenderEmailSignature::'=>$email_signature,
            '::SenderName::'=>$sender_name,
            '::senderPhoneno::'=>$sender_mobile_number,
            '::ProposalTitle::'=>$proposal_name,
            '::Date::'=>date("Y-m-d h:i:s"));




          $proposal_contents  =   strtr($body1,$a1); 
          $subject  =   strtr($subject,$a1);
          $random_string=$this->generateRandomString();
          $body= $proposal_contents;
            // $body  =   '<p>Congratulations!</p><br/><p>' .$proposal_name.'proposal was accepted by'.$company_name.' at'.date("Y-m-d h:i:s").'</p><br/><br/><p>Use Following link to open proposal '.base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id.'</p> <br/> Thanks';



          // echo '<pre>';
          // print_r(strip_tags($subject));
          //  print_r(strip_tags($body));
          // echo '</pre>';
             $email_subject= $proposal_name.'proposal was accepted by'.$company_name.'';
            $this->load->library('email');
            $this->email->set_mailtype("html");
            $this->email->from($email);
            $this->email->to($sender_company_mailid);
          $this->email->subject(preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($subject))));
            $this->email->message($body);
            $send = $this->email->send();


          $proposal_content=$this->Common_mdl->select_record('proposal_proposaltemplate','title','accept_proposal_client');

          $body1=$proposal_content['body'];
          $subject=$proposal_content['subject'];

          $random_string=$this->generateRandomString();
          $link=base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id;
        //  echo $link;
           $a1  =   array('::Client Name::'=>$name,
            '::Client Company::'=>$company_name,
            '::Client Company City::'=>$city,
            '::Client Company Country::'=>$country,
            '::Client Company Religion::'=>$religion,
            '::Client First Name::'=>$name,
            '::Client Last Name::'=>$last_name,
            '::Client Name::'=>$name,
            '::ClientPhoneno::'=> $client_mobile_number,
            '::ClientWebsite::'=>$client_website,
            ':: ClientProposallink::'=>$link,
            ':: ProposalCurrency::'=>$currency,
            ':: ProposalExpirationDate::'=>$expiration_date,
            ':: ProposalName::'=>$proposal_name,
            ':: ProposalCodeNumber::'=>$proposal_no,
            '::SenderCompany::'=>$sender_company,
            '::SenderCompanyAddress::'=>'',
            '::SenderCompanyCity::'=>$sender_city,
            '::SenderCompanyCountry::'=>$sender_country,
            '::SenderCompanyReligion::'=>$sender_religion,
            '::SenderEmail::'=>$sender_email,
            '::SenderEmailSignature::'=>$email_signature,
            '::SenderName::'=>$sender_name,
            '::senderPhoneno::'=>$sender_mobile_number,
            '::ProposalTitle::'=>$proposal_name,
            '::Date::'=>date("Y-m-d h:i:s"));



          $proposal_contents  =   strtr($body1,$a1); 
          $subject  =   strtr($subject,$a1);
          $random_string=$this->generateRandomString();
          $body= $proposal_contents;

          //   echo '<pre>';
          // print_r(strip_tags($subject));
          //  print_r(strip_tags($body));
          // echo '</pre>';

            $random_string=$this->generateRandomString();
           $body  =   '<p>Congratulations! you have accepted the </p><br/><p>"' .$proposal_name.'"at'.date("Y-m-d h:i:s").'</p><br/><br/><p>Use Following link to open proposal '.base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id.'</p> <br/> Thanks';
            $email_subject= $proposal_name.'proposal was accepted';
            $this->load->library('email');
            $this->email->set_mailtype("html");
            $this->email->from($sender_company_mailid);
            $this->email->to($email);
            $this->email->subject(preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($subject))));
            $this->email->message($body);
            $send = $this->email->send();

    	}
    	if($status=='declined'){



           $data=array('proposal_id'=>$id,'tag'=>'declined','created_at'=>date('Y-m-d h:i:s'));
        $inse=$this->Common_mdl->insert('proposal_history',$data);
    
    	
        $data['records']=$this->Common_mdl->select_record('proposals','id',$id); 
          $pro=$this->Common_mdl->select_record('proposals','id',$id); 

          $proposal_no=$pro['proposal_no'];
        $proposal_name=$pro['proposal_name'];
        $company_name=$pro['company_name'];
        $currency=$pro['currency'];
        $expiration_date=$pro['expiration_date'];

        if($company_name==0){
          $company_name=$pro['receiver_company_name'];
        }

        $senders_details=$this->Common_mdl->select_record('user','id',$pro['user_id']);
        $sender_email=$senders_details['crm_email_id']; 
        $sender_name=$senders_details['crm_name']; 
        $sender_last_name=$senders_details['crm_last_name']; 
        $sender_country=$senders_details['crm_country'];
        $sender_city=$senders_details['crm_city'];
        $sender_religion=$senders_details['crm_state']; 
         $sender_company_details=$this->Common_mdl->select_record('client','user_id',$pro['user_id']);
         if($sender_company_details[0]==''){
          $sender_company=$pro['sender_company_name'];
          $sender_mobile_number='';
         }else{
          $sender_company=$sender_company_details['crm_company_name'];
          $sender_mobile_number=$sender_company_details['crm_mobile_number'];
         }


    		     $sender_company_mailid=$pro['sender_company_mailid'];
             $proposal_name=$pro['proposal_name'];
             $company=$pro['company_id'];
              if($company!=1){
                $company_name=$pro['company_name'];
              }else{
                $company_name=$pro['receiver_mail_id'];
              }


             $sender_details=$this->Common_mdl->select_record('proposals','id',$id);
             $sender_id=$sender_details['company_id'];  

             if($sender_id!=1){
              $user_details=$this->Common_mdl->select_record('user','id',$sender_id);
                $client_details=$this->Common_mdl->select_record('client','user_id',$sender_id);
                $client_website=$client_details['crm_company_url'];
                $client_mobile_number=$client_details['crm_mobile_number'];
                $email=$user_details['crm_email_id']; 
                $name=$user_details['crm_name']; 
                $last_name=$user_details['crm_last_name']; 
                $country=$user_details['crm_country'];
                $city=$user_details['crm_city'];
                $religion=$user_details['crm_state'];   
         	}else{
         		 $email=$sender_details['receiver_mail_id']; 
                $client_website='';
                $client_mobile_number='';
                $name=''; 
                $last_name=''; 
                $country='';
                $city='';
                $religion=''; 
         	}


           $proposal_content=$this->Common_mdl->select_record('proposal_proposaltemplate','title','decline_proposal_sender');

          $pdf_signature=$this->Common_mdl->select_record('pdf_signatures','proposal_no',$id);

          if($pdf_signature[0]==''){
            $email_signature='';
          }else{
            $email_signature='<img src='.$pdf_signature['signature_path'].'>';
          }



          $body1=$proposal_content['body'];
          $subject=$proposal_content['subject'];

          $random_string=$this->generateRandomString();
          $link=base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id;
        //  echo $link;
           $a1  =   array('::Client Name::'=>$name,
            '::Client Company::'=>$company_name,
            '::Client Company City::'=>$city,
            '::Client Company Country::'=>$country,
            '::Client Company Religion::'=>$religion,
            '::Client First Name::'=>$name,
            '::Client Last Name::'=>$last_name,
            '::Client Name::'=>$name,
            '::ClientPhoneno::'=> $client_mobile_number,
            '::ClientWebsite::'=>$client_website,
            ':: ClientProposallink::'=>$link,
            ':: ProposalCurrency::'=>$currency,
            ':: ProposalExpirationDate::'=>$expiration_date,
            ':: ProposalName::'=>$proposal_name,
            ':: ProposalCodeNumber::'=>$proposal_no,
            '::SenderCompany::'=>$sender_company,
            '::SenderCompanyAddress::'=>'',
            '::SenderCompanyCity::'=>$sender_city,
            '::SenderCompanyCountry::'=>$sender_country,
            '::SenderCompanyReligion::'=>$sender_religion,
            '::SenderEmail::'=>$sender_email,
            '::SenderEmailSignature::'=>$email_signature,
            '::SenderName::'=>$sender_name,
            '::senderPhoneno::'=>$sender_mobile_number,
            '::ProposalTitle::'=>$proposal_name,
            '::Date::'=>date("Y-m-d h:i:s"));




          $proposal_contents  =   strtr($body1,$a1); 
          $subject  =   strtr($subject,$a1);
          $random_string=$this->generateRandomString();
          $body= $proposal_contents;
            // $body  =   '<p>Congratulations!</p><br/><p>' .$proposal_name.'proposal was accepted by'.$company_name.' at'.date("Y-m-d h:i:s").'</p><br/><br/><p>Use Following link to open proposal '.base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id.'</p> <br/> Thanks';
            $email_subject= $proposal_name.'proposal was accepted by'.$company_name.'';
            $this->load->library('email');
            $this->email->set_mailtype("html");
            $this->email->from($email);
            $this->email->to($sender_company_mailid);
           $this->email->subject(preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($subject))));
            $this->email->message($body);
            $send = $this->email->send();


          $proposal_content=$this->Common_mdl->select_record('proposal_proposaltemplate','title','decline_proposal_client');

          $body1=$proposal_content['body'];
          $subject=$proposal_content['subject'];

          $random_string=$this->generateRandomString();
          $link=base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id;
        //  echo $link;
           $a1  =   array('::Client Name::'=>$name,
            '::Client Company::'=>$company_name,
            '::Client Company City::'=>$city,
            '::Client Company Country::'=>$country,
            '::Client Company Religion::'=>$religion,
            '::Client First Name::'=>$name,
            '::Client Last Name::'=>$last_name,
            '::Client Name::'=>$name,
            '::ClientPhoneno::'=> $client_mobile_number,
            '::ClientWebsite::'=>$client_website,
            ':: ClientProposallink::'=>$link,
            ':: ProposalCurrency::'=>$currency,
            ':: ProposalExpirationDate::'=>$expiration_date,
            ':: ProposalName::'=>$proposal_name,
            ':: ProposalCodeNumber::'=>$proposal_no,
            '::SenderCompany::'=>'invoice due date',
            '::SenderCompanyAddress::'=>'invoice due date',
            '::SenderCompanyCity::'=>$sender_city,
            '::SenderCompanyCountry::'=>$sender_country,
            '::SenderCompanyReligion::'=>$sender_religion,
            '::SenderEmail::'=>$sender_email,
            '::SenderEmailSignature::'=>$email_signature,
            '::SenderName::'=>$sender_name,
            '::senderPhoneno::'=>$sender_mobile_number,
            '::ProposalTitle::'=>$proposal_name,
            '::Date::'=>date("Y-m-d h:i:s"));



          $proposal_contents  =   strtr($body1,$a1); 
          $subject  =   strtr($subject,$a1);
          $random_string=$this->generateRandomString();
          $body= $proposal_contents;



          //   $random_string=$this->generateRandomString();
          //  $body  =   '<p>Congratulations! you have accepted the </p><br/><p>"' .$proposal_name.'"at'.date("Y-m-d h:i:s").'</p><br/><br/><p>Use Following link to open proposal '.base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id.'</p> <br/> Thanks';
            $email_subject= $proposal_name.'proposal was accepted';
            $this->load->library('email');
            $this->email->set_mailtype("html");
            $this->email->from($sender_company_mailid);
            $this->email->to($email);
            $this->email->subject(preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($subject))));
            $this->email->message($body);
            $send = $this->email->send();



            //  $random_string=$this->generateRandomString();
            // $body  =   '<p>' .$proposal_name.'proposal was declined by'.$company_name.' at'.date("Y-m-d h:i:s").'</p><br/><p>Use Following link to open proposal '.base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id.'</p> <br/> Thanks';
            // $email_subject= $proposal_name.'proposal was declined by'.$company_name.'';
            // $this->load->library('email');
            // $this->email->set_mailtype("html");
            // $this->email->from($email);
            // $this->email->to($sender_company_mailid);
            // $this->email->subject($email_subject);
            // $this->email->message($body);
            // $send = $this->email->send();

            //  $random_string=$this->generateRandomString();
            // $body  =   '<p>' .$proposal_name.'proposal was declined at'.date("Y-m-d h:i:s").'</p><br/><p>Use Following link to open proposal '.base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id.'</p> <br/> Thanks';
            // $email_subject= $proposal_name.'proposal was declined by'.$company_name.'';
            // $this->load->library('email');
            // $this->email->set_mailtype("html");
            // $this->email->from($sender_company_mailid);
            // $this->email->to($email);
            // $this->email->subject($email_subject);
            // $this->email->message($body);
            // $send = $this->email->send();
           // echo $send;
    	}

         $data=array('proposal_id'=>$id,'tag'=>$status,'created_at'=>date('Y-m-d h:i:s'));
        $inse=$this->Common_mdl->insert('proposal_history',$data);

    	$data=array('status'=>$status);
    	$update=$this->Common_mdl->update('proposals',$data,'id',$id);
        $update=1;
    	echo json_encode($update);
    }

public function send_proposal(){

        $proposal_id=$_POST['proposal_id'];
        $data=array('status'=>'sent');
      $update=$this->Common_mdl->update('proposals',$data,'id',$proposal_id);


        $proposal_details=$this->db->query("select * from proposals where id='".$proposal_id."'")->row_array(); 
        $data=array('proposal_id'=>$proposal_details['id'],'tag'=>'Sent','created_at'=>date('Y-m-d h:i:s'));
        $inse=$this->Common_mdl->insert('proposal_history',$data);

        if($proposal_details['receiver_mail_id']==''){
          $emailid=$this->db->query("SELECT `crm_email` FROM `client` where `id` = '".$_POST['company_id']."'")->row_array();
            $email=$emailid['crm_email'];
          }else{
            $email=$proposal_details['receiver_mail_id'];
          }
          $from=$proposal_details['sender_company_mailid'];

        $email_subject=$proposal_details['proposal_name'].' from '.$proposal_details['sender_company_name'];
        $body1=$proposal_details['proposal_mail'];
        $filename=FCPATH.'uploads/proposal_pdf/'.$proposal_details['pdf_file'];

       // $file_name= FCPATH.'/cv_uploads/'.$up_data['file_name'];
        //echo $filename;
        $random_string=$this->generateRandomString();
        $a1  =   array('http://remindoo.org/CRMTool/proposals/'=> base_url().'Proposal/proposal/'.$random_string.'---'.$proposal_details['id'].'');
            $body  =   strtr($body1,$a1); 
          $this->email->set_mailtype("html");
        $this->email->from($from);
        $this->email->to($email);
        $this->email->subject($email_subject);
        $this->email->message($body);
        $this->email->attach($filename);

        if($proposal_details['document_attachment']!=''){
          $attaching=explode(',',$proposal_details['document_attachment']);
          for($i=0;$i< count($attaching);$i++){
            $this->email->attach($attaching[$i]);
          }
        }


        if($proposal_details['attachment']!=''){
        $attachment=explode(',',$proposal_details['attachment']);
       // echo '<pre>';
        //print_r($attachment); 
       $files=FCPATH.'attachments/'.$attachment[$i];
       // exit;     
          for($i=0;$i< count($attachment);$i++){
            $this->email->attach($files);
          }         
        } 

       // echo $proposal_details['pdf_file'];       
        $send = $this->email->send(); 

        echo json_encode($send);       
      } 
      function invoive_create($id){
       // echo "sug";
      //  echo $id;

         $pro=$this->Common_mdl->select_record('proposals','id',$id); 


       //  $user_id=$this->db->query("select user_id from client where id='".$pro['company_id']."'")->row_array();


         $invoice_insert=array('user_id'=>$_SESSION['id'],'lead_id'=>$pro['lead_id'],'proposal_id'=>$pro['id'],'from_id'=>$pro['sender_company_mailid'],'to_id'=>$pro['company_id'],'item_name'=>$pro['item_name'],'qty'=>$pro['qty'],'price'=>$pro['price'],'description'=>$pro['description'],'tax'=>$pro['tax'],'discount'=>$pro['discount'],'tax_option'=>$pro['tax_option'],'discount_option'=>$pro['discount_option'],'tax_amount'=>$pro['tax_amount'],'discount_amount'=>$pro['discount_amount'],'date'=>date('d-m-Y'),'created_at'=>date('Y-m-d h:i:s'),'status'=>'draft', 'product_name'=>$pro['product_name'],
          'product_price'=>$pro['product_price'],
          'product_description'=>$pro['product_description'],
          'product_qty'=>$pro['product_qty'],
          'subscription_name'=>$pro['subscription_name'],
          'subscription_price'=>$pro['subscription_price'],
          'subscription_unit'=>$pro['subscription_unit'],
          'subscription_description'=>$pro['subscription_description'],'product_tax'=>$pro['product_tax'],
          'product_discount'=>$pro['product_discount'],
          'subscription_tax'=>$pro['subscription_tax'],
          'subscription_discount'=>$pro['subscription_discount'],'invoice_no'=>mt_rand(100000,999999),'currency'=>$pro['currency']);         

          $inserting=$this->Common_mdl->insert('invoices',$invoice_insert); 

          $da_ta=array('draft_invoice'=>$this->db->insert_id());

          $update=$this->Common_mdl->update('proposals',$da_ta,'id',$id);



      }


      public function pricelist(){
         $this->Security_model->chk_login();
        $user_id=$_SESSION['id'];

               $data['admin_settings']=$this->Common_mdl->select_record('admin_setting','user_id',$user_id);
               $data['pricelist']=$this->Common_mdl->GetAllWithWhere('proposal_pricelist','user_id',$user_id);
               $data['records']=$this->Common_mdl->select_record('pricelist_settings','user_id',$user_id);
         
          $this->load->view('proposal/price_list_settings',$data);

      }

      public function insert_pricelist(){
        $data=array('tax_option'=>$_POST['tax'],
          'discount_option'=>$_POST['discount'],'currency'=>$_POST['currency_symbol'],'pricelist'=>$_POST['pricelist'],'user_id'=>$_SESSION['id']);
       

        $insert=$this->Common_mdl->insert('pricelist_settings',$data);

        redirect('proposals/pricelist');
      }

        public function update_pricelist(){
        $data=array('tax_option'=>$_POST['tax'],
          'discount_option'=>$_POST['discount'],'currency'=>$_POST['currency_symbol'],'pricelist'=>$_POST['pricelist'],'user_id'=>$_SESSION['id']);       

        $insert=$this->Common_mdl->update('pricelist_settings',$data,'id',$_POST['id']);

        redirect('proposals/pricelist');
      }


      
  


		
}
?>