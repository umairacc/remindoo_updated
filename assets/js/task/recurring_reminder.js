 $(document).ready(function(){

 $('#myModal input, select').on('change', function(event){
          event.preventDefault();

          $('.Save_Schedule').addClass('disabled');

          var empty_flds1 = 0;
          //alert($(".required1").length);
          $(".required1").each(
          function() 
          {       
            if(!$.trim($(this).val())) 
            {    
              empty_flds1++;
              //var input_id=$(this).attr('id');
              // console.log(input_id);
              $(this).css('border', '1px solid red');
            }
            else 
            {
              $(this).css('border', '1px solid rgba(0,0,0,.15)');
            } 
          });

          //console.log(empty_flds1);



          
         
         var every = $('#repeats').val(); //select box
         var day_word = $('.repeats').attr('data-date'); //Tuesday
         var date =  $('.repeats').attr('data-day');     //6
         var month = $('.repeats').attr('data-month');   //aug
         var time =  $('#timepicker1').val();
         var word =  $("#every").val(); //every


        if( every == 'week') 
        {
          var Fromdate =  $(this).attr("data-date")
          if(Fromdate!='')
          {
            $('.dayss[value="'+Fromdate+'"]').prop('checked',true);
          }

          var  week_repeat = [];
          $(".dayss").each(
            function()
            {
              if($(this).is(":checked"))
              {
                week_repeat.push($(this).val());
              }
            }
          );
          if(!week_repeat.length)
          {
            $(".empty-week-day").text("Choose Atlest One Option");
            empty_flds1++;
          }
          else 
          {
            $(".empty-week-day").text(' ');
          }
        }
        

          if(empty_flds1==0){
          $('.Save_Schedule').removeClass('disabled');
        var end_on_summary = '<p>No End Date</p>';
        var end_type = $("input[name='end']:checked").val();
        if(end_type == "after")
        {
          end_on_summary="<p>End On After Recurring "+ $("#message").val() + " Tasks</p>";
        }
        else if(end_type == "on")
        {
          var elm = $("#modalendDate");
          var date_text = elm.attr("data-day")+"-" +elm.attr("data-month")+"-" +elm.attr("data-year");
          end_on_summary="<p>End On "+date_text +" </p>";
        }

        if(every=='week')
        {
           $('#summary').html('Every'+' '+ word +' '+every+','+'on '+ week_repeat.join() +' at '+time+end_on_summary);
           $('#summary_new').html('<p>Every'+' '+every+','+'on '+ week_repeat.join() +' at '+time+"</p>"+end_on_summary);
           $('#recuring_sus_msg').val('Every'+' '+every+','+'on '+ week_repeat.join() +' at '+time+end_on_summary);
        } 
        else if(every=='day')
        {
          $('#summary').html('Every'+' '+ word +' '+every+','+' at '+time+end_on_summary);
          $('#summary_new').html('<p>Every'+' '+ word +' '+every+','+' at '+time+"</p>"+end_on_summary);
          $('#recuring_sus_msg').val('Every'+' '+ word +' '+every+','+' at '+time+end_on_summary);
        }
        else if(every=='month')
        {
          $('#summary').html('Every'+' '+ word +' '+every+','+'on day '+date+' at '+time+end_on_summary);
          $('#summary_new').html('<p>Every'+' '+ word +' '+every+','+'on day '+date+' at '+time+"</p>"+end_on_summary);
          $('#recuring_sus_msg').val('Every'+' '+ word +' '+every+','+'on day '+date+' at '+time+end_on_summary);
        }
        else if(every=='year')
        {
          $('#summary').html('Every'+' '+ word +' '+every+','+'on '+month+' '+date +' at '+time+end_on_summary);
          $('#summary_new').html('<p>Every'+' '+ word +' '+every+','+'on '+month+' '+date +' at '+time+"</p>"+end_on_summary);
          $('#recuring_sus_msg').val('Every'+' '+ word +' '+every+','+'on '+month+' '+date +' at '+time+end_on_summary);
        }

     /*$('.dashboard_success_message1').show();
      setTimeout(function(){ $('.dashboard_success_message1').hide();  
        $('#myModal').modal('hide');
         $('.modal-backdrop.show').hide(); }, 1000);*/

        // var modalFromDate=$('#modalFromDate').val($.datepicker.formatDate('dd M yy', new Date()));
   
    }
             
         });





        $('#myModalone input').on('change', function(event){
          var empty_flds = 0;

          $(".required_check").each(function() {  

            //  alert($(this).attr('type'));     
          if(!$.trim($(this).val()))
          {
            empty_flds++;
            //var input_id=$(this).attr('id');
            // console.log(input_id);
            $(this).css('border', '1px solid red');
          }
          else
          {
            $(this).css('border', '1px solid rgba(0,0,0,.15)');
          }

          });
          //console.log(empty_flds);
        if(empty_flds==0)
        {
          
        event.preventDefault();

         var every=$('#specific_time').val();
         var time=$('#timepicker2').val();

                          var new_array=[];                            
                            $('input[class="remind_poup_data"]:checked').each(function() {
                                 new_array.push(this.value);
                              });
                            if(new_array.length)
                            {
                               $('.remider_option_error').html('');

                            var for_reminder_chk = new_array.join(','); 
                            $.ajax({
                              url : base_url+"Tasksummary/remider_summary_task",
                              type : 'POST',
                              dataType : 'json',
                              data : {'date':every,'time':time,'remainder_data':for_reminder_chk},
                              beforeSend :function()
                              {
                                $(".LoadingImage").show();
                              },
                              success : function(res)
                              { $(".LoadingImage").hide();
                                var text='';
                                for (x in res) 
                                {
                                  var data = res[x].split("//");
                                  text += "<p> "+ data[0] + " At " + data[1] +"</p>";
                                }

                                $('#summary_val').html(text);
                                $('#summary_new').html(text);
                                $('#reminder_msg').val(text);
                              /*$('.dashboard_success_message1').show();
                              
                              setTimeout(function(){ $('.dashboard_success_message1').hide();  
                                $('#myModalone').modal('hide');
                                 $('.modal-backdrop.show').hide(); }, 1500);*/

                              }
                            });

                            
                            }
                            else
                            {
                              $('.remider_option_error').html("Please Choose at leats one option");
                            }
          }         
         });

        $("#every ,#message").on("keyup",function(){
      
      $(this).val( Math.ceil( $(this).val() ) );
      if($(this).val() < 1)
      {
        $(this).addClass("required1");
      }

     });


          $( "#modalFromDate, #modalendDate" ).datepicker({   
        dateFormat:'dd-mm-yy',
        minDate:0,
        changeMonth: true,
        changeYear: true,
        onSelect: function(dateText){
         var seldate = $(this).datepicker('getDate');
         var day = $(this).datepicker('getDate').getDate();
         seldate = seldate.toDateString();
         //alert(seldate);
         seldate = seldate.split(' ');
         var weekday=new Array();
             weekday['Mon']="Monday";
             weekday['Tue']="Tuesday";
             weekday['Wed']="Wednesday";
             weekday['Thu']="Thursday";
             weekday['Fri']="Friday";
             weekday['Sat']="Saturday";
             weekday['Sun']="Sunday";
         var dayOfWeek = weekday[seldate[0]];

         var elm_id=$(this).attr("id");

          if(elm_id=="modalFromDate")
          {
            $('#repeats').attr('data-date',dayOfWeek);
            $('#repeats').attr('data-day',day);
            $('#repeats').attr('data-month',seldate[1]);
            $('.dayss').prop('checked', false);
            $('.dayss[value='+dayOfWeek+']').prop('checked', true);
            console.log( $('.dayss:checked').length +"selected day of week");
            $("#modalendDate").datepicker("option","minDate", dateText)

          } 
          else if(elm_id=="modalendDate")
          {
            $(this).attr('data-year',seldate[3]);
            $(this).attr('data-day',day);
            $(this).attr('data-month',seldate[1]);
            $("#modalFromDate").datepicker("option","maxDate", dateText)
          }  
         $(this).trigger('change');
          
         //$('#summary').html(dayOfWeek);
     }
   });

           $('#specific_time').datepicker({ dateFormat: 'dd-mm-yy',minDate:0, changeMonth: true,
        changeYear: true,onSelect: function(dateText){$(this).trigger('change');} }).val();

$("input[name='end']").change(function()
   {

    $(this).closest("div.forms-option").find('input[name="message"], input[name="sche_on_date"]').each(function(){

      $(this).removeClass("required1");      
      $(this).css('border', '1px solid rgba(0,0,0,.15)');
      //$(this).val(' ');
    });

      if( $(this).val()=="after" )
      {
        if( $("input[name=message]").val() == '' )
        {
          $("input[name=message]").addClass("required1");
        }

        $("input[name=sche_on_date]").val('');
      }
      else if($(this).val()=="on" )
      {
        if( $("input[name=sche_on_date]").val() == '')
          {
            $("input[name=sche_on_date]").addClass("required1");
          }  

        $("input[name=message]").val('');
      }
      else
      {
        $("input[name=message]").val('');
        $("input[name=sche_on_date]").val('');  
      }

      
 });
$('.recurring-msg .close').click(function(){

    $('.recurring-msg').find('input').css('border', '1px solid rgba(0,0,0,.15)');
    $('.recurring-msg').find('input[type="text"]').val('');
    $( "#modalFromDate" ).datepicker( "option", "maxDate", null);
    $( "#modalendDate" ).datepicker( "option", "minDate", null);
    $('.recurring-msg').find("input[value='noend']").trigger('click');
    $('.recurring-msg').find("select.repeats").val('day');
    $('.recurring-msg').find("#summary").html('');
    $('.recurring-msg').find('input[name="every"]').val(1).trigger('keyup');
  });

$('.recurring-msg .Save_Schedule').click(function(){
  $('.recurring-msg').modal('hide');
});
  
         });