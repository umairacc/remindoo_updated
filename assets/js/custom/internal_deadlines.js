$('body').on('change', '.ds', function() {
    $.ajax({
        url  : `${window.location.origin}/InternalDeadlines/update_ds`,
        type : 'POST',
        data : {
            'id' : $(this).attr('data-id'),
            'ds' : this.value
        },
        success: function(data) {
            $('.toast').toast('show');
        },
    });
});

$('body').on('change', '.idl', function() {
    $.ajax({
        url: `${window.location.origin}/InternalDeadlines/update_idl`,
        type : 'POST',
        data : {
            'id'  : $(this).attr('data-id'),
            'idl' : this.value
        },
        success: function(data) {
            $('.toast').toast('show');
        },
    });
});

$('body').on('change', '.edl', function() {
    $.ajax({
        url: `${window.location.origin}/InternalDeadlines/update_edl`,
        type : 'POST',
        data : {
            'id'  : $(this).attr('data-id'),
            'edl' : this.value
        },
        success: function(data) {
            $('.toast').toast('show');
        },
    });
});

$('body').on('change', '.progress_status', function() {
    curObj = $(this);
    $.ajax({
        url: `${window.location.origin}/InternalDeadlines/update_due_dates`,
        type : 'POST',
        dataType: 'json',
        data : {            
            'task_id'   : $(this).attr('data-id'),
            'status_id' : this.value,
            'serv_id'   : $(this).attr('data-service'),
        },
        success: function(data) {
            console.log(data);            
            $.each(data, function(index, element) {
                curObj.parent().parent().find('.int-due-dt').text(element.internal_due_date);
                curObj.parent().parent().find('.int-due-dt-delay').text(element.internal_delay);
                curObj.parent().parent().find('.ext-due-dt').text(element.external_due_date);
                curObj.parent().parent().find('.ext-due-dt-delay').text(element.external_delay);                
            });            
        },
    });
});