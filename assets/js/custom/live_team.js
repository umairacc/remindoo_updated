$(document).ready(function(){
    // Initial load
    live_team();
    
    // Auto refresh on every min
    setInterval(function(){
      live_team();      
    }, 60000);    
    
    function live_team(){
      $.ajax({
        url: `${window.location.origin}/LiveTeam/get_live_team_report`,
        type : 'POST',
        success: function(data) {
          $('#live_team_report').empty();
          $('#live_team_report').append(data);                    
          update_loading_status();
          performance_analytics();
        },
      });
    }
    function performance_analytics(){
      
      $.ajax({
        url: `${window.location.origin}/LiveTeam/get_performance_analytics`,
        type : 'POST',
        success: function(data) {          
          var active_tab = $('#active_tab').val();
          $('#performance_analytics').empty();
          $('#performance_analytics').html('');
          $('#performance_analytics').append(data);
          $('#'+active_tab).trigger('click');
          update_loading_status();
        },
      });
    }
    function update_loading_status(){
      $loading_status = $('#loading_status').val();
      $loading_status = parseInt($loading_status)+1;
      $('#loading_status').val($loading_status);
      
      if($loading_status == 2){
        $('.team-loader').hide();
      }
    }
});

$('body').on('click', '.tab-set', function() {
  $('#active_tab').val($(this).attr('id'));
});

$('body').on('click', '.mdl-task-details', function() {
  var details = JSON.parse($(this).attr('data-details')),
      html    = "",
      i       = 1;  
  
  html +=   "<table class='tbl-task-details'>";    
  html +=     "<tr>";    
  html +=       "<th>SL</th>";    
  html +=       "<th>Subject</th>";    
  html +=       "<th>Client</th>";    
  html +=       "<th>Time Spent</th>";    
  html +=     "</tr>";    
  $.each(details, function (key, val) {     
    html +=     "<tr>";
    html +=       "<td>"+i+"</td>";
    html +=       "<td><a href='/user/task_details/"+val.ID+"'>"+(val.SUBJECT).replaceAll("|", "'")+"</a></td>";
    html +=       "<td>"+(val.CLIENT_NAME).replaceAll("|", "'")+"</td>";
    html +=       "<td>"+(val.TIME_SPENT).replaceAll("|", "'")+"</td>";
    html +=     "</tr>";    
    i++;
  });
  html +=   "<table>";  
  
  $('.modal-body').empty();
  $('.modal-body').append(html);
}); 

function cust_decode(data){
  return data.replace(/|/g, "'");
}