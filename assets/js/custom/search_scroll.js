$(document).ready(function () {
	var checkExist_100 = setInterval(function () {
		if ($('.comboTreeDropDownContainer').length) {
			// $('.comboTreeDropDownContainer').prepend('<input type="text" class="search_assign_top">');
			clearInterval(checkExist_100);
		}
	}, 100); // check every 100ms    

	$.extend($.expr[":"], {
		"containsIN": function (elem, i, match, array) {
			return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
	});
});

$(document).on('keyup', ".search_assign_top", function () {
	$(this).parent().scrollTop(0);
	var thisid = $(this).parent().attr('id');
	var user_input = $(this).val().toLowerCase(),
		scrollTop = $(this).parent().scrollTop();

	$(this).parent().find('li').each((id, elem) => {
		var inner_text = elem.innerText.toLowerCase();

		if (user_input.trim() == inner_text.trim()) {
			var pos = $('#' + thisid).find("*:containsIN('" + inner_text + "'):last").position();
			$('#' + thisid).scrollTop(scrollTop + pos.top - 40);
		}
	});
});