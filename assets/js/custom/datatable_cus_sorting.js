(function (factory) {
  if (typeof define === "function" && define.amd) {
    define(["jquery", "moment", "datatables.net"], factory);
  } else {
    factory(jQuery, moment);
  }
}(function ($, moment) {

$.fn.dataTable.moment = function ( format, locale, reverseEmpties ) {
  var types = $.fn.dataTable.ext.type;

  // Add type detection
  types.detect.unshift( function ( d ) {

   // console.log("isDate"+ d );

    if ( d === '' || d === null ) {
      return 'moment-'+format;
    }

    return moment( d, format, locale, true ).isValid() ?
      'moment-'+format :
      null;
    } );

  // Add sorting method - use an integer for the sorting
  types.order[ 'moment-'+format+'-pre' ] = function ( d ) {
    return d === '' || d === null ?
      -Infinity :
      moment( d, format, locale, true ).unix();
  };
};

}));