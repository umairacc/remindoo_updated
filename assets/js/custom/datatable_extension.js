var ExportCells_Text = {
                exportOptions: {
                    format: {
                        header: function (data, column, row, node) {
                          
                            var label = $("<div>"+data+"</div>").contents().filter(function(){ return this.nodeType == 3; }).text().trim();
                         //   console.log(label);
                            return label;
                       },
                       body: function ( data, row, column, node ) {
                                if( typeof $(node).data('search') !== 'undefined')
                                {
                                    data = $(node).data('search');
                                }                 
                                return data;
                        }   
                    }
                }
            };

var DataTable_Export_Buttons =[$.extend(true, {}, ExportCells_Text, {
                                    text:'<i class="fa fa-file-pdf-o" aria-hidden="true"></i>Pdf',
                                    extend: 'pdfHtml5',                                   
                                    orientation: 'landscape', //portrait
                                    pageSize: 'A4',
                                    exportOptions: 
                                    {
                                        columns: [':visible:not(.select_row_TH, .action_TH, .SELECT_ROW_TH, .ACTION_TH )' ]
                                    },                                    
                                    customize: function (doc) {                                  
                                    doc.pageMargins = [20,60,20,30];
                                    // Set the font size fot the entire document
                                    doc.defaultStyle.fontSize = 7;
                                    // Set the fontsize for the table header
                                    doc.styles.tableHeader.fontSize = 7;            
                                    // To use predefined layouts uncomment the line below and comment the custom lines below
                                    // doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
                                    var objLayout = {};
                                    objLayout['hLineWidth'] = function(i) { return .5; };
                                    objLayout['vLineWidth'] = function(i) { return .5; };
                                    objLayout['hLineColor'] = function(i) { return '#aaa'; };
                                    objLayout['vLineColor'] = function(i) { return '#aaa'; };
                                    objLayout['paddingLeft'] = function(i) { return 4; };
                                    objLayout['paddingRight'] = function(i) { return 4; };
                                    doc.content[0].layout = objLayout;
                                    }
                                   
                                }),
                            $.extend(true, {}, ExportCells_Text, {
                                    text  : '<i class="fa fa-file-excel-o" aria-hidden="true"></i>Csv',
                                    extend: 'csv',
                                    exportOptions: 
                                    {
                                        columns: [':visible:not(.select_row_TH, .action_TH, .SELECT_ROW_TH, .ACTION_TH)' ]
                                    }
                                }),  
                             $.extend(true, {}, ExportCells_Text, {
                                    text:'<i class="fa fa-print" aria-hidden="true"></i>Print',
                                    extend: 'print',
                                    exportOptions: 
                                    {
                                        columns: [':visible:not(.select_row_TH, .action_TH, .SELECT_ROW_TH, .ACTION_TH)' ]
                                    }
                                    

                                }), 
                                
                            ];

function Change_Sorting_Event ( Table_Instance )
{
	var Table = Table_Instance;
	Table.columns().every( function (i) {

        var header = $(this.header());
        header.unbind('click.DT');
        var icon = header.find('.sortMask');
        icon.unbind('click.DT');
        if(icon.length)
        {
          Table.order.listener(icon, this.index() );
          //console.log('fnSortListener'+i);
        }
   });
	
}


function Hide_Child_Rows ( Table_Instance )
{ 
	var Table = Table_Instance;
	Table.rows().every( function (){

		if ( this.child.isShown() )
		{
            this.child.hide();
            
            $(this.node()).removeClass('shown');
        } 
	});
}


function ColReorder_Backend_Update ( Table_Instance , Table_Const ,IsSuperAdmin = 0 )
{
	var Table = Table_Instance;

	Table.on( 'column-reorder.dt' , function ( e, settings, details )
	{	
		  Hide_Child_Rows ( Table );
    	Change_Sorting_Event( Table ); 
    	var Reorder = [];
    	Table.columns().every( function()
    	{
    		var Cname = $(this.header()).attr('class').match(/\w+(?=_TH)/);
    		Reorder.push( Cname[0] );
		  });
       if( IsSuperAdmin )
      {
        var URL = base_url+"Super_admin/Update_column_settings_order";
      }
      else
      {    
        var URL = base_url+"Firm/Update_firm_column_settings_order";
      } 
	$.ajax(
		{
			url  		: URL,
			type 		: 'POST',
			data 		: { 'table_name' : Table_Const , 'reorder' : Reorder },
			beforeSend	: function()
			{
			    $('.LoadingImage').show();
			},
			success		: function(res)
			{
			    //console.log(res);
			    $('.LoadingImage').hide();
			}
		}
		);

  	});
}


function ColVis_Backend_Update ( Table_Instance , Table_Const , IsSuperAdmin = 0 )
{
	var Table = Table_Instance;

	Table.on('column-visibility.dt', function ( e, settings, column, state ) {
	//console.log("event  trigger");
	var TH = Table.column(column).header();

	var Col_Const = $(TH).attr('class').match(/\w+(?=_TH)/);

	var State = (state ? 1 : 0);
   
   if( IsSuperAdmin )
   {
      var URL = base_url+"Super_admin/Update_column_settings_visibility";
   }
   else
   {    
      var URL = base_url+"Firm/Update_firm_column_settings_visibility";
   } 

	$.ajax(
    {
      url			: URL,
      type			: 'POST',
      data			: {'table_name':Table_Const,'state':State,'column':Col_Const[0]},
      beforeSend	: function()
      {
        $('.LoadingImage').show();
      },
      success		: function(res)
      {
        //console.log(res);
        $('.LoadingImage').hide();
      }
	});

	});
}


function Filter_IconWrap()
{
	$('th .themicond').on('click',function(e) {
    //console.log( "inside Filter_IconWrap()");
    //console.log( "length of dropdown"+$(this).parent().find(".dropdown-content").length );
      
    if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) 
    {
      $(this).parent().find('.dropdown-content').removeClass('Show_content');
    }
    else
    {
       $('.dropdown-content').removeClass('Show_content');
       $(this).parent().find('.dropdown-content').addClass('Show_content');
    }

    $(this).parent().find('.select-wrapper').toggleClass('special');

    if( ! $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1'))
    {
      $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
      $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
    }
    });
}


function ColVis_Hide ( Table_Instance , Hide_Col)
{
	$.each( Hide_Col ,function(i,v){Hide_Col[i]='.'+v;});
    Table_Instance.columns( Hide_Col ).visible(false);
    Table_Instance.draw();
}

function Redraw_Table( Table_Instance ,Draw = 1)
{ //console.log( 'enter Redraw_Table' );
  
   Table_Instance.columns().every(function(){

    $(this.header()).find('.dropdown-content li').each(function(){

      if($(this).find('input:checked').length)
      {
            $(this).trigger('click');    
      }

    });
    //$(this.header()).find('.dropdown-content li').removeClass('selected');
    //console.log( $(this.header()).find('.dropdown-content input[type="checkbox"]').prop('checked')+"clear ");

    //$(this.header()).find('select.filter_check option').prop('selected',false);
    
   // $(this.header()).find('select.filter_check').trigger('change');

    //console.log($(this.header()).find('select.filter_check').val()+'selectboxval');
   });
   Table_Instance
   .search( '' )
   .columns().search( '' );
   
   if( Draw ) Table_Instance.draw();

  //console.log( 'Leave Redraw_Table' );
}

function AddColumn_Filters(e) 
{
        var api = this.api();

              api.columns('.hasFilter').every( function () {

                        var column = this;
                        var TH = $(column.header());
                        var Filter_Select = TH.find('.filter_check');

                    if( !Filter_Select.length )
                    {

                      Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo( TH );
                      var unique_data = [];
                        column.nodes().each( function ( d, j ) { 
                          var dataSearch = $(d).attr('data-search');
                          
                            if( jQuery.inArray(dataSearch, unique_data) === -1 )
                            {
                              //console.log(d);
                              Filter_Select.append( '<option  value="'+dataSearch+'">'+dataSearch+'</option>' );
                              unique_data.push( dataSearch );
                            }

                        });
                    }
                    

                    Filter_Select.on( 'change', function () {

                        config_filter_section( $(this) );

                        var search =  $(this).val(); 
                        //console.log( search );
                        var search_type =  $(this).data('search-type');
                        if( search_type == "multiple_value")
                        {
                          if(search.length) search= search.join('|'); 
                        }
                        else
                        {
                          if(search.length) search= '^('+search.join('|') +')$'; 
                        }

                       var  class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);  

                       var cur_column = api.column( '.'+class_name+'_TH' );
                       
                        cur_column.search( search, true, false ).draw();
                    } );
                      
                        //console.log(select.attr('style'));
                        Filter_Select.formSelect(); 

                      }); 
                    
         
}

$(document).on('click' , '.filter-data.for-reportdash .box-container .remove' ,function()
{
  $( $(this).attr('data-targetth') ).find('.dropdown-content li').eq( $(this).attr('data-index') ).trigger('click');
});

function config_filter_section( select )
{
  console.log("inside the filter action ");
  var  class_name = select.closest('th').attr('class').match(/\w+(?=_TH)/);  
  class_name = '.'+class_name+'_TH';
                      

  $('.filter-data.for-reportdash .box-container').find('span.remove[data-targetth="'+class_name+'"]').each(function(){
    
    var is_last_box = $(this).closest('.box-container').find('.box-item').length;
    $(this).closest('.box-item').remove();

    if( is_last_box == 1 )
    {
      $('.filter-data.for-reportdash').hide();
      $('div.proposal-info').removeClass('adjust_settings_button');
    }
  });
  var select_val = select.find('option:selected');
  if( select_val.length )
  {
    $('.filter-data.for-reportdash').show();
    $('div.proposal-info').addClass('adjust_settings_button');
    
    select_val.each(function(){
      var i = $(this).index();
      var t = $(this).text();
      $('.filter-data.for-reportdash .box-container').append('<div class="btn btn-default box-item">'+t+'<span class="remove" data-targetth="'+class_name+'" data-index="'+i+'">x</span></div>');
    });
  }
}