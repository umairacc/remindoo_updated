/* Open modal to display email content */
$("body").on("click", ".view-mail-body", function() {
    var data = atob($(this).attr('data-email-body'));
    $('#email_body_content').html(data);        
});

/* Resend email notification */
$("body").on("click", ".resend-email", function() {    
    var email_to      = $(this).attr("data-email-to"),
        email_subject = $(this).attr("data-email-subject"),
        email_body    = $(this).attr("data-email-body");  
        record_key    = $(this).parent().parent().attr("data-record-key");

    $.ajax({
        type : 'POST',
        url  : window.location.origin+"/AwsMailer/ajax_email",
        data : {
            email_to      : email_to,
            email_subject : email_subject,
            email_body    : email_body,
            record_key    : record_key
        },
        //dataType: 'json',
        success: function(data) {
            alert("Email has been sent successfuly..");
            location.reload();
        }
    });
});