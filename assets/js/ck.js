
		'use strict';
		var CONTACTS = [
			{ name: 'Username' },
			{ name: 'Client Name' },
			{ name: 'Accountant Name' },
			{ name: 'Task Number' },
			{ name: 'Task Name' },
			{ name: ' Task Due Date'},
			{ name: ' Task Client' },
			{ name: 'Firm Name' },
			{ name: 'accountant number' },
			{ name: 'invoice amount' },
			{ name: 'firm name' },
			{ name: 'invoice due date' },			
		];
		CKEDITOR.disableAutoInline = true;		
		CKEDITOR.plugins.add( 'hcard', {
			requires: 'widget',
			init: function( editor ) {
				editor.widgets.add( 'hcard', {
					allowedContent: 'span(!h-card); a[href](!u-email,!p-name); span(!p-tel)',
					requiredContent: 'span(h-card)',
					pathName: 'hcard',
					upcast: function( el ) {
						return el.name == 'span' && el.hasClass( 'h-card' );
					}
				} );				
				editor.addFeature( editor.widgets.registered.hcard );				
				editor.on( 'paste', function( evt ) {
					var contact = evt.data.dataTransfer.getData( 'contact' );
					if ( !contact ) {
						return;
					}
					evt.data.dataValue =
						// '<span class="h-card">' +
						// 	'<a href="mailto:' + contact.email + '" class="p-name u-email">' + contact.name + '</a>' +
						// 	' ' +
						// 	'<span class="p-tel">' + contact.tel + '</span>' +
						// '</span>';
						'<span class="content h-card">' +
							'['+contact.name+']'  +
						'</span>';
				} );
			}
		} );

		CKEDITOR.on( 'instanceReady', function() {
			
			CKEDITOR.document.getById( 'contactList' ).on( 'dragstart', function( evt ) {
				
				var target = evt.data.getTarget().getAscendant( 'div', true );
				
				CKEDITOR.plugins.clipboard.initDragDataTransfer( evt );
				var dataTransfer = evt.data.dataTransfer;				
				dataTransfer.setData( 'contact', CONTACTS[ target.data( 'contact' ) ] );	
				dataTransfer.setData( 'text/html', target.getText() );				
			} );
		} );

		// Initialize the editor with the hcard plugin.
		// CKEDITOR.inline( 'editor1', {
		// 	extraPlugins: 'hcard,sourcedialog,justify'
		// } );

		$("#submit").click(function(){
			//alert('ok');

			//console.log($(".cke_wysiwyg_div").value);
			var $(".cke_wysiwyg_div > p").text();
			// var str = "";
			// $('.cke_widget_wrapper > span.h-card').each(function(){
			// 	str += $(this).html();				
			// });	
			// var myString=$("#editor1").html();

			// $("span.cke_reset").each(function () {
			// $(this).remove();
			// });

			// $("span.cke_widget_wrapper").each(function () {
			// $(this).replaceWith($(this).text());
			// });
			// var newString=$("#editor1").html();
			// console.log(newString);

		});