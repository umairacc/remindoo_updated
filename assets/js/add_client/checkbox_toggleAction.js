$(document).ready(function()
{ 

      $('.checkall').parents('tr').find('th:not(:nth-child(2)) .switchery').addClass('disabled');
  
      //$( "select[id^='invoice']" ).parent().dropdown();
   
      $(".checkall, .checkall_reminder, .checkall_text, .checkall_notification").change(function(){  
          //alert("selectall change");
         var child_class;

         if ( $(this).hasClass('checkall') ) child_class = 'services';
         else if ( $(this).hasClass('checkall_reminder') ) child_class = 'reminder';
         else if ( $(this).hasClass('checkall_text') ) child_class = 'text';
         else if ( $(this).hasClass('checkall_notification') ) child_class = 'invoice';
         //alert(child_class+"child_class")
        
        //$(".LoadingImage").show();         
  
                 var checked = $(this).prop('checked');

                  $("."+child_class ).each(function() {

                 // console.log($(this).prop('checked')+"!="+checked);

                     var id= $(this).attr("data-switcheryId");

                     if($(this).prop('checked')!=checked)
                     {
                        $(this).prop("checked",checked);                     
                        switcheryElm[id].setPosition();
                        switcheryElm[id].handleOnchange();
                        //console.log($(this).prop("checked")+"afetr plugin fun");
                     }                  
                  });                         
        // setTimeout(function(){ $(".LoadingImage").hide(); }, 1000);
      });

   $('.service-table-client tr').each(function(){
   //alert("inside others");
       var c = $(this).find('td:nth-child(2)').find('.js-small');
       if( c.is(':checked') ) {
         c.closest('tr').find('td:not(:nth-child(2)) .js-small').next().removeClass('disabled');
       } else {
         c.closest('tr').find('td:not(:nth-child(2)) .js-small').next().addClass('disabled');
       }
         
     });

     $(document).on('change', '#service td:nth-child(2) .js-small', function(){
         //alert("inside Suganyacode");
      if($(this).is(':checked')) {
         $(this).parents('tr').find('td:not(:nth-child(2)) .js-small').next().removeClass('disabled');
      } else {
         $(this).parents('tr').find('td:not(:nth-child(2)) .js-small').next().addClass('disabled');
         $(this).parents('tr').find('.js-small:checked').click();
      }
   
      });



      


      function clear_content_elements( selecter ,plugin_element) {
        $(document).find( selecter ).find(':input').each(function() {
          switch(this.type) {
              case 'password':
              case 'text':
              case 'textarea':
              case 'file':
              case 'select-one':
              case 'select-multiple':
              case 'date':
              case 'number':
              case 'tel':
              case 'email':
              case 'hidden':
                  jQuery(this).val('');
                  break;
              case 'checkbox':
              case 'radio':
                  this.checked = false;
                  break;
          }
        });
        
        $(document).find( selecter ).find('select').each(function() {
          $(this).prop('selectedIndex',0);
        });

        if( plugin_element['tinymce'] != '' )
        {
          tinymce.get( plugin_element['tinymce'] ).setContent( '' );
        }
      }






      //$('.js-small').change(function() {
        $(document).on('change','.js-small',function(){ 
     
    
     
        // var $grid = $('.masonry-container').masonry( 'reload' );
     
       $(this).closest('div').find('.field').prop('disabled', !$(this).is(':checked'));

       var check= $(this).data('id');
       var check1= $(this).attr('id');  

       //console.log("inside The funtion"+check);
       /** 10-09-2018 **/
       /*others tab login detais toggle*/
            if($(this).is(':checked') && (check=="show_login"))
             {
             // alert("asas")
              $(".show_login").show();
             }else if(check=="show_login")
             {
                    $(".show_login").hide();
             }
     /*others tab login detais toggle*/

           /*for personal tax return adtional income notes*/       
    
        if( check == "property_income" && $(this).is(':checked') )
          {
            $(this).closest('.panel-collapse').find(".property_income_notes").show();
            
          }
          else if( check == "property_income" )
          {
            $(this).closest('.panel-collapse').find(".property_income_notes").hide();
          }         
     
      /*for personal tax return adtional income notes*/
 
      
       /*bellow code for create_task_reminder check box inside all service tab { */
       
       // task investigation 
       if($(this).is(':checked')&&(check1=='investigation_create_task_reminder')){
       // alert('reminder chacked');
         $('.investigation_add_custom_reminder_label').css('display','');
       }
       else if(check1=='investigation_create_task_reminder')
       {
         $('.investigation_add_custom_reminder_label').css('display','none');
       }
                 // cissub 
       if($(this).is(':checked')&&(check1=='cissub_create_task_reminder'))
       {
         $('.cissub_add_custom_reminder_label').css('display','');
       }
       else if(check1=='cissub_create_task_reminder')
       {
         $('.cissub_add_custom_reminder_label').css('display','none');
       }
            // register 
       if($(this).is(':checked')&&(check1=='registered_create_task_reminder'))
       {
         $('.registered_add_custom_reminder_label').css('display','');
       }
       else if(check1=='registered_create_task_reminder')
       {
         $('.registered_add_custom_reminder_label').css('display','none');
       }
       // bookkeep 
       if($(this).is(':checked')&&(check1=='bookkeep_create_task_reminder')){
         $('.bookkeep_add_custom_reminder_label').css('display','');
       }
       else if(check1=='bookkeep_create_task_reminder')
       {
         $('.bookkeep_add_custom_reminder_label').css('display','none');
       }
       // managemnt 
       if($(this).is(':checked')&&(check1=='manage_create_task_reminder')){
         $('.manage_add_custom_reminder_label').css('display','');
       }
       else if(check1=='manage_create_task_reminder')
       {
         $('.manage_add_custom_reminder_label').css('display','none');
       }
       // p11d 
       if($(this).is(':checked')&&(check1=='p11d_create_task_reminder')){
         $('.p11d_add_custom_reminder_label').css('display','');
       }
       else if(check1=='p11d_create_task_reminder')
       {
         $('.p11d_add_custom_reminder_label').css('display','none');
       }
        // cis
       if($(this).is(':checked')&&(check1=='cis_create_task_reminder')){
         $('.cis_add_custom_reminder_label').css('display','');
       }
       else if(check1=='cis_create_task_reminder')
       {
         $('.cis_add_custom_reminder_label').css('display','none');
       }
       // workplace pension ae
       if($(this).is(':checked')&&(check1=='pension_create_task_reminder')){
         $('.pension_create_task_reminder_label').css('display','');
       }
       else if(check1=='pension_create_task_reminder')
       {
         $('.pension_create_task_reminder_label').css('display','none');
       }
        // confirmation statement
       if($(this).is(':checked')&&(check1=='create_task_reminder')){
         $('.custom_remain').css('display','');
       }
       else if(check1=='create_task_reminder')
       {
         $('.custom_remain').css('display','none');
       }
        // PayRole
       if($(this).is(':checked')&&(check1=='payroll_create_task_reminder')){
         $('.payroll_add_custom_reminder_label').css('display','');
       }
       else if(check1=='payroll_create_task_reminder')
       {
         $('.payroll_add_custom_reminder_label').css('display','none');
       }
       // vat
       if($(this).is(':checked')&&(check1=='vat_create_task_reminder')){
         $('.vat_add_custom_reminder_label').css('display','');
       }
       else if(check1=='vat_create_task_reminder')
       {
         $('.vat_add_custom_reminder_label').css('display','none');
       }
       // personal tax
       if($(this).is(':checked')&&(check1=='personal_task_reminder')){
         $('.personal_custom_reminder_label').css('display','');
       }
       else if(check1=='personal_task_reminder')
       {
         $('.personal_custom_reminder_label').css('display','none');
       }
       //accounts
       if($(this).is(':checked')&&(check1=='accounts_create_task_reminder')){
         $('.accounts_custom_reminder_label').css('display','');
       }
       else if(check1=='accounts_create_task_reminder')
       {
         $('.accounts_custom_reminder_label').css('display','none');
       }
       /** 07-09-2018 **/
       // insurance
       if($(this).is(':checked')&&(check1=='insurance_create_task_reminder')){
         $('.insurance_add_custom_reminder_label').css('display','');
       }
       else if(check1=='insurance_create_task_reminder')
       {
         $('.insurance_add_custom_reminder_label').css('display','none');
       }
       // company tax
       if($(this).is(':checked')&&(check1=='company_create_task_reminder')){
         $('.company_custom_reminder_label').css('display','');
       }
       else if(check1=='company_create_task_reminder')
       {
         $('.company_custom_reminder_label').css('display','none');
       }

  /*above code for create_task_reminder check box inside all service tab } */

     
     /*{ below code for inside the other tab amlcheck checkbox*/
        if($(this).is(':checked')&&(check1=='client_id_verified')){ // amlcheck checkbox change enabled 
          $('.client_id_verified_data').css('display','');
          var person=$('#person').val();
          //alert(person);
          if(person!=''){
          
              result = person.toString().split(',');
              if($.inArray("Other_Custom",result)!=-1)
              {
              $('.for_other_custom_choose').css('display','');
              }
              else{
              $('.for_other_custom_choose').css('display','none');
              }
          
           }

        }
        else if(check1=='client_id_verified')
        {
           $('.client_id_verified_data').css('display','none');
           $('.for_other_custom_choose').css('display','none'); // for for other custom textbox hide
        }
  /* } above code for inside the other tab amlcheck checkbox*/



     /*{ below code for inside the other tab previous account dettails*/

     if($(this).is(':checked')&&(check=='preacc'))
     {
     
        $(".preacc_content_toggle").show();
        var clickCheckbox = document.querySelector('#chase_for_info');

        if(clickCheckbox.checked) // true
        {
          $(".for_other_notes").css('display','');
        }

     } 
     else if(check=='preacc') 
     {
     
        $(".preacc_content_toggle").attr("style", "display:none");
        $(".for_other_notes").css('display','none');
     
     }
     /* } above code for inside the other tab previous account dettails*/


     /**{ below code for other tab-->prvious-->cahse_info **/
     if($(this).is(':checked')&&(check=='chase_for_info'))
     {
        $(".for_other_notes").css('display','');
     }
     else if(check=='chase_for_info') 
     {
        $(".for_other_notes").css('display','none');
     }
     /** } above code for  other tab-->prvious-->cahse_info **/

   
    


  /*{ below code for toggle all service tab inside the importent tab  */

      if($(this).is(':checked')&&(check=='accounts'))
      {
        $("#accounts").parents('li').show();
        $("tr#service_assignee_tr_2").show();         
        checking('serviceCheckbox');  
      }
      else if(check=='accounts')
      {     
        $("#accounts").parents('li').hide();     
        $("tr#service_assignee_tr_2").hide(); 
        unchecking('serviceCheckbox');
      }

      //bookkeep section enable/disable
     if($(this).is(':checked') && (check=='bookkeep'))
     {     
       $("#bookkeeptab").parents('li').show();
       $("tr#service_assignee_tr_10").show();
       checking('serviceCheckbox'); 
     }
     else if(check=='bookkeep') 
     {

      $("#bookkeeptab").parents('li').hide();
      $("tr#service_assignee_tr_10").hide();     
      unchecking('serviceCheckbox');
     }

     if($(this).is(':checked')&&(check=='cons'))
      {
          $("#confirmation-statement").parents('li').show();
          $("tr#service_assignee_tr_1").show();
          checking('serviceCheckbox');        
      }
      else if(check=='cons')
      {
         $("#confirmation-statement").parents('li').hide();
         $("tr#service_assignee_tr_1").hide();
          unchecking('serviceCheckbox');

      }

     if($(this).is(':checked')&&(check=='companytax'))
     {   
         $("#companytax").parents('li').show();
         $("tr#service_assignee_tr_3").show();
         checking('serviceCheckbox');
     }
     else if(check=='companytax') 
     {            
        $("#companytax").parents('li').hide();
        $("tr#service_assignee_tr_3").hide();   
        unchecking('serviceCheckbox');

     }

     //cis  section enable/disable
     if($(this).is(':checked')&&(check=='cis'))
     {
        $("#contratab").parents('li').show();
        $("tr#service_assignee_tr_8").show();
        checking('serviceCheckbox');    
     }
     else if(check=='cis') 
     {
    
      $("#contratab").parents('li').hide();
      $("tr#service_assignee_tr_8").hide();
      unchecking('serviceCheckbox');

     
     }
     //subcis  section enable/disable
     if($(this).is(':checked')&&(check=='cissub'))
     {
        $("#sub_contratab").parents('li').show();
        $("tr#service_assignee_tr_9").show();
        checking('serviceCheckbox');    
     }
     else if(check=='cissub') 
     {
    
      $("#sub_contratab").parents('li').hide();
      $("tr#service_assignee_tr_9").hide();
      unchecking('serviceCheckbox');

     
     }
     
     if($(this).is(':checked')&&(check=='investgate'))
     {
        $("#investigation-insurance").parents('li').show();
        $("tr#service_assignee_tr_13").show();
        checking('serviceCheckbox');     
     }
     else if(check=='investgate') 
     {
        $("#investigation-insurance").parents('li').hide();        
        $("tr#service_assignee_tr_13").hide();
        unchecking('serviceCheckbox');

     }  

      if($(this).is(':checked')&&(check=='management')){
        
        $("#management-account").parents('li').show();
        $('tr#service_assignee_tr_12').show();
        checking('serviceCheckbox');   
     }
     else if(check=='management') 
     {
        $('#management-account').parents('li').hide();
        $('tr#service_assignee_tr_12').hide();
        unchecking('serviceCheckbox');

     } 
     
     if($(this).is(':checked')&&(check=='payroll')){
     
        $("#payroll").parents('li').show();    
        $("tr#service_assignee_tr_6").show();
        checking('serviceCheckbox');   
     
      // $(".LoadingImage").hide(); 
      } 
      else if(check=='payroll')
      {
             
        $("#payroll").parents('li').hide();
        $("tr#service_assignee_tr_6").hide();
         unchecking('serviceCheckbox');
    
      }

      if($(this).is(':checked')&&(check=='personaltax'))
      {
      $("#personal-tax-returns").parents('li').show();
      $("tr#service_assignee_tr_4").show();
        checking('serviceCheckbox');   
      }
      else if(check=='personaltax')
      {
        $("#personal-tax-returns").parents('li').hide();   
        $("tr#service_assignee_tr_4").hide();
        unchecking('serviceCheckbox');

      }
     
       //p11d  section enable/disable
     if($(this).is(':checked')&&(check=='p11d'))
     {
     
           $("#p11dtab").parents('li').show();
           $('tr#service_assignee_tr_11').show();
           checking('serviceCheckbox');      
     }
     else if(check=='p11d') 
     {     
        $("#p11dtab").parents('li').hide();
        $('tr#service_assignee_tr_11').hide();      
        // $(".LoadingImage").hide(); 
        unchecking('serviceCheckbox');

     }

     //registered section enable/disable
     if($(this).is(':checked')&&(check=='registered'))
     {  
          $("#registeredtab").parents('li').show();
          $("tr#service_assignee_tr_14").show();
          checking('serviceCheckbox');    
     }
     else if(check=='registered')
     {   
        $("#registeredtab").parents('li').hide();
        $("tr#service_assignee_tr_14").hide();

        unchecking('serviceCheckbox');
     }

     //Beneficial Ownership Registration - Overseas section enable/disable
     if($(this).is(':checked')&&(check=='taxadvice' || check=='taxinvest'))
     {
     
        $("#taxadvicetab").parents('li').show();
        $("tr#service_assignee_tr_15").show();
        checking('serviceCheckbox');   
     } 
     else if(check=='taxadvice' || check=='taxinvest')
     {      
        $("#taxadvicetab").parents('li').hide();
        $("tr#service_assignee_tr_15").hide();

        unchecking('serviceCheckbox');
     }

     if($(this).is(':checked')&&(check=='vat'))
     {
     
        $("#vat-Returns").parents('li').show();
        $('tr#service_assignee_tr_5').show();
        checking('serviceCheckbox');
        //     $(".LoadingImage").hide(); 
        // $(".it8 a").click();
      }
      else if(check=='vat')
      {     
        $("#vat-Returns").parents('li').hide();
        $('tr#service_assignee_tr_5').hide();
        unchecking('serviceCheckbox');

      }
   
     //workplace pension section enable/disable
     if($(this).is(':checked')&&(check=='workplace'))
     {   
        $("#worktab").parents('li').show();        
        $('tr#service_assignee_tr_7').show();
        checking('serviceCheckbox');
     }
     else if(check=='workplace') 
     {
       
        $("#worktab").parents('li').hide();  
        $('tr#service_assignee_tr_7').hide();
        unchecking('serviceCheckbox');

         
     }


     if( $(this).is(':checked') && $(this).is('.services.ccp') )
     {   
        var data_id = $(this).attr('data-id');

        $("#"+data_id+"_personal_tax_returns").parents('li').show();        
        var cnt = data_id.split('_');

        $(document).find("select[name='personal_tax_return_required["+cnt[1]+"]'").val( 'yes' ); 
               
        $('tr#service_assignee_tr_4').show();
        checking('serviceCheckbox');
     }
     else if( $(this).is('.services.ccp') ) 
     { 
        var data_id = $(this).attr('data-id');
        $("#"+data_id+"_personal_tax_returns").parents('li').hide();
        var cnt = data_id.split('_');

        $(document).find("select[name='personal_tax_return_required["+cnt[1]+"]'").val( 'no' );

        if( !$("input.services.ccp:checked").length ) 
        {
          $('tr#service_assignee_tr_4').hide();
        }
        unchecking('serviceCheckbox');

     }


  /*} above code for toggle all sevice tab inside the importent tab */



  /*{ below code for inside the payroll previos year chechbox */

     if($(this).is(':checked')&&(check=='preyear')){
     
      $("#enable_preyear").attr("style", "display:block");
     } else if(check=='preyear') {
        
      $("#enable_preyear").attr("style", "display:none");
     
     }
  /*} above code for inside the payroll previos year chechbox */

     

   
  /*{ below code for service reminder checkbox, show remider block inside inditual block*/

     // confirmation remainder
     if($(this).is(':checked')&&(check=='enableconf'))
     {   
      $(".enable_conf").css("display", "block");
      //enable as per firm service reminder check box
      $(".enable_conf").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
      checking('reminderCheckbox');
     } 
     else if(check=='enableconf')
     {      
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_conf") );       
      $(".enable_conf").attr("style", "display:none");
      unchecking('reminderCheckbox');
     }
     
     // Accounts remainder
     if($(this).is(':checked')&&(check=='enableacc'))
     {   
      $(".enable_acc").css("display", "block"); 
      //enable as per firm service reminder check box
      $(".enable_acc").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
      checking('reminderCheckbox');   
     }
     else if(check=='enableacc')
     {      
      //disable  both service reminder types
      

      toggle_ServiceReminder( $(".enable_acc") );
      $(".enable_acc").attr("style", "display:none");
      $(".enable_accdue").attr("style", "display:none");
      unchecking('reminderCheckbox');  
     }
     
     // company tax remainder
     if($(this).is(':checked')&&(check=='enabletax'))
     {   
        $(".enable_tax").css("display", "block");
        //enable as per firm service reminder check box
        $(".enable_tax").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
        checking('reminderCheckbox');
     } 
     else if(check=='enabletax')
     {      
        //disable  both service reminder types
        toggle_ServiceReminder( $(".enable_tax") );
        $(".enable_tax").attr("style", "display:none");
        unchecking('reminderCheckbox');   
     }    
     
     // Personal Tax Return remainder
     if($(this).is(':checked')&&(check=='enablepertax'))
     {   
        $(".enable_pertax").css("display", "block");
        //enable as per firm service reminder check box
        $(".enable_pertax").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
        checking();    
     } 
     else if(check=='enablepertax') 
     {     
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_pertax") );
      $(".enable_pertax").attr("style", "display:none");
      unchecking('reminderCheckbox');    
     }

     // payroll remainder
     if($(this).is(':checked')&&(check=='enablepay'))
     {   
        $(".enable_pay").css("display", "block");
        //enable as per firm service reminder check box
        $(".enable_pay").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
        checking('reminderCheckbox');        
     }
     else if(check=='enablepay') 
     {      
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_pay") );
      $(".enable_pay").attr("style", "display:none");
       unchecking('reminderCheckbox');    
     }

     // WorkPlace Pension - AE remainder
     if($(this).is(':checked')&&(check=='enablework'))
     {   
        $(".enable_work").css("display", "block");
        //enable as per firm service reminder check box
        $(".enable_work").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
        checking('reminderCheckbox');         
     }
     else if(check=='enablework')
     {      
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_work") );
      $(".enable_work").attr("style", "display:none");
       unchecking('reminderCheckbox');
     }

     // C.I.S Contractor remainder
     if($(this).is(':checked')&&(check=='enablecis')){
     
        $(".enable_cis").css("display", "block");
        //enable as per firm service reminder check box
        $(".enable_cis").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
        checking('reminderCheckbox');
         
     } else if(check=='enablecis') {
        
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_cis") );
      $(".enable_cis").attr("style", "display:none");
      unchecking('reminderCheckbox');
      
     }

     // C.I.S Sub Contractor remainder
     if($(this).is(':checked')&&(check=='enablecissub'))
     {
     
        $(".enable_cissub").css("display", "block");
        //enable as per firm service reminder check box
        $(".enable_cissub").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
        checking('reminderCheckbox');   
     }
     else if(check=='enablecissub')
     {      
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_cissub") );
      $(".enable_cissub").attr("style", "display:none");
      unchecking('reminderCheckbox');    
     }

     // P11D remainder
     if($(this).is(':checked')&&(check=='enableplld'))
     {
     
        $(".enable_plld").css("display", "block");
        //enable as per firm service reminder check box
        $(".enable_plld").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
        checking('reminderCheckbox');            
     }
     else if(check=='enableplld')
     {      
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_plld") );
      $(".enable_plld").attr("style", "display:none");
      unchecking('reminderCheckbox');    
     }

     // VAT Quarters remainder
     if($(this).is(':checked')&&(check=='enablevat')){
     
        $(".enable_vat").css("display", "block");
        //enable as per firm service reminder check box
        $(".enable_vat").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
        checking('reminderCheckbox');
     
      
     } else if(check=='enablevat') {
        
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_vat") );
      $(".enable_vat").attr("style", "display:none");
      unchecking('reminderCheckbox');
      
     }

     // Bookkeeping remainder
     if($(this).is(':checked')&&(check=='enablebook')){
     
        $(".enable_book").css("display", "block");
        //enable as per firm service reminder check box
        $(".enable_book").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
        checking('reminderCheckbox');
          
     } else if(check=='enablebook') {
        
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_book") );
      $(".enable_book").attr("style", "display:none");
      unchecking('reminderCheckbox');
      
     }
     // Management Accounts remainder
     if($(this).is(':checked')&&(check=='enablemanagement')){
     
        $(".enable_management").css("display", "block");
        //enable as per firm service reminder check box
        $(".enable_management").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
          checking('reminderCheckbox');
     }
     else if(check=='enablemanagement') {
        
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_management") );
      $(".enable_management").attr("style", "display:none");
      unchecking('reminderCheckbox');
      
     }
     
     // Investigation insurance remainder
     if($(this).is(':checked')&&(check=='enableinvest'))
     {
        $(".enable_invest").css("display", "block");//enable as per firm service reminder check box
        $(".enable_invest").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
        checking('reminderCheckbox');         
     }
     else if(check=='enableinvest')
     {      
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_invest") );
      $(".enable_invest").attr("style", "display:none");
      unchecking('reminderCheckbox');    
     }
     
     // registration add remainder
     if($(this).is(':checked')&&(check=='enablereg'))
     {
      $(".enable_reg").css("display", "block"); 
      //enable as per firm service reminder check box
      $(".enable_reg").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');  
         checking('reminderCheckbox');  
     }
     else if(check=='enablereg')
     {      
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_reg") );
      $(".enable_reg").attr("style", "display:none");
      unchecking('reminderCheckbox');    
     }

     // tax advice add remainder
     if($(this).is(':checked')&&(check=='enabletaxad' || check=='enabletaxinves'))
     {
       // alert('check');
        $(".enable_taxad").css("display", "block");      
        //enable as per firm service reminder check box
          $(".enable_taxad").find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
         checking('reminderCheckbox');       
     }
     else if(check=='enabletaxad' || check=='enabletaxinves')
     {      
      //disable  both service reminder types
      toggle_ServiceReminder( $(".enable_taxad") );
      $(".enable_taxad").attr("style", "display:none");
      unchecking('reminderCheckbox');    
     }

     // tax advice add remainder
     if( $(this).is(':checked') && $(this).is('.reminder, .ccp')  )
     {
        // alert('check');
        var data_id = $(this).attr('data-id');

        $("#"+data_id).css("display", "block");      
        //enable as per firm service reminder check box
        $("#"+data_id).find('input[type="checkbox"].as_per_firm').not(':checked').trigger('click');
        checking('reminderCheckbox');       
     }
     else if( $(this).is('.reminder, .ccp') )
     {
        var data_id = $(this).attr('data-id');      
        //disable  both service reminder types
        toggle_ServiceReminder( $("#"+data_id) );
        $("#"+data_id).attr("style", "display:none");
        unchecking('reminderCheckbox');    
     }

  /*{ above code for service reminder checkbox, show remider block inside inditual block*/


  /*{below code for service text checkbox, show addcutom remide check box inditual remider block*/

     //onet remainder
     if($(this).is(':checked')&&(check=='onet')){
     
      $(".onet").attr("style", "");
      checking('textChecbox');
     } else if(check=='onet') {
        
      $(".onet").attr("style", "display:none");
      unchecking('textChecbox');
     }
     //twot remainder
     if($(this).is(':checked')&&(check=='twot')){
     
      $(".twot").attr("style", "");
      checking('textChecbox');
     } else if(check=='twot') {
        
      $(".twot").attr("style", "display:none");
      unchecking('textChecbox');
     }
     
     //threet remainder
     if($(this).is(':checked')&&(check=='threet')){
     
      $(".threet").attr("style", "");
      checking('textChecbox');
     } else if(check=='threet') {
        
      $(".threet").attr("style", "display:none");
      unchecking('textChecbox');
     }
     
     //fourt remainder
     if($(this).is(':checked')&&(check=='fourt')){
     
      $(".fourt").attr("style", "");
      checking('textChecbox');
     } else if(check=='fourt') {
        
      $(".fourt").attr("style", "display:none");
      unchecking('textChecbox');
     }
     //fivet remainder
     if($(this).is(':checked')&&(check=='fivet')){
     
      $(".fivet").attr("style", "");
      checking('textChecbox');
     } else if(check=='fivet') {
        
      $(".fivet").attr("style", "display:none"); 
      unchecking('textChecbox'); 
     }
     
     //sixt remainder
     if($(this).is(':checked')&&(check=='sixt')){
     
      $(".sixt").attr("style", "");
      checking('textChecbox');
     } else if(check=='sixt') {
        
      $(".sixt").attr("style", "display:none");
      unchecking('textChecbox');  
     }
     
     //sevent remainder
     if($(this).is(':checked')&&(check=='sevent')){
     
      $(".sevent").attr("style", "");
      checking('textChecbox');
     } else if(check=='sevent') {
        
      $(".sevent").attr("style", "display:none"); 
      unchecking('textChecbox'); 
     }
     
     //eightt remainder
     if($(this).is(':checked')&&(check=='eightt')){
     
      $(".eightt").attr("style", "");
      checking('textChecbox');
     } else if(check=='eightt') {
        
      $(".eightt").attr("style", "display:none");
      unchecking('textChecbox');  
     }
     //ninet remainder
     if($(this).is(':checked')&&(check=='ninet')){
     
      $(".ninet").attr("style", "");
      checking('textChecbox');
     } else if(check=='ninet') {
        
      $(".ninet").attr("style", "display:none");  
      unchecking('textChecbox');
     }
     
     //tent remainder
     if($(this).is(':checked')&&(check=='tent')){
     
      $(".tent").attr("style", "");
      checking('textChecbox');
     } else if(check=='tent') {
        
      $(".tent").attr("style", "display:none"); 
      unchecking('textChecbox'); 
     }
     
     //elevent remainder
     if($(this).is(':checked')&&(check=='elevent')){
     
      $(".elevent").attr("style", "");
        checking('textChecbox'); 
     } else if(check=='elevent') {
        
      $(".elevent").attr("style", "display:none"); 
      unchecking('textChecbox'); 
     }
     
     //twelvet remainder
     if($(this).is(':checked')&&(check=='twelvet')){
     
      $(".twelvet").attr("style", "");
        checking('textChecbox');
     } else if(check=='twelvet') {
        
      $(".twelvet").attr("style", "display:none"); 
      unchecking('textChecbox'); 
     }
     
     //thirteent remainder
     if($(this).is(':checked')&&(check=='thirteent')){
     
      $(".thirteent").attr("style", "");
        checking('textChecbox');
     } else if(check=='thirteent') {
        
      $(".thirteent").attr("style", "display:none"); 
      unchecking('textChecbox'); 
     }
     
     //fourteent remainder
     if($(this).is(':checked')&&(check=='fourteent')){
     
      $(".fourteent").attr("style", "");
        checking('textChecbox');
     } else if(check=='fourteent') {
        
      $(".fourteent").attr("style", "display:none");
      unchecking('textChecbox');  
     }
     
     //fifteent remainder
     if($(this).is(':checked')&&(check=='fifteent')){
     
      $(".fifteent").attr("style", "");
        checking('textChecbox');
     } else if(check=='fifteent') {
        
      $(".fifteent").attr("style", "display:none");
      unchecking('textChecbox');  
     }
     
     //sixteent remainder
     if($(this).is(':checked')&&(check=='sixteent')){
     
      $(".sixteent").attr("style", "");
        checking('textChecbox');
     } else if(check=='sixteent') {
        
      $(".sixteent").attr("style", "display:none");
      unchecking('textChecbox');  
     }

     if( $(this).is(':checked') && $(this).is('.text, .ccp') )
     {
        checking('textChecbox');
     }
     else if( $(this).is('.text, .ccp') )
     {
      unchecking('textChecbox');  
     }
  /*} above code for service text checkbox, show addcutom remide check box inditual remider block*/   



  /*{ below code for service invoive checkbox, show invoic block in inditual service block*/

     //confirmation Statement Invoice
     if($(this).is(':checked')&&(check=='incs')){
     
      $(".incs").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='incs') {
        
      $(".incs").attr("style", "display:none");
      unchecking('invoicCheckbox');  
     }
     
      //Accounts Invoice
     if($(this).is(':checked')&&(check=='inas')){
     
      $(".inas").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='inas') {
        
      $(".inas").attr("style", "display:none");
      unchecking('invoicCheckbox');  
     }
     //company Invoice
     if($(this).is(':checked')&&(check=='inct')){
     
      $(".inct").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='inct') {
        
      $(".inct").attr("style", "display:none");
      unchecking('invoicCheckbox');  
     }
     //Personal Tax return Invoice
     if($(this).is(':checked')&&(check=='inpt')){
     
      $(".inpt").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='inpt') {
        
      $(".inpt").attr("style", "display:none");
      unchecking('invoicCheckbox');  
     }
     //Payroll Invoice
     if($(this).is(':checked')&&(check=='inpr')){
     
      $(".inpr").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='inpr') {
        
      $(".inpr").attr("style", "display:none");
      unchecking('invoicCheckbox');  
     }
     
     //Pension and declration return Invoice
     if($(this).is(':checked')&&(check=='inpn')){
     
      $(".inpn").attr("style", "");
      $(".indl").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='inpn') {
        
      $(".inpn").attr("style", "display:none");  
      $(".indl").attr("style", "display:none"); 
      unchecking('invoicCheckbox'); 
     }
     
     //CIS return Invoice
     
     if($(this).is(':checked')&&(check=='incis')){
     
      $(".incis").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='incis') {
        
      $(".incis").attr("style", "display:none");
      unchecking('invoicCheckbox');  
      
     }
     //CIS_SUB return Invoice
     
     if($(this).is(':checked')&&(check=='incis_sub')){
     
      $(".incis_sub").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='incis_sub') {
        
      $(".incis_sub").attr("style", "display:none");
      unchecking('invoicCheckbox');  
      
     }
     //P11d return Invoice
     if($(this).is(':checked')&&(check=='inp11d')){
     
      $(".inp11d").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='inp11d') {
        
      $(".inp11d").attr("style", "display:none");  
      
     }
     //VAT return Invoice
     if($(this).is(':checked')&&(check=='invt')){
     
      $(".invt").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='invt') {
        
      $(".invt").attr("style", "display:none"); 
      unchecking('invoicCheckbox'); 
      
     }
     
     //bookkeep return Invoice
     if($(this).is(':checked')&&(check=='inbook')){
     
      $(".inbook").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='inbook') {
        
      $(".inbook").attr("style", "display:none");  
      unchecking('invoicCheckbox');
     }
     //Management return Invoice
     if($(this).is(':checked')&&(check=='inma')){
     
      $(".inma").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='inma') {
        
      $(".inma").attr("style", "display:none");  
      unchecking('invoicCheckbox');
     }  
     
     //Investigation Insurance Invoice
     if($(this).is(':checked')&&(check=='ininves')){
     
      $(".ininves").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='ininves') {
        
      $(".ininves").attr("style", "display:none");  
      unchecking('invoicCheckbox');
     }
     //Registered Address Invoice
     if($(this).is(':checked')&&(check=='inreg')){
     
      $(".inreg").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='inreg') {
        
      $(".inreg").attr("style", "display:none");  
      unchecking('invoicCheckbox');
     }
     //Tax Advice Invoice
     if($(this).is(':checked')&&(check=='intaxadv')){
     
      $(".intaxadv").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='intaxadv') {
        
      $(".intaxadv").attr("style", "display:none");  
      unchecking('invoicCheckbox');
     }
     //Tax Investigation Invoice
     if($(this).is(':checked')&&(check=='intaxinves'))
     {
      //alert("intaxinves"+$(this).parents('tr').is(":hidden"));     
      $(".intaxinves").attr("style", "");
      checking('invoicCheckbox');
     } else if(check=='intaxinves') {
        
      $(".intaxinves").attr("style", "display:none");  
      unchecking('invoicCheckbox');
     }

     if( $(this).is(':checked') && $(this).is('.text, .ccp') )
     {
        checking('invoicCheckbox');
     }
     else if( $(this).is('.text, .ccp') )
     {
        unchecking('invoicCheckbox');
     }
     
  /*} above code for service invoive checkbox, show invoic block in inditual service block*/


  /*{ below code for add custom remider checkboc toggle,show custom remider popup*/

     // accounts reminder
     
     if( $(this).is(':checked') && ( check == 'accounts_cus' ) )
     {
        $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');     
        $('#add-reminder').modal({backdrop: 'static', keyboard: false});
        clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );
        $('#service_id').val('2');     
        $('.append_placeholder').html('<span target="message" class="add_merge">{crm_ch_yearend}</span><br><span target="message" class="add_merge">{crm_ch_accounts_next_due}</span><br><span target="message" class="add_merge">{crm_accounts_custom_reminder}</span><br>');
        $('#clicked_checkbox_name').val( $(this).attr('name') );  
        /*$("input[name='client_contacts_id']").val('');
        $('#email_template').val('');
        $('#editor').val(' ');
        $(".invoice-msg").val(' ');
        $('.due-days').val(' '); */
     }
     /*else if(check=='accounts_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
     
      
     }*/
     
     // confirmation reminder
     
     if($(this).is(':checked')&&(check=='confirm_cus'))
     {    
      $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');     
      $('#add-reminder').modal({backdrop: 'static', keyboard: false});
      clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );      
      $('#service_id').val('1');      
      $('.append_placeholder').html('<span target="message" class="add_merge">{crm_confirmation_statement_date}</span><br><span target="message" class="add_merge">{crm_confirmation_statement_due_date}</span><br><span target="message" class="add_merge">{crm_confirmation_add_custom_reminder}</span><br>');
      $('#clicked_checkbox_name').val($(this).attr('name'));    
     }
     /*else if(check=='confirm_cus')
     {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html(''); 
     }*/
     // company tax reminder
     
     if($(this).is(':checked')&&(check=='company_cus')){

      $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
      $('#add-reminder').modal({backdrop: 'static', keyboard: false});
      clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );      
      $('#clicked_checkbox_name').val($(this).attr('name'));      
      $('#service_id').val('3');
      $('.append_placeholder').html('<span target="message" class="add_merge">{crm_ch_yearend}</span><br><span target="message" class="add_merge">{crm_accounts_due_date_hmrc}</span><br><span target="message" class="add_merge">{crm_accounts_custom_reminder}</span><br>');
     }/* else if(check=='company_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
      
     }*/
     
     // personal tax reminder
     
     if($(this).is(':checked')&&(check=='personal_cus'))
     {     
        $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
        $('#add-reminder').modal({backdrop: 'static', keyboard: false});
        clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );
        $('#clicked_checkbox_name').val($(this).attr('name'));      
        $('#service_id').val('4');
        $('.append_placeholder').html('<span target="message" class="add_merge">{crm_personal_tax_return_date}</span><br><span target="message" class="add_merge">{crm_personal_custom_reminder}</span><br>');
        var contacts_id  = $(this).closest('div.LISTEN_CONTENT').find('input.client_contacts_id').val();
        $("input[name='client_contacts_id']").val( contacts_id );

     } /*else if(check=='personal_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
      
     }*/
     // payroll reminder
     
     if($(this).is(':checked')&&(check=='payroll_cus'))
     {
      $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
      $('#add-reminder').modal({backdrop: 'static', keyboard: false});
      clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );      
      $('#clicked_checkbox_name').val($(this).attr('name'));      
      $('#service_id').val('6');
      $('.append_placeholder').html('<span target="message" class="add_merge">{crm_payroll_run_date}</span><br><span target="message" class="add_merge">{crm_payroll_add_custom_reminder}</span><br>');
     }/* else if(check=='payroll_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
      
     }*/
     // workplace pension reminder
     
     if($(this).is(':checked')&&(check=='pension_cus'))
     {
      
      $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
      $('#add-reminder').modal({backdrop: 'static', keyboard: false});
      clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );
      $('#clicked_checkbox_name').val($(this).attr('name'));      
      $('#service_id').val('7');
      $('.append_placeholder').html('<span target="message" class="add_merge">{crm_pension_subm_due_date}</span><br><span target="message" class="add_merge">{crm_pension_add_custom_reminder}</span><br>');
     }/* else if(check=='pension_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
      
     }*/
     // investigation insurance reminder
     
     if($(this).is(':checked')&&(check=='insurance_cus'))
     {
      $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
      $('#add-reminder').modal({backdrop: 'static', keyboard: false});
      clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );
      $('#clicked_checkbox_name').val($(this).attr('name'));      
      $('#service_id').val('13');
      $('.append_placeholder').html('<span target="message" class="add_merge">{crm_declaration_of_compliance_due_date}</span><br><span target="message" class="add_merge">{crm_pension_add_custom_reminder}</span><br>');

     }/* else if(check=='insurance_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
     }*/
     
     // Registered reminder
     
     if($(this).is(':checked')&&(check=='registered_cus')){
      $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
      $('#add-reminder').modal({backdrop: 'static', keyboard: false});
      clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );
      $('#clicked_checkbox_name').val($(this).attr('name'));      
      $('#service_id').val('14');
      $('.append_placeholder').html('<span target="message" class="add_merge">{crm_declaration_of_compliance_due_date}</span><br><span target="message" class="add_merge">{crm_pension_add_custom_reminder}</span><br>');

     } /*else if(check=='registered_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
      
     }*/
     
     // Tax advice/investigation reminder
     
     if($(this).is(':checked')&&(check=='investigation_cus')){
      $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
      $('#add-reminder').modal({backdrop: 'static', keyboard: false});
      clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );
      $('#clicked_checkbox_name').val($(this).attr('name'));      
      $('#service_id').val('15');
      $('.append_placeholder').html('<span target="message" class="add_merge">{crm_declaration_of_compliance_due_date}</span><br><span target="message" class="add_merge">{crm_pension_add_custom_reminder}</span><br>');

     }/* else if(check=='investigation_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
      
     }*/
     
     // cis reminder
     
     if($(this).is(':checked')&&(check=='cis_cus'))
     {
        $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
        $('#add-reminder').modal({backdrop: 'static', keyboard: false});
        clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );        
        $('#clicked_checkbox_name').val($(this).attr('name'));      
        $('#service_id').val('8');
        $('.append_placeholder').html('<span target="message" class="add_merge">{crm_cis_contractor_start_date}</span><br><span target="message" class="add_merge">{crm_cis_add_custom_reminder}</span><br>');

     }/* else if(check=='cis_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
      
     }*/
     // cissub_cus reminder
     if($(this).is(':checked')&&(check=='cissub_cus')){
      $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
      $('#add-reminder').modal({backdrop: 'static', keyboard: false});
      clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );
      $('#clicked_checkbox_name').val($(this).attr('name'));      
      $('#service_id').val('9');
      $('.append_placeholder').html('<span target="message" class="add_merge">{crm_cis_subcontractor_start_date}</span><br><span target="message" class="add_merge">{crm_cis_add_custom_reminder}</span><br>');

     
     }/* else if(check=='cissub_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
      
     }*/
     // p11d_cus reminder
     if($(this).is(':checked')&&(check=='p11d_cus')){
      $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
      $('#add-reminder').modal({backdrop: 'static', keyboard: false});
      clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );
      $('#clicked_checkbox_name').val($(this).attr('name'));      
      $('#service_id').val('11');
      $('.append_placeholder').html('<span target="message" class="add_merge">{crm_next_p11d_return_due}</span><br><span target="message" class="add_merge">{crm_p11d_add_custom_reminder}</span><br>');

     }/* else if(check=='p11d_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
      
     }*/
     // bookkeep_cus reminder
     if($(this).is(':checked')&&(check=='bookkeep_cus')){
     $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
      $('#add-reminder').modal({backdrop: 'static', keyboard: false});
      clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );
      $('#clicked_checkbox_name').val($(this).attr('name'));      
      $('#service_id').val('12');
      $('.append_placeholder').html('<span target="message" class="add_merge">{crm_next_booking_date}</span><br><span target="message" class="add_merge">{crm_bookkeep_add_custom_reminder}</span><br>');

     }/* else if(check=='bookkeep_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
      
     }*/
     
     // manage_cus reminder
     
     if($(this).is(':checked')&&(check=='manage_cus')){
      $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
      $('#add-reminder').modal({backdrop: 'static', keyboard: false});
      clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );
      $('#clicked_checkbox_name').val($(this).attr('name'));      
      $('#service_id').val('12');
      $('.append_placeholder').html('<span target="message" class="add_merge">{crm_next_booking_date}</span><br><span target="message" class="add_merge">{crm_bookkeep_add_custom_reminder}</span><br>');

     }/* else if(check=='manage_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
     }*/
     
     //vat_cus reminder
     
     if($(this).is(':checked')&&(check=='vat_cus')){
      $(this).closest('.panel-collapse').find('input:checked.as_per_firm').trigger('click');           
      $('#add-reminder').modal({backdrop: 'static', keyboard: false});
      clear_content_elements( 'div#add-reminder', {'tinymce':'add_custom_reminder_content'} );
      $('#clicked_checkbox_name').val($(this).attr('name'));      
      $('#service_id').val('5');
      $('.append_placeholder').html('<span target="message" class="add_merge">{crm_vat_quarters}</span><br><span target="message" class="add_merge">{crm_vat_add_custom_reminder}</span><br>');

     }/* else if(check=='vat_cus') {
        
      $('#add-reminder').modal('hide');
      $('#service_id').val('');
      $('.append_placeholder').html('');
     }*/
     /*} above code for add custom remider checkboc toggle,show custom remider popup*/


     


     $( ".switching + td .switchery" ).on( "click", function() {
     
      $('.body-popup-content .datepicker, .body-popup-content .dob_picker').removeClass('hasDatepicker').removeData('datepicker').unbind().datepicker({ dateFormat: 'dd-mm-yy', showButtonPanel: false,
        //minDate:0,
       changeMonth: true,
          changeYear: true, beforeShow: function() {
              setTimeout(function() {
                  $('.ui-datepicker').css('z-index', 99999999999999);
     
              }, 0);
          }
           }).val();
     // $('.body-popup-content .datepicker, .body-popup-content .dob_picker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
     // $(".dob_picker, .datepicker")
     
     });
     
     
     $('.body-popup-content .datepicker, .body-popup-content .dob_picker').removeClass('hasDatepicker');
      $('.body-popup-content .datepicker, .body-popup-content .dob_picker').datepicker({ 
        dateFormat: 'yy-mm-dd',
        //minDate:0,
         changeMonth: true,
          changeYear: true,
        onSelect:function(selectedDate){
                  //    alert($(this).attr('name'));
                          $("input[name="+$(this).attr('name')+"]").each(function() {
                             $(this).attr('value',selectedDate);
                            });
                         }, });
     
     // var $grid = $('.body-popup-content  .masonry-container').masonry({
     //    itemSelector: '.body-popup-content  .accordion-panel',
     //    percentPosition: true,
     //    columnWidth: '.body-popup-content  .grid-sizer' 
     //  });
     
     
     
     // setTimeout(function(){ $grid.masonry('layout'); }, 600);
     
        
     
     
     
     
     //    $(document).on( 'click', '.body-popup-content .switchery', function() {
     //      setTimeout(function(){ $grid.masonry('layout'); }, 600);
     //     });
     
     //    $(document).on( 'click', '.switchery', function() {
     //    $('.accordion-panel').css("transform", "");
     //    });
     //     $(document).on( 'change', '#select_responsible_type', function() {
     //    $('.accordion-panel').css("transform", "");
     //    });
     //    $(document).on( 'click', '.addnewclient_pages1 nav li a', function() {
     //    $('.accordion-panel').css("transform", "");
     //    });
     
     //    $(document).on( 'click', 'td.splwitch .switchery', function() {
     
     //      $('.body-popup-content .accordion-panel').css("transform", "");
     
          
     
     //       var $grid = $('.body-popup-content  .masonry-container').masonry({
     //       itemSelector: '.body-popup-content  .accordion-panel',
     //       percentPosition: true,
     //       columnWidth: '.body-popup-content  .grid-sizer' 
     //     });
     
     //       $grid.masonry('reloadItems');
               
     //       setTimeout(function(){ $grid.masonry('layout'); }, 600);  
     
     //     });
     
     
     
        
         $('.body-popup-content .switchery').remove();
        var elem = Array.prototype.slice.call(document.querySelectorAll('.body-popup-content .js-small'));
     
             elem.forEach(function(html) {
                 var switchery = new Switchery(html, {
                     color: '#1abc9c',
                     jackColor: '#fff',
                     size: 'small'
                 });
             });
     
     });
     

});


function toggle_ServiceReminder(obj)
{
      obj.find('input[type="checkbox"]:checked.as_per_firm').trigger('click');   
      obj.find('input[type="checkbox"]:checked.cus_reminder').trigger('click');
}


/*{ below select all checking and unchecking function*/
function checking(Cbox)
      {
          if(Cbox=='serviceCheckbox')
          {
            //importent tab botton
            $('.nav-item#import_tab_li').show();
            //assignee table 
            $('div.Service_Assignee_Table').show();

            if($('.services:checked').length == $('.services').length )
            {
              var checkbox = $('#service th:nth-child(2) input');
              checkbox.prop('checked',true);                     
              switcheryElm[checkbox.attr("data-switcheryId")].setPosition();
              $('.checkall').parents('tr').find('th:not(:nth-child(2)) .switchery').removeClass('disabled');
            }

          }
           else  if(Cbox=='reminderCheckbox' && $('.reminder:checked').length == $('.reminder').length )
          {
            var checkbox = $('#service th:nth-child(3) input');
            checkbox.prop('checked',true);                     
            switcheryElm[checkbox.attr("data-switcheryId")].setPosition();
          }
          else  if(Cbox=='textChecbox' && $('.text:checked').length == $('.text').length )
          {
            var checkbox = $('#service th:nth-child(4) input');
            checkbox.prop('checked',true);                     
            switcheryElm[checkbox.attr("data-switcheryId")].setPosition();
          }
          else  if(Cbox == 'invoicCheckbox' && $('.invoice:checked').length == $('.invoice').length )
          {
            var checkbox = $('#service th:nth-child(5) input');
            checkbox.prop('checked',true);                     
            switcheryElm[checkbox.attr("data-switcheryId")].setPosition();
          }  
      }



      function unchecking(Cbox)
      {
         //console.log($('.services:checked').length);
         
         if(Cbox=='serviceCheckbox')
         {
         
            if($('.services:checked').length < $('.services').length )
           {
              var checkbox = $('#service th:nth-child(2) input');
              
              checkbox.prop('checked',false);
              
              switcheryElm[checkbox.attr("data-switcheryId")].setPosition();

              $('.checkall').parents('tr').find('th:not(:nth-child(2)) .js-small').each(function()
              {              
                 $(this).prop('checked',false);

                 switcheryElm[$(this).attr("data-switcheryid")].setPosition();

                 $(this). parent().find(".switchery").addClass('disabled','disabled');

              });

              
           }
           if(!$('.services:checked').length)
           {
              $('.nav-item#import_tab_li').hide();
              $('div.Service_Assignee_Table').hide();
           }
        }
        else if(Cbox=='reminderCheckbox' && $('.reminder:checked').length < $('.reminder').length )
         {

            
            var checkbox = $('#service th:nth-child(3) input');
            
            checkbox.prop('checked',false);
            
            switcheryElm[checkbox.attr("data-switcheryId")].setPosition();
         }
        else if(Cbox=='textChecbox' && $('.text:checked').length < $('.text').length )
         {

            var checkbox = $('#service th:nth-child(4) input');
            
            checkbox.prop('checked',false);
            
            switcheryElm[checkbox.attr("data-switcheryId")].setPosition();
         }
        else if(Cbox == 'invoicCheckbox' && $('.invoice:checked').length < $('.invoice').length )
         {

            var checkbox = $('#service th:nth-child(5) input');
            
            checkbox.prop('checked',false);
            
            switcheryElm[checkbox.attr("data-switcheryId")].setPosition();
         }
      }

      /*} above select all checking and unchecking function*/        

      $(document).on('change','.other_ser',function()
      { 
           var check = $(this).data('id'); 
           check_other_services(this,check);
      });   
 

      function check_other_services(obj,check)
      { 
         for(var j=0;j<other_services.length;j++)
         { 
            if($(obj).prop('checked') == true && (check == 'tab_'+other_services[j].services_subnames))
            { 
              $("#"+other_services[j].services_subnames).parents('li').show(); 
              $("#"+other_services[j].services_subnames).parents('li').find('.panel').show();
              checking('serviceCheckbox');  
            }
            else if( check=='tab_'+other_services[j].services_subnames )
            {    
              $("#"+other_services[j].services_subnames).parents('li').hide(); 
              $("#"+other_services[j].services_subnames).parents('li').find('.panel').hide(); 
              unchecking('serviceCheckbox');
            }   

            if($(obj).is(':checked')&&(check=='enable_'+other_services[j].services_subnames))
            {         
              $("input[name=crm_"+other_services[j].services_subnames+"_email_remainder").prop('checked',true);  
              switcheryElm[$("input[name=crm_"+other_services[j].services_subnames+"_email_remainder").attr("data-switcheryId")].setPosition();
              $("#"+other_services[j].services_subnames).parents('li').find('.panels').show();  
            }
            else if(check=='enable_'+other_services[j].services_subnames)
            {   
              $("input[name=crm_"+other_services[j].services_subnames+"_email_remainder").prop('checked',false); 
              switcheryElm[$("input[name=crm_"+other_services[j].services_subnames+"_email_remainder").attr("data-switcheryId")].setPosition();
              $("#"+other_services[j].services_subnames).parents('li').find('.panels').hide(); 
            }   
         }
      }

