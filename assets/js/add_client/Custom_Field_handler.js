
/*Custom Field Append And Ordering*/
  var arrange_SATR_custom_fields = function( contents ){

    console.log( "contents length"+contents.length );

    $( contents ).each(function(){
      
      console.log( "inside contents each" );

      var current_content =  $(this);

      current_content.find("div.SATR_custom_fields div.form-group").each(function(){

        var content   = $(this).attr('data-content');

        $(this).addClass('personal_sort');

        content_class = ".CONTENT_"+content.trim().toUpperCase();

        current_content.find( content_class ).append( $(this) );
      });

      
        var sub_content     = current_content.find('div.CONTENT_IMPORTANT_INFORMATION');
        var elements        = sub_content.children('.personal_sort').get();
        elements.sort(function(a, b) {
            var compA = $(a).attr('data-id');
            var compB = $(b).attr('data-id');
            return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
        });
        $.each(elements, function(idx, itm) { 
            sub_content.append( itm );
        });

        var sub_content = current_content.find('div.CONTENT_PERSONAL_TAX_RETURN');
        var elements = sub_content.children('.personal_sort').get();
        elements.sort(function(a, b) {
            var compA = $(a).attr('data-id');
            var compB = $(b).attr('data-id');
            return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
        });
        $.each(elements, function(idx, itm) { 
            sub_content.append(itm);
        });
        var sub_content = current_content.find('div.CONTENT_PERSONAL_TAX_RETURN_REMINDERS');
        var elements = sub_content.children('.personal_sort').get();
        elements.sort(function(a, b) { 
            var compA = $(a).attr('data-id');
            var compB = $(b).attr('data-id');
            return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
        });
        $.each(elements, function(idx, itm) {
            sub_content.append(itm);
        });
    });
  };
  
  function sorting_contact_person(parent)
  {  
    parent.find('.form-group').each(function(){ $(this).addClass('contact_sort'); });    
    var listitems16 = parent.children('.contact_sort').get();
    console.log('inside the funtion child length'+listitems16.length);
    listitems16.sort(function(a, b) { 
      var compA = $(a).attr('data-id');
      var compB = $(b).attr('data-id');
      return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
    })

    $.each(listitems16, function(idx, itm) {
      parent.append(itm);
    })
  }

  $( "div.add_custom_fields_section div.form-group").each(function( index ) {
  $(this).addClass('name_fields');
  //console.log( index + ": " + $( this ).attr('class')+ "ID :" +  $( this ).attr('data-id') );
  var goal=$( this ).attr('data-id');

  var section = $(this).attr('data-section');
  var content = $(this).attr('data-content');
  var content_class = '';
  if(content!='')
  {
   content_class = " .CONTENT_"+content.trim().toUpperCase();
  } 
  switch( section )
  { 
    case 'required_information':
    /*for view page we don't have required_information content*/

      if( $('#required_information').length )
      {
        $(this).addClass('required_information_sort');          
        $('#required_information'+content_class).append( $(this) );
      }
      else
      {
        $(this).addClass('sorting');
        $('#details .CONTENT_IMPORTANT_DETAILS').append( $(this) );
      }
    break;
    case 'important_details':
          $(this).addClass('sorting');          
          $('#details '+content_class).append( $(this) );
    break;
/*    case 'Contact_Person':

          $(this).addClass('contact_sort');          
          $('#contacts1'+content_class).append( $(this) );

    break;*/

    case 'others':

          $(this).addClass('others_details_sort');
          $('#other'+content_class).append( $(this) );
    break;
    case 'AML_check':

          $(this).addClass('aml_sort');          
          $('#amlchecks'+content_class).append( $(this) );
    break; 
    case 'referral':
          $(this).addClass('referal_details_sort');          
          $('#referral'+content_class).append( $(this) );
    break;
    case '2':
          $(this).addClass('accounts_sec');
          $('#accounts'+content_class).append( $(this) );
    break;

    case '10':
          $(this).addClass('bookkeep_sort');
          $('#bookkeeptab'+content_class).append( $(this) );
    break;

    case '1':
          $(this).addClass('sorting_conf');          
          $('#confirmation-statement'+content_class).append( $(this) );
    break;

    case '3':
          $(this).addClass('company_tax_return_sec');          
          $('#companytax'+content_class).append( $(this) );
    break;

    case '8':
          $(this).addClass('contratab_sort');          
          $('#contratab'+content_class).append( $(this) );
    break;
    case '9':
          $(this).addClass('sub_contratab_sort');          
          $('#sub_contratab'+content_class).append( $(this) );
    break;
    case '13':
          $(this).addClass('invest_sort');
          
          $('#investigation-insurance'+content_class).append( $(this) );
    break;
    case '12':
          $(this).addClass('management_sort');          
          $('#management-account'+content_class).append( $(this) );
    break;
    case '6':
          $(this).addClass('payroll_sec');
          $('#payroll'+content_class).append( $(this) );
    break;
    /*case '4':
          $(this).addClass('personal_sort');
          $('.ccp_ptr_cnt.masonry-container'+content_class).append( $(this) );
    break;*/
    case '11':
          $(this).addClass('p11d_sort');
          $('#p11dtab'+content_class).append( $(this) );   
    break;
    case '14':
          $(this).addClass('registered_sort');
          $('#registeredtab'+content_class).append( $(this) );  
    break;
    case '15':
          $(this).addClass('taxadvice_sort');
          $('#taxadvicetab'+content_class).append( $(this) ); 
    break;
    case '5':

          $(this).addClass('vat_sort');
          $('#vat-Returns'+content_class).append( $(this) ); 
    break;
    case '7':

          $(this).addClass('workplace_sort');
          $('#worktab'+content_class).append( $(this) ); 
    break;      
  }

});

var mylist16 = $('#contacts1').find('.CONTENT_CONTACT_PERSON');
console.log('length off contact person'+mylist16.length);
mylist16.each(function(){  
  sorting_contact_person( $(this) );
});


var mylist16 = $('#required_information').find('.CONTENT_REQUIRED_INFORMATION');
var listitems16 = $('#required_information').find('.CONTENT_REQUIRED_INFORMATION').children('.required_information_sort').get();
listitems16.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems16, function(idx, itm) {
 //  console.log(idx);
    mylist16.append(itm);
})

var mylist = $('#details').find('#important_details_section');
var listitems = $('#details').find('#important_details_section').children('.sorting').get();
//console.log(  listitems);
listitems.sort(function(a, b) {

    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    //console.log(compA +"compare"+ compB );
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
});
    //console.log("typeof"+ typeof(listitems) +"Item length" +listitems.length);    

$.each(listitems, function(idx, itm) { 
    //console.log($(itm).attr("data-id") +"Item");    
    mylist.append(itm);
    if(idx==2)
    {
      mylist.append('<div class="clearfix"></div>');
    }
});

var mylist16 = $('#amlchecks').find('.CONTENT_ANTI_MONEY_LAUNDERING_CHECKS');

var listitems16 = $('#amlchecks').find('.CONTENT_ANTI_MONEY_LAUNDERING_CHECKS').children('.aml_sort').get();
console.log( "aml content length=="+ listitems16.length );
listitems16.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})

console.log( listitems16 );

$.each(listitems16, function(idx, itm) {
 //  console.log(idx);
    console.log( $(itm).data('id') );
    mylist16.append(itm);
})

var mylist12 = $('#other').find('#others_box1');
var listitems12 = $('#other').find('#others_box1').children('.others_details_sort').get();
listitems12.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems12, function(idx, itm) {
    console.log(idx+"other tab content"+ $(itm).attr("data-id") );
    mylist12.append(itm);
})
var mylist14 = $('#other').find('#others_box2');
var listitems14 = $('#other').find('#others_box2').children('.others_details_sort').get();
listitems14.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems14, function(idx, itm) {
 //  console.log(idx);
    mylist14.append(itm);
})

var mylist14 = $('#other').find('#others_box3');
var listitems14 = $('#other').find('#others_box3').children('.others_details_sort').get();
listitems14.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems14, function(idx, itm) {
 //  console.log(idx);
    mylist14.append(itm);
})
var mylist14 = $('#other').find('#others_box4');
var listitems14 = $('#other').find('#others_box4').children('.others_details_sort').get();
listitems14.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems14, function(idx, itm) {
 //  console.log(idx);
    mylist14.append(itm);
})
var mylist14 = $('#other').find('#others_box5');
var listitems14 = $('#other').find('#others_box5').children('.others_details_sort').get();
listitems14.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems14, function(idx, itm) {
 //  console.log(idx);
    mylist14.append(itm);
})
var mylist15 = $('#referral').find('#referals_box1');
var listitems15 = $('#referral').find('#referals_box1').children('.referal_details_sort').get();
listitems15.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems15, function(idx, itm) {
  // console.log(idx);
    mylist15.append(itm);
})

/* Accounts */
var mylist16 = $('#accounts').find('#accounts_box');
var listitems16 = $('#accounts').find('#accounts_box').children('.accounts_sec').get();
listitems16.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems16, function(idx, itm) {
 //  console.log(idx);
    mylist16.append(itm);
})
var mylist17 = $('#accounts').find('#accounts_box1');
var listitems17 = $('#accounts').find('#accounts_box1').children('.accounts_sec').get();
listitems17.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems17, function(idx, itm) {
 //  console.log(idx);
    mylist17.append(itm);
})
var mylist18 = $('#accounts').find('#accounts_box2');
var listitems18 = $('#accounts').find('#accounts_box2').children('.accounts_sec').get();
listitems18.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems18, function(idx, itm) {
 //  console.log(idx);
    mylist18.append(itm);
})

var mylist9 = $('#bookkeeptab').find('#bookkeep_box1');
var listitems9 = $('#bookkeeptab').find('#bookkeep_box1').children('.bookkeep_sort').get();
listitems9.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems9, function(idx, itm) {
    mylist9.append(itm);
})
var mylist10 = $('#bookkeeptab').find('#bookkeep_box2');
var listitems10 = $('#bookkeeptab').find('#bookkeep_box2').children('.bookkeep_sort').get();
listitems10.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems10, function(idx, itm) {
    mylist10.append(itm);
})

var mylist1 = $('#confirmation-statement').find('#box');
var listitems1 = $('#confirmation-statement').find('#box').children('.sorting_conf').get();
listitems1.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems1, function(idx, itm) {
    mylist1.append(itm);
})

var mylist1 = $('#confirmation-statement').find('#box1');
var listitems1 = $('#confirmation-statement').find('#box1').children('.sorting_conf').get();
listitems1.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems1, function(idx, itm) {
    mylist1.append(itm);
})
var mylist2 = $('#confirmation-statement').find('#box2');
var listitems2 = $('#confirmation-statement').find('#box2').children('.sorting_conf').get();
listitems2.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems2, function(idx, itm) { 
    mylist2.append(itm);
})
var mylist3 = $('#confirmation-statement').find('#box3');
var listitems3 = $('#confirmation-statement').find('#box3').children('.sorting_conf').get();
listitems3.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems3, function(idx, itm) { 
    mylist3.append(itm);
})







var mylist19 = $('#companytax').find('#companytax_box');
var listitems19 = $('#companytax').find('#companytax_box').children('.company_tax_return_sec').get();
listitems16.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems19, function(idx, itm) {
  // console.log(idx);
    mylist19.append(itm);
})

var mylist20 = $('#companytax').find('#companytax_box1');
var listitems20 = $('#companytax').find('#companytax_box1').children('.company_tax_return_sec').get();
listitems20.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems20, function(idx, itm) {
  // console.log(idx);
    mylist20.append(itm);
})



var mylist20 = $('#contratab').find('#contratab_box');
var listitems20 = $('#contratab').find('#contratab_box').children('.contratab_sort').get();
listitems20.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems20, function(idx, itm) {
  // console.log(idx);
    mylist20.append(itm);
})
var mylist20 = $('#contratab').find('#contratab_box2');
var listitems20 = $('#contratab').find('#contratab_box2').children('.contratab_sort').get();
listitems20.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems20, function(idx, itm) {
  // console.log(idx);
    mylist20.append(itm);
})


var mylist20 = $('#sub_contratab').find('#contratab_box3');
var listitems20 = $('#sub_contratab').find('#contratab_box3').children('.sub_contratab_sort').get();
listitems20.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems20, function(idx, itm) {
  // console.log(idx);
    mylist20.append(itm);
})

var mylist20 = $('#sub_contratab').find('#contratab_box1');
var listitems20 = $('#sub_contratab').find('#contratab_box1').children('.sub_contratab_sort').get();
listitems20.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems20, function(idx, itm) {
  // console.log(idx);
    mylist20.append(itm);
})




var mylist11 = $('#investigation-insurance').find('#invest-box');
var listitems11 = $('#investigation-insurance').find('#invest-box').children('.invest_sort').get();
listitems11.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems11, function(idx, itm) {
    mylist11.append(itm);
})
var mylist13 = $('#investigation-insurance').find('#invest_box1');
var listitems13 = $('#investigation-insurance').find('#invest_box1').children('.invest_sort').get();
listitems13.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems13, function(idx, itm) {
 //  console.log(idx);
    mylist13.append(itm);
})

var mylist7 = $('#management-account').find('#management_account');
var listitems7 = $('#management-account').find('#management_account').children('.management_sort').get();
listitems7.sort(function(a, b) {  
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems7, function(idx, itm) {
    mylist7.append(itm);
})
var mylist8 = $('#management-account').find('#management_account1');
var listitems8 = $('#management-account').find('#management_account1').children('.management_sort').get();
listitems8.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems8, function(idx, itm) {
    mylist8.append(itm);
})

var mylist21 = $('#payroll').find('#payroll_box');
var listitems21 = $('#payroll').find('#payroll_box').children('.payroll_sec').get();
listitems21.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems21, function(idx, itm) {
 //  console.log(idx);
    mylist21.append(itm);
})

var mylist22 = $('#payroll').find('#payroll_box1');
var listitems22 = $('#payroll').find('#payroll_box1').children('.payroll_sec').get();
listitems22.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems22, function(idx, itm) {
 //  console.log(idx);
    mylist22.append(itm);
})
var mylist23 = $('#payroll').find('#payroll_box2');
var listitems23 = $('#payroll').find('#payroll_box2').children('.payroll_sec').get();
listitems23.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems23, function(idx, itm) {
 //  console.log(idx);
    mylist23.append(itm);
})

/*
var mylist4 = $('#personal-tax-returns').find('#personal_box');
var listitems4 = $('#personal-tax-returns').find('#personal_box').children('.personal_sort').get();
listitems4.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems4, function(idx, itm) { 
    mylist4.append(itm);
})
var mylist4 = $('#personal-tax-returns').find('#personal_box1');
var listitems4 = $('#personal-tax-returns').find('#personal_box1').children('.personal_sort').get();
listitems4.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems4, function(idx, itm) { 
    mylist4.append(itm);
})
var mylist4 = $('#personal-tax-returns').find('#personal_box2');
var listitems4 = $('#personal-tax-returns').find('#personal_box2').children('.personal_sort').get();
listitems4.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems4, function(idx, itm) {
    mylist4.append(itm);
})
*/
var mylist26 = $('#p11dtab').find('#p11d_sec');
var listitems26 = $('#p11dtab').find('#p11d_sec').children('.p11d_sort').get();
listitems26.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems26, function(idx, itm) {
  // console.log(idx);
    mylist26.append(itm);
})
var mylist26 = $('#p11dtab').find('#p11d_sec1');
var listitems26 = $('#p11dtab').find('#p11d_sec1').children('.p11d_sort').get();
listitems26.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems26, function(idx, itm) {
  // console.log(idx);
    mylist26.append(itm);
})
var mylist26 = $('#registeredtab').find('#registeredtab_box');
var listitems26 = $('#registeredtab').find('#registeredtab_box').children('.registered_sort').get();
listitems26.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems26, function(idx, itm) {
  // console.log(idx);
    mylist26.append(itm);
})

var mylist26 = $('#registeredtab').find('#registeredtab_box1');
var listitems26 = $('#registeredtab').find('#registeredtab_box1').children('.registered_sort').get();
listitems26.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems26, function(idx, itm) {
  // console.log(idx);
    mylist26.append(itm);
})

var mylist26 = $('#taxadvicetab').find('#taxadvice_box');
var listitems26 = $('#taxadvicetab').find('#taxadvice_box').children('.taxadvice_sort').get();
listitems26.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems26, function(idx, itm) {
  // console.log(idx);
    mylist26.append(itm);
})

var mylist26 = $('#taxadvicetab').find('#taxadvice_box1');
var listitems26 = $('#taxadvicetab').find('#taxadvice_box1').children('.taxadvice_sort').get();
listitems26.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems26, function(idx, itm) {
  // console.log(idx);
    mylist26.append(itm);
})

var mylist5 = $('#vat-Returns').find('#vat_box0');
var listitems5 = $('#vat-Returns').find('#vat_box0').children('.vat_sort').get();
listitems5.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems5, function(idx, itm) {
  // console.log(idx);
    mylist5.append(itm);
})

var mylist5 = $('#vat-Returns').find('#vat_box');
var listitems5 = $('#vat-Returns').find('#vat_box').children('.vat_sort').get();
listitems5.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems5, function(idx, itm) {
  // console.log(idx);
    mylist5.append(itm);
})
var mylist6 = $('#vat-Returns').find('#vat_box1');
var listitems6 = $('#vat-Returns').find('#vat_box1').children('.vat_sort').get();
listitems6.sort(function(a, b) {  
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems6, function(idx, itm) { 
    mylist6.append(itm);
})

var mylist24 = $('#worktab').find('#workplace_sec');
var listitems24 = $('#worktab').find('#workplace_sec').children('.workplace_sort').get();
listitems24.sort(function(a, b) {
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems24, function(idx, itm) {
  // console.log(idx);
    mylist24.append(itm);
})
var mylist25 = $('#worktab').find('#workplace_sec1');
var listitems25 = $('#worktab').find('#workplace_sec1').children('.workplace_sort').get();
listitems25.sort(function(a, b) { 
    var compA = $(a).attr('data-id');
    var compB = $(b).attr('data-id');
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems25, function(idx, itm) {
//console.log(idx);
    mylist25.append(itm);
})
  /*Custom Field Append And Ordering*/
