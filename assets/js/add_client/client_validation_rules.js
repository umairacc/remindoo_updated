var check_firm_type_for_validation = function(element)
    {
      var selectVal=$("#legal_form").val();
      if((selectVal == "Private Limited company") || (selectVal == "Public Limited company") || (selectVal == "Limited Liability Partnership")) {
         return true;
       }
       else
       {
         return false;
       }
   };
   var check_previous_tab_open = function(element){
                               var clickCheckbox = document.querySelector('#previous_account');
                               if(clickCheckbox.checked) // true
                               {
                                 return true;
                               }
                               else
                               {
                                 return false;
                               }
                            };
   var  is_Login_required = function(element)
   {
         // console.log($("input[name='show_login']").prop("checked")+"login required");
        if($("input[name='invite_use']").prop("checked"))
        {
         return true;
        }
        else
        {
         return false;
        }
   };                
var Rules = {  
    company_name: {required: true},
    company_number:{required :check_firm_type_for_validation,remote:check_company_numper},
    company_status:{required: true},
    company_url:{url: true},
    officers_url:{url: true},
    append_cnt:{min:1},
    make_primary:{required:true},
    assignees_values : {required:true}, 
    emailid :
    {
        email:
        {
           depends:check_previous_tab_open
        } 
    }, 
    cun_code:
    {
      required:
      {
         depends:function()
         {
            if( $('input[name="pn_no_rec"]').val() != '' )
            {
               return true;
            }
            else
            {
               return false;
            }
         }
      }
   },
    pn_no_rec:
    {
      contact_no: {depends:check_previous_tab_open},
    },
    user_name:
    { 
      required:
      {
         depends:is_Login_required
      },
      remote:
      {
         param:check_username,
         depends:is_Login_required
      }
   },
    password:
    {
      required:
      {
         depends:is_Login_required
      },
      minlength :
      {
         param:5,
         depends:is_Login_required
      }
    },
    confirm_password :
   {
         required:
         {
            depends:is_Login_required
         },
         minlength :
         {
            param:5,
            depends:is_Login_required
         },
         equalTo :
         {  param:"#password",
            depends:is_Login_required
         }
    },
   confirmation_next_made_up_to:
   {
      required:
      {
         depends:function()
         {
            if($("input[name='confirm[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }     
         }
      },
      
   },
   accounts_next_made_up_to:
   {  
      required:
      {
         depends:function()
         {
            if($("input[name='accounts[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         }
      },         
   },
   accounts_due_date_hmrc:
   {  
      required:
      {

         depends:function()
         {
            if($("input[name='companytax[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         }
      },
      
   },
   personal_tax_return_date:
   {  
      required:
      {
         depends:function()
         {
            if($("input[name='personaltax[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }  
         }
      },
      
   },
   vat_quarters:
   {  
      required:
      {
         depends:function()
         {
            if($("input[name='vat[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }  
         }      
      },
      
   },
   vat_quater_end_date:
   {  
      required:
      {
         depends:function()
         {
            if($("input[name='vat[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }  
         }      
      },
      
   },
   payroll_run_date:
   {  
      required:
      {
         depends:function()
         { 
            if($("input[name='payroll[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         }              
      },
   },
   payroll_run:
   {  
      required:
      {
         depends:function()
         { 
            if($("input[name='payroll[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         }              
      },
   },
   pension_subm_due_date:
   {  
      required:
      {
         depends:function()
         {
            if($("input[name='workplace[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         }
      },
      
      
   },
   cis_contractor_start_date:
   {  
      required:
      {
         depends:function()
         { 
            if($("input[name='cis[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         } 
      },
   },
   crm_cis_frequency:
   {  
      required:
      {
         depends:function()
         { 
            if($("input[name='cis[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         } 
      },
   },
   cis_subcontractor_start_date:
   {  
      required:
      {
         depends:function()
         {
            if($("input[name='cissub[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         }

      },
      
   },
   crm_cis_subcontractor_frequency:
   {  
      required:
      {
         depends:function()
         {
            if($("input[name='cissub[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         }

      },
      
   },
   p11d_due_date:
   {  
      required:
      {
         depends:function()
         {
            if($("input[name='p11d[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         }
      },
      
   },
   next_manage_acc_date:
   {  
      required:
      {
         depends:function()
         {
            if($("input[name='management[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }     
         }
      }
      ,
      
   },
   manage_acc_fre:
   {  
      required:
      {
         depends:function()
         {
            if($("input[name='management[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }     
         }
      }
      ,
      
   },

   next_booking_date:
   {  
      required:
      {
         depends:function()
         { 
            if($("input[name='bookkeep[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         }
      },
      
      
   },
   bookkeeping:
   {  
      required:
      {
         depends:function()
         { 
            if($("input[name='bookkeep[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         }
      }, 
   },
   insurance_renew_date:
   {  
      required:
      {
         depends:function()
         {
            if($("input[name='investgate[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         }
      },
      
      
   },
   registered_renew_date:
   {  
      required:
      {
         depends:function()
         {
            if($("input[name='registered[tab]']").prop("checked"))
            {
            return true;
            }
            else
            {
            return false;
            }
         }
      },
      
   
   },
   investigation_end_date:
   {  
      required:
      {
            depends:function()
         { 
            if($("input[name='taxadvice[tab]']").prop("checked"))
            {       
            return true;
            }
            else
            {
          
            return false;
            }
         }
      },
      
   }  
};

var Message = 
{
   company_name:
   {
      required:"Company Name Required."
   },
   company_number:
   {
      required:"Company Number Required.",
      remote:"Company Number Already Exists."
   },
   company_status:
   {
      required:"Company Status Required."
   },
   append_cnt:"Please Add Contact Persons.",
   make_primary:
   {
      required:"Select any one Contact as Primary."
   },
   assignees_values:
   {
      required:"Please Select Any Assignee To The Client."
   },
   user_name:
   {  
      required:"User Name Required.",
      remote:"User Name Already Exists."
   },
   password:
   {  required:"Password Required.",
      minlength: "Password Must Be Greater Than 5."
   },
   confirm_password:
   {
      required:"Confirm Password Required.",
      minlength: "Confirm Password Must Be Greater Than 5.", 
      equalTo: "Confirm Password Dosen't Match With Password."
   },
   confirmation_next_made_up_to:
   {
      required:"Confirmation Date Required."
   },
   accounts_due_date_hmrc:
   {
      required:"Tax Return Date Required."
   },
   accounts_next_made_up_to:
   {
      required:"Accounts Date Required."
   },
   personal_tax_return_date:
   {
      required:"Personal Tax Return Date Required."
   },
   cis_contractor_start_date:
   {
      required:"CIS Date Required."
   },
   crm_cis_frequency:
   {
      required:"CIS Frequency Required."
   },
   cis_subcontractor_start_date:
   {
      required:"CIS Sub-Contractor Start  Date Required."
   },
   crm_cis_subcontractor_frequency:
   {
      required:"CIS Sub-Contractor Frequency Required."
   },
   payroll_run_date:
   {
     required:"Payroll Date Required." 
   },
   payroll_run:
   {
     required:"Payroll Frequency Required." 
   },
   pension_subm_due_date:
   {
     required:"Work Pension Submission Date Required." 
   },
   p11d_due_date:
   {
     required:"P11D Date Required." 
   },
   vat_quarters:
   {
     required:"Vat Quarters Required." 
   },
   vat_quater_end_date:
   {
      required:"Vat Quarters End Date Required."   
   },
   next_booking_date:
   {
     required:"Next Bookkeeping Date Required." 
   },
   bookkeeping:
   {
    required:"Bookkeeping Frequency Required." 
   },
   next_manage_acc_date:
   {
     required:"Next Management Date Required." 
   },
   manage_acc_fre:
   {
     required:"Management Frequency Required." 
   },
   insurance_renew_date:
   {
     required:"Insurance Renew Date Required." 
   },
   registered_renew_date:
   {
     required:"Registered Office Renew Date Required." 
   },
   investigation_end_date:
   {
     required:"Beneficial Ownership Registration - Overseas End Date Required." 
   }


};